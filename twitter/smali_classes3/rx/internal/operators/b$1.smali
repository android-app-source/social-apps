.class Lrx/internal/operators/b$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lrx/internal/operators/b;->a(Lrx/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcwv;

.field final synthetic b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic c:Lrx/b;

.field final synthetic d:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic e:Lrx/internal/operators/b;


# direct methods
.method constructor <init>(Lrx/internal/operators/b;Lcwv;Ljava/util/concurrent/atomic/AtomicBoolean;Lrx/b;Ljava/util/concurrent/atomic/AtomicInteger;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lrx/internal/operators/b$1;->e:Lrx/internal/operators/b;

    iput-object p2, p0, Lrx/internal/operators/b$1;->a:Lcwv;

    iput-object p3, p0, Lrx/internal/operators/b$1;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p4, p0, Lrx/internal/operators/b$1;->c:Lrx/b;

    iput-object p5, p0, Lrx/internal/operators/b$1;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 132
    iget-object v0, p0, Lrx/internal/operators/b$1;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 133
    iget-object v0, p0, Lrx/internal/operators/b$1;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lrx/internal/operators/b$1;->c:Lrx/b;

    invoke-interface {v0}, Lrx/b;->a()V

    .line 137
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 122
    iget-object v0, p0, Lrx/internal/operators/b$1;->a:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 123
    iget-object v0, p0, Lrx/internal/operators/b$1;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lrx/internal/operators/b$1;->c:Lrx/b;

    invoke-interface {v0, p1}, Lrx/b;->a(Ljava/lang/Throwable;)V

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_0
    invoke-static {p1}, Lcwl;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Lrx/j;)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lrx/internal/operators/b$1;->a:Lcwv;

    invoke-virtual {v0, p1}, Lcwv;->a(Lrx/j;)V

    .line 118
    return-void
.end method
