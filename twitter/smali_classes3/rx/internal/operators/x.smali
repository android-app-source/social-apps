.class public final Lrx/internal/operators/x;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/internal/operators/x$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/c$b",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final a:J

.field final b:Ljava/util/concurrent/TimeUnit;

.field final c:Lrx/f;


# direct methods
.method public constructor <init>(JLjava/util/concurrent/TimeUnit;Lrx/f;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-wide p1, p0, Lrx/internal/operators/x;->a:J

    .line 50
    iput-object p3, p0, Lrx/internal/operators/x;->b:Ljava/util/concurrent/TimeUnit;

    .line 51
    iput-object p4, p0, Lrx/internal/operators/x;->c:Lrx/f;

    .line 52
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lrx/internal/operators/x;->a(Lrx/i;)Lrx/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Lrx/i;)Lrx/i;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-TT;>;)",
            "Lrx/i",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lrx/internal/operators/x;->c:Lrx/f;

    invoke-virtual {v0}, Lrx/f;->a()Lrx/f$a;

    move-result-object v4

    .line 57
    new-instance v5, Lcwh;

    invoke-direct {v5, p1}, Lcwh;-><init>(Lrx/i;)V

    .line 58
    new-instance v3, Lcwx;

    invoke-direct {v3}, Lcwx;-><init>()V

    .line 60
    invoke-virtual {v5, v4}, Lcwh;->a(Lrx/j;)V

    .line 61
    invoke-virtual {v5, v3}, Lcwh;->a(Lrx/j;)V

    .line 63
    new-instance v0, Lrx/internal/operators/x$1;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lrx/internal/operators/x$1;-><init>(Lrx/internal/operators/x;Lrx/i;Lcwx;Lrx/f$a;Lcwh;)V

    return-object v0
.end method
