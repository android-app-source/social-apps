.class Lrx/internal/operators/p$2;
.super Lrx/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lrx/internal/operators/p;->a(Lrx/i;Lcwv;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/i",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lrx/i;

.field final synthetic b:Lcwv;

.field final synthetic c:Lrx/internal/operators/p;


# direct methods
.method constructor <init>(Lrx/internal/operators/p;Lrx/i;Lrx/i;Lcwv;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lrx/internal/operators/p$2;->c:Lrx/internal/operators/p;

    iput-object p3, p0, Lrx/internal/operators/p$2;->a:Lrx/i;

    iput-object p4, p0, Lrx/internal/operators/p$2;->b:Lcwv;

    invoke-direct {p0, p2}, Lrx/i;-><init>(Lrx/i;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lrx/internal/operators/p$2;->a:Lrx/i;

    invoke-virtual {v0, p1}, Lrx/i;->a(Ljava/lang/Object;)V

    .line 121
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0}, Lrx/internal/operators/p$2;->d()V

    .line 116
    iget-object v0, p0, Lrx/internal/operators/p$2;->a:Lrx/i;

    invoke-virtual {v0, p1}, Lrx/i;->a(Ljava/lang/Throwable;)V

    .line 117
    return-void
.end method

.method public by_()V
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0}, Lrx/internal/operators/p$2;->d()V

    .line 125
    iget-object v0, p0, Lrx/internal/operators/p$2;->a:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->by_()V

    .line 126
    return-void
.end method

.method d()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lrx/internal/operators/p$2;->c:Lrx/internal/operators/p;

    iget-object v0, v0, Lrx/internal/operators/p;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 132
    :try_start_0
    iget-object v0, p0, Lrx/internal/operators/p$2;->c:Lrx/internal/operators/p;

    iget-object v0, v0, Lrx/internal/operators/p;->a:Lcwv;

    iget-object v1, p0, Lrx/internal/operators/p$2;->b:Lcwv;

    if-ne v0, v1, :cond_0

    .line 133
    iget-object v0, p0, Lrx/internal/operators/p$2;->c:Lrx/internal/operators/p;

    iget-object v0, v0, Lrx/internal/operators/p;->a:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 134
    iget-object v0, p0, Lrx/internal/operators/p$2;->c:Lrx/internal/operators/p;

    new-instance v1, Lcwv;

    invoke-direct {v1}, Lcwv;-><init>()V

    iput-object v1, v0, Lrx/internal/operators/p;->a:Lcwv;

    .line 135
    iget-object v0, p0, Lrx/internal/operators/p$2;->c:Lrx/internal/operators/p;

    iget-object v0, v0, Lrx/internal/operators/p;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :cond_0
    iget-object v0, p0, Lrx/internal/operators/p$2;->c:Lrx/internal/operators/p;

    iget-object v0, v0, Lrx/internal/operators/p;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 140
    return-void

    .line 138
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lrx/internal/operators/p$2;->c:Lrx/internal/operators/p;

    iget-object v1, v1, Lrx/internal/operators/p;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
