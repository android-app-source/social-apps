.class public final Lrx/internal/operators/al;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/internal/operators/al$a;,
        Lrx/internal/operators/al$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/c$b",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lrx/internal/operators/al;->a:Lrx/c;

    .line 34
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lrx/internal/operators/al;->a(Lrx/i;)Lrx/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Lrx/i;)Lrx/i;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-TT;>;)",
            "Lrx/i",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Lcwx;

    invoke-direct {v0}, Lcwx;-><init>()V

    .line 39
    new-instance v1, Lrx/internal/producers/a;

    invoke-direct {v1}, Lrx/internal/producers/a;-><init>()V

    .line 40
    new-instance v2, Lrx/internal/operators/al$b;

    iget-object v3, p0, Lrx/internal/operators/al;->a:Lrx/c;

    invoke-direct {v2, p1, v0, v1, v3}, Lrx/internal/operators/al$b;-><init>(Lrx/i;Lcwx;Lrx/internal/producers/a;Lrx/c;)V

    .line 41
    invoke-virtual {v0, v2}, Lcwx;->a(Lrx/j;)V

    .line 42
    invoke-virtual {p1, v0}, Lrx/i;->a(Lrx/j;)V

    .line 43
    invoke-virtual {p1, v1}, Lrx/i;->a(Lrx/e;)V

    .line 44
    return-object v2
.end method
