.class final Lrx/internal/operators/bb$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/g$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lrx/internal/operators/bb;->a([Lrx/g;Lrx/functions/h;)Lrx/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/g$a",
        "<TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:[Lrx/g;

.field final synthetic b:Lrx/functions/h;


# direct methods
.method constructor <init>([Lrx/g;Lrx/functions/h;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lrx/internal/operators/bb$1;->a:[Lrx/g;

    iput-object p2, p0, Lrx/internal/operators/bb$1;->b:Lrx/functions/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lrx/h;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/h",
            "<-TR;>;)V"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lrx/internal/operators/bb$1;->a:[Lrx/g;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 40
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string/jumbo v1, "Can\'t zip 0 Singles."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lrx/h;->a(Ljava/lang/Throwable;)V

    .line 94
    :cond_0
    return-void

    .line 44
    :cond_1
    new-instance v4, Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v0, p0, Lrx/internal/operators/bb$1;->a:[Lrx/g;

    array-length v0, v0

    invoke-direct {v4, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 45
    new-instance v6, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    .line 46
    iget-object v0, p0, Lrx/internal/operators/bb$1;->a:[Lrx/g;

    array-length v0, v0

    new-array v2, v0, [Ljava/lang/Object;

    .line 48
    new-instance v7, Lcwv;

    invoke-direct {v7}, Lcwv;-><init>()V

    .line 49
    invoke-virtual {p1, v7}, Lrx/h;->a(Lrx/j;)V

    .line 51
    const/4 v3, 0x0

    :goto_0
    iget-object v0, p0, Lrx/internal/operators/bb$1;->a:[Lrx/g;

    array-length v0, v0

    if-ge v3, v0, :cond_0

    .line 52
    invoke-virtual {v7}, Lcwv;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Lrx/internal/operators/bb$1$1;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lrx/internal/operators/bb$1$1;-><init>(Lrx/internal/operators/bb$1;[Ljava/lang/Object;ILjava/util/concurrent/atomic/AtomicInteger;Lrx/h;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 86
    invoke-virtual {v7, v0}, Lcwv;->a(Lrx/j;)V

    .line 88
    invoke-virtual {v7}, Lcwv;->b()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    .line 92
    iget-object v1, p0, Lrx/internal/operators/bb$1;->a:[Lrx/g;

    aget-object v1, v1, v3

    invoke-virtual {v1, v0}, Lrx/g;->a(Lrx/h;)Lrx/j;

    .line 51
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    check-cast p1, Lrx/h;

    invoke-virtual {p0, p1}, Lrx/internal/operators/bb$1;->a(Lrx/h;)V

    return-void
.end method
