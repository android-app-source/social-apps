.class Lrx/internal/operators/ao$1;
.super Lrx/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lrx/internal/operators/ao;->a(Lrx/i;)Lrx/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/i",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lrx/i;

.field final synthetic b:Lrx/internal/operators/ao;

.field private c:J


# direct methods
.method constructor <init>(Lrx/internal/operators/ao;Lrx/i;Lrx/i;)V
    .locals 2

    .prologue
    .line 39
    iput-object p1, p0, Lrx/internal/operators/ao$1;->b:Lrx/internal/operators/ao;

    iput-object p3, p0, Lrx/internal/operators/ao$1;->a:Lrx/i;

    invoke-direct {p0, p2}, Lrx/i;-><init>(Lrx/i;)V

    .line 41
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lrx/internal/operators/ao$1;->c:J

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lrx/internal/operators/ao$1;->b:Lrx/internal/operators/ao;

    iget-object v0, v0, Lrx/internal/operators/ao;->b:Lrx/f;

    invoke-virtual {v0}, Lrx/f;->b()J

    move-result-wide v0

    .line 51
    iget-wide v2, p0, Lrx/internal/operators/ao$1;->c:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lrx/internal/operators/ao$1;->c:J

    sub-long v2, v0, v2

    iget-object v4, p0, Lrx/internal/operators/ao$1;->b:Lrx/internal/operators/ao;

    iget-wide v4, v4, Lrx/internal/operators/ao;->a:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 52
    :cond_0
    iput-wide v0, p0, Lrx/internal/operators/ao$1;->c:J

    .line 53
    iget-object v0, p0, Lrx/internal/operators/ao$1;->a:Lrx/i;

    invoke-virtual {v0, p1}, Lrx/i;->a(Ljava/lang/Object;)V

    .line 55
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lrx/internal/operators/ao$1;->a:Lrx/i;

    invoke-virtual {v0, p1}, Lrx/i;->a(Ljava/lang/Throwable;)V

    .line 65
    return-void
.end method

.method public by_()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lrx/internal/operators/ao$1;->a:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->by_()V

    .line 60
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 45
    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1}, Lrx/internal/operators/ao$1;->a(J)V

    .line 46
    return-void
.end method
