.class public final Lrx/internal/operators/ax;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/g$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/internal/operators/ax$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/g$a",
        "<TR;>;"
    }
.end annotation


# instance fields
.field final a:Lrx/g$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/g$a",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:Lrx/c$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c$b",
            "<+TR;-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/g$a;Lrx/c$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/g$a",
            "<TT;>;",
            "Lrx/c$b",
            "<+TR;-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lrx/internal/operators/ax;->a:Lrx/g$a;

    .line 40
    iput-object p2, p0, Lrx/internal/operators/ax;->b:Lrx/c$b;

    .line 41
    return-void
.end method

.method public static a(Lrx/i;)Lrx/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/i",
            "<TT;>;)",
            "Lrx/h",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lrx/internal/operators/ax$a;

    invoke-direct {v0, p0}, Lrx/internal/operators/ax$a;-><init>(Lrx/i;)V

    .line 63
    invoke-virtual {p0, v0}, Lrx/i;->a(Lrx/j;)V

    .line 64
    return-object v0
.end method


# virtual methods
.method public a(Lrx/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/h",
            "<-TR;>;)V"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Lrx/internal/operators/aw$a;

    invoke-direct {v0, p1}, Lrx/internal/operators/aw$a;-><init>(Lrx/h;)V

    .line 46
    invoke-virtual {p1, v0}, Lrx/h;->a(Lrx/j;)V

    .line 49
    :try_start_0
    iget-object v1, p0, Lrx/internal/operators/ax;->b:Lrx/c$b;

    invoke-static {v1}, Lcwl;->b(Lrx/c$b;)Lrx/c$b;

    move-result-object v1

    invoke-interface {v1, v0}, Lrx/c$b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/i;

    .line 51
    invoke-static {v0}, Lrx/internal/operators/ax;->a(Lrx/i;)Lrx/h;

    move-result-object v1

    .line 53
    invoke-virtual {v0}, Lrx/i;->c()V

    .line 55
    iget-object v0, p0, Lrx/internal/operators/ax;->a:Lrx/g$a;

    invoke-interface {v0, v1}, Lrx/g$a;->call(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    invoke-static {v0, p1}, Lrx/exceptions/a;->a(Ljava/lang/Throwable;Lrx/h;)V

    goto :goto_0
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lrx/h;

    invoke-virtual {p0, p1}, Lrx/internal/operators/ax;->a(Lrx/h;)V

    return-void
.end method
