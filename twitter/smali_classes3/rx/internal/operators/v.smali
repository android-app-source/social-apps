.class public final Lrx/internal/operators/v;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/internal/operators/v$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "TClosing:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/c$b",
        "<",
        "Ljava/util/List",
        "<TT;>;TT;>;"
    }
.end annotation


# instance fields
.field final a:Lrx/functions/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/c",
            "<+",
            "Lrx/c",
            "<+TTClosing;>;>;"
        }
    .end annotation
.end field

.field final b:I


# direct methods
.method public constructor <init>(Lrx/c;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<+TTClosing;>;I)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Lrx/internal/operators/v$1;

    invoke-direct {v0, p0, p1}, Lrx/internal/operators/v$1;-><init>(Lrx/internal/operators/v;Lrx/c;)V

    iput-object v0, p0, Lrx/internal/operators/v;->a:Lrx/functions/c;

    .line 73
    iput p2, p0, Lrx/internal/operators/v;->b:I

    .line 74
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lrx/internal/operators/v;->a(Lrx/i;)Lrx/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Lrx/i;)Lrx/i;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-",
            "Ljava/util/List",
            "<TT;>;>;)",
            "Lrx/i",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 80
    :try_start_0
    iget-object v0, p0, Lrx/internal/operators/v;->a:Lrx/functions/c;

    invoke-interface {v0}, Lrx/functions/c;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/c;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    new-instance v1, Lrx/internal/operators/v$a;

    new-instance v2, Lcwh;

    invoke-direct {v2, p1}, Lcwh;-><init>(Lrx/i;)V

    invoke-direct {v1, p0, v2}, Lrx/internal/operators/v$a;-><init>(Lrx/internal/operators/v;Lrx/i;)V

    .line 87
    new-instance v2, Lrx/internal/operators/v$2;

    invoke-direct {v2, p0, v1}, Lrx/internal/operators/v$2;-><init>(Lrx/internal/operators/v;Lrx/internal/operators/v$a;)V

    .line 105
    invoke-virtual {p1, v2}, Lrx/i;->a(Lrx/j;)V

    .line 106
    invoke-virtual {p1, v1}, Lrx/i;->a(Lrx/j;)V

    .line 108
    invoke-virtual {v0, v2}, Lrx/c;->a(Lrx/i;)Lrx/j;

    move-object v0, v1

    .line 110
    :goto_0
    return-object v0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    invoke-static {v0, p1}, Lrx/exceptions/a;->a(Ljava/lang/Throwable;Lrx/d;)V

    .line 83
    invoke-static {}, Lcwi;->a()Lrx/i;

    move-result-object v0

    goto :goto_0
.end method
