.class public final Lrx/internal/operators/af;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/internal/operators/af$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/c$b",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final a:J

.field final b:Ljava/util/concurrent/TimeUnit;

.field final c:Lrx/f;


# direct methods
.method public constructor <init>(JLjava/util/concurrent/TimeUnit;Lrx/f;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-wide p1, p0, Lrx/internal/operators/af;->a:J

    .line 43
    iput-object p3, p0, Lrx/internal/operators/af;->b:Ljava/util/concurrent/TimeUnit;

    .line 44
    iput-object p4, p0, Lrx/internal/operators/af;->c:Lrx/f;

    .line 45
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lrx/internal/operators/af;->a(Lrx/i;)Lrx/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Lrx/i;)Lrx/i;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-TT;>;)",
            "Lrx/i",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v2, Lcwh;

    invoke-direct {v2, p1}, Lcwh;-><init>(Lrx/i;)V

    .line 50
    iget-object v0, p0, Lrx/internal/operators/af;->c:Lrx/f;

    invoke-virtual {v0}, Lrx/f;->a()Lrx/f$a;

    move-result-object v0

    .line 51
    invoke-virtual {p1, v0}, Lrx/i;->a(Lrx/j;)V

    .line 53
    new-instance v1, Lrx/internal/operators/af$a;

    invoke-direct {v1, v2}, Lrx/internal/operators/af$a;-><init>(Lrx/i;)V

    .line 54
    invoke-virtual {p1, v1}, Lrx/i;->a(Lrx/j;)V

    .line 55
    iget-wide v2, p0, Lrx/internal/operators/af;->a:J

    iget-wide v4, p0, Lrx/internal/operators/af;->a:J

    iget-object v6, p0, Lrx/internal/operators/af;->b:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v0 .. v6}, Lrx/f$a;->a(Lrx/functions/a;JJLjava/util/concurrent/TimeUnit;)Lrx/j;

    .line 57
    return-object v1
.end method
