.class public final Lrx/internal/operators/ae;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/c$b",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final a:Lrx/functions/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/d",
            "<-",
            "Ljava/lang/Throwable;",
            "+",
            "Lrx/c",
            "<+TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/functions/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/d",
            "<-",
            "Ljava/lang/Throwable;",
            "+",
            "Lrx/c",
            "<+TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lrx/internal/operators/ae;->a:Lrx/functions/d;

    .line 81
    return-void
.end method

.method public static a(Lrx/c;)Lrx/internal/operators/ae;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<+TT;>;)",
            "Lrx/internal/operators/ae",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Lrx/internal/operators/ae;

    new-instance v1, Lrx/internal/operators/ae$1;

    invoke-direct {v1, p0}, Lrx/internal/operators/ae$1;-><init>(Lrx/c;)V

    invoke-direct {v0, v1}, Lrx/internal/operators/ae;-><init>(Lrx/functions/d;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lrx/internal/operators/ae;->a(Lrx/i;)Lrx/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Lrx/i;)Lrx/i;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-TT;>;)",
            "Lrx/i",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 85
    new-instance v0, Lrx/internal/producers/a;

    invoke-direct {v0}, Lrx/internal/producers/a;-><init>()V

    .line 87
    new-instance v1, Lcwx;

    invoke-direct {v1}, Lcwx;-><init>()V

    .line 89
    new-instance v2, Lrx/internal/operators/ae$2;

    invoke-direct {v2, p0, p1, v0, v1}, Lrx/internal/operators/ae$2;-><init>(Lrx/internal/operators/ae;Lrx/i;Lrx/internal/producers/a;Lcwx;)V

    .line 163
    invoke-virtual {v1, v2}, Lcwx;->a(Lrx/j;)V

    .line 165
    invoke-virtual {p1, v1}, Lrx/i;->a(Lrx/j;)V

    .line 166
    invoke-virtual {p1, v0}, Lrx/i;->a(Lrx/e;)V

    .line 168
    return-object v2
.end method
