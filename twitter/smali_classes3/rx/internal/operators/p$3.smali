.class Lrx/internal/operators/p$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lrx/internal/operators/p;->a(Lcwv;)Lrx/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcwv;

.field final synthetic b:Lrx/internal/operators/p;


# direct methods
.method constructor <init>(Lrx/internal/operators/p;Lcwv;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lrx/internal/operators/p$3;->b:Lrx/internal/operators/p;

    iput-object p2, p0, Lrx/internal/operators/p$3;->a:Lcwv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lrx/internal/operators/p$3;->b:Lrx/internal/operators/p;

    iget-object v0, v0, Lrx/internal/operators/p;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 150
    :try_start_0
    iget-object v0, p0, Lrx/internal/operators/p$3;->b:Lrx/internal/operators/p;

    iget-object v0, v0, Lrx/internal/operators/p;->a:Lcwv;

    iget-object v1, p0, Lrx/internal/operators/p$3;->a:Lcwv;

    if-ne v0, v1, :cond_0

    .line 151
    iget-object v0, p0, Lrx/internal/operators/p$3;->b:Lrx/internal/operators/p;

    iget-object v0, v0, Lrx/internal/operators/p;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 152
    iget-object v0, p0, Lrx/internal/operators/p$3;->b:Lrx/internal/operators/p;

    iget-object v0, v0, Lrx/internal/operators/p;->a:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 155
    iget-object v0, p0, Lrx/internal/operators/p$3;->b:Lrx/internal/operators/p;

    new-instance v1, Lcwv;

    invoke-direct {v1}, Lcwv;-><init>()V

    iput-object v1, v0, Lrx/internal/operators/p;->a:Lcwv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    :cond_0
    iget-object v0, p0, Lrx/internal/operators/p$3;->b:Lrx/internal/operators/p;

    iget-object v0, v0, Lrx/internal/operators/p;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 161
    return-void

    .line 159
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lrx/internal/operators/p$3;->b:Lrx/internal/operators/p;

    iget-object v1, v1, Lrx/internal/operators/p;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
