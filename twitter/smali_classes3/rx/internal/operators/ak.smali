.class public final Lrx/internal/operators/ak;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/internal/operators/ak$c;,
        Lrx/internal/operators/ak$d;,
        Lrx/internal/operators/ak$b;,
        Lrx/internal/operators/ak$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/c$b",
        "<TT;",
        "Lrx/c",
        "<+TT;>;>;"
    }
.end annotation


# instance fields
.field final a:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-boolean p1, p0, Lrx/internal/operators/ak;->a:Z

    .line 67
    return-void
.end method

.method public static a(Z)Lrx/internal/operators/ak;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(Z)",
            "Lrx/internal/operators/ak",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59
    if-eqz p0, :cond_0

    .line 60
    sget-object v0, Lrx/internal/operators/ak$b;->a:Lrx/internal/operators/ak;

    .line 62
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lrx/internal/operators/ak$a;->a:Lrx/internal/operators/ak;

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lrx/internal/operators/ak;->a(Lrx/i;)Lrx/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Lrx/i;)Lrx/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-TT;>;)",
            "Lrx/i",
            "<-",
            "Lrx/c",
            "<+TT;>;>;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Lrx/internal/operators/ak$d;

    iget-boolean v1, p0, Lrx/internal/operators/ak;->a:Z

    invoke-direct {v0, p1, v1}, Lrx/internal/operators/ak$d;-><init>(Lrx/i;Z)V

    .line 72
    invoke-virtual {p1, v0}, Lrx/i;->a(Lrx/j;)V

    .line 73
    invoke-virtual {v0}, Lrx/internal/operators/ak$d;->d()V

    .line 74
    return-object v0
.end method
