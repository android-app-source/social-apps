.class public final Lrx/internal/operators/az;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/g$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/internal/operators/az$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/g$a",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lrx/g$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/g$a",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:Lrx/functions/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/d",
            "<",
            "Ljava/lang/Throwable;",
            "+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/g$a;Lrx/functions/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/g$a",
            "<TT;>;",
            "Lrx/functions/d",
            "<",
            "Ljava/lang/Throwable;",
            "+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lrx/internal/operators/az;->a:Lrx/g$a;

    .line 37
    iput-object p2, p0, Lrx/internal/operators/az;->b:Lrx/functions/d;

    .line 38
    return-void
.end method


# virtual methods
.method public a(Lrx/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/h",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Lrx/internal/operators/az$a;

    iget-object v1, p0, Lrx/internal/operators/az;->b:Lrx/functions/d;

    invoke-direct {v0, p1, v1}, Lrx/internal/operators/az$a;-><init>(Lrx/h;Lrx/functions/d;)V

    .line 43
    invoke-virtual {p1, v0}, Lrx/h;->a(Lrx/j;)V

    .line 44
    iget-object v1, p0, Lrx/internal/operators/az;->a:Lrx/g$a;

    invoke-interface {v1, v0}, Lrx/g$a;->call(Ljava/lang/Object;)V

    .line 45
    return-void
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 29
    check-cast p1, Lrx/h;

    invoke-virtual {p0, p1}, Lrx/internal/operators/az;->a(Lrx/h;)V

    return-void
.end method
