.class final Lrx/internal/operators/ak$c;
.super Lrx/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lrx/internal/operators/ak;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/i",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Lrx/internal/operators/ak$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/internal/operators/ak$d",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(JLrx/internal/operators/ak$d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lrx/internal/operators/ak$d",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 387
    invoke-direct {p0}, Lrx/i;-><init>()V

    .line 388
    iput-wide p1, p0, Lrx/internal/operators/ak$c;->a:J

    .line 389
    iput-object p3, p0, Lrx/internal/operators/ak$c;->b:Lrx/internal/operators/ak$d;

    .line 390
    return-void
.end method

.method static synthetic a(Lrx/internal/operators/ak$c;)J
    .locals 2

    .prologue
    .line 381
    iget-wide v0, p0, Lrx/internal/operators/ak$c;->a:J

    return-wide v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 399
    iget-object v0, p0, Lrx/internal/operators/ak$c;->b:Lrx/internal/operators/ak$d;

    invoke-virtual {v0, p1, p0}, Lrx/internal/operators/ak$d;->a(Ljava/lang/Object;Lrx/internal/operators/ak$c;)V

    .line 400
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 404
    iget-object v0, p0, Lrx/internal/operators/ak$c;->b:Lrx/internal/operators/ak$d;

    iget-wide v2, p0, Lrx/internal/operators/ak$c;->a:J

    invoke-virtual {v0, p1, v2, v3}, Lrx/internal/operators/ak$d;->a(Ljava/lang/Throwable;J)V

    .line 405
    return-void
.end method

.method public a(Lrx/e;)V
    .locals 4

    .prologue
    .line 394
    iget-object v0, p0, Lrx/internal/operators/ak$c;->b:Lrx/internal/operators/ak$d;

    iget-wide v2, p0, Lrx/internal/operators/ak$c;->a:J

    invoke-virtual {v0, p1, v2, v3}, Lrx/internal/operators/ak$d;->a(Lrx/e;J)V

    .line 395
    return-void
.end method

.method public by_()V
    .locals 4

    .prologue
    .line 409
    iget-object v0, p0, Lrx/internal/operators/ak$c;->b:Lrx/internal/operators/ak$d;

    iget-wide v2, p0, Lrx/internal/operators/ak$c;->a:J

    invoke-virtual {v0, v2, v3}, Lrx/internal/operators/ak$d;->b(J)V

    .line 410
    return-void
.end method
