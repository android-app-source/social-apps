.class public final Lrx/internal/operators/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/a$a;


# instance fields
.field final a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<+",
            "Lrx/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lrx/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lrx/internal/operators/b;->a:Ljava/lang/Iterable;

    .line 32
    return-void
.end method


# virtual methods
.method public a(Lrx/b;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 36
    new-instance v3, Lcwv;

    invoke-direct {v3}, Lcwv;-><init>()V

    .line 38
    invoke-interface {p1, v3}, Lrx/b;->a(Lrx/j;)V

    .line 43
    :try_start_0
    iget-object v1, p0, Lrx/internal/operators/b;->a:Ljava/lang/Iterable;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 49
    if-nez v8, :cond_1

    .line 50
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "The source iterator returned is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v1}, Lrx/b;->a(Ljava/lang/Throwable;)V

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 44
    :catch_0
    move-exception v1

    .line 45
    invoke-interface {p1, v1}, Lrx/b;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 54
    :cond_1
    new-instance v6, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v6, v9}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 55
    new-instance v4, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    .line 58
    :goto_1
    invoke-virtual {v3}, Lcwv;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    :try_start_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 75
    if-nez v1, :cond_3

    .line 142
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    if-nez v1, :cond_0

    .line 143
    invoke-virtual {v4, v10, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    invoke-interface {p1}, Lrx/b;->a()V

    goto :goto_0

    .line 65
    :catch_1
    move-exception v1

    .line 66
    invoke-virtual {v3}, Lcwv;->B_()V

    .line 67
    invoke-virtual {v4, v10, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 68
    invoke-interface {p1, v1}, Lrx/b;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 70
    :cond_2
    invoke-static {v1}, Lcwl;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 79
    :cond_3
    invoke-virtual {v3}, Lcwv;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    :try_start_2
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lrx/a;

    move-object v7, v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 97
    invoke-virtual {v3}, Lcwv;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 101
    if-nez v7, :cond_6

    .line 102
    invoke-virtual {v3}, Lcwv;->B_()V

    .line 103
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "A completable source is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 104
    invoke-virtual {v4, v10, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 105
    invoke-interface {p1, v1}, Lrx/b;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 87
    :catch_2
    move-exception v1

    .line 88
    invoke-virtual {v3}, Lcwv;->B_()V

    .line 89
    invoke-virtual {v4, v10, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 90
    invoke-interface {p1, v1}, Lrx/b;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 92
    :cond_4
    invoke-static {v1}, Lcwl;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 107
    :cond_5
    invoke-static {v1}, Lcwl;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 112
    :cond_6
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    .line 114
    new-instance v1, Lrx/internal/operators/b$1;

    move-object v2, p0

    move-object v5, p1

    invoke-direct/range {v1 .. v6}, Lrx/internal/operators/b$1;-><init>(Lrx/internal/operators/b;Lcwv;Ljava/util/concurrent/atomic/AtomicBoolean;Lrx/b;Ljava/util/concurrent/atomic/AtomicInteger;)V

    invoke-virtual {v7, v1}, Lrx/a;->a(Lrx/b;)V

    goto :goto_1
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lrx/b;

    invoke-virtual {p0, p1}, Lrx/internal/operators/b;->a(Lrx/b;)V

    return-void
.end method
