.class public final Lrx/internal/operators/OperatorGroupBy;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/internal/operators/OperatorGroupBy$State;,
        Lrx/internal/operators/OperatorGroupBy$c;,
        Lrx/internal/operators/OperatorGroupBy$b;,
        Lrx/internal/operators/OperatorGroupBy$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/c$b",
        "<",
        "Lcwc",
        "<TK;TV;>;TT;>;"
    }
.end annotation


# instance fields
.field final a:Lrx/functions/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/d",
            "<-TT;+TK;>;"
        }
    .end annotation
.end field

.field final b:Lrx/functions/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/d",
            "<-TT;+TV;>;"
        }
    .end annotation
.end field

.field final c:I

.field final d:Z

.field final e:Lrx/functions/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/d",
            "<",
            "Lrx/functions/b",
            "<TK;>;",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/functions/d;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/d",
            "<-TT;+TK;>;)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-static {}, Lrx/internal/util/UtilityFunctions;->b()Lrx/functions/d;

    move-result-object v2

    sget v3, Lrx/internal/util/h;->b:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lrx/internal/operators/OperatorGroupBy;-><init>(Lrx/functions/d;Lrx/functions/d;IZLrx/functions/d;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Lrx/functions/d;Lrx/functions/d;IZLrx/functions/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/d",
            "<-TT;+TK;>;",
            "Lrx/functions/d",
            "<-TT;+TV;>;IZ",
            "Lrx/functions/d",
            "<",
            "Lrx/functions/b",
            "<TK;>;",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lrx/internal/operators/OperatorGroupBy;->a:Lrx/functions/d;

    .line 68
    iput-object p2, p0, Lrx/internal/operators/OperatorGroupBy;->b:Lrx/functions/d;

    .line 69
    iput p3, p0, Lrx/internal/operators/OperatorGroupBy;->c:I

    .line 70
    iput-boolean p4, p0, Lrx/internal/operators/OperatorGroupBy;->d:Z

    .line 71
    iput-object p5, p0, Lrx/internal/operators/OperatorGroupBy;->e:Lrx/functions/d;

    .line 72
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 46
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lrx/internal/operators/OperatorGroupBy;->a(Lrx/i;)Lrx/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Lrx/i;)Lrx/i;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-",
            "Lcwc",
            "<TK;TV;>;>;)",
            "Lrx/i",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 78
    :try_start_0
    new-instance v0, Lrx/internal/operators/OperatorGroupBy$b;

    iget-object v2, p0, Lrx/internal/operators/OperatorGroupBy;->a:Lrx/functions/d;

    iget-object v3, p0, Lrx/internal/operators/OperatorGroupBy;->b:Lrx/functions/d;

    iget v4, p0, Lrx/internal/operators/OperatorGroupBy;->c:I

    iget-boolean v5, p0, Lrx/internal/operators/OperatorGroupBy;->d:Z

    iget-object v6, p0, Lrx/internal/operators/OperatorGroupBy;->e:Lrx/functions/d;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lrx/internal/operators/OperatorGroupBy$b;-><init>(Lrx/i;Lrx/functions/d;Lrx/functions/d;IZLrx/functions/d;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    new-instance v1, Lrx/internal/operators/OperatorGroupBy$1;

    invoke-direct {v1, p0, v0}, Lrx/internal/operators/OperatorGroupBy$1;-><init>(Lrx/internal/operators/OperatorGroupBy;Lrx/internal/operators/OperatorGroupBy$b;)V

    invoke-static {v1}, Lcwy;->a(Lrx/functions/a;)Lrx/j;

    move-result-object v1

    invoke-virtual {p1, v1}, Lrx/i;->a(Lrx/j;)V

    .line 94
    iget-object v1, v0, Lrx/internal/operators/OperatorGroupBy$b;->h:Lrx/internal/operators/OperatorGroupBy$a;

    invoke-virtual {p1, v1}, Lrx/i;->a(Lrx/e;)V

    .line 96
    :goto_0
    return-object v0

    .line 79
    :catch_0
    move-exception v0

    .line 81
    invoke-static {v0, p1}, Lrx/exceptions/a;->a(Ljava/lang/Throwable;Lrx/d;)V

    .line 82
    invoke-static {}, Lcwi;->a()Lrx/i;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lrx/i;->B_()V

    goto :goto_0
.end method
