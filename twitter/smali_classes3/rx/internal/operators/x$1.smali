.class Lrx/internal/operators/x$1;
.super Lrx/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lrx/internal/operators/x;->a(Lrx/i;)Lrx/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/i",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lrx/internal/operators/x$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/internal/operators/x$a",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:Lrx/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/i",
            "<*>;"
        }
    .end annotation
.end field

.field final synthetic c:Lcwx;

.field final synthetic d:Lrx/f$a;

.field final synthetic e:Lcwh;

.field final synthetic f:Lrx/internal/operators/x;


# direct methods
.method constructor <init>(Lrx/internal/operators/x;Lrx/i;Lcwx;Lrx/f$a;Lcwh;)V
    .locals 1

    .prologue
    .line 63
    iput-object p1, p0, Lrx/internal/operators/x$1;->f:Lrx/internal/operators/x;

    iput-object p3, p0, Lrx/internal/operators/x$1;->c:Lcwx;

    iput-object p4, p0, Lrx/internal/operators/x$1;->d:Lrx/f$a;

    iput-object p5, p0, Lrx/internal/operators/x$1;->e:Lcwh;

    invoke-direct {p0, p2}, Lrx/i;-><init>(Lrx/i;)V

    .line 64
    new-instance v0, Lrx/internal/operators/x$a;

    invoke-direct {v0}, Lrx/internal/operators/x$a;-><init>()V

    iput-object v0, p0, Lrx/internal/operators/x$1;->a:Lrx/internal/operators/x$a;

    .line 65
    iput-object p0, p0, Lrx/internal/operators/x$1;->b:Lrx/i;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lrx/internal/operators/x$1;->a:Lrx/internal/operators/x$a;

    invoke-virtual {v0, p1}, Lrx/internal/operators/x$a;->a(Ljava/lang/Object;)I

    move-result v0

    .line 76
    iget-object v1, p0, Lrx/internal/operators/x$1;->c:Lcwx;

    iget-object v2, p0, Lrx/internal/operators/x$1;->d:Lrx/f$a;

    new-instance v3, Lrx/internal/operators/x$1$1;

    invoke-direct {v3, p0, v0}, Lrx/internal/operators/x$1$1;-><init>(Lrx/internal/operators/x$1;I)V

    iget-object v0, p0, Lrx/internal/operators/x$1;->f:Lrx/internal/operators/x;

    iget-wide v4, v0, Lrx/internal/operators/x;->a:J

    iget-object v0, p0, Lrx/internal/operators/x$1;->f:Lrx/internal/operators/x;

    iget-object v0, v0, Lrx/internal/operators/x;->b:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3, v4, v5, v0}, Lrx/f$a;->a(Lrx/functions/a;JLjava/util/concurrent/TimeUnit;)Lrx/j;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcwx;->a(Lrx/j;)V

    .line 82
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lrx/internal/operators/x$1;->e:Lcwh;

    invoke-virtual {v0, p1}, Lcwh;->a(Ljava/lang/Throwable;)V

    .line 87
    invoke-virtual {p0}, Lrx/internal/operators/x$1;->B_()V

    .line 88
    iget-object v0, p0, Lrx/internal/operators/x$1;->a:Lrx/internal/operators/x$a;

    invoke-virtual {v0}, Lrx/internal/operators/x$a;->a()V

    .line 89
    return-void
.end method

.method public by_()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lrx/internal/operators/x$1;->a:Lrx/internal/operators/x$a;

    iget-object v1, p0, Lrx/internal/operators/x$1;->e:Lcwh;

    invoke-virtual {v0, v1, p0}, Lrx/internal/operators/x$a;->a(Lrx/i;Lrx/i;)V

    .line 94
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 69
    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1}, Lrx/internal/operators/x$1;->a(J)V

    .line 70
    return-void
.end method
