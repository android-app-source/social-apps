.class public final Lrx/internal/operators/ag;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/internal/operators/ag$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/c$b",
        "<TR;TT;>;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field final a:Lrx/functions/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/e",
            "<TR;-TT;TR;>;"
        }
    .end annotation
.end field

.field private final b:Lrx/functions/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/c",
            "<TR;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lrx/internal/operators/ag;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lrx/functions/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lrx/functions/e",
            "<TR;-TT;TR;>;)V"
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Lrx/internal/operators/ag$1;

    invoke-direct {v0, p1}, Lrx/internal/operators/ag$1;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v0, p2}, Lrx/internal/operators/ag;-><init>(Lrx/functions/c;Lrx/functions/e;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Lrx/functions/c;Lrx/functions/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/c",
            "<TR;>;",
            "Lrx/functions/e",
            "<TR;-TT;TR;>;)V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lrx/internal/operators/ag;->b:Lrx/functions/c;

    .line 75
    iput-object p2, p0, Lrx/internal/operators/ag;->a:Lrx/functions/e;

    .line 76
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lrx/internal/operators/ag;->a(Lrx/i;)Lrx/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Lrx/i;)Lrx/i;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-TR;>;)",
            "Lrx/i",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lrx/internal/operators/ag;->b:Lrx/functions/c;

    invoke-interface {v0}, Lrx/functions/c;->call()Ljava/lang/Object;

    move-result-object v1

    .line 95
    sget-object v0, Lrx/internal/operators/ag;->c:Ljava/lang/Object;

    if-ne v1, v0, :cond_0

    .line 96
    new-instance v0, Lrx/internal/operators/ag$2;

    invoke-direct {v0, p0, p1, p1}, Lrx/internal/operators/ag$2;-><init>(Lrx/internal/operators/ag;Lrx/i;Lrx/i;)V

    .line 165
    :goto_0
    return-object v0

    .line 129
    :cond_0
    new-instance v2, Lrx/internal/operators/ag$a;

    invoke-direct {v2, v1, p1}, Lrx/internal/operators/ag$a;-><init>(Ljava/lang/Object;Lrx/i;)V

    .line 131
    new-instance v0, Lrx/internal/operators/ag$3;

    invoke-direct {v0, p0, v1, v2}, Lrx/internal/operators/ag$3;-><init>(Lrx/internal/operators/ag;Ljava/lang/Object;Lrx/internal/operators/ag$a;)V

    .line 163
    invoke-virtual {p1, v0}, Lrx/i;->a(Lrx/j;)V

    .line 164
    invoke-virtual {p1, v2}, Lrx/i;->a(Lrx/e;)V

    goto :goto_0
.end method
