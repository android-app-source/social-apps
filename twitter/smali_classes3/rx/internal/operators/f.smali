.class public final Lrx/internal/operators/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/c$a",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<+TT;>;"
        }
    .end annotation
.end field

.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;

.field final d:Lrx/f;


# direct methods
.method public constructor <init>(Lrx/c;JLjava/util/concurrent/TimeUnit;Lrx/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<+TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lrx/internal/operators/f;->a:Lrx/c;

    .line 39
    iput-wide p2, p0, Lrx/internal/operators/f;->b:J

    .line 40
    iput-object p4, p0, Lrx/internal/operators/f;->c:Ljava/util/concurrent/TimeUnit;

    .line 41
    iput-object p5, p0, Lrx/internal/operators/f;->d:Lrx/f;

    .line 42
    return-void
.end method


# virtual methods
.method public a(Lrx/i;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lrx/internal/operators/f;->d:Lrx/f;

    invoke-virtual {v0}, Lrx/f;->a()Lrx/f$a;

    move-result-object v0

    .line 47
    invoke-virtual {p1, v0}, Lrx/i;->a(Lrx/j;)V

    .line 49
    new-instance v1, Lrx/internal/operators/f$1;

    invoke-direct {v1, p0, p1}, Lrx/internal/operators/f$1;-><init>(Lrx/internal/operators/f;Lrx/i;)V

    iget-wide v2, p0, Lrx/internal/operators/f;->b:J

    iget-object v4, p0, Lrx/internal/operators/f;->c:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Lrx/f$a;->a(Lrx/functions/a;JLjava/util/concurrent/TimeUnit;)Lrx/j;

    .line 57
    return-void
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lrx/internal/operators/f;->a(Lrx/i;)V

    return-void
.end method
