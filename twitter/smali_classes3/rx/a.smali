.class public Lrx/a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/a$b;,
        Lrx/a$a;
    }
.end annotation


# static fields
.field static final a:Lrx/a;

.field static final b:Lrx/a;


# instance fields
.field private final c:Lrx/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 70
    new-instance v0, Lrx/a;

    new-instance v1, Lrx/a$1;

    invoke-direct {v1}, Lrx/a$1;-><init>()V

    invoke-direct {v0, v1, v2}, Lrx/a;-><init>(Lrx/a$a;Z)V

    sput-object v0, Lrx/a;->a:Lrx/a;

    .line 79
    new-instance v0, Lrx/a;

    new-instance v1, Lrx/a$5;

    invoke-direct {v1}, Lrx/a$5;-><init>()V

    invoke-direct {v0, v1, v2}, Lrx/a;-><init>(Lrx/a$a;Z)V

    sput-object v0, Lrx/a;->b:Lrx/a;

    return-void
.end method

.method protected constructor <init>(Lrx/a$a;)V
    .locals 1

    .prologue
    .line 1001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1002
    invoke-static {p1}, Lcwl;->a(Lrx/a$a;)Lrx/a$a;

    move-result-object v0

    iput-object v0, p0, Lrx/a;->c:Lrx/a$a;

    .line 1003
    return-void
.end method

.method protected constructor <init>(Lrx/a$a;Z)V
    .locals 0

    .prologue
    .line 1012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1013
    if-eqz p2, :cond_0

    invoke-static {p1}, Lcwl;->a(Lrx/a$a;)Lrx/a$a;

    move-result-object p1

    :cond_0
    iput-object p1, p0, Lrx/a;->c:Lrx/a$a;

    .line 1014
    return-void
.end method

.method static a(Ljava/lang/Throwable;)Ljava/lang/NullPointerException;
    .locals 2

    .prologue
    .line 829
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Actually not, but can\'t pass out an exception otherwise..."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 830
    invoke-virtual {v0, p0}, Ljava/lang/NullPointerException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 831
    return-object v0
.end method

.method static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 773
    if-nez p0, :cond_0

    .line 774
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 776
    :cond_0
    return-object p0
.end method

.method public static a()Lrx/a;
    .locals 3

    .prologue
    .line 294
    sget-object v0, Lrx/a;->a:Lrx/a;

    iget-object v0, v0, Lrx/a;->c:Lrx/a$a;

    invoke-static {v0}, Lcwl;->a(Lrx/a$a;)Lrx/a$a;

    move-result-object v1

    .line 295
    sget-object v0, Lrx/a;->a:Lrx/a;

    iget-object v0, v0, Lrx/a;->c:Lrx/a$a;

    if-ne v1, v0, :cond_0

    .line 296
    sget-object v0, Lrx/a;->a:Lrx/a;

    .line 298
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lrx/a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lrx/a;-><init>(Lrx/a$a;Z)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Iterable;)Lrx/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lrx/a;",
            ">;)",
            "Lrx/a;"
        }
    .end annotation

    .prologue
    .line 653
    invoke-static {p0}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 654
    new-instance v0, Lrx/internal/operators/b;

    invoke-direct {v0, p0}, Lrx/internal/operators/b;-><init>(Ljava/lang/Iterable;)V

    invoke-static {v0}, Lrx/a;->a(Lrx/a$a;)Lrx/a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lrx/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<*>;)",
            "Lrx/a;"
        }
    .end annotation

    .prologue
    .line 490
    invoke-static {p0}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    new-instance v0, Lrx/a$12;

    invoke-direct {v0, p0}, Lrx/a$12;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-static {v0}, Lrx/a;->a(Lrx/a$a;)Lrx/a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lrx/a$a;)Lrx/a;
    .locals 1

    .prologue
    .line 363
    invoke-static {p0}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    :try_start_0
    new-instance v0, Lrx/a;

    invoke-direct {v0, p0}, Lrx/a;-><init>(Lrx/a$a;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 366
    :catch_0
    move-exception v0

    .line 367
    throw v0

    .line 368
    :catch_1
    move-exception v0

    .line 369
    invoke-static {v0}, Lcwl;->a(Ljava/lang/Throwable;)V

    .line 370
    invoke-static {v0}, Lrx/a;->a(Ljava/lang/Throwable;)Ljava/lang/NullPointerException;

    move-result-object v0

    throw v0
.end method

.method public static a(Lrx/c;)Lrx/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<*>;)",
            "Lrx/a;"
        }
    .end annotation

    .prologue
    .line 569
    invoke-static {p0}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 570
    new-instance v0, Lrx/a$2;

    invoke-direct {v0, p0}, Lrx/a$2;-><init>(Lrx/c;)V

    invoke-static {v0}, Lrx/a;->a(Lrx/a$a;)Lrx/a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lrx/functions/a;)Lrx/a;
    .locals 1

    .prologue
    .line 462
    invoke-static {p0}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    new-instance v0, Lrx/a$11;

    invoke-direct {v0, p0}, Lrx/a$11;-><init>(Lrx/functions/a;)V

    invoke-static {v0}, Lrx/a;->a(Lrx/a$a;)Lrx/a;

    move-result-object v0

    return-object v0
.end method

.method private a(Lrx/i;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/i",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 2080
    invoke-static {p1}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2082
    if-eqz p2, :cond_0

    .line 2083
    :try_start_0
    invoke-virtual {p1}, Lrx/i;->c()V

    .line 2085
    :cond_0
    new-instance v0, Lrx/a$8;

    invoke-direct {v0, p0, p1}, Lrx/a$8;-><init>(Lrx/a;Lrx/i;)V

    invoke-virtual {p0, v0}, Lrx/a;->a(Lrx/b;)V

    .line 2101
    invoke-static {p1}, Lcwl;->a(Lrx/j;)Lrx/j;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 2110
    return-void

    .line 2102
    :catch_0
    move-exception v0

    .line 2103
    throw v0

    .line 2104
    :catch_1
    move-exception v0

    .line 2105
    invoke-static {v0}, Lrx/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 2106
    invoke-static {v0}, Lcwl;->c(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    .line 2107
    invoke-static {v0}, Lcwl;->a(Ljava/lang/Throwable;)V

    .line 2108
    invoke-static {v0}, Lrx/a;->a(Ljava/lang/Throwable;)Ljava/lang/NullPointerException;

    move-result-object v0

    throw v0
.end method

.method static b(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2023
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 2024
    invoke-virtual {v0}, Ljava/lang/Thread;->getUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    invoke-interface {v1, v0, p0}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 2025
    return-void
.end method


# virtual methods
.method public final a(Lrx/f;)Lrx/a;
    .locals 1

    .prologue
    .line 1608
    invoke-static {p1}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1609
    new-instance v0, Lrx/a$6;

    invoke-direct {v0, p0, p1}, Lrx/a$6;-><init>(Lrx/a;Lrx/f;)V

    invoke-static {v0}, Lrx/a;->a(Lrx/a$a;)Lrx/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/functions/b;)Lrx/a;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/b",
            "<-",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lrx/a;"
        }
    .end annotation

    .prologue
    .line 1341
    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v1

    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v3

    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v4

    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v5

    move-object v0, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lrx/a;->a(Lrx/functions/b;Lrx/functions/b;Lrx/functions/a;Lrx/functions/a;Lrx/functions/a;)Lrx/a;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lrx/functions/b;Lrx/functions/b;Lrx/functions/a;Lrx/functions/a;Lrx/functions/a;)Lrx/a;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/b",
            "<-",
            "Lrx/j;",
            ">;",
            "Lrx/functions/b",
            "<-",
            "Ljava/lang/Throwable;",
            ">;",
            "Lrx/functions/a;",
            "Lrx/functions/a;",
            "Lrx/functions/a;",
            ")",
            "Lrx/a;"
        }
    .end annotation

    .prologue
    .line 1360
    invoke-static {p1}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1361
    invoke-static {p2}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1362
    invoke-static {p3}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1363
    invoke-static {p4}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1364
    invoke-static {p5}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1365
    new-instance v0, Lrx/a$4;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p2

    move-object v5, p1

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lrx/a$4;-><init>(Lrx/a;Lrx/functions/a;Lrx/functions/a;Lrx/functions/b;Lrx/functions/b;Lrx/functions/a;)V

    invoke-static {v0}, Lrx/a;->a(Lrx/a$a;)Lrx/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/b;)V
    .locals 1

    .prologue
    .line 2033
    invoke-static {p1}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2035
    :try_start_0
    iget-object v0, p0, Lrx/a;->c:Lrx/a$a;

    invoke-static {p0, v0}, Lcwl;->a(Lrx/a;Lrx/a$a;)Lrx/a$a;

    move-result-object v0

    .line 2037
    invoke-interface {v0, p1}, Lrx/a$a;->call(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 2046
    return-void

    .line 2038
    :catch_0
    move-exception v0

    .line 2039
    throw v0

    .line 2040
    :catch_1
    move-exception v0

    .line 2041
    invoke-static {v0}, Lrx/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 2042
    invoke-static {v0}, Lcwl;->e(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    .line 2043
    invoke-static {v0}, Lcwl;->a(Ljava/lang/Throwable;)V

    .line 2044
    invoke-static {v0}, Lrx/a;->a(Ljava/lang/Throwable;)Ljava/lang/NullPointerException;

    move-result-object v0

    throw v0
.end method

.method public final a(Lrx/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/i",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2069
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lrx/a;->a(Lrx/i;Z)V

    .line 2070
    return-void
.end method

.method public final b(Lrx/f;)Lrx/a;
    .locals 1

    .prologue
    .line 2136
    invoke-static {p1}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2138
    new-instance v0, Lrx/a$9;

    invoke-direct {v0, p0, p1}, Lrx/a$9;-><init>(Lrx/a;Lrx/f;)V

    invoke-static {v0}, Lrx/a;->a(Lrx/a$a;)Lrx/a;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lrx/functions/a;)Lrx/j;
    .locals 2

    .prologue
    .line 1925
    invoke-static {p1}, Lrx/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1927
    new-instance v0, Lcww;

    invoke-direct {v0}, Lcww;-><init>()V

    .line 1928
    new-instance v1, Lrx/a$7;

    invoke-direct {v1, p0, p1, v0}, Lrx/a$7;-><init>(Lrx/a;Lrx/functions/a;Lcww;)V

    invoke-virtual {p0, v1}, Lrx/a;->a(Lrx/b;)V

    .line 1958
    return-object v0
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1034
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 1035
    new-array v1, v1, [Ljava/lang/Throwable;

    .line 1037
    new-instance v2, Lrx/a$3;

    invoke-direct {v2, p0, v0, v1}, Lrx/a$3;-><init>(Lrx/a;Ljava/util/concurrent/CountDownLatch;[Ljava/lang/Throwable;)V

    invoke-virtual {p0, v2}, Lrx/a;->a(Lrx/b;)V

    .line 1057
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 1058
    aget-object v0, v1, v6

    if-eqz v0, :cond_0

    .line 1059
    aget-object v0, v1, v6

    invoke-static {v0}, Lrx/exceptions/a;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    .line 1071
    :cond_0
    :goto_0
    return-void

    .line 1064
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1068
    aget-object v0, v1, v6

    if-eqz v0, :cond_0

    .line 1069
    aget-object v0, v1, v6

    invoke-static {v0}, Lrx/exceptions/a;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    goto :goto_0

    .line 1065
    :catch_0
    move-exception v0

    .line 1066
    invoke-static {v0}, Lrx/exceptions/a;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final b(Lrx/b;)V
    .locals 1

    .prologue
    .line 2055
    instance-of v0, p1, Lcwe;

    if-nez v0, :cond_0

    .line 2056
    new-instance v0, Lcwe;

    invoke-direct {v0, p1}, Lcwe;-><init>(Lrx/b;)V

    move-object p1, v0

    .line 2058
    :cond_0
    invoke-virtual {p0, p1}, Lrx/a;->a(Lrx/b;)V

    .line 2059
    return-void
.end method

.method public final c()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2252
    new-instance v0, Lrx/a$10;

    invoke-direct {v0, p0}, Lrx/a$10;-><init>(Lrx/a;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method
