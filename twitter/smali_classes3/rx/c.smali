.class public Lrx/c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/c$c;,
        Lrx/c$b;,
        Lrx/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:Lrx/c$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c$a",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lrx/c$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c$a",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lrx/c;->a:Lrx/c$a;

    .line 62
    return-void
.end method

.method public static a(JJLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/f;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2143
    new-instance v1, Lrx/internal/operators/s;

    move-wide v2, p0

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lrx/internal/operators/s;-><init>(JJLjava/util/concurrent/TimeUnit;Lrx/f;)V

    invoke-static {v1}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/f;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2083
    move-wide v0, p0

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lrx/c;->a(JJLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1932
    new-instance v0, Lrx/internal/operators/OnSubscribeFromIterable;

    invoke-direct {v0, p0}, Lrx/internal/operators/OnSubscribeFromIterable;-><init>(Ljava/lang/Iterable;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;Lrx/functions/h;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+",
            "Lrx/c",
            "<*>;>;",
            "Lrx/functions/h",
            "<+TR;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 3853
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3854
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/c;

    .line 3855
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3857
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lrx/c;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lrx/internal/operators/OperatorZip;

    invoke-direct {v1, p1}, Lrx/internal/operators/OperatorZip;-><init>(Lrx/functions/h;)V

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2199
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lrx/c;->a([Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;Lrx/functions/h;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<+",
            "Lrx/c",
            "<+TT;>;>;",
            "Lrx/functions/h",
            "<+TR;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 1023
    new-instance v0, Lrx/internal/operators/OnSubscribeCombineLatest;

    invoke-direct {v0, p0, p1}, Lrx/internal/operators/OnSubscribeCombineLatest;-><init>(Ljava/lang/Iterable;Lrx/functions/h;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<+TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2036
    new-instance v0, Lrx/internal/operators/i;

    invoke-direct {v0, p0}, Lrx/internal/operators/i;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lrx/c$a;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c$a",
            "<TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v0, Lrx/c;

    invoke-static {p0}, Lcwl;->a(Lrx/c$a;)Lrx/c$a;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/c;-><init>(Lrx/c$a;)V

    return-object v0
.end method

.method public static a(Lrx/c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<+",
            "Lrx/c",
            "<+TT;>;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1136
    invoke-static {}, Lrx/internal/util/UtilityFunctions;->b()Lrx/functions/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lrx/c;Lrx/c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<+TT;>;",
            "Lrx/c",
            "<+TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 402
    invoke-static {p0, p1}, Lrx/internal/operators/OnSubscribeAmb;->a(Lrx/c;Lrx/c;)Lrx/c$a;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<+TT1;>;",
            "Lrx/c",
            "<+TT2;>;",
            "Lrx/functions/e",
            "<-TT1;-TT2;+TR;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 673
    const/4 v0, 0x2

    new-array v0, v0, [Lrx/c;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p2}, Lrx/functions/i;->a(Lrx/functions/e;)Lrx/functions/h;

    move-result-object v1

    invoke-static {v0, v1}, Lrx/c;->a(Ljava/util/List;Lrx/functions/h;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lrx/functions/c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/c",
            "<",
            "Lrx/c",
            "<TT;>;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1765
    new-instance v0, Lrx/internal/operators/e;

    invoke-direct {v0, p0}, Lrx/internal/operators/e;-><init>(Lrx/functions/c;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static a([Ljava/lang/Object;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1955
    array-length v0, p0

    .line 1956
    if-nez v0, :cond_0

    .line 1957
    invoke-static {}, Lrx/c;->d()Lrx/c;

    move-result-object v0

    .line 1962
    :goto_0
    return-object v0

    .line 1959
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1960
    const/4 v0, 0x0

    aget-object v0, p0, v0

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    goto :goto_0

    .line 1962
    :cond_1
    new-instance v0, Lrx/internal/operators/OnSubscribeFromArray;

    invoke-direct {v0, p0}, Lrx/internal/operators/OnSubscribeFromArray;-><init>([Ljava/lang/Object;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Lrx/c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([",
            "Lrx/c",
            "<+TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2910
    invoke-static {p0}, Lrx/c;->a([Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->b(Lrx/c;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method static a(Lrx/i;Lrx/c;)Lrx/j;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/i",
            "<-TT;>;",
            "Lrx/c",
            "<TT;>;)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 10210
    if-nez p0, :cond_0

    .line 10211
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "subscriber can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10213
    :cond_0
    iget-object v0, p1, Lrx/c;->a:Lrx/c$a;

    if-nez v0, :cond_1

    .line 10214
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "onSubscribe function can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10222
    :cond_1
    invoke-virtual {p0}, Lrx/i;->c()V

    .line 10229
    instance-of v0, p0, Lcwf;

    if-nez v0, :cond_2

    .line 10231
    new-instance v0, Lcwf;

    invoke-direct {v0, p0}, Lcwf;-><init>(Lrx/i;)V

    move-object p0, v0

    .line 10238
    :cond_2
    :try_start_0
    iget-object v0, p1, Lrx/c;->a:Lrx/c$a;

    invoke-static {p1, v0}, Lcwl;->a(Lrx/c;Lrx/c$a;)Lrx/c$a;

    move-result-object v0

    invoke-interface {v0, p0}, Lrx/c$a;->call(Ljava/lang/Object;)V

    .line 10239
    invoke-static {p0}, Lcwl;->a(Lrx/j;)Lrx/j;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 10261
    :goto_0
    return-object v0

    .line 10240
    :catch_0
    move-exception v0

    .line 10242
    invoke-static {v0}, Lrx/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 10244
    invoke-virtual {p0}, Lrx/i;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 10245
    invoke-static {v0}, Lcwl;->c(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Lcwl;->a(Ljava/lang/Throwable;)V

    .line 10261
    :goto_1
    invoke-static {}, Lcwy;->b()Lrx/j;

    move-result-object v0

    goto :goto_0

    .line 10249
    :cond_3
    :try_start_1
    invoke-static {v0}, Lcwl;->c(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lrx/i;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 10250
    :catch_1
    move-exception v1

    .line 10251
    invoke-static {v1}, Lrx/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 10254
    new-instance v2, Lrx/exceptions/OnErrorFailedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Error occurred attempting to subscribe ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "] and then again while trying to pass to onError."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Lrx/exceptions/OnErrorFailedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 10256
    invoke-static {v2}, Lcwl;->c(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 10258
    throw v2
.end method

.method public static b(Ljava/lang/Object;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2173
    invoke-static {p0}, Lrx/internal/util/ScalarSynchronousObservable;->a(Ljava/lang/Object;)Lrx/internal/util/ScalarSynchronousObservable;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Throwable;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Throwable;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1811
    new-instance v0, Lrx/internal/operators/r;

    invoke-direct {v0, p0}, Lrx/internal/operators/r;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lrx/c;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<+",
            "Lrx/c",
            "<+TT;>;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2565
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lrx/internal/util/ScalarSynchronousObservable;

    if-ne v0, v1, :cond_0

    .line 2566
    check-cast p0, Lrx/internal/util/ScalarSynchronousObservable;

    invoke-static {}, Lrx/internal/util/UtilityFunctions;->b()Lrx/functions/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/internal/util/ScalarSynchronousObservable;->l(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 2568
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lrx/internal/operators/OperatorMerge;->a(Z)Lrx/internal/operators/OperatorMerge;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lrx/c;Lrx/c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<+TT;>;",
            "Lrx/c",
            "<+TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1164
    invoke-static {p0, p1}, Lrx/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->a(Lrx/c;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<+TT1;>;",
            "Lrx/c",
            "<+TT2;>;",
            "Lrx/functions/e",
            "<-TT1;-TT2;+TR;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 4005
    const/4 v0, 0x2

    new-array v0, v0, [Lrx/c;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lrx/internal/operators/OperatorZip;

    invoke-direct {v1, p2}, Lrx/internal/operators/OperatorZip;-><init>(Lrx/functions/e;)V

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lrx/c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<+",
            "Lrx/c",
            "<+TT;>;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 3588
    const/4 v0, 0x0

    invoke-static {v0}, Lrx/internal/operators/ak;->a(Z)Lrx/internal/operators/ak;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lrx/c;Lrx/c;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<+TT;>;",
            "Lrx/c",
            "<+TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2633
    const/4 v0, 0x2

    new-array v0, v0, [Lrx/c;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Lrx/c;->a([Lrx/c;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1787
    invoke-static {}, Lrx/internal/operators/EmptyObservableHolder;->a()Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static e()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 3439
    invoke-static {}, Lrx/internal/operators/NeverObservableHolder;->a()Lrx/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(I)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5013
    invoke-static {p0, p1}, Lrx/internal/operators/CachedObservable;->b(Lrx/c;I)Lrx/internal/operators/CachedObservable;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5318
    invoke-static {}, Lcws;->c()Lrx/f;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lrx/c;->b(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;Lrx/c;Lrx/f;)Lrx/c;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/c",
            "<+TT;>;",
            "Lrx/f;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 11295
    new-instance v1, Lrx/internal/operators/ap;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lrx/internal/operators/ap;-><init>(JLjava/util/concurrent/TimeUnit;Lrx/c;Lrx/f;)V

    invoke-virtual {p0, v1}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TR;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 5038
    new-instance v0, Lrx/internal/operators/w;

    invoke-direct {v0, p1}, Lrx/internal/operators/w;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Lrx/functions/e;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Lrx/functions/e",
            "<TR;-TT;TR;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 8274
    new-instance v0, Lrx/internal/operators/o;

    invoke-direct {v0, p0, p1, p2}, Lrx/internal/operators/o;-><init>(Lrx/c;Ljava/lang/Object;Lrx/functions/e;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/c$b;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c$b",
            "<+TR;-TT;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 237
    new-instance v0, Lrx/internal/operators/j;

    iget-object v1, p0, Lrx/c;->a:Lrx/c$a;

    invoke-direct {v0, v1, p1}, Lrx/internal/operators/j;-><init>(Lrx/c$a;Lrx/c$b;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Lrx/c$c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c$c",
            "<-TT;+TR;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 264
    invoke-interface {p1, p0}, Lrx/c$c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/c;

    return-object v0
.end method

.method public final a(Lrx/c;I)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<B:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<TB;>;I)",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 4886
    new-instance v0, Lrx/internal/operators/v;

    invoke-direct {v0, p1, p2}, Lrx/internal/operators/v;-><init>(Lrx/c;I)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/c;Lrx/functions/e;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T2:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<+TT2;>;",
            "Lrx/functions/e",
            "<-TT;-TT2;+TR;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 12643
    invoke-static {p0, p1, p2}, Lrx/c;->b(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/f;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/f;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 7615
    sget v0, Lrx/internal/util/h;->b:I

    invoke-virtual {p0, p1, v0}, Lrx/c;->a(Lrx/f;I)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/f;I)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/f;",
            "I)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 7649
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lrx/c;->a(Lrx/f;ZI)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/f;ZI)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/f;",
            "ZI)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 7720
    instance-of v0, p0, Lrx/internal/util/ScalarSynchronousObservable;

    if-eqz v0, :cond_0

    .line 7721
    check-cast p0, Lrx/internal/util/ScalarSynchronousObservable;

    invoke-virtual {p0, p1}, Lrx/internal/util/ScalarSynchronousObservable;->c(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 7723
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lrx/internal/operators/ad;

    invoke-direct {v0, p1, p2, p3}, Lrx/internal/operators/ad;-><init>(Lrx/f;ZI)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lrx/functions/a;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/a;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5797
    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v0

    .line 5798
    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v1

    .line 5799
    new-instance v2, Lrx/internal/util/a;

    invoke-direct {v2, v0, v1, p1}, Lrx/internal/util/a;-><init>(Lrx/functions/b;Lrx/functions/b;Lrx/functions/a;)V

    .line 5801
    new-instance v0, Lrx/internal/operators/g;

    invoke-direct {v0, p0, v2}, Lrx/internal/operators/g;-><init>(Lrx/c;Lrx/d;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/functions/b;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/b",
            "<-",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5874
    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v0

    .line 5875
    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v1

    .line 5876
    new-instance v2, Lrx/internal/util/a;

    invoke-direct {v2, v0, p1, v1}, Lrx/internal/util/a;-><init>(Lrx/functions/b;Lrx/functions/b;Lrx/functions/a;)V

    .line 5878
    new-instance v0, Lrx/internal/operators/g;

    invoke-direct {v0, p0, v2}, Lrx/internal/operators/g;-><init>(Lrx/c;Lrx/d;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/functions/d;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/d",
            "<-TT;+",
            "Lrx/c",
            "<+TR;>;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 5102
    instance-of v0, p0, Lrx/internal/util/ScalarSynchronousObservable;

    if-eqz v0, :cond_0

    .line 5103
    check-cast p0, Lrx/internal/util/ScalarSynchronousObservable;

    .line 5104
    invoke-virtual {p0, p1}, Lrx/internal/util/ScalarSynchronousObservable;->l(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 5106
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lrx/internal/operators/d;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, Lrx/internal/operators/d;-><init>(Lrx/c;Lrx/functions/d;II)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lrx/functions/d;I)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/d",
            "<-TT;+",
            "Lrx/c",
            "<+TR;>;>;I)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 6420
    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    .line 6421
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "capacityHint > 0 required but it was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6423
    :cond_0
    new-instance v0, Lrx/internal/operators/OperatorEagerConcatMap;

    const v1, 0x7fffffff

    invoke-direct {v0, p1, p2, v1}, Lrx/internal/operators/OperatorEagerConcatMap;-><init>(Lrx/functions/d;II)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/functions/e;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/e",
            "<-TT;-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5776
    new-instance v0, Lrx/internal/operators/aa;

    invoke-direct {v0, p1}, Lrx/internal/operators/aa;-><init>(Lrx/functions/e;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/d;)Lrx/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/d",
            "<-TT;>;)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 10107
    instance-of v0, p1, Lrx/i;

    if-eqz v0, :cond_0

    .line 10108
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    .line 10113
    :goto_0
    return-object v0

    .line 10110
    :cond_0
    if-nez p1, :cond_1

    .line 10111
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "observer is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10113
    :cond_1
    new-instance v0, Lrx/internal/util/e;

    invoke-direct {v0, p1}, Lrx/internal/util/e;-><init>(Lrx/d;)V

    invoke-virtual {p0, v0}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lrx/i;)Lrx/j;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-TT;>;)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 10140
    :try_start_0
    invoke-virtual {p1}, Lrx/i;->c()V

    .line 10142
    iget-object v0, p0, Lrx/c;->a:Lrx/c$a;

    invoke-static {p0, v0}, Lcwl;->a(Lrx/c;Lrx/c$a;)Lrx/c$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lrx/c$a;->call(Ljava/lang/Object;)V

    .line 10143
    invoke-static {p1}, Lcwl;->a(Lrx/j;)Lrx/j;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 10160
    :goto_0
    return-object v0

    .line 10144
    :catch_0
    move-exception v0

    .line 10146
    invoke-static {v0}, Lrx/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 10149
    :try_start_1
    invoke-static {v0}, Lcwl;->c(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {p1, v1}, Lrx/i;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 10160
    invoke-static {}, Lcwy;->b()Lrx/j;

    move-result-object v0

    goto :goto_0

    .line 10150
    :catch_1
    move-exception v1

    .line 10151
    invoke-static {v1}, Lrx/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 10154
    new-instance v2, Lrx/exceptions/OnErrorFailedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Error occurred attempting to subscribe ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "] and then again while trying to pass to onError."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Lrx/exceptions/OnErrorFailedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 10156
    invoke-static {v2}, Lcwl;->c(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 10158
    throw v2
.end method

.method public final b(I)Lcwb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcwb",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 8744
    invoke-static {p0, p1}, Lrx/internal/operators/OperatorReplay;->b(Lrx/c;I)Lcwb;

    move-result-object v0

    return-object v0
.end method

.method public final b(JLjava/util/concurrent/TimeUnit;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5508
    invoke-static {}, Lcws;->c()Lrx/f;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lrx/c;->c(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final b(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/f;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5360
    new-instance v0, Lrx/internal/operators/x;

    invoke-direct {v0, p1, p2, p3, p4}, Lrx/internal/operators/x;-><init>(JLjava/util/concurrent/TimeUnit;Lrx/f;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Class;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TR;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 7745
    invoke-static {p1}, Lrx/internal/util/InternalObservableUtils;->a(Ljava/lang/Class;)Lrx/functions/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lrx/c;->a(Ljava/lang/Class;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Lrx/functions/e;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Lrx/functions/e",
            "<TR;-TT;TR;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 9251
    new-instance v0, Lrx/internal/operators/ag;

    invoke-direct {v0, p1, p2}, Lrx/internal/operators/ag;-><init>(Ljava/lang/Object;Lrx/functions/e;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lrx/f;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/f;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 10286
    instance-of v0, p0, Lrx/internal/util/ScalarSynchronousObservable;

    if-eqz v0, :cond_0

    .line 10287
    check-cast p0, Lrx/internal/util/ScalarSynchronousObservable;

    invoke-virtual {p0, p1}, Lrx/internal/util/ScalarSynchronousObservable;->c(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 10289
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lrx/internal/operators/aj;

    invoke-direct {v0, p0, p1}, Lrx/internal/operators/aj;-><init>(Lrx/c;Lrx/f;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lrx/functions/a;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/a;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5953
    new-instance v0, Lrx/internal/operators/ab;

    invoke-direct {v0, p1}, Lrx/internal/operators/ab;-><init>(Lrx/functions/a;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lrx/functions/b;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/b",
            "<-TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5899
    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v0

    .line 5900
    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v1

    .line 5901
    new-instance v2, Lrx/internal/util/a;

    invoke-direct {v2, p1, v0, v1}, Lrx/internal/util/a;-><init>(Lrx/functions/b;Lrx/functions/b;Lrx/functions/a;)V

    .line 5903
    new-instance v0, Lrx/internal/operators/g;

    invoke-direct {v0, p0, v2}, Lrx/internal/operators/g;-><init>(Lrx/c;Lrx/d;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lrx/functions/d;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/d",
            "<-TT;+TU;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5704
    new-instance v0, Lrx/internal/operators/z;

    invoke-direct {v0, p1}, Lrx/internal/operators/z;-><init>(Lrx/functions/d;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lrx/functions/e;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/e",
            "<-TT;-TT;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 11663
    new-instance v0, Lrx/internal/operators/as;

    const/16 v1, 0xa

    invoke-direct {v0, p1, v1}, Lrx/internal/operators/as;-><init>(Lrx/functions/e;I)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 318
    new-instance v0, Lrx/g;

    invoke-static {p0}, Lrx/internal/operators/q;->a(Lrx/c;)Lrx/internal/operators/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/g;-><init>(Lrx/g$a;)V

    return-object v0
.end method

.method public final b(Lrx/i;)Lrx/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-TT;>;)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 10205
    invoke-static {p1, p0}, Lrx/c;->a(Lrx/i;Lrx/c;)Lrx/j;

    move-result-object v0

    return-object v0
.end method

.method public c()Lrx/a;
    .locals 1

    .prologue
    .line 346
    invoke-static {p0}, Lrx/a;->a(Lrx/c;)Lrx/a;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 9435
    new-instance v0, Lrx/internal/operators/ai;

    invoke-direct {v0, p1}, Lrx/internal/operators/ai;-><init>(I)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final c(JLjava/util/concurrent/TimeUnit;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 9120
    invoke-static {}, Lcws;->c()Lrx/f;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lrx/c;->e(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final c(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/f;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5533
    new-instance v0, Lrx/internal/operators/y;

    invoke-direct {v0, p1, p2, p3, p4}, Lrx/internal/operators/y;-><init>(JLjava/util/concurrent/TimeUnit;Lrx/f;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 9383
    new-instance v0, Lrx/internal/operators/ah;

    invoke-direct {v0, p1}, Lrx/internal/operators/ah;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lrx/functions/a;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/a;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5979
    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v0

    .line 5980
    invoke-static {p1}, Lrx/functions/Actions;->a(Lrx/functions/a;)Lrx/functions/b;

    move-result-object v1

    .line 5982
    new-instance v2, Lrx/internal/util/a;

    invoke-direct {v2, v0, v1, p1}, Lrx/internal/util/a;-><init>(Lrx/functions/b;Lrx/functions/b;Lrx/functions/a;)V

    .line 5984
    new-instance v0, Lrx/internal/operators/g;

    invoke-direct {v0, p0, v2}, Lrx/internal/operators/g;-><init>(Lrx/c;Lrx/d;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lrx/functions/d;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/d",
            "<-TT;+",
            "Lrx/c",
            "<+TR;>;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 6394
    sget v0, Lrx/internal/util/h;->b:I

    invoke-virtual {p0, p1, v0}, Lrx/c;->a(Lrx/functions/d;I)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lrx/functions/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/b",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 7062
    invoke-virtual {p0, p1}, Lrx/c;->d(Lrx/functions/b;)Lrx/j;

    .line 7063
    return-void
.end method

.method public final d(I)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 10381
    new-instance v0, Lrx/internal/operators/am;

    invoke-direct {v0, p1}, Lrx/internal/operators/am;-><init>(I)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final d(JLjava/util/concurrent/TimeUnit;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 10847
    invoke-static {}, Lcws;->c()Lrx/f;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lrx/c;->f(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final d(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/f;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5581
    new-instance v0, Lrx/internal/operators/f;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lrx/internal/operators/f;-><init>(Lrx/c;JLjava/util/concurrent/TimeUnit;Lrx/f;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Object;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 9692
    invoke-static {p1}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    invoke-static {v0, p0}, Lrx/c;->b(Lrx/c;Lrx/c;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lrx/c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<+TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 4506
    invoke-static {p0, p1}, Lrx/c;->a(Lrx/c;Lrx/c;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lrx/functions/a;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/a;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 6013
    new-instance v0, Lrx/internal/operators/ac;

    invoke-direct {v0, p1}, Lrx/internal/operators/ac;-><init>(Lrx/functions/a;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lrx/functions/d;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/d",
            "<-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 6560
    new-instance v0, Lrx/internal/operators/h;

    invoke-direct {v0, p0, p1}, Lrx/internal/operators/h;-><init>(Lrx/c;Lrx/functions/d;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lrx/functions/b;)Lrx/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/b",
            "<-TT;>;)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 10004
    if-nez p1, :cond_0

    .line 10005
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "onNext can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10008
    :cond_0
    sget-object v0, Lrx/internal/util/InternalObservableUtils;->g:Lrx/functions/b;

    .line 10009
    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v1

    .line 10010
    new-instance v2, Lrx/internal/util/b;

    invoke-direct {v2, p1, v0, v1}, Lrx/internal/util/b;-><init>(Lrx/functions/b;Lrx/functions/b;Lrx/functions/a;)V

    invoke-virtual {p0, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    return-object v0
.end method

.method public final e(JLjava/util/concurrent/TimeUnit;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 10906
    invoke-virtual {p0, p1, p2, p3}, Lrx/c;->c(JLjava/util/concurrent/TimeUnit;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final e(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/f;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 9148
    new-instance v0, Lrx/internal/operators/af;

    invoke-direct {v0, p1, p2, p3, p4}, Lrx/internal/operators/af;-><init>(JLjava/util/concurrent/TimeUnit;Lrx/f;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lrx/c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<+TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5188
    invoke-static {p0, p1}, Lrx/c;->b(Lrx/c;Lrx/c;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lrx/functions/d;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/d",
            "<-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 6654
    invoke-virtual {p0, p1}, Lrx/c;->k(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->o()Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 4524
    invoke-static {}, Lrx/internal/operators/u;->a()Lrx/internal/operators/u;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final f(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/f;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 10877
    new-instance v0, Lrx/internal/operators/ao;

    invoke-direct {v0, p1, p2, p3, p4}, Lrx/internal/operators/ao;-><init>(JLjava/util/concurrent/TimeUnit;Lrx/f;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lrx/c;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<+TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5413
    if-nez p1, :cond_0

    .line 5414
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "alternate is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5416
    :cond_0
    new-instance v0, Lrx/internal/operators/al;

    invoke-direct {v0, p1}, Lrx/internal/operators/al;-><init>(Lrx/c;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lrx/functions/d;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/d",
            "<-TT;+",
            "Lrx/c",
            "<+TR;>;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 6731
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lrx/internal/util/ScalarSynchronousObservable;

    if-ne v0, v1, :cond_0

    .line 6732
    check-cast p0, Lrx/internal/util/ScalarSynchronousObservable;

    invoke-virtual {p0, p1}, Lrx/internal/util/ScalarSynchronousObservable;->l(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 6734
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->b(Lrx/c;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 4941
    invoke-static {p0}, Lrx/internal/operators/CachedObservable;->k(Lrx/c;)Lrx/internal/operators/CachedObservable;

    move-result-object v0

    return-object v0
.end method

.method public final g(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/f;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 11324
    const/4 v5, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v6, p4

    invoke-virtual/range {v1 .. v6}, Lrx/c;->a(JLjava/util/concurrent/TimeUnit;Lrx/c;Lrx/f;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final g(Lrx/c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<+TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 7581
    invoke-static {p0, p1}, Lrx/c;->c(Lrx/c;Lrx/c;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final g(Lrx/functions/d;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/d",
            "<-TT;+TK;>;)",
            "Lrx/c",
            "<",
            "Lcwc",
            "<TK;TT;>;>;"
        }
    .end annotation

    .prologue
    .line 7262
    new-instance v0, Lrx/internal/operators/OperatorGroupBy;

    invoke-direct {v0, p1}, Lrx/internal/operators/OperatorGroupBy;-><init>(Lrx/functions/d;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5231
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lrx/internal/util/InternalObservableUtils;->e:Lrx/internal/util/InternalObservableUtils$e;

    invoke-virtual {p0, v0, v1}, Lrx/c;->a(Ljava/lang/Object;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final h(Lrx/c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<+TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 8010
    invoke-static {p1}, Lrx/internal/operators/ae;->a(Lrx/c;)Lrx/internal/operators/ae;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final h(Lrx/functions/d;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/d",
            "<-TT;+TR;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 7532
    new-instance v0, Lrx/internal/operators/k;

    invoke-direct {v0, p0, p1}, Lrx/internal/operators/k;-><init>(Lrx/c;Lrx/functions/d;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5680
    invoke-static {}, Lrx/internal/operators/z;->a()Lrx/internal/operators/z;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final i(Lrx/c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 9644
    invoke-static {p1, p0}, Lrx/c;->b(Lrx/c;Lrx/c;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final i(Lrx/functions/d;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/d",
            "<-",
            "Lrx/c",
            "<TT;>;+",
            "Lrx/c",
            "<TR;>;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 8161
    invoke-static {p0, p1}, Lrx/internal/operators/OperatorPublish;->a(Lrx/c;Lrx/functions/d;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5725
    invoke-static {}, Lrx/internal/operators/aa;->a()Lrx/internal/operators/aa;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final j(Lrx/c;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<+TE;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 10767
    new-instance v0, Lrx/internal/operators/an;

    invoke-direct {v0, p1}, Lrx/internal/operators/an;-><init>(Lrx/c;)V

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final j(Lrx/functions/d;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/d",
            "<-TT;+",
            "Lrx/c",
            "<+TR;>;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 10319
    invoke-virtual {p0, p1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->c(Lrx/c;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 6631
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->o()Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final k(Lrx/functions/d;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/d",
            "<-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 10456
    invoke-virtual {p0, p1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 7931
    invoke-static {}, Lrx/internal/operators/OperatorOnBackpressureLatest;->a()Lrx/internal/operators/OperatorOnBackpressureLatest;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final m()Lcwb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcwb",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 8132
    invoke-static {p0}, Lrx/internal/operators/OperatorPublish;->k(Lrx/c;)Lcwb;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 9303
    invoke-virtual {p0}, Lrx/c;->m()Lcwb;

    move-result-object v0

    invoke-virtual {v0}, Lcwb;->r()Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 9328
    invoke-static {}, Lrx/internal/operators/ah;->a()Lrx/internal/operators/ah;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lcwa;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcwa",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 11386
    invoke-static {p0}, Lcwa;->a(Lrx/c;)Lcwa;

    move-result-object v0

    return-object v0
.end method

.method public final q()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 11416
    invoke-static {}, Lrx/internal/operators/ar;->a()Lrx/internal/operators/ar;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    return-object v0
.end method
