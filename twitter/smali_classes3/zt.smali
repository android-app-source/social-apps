.class public Lzt;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lzu;

.field private final b:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

.field private final c:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

.field private d:Z


# direct methods
.method constructor <init>(Lzu;Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;ZLcom/twitter/analytics/feature/model/MomentScribeDetails$a;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lzt;->a:Lzu;

    .line 64
    iput-object p2, p0, Lzt;->b:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    .line 65
    iput-boolean p3, p0, Lzt;->d:Z

    .line 66
    iput-object p4, p0, Lzt;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 67
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;J)Lzt;
    .locals 5

    .prologue
    .line 56
    new-instance v0, Lzt;

    invoke-static {}, Lzu;->a()Lzu;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 57
    invoke-virtual {v3, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v3

    invoke-direct {v0, v1, p0, v2, v3}, Lzt;-><init>(Lzu;Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;ZLcom/twitter/analytics/feature/model/MomentScribeDetails$a;)V

    .line 56
    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 151
    iget-object v0, p0, Lzt;->a:Lzu;

    const-string/jumbo v1, "moments:maker:recommended_tweets:%s:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "search"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "user"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lzu;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method private c(J)V
    .locals 7

    .prologue
    .line 131
    iget-object v1, p0, Lzt;->a:Lzu;

    iget-object v0, p0, Lzt;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:recommended_tweets:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "likes"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "select"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 133
    return-void
.end method

.method private d()V
    .locals 5

    .prologue
    .line 165
    iget-object v0, p0, Lzt;->a:Lzu;

    const-string/jumbo v1, "moments:maker:recommended_tweets:%s:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "search"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "query"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lzu;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method private d(J)V
    .locals 7

    .prologue
    .line 136
    iget-object v1, p0, Lzt;->a:Lzu;

    iget-object v0, p0, Lzt;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:recommended_tweets:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "likes"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "deselect"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method private e(J)V
    .locals 7

    .prologue
    .line 141
    iget-object v1, p0, Lzt;->a:Lzu;

    iget-object v0, p0, Lzt;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:recommended_tweets:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "tweets"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "select"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method private f(J)V
    .locals 7

    .prologue
    .line 146
    iget-object v1, p0, Lzt;->a:Lzu;

    iget-object v0, p0, Lzt;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:recommended_tweets:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "tweets"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "deselect"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 148
    return-void
.end method

.method private g(J)V
    .locals 7

    .prologue
    .line 155
    iget-object v1, p0, Lzt;->a:Lzu;

    iget-object v0, p0, Lzt;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:recommended_tweets:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "search"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "select_from_user"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 157
    return-void
.end method

.method private h(J)V
    .locals 7

    .prologue
    .line 160
    iget-object v1, p0, Lzt;->a:Lzu;

    iget-object v0, p0, Lzt;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:recommended_tweets:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "search"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "deselect_from_user"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 162
    return-void
.end method

.method private i(J)V
    .locals 7

    .prologue
    .line 169
    iget-object v1, p0, Lzt;->a:Lzu;

    iget-object v0, p0, Lzt;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:recommended_tweets:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "search"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "select_from_query"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method private j(J)V
    .locals 7

    .prologue
    .line 174
    iget-object v1, p0, Lzt;->a:Lzu;

    iget-object v0, p0, Lzt;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:recommended_tweets:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "search"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "deselect_from_query"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 176
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lzt;->b:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    sget-object v1, Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;->d:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/g;->b(Z)Z

    .line 123
    iget-boolean v0, p0, Lzt;->d:Z

    if-eqz v0, :cond_1

    .line 124
    invoke-direct {p0}, Lzt;->c()V

    .line 128
    :goto_1
    return-void

    .line 122
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 126
    :cond_1
    invoke-direct {p0}, Lzt;->d()V

    goto :goto_1
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 74
    sget-object v0, Lzt$1;->a:[I

    iget-object v1, p0, Lzt;->b:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 95
    :goto_0
    return-void

    .line 76
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lzt;->e(J)V

    goto :goto_0

    .line 80
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lzt;->c(J)V

    goto :goto_0

    .line 84
    :pswitch_2
    iget-boolean v0, p0, Lzt;->d:Z

    if-eqz v0, :cond_0

    .line 85
    invoke-direct {p0, p1, p2}, Lzt;->g(J)V

    goto :goto_0

    .line 87
    :cond_0
    invoke-direct {p0, p1, p2}, Lzt;->i(J)V

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 70
    iput-boolean p1, p0, Lzt;->d:Z

    .line 71
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    .line 185
    iget-object v0, p0, Lzt;->b:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    sget-object v1, Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;->d:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    if-ne v0, v1, :cond_0

    .line 186
    iget-object v0, p0, Lzt;->a:Lzu;

    const-string/jumbo v1, "moments:maker:recommended_tweets:%s:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "search"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "query_no_results"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lzu;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 188
    :cond_0
    return-void
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 98
    sget-object v0, Lzt$1;->a:[I

    iget-object v1, p0, Lzt;->b:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 119
    :goto_0
    return-void

    .line 100
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lzt;->f(J)V

    goto :goto_0

    .line 104
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lzt;->d(J)V

    goto :goto_0

    .line 108
    :pswitch_2
    iget-boolean v0, p0, Lzt;->d:Z

    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0, p1, p2}, Lzt;->h(J)V

    goto :goto_0

    .line 111
    :cond_0
    invoke-direct {p0, p1, p2}, Lzt;->j(J)V

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
