.class public final Lqq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lqr;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lqq$b;,
        Lqq$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lanr;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lqs;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lqk;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lauj",
            "<",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/av/i;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/provider/t;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lqe;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lqg;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lauj",
            "<",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            "Lqm;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lqi;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/av/watchmode/c;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/av/watchmode/b;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/av/watchmode/e;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lqq;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lqq;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lqq$a;)V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    sget-boolean v0, Lqq;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 115
    :cond_0
    invoke-direct {p0, p1}, Lqq;->a(Lqq$a;)V

    .line 116
    return-void
.end method

.method synthetic constructor <init>(Lqq$a;Lqq$1;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lqq;-><init>(Lqq$a;)V

    return-void
.end method

.method static synthetic a(Lqq;)Lcta;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lqq;->r:Lcta;

    return-object v0
.end method

.method public static a()Lqq$a;
    .locals 2

    .prologue
    .line 119
    new-instance v0, Lqq$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lqq$a;-><init>(Lqq$1;)V

    return-object v0
.end method

.method private a(Lqq$a;)V
    .locals 4

    .prologue
    .line 125
    .line 128
    invoke-static {p1}, Lqq$a;->a(Lqq$a;)Lano;

    move-result-object v0

    .line 127
    invoke-static {v0}, Lanp;->a(Lano;)Ldagger/internal/c;

    move-result-object v0

    .line 126
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->b:Lcta;

    .line 130
    iget-object v0, p0, Lqq;->b:Lcta;

    .line 131
    invoke-static {v0}, Lqw;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lqq;->c:Lcta;

    .line 134
    iget-object v0, p0, Lqq;->c:Lcta;

    .line 135
    invoke-static {v0}, Lqv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lqq;->d:Lcta;

    .line 138
    new-instance v0, Lqq$1;

    invoke-direct {v0, p0, p1}, Lqq$1;-><init>(Lqq;Lqq$a;)V

    iput-object v0, p0, Lqq;->e:Lcta;

    .line 151
    new-instance v0, Lqq$2;

    invoke-direct {v0, p0, p1}, Lqq$2;-><init>(Lqq;Lqq$a;)V

    iput-object v0, p0, Lqq;->f:Lcta;

    .line 167
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lqq;->e:Lcta;

    iget-object v2, p0, Lqq;->f:Lcta;

    .line 166
    invoke-static {v0, v1, v2}, Lql;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 165
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->g:Lcta;

    .line 171
    iget-object v0, p0, Lqq;->g:Lcta;

    .line 172
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->h:Lcta;

    .line 174
    new-instance v0, Lqq$3;

    invoke-direct {v0, p0, p1}, Lqq$3;-><init>(Lqq;Lqq$a;)V

    iput-object v0, p0, Lqq;->i:Lcta;

    .line 190
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lqq;->i:Lcta;

    .line 189
    invoke-static {v0, v1}, Lqf;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 188
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->j:Lcta;

    .line 192
    iget-object v0, p0, Lqq;->d:Lcta;

    iget-object v1, p0, Lqq;->h:Lcta;

    iget-object v2, p0, Lqq;->j:Lcta;

    .line 194
    invoke-static {v0, v1, v2}, Lqh;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 193
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->k:Lcta;

    .line 199
    iget-object v0, p0, Lqq;->k:Lcta;

    .line 200
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->l:Lcta;

    .line 202
    iget-object v0, p0, Lqq;->l:Lcta;

    .line 204
    invoke-static {v0}, Lqj;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 203
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->m:Lcta;

    .line 206
    iget-object v0, p0, Lqq;->c:Lcta;

    .line 207
    invoke-static {v0}, Lqu;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lqq;->n:Lcta;

    .line 210
    iget-object v0, p0, Lqq;->m:Lcta;

    iget-object v1, p0, Lqq;->d:Lcta;

    iget-object v2, p0, Lqq;->n:Lcta;

    .line 212
    invoke-static {v0, v1, v2}, Lcom/twitter/android/av/watchmode/d;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 211
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->o:Lcta;

    .line 217
    iget-object v0, p0, Lqq;->o:Lcta;

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->p:Lcta;

    .line 219
    iget-object v0, p0, Lqq;->p:Lcta;

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->q:Lcta;

    .line 224
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lqq;->e:Lcta;

    iget-object v2, p0, Lqq;->n:Lcta;

    iget-object v3, p0, Lqq;->d:Lcta;

    .line 223
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/av/watchmode/f;->a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 222
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->r:Lcta;

    .line 229
    iget-object v0, p0, Lqq;->r:Lcta;

    .line 230
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->s:Lcta;

    .line 232
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 233
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lqq;->q:Lcta;

    .line 234
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lqq;->s:Lcta;

    .line 235
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 236
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lqq;->t:Lcta;

    .line 238
    new-instance v0, Lqq$4;

    invoke-direct {v0, p0, p1}, Lqq$4;-><init>(Lqq;Lqq$a;)V

    iput-object v0, p0, Lqq;->u:Lcta;

    .line 251
    iget-object v0, p0, Lqq;->u:Lcta;

    .line 253
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 252
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq;->v:Lcta;

    .line 255
    new-instance v0, Lqq$5;

    invoke-direct {v0, p0, p1}, Lqq$5;-><init>(Lqq;Lqq$a;)V

    iput-object v0, p0, Lqq;->w:Lcta;

    .line 267
    return-void
.end method

.method static synthetic b(Lqq;)Lcta;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lqq;->p:Lcta;

    return-object v0
.end method

.method static synthetic c(Lqq;)Lcta;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lqq;->w:Lcta;

    return-object v0
.end method

.method static synthetic d(Lqq;)Lcta;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lqq;->s:Lcta;

    return-object v0
.end method

.method static synthetic e(Lqq;)Lcta;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lqq;->q:Lcta;

    return-object v0
.end method


# virtual methods
.method public a(Lant;)Lqx;
    .locals 2

    .prologue
    .line 281
    new-instance v0, Lqq$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lqq$b;-><init>(Lqq;Lant;Lqq$1;)V

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lqq;->t:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lqq;->v:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method
