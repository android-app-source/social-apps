.class Lcyg$d;
.super Lcyg$e;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcyg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcyg;


# direct methods
.method constructor <init>(Lcyg;)V
    .locals 1

    .prologue
    .line 999
    iput-object p1, p0, Lcyg$d;->a:Lcyg;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcyg$e;-><init>(Lcyg$1;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 14

    .prologue
    const/4 v2, -0x2

    const/4 v4, 0x1

    const-wide/16 v12, 0x3e8

    const/4 v1, 0x0

    .line 1003
    iget-object v0, p0, Lcyg$d;->a:Lcyg;

    iget-object v0, v0, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1004
    iget-object v0, p0, Lcyg$d;->a:Lcyg;

    iget-object v0, v0, Lcyg;->d:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 1005
    new-instance v7, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v7}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 1008
    :try_start_0
    iget-object v0, p0, Lcyg$d;->a:Lcyg;

    iget-object v0, v0, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1022
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcyg$d;->a:Lcyg;

    iget-object v0, v0, Lcyg;->d:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    move v0, v1

    move v2, v1

    move v3, v1

    .line 1039
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcyg$d;->e()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1040
    iget-object v8, p0, Lcyg$d;->a:Lcyg;

    iget-object v8, v8, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v8, v7, v12, v13}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v8

    .line 1041
    if-ltz v8, :cond_3

    .line 1044
    aget-object v0, v5, v8

    .line 1045
    iget v9, v7, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1046
    iget v9, v7, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v10, v7, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v9, v10

    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 1047
    iget-object v9, p0, Lcyg$d;->a:Lcyg;

    iget-object v9, v9, Lcyg;->f:Lcyf$a;

    invoke-interface {v9, v0, v7}, Lcyf$a;->b(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 1050
    iget-object v0, p0, Lcyg$d;->a:Lcyg;

    iget-object v0, v0, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0, v8, v1}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    move v0, v4

    .line 1052
    :cond_3
    iget-object v8, p0, Lcyg$d;->a:Lcyg;

    iget-object v8, v8, Lcyg;->d:Landroid/media/MediaCodec;

    invoke-virtual {v8, v7, v12, v13}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v8

    .line 1053
    if-ltz v8, :cond_4

    .line 1056
    aget-object v2, v6, v8

    .line 1057
    iget v9, v7, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v2, v9}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 1058
    iget-object v9, p0, Lcyg$d;->a:Lcyg;

    iget-object v9, v9, Lcyg;->f:Lcyf$a;

    invoke-interface {v9, v2, v7}, Lcyf$a;->a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 1061
    iget-object v2, p0, Lcyg$d;->a:Lcyg;

    iget-object v2, v2, Lcyg;->d:Landroid/media/MediaCodec;

    invoke-virtual {v2, v8, v1}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    move v2, v4

    .line 1063
    :cond_4
    if-nez v3, :cond_2

    if-eqz v0, :cond_2

    if-eqz v2, :cond_2

    .line 1067
    iget-object v3, p0, Lcyg$d;->a:Lcyg;

    iget-object v3, v3, Lcyg;->y:Landroid/media/MediaFormat;

    if-nez v3, :cond_5

    .line 1068
    iget-object v3, p0, Lcyg$d;->a:Lcyg;

    iget-object v8, p0, Lcyg$d;->a:Lcyg;

    iget-object v8, v8, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v8}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v8

    iput-object v8, v3, Lcyg;->y:Landroid/media/MediaFormat;

    .line 1070
    :cond_5
    iget-object v3, p0, Lcyg$d;->a:Lcyg;

    iget-object v3, v3, Lcyg;->f:Lcyf$a;

    iget-object v8, p0, Lcyg$d;->a:Lcyg;

    iget-object v8, v8, Lcyg;->y:Landroid/media/MediaFormat;

    iget-object v9, p0, Lcyg$d;->a:Lcyg;

    iget-object v9, v9, Lcyg;->d:Landroid/media/MediaCodec;

    invoke-virtual {v9}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v9

    invoke-interface {v3, v8, v9}, Lcyf$a;->a(Landroid/media/MediaFormat;Landroid/media/MediaFormat;)V

    .line 1071
    invoke-virtual {p0}, Lcyg$d;->f()V

    move v3, v4

    .line 1072
    goto :goto_0

    .line 1009
    :catch_0
    move-exception v0

    .line 1011
    :cond_6
    invoke-virtual {p0}, Lcyg$d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1012
    iget-object v0, p0, Lcyg$d;->a:Lcyg;

    iget-object v0, v0, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0, v7, v12, v13}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v0

    .line 1013
    if-eq v0, v2, :cond_0

    .line 1015
    if-ltz v0, :cond_6

    .line 1016
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Unexpected audio buffer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1023
    :catch_1
    move-exception v0

    .line 1025
    :cond_7
    invoke-virtual {p0}, Lcyg$d;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1026
    iget-object v0, p0, Lcyg$d;->a:Lcyg;

    iget-object v0, v0, Lcyg;->d:Landroid/media/MediaCodec;

    invoke-virtual {v0, v7, v12, v13}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v0

    .line 1027
    if-eq v0, v2, :cond_1

    .line 1029
    if-ltz v0, :cond_7

    .line 1030
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Unexpected video buffer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1075
    :cond_8
    return-void
.end method
