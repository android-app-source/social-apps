.class public Lnt;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbkn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)Z
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    .line 42
    invoke-static {v0}, Lcom/twitter/model/av/f;->a(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/model/av/DynamicAd;

    move-result-object v0

    .line 43
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/av/DynamicAd;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p2}, Lcom/twitter/model/av/DynamicAd;->a(Lcom/twitter/model/av/AVMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/playback/AVPlayer;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/av/playback/AVPlayer;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbiy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/av/playback/AVPlayer;",
            "Lcom/twitter/model/av/AVMedia;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbiy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lnt;->b(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    new-instance v0, Lns;

    invoke-direct {v0, p1, p2}, Lns;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 32
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
