.class public Ltq;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ltr;

.field private final b:Lto;

.field private c:Lbrc;

.field private d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private e:Z

.field private final f:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;


# direct methods
.method constructor <init>(Landroid/content/Context;Ltr;Lto;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltq;->e:Z

    .line 40
    new-instance v0, Ltq$1;

    invoke-direct {v0, p0}, Ltq$1;-><init>(Ltq;)V

    iput-object v0, p0, Ltq;->f:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;

    .line 68
    iput-object p2, p0, Ltq;->a:Ltr;

    .line 69
    iput-object p3, p0, Ltq;->b:Lto;

    .line 70
    iget-object v0, p0, Ltq;->b:Lto;

    iget-object v1, p0, Ltq;->f:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;

    invoke-virtual {v0, v1}, Lto;->a(Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;)V

    .line 71
    iget-object v0, p0, Ltq;->a:Ltr;

    iget-object v1, p0, Ltq;->b:Lto;

    invoke-virtual {v1}, Lto;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltr;->a(Landroid/view/View;)V

    .line 72
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Ltq;
    .locals 5

    .prologue
    .line 57
    new-instance v0, Ltq;

    new-instance v1, Ltr;

    invoke-direct {v1, p0}, Ltr;-><init>(Landroid/content/Context;)V

    new-instance v2, Lto;

    new-instance v3, Ltp;

    invoke-direct {v3, p0}, Ltp;-><init>(Landroid/content/Context;)V

    new-instance v4, Lbre;

    invoke-direct {v4, p1}, Lbre;-><init>(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    invoke-direct {v2, p0, v3, v4}, Lto;-><init>(Landroid/app/Activity;Ltp;Lbre;)V

    invoke-direct {v0, p0, v1, v2}, Ltq;-><init>(Landroid/content/Context;Ltr;Lto;)V

    return-object v0
.end method

.method static synthetic a(Ltq;)Ltr;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltq;->a:Ltr;

    return-object v0
.end method

.method static synthetic a(Ltq;Z)Z
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Ltq;->e:Z

    return p1
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Ltq;->c:Lbrc;

    if-nez v0, :cond_0

    .line 96
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Ltq;->b:Lto;

    iget-object v1, p0, Ltq;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v2, p0, Ltq;->c:Lbrc;

    invoke-virtual {v0, v1, v2}, Lto;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lbrc;)V

    goto :goto_0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ltq;->a:Ltr;

    invoke-virtual {v0}, Ltr;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v0, p0, Ltq;->b:Lto;

    invoke-virtual {v0, p1}, Lto;->a(Landroid/view/View$OnClickListener;)V

    .line 111
    return-void
.end method

.method public a(Lbrc;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 1

    .prologue
    .line 80
    iput-object p1, p0, Ltq;->c:Lbrc;

    .line 81
    iget-object v0, p0, Ltq;->a:Ltr;

    invoke-virtual {v0, p1}, Ltr;->a(Lbrc;)V

    .line 82
    iput-object p2, p0, Ltq;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 84
    iget-object v0, p0, Ltq;->b:Lto;

    invoke-virtual {v0, p2, p1}, Lto;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lbrc;)V

    .line 85
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ltq;->b:Lto;

    invoke-virtual {v0, p1}, Lto;->a(Lcom/twitter/model/core/Tweet;)V

    .line 89
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Ltq;->c:Lbrc;

    .line 100
    iget-object v0, p0, Ltq;->b:Lto;

    invoke-virtual {v0}, Lto;->d()V

    .line 101
    return-void
.end method

.method public c()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Ltq;->b:Lto;

    invoke-virtual {v0}, Lto;->e()Lcom/twitter/library/widget/a;

    move-result-object v0

    return-object v0
.end method

.method public d()Landroid/view/View;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ltq;->a:Ltr;

    invoke-virtual {v0}, Ltr;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Ltq;->e:Z

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Ltq;->b:Lto;

    invoke-virtual {v0}, Lto;->c()V

    .line 129
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Ltq;->b:Lto;

    invoke-virtual {v0}, Lto;->b()V

    .line 133
    return-void
.end method
