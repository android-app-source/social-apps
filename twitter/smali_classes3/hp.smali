.class public Lhp;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected final a:Landroid/graphics/Matrix;

.field protected b:Landroid/graphics/RectF;

.field protected c:F

.field protected d:F

.field protected e:[F

.field protected f:Landroid/graphics/Matrix;

.field protected final g:[F

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x9

    const v3, 0x7f7fffff    # Float.MAX_VALUE

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lhp;->a:Landroid/graphics/Matrix;

    .line 24
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    .line 26
    iput v1, p0, Lhp;->c:F

    .line 27
    iput v1, p0, Lhp;->d:F

    .line 32
    iput v2, p0, Lhp;->h:F

    .line 37
    iput v3, p0, Lhp;->i:F

    .line 42
    iput v2, p0, Lhp;->j:F

    .line 47
    iput v3, p0, Lhp;->k:F

    .line 52
    iput v2, p0, Lhp;->l:F

    .line 57
    iput v2, p0, Lhp;->m:F

    .line 62
    iput v1, p0, Lhp;->n:F

    .line 67
    iput v1, p0, Lhp;->o:F

    .line 72
    iput v1, p0, Lhp;->p:F

    .line 77
    iput v1, p0, Lhp;->q:F

    .line 307
    new-array v0, v4, [F

    iput-object v0, p0, Lhp;->e:[F

    .line 373
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lhp;->f:Landroid/graphics/Matrix;

    .line 402
    new-array v0, v4, [F

    iput-object v0, p0, Lhp;->g:[F

    .line 84
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    return v0
.end method

.method public a(Landroid/graphics/Matrix;Landroid/view/View;Z)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lhp;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 415
    iget-object v0, p0, Lhp;->a:Landroid/graphics/Matrix;

    iget-object v1, p0, Lhp;->b:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lhp;->a(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V

    .line 417
    if-eqz p3, :cond_0

    .line 418
    invoke-virtual {p2}, Landroid/view/View;->invalidate()V

    .line 420
    :cond_0
    iget-object v0, p0, Lhp;->a:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 421
    return-object p1
.end method

.method public a(F)V
    .locals 2

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 475
    cmpg-float v1, p1, v0

    if-gez v1, :cond_0

    move p1, v0

    .line 478
    :cond_0
    iput p1, p0, Lhp;->j:F

    .line 480
    iget-object v0, p0, Lhp;->a:Landroid/graphics/Matrix;

    iget-object v1, p0, Lhp;->b:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lhp;->a(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V

    .line 481
    return-void
.end method

.method public a(FF)V
    .locals 4

    .prologue
    .line 95
    invoke-virtual {p0}, Lhp;->a()F

    move-result v0

    .line 96
    invoke-virtual {p0}, Lhp;->c()F

    move-result v1

    .line 97
    invoke-virtual {p0}, Lhp;->b()F

    move-result v2

    .line 98
    invoke-virtual {p0}, Lhp;->d()F

    move-result v3

    .line 100
    iput p2, p0, Lhp;->d:F

    .line 101
    iput p1, p0, Lhp;->c:F

    .line 103
    invoke-virtual {p0, v0, v1, v2, v3}, Lhp;->a(FFFF)V

    .line 104
    return-void
.end method

.method public a(FFFF)V
    .locals 3

    .prologue
    .line 115
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v1, p0, Lhp;->c:F

    sub-float/2addr v1, p3

    iget v2, p0, Lhp;->d:F

    sub-float/2addr v2, p4

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 117
    return-void
.end method

.method public a(FFFFLandroid/graphics/Matrix;)V
    .locals 1

    .prologue
    .line 263
    invoke-virtual {p5}, Landroid/graphics/Matrix;->reset()V

    .line 264
    iget-object v0, p0, Lhp;->a:Landroid/graphics/Matrix;

    invoke-virtual {p5, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 265
    invoke-virtual {p5, p1, p2, p3, p4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 266
    return-void
.end method

.method public a(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 431
    iget-object v1, p0, Lhp;->g:[F

    invoke-virtual {p1, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 433
    iget-object v1, p0, Lhp;->g:[F

    aget v2, v1, v8

    .line 434
    iget-object v1, p0, Lhp;->g:[F

    aget v1, v1, v7

    .line 436
    iget-object v3, p0, Lhp;->g:[F

    const/4 v4, 0x5

    aget v3, v3, v4

    .line 437
    iget-object v4, p0, Lhp;->g:[F

    aget v4, v4, v9

    .line 440
    iget v5, p0, Lhp;->j:F

    invoke-static {v5, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v5, p0, Lhp;->k:F

    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, Lhp;->l:F

    .line 443
    iget v1, p0, Lhp;->h:F

    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v4, p0, Lhp;->i:F

    invoke-static {v1, v4}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, Lhp;->m:F

    .line 448
    if-eqz p2, :cond_0

    .line 449
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 450
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v0

    .line 453
    :goto_0
    neg-float v1, v1

    iget v4, p0, Lhp;->l:F

    sub-float/2addr v4, v6

    mul-float/2addr v1, v4

    .line 454
    iget v4, p0, Lhp;->p:F

    sub-float/2addr v1, v4

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v2, p0, Lhp;->p:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, Lhp;->n:F

    .line 456
    iget v1, p0, Lhp;->m:F

    sub-float/2addr v1, v6

    mul-float/2addr v0, v1

    .line 457
    iget v1, p0, Lhp;->q:F

    add-float/2addr v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v1, p0, Lhp;->q:F

    neg-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lhp;->o:F

    .line 459
    iget-object v0, p0, Lhp;->g:[F

    iget v1, p0, Lhp;->n:F

    aput v1, v0, v8

    .line 460
    iget-object v0, p0, Lhp;->g:[F

    iget v1, p0, Lhp;->l:F

    aput v1, v0, v7

    .line 462
    iget-object v0, p0, Lhp;->g:[F

    const/4 v1, 0x5

    iget v2, p0, Lhp;->o:F

    aput v2, v0, v1

    .line 463
    iget-object v0, p0, Lhp;->g:[F

    iget v1, p0, Lhp;->m:F

    aput v1, v0, v9

    .line 465
    iget-object v0, p0, Lhp;->g:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->setValues([F)V

    .line 466
    return-void

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method public a([FLandroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 387
    iget-object v0, p0, Lhp;->f:Landroid/graphics/Matrix;

    .line 388
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 389
    iget-object v1, p0, Lhp;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 391
    const/4 v1, 0x0

    aget v1, p1, v1

    invoke-virtual {p0}, Lhp;->a()F

    move-result v2

    sub-float/2addr v1, v2

    .line 392
    aget v2, p1, v4

    invoke-virtual {p0}, Lhp;->c()F

    move-result v3

    sub-float/2addr v2, v3

    .line 394
    neg-float v1, v1

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 396
    invoke-virtual {p0, v0, p2, v4}, Lhp;->a(Landroid/graphics/Matrix;Landroid/view/View;Z)Landroid/graphics/Matrix;

    .line 397
    return-void
.end method

.method public b()F
    .locals 2

    .prologue
    .line 124
    iget v0, p0, Lhp;->c:F

    iget-object v1, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public b(F)V
    .locals 2

    .prologue
    .line 490
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 491
    const p1, 0x7f7fffff    # Float.MAX_VALUE

    .line 493
    :cond_0
    iput p1, p0, Lhp;->k:F

    .line 495
    iget-object v0, p0, Lhp;->a:Landroid/graphics/Matrix;

    iget-object v1, p0, Lhp;->b:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lhp;->a(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V

    .line 496
    return-void
.end method

.method public b(FF)Z
    .locals 1

    .prologue
    .line 587
    invoke-virtual {p0, p1}, Lhp;->e(F)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lhp;->f(F)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()F
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    return v0
.end method

.method public c(F)V
    .locals 2

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 525
    cmpg-float v1, p1, v0

    if-gez v1, :cond_0

    move p1, v0

    .line 528
    :cond_0
    iput p1, p0, Lhp;->h:F

    .line 530
    iget-object v0, p0, Lhp;->a:Landroid/graphics/Matrix;

    iget-object v1, p0, Lhp;->b:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lhp;->a(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V

    .line 531
    return-void
.end method

.method public d()F
    .locals 2

    .prologue
    .line 132
    iget v0, p0, Lhp;->d:F

    iget-object v1, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public d(F)V
    .locals 2

    .prologue
    .line 540
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 541
    const p1, 0x7f7fffff    # Float.MAX_VALUE

    .line 543
    :cond_0
    iput p1, p0, Lhp;->i:F

    .line 545
    iget-object v0, p0, Lhp;->a:Landroid/graphics/Matrix;

    iget-object v1, p0, Lhp;->b:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lhp;->a(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V

    .line 546
    return-void
.end method

.method public e()F
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    return v0
.end method

.method public e(F)Z
    .locals 1

    .prologue
    .line 579
    invoke-virtual {p0, p1}, Lhp;->g(F)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lhp;->h(F)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()F
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    return v0
.end method

.method public f(F)Z
    .locals 1

    .prologue
    .line 583
    invoke-virtual {p0, p1}, Lhp;->i(F)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lhp;->j(F)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()F
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    return v0
.end method

.method public g(F)Z
    .locals 2

    .prologue
    .line 591
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v1, p1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()F
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    return v0
.end method

.method public h(F)Z
    .locals 3

    .prologue
    const/high16 v1, 0x42c80000    # 100.0f

    .line 595
    mul-float v0, p1, v1

    float-to-int v0, v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    .line 596
    iget-object v1, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v0, v2

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()F
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    return v0
.end method

.method public i(F)Z
    .locals 1

    .prologue
    .line 600
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()F
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    return v0
.end method

.method public j(F)Z
    .locals 2

    .prologue
    const/high16 v1, 0x42c80000    # 100.0f

    .line 604
    mul-float v0, p1, v1

    float-to-int v0, v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    .line 605
    iget-object v1, p0, Lhp;->b:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    return-object v0
.end method

.method public k(F)V
    .locals 1

    .prologue
    .line 692
    invoke-static {p1}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lhp;->p:F

    .line 693
    return-void
.end method

.method public l()Lhk;
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    iget-object v1, p0, Lhp;->b:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    invoke-static {v0, v1}, Lhk;->a(FF)Lhk;

    move-result-object v0

    return-object v0
.end method

.method public l(F)V
    .locals 1

    .prologue
    .line 702
    invoke-static {p1}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lhp;->q:F

    .line 703
    return-void
.end method

.method public m()F
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lhp;->d:F

    return v0
.end method

.method public n()F
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lhp;->c:F

    return v0
.end method

.method public o()F
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lhp;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lhp;->b:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public p()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lhp;->a:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public q()F
    .locals 1

    .prologue
    .line 612
    iget v0, p0, Lhp;->l:F

    return v0
.end method

.method public r()F
    .locals 1

    .prologue
    .line 619
    iget v0, p0, Lhp;->m:F

    return v0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 663
    invoke-virtual {p0}, Lhp;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lhp;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Z
    .locals 2

    .prologue
    .line 672
    iget v0, p0, Lhp;->m:F

    iget v1, p0, Lhp;->h:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lhp;->h:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 682
    iget v0, p0, Lhp;->l:F

    iget v1, p0, Lhp;->j:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lhp;->j:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 711
    iget v0, p0, Lhp;->p:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lhp;->q:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 720
    iget v0, p0, Lhp;->l:F

    iget v1, p0, Lhp;->j:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public x()Z
    .locals 2

    .prologue
    .line 729
    iget v0, p0, Lhp;->l:F

    iget v1, p0, Lhp;->k:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()Z
    .locals 2

    .prologue
    .line 738
    iget v0, p0, Lhp;->m:F

    iget v1, p0, Lhp;->h:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()Z
    .locals 2

    .prologue
    .line 747
    iget v0, p0, Lhp;->m:F

    iget v1, p0, Lhp;->i:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
