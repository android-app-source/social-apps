.class public Lcph;
.super Lcpd;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcph$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcpc;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcph$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcpa;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcpd;-><init>()V

    .line 13
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcph;->a:Ljava/util/List;

    .line 14
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcph;->b:Ljava/util/List;

    return-void
.end method

.method private a(Lcpb;Z)V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcph;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcph$a;

    .line 124
    invoke-interface {v0, p1, p2}, Lcph$a;->a(Lcpb;Z)V

    goto :goto_0

    .line 126
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcpb;)V
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcph;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 61
    invoke-virtual {p1}, Lcpb;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcph;->a(Lcpb;Z)V

    .line 63
    iget-object v0, p0, Lcph;->c:Lcpa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcph;->c:Lcpa;

    invoke-virtual {v0}, Lcpa;->h()Lcpa$b;

    move-result-object v0

    move-object v1, v0

    .line 65
    :goto_0
    iget-object v0, p0, Lcph;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpc;

    .line 66
    invoke-interface {v0, p1, v1}, Lcpc;->a(Lcpb;Lcpa$b;)V

    goto :goto_1

    .line 63
    :cond_0
    sget-object v0, Lcpa$b;->c:Lcpa$b;

    move-object v1, v0

    goto :goto_0

    .line 70
    :cond_1
    return-void
.end method

.method public a(Lcpc;)V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcph;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    return-void
.end method

.method public a(Lcph$a;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcph;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    return-void
.end method

.method declared-synchronized a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcph;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpc;

    .line 118
    invoke-interface {v0, p1, p2}, Lcpc;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 120
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcpg;->a(Ljava/lang/Throwable;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Lcpb;

    invoke-direct {v0, p1}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v0}, Lcph;->c(Lcpb;)V

    .line 98
    :cond_0
    return-void
.end method

.method public b()Lcpa;
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcph;->c:Lcpa;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Lcpa;

    invoke-direct {v0}, Lcpa;-><init>()V

    iput-object v0, p0, Lcph;->c:Lcpa;

    .line 23
    iget-object v0, p0, Lcph;->c:Lcpa;

    new-instance v1, Lcph$1;

    invoke-direct {v1, p0}, Lcph$1;-><init>(Lcph;)V

    invoke-virtual {v0, v1}, Lcpa;->a(Lcpa$a;)V

    .line 30
    :cond_0
    iget-object v0, p0, Lcph;->c:Lcpa;

    return-object v0
.end method

.method public declared-synchronized b(Lcpb;)V
    .locals 3

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcph;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 78
    invoke-virtual {p1}, Lcpb;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcph;->a(Lcpb;Z)V

    .line 80
    iget-object v0, p0, Lcph;->c:Lcpa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcph;->c:Lcpa;

    invoke-virtual {v0}, Lcpa;->h()Lcpa$b;

    move-result-object v0

    move-object v1, v0

    .line 82
    :goto_0
    iget-object v0, p0, Lcph;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpc;

    .line 83
    invoke-interface {v0, p1, v1}, Lcpc;->b(Lcpb;Lcpa$b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 80
    :cond_0
    :try_start_1
    sget-object v0, Lcpa$b;->c:Lcpa$b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v0

    goto :goto_0

    .line 87
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized b(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 106
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-static {p1, v0}, Lcpg;->a(Ljava/lang/Throwable;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lcpb;

    invoke-direct {v0, p1}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v0}, Lcph;->d(Lcpb;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    :cond_0
    monitor-exit p0

    return-void

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
