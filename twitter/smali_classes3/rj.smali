.class public Lrj;
.super Lcom/twitter/android/card/n;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/ad$a;
.implements Lcom/twitter/library/card/af$a;
.implements Lcom/twitter/library/card/q$a;
.implements Lcom/twitter/library/widget/a;
.implements Lri$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrj$a;
    }
.end annotation


# instance fields
.field a:Lcom/twitter/model/core/Tweet;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field b:Lbrc;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final c:Lcom/twitter/library/card/ad;

.field private final d:Ltq;

.field private final e:Lts;

.field private final f:Ltn;

.field private final g:Lrj$a;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lri;Lcom/twitter/android/card/b;Ltq;Lts;Ltn;)V
    .locals 3

    .prologue
    .line 75
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/card/n;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lrj;->h:I

    .line 76
    iput-object p5, p0, Lrj;->d:Ltq;

    .line 77
    iput-object p6, p0, Lrj;->e:Lts;

    .line 78
    iput-object p7, p0, Lrj;->f:Ltn;

    .line 79
    new-instance v0, Lcom/twitter/library/card/ad;

    invoke-direct {v0, p0}, Lcom/twitter/library/card/ad;-><init>(Lcom/twitter/library/card/ad$a;)V

    iput-object v0, p0, Lrj;->c:Lcom/twitter/library/card/ad;

    .line 80
    new-instance v0, Lrj$a;

    iget-object v1, p0, Lrj;->d:Ltq;

    invoke-virtual {p0, p1}, Lrj;->a(Landroid/app/Activity;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lrj$a;-><init>(Ltq;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lrj;->g:Lrj$a;

    .line 81
    invoke-virtual {p3, p0}, Lri;->a(Lri$a;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Ltq;Lts;Ltn;)V
    .locals 8

    .prologue
    .line 65
    new-instance v3, Lri;

    invoke-direct {v3, p1}, Lri;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/twitter/android/card/c;

    invoke-direct {v4, p1}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lrj;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lri;Lcom/twitter/android/card/b;Ltq;Lts;Ltn;)V

    .line 67
    return-void
.end method

.method private a(Lcar;)V
    .locals 4

    .prologue
    .line 175
    iget-object v0, p0, Lrj;->e:Lts;

    iget-wide v2, p0, Lrj;->y:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lts;->a(Ljava/lang/Long;Lcar;)Lbrc;

    move-result-object v0

    iput-object v0, p0, Lrj;->b:Lbrc;

    .line 176
    iget-object v0, p0, Lrj;->g:Lrj$a;

    iget-object v1, p0, Lrj;->b:Lbrc;

    iget-object v2, p0, Lrj;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v1, v2}, Lrj$a;->a(Lbrc;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 177
    iget-object v0, p0, Lrj;->b:Lbrc;

    invoke-virtual {v0}, Lbrc;->f()Lcom/twitter/model/livevideo/BroadcastState;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/livevideo/BroadcastState;->b:Lcom/twitter/model/livevideo/BroadcastState;

    if-eq v0, v1, :cond_0

    .line 178
    invoke-virtual {p0}, Lrj;->au_()V

    .line 180
    :cond_0
    return-void
.end method

.method private p()J
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lrj;->b:Lbrc;

    if-nez v0, :cond_0

    const-wide/high16 v0, -0x8000000000000000L

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lrj;->b:Lbrc;

    .line 94
    invoke-virtual {v0}, Lbrc;->c()J

    move-result-wide v0

    goto :goto_0
.end method

.method private q()J
    .locals 3

    .prologue
    const-wide/high16 v0, -0x8000000000000000L

    .line 98
    iget-object v2, p0, Lrj;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lrj;->s()Z

    move-result v2

    if-nez v2, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-object v2, p0, Lrj;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method private s()Z
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lrj;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "live_video_timeline"

    iget-object v1, p0, Lrj;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 116
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 115
    :goto_0
    return v0

    .line 116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/app/Activity;)Landroid/view/View$OnClickListener;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 221
    new-instance v0, Lrj$1;

    invoke-direct {v0, p0, p1}, Lrj$1;-><init>(Lrj;Landroid/app/Activity;)V

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 189
    invoke-super {p0}, Lcom/twitter/android/card/n;->a()V

    .line 190
    iget-object v0, p0, Lrj;->d:Ltq;

    invoke-virtual {v0}, Ltq;->b()V

    .line 191
    invoke-virtual {p0}, Lrj;->k()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lrj;->y:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->b(JLjava/lang/Object;)V

    .line 192
    iget-object v0, p0, Lrj;->f:Ltn;

    invoke-virtual {v0}, Ltn;->a()V

    .line 193
    iget-object v0, p0, Lrj;->c:Lcom/twitter/library/card/ad;

    invoke-virtual {v0}, Lcom/twitter/library/card/ad;->b()V

    .line 194
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lrj;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lrj;->h:I

    .line 216
    return-void
.end method

.method public a(ILcar;)V
    .locals 1

    .prologue
    .line 209
    invoke-direct {p0, p2}, Lrj;->a(Lcar;)V

    .line 210
    const/4 v0, 0x0

    iput v0, p0, Lrj;->h:I

    .line 211
    return-void
.end method

.method public a(JLcar;)V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0, p3}, Lrj;->a(Lcar;)V

    .line 172
    return-void
.end method

.method public a(JLcom/twitter/library/card/CardContext;)V
    .locals 3

    .prologue
    .line 162
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/card/n;->a(JLcom/twitter/library/card/CardContext;)V

    .line 164
    invoke-static {p3}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 165
    iput-object v0, p0, Lrj;->a:Lcom/twitter/model/core/Tweet;

    .line 166
    iget-object v1, p0, Lrj;->d:Ltq;

    invoke-virtual {v1, v0}, Ltq;->a(Lcom/twitter/model/core/Tweet;)V

    .line 167
    return-void
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 4

    .prologue
    .line 134
    invoke-super {p0, p1}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/z$a;)V

    .line 135
    iget-wide v0, p1, Lcom/twitter/library/card/z$a;->b:J

    iput-wide v0, p0, Lrj;->y:J

    .line 136
    invoke-virtual {p0}, Lrj;->k()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lrj;->y:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->a(JLjava/lang/Object;)V

    .line 137
    iget-object v0, p0, Lrj;->f:Ltn;

    iget-wide v2, p0, Lrj;->y:J

    invoke-virtual {v0, v2, v3, p0}, Ltn;->a(JLcom/twitter/library/card/af$a;)V

    .line 138
    iget-object v0, p0, Lrj;->c:Lcom/twitter/library/card/ad;

    invoke-virtual {v0}, Lcom/twitter/library/card/ad;->a()V

    .line 139
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 40
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lrj;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0, p1}, Lcom/twitter/android/card/n;->a(Z)V

    .line 150
    iget-object v0, p0, Lrj;->d:Ltq;

    invoke-virtual {v0}, Ltq;->g()V

    .line 151
    return-void
.end method

.method public aj_()V
    .locals 1

    .prologue
    .line 143
    invoke-super {p0}, Lcom/twitter/android/card/n;->aj_()V

    .line 144
    iget-object v0, p0, Lrj;->c:Lcom/twitter/library/card/ad;

    invoke-virtual {v0}, Lcom/twitter/library/card/ad;->d()V

    .line 145
    return-void
.end method

.method public as_()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lrj;->b:Lbrc;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lrj;->f:Ltn;

    iget-object v1, p0, Lrj;->b:Lbrc;

    invoke-virtual {v0, v1}, Ltn;->a(Lbrc;)V

    .line 130
    :cond_0
    return-void
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lrj;->d:Ltq;

    invoke-virtual {v0}, Ltq;->c()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->at_()V

    .line 251
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lrj;->d:Ltq;

    invoke-virtual {v0}, Ltq;->c()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->au_()V

    .line 256
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lrj;->d:Ltq;

    invoke-virtual {v0}, Ltq;->a()V

    .line 185
    return-void
.end method

.method b(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 230
    const-string/jumbo v0, "live_video_timeline_enabled"

    .line 231
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    .line 232
    if-eqz v0, :cond_1

    iget-object v1, p0, Lrj;->b:Lbrc;

    if-eqz v1, :cond_1

    .line 233
    iget-object v0, p0, Lrj;->b:Lbrc;

    invoke-virtual {v0}, Lbrc;->c()J

    move-result-wide v0

    .line 234
    iget-object v2, p0, Lrj;->b:Lbrc;

    invoke-virtual {v2}, Lbrc;->o()Ljava/lang/String;

    move-result-object v2

    .line 235
    new-instance v3, Lcom/twitter/android/livevideo/landing/b;

    iget-object v4, p0, Lrj;->a:Lcom/twitter/model/core/Tweet;

    invoke-direct {v3, v0, v1, v2, v4}, Lcom/twitter/android/livevideo/landing/b;-><init>(JLjava/lang/String;Lcom/twitter/model/core/Tweet;)V

    .line 236
    invoke-static {p1, v3}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->a(Landroid/content/Context;Lcom/twitter/android/livevideo/landing/b;)Landroid/content/Intent;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lrj;->v:Lcom/twitter/android/card/CardActionHelper;

    const-string/jumbo v2, "card_click"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/card/CardActionHelper;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    if-nez v0, :cond_0

    .line 239
    const v0, 0x7f0a04a6

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lrj;->d:Ltq;

    invoke-virtual {v0}, Ltq;->c()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 155
    invoke-super {p0}, Lcom/twitter/android/card/n;->d()V

    .line 156
    iget-object v0, p0, Lrj;->c:Lcom/twitter/library/card/ad;

    invoke-virtual {v0}, Lcom/twitter/library/card/ad;->c()V

    .line 157
    iget-object v0, p0, Lrj;->d:Ltq;

    invoke-virtual {v0}, Ltq;->f()V

    .line 158
    return-void
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lrj;->d:Ltq;

    invoke-virtual {v0}, Ltq;->d()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public f()I
    .locals 2

    .prologue
    .line 86
    const-string/jumbo v0, "card_registry_capi_live_video_refresh_interval_seconds"

    const/4 v1, 0x0

    .line 87
    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    .line 86
    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lrj;->b:Lbrc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lrj;->d:Ltq;

    invoke-virtual {v0}, Ltq;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lrj;->h:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lrj;->d:Ltq;

    invoke-virtual {v0}, Ltq;->c()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->h()V

    .line 261
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lrj;->d:Ltq;

    invoke-virtual {v0}, Ltq;->c()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->i()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public j()Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;
    .locals 4

    .prologue
    .line 108
    new-instance v0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;-><init>()V

    .line 109
    invoke-direct {p0}, Lrj;->q()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    move-result-object v0

    .line 110
    invoke-direct {p0}, Lrj;->p()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    .line 108
    return-object v0
.end method

.method protected k()Lcom/twitter/library/card/q;
    .locals 1

    .prologue
    .line 204
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    return-object v0
.end method
