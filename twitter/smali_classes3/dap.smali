.class public Ldap;
.super Ldaj;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ldap$b;,
        Ldap$a;
    }
.end annotation


# instance fields
.field private final c:Ltv/periscope/android/view/ag;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 3

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ldaj;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    .line 16
    new-instance v0, Ldap$a;

    invoke-direct {v0, p1, p2}, Ldap$a;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    .line 17
    new-instance v1, Ldap$b;

    invoke-direct {v1, p1, p2}, Ldap$b;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    .line 18
    invoke-interface {p2}, Ltv/periscope/android/ui/broadcast/h;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 19
    new-instance v2, Ltv/periscope/android/view/ag;

    invoke-direct {v2, v0, v1}, Ltv/periscope/android/view/ag;-><init>(Ltv/periscope/android/view/a;Ltv/periscope/android/view/a;)V

    iput-object v2, p0, Ldap;->c:Ltv/periscope/android/view/ag;

    .line 23
    :goto_0
    return-void

    .line 21
    :cond_0
    new-instance v2, Ltv/periscope/android/view/ag;

    invoke-direct {v2, v1, v0}, Ltv/periscope/android/view/ag;-><init>(Ltv/periscope/android/view/a;Ltv/periscope/android/view/a;)V

    iput-object v2, p0, Ldap;->c:Ltv/periscope/android/view/ag;

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ldap;->c:Ltv/periscope/android/view/ag;

    invoke-virtual {v0}, Ltv/periscope/android/view/ag;->a()I

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ldap;->c:Ltv/periscope/android/view/ag;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/ag;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ldap;->c:Ltv/periscope/android/view/ag;

    invoke-virtual {v0}, Ltv/periscope/android/view/ag;->b()I

    move-result v0

    return v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Ldap;->c:Ltv/periscope/android/view/ag;

    invoke-virtual {v0}, Ltv/periscope/android/view/ag;->e()Z

    .line 53
    const/4 v0, 0x1

    return v0
.end method
