.class Lcpt$8$1;
.super Lcpn;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcpt$8;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcpn",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcpt$8;

.field private final b:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method constructor <init>(Lcpt$8;)V
    .locals 1

    .prologue
    .line 220
    iput-object p1, p0, Lcpt$8$1;->a:Lcpt$8;

    invoke-direct {p0}, Lcpn;-><init>()V

    .line 221
    iget-object v0, p0, Lcpt$8$1;->a:Lcpt$8;

    iget-object v0, v0, Lcpt$8;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcpt$8$1;->b:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 232
    iget v0, p0, Lcpt$8$1;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcpt$8$1;->c:I

    .line 233
    iget-object v0, p0, Lcpt$8$1;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcpt$8$1;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcpt$8$1;->c:I

    iget-object v1, p0, Lcpt$8$1;->a:Lcpt$8;

    iget v1, v1, Lcpt$8;->b:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
