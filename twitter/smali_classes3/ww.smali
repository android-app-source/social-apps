.class public Lww;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Ljava/lang/Long;",
        "Lcom/twitter/util/collection/k",
        "<",
        "Lcom/twitter/model/core/TwitterUser;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/provider/t;

.field private final b:Lrx/f;

.field private final c:Lrx/f;


# direct methods
.method public constructor <init>(Lcom/twitter/library/provider/t;)V
    .locals 2

    .prologue
    .line 27
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v0

    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lww;-><init>(Lcom/twitter/library/provider/t;Lrx/f;Lrx/f;)V

    .line 28
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/provider/t;Lrx/f;Lrx/f;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lww;->a:Lcom/twitter/library/provider/t;

    .line 34
    iput-object p2, p0, Lww;->b:Lrx/f;

    .line 35
    iput-object p3, p0, Lww;->c:Lrx/f;

    .line 36
    return-void
.end method

.method static synthetic a(Lww;)Lcom/twitter/library/provider/t;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lww;->a:Lcom/twitter/library/provider/t;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Long;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-static {p1}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lww;->b:Lrx/f;

    .line 42
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lww$1;

    invoke-direct {v1, p0}, Lww$1;-><init>(Lww;)V

    .line 43
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lww;->c:Lrx/f;

    .line 49
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 41
    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lww;->a(Ljava/lang/Long;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    return-void
.end method
