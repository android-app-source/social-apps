.class Lxr$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lxr;->a(J)Lrx/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ljava/lang/Boolean;",
        "Lrx/c",
        "<",
        "Lcom/twitter/util/collection/m",
        "<",
        "Lcom/twitter/model/moments/viewmodels/b;",
        "Lcom/twitter/model/moments/b;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lxr;


# direct methods
.method constructor <init>(Lxr;J)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lxr$1;->b:Lxr;

    iput-wide p2, p0, Lxr$1;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lxr$1;->a(Ljava/lang/Boolean;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Boolean;)Lrx/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lxr$1;->b:Lxr;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lxr;->a(Lxr;Z)Lauj;

    move-result-object v0

    iget-wide v2, p0, Lxr$1;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0
.end method
