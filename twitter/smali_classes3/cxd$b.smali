.class public Lcxd$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcxd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/util/Map;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcxd$d;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcxd$d;",
            ">;"
        }
    .end annotation
.end field

.field d:Ltv/periscope/model/af;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field final synthetic g:Lcxd;

.field private h:Lcxd$d;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcxd;)V
    .locals 1

    .prologue
    .line 408
    iput-object p1, p0, Lcxd$b;->g:Lcxd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 411
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcxd$b;->b:Ljava/util/Map;

    .line 413
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcxd$b;->c:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    .line 443
    iget-object v0, p0, Lcxd$b;->g:Lcxd;

    iget-object v0, v0, Lcxd;->d:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->playable:Z

    if-eqz v0, :cond_1

    .line 444
    iget-object v0, p0, Lcxd$b;->g:Lcxd;

    iget-object v0, v0, Lcxd;->b:Ltv/periscope/android/ui/broadcast/ac;

    if-eqz v0, :cond_0

    .line 445
    const-string/jumbo v0, "BLCM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Preparing ping, with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcxd$b;->g:Lcxd;

    iget-object v2, v2, Lcxd;->e:Ltv/periscope/android/ui/chat/p;

    invoke-interface {v2}, Ltv/periscope/android/ui/chat/p;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " hearts given and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcxd$b;->g:Lcxd;

    iget-object v2, v2, Lcxd;->e:Ltv/periscope/android/ui/chat/p;

    .line 446
    invoke-interface {v2}, Ltv/periscope/android/ui/chat/p;->i()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " comments sent."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 445
    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    :cond_0
    iget-object v0, p0, Lcxd$b;->h:Lcxd$d;

    if-eqz v0, :cond_1

    .line 449
    iget-object v0, p0, Lcxd$b;->h:Lcxd$d;

    invoke-virtual {v0}, Lcxd$d;->d()V

    .line 452
    :cond_1
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 423
    iget-object v0, p0, Lcxd$b;->h:Lcxd$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcxd$b;->h:Lcxd$d;

    invoke-virtual {v0}, Lcxd$d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 424
    invoke-virtual {p0}, Lcxd$b;->b()V

    .line 426
    :cond_0
    iput-object p1, p0, Lcxd$b;->e:Ljava/lang/String;

    .line 427
    new-instance v0, Lcxd$d;

    iget-object v1, p0, Lcxd$b;->g:Lcxd;

    invoke-direct {v0, v1, p1}, Lcxd$d;-><init>(Lcxd;Ljava/lang/String;)V

    .line 428
    iput-object v0, p0, Lcxd$b;->h:Lcxd$d;

    .line 429
    invoke-virtual {v0}, Lcxd$d;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcxd$b;->i:Ljava/lang/String;

    .line 430
    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lcxd$b;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 440
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    iget-object v0, p0, Lcxd$b;->h:Lcxd$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcxd$b;->h:Lcxd$d;

    iget-object v0, v0, Lcxd$d;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 437
    iget-object v0, p0, Lcxd$b;->h:Lcxd$d;

    iput-object p2, v0, Lcxd$d;->a:Ljava/lang/String;

    .line 438
    iget-object v0, p0, Lcxd$b;->b:Ljava/util/Map;

    iget-object v1, p0, Lcxd$b;->h:Lcxd$d;

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method a(Z)V
    .locals 3

    .prologue
    .line 455
    iget-object v0, p0, Lcxd$b;->g:Lcxd;

    invoke-static {v0}, Lcxd;->b(Lcxd;)Lcxd$b;

    move-result-object v0

    if-nez v0, :cond_0

    .line 463
    :goto_0
    return-void

    .line 458
    :cond_0
    if-eqz p1, :cond_1

    .line 459
    iget-object v0, p0, Lcxd$b;->g:Lcxd;

    iget-object v1, p0, Lcxd$b;->g:Lcxd;

    iget-object v1, v1, Lcxd;->a:Ltv/periscope/android/api/ApiManager;

    iget-object v2, p0, Lcxd$b;->g:Lcxd;

    invoke-static {v2}, Lcxd;->b(Lcxd;)Lcxd$b;

    move-result-object v2

    iget-object v2, v2, Lcxd$b;->f:Ljava/lang/String;

    invoke-interface {v1, v2}, Ltv/periscope/android/api/ApiManager;->getAccessChat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcxd;->c:Ljava/lang/String;

    goto :goto_0

    .line 461
    :cond_1
    iget-object v0, p0, Lcxd$b;->g:Lcxd;

    iget-object v1, p0, Lcxd$b;->g:Lcxd;

    iget-object v1, v1, Lcxd;->a:Ltv/periscope/android/api/ApiManager;

    iget-object v2, p0, Lcxd$b;->g:Lcxd;

    invoke-static {v2}, Lcxd;->b(Lcxd;)Lcxd$b;

    move-result-object v2

    iget-object v2, v2, Lcxd$b;->f:Ljava/lang/String;

    invoke-interface {v1, v2}, Ltv/periscope/android/api/ApiManager;->getAccessChatNoRetry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcxd;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method b()V
    .locals 3

    .prologue
    .line 466
    iget-object v0, p0, Lcxd$b;->h:Lcxd$d;

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcxd$b;->h:Lcxd$d;

    invoke-virtual {v0}, Lcxd$d;->a()Ljava/lang/String;

    move-result-object v0

    .line 468
    iget-object v1, p0, Lcxd$b;->c:Ljava/util/Map;

    iget-object v2, p0, Lcxd$b;->h:Lcxd$d;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    iget-object v0, p0, Lcxd$b;->g:Lcxd;

    iget-object v0, v0, Lcxd;->e:Ltv/periscope/android/ui/chat/p;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/p;->k()V

    .line 471
    :cond_0
    return-void
.end method

.method b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 474
    iget-object v0, p0, Lcxd$b;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxd$d;

    .line 475
    if-eqz v0, :cond_0

    .line 476
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcxd$d;->a(Z)V

    .line 477
    iget-object v1, p0, Lcxd$b;->b:Ljava/util/Map;

    iget-object v0, v0, Lcxd$d;->a:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    :cond_0
    return-void
.end method

.method c()Z
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcxd$b;->h:Lcxd$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcxd$b;->h:Lcxd$d;

    invoke-virtual {v0}, Lcxd$d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcxd$b;->h:Lcxd$d;

    iget-object v0, v0, Lcxd$d;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 487
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 488
    const-string/jumbo v0, " [ mSessions: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    iget-object v0, p0, Lcxd$b;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 490
    const-string/jumbo v3, " { "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcxd$b;->b:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxd$d;

    invoke-virtual {v0}, Lcxd$d;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "}. \n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 492
    :cond_0
    const-string/jumbo v0, " ]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
