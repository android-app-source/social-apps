.class final Lls;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Llj;


# instance fields
.field public final a:Llq;

.field public final b:Llw;

.field private c:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Llw;)V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Llq;

    invoke-direct {v0}, Llq;-><init>()V

    invoke-direct {p0, p1, v0}, Lls;-><init>(Llw;Llq;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Llw;Llq;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lls;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 28
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iput-object p2, p0, Lls;->a:Llq;

    .line 30
    iput-object p1, p0, Lls;->b:Llw;

    .line 31
    return-void
.end method

.method static synthetic a(Lls;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lls;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lls;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Llj;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0}, Lls;->e()V

    .line 80
    iget-object v0, p0, Lls;->a:Llq;

    invoke-virtual {v0, p1}, Llq;->d(I)Llq;

    .line 81
    invoke-virtual {p0}, Lls;->c()Llj;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Llj;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Lls;->e()V

    .line 56
    iget-object v0, p0, Lls;->a:Llq;

    invoke-virtual {v0, p1}, Llq;->b(Ljava/lang/String;)Llq;

    .line 57
    invoke-virtual {p0}, Lls;->c()Llj;

    move-result-object v0

    return-object v0
.end method

.method public a(Lll;)Llj;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Lls;->e()V

    .line 50
    iget-object v0, p0, Lls;->a:Llq;

    invoke-virtual {v0, p1}, Llq;->b(Lll;)Llq;

    .line 51
    invoke-virtual {p0}, Lls;->c()Llj;

    move-result-object v0

    return-object v0
.end method

.method public a([B)Llj;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Lls;->e()V

    .line 62
    iget-object v0, p0, Lls;->a:Llq;

    invoke-virtual {v0, p1}, Llq;->b([B)Llq;

    .line 63
    invoke-virtual {p0}, Lls;->c()Llj;

    move-result-object v0

    return-object v0
.end method

.method public a([BII)Llj;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0}, Lls;->e()V

    .line 68
    iget-object v0, p0, Lls;->a:Llq;

    invoke-virtual {v0, p1, p2, p3}, Llq;->c([BII)Llq;

    .line 69
    invoke-virtual {p0}, Lls;->c()Llj;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-direct {p0}, Lls;->e()V

    .line 137
    iget-object v0, p0, Lls;->a:Llq;

    iget-wide v0, v0, Llq;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 138
    iget-object v0, p0, Lls;->b:Llw;

    iget-object v1, p0, Lls;->a:Llq;

    iget-object v2, p0, Lls;->a:Llq;

    iget-wide v2, v2, Llq;->b:J

    invoke-interface {v0, v1, v2, v3}, Llw;->a(Llq;J)V

    .line 140
    :cond_0
    iget-object v0, p0, Lls;->b:Llw;

    invoke-interface {v0}, Llw;->a()V

    .line 141
    return-void
.end method

.method public a(Llq;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Lls;->e()V

    .line 44
    iget-object v0, p0, Lls;->a:Llq;

    invoke-virtual {v0, p1, p2, p3}, Llq;->a(Llq;J)V

    .line 45
    invoke-virtual {p0}, Lls;->c()Llj;

    .line 46
    return-void
.end method

.method public b(I)Llj;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0}, Lls;->e()V

    .line 86
    iget-object v0, p0, Lls;->a:Llq;

    invoke-virtual {v0, p1}, Llq;->e(I)Llq;

    .line 87
    invoke-virtual {p0}, Lls;->c()Llj;

    move-result-object v0

    return-object v0
.end method

.method public b()Llq;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lls;->a:Llq;

    return-object v0
.end method

.method public c()Llj;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Lls;->e()V

    .line 92
    iget-object v0, p0, Lls;->a:Llq;

    invoke-virtual {v0}, Llq;->n()J

    move-result-wide v0

    .line 93
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lls;->b:Llw;

    iget-object v3, p0, Lls;->a:Llq;

    invoke-interface {v2, v3, v0, v1}, Llw;->a(Llq;J)V

    .line 94
    :cond_0
    return-object p0
.end method

.method public close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lls;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    const/4 v1, 0x0

    .line 151
    :try_start_0
    iget-object v0, p0, Lls;->a:Llq;

    iget-wide v2, v0, Llq;->b:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    .line 152
    iget-object v0, p0, Lls;->b:Llw;

    iget-object v2, p0, Lls;->a:Llq;

    iget-object v3, p0, Lls;->a:Llq;

    iget-wide v4, v3, Llq;->b:J

    invoke-interface {v0, v2, v4, v5}, Llw;->a(Llq;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :cond_2
    :goto_1
    :try_start_1
    iget-object v0, p0, Lls;->b:Llw;

    invoke-interface {v0}, Llw;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 164
    :cond_3
    :goto_2
    if-eqz v0, :cond_0

    invoke-static {v0}, Lly;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 155
    goto :goto_1

    .line 160
    :catch_1
    move-exception v0

    .line 161
    if-eqz v1, :cond_3

    move-object v0, v1

    goto :goto_2
.end method

.method public d()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lls$1;

    invoke-direct {v0, p0}, Lls$1;-><init>(Lls;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "buffer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lls;->b:Llw;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
