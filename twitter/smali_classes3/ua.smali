.class public Lua;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:Ltx;

.field private final d:Lbdh;

.field private final e:J

.field private final f:Ljava/lang/String;

.field private final g:Lcom/twitter/model/core/TwitterUser;

.field private final h:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Lbdf;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lrx/f;

.field private final j:Lcwv;

.field private final k:Lrx/functions/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/b",
            "<-",
            "Lbdf;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lrx/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/d",
            "<",
            "Lbdf;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lrx/functions/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/model/livevideo/b;",
            "Lbdf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLtx;Lbdh;)V
    .locals 11

    .prologue
    .line 109
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v7

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object/from16 v6, p5

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v9}, Lua;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLtx;Lrx/f;Lbdh;Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLtx;Lbdh;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 115
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object/from16 v6, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lua;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLtx;Lrx/f;Lbdh;Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLtx;Lrx/f;Lbdh;Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lua;->h:Lrx/subjects/PublishSubject;

    .line 68
    new-instance v0, Lcwv;

    invoke-direct {v0}, Lcwv;-><init>()V

    iput-object v0, p0, Lua;->j:Lcwv;

    .line 70
    new-instance v0, Lua$1;

    invoke-direct {v0, p0}, Lua$1;-><init>(Lua;)V

    iput-object v0, p0, Lua;->k:Lrx/functions/b;

    .line 80
    new-instance v0, Lua$2;

    invoke-direct {v0, p0}, Lua$2;-><init>(Lua;)V

    iput-object v0, p0, Lua;->l:Lrx/d;

    .line 97
    new-instance v0, Lua$3;

    invoke-direct {v0, p0}, Lua$3;-><init>(Lua;)V

    iput-object v0, p0, Lua;->m:Lrx/functions/d;

    .line 122
    iput-object p1, p0, Lua;->a:Landroid/content/Context;

    .line 123
    iput-object p2, p0, Lua;->b:Lcom/twitter/library/client/Session;

    .line 124
    iput-wide p3, p0, Lua;->e:J

    .line 125
    iput-object p5, p0, Lua;->c:Ltx;

    .line 126
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lua;->g:Lcom/twitter/model/core/TwitterUser;

    .line 127
    iput-object p6, p0, Lua;->i:Lrx/f;

    .line 128
    iput-object p8, p0, Lua;->f:Ljava/lang/String;

    .line 129
    iput-object p7, p0, Lua;->d:Lbdh;

    .line 130
    return-void
.end method

.method static synthetic a(Lua;)Ltx;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lua;->c:Ltx;

    return-object v0
.end method

.method static synthetic b(Lua;)Lrx/subjects/PublishSubject;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lua;->h:Lrx/subjects/PublishSubject;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/service/s;
    .locals 2

    .prologue
    .line 187
    new-instance v0, Lajc$a;

    iget-object v1, p0, Lua;->b:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v1}, Lajc$a;-><init>(Lcom/twitter/library/client/Session;)V

    .line 188
    invoke-virtual {v0, p1}, Lajc$a;->c(I)Lajc$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 189
    invoke-virtual {v0, v1}, Lajc$a;->d(I)Lajc$a;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Lajc$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajc;

    .line 191
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lua;->a(Lajc;Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/library/service/s;

    move-result-object v0

    return-object v0
.end method

.method public a(Lajc;Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/library/service/s;
    .locals 9

    .prologue
    .line 165
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lua;->b:Lcom/twitter/library/client/Session;

    .line 166
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p1, Lajc;->i:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 167
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 168
    invoke-virtual {v0, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 169
    new-instance v0, Lbdg;

    iget-object v1, p0, Lua;->a:Landroid/content/Context;

    iget-object v2, p0, Lua;->b:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lua;->g:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, p0, Lua;->e:J

    iget v6, p1, Lajc;->l:I

    iget-object v7, p0, Lua;->d:Lbdh;

    invoke-direct/range {v0 .. v7}, Lbdg;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;JILbdh;)V

    iget-object v1, p0, Lua;->f:Ljava/lang/String;

    .line 171
    invoke-virtual {v0, v1}, Lbdg;->a(Ljava/lang/String;)Lbdg;

    move-result-object v0

    iget-object v1, p1, Lajc;->n:Lcom/twitter/model/timeline/p;

    .line 172
    invoke-virtual {v0, v1}, Lbdg;->a(Lcom/twitter/model/timeline/p;)Lbfy;

    move-result-object v0

    iget-wide v2, p1, Lajc;->g:J

    .line 173
    invoke-virtual {v0, v2, v3}, Lbfy;->a(J)Lbfy;

    move-result-object v0

    iget v1, p1, Lajc;->c:I

    .line 174
    invoke-virtual {v0, v1}, Lbfy;->c(I)Lbge;

    move-result-object v0

    const-string/jumbo v1, "scribe_log"

    .line 175
    invoke-virtual {v0, v1, v8}, Lbge;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbdg;

    .line 176
    invoke-virtual {v0}, Lbdg;->e()Lrx/c;

    move-result-object v1

    invoke-virtual {p0, v1}, Lua;->a(Lrx/c;)V

    .line 177
    return-object v0
.end method

.method public a()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lbdf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lua;->h:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method a(Lrx/c;)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<",
            "Lbdf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lua;->c:Ltx;

    iget-wide v2, p0, Lua;->e:J

    invoke-interface {v0, v2, v3}, Ltx;->a(J)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lua;->m:Lrx/functions/d;

    .line 210
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 211
    new-instance v1, Lua$4;

    invoke-direct {v1, p0}, Lua$4;-><init>(Lua;)V

    .line 212
    invoke-virtual {p1, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v1

    iget-object v2, p0, Lua;->k:Lrx/functions/b;

    .line 219
    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v1

    .line 220
    invoke-virtual {v1, v0}, Lrx/c;->h(Lrx/c;)Lrx/c;

    move-result-object v1

    .line 222
    invoke-static {v1, v0}, Lrx/c;->b(Lrx/c;Lrx/c;)Lrx/c;

    move-result-object v0

    .line 223
    invoke-virtual {v0}, Lrx/c;->k()Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lua;->l:Lrx/d;

    .line 224
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v0

    .line 225
    iget-object v1, p0, Lua;->j:Lcwv;

    invoke-virtual {v1, v0}, Lcwv;->a(Lrx/j;)V

    .line 226
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lua;->h:Lrx/subjects/PublishSubject;

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->by_()V

    .line 151
    iget-object v0, p0, Lua;->j:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 152
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 198
    iget-object v0, p0, Lua;->c:Ltx;

    iget-wide v2, p0, Lua;->e:J

    .line 199
    invoke-interface {v0, v2, v3}, Ltx;->a(J)Lrx/c;

    move-result-object v0

    .line 200
    invoke-static {}, Lcre;->d()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lua;->m:Lrx/functions/d;

    .line 201
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lua;->i:Lrx/f;

    .line 202
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lua;->l:Lrx/d;

    .line 203
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v0

    .line 204
    iget-object v1, p0, Lua;->j:Lcwv;

    invoke-virtual {v1, v0}, Lcwv;->a(Lrx/j;)V

    .line 205
    return-void
.end method
