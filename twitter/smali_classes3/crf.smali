.class public Lcrf;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcrf$c;,
        Lcrf$b;,
        Lcrf$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/view/View;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lrx/c",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    new-instance v0, Lcrf$1;

    invoke-direct {v0, p0}, Lcrf$1;-><init>(Landroid/view/View;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/view/View;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lrx/c",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Lcrf$2;

    invoke-direct {v0, p0}, Lcrf$2;-><init>(Landroid/view/View;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/view/View;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lrx/c",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Lcrf$c;

    invoke-direct {v0, p0}, Lcrf$c;-><init>(Landroid/view/View;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->n()Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/view/View;)Lrx/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lrx/c",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    invoke-static {p0}, Lcrf;->c(Landroid/view/View;)Lrx/c;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lrx/c;->d(JLjava/util/concurrent/TimeUnit;)Lrx/c;

    move-result-object v0

    return-object v0
.end method
