.class public Lsk;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsk$a;
    }
.end annotation


# instance fields
.field public final a:Z

.field public final b:I


# direct methods
.method constructor <init>(Lsk$a;)V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iget-boolean v0, p1, Lsk$a;->a:Z

    iput-boolean v0, p0, Lsk;->a:Z

    .line 9
    iget v0, p1, Lsk$a;->b:I

    iput v0, p0, Lsk;->b:I

    .line 10
    return-void
.end method

.method public static a()Lsk$a;
    .locals 2

    .prologue
    .line 13
    new-instance v0, Lsk$a;

    invoke-direct {v0}, Lsk$a;-><init>()V

    const/4 v1, 0x1

    .line 14
    invoke-virtual {v0, v1}, Lsk$a;->a(Z)Lsk$a;

    move-result-object v0

    .line 13
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 19
    if-ne p0, p1, :cond_1

    .line 25
    :cond_0
    :goto_0
    return v0

    .line 20
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 22
    :cond_3
    check-cast p1, Lsk;

    .line 24
    iget v2, p0, Lsk;->b:I

    iget v3, p1, Lsk;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 25
    :cond_4
    iget-boolean v2, p0, Lsk;->a:Z

    iget-boolean v3, p1, Lsk;->a:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 30
    iget-boolean v0, p0, Lsk;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 31
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lsk;->b:I

    add-int/2addr v0, v1

    .line 32
    return v0

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
