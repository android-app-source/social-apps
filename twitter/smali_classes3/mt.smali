.class public Lmt;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmt$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmt;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-static {p1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmt;->a:Ljava/util/Set;

    .line 22
    return-void
.end method

.method public a(Lmt$a;Lcom/twitter/android/provider/f;)V
    .locals 4

    .prologue
    .line 25
    iget-object v0, p2, Lcom/twitter/android/provider/f;->g:Ljava/lang/String;

    invoke-static {v0}, Lbld;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const v0, 0x7f0205a2

    const v1, 0x7f0a0906

    invoke-interface {p1, v0, v1}, Lmt$a;->a(II)V

    .line 34
    :goto_0
    return-void

    .line 27
    :cond_0
    invoke-static {}, Lbpj;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmt;->a:Ljava/util/Set;

    iget-wide v2, p2, Lcom/twitter/android/provider/f;->a:J

    .line 28
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29
    const v0, 0x7f020835

    const v1, 0x7f0a0361

    invoke-interface {p1, v0, v1}, Lmt$a;->a(II)V

    goto :goto_0

    .line 32
    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lmt$a;->a(Z)V

    goto :goto_0
.end method
