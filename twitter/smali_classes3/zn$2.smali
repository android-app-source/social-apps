.class Lzn$2;
.super Lcqy;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lzn;->a(Lcom/twitter/model/moments/Moment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqy",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/moments/Moment;

.field final synthetic b:Lzn;


# direct methods
.method constructor <init>(Lzn;Lcom/twitter/model/moments/Moment;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lzn$2;->b:Lzn;

    iput-object p2, p0, Lzn$2;->a:Lcom/twitter/model/moments/Moment;

    invoke-direct {p0}, Lcqy;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;)V
    .locals 4

    .prologue
    .line 116
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;-><init>()V

    iget-object v1, p0, Lzn$2;->b:Lzn;

    .line 117
    invoke-static {v1}, Lzn;->a(Lzn;)Lcom/twitter/model/moments/Moment;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->c(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    .line 119
    new-instance v1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    iget-object v2, p0, Lzn$2;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v2, Lcom/twitter/model/moments/Moment;->b:J

    .line 120
    invoke-virtual {v1, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v1

    iget-object v2, p0, Lzn$2;->a:Lcom/twitter/model/moments/Moment;

    iget-boolean v2, v2, Lcom/twitter/model/moments/Moment;->k:Z

    .line 121
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Ljava/lang/Boolean;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v1

    .line 122
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    .line 123
    if-eqz p1, :cond_0

    .line 124
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->c(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 126
    :cond_0
    iget-object v1, p0, Lzn$2;->b:Lzn;

    const-string/jumbo v2, "moments:capsule:pivot:moment:impression"

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-static {v1, v2, v0}, Lzn;->a(Lzn;Ljava/lang/String;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V

    .line 127
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 113
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lzn$2;->a(Ljava/lang/Long;)V

    return-void
.end method
