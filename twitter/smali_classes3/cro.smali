.class public Lcro;
.super Lcrr;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcrr;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/connectivity/b;

.field private final b:Landroid/net/wifi/WifiManager;

.field private final c:Landroid/net/ConnectivityManager;

.field private final d:Landroid/telephony/TelephonyManager;

.field private e:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 48
    .line 49
    invoke-static {}, Lcom/twitter/util/connectivity/b;->a()Lcom/twitter/util/connectivity/b;

    move-result-object v0

    .line 50
    invoke-static {}, Lcom/twitter/util/connectivity/a;->a()Lcom/twitter/util/connectivity/a;

    move-result-object v1

    .line 48
    invoke-direct {p0, p1, v0, v1}, Lcro;-><init>(Landroid/content/Context;Lcom/twitter/util/connectivity/b;Lcom/twitter/util/connectivity/a;)V

    .line 51
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/util/connectivity/b;Lcom/twitter/util/connectivity/a;)V
    .locals 6

    .prologue
    .line 63
    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    const-string/jumbo v0, "phone"

    .line 64
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    const-string/jumbo v0, "connectivity"

    .line 65
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    .line 63
    invoke-direct/range {v0 .. v5}, Lcro;-><init>(Landroid/net/wifi/WifiManager;Landroid/telephony/TelephonyManager;Landroid/net/ConnectivityManager;Lcom/twitter/util/connectivity/b;Lcom/twitter/util/connectivity/a;)V

    .line 68
    return-void
.end method

.method constructor <init>(Landroid/net/wifi/WifiManager;Landroid/telephony/TelephonyManager;Landroid/net/ConnectivityManager;Lcom/twitter/util/connectivity/b;Lcom/twitter/util/connectivity/a;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Lcrr;-><init>()V

    .line 39
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcro;->e:J

    .line 85
    iput-object p1, p0, Lcro;->b:Landroid/net/wifi/WifiManager;

    .line 86
    iput-object p2, p0, Lcro;->d:Landroid/telephony/TelephonyManager;

    .line 87
    iput-object p3, p0, Lcro;->c:Landroid/net/ConnectivityManager;

    .line 88
    iput-object p4, p0, Lcro;->a:Lcom/twitter/util/connectivity/b;

    .line 90
    if-eqz p5, :cond_0

    .line 91
    invoke-virtual {p5, p0}, Lcom/twitter/util/connectivity/a;->a(Lcom/twitter/util/q;)Z

    .line 93
    :cond_0
    return-void
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcro;->a:Lcom/twitter/util/connectivity/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcro;->a:Lcom/twitter/util/connectivity/b;

    invoke-virtual {v0}, Lcom/twitter/util/connectivity/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Landroid/net/NetworkInfo;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcro;->c:Landroid/net/ConnectivityManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcro;->c:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lcro;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    const-string/jumbo v0, "wifi"

    .line 124
    :goto_0
    return-object v0

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcro;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 124
    :pswitch_0
    const-string/jumbo v0, "cellular"

    goto :goto_0

    .line 118
    :pswitch_1
    const-string/jumbo v0, "2g"

    goto :goto_0

    .line 121
    :pswitch_2
    const-string/jumbo v0, "unknown"

    goto :goto_0

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(J)V
    .locals 1

    .prologue
    .line 254
    invoke-direct {p0}, Lcro;->j()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 255
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 256
    :cond_0
    iput-wide p1, p0, Lcro;->e:J

    .line 258
    :cond_1
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcro;->d:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcro;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcro;->d:Landroid/telephony/TelephonyManager;

    .line 139
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    goto :goto_0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 150
    invoke-direct {p0}, Lcro;->j()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 151
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 159
    invoke-direct {p0}, Lcro;->j()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 160
    invoke-direct {p0}, Lcro;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Lcom/twitter/util/network/c;
    .locals 4

    .prologue
    .line 177
    invoke-direct {p0}, Lcro;->j()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 179
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcro;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    invoke-static {v0, v2}, Lcrp;->a(II)Lcom/twitter/util/network/DownloadQuality;

    move-result-object v0

    .line 183
    :goto_0
    invoke-virtual {p0}, Lcro;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v1, "wifi"

    .line 187
    :goto_1
    invoke-virtual {p0}, Lcro;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string/jumbo v2, "wifi"

    .line 191
    :goto_2
    new-instance v3, Lcom/twitter/util/network/c;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/util/network/c;-><init>(Lcom/twitter/util/network/DownloadQuality;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3

    .line 180
    :cond_0
    sget-object v0, Lcom/twitter/util/network/DownloadQuality;->a:Lcom/twitter/util/network/DownloadQuality;

    goto :goto_0

    .line 183
    :cond_1
    if-eqz v1, :cond_2

    .line 185
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    const-string/jumbo v1, ""

    goto :goto_1

    .line 187
    :cond_3
    iget-object v2, p0, Lcro;->d:Landroid/telephony/TelephonyManager;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcro;->d:Landroid/telephony/TelephonyManager;

    .line 189
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_4
    const-string/jumbo v2, ""

    goto :goto_2
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcro;->d:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcro;->d:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 207
    invoke-direct {p0}, Lcro;->j()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 208
    invoke-direct {p0}, Lcro;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 210
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 208
    :goto_0
    return v0

    .line 210
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEvent(Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;)V
    .locals 2

    .prologue
    .line 245
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcro;->a(J)V

    .line 246
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;

    invoke-virtual {p0, p1}, Lcro;->onEvent(Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;)V

    return-void
.end method
