.class public Lzw;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lzu;

.field private final c:Lcom/twitter/analytics/feature/model/MomentScribeDetails;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Lzu;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lzw;->a:Landroid/content/res/Resources;

    .line 53
    iput-object p2, p0, Lzw;->b:Lzu;

    .line 54
    iput-object p3, p0, Lzw;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 55
    return-void
.end method

.method public static a(Landroid/content/Context;J)Lzw;
    .locals 5

    .prologue
    .line 46
    new-instance v1, Lzw;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {}, Lzu;->a()Lzu;

    move-result-object v3

    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 47
    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-direct {v1, v2, v3, v0}, Lzw;-><init>(Landroid/content/res/Resources;Lzu;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V

    .line 46
    return-object v1
.end method

.method private d()V
    .locals 6

    .prologue
    .line 87
    iget-object v0, p0, Lzw;->b:Lzu;

    iget-object v1, p0, Lzw;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:cover:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "title"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "add"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 88
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    .line 91
    iget-object v0, p0, Lzw;->b:Lzu;

    iget-object v1, p0, Lzw;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:cover:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "description"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "add"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    .line 95
    iget-object v0, p0, Lzw;->b:Lzu;

    iget-object v1, p0, Lzw;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:cover:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "title"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "edit"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method private g()V
    .locals 6

    .prologue
    .line 99
    iget-object v0, p0, Lzw;->b:Lzu;

    iget-object v1, p0, Lzw;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:cover:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "description"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "edit"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method private h()V
    .locals 6

    .prologue
    .line 103
    iget-object v0, p0, Lzw;->b:Lzu;

    iget-object v1, p0, Lzw;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:cover:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "title"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "hit_max_chars"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method private i()V
    .locals 6

    .prologue
    .line 107
    iget-object v0, p0, Lzw;->b:Lzu;

    iget-object v1, p0, Lzw;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:cover:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "description"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "hit_max_chars"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method private j()I
    .locals 4

    .prologue
    .line 123
    iget-object v0, p0, Lzw;->a:Landroid/content/res/Resources;

    const v1, 0x7f0f003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 124
    int-to-double v0, v0

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method private k()I
    .locals 4

    .prologue
    .line 128
    iget-object v0, p0, Lzw;->a:Landroid/content/res/Resources;

    const v1, 0x7f0f0033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 129
    int-to-double v0, v0

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 111
    iget-object v0, p0, Lzw;->b:Lzu;

    iget-object v1, p0, Lzw;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:cover:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "set_button"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "click"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 58
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 62
    :cond_2
    invoke-direct {p0}, Lzw;->d()V

    .line 66
    :goto_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0}, Lzw;->j()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 67
    invoke-direct {p0}, Lzw;->h()V

    goto :goto_0

    .line 64
    :cond_3
    invoke-direct {p0}, Lzw;->f()V

    goto :goto_1
.end method

.method public b()V
    .locals 6

    .prologue
    .line 115
    iget-object v0, p0, Lzw;->b:Lzu;

    iget-object v1, p0, Lzw;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:cover:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "set"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "upload"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 73
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 77
    :cond_2
    invoke-direct {p0}, Lzw;->e()V

    .line 81
    :goto_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0}, Lzw;->k()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 82
    invoke-direct {p0}, Lzw;->i()V

    goto :goto_0

    .line 79
    :cond_3
    invoke-direct {p0}, Lzw;->g()V

    goto :goto_1
.end method

.method public c()V
    .locals 6

    .prologue
    .line 119
    iget-object v0, p0, Lzw;->b:Lzu;

    iget-object v1, p0, Lzw;->c:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:cover:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "set"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "existing"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 120
    return-void
.end method
