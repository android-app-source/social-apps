.class public Lxb;
.super Lcom/evernote/android/job/Job;
.source "Twttr"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lxb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lxb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/evernote/android/job/Job;-><init>()V

    return-void
.end method

.method public static a(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lxb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lxb;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a(Lcom/evernote/android/job/Job$a;)Lcom/evernote/android/job/Job$Result;
    .locals 6

    .prologue
    .line 24
    invoke-static {}, Lzb;->a()Lzb$a;

    move-result-object v0

    .line 25
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzb$a;->a(Lamu;)Lzb$a;

    move-result-object v0

    new-instance v1, Lzh;

    invoke-direct {v1}, Lzh;-><init>()V

    .line 26
    invoke-virtual {v0, v1}, Lzb$a;->a(Lzh;)Lzb$a;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lzb$a;->a()Lzf;

    move-result-object v0

    .line 28
    invoke-virtual {p1}, Lcom/evernote/android/job/Job$a;->d()Ly;

    move-result-object v1

    const-string/jumbo v2, "moment_id"

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Ly;->b(Ljava/lang/String;J)J

    move-result-wide v2

    .line 30
    invoke-interface {v0}, Lzf;->b()Lxc;

    move-result-object v0

    .line 31
    invoke-virtual {v0, v2, v3}, Lxc;->a(J)I

    move-result v0

    .line 32
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/evernote/android/job/Job$Result;->a:Lcom/evernote/android/job/Job$Result;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/evernote/android/job/Job$Result;->c:Lcom/evernote/android/job/Job$Result;

    goto :goto_0
.end method
