.class public final Lvw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lvx;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lvw$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/android/livevideo/d;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/d;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lvw;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lvw;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lvw$a;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    sget-boolean v0, Lvw;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_0
    invoke-direct {p0, p1}, Lvw;->a(Lvw$a;)V

    .line 45
    return-void
.end method

.method synthetic constructor <init>(Lvw$a;Lvw$1;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lvw;-><init>(Lvw$a;)V

    return-void
.end method

.method private a(Lvw$a;)V
    .locals 2

    .prologue
    .line 54
    .line 57
    invoke-static {p1}, Lvw$a;->a(Lvw$a;)Lvy;

    move-result-object v0

    .line 56
    invoke-static {v0}, Lwb;->a(Lvy;)Ldagger/internal/c;

    move-result-object v0

    .line 55
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lvw;->b:Lcta;

    .line 59
    new-instance v0, Lvw$1;

    invoke-direct {v0, p0, p1}, Lvw$1;-><init>(Lvw;Lvw$a;)V

    iput-object v0, p0, Lvw;->c:Lcta;

    .line 75
    invoke-static {p1}, Lvw$a;->a(Lvw$a;)Lvy;

    move-result-object v0

    .line 74
    invoke-static {v0}, Lvz;->a(Lvy;)Ldagger/internal/c;

    move-result-object v0

    .line 73
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lvw;->d:Lcta;

    .line 77
    iget-object v0, p0, Lvw;->c:Lcta;

    iget-object v1, p0, Lvw;->d:Lcta;

    .line 79
    invoke-static {v0, v1}, Lwa;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 78
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lvw;->e:Lcta;

    .line 82
    iget-object v0, p0, Lvw;->e:Lcta;

    .line 83
    invoke-static {v0}, Lcom/twitter/android/livevideo/f;->a(Lcta;)Lcsd;

    move-result-object v0

    iput-object v0, p0, Lvw;->f:Lcsd;

    .line 85
    iget-object v0, p0, Lvw;->f:Lcsd;

    .line 87
    invoke-static {v0}, Lcom/twitter/android/livevideo/e;->a(Lcsd;)Ldagger/internal/c;

    move-result-object v0

    .line 86
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lvw;->g:Lcta;

    .line 89
    iget-object v0, p0, Lvw;->g:Lcta;

    .line 90
    invoke-static {v0}, Lcom/twitter/android/livevideo/player/h;->a(Lcta;)Lcsd;

    move-result-object v0

    iput-object v0, p0, Lvw;->h:Lcsd;

    .line 91
    return-void
.end method

.method public static c()Lvw$a;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lvw$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lvw$a;-><init>(Lvw$1;)V

    return-object v0
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lvw;->b:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public a(Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lvw;->h:Lcsd;

    invoke-interface {v0, p1}, Lcsd;->a(Ljava/lang/Object;)V

    .line 106
    return-void
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
