.class public Lcrs;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcrs;


# instance fields
.field private final b:Lcrr;


# direct methods
.method constructor <init>(Lcrr;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcrs;->b:Lcrr;

    .line 19
    return-void
.end method

.method public static declared-synchronized a()Lcrs;
    .locals 3

    .prologue
    .line 23
    const-class v1, Lcrs;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcrs;->a:Lcrs;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcrs;

    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v2

    invoke-direct {v0, v2}, Lcrs;-><init>(Lcrr;)V

    sput-object v0, Lcrs;->a:Lcrs;

    .line 25
    const-class v0, Lcrs;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 27
    :cond_0
    sget-object v0, Lcrs;->a:Lcrs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(I)I
    .locals 4

    .prologue
    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    .line 48
    iget-object v2, p0, Lcrs;->b:Lcrr;

    invoke-virtual {v2}, Lcrr;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    :goto_0
    return p1

    .line 53
    :cond_0
    iget-object v2, p0, Lcrs;->b:Lcrr;

    invoke-virtual {v2}, Lcrr;->b()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 95
    :goto_1
    :pswitch_0
    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_0

    .line 60
    :pswitch_1
    const-wide v0, 0x3fd6666666666666L    # 0.35

    .line 61
    goto :goto_1

    .line 65
    :pswitch_2
    const-wide v0, 0x3fe199999999999aL    # 0.55

    .line 66
    goto :goto_1

    .line 70
    :pswitch_3
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 71
    goto :goto_1

    .line 75
    :pswitch_4
    const-wide v0, 0x3ff3333333333333L    # 1.2

    .line 76
    goto :goto_1

    .line 83
    :pswitch_5
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    .line 84
    goto :goto_1

    .line 87
    :pswitch_6
    const-wide/high16 v0, 0x4004000000000000L    # 2.5

    .line 88
    goto :goto_1

    .line 53
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method
