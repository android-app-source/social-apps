.class public Lzp$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lzq;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lzp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lzp;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lzp;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lzp$a;->a:Lzp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    iput-object p2, p0, Lzp$a;->b:Ljava/lang/String;

    .line 252
    return-void
.end method

.method static synthetic a(Lzp$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lzp$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method private a(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 277
    iget-object v0, p0, Lzp$a;->a:Lzp;

    invoke-static {v0}, Lzp;->a(Lzp;)Lrx/c;

    move-result-object v0

    new-instance v1, Lzp$a$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lzp$a$1;-><init>(Lzp$a;JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 285
    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 1

    .prologue
    .line 256
    const-string/jumbo v0, "moments:modern_guide:%s:moment:follow"

    invoke-direct {p0, p1, p2, v0}, Lzp$a;->a(JLjava/lang/String;)V

    .line 257
    return-void
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 261
    const-string/jumbo v0, "moments:modern_guide:%s:moment:unfollow"

    invoke-direct {p0, p1, p2, v0}, Lzp$a;->a(JLjava/lang/String;)V

    .line 262
    return-void
.end method

.method public c(J)V
    .locals 1

    .prologue
    .line 265
    const-string/jumbo v0, "moments:modern_guide:%s:moment:click"

    invoke-direct {p0, p1, p2, v0}, Lzp$a;->a(JLjava/lang/String;)V

    .line 266
    return-void
.end method

.method public d(J)V
    .locals 1

    .prologue
    .line 269
    const-string/jumbo v0, "moments:modern_guide:%s:moment:impression"

    invoke-direct {p0, p1, p2, v0}, Lzp$a;->a(JLjava/lang/String;)V

    .line 270
    return-void
.end method

.method public e(J)V
    .locals 1

    .prologue
    .line 273
    const-string/jumbo v0, "moments:modern_guide:%s:cta:click"

    invoke-direct {p0, p1, p2, v0}, Lzp$a;->a(JLjava/lang/String;)V

    .line 274
    return-void
.end method
