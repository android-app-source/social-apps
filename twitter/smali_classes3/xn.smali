.class public Lxn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lala;
.implements Lxm;


# instance fields
.field private final a:Lxm;

.field private final b:Lcom/twitter/database/lru/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lxk;


# direct methods
.method public constructor <init>(Lxr;Lcom/twitter/database/lru/l;Lxk;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lxr;",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;",
            "Lxk;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lxn;->a:Lxm;

    .line 38
    iput-object p2, p0, Lxn;->b:Lcom/twitter/database/lru/l;

    .line 39
    iput-object p3, p0, Lxn;->c:Lxk;

    .line 40
    return-void
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lxn;->a:Lxm;

    .line 47
    invoke-interface {v0, p1, p2}, Lxm;->a(J)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lxn;->b:Lcom/twitter/database/lru/l;

    .line 48
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/twitter/database/lru/l;->b(Ljava/lang/Object;)Lrx/g;

    move-result-object v1

    invoke-virtual {v1}, Lrx/g;->c()Lrx/c;

    move-result-object v1

    iget-object v2, p0, Lxn;->c:Lxk;

    .line 49
    invoke-virtual {v2}, Lxk;->a()Lrx/functions/e;

    move-result-object v2

    .line 46
    invoke-static {v0, v1, v2}, Lrx/c;->b(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    .line 45
    invoke-static {v0}, Lrx/c;->b(Lrx/c;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    .line 60
    invoke-static {p0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 62
    :cond_0
    return-void
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lxn;->a:Lxm;

    invoke-interface {v0}, Lxm;->close()V

    .line 55
    return-void
.end method
