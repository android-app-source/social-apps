.class Lta$6;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lta;->a(Lcom/twitter/model/stratostore/SourceLocation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ltg;",
        "Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterestSelection;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/stratostore/SourceLocation;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lta;


# direct methods
.method constructor <init>(Lta;Lcom/twitter/model/stratostore/SourceLocation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lta$6;->e:Lta;

    iput-object p2, p0, Lta$6;->a:Lcom/twitter/model/stratostore/SourceLocation;

    iput-object p3, p0, Lta$6;->b:Ljava/lang/String;

    iput-object p4, p0, Lta$6;->c:Ljava/lang/String;

    iput-object p5, p0, Lta$6;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ltg;)Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterestSelection;
    .locals 5

    .prologue
    .line 227
    iget-object v0, p0, Lta$6;->a:Lcom/twitter/model/stratostore/SourceLocation;

    iget-object v1, p0, Lta$6;->b:Ljava/lang/String;

    iget-object v2, p0, Lta$6;->c:Ljava/lang/String;

    iget-object v3, p0, Lta$6;->d:Ljava/lang/String;

    iget-object v4, p1, Ltg;->e:Ljava/lang/String;

    .line 228
    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterestSelection;->a(Lcom/twitter/model/stratostore/SourceLocation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterestSelection;

    move-result-object v0

    .line 231
    iget v1, p1, Ltg;->g:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 232
    new-instance v1, Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterest;

    iget-wide v2, p1, Ltg;->b:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterest;-><init>(J)V

    iput-object v1, v0, Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterestSelection;->a:Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterest;

    .line 237
    :goto_0
    return-object v0

    .line 234
    :cond_0
    new-instance v1, Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterest;

    iget-object v2, p1, Ltg;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterest;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterestSelection;->a:Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterest;

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 224
    check-cast p1, Ltg;

    invoke-virtual {p0, p1}, Lta$6;->a(Ltg;)Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterestSelection;

    move-result-object v0

    return-object v0
.end method
