.class Lso$a;
.super Lcom/twitter/ui/anim/o;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lso;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/media/stickers/StickerMediaView;

.field private b:F

.field private c:F

.field private d:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/media/stickers/StickerMediaView;Landroid/view/View;Lcom/twitter/ui/anim/c$a;)V
    .locals 0

    .prologue
    .line 244
    invoke-direct {p0, p2, p3}, Lcom/twitter/ui/anim/o;-><init>(Landroid/view/View;Lcom/twitter/ui/anim/c$a;)V

    .line 245
    iput-object p1, p0, Lso$a;->a:Lcom/twitter/android/media/stickers/StickerMediaView;

    .line 246
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 250
    iget-object v0, p0, Lso$a;->a:Lcom/twitter/android/media/stickers/StickerMediaView;

    if-nez v0, :cond_0

    .line 251
    invoke-super {p0, p1, p2}, Lcom/twitter/ui/anim/o;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 284
    :goto_0
    return v0

    .line 254
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 255
    if-nez v0, :cond_1

    .line 256
    iput-boolean v1, p0, Lso$a;->d:Z

    .line 257
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lso$a;->b:F

    .line 258
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lso$a;->c:F

    .line 259
    invoke-super {p0, p1, p2}, Lcom/twitter/ui/anim/o;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 262
    :cond_1
    iget-boolean v2, p0, Lso$a;->d:Z

    if-eqz v2, :cond_2

    .line 263
    packed-switch v0, :pswitch_data_0

    .line 284
    :cond_2
    :goto_1
    invoke-super {p0, p1, p2}, Lcom/twitter/ui/anim/o;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 265
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget v1, p0, Lso$a;->b:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lso$a;->e:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_3

    .line 266
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget v1, p0, Lso$a;->c:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lso$a;->e:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 267
    :cond_3
    iput-boolean v4, p0, Lso$a;->d:Z

    goto :goto_1

    .line 272
    :pswitch_1
    iget-object v0, p0, Lso$a;->a:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/stickers/StickerMediaView;

    iget v2, p0, Lso$a;->b:F

    float-to-int v2, v2

    iget v3, p0, Lso$a;->c:F

    float-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(II)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 273
    goto :goto_0

    .line 275
    :cond_4
    iget-object v0, p0, Lso$a;->a:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-virtual {v0, v4}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Z)V

    goto :goto_1

    .line 263
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
