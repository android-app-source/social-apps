.class public Ldak;
.super Ldaj;
.source "Twttr"


# instance fields
.field private final c:I


# direct methods
.method public constructor <init>(Ltv/periscope/model/p;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ldaj;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    .line 16
    invoke-static {p1}, Ltv/periscope/model/p;->a(Ltv/periscope/model/p;)I

    move-result v0

    iput v0, p0, Ldak;->c:I

    .line 17
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 27
    sget v0, Ltv/periscope/android/library/f$f;->ps__ic_clock:I

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget v0, Ltv/periscope/android/library/f$l;->ps__action_change_expiration:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 32
    sget v0, Ltv/periscope/android/library/f$d;->ps__red:I

    return v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 22
    sget v0, Ltv/periscope/android/library/f$l;->ps__action_change_expiration_subtext:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Ldak;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method public e()Z
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Ldak;->b:Ltv/periscope/android/ui/broadcast/h;

    iget-object v1, p0, Ldak;->a:Ljava/lang/String;

    iget v2, p0, Ldak;->c:I

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/h;->a(Ljava/lang/String;I)V

    .line 49
    const/4 v0, 0x0

    return v0
.end method
