.class public interface abstract Lcty;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lorg/spongycastle/asn1/l;

.field public static final b:Lorg/spongycastle/asn1/l;

.field public static final c:Lorg/spongycastle/asn1/l;

.field public static final d:Lorg/spongycastle/asn1/l;

.field public static final e:Lorg/spongycastle/asn1/l;

.field public static final f:Lorg/spongycastle/asn1/l;

.field public static final g:Lorg/spongycastle/asn1/l;

.field public static final h:Lorg/spongycastle/asn1/l;

.field public static final i:Lorg/spongycastle/asn1/l;

.field public static final j:Lorg/spongycastle/asn1/l;

.field public static final k:Lorg/spongycastle/asn1/l;

.field public static final l:Lorg/spongycastle/asn1/l;

.field public static final m:Lorg/spongycastle/asn1/l;

.field public static final n:Lorg/spongycastle/asn1/l;

.field public static final o:Lorg/spongycastle/asn1/l;

.field public static final p:Lorg/spongycastle/asn1/l;

.field public static final q:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.6.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcty;->a:Lorg/spongycastle/asn1/l;

    .line 15
    sget-object v0, Lcty;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->b:Lorg/spongycastle/asn1/l;

    .line 17
    sget-object v0, Lcty;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->c:Lorg/spongycastle/asn1/l;

    .line 19
    sget-object v0, Lcty;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->d:Lorg/spongycastle/asn1/l;

    .line 21
    sget-object v0, Lcty;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->e:Lorg/spongycastle/asn1/l;

    .line 23
    sget-object v0, Lcty;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "5"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->f:Lorg/spongycastle/asn1/l;

    .line 25
    sget-object v0, Lcty;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "6"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->g:Lorg/spongycastle/asn1/l;

    .line 27
    sget-object v0, Lcty;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "7"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->h:Lorg/spongycastle/asn1/l;

    .line 36
    sget-object v0, Lcty;->f:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "5"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->i:Lorg/spongycastle/asn1/l;

    .line 38
    sget-object v0, Lcty;->f:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "6"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->j:Lorg/spongycastle/asn1/l;

    .line 41
    sget-object v0, Lcty;->i:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "6"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->k:Lorg/spongycastle/asn1/l;

    .line 45
    sget-object v0, Lcty;->i:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "8"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->l:Lorg/spongycastle/asn1/l;

    .line 47
    sget-object v0, Lcty;->l:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->m:Lorg/spongycastle/asn1/l;

    .line 50
    sget-object v0, Lcty;->m:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->n:Lorg/spongycastle/asn1/l;

    .line 52
    sget-object v0, Lcty;->m:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->o:Lorg/spongycastle/asn1/l;

    .line 55
    sget-object v0, Lcty;->m:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->p:Lorg/spongycastle/asn1/l;

    .line 58
    sget-object v0, Lcty;->m:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcty;->q:Lorg/spongycastle/asn1/l;

    return-void
.end method
