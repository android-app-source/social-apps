.class public Lxv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;
.implements Lxm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Ljava/lang/Long;",
        "Lcom/twitter/util/collection/m",
        "<",
        "Lcom/twitter/model/moments/viewmodels/b;",
        "Lcom/twitter/model/moments/b;",
        ">;>;",
        "Lxm;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Lcom/twitter/android/moments/data/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/data/c",
            "<",
            "Lapb;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcnz;Lcom/twitter/android/moments/data/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcnz;",
            "Lcom/twitter/android/moments/data/c",
            "<",
            "Lapb;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-virtual {p1}, Lcnz;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lxv;->a:J

    .line 38
    iput-object p2, p0, Lxv;->b:Lcom/twitter/android/moments/data/c;

    .line 39
    return-void
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lapb$a;

    invoke-direct {v0}, Lapb$a;-><init>()V

    .line 51
    invoke-static {p1, p2}, Lcom/twitter/library/provider/m$c;->a(J)Landroid/net/Uri;

    move-result-object v1

    iget-wide v2, p0, Lxv;->a:J

    invoke-static {v1, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lapb$a;->a(Landroid/net/Uri;)Lapb$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/m$c;->b:[Ljava/lang/String;

    .line 53
    invoke-virtual {v0, v1}, Lapb$a;->b([Ljava/lang/String;)Lapb$a;

    move-result-object v0

    const-string/jumbo v1, "_id ASC"

    .line 54
    invoke-virtual {v0, v1}, Lapb$a;->b(Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    .line 55
    invoke-virtual {v0}, Lapb$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapb;

    .line 56
    iget-object v1, p0, Lxv;->b:Lcom/twitter/android/moments/data/c;

    invoke-virtual {v1}, Lcom/twitter/android/moments/data/c;->a()Lauj;

    move-result-object v1

    invoke-interface {v1, v0}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lxv$1;

    invoke-direct {v1, p0}, Lxv$1;-><init>(Lxv;)V

    .line 57
    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 56
    return-object v0
.end method

.method public a(Ljava/lang/Long;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lxv;->a(J)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lxv;->a(Ljava/lang/Long;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lxv;->b:Lcom/twitter/android/moments/data/c;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/c;->close()V

    .line 76
    return-void
.end method
