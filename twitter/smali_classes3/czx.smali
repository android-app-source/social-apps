.class public Lczx;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lczx;->a:[F

    .line 25
    return-void
.end method

.method public constructor <init>([F)V
    .locals 3

    .prologue
    const/16 v2, 0x10

    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-array v0, v2, [F

    iput-object v0, p0, Lczx;->a:[F

    .line 29
    iget-object v0, p0, Lczx;->a:[F

    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 30
    return-void
.end method


# virtual methods
.method public a(Lczw;)Lczw;
    .locals 7

    .prologue
    .line 33
    iget v0, p1, Lczw;->b:F

    iget-object v1, p0, Lczx;->a:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    mul-float/2addr v0, v1

    iget v1, p1, Lczw;->c:F

    iget-object v2, p0, Lczx;->a:[F

    const/4 v3, 0x4

    aget v2, v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p1, Lczw;->d:F

    iget-object v2, p0, Lczx;->a:[F

    const/16 v3, 0x8

    aget v2, v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lczx;->a:[F

    const/16 v2, 0xc

    aget v1, v1, v2

    iget v2, p1, Lczw;->e:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 34
    iget v1, p1, Lczw;->b:F

    iget-object v2, p0, Lczx;->a:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    mul-float/2addr v1, v2

    iget v2, p1, Lczw;->c:F

    iget-object v3, p0, Lczx;->a:[F

    const/4 v4, 0x5

    aget v3, v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p1, Lczw;->d:F

    iget-object v3, p0, Lczx;->a:[F

    const/16 v4, 0x9

    aget v3, v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lczx;->a:[F

    const/16 v3, 0xd

    aget v2, v2, v3

    iget v3, p1, Lczw;->e:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 35
    iget v2, p1, Lczw;->b:F

    iget-object v3, p0, Lczx;->a:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    mul-float/2addr v2, v3

    iget v3, p1, Lczw;->c:F

    iget-object v4, p0, Lczx;->a:[F

    const/4 v5, 0x6

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p1, Lczw;->d:F

    iget-object v4, p0, Lczx;->a:[F

    const/16 v5, 0xa

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lczx;->a:[F

    const/16 v4, 0xe

    aget v3, v3, v4

    iget v4, p1, Lczw;->e:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 36
    iget v3, p1, Lczw;->b:F

    iget-object v4, p0, Lczx;->a:[F

    const/4 v5, 0x3

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget v4, p1, Lczw;->c:F

    iget-object v5, p0, Lczx;->a:[F

    const/4 v6, 0x7

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Lczw;->d:F

    iget-object v5, p0, Lczx;->a:[F

    const/16 v6, 0xb

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lczx;->a:[F

    const/16 v5, 0xf

    aget v4, v4, v5

    iget v5, p1, Lczw;->e:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    .line 37
    new-instance v4, Lczw;

    invoke-direct {v4, v0, v1, v2, v3}, Lczw;-><init>(FFFF)V

    return-object v4
.end method
