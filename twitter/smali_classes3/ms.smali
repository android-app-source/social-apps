.class Lms;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lmt$a;


# instance fields
.field private final a:Lcom/twitter/media/ui/image/UserImageView;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/ImageView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/widget/ProgressBar;

.field private final h:Lcom/twitter/ui/socialproof/SocialBylineView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongViewCast"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const v0, 0x7f130097

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lms;->a:Lcom/twitter/media/ui/image/UserImageView;

    .line 40
    const v0, 0x7f130059

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lms;->b:Landroid/widget/TextView;

    .line 41
    const v0, 0x7f13009b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lms;->c:Landroid/widget/ImageView;

    .line 42
    const v0, 0x7f130074

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lms;->d:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f1304ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lms;->e:Landroid/widget/TextView;

    .line 44
    const v0, 0x7f13030a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lms;->f:Landroid/widget/ImageView;

    .line 45
    const v0, 0x7f130334

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lms;->g:Landroid/widget/ProgressBar;

    .line 46
    const v0, 0x7f13007a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/socialproof/SocialBylineView;

    iput-object v0, p0, Lms;->h:Lcom/twitter/ui/socialproof/SocialBylineView;

    .line 47
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/media/ui/image/UserImageView;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lms;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    return-object v0
.end method

.method public a(II)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 94
    iget-object v0, p0, Lms;->h:Lcom/twitter/ui/socialproof/SocialBylineView;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lms;->h:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-virtual {v0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lms;->h:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-virtual {v1, p1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setIcon(I)V

    .line 97
    iget-object v1, p0, Lms;->h:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/ui/socialproof/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lms;->h:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setRenderRTL(Z)V

    .line 99
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lms;->a(Z)V

    .line 101
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 85
    const v0, 0x7f0205a2

    const v1, 0x7f0a0906

    invoke-virtual {p0, v0, v1}, Lms;->a(II)V

    .line 86
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lms;->h:Lcom/twitter/ui/socialproof/SocialBylineView;

    if-eqz v0, :cond_0

    .line 106
    iget-object v1, p0, Lms;->h:Lcom/twitter/ui/socialproof/SocialBylineView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    .line 108
    :cond_0
    return-void

    .line 106
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public b()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lms;->b:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public c()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lms;->c:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public d()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lms;->d:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public e()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lms;->e:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public f()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lms;->f:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lms;->a(Z)V

    .line 90
    return-void
.end method
