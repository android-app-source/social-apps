.class public Lcxd$c;
.super Landroid/os/Handler;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcxd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field a:Lcxd$b;


# direct methods
.method constructor <init>(Landroid/os/Looper;Lcxd$b;)V
    .locals 0

    .prologue
    .line 502
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 503
    iput-object p2, p0, Lcxd$c;->a:Lcxd$b;

    .line 504
    return-void
.end method


# virtual methods
.method a(Lcxd$b;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcxd$c;->a:Lcxd$b;

    .line 508
    return-void
.end method

.method a(IJ)Z
    .locals 2

    .prologue
    .line 514
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcxd$c;->sendMessageDelayed(Landroid/os/Message;J)Z

    move-result v0

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 519
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 539
    :goto_0
    return-void

    .line 521
    :pswitch_0
    iget-object v0, p0, Lcxd$c;->a:Lcxd$b;

    invoke-virtual {v0}, Lcxd$b;->a()V

    .line 522
    const/16 v0, 0x7b

    sget-wide v2, Ltv/periscope/android/api/Constants;->API_PING_INTERVAL_MILLIS:J

    invoke-virtual {p0, v0, v2, v3}, Lcxd$c;->a(IJ)Z

    goto :goto_0

    .line 526
    :pswitch_1
    iget-object v0, p0, Lcxd$c;->a:Lcxd$b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcxd$b;->a(Z)V

    goto :goto_0

    .line 531
    :pswitch_2
    iget-object v0, p0, Lcxd$c;->a:Lcxd$b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcxd$b;->a(Z)V

    goto :goto_0

    .line 535
    :pswitch_3
    iget-object v0, p0, Lcxd$c;->a:Lcxd$b;

    iget-object v1, p0, Lcxd$c;->a:Lcxd$b;

    iget-object v1, v1, Lcxd$b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcxd$b;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 519
    nop

    :pswitch_data_0
    .packed-switch 0x7b
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
