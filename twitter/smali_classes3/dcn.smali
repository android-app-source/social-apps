.class public final Ldcn;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static final a:[Ljava/lang/String;

.field static final b:[Ljava/lang/String;

.field static final c:Lokhttp3/CertificatePinner;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "1a21b4952b6293ce18b365ec9c0e934cb381e6d4"

    aput-object v1, v0, v3

    const-string/jumbo v1, "2343d148a255899b947d461a797ec04cfed170b7"

    aput-object v1, v0, v4

    const-string/jumbo v1, "5519b278acb281d7eda7abc18399c3bb690424b5"

    aput-object v1, v0, v5

    const-string/jumbo v1, "1237ba4517eead2926fdc1cdfebeedf2ded9145c"

    aput-object v1, v0, v6

    const-string/jumbo v1, "5abec575dcaef3b08e271943fc7f250c3df661e3"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "e27f7bd877d5df9e0a3f9eb4cb0e2ea9efdb6977"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "22f19e2ec6eaccfc5d2346f4c2e8f6c554dd5e07"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "ed663135d31bd4eca614c429e319069f94c12650"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "b181081a19a4c0941ffae89528c124c99b34acc7"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "3c03436868951cf3692ab8b426daba8fe922e5bd"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "bbc23e290bb328771dad3ea24dbdf423bd06b03d"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "c07a98688d89fbab05640c117daa7d65b8cacc4e"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "713836f2023153472b6eba6546a9101558200509"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "b01989e7effb4aafcb148f58463976224150e1ba"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "bdbea71bab7157f9e475d954d2b727801a822682"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "9ca98d00af740ddd8180d21345a58b8f2e9438d6"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "87e85b6353c623a3128cb0ffbbf551fe59800e22"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "5e4f538685dd4f9eca5fdc0d456f7d51b1dc9b7b"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "d52e13c1abe349dae8b49594ef7c3843606466bd"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "83317e62854253d6d7783190ec919056e991b9e3"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "68330e61358521592983a3c8d2d2e1406e7ab3c1"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "56fef3c2147d4ed38837fdbd3052387201e5778d"

    aput-object v2, v0, v1

    sput-object v0, Ldcn;->a:[Ljava/lang/String;

    .line 46
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "sha1/GiG0lStik84Ys2XsnA6TTLOB5tQ="

    aput-object v1, v0, v3

    const-string/jumbo v1, "sha1/I0PRSKJViZuUfUYaeX7ATP7RcLc="

    aput-object v1, v0, v4

    const-string/jumbo v1, "sha1/VRmyeKyygdftp6vBg5nDu2kEJLU="

    aput-object v1, v0, v5

    const-string/jumbo v1, "sha1/Eje6RRfurSkm/cHN/r7t8t7ZFFw="

    aput-object v1, v0, v6

    const-string/jumbo v1, "sha1/Wr7Fddyu87COJxlD/H8lDD32YeM="

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "sha1/4n972HfV354KP560yw4uqe/baXc="

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "sha1/IvGeLsbqzPxdI0b0wuj2xVTdXgc="

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "sha1/7WYxNdMb1OymFMQp4xkGn5TBJlA="

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "sha1/sYEIGhmkwJQf+uiVKMEkyZs0rMc="

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "sha1/PANDaGiVHPNpKri0Jtq6j+ki5b0="

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "sha1/u8I+KQuzKHcdrT6iTb30I70GsD0="

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "sha1/wHqYaI2J+6sFZAwRfap9ZbjKzE4="

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "sha1/cTg28gIxU0crbrplRqkQFVggBQk="

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "sha1/sBmJ5+/7Sq/LFI9YRjl2IkFQ4bo="

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "sha1/vb6nG6txV/nkddlU0rcngBqCJoI="

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "sha1/nKmNAK90Dd2BgNITRaWLjy6UONY="

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "sha1/h+hbY1PGI6MSjLD/u/VR/lmADiI="

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "sha1/Xk9ThoXdT57KX9wNRW99UbHcm3s="

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "sha1/1S4TwavjSdrotJWU73w4Q2BkZr0="

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "sha1/gzF+YoVCU9bXeDGQ7JGQVumRueM="

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "sha1/aDMOYTWFIVkpg6PI0tLhQG56s8E="

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "sha1/Vv7zwhR9TtOIN/29MFI4cgHld40="

    aput-object v2, v0, v1

    sput-object v0, Ldcn;->b:[Ljava/lang/String;

    .line 73
    new-instance v0, Lokhttp3/CertificatePinner$Builder;

    invoke-direct {v0}, Lokhttp3/CertificatePinner$Builder;-><init>()V

    const-string/jumbo v1, "*.periscope.tv"

    sget-object v2, Ldcn;->b:[Ljava/lang/String;

    .line 74
    invoke-virtual {v0, v1, v2}, Lokhttp3/CertificatePinner$Builder;->add(Ljava/lang/String;[Ljava/lang/String;)Lokhttp3/CertificatePinner$Builder;

    move-result-object v0

    const-string/jumbo v1, "*.twitter.com"

    sget-object v2, Ldcn;->b:[Ljava/lang/String;

    .line 75
    invoke-virtual {v0, v1, v2}, Lokhttp3/CertificatePinner$Builder;->add(Ljava/lang/String;[Ljava/lang/String;)Lokhttp3/CertificatePinner$Builder;

    move-result-object v0

    const-string/jumbo v1, "*.twimg.com"

    sget-object v2, Ldcn;->b:[Ljava/lang/String;

    .line 76
    invoke-virtual {v0, v1, v2}, Lokhttp3/CertificatePinner$Builder;->add(Ljava/lang/String;[Ljava/lang/String;)Lokhttp3/CertificatePinner$Builder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lokhttp3/CertificatePinner$Builder;->build()Lokhttp3/CertificatePinner;

    move-result-object v0

    sput-object v0, Ldcn;->c:Lokhttp3/CertificatePinner;

    .line 78
    return-void
.end method

.method public static a()Lokhttp3/CertificatePinner;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Ldcn;->c:Lokhttp3/CertificatePinner;

    return-object v0
.end method
