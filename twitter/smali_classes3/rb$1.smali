.class Lrb$1;
.super Lcom/twitter/library/util/s;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lrb;->a(Lcom/twitter/library/card/CardContext;Lcom/twitter/model/core/TwitterUser;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/core/TwitterUser;

.field final synthetic b:Lrb;


# direct methods
.method constructor <init>(Lrb;Lcom/twitter/model/core/TwitterUser;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lrb$1;->b:Lrb;

    iput-object p2, p0, Lrb$1;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {p0}, Lcom/twitter/library/util/s;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 7

    .prologue
    const v5, 0x7f0a0544

    const/4 v6, 0x1

    .line 191
    iget-object v0, p0, Lrb$1;->b:Lrb;

    invoke-static {v0}, Lrb;->a(Lrb;)Landroid/app/Activity;

    move-result-object v2

    .line 192
    if-eqz v2, :cond_0

    .line 193
    iget-object v0, p0, Lrb$1;->b:Lrb;

    .line 194
    invoke-virtual {v0}, Lrb;->e()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lrb$1;->b:Lrb;

    invoke-static {v1}, Lrb;->b(Lrb;)Lcom/twitter/android/card/d;

    move-result-object v1

    const-string/jumbo v3, "click"

    iget-object v4, p0, Lrb$1;->b:Lrb;

    invoke-virtual {v4}, Lrb;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4, v0}, Lcom/twitter/android/card/d;->d(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 197
    iget-object v1, p0, Lrb$1;->b:Lrb;

    invoke-static {v1}, Lrb;->b(Lrb;)Lcom/twitter/android/card/d;

    move-result-object v1

    sget-object v3, Lcom/twitter/library/api/PromotedEvent;->n:Lcom/twitter/library/api/PromotedEvent;

    invoke-interface {v1, v3, v0}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 200
    iget-object v0, p0, Lrb$1;->b:Lrb;

    invoke-static {v0}, Lrb;->c(Lrb;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lrb$1;->b:Lrb;

    invoke-static {v0}, Lrb;->d(Lrb;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    new-instance v0, Lcom/twitter/app/dm/j$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/j$a;-><init>()V

    iget-object v1, p0, Lrb$1;->b:Lrb;

    .line 203
    invoke-static {v1}, Lrb;->c(Lrb;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/j$a;->f(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    iget-object v1, p0, Lrb$1;->b:Lrb;

    .line 204
    invoke-static {v1}, Lrb;->e(Lrb;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/j$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    iget-object v1, p0, Lrb$1;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 205
    invoke-virtual {v0, v4, v5}, Lcom/twitter/app/dm/j$a;->a(J)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 206
    invoke-virtual {v0, v6}, Lcom/twitter/app/dm/j$a;->a(Z)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    .line 207
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 201
    invoke-static {v2, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v0

    .line 221
    :goto_0
    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 223
    :cond_0
    return-void

    .line 209
    :cond_1
    iget-object v0, p0, Lrb$1;->b:Lrb;

    invoke-static {v0}, Lrb;->f(Lrb;)Lcom/twitter/library/card/CardContext;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 210
    iget-object v1, p0, Lrb$1;->b:Lrb;

    invoke-static {v1}, Lrb;->g(Lrb;)Landroid/content/Context;

    move-result-object v3

    new-instance v1, Lcom/twitter/app/dm/r$a;

    invoke-direct {v1}, Lcom/twitter/app/dm/r$a;-><init>()V

    new-instance v4, Lcom/twitter/model/core/r;

    invoke-direct {v4, v0}, Lcom/twitter/model/core/r;-><init>(Lcom/twitter/model/core/Tweet;)V

    .line 212
    invoke-virtual {v1, v4}, Lcom/twitter/app/dm/r$a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/app/dm/r$a;

    move-result-object v1

    iget-object v4, p0, Lrb$1;->a:Lcom/twitter/model/core/TwitterUser;

    .line 213
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/twitter/app/dm/r$a;->a(Ljava/util/List;)Lcom/twitter/app/dm/r$a;

    move-result-object v1

    iget-object v4, p0, Lrb$1;->b:Lrb;

    .line 214
    invoke-static {v4}, Lrb;->e(Lrb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/twitter/app/dm/r$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/dm/r$a;

    .line 215
    invoke-virtual {v1, v6}, Lcom/twitter/app/dm/r$a;->c(Z)Lcom/twitter/app/dm/r$a;

    move-result-object v1

    .line 216
    invoke-virtual {v2, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/twitter/app/dm/r$a;->b(Ljava/lang/String;)Lcom/twitter/app/dm/r$a;

    move-result-object v1

    .line 217
    invoke-virtual {v1, v5}, Lcom/twitter/app/dm/r$a;->a(I)Lcom/twitter/app/dm/r$a;

    move-result-object v1

    .line 218
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/r$a;->c(Ljava/lang/String;)Lcom/twitter/app/dm/r$a;

    move-result-object v0

    .line 219
    invoke-virtual {v0}, Lcom/twitter/app/dm/r$a;->e()Lcom/twitter/app/dm/r;

    move-result-object v0

    .line 210
    invoke-static {v3, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/r;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lrb$1;->b:Lrb;

    invoke-static {v0}, Lrb;->h(Lrb;)Landroid/widget/TextView;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lrb$1;->b:Lrb;

    invoke-static {v0}, Lrb;->h(Lrb;)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/ui/widget/TwitterButton;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 230
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/library/util/s;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
