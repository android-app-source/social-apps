.class public final Lcwz;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/Random;

.field private static final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 18
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcwz;->a:Ljava/util/Random;

    .line 21
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcwz;->b:J

    return-void
.end method

.method public static a()J
    .locals 2

    .prologue
    .line 42
    const/4 v0, 0x7

    invoke-static {v0}, Lcwz;->a(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(I)J
    .locals 6

    .prologue
    .line 35
    sget-wide v0, Lcwz;->b:J

    long-to-double v0, v0

    const/4 v2, 0x7

    invoke-static {p0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    .line 36
    sget-wide v2, Lcwz;->b:J

    long-to-double v2, v2

    sget-object v4, Lcwz;->a:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextFloat()F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Lretrofit/RetrofitError;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 50
    invoke-static {p0}, Lcwz;->b(Lretrofit/RetrofitError;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 57
    :cond_0
    :goto_0
    return v0

    .line 53
    :cond_1
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 54
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v2

    invoke-virtual {v2}, Lretrofit/client/Response;->getStatus()I

    move-result v2

    .line 55
    const/16 v3, 0x1f4

    if-eq v2, v3, :cond_2

    const/16 v3, 0x1f7

    if-ne v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static b(I)Z
    .locals 1

    .prologue
    .line 46
    const/16 v0, 0x1f4

    if-lt p0, v0, :cond_0

    const/16 v0, 0x257

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lretrofit/RetrofitError;)Z
    .locals 3

    .prologue
    .line 61
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    .line 62
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 63
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lretrofit/client/Response;->getStatus()I

    move-result v0

    const/16 v2, 0x198

    if-eq v0, v2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v0

    sget-object v2, Lretrofit/RetrofitError$Kind;->NETWORK:Lretrofit/RetrofitError$Kind;

    if-eq v0, v2, :cond_1

    instance-of v0, v1, Ljava/net/UnknownHostException;

    if-nez v0, :cond_1

    instance-of v0, v1, Ljava/net/ConnectException;

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
