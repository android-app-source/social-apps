.class public Lcrq;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcrq$a;
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:Ljava/lang/reflect/Method;

.field private e:Ljava/lang/reflect/Method;

.field private f:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/16 v0, 0x63

    iput v0, p0, Lcrq;->a:I

    .line 29
    iput v3, p0, Lcrq;->b:I

    .line 37
    :try_start_0
    const-class v0, Landroid/telephony/SignalStrength;

    const-string/jumbo v1, "getLteRssnr"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcrq;->d:Ljava/lang/reflect/Method;

    .line 38
    const-class v0, Landroid/telephony/SignalStrength;

    const-string/jumbo v1, "getLteRsrp"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcrq;->e:Ljava/lang/reflect/Method;

    .line 39
    const-class v0, Landroid/telephony/SignalStrength;

    const-string/jumbo v1, "getLteSignalStrength"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcrq;->f:Ljava/lang/reflect/Method;

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcrq;->c:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :goto_0
    const-string/jumbo v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 49
    new-instance v1, Lcrq$a;

    invoke-direct {v1, p0, v4}, Lcrq$a;-><init>(Lcrq;Lcrq$1;)V

    .line 50
    const/16 v2, 0x100

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 51
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    iput-object v4, p0, Lcrq;->d:Ljava/lang/reflect/Method;

    .line 43
    iput-object v4, p0, Lcrq;->e:Ljava/lang/reflect/Method;

    .line 44
    iput-object v4, p0, Lcrq;->f:Ljava/lang/reflect/Method;

    .line 45
    iput-boolean v3, p0, Lcrq;->c:Z

    goto :goto_0
.end method

.method private a(Landroid/telephony/SignalStrength;)I
    .locals 2

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 107
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 108
    iget-boolean v1, p0, Lcrq;->c:Z

    if-eqz v1, :cond_0

    .line 109
    invoke-direct {p0, p1}, Lcrq;->e(Landroid/telephony/SignalStrength;)I

    move-result v0

    .line 111
    :cond_0
    if-nez v0, :cond_1

    .line 112
    invoke-static {p1}, Lcrq;->b(Landroid/telephony/SignalStrength;)I

    move-result v0

    .line 128
    :cond_1
    :goto_0
    return v0

    .line 115
    :cond_2
    invoke-static {p1}, Lcrq;->c(Landroid/telephony/SignalStrength;)I

    move-result v0

    .line 116
    invoke-static {p1}, Lcrq;->d(Landroid/telephony/SignalStrength;)I

    move-result v1

    .line 117
    if-eqz v1, :cond_1

    .line 120
    if-nez v0, :cond_3

    move v0, v1

    .line 122
    goto :goto_0

    .line 125
    :cond_3
    if-lt v0, v1, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcrq;I)I
    .locals 0

    .prologue
    .line 19
    iput p1, p0, Lcrq;->a:I

    return p1
.end method

.method static synthetic a(Lcrq;Landroid/telephony/SignalStrength;)I
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcrq;->a(Landroid/telephony/SignalStrength;)I

    move-result v0

    return v0
.end method

.method public static a()Lcrq;
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lcot;->ak()Lcot;

    move-result-object v0

    check-cast v0, Lcor;

    invoke-interface {v0}, Lcor;->F()Lcrq;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/telephony/SignalStrength;)I
    .locals 3

    .prologue
    const/4 v0, 0x2

    .line 136
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v1

    .line 138
    if-le v1, v0, :cond_0

    const/16 v2, 0x63

    if-ne v1, v2, :cond_2

    .line 139
    :cond_0
    const/4 v0, 0x0

    .line 150
    :cond_1
    :goto_0
    return v0

    .line 140
    :cond_2
    const/16 v2, 0xc

    if-lt v1, v2, :cond_3

    .line 141
    const/4 v0, 0x4

    goto :goto_0

    .line 142
    :cond_3
    const/16 v2, 0x8

    if-lt v1, v2, :cond_4

    .line 143
    const/4 v0, 0x3

    goto :goto_0

    .line 144
    :cond_4
    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 147
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcrq;I)I
    .locals 0

    .prologue
    .line 19
    iput p1, p0, Lcrq;->b:I

    return p1
.end method

.method private static c(Landroid/telephony/SignalStrength;)I
    .locals 8

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 154
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v0

    .line 155
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaEcio()I

    move-result v6

    .line 158
    const/16 v7, -0x4b

    if-lt v0, v7, :cond_0

    move v0, v1

    .line 172
    :goto_0
    const/16 v7, -0x5a

    if-lt v6, v7, :cond_4

    .line 184
    :goto_1
    if-ge v0, v1, :cond_8

    :goto_2
    return v0

    .line 160
    :cond_0
    const/16 v7, -0x55

    if-lt v0, v7, :cond_1

    move v0, v2

    .line 161
    goto :goto_0

    .line 162
    :cond_1
    const/16 v7, -0x5f

    if-lt v0, v7, :cond_2

    move v0, v3

    .line 163
    goto :goto_0

    .line 164
    :cond_2
    const/16 v7, -0x64

    if-lt v0, v7, :cond_3

    move v0, v4

    .line 165
    goto :goto_0

    :cond_3
    move v0, v5

    .line 167
    goto :goto_0

    .line 174
    :cond_4
    const/16 v1, -0x6e

    if-lt v6, v1, :cond_5

    move v1, v2

    .line 175
    goto :goto_1

    .line 176
    :cond_5
    const/16 v1, -0x82

    if-lt v6, v1, :cond_6

    move v1, v3

    .line 177
    goto :goto_1

    .line 178
    :cond_6
    const/16 v1, -0x96

    if-lt v6, v1, :cond_7

    move v1, v4

    .line 179
    goto :goto_1

    :cond_7
    move v1, v5

    .line 181
    goto :goto_1

    :cond_8
    move v0, v1

    .line 184
    goto :goto_2
.end method

.method private static d(Landroid/telephony/SignalStrength;)I
    .locals 8

    .prologue
    const/4 v1, 0x4

    const/4 v3, 0x2

    const/4 v5, 0x0

    const/4 v2, 0x3

    const/4 v4, 0x1

    .line 188
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoDbm()I

    move-result v0

    .line 189
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoSnr()I

    move-result v6

    .line 192
    const/16 v7, -0x41

    if-lt v0, v7, :cond_0

    move v0, v1

    .line 205
    :goto_0
    const/4 v7, 0x7

    if-lt v6, v7, :cond_4

    .line 217
    :goto_1
    if-ge v0, v1, :cond_8

    :goto_2
    return v0

    .line 194
    :cond_0
    const/16 v7, -0x4b

    if-lt v0, v7, :cond_1

    move v0, v2

    .line 195
    goto :goto_0

    .line 196
    :cond_1
    const/16 v7, -0x5a

    if-lt v0, v7, :cond_2

    move v0, v3

    .line 197
    goto :goto_0

    .line 198
    :cond_2
    const/16 v7, -0x69

    if-lt v0, v7, :cond_3

    move v0, v4

    .line 199
    goto :goto_0

    :cond_3
    move v0, v5

    .line 201
    goto :goto_0

    .line 207
    :cond_4
    const/4 v1, 0x5

    if-lt v6, v1, :cond_5

    move v1, v2

    .line 208
    goto :goto_1

    .line 209
    :cond_5
    if-lt v6, v2, :cond_6

    move v1, v3

    .line 210
    goto :goto_1

    .line 211
    :cond_6
    if-lt v6, v4, :cond_7

    move v1, v4

    .line 212
    goto :goto_1

    :cond_7
    move v1, v5

    .line 214
    goto :goto_1

    :cond_8
    move v0, v1

    .line 217
    goto :goto_2
.end method

.method private e(Landroid/telephony/SignalStrength;)I
    .locals 9

    .prologue
    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 223
    :try_start_0
    iget-object v0, p0, Lcrq;->e:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 224
    iget-object v0, p0, Lcrq;->d:Ljava/lang/reflect/Method;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 225
    iget-object v0, p0, Lcrq;->f:Ljava/lang/reflect/Method;

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 239
    const/16 v0, -0x2c

    if-le v1, v0, :cond_1

    move v0, v6

    .line 259
    :goto_0
    const/16 v1, 0x12c

    if-le v7, v1, :cond_6

    move v1, v6

    .line 274
    :goto_1
    if-eq v1, v6, :cond_c

    if-eq v0, v6, :cond_c

    .line 280
    if-ge v0, v1, :cond_b

    .line 304
    :cond_0
    :goto_2
    return v0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    iput-boolean v2, p0, Lcrq;->c:Z

    move v0, v2

    .line 228
    goto :goto_2

    .line 241
    :cond_1
    const/16 v0, -0x55

    if-lt v1, v0, :cond_2

    .line 242
    const/4 v0, 0x4

    goto :goto_0

    .line 243
    :cond_2
    const/16 v0, -0x5f

    if-lt v1, v0, :cond_3

    move v0, v3

    .line 244
    goto :goto_0

    .line 245
    :cond_3
    const/16 v0, -0x69

    if-lt v1, v0, :cond_4

    move v0, v4

    .line 246
    goto :goto_0

    .line 247
    :cond_4
    const/16 v0, -0x73

    if-lt v1, v0, :cond_5

    move v0, v5

    .line 248
    goto :goto_0

    .line 249
    :cond_5
    const/16 v0, -0x8c

    if-lt v1, v0, :cond_14

    move v0, v2

    .line 250
    goto :goto_0

    .line 261
    :cond_6
    const/16 v1, 0x82

    if-lt v7, v1, :cond_7

    .line 262
    const/4 v1, 0x4

    goto :goto_1

    .line 263
    :cond_7
    const/16 v1, 0x2d

    if-lt v7, v1, :cond_8

    move v1, v3

    .line 264
    goto :goto_1

    .line 265
    :cond_8
    const/16 v1, 0xa

    if-lt v7, v1, :cond_9

    move v1, v4

    .line 266
    goto :goto_1

    .line 267
    :cond_9
    const/16 v1, -0x1e

    if-lt v7, v1, :cond_a

    move v1, v5

    .line 268
    goto :goto_1

    .line 269
    :cond_a
    const/16 v1, -0xc8

    if-lt v7, v1, :cond_13

    move v1, v2

    .line 270
    goto :goto_1

    :cond_b
    move v0, v1

    .line 280
    goto :goto_2

    .line 283
    :cond_c
    if-eq v1, v6, :cond_d

    move v0, v1

    .line 284
    goto :goto_2

    .line 287
    :cond_d
    if-ne v0, v6, :cond_0

    .line 292
    const/16 v0, 0x3f

    if-le v8, v0, :cond_e

    move v0, v2

    .line 293
    goto :goto_2

    .line 294
    :cond_e
    const/16 v0, 0xc

    if-lt v8, v0, :cond_f

    .line 295
    const/4 v0, 0x4

    goto :goto_2

    .line 296
    :cond_f
    const/16 v0, 0x8

    if-lt v8, v0, :cond_10

    move v0, v3

    .line 297
    goto :goto_2

    .line 298
    :cond_10
    const/4 v0, 0x5

    if-lt v8, v0, :cond_11

    move v0, v4

    .line 299
    goto :goto_2

    .line 300
    :cond_11
    if-ltz v8, :cond_12

    move v0, v5

    .line 301
    goto :goto_2

    :cond_12
    move v0, v2

    goto :goto_2

    :cond_13
    move v1, v6

    goto :goto_1

    :cond_14
    move v0, v6

    goto :goto_0
.end method


# virtual methods
.method public b()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcrq;->a:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcrq;->b:I

    packed-switch v0, :pswitch_data_0

    .line 82
    const-string/jumbo v0, "unknown"

    :goto_0
    return-object v0

    .line 70
    :pswitch_0
    const-string/jumbo v0, "great"

    goto :goto_0

    .line 73
    :pswitch_1
    const-string/jumbo v0, "good"

    goto :goto_0

    .line 76
    :pswitch_2
    const-string/jumbo v0, "moderate"

    goto :goto_0

    .line 79
    :pswitch_3
    const-string/jumbo v0, "poor"

    goto :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
