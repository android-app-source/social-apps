.class public final Lzc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lzc$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private A:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxn;",
            ">;"
        }
    .end annotation
.end field

.field private B:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private C:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private D:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/support/v4/content/LocalBroadcastManager;",
            ">;"
        }
    .end annotation
.end field

.field private E:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcrc;",
            ">;"
        }
    .end annotation
.end field

.field private F:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcif;",
            ">;"
        }
    .end annotation
.end field

.field private G:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxp;",
            ">;"
        }
    .end annotation
.end field

.field private H:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/util/m;",
            ">;"
        }
    .end annotation
.end field

.field private I:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcqu;",
            ">;"
        }
    .end annotation
.end field

.field private J:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxe;",
            ">;"
        }
    .end annotation
.end field

.field private K:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxh;",
            ">;"
        }
    .end annotation
.end field

.field private L:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/j;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyf;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/schema/TwitterSchema;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lwo;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcnz;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/moments/data/c",
            "<",
            "Lapb;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxv;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/provider/t;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbsb;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/Long;",
            "Lbdk;",
            ">;>;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lwq$a;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lwq;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxr;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lawb;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxx;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyd;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxt;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laun",
            "<",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private x:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyh;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyg;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const-class v0, Lzc;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lzc;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lzc$a;)V
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    sget-boolean v0, Lzc;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 175
    :cond_0
    invoke-direct {p0, p1}, Lzc;->a(Lzc$a;)V

    .line 176
    return-void
.end method

.method synthetic constructor <init>(Lzc$a;Lzc$1;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lzc;-><init>(Lzc$a;)V

    return-void
.end method

.method public static a()Lzc$a;
    .locals 2

    .prologue
    .line 179
    new-instance v0, Lzc$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lzc$a;-><init>(Lzc$1;)V

    return-object v0
.end method

.method private a(Lzc$a;)V
    .locals 3

    .prologue
    .line 185
    new-instance v0, Lzc$1;

    invoke-direct {v0, p0, p1}, Lzc$1;-><init>(Lzc;Lzc$a;)V

    iput-object v0, p0, Lzc;->b:Lcta;

    .line 198
    new-instance v0, Lzc$2;

    invoke-direct {v0, p0, p1}, Lzc$2;-><init>(Lzc;Lzc$a;)V

    iput-object v0, p0, Lzc;->c:Lcta;

    .line 211
    iget-object v0, p0, Lzc;->b:Lcta;

    iget-object v1, p0, Lzc;->c:Lcta;

    .line 213
    invoke-static {v0, v1}, Lyp;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 212
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->d:Lcta;

    .line 216
    iget-object v0, p0, Lzc;->d:Lcta;

    .line 217
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->e:Lcta;

    .line 219
    new-instance v0, Lzc$3;

    invoke-direct {v0, p0, p1}, Lzc$3;-><init>(Lzc;Lzc$a;)V

    iput-object v0, p0, Lzc;->f:Lcta;

    .line 232
    iget-object v0, p0, Lzc;->f:Lcta;

    .line 234
    invoke-static {v0}, Lyl;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 233
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->g:Lcta;

    .line 237
    new-instance v0, Lzc$4;

    invoke-direct {v0, p0, p1}, Lzc$4;-><init>(Lzc;Lzc$a;)V

    iput-object v0, p0, Lzc;->h:Lcta;

    .line 250
    iget-object v0, p0, Lzc;->b:Lcta;

    .line 252
    invoke-static {v0}, Lyn;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 251
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->i:Lcta;

    .line 255
    iget-object v0, p0, Lzc;->h:Lcta;

    iget-object v1, p0, Lzc;->i:Lcta;

    .line 257
    invoke-static {v0, v1}, Lxw;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 256
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->j:Lcta;

    .line 260
    new-instance v0, Lzc$5;

    invoke-direct {v0, p0, p1}, Lzc$5;-><init>(Lzc;Lzc$a;)V

    iput-object v0, p0, Lzc;->k:Lcta;

    .line 273
    iget-object v0, p0, Lzc;->b:Lcta;

    iget-object v1, p0, Lzc;->k:Lcta;

    iget-object v2, p0, Lzc;->h:Lcta;

    .line 275
    invoke-static {v0, v1, v2}, Lbsc;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 274
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->l:Lcta;

    .line 280
    iget-object v0, p0, Lzc;->b:Lcta;

    iget-object v1, p0, Lzc;->c:Lcta;

    iget-object v2, p0, Lzc;->l:Lcta;

    .line 282
    invoke-static {v0, v1, v2}, Lym;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 281
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->m:Lcta;

    .line 288
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lzc;->m:Lcta;

    .line 287
    invoke-static {v0, v1}, Lws;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 286
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->n:Lcta;

    .line 291
    iget-object v0, p0, Lzc;->n:Lcta;

    iget-object v1, p0, Lzc;->j:Lcta;

    .line 293
    invoke-static {v0, v1}, Lwr;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 292
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->o:Lcta;

    .line 296
    iget-object v0, p0, Lzc;->g:Lcta;

    iget-object v1, p0, Lzc;->j:Lcta;

    iget-object v2, p0, Lzc;->o:Lcta;

    .line 298
    invoke-static {v0, v1, v2}, Lxs;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 297
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->p:Lcta;

    .line 303
    new-instance v0, Lzc$6;

    invoke-direct {v0, p0, p1}, Lzc$6;-><init>(Lzc;Lzc$a;)V

    iput-object v0, p0, Lzc;->q:Lcta;

    .line 316
    iget-object v0, p0, Lzc;->q:Lcta;

    .line 318
    invoke-static {v0}, Lyv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 317
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->r:Lcta;

    .line 321
    iget-object v0, p0, Lzc;->q:Lcta;

    .line 323
    invoke-static {v0}, Lyo;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 322
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->s:Lcta;

    .line 326
    iget-object v0, p0, Lzc;->s:Lcta;

    .line 328
    invoke-static {v0}, Lxy;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 327
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->t:Lcta;

    .line 331
    iget-object v0, p0, Lzc;->d:Lcta;

    .line 333
    invoke-static {v0}, Lye;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 332
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->u:Lcta;

    .line 335
    iget-object v0, p0, Lzc;->t:Lcta;

    iget-object v1, p0, Lzc;->u:Lcta;

    .line 337
    invoke-static {v0, v1}, Lxu;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 336
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->v:Lcta;

    .line 341
    iget-object v0, p0, Lzc;->v:Lcta;

    .line 342
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->w:Lcta;

    .line 344
    iget-object v0, p0, Lzc;->w:Lcta;

    .line 348
    invoke-static {}, Lcom/twitter/android/moments/data/u;->c()Ldagger/internal/c;

    move-result-object v1

    iget-object v2, p0, Lzc;->d:Lcta;

    .line 346
    invoke-static {v0, v1, v2}, Lyi;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 345
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->x:Lcta;

    .line 351
    iget-object v0, p0, Lzc;->x:Lcta;

    .line 352
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->y:Lcta;

    .line 354
    iget-object v0, p0, Lzc;->y:Lcta;

    .line 356
    invoke-static {v0}, Lxl;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 355
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->z:Lcta;

    .line 358
    iget-object v0, p0, Lzc;->p:Lcta;

    iget-object v1, p0, Lzc;->r:Lcta;

    iget-object v2, p0, Lzc;->z:Lcta;

    .line 360
    invoke-static {v0, v1, v2}, Lxo;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 359
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->A:Lcta;

    .line 365
    iget-object v0, p0, Lzc;->A:Lcta;

    .line 366
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->B:Lcta;

    .line 368
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 369
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lzc;->e:Lcta;

    .line 370
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lzc;->B:Lcta;

    .line 371
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 372
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lzc;->C:Lcta;

    .line 374
    new-instance v0, Lzc$7;

    invoke-direct {v0, p0, p1}, Lzc$7;-><init>(Lzc;Lzc$a;)V

    iput-object v0, p0, Lzc;->D:Lcta;

    .line 387
    iget-object v0, p0, Lzc;->D:Lcta;

    .line 388
    invoke-static {v0}, Lcrd;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lzc;->E:Lcta;

    .line 390
    iget-object v0, p0, Lzc;->E:Lcta;

    .line 392
    invoke-static {v0}, Lcig;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 391
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->F:Lcta;

    .line 394
    iget-object v0, p0, Lzc;->A:Lcta;

    iget-object v1, p0, Lzc;->F:Lcta;

    .line 396
    invoke-static {v0, v1}, Lxq;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 395
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->G:Lcta;

    .line 401
    invoke-static {}, Lyy;->c()Ldagger/internal/c;

    move-result-object v0

    .line 400
    invoke-static {v0}, Lcom/twitter/library/util/n;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lzc;->H:Lcta;

    .line 405
    invoke-static {}, Lyx;->c()Ldagger/internal/c;

    move-result-object v0

    .line 404
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->I:Lcta;

    .line 407
    iget-object v0, p0, Lzc;->H:Lcta;

    .line 410
    invoke-static {}, Lxg;->c()Ldagger/internal/c;

    move-result-object v1

    iget-object v2, p0, Lzc;->I:Lcta;

    .line 408
    invoke-static {v0, v1, v2}, Lxf;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lzc;->J:Lcta;

    .line 413
    iget-object v0, p0, Lzc;->r:Lcta;

    iget-object v1, p0, Lzc;->J:Lcta;

    iget-object v2, p0, Lzc;->F:Lcta;

    .line 415
    invoke-static {v0, v1, v2}, Lxi;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 414
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->K:Lcta;

    .line 420
    iget-object v0, p0, Lzc;->r:Lcta;

    .line 422
    invoke-static {v0}, Lcom/twitter/library/api/moments/maker/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 421
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzc;->L:Lcta;

    .line 424
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 428
    iget-object v0, p0, Lzc;->C:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lxp;
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lzc;->G:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxp;

    return-object v0
.end method

.method public d()Lxh;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lzc;->K:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxh;

    return-object v0
.end method

.method public e()Lcom/twitter/library/api/moments/maker/j;
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lzc;->L:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/moments/maker/j;

    return-object v0
.end method
