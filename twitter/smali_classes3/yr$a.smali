.class public final Lyr$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lyr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lamu;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lyr$1;)V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Lyr$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lyr$a;)Lamu;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lyr$a;->a:Lamu;

    return-object v0
.end method


# virtual methods
.method public a(Lamu;)Lyr$a;
    .locals 1

    .prologue
    .line 211
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamu;

    iput-object v0, p0, Lyr$a;->a:Lamu;

    .line 212
    return-object p0
.end method

.method public a()Lyt;
    .locals 3

    .prologue
    .line 171
    iget-object v0, p0, Lyr$a;->a:Lamu;

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lamu;

    .line 173
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_0
    new-instance v0, Lyr;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lyr;-><init>(Lyr$a;Lyr$1;)V

    return-object v0
.end method
