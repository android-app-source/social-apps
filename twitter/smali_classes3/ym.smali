.class public final Lym;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/util/object/d",
        "<",
        "Ljava/lang/Long;",
        "Lbdk;",
        ">;>;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbsb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lym;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lym;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;",
            "Lcta",
            "<",
            "Lbsb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-boolean v0, Lym;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 29
    :cond_0
    iput-object p1, p0, Lym;->b:Lcta;

    .line 30
    sget-boolean v0, Lym;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 31
    :cond_1
    iput-object p2, p0, Lym;->c:Lcta;

    .line 32
    sget-boolean v0, Lym;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_2
    iput-object p3, p0, Lym;->d:Lcta;

    .line 34
    return-void
.end method

.method public static a(Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;",
            "Lcta",
            "<",
            "Lbsb;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/Long;",
            "Lbdk;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lym;

    invoke-direct {v0, p0, p1, p2}, Lym;-><init>(Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/util/object/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/Long;",
            "Lbdk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lym;->b:Lcta;

    .line 40
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lym;->c:Lcta;

    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lym;->d:Lcta;

    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbsb;

    .line 39
    invoke-static {v0, v1, v2}, Lyk;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lbsb;)Lcom/twitter/util/object/d;

    move-result-object v0

    const-string/jumbo v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 38
    invoke-static {v0, v1}, Ldagger/internal/e;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/object/d;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lym;->a()Lcom/twitter/util/object/d;

    move-result-object v0

    return-object v0
.end method
