.class public Lqi;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            "Lqm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lauj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lauj",
            "<",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            "Lqm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Laug;

    invoke-direct {v0, p1}, Laug;-><init>(Lauj;)V

    iput-object v0, p0, Lqi;->a:Lauj;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/playback/AVDataSource;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            ")",
            "Lrx/c",
            "<",
            "Lqm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lqi;->a:Lauj;

    invoke-interface {v0, p1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lqi;->a:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 43
    return-void
.end method
