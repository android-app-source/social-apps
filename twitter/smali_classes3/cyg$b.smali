.class Lcyg$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcyg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcyg;

.field private b:[B

.field private c:I

.field private d:J


# direct methods
.method constructor <init>(Lcyg;I)V
    .locals 1

    .prologue
    .line 709
    iput-object p1, p0, Lcyg$b;->a:Lcyg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 710
    mul-int/lit8 v0, p2, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lcyg$b;->b:[B

    .line 711
    return-void
.end method


# virtual methods
.method public a(Landroid/media/AudioRecord;J)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 714
    iget-object v0, p0, Lcyg$b;->b:[B

    iget-object v1, p0, Lcyg$b;->b:[B

    array-length v1, v1

    invoke-virtual {p1, v0, v3, v1}, Landroid/media/AudioRecord;->read([BII)I

    move-result v0

    iput v0, p0, Lcyg$b;->c:I

    .line 715
    iget-object v0, p0, Lcyg$b;->a:Lcyg;

    iget-boolean v0, v0, Lcyg;->q:Z

    if-eqz v0, :cond_0

    .line 717
    invoke-virtual {p0}, Lcyg$b;->b()V

    .line 719
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v0

    sub-long/2addr v0, p2

    iput-wide v0, p0, Lcyg$b;->d:J

    .line 721
    iget v0, p0, Lcyg$b;->c:I

    if-gez v0, :cond_1

    .line 722
    const-string/jumbo v0, "CameraBroadcaster"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Audio record read error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcyg$b;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    iput v3, p0, Lcyg$b;->c:I

    .line 725
    :cond_1
    iget v0, p0, Lcyg$b;->c:I

    return v0
.end method

.method a()J
    .locals 2

    .prologue
    .line 733
    iget-wide v0, p0, Lcyg$b;->d:J

    return-wide v0
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    .line 737
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 738
    iget v0, p0, Lcyg$b;->c:I

    if-lez v0, :cond_0

    .line 739
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v1, p0, Lcyg$b;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 740
    iget-object v1, p0, Lcyg$b;->b:[B

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 741
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 743
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 746
    iget-object v0, p0, Lcyg$b;->b:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 747
    return-void
.end method
