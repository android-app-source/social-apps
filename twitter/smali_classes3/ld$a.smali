.class public final Lld$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lld;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lld$a$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lld;

.field private final b:Lld$b;

.field private final c:[Z

.field private d:Z

.field private e:Z


# direct methods
.method private constructor <init>(Lld;Lld$b;)V
    .locals 1

    .prologue
    .line 723
    iput-object p1, p0, Lld$a;->a:Lld;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 724
    iput-object p2, p0, Lld$a;->b:Lld$b;

    .line 725
    invoke-static {p2}, Lld$b;->d(Lld$b;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lld$a;->c:[Z

    .line 726
    return-void

    .line 725
    :cond_0
    invoke-static {p1}, Lld;->e(Lld;)I

    move-result v0

    new-array v0, v0, [Z

    goto :goto_0
.end method

.method synthetic constructor <init>(Lld;Lld$b;Lld$1;)V
    .locals 0

    .prologue
    .line 717
    invoke-direct {p0, p1, p2}, Lld$a;-><init>(Lld;Lld$b;)V

    return-void
.end method

.method static synthetic a(Lld$a;)Lld$b;
    .locals 1

    .prologue
    .line 717
    iget-object v0, p0, Lld$a;->b:Lld$b;

    return-object v0
.end method

.method static synthetic a(Lld$a;Z)Z
    .locals 0

    .prologue
    .line 717
    iput-boolean p1, p0, Lld$a;->d:Z

    return p1
.end method

.method static synthetic b(Lld$a;)[Z
    .locals 1

    .prologue
    .line 717
    iget-object v0, p0, Lld$a;->c:[Z

    return-object v0
.end method


# virtual methods
.method public a(I)Ljava/io/OutputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 765
    iget-object v2, p0, Lld$a;->a:Lld;

    monitor-enter v2

    .line 766
    :try_start_0
    iget-object v0, p0, Lld$a;->b:Lld$b;

    invoke-static {v0}, Lld$b;->a(Lld$b;)Lld$a;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 767
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 787
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 769
    :cond_0
    :try_start_1
    iget-object v0, p0, Lld$a;->b:Lld$b;

    invoke-static {v0}, Lld$b;->d(Lld$b;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 770
    iget-object v0, p0, Lld$a;->c:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    .line 772
    :cond_1
    iget-object v0, p0, Lld$a;->b:Lld$b;

    invoke-virtual {v0, p1}, Lld$b;->b(I)Ljava/io/File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 775
    :try_start_2
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v0

    .line 786
    :goto_0
    :try_start_3
    new-instance v0, Lld$a$a;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v3}, Lld$a$a;-><init>(Lld$a;Ljava/io/OutputStream;Lld$1;)V

    monitor-exit v2

    :goto_1
    return-object v0

    .line 776
    :catch_0
    move-exception v0

    .line 778
    iget-object v0, p0, Lld$a;->a:Lld;

    invoke-static {v0}, Lld;->f(Lld;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 780
    :try_start_4
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v1, v0

    .line 784
    goto :goto_0

    .line 781
    :catch_1
    move-exception v0

    .line 783
    :try_start_5
    invoke-static {}, Lld;->b()Ljava/io/OutputStream;

    move-result-object v0

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public a()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 802
    iget-boolean v0, p0, Lld$a;->d:Z

    if-eqz v0, :cond_0

    .line 803
    iget-object v0, p0, Lld$a;->a:Lld;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lld;->a(Lld;Lld$a;Z)V

    .line 804
    iget-object v0, p0, Lld$a;->a:Lld;

    iget-object v1, p0, Lld$a;->b:Lld$b;

    invoke-static {v1}, Lld$b;->c(Lld$b;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lld;->c(Ljava/lang/String;)Z

    .line 808
    :goto_0
    iput-boolean v2, p0, Lld$a;->e:Z

    .line 809
    return-void

    .line 806
    :cond_0
    iget-object v0, p0, Lld$a;->a:Lld;

    invoke-static {v0, p0, v2}, Lld;->a(Lld;Lld$a;Z)V

    goto :goto_0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 816
    iget-object v0, p0, Lld$a;->a:Lld;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lld;->a(Lld;Lld$a;Z)V

    .line 817
    return-void
.end method
