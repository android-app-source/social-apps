.class Lwz$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lwz;->a(Lcev;)Lrx/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Lcom/twitter/util/collection/m",
        "<",
        "Lcom/twitter/model/json/moments/maker/JsonCreateMomentResponse;",
        "Lcom/twitter/library/api/moments/maker/d;",
        ">;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lwz;


# direct methods
.method constructor <init>(Lwz;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lwz$4;->a:Lwz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/collection/m;)Ljava/lang/Boolean;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/json/moments/maker/JsonCreateMomentResponse;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    .line 74
    invoke-virtual {p1}, Lcom/twitter/util/collection/m;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/moments/maker/JsonCreateMomentResponse;

    iget-object v0, v0, Lcom/twitter/model/json/moments/maker/JsonCreateMomentResponse;->a:Lcen;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 70
    check-cast p1, Lcom/twitter/util/collection/m;

    invoke-virtual {p0, p1}, Lwz$4;->a(Lcom/twitter/util/collection/m;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
