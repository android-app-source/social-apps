.class public Lcyk;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a([I)[I
    .locals 1

    .prologue
    .line 170
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-static {p0, v0}, Lcyk;->a([ILjava/nio/ByteOrder;)[I

    move-result-object v0

    return-object v0
.end method

.method public static a([ILjava/nio/ByteOrder;)[I
    .locals 5

    .prologue
    .line 174
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    if-ne p1, v0, :cond_0

    .line 175
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 176
    aget v1, p0, v0

    .line 180
    const v2, -0xff0100

    and-int/2addr v2, v1

    shl-int/lit8 v3, v1, 0x10

    const/high16 v4, 0xff0000

    and-int/2addr v3, v4

    or-int/2addr v2, v3

    shr-int/lit8 v1, v1, 0x10

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v1, v2

    aput v1, p0, v0

    .line 175
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 183
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_1

    .line 184
    aget v1, p0, v0

    .line 188
    shr-int/lit8 v2, v1, 0x8

    const v3, 0xffffff

    and-int/2addr v2, v3

    shl-int/lit8 v1, v1, 0x18

    const/high16 v3, -0x1000000

    and-int/2addr v1, v3

    or-int/2addr v1, v2

    aput v1, p0, v0

    .line 183
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 191
    :cond_1
    return-object p0
.end method
