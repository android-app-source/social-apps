.class public final Luc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Luf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Luc$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private A:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/a;",
            ">;"
        }
    .end annotation
.end field

.field private B:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/d;",
            ">;"
        }
    .end annotation
.end field

.field private C:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field

.field private D:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/android/livevideo/d;",
            ">;"
        }
    .end annotation
.end field

.field private E:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/d;",
            ">;"
        }
    .end annotation
.end field

.field private F:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private G:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lvm;",
            ">;"
        }
    .end annotation
.end field

.field private H:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lakr",
            "<",
            "Lcom/twitter/android/composer/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private I:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/model/timeline/ap;",
            ">;"
        }
    .end annotation
.end field

.field private J:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lvk;",
            ">;"
        }
    .end annotation
.end field

.field private K:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lvj;",
            ">;"
        }
    .end annotation
.end field

.field private L:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lank;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/d;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/media/selection/c;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lvo;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/player/f;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/b;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/ai;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lvq;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/provider/t;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcqt;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ltt;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lua;",
            ">;"
        }
    .end annotation
.end field

.field private x:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ltv;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lrx/f;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lvu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Luc;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Luc;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Luc$a;)V
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    sget-boolean v0, Luc;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 146
    :cond_0
    invoke-direct {p0, p1}, Luc;->a(Luc$a;)V

    .line 147
    return-void
.end method

.method synthetic constructor <init>(Luc$a;Luc$1;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Luc;-><init>(Luc$a;)V

    return-void
.end method

.method private a(Luc$a;)V
    .locals 13

    .prologue
    .line 156
    .line 158
    invoke-static {p1}, Luc$a;->a(Luc$a;)Lug;

    move-result-object v0

    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 157
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->b:Lcta;

    .line 160
    iget-object v0, p0, Luc;->b:Lcta;

    .line 162
    invoke-static {v0}, Lanv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 161
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->c:Lcta;

    .line 168
    invoke-static {p1}, Luc$a;->a(Luc$a;)Lug;

    move-result-object v0

    .line 167
    invoke-static {v0}, Laoa;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 166
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->d:Lcta;

    .line 173
    invoke-static {p1}, Luc$a;->a(Luc$a;)Lug;

    move-result-object v0

    .line 172
    invoke-static {v0}, Lup;->a(Lug;)Ldagger/internal/c;

    move-result-object v0

    .line 171
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->e:Lcta;

    .line 175
    new-instance v0, Luc$1;

    invoke-direct {v0, p0, p1}, Luc$1;-><init>(Luc;Luc$a;)V

    iput-object v0, p0, Luc;->f:Lcta;

    .line 191
    invoke-static {p1}, Luc$a;->a(Luc$a;)Lug;

    move-result-object v0

    iget-object v1, p0, Luc;->f:Lcta;

    .line 190
    invoke-static {v0, v1}, Luo;->a(Lug;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 189
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->g:Lcta;

    .line 196
    invoke-static {p1}, Luc$a;->a(Luc$a;)Lug;

    move-result-object v0

    .line 195
    invoke-static {v0}, Luk;->a(Lug;)Ldagger/internal/c;

    move-result-object v0

    .line 194
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->h:Lcta;

    .line 200
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    .line 199
    invoke-static {v0}, Lcom/twitter/android/livevideo/player/g;->a(Lcsd;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Luc;->i:Lcta;

    .line 202
    iget-object v0, p0, Luc;->b:Lcta;

    .line 204
    invoke-static {v0}, Lun;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 203
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->j:Lcta;

    .line 207
    iget-object v0, p0, Luc;->j:Lcta;

    .line 209
    invoke-static {v0}, Luv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 208
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->k:Lcta;

    .line 215
    invoke-static {p1}, Luc$a;->a(Luc$a;)Lug;

    move-result-object v0

    .line 214
    invoke-static {v0}, Luj;->a(Lug;)Ldagger/internal/c;

    move-result-object v0

    .line 213
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->l:Lcta;

    .line 217
    iget-object v0, p0, Luc;->j:Lcta;

    .line 219
    invoke-static {v0}, Luw;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 218
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->m:Lcta;

    .line 222
    iget-object v0, p0, Luc;->f:Lcta;

    iget-object v1, p0, Luc;->m:Lcta;

    .line 224
    invoke-static {v0, v1}, Luh;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 223
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->n:Lcta;

    .line 230
    invoke-static {p1}, Luc$a;->a(Luc$a;)Lug;

    move-result-object v0

    iget-object v1, p0, Luc;->j:Lcta;

    iget-object v2, p0, Luc;->m:Lcta;

    .line 229
    invoke-static {v0, v1, v2}, Lur;->a(Lug;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 228
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->o:Lcta;

    .line 235
    invoke-static {}, Lbdi;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->p:Lcta;

    .line 237
    iget-object v0, p0, Luc;->o:Lcta;

    iget-object v1, p0, Luc;->p:Lcta;

    .line 239
    invoke-static {v0, v1}, Lvr;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 238
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->q:Lcta;

    .line 242
    new-instance v0, Luc$2;

    invoke-direct {v0, p0, p1}, Luc$2;-><init>(Luc;Luc$a;)V

    iput-object v0, p0, Luc;->r:Lcta;

    .line 258
    invoke-static {p1}, Luc$a;->a(Luc$a;)Lug;

    move-result-object v0

    .line 257
    invoke-static {v0}, Lui;->a(Lug;)Ldagger/internal/c;

    move-result-object v0

    .line 256
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->s:Lcta;

    .line 260
    new-instance v0, Luc$3;

    invoke-direct {v0, p0, p1}, Luc$3;-><init>(Luc;Luc$a;)V

    iput-object v0, p0, Luc;->t:Lcta;

    .line 273
    new-instance v0, Luc$4;

    invoke-direct {v0, p0, p1}, Luc$4;-><init>(Luc;Luc$a;)V

    iput-object v0, p0, Luc;->u:Lcta;

    .line 286
    iget-object v0, p0, Luc;->t:Lcta;

    iget-object v1, p0, Luc;->u:Lcta;

    .line 288
    invoke-static {v0, v1}, Ltu;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 287
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->v:Lcta;

    .line 291
    iget-object v0, p0, Luc;->s:Lcta;

    iget-object v1, p0, Luc;->f:Lcta;

    iget-object v2, p0, Luc;->j:Lcta;

    iget-object v3, p0, Luc;->v:Lcta;

    iget-object v4, p0, Luc;->p:Lcta;

    .line 293
    invoke-static {v0, v1, v2, v3, v4}, Lus;->a(Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 292
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->w:Lcta;

    .line 300
    iget-object v0, p0, Luc;->r:Lcta;

    iget-object v1, p0, Luc;->w:Lcta;

    .line 302
    invoke-static {v0, v1}, Ltw;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 301
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->x:Lcta;

    .line 306
    invoke-static {}, Lum;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->y:Lcta;

    .line 308
    iget-object v0, p0, Luc;->q:Lcta;

    iget-object v1, p0, Luc;->x:Lcta;

    iget-object v2, p0, Luc;->y:Lcta;

    .line 310
    invoke-static {v0, v1, v2}, Lvv;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 309
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->z:Lcta;

    .line 318
    invoke-static {p1}, Luc$a;->a(Luc$a;)Lug;

    move-result-object v0

    .line 317
    invoke-static {v0}, Luq;->a(Lug;)Ldagger/internal/c;

    move-result-object v0

    .line 316
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->A:Lcta;

    .line 323
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Luc;->c:Lcta;

    iget-object v2, p0, Luc;->d:Lcta;

    iget-object v3, p0, Luc;->e:Lcta;

    iget-object v4, p0, Luc;->g:Lcta;

    iget-object v5, p0, Luc;->h:Lcta;

    iget-object v6, p0, Luc;->i:Lcta;

    iget-object v7, p0, Luc;->k:Lcta;

    iget-object v8, p0, Luc;->j:Lcta;

    iget-object v9, p0, Luc;->l:Lcta;

    iget-object v10, p0, Luc;->n:Lcta;

    iget-object v11, p0, Luc;->z:Lcta;

    iget-object v12, p0, Luc;->A:Lcta;

    .line 322
    invoke-static/range {v0 .. v12}, Lcom/twitter/android/livevideo/landing/e;->a(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 321
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->B:Lcta;

    .line 337
    iget-object v0, p0, Luc;->B:Lcta;

    .line 338
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->C:Lcta;

    .line 340
    iget-object v0, p0, Luc;->n:Lcta;

    .line 341
    invoke-static {v0}, Lcom/twitter/android/livevideo/f;->a(Lcta;)Lcsd;

    move-result-object v0

    iput-object v0, p0, Luc;->D:Lcsd;

    .line 343
    iget-object v0, p0, Luc;->D:Lcsd;

    .line 345
    invoke-static {v0}, Lcom/twitter/android/livevideo/e;->a(Lcsd;)Ldagger/internal/c;

    move-result-object v0

    .line 344
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->E:Lcta;

    .line 347
    new-instance v0, Luc$5;

    invoke-direct {v0, p0, p1}, Luc$5;-><init>(Luc;Luc$a;)V

    iput-object v0, p0, Luc;->F:Lcta;

    .line 360
    iget-object v0, p0, Luc;->B:Lcta;

    .line 362
    invoke-static {v0}, Lul;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 361
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->G:Lcta;

    .line 365
    iget-object v0, p0, Luc;->b:Lcta;

    .line 367
    invoke-static {v0}, Lut;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 366
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->H:Lcta;

    .line 370
    iget-object v0, p0, Luc;->j:Lcta;

    .line 372
    invoke-static {v0}, Luu;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 371
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->I:Lcta;

    .line 375
    iget-object v0, p0, Luc;->F:Lcta;

    iget-object v1, p0, Luc;->G:Lcta;

    iget-object v2, p0, Luc;->g:Lcta;

    iget-object v3, p0, Luc;->f:Lcta;

    iget-object v4, p0, Luc;->r:Lcta;

    iget-object v5, p0, Luc;->H:Lcta;

    iget-object v6, p0, Luc;->I:Lcta;

    iget-object v7, p0, Luc;->m:Lcta;

    .line 377
    invoke-static/range {v0 .. v7}, Lvl;->a(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 376
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->J:Lcta;

    .line 387
    iget-object v0, p0, Luc;->J:Lcta;

    .line 388
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Luc;->K:Lcta;

    .line 390
    iget-object v0, p0, Luc;->E:Lcta;

    iget-object v1, p0, Luc;->B:Lcta;

    iget-object v2, p0, Luc;->K:Lcta;

    iget-object v3, p0, Luc;->g:Lcta;

    iget-object v4, p0, Luc;->j:Lcta;

    iget-object v5, p0, Luc;->n:Lcta;

    .line 391
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/livevideo/landing/c;->a(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Lcsd;

    move-result-object v0

    iput-object v0, p0, Luc;->L:Lcsd;

    .line 398
    return-void
.end method

.method public static c()Luc$a;
    .locals 2

    .prologue
    .line 150
    new-instance v0, Luc$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Luc$a;-><init>(Luc$1;)V

    return-object v0
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Luc;->C:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public a(Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;)V
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Luc;->L:Lcsd;

    invoke-interface {v0, p1}, Lcsd;->a(Ljava/lang/Object;)V

    .line 413
    return-void
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
