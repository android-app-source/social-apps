.class public Lczj;
.super Lczl;
.source "Twttr"


# static fields
.field static final a:[I


# instance fields
.field private g:Z

.field private h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lczj;->a:[I

    return-void

    :array_0
    .array-data 4
        0x17700
        0x15888
        0xfa00
        0xbb80
        0xac44
        0x7d00
        0x5dc0
        0x5622
        0x3e80
        0x2ee0
        0x2b11
        0x1f40
        0x1cb6
    .end array-data
.end method

.method public constructor <init>(IILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    const-string/jumbo v0, "audio/mp4a-latm"

    invoke-direct {p0, p1, v0, p2, p3}, Lczl;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    .line 18
    iput-boolean v3, p0, Lczj;->g:Z

    .line 27
    invoke-static {p3}, Lczj;->a(Ljava/lang/String;)[B

    move-result-object v1

    .line 28
    aget-byte v0, v1, v4

    shr-int/lit8 v0, v0, 0x3

    and-int/lit8 v0, v0, 0x7

    .line 29
    const/4 v2, 0x7

    if-ne v0, v2, :cond_0

    .line 31
    const/16 v0, 0x8

    .line 33
    :cond_0
    aget-byte v2, v1, v3

    and-int/lit8 v2, v2, 0x7

    shl-int/lit8 v2, v2, 0x1

    aget-byte v1, v1, v4

    and-int/lit16 v1, v1, 0x80

    shr-int/lit8 v1, v1, 0x7

    or-int/2addr v2, v1

    .line 34
    const v1, 0xac44

    .line 35
    sget-object v3, Lczj;->a:[I

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 37
    sget-object v1, Lczj;->a:[I

    aget v1, v1, v2

    .line 39
    :cond_1
    iput v0, p0, Lczj;->h:I

    .line 40
    iput v1, p0, Lczj;->i:I

    .line 41
    return-void
.end method

.method public static a(Ljava/lang/String;)[B
    .locals 7

    .prologue
    const/16 v6, 0x10

    .line 45
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 46
    div-int/lit8 v0, v1, 0x2

    new-array v2, v0, [B

    .line 47
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 48
    div-int/lit8 v3, v0, 0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    add-int/lit8 v5, v0, 0x1

    .line 49
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 47
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 51
    :cond_0
    return-object v2
.end method


# virtual methods
.method public readData(IJLcom/google/android/exoplayer/MediaFormatHolder;Lcom/google/android/exoplayer/SampleHolder;)I
    .locals 10

    .prologue
    .line 61
    iget-object v0, p0, Lczj;->c:Ljava/lang/String;

    invoke-static {v0}, Lczj;->a(Ljava/lang/String;)[B

    move-result-object v8

    .line 63
    iget-boolean v0, p0, Lczj;->g:Z

    if-nez v0, :cond_0

    .line 64
    iget v0, p0, Lczj;->d:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "audio/mp4a-latm"

    iget v2, p0, Lczj;->e:I

    const/4 v3, -0x1

    const-wide/16 v4, -0x1

    iget v6, p0, Lczj;->h:I

    iget v7, p0, Lczj;->i:I

    .line 66
    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    .line 64
    invoke-static/range {v0 .. v9}, Lcom/google/android/exoplayer/MediaFormat;->createAudioFormat(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/android/exoplayer/MediaFormat;

    move-result-object v0

    iput-object v0, p4, Lcom/google/android/exoplayer/MediaFormatHolder;->format:Lcom/google/android/exoplayer/MediaFormat;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lczj;->g:Z

    .line 68
    const/4 v0, -0x4

    .line 93
    :goto_0
    return v0

    .line 72
    :cond_0
    invoke-virtual {p0}, Lczj;->a()Ltv/periscope/android/video/rtmp/h;

    move-result-object v0

    .line 73
    if-nez v0, :cond_1

    .line 93
    const/4 v0, -0x2

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/h;->e()Ltv/periscope/android/video/rtmp/f;

    move-result-object v1

    iget-object v1, v1, Ltv/periscope/android/video/rtmp/f;->a:[B

    .line 77
    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/h;->e()Ltv/periscope/android/video/rtmp/f;

    move-result-object v2

    iget v2, v2, Ltv/periscope/android/video/rtmp/f;->b:I

    .line 78
    const/4 v3, 0x1

    aget-byte v3, v1, v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 79
    add-int/lit8 v2, v2, -0x2

    .line 80
    iget-object v3, p5, Lcom/google/android/exoplayer/SampleHolder;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    if-ge v3, v2, :cond_2

    .line 81
    invoke-virtual {p5, v2}, Lcom/google/android/exoplayer/SampleHolder;->ensureSpaceForWrite(I)V

    .line 83
    :cond_2
    iget-object v3, p5, Lcom/google/android/exoplayer/SampleHolder;->data:Ljava/nio/ByteBuffer;

    const/4 v4, 0x2

    invoke-virtual {v3, v1, v4, v2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 84
    iput v2, p5, Lcom/google/android/exoplayer/SampleHolder;->size:I

    .line 85
    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/h;->d()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, p5, Lcom/google/android/exoplayer/SampleHolder;->timeUs:J

    .line 86
    const/4 v1, 0x1

    iput v1, p5, Lcom/google/android/exoplayer/SampleHolder;->flags:I

    .line 88
    iget-object v1, p0, Lczj;->f:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v1, v0}, Ltv/periscope/android/video/rtmp/Connection;->a(Ltv/periscope/android/video/rtmp/h;)V

    .line 89
    const/4 v0, -0x3

    goto :goto_0
.end method

.method public readDiscontinuity(I)J
    .locals 2

    .prologue
    .line 56
    const-wide/high16 v0, -0x8000000000000000L

    return-wide v0
.end method
