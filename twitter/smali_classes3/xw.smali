.class public final Lxw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lxv;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcnz;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/moments/data/c",
            "<",
            "Lapb;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lxw;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lxw;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcnz;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/moments/data/c",
            "<",
            "Lapb;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sget-boolean v0, Lxw;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 27
    :cond_0
    iput-object p1, p0, Lxw;->b:Lcta;

    .line 28
    sget-boolean v0, Lxw;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 29
    :cond_1
    iput-object p2, p0, Lxw;->c:Lcta;

    .line 30
    return-void
.end method

.method public static a(Lcta;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcnz;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/moments/data/c",
            "<",
            "Lapb;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            ">;>;>;)",
            "Ldagger/internal/c",
            "<",
            "Lxv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Lxw;

    invoke-direct {v0, p0, p1}, Lxw;-><init>(Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lxv;
    .locals 3

    .prologue
    .line 34
    new-instance v2, Lxv;

    iget-object v0, p0, Lxw;->b:Lcta;

    .line 35
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnz;

    iget-object v1, p0, Lxw;->c:Lcta;

    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/moments/data/c;

    invoke-direct {v2, v0, v1}, Lxv;-><init>(Lcnz;Lcom/twitter/android/moments/data/c;)V

    .line 34
    return-object v2
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lxw;->a()Lxv;

    move-result-object v0

    return-object v0
.end method
