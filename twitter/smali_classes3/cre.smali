.class public Lcre;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcre$a;
    }
.end annotation


# direct methods
.method public static a(Lrx/a;)Lrx/a;
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lrx/a;->c()Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->g()Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->c()Lrx/a;

    move-result-object v0

    .line 217
    invoke-static {}, Lcqv;->b()Lcqv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/a;->b(Lrx/b;)V

    .line 218
    return-object v0
.end method

.method public static a(Lrx/functions/a;)Lrx/a;
    .locals 1

    .prologue
    .line 226
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v0

    invoke-static {p0, v0}, Lcre;->a(Lrx/functions/a;Lrx/f;)Lrx/a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lrx/functions/a;Lrx/f;)Lrx/a;
    .locals 1

    .prologue
    .line 234
    invoke-static {p0}, Lrx/a;->a(Lrx/functions/a;)Lrx/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lrx/a;->b(Lrx/f;)Lrx/a;

    move-result-object v0

    invoke-static {v0}, Lcre;->a(Lrx/a;)Lrx/a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lrx/c;Ljava/lang/Class;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<TS;>;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 184
    new-instance v0, Lcre$2;

    invoke-direct {v0, p1}, Lcre$2;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcre$10;

    invoke-direct {v1}, Lcre$10;-><init>()V

    .line 190
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 184
    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lrx/functions/d",
            "<-",
            "Ljava/lang/Object;",
            "TT;>;"
        }
    .end annotation

    .prologue
    .line 169
    new-instance v0, Lcre$9;

    invoke-direct {v0, p0}, Lcre$9;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a()Lrx/functions/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/e",
            "<TT1;TT2;TT1;>;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Lcre$1;

    invoke-direct {v0}, Lcre$1;-><init>()V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<+TT;>;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 244
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v0

    invoke-static {p0, v0}, Lcre;->a(Ljava/util/concurrent/Callable;Lrx/f;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;Lcom/twitter/util/concurrent/d;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<+TT;>;",
            "Lcom/twitter/util/concurrent/d",
            "<-TT;>;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 265
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcre;->a(Ljava/util/concurrent/Callable;Lcom/twitter/util/concurrent/d;Lrx/f;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;Lcom/twitter/util/concurrent/d;Lrx/f;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<+TT;>;",
            "Lcom/twitter/util/concurrent/d",
            "<-TT;>;",
            "Lrx/f;",
            ")",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 275
    invoke-static {p0}, Lrx/g;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    .line 276
    invoke-virtual {v0, p2}, Lrx/g;->b(Lrx/f;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcre$3;

    invoke-direct {v1, p1}, Lcre$3;-><init>(Lcom/twitter/util/concurrent/d;)V

    .line 277
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/c$b;)Lrx/g;

    move-result-object v0

    .line 275
    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;Lrx/f;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<+TT;>;",
            "Lrx/f;",
            ")",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 255
    invoke-static {p0}, Lrx/g;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lrx/g;->b(Lrx/f;)Lrx/g;

    move-result-object v0

    invoke-static {v0}, Lcre;->a(Lrx/g;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lrx/c;Ljava/lang/Object;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c",
            "<TT;>;TT;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 142
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lrx/c;->c(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->b()Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lrx/g;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/g",
            "<TT;>;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 205
    invoke-virtual {p0}, Lrx/g;->a()Lrx/g;

    move-result-object v0

    .line 206
    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 207
    return-object v0
.end method

.method public static b()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<IT::",
            "Ljava/lang/Iterable;",
            ">()",
            "Lrx/functions/d",
            "<TIT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lcre$4;

    invoke-direct {v0}, Lcre$4;-><init>()V

    return-object v0
.end method

.method public static c()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/d",
            "<",
            "Ljava/lang/Iterable",
            "<TT;>;TT;>;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v0, Lcre$5;

    invoke-direct {v0}, Lcre$5;-><init>()V

    return-object v0
.end method

.method public static d()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/d",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Lcre$6;

    invoke-direct {v0}, Lcre$6;-><init>()V

    return-object v0
.end method

.method public static e()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/util/collection/m",
            "<TT;*>;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, Lcre$7;

    invoke-direct {v0}, Lcre$7;-><init>()V

    return-object v0
.end method

.method public static f()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    new-instance v0, Lcre$8;

    invoke-direct {v0}, Lcre$8;-><init>()V

    return-object v0
.end method

.method public static g()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<-",
            "Ljava/lang/Object;",
            "Lcqx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    sget-object v0, Lcqx;->a:Lcqx;

    invoke-static {v0}, Lcre;->a(Ljava/lang/Object;)Lrx/functions/d;

    move-result-object v0

    return-object v0
.end method
