.class public Lrq;
.super Lcom/twitter/library/api/upload/w;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/twitter/model/drafts/a;

.field private final c:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/media/model/MediaFile;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/twitter/model/core/ac;

.field private final j:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/library/api/progress/ProgressUpdatedEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;Ljava/util/LinkedHashMap;Ljava/lang/String;Lcom/twitter/util/q;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/model/drafts/a;",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/media/model/MediaFile;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/library/api/progress/ProgressUpdatedEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    const-class v0, Lrq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/upload/w;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 142
    iput-object p3, p0, Lrq;->b:Lcom/twitter/model/drafts/a;

    .line 143
    iput-object p4, p0, Lrq;->c:Ljava/util/LinkedHashMap;

    .line 144
    iput-object p5, p0, Lrq;->a:Ljava/lang/String;

    .line 145
    iput-object p6, p0, Lrq;->j:Lcom/twitter/util/q;

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "TweetPosterOperation_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p3, Lcom/twitter/model/drafts/a;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lrq;->k:Ljava/lang/String;

    .line 149
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lrq;->g(I)Lcom/twitter/library/service/s;

    .line 151
    new-instance v0, Lcom/twitter/library/service/o;

    sget v1, Lcom/twitter/library/service/o;->b:I

    sget v2, Lcom/twitter/library/service/o;->c:I

    sget-object v3, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x18

    .line 154
    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    long-to-int v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/service/o;-><init>(III)V

    .line 155
    new-instance v1, Lcom/twitter/library/service/f;

    invoke-direct {v1}, Lcom/twitter/library/service/f;-><init>()V

    new-instance v2, Lcom/twitter/library/service/l;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/twitter/library/service/l;-><init>(I)V

    .line 156
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/service/g;

    invoke-direct {v2, p1}, Lcom/twitter/library/service/g;-><init>(Landroid/content/Context;)V

    .line 157
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v1

    .line 158
    invoke-virtual {v1, v0}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v0

    .line 159
    invoke-virtual {p0, v0}, Lrq;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 160
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/model/drafts/a;Ljava/util/Map;Lcom/twitter/model/core/ac;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/model/drafts/a;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/media/model/MediaFile;",
            ">;",
            "Lcom/twitter/model/core/ac;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 327
    iget-object v1, p3, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    iget-object v1, v1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v1}, Lcom/twitter/model/core/k;->c()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 328
    iget-object v1, p3, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    iget-object v3, v1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    .line 331
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {v3}, Lcom/twitter/model/core/f;->b()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 332
    new-instance v1, Ljava/lang/Exception;

    const-string/jumbo v2, "The size of the local output media (%d) was not the same as the media returned from the request (%d) for draft ID (%d) and status ID (%d)"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    .line 336
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x1

    .line 337
    invoke-virtual {v3}, Lcom/twitter/model/core/f;->b()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-wide v6, p1, Lcom/twitter/model/drafts/a;->b:J

    .line 338
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-wide v6, p3, Lcom/twitter/model/core/ac;->a:J

    .line 339
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    .line 332
    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 342
    :cond_0
    invoke-static {p0}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v1

    .line 343
    invoke-virtual {v1}, Lcom/twitter/library/media/manager/g;->b()Lcom/twitter/library/media/manager/e;

    move-result-object v4

    move v2, v0

    .line 344
    :goto_0
    invoke-virtual {v3}, Lcom/twitter/model/core/f;->b()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 345
    invoke-virtual {v3, v2}, Lcom/twitter/model/core/f;->a(I)Lcom/twitter/model/core/d;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 348
    iget-wide v6, v0, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/media/model/MediaFile;

    .line 349
    if-eqz v1, :cond_1

    .line 357
    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->m:Ljava/lang/String;

    sget-object v5, Lcom/twitter/media/util/TweetImageVariant;->d:Lcom/twitter/media/util/TweetImageVariant;

    .line 358
    invoke-static {v0, v5}, Lcom/twitter/media/util/TweetImageVariant;->a(Ljava/lang/String;Lcom/twitter/media/util/TweetImageVariant;)Ljava/lang/String;

    move-result-object v0

    .line 361
    iget-object v1, v1, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    invoke-virtual {v4, v0, v1}, Lcom/twitter/library/media/manager/e;->a(Ljava/lang/String;Ljava/io/File;)V

    .line 344
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 365
    :cond_2
    return-void
.end method

.method private a(Lcom/twitter/library/service/u;Lcom/twitter/model/drafts/a;)V
    .locals 5

    .prologue
    .line 418
    if-eqz p2, :cond_1

    iget-object v0, p2, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "has_media"

    move-object v1, v0

    .line 420
    :goto_0
    invoke-virtual {p0}, Lrq;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/library/service/v;->c:J

    .line 421
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "app:twitter_service:tweet:create"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "retry"

    aput-object v4, v2, v3

    .line 422
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 423
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 425
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v1

    .line 426
    if-eqz v1, :cond_0

    .line 427
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    .line 428
    invoke-static {v0, v1}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/network/l;)V

    .line 429
    invoke-static {v0, v2, v1}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/model/ScribeLog;Ljava/lang/String;Lcom/twitter/network/l;)V

    .line 431
    :cond_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 432
    return-void

    .line 418
    :cond_1
    const-string/jumbo v0, "no_media"

    move-object v1, v0

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 380
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 398
    :goto_0
    return v0

    .line 383
    :cond_0
    invoke-virtual {p0}, Lrq;->L()I

    move-result v0

    .line 384
    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    move v0, v1

    .line 386
    goto :goto_0

    .line 389
    :cond_1
    invoke-virtual {p2}, Lcom/twitter/library/api/i;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    .line 390
    if-nez v0, :cond_2

    move v0, v1

    .line 391
    goto :goto_0

    .line 393
    :cond_2
    invoke-virtual {v0}, Lcom/twitter/model/core/z;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/y;

    .line 394
    iget v0, v0, Lcom/twitter/model/core/y;->b:I

    const/16 v3, 0xbb

    if-ne v0, v3, :cond_3

    .line 395
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 398
    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/twitter/model/core/ac;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lrq;->i:Lcom/twitter/model/core/ac;

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 440
    if-gez p1, :cond_0

    .line 441
    iget-object v0, p0, Lrq;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/api/progress/ProgressUpdatedEvent;->a(Ljava/lang/String;I)Lcom/twitter/library/api/progress/ProgressUpdatedEvent;

    move-result-object v0

    .line 447
    :goto_0
    iget-object v1, p0, Lrq;->j:Lcom/twitter/util/q;

    invoke-interface {v1, v0}, Lcom/twitter/util/q;->onEvent(Ljava/lang/Object;)V

    .line 448
    return-void

    .line 442
    :cond_0
    const/16 v0, 0x2710

    if-lt p1, v0, :cond_1

    .line 443
    iget-object v0, p0, Lrq;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/api/progress/ProgressUpdatedEvent;->b(Ljava/lang/String;I)Lcom/twitter/library/api/progress/ProgressUpdatedEvent;

    move-result-object v0

    goto :goto_0

    .line 445
    :cond_1
    iget-object v0, p0, Lrq;->k:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/twitter/library/api/progress/ProgressUpdatedEvent;->a(Ljava/lang/String;II)Lcom/twitter/library/api/progress/ProgressUpdatedEvent;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/service/u;)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 200
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lrq;->a(I)V

    .line 203
    const-class v0, Lcom/twitter/model/core/ac;

    .line 204
    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v1

    .line 206
    invoke-virtual {p0}, Lrq;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    invoke-virtual {p0}, Lrq;->b()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 210
    invoke-virtual {p0}, Lrq;->e()Lcom/twitter/library/network/ab$a;

    move-result-object v2

    .line 211
    const-string/jumbo v3, "ext"

    sget-object v4, Lcom/twitter/library/service/d;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const v7, 0x7fffffff

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;[Ljava/lang/String;II)I

    .line 216
    new-instance v3, Lcom/twitter/library/api/upload/y;

    iget-object v4, p0, Lrq;->p:Landroid/content/Context;

    .line 217
    invoke-virtual {p0}, Lrq;->M()Lcom/twitter/library/service/v;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/api/upload/y;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    invoke-virtual {v3, v1}, Lcom/twitter/library/api/upload/y;->a(Lcom/twitter/network/j;)Lcom/twitter/library/api/upload/y;

    move-result-object v3

    .line 219
    const-string/jumbo v4, "android_tweet_post_body_enabled"

    invoke-static {v4}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 220
    invoke-virtual {v2}, Lcom/twitter/library/network/ab$a;->c()Ljava/lang/String;

    move-result-object v2

    .line 221
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v4

    invoke-virtual {v4}, Lcpd;->b()Lcpa;

    move-result-object v4

    const-string/jumbo v5, "tweet_poster_body"

    invoke-virtual {v4, v5, v2}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    invoke-virtual {v3, v2}, Lcom/twitter/library/api/upload/y;->a(Ljava/lang/String;)Lcom/twitter/library/api/upload/y;

    .line 227
    :goto_0
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v2

    invoke-virtual {v2}, Lcpd;->b()Lcpa;

    move-result-object v2

    const-string/jumbo v4, "tweet_poster_url"

    invoke-virtual {v2, v4, v0}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    invoke-virtual {v3, v0}, Lcom/twitter/library/api/upload/y;->a(Ljava/lang/StringBuilder;)Lcom/twitter/library/api/upload/y;

    .line 230
    invoke-virtual {p0}, Lrq;->v()V

    .line 231
    invoke-virtual {v3}, Lcom/twitter/library/api/upload/y;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v3, v0, p1}, Lcom/twitter/library/api/upload/y;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;)Lcom/twitter/library/service/u;

    .line 232
    invoke-virtual {p0}, Lrq;->w()V

    .line 234
    :cond_0
    const/16 v0, 0x1d4c

    invoke-virtual {p0, v0}, Lrq;->a(I)V

    .line 236
    invoke-virtual {p0}, Lrq;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/twitter/library/service/v;

    .line 237
    iget-wide v2, v7, Lcom/twitter/library/service/v;->c:J

    .line 238
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    .line 240
    invoke-direct {p0, p1, v1}, Lrq;->a(Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)Z

    move-result v4

    .line 241
    iget-object v5, p1, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v8, "IsRetriedDuplicateTweet"

    invoke-virtual {v5, v8, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 243
    if-eqz v0, :cond_7

    .line 244
    invoke-virtual {v1}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    iput-object v0, p0, Lrq;->i:Lcom/twitter/model/core/ac;

    .line 245
    iget-object v0, p0, Lrq;->i:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_6

    .line 246
    iget-object v0, p0, Lrq;->i:Lcom/twitter/model/core/ac;

    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->a()J

    move-result-wide v0

    .line 247
    iget-object v4, p0, Lrq;->o:Landroid/os/Bundle;

    const-string/jumbo v5, "status_id"

    invoke-virtual {v4, v5, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 251
    invoke-virtual {p0}, Lrq;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 252
    new-instance v2, Lbhb;

    iget-object v3, p0, Lrq;->p:Landroid/content/Context;

    invoke-direct {v2, v3, v7, v0, v1}, Lbhb;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;J)V

    .line 253
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 304
    :cond_1
    :goto_1
    const/16 v0, 0x2710

    invoke-virtual {p0, v0}, Lrq;->a(I)V

    .line 305
    return-void

    .line 224
    :cond_2
    invoke-virtual {v2}, Lcom/twitter/library/network/ab$a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 258
    :cond_3
    iget-object v0, p0, Lrq;->p:Landroid/content/Context;

    iget-object v1, p0, Lrq;->b:Lcom/twitter/model/drafts/a;

    iget-object v4, p0, Lrq;->c:Ljava/util/LinkedHashMap;

    iget-object v5, p0, Lrq;->i:Lcom/twitter/model/core/ac;

    invoke-static {v0, v1, v4, v5}, Lrq;->a(Landroid/content/Context;Lcom/twitter/model/drafts/a;Ljava/util/Map;Lcom/twitter/model/core/ac;)V

    .line 260
    const/16 v0, 0x2134

    invoke-virtual {p0, v0}, Lrq;->a(I)V

    .line 264
    invoke-virtual {p0}, Lrq;->R()Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 265
    invoke-virtual {p0}, Lrq;->S()Laut;

    move-result-object v4

    .line 267
    iget-object v1, p0, Lrq;->i:Lcom/twitter/model/core/ac;

    iget-object v5, p0, Lrq;->b:Lcom/twitter/model/drafts/a;

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/core/ac;JLaut;Lcom/twitter/model/drafts/a;Z)V

    .line 269
    const/16 v0, 0x251c

    invoke-virtual {p0, v0}, Lrq;->a(I)V

    .line 271
    iget-object v0, p0, Lrq;->i:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->c()Z

    move-result v0

    if-nez v0, :cond_5

    .line 272
    new-instance v1, Ljava/util/HashSet;

    iget-object v0, p0, Lrq;->i:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->b()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 273
    iget-object v0, p0, Lrq;->i:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    .line 274
    iget-wide v4, v0, Lcom/twitter/model/core/q;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 276
    :cond_4
    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v0

    .line 277
    new-instance v1, Lcom/twitter/library/api/search/a;

    iget-object v2, p0, Lrq;->p:Landroid/content/Context;

    invoke-direct {v1, v2, v7, v0}, Lcom/twitter/library/api/search/a;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;[J)V

    invoke-virtual {p0, v1}, Lrq;->b(Lcom/twitter/async/service/AsyncOperation;)V

    .line 280
    :cond_5
    iget-object v0, p0, Lrq;->i:Lcom/twitter/model/core/ac;

    iget-wide v0, v0, Lcom/twitter/model/core/ac;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 282
    new-instance v0, Lbga;

    iget-object v1, p0, Lrq;->p:Landroid/content/Context;

    iget-object v2, p0, Lrq;->i:Lcom/twitter/model/core/ac;

    iget-wide v2, v2, Lcom/twitter/model/core/ac;->j:J

    invoke-direct {v0, v1, v7, v2, v3}, Lbga;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;J)V

    invoke-virtual {p0, v0}, Lrq;->b(Lcom/twitter/async/service/AsyncOperation;)V

    goto/16 :goto_1

    .line 286
    :cond_6
    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v1, "Received null status."

    invoke-direct {v0, v1}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 289
    :cond_7
    const-string/jumbo v2, "custom_errors"

    invoke-virtual {v1}, Lcom/twitter/library/api/i;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    invoke-static {v0}, Lcom/twitter/model/core/z;->a(Lcom/twitter/model/core/z;)[I

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lrq;->a(Ljava/lang/String;[I)Lcom/twitter/library/service/s;

    .line 293
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    .line 294
    if-eqz v0, :cond_8

    iget v0, v0, Lcom/twitter/network/l;->a:I

    const/16 v2, 0x190

    if-ne v0, v2, :cond_8

    .line 296
    iget-object v0, p1, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "MediaExpired"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 299
    :cond_8
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->i()Lcom/twitter/library/service/r;

    move-result-object v0

    if-nez v0, :cond_1

    .line 300
    invoke-virtual {v1}, Lcom/twitter/library/api/i;->a()Lcom/twitter/library/service/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/u;->a(Lcom/twitter/library/service/r;)V

    goto/16 :goto_1
.end method

.method public b()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lrq;->q:Lcom/twitter/library/network/ab;

    invoke-static {v0}, Lcom/twitter/library/api/upload/p;->a(Lcom/twitter/library/network/ab;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/async/service/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 407
    invoke-super {p0, p1}, Lcom/twitter/library/api/upload/w;->b(Lcom/twitter/async/service/j;)V

    .line 408
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lrq;->g(I)Lcom/twitter/library/service/s;

    .line 409
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    iget-object v1, p0, Lrq;->b:Lcom/twitter/model/drafts/a;

    invoke-direct {p0, v0, v1}, Lrq;->a(Lcom/twitter/library/service/u;Lcom/twitter/model/drafts/a;)V

    .line 410
    return-void
.end method

.method public e()Lcom/twitter/library/network/ab$a;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 174
    invoke-virtual {p0}, Lrq;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    .line 175
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v2

    .line 177
    if-eqz v1, :cond_1

    .line 178
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v3

    .line 179
    iget-object v1, v1, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/twitter/library/client/v;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 180
    if-eqz v1, :cond_0

    invoke-virtual {v2, v1}, Lbqg;->a(Lcom/twitter/library/client/Session;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 181
    invoke-virtual {v2, v1}, Lbqg;->c(Lcom/twitter/library/client/Session;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v5, v0

    .line 185
    :goto_0
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lrq;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 187
    new-instance v0, Lcom/twitter/library/network/ab$a;

    invoke-direct {v0}, Lcom/twitter/library/network/ab$a;-><init>()V

    .line 188
    iget-object v1, p0, Lrq;->q:Lcom/twitter/library/network/ab;

    iget-object v2, p0, Lrq;->b:Lcom/twitter/model/drafts/a;

    iget-object v4, p0, Lrq;->a:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/api/upload/p;->a(Lcom/twitter/library/network/ab$a;Lcom/twitter/library/network/ab;Lcom/twitter/model/drafts/a;Ljava/util/List;Ljava/lang/String;Z)V

    .line 191
    return-object v0

    :cond_1
    move v5, v0

    .line 183
    goto :goto_0
.end method
