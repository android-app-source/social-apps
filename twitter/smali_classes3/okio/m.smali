.class public final Lokio/m;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lokio/m$b;,
        Lokio/m$a;
    }
.end annotation


# instance fields
.field final a:J

.field final b:Lokio/c;

.field c:Z

.field d:Z

.field private final e:Lokio/r;

.field private final f:Lokio/s;


# direct methods
.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lokio/c;

    invoke-direct {v0}, Lokio/c;-><init>()V

    iput-object v0, p0, Lokio/m;->b:Lokio/c;

    .line 41
    new-instance v0, Lokio/m$a;

    invoke-direct {v0, p0}, Lokio/m$a;-><init>(Lokio/m;)V

    iput-object v0, p0, Lokio/m;->e:Lokio/r;

    .line 42
    new-instance v0, Lokio/m$b;

    invoke-direct {v0, p0}, Lokio/m$b;-><init>(Lokio/m;)V

    iput-object v0, p0, Lokio/m;->f:Lokio/s;

    .line 45
    const-wide/16 v0, 0x1

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "maxBufferSize < 1: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    iput-wide p1, p0, Lokio/m;->a:J

    .line 49
    return-void
.end method


# virtual methods
.method public a()Lokio/s;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lokio/m;->f:Lokio/s;

    return-object v0
.end method

.method public b()Lokio/r;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lokio/m;->e:Lokio/r;

    return-object v0
.end method
