.class Lczf$a;
.super Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lczf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lczf;


# direct methods
.method public constructor <init>(Lczf;Landroid/content/Context;Lcom/google/android/exoplayer/SampleSource;IJLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;I)V
    .locals 11

    .prologue
    .line 92
    iput-object p1, p0, Lczf$a;->a:Lczf;

    .line 93
    sget-object v4, Lcom/google/android/exoplayer/MediaCodecSelector;->DEFAULT:Lcom/google/android/exoplayer/MediaCodecSelector;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    move-wide/from16 v6, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/MediaCodecSelector;IJLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;I)V

    .line 95
    return-void
.end method


# virtual methods
.method protected dropOutputBuffer(Landroid/media/MediaCodec;I)V
    .locals 0

    .prologue
    .line 113
    invoke-super {p0, p1, p2}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;->dropOutputBuffer(Landroid/media/MediaCodec;I)V

    .line 114
    return-void
.end method

.method protected renderOutputBuffer(Landroid/media/MediaCodec;I)V
    .locals 4

    .prologue
    .line 99
    iget-object v0, p0, Lczf$a;->a:Lczf;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lczf;->a(Lczf;J)V

    .line 100
    invoke-super {p0, p1, p2}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;->renderOutputBuffer(Landroid/media/MediaCodec;I)V

    .line 101
    return-void
.end method

.method protected renderOutputBufferV21(Landroid/media/MediaCodec;IJ)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lczf$a;->a:Lczf;

    invoke-static {v0, p3, p4}, Lczf;->a(Lczf;J)V

    .line 106
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;->renderOutputBufferV21(Landroid/media/MediaCodec;IJ)V

    .line 107
    return-void
.end method

.method protected skipOutputBuffer(Landroid/media/MediaCodec;I)V
    .locals 0

    .prologue
    .line 120
    invoke-super {p0, p1, p2}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;->skipOutputBuffer(Landroid/media/MediaCodec;I)V

    .line 121
    return-void
.end method
