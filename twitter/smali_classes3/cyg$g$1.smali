.class Lcyg$g$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/graphics/b$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcyg$g;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcyg$g;


# direct methods
.method constructor <init>(Lcyg$g;)V
    .locals 0

    .prologue
    .line 908
    iput-object p1, p0, Lcyg$g$1;->a:Lcyg$g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 911
    iget-object v0, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v0, v0, Lcyg$g;->a:Lcyg;

    iget-object v1, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v1, v1, Lcyg$g;->a:Lcyg;

    iget v1, v1, Lcyg;->t:I

    iput v1, v0, Lcyg;->u:I

    .line 912
    iget-object v0, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v0, v0, Lcyg$g;->a:Lcyg;

    iget-boolean v0, v0, Lcyg;->A:Z

    if-eqz v0, :cond_0

    .line 914
    :try_start_0
    iget-object v0, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v0, v0, Lcyg$g;->a:Lcyg;

    invoke-static {v0}, Lcyg;->a(Lcyg;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 920
    :cond_0
    :goto_0
    iget-object v0, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v0, v0, Lcyg$g;->a:Lcyg;

    iget-object v0, v0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/i;->b()J

    move-result-wide v0

    .line 921
    cmp-long v2, v0, v8

    if-gtz v2, :cond_5

    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    iget v2, v2, Lcyg;->B:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_5

    .line 922
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v0

    .line 927
    :cond_1
    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    iget-wide v2, v2, Lcyg;->m:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 928
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    .line 929
    const-string/jumbo v4, "RTMP"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Video Surface: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v7, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 930
    invoke-virtual {v6, v0, v1, v7}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " elapsedRealtimeNanos: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v7, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 931
    invoke-virtual {v6, v2, v3, v7}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 929
    invoke-static {v4, v5}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    sub-long v4, v0, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/16 v6, 0x1

    cmp-long v4, v4, v6

    if-lez v4, :cond_6

    .line 934
    iget-object v4, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v4, v4, Lcyg$g;->a:Lcyg;

    sub-long/2addr v2, v0

    iput-wide v2, v4, Lcyg;->m:J

    .line 935
    const-string/jumbo v2, "RTMP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Video surface time is offset by "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v5, v5, Lcyg$g;->a:Lcyg;

    iget-wide v6, v5, Lcyg;->m:J

    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 936
    invoke-virtual {v4, v6, v7, v5}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 935
    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    const-string/jumbo v2, "RTMP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "First video pts (msecs): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v5, v5, Lcyg$g;->a:Lcyg;

    iget-wide v6, v5, Lcyg;->m:J

    sub-long v6, v0, v6

    iget-object v5, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v5, v5, Lcyg$g;->a:Lcyg;

    iget-wide v8, v5, Lcyg;->l:J

    sub-long/2addr v6, v8

    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 938
    invoke-virtual {v4, v6, v7, v5}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 937
    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    :cond_2
    :goto_1
    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    iget-wide v2, v2, Lcyg;->m:J

    add-long/2addr v0, v2

    .line 945
    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    iget-wide v2, v2, Lcyg;->l:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_4

    .line 946
    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    iget-object v2, v2, Lcyg;->j:Ltv/periscope/android/graphics/o;

    if-eqz v2, :cond_3

    .line 947
    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    iget-object v2, v2, Lcyg;->j:Ltv/periscope/android/graphics/o;

    iget-object v3, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v3, v3, Lcyg$g;->a:Lcyg;

    iget-object v3, v3, Lcyg;->h:Ltv/periscope/android/graphics/i;

    invoke-virtual {v2, v3}, Ltv/periscope/android/graphics/o;->a(Ltv/periscope/android/graphics/i;)V

    .line 948
    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    iget-boolean v2, v2, Lcyg;->p:Z

    if-eqz v2, :cond_3

    .line 949
    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    iget-boolean v2, v2, Lcyg;->x:Z

    if-eqz v2, :cond_3

    .line 950
    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    invoke-virtual {v2}, Lcyg;->k()V

    .line 951
    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcyg;->x:Z

    .line 955
    :cond_3
    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    iget-wide v2, v2, Lcyg;->l:J

    sub-long/2addr v0, v2

    .line 956
    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    iget-object v2, v2, Lcyg;->i:Ltv/periscope/android/graphics/b;

    invoke-virtual {v2, v0, v1}, Ltv/periscope/android/graphics/b;->a(J)V

    .line 957
    iget-object v0, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v0, v0, Lcyg$g;->a:Lcyg;

    iget-object v0, v0, Lcyg;->i:Ltv/periscope/android/graphics/b;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/b;->c()V

    .line 959
    :cond_4
    :goto_2
    return-void

    .line 915
    :catch_0
    move-exception v0

    .line 916
    invoke-static {v0}, Lf;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 923
    :cond_5
    cmp-long v2, v0, v8

    if-gtz v2, :cond_1

    .line 924
    iget-object v0, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v0, v0, Lcyg$g;->a:Lcyg;

    iget v1, v0, Lcyg;->B:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcyg;->B:I

    goto :goto_2

    .line 941
    :cond_6
    iget-object v2, p0, Lcyg$g$1;->a:Lcyg$g;

    iget-object v2, v2, Lcyg$g;->a:Lcyg;

    iput-wide v8, v2, Lcyg;->m:J

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 963
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Dropped frame, failed to acquire video context."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lf;->a(Ljava/lang/Throwable;)V

    .line 964
    return-void
.end method
