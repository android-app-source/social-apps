.class Lxa$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lxa;->d()Lrx/functions/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ljava/lang/Long;",
        "Lrx/c",
        "<",
        "Lcom/twitter/util/collection/m",
        "<",
        "Lcom/twitter/model/json/moments/maker/JsonDeleteMomentResponse;",
        "Lcom/twitter/library/api/moments/maker/d;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lxa;


# direct methods
.method constructor <init>(Lxa;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lxa$3;->a:Lxa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 105
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lxa$3;->a(Ljava/lang/Long;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Long;)Lrx/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/json/moments/maker/JsonDeleteMomentResponse;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lxa$3;->a:Lxa;

    invoke-static {v0}, Lxa;->c(Lxa;)Lcom/twitter/library/api/moments/maker/f;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/api/moments/maker/c;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/api/moments/maker/c;-><init>(J)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/moments/maker/f;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0
.end method
