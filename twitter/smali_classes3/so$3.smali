.class Lso$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lso;->a(Lsn;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/ui/widget/MultiTouchImageView;

.field final synthetic b:Lso;


# direct methods
.method constructor <init>(Lso;Lcom/twitter/ui/widget/MultiTouchImageView;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lso$3;->b:Lso;

    iput-object p2, p0, Lso$3;->a:Lcom/twitter/ui/widget/MultiTouchImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 5

    .prologue
    .line 170
    iget-object v0, p0, Lso$3;->b:Lso;

    iget-object v0, v0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lso$3;->b:Lso;

    iget-object v0, v0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getLeft()I

    move-result v0

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lso$3;->b:Lso;

    iget-object v0, v0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    .line 171
    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getTop()I

    move-result v0

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lso$3;->b:Lso;

    iget-object v0, v0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    .line 172
    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getRight()I

    move-result v0

    if-ne p4, v0, :cond_1

    iget-object v0, p0, Lso$3;->b:Lso;

    iget-object v0, v0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    .line 173
    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->getBottom()I

    move-result v0

    if-ne p5, v0, :cond_1

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-object v0, p0, Lso$3;->a:Lcom/twitter/ui/widget/MultiTouchImageView;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/MultiTouchImageView;->getActiveRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 177
    iget-object v1, p0, Lso$3;->b:Lso;

    iget-object v1, v1, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    .line 178
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    .line 179
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    .line 180
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-int v4, p4, v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    .line 181
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    sub-int v0, p5, v0

    .line 177
    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->setPadding(IIII)V

    .line 182
    iget-object v0, p0, Lso$3;->b:Lso;

    iget-object v0, v0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/twitter/android/media/stickers/StickerMediaView;->layout(IIII)V

    goto :goto_0
.end method
