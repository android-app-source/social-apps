.class Lcyg$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/graphics/b$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcyg;->onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/graphics/SurfaceTexture;

.field final synthetic b:Lcyg;


# direct methods
.method constructor <init>(Lcyg;Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 626
    iput-object p1, p0, Lcyg$4;->b:Lcyg;

    iput-object p2, p0, Lcyg$4;->a:Landroid/graphics/SurfaceTexture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 629
    iget-object v0, p0, Lcyg$4;->b:Lcyg;

    iget-object v1, v0, Lcyg;->z:Ljava/lang/Object;

    monitor-enter v1

    .line 630
    :try_start_0
    iget-object v0, p0, Lcyg$4;->b:Lcyg;

    iget-object v0, v0, Lcyg;->f:Lcyf$a;

    iget-object v2, p0, Lcyg$4;->a:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v2}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcyf$a;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 631
    iget-object v0, p0, Lcyg$4;->b:Lcyg;

    iget-object v0, v0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/i;->e()V

    .line 636
    :goto_0
    iget-object v0, p0, Lcyg$4;->b:Lcyg;

    iget-object v0, v0, Lcyg;->g:Ltv/periscope/android/graphics/GLRenderView;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/GLRenderView;->a()V

    .line 637
    monitor-exit v1

    .line 638
    return-void

    .line 633
    :cond_0
    iget-object v0, p0, Lcyg$4;->b:Lcyg;

    iget-object v0, v0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    iget-object v2, p0, Lcyg$4;->b:Lcyg;

    iget-object v2, v2, Lcyg;->w:Ltv/periscope/android/util/Size;

    invoke-virtual {v2}, Ltv/periscope/android/util/Size;->a()I

    move-result v2

    iget-object v3, p0, Lcyg$4;->b:Lcyg;

    iget-object v3, v3, Lcyg;->w:Ltv/periscope/android/util/Size;

    invoke-virtual {v3}, Ltv/periscope/android/util/Size;->b()I

    move-result v3

    invoke-static {v2, v3}, Ltv/periscope/android/util/Size;->a(II)Ltv/periscope/android/util/Size;

    move-result-object v2

    iget-object v3, p0, Lcyg$4;->b:Lcyg;

    iget v3, v3, Lcyg;->v:I

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/graphics/i;->a(Ltv/periscope/android/util/Size;I)V

    goto :goto_0

    .line 637
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 642
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Dropped frame, failed to acquire video context."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lf;->a(Ljava/lang/Throwable;)V

    .line 643
    return-void
.end method
