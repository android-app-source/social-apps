.class public Lcze;
.super Lczg;
.source "Twttr"


# instance fields
.field private c:Z

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(IIILczg$a;)V
    .locals 1

    .prologue
    .line 25
    const-string/jumbo v0, "audio/mp4a-latm"

    invoke-direct {p0, p1, v0, p4}, Lczg;-><init>(ILjava/lang/String;Lczg$a;)V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcze;->c:Z

    .line 26
    iput p2, p0, Lcze;->d:I

    .line 27
    iput p3, p0, Lcze;->e:I

    .line 28
    return-void
.end method


# virtual methods
.method public readData(IJLcom/google/android/exoplayer/MediaFormatHolder;Lcom/google/android/exoplayer/SampleHolder;)I
    .locals 10

    .prologue
    .line 34
    iget-boolean v0, p0, Lcze;->c:Z

    if-nez v0, :cond_0

    .line 36
    iget v0, p0, Lcze;->e:I

    iget v1, p0, Lcze;->d:I

    invoke-static {v0, v1}, Ltv/periscope/android/video/a;->a(II)[B

    move-result-object v8

    .line 38
    iget v0, p0, Lcze;->b:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "audio/mp4a-latm"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-wide/16 v4, -0x1

    iget v6, p0, Lcze;->d:I

    iget v7, p0, Lcze;->e:I

    .line 40
    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    .line 38
    invoke-static/range {v0 .. v9}, Lcom/google/android/exoplayer/MediaFormat;->createAudioFormat(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/android/exoplayer/MediaFormat;

    move-result-object v0

    iput-object v0, p4, Lcom/google/android/exoplayer/MediaFormatHolder;->format:Lcom/google/android/exoplayer/MediaFormat;

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcze;->c:Z

    .line 42
    const/4 v0, -0x4

    .line 60
    :goto_0
    return v0

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcze;->a()Lczg$b;

    move-result-object v0

    .line 46
    if-nez v0, :cond_1

    .line 48
    const/4 v0, -0x2

    goto :goto_0

    .line 51
    :cond_1
    invoke-virtual {v0}, Lczg$b;->b()[B

    move-result-object v1

    .line 52
    iget-object v2, p5, Lcom/google/android/exoplayer/SampleHolder;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    array-length v3, v1

    if-ge v2, v3, :cond_2

    .line 54
    array-length v2, v1

    invoke-virtual {p5, v2}, Lcom/google/android/exoplayer/SampleHolder;->ensureSpaceForWrite(I)V

    .line 56
    :cond_2
    iget-object v2, p5, Lcom/google/android/exoplayer/SampleHolder;->data:Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    array-length v4, v1

    invoke-virtual {v2, v1, v3, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 57
    array-length v1, v1

    iput v1, p5, Lcom/google/android/exoplayer/SampleHolder;->size:I

    .line 58
    invoke-virtual {v0}, Lczg$b;->a()D

    move-result-wide v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p5, Lcom/google/android/exoplayer/SampleHolder;->timeUs:J

    .line 59
    const/4 v0, 0x1

    iput v0, p5, Lcom/google/android/exoplayer/SampleHolder;->flags:I

    .line 60
    const/4 v0, -0x3

    goto :goto_0
.end method
