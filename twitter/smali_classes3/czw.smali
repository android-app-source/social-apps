.class public Lczw;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lczw;


# instance fields
.field public b:F

.field public c:F

.field public d:F

.field public e:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 7
    new-instance v0, Lczw;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v2, v2, v1}, Lczw;-><init>(FFFF)V

    sput-object v0, Lczw;->a:Lczw;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p1, p0, Lczw;->b:F

    .line 18
    iput p2, p0, Lczw;->c:F

    .line 19
    iput p3, p0, Lczw;->d:F

    .line 20
    iput p4, p0, Lczw;->e:F

    .line 21
    return-void
.end method

.method public constructor <init>(Lczt;F)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iget v0, p1, Lczt;->a:F

    iput v0, p0, Lczw;->b:F

    .line 32
    iget v0, p1, Lczt;->b:F

    iput v0, p0, Lczw;->c:F

    .line 33
    iget v0, p1, Lczt;->c:F

    iput v0, p0, Lczw;->d:F

    .line 34
    iput p2, p0, Lczw;->e:F

    .line 35
    return-void
.end method

.method public static a(FLczt;)Lczw;
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 164
    float-to-double v0, p0

    div-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 165
    new-instance v1, Lczw;

    iget v2, p1, Lczt;->a:F

    mul-float/2addr v2, v0

    iget v3, p1, Lczt;->b:F

    mul-float/2addr v3, v0

    iget v4, p1, Lczt;->c:F

    mul-float/2addr v0, v4

    float-to-double v4, p0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    invoke-direct {v1, v2, v3, v0, v4}, Lczw;-><init>(FFFF)V

    return-object v1
.end method

.method public static a(Lczt;Lczt;Lczt;)Lczw;
    .locals 11

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v9, 0x3e800000    # 0.25f

    const/4 v8, 0x0

    .line 141
    iget v0, p0, Lczt;->a:F

    .line 142
    iget v1, p1, Lczt;->b:F

    .line 143
    iget v2, p2, Lczt;->c:F

    .line 145
    sub-float v3, v0, v1

    sub-float/2addr v3, v2

    add-float/2addr v3, v10

    invoke-static {v8, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    mul-float/2addr v3, v9

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v3, v4

    .line 146
    neg-float v4, v0

    add-float/2addr v4, v1

    sub-float/2addr v4, v2

    add-float/2addr v4, v10

    invoke-static {v8, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    mul-float/2addr v4, v9

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    .line 147
    neg-float v5, v0

    sub-float/2addr v5, v1

    add-float/2addr v5, v2

    add-float/2addr v5, v10

    invoke-static {v8, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    mul-float/2addr v5, v9

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v5, v6

    .line 148
    add-float/2addr v0, v1

    add-float/2addr v0, v2

    add-float/2addr v0, v10

    invoke-static {v8, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    mul-float/2addr v0, v9

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 149
    iget v1, p2, Lczt;->b:F

    iget v2, p1, Lczt;->c:F

    sub-float/2addr v1, v2

    invoke-static {v3, v1}, Ljava/lang/Math;->copySign(FF)F

    move-result v1

    .line 150
    iget v2, p0, Lczt;->c:F

    iget v3, p2, Lczt;->a:F

    sub-float/2addr v2, v3

    invoke-static {v4, v2}, Ljava/lang/Math;->copySign(FF)F

    move-result v2

    .line 151
    iget v3, p1, Lczt;->a:F

    iget v4, p0, Lczt;->b:F

    sub-float/2addr v3, v4

    invoke-static {v5, v3}, Ljava/lang/Math;->copySign(FF)F

    move-result v3

    .line 152
    new-instance v4, Lczw;

    invoke-direct {v4, v1, v2, v3, v0}, Lczw;-><init>(FFFF)V

    return-object v4
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p0}, Lczw;->c(Lczw;)F

    move-result v0

    return v0
.end method

.method public a(F)Lczw;
    .locals 5

    .prologue
    .line 58
    new-instance v0, Lczw;

    iget v1, p0, Lczw;->b:F

    mul-float/2addr v1, p1

    iget v2, p0, Lczw;->c:F

    mul-float/2addr v2, p1

    iget v3, p0, Lczw;->d:F

    mul-float/2addr v3, p1

    iget v4, p0, Lczw;->e:F

    mul-float/2addr v4, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lczw;-><init>(FFFF)V

    return-object v0
.end method

.method public a(Lczw;)Lczw;
    .locals 6

    .prologue
    .line 38
    new-instance v0, Lczw;

    iget v1, p0, Lczw;->b:F

    iget v2, p1, Lczw;->b:F

    add-float/2addr v1, v2

    iget v2, p0, Lczw;->c:F

    iget v3, p1, Lczw;->c:F

    add-float/2addr v2, v3

    iget v3, p0, Lczw;->d:F

    iget v4, p1, Lczw;->d:F

    add-float/2addr v3, v4

    iget v4, p0, Lczw;->e:F

    iget v5, p1, Lczw;->e:F

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Lczw;-><init>(FFFF)V

    return-object v0
.end method

.method public b()F
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lczw;->a()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public b(Lczw;)Lczw;
    .locals 7

    .prologue
    .line 62
    new-instance v0, Lczw;

    iget v1, p0, Lczw;->e:F

    iget v2, p1, Lczw;->b:F

    mul-float/2addr v1, v2

    iget v2, p0, Lczw;->b:F

    iget v3, p1, Lczw;->e:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lczw;->c:F

    iget v3, p1, Lczw;->d:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lczw;->d:F

    iget v3, p1, Lczw;->c:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lczw;->e:F

    iget v3, p1, Lczw;->c:F

    mul-float/2addr v2, v3

    iget v3, p0, Lczw;->c:F

    iget v4, p1, Lczw;->e:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lczw;->d:F

    iget v4, p1, Lczw;->b:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lczw;->b:F

    iget v4, p1, Lczw;->d:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lczw;->e:F

    iget v4, p1, Lczw;->d:F

    mul-float/2addr v3, v4

    iget v4, p0, Lczw;->d:F

    iget v5, p1, Lczw;->e:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Lczw;->b:F

    iget v5, p1, Lczw;->c:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Lczw;->c:F

    iget v5, p1, Lczw;->b:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lczw;->e:F

    iget v5, p1, Lczw;->e:F

    mul-float/2addr v4, v5

    iget v5, p0, Lczw;->b:F

    iget v6, p1, Lczw;->b:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget v5, p0, Lczw;->c:F

    iget v6, p1, Lczw;->c:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget v5, p0, Lczw;->d:F

    iget v6, p1, Lczw;->d:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Lczw;-><init>(FFFF)V

    return-object v0
.end method

.method public c(Lczw;)F
    .locals 3

    .prologue
    .line 71
    iget v0, p0, Lczw;->b:F

    iget v1, p1, Lczw;->b:F

    mul-float/2addr v0, v1

    iget v1, p0, Lczw;->c:F

    iget v2, p1, Lczw;->c:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lczw;->d:F

    iget v2, p1, Lczw;->d:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lczw;->e:F

    iget v2, p1, Lczw;->e:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public c()Lczw;
    .locals 2

    .prologue
    .line 83
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lczw;->b()F

    move-result v1

    div-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lczw;->a(F)Lczw;

    move-result-object v0

    return-object v0
.end method

.method public d()Lczw;
    .locals 2

    .prologue
    .line 87
    iget v0, p0, Lczw;->e:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 88
    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p0, v0}, Lczw;->a(F)Lczw;

    move-result-object v0

    invoke-virtual {v0}, Lczw;->c()Lczw;

    move-result-object v0

    .line 90
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lczw;->c()Lczw;

    move-result-object v0

    goto :goto_0
.end method

.method public e()[F
    .locals 10

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    .line 99
    iget v0, p0, Lczw;->b:F

    iget v1, p0, Lczw;->b:F

    add-float/2addr v0, v1

    .line 100
    iget v1, p0, Lczw;->c:F

    iget v2, p0, Lczw;->c:F

    add-float/2addr v1, v2

    .line 101
    iget v2, p0, Lczw;->d:F

    iget v3, p0, Lczw;->d:F

    add-float/2addr v2, v3

    .line 102
    iget v3, p0, Lczw;->e:F

    iget v4, p0, Lczw;->e:F

    add-float/2addr v3, v4

    .line 104
    const/16 v4, 0x10

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v6, p0, Lczw;->c:F

    mul-float/2addr v6, v1

    sub-float v6, v9, v6

    iget v7, p0, Lczw;->d:F

    mul-float/2addr v7, v2

    sub-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lczw;->c:F

    mul-float/2addr v6, v0

    iget v7, p0, Lczw;->d:F

    mul-float/2addr v7, v3

    add-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x2

    iget v6, p0, Lczw;->d:F

    mul-float/2addr v6, v0

    iget v7, p0, Lczw;->c:F

    mul-float/2addr v7, v3

    sub-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x3

    aput v8, v4, v5

    const/4 v5, 0x4

    iget v6, p0, Lczw;->c:F

    mul-float/2addr v6, v0

    iget v7, p0, Lczw;->d:F

    mul-float/2addr v7, v3

    sub-float/2addr v6, v7

    aput v6, v4, v5

    const/4 v5, 0x5

    iget v6, p0, Lczw;->b:F

    mul-float/2addr v6, v0

    sub-float v6, v9, v6

    iget v7, p0, Lczw;->d:F

    mul-float/2addr v2, v7

    sub-float v2, v6, v2

    aput v2, v4, v5

    const/4 v2, 0x6

    iget v5, p0, Lczw;->d:F

    mul-float/2addr v5, v1

    iget v6, p0, Lczw;->b:F

    mul-float/2addr v6, v3

    add-float/2addr v5, v6

    aput v5, v4, v2

    const/4 v2, 0x7

    aput v8, v4, v2

    const/16 v2, 0x8

    iget v5, p0, Lczw;->d:F

    mul-float/2addr v5, v0

    iget v6, p0, Lczw;->c:F

    mul-float/2addr v6, v3

    add-float/2addr v5, v6

    aput v5, v4, v2

    const/16 v2, 0x9

    iget v5, p0, Lczw;->d:F

    mul-float/2addr v5, v1

    iget v6, p0, Lczw;->b:F

    mul-float/2addr v3, v6

    sub-float v3, v5, v3

    aput v3, v4, v2

    const/16 v2, 0xa

    iget v3, p0, Lczw;->b:F

    mul-float/2addr v0, v3

    sub-float v0, v9, v0

    iget v3, p0, Lczw;->c:F

    mul-float/2addr v1, v3

    sub-float/2addr v0, v1

    aput v0, v4, v2

    const/16 v0, 0xb

    aput v8, v4, v0

    const/16 v0, 0xc

    aput v8, v4, v0

    const/16 v0, 0xd

    aput v8, v4, v0

    const/16 v0, 0xe

    aput v8, v4, v0

    const/16 v0, 0xf

    aput v9, v4, v0

    return-object v4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 120
    if-ne p0, p1, :cond_1

    .line 124
    :cond_0
    :goto_0
    return v0

    .line 121
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 123
    :cond_3
    check-cast p1, Lczw;

    .line 124
    iget v2, p1, Lczw;->b:F

    iget v3, p0, Lczw;->b:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_4

    iget v2, p1, Lczw;->c:F

    iget v3, p0, Lczw;->c:F

    .line 125
    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_4

    iget v2, p1, Lczw;->d:F

    iget v3, p0, Lczw;->d:F

    .line 126
    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_4

    iget v2, p1, Lczw;->e:F

    iget v3, p0, Lczw;->e:F

    .line 127
    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 133
    iget v0, p0, Lczw;->b:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    iget v0, p0, Lczw;->b:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 134
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget v0, p0, Lczw;->c:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_2

    iget v0, p0, Lczw;->c:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 135
    mul-int/lit8 v2, v0, 0x1f

    iget v0, p0, Lczw;->d:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_3

    iget v0, p0, Lczw;->d:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 136
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lczw;->e:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget v1, p0, Lczw;->e:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 137
    return v0

    :cond_1
    move v0, v1

    .line 133
    goto :goto_0

    :cond_2
    move v0, v1

    .line 134
    goto :goto_1

    :cond_3
    move v0, v1

    .line 135
    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "float4("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lczw;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lczw;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lczw;->d:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lczw;->e:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
