.class public Ldh$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ldh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Lcg;

.field private b:Laz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laz",
            "<",
            "Lcy;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcn;

.field private final d:Landroid/content/Context;

.field private e:Z

.field private f:Laz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laz",
            "<",
            "Lcy;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ldf;

.field private h:Lcv;

.field private i:Lcom/facebook/imagepipeline/decoder/a;

.field private j:Laz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laz",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/facebook/cache/disk/b;

.field private l:Lcom/facebook/common/memory/b;

.field private m:Lcom/facebook/imagepipeline/producers/ac;

.field private n:Lcom/facebook/imagepipeline/bitmaps/e;

.field private o:Lcom/facebook/imagepipeline/memory/s;

.field private p:Lcom/facebook/imagepipeline/decoder/b;

.field private q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ldx;",
            ">;"
        }
    .end annotation
.end field

.field private r:Z

.field private s:Lcom/facebook/cache/disk/b;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 262
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldh$a;->e:Z

    .line 275
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldh$a;->r:Z

    .line 280
    invoke-static {p1}, Lax;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldh$a;->d:Landroid/content/Context;

    .line 281
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ldh$1;)V
    .locals 0

    .prologue
    .line 256
    invoke-direct {p0, p1}, Ldh$a;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Ldh$a;)Lcg;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->a:Lcg;

    return-object v0
.end method

.method static synthetic b(Ldh$a;)Laz;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->b:Laz;

    return-object v0
.end method

.method static synthetic c(Ldh$a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Ldh$a;)Lcn;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->c:Lcn;

    return-object v0
.end method

.method static synthetic e(Ldh$a;)Z
    .locals 1

    .prologue
    .line 256
    iget-boolean v0, p0, Ldh$a;->e:Z

    return v0
.end method

.method static synthetic f(Ldh$a;)Laz;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->f:Laz;

    return-object v0
.end method

.method static synthetic g(Ldh$a;)Lcv;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->h:Lcv;

    return-object v0
.end method

.method static synthetic h(Ldh$a;)Lcom/facebook/imagepipeline/decoder/a;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->i:Lcom/facebook/imagepipeline/decoder/a;

    return-object v0
.end method

.method static synthetic i(Ldh$a;)Laz;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->j:Laz;

    return-object v0
.end method

.method static synthetic j(Ldh$a;)Lcom/facebook/cache/disk/b;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->k:Lcom/facebook/cache/disk/b;

    return-object v0
.end method

.method static synthetic k(Ldh$a;)Lcom/facebook/common/memory/b;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->l:Lcom/facebook/common/memory/b;

    return-object v0
.end method

.method static synthetic l(Ldh$a;)Lcom/facebook/imagepipeline/producers/ac;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->m:Lcom/facebook/imagepipeline/producers/ac;

    return-object v0
.end method

.method static synthetic m(Ldh$a;)Lcom/facebook/imagepipeline/bitmaps/e;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->n:Lcom/facebook/imagepipeline/bitmaps/e;

    return-object v0
.end method

.method static synthetic n(Ldh$a;)Lcom/facebook/imagepipeline/memory/s;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->o:Lcom/facebook/imagepipeline/memory/s;

    return-object v0
.end method

.method static synthetic o(Ldh$a;)Lcom/facebook/imagepipeline/decoder/b;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->p:Lcom/facebook/imagepipeline/decoder/b;

    return-object v0
.end method

.method static synthetic p(Ldh$a;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->q:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic q(Ldh$a;)Z
    .locals 1

    .prologue
    .line 256
    iget-boolean v0, p0, Ldh$a;->r:Z

    return v0
.end method

.method static synthetic r(Ldh$a;)Lcom/facebook/cache/disk/b;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->s:Lcom/facebook/cache/disk/b;

    return-object v0
.end method

.method static synthetic s(Ldh$a;)Ldf;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldh$a;->g:Ldf;

    return-object v0
.end method


# virtual methods
.method public a(Laz;)Ldh$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laz",
            "<",
            "Lcy;",
            ">;)",
            "Ldh$a;"
        }
    .end annotation

    .prologue
    .line 290
    invoke-static {p1}, Lax;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laz;

    iput-object v0, p0, Ldh$a;->b:Laz;

    .line 292
    return-object p0
.end method

.method public a(Lcom/facebook/cache/disk/b;)Ldh$a;
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Ldh$a;->k:Lcom/facebook/cache/disk/b;

    .line 334
    return-object p0
.end method

.method public a(Lcom/facebook/common/memory/b;)Ldh$a;
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Ldh$a;->l:Lcom/facebook/common/memory/b;

    .line 339
    return-object p0
.end method

.method public a(Lcom/facebook/imagepipeline/producers/ac;)Ldh$a;
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Ldh$a;->m:Lcom/facebook/imagepipeline/producers/ac;

    .line 344
    return-object p0
.end method

.method public a(Ldf;)Ldh$a;
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Ldh$a;->g:Ldf;

    .line 309
    return-object p0
.end method

.method public a()Ldh;
    .locals 2

    .prologue
    .line 378
    new-instance v0, Ldh;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ldh;-><init>(Ldh$a;Ldh$1;)V

    return-object v0
.end method

.method public b(Laz;)Ldh$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laz",
            "<",
            "Lcy;",
            ">;)",
            "Ldh$a;"
        }
    .end annotation

    .prologue
    .line 302
    invoke-static {p1}, Lax;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laz;

    iput-object v0, p0, Ldh$a;->f:Laz;

    .line 304
    return-object p0
.end method
