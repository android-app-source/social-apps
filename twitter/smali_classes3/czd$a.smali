.class final Lczd$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/google/android/exoplayer/util/ManifestFetcher$ManifestCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lczd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/exoplayer/util/ManifestFetcher$ManifestCallback",
        "<",
        "Lcom/google/android/exoplayer/hls/HlsPlaylist;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lczi;

.field private final e:Lcom/google/android/exoplayer/util/ManifestFetcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/exoplayer/util/ManifestFetcher",
            "<",
            "Lcom/google/android/exoplayer/hls/HlsPlaylist;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lczi;Lcom/google/android/exoplayer/upstream/BandwidthMeter;)V
    .locals 13

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lczd$a;->a:Landroid/content/Context;

    .line 105
    iput-object p2, p0, Lczd$a;->b:Ljava/lang/String;

    .line 106
    move-object/from16 v0, p3

    iput-object v0, p0, Lczd$a;->c:Ljava/lang/String;

    .line 107
    move-object/from16 v0, p4

    iput-object v0, p0, Lczd$a;->d:Lczi;

    .line 108
    move-object/from16 v0, p5

    iput-object v0, p0, Lczd$a;->f:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    .line 109
    new-instance v9, Lcom/google/android/exoplayer/hls/HlsPlaylistParser;

    invoke-direct {v9}, Lcom/google/android/exoplayer/hls/HlsPlaylistParser;-><init>()V

    .line 110
    new-instance v10, Lcom/google/android/exoplayer/util/ManifestFetcher;

    new-instance v11, Lcom/google/android/exoplayer/upstream/DefaultUriDataSource;

    const/4 v12, 0x0

    new-instance v1, Lczb;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x1f40

    const/16 v6, 0x1f40

    const/4 v7, 0x1

    .line 113
    invoke-virtual/range {p4 .. p4}, Lczi;->a()Ldct;

    move-result-object v8

    move-object v2, p2

    invoke-direct/range {v1 .. v8}, Lczb;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer/util/Predicate;Lcom/google/android/exoplayer/upstream/TransferListener;IIZLdct;)V

    invoke-direct {v11, p1, v12, v1}, Lcom/google/android/exoplayer/upstream/DefaultUriDataSource;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer/upstream/TransferListener;Lcom/google/android/exoplayer/upstream/UriDataSource;)V

    move-object/from16 v0, p3

    invoke-direct {v10, v0, v11, v9}, Lcom/google/android/exoplayer/util/ManifestFetcher;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer/upstream/UriDataSource;Lcom/google/android/exoplayer/upstream/UriLoadable$Parser;)V

    iput-object v10, p0, Lczd$a;->e:Lcom/google/android/exoplayer/util/ManifestFetcher;

    .line 115
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lczd$a;->e:Lcom/google/android/exoplayer/util/ManifestFetcher;

    iget-object v1, p0, Lczd$a;->d:Lczi;

    invoke-virtual {v1}, Lczi;->i()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/exoplayer/util/ManifestFetcher;->singleLoad(Landroid/os/Looper;Lcom/google/android/exoplayer/util/ManifestFetcher$ManifestCallback;)V

    .line 119
    return-void
.end method

.method public a(Lcom/google/android/exoplayer/hls/HlsPlaylist;)V
    .locals 22
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 137
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lczd$a;->g:Z

    if-eqz v2, :cond_0

    .line 186
    :goto_0
    return-void

    .line 141
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lczd$a;->d:Lczi;

    invoke-virtual {v2}, Lczi;->i()Landroid/os/Handler;

    move-result-object v12

    .line 142
    new-instance v10, Lcom/google/android/exoplayer/DefaultLoadControl;

    new-instance v2, Lcom/google/android/exoplayer/upstream/DefaultAllocator;

    const/high16 v3, 0x10000

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer/upstream/DefaultAllocator;-><init>(I)V

    invoke-direct {v10, v2}, Lcom/google/android/exoplayer/DefaultLoadControl;-><init>(Lcom/google/android/exoplayer/upstream/Allocator;)V

    .line 143
    new-instance v11, Lcom/google/android/exoplayer/hls/PtsTimestampAdjusterProvider;

    invoke-direct {v11}, Lcom/google/android/exoplayer/hls/PtsTimestampAdjusterProvider;-><init>()V

    .line 146
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/exoplayer/hls/HlsMasterPlaylist;

    if-eqz v2, :cond_1

    move-object/from16 v2, p1

    .line 147
    check-cast v2, Lcom/google/android/exoplayer/hls/HlsMasterPlaylist;

    .line 149
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lczd$a;->a:Landroid/content/Context;

    iget-object v2, v2, Lcom/google/android/exoplayer/hls/HlsMasterPlaylist;->variants:Ljava/util/List;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v2, v4, v5}, Lcom/google/android/exoplayer/chunk/VideoFormatSelectorUtil;->selectVideoFormatsForDefaultDisplay(Landroid/content/Context;Ljava/util/List;[Ljava/lang/String;Z)[I
    :try_end_0
    .catch Lcom/google/android/exoplayer/MediaCodecUtil$DecoderQueryException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 155
    array-length v2, v2

    if-nez v2, :cond_1

    .line 156
    move-object/from16 v0, p0

    iget-object v2, v0, Lczd$a;->d:Lczi;

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string/jumbo v4, "No variants selected."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lczi;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 151
    :catch_0
    move-exception v2

    .line 152
    move-object/from16 v0, p0

    iget-object v3, v0, Lczd$a;->d:Lczi;

    invoke-virtual {v3, v2}, Lczi;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 161
    :cond_1
    new-instance v13, Lcom/google/android/exoplayer/upstream/DefaultUriDataSource;

    move-object/from16 v0, p0

    iget-object v14, v0, Lczd$a;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v15, v0, Lczd$a;->f:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    new-instance v2, Lczb;

    move-object/from16 v0, p0

    iget-object v3, v0, Lczd$a;->b:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lczd$a;->f:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    const/16 v6, 0x1f40

    const/16 v7, 0x1f40

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lczd$a;->d:Lczi;

    .line 164
    invoke-virtual {v9}, Lczi;->a()Ldct;

    move-result-object v9

    invoke-direct/range {v2 .. v9}, Lczb;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer/util/Predicate;Lcom/google/android/exoplayer/upstream/TransferListener;IIZLdct;)V

    invoke-direct {v13, v14, v15, v2}, Lcom/google/android/exoplayer/upstream/DefaultUriDataSource;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer/upstream/TransferListener;Lcom/google/android/exoplayer/upstream/UriDataSource;)V

    .line 165
    new-instance v2, Lcom/google/android/exoplayer/hls/HlsChunkSource;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lczd$a;->a:Landroid/content/Context;

    .line 166
    invoke-static {v4}, Lcom/google/android/exoplayer/hls/DefaultHlsTrackSelector;->newDefaultInstance(Landroid/content/Context;)Lcom/google/android/exoplayer/hls/DefaultHlsTrackSelector;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lczd$a;->f:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    move-object v4, v13

    move-object/from16 v5, p1

    move-object v8, v11

    invoke-direct/range {v2 .. v8}, Lcom/google/android/exoplayer/hls/HlsChunkSource;-><init>(ZLcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/hls/HlsPlaylist;Lcom/google/android/exoplayer/hls/HlsTrackSelector;Lcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/exoplayer/hls/PtsTimestampAdjusterProvider;)V

    .line 167
    new-instance v3, Lcom/google/android/exoplayer/hls/HlsSampleSource;

    const/high16 v6, 0xfe0000

    move-object/from16 v0, p0

    iget-object v8, v0, Lczd$a;->d:Lczi;

    const/4 v9, 0x0

    move-object v4, v2

    move-object v5, v10

    move-object v7, v12

    invoke-direct/range {v3 .. v9}, Lcom/google/android/exoplayer/hls/HlsSampleSource;-><init>(Lcom/google/android/exoplayer/hls/HlsChunkSource;Lcom/google/android/exoplayer/LoadControl;ILandroid/os/Handler;Lcom/google/android/exoplayer/hls/HlsSampleSource$EventListener;I)V

    .line 169
    new-instance v5, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;

    move-object/from16 v0, p0

    iget-object v6, v0, Lczd$a;->a:Landroid/content/Context;

    sget-object v8, Lcom/google/android/exoplayer/MediaCodecSelector;->DEFAULT:Lcom/google/android/exoplayer/MediaCodecSelector;

    const/4 v9, 0x1

    const-wide/16 v10, 0x1388

    move-object/from16 v0, p0

    iget-object v13, v0, Lczd$a;->d:Lczi;

    const/16 v14, 0x32

    move-object v7, v3

    invoke-direct/range {v5 .. v14}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/MediaCodecSelector;IJLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;I)V

    .line 172
    new-instance v13, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer;

    sget-object v15, Lcom/google/android/exoplayer/MediaCodecSelector;->DEFAULT:Lcom/google/android/exoplayer/MediaCodecSelector;

    const/16 v16, 0x0

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lczd$a;->d:Lczi;

    .line 173
    invoke-virtual {v2}, Lczi;->i()Landroid/os/Handler;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lczd$a;->d:Lczi;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lczd$a;->a:Landroid/content/Context;

    .line 174
    invoke-static {v2}, Lcom/google/android/exoplayer/audio/AudioCapabilities;->getCapabilities(Landroid/content/Context;)Lcom/google/android/exoplayer/audio/AudioCapabilities;

    move-result-object v20

    const/16 v21, 0x3

    move-object v14, v3

    invoke-direct/range {v13 .. v21}, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer;-><init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/MediaCodecSelector;Lcom/google/android/exoplayer/drm/DrmSessionManager;ZLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$EventListener;Lcom/google/android/exoplayer/audio/AudioCapabilities;I)V

    .line 175
    new-instance v2, Lcom/google/android/exoplayer/metadata/MetadataTrackRenderer;

    new-instance v4, Lcom/google/android/exoplayer/metadata/id3/Id3Parser;

    invoke-direct {v4}, Lcom/google/android/exoplayer/metadata/id3/Id3Parser;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lczd$a;->d:Lczi;

    .line 176
    invoke-virtual {v12}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v2, v3, v4, v6, v7}, Lcom/google/android/exoplayer/metadata/MetadataTrackRenderer;-><init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/metadata/MetadataParser;Lcom/google/android/exoplayer/metadata/MetadataTrackRenderer$MetadataRenderer;Landroid/os/Looper;)V

    .line 177
    new-instance v4, Lcom/google/android/exoplayer/text/eia608/Eia608TrackRenderer;

    move-object/from16 v0, p0

    iget-object v6, v0, Lczd$a;->d:Lczi;

    .line 178
    invoke-virtual {v12}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v4, v3, v6, v7}, Lcom/google/android/exoplayer/text/eia608/Eia608TrackRenderer;-><init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/text/TextRenderer;Landroid/os/Looper;)V

    .line 180
    const/4 v3, 0x4

    new-array v3, v3, [Lcom/google/android/exoplayer/TrackRenderer;

    .line 181
    const/4 v6, 0x0

    aput-object v5, v3, v6

    .line 182
    const/4 v5, 0x1

    aput-object v13, v3, v5

    .line 183
    const/4 v5, 0x3

    aput-object v2, v3, v5

    .line 184
    const/4 v2, 0x2

    aput-object v4, v3, v2

    .line 185
    move-object/from16 v0, p0

    iget-object v2, v0, Lczd$a;->d:Lczi;

    move-object/from16 v0, p0

    iget-object v4, v0, Lczd$a;->f:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    invoke-virtual {v2, v3, v4}, Lczi;->a([Lcom/google/android/exoplayer/TrackRenderer;Lcom/google/android/exoplayer/upstream/BandwidthMeter;)V

    goto/16 :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lczd$a;->g:Z

    .line 123
    return-void
.end method

.method public synthetic onSingleManifest(Ljava/lang/Object;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 91
    check-cast p1, Lcom/google/android/exoplayer/hls/HlsPlaylist;

    invoke-virtual {p0, p1}, Lczd$a;->a(Lcom/google/android/exoplayer/hls/HlsPlaylist;)V

    return-void
.end method

.method public onSingleManifestError(Ljava/io/IOException;)V
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lczd$a;->g:Z

    if-eqz v0, :cond_0

    .line 132
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Lczd$a;->d:Lczi;

    invoke-virtual {v0, p1}, Lczi;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method
