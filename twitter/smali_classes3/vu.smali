.class public Lvu;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lvu$a;
    }
.end annotation


# instance fields
.field private final a:Lvq;

.field private final b:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Lcom/twitter/model/livevideo/b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lrx/f;

.field private final d:Ltv;

.field private final e:Lcwv;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/livevideo/e;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lrx/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/d",
            "<",
            "Lcom/twitter/model/livevideo/b;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lvu$a;

.field private i:Lrx/j;


# direct methods
.method public constructor <init>(Lvq;Ltv;Lrx/f;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lvu;->b:Lrx/subjects/PublishSubject;

    .line 38
    new-instance v0, Lcwv;

    invoke-direct {v0}, Lcwv;-><init>()V

    iput-object v0, p0, Lvu;->e:Lcwv;

    .line 42
    new-instance v0, Lvu$1;

    invoke-direct {v0, p0}, Lvu$1;-><init>(Lvu;)V

    iput-object v0, p0, Lvu;->g:Lrx/d;

    .line 62
    sget-object v0, Lvu$a;->a:Lvu$a;

    iput-object v0, p0, Lvu;->h:Lvu$a;

    .line 70
    iput-object p1, p0, Lvu;->a:Lvq;

    .line 71
    iput-object p3, p0, Lvu;->c:Lrx/f;

    .line 72
    iput-object p2, p0, Lvu;->d:Ltv;

    .line 73
    return-void
.end method

.method static synthetic a(Lvu;)Lrx/subjects/PublishSubject;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lvu;->b:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method private a(Lcom/twitter/model/livevideo/b;)V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->l:Ljava/util/List;

    iget-object v1, p0, Lvu;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->l:Ljava/util/List;

    iput-object v0, p0, Lvu;->f:Ljava/util/List;

    .line 127
    iget-object v0, p0, Lvu;->a:Lvq;

    invoke-virtual {v0, p1}, Lvq;->a(Lcom/twitter/model/livevideo/b;)Ljava/util/List;

    move-result-object v0

    .line 128
    iget-object v1, p0, Lvu;->h:Lvu$a;

    invoke-interface {v1, v0}, Lvu$a;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method private a(Ltv;Lrx/f;)V
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p1}, Ltv;->a()Lrx/c;

    move-result-object v0

    .line 115
    invoke-virtual {v0, p2}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 116
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lvu;->g:Lrx/d;

    .line 117
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v0

    .line 118
    iget-object v1, p0, Lvu;->e:Lcwv;

    invoke-virtual {v1, v0}, Lcwv;->a(Lrx/j;)V

    .line 119
    return-void
.end method

.method static synthetic a(Lvu;Lcom/twitter/model/livevideo/b;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lvu;->a(Lcom/twitter/model/livevideo/b;)V

    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lvu;->i:Lrx/j;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lvu;->i:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 110
    :cond_0
    iget-object v0, p0, Lvu;->h:Lvu$a;

    invoke-interface {v0}, Lvu$a;->b()V

    .line 111
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lvu;->b:Lrx/subjects/PublishSubject;

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->by_()V

    .line 87
    iget-object v0, p0, Lvu;->e:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 88
    iget-object v0, p0, Lvu;->h:Lvu$a;

    invoke-interface {v0}, Lvu$a;->b()V

    .line 89
    return-void
.end method

.method public a(Lvu$a;)V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Lvu;->d()V

    .line 77
    iput-object p1, p0, Lvu;->h:Lvu$a;

    .line 78
    iget-object v0, p0, Lvu;->h:Lvu$a;

    invoke-interface {v0}, Lvu$a;->a()Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lvu;->g:Lrx/d;

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lvu;->i:Lrx/j;

    .line 79
    iget-object v0, p0, Lvu;->e:Lcwv;

    iget-object v1, p0, Lvu;->i:Lrx/j;

    invoke-virtual {v0, v1}, Lcwv;->a(Lrx/j;)V

    .line 80
    return-void
.end method

.method public b()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/livevideo/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lvu;->b:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lvu;->d:Ltv;

    iget-object v1, p0, Lvu;->c:Lrx/f;

    invoke-direct {p0, v0, v1}, Lvu;->a(Ltv;Lrx/f;)V

    .line 104
    return-void
.end method
