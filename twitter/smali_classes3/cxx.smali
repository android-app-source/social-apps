.class public Lcxx;
.super Ltv/periscope/android/ui/broadcast/b;
.source "Twttr"


# instance fields
.field private final d:Ltv/periscope/android/ui/broadcast/h;

.field private final e:Lcyn;


# direct methods
.method public constructor <init>(Ltv/periscope/android/view/b;Ldae;Ltv/periscope/android/ui/broadcast/h;Lcyn;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/b;-><init>(Ltv/periscope/android/view/b;Ldae;)V

    .line 35
    iput-object p3, p0, Lcxx;->d:Ltv/periscope/android/ui/broadcast/h;

    .line 36
    iput-object p4, p0, Lcxx;->e:Lcyn;

    .line 37
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ltv/periscope/model/chat/Message;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/view/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcxx;->e:Lcyn;

    invoke-interface {v0, p1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v1

    .line 43
    if-nez v1, :cond_0

    .line 44
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    .line 46
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 48
    invoke-virtual {v1}, Ltv/periscope/model/p;->L()Z

    move-result v2

    if-nez v2, :cond_1

    .line 55
    new-instance v2, Ldah;

    iget-object v3, p0, Lcxx;->d:Ltv/periscope/android/ui/broadcast/h;

    invoke-direct {v2, p1, v3}, Ldah;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    new-instance v2, Ldai;

    iget-object v3, p0, Lcxx;->d:Ltv/periscope/android/ui/broadcast/h;

    invoke-direct {v2, p1, v3}, Ldai;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_1
    invoke-virtual {v1}, Ltv/periscope/model/p;->q()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Ltv/periscope/model/p;->L()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ltv/periscope/model/p;->N()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 60
    new-instance v1, Ldav;

    iget-object v2, p0, Lcxx;->d:Ltv/periscope/android/ui/broadcast/h;

    invoke-direct {v1, p1, v2}, Ldav;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_2
    new-instance v1, Ldap;

    iget-object v2, p0, Lcxx;->d:Ltv/periscope/android/ui/broadcast/h;

    invoke-direct {v1, p1, v2}, Ldap;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
