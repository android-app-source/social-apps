.class public Ltc;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lsu;

.field private final b:Lrx/subjects/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/d",
            "<",
            "Lsx;",
            "Lsx;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lrx/subjects/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/d",
            "<",
            "Lsx;",
            "Lsx;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lrx/subjects/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/d",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lsu;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Ltc;->a:Lsu;

    .line 37
    invoke-static {}, Lrx/subjects/ReplaySubject;->r()Lrx/subjects/ReplaySubject;

    move-result-object v0

    invoke-virtual {v0}, Lrx/subjects/ReplaySubject;->t()Lrx/subjects/c;

    move-result-object v0

    iput-object v0, p0, Ltc;->b:Lrx/subjects/d;

    .line 38
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Void;

    invoke-static {v0}, Lrx/subjects/b;->e(Ljava/lang/Object;)Lrx/subjects/b;

    move-result-object v0

    invoke-virtual {v0}, Lrx/subjects/b;->t()Lrx/subjects/c;

    move-result-object v0

    iput-object v0, p0, Ltc;->d:Lrx/subjects/d;

    .line 39
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->t()Lrx/subjects/c;

    move-result-object v0

    iput-object v0, p0, Ltc;->c:Lrx/subjects/d;

    .line 40
    return-void
.end method

.method protected static a(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lsx;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lsx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 118
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    .line 119
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 120
    instance-of v1, v0, Ltg;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ltg;

    iget-boolean v1, v1, Ltg;->i:Z

    if-eqz v1, :cond_0

    .line 121
    iget-object v0, v0, Lsx;->d:Ljava/util/List;

    invoke-static {v0}, Ltc;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 124
    :cond_1
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;JZ)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcdi;",
            ">;JZ)",
            "Ljava/util/List",
            "<",
            "Lsx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v14

    .line 144
    invoke-interface/range {p0 .. p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcdi;

    .line 145
    new-instance v2, Ltg;

    iget-object v3, v9, Lcdi;->a:Ljava/lang/String;

    iget-wide v4, v9, Lcdi;->b:J

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-object v11, v9, Lcdi;->e:Ljava/lang/String;

    iget-object v10, v9, Lcdi;->d:Ljava/util/List;

    iget-wide v12, v9, Lcdi;->b:J

    .line 147
    move/from16 v0, p3

    invoke-static {v10, v12, v13, v0}, Ltc;->a(Ljava/lang/Iterable;JZ)Ljava/util/List;

    move-result-object v12

    if-nez p3, :cond_0

    iget-object v9, v9, Lcdi;->d:Ljava/util/List;

    .line 148
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    const/4 v13, 0x1

    :goto_1
    move-wide/from16 v9, p1

    invoke-direct/range {v2 .. v13}, Ltg;-><init>(Ljava/lang/String;JIZZJLjava/lang/String;Ljava/util/List;Z)V

    .line 145
    invoke-virtual {v14, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 148
    :cond_1
    const/4 v13, 0x0

    goto :goto_1

    .line 150
    :cond_2
    invoke-virtual {v14}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    return-object v2
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Ltc;->d:Lrx/subjects/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrx/subjects/d;->a(Ljava/lang/Object;)V

    .line 58
    return-void
.end method

.method public a(Lsx;)V
    .locals 4

    .prologue
    .line 46
    iget-wide v0, p1, Lsx;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Ltc;->b:Lrx/subjects/d;

    invoke-virtual {v0, p1}, Lrx/subjects/d;->a(Ljava/lang/Object;)V

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Ltc;->c:Lrx/subjects/d;

    invoke-virtual {v0, p1}, Lrx/subjects/d;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected b(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcdi;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lsx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    const-wide/16 v0, -0x1

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Ltc;->a(Ljava/lang/Iterable;JZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()Lrx/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcbi",
            "<",
            "Lsx;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Ltc;->a:Lsu;

    const/4 v1, 0x0

    .line 64
    invoke-virtual {v0, v1}, Lsu;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Ltc$1;

    invoke-direct {v1, p0}, Ltc$1;-><init>(Ltc;)V

    .line 65
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 71
    invoke-static {}, Lcbl;->f()Lcbi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 73
    iget-object v1, p0, Ltc;->c:Lrx/subjects/d;

    .line 75
    invoke-static {}, Lsw;->a()Lrx/functions/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/subjects/d;->b(Lrx/functions/d;)Lrx/c;

    move-result-object v1

    .line 76
    invoke-static {}, Lcbl;->f()Lcbi;

    move-result-object v2

    .line 77
    invoke-static {}, Lcbk;->b()Lrx/functions/e;

    move-result-object v3

    .line 76
    invoke-virtual {v1, v2, v3}, Lrx/c;->b(Ljava/lang/Object;Lrx/functions/e;)Lrx/c;

    move-result-object v1

    .line 78
    invoke-static {}, Lcbl;->f()Lcbi;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/c;->d(Ljava/lang/Object;)Lrx/c;

    move-result-object v1

    .line 80
    new-instance v2, Ltc$2;

    invoke-direct {v2, p0}, Ltc$2;-><init>(Ltc;)V

    .line 81
    invoke-static {v0, v1, v2}, Lrx/c;->a(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    .line 92
    iget-object v1, p0, Ltc;->d:Lrx/subjects/d;

    .line 94
    invoke-static {}, Lcre;->a()Lrx/functions/e;

    move-result-object v2

    .line 93
    invoke-static {v0, v1, v2}, Lrx/c;->a(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    new-instance v1, Ltc$3;

    invoke-direct {v1, p0}, Ltc$3;-><init>(Ltc;)V

    .line 95
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 103
    iget-object v1, p0, Ltc;->b:Lrx/subjects/d;

    .line 105
    invoke-static {}, Lsw;->a()Lrx/functions/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/subjects/d;->b(Lrx/functions/d;)Lrx/c;

    move-result-object v1

    .line 106
    invoke-static {}, Lcbl;->f()Lcbi;

    move-result-object v2

    .line 107
    invoke-static {}, Lcbk;->b()Lrx/functions/e;

    move-result-object v3

    .line 106
    invoke-virtual {v1, v2, v3}, Lrx/c;->b(Ljava/lang/Object;Lrx/functions/e;)Lrx/c;

    move-result-object v1

    .line 108
    invoke-static {}, Lcbl;->f()Lcbi;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/c;->d(Ljava/lang/Object;)Lrx/c;

    move-result-object v1

    .line 109
    invoke-virtual {v1}, Lrx/c;->j()Lrx/c;

    move-result-object v1

    .line 112
    invoke-static {}, Lcbk;->a()Lrx/functions/e;

    move-result-object v2

    .line 111
    invoke-static {v1, v0, v2}, Lrx/c;->a(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    return-object v0
.end method
