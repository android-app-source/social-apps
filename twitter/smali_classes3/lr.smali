.class public final Llr;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Llw;)Llj;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lls;

    invoke-direct {v0, p0}, Lls;-><init>(Llw;)V

    return-object v0
.end method

.method public static a(Llx;)Llk;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Llt;

    invoke-direct {v0, p0}, Llt;-><init>(Llx;)V

    return-object v0
.end method

.method public static a(Ljava/io/OutputStream;)Llw;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Llr$1;

    invoke-direct {v0, p0}, Llr$1;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;)Llx;
    .locals 1

    .prologue
    .line 103
    new-instance v0, Llr$2;

    invoke-direct {v0, p0}, Llr$2;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method public static a(Llq;JJLjava/io/OutputStream;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 37
    iget-wide v0, p0, Llq;->b:J

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lly;->a(JJJ)V

    .line 40
    iget-object v0, p0, Llq;->a:Llu;

    .line 41
    :goto_0
    iget v1, v0, Llu;->c:I

    iget v2, v0, Llu;->b:I

    sub-int/2addr v1, v2

    int-to-long v2, v1

    cmp-long v1, p1, v2

    if-ltz v1, :cond_0

    .line 42
    iget v1, v0, Llu;->c:I

    iget v2, v0, Llu;->b:I

    sub-int/2addr v1, v2

    int-to-long v2, v1

    sub-long/2addr p1, v2

    .line 43
    iget-object v0, v0, Llu;->d:Llu;

    goto :goto_0

    .line 47
    :cond_0
    :goto_1
    cmp-long v1, p3, v6

    if-lez v1, :cond_1

    .line 48
    iget v1, v0, Llu;->b:I

    int-to-long v2, v1

    add-long/2addr v2, p1

    long-to-int v1, v2

    .line 49
    iget v2, v0, Llu;->c:I

    sub-int/2addr v2, v1

    int-to-long v2, v2

    invoke-static {v2, v3, p3, p4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    .line 50
    iget-object v3, v0, Llu;->a:[B

    invoke-virtual {p5, v3, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 51
    int-to-long v2, v2

    sub-long/2addr p3, v2

    move-wide p1, v6

    .line 53
    goto :goto_1

    .line 54
    :cond_1
    return-void
.end method
