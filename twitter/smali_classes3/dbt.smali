.class public Ldbt;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static a:J

.field private static final f:J


# instance fields
.field private final b:Lcyw;

.field private final c:Lrx/f;

.field private final d:Lrx/f;

.field private final e:Ldbs;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 32
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ldbt;->f:J

    return-void
.end method

.method constructor <init>(Lcyw;Lrx/f;Lrx/f;Ldbs;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Ldbt;->b:Lcyw;

    .line 45
    iput-object p2, p0, Ldbt;->c:Lrx/f;

    .line 46
    iput-object p3, p0, Ldbt;->d:Lrx/f;

    .line 47
    iput-object p4, p0, Ldbt;->e:Ldbs;

    .line 48
    return-void
.end method

.method public constructor <init>(Ltv/periscope/android/library/c;)V
    .locals 6

    .prologue
    .line 37
    invoke-interface {p1}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v0

    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v2

    new-instance v3, Ldbs;

    .line 38
    invoke-interface {p1}, Ltv/periscope/android/library/c;->j()Ltv/periscope/android/session/a;

    move-result-object v4

    invoke-interface {p1}, Ltv/periscope/android/library/c;->e()Ltv/periscope/android/api/ApiService;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ldbs;-><init>(Ltv/periscope/android/session/a;Ltv/periscope/android/api/ApiService;)V

    .line 37
    invoke-direct {p0, v0, v1, v2, v3}, Ldbt;-><init>(Lcyw;Lrx/f;Lrx/f;Ldbs;)V

    .line 39
    return-void
.end method


# virtual methods
.method a()Z
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 57
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Ldbt;->f:J

    sub-long/2addr v0, v2

    sget-wide v2, Ldbt;->a:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Ldbt;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Ldbt;->a:J

    .line 67
    invoke-virtual {p0}, Ldbt;->c()Lrx/c;

    move-result-object v0

    new-instance v1, Ldbt$1;

    invoke-direct {v1, p0}, Ldbt$1;-><init>(Ldbt;)V

    .line 68
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    goto :goto_0
.end method

.method c()Lrx/c;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Ldbt;->e:Ldbs;

    .line 89
    iget-object v1, p0, Ldbt;->b:Lcyw;

    .line 90
    iget-object v2, p0, Ldbt;->b:Lcyw;

    invoke-interface {v2}, Lcyw;->c()Ljava/lang/String;

    move-result-object v2

    .line 91
    invoke-static {v2}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 93
    invoke-static {}, Lrx/c;->d()Lrx/c;

    move-result-object v0

    .line 96
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, v2}, Ldbs;->a(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    iget-object v2, p0, Ldbt;->c:Lrx/f;

    .line 97
    invoke-virtual {v0, v2}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v2, Ldbt$3;

    invoke-direct {v2, p0, v1}, Ldbt$3;-><init>(Ldbt;Lcyw;)V

    .line 98
    invoke-virtual {v0, v2}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    new-instance v1, Ldbt$2;

    invoke-direct {v1, p0}, Ldbt$2;-><init>(Ldbt;)V

    .line 104
    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Ldbt;->d:Lrx/f;

    .line 111
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method
