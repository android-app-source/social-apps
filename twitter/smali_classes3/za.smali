.class public final Lza;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lze;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lza$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private A:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxn;",
            ">;"
        }
    .end annotation
.end field

.field private B:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private C:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private D:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private E:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private F:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/support/v4/content/LocalBroadcastManager;",
            ">;"
        }
    .end annotation
.end field

.field private G:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcrc;",
            ">;"
        }
    .end annotation
.end field

.field private H:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcif;",
            ">;"
        }
    .end annotation
.end field

.field private I:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxp;",
            ">;"
        }
    .end annotation
.end field

.field private J:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/util/m;",
            ">;"
        }
    .end annotation
.end field

.field private K:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcqu;",
            ">;"
        }
    .end annotation
.end field

.field private L:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxe;",
            ">;"
        }
    .end annotation
.end field

.field private M:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxh;",
            ">;"
        }
    .end annotation
.end field

.field private N:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcii;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyf;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/schema/TwitterSchema;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lwo;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcnz;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/moments/data/c",
            "<",
            "Lapb;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxv;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/provider/t;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbsb;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/Long;",
            "Lbdk;",
            ">;>;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lwq$a;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lwq;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxr;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lawb;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxx;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyd;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxt;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laun",
            "<",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private x:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyh;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyg;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    const-class v0, Lza;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lza;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lza$a;)V
    .locals 1

    .prologue
    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    sget-boolean v0, Lza;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 182
    :cond_0
    invoke-direct {p0, p1}, Lza;->a(Lza$a;)V

    .line 183
    return-void
.end method

.method synthetic constructor <init>(Lza$a;Lza$1;)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lza;-><init>(Lza$a;)V

    return-void
.end method

.method public static a()Lza$a;
    .locals 2

    .prologue
    .line 186
    new-instance v0, Lza$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lza$a;-><init>(Lza$1;)V

    return-object v0
.end method

.method private a(Lza$a;)V
    .locals 3

    .prologue
    .line 192
    new-instance v0, Lza$1;

    invoke-direct {v0, p0, p1}, Lza$1;-><init>(Lza;Lza$a;)V

    iput-object v0, p0, Lza;->b:Lcta;

    .line 205
    new-instance v0, Lza$2;

    invoke-direct {v0, p0, p1}, Lza$2;-><init>(Lza;Lza$a;)V

    iput-object v0, p0, Lza;->c:Lcta;

    .line 218
    iget-object v0, p0, Lza;->b:Lcta;

    iget-object v1, p0, Lza;->c:Lcta;

    .line 220
    invoke-static {v0, v1}, Lyp;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 219
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->d:Lcta;

    .line 223
    iget-object v0, p0, Lza;->d:Lcta;

    .line 224
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->e:Lcta;

    .line 226
    new-instance v0, Lza$3;

    invoke-direct {v0, p0, p1}, Lza$3;-><init>(Lza;Lza$a;)V

    iput-object v0, p0, Lza;->f:Lcta;

    .line 239
    iget-object v0, p0, Lza;->f:Lcta;

    .line 241
    invoke-static {v0}, Lyl;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 240
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->g:Lcta;

    .line 244
    new-instance v0, Lza$4;

    invoke-direct {v0, p0, p1}, Lza$4;-><init>(Lza;Lza$a;)V

    iput-object v0, p0, Lza;->h:Lcta;

    .line 257
    iget-object v0, p0, Lza;->b:Lcta;

    .line 259
    invoke-static {v0}, Lyn;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 258
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->i:Lcta;

    .line 262
    iget-object v0, p0, Lza;->h:Lcta;

    iget-object v1, p0, Lza;->i:Lcta;

    .line 264
    invoke-static {v0, v1}, Lxw;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 263
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->j:Lcta;

    .line 267
    new-instance v0, Lza$5;

    invoke-direct {v0, p0, p1}, Lza$5;-><init>(Lza;Lza$a;)V

    iput-object v0, p0, Lza;->k:Lcta;

    .line 280
    iget-object v0, p0, Lza;->b:Lcta;

    iget-object v1, p0, Lza;->k:Lcta;

    iget-object v2, p0, Lza;->h:Lcta;

    .line 282
    invoke-static {v0, v1, v2}, Lbsc;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 281
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->l:Lcta;

    .line 287
    iget-object v0, p0, Lza;->b:Lcta;

    iget-object v1, p0, Lza;->c:Lcta;

    iget-object v2, p0, Lza;->l:Lcta;

    .line 289
    invoke-static {v0, v1, v2}, Lym;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 288
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->m:Lcta;

    .line 295
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lza;->m:Lcta;

    .line 294
    invoke-static {v0, v1}, Lws;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 293
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->n:Lcta;

    .line 298
    iget-object v0, p0, Lza;->n:Lcta;

    iget-object v1, p0, Lza;->j:Lcta;

    .line 300
    invoke-static {v0, v1}, Lwr;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 299
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->o:Lcta;

    .line 303
    iget-object v0, p0, Lza;->g:Lcta;

    iget-object v1, p0, Lza;->j:Lcta;

    iget-object v2, p0, Lza;->o:Lcta;

    .line 305
    invoke-static {v0, v1, v2}, Lxs;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 304
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->p:Lcta;

    .line 310
    new-instance v0, Lza$6;

    invoke-direct {v0, p0, p1}, Lza$6;-><init>(Lza;Lza$a;)V

    iput-object v0, p0, Lza;->q:Lcta;

    .line 323
    iget-object v0, p0, Lza;->q:Lcta;

    .line 325
    invoke-static {v0}, Lyv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 324
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->r:Lcta;

    .line 328
    iget-object v0, p0, Lza;->q:Lcta;

    .line 330
    invoke-static {v0}, Lyo;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 329
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->s:Lcta;

    .line 333
    iget-object v0, p0, Lza;->s:Lcta;

    .line 335
    invoke-static {v0}, Lxy;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 334
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->t:Lcta;

    .line 338
    iget-object v0, p0, Lza;->d:Lcta;

    .line 340
    invoke-static {v0}, Lye;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 339
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->u:Lcta;

    .line 342
    iget-object v0, p0, Lza;->t:Lcta;

    iget-object v1, p0, Lza;->u:Lcta;

    .line 344
    invoke-static {v0, v1}, Lxu;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 343
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->v:Lcta;

    .line 348
    iget-object v0, p0, Lza;->v:Lcta;

    .line 349
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->w:Lcta;

    .line 351
    iget-object v0, p0, Lza;->w:Lcta;

    .line 355
    invoke-static {}, Lcom/twitter/android/moments/data/u;->c()Ldagger/internal/c;

    move-result-object v1

    iget-object v2, p0, Lza;->d:Lcta;

    .line 353
    invoke-static {v0, v1, v2}, Lyi;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 352
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->x:Lcta;

    .line 358
    iget-object v0, p0, Lza;->x:Lcta;

    .line 359
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->y:Lcta;

    .line 361
    iget-object v0, p0, Lza;->y:Lcta;

    .line 363
    invoke-static {v0}, Lxl;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 362
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->z:Lcta;

    .line 365
    iget-object v0, p0, Lza;->p:Lcta;

    iget-object v1, p0, Lza;->r:Lcta;

    iget-object v2, p0, Lza;->z:Lcta;

    .line 367
    invoke-static {v0, v1, v2}, Lxo;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 366
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->A:Lcta;

    .line 372
    iget-object v0, p0, Lza;->A:Lcta;

    .line 373
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->B:Lcta;

    .line 375
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 376
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lza;->e:Lcta;

    .line 377
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lza;->B:Lcta;

    .line 378
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 379
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lza;->C:Lcta;

    .line 381
    new-instance v0, Lza$7;

    invoke-direct {v0, p0, p1}, Lza$7;-><init>(Lza;Lza$a;)V

    iput-object v0, p0, Lza;->D:Lcta;

    .line 394
    iget-object v0, p0, Lza;->D:Lcta;

    .line 396
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 395
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->E:Lcta;

    .line 398
    new-instance v0, Lza$8;

    invoke-direct {v0, p0, p1}, Lza$8;-><init>(Lza;Lza$a;)V

    iput-object v0, p0, Lza;->F:Lcta;

    .line 411
    iget-object v0, p0, Lza;->F:Lcta;

    .line 412
    invoke-static {v0}, Lcrd;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lza;->G:Lcta;

    .line 414
    iget-object v0, p0, Lza;->G:Lcta;

    .line 416
    invoke-static {v0}, Lcig;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 415
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->H:Lcta;

    .line 418
    iget-object v0, p0, Lza;->A:Lcta;

    iget-object v1, p0, Lza;->H:Lcta;

    .line 420
    invoke-static {v0, v1}, Lxq;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 419
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->I:Lcta;

    .line 425
    invoke-static {}, Lyy;->c()Ldagger/internal/c;

    move-result-object v0

    .line 424
    invoke-static {v0}, Lcom/twitter/library/util/n;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lza;->J:Lcta;

    .line 429
    invoke-static {}, Lyx;->c()Ldagger/internal/c;

    move-result-object v0

    .line 428
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->K:Lcta;

    .line 431
    iget-object v0, p0, Lza;->J:Lcta;

    .line 434
    invoke-static {}, Lxg;->c()Ldagger/internal/c;

    move-result-object v1

    iget-object v2, p0, Lza;->K:Lcta;

    .line 432
    invoke-static {v0, v1, v2}, Lxf;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lza;->L:Lcta;

    .line 437
    iget-object v0, p0, Lza;->r:Lcta;

    iget-object v1, p0, Lza;->L:Lcta;

    iget-object v2, p0, Lza;->H:Lcta;

    .line 439
    invoke-static {v0, v1, v2}, Lxi;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 438
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lza;->M:Lcta;

    .line 444
    iget-object v0, p0, Lza;->G:Lcta;

    .line 445
    invoke-static {v0}, Lcij;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lza;->N:Lcta;

    .line 446
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 450
    iget-object v0, p0, Lza;->C:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lza;->E:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lxn;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lza;->A:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxn;

    return-object v0
.end method

.method public e()Lxp;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lza;->I:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxp;

    return-object v0
.end method

.method public f()Lxh;
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lza;->M:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxh;

    return-object v0
.end method

.method public g()Lyf;
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lza;->d:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyf;

    return-object v0
.end method

.method public h()Lyg;
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lza;->y:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyg;

    return-object v0
.end method

.method public i()Lcif;
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lza;->H:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcif;

    return-object v0
.end method

.method public j()Lcii;
    .locals 3

    .prologue
    .line 490
    new-instance v1, Lcii;

    new-instance v2, Lcrc;

    iget-object v0, p0, Lza;->F:Lcta;

    .line 491
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/content/LocalBroadcastManager;

    invoke-direct {v2, v0}, Lcrc;-><init>(Landroid/support/v4/content/LocalBroadcastManager;)V

    invoke-direct {v1, v2}, Lcii;-><init>(Lcrc;)V

    .line 490
    return-object v1
.end method
