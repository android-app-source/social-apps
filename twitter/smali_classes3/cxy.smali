.class Lcxy;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/chat/h;
.implements Ltv/periscope/android/player/e;
.implements Ltv/periscope/android/ui/broadcast/ChatRoomView$a;
.implements Ltv/periscope/android/ui/chat/k;
.implements Ltv/periscope/android/ui/chat/o$a;
.implements Ltv/periscope/android/ui/chat/o$b;
.implements Ltv/periscope/android/ui/chat/o$c;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/content/res/Resources;

.field private final c:Ltv/periscope/android/ui/broadcast/v;

.field private final d:Ltv/periscope/android/library/c;

.field private final e:Lcyw;

.field private final f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

.field private final g:Ltv/periscope/android/ui/broadcast/u;

.field private final h:Lcrz;

.field private final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ltv/periscope/android/ui/chat/b;

.field private l:Ltv/periscope/android/ui/chat/g;

.field private m:Ltv/periscope/android/view/i;

.field private n:Ltv/periscope/android/view/t;

.field private o:Ljava/lang/String;

.field private p:J

.field private q:J


# direct methods
.method constructor <init>(Landroid/app/Activity;Ltv/periscope/android/library/c;Ltv/periscope/android/ui/broadcast/ChatRoomView;Ltv/periscope/android/ui/broadcast/u;Ltv/periscope/android/view/t;Lcrz;)V
    .locals 7

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcxy;->i:Ljava/util/Set;

    .line 71
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcxy;->j:Ljava/util/Set;

    .line 85
    iput-object p1, p0, Lcxy;->a:Landroid/app/Activity;

    .line 86
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcxy;->b:Landroid/content/res/Resources;

    .line 87
    iput-object p2, p0, Lcxy;->d:Ltv/periscope/android/library/c;

    .line 88
    invoke-interface {p2}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v0

    iput-object v0, p0, Lcxy;->e:Lcyw;

    .line 89
    iput-object p3, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    .line 90
    iput-object p4, p0, Lcxy;->g:Ltv/periscope/android/ui/broadcast/u;

    .line 91
    iput-object p6, p0, Lcxy;->h:Lcrz;

    .line 92
    new-instance v0, Ltv/periscope/android/ui/broadcast/v;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcxy;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    new-instance v2, Ltv/periscope/android/ui/broadcast/moderator/l;

    invoke-direct {v2}, Ltv/periscope/android/ui/broadcast/moderator/l;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/broadcast/v;-><init>(Ljava/lang/ref/WeakReference;Ltv/periscope/android/ui/broadcast/moderator/f;Ltv/periscope/android/ui/broadcast/moderator/g;ZZ)V

    iput-object v0, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    .line 94
    iget-object v0, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    new-instance v1, Ltv/periscope/android/ui/chat/c;

    invoke-direct {v1}, Ltv/periscope/android/ui/chat/c;-><init>()V

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/android/ui/chat/m;)V

    .line 95
    iget-object v0, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    iget-object v1, p0, Lcxy;->e:Lcyw;

    sget-object v2, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v3, p0

    move-object v4, p0

    invoke-virtual/range {v0 .. v6}, Ltv/periscope/android/ui/broadcast/v;->a(Lcyw;Ltv/periscope/android/player/PlayMode;Ltv/periscope/android/player/e;Ltv/periscope/android/chat/h;Ltv/periscope/android/chat/i$a;Z)V

    .line 97
    iput-object p5, p0, Lcxy;->n:Ltv/periscope/android/view/t;

    .line 98
    iget-object v0, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setFriendsWatchingListener(Ltv/periscope/android/ui/broadcast/ChatRoomView$a;)V

    .line 99
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, Lcxy;->n:Ltv/periscope/android/view/t;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcxy;->n:Ltv/periscope/android/view/t;

    new-instance v1, Ltv/periscope/android/ui/d;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Ltv/periscope/android/ui/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltv/periscope/android/view/t;->a(Ljava/lang/Object;)V

    .line 110
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 307
    return-void
.end method

.method a(Landroid/location/Location;)V
    .locals 8

    .prologue
    .line 164
    invoke-static {}, Ldaf;->b()J

    move-result-wide v0

    invoke-static {}, Ldaf;->b()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 165
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    .line 164
    invoke-static/range {v0 .. v5}, Ltv/periscope/model/chat/Message;->a(JJLjava/lang/Double;Ljava/lang/Double;)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 166
    iget-object v1, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/v;->b(Ltv/periscope/model/chat/Message;)V

    .line 167
    return-void
.end method

.method public a(Lcxc;)V
    .locals 0

    .prologue
    .line 278
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcxy;->b(Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public a(Ljava/lang/String;JZ)V
    .locals 8

    .prologue
    .line 252
    iget-object v1, p0, Lcxy;->e:Lcyw;

    iget-object v2, p0, Lcxy;->o:Ljava/lang/String;

    const/4 v6, 0x0

    move-object v3, p1

    move-wide v4, p2

    invoke-interface/range {v1 .. v6}, Lcyw;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 253
    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/Message;)V
    .locals 7

    .prologue
    .line 142
    iget-object v0, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    const-string/jumbo v4, ""

    .line 146
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 147
    invoke-virtual {p3}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v4

    .line 149
    :cond_2
    invoke-static {}, Ldaf;->b()J

    move-result-wide v0

    invoke-static {}, Ldaf;->b()J

    move-result-wide v2

    move-object v5, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Ltv/periscope/model/chat/Message;->a(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/v;->b(Ltv/periscope/model/chat/Message;)V

    .line 152
    iget-object v1, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ltv/periscope/model/chat/Message;)V

    .line 153
    iget-object v0, p0, Lcxy;->k:Ltv/periscope/android/ui/chat/b;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcxy;->k:Ltv/periscope/android/ui/chat/b;

    invoke-virtual {p3}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/b;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/chatman/api/Occupant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 243
    return-void
.end method

.method public a(Ltv/periscope/chatman/api/Sender;Z)V
    .locals 6

    .prologue
    .line 315
    iget-object v0, p1, Ltv/periscope/chatman/api/Sender;->userId:Ljava/lang/String;

    .line 316
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcxy;->i:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 317
    iget-object v1, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v2, p1, Ltv/periscope/chatman/api/Sender;->profileImageUrl:Ljava/lang/String;

    iget-object v3, p1, Ltv/periscope/chatman/api/Sender;->participantIndex:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v0, v2, v4, v5}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 318
    iget-object v1, p0, Lcxy;->i:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 319
    iget-object v1, p0, Lcxy;->j:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 320
    iget-object v0, p0, Lcxy;->h:Lcrz;

    const-string/jumbo v1, "Broadcast.NumFollowingJoined"

    iget-object v2, p0, Lcxy;->j:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcrz;->a(Ljava/lang/String;I)V

    .line 322
    :cond_0
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;)V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcxy;->l:Ltv/periscope/android/ui/chat/g;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/chat/g;->b(Ltv/periscope/model/chat/Message;)I

    move-result v0

    .line 298
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 303
    :goto_0
    return-void

    .line 301
    :cond_0
    iget-object v1, p0, Lcxy;->h:Lcrz;

    invoke-static {v1}, Ltv/periscope/android/analytics/d;->f(Lcrz;)V

    .line 302
    iget-object v1, p0, Lcxy;->g:Ltv/periscope/android/ui/broadcast/u;

    invoke-interface {v1, p1, v0}, Ltv/periscope/android/ui/broadcast/u;->a(Ltv/periscope/model/chat/Message;I)V

    goto :goto_0
.end method

.method public a(Ltv/periscope/model/chat/Message;Z)V
    .locals 5

    .prologue
    .line 213
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v0

    .line 214
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 215
    iget-object v1, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v4, p0, Lcxy;->b:Landroid/content/res/Resources;

    invoke-static {v4, v2, v3}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v2

    invoke-virtual {v1, v2, p2, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(IZLjava/lang/String;)V

    .line 216
    iget-object v1, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b(Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method a(Ltv/periscope/model/u;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 121
    iput-object p2, p0, Lcxy;->o:Ljava/lang/String;

    .line 122
    new-instance v0, Ltv/periscope/android/ui/chat/b;

    iget-object v1, p0, Lcxy;->e:Lcyw;

    iget-object v2, p0, Lcxy;->o:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/chat/b;-><init>(Lcyw;Ljava/lang/String;)V

    iput-object v0, p0, Lcxy;->k:Ltv/periscope/android/ui/chat/b;

    .line 123
    iget-object v0, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    sget-object v1, Ltv/periscope/model/StreamType;->b:Ltv/periscope/model/StreamType;

    invoke-virtual {v0, v1, p1}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/model/StreamType;Ltv/periscope/model/u;)V

    .line 124
    iget-object v0, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    iget-object v1, p0, Lcxy;->e:Lcyw;

    iget-object v2, p0, Lcxy;->d:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->h()Lcyn;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v7, p0, Lcxy;->k:Ltv/periscope/android/ui/chat/b;

    iget-object v8, p0, Lcxy;->o:Ljava/lang/String;

    move-object v4, p0

    move-object v5, p0

    move-object v6, p0

    invoke-virtual/range {v0 .. v8}, Ltv/periscope/android/ui/broadcast/v;->a(Lcyw;Lcyn;ZLtv/periscope/android/ui/chat/o$c;Ltv/periscope/android/ui/chat/o$a;Ltv/periscope/android/ui/chat/o$b;Ltv/periscope/android/ui/chat/b;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    sget-object v1, Ltv/periscope/model/StreamType;->b:Ltv/periscope/model/StreamType;

    sget-object v2, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    iget-object v3, p0, Lcxy;->d:Ltv/periscope/android/library/c;

    invoke-interface {v3}, Ltv/periscope/android/library/c;->b()Lretrofit/RestAdapter$LogLevel;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, p3}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/model/StreamType;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p1}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcxy;->q:J

    .line 129
    new-instance v0, Ltv/periscope/android/ui/chat/d;

    iget-object v1, p0, Lcxy;->b:Landroid/content/res/Resources;

    iget-object v2, p0, Lcxy;->e:Lcyw;

    invoke-interface {v2}, Lcyw;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcxy;->e:Lcyw;

    .line 130
    invoke-interface {v3}, Lcyw;->e()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v6, p0, Lcxy;->k:Ltv/periscope/android/ui/chat/b;

    iget-object v7, p0, Lcxy;->d:Ltv/periscope/android/library/c;

    invoke-interface {v7}, Ltv/periscope/android/library/c;->v()Ldae;

    move-result-object v7

    iget-object v8, p0, Lcxy;->e:Lcyw;

    move-object v9, p3

    invoke-direct/range {v0 .. v9}, Ltv/periscope/android/ui/chat/d;-><init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;ZZLtv/periscope/android/ui/chat/a;Ldae;Lcyw;Ljava/lang/String;)V

    .line 131
    new-instance v1, Ltv/periscope/android/ui/chat/g;

    iget-object v2, p0, Lcxy;->a:Landroid/app/Activity;

    new-instance v5, Ltv/periscope/android/ui/chat/t;

    invoke-direct {v5}, Ltv/periscope/android/ui/chat/t;-><init>()V

    new-instance v6, Ltv/periscope/android/ui/chat/v;

    invoke-direct {v6}, Ltv/periscope/android/ui/chat/v;-><init>()V

    move-object v3, p0

    move-object v4, v0

    invoke-direct/range {v1 .. v6}, Ltv/periscope/android/ui/chat/g;-><init>(Landroid/content/Context;Ltv/periscope/android/ui/chat/k;Ltv/periscope/android/view/ak;Ltv/periscope/android/ui/chat/y;Ltv/periscope/android/ui/chat/ak;)V

    iput-object v1, p0, Lcxy;->l:Ltv/periscope/android/ui/chat/g;

    .line 133
    iget-object v0, p0, Lcxy;->l:Ltv/periscope/android/ui/chat/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/g;->a(Z)V

    .line 134
    iget-object v0, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v1, p0, Lcxy;->l:Ltv/periscope/android/ui/chat/g;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setChatMessageAdapter(Ltv/periscope/android/ui/chat/g;)V

    .line 135
    new-instance v0, Ltv/periscope/android/view/j;

    iget-object v1, p0, Lcxy;->l:Ltv/periscope/android/ui/chat/g;

    iget-object v2, p0, Lcxy;->k:Ltv/periscope/android/ui/chat/b;

    iget-object v3, p0, Lcxy;->e:Lcyw;

    invoke-direct {v0, v1, v2, v3}, Ltv/periscope/android/view/j;-><init>(Ltv/periscope/android/ui/chat/l;Ltv/periscope/android/ui/chat/a;Lcyw;)V

    iput-object v0, p0, Lcxy;->m:Ltv/periscope/android/view/i;

    .line 136
    return-void
.end method

.method public a(Ltv/periscope/android/chat/g;)Z
    .locals 1

    .prologue
    .line 282
    const/4 v0, 0x1

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->e()V

    .line 114
    return-void
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcxy;->g:Ltv/periscope/android/ui/broadcast/u;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/u;->b(J)V

    .line 258
    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/chatman/api/Occupant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p0, Lcxy;->e:Lcyw;

    iget-object v1, p0, Lcxy;->e:Lcyw;

    invoke-interface {v1}, Lcyw;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcxy;->o:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p1}, Lcyw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 248
    return-void
.end method

.method public b(Ltv/periscope/chatman/api/Sender;Z)V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p1, Ltv/periscope/chatman/api/Sender;->userId:Ljava/lang/String;

    .line 327
    iget-object v1, p0, Lcxy;->i:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 328
    iget-object v1, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c(Ljava/lang/String;)V

    .line 330
    :cond_0
    return-void
.end method

.method public b(Ltv/periscope/model/chat/Message;)V
    .locals 0

    .prologue
    .line 311
    return-void
.end method

.method public b(Ltv/periscope/model/chat/Message;Z)V
    .locals 4

    .prologue
    .line 221
    iget-object v0, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v1, p0, Lcxy;->b:Landroid/content/res/Resources;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b(IZ)V

    .line 222
    return-void
.end method

.method c()V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->f()V

    .line 118
    return-void
.end method

.method public c(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->h()V

    .line 209
    return-void
.end method

.method public c_(J)V
    .locals 3

    .prologue
    .line 236
    iput-wide p1, p0, Lcxy;->p:J

    .line 237
    iget-object v0, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v1, p0, Lcxy;->b:Landroid/content/res/Resources;

    const/4 v2, 0x1

    invoke-static {v1, p1, p2, v2}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setParticipantCount(Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcxy;->g:Ltv/periscope/android/ui/broadcast/u;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/u;->b_(J)V

    .line 239
    return-void
.end method

.method d()V
    .locals 4

    .prologue
    .line 159
    invoke-static {}, Ldaf;->b()J

    move-result-wide v0

    invoke-static {}, Ldaf;->b()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ltv/periscope/model/chat/Message;->b(JJ)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 160
    iget-object v1, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/v;->b(Ltv/periscope/model/chat/Message;)V

    .line 161
    return-void
.end method

.method public d(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ltv/periscope/model/chat/Message;)V

    .line 227
    return-void
.end method

.method e()V
    .locals 8

    .prologue
    .line 170
    iget-object v0, p0, Lcxy;->e:Lcyw;

    invoke-interface {v0}, Lcyw;->b()Ltv/periscope/android/api/PsUser;

    move-result-object v2

    .line 171
    iget-object v0, v2, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    iget-object v1, v2, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    iget-object v2, v2, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    iget-wide v4, p0, Lcxy;->q:J

    .line 172
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {}, Ldaf;->b()J

    move-result-wide v4

    invoke-static {}, Ldaf;->b()J

    move-result-wide v6

    .line 171
    invoke-static/range {v0 .. v7}, Ltv/periscope/model/chat/Message;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JJ)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 173
    iget-object v1, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v2, p0, Lcxy;->b:Landroid/content/res/Resources;

    iget-wide v4, p0, Lcxy;->q:J

    invoke-static {v2, v4, v5}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b(IZ)V

    .line 175
    iget-object v1, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/v;->b(Ltv/periscope/model/chat/Message;)V

    .line 176
    return-void
.end method

.method public e(Ltv/periscope/model/chat/Message;)V
    .locals 0

    .prologue
    .line 268
    return-void
.end method

.method f()V
    .locals 6

    .prologue
    .line 179
    iget-object v0, p0, Lcxy;->b:Landroid/content/res/Resources;

    sget v1, Ltv/periscope/android/library/f$l;->ps__chat_prompt_follow_broadcaster:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcxy;->e:Lcyw;

    .line 180
    invoke-interface {v5}, Lcyw;->b()Ltv/periscope/android/api/PsUser;

    move-result-object v5

    iget-object v5, v5, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 181
    iget-object v0, p0, Lcxy;->e:Lcyw;

    invoke-interface {v0}, Lcyw;->c()Ljava/lang/String;

    move-result-object v0

    .line 182
    invoke-static {}, Ldaf;->b()J

    move-result-wide v2

    invoke-static {}, Ldaf;->b()J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Ltv/periscope/model/chat/Message;->a(Ljava/lang/String;Ljava/lang/String;JJ)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/v;->b(Ltv/periscope/model/chat/Message;)V

    .line 184
    iget-object v0, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/chat/MessageType;->y:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, p0, Lcxy;->b:Landroid/content/res/Resources;

    sget v3, Ltv/periscope/android/library/f$l;->ps__broadcaster_action_ask_for_follow_confirmation:I

    .line 185
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->h(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v1

    .line 184
    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ltv/periscope/model/chat/Message;)V

    .line 187
    return-void
.end method

.method g()V
    .locals 4

    .prologue
    .line 190
    invoke-static {}, Ldaf;->b()J

    move-result-wide v0

    invoke-static {}, Ldaf;->b()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ltv/periscope/model/chat/Message;->a(JJ)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 191
    iget-object v1, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/v;->b(Ltv/periscope/model/chat/Message;)V

    .line 192
    iget-object v0, p0, Lcxy;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/chat/MessageType;->y:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, p0, Lcxy;->b:Landroid/content/res/Resources;

    sget v3, Ltv/periscope/android/library/f$l;->ps__broadcaster_action_ask_for_share_confirmation:I

    .line 193
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->h(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v1

    .line 192
    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ltv/periscope/model/chat/Message;)V

    .line 194
    return-void
.end method

.method h()Ltv/periscope/android/api/ChatStats;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcxy;->c:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->r()Ltv/periscope/android/api/ChatStats;

    move-result-object v0

    return-object v0
.end method

.method i()Ltv/periscope/android/view/i;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcxy;->m:Ltv/periscope/android/view/i;

    return-object v0
.end method

.method public j()V
    .locals 0

    .prologue
    .line 232
    return-void
.end method

.method public k()J
    .locals 2

    .prologue
    .line 262
    iget-wide v0, p0, Lcxy;->p:J

    return-wide v0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 272
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public m()J
    .locals 2

    .prologue
    .line 287
    invoke-static {}, Ldaf;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public n()J
    .locals 2

    .prologue
    .line 292
    const-wide/16 v0, 0x0

    return-wide v0
.end method
