.class public Lsd;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field private final a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/composer/mediarail/view/MediaRailView;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x1

    iput v0, p0, Lsd;->b:I

    .line 25
    iput-object p1, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    .line 26
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v0}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->clearAnimation()V

    .line 123
    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 47
    iget v0, p0, Lsd;->b:I

    if-eq v0, p1, :cond_0

    .line 48
    iput p1, p0, Lsd;->b:I

    .line 49
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 50
    iget-object v0, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setAlpha(F)V

    .line 51
    iget-object v0, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setTranslationY(F)V

    .line 52
    iget-object v0, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setVisibility(I)V

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    if-nez p1, :cond_0

    .line 54
    iget-object v0, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setAlpha(F)V

    .line 55
    iget-object v0, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    iget-object v1, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setTranslationY(F)V

    .line 56
    iget-object v0, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 38
    iget v1, p0, Lsd;->b:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lsd;->b:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 62
    const/16 v0, 0xc8

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lsd;->g()V

    .line 67
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lsd;->a(I)V

    .line 68
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Lsd;->g()V

    .line 72
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lsd;->a(I)V

    .line 73
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 76
    invoke-virtual {p0}, Lsd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-direct {p0}, Lsd;->g()V

    .line 78
    const/4 v0, 0x2

    iput v0, p0, Lsd;->b:I

    .line 79
    iget-object v0, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v0}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 80
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 81
    invoke-virtual {p0}, Lsd;->b()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 83
    :cond_0
    return-void
.end method

.method public f()V
    .locals 4

    .prologue
    .line 86
    invoke-virtual {p0}, Lsd;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-direct {p0}, Lsd;->g()V

    .line 88
    const/4 v0, 0x3

    iput v0, p0, Lsd;->b:I

    .line 89
    iget-object v0, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v0}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 90
    invoke-virtual {p0}, Lsd;->b()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 92
    :cond_0
    return-void
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 103
    iget v0, p0, Lsd;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 104
    const/4 v0, 0x1

    iput v0, p0, Lsd;->b:I

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    iget v0, p0, Lsd;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 106
    iget-object v0, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setVisibility(I)V

    .line 107
    const/4 v0, 0x0

    iput v0, p0, Lsd;->b:I

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 119
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 96
    iget v0, p0, Lsd;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 97
    iget-object v0, p0, Lsd;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setVisibility(I)V

    .line 99
    :cond_0
    return-void
.end method
