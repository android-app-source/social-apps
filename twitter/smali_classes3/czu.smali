.class public Lczu;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lczu;->a:[F

    .line 18
    return-void
.end method

.method public constructor <init>(F)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lczu;->a:[F

    .line 22
    iget-object v0, p0, Lczu;->a:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 23
    iget-object v0, p0, Lczu;->a:[F

    const/4 v1, 0x3

    aput v2, v0, v1

    .line 24
    iget-object v0, p0, Lczu;->a:[F

    const/4 v1, 0x6

    aput v2, v0, v1

    .line 25
    iget-object v0, p0, Lczu;->a:[F

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 26
    iget-object v0, p0, Lczu;->a:[F

    const/4 v1, 0x4

    aput p1, v0, v1

    .line 27
    iget-object v0, p0, Lczu;->a:[F

    const/4 v1, 0x7

    aput v2, v0, v1

    .line 28
    iget-object v0, p0, Lczu;->a:[F

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 29
    iget-object v0, p0, Lczu;->a:[F

    const/4 v1, 0x5

    aput v2, v0, v1

    .line 30
    iget-object v0, p0, Lczu;->a:[F

    const/16 v1, 0x8

    aput p1, v0, v1

    .line 31
    return-void
.end method

.method public static a(Lczt;F)Lczu;
    .locals 4

    .prologue
    .line 159
    new-instance v0, Lczu;

    invoke-direct {v0}, Lczu;-><init>()V

    .line 160
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x0

    aput p1, v1, v2

    .line 161
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x4

    aput p1, v1, v2

    .line 162
    iget-object v1, v0, Lczu;->a:[F

    const/16 v2, 0x8

    aput p1, v1, v2

    .line 163
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x1

    iget v3, p0, Lczt;->c:F

    aput v3, v1, v2

    .line 164
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x3

    iget v3, p0, Lczt;->c:F

    neg-float v3, v3

    aput v3, v1, v2

    .line 165
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x2

    iget v3, p0, Lczt;->b:F

    neg-float v3, v3

    aput v3, v1, v2

    .line 166
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x6

    iget v3, p0, Lczt;->b:F

    aput v3, v1, v2

    .line 167
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x5

    iget v3, p0, Lczt;->a:F

    aput v3, v1, v2

    .line 168
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x7

    iget v3, p0, Lczt;->a:F

    neg-float v3, v3

    aput v3, v1, v2

    .line 169
    return-object v0
.end method

.method public static a(Lczw;)Lczu;
    .locals 14

    .prologue
    const/high16 v13, 0x3f800000    # 1.0f

    const/high16 v12, 0x40000000    # 2.0f

    .line 132
    new-instance v0, Lczu;

    invoke-direct {v0}, Lczu;-><init>()V

    .line 133
    iget v1, p0, Lczw;->e:F

    .line 134
    iget v2, p0, Lczw;->b:F

    .line 135
    iget v3, p0, Lczw;->c:F

    .line 136
    iget v4, p0, Lczw;->d:F

    .line 137
    mul-float v5, v12, v2

    mul-float/2addr v5, v2

    .line 138
    mul-float v6, v12, v3

    mul-float/2addr v6, v3

    .line 139
    mul-float v7, v12, v4

    mul-float/2addr v7, v4

    .line 140
    mul-float v8, v12, v2

    mul-float/2addr v8, v3

    .line 141
    mul-float v9, v12, v4

    mul-float/2addr v9, v1

    .line 142
    mul-float v10, v12, v2

    mul-float/2addr v10, v4

    .line 143
    mul-float v11, v12, v3

    mul-float/2addr v11, v1

    .line 144
    mul-float/2addr v3, v12

    mul-float/2addr v3, v4

    .line 145
    mul-float/2addr v2, v12

    mul-float/2addr v1, v2

    .line 146
    iget-object v2, v0, Lczu;->a:[F

    const/4 v4, 0x0

    sub-float v12, v13, v6

    sub-float/2addr v12, v7

    aput v12, v2, v4

    .line 147
    iget-object v2, v0, Lczu;->a:[F

    const/4 v4, 0x1

    sub-float v12, v8, v9

    aput v12, v2, v4

    .line 148
    iget-object v2, v0, Lczu;->a:[F

    const/4 v4, 0x2

    add-float v12, v10, v11

    aput v12, v2, v4

    .line 149
    iget-object v2, v0, Lczu;->a:[F

    const/4 v4, 0x3

    add-float/2addr v8, v9

    aput v8, v2, v4

    .line 150
    iget-object v2, v0, Lczu;->a:[F

    const/4 v4, 0x4

    sub-float v8, v13, v5

    sub-float v7, v8, v7

    aput v7, v2, v4

    .line 151
    iget-object v2, v0, Lczu;->a:[F

    const/4 v4, 0x5

    sub-float v7, v3, v1

    aput v7, v2, v4

    .line 152
    iget-object v2, v0, Lczu;->a:[F

    const/4 v4, 0x6

    sub-float v7, v10, v11

    aput v7, v2, v4

    .line 153
    iget-object v2, v0, Lczu;->a:[F

    const/4 v4, 0x7

    add-float/2addr v1, v3

    aput v1, v2, v4

    .line 154
    iget-object v1, v0, Lczu;->a:[F

    const/16 v2, 0x8

    sub-float v3, v13, v5

    sub-float/2addr v3, v6

    aput v3, v1, v2

    .line 155
    return-object v0
.end method


# virtual methods
.method public a(Lczt;)Lczt;
    .locals 6

    .prologue
    .line 48
    iget v0, p1, Lczt;->a:F

    iget-object v1, p0, Lczu;->a:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    mul-float/2addr v0, v1

    iget v1, p1, Lczt;->b:F

    iget-object v2, p0, Lczu;->a:[F

    const/4 v3, 0x3

    aget v2, v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p1, Lczt;->c:F

    iget-object v2, p0, Lczu;->a:[F

    const/4 v3, 0x6

    aget v2, v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 49
    iget v1, p1, Lczt;->a:F

    iget-object v2, p0, Lczu;->a:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    mul-float/2addr v1, v2

    iget v2, p1, Lczt;->b:F

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x4

    aget v3, v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p1, Lczt;->c:F

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x7

    aget v3, v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 50
    iget v2, p1, Lczt;->a:F

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    mul-float/2addr v2, v3

    iget v3, p1, Lczt;->b:F

    iget-object v4, p0, Lczu;->a:[F

    const/4 v5, 0x5

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p1, Lczt;->c:F

    iget-object v4, p0, Lczu;->a:[F

    const/16 v5, 0x8

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 51
    new-instance v3, Lczt;

    invoke-direct {v3, v0, v1, v2}, Lczt;-><init>(FFF)V

    return-object v3
.end method

.method public a()Lczu;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 79
    new-instance v0, Lczu;

    invoke-direct {v0}, Lczu;-><init>()V

    .line 80
    iget-object v1, v0, Lczu;->a:[F

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v3

    aput v2, v1, v3

    .line 81
    iget-object v1, v0, Lczu;->a:[F

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v4

    aput v2, v1, v6

    .line 82
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x6

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v5

    aput v3, v1, v2

    .line 83
    iget-object v1, v0, Lczu;->a:[F

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v6

    aput v2, v1, v4

    .line 84
    iget-object v1, v0, Lczu;->a:[F

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v7

    aput v2, v1, v7

    .line 85
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x7

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x5

    aget v3, v3, v4

    aput v3, v1, v2

    .line 86
    iget-object v1, v0, Lczu;->a:[F

    iget-object v2, p0, Lczu;->a:[F

    const/4 v3, 0x6

    aget v2, v2, v3

    aput v2, v1, v5

    .line 87
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x5

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x7

    aget v3, v3, v4

    aput v3, v1, v2

    .line 88
    iget-object v1, v0, Lczu;->a:[F

    const/16 v2, 0x8

    iget-object v3, p0, Lczu;->a:[F

    const/16 v4, 0x8

    aget v3, v3, v4

    aput v3, v1, v2

    .line 89
    return-object v0
.end method

.method public a(F)Lczu;
    .locals 4

    .prologue
    .line 55
    new-instance v1, Lczu;

    invoke-direct {v1}, Lczu;-><init>()V

    .line 56
    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x9

    if-ge v0, v2, :cond_0

    .line 57
    iget-object v2, v1, Lczu;->a:[F

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v0

    mul-float/2addr v3, p1

    aput v3, v2, v0

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_0
    return-object v1
.end method

.method public a(Lczu;)Lczu;
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 34
    new-instance v0, Lczu;

    invoke-direct {v0}, Lczu;-><init>()V

    .line 35
    iget-object v1, v0, Lczu;->a:[F

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v7

    iget-object v3, p1, Lczu;->a:[F

    aget v3, v3, v7

    mul-float/2addr v2, v3

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v10

    iget-object v4, p1, Lczu;->a:[F

    aget v4, v4, v8

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x6

    aget v3, v3, v4

    iget-object v4, p1, Lczu;->a:[F

    aget v4, v4, v9

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v1, v7

    .line 36
    iget-object v1, v0, Lczu;->a:[F

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v8

    iget-object v3, p1, Lczu;->a:[F

    aget v3, v3, v7

    mul-float/2addr v2, v3

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v11

    iget-object v4, p1, Lczu;->a:[F

    aget v4, v4, v8

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x7

    aget v3, v3, v4

    iget-object v4, p1, Lczu;->a:[F

    aget v4, v4, v9

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v1, v8

    .line 37
    iget-object v1, v0, Lczu;->a:[F

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v9

    iget-object v3, p1, Lczu;->a:[F

    aget v3, v3, v7

    mul-float/2addr v2, v3

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x5

    aget v3, v3, v4

    iget-object v4, p1, Lczu;->a:[F

    aget v4, v4, v8

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lczu;->a:[F

    const/16 v4, 0x8

    aget v3, v3, v4

    iget-object v4, p1, Lczu;->a:[F

    aget v4, v4, v9

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v1, v9

    .line 38
    iget-object v1, v0, Lczu;->a:[F

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v7

    iget-object v3, p1, Lczu;->a:[F

    aget v3, v3, v10

    mul-float/2addr v2, v3

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v10

    iget-object v4, p1, Lczu;->a:[F

    aget v4, v4, v11

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x6

    aget v3, v3, v4

    iget-object v4, p1, Lczu;->a:[F

    const/4 v5, 0x5

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v1, v10

    .line 39
    iget-object v1, v0, Lczu;->a:[F

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v8

    iget-object v3, p1, Lczu;->a:[F

    aget v3, v3, v10

    mul-float/2addr v2, v3

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v11

    iget-object v4, p1, Lczu;->a:[F

    aget v4, v4, v11

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x7

    aget v3, v3, v4

    iget-object v4, p1, Lczu;->a:[F

    const/4 v5, 0x5

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v1, v11

    .line 40
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x5

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v9

    iget-object v4, p1, Lczu;->a:[F

    aget v4, v4, v10

    mul-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    const/4 v5, 0x5

    aget v4, v4, v5

    iget-object v5, p1, Lczu;->a:[F

    aget v5, v5, v11

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    const/16 v5, 0x8

    aget v4, v4, v5

    iget-object v5, p1, Lczu;->a:[F

    const/4 v6, 0x5

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    aput v3, v1, v2

    .line 41
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x6

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v7

    iget-object v4, p1, Lczu;->a:[F

    const/4 v5, 0x6

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    aget v4, v4, v10

    iget-object v5, p1, Lczu;->a:[F

    const/4 v6, 0x7

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    const/4 v5, 0x6

    aget v4, v4, v5

    iget-object v5, p1, Lczu;->a:[F

    const/16 v6, 0x8

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    aput v3, v1, v2

    .line 42
    iget-object v1, v0, Lczu;->a:[F

    const/4 v2, 0x7

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v8

    iget-object v4, p1, Lczu;->a:[F

    const/4 v5, 0x6

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    aget v4, v4, v11

    iget-object v5, p1, Lczu;->a:[F

    const/4 v6, 0x7

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    const/4 v5, 0x7

    aget v4, v4, v5

    iget-object v5, p1, Lczu;->a:[F

    const/16 v6, 0x8

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    aput v3, v1, v2

    .line 43
    iget-object v1, v0, Lczu;->a:[F

    const/16 v2, 0x8

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v9

    iget-object v4, p1, Lczu;->a:[F

    const/4 v5, 0x6

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    const/4 v5, 0x5

    aget v4, v4, v5

    iget-object v5, p1, Lczu;->a:[F

    const/4 v6, 0x7

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    const/16 v5, 0x8

    aget v4, v4, v5

    iget-object v5, p1, Lczu;->a:[F

    const/16 v6, 0x8

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    aput v3, v1, v2

    .line 44
    return-object v0
.end method

.method public b()F
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 93
    iget-object v0, p0, Lczu;->a:[F

    aget v0, v0, v4

    iget-object v1, p0, Lczu;->a:[F

    aget v1, v1, v8

    mul-float/2addr v0, v1

    iget-object v1, p0, Lczu;->a:[F

    const/16 v2, 0x8

    aget v1, v1, v2

    mul-float/2addr v0, v1

    iget-object v1, p0, Lczu;->a:[F

    aget v1, v1, v7

    iget-object v2, p0, Lczu;->a:[F

    const/4 v3, 0x7

    aget v2, v2, v3

    mul-float/2addr v1, v2

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v6

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lczu;->a:[F

    const/4 v2, 0x6

    aget v1, v1, v2

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v5

    mul-float/2addr v1, v2

    iget-object v2, p0, Lczu;->a:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lczu;->a:[F

    aget v1, v1, v4

    iget-object v2, p0, Lczu;->a:[F

    const/4 v3, 0x7

    aget v2, v2, v3

    mul-float/2addr v1, v2

    iget-object v2, p0, Lczu;->a:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget-object v1, p0, Lczu;->a:[F

    aget v1, v1, v7

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v5

    mul-float/2addr v1, v2

    iget-object v2, p0, Lczu;->a:[F

    const/16 v3, 0x8

    aget v2, v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget-object v1, p0, Lczu;->a:[F

    const/4 v2, 0x6

    aget v1, v1, v2

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v8

    mul-float/2addr v1, v2

    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v6

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method

.method public b(Lczu;)Lczu;
    .locals 5

    .prologue
    .line 63
    new-instance v1, Lczu;

    invoke-direct {v1}, Lczu;-><init>()V

    .line 64
    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x9

    if-ge v0, v2, :cond_0

    .line 65
    iget-object v2, v1, Lczu;->a:[F

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v0

    iget-object v4, p1, Lczu;->a:[F

    aget v4, v4, v0

    add-float/2addr v3, v4

    aput v3, v2, v0

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_0
    return-object v1
.end method

.method public b(F)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 122
    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v1

    cmpg-float v2, v2, v4

    if-ltz v2, :cond_0

    iget-object v2, p0, Lczu;->a:[F

    const/4 v3, 0x4

    aget v2, v2, v3

    cmpg-float v2, v2, v4

    if-ltz v2, :cond_0

    iget-object v2, p0, Lczu;->a:[F

    const/16 v3, 0x8

    aget v2, v2, v3

    cmpg-float v2, v2, v4

    if-gez v2, :cond_2

    :cond_0
    move v0, v1

    .line 128
    :cond_1
    :goto_0
    return v0

    .line 125
    :cond_2
    iget-object v2, p0, Lczu;->a:[F

    aget v2, v2, v0

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, p1

    if-gtz v2, :cond_3

    iget-object v2, p0, Lczu;->a:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x6

    aget v3, v3, v4

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, p1

    if-gtz v2, :cond_3

    iget-object v2, p0, Lczu;->a:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x7

    aget v3, v3, v4

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_1

    :cond_3
    move v0, v1

    .line 126
    goto :goto_0
.end method

.method public c()Lczu;
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v7, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 103
    new-instance v0, Lczu;

    invoke-direct {v0}, Lczu;-><init>()V

    .line 104
    invoke-virtual {p0}, Lczu;->b()F

    move-result v1

    .line 105
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-nez v2, :cond_0

    .line 118
    :goto_0
    return-object v0

    .line 108
    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    div-float v1, v2, v1

    .line 109
    iget-object v2, v0, Lczu;->a:[F

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v11

    iget-object v4, p0, Lczu;->a:[F

    const/16 v5, 0x8

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    const/4 v5, 0x5

    aget v4, v4, v5

    iget-object v5, p0, Lczu;->a:[F

    const/4 v6, 0x7

    aget v5, v5, v6

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v3, v1

    aput v3, v2, v8

    .line 110
    iget-object v2, v0, Lczu;->a:[F

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v7

    iget-object v4, p0, Lczu;->a:[F

    const/4 v5, 0x7

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    aget v4, v4, v9

    iget-object v5, p0, Lczu;->a:[F

    const/16 v6, 0x8

    aget v5, v5, v6

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v3, v1

    aput v3, v2, v9

    .line 111
    iget-object v2, v0, Lczu;->a:[F

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v9

    iget-object v4, p0, Lczu;->a:[F

    const/4 v5, 0x5

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    aget v4, v4, v7

    iget-object v5, p0, Lczu;->a:[F

    aget v5, v5, v11

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v3, v1

    aput v3, v2, v7

    .line 112
    iget-object v2, v0, Lczu;->a:[F

    iget-object v3, p0, Lczu;->a:[F

    const/4 v4, 0x5

    aget v3, v3, v4

    iget-object v4, p0, Lczu;->a:[F

    const/4 v5, 0x6

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    aget v4, v4, v10

    iget-object v5, p0, Lczu;->a:[F

    const/16 v6, 0x8

    aget v5, v5, v6

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v3, v1

    aput v3, v2, v10

    .line 113
    iget-object v2, v0, Lczu;->a:[F

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v8

    iget-object v4, p0, Lczu;->a:[F

    const/16 v5, 0x8

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lczu;->a:[F

    aget v4, v4, v7

    iget-object v5, p0, Lczu;->a:[F

    const/4 v6, 0x6

    aget v5, v5, v6

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v3, v1

    aput v3, v2, v11

    .line 114
    iget-object v2, v0, Lczu;->a:[F

    const/4 v3, 0x5

    iget-object v4, p0, Lczu;->a:[F

    aget v4, v4, v7

    iget-object v5, p0, Lczu;->a:[F

    aget v5, v5, v10

    mul-float/2addr v4, v5

    iget-object v5, p0, Lczu;->a:[F

    aget v5, v5, v8

    iget-object v6, p0, Lczu;->a:[F

    const/4 v7, 0x5

    aget v6, v6, v7

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    mul-float/2addr v4, v1

    aput v4, v2, v3

    .line 115
    iget-object v2, v0, Lczu;->a:[F

    const/4 v3, 0x6

    iget-object v4, p0, Lczu;->a:[F

    aget v4, v4, v10

    iget-object v5, p0, Lczu;->a:[F

    const/4 v6, 0x7

    aget v5, v5, v6

    mul-float/2addr v4, v5

    iget-object v5, p0, Lczu;->a:[F

    aget v5, v5, v11

    iget-object v6, p0, Lczu;->a:[F

    const/4 v7, 0x6

    aget v6, v6, v7

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    mul-float/2addr v4, v1

    aput v4, v2, v3

    .line 116
    iget-object v2, v0, Lczu;->a:[F

    const/4 v3, 0x7

    iget-object v4, p0, Lczu;->a:[F

    aget v4, v4, v9

    iget-object v5, p0, Lczu;->a:[F

    const/4 v6, 0x6

    aget v5, v5, v6

    mul-float/2addr v4, v5

    iget-object v5, p0, Lczu;->a:[F

    aget v5, v5, v8

    iget-object v6, p0, Lczu;->a:[F

    const/4 v7, 0x7

    aget v6, v6, v7

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    mul-float/2addr v4, v1

    aput v4, v2, v3

    .line 117
    iget-object v2, v0, Lczu;->a:[F

    const/16 v3, 0x8

    iget-object v4, p0, Lczu;->a:[F

    aget v4, v4, v8

    iget-object v5, p0, Lczu;->a:[F

    aget v5, v5, v11

    mul-float/2addr v4, v5

    iget-object v5, p0, Lczu;->a:[F

    aget v5, v5, v9

    iget-object v6, p0, Lczu;->a:[F

    aget v6, v6, v10

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    mul-float/2addr v1, v4

    aput v1, v2, v3

    goto/16 :goto_0
.end method

.method public c(Lczu;)Lczu;
    .locals 5

    .prologue
    .line 71
    new-instance v1, Lczu;

    invoke-direct {v1}, Lczu;-><init>()V

    .line 72
    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x9

    if-ge v0, v2, :cond_0

    .line 73
    iget-object v2, v1, Lczu;->a:[F

    iget-object v3, p0, Lczu;->a:[F

    aget v3, v3, v0

    iget-object v4, p1, Lczu;->a:[F

    aget v4, v4, v0

    sub-float/2addr v3, v4

    aput v3, v2, v0

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :cond_0
    return-object v1
.end method

.method public d(Lczu;)Lczu;
    .locals 10

    .prologue
    .line 173
    new-instance v0, Lczu;

    invoke-direct {v0}, Lczu;-><init>()V

    .line 174
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v4, 0x0

    aget v1, v1, v4

    iget-object v4, p1, Lczu;->a:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v1, v4

    float-to-double v4, v1

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x3

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    add-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x6

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x2

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    iget-object v1, p0, Lczu;->a:[F

    const/4 v4, 0x0

    aget v1, v1, v4

    float-to-double v4, v1

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x3

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x4

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    iget-object v1, p0, Lczu;->a:[F

    const/4 v8, 0x6

    aget v1, v1, v8

    iget-object v8, p1, Lczu;->a:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    mul-float/2addr v1, v8

    float-to-double v8, v1

    add-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x3

    aget v1, v1, v6

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x6

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/16 v7, 0x8

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x6

    aget v1, v1, v6

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 175
    iget-object v1, v0, Lczu;->a:[F

    const/4 v4, 0x0

    double-to-float v2, v2

    aput v2, v1, v4

    .line 176
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v4, 0x0

    aget v1, v1, v4

    iget-object v4, p1, Lczu;->a:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v1, v4

    float-to-double v4, v1

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x3

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    add-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x6

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x2

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    iget-object v1, p0, Lczu;->a:[F

    const/4 v4, 0x1

    aget v1, v1, v4

    float-to-double v4, v1

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x3

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x4

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    iget-object v1, p0, Lczu;->a:[F

    const/4 v8, 0x6

    aget v1, v1, v8

    iget-object v8, p1, Lczu;->a:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    mul-float/2addr v1, v8

    float-to-double v8, v1

    add-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x4

    aget v1, v1, v6

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x6

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/16 v7, 0x8

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x7

    aget v1, v1, v6

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 177
    iget-object v1, v0, Lczu;->a:[F

    const/4 v4, 0x3

    double-to-float v5, v2

    aput v5, v1, v4

    .line 178
    iget-object v1, v0, Lczu;->a:[F

    const/4 v4, 0x1

    double-to-float v2, v2

    aput v2, v1, v4

    .line 179
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v4, 0x0

    aget v1, v1, v4

    iget-object v4, p1, Lczu;->a:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v1, v4

    float-to-double v4, v1

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x3

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    add-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x6

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x2

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    iget-object v1, p0, Lczu;->a:[F

    const/4 v4, 0x2

    aget v1, v1, v4

    float-to-double v4, v1

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x3

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x4

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    iget-object v1, p0, Lczu;->a:[F

    const/4 v8, 0x6

    aget v1, v1, v8

    iget-object v8, p1, Lczu;->a:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    mul-float/2addr v1, v8

    float-to-double v8, v1

    add-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x5

    aget v1, v1, v6

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x6

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/16 v7, 0x8

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/16 v6, 0x8

    aget v1, v1, v6

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 180
    iget-object v1, v0, Lczu;->a:[F

    const/4 v4, 0x6

    double-to-float v5, v2

    aput v5, v1, v4

    .line 181
    iget-object v1, v0, Lczu;->a:[F

    const/4 v4, 0x2

    double-to-float v2, v2

    aput v2, v1, v4

    .line 182
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v4, 0x1

    aget v1, v1, v4

    iget-object v4, p1, Lczu;->a:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v1, v4

    float-to-double v4, v1

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x4

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    add-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x7

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x2

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    iget-object v1, p0, Lczu;->a:[F

    const/4 v4, 0x1

    aget v1, v1, v4

    float-to-double v4, v1

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x4

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x4

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    iget-object v1, p0, Lczu;->a:[F

    const/4 v8, 0x7

    aget v1, v1, v8

    iget-object v8, p1, Lczu;->a:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    mul-float/2addr v1, v8

    float-to-double v8, v1

    add-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x4

    aget v1, v1, v6

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x7

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/16 v7, 0x8

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x7

    aget v1, v1, v6

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 183
    iget-object v1, v0, Lczu;->a:[F

    const/4 v4, 0x4

    double-to-float v2, v2

    aput v2, v1, v4

    .line 184
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v4, 0x1

    aget v1, v1, v4

    iget-object v4, p1, Lczu;->a:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v1, v4

    float-to-double v4, v1

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x4

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    add-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x7

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x2

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    iget-object v1, p0, Lczu;->a:[F

    const/4 v4, 0x2

    aget v1, v1, v4

    float-to-double v4, v1

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x4

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x4

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    iget-object v1, p0, Lczu;->a:[F

    const/4 v8, 0x7

    aget v1, v1, v8

    iget-object v8, p1, Lczu;->a:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    mul-float/2addr v1, v8

    float-to-double v8, v1

    add-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x5

    aget v1, v1, v6

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x7

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/16 v7, 0x8

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/16 v6, 0x8

    aget v1, v1, v6

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 185
    iget-object v1, v0, Lczu;->a:[F

    const/4 v4, 0x7

    double-to-float v5, v2

    aput v5, v1, v4

    .line 186
    iget-object v1, v0, Lczu;->a:[F

    const/4 v4, 0x5

    double-to-float v2, v2

    aput v2, v1, v4

    .line 187
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v4, 0x2

    aget v1, v1, v4

    iget-object v4, p1, Lczu;->a:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v1, v4

    float-to-double v4, v1

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x5

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    add-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/16 v6, 0x8

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x2

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    iget-object v1, p0, Lczu;->a:[F

    const/4 v4, 0x2

    aget v1, v1, v4

    float-to-double v4, v1

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x5

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/4 v7, 0x4

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    iget-object v1, p0, Lczu;->a:[F

    const/16 v8, 0x8

    aget v1, v1, v8

    iget-object v8, p1, Lczu;->a:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    mul-float/2addr v1, v8

    float-to-double v8, v1

    add-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/4 v6, 0x5

    aget v1, v1, v6

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-object v1, p0, Lczu;->a:[F

    const/16 v6, 0x8

    aget v1, v1, v6

    iget-object v6, p1, Lczu;->a:[F

    const/16 v7, 0x8

    aget v6, v6, v7

    mul-float/2addr v1, v6

    float-to-double v6, v1

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v1, p0, Lczu;->a:[F

    const/16 v6, 0x8

    aget v1, v1, v6

    float-to-double v6, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 188
    iget-object v1, v0, Lczu;->a:[F

    const/16 v4, 0x8

    double-to-float v2, v2

    aput v2, v1, v4

    .line 189
    return-object v0
.end method
