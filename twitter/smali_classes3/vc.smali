.class public Lvc;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:J

.field private final c:Lcom/twitter/android/widget/NewItemBannerView;

.field private final d:Lank;

.field private final e:Lcom/twitter/android/timeline/br$a;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/livevideo/landing/b;Landroid/os/Bundle;Lcom/twitter/android/widget/NewItemBannerView;Lcom/twitter/android/timeline/br$a;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lvc;->a:Landroid/content/Context;

    .line 58
    iget-wide v0, p2, Lcom/twitter/android/livevideo/landing/b;->a:J

    iput-wide v0, p0, Lvc;->b:J

    .line 59
    iput-object p4, p0, Lvc;->c:Lcom/twitter/android/widget/NewItemBannerView;

    .line 60
    invoke-static {p3}, Lank;->a(Landroid/os/Bundle;)Lank;

    move-result-object v0

    iput-object v0, p0, Lvc;->d:Lank;

    .line 61
    iput-object p5, p0, Lvc;->e:Lcom/twitter/android/timeline/br$a;

    .line 62
    iput-object p6, p0, Lvc;->f:Ljava/lang/String;

    .line 63
    return-void
.end method

.method static a()Lub;
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lub;

    invoke-direct {v0}, Lub;-><init>()V

    return-object v0
.end method


# virtual methods
.method a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ltx;Lbdh;)Lua;
    .locals 9

    .prologue
    .line 84
    new-instance v1, Lua;

    iget-wide v4, p0, Lvc;->b:J

    iget-object v8, p0, Lvc;->f:Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v8}, Lua;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLtx;Lbdh;Ljava/lang/String;)V

    return-object v1
.end method

.method b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lvc;->a:Landroid/content/Context;

    return-object v0
.end method

.method c()Lcom/twitter/library/scribe/n;
    .locals 4

    .prologue
    .line 91
    new-instance v0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;-><init>()V

    iget-wide v2, p0, Lvc;->b:J

    .line 93
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    move-result-object v0

    iget-object v1, p0, Lvc;->f:Ljava/lang/String;

    .line 94
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    .line 96
    new-instance v1, Lcom/twitter/library/scribe/e;

    invoke-direct {v1, v0}, Lcom/twitter/library/scribe/e;-><init>(Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;)V

    return-object v1
.end method

.method d()Lcom/twitter/analytics/model/ScribeItem;
    .locals 3

    .prologue
    .line 104
    iget-wide v0, p0, Lvc;->b:J

    iget-object v2, p0, Lvc;->f:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/b;->a(JLjava/lang/String;)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    return-object v0
.end method

.method e()Lcom/twitter/android/timeline/br;
    .locals 4

    .prologue
    .line 111
    const/16 v0, 0xd

    iget-object v1, p0, Lvc;->c:Lcom/twitter/android/widget/NewItemBannerView;

    iget-object v2, p0, Lvc;->e:Lcom/twitter/android/timeline/br$a;

    iget-object v3, p0, Lvc;->d:Lank;

    .line 112
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/timeline/bs;->a(ILcom/twitter/android/widget/NewItemBannerView;Lcom/twitter/android/timeline/br$a;Lank;)Lcom/twitter/android/timeline/br;

    move-result-object v0

    .line 111
    return-object v0
.end method
