.class Lcyb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcyf$a;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcyb$c;,
        Lcyb$b;,
        Lcyb$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ltv/periscope/android/video/b;

.field private final c:Lcyb$a;

.field private final d:Lcyb$b;

.field private final e:Lde/greenrobot/event/c;

.field private final f:Lczz;

.field private final g:Ltv/periscope/android/ui/broadcast/h;

.field private h:Ltv/periscope/android/video/RTMPPublisher;

.field private i:Lcyf;

.field private j:Lcyb$c;

.field private final k:Ljava/util/concurrent/Semaphore;

.field private l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/lang/String;

.field private n:Landroid/media/MediaFormat;

.field private o:Landroid/media/MediaFormat;

.field private p:I

.field private q:Z

.field private r:D

.field private s:J

.field private t:J

.field private u:J


# direct methods
.method constructor <init>(Landroid/app/Activity;Ltv/periscope/android/graphics/GLRenderView;Ltv/periscope/android/library/c;Ltv/periscope/android/ui/broadcast/h;Lczz;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1, v1}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    iput-object v0, p0, Lcyb;->k:Ljava/util/concurrent/Semaphore;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcyb;->l:Ljava/util/HashMap;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcyb;->q:Z

    .line 73
    iput-wide v4, p0, Lcyb;->s:J

    .line 74
    iput-wide v4, p0, Lcyb;->t:J

    .line 75
    iput-wide v4, p0, Lcyb;->u:J

    .line 90
    iput-object p1, p0, Lcyb;->a:Landroid/content/Context;

    .line 91
    invoke-interface {p3}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v0

    iput-object v0, p0, Lcyb;->e:Lde/greenrobot/event/c;

    .line 92
    iput-object p4, p0, Lcyb;->g:Ltv/periscope/android/ui/broadcast/h;

    .line 93
    iput-object p5, p0, Lcyb;->f:Lczz;

    .line 94
    iput p6, p0, Lcyb;->p:I

    .line 96
    new-instance v0, Lcyb$c;

    const/4 v1, 0x2

    invoke-direct {v0, p0, p1, v1}, Lcyb$c;-><init>(Lcyb;Landroid/content/Context;I)V

    iput-object v0, p0, Lcyb;->j:Lcyb$c;

    .line 97
    iget-object v0, p0, Lcyb;->j:Lcyb$c;

    invoke-virtual {v0}, Lcyb$c;->canDetectOrientation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcyb;->j:Lcyb$c;

    invoke-virtual {v0}, Lcyb$c;->enable()V

    .line 104
    :goto_0
    invoke-static {p1}, Lcyh;->a(Landroid/content/Context;)Lcyf;

    move-result-object v0

    iput-object v0, p0, Lcyb;->i:Lcyf;

    .line 105
    iget-object v0, p0, Lcyb;->i:Lcyf;

    invoke-interface {v0, p0}, Lcyf;->a(Lcyf$a;)V

    .line 108
    iget v0, p0, Lcyb;->p:I

    invoke-direct {p0, v0}, Lcyb;->b(I)V

    .line 110
    new-instance v0, Lcyb$a;

    invoke-direct {v0, p1}, Lcyb$a;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcyb;->c:Lcyb$a;

    .line 111
    new-instance v0, Lcyb$b;

    invoke-direct {v0, p0, v3}, Lcyb$b;-><init>(Lcyb;Lcyb$1;)V

    iput-object v0, p0, Lcyb;->d:Lcyb$b;

    .line 112
    new-instance v0, Ltv/periscope/android/video/b;

    invoke-direct {v0}, Ltv/periscope/android/video/b;-><init>()V

    iput-object v0, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    .line 113
    iget-object v0, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    const/16 v1, 0x140

    const/16 v2, 0x238

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/video/b;->a(II)V

    .line 115
    iget-object v0, p0, Lcyb;->i:Lcyf;

    iget-object v1, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    invoke-virtual {v1}, Ltv/periscope/android/video/b;->c()I

    move-result v1

    mul-int/lit8 v1, v1, 0x8

    invoke-interface {v0, v1}, Lcyf;->b(I)V

    .line 116
    iget-object v0, p0, Lcyb;->i:Lcyf;

    invoke-interface {v0, p2}, Lcyf;->a(Ltv/periscope/android/graphics/GLRenderView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    const-string/jumbo v0, "BroadcasterVideoController"

    const-string/jumbo v1, "Unable to connect to camera"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcyb;->i:Lcyf;

    invoke-interface {v0}, Lcyf;->a()V

    .line 122
    iget-object v0, p0, Lcyb;->i:Lcyf;

    invoke-interface {v0}, Lcyf;->d()V

    .line 123
    iput-object v3, p0, Lcyb;->i:Lcyf;

    .line 125
    :cond_0
    return-void

    .line 100
    :cond_1
    iput-object v3, p0, Lcyb;->j:Lcyb$c;

    goto :goto_0
.end method

.method static synthetic a(Lcyb;D)D
    .locals 1

    .prologue
    .line 45
    iput-wide p1, p0, Lcyb;->r:D

    return-wide p1
.end method

.method private a(FI)F
    .locals 2

    .prologue
    .line 253
    const/high16 v0, 0x41000000    # 8.0f

    mul-float/2addr v0, p1

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    int-to-float v1, p2

    sub-float/2addr v0, v1

    .line 256
    const/high16 v1, 0x42820000    # 65.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    return v0
.end method

.method static synthetic a(Lcyb;J)J
    .locals 1

    .prologue
    .line 45
    iput-wide p1, p0, Lcyb;->u:J

    return-wide p1
.end method

.method static synthetic a(Lcyb;)Ljava/util/concurrent/Semaphore;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcyb;->k:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic a(Lcyb;I)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcyb;->b(I)V

    return-void
.end method

.method static synthetic a(Lcyb;Z)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcyb;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 325
    iget-object v0, p0, Lcyb;->i:Lcyf;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcyb;->l:Ljava/util/HashMap;

    const-string/jumbo v1, "bps"

    iget-object v2, p0, Lcyb;->i:Lcyf;

    invoke-interface {v2}, Lcyf;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    :cond_0
    iget-object v0, p0, Lcyb;->l:Ljava/util/HashMap;

    const-string/jumbo v1, "UploadRate"

    iget-object v2, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    invoke-virtual {v2}, Ltv/periscope/android/video/b;->a()I

    move-result v2

    mul-int/lit8 v2, v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    iget-object v0, p0, Lcyb;->l:Ljava/util/HashMap;

    const-string/jumbo v1, "live"

    iget-boolean v2, p0, Lcyb;->q:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    iget-object v0, p0, Lcyb;->j:Lcyb$c;

    if-eqz v0, :cond_1

    .line 332
    iget-object v0, p0, Lcyb;->l:Ljava/util/HashMap;

    const-string/jumbo v1, "rotation"

    iget-wide v2, p0, Lcyb;->r:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    :cond_1
    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    if-eqz v0, :cond_2

    .line 340
    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    iget-object v1, p0, Lcyb;->l:Ljava/util/HashMap;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Ltv/periscope/android/video/RTMPPublisher;->a(Ljava/util/HashMap;ZZ)V

    .line 342
    :cond_2
    return-void
.end method

.method static synthetic b(Lcyb;J)J
    .locals 1

    .prologue
    .line 45
    iput-wide p1, p0, Lcyb;->s:J

    return-wide p1
.end method

.method static synthetic b(Lcyb;)Lcyf;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcyb;->i:Lcyf;

    return-object v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 351
    iget-object v1, p0, Lcyb;->i:Lcyf;

    if-nez v1, :cond_0

    .line 372
    :goto_0
    return-void

    .line 357
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 371
    :goto_1
    :pswitch_0
    iget-object v1, p0, Lcyb;->i:Lcyf;

    invoke-interface {v1, v0}, Lcyf;->a(I)V

    goto :goto_0

    .line 362
    :pswitch_1
    const/16 v0, 0x5a

    .line 363
    goto :goto_1

    .line 365
    :pswitch_2
    const/16 v0, 0xb4

    .line 366
    goto :goto_1

    .line 368
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_1

    .line 357
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(FI)Z
    .locals 1

    .prologue
    .line 263
    mul-int/lit16 v0, p2, 0x400

    div-int/lit8 v0, v0, 0x8

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcyb;J)J
    .locals 1

    .prologue
    .line 45
    iput-wide p1, p0, Lcyb;->t:J

    return-wide p1
.end method

.method static synthetic c(Lcyb;)Ltv/periscope/android/video/RTMPPublisher;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    return-object v0
.end method

.method static synthetic d(Lcyb;)Lcyb$a;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcyb;->c:Lcyb$a;

    return-object v0
.end method

.method static synthetic e(Lcyb;)J
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcyb;->u:J

    return-wide v0
.end method

.method static synthetic f(Lcyb;)J
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcyb;->s:J

    return-wide v0
.end method

.method static synthetic g(Lcyb;)J
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcyb;->t:J

    return-wide v0
.end method

.method static synthetic h(Lcyb;)Ltv/periscope/android/video/b;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    return-object v0
.end method

.method static synthetic i(Lcyb;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcyb;->l:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic j(Lcyb;)D
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcyb;->r:D

    return-wide v0
.end method

.method static synthetic k(Lcyb;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcyb;->p:I

    return v0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 452
    iget-object v0, p0, Lcyb;->c:Lcyb$a;

    iget-object v1, p0, Lcyb;->d:Lcyb$b;

    invoke-virtual {v0, v1}, Lcyb$a;->a(Ljava/lang/Runnable;)V

    .line 453
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 456
    iget-object v0, p0, Lcyb;->c:Lcyb$a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcyb$a;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 457
    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcyb;->i:Lcyf;

    if-eqz v0, :cond_0

    .line 200
    const-string/jumbo v0, "BroadcasterVideoController"

    const-string/jumbo v1, "Starting encoding"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcyb;->i:Lcyf;

    invoke-interface {v0}, Lcyf;->b()V

    .line 203
    :cond_0
    return-void
.end method

.method a(I)V
    .locals 0

    .prologue
    .line 346
    iput p1, p0, Lcyb;->p:I

    .line 347
    invoke-direct {p0, p1}, Lcyb;->b(I)V

    .line 348
    return-void
.end method

.method public a(Landroid/media/MediaFormat;Landroid/media/MediaFormat;)V
    .locals 1

    .prologue
    .line 417
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 418
    iput-object p1, p0, Lcyb;->n:Landroid/media/MediaFormat;

    .line 419
    iput-object p2, p0, Lcyb;->o:Landroid/media/MediaFormat;

    .line 420
    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/video/RTMPPublisher;->a(Landroid/media/MediaFormat;Landroid/media/MediaFormat;)V

    .line 424
    :cond_0
    return-void
.end method

.method a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    invoke-virtual {v0, p1}, Ltv/periscope/android/video/RTMPPublisher;->a(Ljava/lang/Runnable;)V

    .line 217
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 206
    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcyb;->i:Lcyf;

    if-eqz v0, :cond_0

    .line 207
    invoke-static {p1}, Ltv/periscope/android/util/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcyb;->m:Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    iget-object v1, p0, Lcyb;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ltv/periscope/android/video/RTMPPublisher;->a(Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcyb;->i:Lcyf;

    invoke-interface {v0}, Lcyf;->c()V

    .line 210
    iput-boolean v2, p0, Lcyb;->q:Z

    .line 211
    invoke-direct {p0, v2}, Lcyb;->a(Z)V

    .line 213
    :cond_0
    return-void
.end method

.method public a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 6

    .prologue
    .line 428
    monitor-enter p0

    .line 429
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcyb;->s:J

    .line 430
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 431
    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/video/RTMPPublisher;->b(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 433
    iget-object v0, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    iget-wide v2, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ltv/periscope/android/video/b;->a(ID)V

    .line 435
    :cond_0
    return-void

    .line 430
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method a(Ltv/periscope/model/w;Ltv/periscope/android/video/RTMPPublisher$a;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 159
    if-eqz p3, :cond_2

    .line 160
    invoke-virtual {p1}, Ltv/periscope/model/w;->j()Ljava/lang/String;

    move-result-object v0

    .line 161
    invoke-virtual {p1}, Ltv/periscope/model/w;->k()I

    move-result v3

    .line 167
    :goto_0
    iget-object v1, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    if-eqz v1, :cond_4

    .line 168
    const/4 v1, 0x1

    .line 169
    invoke-direct {p0}, Lcyb;->l()V

    .line 170
    iget-object v2, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    .line 171
    const/4 v4, 0x0

    iput-object v4, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    .line 172
    invoke-virtual {v2}, Ltv/periscope/android/video/RTMPPublisher;->f()V

    move v7, v1

    .line 174
    :goto_1
    const-string/jumbo v1, "rtmps"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 175
    const-string/jumbo v1, "PSPS"

    .line 179
    :goto_2
    iget-object v0, p0, Lcyb;->f:Lczz;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Setting up RTMP with "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, ", wasRunning was "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lczz;->a(Ljava/lang/String;)V

    .line 180
    new-instance v0, Ltv/periscope/android/video/RTMPPublisher;

    invoke-virtual {p1}, Ltv/periscope/model/w;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ltv/periscope/model/w;->g()Ljava/lang/String;

    move-result-object v4

    .line 181
    invoke-virtual {p1}, Ltv/periscope/model/w;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Ltv/periscope/model/w;->i()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/video/RTMPPublisher;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-virtual {v0, p2}, Ltv/periscope/android/video/RTMPPublisher;->a(Ltv/periscope/android/video/RTMPPublisher$a;)V

    .line 183
    iget-object v1, p0, Lcyb;->l:Ljava/util/HashMap;

    invoke-virtual {v0, v1, v8, v8}, Ltv/periscope/android/video/RTMPPublisher;->a(Ljava/util/HashMap;ZZ)V

    .line 184
    if-eqz v7, :cond_1

    iget-object v1, p0, Lcyb;->n:Landroid/media/MediaFormat;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcyb;->o:Landroid/media/MediaFormat;

    if-eqz v1, :cond_1

    .line 185
    iget-object v1, p0, Lcyb;->n:Landroid/media/MediaFormat;

    iget-object v2, p0, Lcyb;->o:Landroid/media/MediaFormat;

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/video/RTMPPublisher;->a(Landroid/media/MediaFormat;Landroid/media/MediaFormat;)V

    .line 187
    iget-object v1, p0, Lcyb;->i:Lcyf;

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcyb;->i:Lcyf;

    invoke-interface {v1}, Lcyf;->g()V

    .line 190
    :cond_0
    invoke-virtual {v0}, Ltv/periscope/android/video/RTMPPublisher;->e()V

    .line 192
    :cond_1
    iput-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    .line 193
    invoke-direct {p0}, Lcyb;->k()V

    .line 194
    return-void

    .line 163
    :cond_2
    invoke-virtual {p1}, Ltv/periscope/model/w;->d()Ljava/lang/String;

    move-result-object v0

    .line 164
    invoke-virtual {p1}, Ltv/periscope/model/w;->f()I

    move-result v3

    goto/16 :goto_0

    .line 177
    :cond_3
    const-string/jumbo v1, "PSP"

    goto :goto_2

    :cond_4
    move v7, v8

    goto :goto_1
.end method

.method public a([I)V
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcyb;->e:Lde/greenrobot/event/c;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 413
    return-void
.end method

.method a(F)Z
    .locals 5

    .prologue
    .line 133
    const/16 v0, 0xfa0

    invoke-direct {p0, p1, v0}, Lcyb;->a(FI)F

    move-result v1

    .line 136
    const/16 v0, 0x46

    invoke-direct {p0, v1, v0}, Lcyb;->b(FI)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 137
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Upload "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/high16 v3, 0x41000000    # 8.0f

    mul-float/2addr v3, p1

    const/high16 v4, 0x44800000    # 1024.0f

    div-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Video rate "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    float-to-int v1, v1

    mul-int/lit8 v1, v1, 0x8

    div-int/lit16 v1, v1, 0x400

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " kbits/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 139
    const-string/jumbo v2, "BroadcasterVideoController"

    invoke-static {v2, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    if-eqz v0, :cond_2

    .line 142
    const-string/jumbo v1, "BroadcasterVideoController"

    const-string/jumbo v2, "Video byterate is acceptable."

    invoke-static {v1, v2}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object v1, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    float-to-int v2, p1

    invoke-virtual {v1, v2}, Ltv/periscope/android/video/b;->a(I)V

    .line 146
    iget-object v1, p0, Lcyb;->i:Lcyf;

    if-eqz v1, :cond_0

    .line 147
    iget-object v1, p0, Lcyb;->i:Lcyf;

    iget-object v2, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    invoke-virtual {v2}, Ltv/periscope/android/video/b;->c()I

    move-result v2

    mul-int/lit8 v2, v2, 0x8

    invoke-interface {v1, v2}, Lcyf;->b(I)V

    .line 148
    iget-object v1, p0, Lcyb;->i:Lcyf;

    iget-object v2, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    invoke-virtual {v2}, Ltv/periscope/android/video/b;->g()I

    move-result v2

    invoke-interface {v1, v2}, Lcyf;->d(I)V

    .line 153
    :cond_0
    :goto_1
    return v0

    .line 136
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 151
    :cond_2
    const-string/jumbo v1, "BroadcasterVideoController"

    const-string/jumbo v2, "Video byterate is NOT acceptable."

    invoke-static {v1, v2}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(J)Z
    .locals 5

    .prologue
    .line 447
    long-to-double v0, p1

    const-wide v2, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v0, v2

    .line 448
    iget-object v2, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    invoke-virtual {v2, v0, v1}, Ltv/periscope/android/video/b;->a(D)Z

    move-result v0

    return v0
.end method

.method b()V
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcyb;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 221
    const-string/jumbo v0, "BroadcasterVideoController"

    const-string/jumbo v1, "Video was not saved to camera, deleting"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcyb;->m:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 223
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 225
    :cond_0
    return-void
.end method

.method b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 228
    iget-boolean v0, p0, Lcyb;->q:Z

    .line 229
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcyb;->q:Z

    .line 230
    iget-object v1, p0, Lcyb;->i:Lcyf;

    if-eqz v1, :cond_2

    .line 231
    invoke-direct {p0, v3}, Lcyb;->a(Z)V

    .line 232
    invoke-direct {p0}, Lcyb;->l()V

    .line 233
    iget-object v1, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    invoke-virtual {v1}, Ltv/periscope/android/video/b;->h()V

    .line 234
    iget-object v1, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    if-eqz v1, :cond_0

    .line 235
    iget-object v1, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    invoke-virtual {v1}, Ltv/periscope/android/video/RTMPPublisher;->f()V

    .line 236
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcyb;->g:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/h;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    invoke-virtual {v0}, Ltv/periscope/android/video/RTMPPublisher;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    iget-object v0, p0, Lcyb;->a:Landroid/content/Context;

    iget-object v1, p0, Lcyb;->a:Landroid/content/Context;

    sget v2, Ltv/periscope/android/library/f$l;->ps__save_broadcast_unsuccessful:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 242
    :cond_0
    iget-object v0, p0, Lcyb;->m:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcyb;->g:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/h;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    iget-object v0, p0, Lcyb;->g:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcast/h;->e(Ljava/lang/String;)V

    .line 246
    :cond_1
    iget-object v0, p0, Lcyb;->i:Lcyf;

    invoke-interface {v0}, Lcyf;->a()V

    .line 247
    iget-object v0, p0, Lcyb;->i:Lcyf;

    invoke-interface {v0}, Lcyf;->d()V

    .line 249
    :cond_2
    return-void
.end method

.method public b(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 6

    .prologue
    .line 439
    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/video/RTMPPublisher;->a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 441
    iget-object v0, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    iget-wide v2, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ltv/periscope/android/video/b;->b(ID)V

    .line 443
    :cond_0
    return-void
.end method

.method c()I
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    invoke-virtual {v0}, Ltv/periscope/android/video/b;->c()I

    move-result v0

    mul-int/lit8 v0, v0, 0x8

    div-int/lit16 v0, v0, 0x400

    return v0
.end method

.method d()Lcyf;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcyb;->i:Lcyf;

    return-object v0
.end method

.method e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcyb;->m:Ljava/lang/String;

    return-object v0
.end method

.method f()I
    .locals 4

    .prologue
    .line 281
    iget-wide v0, p0, Lcyb;->r:D

    const-wide v2, 0x4046800000000000L    # 45.0

    add-double/2addr v0, v2

    double-to-int v0, v0

    div-int/lit8 v0, v0, 0x5a

    rem-int/lit8 v0, v0, 0x4

    .line 285
    sget-object v1, Lcxz;->a:[I

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 297
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 287
    :pswitch_0
    const/16 v0, 0x5a

    goto :goto_0

    .line 290
    :pswitch_1
    const/16 v0, 0xb4

    goto :goto_0

    .line 293
    :pswitch_2
    const/16 v0, 0x10e

    goto :goto_0

    .line 285
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public g()Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 303
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 305
    iget-object v1, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    if-eqz v1, :cond_0

    .line 306
    iget-object v1, p0, Lcyb;->h:Ltv/periscope/android/video/RTMPPublisher;

    invoke-virtual {v1}, Ltv/periscope/android/video/RTMPPublisher;->k()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 308
    :cond_0
    iget-object v1, p0, Lcyb;->b:Ltv/periscope/android/video/b;

    invoke-virtual {v1}, Ltv/periscope/android/video/b;->i()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 309
    return-object v0
.end method

.method public h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 313
    iget-object v0, p0, Lcyb;->i:Lcyf;

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcyb;->i:Lcyf;

    invoke-interface {v0}, Lcyf;->a()V

    .line 315
    iget-object v0, p0, Lcyb;->i:Lcyf;

    invoke-interface {v0}, Lcyf;->d()V

    .line 316
    iput-object v1, p0, Lcyb;->i:Lcyf;

    .line 318
    :cond_0
    iget-object v0, p0, Lcyb;->j:Lcyb$c;

    if-eqz v0, :cond_1

    .line 319
    iget-object v0, p0, Lcyb;->j:Lcyb$c;

    invoke-virtual {v0}, Lcyb$c;->disable()V

    .line 320
    iput-object v1, p0, Lcyb;->j:Lcyb$c;

    .line 322
    :cond_1
    return-void
.end method

.method i()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 375
    iget-object v1, p0, Lcyb;->i:Lcyf;

    if-nez v1, :cond_0

    .line 402
    :goto_0
    return-void

    .line 378
    :cond_0
    iget-object v1, p0, Lcyb;->i:Lcyf;

    invoke-interface {v1}, Lcyf;->f()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 379
    :goto_1
    if-eqz v0, :cond_2

    .line 380
    iget-object v0, p0, Lcyb;->f:Lczz;

    const-string/jumbo v1, "Flip to rear facing camera"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 384
    :goto_2
    iget v0, p0, Lcyb;->p:I

    .line 385
    new-instance v1, Lcyb$1;

    const-string/jumbo v2, "Camera flip"

    invoke-static {v2}, Ltv/periscope/android/util/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcyb$1;-><init>(Lcyb;Ljava/lang/String;I)V

    .line 401
    invoke-virtual {v1}, Lcyb$1;->start()V

    goto :goto_0

    .line 378
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 382
    :cond_2
    iget-object v0, p0, Lcyb;->f:Lczz;

    const-string/jumbo v1, "Flip to front facing camera"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public j()V
    .locals 0

    .prologue
    .line 407
    return-void
.end method
