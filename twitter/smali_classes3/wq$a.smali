.class public Lwq$a;
.super Laum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lwq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laum",
        "<",
        "Ljava/lang/Long;",
        "Lcom/twitter/util/collection/m",
        "<",
        "Ljava/lang/Long;",
        "Lcom/twitter/model/moments/b;",
        ">;",
        "Lbdk;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/Long;",
            "Lbdk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/util/object/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/Long;",
            "Lbdk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0}, Laum;-><init>()V

    .line 86
    iput-object p1, p0, Lwq$a;->a:Lcom/twitter/util/object/d;

    .line 87
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Long;)Lbdk;
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lwq$a;->a:Lcom/twitter/util/object/d;

    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/library/service/s;
    .locals 1

    .prologue
    .line 78
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lwq$a;->a(Ljava/lang/Long;)Lbdk;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbdk;)Lcom/twitter/util/collection/m;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbdk;",
            ")",
            "Lcom/twitter/util/collection/m",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p1}, Lbdk;->e()Lcom/twitter/model/moments/b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {p1}, Lbdk;->e()Lcom/twitter/model/moments/b;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/m;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/m;

    move-result-object v0

    .line 95
    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p1, Lbdk;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/m;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/m;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/library/service/s;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    check-cast p1, Lbdk;

    invoke-virtual {p0, p1}, Lwq$a;->a(Lbdk;)Lcom/twitter/util/collection/m;

    move-result-object v0

    return-object v0
.end method
