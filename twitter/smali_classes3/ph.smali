.class public Lph;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:J

.field private volatile b:J

.field private volatile c:J

.field private final d:Lpg;


# direct methods
.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 24
    const-wide/16 v0, 0xa

    invoke-direct {p0, p1, p2, v0, v1}, Lph;-><init>(JJ)V

    .line 25
    return-void
.end method

.method constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lpg;

    invoke-direct {v0, p3, p4}, Lpg;-><init>(J)V

    invoke-direct {p0, p1, p2, v0}, Lph;-><init>(JLpg;)V

    .line 32
    return-void
.end method

.method constructor <init>(JLpg;)V
    .locals 3

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lph;->c:J

    .line 38
    iput-wide p1, p0, Lph;->a:J

    .line 39
    iput-object p3, p0, Lph;->d:Lpg;

    .line 40
    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 69
    iget-wide v0, p0, Lph;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 70
    iget-object v0, p0, Lph;->d:Lpg;

    invoke-virtual {v0}, Lpg;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lph;->c:J

    .line 72
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lph;->f()V

    .line 57
    return-void
.end method

.method public declared-synchronized a(J)V
    .locals 1

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lph;->d:Lpg;

    invoke-virtual {v0}, Lpg;->a()V

    .line 49
    iput-wide p1, p0, Lph;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    monitor-exit p0

    return-void

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()Z
    .locals 4

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lph;->f()V

    .line 80
    iget-object v0, p0, Lph;->d:Lpg;

    invoke-virtual {v0}, Lpg;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lph;->c:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lph;->a:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lph;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    const/4 v0, 0x1

    .line 85
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 2

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lph;->d:Lpg;

    invoke-virtual {v0}, Lpg;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lph;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    monitor-exit p0

    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()J
    .locals 2

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lph;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lph;->d:Lpg;

    invoke-virtual {v0}, Lpg;->c()V

    .line 124
    return-void
.end method
