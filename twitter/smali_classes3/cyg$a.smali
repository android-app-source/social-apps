.class Lcyg$a;
.super Lcyg$e;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcyg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcyg;


# direct methods
.method constructor <init>(Lcyg;)V
    .locals 1

    .prologue
    .line 868
    iput-object p1, p0, Lcyg$a;->a:Lcyg;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcyg$e;-><init>(Lcyg$1;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const-wide/16 v10, 0x3e8

    .line 871
    iget-object v0, p0, Lcyg$a;->a:Lcyg;

    iget-object v0, v0, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V

    .line 872
    invoke-virtual {p0}, Lcyg$a;->f()V

    .line 873
    iget-object v0, p0, Lcyg$a;->a:Lcyg;

    iget-object v0, v0, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 874
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcyg$a;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 875
    const/4 v1, 0x0

    .line 877
    :try_start_0
    iget-object v0, p0, Lcyg$a;->a:Lcyg;

    iget-object v0, v0, Lcyg;->o:Ljava/util/concurrent/ArrayBlockingQueue;

    const-wide/16 v4, 0x3e8

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v3}, Ljava/util/concurrent/ArrayBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyg$b;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v7, v0

    .line 880
    :goto_1
    if-eqz v7, :cond_0

    .line 881
    :cond_1
    invoke-virtual {p0}, Lcyg$a;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 882
    iget-object v0, p0, Lcyg$a;->a:Lcyg;

    iget-object v0, v0, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0, v10, v11}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v1

    .line 883
    const/4 v0, -0x1

    if-eq v1, v0, :cond_1

    .line 886
    aget-object v3, v8, v1

    .line 887
    invoke-virtual {v7, v3}, Lcyg$b;->a(Ljava/nio/ByteBuffer;)V

    .line 888
    iget-object v0, p0, Lcyg$a;->a:Lcyg;

    iget-object v0, v0, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    .line 889
    invoke-virtual {v7}, Lcyg$b;->a()J

    move-result-wide v4

    div-long/2addr v4, v10

    move v6, v2

    .line 888
    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 892
    :cond_2
    iget-object v0, p0, Lcyg$a;->a:Lcyg;

    invoke-virtual {v0, v7}, Lcyg;->b(Lcyg$b;)V

    goto :goto_0

    .line 878
    :catch_0
    move-exception v0

    move-object v7, v1

    goto :goto_1

    .line 895
    :cond_3
    iget-object v0, p0, Lcyg$a;->a:Lcyg;

    iget-object v0, v0, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 896
    return-void
.end method
