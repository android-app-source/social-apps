.class Lsi$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsi;->a(Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsi;


# direct methods
.method constructor <init>(Lsi;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lsi$2;->a:Lsi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 4

    .prologue
    .line 218
    iget-object v0, p0, Lsi$2;->a:Lsi;

    invoke-static {v0}, Lsi;->c(Lsi;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 219
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 223
    :cond_0
    iget-object v0, p0, Lsi$2;->a:Lsi;

    iget-object v1, p0, Lsi$2;->a:Lsi;

    invoke-static {v1}, Lsi;->d(Lsi;)Lcom/twitter/ui/widget/ClippedImageView;

    move-result-object v1

    iget-object v2, p0, Lsi$2;->a:Lsi;

    invoke-static {v2}, Lsi;->e(Lsi;)Lcom/twitter/ui/anim/b$a;

    move-result-object v2

    iget-object v3, p0, Lsi$2;->a:Lsi;

    invoke-static {v3}, Lsi;->b(Lsi;)Lcom/twitter/ui/anim/a$a;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lsi;->a(Lcom/twitter/ui/widget/ClippedImageView;Lcom/twitter/ui/anim/b$a;Lcom/twitter/ui/anim/a$a;)V

    .line 225
    const/4 v0, 0x1

    return v0
.end method
