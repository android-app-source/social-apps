.class public Lcyt;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcys;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcys;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lde/greenrobot/event/c;


# direct methods
.method public constructor <init>(Lde/greenrobot/event/c;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcyt;->a:Ljava/util/Map;

    .line 29
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcyt;->b:Ljava/util/Map;

    .line 34
    iput-object p1, p0, Lcyt;->c:Lde/greenrobot/event/c;

    .line 35
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)Ltv/periscope/model/r;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcys;",
            ">;)",
            "Ltv/periscope/model/r;"
        }
    .end annotation

    .prologue
    .line 250
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcys;

    .line 251
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcys;->a()Ltv/periscope/model/r;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcyt;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ltv/periscope/model/r;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcyt;->a:Ljava/util/Map;

    invoke-direct {p0, p1, v0}, Lcyt;->a(Ljava/lang/String;Ljava/util/Map;)Ltv/periscope/model/r;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ltv/periscope/model/r;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcyt;->b:Ljava/util/Map;

    invoke-direct {p0, p1, v0}, Lcyt;->a(Ljava/lang/String;Ljava/util/Map;)Ltv/periscope/model/r;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Ltv/periscope/model/r;
    .locals 1

    .prologue
    .line 240
    invoke-virtual {p0, p1}, Lcyt;->a(Ljava/lang/String;)Ltv/periscope/model/r;

    move-result-object v0

    .line 241
    if-eqz v0, :cond_0

    .line 245
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcyt;->b(Ljava/lang/String;)Ltv/periscope/model/r;

    move-result-object v0

    goto :goto_0
.end method
