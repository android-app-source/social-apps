.class public Lwo;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lwo$b;,
        Lwo$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Long;


# instance fields
.field private final b:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lwo;->a:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lauj;Lauj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;",
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lwo;->b:Lauj;

    .line 51
    iput-object p2, p0, Lwo;->c:Lauj;

    .line 52
    return-void
.end method

.method static synthetic a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lwo;->a:Ljava/lang/Long;

    return-object v0
.end method

.method public static a(Lcom/twitter/database/model/i;)Lwo;
    .locals 5

    .prologue
    .line 33
    new-instance v1, Lwp;

    new-instance v2, Lauo;

    const-class v0, Laxy;

    .line 35
    invoke-interface {p0, v0}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Laxy;

    invoke-interface {v0}, Laxy;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    new-instance v3, Lwo$b;

    invoke-direct {v3}, Lwo$b;-><init>()V

    invoke-direct {v2, v0, v3}, Lauo;-><init>(Lcom/twitter/database/model/l;Lcbr;)V

    const-string/jumbo v0, "_id"

    invoke-direct {v1, v2, v0}, Lwp;-><init>(Lauj;Ljava/lang/String;)V

    .line 38
    new-instance v2, Lwp;

    new-instance v3, Lauo;

    const-class v0, Layd;

    .line 41
    invoke-interface {p0, v0}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Layd;

    invoke-interface {v0}, Layd;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    new-instance v4, Lwo$a;

    invoke-direct {v4}, Lwo$a;-><init>()V

    invoke-direct {v3, v0, v4}, Lauo;-><init>(Lcom/twitter/database/model/l;Lcbr;)V

    const-string/jumbo v0, "moment_id"

    invoke-direct {v2, v3, v0}, Lwp;-><init>(Lauj;Ljava/lang/String;)V

    .line 44
    new-instance v0, Lwo;

    invoke-direct {v0, v1, v2}, Lwo;-><init>(Lauj;Lauj;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Long;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 57
    iget-object v0, p0, Lwo;->b:Lauj;

    .line 58
    invoke-interface {v0, p1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lwo;->c:Lauj;

    .line 59
    invoke-interface {v1, p1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v1

    invoke-virtual {v1, v2}, Lrx/c;->d(I)Lrx/c;

    move-result-object v1

    new-instance v2, Lwo$1;

    invoke-direct {v2, p0}, Lwo$1;-><init>(Lwo;)V

    .line 57
    invoke-static {v0, v1, v2}, Lrx/c;->b(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 24
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lwo;->a(Ljava/lang/Long;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lwo;->b:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 73
    iget-object v0, p0, Lwo;->c:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 74
    return-void
.end method
