.class public final Lcws;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final d:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcws;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lrx/f;

.field private final b:Lrx/f;

.field private final c:Lrx/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lcws;->d:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {}, Lcwo;->a()Lcwo;

    move-result-object v0

    invoke-virtual {v0}, Lcwo;->f()Lcwp;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcwp;->d()Lrx/f;

    move-result-object v1

    .line 80
    if-eqz v1, :cond_0

    .line 81
    iput-object v1, p0, Lcws;->a:Lrx/f;

    .line 86
    :goto_0
    invoke-virtual {v0}, Lcwp;->e()Lrx/f;

    move-result-object v1

    .line 87
    if-eqz v1, :cond_1

    .line 88
    iput-object v1, p0, Lcws;->b:Lrx/f;

    .line 93
    :goto_1
    invoke-virtual {v0}, Lcwp;->f()Lrx/f;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_2

    .line 95
    iput-object v0, p0, Lcws;->c:Lrx/f;

    .line 99
    :goto_2
    return-void

    .line 83
    :cond_0
    invoke-static {}, Lcwp;->a()Lrx/f;

    move-result-object v1

    iput-object v1, p0, Lcws;->a:Lrx/f;

    goto :goto_0

    .line 90
    :cond_1
    invoke-static {}, Lcwp;->b()Lrx/f;

    move-result-object v1

    iput-object v1, p0, Lcws;->b:Lrx/f;

    goto :goto_1

    .line 97
    :cond_2
    invoke-static {}, Lcwp;->c()Lrx/f;

    move-result-object v0

    iput-object v0, p0, Lcws;->c:Lrx/f;

    goto :goto_2
.end method

.method public static a()Lrx/f;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lrx/internal/schedulers/e;->b:Lrx/internal/schedulers/e;

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Executor;)Lrx/f;
    .locals 1

    .prologue
    .line 181
    new-instance v0, Lrx/internal/schedulers/c;

    invoke-direct {v0, p0}, Lrx/internal/schedulers/c;-><init>(Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public static b()Lrx/f;
    .locals 1

    .prologue
    .line 128
    invoke-static {}, Lcws;->f()Lcws;

    move-result-object v0

    iget-object v0, v0, Lcws;->c:Lrx/f;

    invoke-static {v0}, Lcwl;->c(Lrx/f;)Lrx/f;

    move-result-object v0

    return-object v0
.end method

.method public static c()Lrx/f;
    .locals 1

    .prologue
    .line 143
    invoke-static {}, Lcws;->f()Lcws;

    move-result-object v0

    iget-object v0, v0, Lcws;->a:Lrx/f;

    invoke-static {v0}, Lcwl;->a(Lrx/f;)Lrx/f;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lrx/f;
    .locals 1

    .prologue
    .line 160
    invoke-static {}, Lcws;->f()Lcws;

    move-result-object v0

    iget-object v0, v0, Lcws;->b:Lrx/f;

    invoke-static {v0}, Lcwl;->b(Lrx/f;)Lrx/f;

    move-result-object v0

    return-object v0
.end method

.method private static f()Lcws;
    .locals 3

    .prologue
    .line 62
    :goto_0
    sget-object v0, Lcws;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcws;

    .line 63
    if-eqz v0, :cond_1

    .line 68
    :cond_0
    return-object v0

    .line 66
    :cond_1
    new-instance v0, Lcws;

    invoke-direct {v0}, Lcws;-><init>()V

    .line 67
    sget-object v1, Lcws;->d:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 70
    invoke-virtual {v0}, Lcws;->e()V

    goto :goto_0
.end method


# virtual methods
.method declared-synchronized e()V
    .locals 1

    .prologue
    .line 242
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcws;->a:Lrx/f;

    instance-of v0, v0, Lrx/internal/schedulers/h;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcws;->a:Lrx/f;

    check-cast v0, Lrx/internal/schedulers/h;

    invoke-interface {v0}, Lrx/internal/schedulers/h;->d()V

    .line 245
    :cond_0
    iget-object v0, p0, Lcws;->b:Lrx/f;

    instance-of v0, v0, Lrx/internal/schedulers/h;

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Lcws;->b:Lrx/f;

    check-cast v0, Lrx/internal/schedulers/h;

    invoke-interface {v0}, Lrx/internal/schedulers/h;->d()V

    .line 248
    :cond_1
    iget-object v0, p0, Lcws;->c:Lrx/f;

    instance-of v0, v0, Lrx/internal/schedulers/h;

    if-eqz v0, :cond_2

    .line 249
    iget-object v0, p0, Lcws;->c:Lrx/f;

    check-cast v0, Lrx/internal/schedulers/h;

    invoke-interface {v0}, Lrx/internal/schedulers/h;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    :cond_2
    monitor-exit p0

    return-void

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
