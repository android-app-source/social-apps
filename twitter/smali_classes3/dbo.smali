.class public abstract Ldbo;
.super Ldbp;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private final a:Ltv/periscope/model/chat/Message;

.field private final b:Lcyw;

.field private final c:Ldae;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcyw;Ldae;Ltv/periscope/model/chat/Message;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p5, p6, p7}, Ldbp;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 32
    iput-object p4, p0, Ldbo;->a:Ltv/periscope/model/chat/Message;

    .line 33
    iput-object p2, p0, Ldbo;->b:Lcyw;

    .line 34
    iput-object p3, p0, Ldbo;->c:Ldae;

    .line 35
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 40
    invoke-virtual {p0, p1}, Ldbo;->b(Landroid/content/Context;)Landroid/view/View;

    move-result-object v9

    .line 42
    new-instance v10, Ltv/periscope/android/ui/chat/e;

    sget v0, Ltv/periscope/android/library/f$g;->chat_row:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {v10, v0, v6}, Ltv/periscope/android/ui/chat/e;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    .line 43
    new-instance v0, Ltv/periscope/android/ui/chat/d;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Ldbo;->b:Lcyw;

    invoke-interface {v2}, Lcyw;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ldbo;->b:Lcyw;

    .line 44
    invoke-interface {v3}, Lcyw;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Ldbo;->c:Ldae;

    iget-object v8, p0, Ldbo;->b:Lcyw;

    move v5, v4

    invoke-direct/range {v0 .. v8}, Ltv/periscope/android/ui/chat/d;-><init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;ZZLtv/periscope/android/ui/chat/a;Ldae;Lcyw;)V

    .line 45
    iget-object v1, p0, Ldbo;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v0, v10, v1, v4}, Ltv/periscope/android/ui/chat/d;->a(Ltv/periscope/android/ui/chat/e;Ltv/periscope/model/chat/Message;I)V

    .line 46
    iget-object v0, v10, Ltv/periscope/android/ui/chat/e;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$f;->ps__bg_chat_with_outline:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 48
    iget-object v0, v10, Ltv/periscope/android/ui/chat/e;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 50
    iget-object v0, v10, Ltv/periscope/android/ui/chat/e;->c:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 52
    return-object v9
.end method

.method protected abstract b(Landroid/content/Context;)Landroid/view/View;
.end method
