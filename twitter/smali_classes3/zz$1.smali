.class Lzz$1;
.super Lcqy;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lzz;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/Moment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqy",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/moments/Moment;

.field final synthetic b:Lcom/twitter/model/moments/Moment;

.field final synthetic c:Lzz;


# direct methods
.method constructor <init>(Lzz;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/Moment;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lzz$1;->c:Lzz;

    iput-object p2, p0, Lzz$1;->a:Lcom/twitter/model/moments/Moment;

    iput-object p3, p0, Lzz$1;->b:Lcom/twitter/model/moments/Moment;

    invoke-direct {p0}, Lcqy;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;)V
    .locals 7

    .prologue
    .line 69
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;-><init>()V

    iget-object v1, p0, Lzz$1;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    .line 70
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->c(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    .line 72
    new-instance v1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 73
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    iget-object v1, p0, Lzz$1;->b:Lcom/twitter/model/moments/Moment;

    iget-boolean v1, v1, Lcom/twitter/model/moments/Moment;->k:Z

    .line 74
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Ljava/lang/Boolean;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    iget-object v1, p0, Lzz$1;->b:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    .line 75
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    .line 76
    if-eqz p1, :cond_0

    .line 77
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->c(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 79
    :cond_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lzz$1;->c:Lzz;

    invoke-static {v2}, Lzz;->a(Lzz;)J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "moments:capsule:pivot:moment:open"

    aput-object v6, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    .line 81
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 80
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 79
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 82
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 66
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lzz$1;->a(Ljava/lang/Long;)V

    return-void
.end method
