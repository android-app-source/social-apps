.class Ltk;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lti;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lti",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ltl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ltl",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Lti$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lti$a",
            "<TT;>;"
        }
    .end annotation
.end field

.field private c:J

.field private d:Z

.field private e:Z


# direct methods
.method constructor <init>(Lti$a;JJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lti$a",
            "<TT;>;JJ)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Ltk;->b:Lti$a;

    .line 32
    new-instance v0, Ltl;

    iget-object v1, p0, Ltk;->b:Lti$a;

    move-wide v2, p2

    move-wide v4, p4

    invoke-direct/range {v0 .. v5}, Ltl;-><init>(Lti$a;JJ)V

    iput-object v0, p0, Ltk;->a:Ltl;

    .line 33
    return-void
.end method

.method static synthetic a(Ltk;)Ltl;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Ltk;->a:Ltl;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Ltk;->a:Ltl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltl;->a(Landroid/view/ViewGroup;)V

    .line 84
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 57
    if-ne p1, v1, :cond_1

    .line 58
    iput-boolean v1, p0, Ltk;->e:Z

    .line 59
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Ltk;->c:J

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 61
    iput-boolean v1, p0, Ltk;->d:Z

    goto :goto_0

    .line 62
    :cond_2
    if-nez p1, :cond_0

    .line 64
    iget-boolean v0, p0, Ltk;->d:Z

    if-eqz v0, :cond_3

    .line 65
    iget-wide v0, p0, Ltk;->c:J

    .line 69
    :goto_1
    iget-object v2, p0, Ltk;->a:Ltl;

    invoke-virtual {v2, v0, v1, v3}, Ltl;->a(JZ)Ljava/util/List;

    .line 70
    iput-boolean v3, p0, Ltk;->d:Z

    goto :goto_0

    .line 67
    :cond_3
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    goto :goto_1
.end method

.method public a(JJ)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 89
    iget-object v0, p0, Ltk;->b:Lti$a;

    invoke-interface {v0}, Lti$a;->a()Ljava/lang/String;

    move-result-object v0

    .line 91
    iget-object v1, p0, Ltk;->a:Ltl;

    .line 92
    invoke-virtual {v1, p3, p4, v3}, Ltl;->a(JZ)Ljava/util/List;

    move-result-object v1

    .line 93
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 94
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 95
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 94
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 96
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 98
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 37
    iget-boolean v0, p0, Ltk;->e:Z

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Ltk;->a:Ltl;

    invoke-virtual {v0, p1, p2}, Ltl;->a(Landroid/view/View;Ljava/lang/Object;)V

    .line 41
    iget-boolean v0, p0, Ltk;->d:Z

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Ltk;->a:Ltl;

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ltl;->a(JZ)Ljava/util/List;

    .line 45
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Ltk$1;

    invoke-direct {v1, p0, p1, p2}, Ltk$1;-><init>(Ltk;Landroid/view/View;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 53
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 76
    iget-object v0, p0, Ltk;->a:Ltl;

    invoke-virtual {v0, p1}, Ltl;->a(Landroid/view/ViewGroup;)V

    .line 77
    iget-object v0, p0, Ltk;->a:Ltl;

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ltl;->a(JZ)Ljava/util/List;

    .line 78
    return-void
.end method
