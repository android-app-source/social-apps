.class public final Lvl;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lvk;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lvm;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/media/selection/c;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lakr",
            "<",
            "Lcom/twitter/android/composer/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/model/timeline/ap;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lvl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lvl;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lcta",
            "<",
            "Lvm;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/media/selection/c;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;",
            "Lcta",
            "<",
            "Lakr",
            "<",
            "Lcom/twitter/android/composer/a;",
            ">;>;",
            "Lcta",
            "<",
            "Lcom/twitter/model/timeline/ap;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    sget-boolean v0, Lvl;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 47
    :cond_0
    iput-object p1, p0, Lvl;->b:Lcta;

    .line 48
    sget-boolean v0, Lvl;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 49
    :cond_1
    iput-object p2, p0, Lvl;->c:Lcta;

    .line 50
    sget-boolean v0, Lvl;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 51
    :cond_2
    iput-object p3, p0, Lvl;->d:Lcta;

    .line 52
    sget-boolean v0, Lvl;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 53
    :cond_3
    iput-object p4, p0, Lvl;->e:Lcta;

    .line 54
    sget-boolean v0, Lvl;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 55
    :cond_4
    iput-object p5, p0, Lvl;->f:Lcta;

    .line 56
    sget-boolean v0, Lvl;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 57
    :cond_5
    iput-object p6, p0, Lvl;->g:Lcta;

    .line 58
    sget-boolean v0, Lvl;->a:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 59
    :cond_6
    iput-object p7, p0, Lvl;->h:Lcta;

    .line 60
    sget-boolean v0, Lvl;->a:Z

    if-nez v0, :cond_7

    if-nez p8, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 61
    :cond_7
    iput-object p8, p0, Lvl;->i:Lcta;

    .line 62
    return-void
.end method

.method public static a(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lcta",
            "<",
            "Lvm;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/media/selection/c;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;",
            "Lcta",
            "<",
            "Lakr",
            "<",
            "Lcom/twitter/android/composer/a;",
            ">;>;",
            "Lcta",
            "<",
            "Lcom/twitter/model/timeline/ap;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lvk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Lvl;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lvl;-><init>(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lvk;
    .locals 9

    .prologue
    .line 66
    new-instance v0, Lvk;

    iget-object v1, p0, Lvl;->b:Lcta;

    .line 67
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lvl;->c:Lcta;

    .line 68
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lvm;

    iget-object v3, p0, Lvl;->d:Lcta;

    .line 69
    invoke-interface {v3}, Lcta;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/media/selection/c;

    iget-object v4, p0, Lvl;->e:Lcta;

    .line 70
    invoke-interface {v4}, Lcta;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/client/Session;

    iget-object v5, p0, Lvl;->f:Lcta;

    .line 71
    invoke-interface {v5}, Lcta;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/library/client/p;

    iget-object v6, p0, Lvl;->g:Lcta;

    .line 72
    invoke-interface {v6}, Lcta;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lakr;

    iget-object v7, p0, Lvl;->h:Lcta;

    .line 73
    invoke-interface {v7}, Lcta;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/twitter/model/timeline/ap;

    iget-object v8, p0, Lvl;->i:Lcta;

    .line 74
    invoke-interface {v8}, Lcta;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct/range {v0 .. v8}, Lvk;-><init>(Landroid/content/Context;Lvm;Lcom/twitter/android/media/selection/c;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Lakr;Lcom/twitter/model/timeline/ap;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 66
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lvl;->a()Lvk;

    move-result-object v0

    return-object v0
.end method
