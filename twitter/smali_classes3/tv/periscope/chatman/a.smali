.class public Ltv/periscope/chatman/a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/chatman/a$c;,
        Ltv/periscope/chatman/a$a;,
        Ltv/periscope/chatman/a$d;,
        Ltv/periscope/chatman/a$b;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/concurrent/ScheduledExecutorService;

.field private static final b:J


# instance fields
.field private final c:Ljava/util/concurrent/BlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingDeque",
            "<",
            "Ltv/periscope/chatman/api/WireMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ltv/periscope/chatman/a$b;

.field private final e:Ltv/periscope/chatman/model/a;

.field private final f:Ltv/periscope/chatman/api/HttpService;

.field private final g:Ltv/periscope/chatman/a$a;

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:Ltv/periscope/chatman/c;

.field private k:I

.field private l:I

.field private volatile m:Ljava/lang/String;

.field private volatile n:Z

.field private volatile o:Ltv/periscope/chatman/b;

.field private volatile p:Ltv/periscope/chatman/a$d;

.field private q:[I

.field private r:Ltv/periscope/chatman/api/HistoryRequest;

.field private s:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 57
    new-instance v0, Ltv/periscope/chatman/e;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ltv/periscope/chatman/e;-><init>(I)V

    sput-object v0, Ltv/periscope/chatman/a;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 59
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ltv/periscope/chatman/a;->b:J

    return-void
.end method

.method private constructor <init>(Ltv/periscope/chatman/a$b;Ltv/periscope/chatman/model/a;ILretrofit/RestAdapter$LogLevel;Ljava/lang/String;Ltv/periscope/chatman/c;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, Ltv/periscope/chatman/a;->c:Ljava/util/concurrent/BlockingDeque;

    .line 65
    new-instance v0, Ltv/periscope/chatman/a$a;

    invoke-direct {v0, p0}, Ltv/periscope/chatman/a$a;-><init>(Ltv/periscope/chatman/a;)V

    iput-object v0, p0, Ltv/periscope/chatman/a;->g:Ltv/periscope/chatman/a$a;

    .line 72
    iput-boolean v6, p0, Ltv/periscope/chatman/a;->n:Z

    .line 76
    const/4 v0, 0x0

    new-array v0, v0, [I

    iput-object v0, p0, Ltv/periscope/chatman/a;->q:[I

    .line 82
    iput-object p1, p0, Ltv/periscope/chatman/a;->d:Ltv/periscope/chatman/a$b;

    .line 83
    iput-object p2, p0, Ltv/periscope/chatman/a;->e:Ltv/periscope/chatman/model/a;

    .line 84
    new-instance v0, Ltv/periscope/chatman/api/HttpClient;

    sget-object v1, Ltv/periscope/chatman/a;->a:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v2, Ltv/periscope/chatman/a;->a:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {p2}, Ltv/periscope/chatman/model/a;->a()Ljava/lang/String;

    move-result-object v3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Ltv/periscope/chatman/api/HttpClient;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/lang/String;Lretrofit/RestAdapter$LogLevel;Ljava/lang/String;)V

    .line 85
    invoke-virtual {v0}, Ltv/periscope/chatman/api/HttpClient;->getService()Ltv/periscope/chatman/api/HttpService;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/chatman/a;->f:Ltv/periscope/chatman/api/HttpService;

    .line 86
    iput-boolean v6, p0, Ltv/periscope/chatman/a;->n:Z

    .line 87
    iput p3, p0, Ltv/periscope/chatman/a;->h:I

    .line 88
    iput-object p5, p0, Ltv/periscope/chatman/a;->i:Ljava/lang/String;

    .line 89
    iput-object p6, p0, Ltv/periscope/chatman/a;->j:Ltv/periscope/chatman/c;

    .line 90
    return-void
.end method

.method static synthetic a(Ltv/periscope/chatman/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Ltv/periscope/chatman/a;->m:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Ltv/periscope/chatman/a;)Ljava/util/concurrent/BlockingDeque;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ltv/periscope/chatman/a;->c:Ljava/util/concurrent/BlockingDeque;

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/chatman/a;Ltv/periscope/chatman/a$d;)Ltv/periscope/chatman/a$d;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Ltv/periscope/chatman/a;->p:Ltv/periscope/chatman/a$d;

    return-object p1
.end method

.method public static a(Ltv/periscope/chatman/a$b;Ljava/lang/String;Ljava/lang/String;ILretrofit/RestAdapter$LogLevel;Ljava/lang/String;Ltv/periscope/chatman/c;)Ltv/periscope/chatman/a;
    .locals 7

    .prologue
    .line 94
    invoke-static {p1}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "accessToken="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", endpoint="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_1
    new-instance v0, Ltv/periscope/chatman/a;

    invoke-static {p2, p1}, Ltv/periscope/chatman/model/a;->a(Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/chatman/model/a;

    move-result-object v2

    move-object v1, p0

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Ltv/periscope/chatman/a;-><init>(Ltv/periscope/chatman/a$b;Ltv/periscope/chatman/model/a;ILretrofit/RestAdapter$LogLevel;Ljava/lang/String;Ltv/periscope/chatman/c;)V

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/chatman/a;Ltv/periscope/chatman/b;)Ltv/periscope/chatman/b;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Ltv/periscope/chatman/a;->o:Ltv/periscope/chatman/b;

    return-object p1
.end method

.method static synthetic a(Ltv/periscope/chatman/a;Ltv/periscope/chatman/api/ControlMessage;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Ltv/periscope/chatman/a;->a(Ltv/periscope/chatman/api/ControlMessage;)V

    return-void
.end method

.method private a(Ltv/periscope/chatman/api/ControlMessage;)V
    .locals 5

    .prologue
    .line 145
    iget v0, p1, Ltv/periscope/chatman/api/ControlMessage;->bodyKind:I

    packed-switch v0, :pswitch_data_0

    .line 192
    :pswitch_0
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown control message, kind=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Ltv/periscope/chatman/api/ControlMessage;->bodyKind:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldcs;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :goto_0
    return-void

    .line 147
    :pswitch_1
    sget-object v0, Ldco;->a:Lcom/google/gson/e;

    iget-object v1, p1, Ltv/periscope/chatman/api/ControlMessage;->body:Ljava/lang/String;

    const-class v2, Ltv/periscope/chatman/api/ControlMessage$Join;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/ControlMessage$Join;

    .line 148
    invoke-static {}, Ltv/periscope/chatman/model/Join;->c()Ltv/periscope/chatman/model/Join$a;

    move-result-object v1

    iget-object v0, v0, Ltv/periscope/chatman/api/ControlMessage$Join;->room:Ljava/lang/String;

    .line 149
    invoke-virtual {v1, v0}, Ltv/periscope/chatman/model/Join$a;->a(Ljava/lang/String;)Ltv/periscope/chatman/model/Join$a;

    move-result-object v0

    iget-object v1, p1, Ltv/periscope/chatman/api/ControlMessage;->sender:Ltv/periscope/chatman/api/Sender;

    .line 150
    invoke-virtual {v0, v1}, Ltv/periscope/chatman/model/Join$a;->a(Ltv/periscope/chatman/api/Sender;)Ltv/periscope/chatman/model/Join$a;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Ltv/periscope/chatman/model/Join$a;->a()Ltv/periscope/chatman/model/Join;

    move-result-object v0

    .line 152
    iget-object v1, p0, Ltv/periscope/chatman/a;->d:Ltv/periscope/chatman/a$b;

    invoke-interface {v1, v0}, Ltv/periscope/chatman/a$b;->a(Ltv/periscope/chatman/model/Join;)V

    goto :goto_0

    .line 156
    :pswitch_2
    sget-object v0, Ldco;->a:Lcom/google/gson/e;

    iget-object v1, p1, Ltv/periscope/chatman/api/ControlMessage;->body:Ljava/lang/String;

    const-class v2, Ltv/periscope/chatman/api/ControlMessage$Leave;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/ControlMessage$Leave;

    .line 157
    invoke-static {}, Ltv/periscope/chatman/model/Leave;->c()Ltv/periscope/chatman/model/Leave$a;

    move-result-object v1

    iget-object v0, v0, Ltv/periscope/chatman/api/ControlMessage$Leave;->room:Ljava/lang/String;

    .line 158
    invoke-virtual {v1, v0}, Ltv/periscope/chatman/model/Leave$a;->a(Ljava/lang/String;)Ltv/periscope/chatman/model/Leave$a;

    move-result-object v0

    iget-object v1, p1, Ltv/periscope/chatman/api/ControlMessage;->sender:Ltv/periscope/chatman/api/Sender;

    .line 159
    invoke-virtual {v0, v1}, Ltv/periscope/chatman/model/Leave$a;->a(Ltv/periscope/chatman/api/Sender;)Ltv/periscope/chatman/model/Leave$a;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Ltv/periscope/chatman/model/Leave$a;->a()Ltv/periscope/chatman/model/Leave;

    move-result-object v0

    .line 161
    iget-object v1, p0, Ltv/periscope/chatman/a;->d:Ltv/periscope/chatman/a$b;

    invoke-interface {v1, v0}, Ltv/periscope/chatman/a$b;->a(Ltv/periscope/chatman/model/Leave;)V

    goto :goto_0

    .line 165
    :pswitch_3
    sget-object v0, Ldco;->a:Lcom/google/gson/e;

    iget-object v1, p1, Ltv/periscope/chatman/api/ControlMessage;->body:Ljava/lang/String;

    const-class v2, Ltv/periscope/chatman/api/ControlMessage$Presence;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/ControlMessage$Presence;

    .line 166
    invoke-static {}, Ltv/periscope/chatman/model/Presence;->c()Ltv/periscope/chatman/model/Presence$a;

    move-result-object v1

    iget v2, v0, Ltv/periscope/chatman/api/ControlMessage$Presence;->occupancy:I

    int-to-long v2, v2

    .line 167
    invoke-virtual {v1, v2, v3}, Ltv/periscope/chatman/model/Presence$a;->a(J)Ltv/periscope/chatman/model/Presence$a;

    move-result-object v1

    iget v0, v0, Ltv/periscope/chatman/api/ControlMessage$Presence;->totalParticipants:I

    int-to-long v2, v0

    .line 168
    invoke-virtual {v1, v2, v3}, Ltv/periscope/chatman/model/Presence$a;->b(J)Ltv/periscope/chatman/model/Presence$a;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Ltv/periscope/chatman/model/Presence$a;->a()Ltv/periscope/chatman/model/Presence;

    move-result-object v0

    .line 170
    iget-object v1, p0, Ltv/periscope/chatman/a;->d:Ltv/periscope/chatman/a$b;

    invoke-interface {v1, v0}, Ltv/periscope/chatman/a$b;->a(Ltv/periscope/chatman/model/Presence;)V

    goto :goto_0

    .line 174
    :pswitch_4
    sget-object v0, Ldco;->a:Lcom/google/gson/e;

    iget-object v1, p1, Ltv/periscope/chatman/api/ControlMessage;->body:Ljava/lang/String;

    const-class v2, Ltv/periscope/chatman/api/ControlMessage$Roster;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/ControlMessage$Roster;

    .line 175
    invoke-static {}, Ltv/periscope/chatman/model/Roster;->b()Ltv/periscope/chatman/model/Roster$a;

    move-result-object v1

    iget-object v0, v0, Ltv/periscope/chatman/api/ControlMessage$Roster;->occupants:Ljava/util/List;

    .line 176
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ltv/periscope/chatman/model/Roster$a;->a(Ljava/util/List;)Ltv/periscope/chatman/model/Roster$a;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Ltv/periscope/chatman/model/Roster$a;->a()Ltv/periscope/chatman/model/Roster;

    move-result-object v0

    .line 178
    iget-object v1, p0, Ltv/periscope/chatman/a;->d:Ltv/periscope/chatman/a$b;

    invoke-interface {v1, v0}, Ltv/periscope/chatman/a$b;->a(Ltv/periscope/chatman/model/Roster;)V

    goto/16 :goto_0

    .line 182
    :pswitch_5
    sget-object v0, Ldco;->a:Lcom/google/gson/e;

    iget-object v1, p1, Ltv/periscope/chatman/api/ControlMessage;->body:Ljava/lang/String;

    const-class v2, Ltv/periscope/chatman/api/ControlMessage$Ban;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/ControlMessage$Ban;

    .line 183
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, v0, Ltv/periscope/chatman/api/ControlMessage$Ban;->duration:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    long-to-int v1, v2

    .line 184
    invoke-static {}, Ltv/periscope/chatman/model/Ban;->c()Ltv/periscope/chatman/model/Ban$a;

    move-result-object v2

    iget v0, v0, Ltv/periscope/chatman/api/ControlMessage$Ban;->banType:I

    .line 185
    invoke-static {v0}, Ltv/periscope/model/chat/MessageType$SentenceType;->a(I)Ltv/periscope/model/chat/MessageType$SentenceType;

    move-result-object v0

    invoke-virtual {v2, v0}, Ltv/periscope/chatman/model/Ban$a;->a(Ltv/periscope/model/chat/MessageType$SentenceType;)Ltv/periscope/chatman/model/Ban$a;

    move-result-object v0

    .line 186
    invoke-virtual {v0, v1}, Ltv/periscope/chatman/model/Ban$a;->a(I)Ltv/periscope/chatman/model/Ban$a;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Ltv/periscope/chatman/model/Ban$a;->a()Ltv/periscope/chatman/model/Ban;

    move-result-object v0

    .line 188
    iget-object v1, p0, Ltv/periscope/chatman/a;->d:Ltv/periscope/chatman/a$b;

    invoke-interface {v1, v0}, Ltv/periscope/chatman/a$b;->a(Ltv/periscope/chatman/model/Ban;)V

    goto/16 :goto_0

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method static a(I)Z
    .locals 1

    .prologue
    .line 707
    const/16 v0, 0x10cc

    if-lt p0, v0, :cond_0

    const/16 v0, 0x112f

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Ltv/periscope/chatman/a;)Ltv/periscope/chatman/a$a;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ltv/periscope/chatman/a;->g:Ltv/periscope/chatman/a$a;

    return-object v0
.end method

.method static synthetic c(Ltv/periscope/chatman/a;)Ltv/periscope/chatman/a$b;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ltv/periscope/chatman/a;->d:Ltv/periscope/chatman/a$b;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 225
    iget-boolean v0, p0, Ltv/periscope/chatman/a;->n:Z

    if-nez v0, :cond_0

    .line 251
    :goto_0
    return-void

    .line 229
    :cond_0
    iget-object v0, p0, Ltv/periscope/chatman/a;->m:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 230
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "No room to leave. Never joined a room."

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 234
    :cond_1
    iget v0, p0, Ltv/periscope/chatman/a;->h:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_2

    .line 235
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "leave room="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " not allowed: cap="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/chatman/a;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 239
    :cond_2
    iget-object v0, p0, Ltv/periscope/chatman/a;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 240
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "not in room="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " to leave it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 241
    const-string/jumbo v1, "CM"

    const-string/jumbo v2, "leaveroom"

    invoke-static {v1, v2, v0}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 245
    :cond_3
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "queue leave room "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/chatman/a;->m:Ljava/lang/String;

    .line 248
    iget-object v0, p0, Ltv/periscope/chatman/a;->c:Ljava/util/concurrent/BlockingDeque;

    new-instance v1, Ltv/periscope/chatman/api/ControlMessage$Leave;

    invoke-direct {v1, p1}, Ltv/periscope/chatman/api/ControlMessage$Leave;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ltv/periscope/chatman/api/ControlMessage;->create(Ltv/periscope/chatman/api/Kind;)Ltv/periscope/chatman/api/ControlMessage;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/chatman/api/WireMessage;->create(Ltv/periscope/chatman/api/Kind;)Ltv/periscope/chatman/api/WireMessage;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingDeque;->offer(Ljava/lang/Object;)Z

    .line 250
    iget-object v0, p0, Ltv/periscope/chatman/a;->d:Ltv/periscope/chatman/a$b;

    invoke-interface {v0, p1}, Ltv/periscope/chatman/a$b;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic d(Ltv/periscope/chatman/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ltv/periscope/chatman/a;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Ltv/periscope/chatman/a;->a:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method static synthetic e(Ltv/periscope/chatman/a;)Ltv/periscope/chatman/a$d;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ltv/periscope/chatman/a;->p:Ltv/periscope/chatman/a$d;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 111
    iget-boolean v0, p0, Ltv/periscope/chatman/a;->n:Z

    if-nez v0, :cond_1

    .line 112
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "client already killed"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "kill client"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/chatman/a;->n:Z

    .line 118
    invoke-virtual {p0}, Ltv/periscope/chatman/a;->d()V

    .line 119
    iget-object v0, p0, Ltv/periscope/chatman/a;->p:Ltv/periscope/chatman/a$d;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Ltv/periscope/chatman/a;->p:Ltv/periscope/chatman/a$d;

    invoke-virtual {v0}, Ltv/periscope/chatman/a$d;->b()V

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/chatman/a;->p:Ltv/periscope/chatman/a$d;

    goto :goto_0
.end method

.method static synthetic f(Ltv/periscope/chatman/a;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Ltv/periscope/chatman/a;->n:Z

    return v0
.end method

.method static synthetic g(Ltv/periscope/chatman/a;)I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Ltv/periscope/chatman/a;->h:I

    return v0
.end method

.method static synthetic h(Ltv/periscope/chatman/a;)I
    .locals 2

    .prologue
    .line 46
    iget v0, p0, Ltv/periscope/chatman/a;->k:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Ltv/periscope/chatman/a;->k:I

    return v0
.end method

.method static synthetic i(Ltv/periscope/chatman/a;)I
    .locals 2

    .prologue
    .line 46
    iget v0, p0, Ltv/periscope/chatman/a;->l:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Ltv/periscope/chatman/a;->l:I

    return v0
.end method

.method static synthetic j(Ltv/periscope/chatman/a;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ltv/periscope/chatman/a;->f()V

    return-void
.end method

.method static synthetic k(Ltv/periscope/chatman/a;)Ltv/periscope/chatman/api/HttpService;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ltv/periscope/chatman/a;->f:Ltv/periscope/chatman/api/HttpService;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 103
    iget-boolean v0, p0, Ltv/periscope/chatman/a;->n:Z

    if-nez v0, :cond_0

    .line 104
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "no leave sent. already disconnected"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_0
    iget-object v0, p0, Ltv/periscope/chatman/a;->m:Ljava/lang/String;

    invoke-direct {p0, v0}, Ltv/periscope/chatman/a;->c(Ljava/lang/String;)V

    .line 107
    invoke-direct {p0}, Ltv/periscope/chatman/a;->f()V

    .line 108
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 201
    iget-boolean v0, p0, Ltv/periscope/chatman/a;->n:Z

    if-nez v0, :cond_0

    .line 222
    :goto_0
    return-void

    .line 205
    :cond_0
    iget v0, p0, Ltv/periscope/chatman/a;->h:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_1

    .line 206
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "send not allowed: cap="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/chatman/a;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 210
    :cond_1
    iget-object v0, p0, Ltv/periscope/chatman/a;->m:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 211
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "no room to send message"

    invoke-static {v0, v1}, Ldcs;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 216
    :cond_2
    iget-object v0, p0, Ltv/periscope/chatman/a;->c:Ljava/util/concurrent/BlockingDeque;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingDeque;->size()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_3

    .line 217
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "queue full, drop message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldcs;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 221
    :cond_3
    iget-object v0, p0, Ltv/periscope/chatman/a;->c:Ljava/util/concurrent/BlockingDeque;

    new-instance v1, Ltv/periscope/chatman/api/ChatMessage;

    iget-object v2, p0, Ltv/periscope/chatman/a;->m:Ljava/lang/String;

    sget-object v3, Ldco;->a:Lcom/google/gson/e;

    invoke-virtual {v3, p1}, Lcom/google/gson/e;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, p2}, Ltv/periscope/chatman/api/ChatMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Ltv/periscope/chatman/api/WireMessage;->create(Ltv/periscope/chatman/api/Kind;)Ltv/periscope/chatman/api/WireMessage;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingDeque;->offer(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 126
    iget v0, p0, Ltv/periscope/chatman/a;->h:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    .line 127
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "connect not allowed: cap="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/chatman/a;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Ltv/periscope/chatman/a;->o:Ltv/periscope/chatman/b;

    if-eqz v0, :cond_1

    .line 132
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "already connecting"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 136
    :cond_1
    iget-object v0, p0, Ltv/periscope/chatman/a;->p:Ltv/periscope/chatman/a$d;

    if-eqz v0, :cond_2

    .line 137
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "already connected"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :cond_2
    iget-object v0, p0, Ltv/periscope/chatman/a;->j:Ltv/periscope/chatman/c;

    iget-object v1, p0, Ltv/periscope/chatman/a;->e:Ltv/periscope/chatman/model/a;

    sget-object v3, Ltv/periscope/chatman/a;->a:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v4, p0, Ltv/periscope/chatman/a;->g:Ltv/periscope/chatman/a$a;

    iget-object v5, p0, Ltv/periscope/chatman/a;->i:Ljava/lang/String;

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Ltv/periscope/chatman/c;->a(Ltv/periscope/chatman/model/a;Ljava/lang/String;Ljava/util/concurrent/Executor;Ltv/periscope/chatman/b$a;Ljava/lang/String;)Ltv/periscope/chatman/b;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/chatman/a;->o:Ltv/periscope/chatman/b;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 8

    .prologue
    .line 272
    iget v0, p0, Ltv/periscope/chatman/a;->h:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    .line 273
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "history read not allowed. cap="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/chatman/a;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    iget-object v0, p0, Ltv/periscope/chatman/a;->f:Ltv/periscope/chatman/api/HttpService;

    if-eqz v0, :cond_0

    .line 281
    invoke-static {p1}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    iget-object v6, p0, Ltv/periscope/chatman/a;->q:[I

    monitor-enter v6

    .line 287
    :try_start_0
    iget-object v0, p0, Ltv/periscope/chatman/a;->s:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ltv/periscope/chatman/a;->s:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_2

    .line 288
    iget-object v0, p0, Ltv/periscope/chatman/a;->s:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 290
    :cond_2
    new-instance v0, Ltv/periscope/chatman/api/HistoryRequest;

    const/16 v1, 0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Ltv/periscope/chatman/api/HistoryRequest;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/Integer;)V

    iput-object v0, p0, Ltv/periscope/chatman/a;->r:Ltv/periscope/chatman/api/HistoryRequest;

    .line 291
    sget-object v0, Ltv/periscope/chatman/a;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Ltv/periscope/chatman/a$c;

    sget-wide v2, Ltv/periscope/chatman/a;->b:J

    iget-object v4, p0, Ltv/periscope/chatman/a;->r:Ltv/periscope/chatman/api/HistoryRequest;

    invoke-direct {v1, p0, v2, v3, v4}, Ltv/periscope/chatman/a$c;-><init>(Ltv/periscope/chatman/a;JLtv/periscope/chatman/api/HistoryRequest;)V

    const-wide/16 v2, 0x0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/chatman/a;->s:Ljava/util/concurrent/ScheduledFuture;

    .line 294
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Ltv/periscope/chatman/api/HistoryRequest;)Z
    .locals 2

    .prologue
    .line 688
    iget-object v1, p0, Ltv/periscope/chatman/a;->q:[I

    monitor-enter v1

    .line 689
    :try_start_0
    iget-object v0, p0, Ltv/periscope/chatman/a;->r:Ltv/periscope/chatman/api/HistoryRequest;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 690
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 298
    iget v0, p0, Ltv/periscope/chatman/a;->k:I

    return v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 254
    iget-boolean v0, p0, Ltv/periscope/chatman/a;->n:Z

    if-nez v0, :cond_0

    .line 269
    :goto_0
    return-void

    .line 258
    :cond_0
    iget v0, p0, Ltv/periscope/chatman/a;->h:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    .line 259
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "roster read not allowed. cap="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/chatman/a;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 263
    :cond_1
    iget-object v0, p0, Ltv/periscope/chatman/a;->m:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 264
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "roster message before joining a room"

    invoke-static {v0, v1}, Ldcs;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 268
    :cond_2
    iget-object v0, p0, Ltv/periscope/chatman/a;->c:Ljava/util/concurrent/BlockingDeque;

    new-instance v1, Ltv/periscope/chatman/api/ControlMessage$Roster;

    invoke-direct {v1, p1}, Ltv/periscope/chatman/api/ControlMessage$Roster;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ltv/periscope/chatman/api/ControlMessage;->create(Ltv/periscope/chatman/api/Kind;)Ltv/periscope/chatman/api/ControlMessage;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/chatman/api/WireMessage;->create(Ltv/periscope/chatman/api/Kind;)Ltv/periscope/chatman/api/WireMessage;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingDeque;->offer(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method b(Ltv/periscope/chatman/api/HistoryRequest;)V
    .locals 2

    .prologue
    .line 698
    iget-object v1, p0, Ltv/periscope/chatman/a;->q:[I

    monitor-enter v1

    .line 699
    :try_start_0
    iget-object v0, p0, Ltv/periscope/chatman/a;->r:Ltv/periscope/chatman/api/HistoryRequest;

    if-ne v0, p1, :cond_0

    .line 700
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/chatman/a;->r:Ltv/periscope/chatman/api/HistoryRequest;

    .line 701
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/chatman/a;->s:Ljava/util/concurrent/ScheduledFuture;

    .line 703
    :cond_0
    monitor-exit v1

    .line 704
    return-void

    .line 703
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 302
    iget v0, p0, Ltv/periscope/chatman/a;->l:I

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Ltv/periscope/chatman/a;->o:Ltv/periscope/chatman/b;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Ltv/periscope/chatman/a;->o:Ltv/periscope/chatman/b;

    invoke-interface {v0}, Ltv/periscope/chatman/b;->b()V

    .line 312
    :cond_0
    return-void
.end method
