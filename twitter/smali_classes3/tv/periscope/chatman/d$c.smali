.class Ltv/periscope/chatman/d$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lokhttp3/ws/WebSocketListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/chatman/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/chatman/d;


# direct methods
.method constructor <init>(Ltv/periscope/chatman/d;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClose(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 324
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onclose code="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", reason="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", ws="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v2}, Ltv/periscope/chatman/d;->g(Ltv/periscope/chatman/d;)Lokhttp3/ws/WebSocket;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldcs;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v0}, Ltv/periscope/chatman/d;->g(Ltv/periscope/chatman/d;)Lokhttp3/ws/WebSocket;

    move-result-object v0

    if-nez v0, :cond_0

    .line 332
    :goto_0
    return-void

    .line 330
    :cond_0
    iget-object v0, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ltv/periscope/chatman/d;->a(Ltv/periscope/chatman/d;Lokhttp3/ws/WebSocket;)Lokhttp3/ws/WebSocket;

    .line 331
    iget-object v0, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v0}, Ltv/periscope/chatman/d;->d(Ltv/periscope/chatman/d;)Ltv/periscope/chatman/b$a;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/chatman/b$a;->a(I)V

    goto :goto_0
.end method

.method public onFailure(Ljava/io/IOException;Lokhttp3/Response;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 336
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "socket i/o failure, ws="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v2}, Ltv/periscope/chatman/d;->g(Ltv/periscope/chatman/d;)Lokhttp3/ws/WebSocket;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Ldcs;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 339
    iget-object v0, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v0}, Ltv/periscope/chatman/d;->e(Ltv/periscope/chatman/d;)Ltv/periscope/chatman/d$a;

    move-result-object v0

    .line 340
    iget-object v1, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v1, v3}, Ltv/periscope/chatman/d;->a(Ltv/periscope/chatman/d;Ltv/periscope/chatman/d$a;)Ltv/periscope/chatman/d$a;

    .line 341
    if-eqz v0, :cond_0

    .line 342
    invoke-virtual {v0, p1}, Ltv/periscope/chatman/d$a;->a(Ljava/lang/Throwable;)V

    .line 345
    :cond_0
    iget-object v0, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v0}, Ltv/periscope/chatman/d;->g(Ltv/periscope/chatman/d;)Lokhttp3/ws/WebSocket;

    move-result-object v0

    if-nez v0, :cond_1

    .line 357
    :goto_0
    return-void

    .line 349
    :cond_1
    if-eqz p2, :cond_2

    .line 350
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Connection Failed {Code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lokhttp3/Response;->code()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Ldcs;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 354
    :goto_1
    iget-object v0, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v0, v3}, Ltv/periscope/chatman/d;->a(Ltv/periscope/chatman/d;Lokhttp3/ws/WebSocket;)Lokhttp3/ws/WebSocket;

    .line 356
    iget-object v0, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v0}, Ltv/periscope/chatman/d;->d(Ltv/periscope/chatman/d;)Ltv/periscope/chatman/b$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-interface {v0, v1}, Ltv/periscope/chatman/b$a;->a(Ltv/periscope/chatman/b;)V

    goto :goto_0

    .line 352
    :cond_2
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "Connection Failed"

    invoke-static {v0, v1, p1}, Ldcs;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public onMessage(Lokhttp3/ResponseBody;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 287
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->contentType()Lokhttp3/MediaType;

    move-result-object v0

    .line 288
    const-string/jumbo v1, "CM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "got message "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :try_start_0
    sget-object v1, Lokhttp3/ws/WebSocket;->TEXT:Lokhttp3/MediaType;

    if-eq v0, v1, :cond_0

    .line 291
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "unexpected binary message"

    invoke-static {v0, v1}, Ldcs;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "unexpected binary message"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lokhttp3/ResponseBody;->close()V

    throw v0

    .line 294
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->source()Lokio/e;

    move-result-object v0

    invoke-interface {v0}, Lokio/e;->r()Ljava/lang/String;

    move-result-object v1

    .line 295
    const-string/jumbo v0, "CM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "message payload "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297
    :try_start_2
    sget-object v0, Ldco;->a:Lcom/google/gson/e;

    const-class v2, Ltv/periscope/chatman/api/WireMessage;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/WireMessage;

    .line 298
    iget-object v2, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v2}, Ltv/periscope/chatman/d;->d(Ltv/periscope/chatman/d;)Ltv/periscope/chatman/b$a;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Ltv/periscope/chatman/b$a;->a(Ltv/periscope/chatman/api/WireMessage;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 303
    :goto_0
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->close()V

    .line 305
    return-void

    .line 299
    :catch_0
    move-exception v0

    .line 300
    :try_start_3
    const-string/jumbo v1, "CM"

    const-string/jumbo v2, "decode message error"

    invoke-static {v1, v2, v0}, Ldcs;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public onOpen(Lokhttp3/ws/WebSocket;Lokhttp3/Response;)V
    .locals 3

    .prologue
    .line 277
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "websocket opened"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v0, p1}, Ltv/periscope/chatman/d;->a(Ltv/periscope/chatman/d;Lokhttp3/ws/WebSocket;)Lokhttp3/ws/WebSocket;

    .line 280
    iget-object v0, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v0}, Ltv/periscope/chatman/d;->e(Ltv/periscope/chatman/d;)Ltv/periscope/chatman/d$a;

    move-result-object v0

    .line 281
    iget-object v1, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ltv/periscope/chatman/d;->a(Ltv/periscope/chatman/d;Ltv/periscope/chatman/d$a;)Ltv/periscope/chatman/d$a;

    .line 282
    invoke-virtual {v0}, Ltv/periscope/chatman/d$a;->run()V

    .line 283
    return-void
.end method

.method public onPong(Lokio/c;)V
    .locals 6

    .prologue
    .line 309
    if-nez p1, :cond_0

    .line 310
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "unexpected pong with no payload"

    invoke-static {v0, v1}, Ldcs;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    :goto_0
    return-void

    .line 313
    :cond_0
    invoke-virtual {p1}, Lokio/c;->l()J

    move-result-wide v0

    .line 314
    iget-object v2, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v2}, Ltv/periscope/chatman/d;->f(Ltv/periscope/chatman/d;)J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 315
    const-string/jumbo v2, "CM"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unexpected pong got="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", want="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    invoke-static {v1}, Ltv/periscope/chatman/d;->f(Ltv/periscope/chatman/d;)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ldcs;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 318
    :cond_1
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "<--- pong"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    iget-object v0, p0, Ltv/periscope/chatman/d$c;->a:Ltv/periscope/chatman/d;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Ltv/periscope/chatman/d;->a(Ltv/periscope/chatman/d;J)J

    goto :goto_0
.end method
