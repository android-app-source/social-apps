.class Ltv/periscope/chatman/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/chatman/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/chatman/d$c;,
        Ltv/periscope/chatman/d$b;,
        Ltv/periscope/chatman/d$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field private final c:Ltv/periscope/chatman/b$a;

.field private final d:Ltv/periscope/chatman/d$c;

.field private final e:Ljava/lang/String;

.field private volatile f:Lokhttp3/ws/WebSocket;

.field private volatile g:Ltv/periscope/chatman/d$a;

.field private volatile h:Z

.field private volatile i:J

.field private volatile j:Z


# direct methods
.method private constructor <init>(Ltv/periscope/chatman/b$a;Ltv/periscope/chatman/model/a;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Ltv/periscope/chatman/d;->c:Ltv/periscope/chatman/b$a;

    .line 56
    new-instance v0, Ltv/periscope/chatman/d$c;

    invoke-direct {v0, p0}, Ltv/periscope/chatman/d$c;-><init>(Ltv/periscope/chatman/d;)V

    iput-object v0, p0, Ltv/periscope/chatman/d;->d:Ltv/periscope/chatman/d$c;

    .line 57
    invoke-virtual {p2}, Ltv/periscope/chatman/model/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/chatman/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/chatman/d;->a:Ljava/lang/String;

    .line 58
    invoke-virtual {p2}, Ltv/periscope/chatman/model/a;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/chatman/d;->b:Ljava/lang/String;

    .line 59
    iput-object p3, p0, Ltv/periscope/chatman/d;->e:Ljava/lang/String;

    .line 60
    return-void
.end method

.method static synthetic a(Ltv/periscope/chatman/d;J)J
    .locals 1

    .prologue
    .line 36
    iput-wide p1, p0, Ltv/periscope/chatman/d;->i:J

    return-wide p1
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    const-string/jumbo v0, "127.0.0.1:8088"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const-string/jumbo p0, "10.0.2.2:8088"

    .line 75
    :cond_0
    const-string/jumbo v0, "http://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ws://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "http://"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    .line 77
    :cond_1
    const-string/jumbo v0, "https://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "wss://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "https://"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 80
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ws://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/chatman/d;Lokhttp3/ws/WebSocket;)Lokhttp3/ws/WebSocket;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Ltv/periscope/chatman/d;->f:Lokhttp3/ws/WebSocket;

    return-object p1
.end method

.method static synthetic a(Ltv/periscope/chatman/d;Ltv/periscope/chatman/d$a;)Ltv/periscope/chatman/d$a;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Ltv/periscope/chatman/d;->g:Ltv/periscope/chatman/d$a;

    return-object p1
.end method

.method public static a(Ltv/periscope/chatman/model/a;Ljava/lang/String;Ljava/util/concurrent/Executor;Ltv/periscope/chatman/b$a;Ljava/lang/String;)Ltv/periscope/chatman/d;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ltv/periscope/chatman/d;

    invoke-direct {v0, p3, p0, p4}, Ltv/periscope/chatman/d;-><init>(Ltv/periscope/chatman/b$a;Ltv/periscope/chatman/model/a;Ljava/lang/String;)V

    .line 65
    new-instance v1, Ltv/periscope/chatman/d$b;

    invoke-direct {v1, v0, p1}, Ltv/periscope/chatman/d$b;-><init>(Ltv/periscope/chatman/d;Ljava/lang/String;)V

    invoke-interface {p2, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 66
    return-object v0
.end method

.method static synthetic a(Ltv/periscope/chatman/d;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Ltv/periscope/chatman/d;->j:Z

    return v0
.end method

.method static synthetic a(Ltv/periscope/chatman/d;Z)Z
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Ltv/periscope/chatman/d;->h:Z

    return p1
.end method

.method static synthetic b(Ltv/periscope/chatman/d;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Ltv/periscope/chatman/d;->h:Z

    return v0
.end method

.method static synthetic c(Ltv/periscope/chatman/d;)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ltv/periscope/chatman/d;->d()Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method private d()Ljava/util/concurrent/Future;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0xa

    .line 100
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "open ep="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/chatman/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    new-instance v0, Ltv/periscope/chatman/d$a;

    new-instance v1, Ltv/periscope/chatman/d$1;

    invoke-direct {v1, p0}, Ltv/periscope/chatman/d$1;-><init>(Ltv/periscope/chatman/d;)V

    invoke-direct {v0, v1}, Ltv/periscope/chatman/d$a;-><init>(Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, Ltv/periscope/chatman/d;->g:Ltv/periscope/chatman/d$a;

    .line 109
    new-instance v0, Lokhttp3/OkHttpClient;

    invoke-direct {v0}, Lokhttp3/OkHttpClient;-><init>()V

    invoke-virtual {v0}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 110
    invoke-static {}, Ldcn;->a()Lokhttp3/CertificatePinner;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->certificatePinner(Lokhttp3/CertificatePinner;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 111
    invoke-virtual {v0, v4, v5, v1}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    const-wide/16 v2, 0x3c

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 112
    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 113
    invoke-virtual {v0, v4, v5, v1}, Lokhttp3/OkHttpClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    .line 115
    new-instance v1, Lokhttp3/Request$Builder;

    invoke-direct {v1}, Lokhttp3/Request$Builder;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Ltv/periscope/chatman/d;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/chatapi/v1/chatnow"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 116
    invoke-virtual {v1, v2}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v1

    const-string/jumbo v2, "User-Agent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "ChatMan/1 (Android) "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Ltv/periscope/chatman/d;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 117
    invoke-virtual {v1, v2, v3}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v1

    .line 118
    invoke-virtual {v1}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v1

    .line 119
    invoke-static {v0, v1}, Lokhttp3/ws/WebSocketCall;->create(Lokhttp3/OkHttpClient;Lokhttp3/Request;)Lokhttp3/ws/WebSocketCall;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/chatman/d;->d:Ltv/periscope/chatman/d$c;

    invoke-virtual {v1, v2}, Lokhttp3/ws/WebSocketCall;->enqueue(Lokhttp3/ws/WebSocketListener;)V

    .line 120
    invoke-virtual {v0}, Lokhttp3/OkHttpClient;->dispatcher()Lokhttp3/Dispatcher;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Dispatcher;->executorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 122
    iget-object v0, p0, Ltv/periscope/chatman/d;->g:Ltv/periscope/chatman/d$a;

    return-object v0
.end method

.method static synthetic d(Ltv/periscope/chatman/d;)Ltv/periscope/chatman/b$a;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Ltv/periscope/chatman/d;->c:Ltv/periscope/chatman/b$a;

    return-object v0
.end method

.method static synthetic e(Ltv/periscope/chatman/d;)Ltv/periscope/chatman/d$a;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Ltv/periscope/chatman/d;->g:Ltv/periscope/chatman/d$a;

    return-object v0
.end method

.method static synthetic f(Ltv/periscope/chatman/d;)J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Ltv/periscope/chatman/d;->i:J

    return-wide v0
.end method

.method static synthetic g(Ltv/periscope/chatman/d;)Lokhttp3/ws/WebSocket;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Ltv/periscope/chatman/d;->f:Lokhttp3/ws/WebSocket;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Ltv/periscope/chatman/d;->f:Lokhttp3/ws/WebSocket;

    if-nez v0, :cond_0

    .line 162
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "ping error, socket closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_0
    iget-wide v0, p0, Ltv/periscope/chatman/d;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 165
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "no pong for last ping"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 169
    iget-object v2, p0, Ltv/periscope/chatman/d;->f:Lokhttp3/ws/WebSocket;

    new-instance v3, Lokio/c;

    invoke-direct {v3}, Lokio/c;-><init>()V

    invoke-virtual {v3, v0, v1}, Lokio/c;->i(J)Lokio/c;

    move-result-object v3

    invoke-interface {v2, v3}, Lokhttp3/ws/WebSocket;->sendPing(Lokio/c;)V

    .line 170
    iput-wide v0, p0, Ltv/periscope/chatman/d;->i:J

    .line 172
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "---> ping"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method public a(Ltv/periscope/chatman/api/WireMessage;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Ltv/periscope/chatman/d;->f:Lokhttp3/ws/WebSocket;

    .line 145
    if-nez v0, :cond_0

    .line 146
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "websocket closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_0
    sget-object v1, Ldco;->a:Lcom/google/gson/e;

    invoke-virtual {v1, p1}, Lcom/google/gson/e;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 149
    const-string/jumbo v2, "CM"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "send json "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :try_start_0
    sget-object v2, Lokhttp3/ws/WebSocket;->TEXT:Lokhttp3/MediaType;

    invoke-static {v2, v1}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v1

    invoke-interface {v0, v1}, Lokhttp3/ws/WebSocket;->sendMessage(Lokhttp3/RequestBody;)V

    .line 152
    iget-object v0, p0, Ltv/periscope/chatman/d;->c:Ltv/periscope/chatman/b$a;

    invoke-interface {v0, p1}, Ltv/periscope/chatman/b$a;->a(Ltv/periscope/chatman/api/WireMessage;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    :goto_0
    return-void

    .line 153
    :catch_0
    move-exception v0

    .line 155
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "websocket is closed before sendmessage"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/chatman/d;->j:Z

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/chatman/d;->h:Z

    .line 91
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Ltv/periscope/chatman/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Ltv/periscope/chatman/d;->f:Lokhttp3/ws/WebSocket;

    .line 128
    if-nez v0, :cond_0

    .line 129
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "already closed"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :goto_0
    return-void

    .line 132
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Ltv/periscope/chatman/d;->f:Lokhttp3/ws/WebSocket;

    .line 133
    const-string/jumbo v1, "CM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "close by self ws="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const/16 v1, 0x3e8

    :try_start_0
    const-string/jumbo v2, "close by self"

    invoke-interface {v0, v1, v2}, Lokhttp3/ws/WebSocket;->close(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 136
    :catch_0
    move-exception v0

    .line 138
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "the underlying websocket is already closed"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
