.class Ltv/periscope/chatman/a$c;
.super Ldcp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/chatman/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldcp",
        "<",
        "Ltv/periscope/chatman/api/HttpResponse",
        "<",
        "Ltv/periscope/chatman/api/HistoryResponse;",
        "Lretrofit/RetrofitError;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/chatman/a;

.field private final b:J

.field private final c:Ltv/periscope/chatman/api/HistoryRequest;


# direct methods
.method protected constructor <init>(Ltv/periscope/chatman/a;JLtv/periscope/chatman/api/HistoryRequest;)V
    .locals 4

    .prologue
    .line 569
    iput-object p1, p0, Ltv/periscope/chatman/a$c;->a:Ltv/periscope/chatman/a;

    .line 570
    invoke-direct {p0, p2, p3}, Ldcp;-><init>(J)V

    .line 565
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/32 v2, 0x58168980

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/chatman/a$c;->b:J

    .line 571
    iput-object p4, p0, Ltv/periscope/chatman/a$c;->c:Ltv/periscope/chatman/api/HistoryRequest;

    .line 572
    return-void
.end method


# virtual methods
.method protected a()Ltv/periscope/chatman/api/HttpResponse;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ltv/periscope/chatman/api/HttpResponse",
            "<",
            "Ltv/periscope/chatman/api/HistoryResponse;",
            "Lretrofit/RetrofitError;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 576
    iget-object v1, p0, Ltv/periscope/chatman/a$c;->a:Ltv/periscope/chatman/a;

    iget-object v2, p0, Ltv/periscope/chatman/a$c;->c:Ltv/periscope/chatman/api/HistoryRequest;

    invoke-virtual {v1, v2}, Ltv/periscope/chatman/a;->a(Ltv/periscope/chatman/api/HistoryRequest;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 577
    const-string/jumbo v1, "CM"

    const-string/jumbo v2, "This history request is no longer in progress, returning early from execute"

    invoke-static {v1, v2}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    new-instance v1, Ltv/periscope/chatman/api/HttpResponse;

    invoke-direct {v1, v0, v0}, Ltv/periscope/chatman/api/HttpResponse;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    .line 588
    :goto_0
    return-object v0

    .line 584
    :cond_0
    :try_start_0
    iget-object v1, p0, Ltv/periscope/chatman/a$c;->a:Ltv/periscope/chatman/a;

    invoke-static {v1}, Ltv/periscope/chatman/a;->k(Ltv/periscope/chatman/a;)Ltv/periscope/chatman/api/HttpService;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/chatman/a$c;->c:Ltv/periscope/chatman/api/HistoryRequest;

    invoke-interface {v1, v2}, Ltv/periscope/chatman/api/HttpService;->history(Ltv/periscope/chatman/api/HistoryRequest;)Ltv/periscope/chatman/api/HistoryResponse;
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 588
    :goto_1
    new-instance v2, Ltv/periscope/chatman/api/HttpResponse;

    invoke-direct {v2, v1, v0}, Ltv/periscope/chatman/api/HttpResponse;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v2

    goto :goto_0

    .line 585
    :catch_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    .line 586
    goto :goto_1
.end method

.method protected a(Ltv/periscope/chatman/api/HttpResponse;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/chatman/api/HttpResponse",
            "<",
            "Ltv/periscope/chatman/api/HistoryResponse;",
            "Lretrofit/RetrofitError;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 593
    iget-object v0, p0, Ltv/periscope/chatman/a$c;->a:Ltv/periscope/chatman/a;

    iget-object v2, p0, Ltv/periscope/chatman/a$c;->c:Ltv/periscope/chatman/api/HistoryRequest;

    invoke-virtual {v0, v2}, Ltv/periscope/chatman/a;->a(Ltv/periscope/chatman/api/HistoryRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 594
    const-string/jumbo v0, "CM"

    const-string/jumbo v2, "This history request is no longer in progress, returning early from canRetry"

    invoke-static {v0, v2}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    :goto_0
    return v1

    .line 598
    :cond_0
    iget-object v0, p1, Ltv/periscope/chatman/api/HttpResponse;->errorResponse:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p1, Ltv/periscope/chatman/api/HttpResponse;->errorResponse:Ljava/lang/Object;

    check-cast v0, Lretrofit/RetrofitError;

    invoke-static {v0}, Lcwz;->a(Lretrofit/RetrofitError;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 599
    :goto_1
    const-string/jumbo v1, "CM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "History call canRetry "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 600
    goto :goto_0

    :cond_1
    move v0, v1

    .line 598
    goto :goto_1
.end method

.method protected b(Ltv/periscope/chatman/api/HttpResponse;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/chatman/api/HttpResponse",
            "<",
            "Ltv/periscope/chatman/api/HistoryResponse;",
            "Lretrofit/RetrofitError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 605
    iget-object v0, p0, Ltv/periscope/chatman/a$c;->a:Ltv/periscope/chatman/a;

    iget-object v1, p0, Ltv/periscope/chatman/a$c;->c:Ltv/periscope/chatman/api/HistoryRequest;

    invoke-virtual {v0, v1}, Ltv/periscope/chatman/a;->a(Ltv/periscope/chatman/api/HistoryRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 606
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "This history request is no longer in progress, returning early from noRetriesLeft"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    :cond_0
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "History call failed with retry-able error but there are no retries left"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    return-void
.end method

.method protected c(Ltv/periscope/chatman/api/HttpResponse;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/chatman/api/HttpResponse",
            "<",
            "Ltv/periscope/chatman/api/HistoryResponse;",
            "Lretrofit/RetrofitError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 615
    iget-object v0, p0, Ltv/periscope/chatman/a$c;->a:Ltv/periscope/chatman/a;

    iget-object v1, p0, Ltv/periscope/chatman/a$c;->c:Ltv/periscope/chatman/api/HistoryRequest;

    invoke-virtual {v0, v1}, Ltv/periscope/chatman/a;->a(Ltv/periscope/chatman/api/HistoryRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 616
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "This history request is no longer in progress, returning early from retry"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    :cond_0
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Retrying history call, scheduling to run in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ltv/periscope/chatman/a$c;->currentBackoff()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    invoke-static {}, Ltv/periscope/chatman/a;->e()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    invoke-virtual {p0}, Ltv/periscope/chatman/a$c;->currentBackoff()J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, p0, v2, v3, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 621
    return-void
.end method

.method protected synthetic canRetry(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 557
    check-cast p1, Ltv/periscope/chatman/api/HttpResponse;

    invoke-virtual {p0, p1}, Ltv/periscope/chatman/a$c;->a(Ltv/periscope/chatman/api/HttpResponse;)Z

    move-result v0

    return v0
.end method

.method protected d(Ltv/periscope/chatman/api/HttpResponse;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/chatman/api/HttpResponse",
            "<",
            "Ltv/periscope/chatman/api/HistoryResponse;",
            "Lretrofit/RetrofitError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 625
    iget-object v0, p0, Ltv/periscope/chatman/a$c;->a:Ltv/periscope/chatman/a;

    iget-object v1, p0, Ltv/periscope/chatman/a$c;->c:Ltv/periscope/chatman/api/HistoryRequest;

    invoke-virtual {v0, v1}, Ltv/periscope/chatman/a;->a(Ltv/periscope/chatman/api/HistoryRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 626
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "This history request is no longer in progress, returning early from finish"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    :goto_0
    return-void

    .line 630
    :cond_0
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "History call finished"

    invoke-static {v0, v1}, Ldcs;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    iget-object v0, p1, Ltv/periscope/chatman/api/HttpResponse;->successResponse:Ljava/lang/Object;

    if-eqz v0, :cond_7

    .line 632
    iget-object v0, p1, Ltv/periscope/chatman/api/HttpResponse;->successResponse:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/chatman/api/HistoryResponse;

    .line 633
    invoke-static {}, Ltv/periscope/chatman/model/k;->e()Ltv/periscope/chatman/model/k$a;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/chatman/a$c;->c:Ltv/periscope/chatman/api/HistoryRequest;

    iget-wide v2, v2, Ltv/periscope/chatman/api/HistoryRequest;->since:J

    .line 634
    invoke-virtual {v1, v2, v3}, Ltv/periscope/chatman/model/k$a;->a(J)Ltv/periscope/chatman/model/k$a;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/chatman/a$c;->c:Ltv/periscope/chatman/api/HistoryRequest;

    iget-object v2, v2, Ltv/periscope/chatman/api/HistoryRequest;->cursor:Ljava/lang/String;

    .line 635
    invoke-virtual {v1, v2}, Ltv/periscope/chatman/model/k$a;->a(Ljava/lang/String;)Ltv/periscope/chatman/model/k$a;

    move-result-object v1

    iget-object v2, v0, Ltv/periscope/chatman/api/HistoryResponse;->cursor:Ljava/lang/String;

    .line 636
    invoke-virtual {v1, v2}, Ltv/periscope/chatman/model/k$a;->b(Ljava/lang/String;)Ltv/periscope/chatman/model/k$a;

    move-result-object v8

    .line 637
    const/4 v3, 0x0

    .line 638
    iget-object v1, v0, Ltv/periscope/chatman/api/HistoryResponse;->messages:Ljava/util/List;

    if-eqz v1, :cond_9

    .line 639
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 640
    iget-object v1, v0, Ltv/periscope/chatman/api/HistoryResponse;->messages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ltv/periscope/chatman/api/WireMessage;

    .line 641
    iget v1, v6, Ltv/periscope/chatman/api/WireMessage;->kind:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 642
    sget-object v1, Ldco;->a:Lcom/google/gson/e;

    iget-object v2, v6, Ltv/periscope/chatman/api/WireMessage;->payload:Ljava/lang/String;

    const-class v4, Ltv/periscope/chatman/api/ChatMessage;

    invoke-virtual {v1, v2, v4}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Ltv/periscope/chatman/api/ChatMessage;

    .line 643
    if-nez v3, :cond_8

    .line 644
    iget-wide v2, v4, Ltv/periscope/chatman/api/ChatMessage;->timestamp:J

    iget-wide v12, p0, Ltv/periscope/chatman/a$c;->b:J

    cmp-long v1, v2, v12

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v7, v1

    .line 646
    :goto_3
    iget-object v1, v4, Ltv/periscope/chatman/api/ChatMessage;->room:Ljava/lang/String;

    iget-object v2, v4, Ltv/periscope/chatman/api/ChatMessage;->body:Ljava/lang/String;

    iget-object v3, v4, Ltv/periscope/chatman/api/ChatMessage;->sender:Ltv/periscope/chatman/api/Sender;

    iget-wide v4, v4, Ltv/periscope/chatman/api/ChatMessage;->timestamp:J

    sget-object v11, Ldco;->a:Lcom/google/gson/e;

    invoke-virtual {v11, v6}, Lcom/google/gson/e;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Ltv/periscope/chatman/model/j;->a(Ljava/lang/String;Ljava/lang/String;Ltv/periscope/chatman/api/Sender;JLjava/lang/String;)Ltv/periscope/chatman/model/j;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v7

    .line 647
    goto :goto_1

    .line 644
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 647
    :cond_3
    iget v1, v6, Ltv/periscope/chatman/api/WireMessage;->kind:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 648
    sget-object v1, Ldco;->a:Lcom/google/gson/e;

    iget-object v2, v6, Ltv/periscope/chatman/api/WireMessage;->payload:Ljava/lang/String;

    const-class v4, Ltv/periscope/chatman/api/ControlMessage;

    invoke-virtual {v1, v2, v4}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ltv/periscope/chatman/api/ControlMessage;

    .line 649
    iget v1, v2, Ltv/periscope/chatman/api/ControlMessage;->bodyKind:I

    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    .line 650
    if-nez v3, :cond_4

    .line 651
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 653
    :cond_4
    sget-object v1, Ldco;->a:Lcom/google/gson/e;

    iget-object v4, v2, Ltv/periscope/chatman/api/ControlMessage;->body:Ljava/lang/String;

    const-class v5, Ltv/periscope/chatman/api/ControlMessage$Join;

    invoke-virtual {v1, v4, v5}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltv/periscope/chatman/api/ControlMessage$Join;

    .line 654
    invoke-static {}, Ltv/periscope/chatman/model/Join;->c()Ltv/periscope/chatman/model/Join$a;

    move-result-object v4

    iget-object v1, v1, Ltv/periscope/chatman/api/ControlMessage$Join;->room:Ljava/lang/String;

    .line 655
    invoke-virtual {v4, v1}, Ltv/periscope/chatman/model/Join$a;->a(Ljava/lang/String;)Ltv/periscope/chatman/model/Join$a;

    move-result-object v1

    iget-object v2, v2, Ltv/periscope/chatman/api/ControlMessage;->sender:Ltv/periscope/chatman/api/Sender;

    .line 656
    invoke-virtual {v1, v2}, Ltv/periscope/chatman/model/Join$a;->a(Ltv/periscope/chatman/api/Sender;)Ltv/periscope/chatman/model/Join$a;

    move-result-object v1

    .line 657
    invoke-virtual {v1}, Ltv/periscope/chatman/model/Join$a;->a()Ltv/periscope/chatman/model/Join;

    move-result-object v1

    .line 654
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 661
    :cond_5
    invoke-virtual {v8, v9}, Ltv/periscope/chatman/model/k$a;->a(Ljava/util/Collection;)Ltv/periscope/chatman/model/k$a;

    move-object v1, v3

    .line 663
    :goto_4
    if-nez v1, :cond_6

    .line 664
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 667
    :cond_6
    invoke-virtual {v8}, Ltv/periscope/chatman/model/k$a;->a()Ltv/periscope/chatman/model/k;

    move-result-object v2

    .line 670
    iget-object v3, p0, Ltv/periscope/chatman/a$c;->a:Ltv/periscope/chatman/a;

    invoke-static {v3}, Ltv/periscope/chatman/a;->c(Ltv/periscope/chatman/a;)Ltv/periscope/chatman/a$b;

    move-result-object v3

    iget-object v0, v0, Ltv/periscope/chatman/api/HistoryResponse;->cursor:Ljava/lang/String;

    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v3, v2, v0, v1}, Ltv/periscope/chatman/a$b;->a(Ltv/periscope/chatman/model/k;ZZ)V

    .line 675
    :cond_7
    iget-object v0, p0, Ltv/periscope/chatman/a$c;->a:Ltv/periscope/chatman/a;

    iget-object v1, p0, Ltv/periscope/chatman/a$c;->c:Ltv/periscope/chatman/api/HistoryRequest;

    invoke-virtual {v0, v1}, Ltv/periscope/chatman/a;->b(Ltv/periscope/chatman/api/HistoryRequest;)V

    goto/16 :goto_0

    :cond_8
    move-object v7, v3

    goto/16 :goto_3

    :cond_9
    move-object v1, v3

    goto :goto_4
.end method

.method protected synthetic execute()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 557
    invoke-virtual {p0}, Ltv/periscope/chatman/a$c;->a()Ltv/periscope/chatman/api/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic finish(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 557
    check-cast p1, Ltv/periscope/chatman/api/HttpResponse;

    invoke-virtual {p0, p1}, Ltv/periscope/chatman/a$c;->d(Ltv/periscope/chatman/api/HttpResponse;)V

    return-void
.end method

.method protected id()Ljava/lang/String;
    .locals 2

    .prologue
    .line 680
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ChatClient:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic noRetriesLeft(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 557
    check-cast p1, Ltv/periscope/chatman/api/HttpResponse;

    invoke-virtual {p0, p1}, Ltv/periscope/chatman/a$c;->b(Ltv/periscope/chatman/api/HttpResponse;)V

    return-void
.end method

.method protected synthetic retry(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 557
    check-cast p1, Ltv/periscope/chatman/api/HttpResponse;

    invoke-virtual {p0, p1}, Ltv/periscope/chatman/a$c;->c(Ltv/periscope/chatman/api/HttpResponse;)V

    return-void
.end method
