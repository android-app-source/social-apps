.class final Ltv/periscope/chatman/model/h;
.super Ltv/periscope/chatman/model/Presence;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/chatman/model/h$a;
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method private constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ltv/periscope/chatman/model/Presence;-><init>()V

    .line 15
    iput-wide p1, p0, Ltv/periscope/chatman/model/h;->a:J

    .line 16
    iput-wide p3, p0, Ltv/periscope/chatman/model/h;->b:J

    .line 17
    return-void
.end method

.method synthetic constructor <init>(JJLtv/periscope/chatman/model/h$1;)V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0, p1, p2, p3, p4}, Ltv/periscope/chatman/model/h;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Ltv/periscope/chatman/model/h;->a:J

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 26
    iget-wide v0, p0, Ltv/periscope/chatman/model/h;->b:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 39
    if-ne p1, p0, :cond_1

    .line 47
    :cond_0
    :goto_0
    return v0

    .line 42
    :cond_1
    instance-of v2, p1, Ltv/periscope/chatman/model/Presence;

    if-eqz v2, :cond_3

    .line 43
    check-cast p1, Ltv/periscope/chatman/model/Presence;

    .line 44
    iget-wide v2, p0, Ltv/periscope/chatman/model/h;->a:J

    invoke-virtual {p1}, Ltv/periscope/chatman/model/Presence;->a()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/chatman/model/h;->b:J

    .line 45
    invoke-virtual {p1}, Ltv/periscope/chatman/model/Presence;->b()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 47
    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const v7, 0xf4243

    const/16 v6, 0x20

    .line 52
    .line 54
    int-to-long v0, v7

    iget-wide v2, p0, Ltv/periscope/chatman/model/h;->a:J

    ushr-long/2addr v2, v6

    iget-wide v4, p0, Ltv/periscope/chatman/model/h;->a:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 55
    mul-int/2addr v0, v7

    .line 56
    int-to-long v0, v0

    iget-wide v2, p0, Ltv/periscope/chatman/model/h;->b:J

    ushr-long/2addr v2, v6

    iget-wide v4, p0, Ltv/periscope/chatman/model/h;->b:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 57
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Presence{occupancy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/chatman/model/h;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", totalParticipants="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/chatman/model/h;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
