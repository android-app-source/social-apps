.class final Ltv/periscope/chatman/model/g;
.super Ltv/periscope/chatman/model/Leave;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/chatman/model/g$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ltv/periscope/chatman/api/Sender;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ltv/periscope/chatman/api/Sender;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ltv/periscope/chatman/model/Leave;-><init>()V

    .line 16
    iput-object p1, p0, Ltv/periscope/chatman/model/g;->a:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Ltv/periscope/chatman/model/g;->b:Ltv/periscope/chatman/api/Sender;

    .line 18
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ltv/periscope/chatman/api/Sender;Ltv/periscope/chatman/model/g$1;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ltv/periscope/chatman/model/g;-><init>(Ljava/lang/String;Ltv/periscope/chatman/api/Sender;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Ltv/periscope/chatman/model/g;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ltv/periscope/chatman/api/Sender;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Ltv/periscope/chatman/model/g;->b:Ltv/periscope/chatman/api/Sender;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 40
    if-ne p1, p0, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v0

    .line 43
    :cond_1
    instance-of v2, p1, Ltv/periscope/chatman/model/Leave;

    if-eqz v2, :cond_3

    .line 44
    check-cast p1, Ltv/periscope/chatman/model/Leave;

    .line 45
    iget-object v2, p0, Ltv/periscope/chatman/model/g;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/chatman/model/Leave;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/chatman/model/g;->b:Ltv/periscope/chatman/api/Sender;

    .line 46
    invoke-virtual {p1}, Ltv/periscope/chatman/model/Leave;->b()Ltv/periscope/chatman/api/Sender;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 48
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    const v1, 0xf4243

    .line 53
    .line 55
    iget-object v0, p0, Ltv/periscope/chatman/model/g;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v1

    .line 56
    mul-int/2addr v0, v1

    .line 57
    iget-object v1, p0, Ltv/periscope/chatman/model/g;->b:Ltv/periscope/chatman/api/Sender;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 58
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Leave{room="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/chatman/model/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", sender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/chatman/model/g;->b:Ltv/periscope/chatman/api/Sender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
