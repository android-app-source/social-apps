.class final Ltv/periscope/chatman/model/g$a;
.super Ltv/periscope/chatman/model/Leave$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/chatman/model/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ltv/periscope/chatman/api/Sender;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ltv/periscope/chatman/model/Leave$a;-><init>()V

    .line 65
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ltv/periscope/chatman/model/Leave$a;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Ltv/periscope/chatman/model/g$a;->a:Ljava/lang/String;

    .line 73
    return-object p0
.end method

.method public a(Ltv/periscope/chatman/api/Sender;)Ltv/periscope/chatman/model/Leave$a;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Ltv/periscope/chatman/model/g$a;->b:Ltv/periscope/chatman/api/Sender;

    .line 78
    return-object p0
.end method

.method public a()Ltv/periscope/chatman/model/Leave;
    .locals 4

    .prologue
    .line 82
    const-string/jumbo v0, ""

    .line 83
    iget-object v1, p0, Ltv/periscope/chatman/model/g$a;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 84
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " room"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 86
    :cond_0
    iget-object v1, p0, Ltv/periscope/chatman/model/g$a;->b:Ltv/periscope/chatman/api/Sender;

    if-nez v1, :cond_1

    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " sender"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 89
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 90
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Missing required properties:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 92
    :cond_2
    new-instance v0, Ltv/periscope/chatman/model/g;

    iget-object v1, p0, Ltv/periscope/chatman/model/g$a;->a:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/chatman/model/g$a;->b:Ltv/periscope/chatman/api/Sender;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Ltv/periscope/chatman/model/g;-><init>(Ljava/lang/String;Ltv/periscope/chatman/api/Sender;Ltv/periscope/chatman/model/g$1;)V

    return-object v0
.end method
