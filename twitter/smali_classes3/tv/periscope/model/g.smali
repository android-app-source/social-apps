.class final Ltv/periscope/model/g;
.super Ltv/periscope/model/u;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/Long;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:I


# direct methods
.method constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ltv/periscope/model/u;-><init>()V

    .line 30
    iput-object p1, p0, Ltv/periscope/model/g;->a:Ljava/lang/Long;

    .line 31
    iput-object p2, p0, Ltv/periscope/model/g;->b:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Ltv/periscope/model/g;->c:Ljava/lang/String;

    .line 33
    iput-boolean p4, p0, Ltv/periscope/model/g;->d:Z

    .line 34
    iput-object p5, p0, Ltv/periscope/model/g;->e:Ljava/lang/String;

    .line 35
    iput-object p6, p0, Ltv/periscope/model/g;->f:Ljava/lang/String;

    .line 36
    iput-object p7, p0, Ltv/periscope/model/g;->g:Ljava/lang/String;

    .line 37
    iput-object p8, p0, Ltv/periscope/model/g;->h:Ljava/lang/String;

    .line 38
    iput p9, p0, Ltv/periscope/model/g;->i:I

    .line 39
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ltv/periscope/model/g;->a:Ljava/lang/Long;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Ltv/periscope/model/g;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Ltv/periscope/model/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Ltv/periscope/model/g;->d:Z

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Ltv/periscope/model/g;->e:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 110
    if-ne p1, p0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return v0

    .line 113
    :cond_1
    instance-of v2, p1, Ltv/periscope/model/u;

    if-eqz v2, :cond_a

    .line 114
    check-cast p1, Ltv/periscope/model/u;

    .line 115
    iget-object v2, p0, Ltv/periscope/model/g;->a:Ljava/lang/Long;

    if-nez v2, :cond_3

    invoke-virtual {p1}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v2, p0, Ltv/periscope/model/g;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 116
    invoke-virtual {p1}, Ltv/periscope/model/u;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    iget-object v2, p0, Ltv/periscope/model/g;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 117
    invoke-virtual {p1}, Ltv/periscope/model/u;->c()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_3
    iget-boolean v2, p0, Ltv/periscope/model/g;->d:Z

    .line 118
    invoke-virtual {p1}, Ltv/periscope/model/u;->d()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Ltv/periscope/model/g;->e:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 119
    invoke-virtual {p1}, Ltv/periscope/model/u;->e()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_4
    iget-object v2, p0, Ltv/periscope/model/g;->f:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 120
    invoke-virtual {p1}, Ltv/periscope/model/u;->f()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_5
    iget-object v2, p0, Ltv/periscope/model/g;->g:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 121
    invoke-virtual {p1}, Ltv/periscope/model/u;->g()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_6
    iget-object v2, p0, Ltv/periscope/model/g;->h:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 122
    invoke-virtual {p1}, Ltv/periscope/model/u;->h()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_7
    iget v2, p0, Ltv/periscope/model/g;->i:I

    .line 123
    invoke-virtual {p1}, Ltv/periscope/model/u;->i()I

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 115
    :cond_3
    iget-object v2, p0, Ltv/periscope/model/g;->a:Ljava/lang/Long;

    invoke-virtual {p1}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    .line 116
    :cond_4
    iget-object v2, p0, Ltv/periscope/model/g;->b:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/u;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    .line 117
    :cond_5
    iget-object v2, p0, Ltv/periscope/model/g;->c:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/u;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_3

    .line 119
    :cond_6
    iget-object v2, p0, Ltv/periscope/model/g;->e:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/u;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_4

    .line 120
    :cond_7
    iget-object v2, p0, Ltv/periscope/model/g;->f:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/u;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_5

    .line 121
    :cond_8
    iget-object v2, p0, Ltv/periscope/model/g;->g:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/u;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_6

    .line 122
    :cond_9
    iget-object v2, p0, Ltv/periscope/model/g;->h:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/u;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_7

    :cond_a
    move v0, v1

    .line 125
    goto/16 :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ltv/periscope/model/g;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/model/g;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Ltv/periscope/model/g;->h:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const v3, 0xf4243

    .line 130
    .line 132
    iget-object v0, p0, Ltv/periscope/model/g;->a:Ljava/lang/Long;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v3

    .line 133
    mul-int v2, v0, v3

    .line 134
    iget-object v0, p0, Ltv/periscope/model/g;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    xor-int/2addr v0, v2

    .line 135
    mul-int v2, v0, v3

    .line 136
    iget-object v0, p0, Ltv/periscope/model/g;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    xor-int/2addr v0, v2

    .line 137
    mul-int v2, v0, v3

    .line 138
    iget-boolean v0, p0, Ltv/periscope/model/g;->d:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x4cf

    :goto_3
    xor-int/2addr v0, v2

    .line 139
    mul-int v2, v0, v3

    .line 140
    iget-object v0, p0, Ltv/periscope/model/g;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    xor-int/2addr v0, v2

    .line 141
    mul-int v2, v0, v3

    .line 142
    iget-object v0, p0, Ltv/periscope/model/g;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    xor-int/2addr v0, v2

    .line 143
    mul-int v2, v0, v3

    .line 144
    iget-object v0, p0, Ltv/periscope/model/g;->g:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    xor-int/2addr v0, v2

    .line 145
    mul-int/2addr v0, v3

    .line 146
    iget-object v2, p0, Ltv/periscope/model/g;->h:Ljava/lang/String;

    if-nez v2, :cond_7

    :goto_7
    xor-int/2addr v0, v1

    .line 147
    mul-int/2addr v0, v3

    .line 148
    iget v1, p0, Ltv/periscope/model/g;->i:I

    xor-int/2addr v0, v1

    .line 149
    return v0

    .line 132
    :cond_0
    iget-object v0, p0, Ltv/periscope/model/g;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_0

    .line 134
    :cond_1
    iget-object v0, p0, Ltv/periscope/model/g;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 136
    :cond_2
    iget-object v0, p0, Ltv/periscope/model/g;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 138
    :cond_3
    const/16 v0, 0x4d5

    goto :goto_3

    .line 140
    :cond_4
    iget-object v0, p0, Ltv/periscope/model/g;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 142
    :cond_5
    iget-object v0, p0, Ltv/periscope/model/g;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 144
    :cond_6
    iget-object v0, p0, Ltv/periscope/model/g;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 146
    :cond_7
    iget-object v1, p0, Ltv/periscope/model/g;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public i()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Ltv/periscope/model/g;->i:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ChatAccess{participantIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/g;->a:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", roomId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", lifeCycleToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", shouldLog="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/model/g;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", accessToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", replayAccessToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/g;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", endpoint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/g;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", replayEndpoint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/g;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", chatmanPerms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ltv/periscope/model/g;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
