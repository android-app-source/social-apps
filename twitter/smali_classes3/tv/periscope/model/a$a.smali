.class final Ltv/periscope/model/a$a;
.super Ltv/periscope/model/p$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/model/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private A:Ljava/lang/Integer;

.field private B:Ljava/lang/Boolean;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/Boolean;

.field private a:Ljava/lang/Long;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ltv/periscope/model/z;

.field private f:Ljava/lang/Long;

.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/Long;

.field private l:Ljava/lang/Long;

.field private m:Ljava/lang/Long;

.field private n:Ljava/lang/Double;

.field private o:Ljava/lang/Double;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/Boolean;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/Boolean;

.field private x:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private z:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 455
    invoke-direct {p0}, Ltv/periscope/model/p$a;-><init>()V

    .line 456
    return-void
.end method


# virtual methods
.method public a(D)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 557
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->n:Ljava/lang/Double;

    .line 558
    return-object p0
.end method

.method public a(I)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 622
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->A:Ljava/lang/Integer;

    .line 623
    return-object p0
.end method

.method public a(J)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 492
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->a:Ljava/lang/Long;

    .line 493
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 502
    iput-object p1, p0, Ltv/periscope/model/a$a;->c:Ljava/lang/String;

    .line 503
    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Ltv/periscope/model/p$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ltv/periscope/model/p$a;"
        }
    .end annotation

    .prologue
    .line 607
    iput-object p1, p0, Ltv/periscope/model/a$a;->x:Ljava/util/ArrayList;

    .line 608
    return-object p0
.end method

.method public a(Ltv/periscope/model/z;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 512
    iput-object p1, p0, Ltv/periscope/model/a$a;->e:Ltv/periscope/model/z;

    .line 513
    return-object p0
.end method

.method public a(Z)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 522
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->g:Ljava/lang/Boolean;

    .line 523
    return-object p0
.end method

.method public a()Ltv/periscope/model/p;
    .locals 43

    .prologue
    .line 647
    const-string/jumbo v2, ""

    .line 648
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->a:Ljava/lang/Long;

    if-nez v3, :cond_0

    .line 649
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " timedOutTime"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 651
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->b:Ljava/lang/Long;

    if-nez v3, :cond_1

    .line 652
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " pingTime"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 654
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->c:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 655
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 657
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->e:Ltv/periscope/model/z;

    if-nez v3, :cond_3

    .line 658
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " location"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 660
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->f:Ljava/lang/Long;

    if-nez v3, :cond_4

    .line 661
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " createdAtMillis"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 663
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->g:Ljava/lang/Boolean;

    if-nez v3, :cond_5

    .line 664
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " featured"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 666
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->k:Ljava/lang/Long;

    if-nez v3, :cond_6

    .line 667
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " featuredTimecodeMs"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 669
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->l:Ljava/lang/Long;

    if-nez v3, :cond_7

    .line 670
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " sortScore"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 672
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->m:Ljava/lang/Long;

    if-nez v3, :cond_8

    .line 673
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " startTimeMillis"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 675
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->n:Ljava/lang/Double;

    if-nez v3, :cond_9

    .line 676
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ipLat"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 678
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->o:Ljava/lang/Double;

    if-nez v3, :cond_a

    .line 679
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ipLong"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 681
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->p:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 682
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " userId"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 684
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->q:Ljava/lang/Boolean;

    if-nez v3, :cond_c

    .line 685
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " locked"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 687
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->t:Ljava/lang/String;

    if-nez v3, :cond_d

    .line 688
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " userDisplayName"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 690
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->w:Ljava/lang/Boolean;

    if-nez v3, :cond_e

    .line 691
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " hasLocation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 693
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->A:Ljava/lang/Integer;

    if-nez v3, :cond_f

    .line 694
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " cameraRotation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 696
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->B:Ljava/lang/Boolean;

    if-nez v3, :cond_10

    .line 697
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " highlightAvailable"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 699
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->E:Ljava/lang/Boolean;

    if-nez v3, :cond_11

    .line 700
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " is360"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 702
    :cond_11
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_12

    .line 703
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Missing required properties:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 705
    :cond_12
    new-instance v2, Ltv/periscope/model/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/a$a;->a:Ljava/lang/Long;

    .line 706
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/model/a$a;->b:Ljava/lang/Long;

    .line 707
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v7, v0, Ltv/periscope/model/a$a;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Ltv/periscope/model/a$a;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Ltv/periscope/model/a$a;->e:Ltv/periscope/model/z;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/model/a$a;->f:Ljava/lang/Long;

    .line 711
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Ltv/periscope/model/a$a;->g:Ljava/lang/Boolean;

    .line 712
    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Ltv/periscope/model/a$a;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Ltv/periscope/model/a$a;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Ltv/periscope/model/a$a;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->k:Ljava/lang/Long;

    move-object/from16 v16, v0

    .line 716
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->l:Ljava/lang/Long;

    move-object/from16 v18, v0

    .line 717
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->m:Ljava/lang/Long;

    move-object/from16 v20, v0

    .line 718
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->n:Ljava/lang/Double;

    move-object/from16 v22, v0

    .line 719
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->o:Ljava/lang/Double;

    move-object/from16 v24, v0

    .line 720
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->p:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->q:Ljava/lang/Boolean;

    move-object/from16 v27, v0

    .line 722
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v27

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->r:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->s:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->t:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->u:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->v:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->w:Ljava/lang/Boolean;

    move-object/from16 v33, v0

    .line 728
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v33

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->x:Ljava/util/ArrayList;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->y:Ljava/util/ArrayList;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->z:Ljava/util/ArrayList;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->A:Ljava/lang/Integer;

    move-object/from16 v37, v0

    .line 732
    invoke-virtual/range {v37 .. v37}, Ljava/lang/Integer;->intValue()I

    move-result v37

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->B:Ljava/lang/Boolean;

    move-object/from16 v38, v0

    .line 733
    invoke-virtual/range {v38 .. v38}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v38

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->C:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->D:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/a$a;->E:Ljava/lang/Boolean;

    move-object/from16 v41, v0

    .line 736
    invoke-virtual/range {v41 .. v41}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v41

    const/16 v42, 0x0

    invoke-direct/range {v2 .. v42}, Ltv/periscope/model/a;-><init>(JJLjava/lang/String;Ljava/lang/String;Ltv/periscope/model/z;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJDDLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IZLjava/lang/String;Ljava/lang/String;ZLtv/periscope/model/a$1;)V

    .line 705
    return-object v2
.end method

.method public b(D)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 562
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->o:Ljava/lang/Double;

    .line 563
    return-object p0
.end method

.method public b(J)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 497
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->b:Ljava/lang/Long;

    .line 498
    return-object p0
.end method

.method public b(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Ltv/periscope/model/a$a;->d:Ljava/lang/String;

    .line 508
    return-object p0
.end method

.method public b(Ljava/util/ArrayList;)Ltv/periscope/model/p$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ltv/periscope/model/p$a;"
        }
    .end annotation

    .prologue
    .line 612
    iput-object p1, p0, Ltv/periscope/model/a$a;->y:Ljava/util/ArrayList;

    .line 613
    return-object p0
.end method

.method public b(Z)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 572
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->q:Ljava/lang/Boolean;

    .line 573
    return-object p0
.end method

.method public c(J)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 517
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->f:Ljava/lang/Long;

    .line 518
    return-object p0
.end method

.method public c(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 527
    iput-object p1, p0, Ltv/periscope/model/a$a;->h:Ljava/lang/String;

    .line 528
    return-object p0
.end method

.method public c(Ljava/util/ArrayList;)Ltv/periscope/model/p$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ltv/periscope/model/p$a;"
        }
    .end annotation

    .prologue
    .line 617
    iput-object p1, p0, Ltv/periscope/model/a$a;->z:Ljava/util/ArrayList;

    .line 618
    return-object p0
.end method

.method public c(Z)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 602
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->w:Ljava/lang/Boolean;

    .line 603
    return-object p0
.end method

.method public d(J)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 542
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->k:Ljava/lang/Long;

    .line 543
    return-object p0
.end method

.method public d(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 532
    iput-object p1, p0, Ltv/periscope/model/a$a;->i:Ljava/lang/String;

    .line 533
    return-object p0
.end method

.method public d(Z)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 627
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->B:Ljava/lang/Boolean;

    .line 628
    return-object p0
.end method

.method public e(J)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 547
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->l:Ljava/lang/Long;

    .line 548
    return-object p0
.end method

.method public e(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 537
    iput-object p1, p0, Ltv/periscope/model/a$a;->j:Ljava/lang/String;

    .line 538
    return-object p0
.end method

.method public e(Z)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 642
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->E:Ljava/lang/Boolean;

    .line 643
    return-object p0
.end method

.method public f(J)Ltv/periscope/model/p$a;
    .locals 1

    .prologue
    .line 552
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/a$a;->m:Ljava/lang/Long;

    .line 553
    return-object p0
.end method

.method public f(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 567
    iput-object p1, p0, Ltv/periscope/model/a$a;->p:Ljava/lang/String;

    .line 568
    return-object p0
.end method

.method public g(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 577
    iput-object p1, p0, Ltv/periscope/model/a$a;->r:Ljava/lang/String;

    .line 578
    return-object p0
.end method

.method public h(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 582
    iput-object p1, p0, Ltv/periscope/model/a$a;->s:Ljava/lang/String;

    .line 583
    return-object p0
.end method

.method public i(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 587
    iput-object p1, p0, Ltv/periscope/model/a$a;->t:Ljava/lang/String;

    .line 588
    return-object p0
.end method

.method public j(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 592
    iput-object p1, p0, Ltv/periscope/model/a$a;->u:Ljava/lang/String;

    .line 593
    return-object p0
.end method

.method public k(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 597
    iput-object p1, p0, Ltv/periscope/model/a$a;->v:Ljava/lang/String;

    .line 598
    return-object p0
.end method

.method public l(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 632
    iput-object p1, p0, Ltv/periscope/model/a$a;->C:Ljava/lang/String;

    .line 633
    return-object p0
.end method

.method public m(Ljava/lang/String;)Ltv/periscope/model/p$a;
    .locals 0

    .prologue
    .line 637
    iput-object p1, p0, Ltv/periscope/model/a$a;->D:Ljava/lang/String;

    .line 638
    return-object p0
.end method
