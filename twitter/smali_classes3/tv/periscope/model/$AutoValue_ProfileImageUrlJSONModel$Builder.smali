.class final Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;
.super Ltv/periscope/model/ProfileImageUrlJSONModel$Builder;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private height:Ljava/lang/Integer;

.field private sslUrl:Ljava/lang/String;

.field private url:Ljava/lang/String;

.field private width:Ljava/lang/Integer;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ltv/periscope/model/ProfileImageUrlJSONModel$Builder;-><init>()V

    .line 95
    return-void
.end method

.method constructor <init>(Ltv/periscope/model/ProfileImageUrlJSONModel;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ltv/periscope/model/ProfileImageUrlJSONModel$Builder;-><init>()V

    .line 97
    invoke-virtual {p1}, Ltv/periscope/model/ProfileImageUrlJSONModel;->width()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->width:Ljava/lang/Integer;

    .line 98
    invoke-virtual {p1}, Ltv/periscope/model/ProfileImageUrlJSONModel;->height()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->height:Ljava/lang/Integer;

    .line 99
    invoke-virtual {p1}, Ltv/periscope/model/ProfileImageUrlJSONModel;->url()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->url:Ljava/lang/String;

    .line 100
    invoke-virtual {p1}, Ltv/periscope/model/ProfileImageUrlJSONModel;->sslUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->sslUrl:Ljava/lang/String;

    .line 101
    return-void
.end method


# virtual methods
.method public build()Ltv/periscope/model/ProfileImageUrlJSONModel;
    .locals 5

    .prologue
    .line 124
    const-string/jumbo v0, ""

    .line 125
    iget-object v1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->width:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 126
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " width"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 128
    :cond_0
    iget-object v1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->height:Ljava/lang/Integer;

    if-nez v1, :cond_1

    .line 129
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " height"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 131
    :cond_1
    iget-object v1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->url:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 132
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " url"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 134
    :cond_2
    iget-object v1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->sslUrl:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 135
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " sslUrl"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 137
    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 138
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Missing required properties:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 140
    :cond_4
    new-instance v0, Ltv/periscope/model/AutoValue_ProfileImageUrlJSONModel;

    iget-object v1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->width:Ljava/lang/Integer;

    iget-object v2, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->height:Ljava/lang/Integer;

    iget-object v3, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->url:Ljava/lang/String;

    iget-object v4, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->sslUrl:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Ltv/periscope/model/AutoValue_ProfileImageUrlJSONModel;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public setHeight(Ljava/lang/Integer;)Ltv/periscope/model/ProfileImageUrlJSONModel$Builder;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->height:Ljava/lang/Integer;

    .line 110
    return-object p0
.end method

.method public setSslUrl(Ljava/lang/String;)Ltv/periscope/model/ProfileImageUrlJSONModel$Builder;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->sslUrl:Ljava/lang/String;

    .line 120
    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Ltv/periscope/model/ProfileImageUrlJSONModel$Builder;
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->url:Ljava/lang/String;

    .line 115
    return-object p0
.end method

.method public setWidth(Ljava/lang/Integer;)Ltv/periscope/model/ProfileImageUrlJSONModel$Builder;
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;->width:Ljava/lang/Integer;

    .line 105
    return-object p0
.end method
