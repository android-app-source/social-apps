.class final Ltv/periscope/model/d;
.super Ltv/periscope/model/ChannelAction;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/model/d$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ltv/periscope/model/ChannelAction$ActionType;

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ltv/periscope/model/ChannelAction$ActionType;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ltv/periscope/model/ChannelAction;-><init>()V

    .line 25
    iput-object p1, p0, Ltv/periscope/model/d;->a:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Ltv/periscope/model/d;->b:Ltv/periscope/model/ChannelAction$ActionType;

    .line 27
    iput-object p3, p0, Ltv/periscope/model/d;->c:Ljava/lang/String;

    .line 28
    iput-wide p4, p0, Ltv/periscope/model/d;->d:J

    .line 29
    iput-object p6, p0, Ltv/periscope/model/d;->e:Ljava/lang/String;

    .line 30
    iput-object p7, p0, Ltv/periscope/model/d;->f:Ljava/lang/String;

    .line 31
    iput-object p8, p0, Ltv/periscope/model/d;->g:Ljava/lang/String;

    .line 32
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ltv/periscope/model/ChannelAction$ActionType;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/d$1;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct/range {p0 .. p8}, Ltv/periscope/model/d;-><init>(Ljava/lang/String;Ltv/periscope/model/ChannelAction$ActionType;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Ltv/periscope/model/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ltv/periscope/model/ChannelAction$ActionType;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ltv/periscope/model/d;->b:Ltv/periscope/model/ChannelAction$ActionType;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ltv/periscope/model/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Ltv/periscope/model/d;->d:J

    return-wide v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Ltv/periscope/model/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 84
    if-ne p1, p0, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v0

    .line 87
    :cond_1
    instance-of v2, p1, Ltv/periscope/model/ChannelAction;

    if-eqz v2, :cond_3

    .line 88
    check-cast p1, Ltv/periscope/model/ChannelAction;

    .line 89
    iget-object v2, p0, Ltv/periscope/model/d;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/ChannelAction;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/d;->b:Ltv/periscope/model/ChannelAction$ActionType;

    .line 90
    invoke-virtual {p1}, Ltv/periscope/model/ChannelAction;->b()Ltv/periscope/model/ChannelAction$ActionType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/ChannelAction$ActionType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/d;->c:Ljava/lang/String;

    .line 91
    invoke-virtual {p1}, Ltv/periscope/model/ChannelAction;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/d;->d:J

    .line 92
    invoke-virtual {p1}, Ltv/periscope/model/ChannelAction;->d()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/d;->e:Ljava/lang/String;

    .line 93
    invoke-virtual {p1}, Ltv/periscope/model/ChannelAction;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/d;->f:Ljava/lang/String;

    .line 94
    invoke-virtual {p1}, Ltv/periscope/model/ChannelAction;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/d;->g:Ljava/lang/String;

    .line 95
    invoke-virtual {p1}, Ltv/periscope/model/ChannelAction;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 97
    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Ltv/periscope/model/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ltv/periscope/model/d;->g:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const v6, 0xf4243

    .line 102
    .line 104
    iget-object v0, p0, Ltv/periscope/model/d;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v6

    .line 105
    mul-int/2addr v0, v6

    .line 106
    iget-object v1, p0, Ltv/periscope/model/d;->b:Ltv/periscope/model/ChannelAction$ActionType;

    invoke-virtual {v1}, Ltv/periscope/model/ChannelAction$ActionType;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 107
    mul-int/2addr v0, v6

    .line 108
    iget-object v1, p0, Ltv/periscope/model/d;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 109
    mul-int/2addr v0, v6

    .line 110
    int-to-long v0, v0

    iget-wide v2, p0, Ltv/periscope/model/d;->d:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    iget-wide v4, p0, Ltv/periscope/model/d;->d:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 111
    mul-int/2addr v0, v6

    .line 112
    iget-object v1, p0, Ltv/periscope/model/d;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 113
    mul-int/2addr v0, v6

    .line 114
    iget-object v1, p0, Ltv/periscope/model/d;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 115
    mul-int/2addr v0, v6

    .line 116
    iget-object v1, p0, Ltv/periscope/model/d;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 117
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ChannelAction{userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", actionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/d;->b:Ltv/periscope/model/ChannelAction$ActionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", channelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", timestampMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/d;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", memberId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/d;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", broadcastId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", channelName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/d;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
