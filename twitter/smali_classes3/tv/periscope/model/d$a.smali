.class final Ltv/periscope/model/d$a;
.super Ltv/periscope/model/ChannelAction$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/model/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ltv/periscope/model/ChannelAction$ActionType;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Ltv/periscope/model/ChannelAction$a;-><init>()V

    .line 129
    return-void
.end method


# virtual methods
.method public a(J)Ltv/periscope/model/ChannelAction$a;
    .locals 1

    .prologue
    .line 156
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/d$a;->d:Ljava/lang/Long;

    .line 157
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$a;
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Ltv/periscope/model/d$a;->a:Ljava/lang/String;

    .line 142
    return-object p0
.end method

.method public a(Ltv/periscope/model/ChannelAction$ActionType;)Ltv/periscope/model/ChannelAction$a;
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Ltv/periscope/model/d$a;->b:Ltv/periscope/model/ChannelAction$ActionType;

    .line 147
    return-object p0
.end method

.method public a()Ltv/periscope/model/ChannelAction;
    .locals 10

    .prologue
    .line 176
    const-string/jumbo v0, ""

    .line 177
    iget-object v1, p0, Ltv/periscope/model/d$a;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " userId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 180
    :cond_0
    iget-object v1, p0, Ltv/periscope/model/d$a;->b:Ltv/periscope/model/ChannelAction$ActionType;

    if-nez v1, :cond_1

    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " actionType"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 183
    :cond_1
    iget-object v1, p0, Ltv/periscope/model/d$a;->c:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " channelId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 186
    :cond_2
    iget-object v1, p0, Ltv/periscope/model/d$a;->d:Ljava/lang/Long;

    if-nez v1, :cond_3

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " timestampMs"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 189
    :cond_3
    iget-object v1, p0, Ltv/periscope/model/d$a;->e:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " memberId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 192
    :cond_4
    iget-object v1, p0, Ltv/periscope/model/d$a;->f:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 193
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " broadcastId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 195
    :cond_5
    iget-object v1, p0, Ltv/periscope/model/d$a;->g:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 196
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " channelName"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 198
    :cond_6
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 199
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Missing required properties:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 201
    :cond_7
    new-instance v0, Ltv/periscope/model/d;

    iget-object v1, p0, Ltv/periscope/model/d$a;->a:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/model/d$a;->b:Ltv/periscope/model/ChannelAction$ActionType;

    iget-object v3, p0, Ltv/periscope/model/d$a;->c:Ljava/lang/String;

    iget-object v4, p0, Ltv/periscope/model/d$a;->d:Ljava/lang/Long;

    .line 205
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v6, p0, Ltv/periscope/model/d$a;->e:Ljava/lang/String;

    iget-object v7, p0, Ltv/periscope/model/d$a;->f:Ljava/lang/String;

    iget-object v8, p0, Ltv/periscope/model/d$a;->g:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Ltv/periscope/model/d;-><init>(Ljava/lang/String;Ltv/periscope/model/ChannelAction$ActionType;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/d$1;)V

    .line 201
    return-object v0
.end method

.method public b(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$a;
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Ltv/periscope/model/d$a;->c:Ljava/lang/String;

    .line 152
    return-object p0
.end method

.method public c(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$a;
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Ltv/periscope/model/d$a;->e:Ljava/lang/String;

    .line 162
    return-object p0
.end method

.method public d(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$a;
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Ltv/periscope/model/d$a;->f:Ljava/lang/String;

    .line 167
    return-object p0
.end method

.method public e(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$a;
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Ltv/periscope/model/d$a;->g:Ljava/lang/String;

    .line 172
    return-object p0
.end method
