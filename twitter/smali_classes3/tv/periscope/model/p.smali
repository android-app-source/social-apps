.class public abstract Ltv/periscope/model/p;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/model/p$a;
    }
.end annotation


# instance fields
.field private a:Z

.field private b:J

.field private c:Ltv/periscope/model/BroadcastState;

.field private d:Z

.field private e:Z

.field private f:J

.field private g:Ljava/lang/Long;

.field private h:I

.field private i:Ljava/lang/Long;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/Long;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/model/p;->b:J

    .line 15
    sget-object v0, Ltv/periscope/model/BroadcastState;->e:Ltv/periscope/model/BroadcastState;

    iput-object v0, p0, Ltv/periscope/model/p;->c:Ltv/periscope/model/BroadcastState;

    .line 34
    return-void
.end method

.method public static G()Ltv/periscope/model/p$a;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 37
    new-instance v0, Ltv/periscope/model/a$a;

    invoke-direct {v0}, Ltv/periscope/model/a$a;-><init>()V

    sget-object v1, Ltv/periscope/model/z;->a:Ltv/periscope/model/z;

    .line 38
    invoke-virtual {v0, v1}, Ltv/periscope/model/a$a;->a(Ltv/periscope/model/z;)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 39
    invoke-virtual {v0, v2}, Ltv/periscope/model/p$a;->a(Z)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 40
    invoke-virtual {v0, v3}, Ltv/periscope/model/p$a;->c(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 41
    invoke-virtual {v0, v3}, Ltv/periscope/model/p$a;->d(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 42
    invoke-virtual {v0, v3}, Ltv/periscope/model/p$a;->e(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 43
    invoke-virtual {v0, v4, v5}, Ltv/periscope/model/p$a;->d(J)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 44
    invoke-virtual {v0, v4, v5}, Ltv/periscope/model/p$a;->e(J)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 45
    invoke-virtual {v0, v4, v5}, Ltv/periscope/model/p$a;->c(J)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 46
    invoke-virtual {v0, v4, v5}, Ltv/periscope/model/p$a;->f(J)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 47
    invoke-virtual {v0, v6, v7}, Ltv/periscope/model/p$a;->b(D)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 48
    invoke-virtual {v0, v6, v7}, Ltv/periscope/model/p$a;->a(D)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 49
    invoke-virtual {v0, v2}, Ltv/periscope/model/p$a;->b(Z)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 50
    invoke-virtual {v0, v2}, Ltv/periscope/model/p$a;->c(Z)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v3}, Ltv/periscope/model/p$a;->a(Ljava/util/ArrayList;)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 52
    invoke-virtual {v0, v3}, Ltv/periscope/model/p$a;->b(Ljava/util/ArrayList;)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v4, v5}, Ltv/periscope/model/p$a;->b(J)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 54
    invoke-virtual {v0, v4, v5}, Ltv/periscope/model/p$a;->a(J)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 55
    invoke-virtual {v0, v3}, Ltv/periscope/model/p$a;->c(Ljava/util/ArrayList;)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 56
    invoke-virtual {v0, v2}, Ltv/periscope/model/p$a;->a(I)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 57
    invoke-virtual {v0, v2}, Ltv/periscope/model/p$a;->e(Z)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 58
    invoke-virtual {v0, v2}, Ltv/periscope/model/p$a;->d(Z)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 37
    return-object v0
.end method

.method public static a(Ltv/periscope/model/p;)I
    .locals 4

    .prologue
    .line 228
    invoke-virtual {p0}, Ltv/periscope/model/p;->I()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ltv/periscope/model/p;->H()J

    move-result-wide v0

    .line 229
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 230
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v0

    long-to-int v0, v0

    rsub-int/lit8 v0, v0, 0x18

    return v0

    .line 228
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/model/p;->I()J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public abstract A()I
.end method

.method public abstract B()Z
.end method

.method public abstract C()Ljava/lang/String;
.end method

.method public abstract D()Ljava/lang/String;
.end method

.method public abstract E()Z
.end method

.method public F()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Ltv/periscope/model/p;->h:I

    return v0
.end method

.method public H()J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 92
    iget-wide v0, p0, Ltv/periscope/model/p;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 93
    iget-wide v0, p0, Ltv/periscope/model/p;->b:J

    .line 99
    :goto_0
    return-wide v0

    .line 94
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/model/p;->a()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 95
    invoke-virtual {p0}, Ltv/periscope/model/p;->a()J

    move-result-wide v0

    goto :goto_0

    .line 96
    :cond_1
    invoke-virtual {p0}, Ltv/periscope/model/p;->b()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 97
    invoke-virtual {p0}, Ltv/periscope/model/p;->b()J

    move-result-wide v0

    goto :goto_0

    .line 99
    :cond_2
    invoke-virtual {p0}, Ltv/periscope/model/p;->m()J

    move-result-wide v0

    goto :goto_0
.end method

.method public I()J
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Ltv/periscope/model/p;->b:J

    return-wide v0
.end method

.method public J()Ltv/periscope/model/BroadcastState;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Ltv/periscope/model/p;->c:Ltv/periscope/model/BroadcastState;

    return-object v0
.end method

.method public K()Z
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Ltv/periscope/model/p;->J()Ltv/periscope/model/BroadcastState;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/BroadcastState;->c:Ltv/periscope/model/BroadcastState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L()Z
    .locals 2

    .prologue
    .line 169
    invoke-virtual {p0}, Ltv/periscope/model/p;->J()Ltv/periscope/model/BroadcastState;

    move-result-object v0

    .line 170
    sget-object v1, Ltv/periscope/model/BroadcastState;->e:Ltv/periscope/model/BroadcastState;

    if-eq v0, v1, :cond_0

    sget-object v1, Ltv/periscope/model/BroadcastState;->d:Ltv/periscope/model/BroadcastState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public M()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Ltv/periscope/model/p;->a:Z

    return v0
.end method

.method public N()Z
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Ltv/periscope/model/p;->d:Z

    return v0
.end method

.method public O()Z
    .locals 1

    .prologue
    .line 202
    iget-boolean v0, p0, Ltv/periscope/model/p;->e:Z

    return v0
.end method

.method public P()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Ltv/periscope/model/p;->g:Ljava/lang/Long;

    return-object v0
.end method

.method public Q()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Ltv/periscope/model/p;->i:Ljava/lang/Long;

    return-object v0
.end method

.method public R()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Ltv/periscope/model/p;->j:Ljava/lang/String;

    return-object v0
.end method

.method public S()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Ltv/periscope/model/p;->k:Ljava/lang/Long;

    return-object v0
.end method

.method public abstract a()J
.end method

.method public a(Ljava/util/concurrent/TimeUnit;)J
    .locals 4

    .prologue
    .line 174
    invoke-virtual {p0}, Ltv/periscope/model/p;->I()J

    move-result-wide v0

    invoke-virtual {p0}, Ltv/periscope/model/p;->m()J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 30
    iput p1, p0, Ltv/periscope/model/p;->h:I

    .line 31
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 108
    iput-wide p1, p0, Ltv/periscope/model/p;->b:J

    .line 109
    return-void
.end method

.method public a(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Ltv/periscope/model/p;->g:Ljava/lang/Long;

    .line 215
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Ltv/periscope/model/p;->j:Ljava/lang/String;

    .line 250
    return-void
.end method

.method public a(Ltv/periscope/model/BroadcastState;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Ltv/periscope/model/p;->c:Ltv/periscope/model/BroadcastState;

    .line 139
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 182
    iput-boolean p1, p0, Ltv/periscope/model/p;->a:Z

    .line 183
    return-void
.end method

.method public abstract b()J
.end method

.method public b(Ljava/util/concurrent/TimeUnit;)J
    .locals 4

    .prologue
    .line 178
    invoke-virtual {p0}, Ltv/periscope/model/p;->H()J

    move-result-wide v0

    invoke-virtual {p0}, Ltv/periscope/model/p;->m()J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 210
    iput-wide p1, p0, Ltv/periscope/model/p;->f:J

    .line 211
    return-void
.end method

.method public b(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Ltv/periscope/model/p;->i:Ljava/lang/Long;

    .line 240
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 194
    iput-boolean p1, p0, Ltv/periscope/model/p;->d:Z

    .line 195
    return-void
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public c(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Ltv/periscope/model/p;->k:Ljava/lang/Long;

    .line 258
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 198
    iput-boolean p1, p0, Ltv/periscope/model/p;->e:Z

    .line 199
    return-void
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract e()Ltv/periscope/model/z;
.end method

.method public abstract f()J
.end method

.method public abstract g()Z
.end method

.method public abstract h()Ljava/lang/String;
.end method

.method public abstract i()Ljava/lang/String;
.end method

.method public abstract j()Ljava/lang/String;
.end method

.method public abstract k()J
.end method

.method public abstract l()J
.end method

.method public abstract m()J
.end method

.method public abstract n()D
.end method

.method public abstract o()D
.end method

.method public abstract p()Ljava/lang/String;
.end method

.method public abstract q()Z
.end method

.method public abstract r()Ljava/lang/String;
.end method

.method public abstract s()Ljava/lang/String;
.end method

.method public abstract t()Ljava/lang/String;
.end method

.method public abstract u()Ljava/lang/String;
.end method

.method public abstract v()Ljava/lang/String;
.end method

.method public abstract w()Z
.end method

.method public abstract x()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract y()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract z()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
