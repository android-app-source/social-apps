.class final Ltv/periscope/model/c$a;
.super Ltv/periscope/model/r$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/model/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/t;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ltv/periscope/model/ChannelType;

.field private h:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Ltv/periscope/model/r$a;-><init>()V

    .line 143
    return-void
.end method


# virtual methods
.method public a(J)Ltv/periscope/model/r$a;
    .locals 1

    .prologue
    .line 166
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/c$a;->c:Ljava/lang/Long;

    .line 167
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ltv/periscope/model/r$a;
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Ltv/periscope/model/c$a;->a:Ljava/lang/String;

    .line 157
    return-object p0
.end method

.method public a(Ljava/util/List;)Ltv/periscope/model/r$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/t;",
            ">;)",
            "Ltv/periscope/model/r$a;"
        }
    .end annotation

    .prologue
    .line 181
    iput-object p1, p0, Ltv/periscope/model/c$a;->f:Ljava/util/List;

    .line 182
    return-object p0
.end method

.method public a(Ltv/periscope/model/ChannelType;)Ltv/periscope/model/r$a;
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Ltv/periscope/model/c$a;->g:Ltv/periscope/model/ChannelType;

    .line 187
    return-object p0
.end method

.method public a(Z)Ltv/periscope/model/r$a;
    .locals 1

    .prologue
    .line 171
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/c$a;->d:Ljava/lang/Boolean;

    .line 172
    return-object p0
.end method

.method public a()Ltv/periscope/model/r;
    .locals 12

    .prologue
    .line 196
    const-string/jumbo v0, ""

    .line 197
    iget-object v1, p0, Ltv/periscope/model/c$a;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 198
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " channelId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 200
    :cond_0
    iget-object v1, p0, Ltv/periscope/model/c$a;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 201
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " description"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 203
    :cond_1
    iget-object v1, p0, Ltv/periscope/model/c$a;->c:Ljava/lang/Long;

    if-nez v1, :cond_2

    .line 204
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " numberOfLiveStreams"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 206
    :cond_2
    iget-object v1, p0, Ltv/periscope/model/c$a;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_3

    .line 207
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " featured"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 209
    :cond_3
    iget-object v1, p0, Ltv/periscope/model/c$a;->e:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 210
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " publicTag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 212
    :cond_4
    iget-object v1, p0, Ltv/periscope/model/c$a;->f:Ljava/util/List;

    if-nez v1, :cond_5

    .line 213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " thumbnails"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 215
    :cond_5
    iget-object v1, p0, Ltv/periscope/model/c$a;->g:Ltv/periscope/model/ChannelType;

    if-nez v1, :cond_6

    .line 216
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " channelType"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 218
    :cond_6
    iget-object v1, p0, Ltv/periscope/model/c$a;->h:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 219
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ownerId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 221
    :cond_7
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    .line 222
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Missing required properties:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 224
    :cond_8
    new-instance v1, Ltv/periscope/model/c;

    iget-object v2, p0, Ltv/periscope/model/c$a;->a:Ljava/lang/String;

    iget-object v3, p0, Ltv/periscope/model/c$a;->b:Ljava/lang/String;

    iget-object v0, p0, Ltv/periscope/model/c$a;->c:Ljava/lang/Long;

    .line 227
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p0, Ltv/periscope/model/c$a;->d:Ljava/lang/Boolean;

    .line 228
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iget-object v7, p0, Ltv/periscope/model/c$a;->e:Ljava/lang/String;

    iget-object v8, p0, Ltv/periscope/model/c$a;->f:Ljava/util/List;

    iget-object v9, p0, Ltv/periscope/model/c$a;->g:Ltv/periscope/model/ChannelType;

    iget-object v10, p0, Ltv/periscope/model/c$a;->h:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-direct/range {v1 .. v11}, Ltv/periscope/model/c;-><init>(Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/util/List;Ltv/periscope/model/ChannelType;Ljava/lang/String;Ltv/periscope/model/c$1;)V

    .line 224
    return-object v1
.end method

.method public b(Ljava/lang/String;)Ltv/periscope/model/r$a;
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Ltv/periscope/model/c$a;->b:Ljava/lang/String;

    .line 162
    return-object p0
.end method

.method public c(Ljava/lang/String;)Ltv/periscope/model/r$a;
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Ltv/periscope/model/c$a;->e:Ljava/lang/String;

    .line 177
    return-object p0
.end method

.method public d(Ljava/lang/String;)Ltv/periscope/model/r$a;
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Ltv/periscope/model/c$a;->h:Ljava/lang/String;

    .line 192
    return-object p0
.end method
