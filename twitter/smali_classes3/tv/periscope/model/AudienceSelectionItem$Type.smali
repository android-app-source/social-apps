.class public final enum Ltv/periscope/model/AudienceSelectionItem$Type;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/model/AudienceSelectionItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/model/AudienceSelectionItem$Type;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltv/periscope/model/AudienceSelectionItem$Type;

.field public static final enum b:Ltv/periscope/model/AudienceSelectionItem$Type;

.field public static final enum c:Ltv/periscope/model/AudienceSelectionItem$Type;

.field public static final enum d:Ltv/periscope/model/AudienceSelectionItem$Type;

.field public static final enum e:Ltv/periscope/model/AudienceSelectionItem$Type;

.field public static final enum f:Ltv/periscope/model/AudienceSelectionItem$Type;

.field public static final enum g:Ltv/periscope/model/AudienceSelectionItem$Type;

.field public static final enum h:Ltv/periscope/model/AudienceSelectionItem$Type;

.field private static final synthetic i:[Ltv/periscope/model/AudienceSelectionItem$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Ltv/periscope/model/AudienceSelectionItem$Type;

    const-string/jumbo v1, "Header"

    invoke-direct {v0, v1, v3}, Ltv/periscope/model/AudienceSelectionItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/model/AudienceSelectionItem$Type;->a:Ltv/periscope/model/AudienceSelectionItem$Type;

    .line 9
    new-instance v0, Ltv/periscope/model/AudienceSelectionItem$Type;

    const-string/jumbo v1, "Public"

    invoke-direct {v0, v1, v4}, Ltv/periscope/model/AudienceSelectionItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/model/AudienceSelectionItem$Type;->b:Ltv/periscope/model/AudienceSelectionItem$Type;

    .line 10
    new-instance v0, Ltv/periscope/model/AudienceSelectionItem$Type;

    const-string/jumbo v1, "ChannelId"

    invoke-direct {v0, v1, v5}, Ltv/periscope/model/AudienceSelectionItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/model/AudienceSelectionItem$Type;->c:Ltv/periscope/model/AudienceSelectionItem$Type;

    .line 11
    new-instance v0, Ltv/periscope/model/AudienceSelectionItem$Type;

    const-string/jumbo v1, "RecentAudience"

    invoke-direct {v0, v1, v6}, Ltv/periscope/model/AudienceSelectionItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/model/AudienceSelectionItem$Type;->d:Ltv/periscope/model/AudienceSelectionItem$Type;

    .line 12
    new-instance v0, Ltv/periscope/model/AudienceSelectionItem$Type;

    const-string/jumbo v1, "CreateChannel"

    invoke-direct {v0, v1, v7}, Ltv/periscope/model/AudienceSelectionItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/model/AudienceSelectionItem$Type;->e:Ltv/periscope/model/AudienceSelectionItem$Type;

    .line 13
    new-instance v0, Ltv/periscope/model/AudienceSelectionItem$Type;

    const-string/jumbo v1, "Divider"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ltv/periscope/model/AudienceSelectionItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/model/AudienceSelectionItem$Type;->f:Ltv/periscope/model/AudienceSelectionItem$Type;

    .line 14
    new-instance v0, Ltv/periscope/model/AudienceSelectionItem$Type;

    const-string/jumbo v1, "SearchBar"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Ltv/periscope/model/AudienceSelectionItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/model/AudienceSelectionItem$Type;->g:Ltv/periscope/model/AudienceSelectionItem$Type;

    .line 15
    new-instance v0, Ltv/periscope/model/AudienceSelectionItem$Type;

    const-string/jumbo v1, "UserId"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Ltv/periscope/model/AudienceSelectionItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/model/AudienceSelectionItem$Type;->h:Ltv/periscope/model/AudienceSelectionItem$Type;

    .line 7
    const/16 v0, 0x8

    new-array v0, v0, [Ltv/periscope/model/AudienceSelectionItem$Type;

    sget-object v1, Ltv/periscope/model/AudienceSelectionItem$Type;->a:Ltv/periscope/model/AudienceSelectionItem$Type;

    aput-object v1, v0, v3

    sget-object v1, Ltv/periscope/model/AudienceSelectionItem$Type;->b:Ltv/periscope/model/AudienceSelectionItem$Type;

    aput-object v1, v0, v4

    sget-object v1, Ltv/periscope/model/AudienceSelectionItem$Type;->c:Ltv/periscope/model/AudienceSelectionItem$Type;

    aput-object v1, v0, v5

    sget-object v1, Ltv/periscope/model/AudienceSelectionItem$Type;->d:Ltv/periscope/model/AudienceSelectionItem$Type;

    aput-object v1, v0, v6

    sget-object v1, Ltv/periscope/model/AudienceSelectionItem$Type;->e:Ltv/periscope/model/AudienceSelectionItem$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ltv/periscope/model/AudienceSelectionItem$Type;->f:Ltv/periscope/model/AudienceSelectionItem$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ltv/periscope/model/AudienceSelectionItem$Type;->g:Ltv/periscope/model/AudienceSelectionItem$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ltv/periscope/model/AudienceSelectionItem$Type;->h:Ltv/periscope/model/AudienceSelectionItem$Type;

    aput-object v2, v0, v1

    sput-object v0, Ltv/periscope/model/AudienceSelectionItem$Type;->i:[Ltv/periscope/model/AudienceSelectionItem$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/model/AudienceSelectionItem$Type;
    .locals 1

    .prologue
    .line 7
    const-class v0, Ltv/periscope/model/AudienceSelectionItem$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/AudienceSelectionItem$Type;

    return-object v0
.end method

.method public static values()[Ltv/periscope/model/AudienceSelectionItem$Type;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Ltv/periscope/model/AudienceSelectionItem$Type;->i:[Ltv/periscope/model/AudienceSelectionItem$Type;

    invoke-virtual {v0}, [Ltv/periscope/model/AudienceSelectionItem$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/model/AudienceSelectionItem$Type;

    return-object v0
.end method
