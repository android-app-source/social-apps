.class final Ltv/periscope/model/a;
.super Ltv/periscope/model/p;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/model/a$a;
    }
.end annotation


# instance fields
.field private final A:I

.field private final B:Z

.field private final C:Ljava/lang/String;

.field private final D:Ljava/lang/String;

.field private final E:Z

.field private final a:J

.field private final b:J

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ltv/periscope/model/z;

.field private final f:J

.field private final g:Z

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:J

.field private final l:J

.field private final m:J

.field private final n:D

.field private final o:D

.field private final p:Ljava/lang/String;

.field private final q:Z

.field private final r:Ljava/lang/String;

.field private final s:Ljava/lang/String;

.field private final t:Ljava/lang/String;

.field private final u:Ljava/lang/String;

.field private final v:Ljava/lang/String;

.field private final w:Z

.field private final x:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ltv/periscope/model/z;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJDDLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IZLjava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ltv/periscope/model/z;",
            "JZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJJDD",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;IZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0}, Ltv/periscope/model/p;-><init>()V

    .line 75
    iput-wide p1, p0, Ltv/periscope/model/a;->a:J

    .line 76
    iput-wide p3, p0, Ltv/periscope/model/a;->b:J

    .line 77
    iput-object p5, p0, Ltv/periscope/model/a;->c:Ljava/lang/String;

    .line 78
    iput-object p6, p0, Ltv/periscope/model/a;->d:Ljava/lang/String;

    .line 79
    iput-object p7, p0, Ltv/periscope/model/a;->e:Ltv/periscope/model/z;

    .line 80
    iput-wide p8, p0, Ltv/periscope/model/a;->f:J

    .line 81
    iput-boolean p10, p0, Ltv/periscope/model/a;->g:Z

    .line 82
    iput-object p11, p0, Ltv/periscope/model/a;->h:Ljava/lang/String;

    .line 83
    iput-object p12, p0, Ltv/periscope/model/a;->i:Ljava/lang/String;

    .line 84
    iput-object p13, p0, Ltv/periscope/model/a;->j:Ljava/lang/String;

    .line 85
    move-wide/from16 v0, p14

    iput-wide v0, p0, Ltv/periscope/model/a;->k:J

    .line 86
    move-wide/from16 v0, p16

    iput-wide v0, p0, Ltv/periscope/model/a;->l:J

    .line 87
    move-wide/from16 v0, p18

    iput-wide v0, p0, Ltv/periscope/model/a;->m:J

    .line 88
    move-wide/from16 v0, p20

    iput-wide v0, p0, Ltv/periscope/model/a;->n:D

    .line 89
    move-wide/from16 v0, p22

    iput-wide v0, p0, Ltv/periscope/model/a;->o:D

    .line 90
    move-object/from16 v0, p24

    iput-object v0, p0, Ltv/periscope/model/a;->p:Ljava/lang/String;

    .line 91
    move/from16 v0, p25

    iput-boolean v0, p0, Ltv/periscope/model/a;->q:Z

    .line 92
    move-object/from16 v0, p26

    iput-object v0, p0, Ltv/periscope/model/a;->r:Ljava/lang/String;

    .line 93
    move-object/from16 v0, p27

    iput-object v0, p0, Ltv/periscope/model/a;->s:Ljava/lang/String;

    .line 94
    move-object/from16 v0, p28

    iput-object v0, p0, Ltv/periscope/model/a;->t:Ljava/lang/String;

    .line 95
    move-object/from16 v0, p29

    iput-object v0, p0, Ltv/periscope/model/a;->u:Ljava/lang/String;

    .line 96
    move-object/from16 v0, p30

    iput-object v0, p0, Ltv/periscope/model/a;->v:Ljava/lang/String;

    .line 97
    move/from16 v0, p31

    iput-boolean v0, p0, Ltv/periscope/model/a;->w:Z

    .line 98
    move-object/from16 v0, p32

    iput-object v0, p0, Ltv/periscope/model/a;->x:Ljava/util/ArrayList;

    .line 99
    move-object/from16 v0, p33

    iput-object v0, p0, Ltv/periscope/model/a;->y:Ljava/util/ArrayList;

    .line 100
    move-object/from16 v0, p34

    iput-object v0, p0, Ltv/periscope/model/a;->z:Ljava/util/ArrayList;

    .line 101
    move/from16 v0, p35

    iput v0, p0, Ltv/periscope/model/a;->A:I

    .line 102
    move/from16 v0, p36

    iput-boolean v0, p0, Ltv/periscope/model/a;->B:Z

    .line 103
    move-object/from16 v0, p37

    iput-object v0, p0, Ltv/periscope/model/a;->C:Ljava/lang/String;

    .line 104
    move-object/from16 v0, p38

    iput-object v0, p0, Ltv/periscope/model/a;->D:Ljava/lang/String;

    .line 105
    move/from16 v0, p39

    iput-boolean v0, p0, Ltv/periscope/model/a;->E:Z

    .line 106
    return-void
.end method

.method synthetic constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ltv/periscope/model/z;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJDDLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IZLjava/lang/String;Ljava/lang/String;ZLtv/periscope/model/a$1;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct/range {p0 .. p39}, Ltv/periscope/model/a;-><init>(JJLjava/lang/String;Ljava/lang/String;Ltv/periscope/model/z;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJDDLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IZLjava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public A()I
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Ltv/periscope/model/a;->A:I

    return v0
.end method

.method public B()Z
    .locals 1

    .prologue
    .line 256
    iget-boolean v0, p0, Ltv/periscope/model/a;->B:Z

    return v0
.end method

.method public C()Ljava/lang/String;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Ltv/periscope/model/a;->C:Ljava/lang/String;

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Ltv/periscope/model/a;->D:Ljava/lang/String;

    return-object v0
.end method

.method public E()Z
    .locals 1

    .prologue
    .line 273
    iget-boolean v0, p0, Ltv/periscope/model/a;->E:Z

    return v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 110
    iget-wide v0, p0, Ltv/periscope/model/a;->a:J

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 115
    iget-wide v0, p0, Ltv/periscope/model/a;->b:J

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ltv/periscope/model/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Ltv/periscope/model/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ltv/periscope/model/z;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Ltv/periscope/model/a;->e:Ltv/periscope/model/z;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 315
    if-ne p1, p0, :cond_1

    .line 352
    :cond_0
    :goto_0
    return v0

    .line 318
    :cond_1
    instance-of v2, p1, Ltv/periscope/model/p;

    if-eqz v2, :cond_10

    .line 319
    check-cast p1, Ltv/periscope/model/p;

    .line 320
    iget-wide v2, p0, Ltv/periscope/model/a;->a:J

    invoke-virtual {p1}, Ltv/periscope/model/p;->a()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/a;->b:J

    .line 321
    invoke-virtual {p1}, Ltv/periscope/model/p;->b()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/a;->c:Ljava/lang/String;

    .line 322
    invoke-virtual {p1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/a;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 323
    invoke-virtual {p1}, Ltv/periscope/model/p;->d()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v2, p0, Ltv/periscope/model/a;->e:Ltv/periscope/model/z;

    .line 324
    invoke-virtual {p1}, Ltv/periscope/model/p;->e()Ltv/periscope/model/z;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/a;->f:J

    .line 325
    invoke-virtual {p1}, Ltv/periscope/model/p;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-boolean v2, p0, Ltv/periscope/model/a;->g:Z

    .line 326
    invoke-virtual {p1}, Ltv/periscope/model/p;->g()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Ltv/periscope/model/a;->h:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 327
    invoke-virtual {p1}, Ltv/periscope/model/p;->h()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    iget-object v2, p0, Ltv/periscope/model/a;->i:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 328
    invoke-virtual {p1}, Ltv/periscope/model/p;->i()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_3
    iget-object v2, p0, Ltv/periscope/model/a;->j:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 329
    invoke-virtual {p1}, Ltv/periscope/model/p;->j()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_4
    iget-wide v2, p0, Ltv/periscope/model/a;->k:J

    .line 330
    invoke-virtual {p1}, Ltv/periscope/model/p;->k()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/a;->l:J

    .line 331
    invoke-virtual {p1}, Ltv/periscope/model/p;->l()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/a;->m:J

    .line 332
    invoke-virtual {p1}, Ltv/periscope/model/p;->m()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/a;->n:D

    .line 333
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-virtual {p1}, Ltv/periscope/model/p;->n()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/a;->o:D

    .line 334
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-virtual {p1}, Ltv/periscope/model/p;->o()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/a;->p:Ljava/lang/String;

    .line 335
    invoke-virtual {p1}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Ltv/periscope/model/a;->q:Z

    .line 336
    invoke-virtual {p1}, Ltv/periscope/model/p;->q()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Ltv/periscope/model/a;->r:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 337
    invoke-virtual {p1}, Ltv/periscope/model/p;->r()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_5
    iget-object v2, p0, Ltv/periscope/model/a;->s:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 338
    invoke-virtual {p1}, Ltv/periscope/model/p;->s()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_6
    iget-object v2, p0, Ltv/periscope/model/a;->t:Ljava/lang/String;

    .line 339
    invoke-virtual {p1}, Ltv/periscope/model/p;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/a;->u:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 340
    invoke-virtual {p1}, Ltv/periscope/model/p;->u()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_7
    iget-object v2, p0, Ltv/periscope/model/a;->v:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 341
    invoke-virtual {p1}, Ltv/periscope/model/p;->v()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_8
    iget-boolean v2, p0, Ltv/periscope/model/a;->w:Z

    .line 342
    invoke-virtual {p1}, Ltv/periscope/model/p;->w()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Ltv/periscope/model/a;->x:Ljava/util/ArrayList;

    if-nez v2, :cond_b

    .line 343
    invoke-virtual {p1}, Ltv/periscope/model/p;->x()Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_9
    iget-object v2, p0, Ltv/periscope/model/a;->y:Ljava/util/ArrayList;

    if-nez v2, :cond_c

    .line 344
    invoke-virtual {p1}, Ltv/periscope/model/p;->y()Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_a
    iget-object v2, p0, Ltv/periscope/model/a;->z:Ljava/util/ArrayList;

    if-nez v2, :cond_d

    .line 345
    invoke-virtual {p1}, Ltv/periscope/model/p;->z()Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_b
    iget v2, p0, Ltv/periscope/model/a;->A:I

    .line 346
    invoke-virtual {p1}, Ltv/periscope/model/p;->A()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Ltv/periscope/model/a;->B:Z

    .line 347
    invoke-virtual {p1}, Ltv/periscope/model/p;->B()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Ltv/periscope/model/a;->C:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 348
    invoke-virtual {p1}, Ltv/periscope/model/p;->C()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_c
    iget-object v2, p0, Ltv/periscope/model/a;->D:Ljava/lang/String;

    if-nez v2, :cond_f

    .line 349
    invoke-virtual {p1}, Ltv/periscope/model/p;->D()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_d
    iget-boolean v2, p0, Ltv/periscope/model/a;->E:Z

    .line 350
    invoke-virtual {p1}, Ltv/periscope/model/p;->E()Z

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto/16 :goto_0

    .line 323
    :cond_3
    iget-object v2, p0, Ltv/periscope/model/a;->d:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/p;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1

    .line 327
    :cond_4
    iget-object v2, p0, Ltv/periscope/model/a;->h:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/p;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_2

    .line 328
    :cond_5
    iget-object v2, p0, Ltv/periscope/model/a;->i:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/p;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_3

    .line 329
    :cond_6
    iget-object v2, p0, Ltv/periscope/model/a;->j:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/p;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_4

    .line 337
    :cond_7
    iget-object v2, p0, Ltv/periscope/model/a;->r:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/p;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_5

    .line 338
    :cond_8
    iget-object v2, p0, Ltv/periscope/model/a;->s:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/p;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_6

    .line 340
    :cond_9
    iget-object v2, p0, Ltv/periscope/model/a;->u:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/p;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_7

    .line 341
    :cond_a
    iget-object v2, p0, Ltv/periscope/model/a;->v:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/p;->v()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_8

    .line 343
    :cond_b
    iget-object v2, p0, Ltv/periscope/model/a;->x:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ltv/periscope/model/p;->x()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_9

    .line 344
    :cond_c
    iget-object v2, p0, Ltv/periscope/model/a;->y:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ltv/periscope/model/p;->y()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_a

    .line 345
    :cond_d
    iget-object v2, p0, Ltv/periscope/model/a;->z:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ltv/periscope/model/p;->z()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_b

    .line 348
    :cond_e
    iget-object v2, p0, Ltv/periscope/model/a;->C:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/p;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_c

    .line 349
    :cond_f
    iget-object v2, p0, Ltv/periscope/model/a;->D:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/p;->D()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_d

    :cond_10
    move v0, v1

    .line 352
    goto/16 :goto_0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 136
    iget-wide v0, p0, Ltv/periscope/model/a;->f:J

    return-wide v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Ltv/periscope/model/a;->g:Z

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Ltv/periscope/model/a;->h:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 12

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/16 v11, 0x20

    const/4 v1, 0x0

    const v10, 0xf4243

    .line 357
    .line 359
    int-to-long v4, v10

    iget-wide v6, p0, Ltv/periscope/model/a;->a:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Ltv/periscope/model/a;->a:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 360
    mul-int/2addr v0, v10

    .line 361
    int-to-long v4, v0

    iget-wide v6, p0, Ltv/periscope/model/a;->b:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Ltv/periscope/model/a;->b:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 362
    mul-int/2addr v0, v10

    .line 363
    iget-object v4, p0, Ltv/periscope/model/a;->c:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    xor-int/2addr v0, v4

    .line 364
    mul-int v4, v0, v10

    .line 365
    iget-object v0, p0, Ltv/periscope/model/a;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v4

    .line 366
    mul-int/2addr v0, v10

    .line 367
    iget-object v4, p0, Ltv/periscope/model/a;->e:Ltv/periscope/model/z;

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    xor-int/2addr v0, v4

    .line 368
    mul-int/2addr v0, v10

    .line 369
    int-to-long v4, v0

    iget-wide v6, p0, Ltv/periscope/model/a;->f:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Ltv/periscope/model/a;->f:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 370
    mul-int v4, v0, v10

    .line 371
    iget-boolean v0, p0, Ltv/periscope/model/a;->g:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    xor-int/2addr v0, v4

    .line 372
    mul-int v4, v0, v10

    .line 373
    iget-object v0, p0, Ltv/periscope/model/a;->h:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    xor-int/2addr v0, v4

    .line 374
    mul-int v4, v0, v10

    .line 375
    iget-object v0, p0, Ltv/periscope/model/a;->i:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    xor-int/2addr v0, v4

    .line 376
    mul-int v4, v0, v10

    .line 377
    iget-object v0, p0, Ltv/periscope/model/a;->j:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    xor-int/2addr v0, v4

    .line 378
    mul-int/2addr v0, v10

    .line 379
    int-to-long v4, v0

    iget-wide v6, p0, Ltv/periscope/model/a;->k:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Ltv/periscope/model/a;->k:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 380
    mul-int/2addr v0, v10

    .line 381
    int-to-long v4, v0

    iget-wide v6, p0, Ltv/periscope/model/a;->l:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Ltv/periscope/model/a;->l:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 382
    mul-int/2addr v0, v10

    .line 383
    int-to-long v4, v0

    iget-wide v6, p0, Ltv/periscope/model/a;->m:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Ltv/periscope/model/a;->m:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 384
    mul-int/2addr v0, v10

    .line 385
    int-to-long v4, v0

    iget-wide v6, p0, Ltv/periscope/model/a;->n:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Ltv/periscope/model/a;->n:D

    invoke-static {v8, v9}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v8

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 386
    mul-int/2addr v0, v10

    .line 387
    int-to-long v4, v0

    iget-wide v6, p0, Ltv/periscope/model/a;->o:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Ltv/periscope/model/a;->o:D

    invoke-static {v8, v9}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v8

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 388
    mul-int/2addr v0, v10

    .line 389
    iget-object v4, p0, Ltv/periscope/model/a;->p:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    xor-int/2addr v0, v4

    .line 390
    mul-int v4, v0, v10

    .line 391
    iget-boolean v0, p0, Ltv/periscope/model/a;->q:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    xor-int/2addr v0, v4

    .line 392
    mul-int v4, v0, v10

    .line 393
    iget-object v0, p0, Ltv/periscope/model/a;->r:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    xor-int/2addr v0, v4

    .line 394
    mul-int v4, v0, v10

    .line 395
    iget-object v0, p0, Ltv/periscope/model/a;->s:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    xor-int/2addr v0, v4

    .line 396
    mul-int/2addr v0, v10

    .line 397
    iget-object v4, p0, Ltv/periscope/model/a;->t:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    xor-int/2addr v0, v4

    .line 398
    mul-int v4, v0, v10

    .line 399
    iget-object v0, p0, Ltv/periscope/model/a;->u:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    xor-int/2addr v0, v4

    .line 400
    mul-int v4, v0, v10

    .line 401
    iget-object v0, p0, Ltv/periscope/model/a;->v:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    xor-int/2addr v0, v4

    .line 402
    mul-int v4, v0, v10

    .line 403
    iget-boolean v0, p0, Ltv/periscope/model/a;->w:Z

    if-eqz v0, :cond_a

    move v0, v2

    :goto_a
    xor-int/2addr v0, v4

    .line 404
    mul-int v4, v0, v10

    .line 405
    iget-object v0, p0, Ltv/periscope/model/a;->x:Ljava/util/ArrayList;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    xor-int/2addr v0, v4

    .line 406
    mul-int v4, v0, v10

    .line 407
    iget-object v0, p0, Ltv/periscope/model/a;->y:Ljava/util/ArrayList;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    xor-int/2addr v0, v4

    .line 408
    mul-int v4, v0, v10

    .line 409
    iget-object v0, p0, Ltv/periscope/model/a;->z:Ljava/util/ArrayList;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    xor-int/2addr v0, v4

    .line 410
    mul-int/2addr v0, v10

    .line 411
    iget v4, p0, Ltv/periscope/model/a;->A:I

    xor-int/2addr v0, v4

    .line 412
    mul-int v4, v0, v10

    .line 413
    iget-boolean v0, p0, Ltv/periscope/model/a;->B:Z

    if-eqz v0, :cond_e

    move v0, v2

    :goto_e
    xor-int/2addr v0, v4

    .line 414
    mul-int v4, v0, v10

    .line 415
    iget-object v0, p0, Ltv/periscope/model/a;->C:Ljava/lang/String;

    if-nez v0, :cond_f

    move v0, v1

    :goto_f
    xor-int/2addr v0, v4

    .line 416
    mul-int/2addr v0, v10

    .line 417
    iget-object v4, p0, Ltv/periscope/model/a;->D:Ljava/lang/String;

    if-nez v4, :cond_10

    :goto_10
    xor-int/2addr v0, v1

    .line 418
    mul-int/2addr v0, v10

    .line 419
    iget-boolean v1, p0, Ltv/periscope/model/a;->E:Z

    if-eqz v1, :cond_11

    :goto_11
    xor-int/2addr v0, v2

    .line 420
    return v0

    .line 365
    :cond_0
    iget-object v0, p0, Ltv/periscope/model/a;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_1
    move v0, v3

    .line 371
    goto/16 :goto_1

    .line 373
    :cond_2
    iget-object v0, p0, Ltv/periscope/model/a;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 375
    :cond_3
    iget-object v0, p0, Ltv/periscope/model/a;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 377
    :cond_4
    iget-object v0, p0, Ltv/periscope/model/a;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_4

    :cond_5
    move v0, v3

    .line 391
    goto/16 :goto_5

    .line 393
    :cond_6
    iget-object v0, p0, Ltv/periscope/model/a;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_6

    .line 395
    :cond_7
    iget-object v0, p0, Ltv/periscope/model/a;->s:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 399
    :cond_8
    iget-object v0, p0, Ltv/periscope/model/a;->u:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_8

    .line 401
    :cond_9
    iget-object v0, p0, Ltv/periscope/model/a;->v:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_9

    :cond_a
    move v0, v3

    .line 403
    goto/16 :goto_a

    .line 405
    :cond_b
    iget-object v0, p0, Ltv/periscope/model/a;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->hashCode()I

    move-result v0

    goto/16 :goto_b

    .line 407
    :cond_c
    iget-object v0, p0, Ltv/periscope/model/a;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->hashCode()I

    move-result v0

    goto/16 :goto_c

    .line 409
    :cond_d
    iget-object v0, p0, Ltv/periscope/model/a;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->hashCode()I

    move-result v0

    goto/16 :goto_d

    :cond_e
    move v0, v3

    .line 413
    goto :goto_e

    .line 415
    :cond_f
    iget-object v0, p0, Ltv/periscope/model/a;->C:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_f

    .line 417
    :cond_10
    iget-object v1, p0, Ltv/periscope/model/a;->D:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_11
    move v2, v3

    .line 419
    goto :goto_11
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Ltv/periscope/model/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Ltv/periscope/model/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method public k()J
    .locals 2

    .prologue
    .line 164
    iget-wide v0, p0, Ltv/periscope/model/a;->k:J

    return-wide v0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 169
    iget-wide v0, p0, Ltv/periscope/model/a;->l:J

    return-wide v0
.end method

.method public m()J
    .locals 2

    .prologue
    .line 174
    iget-wide v0, p0, Ltv/periscope/model/a;->m:J

    return-wide v0
.end method

.method public n()D
    .locals 2

    .prologue
    .line 179
    iget-wide v0, p0, Ltv/periscope/model/a;->n:D

    return-wide v0
.end method

.method public o()D
    .locals 2

    .prologue
    .line 184
    iget-wide v0, p0, Ltv/periscope/model/a;->o:D

    return-wide v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Ltv/periscope/model/a;->p:Ljava/lang/String;

    return-object v0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Ltv/periscope/model/a;->q:Z

    return v0
.end method

.method public r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Ltv/periscope/model/a;->r:Ljava/lang/String;

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Ltv/periscope/model/a;->s:Ljava/lang/String;

    return-object v0
.end method

.method public t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Ltv/periscope/model/a;->t:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Broadcast{timedOutTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/a;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", pingTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/a;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->e:Ltv/periscope/model/z;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", createdAtMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/a;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", featured="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/model/a;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", featuredCategory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", featuredCategoryColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", featuredReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", featuredTimecodeMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/a;->k:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", sortScore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/a;->l:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", startTimeMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/a;->m:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", ipLat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/a;->n:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", ipLong="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/a;->o:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", locked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/model/a;->q:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", imageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", imageUrlSmall="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", userDisplayName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", profileImageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", twitterUsername="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", hasLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/model/a;->w:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", shareUserIds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", shareUserDisplayNames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", heartThemes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->z:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", cameraRotation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ltv/periscope/model/a;->A:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", highlightAvailable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/model/a;->B:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", username="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", tweetId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/a;->D:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", is360="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/model/a;->E:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Ltv/periscope/model/a;->u:Ljava/lang/String;

    return-object v0
.end method

.method public v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Ltv/periscope/model/a;->v:Ljava/lang/String;

    return-object v0
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Ltv/periscope/model/a;->w:Z

    return v0
.end method

.method public x()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    iget-object v0, p0, Ltv/periscope/model/a;->x:Ljava/util/ArrayList;

    return-object v0
.end method

.method public y()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Ltv/periscope/model/a;->y:Ljava/util/ArrayList;

    return-object v0
.end method

.method public z()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Ltv/periscope/model/a;->z:Ljava/util/ArrayList;

    return-object v0
.end method
