.class public final enum Ltv/periscope/model/ChannelType;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/model/ChannelType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltv/periscope/model/ChannelType;

.field public static final enum b:Ltv/periscope/model/ChannelType;

.field public static final enum c:Ltv/periscope/model/ChannelType;

.field private static final synthetic d:[Ltv/periscope/model/ChannelType;


# instance fields
.field private final mTypeId:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Ltv/periscope/model/ChannelType;

    const-string/jumbo v1, "Public"

    invoke-direct {v0, v1, v2, v2}, Ltv/periscope/model/ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ltv/periscope/model/ChannelType;->a:Ltv/periscope/model/ChannelType;

    .line 5
    new-instance v0, Ltv/periscope/model/ChannelType;

    const-string/jumbo v1, "Private"

    invoke-direct {v0, v1, v3, v3}, Ltv/periscope/model/ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ltv/periscope/model/ChannelType;->b:Ltv/periscope/model/ChannelType;

    .line 6
    new-instance v0, Ltv/periscope/model/ChannelType;

    const-string/jumbo v1, "Curated"

    invoke-direct {v0, v1, v4, v4}, Ltv/periscope/model/ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ltv/periscope/model/ChannelType;->c:Ltv/periscope/model/ChannelType;

    .line 3
    const/4 v0, 0x3

    new-array v0, v0, [Ltv/periscope/model/ChannelType;

    sget-object v1, Ltv/periscope/model/ChannelType;->a:Ltv/periscope/model/ChannelType;

    aput-object v1, v0, v2

    sget-object v1, Ltv/periscope/model/ChannelType;->b:Ltv/periscope/model/ChannelType;

    aput-object v1, v0, v3

    sget-object v1, Ltv/periscope/model/ChannelType;->c:Ltv/periscope/model/ChannelType;

    aput-object v1, v0, v4

    sput-object v0, Ltv/periscope/model/ChannelType;->d:[Ltv/periscope/model/ChannelType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10
    iput p3, p0, Ltv/periscope/model/ChannelType;->mTypeId:I

    .line 11
    return-void
.end method

.method public static a(I)Ltv/periscope/model/ChannelType;
    .locals 1

    .prologue
    .line 18
    packed-switch p0, :pswitch_data_0

    .line 29
    sget-object v0, Ltv/periscope/model/ChannelType;->a:Ltv/periscope/model/ChannelType;

    :goto_0
    return-object v0

    .line 20
    :pswitch_0
    sget-object v0, Ltv/periscope/model/ChannelType;->a:Ltv/periscope/model/ChannelType;

    goto :goto_0

    .line 23
    :pswitch_1
    sget-object v0, Ltv/periscope/model/ChannelType;->b:Ltv/periscope/model/ChannelType;

    goto :goto_0

    .line 26
    :pswitch_2
    sget-object v0, Ltv/periscope/model/ChannelType;->c:Ltv/periscope/model/ChannelType;

    goto :goto_0

    .line 18
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/model/ChannelType;
    .locals 1

    .prologue
    .line 3
    const-class v0, Ltv/periscope/model/ChannelType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/ChannelType;

    return-object v0
.end method

.method public static values()[Ltv/periscope/model/ChannelType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Ltv/periscope/model/ChannelType;->d:[Ltv/periscope/model/ChannelType;

    invoke-virtual {v0}, [Ltv/periscope/model/ChannelType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/model/ChannelType;

    return-object v0
.end method
