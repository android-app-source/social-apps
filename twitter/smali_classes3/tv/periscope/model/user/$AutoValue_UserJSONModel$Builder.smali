.class final Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;
.super Ltv/periscope/model/user/UserJSONModel$Builder;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/model/user/$AutoValue_UserJSONModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private className:Ljava/lang/String;

.field private createdAt:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private displayName:Ljava/lang/String;

.field private hasDigitsId:Ljava/lang/Boolean;

.field private id:Ljava/lang/String;

.field private initials:Ljava/lang/String;

.field private isBlocked:Ljava/lang/Boolean;

.field private isBluebirdUser:Ljava/lang/Boolean;

.field private isEmployee:Ljava/lang/Boolean;

.field private isFollowing:Ljava/lang/Boolean;

.field private isMuted:Ljava/lang/Boolean;

.field private isVerified:Ljava/lang/Boolean;

.field private numFollowers:Ljava/lang/Long;

.field private numFollowing:Ljava/lang/Long;

.field private numHearts:Ljava/lang/Long;

.field private numHeartsGiven:Ljava/lang/Long;

.field private participantIndex:Ljava/lang/Long;

.field private profileImageUrls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/ProfileImageUrlJSONModel;",
            ">;"
        }
    .end annotation
.end field

.field private twitterId:Ljava/lang/String;

.field private twitterUsername:Ljava/lang/String;

.field private updatedAt:Ljava/lang/String;

.field private username:Ljava/lang/String;

.field private vipBadge:Ltv/periscope/model/user/UserJSONModel$VipBadge;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 395
    invoke-direct {p0}, Ltv/periscope/model/user/UserJSONModel$Builder;-><init>()V

    .line 396
    return-void
.end method

.method constructor <init>(Ltv/periscope/model/user/UserJSONModel;)V
    .locals 1

    .prologue
    .line 397
    invoke-direct {p0}, Ltv/periscope/model/user/UserJSONModel$Builder;-><init>()V

    .line 398
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->className()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->className:Ljava/lang/String;

    .line 399
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->id()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->id:Ljava/lang/String;

    .line 400
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->createdAt()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->createdAt:Ljava/lang/String;

    .line 401
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->updatedAt()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->updatedAt:Ljava/lang/String;

    .line 402
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->username()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->username:Ljava/lang/String;

    .line 403
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->displayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->displayName:Ljava/lang/String;

    .line 404
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->initials()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->initials:Ljava/lang/String;

    .line 405
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->description()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->description:Ljava/lang/String;

    .line 406
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->profileImageUrls()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->profileImageUrls:Ljava/util/List;

    .line 407
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->numFollowers()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->numFollowers:Ljava/lang/Long;

    .line 408
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->numFollowing()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->numFollowing:Ljava/lang/Long;

    .line 409
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isFollowing()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isFollowing:Ljava/lang/Boolean;

    .line 410
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isMuted()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isMuted:Ljava/lang/Boolean;

    .line 411
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isBlocked()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isBlocked:Ljava/lang/Boolean;

    .line 412
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->hasDigitsId()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->hasDigitsId:Ljava/lang/Boolean;

    .line 413
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->numHearts()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->numHearts:Ljava/lang/Long;

    .line 414
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isEmployee()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isEmployee:Ljava/lang/Boolean;

    .line 415
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->numHeartsGiven()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->numHeartsGiven:Ljava/lang/Long;

    .line 416
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->participantIndex()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->participantIndex:Ljava/lang/Long;

    .line 417
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isVerified()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isVerified:Ljava/lang/Boolean;

    .line 418
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isBluebirdUser()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isBluebirdUser:Ljava/lang/Boolean;

    .line 419
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->twitterUsername()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->twitterUsername:Ljava/lang/String;

    .line 420
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->twitterId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->twitterId:Ljava/lang/String;

    .line 421
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->vipBadge()Ltv/periscope/model/user/UserJSONModel$VipBadge;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->vipBadge:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    .line 422
    return-void
.end method


# virtual methods
.method public build()Ltv/periscope/model/user/UserJSONModel;
    .locals 26

    .prologue
    .line 545
    const-string/jumbo v1, ""

    .line 546
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->className:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 547
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " className"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 549
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->id:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 550
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 552
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->createdAt:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 553
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " createdAt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 555
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->username:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 556
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " username"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 558
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->displayName:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 559
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " displayName"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 561
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->profileImageUrls:Ljava/util/List;

    if-nez v2, :cond_5

    .line 562
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " profileImageUrls"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 564
    :cond_5
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 565
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Missing required properties:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 567
    :cond_6
    new-instance v1, Ltv/periscope/model/user/AutoValue_UserJSONModel;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->className:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->createdAt:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->updatedAt:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->username:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->displayName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->initials:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->description:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->profileImageUrls:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v11, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->numFollowers:Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v12, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->numFollowing:Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v13, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isFollowing:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v14, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isMuted:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v15, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isBlocked:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->hasDigitsId:Ljava/lang/Boolean;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->numHearts:Ljava/lang/Long;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isEmployee:Ljava/lang/Boolean;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->numHeartsGiven:Ljava/lang/Long;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->participantIndex:Ljava/lang/Long;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isVerified:Ljava/lang/Boolean;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isBluebirdUser:Ljava/lang/Boolean;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->twitterUsername:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->twitterId:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->vipBadge:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    move-object/from16 v25, v0

    invoke-direct/range {v1 .. v25}, Ltv/periscope/model/user/AutoValue_UserJSONModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/user/UserJSONModel$VipBadge;)V

    return-object v1
.end method

.method public setClassName(Ljava/lang/String;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->className:Ljava/lang/String;

    .line 426
    return-object p0
.end method

.method public setCreatedAt(Ljava/lang/String;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->createdAt:Ljava/lang/String;

    .line 436
    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 460
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->description:Ljava/lang/String;

    .line 461
    return-object p0
.end method

.method public setDisplayName(Ljava/lang/String;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 450
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->displayName:Ljava/lang/String;

    .line 451
    return-object p0
.end method

.method public setHasDigitsId(Ljava/lang/Boolean;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 495
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->hasDigitsId:Ljava/lang/Boolean;

    .line 496
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->id:Ljava/lang/String;

    .line 431
    return-object p0
.end method

.method public setInitials(Ljava/lang/String;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->initials:Ljava/lang/String;

    .line 456
    return-object p0
.end method

.method public setIsBlocked(Ljava/lang/Boolean;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isBlocked:Ljava/lang/Boolean;

    .line 491
    return-object p0
.end method

.method public setIsBluebirdUser(Ljava/lang/Boolean;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 525
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isBluebirdUser:Ljava/lang/Boolean;

    .line 526
    return-object p0
.end method

.method public setIsEmployee(Ljava/lang/Boolean;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 505
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isEmployee:Ljava/lang/Boolean;

    .line 506
    return-object p0
.end method

.method public setIsFollowing(Ljava/lang/Boolean;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 480
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isFollowing:Ljava/lang/Boolean;

    .line 481
    return-object p0
.end method

.method public setIsMuted(Ljava/lang/Boolean;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isMuted:Ljava/lang/Boolean;

    .line 486
    return-object p0
.end method

.method public setIsVerified(Ljava/lang/Boolean;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 520
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->isVerified:Ljava/lang/Boolean;

    .line 521
    return-object p0
.end method

.method public setNumFollowers(Ljava/lang/Long;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 470
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->numFollowers:Ljava/lang/Long;

    .line 471
    return-object p0
.end method

.method public setNumFollowing(Ljava/lang/Long;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 475
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->numFollowing:Ljava/lang/Long;

    .line 476
    return-object p0
.end method

.method public setNumHearts(Ljava/lang/Long;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 500
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->numHearts:Ljava/lang/Long;

    .line 501
    return-object p0
.end method

.method public setNumHeartsGiven(Ljava/lang/Long;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 510
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->numHeartsGiven:Ljava/lang/Long;

    .line 511
    return-object p0
.end method

.method public setParticipantIndex(Ljava/lang/Long;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 515
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->participantIndex:Ljava/lang/Long;

    .line 516
    return-object p0
.end method

.method public setProfileImageUrls(Ljava/util/List;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/ProfileImageUrlJSONModel;",
            ">;)",
            "Ltv/periscope/model/user/UserJSONModel$Builder;"
        }
    .end annotation

    .prologue
    .line 465
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->profileImageUrls:Ljava/util/List;

    .line 466
    return-object p0
.end method

.method public setTwitterId(Ljava/lang/String;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 535
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->twitterId:Ljava/lang/String;

    .line 536
    return-object p0
.end method

.method public setTwitterUsername(Ljava/lang/String;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 530
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->twitterUsername:Ljava/lang/String;

    .line 531
    return-object p0
.end method

.method public setUpdatedAt(Ljava/lang/String;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->updatedAt:Ljava/lang/String;

    .line 441
    return-object p0
.end method

.method public setUsername(Ljava/lang/String;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 445
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->username:Ljava/lang/String;

    .line 446
    return-object p0
.end method

.method public setVipBadge(Ltv/periscope/model/user/UserJSONModel$VipBadge;)Ltv/periscope/model/user/UserJSONModel$Builder;
    .locals 0

    .prologue
    .line 540
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;->vipBadge:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    .line 541
    return-object p0
.end method
