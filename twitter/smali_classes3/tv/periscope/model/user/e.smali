.class public abstract Ltv/periscope/model/user/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/model/AudienceSelectionItem;
.implements Ltv/periscope/model/ChannelItem;
.implements Ltv/periscope/model/user/UserItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/user/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 34
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 35
    invoke-static {v0}, Ltv/periscope/model/user/e;->a(Ljava/lang/String;)Ltv/periscope/model/user/e;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 37
    :cond_0
    return-object v1
.end method

.method public static a(Ljava/lang/String;)Ltv/periscope/model/user/e;
    .locals 4

    .prologue
    .line 51
    new-instance v0, Ltv/periscope/model/user/b;

    sget-object v1, Ltv/periscope/model/user/UserItem$Type;->c:Ltv/periscope/model/user/UserItem$Type;

    sget-object v2, Ltv/periscope/model/ChannelItem$Type;->d:Ltv/periscope/model/ChannelItem$Type;

    sget-object v3, Ltv/periscope/model/AudienceSelectionItem$Type;->h:Ltv/periscope/model/AudienceSelectionItem$Type;

    invoke-direct {v0, p0, v1, v2, v3}, Ltv/periscope/model/user/b;-><init>(Ljava/lang/String;Ltv/periscope/model/user/UserItem$Type;Ltv/periscope/model/ChannelItem$Type;Ltv/periscope/model/AudienceSelectionItem$Type;)V

    return-object v0
.end method

.method public static b(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 43
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/user/UserItem;

    .line 44
    check-cast v0, Ltv/periscope/model/user/e;

    invoke-virtual {v0}, Ltv/periscope/model/user/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 46
    :cond_0
    return-object v1
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract b()Ltv/periscope/model/ChannelItem$Type;
.end method

.method public abstract c()Ltv/periscope/model/AudienceSelectionItem$Type;
.end method

.method public abstract type()Ltv/periscope/model/user/UserItem$Type;
.end method
