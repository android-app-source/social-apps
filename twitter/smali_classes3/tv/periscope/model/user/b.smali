.class final Ltv/periscope/model/user/b;
.super Ltv/periscope/model/user/e;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ltv/periscope/model/user/UserItem$Type;

.field private final c:Ltv/periscope/model/ChannelItem$Type;

.field private final d:Ltv/periscope/model/AudienceSelectionItem$Type;


# direct methods
.method constructor <init>(Ljava/lang/String;Ltv/periscope/model/user/UserItem$Type;Ltv/periscope/model/ChannelItem$Type;Ltv/periscope/model/AudienceSelectionItem$Type;)V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ltv/periscope/model/user/e;-><init>()V

    .line 20
    if-nez p1, :cond_0

    .line 21
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Null userId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Ltv/periscope/model/user/b;->a:Ljava/lang/String;

    .line 24
    if-nez p2, :cond_1

    .line 25
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Null type"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_1
    iput-object p2, p0, Ltv/periscope/model/user/b;->b:Ltv/periscope/model/user/UserItem$Type;

    .line 28
    if-nez p3, :cond_2

    .line 29
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Null channelItemType"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_2
    iput-object p3, p0, Ltv/periscope/model/user/b;->c:Ltv/periscope/model/ChannelItem$Type;

    .line 32
    if-nez p4, :cond_3

    .line 33
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Null audienceSelectionItemType"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_3
    iput-object p4, p0, Ltv/periscope/model/user/b;->d:Ltv/periscope/model/AudienceSelectionItem$Type;

    .line 36
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ltv/periscope/model/user/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ltv/periscope/model/ChannelItem$Type;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Ltv/periscope/model/user/b;->c:Ltv/periscope/model/ChannelItem$Type;

    return-object v0
.end method

.method public c()Ltv/periscope/model/AudienceSelectionItem$Type;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ltv/periscope/model/user/b;->d:Ltv/periscope/model/AudienceSelectionItem$Type;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 74
    if-ne p1, p0, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    instance-of v2, p1, Ltv/periscope/model/user/e;

    if-eqz v2, :cond_3

    .line 78
    check-cast p1, Ltv/periscope/model/user/e;

    .line 79
    iget-object v2, p0, Ltv/periscope/model/user/b;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/user/e;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/user/b;->b:Ltv/periscope/model/user/UserItem$Type;

    .line 80
    invoke-virtual {p1}, Ltv/periscope/model/user/e;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/user/UserItem$Type;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/user/b;->c:Ltv/periscope/model/ChannelItem$Type;

    .line 81
    invoke-virtual {p1}, Ltv/periscope/model/user/e;->b()Ltv/periscope/model/ChannelItem$Type;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/ChannelItem$Type;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/user/b;->d:Ltv/periscope/model/AudienceSelectionItem$Type;

    .line 82
    invoke-virtual {p1}, Ltv/periscope/model/user/e;->c()Ltv/periscope/model/AudienceSelectionItem$Type;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/AudienceSelectionItem$Type;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 84
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 89
    .line 91
    iget-object v0, p0, Ltv/periscope/model/user/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    .line 92
    mul-int/2addr v0, v2

    .line 93
    iget-object v1, p0, Ltv/periscope/model/user/b;->b:Ltv/periscope/model/user/UserItem$Type;

    invoke-virtual {v1}, Ltv/periscope/model/user/UserItem$Type;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 94
    mul-int/2addr v0, v2

    .line 95
    iget-object v1, p0, Ltv/periscope/model/user/b;->c:Ltv/periscope/model/ChannelItem$Type;

    invoke-virtual {v1}, Ltv/periscope/model/ChannelItem$Type;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 96
    mul-int/2addr v0, v2

    .line 97
    iget-object v1, p0, Ltv/periscope/model/user/b;->d:Ltv/periscope/model/AudienceSelectionItem$Type;

    invoke-virtual {v1}, Ltv/periscope/model/AudienceSelectionItem$Type;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 98
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "UserId{userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/b;->b:Ltv/periscope/model/user/UserItem$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", channelItemType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/b;->c:Ltv/periscope/model/ChannelItem$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", audienceSelectionItemType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/b;->d:Ltv/periscope/model/AudienceSelectionItem$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Ltv/periscope/model/user/UserItem$Type;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ltv/periscope/model/user/b;->b:Ltv/periscope/model/user/UserItem$Type;

    return-object v0
.end method
