.class public final Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;
.super Lcom/google/gson/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/model/user/AutoValue_UserJSONModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/r",
        "<",
        "Ltv/periscope/model/user/UserJSONModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final classNameAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final createdAtAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final descriptionAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final displayNameAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final hasDigitsIdAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final idAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final initialsAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final isBlockedAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isBluebirdUserAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isEmployeeAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isFollowingAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isMutedAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isVerifiedAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final numFollowersAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final numFollowingAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final numHeartsAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final numHeartsGivenAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final participantIndexAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final profileImageUrlsAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/ProfileImageUrlJSONModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final twitterIdAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final twitterUsernameAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final updatedAtAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final usernameAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final vipBadgeAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ltv/periscope/model/user/UserJSONModel$VipBadge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/e;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/gson/r;-><init>()V

    .line 48
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->classNameAdapter:Lcom/google/gson/r;

    .line 49
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->idAdapter:Lcom/google/gson/r;

    .line 50
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->createdAtAdapter:Lcom/google/gson/r;

    .line 51
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->updatedAtAdapter:Lcom/google/gson/r;

    .line 52
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->usernameAdapter:Lcom/google/gson/r;

    .line 53
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->displayNameAdapter:Lcom/google/gson/r;

    .line 54
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->initialsAdapter:Lcom/google/gson/r;

    .line 55
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/r;

    .line 56
    new-instance v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter$1;

    invoke-direct {v0, p0}, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter$1;-><init>(Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;)V

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Lkr;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->profileImageUrlsAdapter:Lcom/google/gson/r;

    .line 57
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->numFollowersAdapter:Lcom/google/gson/r;

    .line 58
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->numFollowingAdapter:Lcom/google/gson/r;

    .line 59
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isFollowingAdapter:Lcom/google/gson/r;

    .line 60
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isMutedAdapter:Lcom/google/gson/r;

    .line 61
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isBlockedAdapter:Lcom/google/gson/r;

    .line 62
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->hasDigitsIdAdapter:Lcom/google/gson/r;

    .line 63
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->numHeartsAdapter:Lcom/google/gson/r;

    .line 64
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isEmployeeAdapter:Lcom/google/gson/r;

    .line 65
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->numHeartsGivenAdapter:Lcom/google/gson/r;

    .line 66
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->participantIndexAdapter:Lcom/google/gson/r;

    .line 67
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isVerifiedAdapter:Lcom/google/gson/r;

    .line 68
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isBluebirdUserAdapter:Lcom/google/gson/r;

    .line 69
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->twitterUsernameAdapter:Lcom/google/gson/r;

    .line 70
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->twitterIdAdapter:Lcom/google/gson/r;

    .line 71
    const-class v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->vipBadgeAdapter:Lcom/google/gson/r;

    .line 72
    return-void
.end method


# virtual methods
.method public bridge synthetic read(Lcom/google/gson/stream/a;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->read(Lcom/google/gson/stream/a;)Ltv/periscope/model/user/UserJSONModel;

    move-result-object v0

    return-object v0
.end method

.method public read(Lcom/google/gson/stream/a;)Ltv/periscope/model/user/UserJSONModel;
    .locals 28
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/a;->c()V

    .line 165
    const/4 v2, 0x0

    .line 166
    const/4 v3, 0x0

    .line 167
    const/4 v4, 0x0

    .line 168
    const/4 v5, 0x0

    .line 169
    const/4 v6, 0x0

    .line 170
    const/4 v7, 0x0

    .line 171
    const/4 v8, 0x0

    .line 172
    const/4 v9, 0x0

    .line 173
    const/4 v10, 0x0

    .line 174
    const/4 v11, 0x0

    .line 175
    const/4 v12, 0x0

    .line 176
    const/4 v13, 0x0

    .line 177
    const/4 v14, 0x0

    .line 178
    const/4 v15, 0x0

    .line 179
    const/16 v16, 0x0

    .line 180
    const/16 v17, 0x0

    .line 181
    const/16 v18, 0x0

    .line 182
    const/16 v19, 0x0

    .line 183
    const/16 v20, 0x0

    .line 184
    const/16 v21, 0x0

    .line 185
    const/16 v22, 0x0

    .line 186
    const/16 v23, 0x0

    .line 187
    const/16 v24, 0x0

    .line 188
    const/16 v25, 0x0

    .line 189
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/a;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 190
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/a;->g()Ljava/lang/String;

    move-result-object v26

    .line 191
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/a;->f()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v27, Lcom/google/gson/stream/JsonToken;->i:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v27

    if-ne v1, v0, :cond_0

    .line 192
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/a;->n()V

    goto :goto_0

    .line 195
    :cond_0
    const/4 v1, -0x1

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->hashCode()I

    move-result v27

    sparse-switch v27, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 293
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/a;->n()V

    goto :goto_0

    .line 195
    :sswitch_0
    const-string/jumbo v27, "class_name"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v27, "id"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v27, "created_at"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v27, "updated_at"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_4
    const-string/jumbo v27, "username"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/4 v1, 0x4

    goto :goto_1

    :sswitch_5
    const-string/jumbo v27, "display_name"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/4 v1, 0x5

    goto :goto_1

    :sswitch_6
    const-string/jumbo v27, "initials"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/4 v1, 0x6

    goto :goto_1

    :sswitch_7
    const-string/jumbo v27, "description"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/4 v1, 0x7

    goto :goto_1

    :sswitch_8
    const-string/jumbo v27, "profile_image_urls"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0x8

    goto :goto_1

    :sswitch_9
    const-string/jumbo v27, "n_followers"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0x9

    goto :goto_1

    :sswitch_a
    const-string/jumbo v27, "n_following"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0xa

    goto/16 :goto_1

    :sswitch_b
    const-string/jumbo v27, "is_following"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0xb

    goto/16 :goto_1

    :sswitch_c
    const-string/jumbo v27, "is_muted"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0xc

    goto/16 :goto_1

    :sswitch_d
    const-string/jumbo v27, "is_blocked"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0xd

    goto/16 :goto_1

    :sswitch_e
    const-string/jumbo v27, "has_digits_id"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0xe

    goto/16 :goto_1

    :sswitch_f
    const-string/jumbo v27, "n_hearts"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0xf

    goto/16 :goto_1

    :sswitch_10
    const-string/jumbo v27, "is_employee"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0x10

    goto/16 :goto_1

    :sswitch_11
    const-string/jumbo v27, "n_hearts_given"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0x11

    goto/16 :goto_1

    :sswitch_12
    const-string/jumbo v27, "participant_index"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0x12

    goto/16 :goto_1

    :sswitch_13
    const-string/jumbo v27, "is_twitter_verified"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0x13

    goto/16 :goto_1

    :sswitch_14
    const-string/jumbo v27, "is_bluebird_user"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0x14

    goto/16 :goto_1

    :sswitch_15
    const-string/jumbo v27, "twitter_screen_name"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0x15

    goto/16 :goto_1

    :sswitch_16
    const-string/jumbo v27, "twitter_id"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0x16

    goto/16 :goto_1

    :sswitch_17
    const-string/jumbo v27, "vip"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const/16 v1, 0x17

    goto/16 :goto_1

    .line 197
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->classNameAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v2, v1

    .line 198
    goto/16 :goto_0

    .line 201
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->idAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v3, v1

    .line 202
    goto/16 :goto_0

    .line 205
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->createdAtAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v4, v1

    .line 206
    goto/16 :goto_0

    .line 209
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->updatedAtAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v5, v1

    .line 210
    goto/16 :goto_0

    .line 213
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->usernameAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v6, v1

    .line 214
    goto/16 :goto_0

    .line 217
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->displayNameAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v7, v1

    .line 218
    goto/16 :goto_0

    .line 221
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->initialsAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v8, v1

    .line 222
    goto/16 :goto_0

    .line 225
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v9, v1

    .line 226
    goto/16 :goto_0

    .line 229
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->profileImageUrlsAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    move-object v10, v1

    .line 230
    goto/16 :goto_0

    .line 233
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->numFollowersAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    move-object v11, v1

    .line 234
    goto/16 :goto_0

    .line 237
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->numFollowingAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    move-object v12, v1

    .line 238
    goto/16 :goto_0

    .line 241
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isFollowingAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    move-object v13, v1

    .line 242
    goto/16 :goto_0

    .line 245
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isMutedAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    move-object v14, v1

    .line 246
    goto/16 :goto_0

    .line 249
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isBlockedAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    move-object v15, v1

    .line 250
    goto/16 :goto_0

    .line 253
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->hasDigitsIdAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v16, v1

    .line 254
    goto/16 :goto_0

    .line 257
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->numHeartsAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    move-object/from16 v17, v1

    .line 258
    goto/16 :goto_0

    .line 261
    :pswitch_10
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isEmployeeAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v18, v1

    .line 262
    goto/16 :goto_0

    .line 265
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->numHeartsGivenAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    move-object/from16 v19, v1

    .line 266
    goto/16 :goto_0

    .line 269
    :pswitch_12
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->participantIndexAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    move-object/from16 v20, v1

    .line 270
    goto/16 :goto_0

    .line 273
    :pswitch_13
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isVerifiedAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v21, v1

    .line 274
    goto/16 :goto_0

    .line 277
    :pswitch_14
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isBluebirdUserAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v22, v1

    .line 278
    goto/16 :goto_0

    .line 281
    :pswitch_15
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->twitterUsernameAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v23, v1

    .line 282
    goto/16 :goto_0

    .line 285
    :pswitch_16
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->twitterIdAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v24, v1

    .line 286
    goto/16 :goto_0

    .line 289
    :pswitch_17
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->vipBadgeAdapter:Lcom/google/gson/r;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltv/periscope/model/user/UserJSONModel$VipBadge;

    move-object/from16 v25, v1

    .line 290
    goto/16 :goto_0

    .line 297
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/a;->d()V

    .line 298
    new-instance v1, Ltv/periscope/model/user/AutoValue_UserJSONModel;

    invoke-direct/range {v1 .. v25}, Ltv/periscope/model/user/AutoValue_UserJSONModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/user/UserJSONModel$VipBadge;)V

    return-object v1

    .line 195
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7df2a624 -> :sswitch_11
        -0x66ca7c04 -> :sswitch_7
        -0x41a80e62 -> :sswitch_8
        -0x4099354e -> :sswitch_15
        -0x30d3527c -> :sswitch_9
        -0x30d34400 -> :sswitch_a
        -0x242b3a77 -> :sswitch_13
        -0x207ef244 -> :sswitch_b
        -0x1dfa9851 -> :sswitch_e
        -0x119c6dc9 -> :sswitch_3
        -0x11504b0e -> :sswitch_0
        -0xfd6772a -> :sswitch_4
        -0x499a222 -> :sswitch_f
        -0x3843199 -> :sswitch_16
        0xd1b -> :sswitch_1
        0x1c81d -> :sswitch_17
        0x71b5a16 -> :sswitch_c
        0x100991af -> :sswitch_6
        0x1b533a23 -> :sswitch_10
        0x4a76ae22 -> :sswitch_14
        0x4f4408c6 -> :sswitch_12
        0x51a3a8ea -> :sswitch_2
        0x5825cbd7 -> :sswitch_d
        0x604443e8 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p2, Ltv/periscope/model/user/UserJSONModel;

    invoke-virtual {p0, p1, p2}, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->write(Lcom/google/gson/stream/b;Ltv/periscope/model/user/UserJSONModel;)V

    return-void
.end method

.method public write(Lcom/google/gson/stream/b;Ltv/periscope/model/user/UserJSONModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p1}, Lcom/google/gson/stream/b;->d()Lcom/google/gson/stream/b;

    .line 76
    const-string/jumbo v0, "class_name"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 77
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->classNameAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->className()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 78
    const-string/jumbo v0, "id"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 79
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->idAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->id()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 80
    const-string/jumbo v0, "created_at"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 81
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->createdAtAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->createdAt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 82
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->updatedAt()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 83
    const-string/jumbo v0, "updated_at"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 84
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->updatedAtAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->updatedAt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 86
    :cond_0
    const-string/jumbo v0, "username"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 87
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->usernameAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->username()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 88
    const-string/jumbo v0, "display_name"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 89
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->displayNameAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->displayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 90
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->initials()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 91
    const-string/jumbo v0, "initials"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 92
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->initialsAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->initials()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 94
    :cond_1
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->description()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 95
    const-string/jumbo v0, "description"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 96
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 98
    :cond_2
    const-string/jumbo v0, "profile_image_urls"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 99
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->profileImageUrlsAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->profileImageUrls()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 100
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->numFollowers()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 101
    const-string/jumbo v0, "n_followers"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 102
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->numFollowersAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->numFollowers()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 104
    :cond_3
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->numFollowing()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 105
    const-string/jumbo v0, "n_following"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 106
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->numFollowingAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->numFollowing()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 108
    :cond_4
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->isFollowing()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 109
    const-string/jumbo v0, "is_following"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 110
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isFollowingAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->isFollowing()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 112
    :cond_5
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->isMuted()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 113
    const-string/jumbo v0, "is_muted"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 114
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isMutedAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->isMuted()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 116
    :cond_6
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->isBlocked()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 117
    const-string/jumbo v0, "is_blocked"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 118
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isBlockedAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->isBlocked()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 120
    :cond_7
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->hasDigitsId()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 121
    const-string/jumbo v0, "has_digits_id"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 122
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->hasDigitsIdAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->hasDigitsId()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 124
    :cond_8
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->numHearts()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 125
    const-string/jumbo v0, "n_hearts"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 126
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->numHeartsAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->numHearts()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 128
    :cond_9
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->isEmployee()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 129
    const-string/jumbo v0, "is_employee"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 130
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isEmployeeAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->isEmployee()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 132
    :cond_a
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->numHeartsGiven()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 133
    const-string/jumbo v0, "n_hearts_given"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 134
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->numHeartsGivenAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->numHeartsGiven()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 136
    :cond_b
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->participantIndex()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 137
    const-string/jumbo v0, "participant_index"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 138
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->participantIndexAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->participantIndex()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 140
    :cond_c
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->isVerified()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 141
    const-string/jumbo v0, "is_twitter_verified"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 142
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isVerifiedAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->isVerified()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 144
    :cond_d
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->isBluebirdUser()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 145
    const-string/jumbo v0, "is_bluebird_user"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 146
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->isBluebirdUserAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->isBluebirdUser()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 148
    :cond_e
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->twitterUsername()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 149
    const-string/jumbo v0, "twitter_screen_name"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 150
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->twitterUsernameAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->twitterUsername()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 152
    :cond_f
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->twitterId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 153
    const-string/jumbo v0, "twitter_id"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 154
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->twitterIdAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->twitterId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 156
    :cond_10
    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->vipBadge()Ltv/periscope/model/user/UserJSONModel$VipBadge;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 157
    const-string/jumbo v0, "vip"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 158
    iget-object v0, p0, Ltv/periscope/model/user/AutoValue_UserJSONModel$GsonTypeAdapter;->vipBadgeAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/user/UserJSONModel;->vipBadge()Ltv/periscope/model/user/UserJSONModel$VipBadge;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 160
    :cond_11
    invoke-virtual {p1}, Lcom/google/gson/stream/b;->e()Lcom/google/gson/stream/b;

    .line 161
    return-void
.end method
