.class final Ltv/periscope/model/user/a;
.super Ltv/periscope/model/user/d;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ltv/periscope/model/ChannelType;

.field private final c:Ltv/periscope/model/user/UserItem$Type;

.field private final d:Ltv/periscope/model/ChannelItem$Type;

.field private final e:Ltv/periscope/model/AudienceSelectionItem$Type;


# direct methods
.method constructor <init>(Ljava/lang/String;Ltv/periscope/model/ChannelType;Ltv/periscope/model/user/UserItem$Type;Ltv/periscope/model/ChannelItem$Type;Ltv/periscope/model/AudienceSelectionItem$Type;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ltv/periscope/model/user/d;-><init>()V

    .line 24
    if-nez p1, :cond_0

    .line 25
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Null channelId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_0
    iput-object p1, p0, Ltv/periscope/model/user/a;->a:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Ltv/periscope/model/user/a;->b:Ltv/periscope/model/ChannelType;

    .line 29
    if-nez p3, :cond_1

    .line 30
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Null type"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_1
    iput-object p3, p0, Ltv/periscope/model/user/a;->c:Ltv/periscope/model/user/UserItem$Type;

    .line 33
    if-nez p4, :cond_2

    .line 34
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Null channelItemType"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_2
    iput-object p4, p0, Ltv/periscope/model/user/a;->d:Ltv/periscope/model/ChannelItem$Type;

    .line 37
    if-nez p5, :cond_3

    .line 38
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Null audienceSelectionItemType"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_3
    iput-object p5, p0, Ltv/periscope/model/user/a;->e:Ltv/periscope/model/AudienceSelectionItem$Type;

    .line 41
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ltv/periscope/model/user/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ltv/periscope/model/ChannelType;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Ltv/periscope/model/user/a;->b:Ltv/periscope/model/ChannelType;

    return-object v0
.end method

.method public c()Ltv/periscope/model/ChannelItem$Type;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Ltv/periscope/model/user/a;->d:Ltv/periscope/model/ChannelItem$Type;

    return-object v0
.end method

.method public d()Ltv/periscope/model/AudienceSelectionItem$Type;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Ltv/periscope/model/user/a;->e:Ltv/periscope/model/AudienceSelectionItem$Type;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 86
    if-ne p1, p0, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v0

    .line 89
    :cond_1
    instance-of v2, p1, Ltv/periscope/model/user/d;

    if-eqz v2, :cond_4

    .line 90
    check-cast p1, Ltv/periscope/model/user/d;

    .line 91
    iget-object v2, p0, Ltv/periscope/model/user/a;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/user/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/user/a;->b:Ltv/periscope/model/ChannelType;

    if-nez v2, :cond_3

    .line 92
    invoke-virtual {p1}, Ltv/periscope/model/user/d;->b()Ltv/periscope/model/ChannelType;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v2, p0, Ltv/periscope/model/user/a;->c:Ltv/periscope/model/user/UserItem$Type;

    .line 93
    invoke-virtual {p1}, Ltv/periscope/model/user/d;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/user/UserItem$Type;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/user/a;->d:Ltv/periscope/model/ChannelItem$Type;

    .line 94
    invoke-virtual {p1}, Ltv/periscope/model/user/d;->c()Ltv/periscope/model/ChannelItem$Type;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/ChannelItem$Type;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/user/a;->e:Ltv/periscope/model/AudienceSelectionItem$Type;

    .line 95
    invoke-virtual {p1}, Ltv/periscope/model/user/d;->d()Ltv/periscope/model/AudienceSelectionItem$Type;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/AudienceSelectionItem$Type;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 92
    :cond_3
    iget-object v2, p0, Ltv/periscope/model/user/a;->b:Ltv/periscope/model/ChannelType;

    invoke-virtual {p1}, Ltv/periscope/model/user/d;->b()Ltv/periscope/model/ChannelType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/ChannelType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_4
    move v0, v1

    .line 97
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 102
    .line 104
    iget-object v0, p0, Ltv/periscope/model/user/a;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    .line 105
    mul-int v1, v0, v2

    .line 106
    iget-object v0, p0, Ltv/periscope/model/user/a;->b:Ltv/periscope/model/ChannelType;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 107
    mul-int/2addr v0, v2

    .line 108
    iget-object v1, p0, Ltv/periscope/model/user/a;->c:Ltv/periscope/model/user/UserItem$Type;

    invoke-virtual {v1}, Ltv/periscope/model/user/UserItem$Type;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 109
    mul-int/2addr v0, v2

    .line 110
    iget-object v1, p0, Ltv/periscope/model/user/a;->d:Ltv/periscope/model/ChannelItem$Type;

    invoke-virtual {v1}, Ltv/periscope/model/ChannelItem$Type;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 111
    mul-int/2addr v0, v2

    .line 112
    iget-object v1, p0, Ltv/periscope/model/user/a;->e:Ltv/periscope/model/AudienceSelectionItem$Type;

    invoke-virtual {v1}, Ltv/periscope/model/AudienceSelectionItem$Type;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 113
    return v0

    .line 106
    :cond_0
    iget-object v0, p0, Ltv/periscope/model/user/a;->b:Ltv/periscope/model/ChannelType;

    invoke-virtual {v0}, Ltv/periscope/model/ChannelType;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ChannelId{channelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", channelType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/a;->b:Ltv/periscope/model/ChannelType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/a;->c:Ltv/periscope/model/user/UserItem$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", channelItemType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/a;->d:Ltv/periscope/model/ChannelItem$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", audienceSelectionItemType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/a;->e:Ltv/periscope/model/AudienceSelectionItem$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Ltv/periscope/model/user/UserItem$Type;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Ltv/periscope/model/user/a;->c:Ltv/periscope/model/user/UserItem$Type;

    return-object v0
.end method
