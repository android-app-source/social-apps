.class public abstract Ltv/periscope/model/user/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/model/AudienceSelectionItem;
.implements Ltv/periscope/model/ChannelItem;
.implements Ltv/periscope/model/user/UserItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/user/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-static {p0, v0}, Ltv/periscope/model/user/d;->a(Ljava/util/List;Ltv/periscope/model/ChannelType;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;Ltv/periscope/model/ChannelType;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ltv/periscope/model/ChannelType;",
            ")",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/user/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 63
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 64
    invoke-static {v0, p1}, Ltv/periscope/model/user/d;->a(Ljava/lang/String;Ltv/periscope/model/ChannelType;)Ltv/periscope/model/user/d;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    :cond_0
    return-object v1
.end method

.method public static a(Ljava/lang/String;Ltv/periscope/model/ChannelType;)Ltv/periscope/model/user/d;
    .locals 6

    .prologue
    .line 71
    new-instance v0, Ltv/periscope/model/user/a;

    sget-object v3, Ltv/periscope/model/user/UserItem$Type;->i:Ltv/periscope/model/user/UserItem$Type;

    sget-object v4, Ltv/periscope/model/ChannelItem$Type;->c:Ltv/periscope/model/ChannelItem$Type;

    sget-object v5, Ltv/periscope/model/AudienceSelectionItem$Type;->c:Ltv/periscope/model/AudienceSelectionItem$Type;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/model/user/a;-><init>(Ljava/lang/String;Ltv/periscope/model/ChannelType;Ltv/periscope/model/user/UserItem$Type;Ltv/periscope/model/ChannelItem$Type;Ltv/periscope/model/AudienceSelectionItem$Type;)V

    return-object v0
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract b()Ltv/periscope/model/ChannelType;
.end method

.method public abstract c()Ltv/periscope/model/ChannelItem$Type;
.end method

.method public abstract d()Ltv/periscope/model/AudienceSelectionItem$Type;
.end method

.method public abstract type()Ltv/periscope/model/user/UserItem$Type;
.end method
