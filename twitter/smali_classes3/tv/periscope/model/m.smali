.class final Ltv/periscope/model/m;
.super Ltv/periscope/model/ac;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/model/m$a;
    }
.end annotation


# instance fields
.field private final a:Ltv/periscope/model/user/UserItem;

.field private final b:Z

.field private final c:I

.field private final d:I

.field private final e:J


# direct methods
.method private constructor <init>(Ltv/periscope/model/user/UserItem;ZIIJ)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ltv/periscope/model/ac;-><init>()V

    .line 22
    iput-object p1, p0, Ltv/periscope/model/m;->a:Ltv/periscope/model/user/UserItem;

    .line 23
    iput-boolean p2, p0, Ltv/periscope/model/m;->b:Z

    .line 24
    iput p3, p0, Ltv/periscope/model/m;->c:I

    .line 25
    iput p4, p0, Ltv/periscope/model/m;->d:I

    .line 26
    iput-wide p5, p0, Ltv/periscope/model/m;->e:J

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Ltv/periscope/model/user/UserItem;ZIIJLtv/periscope/model/m$1;)V
    .locals 1

    .prologue
    .line 8
    invoke-direct/range {p0 .. p6}, Ltv/periscope/model/m;-><init>(Ltv/periscope/model/user/UserItem;ZIIJ)V

    return-void
.end method


# virtual methods
.method public a()Ltv/periscope/model/user/UserItem;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ltv/periscope/model/m;->a:Ltv/periscope/model/user/UserItem;

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Ltv/periscope/model/m;->b:Z

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Ltv/periscope/model/m;->c:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Ltv/periscope/model/m;->d:I

    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Ltv/periscope/model/m;->e:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 67
    if-ne p1, p0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v0

    .line 70
    :cond_1
    instance-of v2, p1, Ltv/periscope/model/ac;

    if-eqz v2, :cond_3

    .line 71
    check-cast p1, Ltv/periscope/model/ac;

    .line 72
    iget-object v2, p0, Ltv/periscope/model/m;->a:Ltv/periscope/model/user/UserItem;

    invoke-virtual {p1}, Ltv/periscope/model/ac;->a()Ltv/periscope/model/user/UserItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Ltv/periscope/model/m;->b:Z

    .line 73
    invoke-virtual {p1}, Ltv/periscope/model/ac;->b()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p0, Ltv/periscope/model/m;->c:I

    .line 74
    invoke-virtual {p1}, Ltv/periscope/model/ac;->c()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p0, Ltv/periscope/model/m;->d:I

    .line 75
    invoke-virtual {p1}, Ltv/periscope/model/ac;->d()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/m;->e:J

    .line 76
    invoke-virtual {p1}, Ltv/periscope/model/ac;->e()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 78
    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const v2, 0xf4243

    .line 83
    .line 85
    iget-object v0, p0, Ltv/periscope/model/m;->a:Ltv/periscope/model/user/UserItem;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    .line 86
    mul-int v1, v0, v2

    .line 87
    iget-boolean v0, p0, Ltv/periscope/model/m;->b:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    xor-int/2addr v0, v1

    .line 88
    mul-int/2addr v0, v2

    .line 89
    iget v1, p0, Ltv/periscope/model/m;->c:I

    xor-int/2addr v0, v1

    .line 90
    mul-int/2addr v0, v2

    .line 91
    iget v1, p0, Ltv/periscope/model/m;->d:I

    xor-int/2addr v0, v1

    .line 92
    mul-int/2addr v0, v2

    .line 93
    int-to-long v0, v0

    iget-wide v2, p0, Ltv/periscope/model/m;->e:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    iget-wide v4, p0, Ltv/periscope/model/m;->e:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 94
    return v0

    .line 87
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Superfan{userObject="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/m;->a:Ltv/periscope/model/user/UserItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", isFollowing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/model/m;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", score="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ltv/periscope/model/m;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", rank="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ltv/periscope/model/m;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", superfanSince="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/m;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
