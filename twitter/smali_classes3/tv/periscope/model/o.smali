.class final Ltv/periscope/model/o;
.super Ltv/periscope/model/af;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ltv/periscope/model/p;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/v;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/lang/String;

.field private final j:[I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/p;Ljava/util/List;Ljava/lang/String;[I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ltv/periscope/model/p;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/v;",
            ">;",
            "Ljava/lang/String;",
            "[I)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ltv/periscope/model/af;-><init>()V

    .line 34
    iput-object p1, p0, Ltv/periscope/model/o;->a:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Ltv/periscope/model/o;->b:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Ltv/periscope/model/o;->c:Ljava/lang/String;

    .line 37
    iput-object p4, p0, Ltv/periscope/model/o;->d:Ljava/lang/String;

    .line 38
    iput-object p5, p0, Ltv/periscope/model/o;->e:Ljava/lang/String;

    .line 39
    iput-object p6, p0, Ltv/periscope/model/o;->f:Ljava/lang/String;

    .line 40
    if-nez p7, :cond_0

    .line 41
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Null broadcast"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    iput-object p7, p0, Ltv/periscope/model/o;->g:Ltv/periscope/model/p;

    .line 44
    if-nez p8, :cond_1

    .line 45
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Null cookies"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_1
    iput-object p8, p0, Ltv/periscope/model/o;->h:Ljava/util/List;

    .line 48
    iput-object p9, p0, Ltv/periscope/model/o;->i:Ljava/lang/String;

    .line 49
    iput-object p10, p0, Ltv/periscope/model/o;->j:[I

    .line 50
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Ltv/periscope/model/o;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Ltv/periscope/model/o;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Ltv/periscope/model/o;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ltv/periscope/model/o;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/model/o;->e:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 128
    if-ne p1, p0, :cond_0

    .line 144
    :goto_0
    return v1

    .line 131
    :cond_0
    instance-of v0, p1, Ltv/periscope/model/af;

    if-eqz v0, :cond_b

    .line 132
    check-cast p1, Ltv/periscope/model/af;

    .line 133
    iget-object v0, p0, Ltv/periscope/model/o;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ltv/periscope/model/af;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, Ltv/periscope/model/o;->b:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 134
    invoke-virtual {p1}, Ltv/periscope/model/af;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_1
    iget-object v0, p0, Ltv/periscope/model/o;->c:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 135
    invoke-virtual {p1}, Ltv/periscope/model/af;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_2
    iget-object v0, p0, Ltv/periscope/model/o;->d:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 136
    invoke-virtual {p1}, Ltv/periscope/model/af;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_3
    iget-object v0, p0, Ltv/periscope/model/o;->e:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 137
    invoke-virtual {p1}, Ltv/periscope/model/af;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_4
    iget-object v0, p0, Ltv/periscope/model/o;->f:Ljava/lang/String;

    if-nez v0, :cond_8

    .line 138
    invoke-virtual {p1}, Ltv/periscope/model/af;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_5
    iget-object v0, p0, Ltv/periscope/model/o;->g:Ltv/periscope/model/p;

    .line 139
    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ltv/periscope/model/o;->h:Ljava/util/List;

    .line 140
    invoke-virtual {p1}, Ltv/periscope/model/af;->h()Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ltv/periscope/model/o;->i:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 141
    invoke-virtual {p1}, Ltv/periscope/model/af;->i()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_6
    iget-object v3, p0, Ltv/periscope/model/o;->j:[I

    instance-of v0, p1, Ltv/periscope/model/o;

    if-eqz v0, :cond_a

    check-cast p1, Ltv/periscope/model/o;

    iget-object v0, p1, Ltv/periscope/model/o;->j:[I

    .line 142
    :goto_7
    invoke-static {v3, v0}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_8
    move v1, v0

    .line 133
    goto :goto_0

    :cond_2
    iget-object v0, p0, Ltv/periscope/model/o;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/af;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_3
    move v0, v2

    .line 142
    goto :goto_8

    .line 134
    :cond_4
    iget-object v0, p0, Ltv/periscope/model/o;->b:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/af;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    .line 135
    :cond_5
    iget-object v0, p0, Ltv/periscope/model/o;->c:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/af;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_2

    .line 136
    :cond_6
    iget-object v0, p0, Ltv/periscope/model/o;->d:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/af;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_3

    .line 137
    :cond_7
    iget-object v0, p0, Ltv/periscope/model/o;->e:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/af;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_4

    .line 138
    :cond_8
    iget-object v0, p0, Ltv/periscope/model/o;->f:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/af;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto/16 :goto_5

    .line 141
    :cond_9
    iget-object v0, p0, Ltv/periscope/model/o;->i:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/af;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_6

    .line 142
    :cond_a
    invoke-virtual {p1}, Ltv/periscope/model/af;->j()[I

    move-result-object v0

    goto :goto_7

    :cond_b
    move v1, v2

    .line 144
    goto/16 :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Ltv/periscope/model/o;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ltv/periscope/model/p;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Ltv/periscope/model/o;->g:Ltv/periscope/model/p;

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/v;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Ltv/periscope/model/o;->h:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const v3, 0xf4243

    .line 149
    .line 151
    iget-object v0, p0, Ltv/periscope/model/o;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v3

    .line 152
    mul-int v2, v0, v3

    .line 153
    iget-object v0, p0, Ltv/periscope/model/o;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    xor-int/2addr v0, v2

    .line 154
    mul-int v2, v0, v3

    .line 155
    iget-object v0, p0, Ltv/periscope/model/o;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    xor-int/2addr v0, v2

    .line 156
    mul-int v2, v0, v3

    .line 157
    iget-object v0, p0, Ltv/periscope/model/o;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    xor-int/2addr v0, v2

    .line 158
    mul-int v2, v0, v3

    .line 159
    iget-object v0, p0, Ltv/periscope/model/o;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    xor-int/2addr v0, v2

    .line 160
    mul-int v2, v0, v3

    .line 161
    iget-object v0, p0, Ltv/periscope/model/o;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    xor-int/2addr v0, v2

    .line 162
    mul-int/2addr v0, v3

    .line 163
    iget-object v2, p0, Ltv/periscope/model/o;->g:Ltv/periscope/model/p;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 164
    mul-int/2addr v0, v3

    .line 165
    iget-object v2, p0, Ltv/periscope/model/o;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 166
    mul-int/2addr v0, v3

    .line 167
    iget-object v2, p0, Ltv/periscope/model/o;->i:Ljava/lang/String;

    if-nez v2, :cond_6

    :goto_6
    xor-int/2addr v0, v1

    .line 168
    mul-int/2addr v0, v3

    .line 169
    iget-object v1, p0, Ltv/periscope/model/o;->j:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    xor-int/2addr v0, v1

    .line 170
    return v0

    .line 151
    :cond_0
    iget-object v0, p0, Ltv/periscope/model/o;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 153
    :cond_1
    iget-object v0, p0, Ltv/periscope/model/o;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 155
    :cond_2
    iget-object v0, p0, Ltv/periscope/model/o;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 157
    :cond_3
    iget-object v0, p0, Ltv/periscope/model/o;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 159
    :cond_4
    iget-object v0, p0, Ltv/periscope/model/o;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 161
    :cond_5
    iget-object v0, p0, Ltv/periscope/model/o;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 167
    :cond_6
    iget-object v1, p0, Ltv/periscope/model/o;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Ltv/periscope/model/o;->i:Ljava/lang/String;

    return-object v0
.end method

.method public j()[I
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Ltv/periscope/model/o;->j:[I

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "VideoAccess{chatToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/o;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", lifeCycleToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/o;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", lhlsUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/o;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", rtmpUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/o;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", replayUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/o;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", hlsUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/o;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", broadcast="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/o;->g:Ltv/periscope/model/p;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", cookies="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/o;->h:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", shareUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/o;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", pspVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/o;->j:[I

    .line 122
    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    return-object v0
.end method
