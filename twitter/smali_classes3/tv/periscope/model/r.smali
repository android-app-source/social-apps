.class public abstract Ltv/periscope/model/r;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/model/user/UserItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/model/r$a;
    }
.end annotation


# instance fields
.field public a:J

.field private b:Ljava/lang/String;

.field private c:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ltv/periscope/model/r;->a:J

    return-void
.end method

.method public static j()Ltv/periscope/model/r$a;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Ltv/periscope/model/c$a;

    invoke-direct {v0}, Ltv/periscope/model/c$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 34
    iput-wide p1, p0, Ltv/periscope/model/r;->c:J

    .line 35
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Ltv/periscope/model/r;->b:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()J
.end method

.method public abstract d()Z
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract f()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/t;",
            ">;"
        }
    .end annotation
.end method

.method public abstract g()Ltv/periscope/model/ChannelType;
.end method

.method public abstract h()Ljava/lang/String;
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Ltv/periscope/model/r;->b:Ljava/lang/String;

    return-object v0
.end method

.method public type()Ltv/periscope/model/user/UserItem$Type;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Ltv/periscope/model/user/UserItem$Type;->h:Ltv/periscope/model/user/UserItem$Type;

    return-object v0
.end method
