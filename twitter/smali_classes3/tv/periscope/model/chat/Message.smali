.class public abstract Ltv/periscope/model/chat/Message;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/model/chat/Message$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static N()J
    .locals 4

    .prologue
    .line 33
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static O()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static P()Ltv/periscope/model/chat/Message$a;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ltv/periscope/model/chat/AutoValue_Message$a;

    invoke-direct {v0}, Ltv/periscope/model/chat/AutoValue_Message$a;-><init>()V

    const/4 v1, 0x2

    .line 42
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/AutoValue_Message$a;->a(Ljava/lang/Integer;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 41
    return-object v0
.end method

.method public static Q()Ltv/periscope/model/chat/Message;
    .locals 2

    .prologue
    .line 192
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->h:Ltv/periscope/model/chat/MessageType;

    .line 193
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 192
    return-object v0
.end method

.method public static a(J)Ljava/math/BigInteger;
    .locals 4

    .prologue
    .line 102
    invoke-static {p0, p1}, Lorg/apache/commons/net/ntp/TimeStamp;->b(J)Lorg/apache/commons/net/ntp/TimeStamp;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lorg/apache/commons/net/ntp/TimeStamp;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    const-wide v2, 0x100000000L

    .line 106
    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    .line 107
    invoke-virtual {v0}, Lorg/apache/commons/net/ntp/TimeStamp;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    .line 105
    return-object v0
.end method

.method public static a(JJ)Ltv/periscope/model/chat/Message;
    .locals 4

    .prologue
    .line 158
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->I:Ltv/periscope/model/chat/MessageType;

    .line 159
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 160
    invoke-static {p0, p1}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 161
    invoke-static {p2, p3}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 162
    invoke-static {}, Ltv/periscope/model/chat/Message;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 163
    invoke-static {}, Ltv/periscope/model/chat/Message;->N()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 158
    return-object v0
.end method

.method public static a(JJLjava/lang/Double;Ljava/lang/Double;)Ltv/periscope/model/chat/Message;
    .locals 4

    .prologue
    .line 180
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->e:Ltv/periscope/model/chat/MessageType;

    .line 181
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 182
    invoke-static {p0, p1}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 183
    invoke-static {p2, p3}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 184
    invoke-static {}, Ltv/periscope/model/chat/Message;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 185
    invoke-static {}, Ltv/periscope/model/chat/Message;->N()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 186
    invoke-virtual {v0, p4}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Double;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 187
    invoke-virtual {v0, p5}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/Double;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 180
    return-object v0
.end method

.method public static a(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/model/chat/Message;
    .locals 4

    .prologue
    .line 203
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->m:Ltv/periscope/model/chat/MessageType;

    .line 204
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 205
    invoke-static {p0, p1}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 206
    invoke-static {}, Ltv/periscope/model/chat/Message;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 207
    invoke-static {}, Ltv/periscope/model/chat/Message;->N()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 208
    invoke-virtual {v0, p4}, Ltv/periscope/model/chat/Message$a;->i(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 209
    invoke-virtual {v0, p5}, Ltv/periscope/model/chat/Message$a;->j(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 210
    invoke-virtual {v0, p6}, Ltv/periscope/model/chat/Message$a;->k(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 211
    invoke-static {p2, p3}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 212
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 203
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ltv/periscope/model/chat/Message;
    .locals 2

    .prologue
    .line 266
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->w:Ltv/periscope/model/chat/MessageType;

    .line 267
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 268
    invoke-virtual {v0, p0}, Ltv/periscope/model/chat/Message$a;->r(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 269
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 266
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Long;JJ)Ltv/periscope/model/chat/Message;
    .locals 4

    .prologue
    .line 113
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->c:Ltv/periscope/model/chat/MessageType;

    .line 114
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 115
    invoke-static {p0}, Ldcq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 116
    invoke-virtual {v0, p1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 117
    invoke-static {p2, p3}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 118
    invoke-static {}, Ltv/periscope/model/chat/Message;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 119
    invoke-static {}, Ltv/periscope/model/chat/Message;->N()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 120
    invoke-static {p4, p5}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 113
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;JJ)Ltv/periscope/model/chat/Message;
    .locals 4

    .prologue
    .line 145
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->H:Ltv/periscope/model/chat/MessageType;

    .line 146
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 147
    invoke-virtual {v0, p1}, Ltv/periscope/model/chat/Message$a;->h(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 148
    invoke-static {p0}, Ldcq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 149
    invoke-static {p2, p3}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 150
    invoke-static {p4, p5}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 151
    invoke-static {}, Ltv/periscope/model/chat/Message;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 152
    invoke-static {}, Ltv/periscope/model/chat/Message;->N()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 145
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JJ)Ltv/periscope/model/chat/Message;
    .locals 4

    .prologue
    .line 252
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->q:Ltv/periscope/model/chat/MessageType;

    .line 253
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 254
    invoke-static {p4, p5}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 255
    invoke-static {}, Ltv/periscope/model/chat/Message;->N()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 256
    invoke-static {p2}, Ldcq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 257
    invoke-virtual {v0, p0}, Ltv/periscope/model/chat/Message$a;->d(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 258
    invoke-virtual {v0, p1}, Ltv/periscope/model/chat/Message$a;->e(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 259
    invoke-static {}, Ltv/periscope/model/chat/Message;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 260
    invoke-virtual {v0, p3}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 261
    invoke-static {p6, p7}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 262
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 252
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JJLtv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message;
    .locals 4

    .prologue
    .line 237
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 238
    invoke-virtual {v0, p8}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 239
    invoke-static {p4, p5}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 240
    invoke-static {}, Ltv/periscope/model/chat/Message;->N()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 241
    invoke-static {p2}, Ldcq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 242
    invoke-virtual {v0, p0}, Ltv/periscope/model/chat/Message$a;->d(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 243
    invoke-virtual {v0, p1}, Ltv/periscope/model/chat/Message$a;->e(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 244
    invoke-static {}, Ltv/periscope/model/chat/Message;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 245
    invoke-virtual {v0, p3}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 246
    invoke-static {p6, p7}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 237
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/model/chat/Message;
    .locals 4

    .prologue
    .line 218
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->o:Ltv/periscope/model/chat/MessageType;

    .line 219
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 220
    invoke-static {p4, p5}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 221
    invoke-static {}, Ltv/periscope/model/chat/Message;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 222
    invoke-virtual {v0, p0}, Ltv/periscope/model/chat/Message$a;->d(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 223
    invoke-virtual {v0, p1}, Ltv/periscope/model/chat/Message$a;->e(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 224
    invoke-virtual {v0, p3}, Ltv/periscope/model/chat/Message$a;->g(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 225
    invoke-static {p2}, Ldcq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 226
    invoke-static {}, Ltv/periscope/model/chat/Message;->N()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 227
    invoke-static {p6, p7}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 228
    invoke-virtual {v0, p8}, Ltv/periscope/model/chat/Message$a;->l(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 229
    invoke-virtual {v0, p9}, Ltv/periscope/model/chat/Message$a;->m(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 230
    invoke-virtual {v0, p10}, Ltv/periscope/model/chat/Message$a;->n(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 231
    invoke-virtual {v0, p11}, Ltv/periscope/model/chat/Message$a;->o(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 218
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JJ)Ltv/periscope/model/chat/Message;
    .locals 4

    .prologue
    .line 128
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->d:Ltv/periscope/model/chat/MessageType;

    .line 129
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 130
    invoke-static {p3}, Ldcq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 131
    invoke-virtual {v0, p5}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 132
    invoke-static {p6, p7}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 133
    invoke-static {}, Ltv/periscope/model/chat/Message;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 134
    invoke-static {}, Ltv/periscope/model/chat/Message;->N()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 135
    invoke-virtual {v0, p0}, Ltv/periscope/model/chat/Message$a;->d(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 136
    invoke-virtual {v0, p1}, Ltv/periscope/model/chat/Message$a;->e(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 137
    invoke-virtual {v0, p2}, Ltv/periscope/model/chat/Message$a;->f(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 138
    invoke-virtual {v0, p4}, Ltv/periscope/model/chat/Message$a;->g(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 139
    invoke-static {p8, p9}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 128
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JJLjava/lang/Long;)Ltv/periscope/model/chat/Message;
    .locals 7

    .prologue
    .line 78
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    sget-object v3, Ltv/periscope/model/chat/MessageType;->g:Ltv/periscope/model/chat/MessageType;

    .line 79
    invoke-virtual {v2, v3}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 80
    invoke-static {p4}, Ldcq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 81
    invoke-virtual {v2, p6}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 82
    invoke-static {p7, p8}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 83
    invoke-static {}, Ltv/periscope/model/chat/Message;->O()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 84
    invoke-static {}, Ltv/periscope/model/chat/Message;->N()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 85
    invoke-virtual {v2, p1}, Ltv/periscope/model/chat/Message$a;->d(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 86
    invoke-virtual {v2, p2}, Ltv/periscope/model/chat/Message$a;->e(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 87
    invoke-virtual {v2, p3}, Ltv/periscope/model/chat/Message$a;->f(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 88
    invoke-virtual {v2, p5}, Ltv/periscope/model/chat/Message$a;->g(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 89
    invoke-virtual {v2, p0}, Ltv/periscope/model/chat/Message$a;->h(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 90
    move-object/from16 v0, p11

    invoke-virtual {v2, v0}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 91
    invoke-static/range {p9 .. p10}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/chat/Message$a;->b(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    .line 92
    invoke-virtual {v2}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v2

    .line 78
    return-object v2
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JJ)Ltv/periscope/model/chat/Message;
    .locals 4

    .prologue
    .line 52
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->b:Ltv/periscope/model/chat/MessageType;

    .line 53
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 54
    invoke-static {p4}, Ldcq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 55
    invoke-virtual {v0, p7}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 56
    invoke-static {p8, p9}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 57
    invoke-static {}, Ltv/periscope/model/chat/Message;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 58
    invoke-static {}, Ltv/periscope/model/chat/Message;->N()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 59
    invoke-virtual {v0, p1}, Ltv/periscope/model/chat/Message$a;->d(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 60
    invoke-virtual {v0, p2}, Ltv/periscope/model/chat/Message$a;->e(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 61
    invoke-virtual {v0, p3}, Ltv/periscope/model/chat/Message$a;->f(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 62
    invoke-virtual {v0, p5}, Ltv/periscope/model/chat/Message$a;->g(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 63
    invoke-virtual {v0, p0}, Ltv/periscope/model/chat/Message$a;->h(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 64
    invoke-virtual {v0, p6}, Ltv/periscope/model/chat/Message$a;->u(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 65
    invoke-static {p10, p11}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 52
    return-object v0
.end method

.method public static a(Ltv/periscope/model/chat/MessageType$SentenceType;Ltv/periscope/model/chat/MessageType$ReportType;Ljava/lang/String;)Ltv/periscope/model/chat/Message;
    .locals 2

    .prologue
    .line 274
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->z:Ltv/periscope/model/chat/MessageType;

    .line 275
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 276
    invoke-virtual {v0, p0}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType$SentenceType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 277
    invoke-virtual {v0, p1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType$ReportType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 278
    invoke-virtual {v0, p2}, Ltv/periscope/model/chat/Message$a;->q(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 279
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 274
    return-object v0
.end method

.method public static b(JJ)Ltv/periscope/model/chat/Message;
    .locals 4

    .prologue
    .line 168
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->f:Ltv/periscope/model/chat/MessageType;

    .line 169
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 170
    invoke-static {p0, p1}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 171
    invoke-static {p2, p3}, Ltv/periscope/model/chat/Message;->a(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 172
    invoke-static {}, Ltv/periscope/model/chat/Message;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 173
    invoke-static {}, Ltv/periscope/model/chat/Message;->N()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 168
    return-object v0
.end method


# virtual methods
.method public abstract A()Ltv/periscope/model/chat/MessageType$ReportType;
.end method

.method public abstract B()Ljava/lang/String;
.end method

.method public abstract C()Ljava/lang/String;
.end method

.method public abstract D()Ljava/lang/String;
.end method

.method public abstract E()Ltv/periscope/model/chat/MessageType$VerdictType;
.end method

.method public abstract F()Ljava/lang/String;
.end method

.method public abstract G()Ljava/lang/Integer;
.end method

.method public abstract H()Ltv/periscope/model/chat/MessageType$SentenceType;
.end method

.method public abstract I()Ljava/lang/Integer;
.end method

.method public abstract J()Ljava/lang/String;
.end method

.method public abstract K()Ljava/lang/String;
.end method

.method public abstract L()Ljava/lang/Boolean;
.end method

.method public abstract M()Ljava/lang/Boolean;
.end method

.method public R()Z
    .locals 1

    .prologue
    .line 404
    invoke-virtual {p0}, Ltv/periscope/model/chat/Message;->M()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ltv/periscope/model/chat/Message;->M()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a()Ljava/lang/Integer;
.end method

.method public abstract b()Ltv/periscope/model/chat/MessageType;
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract e()Ljava/lang/Long;
.end method

.method public abstract f()Ljava/math/BigInteger;
.end method

.method public abstract g()Ljava/lang/String;
.end method

.method public abstract h()Ljava/lang/Long;
.end method

.method public abstract i()Ljava/lang/String;
.end method

.method public abstract j()Ljava/lang/String;
.end method

.method public abstract k()Ljava/lang/String;
.end method

.method public abstract l()Ljava/lang/String;
.end method

.method public abstract m()Ljava/lang/String;
.end method

.method public abstract n()Ljava/lang/String;
.end method

.method public abstract o()Ljava/lang/Double;
.end method

.method public abstract p()Ljava/lang/Double;
.end method

.method public abstract q()Ljava/lang/Double;
.end method

.method public abstract r()Ljava/lang/Long;
.end method

.method public abstract s()Ljava/lang/String;
.end method

.method public abstract t()Ljava/lang/String;
.end method

.method public abstract u()Ljava/lang/String;
.end method

.method public abstract v()Ljava/math/BigInteger;
.end method

.method public abstract w()Ljava/lang/String;
.end method

.method public abstract x()Ljava/lang/String;
.end method

.method public abstract y()Ljava/lang/String;
.end method

.method public abstract z()Ljava/lang/String;
.end method
