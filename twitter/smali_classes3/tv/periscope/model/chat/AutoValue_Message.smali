.class final Ltv/periscope/model/chat/AutoValue_Message;
.super Ltv/periscope/model/chat/Message;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/model/chat/AutoValue_Message$a;
    }
.end annotation


# instance fields
.field private final blockedMessageUUID:Ljava/lang/String;

.field private final body:Ljava/lang/String;

.field private final broadcasterBlockedMessage:Ljava/lang/String;

.field private final broadcasterBlockedUserId:Ljava/lang/String;

.field private final broadcasterBlockedUsername:Ljava/lang/String;

.field private final broadcasterNtp:Ljava/math/BigInteger;

.field private final displayName:Ljava/lang/String;

.field private final initials:Ljava/lang/String;

.field private final invitedCount:Ljava/lang/Long;

.field private final juryDuration:Ljava/lang/Integer;

.field private final juryVerdict:Ltv/periscope/model/chat/MessageType$VerdictType;

.field private final latitude:Ljava/lang/Double;

.field private final longitude:Ljava/lang/Double;

.field private final newUser:Ljava/lang/Boolean;

.field private final ntp:Ljava/math/BigInteger;

.field private final participantIndex:Ljava/lang/Long;

.field private final profileImageUrl:Ljava/lang/String;

.field private final rawWireJson:Ljava/lang/String;

.field private final reportType:Ltv/periscope/model/chat/MessageType$ReportType;

.field private final reportedMessageBody:Ljava/lang/String;

.field private final reportedMessageBroadcastID:Ljava/lang/String;

.field private final reportedMessageUUID:Ljava/lang/String;

.field private final reportedMessageUsername:Ljava/lang/String;

.field private final sentenceDuration:Ljava/lang/Integer;

.field private final sentenceType:Ltv/periscope/model/chat/MessageType$SentenceType;

.field private final signature:Ljava/lang/String;

.field private final superfan:Ljava/lang/Boolean;

.field private final timestamp:Ljava/lang/Long;

.field private final timestampPlaybackOffset:Ljava/lang/Double;

.field private final twitterId:Ljava/lang/String;

.field private final type:Ltv/periscope/model/chat/MessageType;

.field private final userId:Ljava/lang/String;

.field private final username:Ljava/lang/String;

.field private final uuid:Ljava/lang/String;

.field private final version:Ljava/lang/Integer;

.field private final viewerBlockedMessage:Ljava/lang/String;

.field private final viewerBlockedUserId:Ljava/lang/String;

.field private final viewerBlockedUsername:Ljava/lang/String;

.field private final vipBadge:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/Integer;Ltv/periscope/model/chat/MessageType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/math/BigInteger;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigInteger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$ReportType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$VerdictType;Ljava/lang/String;Ljava/lang/Integer;Ltv/periscope/model/chat/MessageType$SentenceType;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ltv/periscope/model/chat/Message;-><init>()V

    .line 91
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message;->version:Ljava/lang/Integer;

    .line 92
    iput-object p2, p0, Ltv/periscope/model/chat/AutoValue_Message;->type:Ltv/periscope/model/chat/MessageType;

    .line 93
    iput-object p3, p0, Ltv/periscope/model/chat/AutoValue_Message;->userId:Ljava/lang/String;

    .line 94
    iput-object p4, p0, Ltv/periscope/model/chat/AutoValue_Message;->twitterId:Ljava/lang/String;

    .line 95
    iput-object p5, p0, Ltv/periscope/model/chat/AutoValue_Message;->participantIndex:Ljava/lang/Long;

    .line 96
    iput-object p6, p0, Ltv/periscope/model/chat/AutoValue_Message;->ntp:Ljava/math/BigInteger;

    .line 97
    iput-object p7, p0, Ltv/periscope/model/chat/AutoValue_Message;->uuid:Ljava/lang/String;

    .line 98
    iput-object p8, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestamp:Ljava/lang/Long;

    .line 99
    iput-object p9, p0, Ltv/periscope/model/chat/AutoValue_Message;->signature:Ljava/lang/String;

    .line 100
    iput-object p10, p0, Ltv/periscope/model/chat/AutoValue_Message;->username:Ljava/lang/String;

    .line 101
    iput-object p11, p0, Ltv/periscope/model/chat/AutoValue_Message;->displayName:Ljava/lang/String;

    .line 102
    iput-object p12, p0, Ltv/periscope/model/chat/AutoValue_Message;->initials:Ljava/lang/String;

    .line 103
    iput-object p13, p0, Ltv/periscope/model/chat/AutoValue_Message;->profileImageUrl:Ljava/lang/String;

    .line 104
    iput-object p14, p0, Ltv/periscope/model/chat/AutoValue_Message;->body:Ljava/lang/String;

    .line 105
    move-object/from16 v0, p15

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestampPlaybackOffset:Ljava/lang/Double;

    .line 106
    move-object/from16 v0, p16

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->latitude:Ljava/lang/Double;

    .line 107
    move-object/from16 v0, p17

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->longitude:Ljava/lang/Double;

    .line 108
    move-object/from16 v0, p18

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->invitedCount:Ljava/lang/Long;

    .line 109
    move-object/from16 v0, p19

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedMessage:Ljava/lang/String;

    .line 110
    move-object/from16 v0, p20

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUserId:Ljava/lang/String;

    .line 111
    move-object/from16 v0, p21

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUsername:Ljava/lang/String;

    .line 112
    move-object/from16 v0, p22

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterNtp:Ljava/math/BigInteger;

    .line 113
    move-object/from16 v0, p23

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->blockedMessageUUID:Ljava/lang/String;

    .line 114
    move-object/from16 v0, p24

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedMessage:Ljava/lang/String;

    .line 115
    move-object/from16 v0, p25

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUserId:Ljava/lang/String;

    .line 116
    move-object/from16 v0, p26

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUsername:Ljava/lang/String;

    .line 117
    move-object/from16 v0, p27

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportType:Ltv/periscope/model/chat/MessageType$ReportType;

    .line 118
    move-object/from16 v0, p28

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUUID:Ljava/lang/String;

    .line 119
    move-object/from16 v0, p29

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBody:Ljava/lang/String;

    .line 120
    move-object/from16 v0, p30

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUsername:Ljava/lang/String;

    .line 121
    move-object/from16 v0, p31

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryVerdict:Ltv/periscope/model/chat/MessageType$VerdictType;

    .line 122
    move-object/from16 v0, p32

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBroadcastID:Ljava/lang/String;

    .line 123
    move-object/from16 v0, p33

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryDuration:Ljava/lang/Integer;

    .line 124
    move-object/from16 v0, p34

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceType:Ltv/periscope/model/chat/MessageType$SentenceType;

    .line 125
    move-object/from16 v0, p35

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceDuration:Ljava/lang/Integer;

    .line 126
    move-object/from16 v0, p36

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->rawWireJson:Ljava/lang/String;

    .line 127
    move-object/from16 v0, p37

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->vipBadge:Ljava/lang/String;

    .line 128
    move-object/from16 v0, p38

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->superfan:Ljava/lang/Boolean;

    .line 129
    move-object/from16 v0, p39

    iput-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->newUser:Ljava/lang/Boolean;

    .line 130
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Integer;Ltv/periscope/model/chat/MessageType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/math/BigInteger;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigInteger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$ReportType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$VerdictType;Ljava/lang/String;Ljava/lang/Integer;Ltv/periscope/model/chat/MessageType$SentenceType;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ltv/periscope/model/chat/AutoValue_Message$1;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct/range {p0 .. p39}, Ltv/periscope/model/chat/AutoValue_Message;-><init>(Ljava/lang/Integer;Ltv/periscope/model/chat/MessageType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/math/BigInteger;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigInteger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$ReportType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$VerdictType;Ljava/lang/String;Ljava/lang/Integer;Ltv/periscope/model/chat/MessageType$SentenceType;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    return-void
.end method


# virtual methods
.method public A()Ltv/periscope/model/chat/MessageType$ReportType;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportType:Ltv/periscope/model/chat/MessageType$ReportType;

    return-object v0
.end method

.method public B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUUID:Ljava/lang/String;

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBody:Ljava/lang/String;

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUsername:Ljava/lang/String;

    return-object v0
.end method

.method public E()Ltv/periscope/model/chat/MessageType$VerdictType;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryVerdict:Ltv/periscope/model/chat/MessageType$VerdictType;

    return-object v0
.end method

.method public F()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBroadcastID:Ljava/lang/String;

    return-object v0
.end method

.method public G()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryDuration:Ljava/lang/Integer;

    return-object v0
.end method

.method public H()Ltv/periscope/model/chat/MessageType$SentenceType;
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceType:Ltv/periscope/model/chat/MessageType$SentenceType;

    return-object v0
.end method

.method public I()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceDuration:Ljava/lang/Integer;

    return-object v0
.end method

.method public J()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->rawWireJson:Ljava/lang/String;

    return-object v0
.end method

.method public K()Ljava/lang/String;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->vipBadge:Ljava/lang/String;

    return-object v0
.end method

.method public L()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->superfan:Ljava/lang/Boolean;

    return-object v0
.end method

.method public M()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->newUser:Ljava/lang/Boolean;

    return-object v0
.end method

.method public a()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->version:Ljava/lang/Integer;

    return-object v0
.end method

.method public b()Ltv/periscope/model/chat/MessageType;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->type:Ltv/periscope/model/chat/MessageType;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->twitterId:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->participantIndex:Ljava/lang/Long;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 411
    if-ne p1, p0, :cond_1

    .line 456
    :cond_0
    :goto_0
    return v0

    .line 414
    :cond_1
    instance-of v2, p1, Ltv/periscope/model/chat/Message;

    if-eqz v2, :cond_28

    .line 415
    check-cast p1, Ltv/periscope/model/chat/Message;

    .line 416
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->version:Ljava/lang/Integer;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->a()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->type:Ltv/periscope/model/chat/MessageType;

    .line 417
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/chat/MessageType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->userId:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 418
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->twitterId:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 419
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->d()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->participantIndex:Ljava/lang/Long;

    if-nez v2, :cond_5

    .line 420
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_3
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->ntp:Ljava/math/BigInteger;

    if-nez v2, :cond_6

    .line 421
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->f()Ljava/math/BigInteger;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_4
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->uuid:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 422
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_5
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestamp:Ljava/lang/Long;

    if-nez v2, :cond_8

    .line 423
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->h()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_6
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->signature:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 424
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->i()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_7
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->username:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 425
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->j()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_8
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->displayName:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 426
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->k()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_9
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->initials:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 427
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->l()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_a
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->profileImageUrl:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 428
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->m()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_b
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->body:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 429
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_c
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestampPlaybackOffset:Ljava/lang/Double;

    if-nez v2, :cond_f

    .line 430
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->o()Ljava/lang/Double;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_d
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->latitude:Ljava/lang/Double;

    if-nez v2, :cond_10

    .line 431
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->p()Ljava/lang/Double;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_e
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->longitude:Ljava/lang/Double;

    if-nez v2, :cond_11

    .line 432
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->q()Ljava/lang/Double;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_f
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->invitedCount:Ljava/lang/Long;

    if-nez v2, :cond_12

    .line 433
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->r()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_10
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedMessage:Ljava/lang/String;

    if-nez v2, :cond_13

    .line 434
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->s()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_11
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUserId:Ljava/lang/String;

    if-nez v2, :cond_14

    .line 435
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->t()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_12
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUsername:Ljava/lang/String;

    if-nez v2, :cond_15

    .line 436
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->u()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_13
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterNtp:Ljava/math/BigInteger;

    if-nez v2, :cond_16

    .line 437
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->v()Ljava/math/BigInteger;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_14
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->blockedMessageUUID:Ljava/lang/String;

    if-nez v2, :cond_17

    .line 438
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->w()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_15
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedMessage:Ljava/lang/String;

    if-nez v2, :cond_18

    .line 439
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->x()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_16
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUserId:Ljava/lang/String;

    if-nez v2, :cond_19

    .line 440
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->y()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_17
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUsername:Ljava/lang/String;

    if-nez v2, :cond_1a

    .line 441
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->z()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_18
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportType:Ltv/periscope/model/chat/MessageType$ReportType;

    if-nez v2, :cond_1b

    .line 442
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->A()Ltv/periscope/model/chat/MessageType$ReportType;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_19
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUUID:Ljava/lang/String;

    if-nez v2, :cond_1c

    .line 443
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1a
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBody:Ljava/lang/String;

    if-nez v2, :cond_1d

    .line 444
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->C()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1b
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUsername:Ljava/lang/String;

    if-nez v2, :cond_1e

    .line 445
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->D()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1c
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryVerdict:Ltv/periscope/model/chat/MessageType$VerdictType;

    if-nez v2, :cond_1f

    .line 446
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->E()Ltv/periscope/model/chat/MessageType$VerdictType;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1d
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBroadcastID:Ljava/lang/String;

    if-nez v2, :cond_20

    .line 447
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->F()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1e
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryDuration:Ljava/lang/Integer;

    if-nez v2, :cond_21

    .line 448
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->G()Ljava/lang/Integer;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1f
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceType:Ltv/periscope/model/chat/MessageType$SentenceType;

    if-nez v2, :cond_22

    .line 449
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->H()Ltv/periscope/model/chat/MessageType$SentenceType;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_20
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceDuration:Ljava/lang/Integer;

    if-nez v2, :cond_23

    .line 450
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->I()Ljava/lang/Integer;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_21
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->rawWireJson:Ljava/lang/String;

    if-nez v2, :cond_24

    .line 451
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->J()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_22
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->vipBadge:Ljava/lang/String;

    if-nez v2, :cond_25

    .line 452
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->K()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_23
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->superfan:Ljava/lang/Boolean;

    if-nez v2, :cond_26

    .line 453
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->L()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_24
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->newUser:Ljava/lang/Boolean;

    if-nez v2, :cond_27

    .line 454
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->M()Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto/16 :goto_0

    .line 418
    :cond_3
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->userId:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1

    .line 419
    :cond_4
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->twitterId:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_2

    .line 420
    :cond_5
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->participantIndex:Ljava/lang/Long;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_3

    .line 421
    :cond_6
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->ntp:Ljava/math/BigInteger;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->f()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_4

    .line 422
    :cond_7
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->uuid:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_5

    .line 423
    :cond_8
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestamp:Ljava/lang/Long;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->h()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_6

    .line 424
    :cond_9
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->signature:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_7

    .line 425
    :cond_a
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->username:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_8

    .line 426
    :cond_b
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->displayName:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_9

    .line 427
    :cond_c
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->initials:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_a

    .line 428
    :cond_d
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_b

    .line 429
    :cond_e
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->body:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_c

    .line 430
    :cond_f
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestampPlaybackOffset:Ljava/lang/Double;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->o()Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_d

    .line 431
    :cond_10
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->latitude:Ljava/lang/Double;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->p()Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_e

    .line 432
    :cond_11
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->longitude:Ljava/lang/Double;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->q()Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_f

    .line 433
    :cond_12
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->invitedCount:Ljava/lang/Long;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->r()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_10

    .line 434
    :cond_13
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedMessage:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_11

    .line 435
    :cond_14
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUserId:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_12

    .line 436
    :cond_15
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUsername:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_13

    .line 437
    :cond_16
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterNtp:Ljava/math/BigInteger;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->v()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_14

    .line 438
    :cond_17
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->blockedMessageUUID:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_15

    .line 439
    :cond_18
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedMessage:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->x()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_16

    .line 440
    :cond_19
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUserId:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->y()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_17

    .line 441
    :cond_1a
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUsername:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_18

    .line 442
    :cond_1b
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportType:Ltv/periscope/model/chat/MessageType$ReportType;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->A()Ltv/periscope/model/chat/MessageType$ReportType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/chat/MessageType$ReportType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_19

    .line 443
    :cond_1c
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUUID:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1a

    .line 444
    :cond_1d
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBody:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1b

    .line 445
    :cond_1e
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUsername:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->D()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1c

    .line 446
    :cond_1f
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryVerdict:Ltv/periscope/model/chat/MessageType$VerdictType;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->E()Ltv/periscope/model/chat/MessageType$VerdictType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/chat/MessageType$VerdictType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1d

    .line 447
    :cond_20
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBroadcastID:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->F()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1e

    .line 448
    :cond_21
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryDuration:Ljava/lang/Integer;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->G()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1f

    .line 449
    :cond_22
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceType:Ltv/periscope/model/chat/MessageType$SentenceType;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->H()Ltv/periscope/model/chat/MessageType$SentenceType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/chat/MessageType$SentenceType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_20

    .line 450
    :cond_23
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceDuration:Ljava/lang/Integer;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->I()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_21

    .line 451
    :cond_24
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->rawWireJson:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->J()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_22

    .line 452
    :cond_25
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->vipBadge:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->K()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_23

    .line 453
    :cond_26
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->superfan:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->L()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_24

    .line 454
    :cond_27
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->newUser:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->M()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_0

    :cond_28
    move v0, v1

    .line 456
    goto/16 :goto_0
.end method

.method public f()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->ntp:Ljava/math/BigInteger;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->uuid:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestamp:Ljava/lang/Long;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const v3, 0xf4243

    .line 461
    .line 463
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->version:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    xor-int/2addr v0, v3

    .line 464
    mul-int/2addr v0, v3

    .line 465
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->type:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {v2}, Ltv/periscope/model/chat/MessageType;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 466
    mul-int v2, v0, v3

    .line 467
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->userId:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v2

    .line 468
    mul-int v2, v0, v3

    .line 469
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->twitterId:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    xor-int/2addr v0, v2

    .line 470
    mul-int v2, v0, v3

    .line 471
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->participantIndex:Ljava/lang/Long;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    xor-int/2addr v0, v2

    .line 472
    mul-int v2, v0, v3

    .line 473
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->ntp:Ljava/math/BigInteger;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    xor-int/2addr v0, v2

    .line 474
    mul-int v2, v0, v3

    .line 475
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->uuid:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    xor-int/2addr v0, v2

    .line 476
    mul-int v2, v0, v3

    .line 477
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestamp:Ljava/lang/Long;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    xor-int/2addr v0, v2

    .line 478
    mul-int v2, v0, v3

    .line 479
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->signature:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    xor-int/2addr v0, v2

    .line 480
    mul-int v2, v0, v3

    .line 481
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->username:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    xor-int/2addr v0, v2

    .line 482
    mul-int v2, v0, v3

    .line 483
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->displayName:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    xor-int/2addr v0, v2

    .line 484
    mul-int v2, v0, v3

    .line 485
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->initials:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    xor-int/2addr v0, v2

    .line 486
    mul-int v2, v0, v3

    .line 487
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->profileImageUrl:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    xor-int/2addr v0, v2

    .line 488
    mul-int v2, v0, v3

    .line 489
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->body:Ljava/lang/String;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    xor-int/2addr v0, v2

    .line 490
    mul-int v2, v0, v3

    .line 491
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestampPlaybackOffset:Ljava/lang/Double;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    xor-int/2addr v0, v2

    .line 492
    mul-int v2, v0, v3

    .line 493
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->latitude:Ljava/lang/Double;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    xor-int/2addr v0, v2

    .line 494
    mul-int v2, v0, v3

    .line 495
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->longitude:Ljava/lang/Double;

    if-nez v0, :cond_e

    move v0, v1

    :goto_e
    xor-int/2addr v0, v2

    .line 496
    mul-int v2, v0, v3

    .line 497
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->invitedCount:Ljava/lang/Long;

    if-nez v0, :cond_f

    move v0, v1

    :goto_f
    xor-int/2addr v0, v2

    .line 498
    mul-int v2, v0, v3

    .line 499
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedMessage:Ljava/lang/String;

    if-nez v0, :cond_10

    move v0, v1

    :goto_10
    xor-int/2addr v0, v2

    .line 500
    mul-int v2, v0, v3

    .line 501
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUserId:Ljava/lang/String;

    if-nez v0, :cond_11

    move v0, v1

    :goto_11
    xor-int/2addr v0, v2

    .line 502
    mul-int v2, v0, v3

    .line 503
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUsername:Ljava/lang/String;

    if-nez v0, :cond_12

    move v0, v1

    :goto_12
    xor-int/2addr v0, v2

    .line 504
    mul-int v2, v0, v3

    .line 505
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterNtp:Ljava/math/BigInteger;

    if-nez v0, :cond_13

    move v0, v1

    :goto_13
    xor-int/2addr v0, v2

    .line 506
    mul-int v2, v0, v3

    .line 507
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->blockedMessageUUID:Ljava/lang/String;

    if-nez v0, :cond_14

    move v0, v1

    :goto_14
    xor-int/2addr v0, v2

    .line 508
    mul-int v2, v0, v3

    .line 509
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedMessage:Ljava/lang/String;

    if-nez v0, :cond_15

    move v0, v1

    :goto_15
    xor-int/2addr v0, v2

    .line 510
    mul-int v2, v0, v3

    .line 511
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUserId:Ljava/lang/String;

    if-nez v0, :cond_16

    move v0, v1

    :goto_16
    xor-int/2addr v0, v2

    .line 512
    mul-int v2, v0, v3

    .line 513
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUsername:Ljava/lang/String;

    if-nez v0, :cond_17

    move v0, v1

    :goto_17
    xor-int/2addr v0, v2

    .line 514
    mul-int v2, v0, v3

    .line 515
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportType:Ltv/periscope/model/chat/MessageType$ReportType;

    if-nez v0, :cond_18

    move v0, v1

    :goto_18
    xor-int/2addr v0, v2

    .line 516
    mul-int v2, v0, v3

    .line 517
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUUID:Ljava/lang/String;

    if-nez v0, :cond_19

    move v0, v1

    :goto_19
    xor-int/2addr v0, v2

    .line 518
    mul-int v2, v0, v3

    .line 519
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBody:Ljava/lang/String;

    if-nez v0, :cond_1a

    move v0, v1

    :goto_1a
    xor-int/2addr v0, v2

    .line 520
    mul-int v2, v0, v3

    .line 521
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUsername:Ljava/lang/String;

    if-nez v0, :cond_1b

    move v0, v1

    :goto_1b
    xor-int/2addr v0, v2

    .line 522
    mul-int v2, v0, v3

    .line 523
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryVerdict:Ltv/periscope/model/chat/MessageType$VerdictType;

    if-nez v0, :cond_1c

    move v0, v1

    :goto_1c
    xor-int/2addr v0, v2

    .line 524
    mul-int v2, v0, v3

    .line 525
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBroadcastID:Ljava/lang/String;

    if-nez v0, :cond_1d

    move v0, v1

    :goto_1d
    xor-int/2addr v0, v2

    .line 526
    mul-int v2, v0, v3

    .line 527
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryDuration:Ljava/lang/Integer;

    if-nez v0, :cond_1e

    move v0, v1

    :goto_1e
    xor-int/2addr v0, v2

    .line 528
    mul-int v2, v0, v3

    .line 529
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceType:Ltv/periscope/model/chat/MessageType$SentenceType;

    if-nez v0, :cond_1f

    move v0, v1

    :goto_1f
    xor-int/2addr v0, v2

    .line 530
    mul-int v2, v0, v3

    .line 531
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceDuration:Ljava/lang/Integer;

    if-nez v0, :cond_20

    move v0, v1

    :goto_20
    xor-int/2addr v0, v2

    .line 532
    mul-int v2, v0, v3

    .line 533
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->rawWireJson:Ljava/lang/String;

    if-nez v0, :cond_21

    move v0, v1

    :goto_21
    xor-int/2addr v0, v2

    .line 534
    mul-int v2, v0, v3

    .line 535
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->vipBadge:Ljava/lang/String;

    if-nez v0, :cond_22

    move v0, v1

    :goto_22
    xor-int/2addr v0, v2

    .line 536
    mul-int v2, v0, v3

    .line 537
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->superfan:Ljava/lang/Boolean;

    if-nez v0, :cond_23

    move v0, v1

    :goto_23
    xor-int/2addr v0, v2

    .line 538
    mul-int/2addr v0, v3

    .line 539
    iget-object v2, p0, Ltv/periscope/model/chat/AutoValue_Message;->newUser:Ljava/lang/Boolean;

    if-nez v2, :cond_24

    :goto_24
    xor-int/2addr v0, v1

    .line 540
    return v0

    .line 467
    :cond_0
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->userId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 469
    :cond_1
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->twitterId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 471
    :cond_2
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->participantIndex:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 473
    :cond_3
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->ntp:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 475
    :cond_4
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->uuid:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 477
    :cond_5
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 479
    :cond_6
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->signature:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_6

    .line 481
    :cond_7
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->username:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 483
    :cond_8
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->displayName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_8

    .line 485
    :cond_9
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->initials:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_9

    .line 487
    :cond_a
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_a

    .line 489
    :cond_b
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->body:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_b

    .line 491
    :cond_c
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestampPlaybackOffset:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto/16 :goto_c

    .line 493
    :cond_d
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->latitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto/16 :goto_d

    .line 495
    :cond_e
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->longitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto/16 :goto_e

    .line 497
    :cond_f
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->invitedCount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_f

    .line 499
    :cond_10
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedMessage:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_10

    .line 501
    :cond_11
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUserId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_11

    .line 503
    :cond_12
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUsername:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_12

    .line 505
    :cond_13
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterNtp:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->hashCode()I

    move-result v0

    goto/16 :goto_13

    .line 507
    :cond_14
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->blockedMessageUUID:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_14

    .line 509
    :cond_15
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedMessage:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_15

    .line 511
    :cond_16
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUserId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_16

    .line 513
    :cond_17
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUsername:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_17

    .line 515
    :cond_18
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportType:Ltv/periscope/model/chat/MessageType$ReportType;

    invoke-virtual {v0}, Ltv/periscope/model/chat/MessageType$ReportType;->hashCode()I

    move-result v0

    goto/16 :goto_18

    .line 517
    :cond_19
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUUID:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_19

    .line 519
    :cond_1a
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBody:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1a

    .line 521
    :cond_1b
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUsername:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1b

    .line 523
    :cond_1c
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryVerdict:Ltv/periscope/model/chat/MessageType$VerdictType;

    invoke-virtual {v0}, Ltv/periscope/model/chat/MessageType$VerdictType;->hashCode()I

    move-result v0

    goto/16 :goto_1c

    .line 525
    :cond_1d
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBroadcastID:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1d

    .line 527
    :cond_1e
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryDuration:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_1e

    .line 529
    :cond_1f
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceType:Ltv/periscope/model/chat/MessageType$SentenceType;

    invoke-virtual {v0}, Ltv/periscope/model/chat/MessageType$SentenceType;->hashCode()I

    move-result v0

    goto/16 :goto_1f

    .line 531
    :cond_20
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceDuration:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_20

    .line 533
    :cond_21
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->rawWireJson:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_21

    .line 535
    :cond_22
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->vipBadge:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_22

    .line 537
    :cond_23
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->superfan:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_23

    .line 539
    :cond_24
    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->newUser:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto/16 :goto_24
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->signature:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->username:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->initials:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->profileImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->body:Ljava/lang/String;

    return-object v0
.end method

.method public o()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestampPlaybackOffset:Ljava/lang/Double;

    return-object v0
.end method

.method public p()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->latitude:Ljava/lang/Double;

    return-object v0
.end method

.method public q()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->longitude:Ljava/lang/Double;

    return-object v0
.end method

.method public r()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->invitedCount:Ljava/lang/Long;

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedMessage:Ljava/lang/String;

    return-object v0
.end method

.method public t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUserId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 366
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Message{version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->type:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->userId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", twitterId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->twitterId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", participantIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->participantIndex:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", ntp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->ntp:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", uuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->uuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->signature:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", username="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->username:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", displayName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", initials="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->initials:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", profileImageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", body="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->body:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", timestampPlaybackOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->timestampPlaybackOffset:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", latitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->latitude:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", longitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->longitude:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", invitedCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->invitedCount:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", broadcasterBlockedMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", broadcasterBlockedUserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", broadcasterBlockedUsername="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", broadcasterNtp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterNtp:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", blockedMessageUUID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->blockedMessageUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", viewerBlockedMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", viewerBlockedUserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", viewerBlockedUsername="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", reportType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportType:Ltv/periscope/model/chat/MessageType$ReportType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", reportedMessageUUID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", reportedMessageBody="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBody:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", reportedMessageUsername="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", juryVerdict="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryVerdict:Ltv/periscope/model/chat/MessageType$VerdictType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", reportedMessageBroadcastID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->reportedMessageBroadcastID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", juryDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->juryDuration:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", sentenceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceType:Ltv/periscope/model/chat/MessageType$SentenceType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", sentenceDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->sentenceDuration:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", rawWireJson="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->rawWireJson:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", vipBadge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->vipBadge:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", superfan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->superfan:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", newUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/chat/AutoValue_Message;->newUser:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterBlockedUsername:Ljava/lang/String;

    return-object v0
.end method

.method public v()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->broadcasterNtp:Ljava/math/BigInteger;

    return-object v0
.end method

.method public w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->blockedMessageUUID:Ljava/lang/String;

    return-object v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedMessage:Ljava/lang/String;

    return-object v0
.end method

.method public y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUserId:Ljava/lang/String;

    return-object v0
.end method

.method public z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Ltv/periscope/model/chat/AutoValue_Message;->viewerBlockedUsername:Ljava/lang/String;

    return-object v0
.end method
