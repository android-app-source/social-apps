.class public abstract Ltv/periscope/model/chat/Message$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/model/chat/Message;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Boolean;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract a(Ljava/lang/Double;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract a(Ljava/lang/Integer;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract a(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract a(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract a(Ltv/periscope/model/chat/MessageType$ReportType;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract a(Ltv/periscope/model/chat/MessageType$SentenceType;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract a(Ltv/periscope/model/chat/MessageType$VerdictType;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract a()Ltv/periscope/model/chat/Message;
.end method

.method public abstract b(Ljava/lang/Boolean;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract b(Ljava/lang/Double;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract b(Ljava/lang/Integer;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract b(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract b(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract c(Ljava/lang/Double;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract c(Ljava/lang/Integer;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract c(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract d(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract e(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract f(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract g(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract h(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract i(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract j(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract k(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract l(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract m(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract n(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract o(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract p(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract q(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract r(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract s(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract t(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method

.method public abstract u(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
.end method
