.class final Ltv/periscope/model/chat/AutoValue_Message$a;
.super Ltv/periscope/model/chat/Message$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/model/chat/AutoValue_Message;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private A:Ltv/periscope/model/chat/MessageType$ReportType;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ltv/periscope/model/chat/MessageType$VerdictType;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/Integer;

.field private H:Ltv/periscope/model/chat/MessageType$SentenceType;

.field private I:Ljava/lang/Integer;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/Boolean;

.field private M:Ljava/lang/Boolean;

.field private a:Ljava/lang/Integer;

.field private b:Ltv/periscope/model/chat/MessageType;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Long;

.field private f:Ljava/math/BigInteger;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Long;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/Double;

.field private p:Ljava/lang/Double;

.field private q:Ljava/lang/Double;

.field private r:Ljava/lang/Long;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/math/BigInteger;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 583
    invoke-direct {p0}, Ltv/periscope/model/chat/Message$a;-><init>()V

    .line 584
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Boolean;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 813
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->L:Ljava/lang/Boolean;

    .line 814
    return-object p0
.end method

.method public a(Ljava/lang/Double;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 698
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->o:Ljava/lang/Double;

    .line 699
    return-object p0
.end method

.method public a(Ljava/lang/Integer;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 628
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->a:Ljava/lang/Integer;

    .line 629
    return-object p0
.end method

.method public a(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 648
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->e:Ljava/lang/Long;

    .line 649
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 638
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->c:Ljava/lang/String;

    .line 639
    return-object p0
.end method

.method public a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 653
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->f:Ljava/math/BigInteger;

    .line 654
    return-object p0
.end method

.method public a(Ltv/periscope/model/chat/MessageType$ReportType;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 758
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->A:Ltv/periscope/model/chat/MessageType$ReportType;

    .line 759
    return-object p0
.end method

.method public a(Ltv/periscope/model/chat/MessageType$SentenceType;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 793
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->H:Ltv/periscope/model/chat/MessageType$SentenceType;

    .line 794
    return-object p0
.end method

.method public a(Ltv/periscope/model/chat/MessageType$VerdictType;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 778
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->E:Ltv/periscope/model/chat/MessageType$VerdictType;

    .line 779
    return-object p0
.end method

.method public a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 633
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->b:Ltv/periscope/model/chat/MessageType;

    .line 634
    return-object p0
.end method

.method public a()Ltv/periscope/model/chat/Message;
    .locals 42

    .prologue
    .line 823
    const-string/jumbo v1, ""

    .line 824
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->a:Ljava/lang/Integer;

    if-nez v2, :cond_0

    .line 825
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " version"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 827
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->b:Ltv/periscope/model/chat/MessageType;

    if-nez v2, :cond_1

    .line 828
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 830
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 831
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Missing required properties:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 833
    :cond_2
    new-instance v1, Ltv/periscope/model/chat/AutoValue_Message;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->a:Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->b:Ltv/periscope/model/chat/MessageType;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->e:Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v7, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->f:Ljava/math/BigInteger;

    move-object/from16 v0, p0

    iget-object v8, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->h:Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->o:Ljava/lang/Double;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->p:Ljava/lang/Double;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->q:Ljava/lang/Double;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->r:Ljava/lang/Long;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->s:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->t:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->u:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->v:Ljava/math/BigInteger;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->w:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->x:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->y:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->z:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->A:Ltv/periscope/model/chat/MessageType$ReportType;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->B:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->C:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->D:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->E:Ltv/periscope/model/chat/MessageType$VerdictType;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->F:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->G:Ljava/lang/Integer;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->H:Ltv/periscope/model/chat/MessageType$SentenceType;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->I:Ljava/lang/Integer;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->J:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->K:Ljava/lang/String;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->L:Ljava/lang/Boolean;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/model/chat/AutoValue_Message$a;->M:Ljava/lang/Boolean;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    invoke-direct/range {v1 .. v41}, Ltv/periscope/model/chat/AutoValue_Message;-><init>(Ljava/lang/Integer;Ltv/periscope/model/chat/MessageType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/math/BigInteger;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigInteger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$ReportType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$VerdictType;Ljava/lang/String;Ljava/lang/Integer;Ltv/periscope/model/chat/MessageType$SentenceType;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ltv/periscope/model/chat/AutoValue_Message$1;)V

    return-object v1
.end method

.method public b(Ljava/lang/Boolean;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 818
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->M:Ljava/lang/Boolean;

    .line 819
    return-object p0
.end method

.method public b(Ljava/lang/Double;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 703
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->p:Ljava/lang/Double;

    .line 704
    return-object p0
.end method

.method public b(Ljava/lang/Integer;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 788
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->G:Ljava/lang/Integer;

    .line 789
    return-object p0
.end method

.method public b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 663
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->h:Ljava/lang/Long;

    .line 664
    return-object p0
.end method

.method public b(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 643
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->d:Ljava/lang/String;

    .line 644
    return-object p0
.end method

.method public b(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 733
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->v:Ljava/math/BigInteger;

    .line 734
    return-object p0
.end method

.method public c(Ljava/lang/Double;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 708
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->q:Ljava/lang/Double;

    .line 709
    return-object p0
.end method

.method public c(Ljava/lang/Integer;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 798
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->I:Ljava/lang/Integer;

    .line 799
    return-object p0
.end method

.method public c(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 713
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->r:Ljava/lang/Long;

    .line 714
    return-object p0
.end method

.method public c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 658
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->g:Ljava/lang/String;

    .line 659
    return-object p0
.end method

.method public d(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 673
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->j:Ljava/lang/String;

    .line 674
    return-object p0
.end method

.method public e(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 678
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->k:Ljava/lang/String;

    .line 679
    return-object p0
.end method

.method public f(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 683
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->l:Ljava/lang/String;

    .line 684
    return-object p0
.end method

.method public g(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 688
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->m:Ljava/lang/String;

    .line 689
    return-object p0
.end method

.method public h(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 693
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->n:Ljava/lang/String;

    .line 694
    return-object p0
.end method

.method public i(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 718
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->s:Ljava/lang/String;

    .line 719
    return-object p0
.end method

.method public j(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 723
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->t:Ljava/lang/String;

    .line 724
    return-object p0
.end method

.method public k(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 728
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->u:Ljava/lang/String;

    .line 729
    return-object p0
.end method

.method public l(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 738
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->w:Ljava/lang/String;

    .line 739
    return-object p0
.end method

.method public m(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 743
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->x:Ljava/lang/String;

    .line 744
    return-object p0
.end method

.method public n(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 748
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->y:Ljava/lang/String;

    .line 749
    return-object p0
.end method

.method public o(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 753
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->z:Ljava/lang/String;

    .line 754
    return-object p0
.end method

.method public p(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 763
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->B:Ljava/lang/String;

    .line 764
    return-object p0
.end method

.method public q(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 768
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->C:Ljava/lang/String;

    .line 769
    return-object p0
.end method

.method public r(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 773
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->D:Ljava/lang/String;

    .line 774
    return-object p0
.end method

.method public s(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 783
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->F:Ljava/lang/String;

    .line 784
    return-object p0
.end method

.method public t(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 803
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->J:Ljava/lang/String;

    .line 804
    return-object p0
.end method

.method public u(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;
    .locals 0

    .prologue
    .line 808
    iput-object p1, p0, Ltv/periscope/model/chat/AutoValue_Message$a;->K:Ljava/lang/String;

    .line 809
    return-object p0
.end method
