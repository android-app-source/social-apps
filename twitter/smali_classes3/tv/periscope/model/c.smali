.class final Ltv/periscope/model/c;
.super Ltv/periscope/model/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/model/c$a;
    }
.end annotation


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:Z

.field private final f:Ljava/lang/String;

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/t;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ltv/periscope/model/ChannelType;

.field private final i:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/util/List;Ltv/periscope/model/ChannelType;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/t;",
            ">;",
            "Ltv/periscope/model/ChannelType;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ltv/periscope/model/r;-><init>()V

    .line 28
    iput-object p1, p0, Ltv/periscope/model/c;->b:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Ltv/periscope/model/c;->c:Ljava/lang/String;

    .line 30
    iput-wide p3, p0, Ltv/periscope/model/c;->d:J

    .line 31
    iput-boolean p5, p0, Ltv/periscope/model/c;->e:Z

    .line 32
    iput-object p6, p0, Ltv/periscope/model/c;->f:Ljava/lang/String;

    .line 33
    iput-object p7, p0, Ltv/periscope/model/c;->g:Ljava/util/List;

    .line 34
    iput-object p8, p0, Ltv/periscope/model/c;->h:Ltv/periscope/model/ChannelType;

    .line 35
    iput-object p9, p0, Ltv/periscope/model/c;->i:Ljava/lang/String;

    .line 36
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/util/List;Ltv/periscope/model/ChannelType;Ljava/lang/String;Ltv/periscope/model/c$1;)V
    .locals 1

    .prologue
    .line 8
    invoke-direct/range {p0 .. p9}, Ltv/periscope/model/c;-><init>(Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/util/List;Ltv/periscope/model/ChannelType;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Ltv/periscope/model/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Ltv/periscope/model/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Ltv/periscope/model/c;->d:J

    return-wide v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Ltv/periscope/model/c;->e:Z

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Ltv/periscope/model/c;->f:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 94
    if-ne p1, p0, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v0

    .line 97
    :cond_1
    instance-of v2, p1, Ltv/periscope/model/r;

    if-eqz v2, :cond_3

    .line 98
    check-cast p1, Ltv/periscope/model/r;

    .line 99
    iget-object v2, p0, Ltv/periscope/model/c;->b:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/r;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/c;->c:Ljava/lang/String;

    .line 100
    invoke-virtual {p1}, Ltv/periscope/model/r;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/c;->d:J

    .line 101
    invoke-virtual {p1}, Ltv/periscope/model/r;->c()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-boolean v2, p0, Ltv/periscope/model/c;->e:Z

    .line 102
    invoke-virtual {p1}, Ltv/periscope/model/r;->d()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Ltv/periscope/model/c;->f:Ljava/lang/String;

    .line 103
    invoke-virtual {p1}, Ltv/periscope/model/r;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/c;->g:Ljava/util/List;

    .line 104
    invoke-virtual {p1}, Ltv/periscope/model/r;->f()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/c;->h:Ltv/periscope/model/ChannelType;

    .line 105
    invoke-virtual {p1}, Ltv/periscope/model/r;->g()Ltv/periscope/model/ChannelType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/ChannelType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/c;->i:Ljava/lang/String;

    .line 106
    invoke-virtual {p1}, Ltv/periscope/model/r;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 108
    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Ltv/periscope/model/c;->g:Ljava/util/List;

    return-object v0
.end method

.method public g()Ltv/periscope/model/ChannelType;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Ltv/periscope/model/c;->h:Ltv/periscope/model/ChannelType;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ltv/periscope/model/c;->i:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const v6, 0xf4243

    .line 113
    .line 115
    iget-object v0, p0, Ltv/periscope/model/c;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v6

    .line 116
    mul-int/2addr v0, v6

    .line 117
    iget-object v1, p0, Ltv/periscope/model/c;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 118
    mul-int/2addr v0, v6

    .line 119
    int-to-long v0, v0

    iget-wide v2, p0, Ltv/periscope/model/c;->d:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    iget-wide v4, p0, Ltv/periscope/model/c;->d:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 120
    mul-int v1, v0, v6

    .line 121
    iget-boolean v0, p0, Ltv/periscope/model/c;->e:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    xor-int/2addr v0, v1

    .line 122
    mul-int/2addr v0, v6

    .line 123
    iget-object v1, p0, Ltv/periscope/model/c;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 124
    mul-int/2addr v0, v6

    .line 125
    iget-object v1, p0, Ltv/periscope/model/c;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 126
    mul-int/2addr v0, v6

    .line 127
    iget-object v1, p0, Ltv/periscope/model/c;->h:Ltv/periscope/model/ChannelType;

    invoke-virtual {v1}, Ltv/periscope/model/ChannelType;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 128
    mul-int/2addr v0, v6

    .line 129
    iget-object v1, p0, Ltv/periscope/model/c;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 130
    return v0

    .line 121
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Channel{channelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", numberOfLiveStreams="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/c;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", featured="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/model/c;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", publicTag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/c;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", thumbnails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/c;->g:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", channelType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/c;->h:Ltv/periscope/model/ChannelType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", ownerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/c;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
