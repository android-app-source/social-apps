.class public final enum Ltv/periscope/model/ChannelAction$ActionType;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/model/ChannelAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/model/ChannelAction$ActionType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltv/periscope/model/ChannelAction$ActionType;

.field public static final enum b:Ltv/periscope/model/ChannelAction$ActionType;

.field public static final enum c:Ltv/periscope/model/ChannelAction$ActionType;

.field public static final enum d:Ltv/periscope/model/ChannelAction$ActionType;

.field private static final synthetic e:[Ltv/periscope/model/ChannelAction$ActionType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Ltv/periscope/model/ChannelAction$ActionType;

    const-string/jumbo v1, "ChannelCreate"

    invoke-direct {v0, v1, v2}, Ltv/periscope/model/ChannelAction$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/model/ChannelAction$ActionType;->a:Ltv/periscope/model/ChannelAction$ActionType;

    .line 22
    new-instance v0, Ltv/periscope/model/ChannelAction$ActionType;

    const-string/jumbo v1, "ChannelRename"

    invoke-direct {v0, v1, v3}, Ltv/periscope/model/ChannelAction$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/model/ChannelAction$ActionType;->b:Ltv/periscope/model/ChannelAction$ActionType;

    .line 23
    new-instance v0, Ltv/periscope/model/ChannelAction$ActionType;

    const-string/jumbo v1, "MemberAdd"

    invoke-direct {v0, v1, v4}, Ltv/periscope/model/ChannelAction$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/model/ChannelAction$ActionType;->c:Ltv/periscope/model/ChannelAction$ActionType;

    .line 24
    new-instance v0, Ltv/periscope/model/ChannelAction$ActionType;

    const-string/jumbo v1, "MemberRemove"

    invoke-direct {v0, v1, v5}, Ltv/periscope/model/ChannelAction$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/model/ChannelAction$ActionType;->d:Ltv/periscope/model/ChannelAction$ActionType;

    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [Ltv/periscope/model/ChannelAction$ActionType;

    sget-object v1, Ltv/periscope/model/ChannelAction$ActionType;->a:Ltv/periscope/model/ChannelAction$ActionType;

    aput-object v1, v0, v2

    sget-object v1, Ltv/periscope/model/ChannelAction$ActionType;->b:Ltv/periscope/model/ChannelAction$ActionType;

    aput-object v1, v0, v3

    sget-object v1, Ltv/periscope/model/ChannelAction$ActionType;->c:Ltv/periscope/model/ChannelAction$ActionType;

    aput-object v1, v0, v4

    sget-object v1, Ltv/periscope/model/ChannelAction$ActionType;->d:Ltv/periscope/model/ChannelAction$ActionType;

    aput-object v1, v0, v5

    sput-object v0, Ltv/periscope/model/ChannelAction$ActionType;->e:[Ltv/periscope/model/ChannelAction$ActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$ActionType;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 33
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unsupported action type string: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :sswitch_0
    const-string/jumbo v1, "c"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v1, "u"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v1, "a"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string/jumbo v1, "r"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 35
    :pswitch_0
    sget-object v0, Ltv/periscope/model/ChannelAction$ActionType;->a:Ltv/periscope/model/ChannelAction$ActionType;

    .line 44
    :goto_1
    return-object v0

    .line 38
    :pswitch_1
    sget-object v0, Ltv/periscope/model/ChannelAction$ActionType;->b:Ltv/periscope/model/ChannelAction$ActionType;

    goto :goto_1

    .line 41
    :pswitch_2
    sget-object v0, Ltv/periscope/model/ChannelAction$ActionType;->c:Ltv/periscope/model/ChannelAction$ActionType;

    goto :goto_1

    .line 44
    :pswitch_3
    sget-object v0, Ltv/periscope/model/ChannelAction$ActionType;->d:Ltv/periscope/model/ChannelAction$ActionType;

    goto :goto_1

    .line 33
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_2
        0x63 -> :sswitch_0
        0x72 -> :sswitch_3
        0x75 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$ActionType;
    .locals 1

    .prologue
    .line 20
    const-class v0, Ltv/periscope/model/ChannelAction$ActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/ChannelAction$ActionType;

    return-object v0
.end method

.method public static values()[Ltv/periscope/model/ChannelAction$ActionType;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Ltv/periscope/model/ChannelAction$ActionType;->e:[Ltv/periscope/model/ChannelAction$ActionType;

    invoke-virtual {v0}, [Ltv/periscope/model/ChannelAction$ActionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/model/ChannelAction$ActionType;

    return-object v0
.end method
