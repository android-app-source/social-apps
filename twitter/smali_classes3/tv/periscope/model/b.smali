.class final Ltv/periscope/model/b;
.super Ltv/periscope/model/q;
.source "Twttr"


# instance fields
.field private final a:J

.field private final b:J

.field private final c:J

.field private final d:J

.field private final e:J

.field private final f:J

.field private final g:J

.field private final h:J


# direct methods
.method constructor <init>(JJJJJJJJ)V
    .locals 3

    .prologue
    .line 26
    invoke-direct {p0}, Ltv/periscope/model/q;-><init>()V

    .line 27
    iput-wide p1, p0, Ltv/periscope/model/b;->a:J

    .line 28
    iput-wide p3, p0, Ltv/periscope/model/b;->b:J

    .line 29
    iput-wide p5, p0, Ltv/periscope/model/b;->c:J

    .line 30
    iput-wide p7, p0, Ltv/periscope/model/b;->d:J

    .line 31
    iput-wide p9, p0, Ltv/periscope/model/b;->e:J

    .line 32
    iput-wide p11, p0, Ltv/periscope/model/b;->f:J

    .line 33
    move-wide/from16 v0, p13

    iput-wide v0, p0, Ltv/periscope/model/b;->g:J

    .line 34
    move-wide/from16 v0, p15

    iput-wide v0, p0, Ltv/periscope/model/b;->h:J

    .line 35
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Ltv/periscope/model/b;->a:J

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Ltv/periscope/model/b;->b:J

    return-wide v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Ltv/periscope/model/b;->c:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Ltv/periscope/model/b;->d:J

    return-wide v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Ltv/periscope/model/b;->e:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 93
    if-ne p1, p0, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 96
    :cond_1
    instance-of v2, p1, Ltv/periscope/model/q;

    if-eqz v2, :cond_3

    .line 97
    check-cast p1, Ltv/periscope/model/q;

    .line 98
    iget-wide v2, p0, Ltv/periscope/model/b;->a:J

    invoke-virtual {p1}, Ltv/periscope/model/q;->a()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/b;->b:J

    .line 99
    invoke-virtual {p1}, Ltv/periscope/model/q;->b()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/b;->c:J

    .line 100
    invoke-virtual {p1}, Ltv/periscope/model/q;->c()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/b;->d:J

    .line 101
    invoke-virtual {p1}, Ltv/periscope/model/q;->d()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/b;->e:J

    .line 102
    invoke-virtual {p1}, Ltv/periscope/model/q;->e()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/b;->f:J

    .line 103
    invoke-virtual {p1}, Ltv/periscope/model/q;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/b;->g:J

    .line 104
    invoke-virtual {p1}, Ltv/periscope/model/q;->g()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ltv/periscope/model/b;->h:J

    .line 105
    invoke-virtual {p1}, Ltv/periscope/model/q;->h()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 107
    goto :goto_0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Ltv/periscope/model/b;->f:J

    return-wide v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Ltv/periscope/model/b;->g:J

    return-wide v0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Ltv/periscope/model/b;->h:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const v7, 0xf4243

    const/16 v6, 0x20

    .line 112
    .line 114
    int-to-long v0, v7

    iget-wide v2, p0, Ltv/periscope/model/b;->a:J

    ushr-long/2addr v2, v6

    iget-wide v4, p0, Ltv/periscope/model/b;->a:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 115
    mul-int/2addr v0, v7

    .line 116
    int-to-long v0, v0

    iget-wide v2, p0, Ltv/periscope/model/b;->b:J

    ushr-long/2addr v2, v6

    iget-wide v4, p0, Ltv/periscope/model/b;->b:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 117
    mul-int/2addr v0, v7

    .line 118
    int-to-long v0, v0

    iget-wide v2, p0, Ltv/periscope/model/b;->c:J

    ushr-long/2addr v2, v6

    iget-wide v4, p0, Ltv/periscope/model/b;->c:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 119
    mul-int/2addr v0, v7

    .line 120
    int-to-long v0, v0

    iget-wide v2, p0, Ltv/periscope/model/b;->d:J

    ushr-long/2addr v2, v6

    iget-wide v4, p0, Ltv/periscope/model/b;->d:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 121
    mul-int/2addr v0, v7

    .line 122
    int-to-long v0, v0

    iget-wide v2, p0, Ltv/periscope/model/b;->e:J

    ushr-long/2addr v2, v6

    iget-wide v4, p0, Ltv/periscope/model/b;->e:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 123
    mul-int/2addr v0, v7

    .line 124
    int-to-long v0, v0

    iget-wide v2, p0, Ltv/periscope/model/b;->f:J

    ushr-long/2addr v2, v6

    iget-wide v4, p0, Ltv/periscope/model/b;->f:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 125
    mul-int/2addr v0, v7

    .line 126
    int-to-long v0, v0

    iget-wide v2, p0, Ltv/periscope/model/b;->g:J

    ushr-long/2addr v2, v6

    iget-wide v4, p0, Ltv/periscope/model/b;->g:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 127
    mul-int/2addr v0, v7

    .line 128
    int-to-long v0, v0

    iget-wide v2, p0, Ltv/periscope/model/b;->h:J

    ushr-long/2addr v2, v6

    iget-wide v4, p0, Ltv/periscope/model/b;->h:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 129
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "BroadcastViewerMeta{numReplayWatched="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/b;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", numLiveWatched="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/b;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", liveWatchedTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/b;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", liveWatchedTimePerUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/b;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", replayWatchedTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/b;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", replayWatchedTimePerUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/b;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", totalWatchedTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/b;->g:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", totalWatchedTimePerUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/model/b;->h:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
