.class public final Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;
.super Lcom/google/gson/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/r",
        "<",
        "Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final firstDegreeConnectionDisplayNameAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mutualFollowersAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mutualFollowersCountAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final recommendationCategoryAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel$RecommendationCategory;",
            ">;"
        }
    .end annotation
.end field

.field private final userAdapter:Lcom/google/gson/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/r",
            "<",
            "Ltv/periscope/model/user/UserJSONModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/e;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/gson/r;-><init>()V

    .line 28
    const-class v0, Ltv/periscope/model/user/UserJSONModel;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->userAdapter:Lcom/google/gson/r;

    .line 29
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->firstDegreeConnectionDisplayNameAdapter:Lcom/google/gson/r;

    .line 30
    const-class v0, Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel$RecommendationCategory;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->recommendationCategoryAdapter:Lcom/google/gson/r;

    .line 31
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Ljava/lang/Class;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->mutualFollowersCountAdapter:Lcom/google/gson/r;

    .line 32
    new-instance v0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter$1;

    invoke-direct {v0, p0}, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter$1;-><init>(Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;)V

    invoke-virtual {p1, v0}, Lcom/google/gson/e;->a(Lkr;)Lcom/google/gson/r;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->mutualFollowersAdapter:Lcom/google/gson/r;

    .line 33
    return-void
.end method


# virtual methods
.method public bridge synthetic read(Lcom/google/gson/stream/a;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->read(Lcom/google/gson/stream/a;)Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;

    move-result-object v0

    return-object v0
.end method

.method public read(Lcom/google/gson/stream/a;)Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 57
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->c()V

    move-object v4, v5

    move-object v3, v5

    move-object v2, v5

    move-object v1, v5

    .line 63
    :goto_0
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 64
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->g()Ljava/lang/String;

    move-result-object v6

    .line 65
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->f()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v7, Lcom/google/gson/stream/JsonToken;->i:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v7, :cond_0

    .line 66
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->n()V

    goto :goto_0

    .line 69
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 91
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->n()V

    goto :goto_0

    .line 69
    :sswitch_0
    const-string/jumbo v7, "user"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v7, "first_degree_connection"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v7, "recommendation_category"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v7, "mutual_followers_count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string/jumbo v7, "mutual_followers"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v0, 0x4

    goto :goto_1

    .line 71
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->userAdapter:Lcom/google/gson/r;

    invoke-virtual {v0, p1}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/user/UserJSONModel;

    move-object v1, v0

    .line 72
    goto :goto_0

    .line 75
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->firstDegreeConnectionDisplayNameAdapter:Lcom/google/gson/r;

    invoke-virtual {v0, p1}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 76
    goto :goto_0

    .line 79
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->recommendationCategoryAdapter:Lcom/google/gson/r;

    invoke-virtual {v0, p1}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel$RecommendationCategory;

    move-object v3, v0

    .line 80
    goto :goto_0

    .line 83
    :pswitch_3
    iget-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->mutualFollowersCountAdapter:Lcom/google/gson/r;

    invoke-virtual {v0, p1}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    move-object v4, v0

    .line 84
    goto/16 :goto_0

    .line 87
    :pswitch_4
    iget-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->mutualFollowersAdapter:Lcom/google/gson/r;

    invoke-virtual {v0, p1}, Lcom/google/gson/r;->read(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object v5, v0

    .line 88
    goto/16 :goto_0

    .line 95
    :cond_2
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->d()V

    .line 96
    new-instance v0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel;-><init>(Ltv/periscope/model/user/UserJSONModel;Ljava/lang/String;Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel$RecommendationCategory;Ljava/lang/Long;Ljava/util/List;)V

    return-object v0

    .line 69
    nop

    :sswitch_data_0
    .sparse-switch
        -0x185eb13c -> :sswitch_2
        -0x3de219e -> :sswitch_1
        0x36ebcb -> :sswitch_0
        0x6774daa -> :sswitch_4
        0x19b09a3a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    check-cast p2, Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;

    invoke-virtual {p0, p1, p2}, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->write(Lcom/google/gson/stream/b;Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;)V

    return-void
.end method

.method public write(Lcom/google/gson/stream/b;Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p1}, Lcom/google/gson/stream/b;->d()Lcom/google/gson/stream/b;

    .line 37
    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 38
    iget-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->userAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;->user()Ltv/periscope/model/user/UserJSONModel;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 39
    invoke-virtual {p2}, Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;->firstDegreeConnectionDisplayName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 40
    const-string/jumbo v0, "first_degree_connection"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 41
    iget-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->firstDegreeConnectionDisplayNameAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;->firstDegreeConnectionDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 43
    :cond_0
    const-string/jumbo v0, "recommendation_category"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 44
    iget-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->recommendationCategoryAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;->recommendationCategory()Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel$RecommendationCategory;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 45
    invoke-virtual {p2}, Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;->mutualFollowersCount()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 46
    const-string/jumbo v0, "mutual_followers_count"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 47
    iget-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->mutualFollowersCountAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;->mutualFollowersCount()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 49
    :cond_1
    invoke-virtual {p2}, Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;->mutualFollowers()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 50
    const-string/jumbo v0, "mutual_followers"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->a(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 51
    iget-object v0, p0, Ltv/periscope/model/peopleyoumaylike/AutoValue_PeopleYouMayLikeJSONModel$GsonTypeAdapter;->mutualFollowersAdapter:Lcom/google/gson/r;

    invoke-virtual {p2}, Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;->mutualFollowers()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/r;->write(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 53
    :cond_2
    invoke-virtual {p1}, Lcom/google/gson/stream/b;->e()Lcom/google/gson/stream/b;

    .line 54
    return-void
.end method
