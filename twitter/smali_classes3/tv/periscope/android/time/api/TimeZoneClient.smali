.class public Ltv/periscope/android/time/api/TimeZoneClient;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/time/api/TimeZoneClient$a;,
        Ltv/periscope/android/time/api/TimeZoneClient$TimeZoneService;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private b:Ltv/periscope/android/time/api/TimeZoneClient$TimeZoneService;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Ltv/periscope/android/time/api/TimeZoneClient;->a:Ljava/util/concurrent/Executor;

    .line 25
    return-void
.end method

.method private a()Ltv/periscope/android/time/api/TimeZoneClient$TimeZoneService;
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Ltv/periscope/android/time/api/TimeZoneClient;->b:Ltv/periscope/android/time/api/TimeZoneClient$TimeZoneService;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lretrofit/RestAdapter$Builder;

    invoke-direct {v0}, Lretrofit/RestAdapter$Builder;-><init>()V

    iget-object v1, p0, Ltv/periscope/android/time/api/TimeZoneClient;->a:Ljava/util/concurrent/Executor;

    new-instance v2, Lretrofit/android/MainThreadExecutor;

    invoke-direct {v2}, Lretrofit/android/MainThreadExecutor;-><init>()V

    .line 37
    invoke-virtual {v0, v1, v2}, Lretrofit/RestAdapter$Builder;->setExecutors(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    const-string/jumbo v1, "https://maps.googleapis.com/maps/api/"

    .line 38
    invoke-virtual {v0, v1}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v0

    .line 40
    const-class v1, Ltv/periscope/android/time/api/TimeZoneClient$TimeZoneService;

    invoke-virtual {v0, v1}, Lretrofit/RestAdapter;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/time/api/TimeZoneClient$TimeZoneService;

    iput-object v0, p0, Ltv/periscope/android/time/api/TimeZoneClient;->b:Ltv/periscope/android/time/api/TimeZoneClient$TimeZoneService;

    .line 42
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/time/api/TimeZoneClient;->b:Ltv/periscope/android/time/api/TimeZoneClient$TimeZoneService;

    return-object v0
.end method


# virtual methods
.method public a(DDLretrofit/Callback;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DD",
            "Lretrofit/Callback",
            "<",
            "Ltv/periscope/android/time/api/TimeZoneClient$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 29
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 30
    invoke-direct {p0}, Ltv/periscope/android/time/api/TimeZoneClient;->a()Ltv/periscope/android/time/api/TimeZoneClient$TimeZoneService;

    move-result-object v2

    invoke-interface {v2, v0, v1, p5}, Ltv/periscope/android/time/api/TimeZoneClient$TimeZoneService;->getTimeZoneForLocation(Ljava/lang/String;Ljava/lang/String;Lretrofit/Callback;)V

    .line 31
    return-void
.end method
