.class public abstract Ltv/periscope/android/api/DefaultAuthedEventHandler;
.super Ltv/periscope/android/api/DefaultEventHandler;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DefaultAuthedEventHandler"


# instance fields
.field private final mApiManager:Ltv/periscope/android/api/AuthedApiManager;

.field private final mBackendServiceManager:Lcym;

.field private final mPendingFollowActions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ltv/periscope/android/api/FollowAction;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcyn;Lcyw;Lde/greenrobot/event/c;Lcym;Ltv/periscope/android/api/AuthedApiManager;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Ltv/periscope/android/api/DefaultEventHandler;-><init>(Landroid/content/Context;Lcyn;Lcyw;Lde/greenrobot/event/c;)V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mPendingFollowActions:Ljava/util/HashMap;

    .line 48
    iput-object p5, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mBackendServiceManager:Lcym;

    .line 49
    iput-object p6, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mApiManager:Ltv/periscope/android/api/AuthedApiManager;

    .line 50
    return-void
.end method

.method private executeNextFollowAction(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ltv/periscope/android/api/FollowAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 226
    const-string/jumbo v0, "DefaultAuthedEventHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Executing next action in list for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/FollowAction;

    .line 231
    sget-object v1, Ltv/periscope/android/api/DefaultAuthedEventHandler$1;->$SwitchMap$tv$periscope$android$api$FollowAction$FollowActionType:[I

    iget-object v2, v0, Ltv/periscope/android/api/FollowAction;->type:Ltv/periscope/android/api/FollowAction$FollowActionType;

    invoke-virtual {v2}, Ltv/periscope/android/api/FollowAction$FollowActionType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 233
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mApiManager:Ltv/periscope/android/api/AuthedApiManager;

    invoke-virtual {v0, p1}, Ltv/periscope/android/api/AuthedApiManager;->executeFollow(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 237
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mApiManager:Ltv/periscope/android/api/AuthedApiManager;

    invoke-virtual {v0, p1}, Ltv/periscope/android/api/AuthedApiManager;->executeUnfollow(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 241
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mApiManager:Ltv/periscope/android/api/AuthedApiManager;

    invoke-virtual {v0, p1}, Ltv/periscope/android/api/AuthedApiManager;->executeMute(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 245
    :pswitch_3
    iget-object v0, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mApiManager:Ltv/periscope/android/api/AuthedApiManager;

    invoke-virtual {v0, p1}, Ltv/periscope/android/api/AuthedApiManager;->executeUnmute(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 249
    :pswitch_4
    iget-object v1, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mApiManager:Ltv/periscope/android/api/AuthedApiManager;

    iget-object v0, v0, Ltv/periscope/android/api/FollowAction;->request:Ltv/periscope/android/api/PsRequest;

    check-cast v0, Ltv/periscope/android/api/BlockRequest;

    invoke-virtual {v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->executeBlock(Ltv/periscope/android/api/BlockRequest;)Ljava/lang/String;

    goto :goto_0

    .line 253
    :pswitch_5
    iget-object v0, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mApiManager:Ltv/periscope/android/api/AuthedApiManager;

    invoke-virtual {v0, p1}, Ltv/periscope/android/api/AuthedApiManager;->executeUnblock(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method addFollowAction(Ljava/lang/String;Ltv/periscope/android/api/FollowAction$FollowActionType;)Z
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->addFollowAction(Ljava/lang/String;Ltv/periscope/android/api/FollowAction$FollowActionType;Ltv/periscope/android/api/PsRequest;)Z

    move-result v0

    return v0
.end method

.method addFollowAction(Ljava/lang/String;Ltv/periscope/android/api/FollowAction$FollowActionType;Ltv/periscope/android/api/PsRequest;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 273
    iget-object v0, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mPendingFollowActions:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 274
    if-nez v0, :cond_2

    .line 275
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 276
    iget-object v1, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mPendingFollowActions:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 278
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    new-instance v0, Ltv/periscope/android/api/FollowAction;

    invoke-direct {v0, p2, p3}, Ltv/periscope/android/api/FollowAction;-><init>(Ltv/periscope/android/api/FollowAction$FollowActionType;Ltv/periscope/android/api/PsRequest;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    const-string/jumbo v0, "DefaultAuthedEventHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Queueing action on empty list: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const/4 v0, 0x1

    .line 289
    :goto_1
    return v0

    .line 282
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/FollowAction;

    iget-object v0, v0, Ltv/periscope/android/api/FollowAction;->type:Ltv/periscope/android/api/FollowAction$FollowActionType;

    if-eq v0, p2, :cond_1

    .line 283
    const-string/jumbo v0, "DefaultAuthedEventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Queueing action on list: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " of size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    new-instance v0, Ltv/periscope/android/api/FollowAction;

    invoke-direct {v0, p2, p3}, Ltv/periscope/android/api/FollowAction;-><init>(Ltv/periscope/android/api/FollowAction$FollowActionType;Ltv/periscope/android/api/PsRequest;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v2

    .line 285
    goto :goto_1

    .line 288
    :cond_1
    const-string/jumbo v0, "DefaultAuthedEventHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Not queueing action "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 289
    goto :goto_1

    :cond_2
    move-object v1, v0

    goto/16 :goto_0
.end method

.method protected handleEvent(Ltv/periscope/android/event/ApiEvent;)V
    .locals 21

    .prologue
    .line 91
    invoke-super/range {p0 .. p1}, Ltv/periscope/android/api/DefaultEventHandler;->handleEvent(Ltv/periscope/android/event/ApiEvent;)V

    .line 92
    sget-object v2, Ltv/periscope/android/api/DefaultAuthedEventHandler$1;->$SwitchMap$tv$periscope$android$event$ApiEvent$Type:[I

    move-object/from16 v0, p1

    iget-object v3, v0, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v3}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 94
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 96
    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    .line 97
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mUserCache:Lcyw;

    invoke-interface {v3, v2}, Lcyw;->a(Ljava/util/List;)V

    goto :goto_0

    .line 102
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 104
    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    .line 105
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mUserCache:Lcyw;

    invoke-interface {v3, v2}, Lcyw;->c(Ljava/util/List;)V

    goto :goto_0

    .line 111
    :pswitch_2
    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->c:Ltv/periscope/android/api/ApiRequest;

    check-cast v2, Ltv/periscope/android/api/BlockRequest;

    .line 112
    if-eqz v2, :cond_0

    .line 113
    iget-object v2, v2, Ltv/periscope/android/api/BlockRequest;->userId:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->handleFollowActionComplete(Ltv/periscope/android/event/ApiEvent;Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :pswitch_3
    invoke-virtual/range {p1 .. p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 120
    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    .line 121
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mUserCache:Lcyw;

    invoke-interface {v3, v2}, Lcyw;->b(Ljava/util/List;)V

    goto :goto_0

    .line 126
    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 127
    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v2, Ltv/periscope/model/user/f;

    .line 128
    invoke-virtual {v2}, Ltv/periscope/model/user/f;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mUserCache:Lcyw;

    invoke-interface {v4}, Lcyw;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 129
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mUserCache:Lcyw;

    invoke-interface {v3, v2}, Lcyw;->a(Ltv/periscope/model/user/f;)V

    goto/16 :goto_0

    .line 135
    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v2, Ltv/periscope/android/api/GetUserResponse;

    .line 137
    iget-object v3, v2, Ltv/periscope/android/api/GetUserResponse;->user:Ltv/periscope/android/api/PsUser;

    iget-object v3, v3, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mUserCache:Lcyw;

    invoke-interface {v4}, Lcyw;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 138
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mUserCache:Lcyw;

    iget-object v4, v2, Ltv/periscope/android/api/GetUserResponse;->user:Ltv/periscope/android/api/PsUser;

    invoke-interface {v3, v4}, Lcyw;->a(Ltv/periscope/android/api/PsUser;)V

    .line 140
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mUserCache:Lcyw;

    iget-object v2, v2, Ltv/periscope/android/api/GetUserResponse;->user:Ltv/periscope/android/api/PsUser;

    invoke-interface {v3, v2}, Lcyw;->b(Ltv/periscope/android/api/PsUser;)V

    goto/16 :goto_0

    .line 145
    :pswitch_6
    invoke-virtual/range {p1 .. p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146
    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v2, Ltv/periscope/model/w;

    .line 147
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 148
    invoke-virtual {v2}, Ltv/periscope/model/w;->c()Ltv/periscope/model/p;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mBroadcastCache:Lcyn;

    invoke-interface {v2, v3}, Lcyn;->a(Ljava/util/List;)V

    goto/16 :goto_0

    .line 154
    :pswitch_7
    invoke-virtual/range {p1 .. p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 155
    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    move-object/from16 v18, v2

    check-cast v18, Ltv/periscope/android/api/GetBroadcastViewersResponse;

    .line 159
    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mBroadcastCache:Lcyn;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->broadcastId:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    iget-wide v2, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->numReplayWatched:J

    move-object/from16 v0, v18

    iget-wide v4, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->numLiveWatched:J

    move-object/from16 v0, v18

    iget-wide v6, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->liveWatchedTime:J

    move-object/from16 v0, v18

    iget-wide v8, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->liveWatchedTimePerUser:J

    move-object/from16 v0, v18

    iget-wide v10, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->replayWatchedTime:J

    move-object/from16 v0, v18

    iget-wide v12, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->replayWatchedTimePerUser:J

    move-object/from16 v0, v18

    iget-wide v14, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->totalWatchedTime:J

    move-object/from16 v0, v18

    iget-wide v0, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->totalWatchedTimePerUser:J

    move-wide/from16 v16, v0

    .line 160
    invoke-static/range {v2 .. v17}, Ltv/periscope/model/q;->a(JJJJJJJJ)Ltv/periscope/model/q;

    move-result-object v2

    .line 159
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v2}, Lcyn;->a(Ljava/lang/String;Ltv/periscope/model/q;)V

    .line 163
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mUserCache:Lcyw;

    move-object/from16 v0, v18

    iget-object v3, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->broadcasterId:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v4, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->broadcastId:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v5, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->live:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    iget-object v6, v0, Ltv/periscope/android/api/GetBroadcastViewersResponse;->replay:Ljava/util/ArrayList;

    invoke-interface {v2, v3, v4, v5, v6}, Lcyw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    goto/16 :goto_0

    .line 169
    :pswitch_8
    invoke-virtual/range {p1 .. p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    move-object/from16 v0, p1

    iget-object v2, v0, Ltv/periscope/android/event/ApiEvent;->c:Ltv/periscope/android/api/ApiRequest;

    check-cast v2, Ltv/periscope/android/api/AuthorizeTokenRequest;

    .line 171
    move-object/from16 v0, p1

    iget-object v3, v0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v3, Ltv/periscope/android/api/AuthorizeTokenResponse;

    .line 172
    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mBackendServiceManager:Lcym;

    iget-object v2, v2, Ltv/periscope/android/api/AuthorizeTokenRequest;->service:Ljava/lang/String;

    iget-object v3, v3, Ltv/periscope/android/api/AuthorizeTokenResponse;->authorizationToken:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Lcym;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method handleFollowActionComplete(Ltv/periscope/android/event/ApiEvent;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 194
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->d()Ltv/periscope/android/api/ErrorResponse;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_0

    .line 197
    iget-object v0, v0, Ltv/periscope/android/api/ErrorResponse;->error:Ltv/periscope/android/api/ErrorResponseItem;

    iget v0, v0, Ltv/periscope/android/api/ErrorResponseItem;->code:I

    sparse-switch v0, :sswitch_data_0

    .line 208
    :cond_0
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mPendingFollowActions:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 209
    if-nez v0, :cond_1

    .line 210
    const-string/jumbo v0, "DefaultAuthedEventHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ActionList was null for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :goto_1
    return-void

    .line 199
    :sswitch_0
    iget-object v0, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mAppContext:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__block_limit_error:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 202
    :sswitch_1
    iget-object v0, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mAppContext:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__follow_limit_error:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 213
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 214
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/api/FollowAction;

    .line 215
    const-string/jumbo v2, "DefaultAuthedEventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Removing completed follow action "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Ltv/periscope/android/api/FollowAction;->type:Ltv/periscope/android/api/FollowAction$FollowActionType;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " for user "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 218
    const-string/jumbo v0, "DefaultAuthedEventHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Follow actions for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " are empty, removing the queue."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mPendingFollowActions:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 221
    :cond_3
    invoke-direct {p0, p2, v0}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->executeNextFollowAction(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_1

    .line 197
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0xa1 -> :sswitch_1
    .end sparse-switch
.end method

.method protected handleUnauthorizedApiResponse(Ltv/periscope/android/event/ApiEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 66
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->d()Ltv/periscope/android/api/ErrorResponse;

    move-result-object v0

    .line 67
    if-eqz v0, :cond_1

    iget-object v1, v0, Ltv/periscope/android/api/ErrorResponse;->error:Ltv/periscope/android/api/ErrorResponseItem;

    iget v1, v1, Ltv/periscope/android/api/ErrorResponseItem;->code:I

    const/16 v2, 0x40

    if-ne v1, v2, :cond_1

    .line 68
    const-string/jumbo v1, "DefaultAuthedEventHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Banned user detected for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", logging out."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v1, v0, Ltv/periscope/android/api/ErrorResponse;->error:Ltv/periscope/android/api/ErrorResponseItem;

    iget-object v1, v1, Ltv/periscope/android/api/ErrorResponseItem;->rectifyUrl:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mEventBus:Lde/greenrobot/event/c;

    new-instance v2, Ltv/periscope/android/event/AppEvent;

    sget-object v3, Ltv/periscope/android/event/AppEvent$Type;->e:Ltv/periscope/android/event/AppEvent$Type;

    iget-object v0, v0, Ltv/periscope/android/api/ErrorResponse;->error:Ltv/periscope/android/api/ErrorResponseItem;

    iget-object v0, v0, Ltv/periscope/android/api/ErrorResponseItem;->rectifyUrl:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Ltv/periscope/android/event/AppEvent;-><init>(Ltv/periscope/android/event/AppEvent$Type;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 87
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v0, v0, Ltv/periscope/android/api/ErrorResponse;->error:Ltv/periscope/android/api/ErrorResponseItem;

    iget v0, v0, Ltv/periscope/android/api/ErrorResponseItem;->reason:I

    packed-switch v0, :pswitch_data_0

    .line 79
    new-instance v0, Ltv/periscope/android/event/AppEvent;

    sget-object v1, Ltv/periscope/android/event/AppEvent$Type;->c:Ltv/periscope/android/event/AppEvent$Type;

    invoke-direct {v0, v1}, Ltv/periscope/android/event/AppEvent;-><init>(Ltv/periscope/android/event/AppEvent$Type;)V

    invoke-virtual {p0, v0, v4}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->unauthorizedLogout(Ltv/periscope/android/event/AppEvent;Z)V

    goto :goto_0

    .line 75
    :pswitch_0
    new-instance v0, Ltv/periscope/android/event/AppEvent;

    sget-object v1, Ltv/periscope/android/event/AppEvent$Type;->d:Ltv/periscope/android/event/AppEvent$Type;

    invoke-direct {v0, v1}, Ltv/periscope/android/event/AppEvent;-><init>(Ltv/periscope/android/event/AppEvent$Type;)V

    invoke-virtual {p0, v0, v4}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->unauthorizedLogout(Ltv/periscope/android/event/AppEvent;Z)V

    goto :goto_0

    .line 85
    :cond_1
    invoke-virtual {p0, p1}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->handleUnauthorizedLogin(Ltv/periscope/android/event/ApiEvent;)V

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected abstract handleUnauthorizedLogin(Ltv/periscope/android/event/ApiEvent;)V
.end method

.method protected handleUnauthorizedServiceResponse(Ltv/periscope/android/event/ServiceEvent;)V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p1, Ltv/periscope/android/event/ServiceEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/api/BackendServiceName;

    .line 57
    iget-object v1, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mBackendServiceManager:Lcym;

    invoke-virtual {v1, v0}, Lcym;->a(Ltv/periscope/android/api/BackendServiceName;)V

    .line 58
    iget-object v1, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mApiManager:Ltv/periscope/android/api/AuthedApiManager;

    invoke-virtual {v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->getAuthorizeTokenForBackendService(Ltv/periscope/android/api/BackendServiceName;)Ljava/lang/String;

    .line 59
    iget-object v1, p1, Ltv/periscope/android/event/ServiceEvent;->g:Ltv/periscope/android/api/ApiRunnable;

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mBackendServiceManager:Lcym;

    iget-object v2, p1, Ltv/periscope/android/event/ServiceEvent;->g:Ltv/periscope/android/api/ApiRunnable;

    invoke-virtual {v1, v0, v2}, Lcym;->a(Ltv/periscope/android/api/BackendServiceName;Ltv/periscope/android/api/ApiRunnable;)V

    .line 62
    :cond_0
    return-void
.end method

.method protected unauthorizedLogout(Ltv/periscope/android/event/AppEvent;Z)V
    .locals 3

    .prologue
    .line 180
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 181
    const-string/jumbo v1, "Reason"

    iget-object v2, p1, Ltv/periscope/android/event/AppEvent;->a:Ltv/periscope/android/event/AppEvent$Type;

    invoke-virtual {v2}, Ltv/periscope/android/event/AppEvent$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v1, Ltv/periscope/android/analytics/Event;->aK:Ltv/periscope/android/analytics/Event;

    invoke-static {v1, v0}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/Event;Ljava/util/HashMap;)V

    .line 183
    iget-object v0, p0, Ltv/periscope/android/api/DefaultAuthedEventHandler;->mApiManager:Ltv/periscope/android/api/AuthedApiManager;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/api/AuthedApiManager;->logout(Ltv/periscope/android/event/AppEvent;Z)V

    .line 184
    return-void
.end method
