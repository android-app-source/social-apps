.class public Ltv/periscope/android/api/ApiRunnable;
.super Ldcp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/api/ApiRunnable$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldcp",
        "<",
        "Ltv/periscope/android/event/ApiEvent;",
        ">;"
    }
.end annotation


# static fields
.field public static final ACTION_CODE_ACCESS_CHAT:I = 0x42

.field public static final ACTION_CODE_ACCESS_VIDEO:I = 0x41

.field public static final ACTION_CODE_ACTIVE_JUROR:I = 0x4b

.field public static final ACTION_CODE_ADD_MEMBERS_TO_CHANNEL:I = 0x57

.field public static final ACTION_CODE_ADJUST_BROADCAST_RANK:I = 0x3a

.field public static final ACTION_CODE_ASSOCIATE_DIGITS_ACCOUNT:I = 0x51

.field public static final ACTION_CODE_ASSOCIATE_TWEET_WITH_BROADCAST:I = 0x68

.field public static final ACTION_CODE_AUTHORIZE_TOKEN:I = 0x47

.field public static final ACTION_CODE_BATCH_FOLLOW:I = 0x15

.field public static final ACTION_CODE_BLOCK:I = 0x23

.field public static final ACTION_CODE_BROADCAST_META:I = 0x36

.field public static final ACTION_CODE_BROADCAST_SEARCH:I = 0x45

.field public static final ACTION_CODE_CHANNELS_SEARCH:I = 0x50

.field public static final ACTION_CODE_CLEAR_WATCHED_HISTORY:I = 0x67

.field public static final ACTION_CODE_CREATE_BROADCAST:I = 0x4e

.field public static final ACTION_CODE_CREATE_CHANNEL:I = 0x5c

.field public static final ACTION_CODE_CREATE_EXTERNAL_ENCODER:I = 0x6a

.field public static final ACTION_CODE_DEACTIVATE_ACCOUNT:I = 0x4f

.field public static final ACTION_CODE_DELETE_BROADCAST:I = 0x2b

.field public static final ACTION_CODE_DELETE_CHANNEL:I = 0x5b

.field public static final ACTION_CODE_DELETE_CHANNEL_MEMBER:I = 0x58

.field public static final ACTION_CODE_DELETE_EXTERNAL_ENCODER:I = 0x6c

.field public static final ACTION_CODE_END_BROADCAST:I = 0x1f

.field public static final ACTION_CODE_END_WATCHING:I = 0x1c

.field public static final ACTION_CODE_FIND_FRIENDS:I = 0xe

.field public static final ACTION_CODE_FOLLOW:I = 0x8

.field public static final ACTION_CODE_GET_AND_HYDRATE_CHANNEL_ACTIONS:I = 0x5e

.field public static final ACTION_CODE_GET_AND_HYDRATE_CHANNEL_MEMBERS:I = 0x56

.field public static final ACTION_CODE_GET_BLOCKED:I = 0x25

.field public static final ACTION_CODE_GET_BROADCASTS:I = 0x13

.field public static final ACTION_CODE_GET_BROADCASTS_FOR_CHANNEL:I = 0x4c

.field public static final ACTION_CODE_GET_BROADCASTS_FOR_TELEPORT:I = 0x40

.field public static final ACTION_CODE_GET_BROADCAST_FOR_EXTERNAL_ENCODER:I = 0x6e

.field public static final ACTION_CODE_GET_BROADCAST_ID_FROM_URL:I = 0x27

.field public static final ACTION_CODE_GET_BROADCAST_TRAILER:I = 0x52

.field public static final ACTION_CODE_GET_BROADCAST_VIEWERS:I = 0x14

.field public static final ACTION_CODE_GET_CHANNELS_FOR_MEMBER:I = 0x55

.field public static final ACTION_CODE_GET_CHANNEL_INFO:I = 0x53

.field public static final ACTION_CODE_GET_EXTERNAL_ENCODERS:I = 0x6d

.field public static final ACTION_CODE_GET_FOLLOWERS:I = 0x6

.field public static final ACTION_CODE_GET_FOLLOWERS_BY_ID:I = 0xc

.field public static final ACTION_CODE_GET_FOLLOWING:I = 0x7

.field public static final ACTION_CODE_GET_FOLLOWING_BY_ID:I = 0xb

.field public static final ACTION_CODE_GET_FOLLOWING_IDS_ONLY:I = 0x5f

.field public static final ACTION_CODE_GET_GLOBAL_BROADCAST_FEED:I = 0x4

.field public static final ACTION_CODE_GET_MUTUAL_FOLLOWS:I = 0x3c

.field public static final ACTION_CODE_GET_RECENT_GLOBAL_BROADCAST_FEED:I = 0x63

.field public static final ACTION_CODE_GET_SETTINGS:I = 0x1e

.field public static final ACTION_CODE_GET_SUGGESTED_CHANNELS:I = 0x46

.field public static final ACTION_CODE_GET_SUPERFANS:I = 0x65

.field public static final ACTION_CODE_GET_TRENDING_LOCATIONS:I = 0x48

.field public static final ACTION_CODE_GET_USER:I = 0x5

.field public static final ACTION_CODE_GET_USERS:I = 0x64

.field public static final ACTION_CODE_GET_USER_BY_ID:I = 0xa

.field public static final ACTION_CODE_GET_USER_BY_USERNAME:I = 0x3d

.field public static final ACTION_CODE_GET_USER_STATS:I = 0x5a

.field public static final ACTION_CODE_HELLO:I = 0x3b

.field public static final ACTION_CODE_LIVE_PLAYBACK_META:I = 0x35

.field public static final ACTION_CODE_MAIN_FEATURED:I = 0x12

.field public static final ACTION_CODE_MAIN_FOLLOWING:I = 0x11

.field public static final ACTION_CODE_MAIN_GLOBAL_MAP:I = 0x34

.field public static final ACTION_CODE_MARK_BROADCAST_PERSISTENT:I = 0x4d

.field public static final ACTION_CODE_MUTE:I = 0x37

.field public static final ACTION_CODE_PATCH_CHANNEL:I = 0x5d

.field public static final ACTION_CODE_PATCH_CHANNEL_MEMBER:I = 0x59

.field public static final ACTION_CODE_PING_BROADCAST:I = 0x20

.field public static final ACTION_CODE_PING_WATCHING:I = 0x1b

.field public static final ACTION_CODE_PUBLIC_ACCESS_CHAT:I = 0xd2

.field public static final ACTION_CODE_PUBLIC_ACCESS_VIDEO:I = 0xd1

.field public static final ACTION_CODE_PUBLIC_BLOCK:I = 0xd0

.field public static final ACTION_CODE_PUBLIC_END_WATCHING:I = 0xcb

.field public static final ACTION_CODE_PUBLIC_GET_BROADCAST:I = 0xd6

.field public static final ACTION_CODE_PUBLIC_GET_BROADCASTS:I = 0xc8

.field public static final ACTION_CODE_PUBLIC_GET_BROADCAST_ID_FROM_URL:I = 0xd5

.field public static final ACTION_CODE_PUBLIC_GET_USER:I = 0xd4

.field public static final ACTION_CODE_PUBLIC_MARK_ABUSE:I = 0xcf

.field public static final ACTION_CODE_PUBLIC_PING_WATCHING:I = 0xca

.field public static final ACTION_CODE_PUBLIC_REPLAY_THUMBNAIL_PLAYLIST:I = 0xce

.field public static final ACTION_CODE_PUBLIC_START_WATCHING:I = 0xd3

.field public static final ACTION_CODE_PUBLISH_BROADCAST:I = 0x1a

.field public static final ACTION_CODE_RECENTLY_WATCHED_BROADCASTS:I = 0x66

.field public static final ACTION_CODE_REPLAY_PLAYBACK_META:I = 0x60

.field public static final ACTION_CODE_REPLAY_THUMBNAIL_PLAYLIST:I = 0x3e

.field public static final ACTION_CODE_REPORT_BROADCAST:I = 0x29

.field public static final ACTION_CODE_REPORT_COMMENT:I = 0x49

.field public static final ACTION_CODE_RETWEET_BROADCAST:I = 0x62

.field public static final ACTION_CODE_SET_EXTERNAL_ENCODER_NAME:I = 0x6b

.field public static final ACTION_CODE_SET_SETTINGS:I = 0x1d

.field public static final ACTION_CODE_SHARE_BROADCAST:I = 0x26

.field public static final ACTION_CODE_START_WATCHING:I = 0x43

.field public static final ACTION_CODE_SUPPORTED_LANGUAGES:I = 0x39

.field public static final ACTION_CODE_TWEET_BROADCAST_PUBLISHED:I = 0x61

.field public static final ACTION_CODE_TWITTER_LOGIN:I = 0x1

.field public static final ACTION_CODE_TWITTER_TOKEN_LOGIN:I = 0x54

.field public static final ACTION_CODE_UNBAN:I = 0x3f

.field public static final ACTION_CODE_UNBLOCK:I = 0x24

.field public static final ACTION_CODE_UNFOLLOW:I = 0x9

.field public static final ACTION_CODE_UNKNOWN:I = -0x1

.field public static final ACTION_CODE_UNMUTE:I = 0x38

.field public static final ACTION_CODE_UPDATE_PROFILE_DESCRIPTION:I = 0x2e

.field public static final ACTION_CODE_UPDATE_PROFILE_DISPLAY_NAME:I = 0x2f

.field public static final ACTION_CODE_UPLOAD_BROADCASTER_LOGS:I = 0x69

.field public static final ACTION_CODE_UPLOAD_PROFILE_IMAGE:I = 0x2d

.field public static final ACTION_CODE_UPLOAD_TEST:I = 0x28

.field public static final ACTION_CODE_USER_BROADCASTS:I = 0x2c

.field public static final ACTION_CODE_USER_SEARCH:I = 0xf

.field public static final ACTION_CODE_VALIDATE_USERNAME:I = 0x2

.field public static final ACTION_CODE_VERIFY_USERNAME:I = 0x3

.field public static final ACTION_CODE_VOTE:I = 0x4a

.field public static final BACKOFF_INTERVAL_NONE:J = 0x0L

.field private static final DEADLINE_MS:J

.field public static final DEFAULT_BACKOFF_MS:J = 0x3e8L

.field public static final DEFAULT_RETRIES:I = 0x5

.field public static final EXTRA_ABUSE_TYPE:Ljava/lang/String; = "e_abuse_type"

.field public static final EXTRA_AUTHORIZE_SERVICE_NAME:Ljava/lang/String; = "e_service_name"

.field public static final EXTRA_BACKGROUND:Ljava/lang/String; = "e_background"

.field public static final EXTRA_BIT_RATE:Ljava/lang/String; = "e_bit_rate"

.field public static final EXTRA_BROADCAST_ID:Ljava/lang/String; = "e_broadcast_id"

.field public static final EXTRA_CACHE_DIR:Ljava/lang/String; = "e_cache_dir"

.field public static final EXTRA_CAMERA_ROTATION:Ljava/lang/String; = "e_camera_rotation"

.field public static final EXTRA_CHANNEL_MEMBER_MUTED:Ljava/lang/String; = "e_channel_member_muted"

.field public static final EXTRA_COOKIE:Ljava/lang/String; = "e_cookie"

.field public static final EXTRA_CREATE_USER:Ljava/lang/String; = "e_create_user"

.field public static final EXTRA_DECREASE_RANK:Ljava/lang/String; = "e_decrease_rank"

.field public static final EXTRA_DESCRIPTION:Ljava/lang/String; = "e_description"

.field public static final EXTRA_DIGITS_IDS:Ljava/lang/String; = "e_digits_ids"

.field public static final EXTRA_DISPLAY_NAME:Ljava/lang/String; = "e_display_name"

.field public static final EXTRA_EVENT_TYPE:Ljava/lang/String; = "e_event_type"

.field public static final EXTRA_FILE_DIR:Ljava/lang/String; = "e_file_dir"

.field public static final EXTRA_FOLLOWING_ONLY_CHAT:Ljava/lang/String; = "e_following_only_chat"

.field public static final EXTRA_HAS_LOCATION:Ljava/lang/String; = "e_has_loc"

.field public static final EXTRA_HAS_MODERATION:Ljava/lang/String; = "e_has_moderation"

.field public static final EXTRA_HEIGHT:Ljava/lang/String; = "extra_height"

.field public static final EXTRA_IDS:Ljava/lang/String; = "extra_ids"

.field public static final EXTRA_INCREASE_RANK:Ljava/lang/String; = "e_increase_rank"

.field public static final EXTRA_INSTALL_ID:Ljava/lang/String; = "e_install_id"

.field public static final EXTRA_LANGUAGES:Ljava/lang/String; = "e_languages"

.field public static final EXTRA_LAT:Ljava/lang/String; = "e_lat"

.field public static final EXTRA_LOCALE:Ljava/lang/String; = "e_locale"

.field public static final EXTRA_LOCKED_IDs:Ljava/lang/String; = "e_locked_ids"

.field public static final EXTRA_LOCKED_PRIVATE_CHANNEL_IDs:Ljava/lang/String; = "e_locked_private_channel_ids"

.field public static final EXTRA_LOGGER_NAME:Ljava/lang/String; = "e_logger_name"

.field public static final EXTRA_LONG:Ljava/lang/String; = "e_long"

.field public static final EXTRA_MY_USER_ID:Ljava/lang/String; = "e_my_user_id"

.field public static final EXTRA_NUM_COMMENTS:Ljava/lang/String; = "e_n_comments"

.field public static final EXTRA_NUM_HEARTS:Ljava/lang/String; = "e_num_hearts"

.field public static final EXTRA_ONLY_PUBLIC_PUBLISH:Ljava/lang/String; = "e_only_public_publish"

.field public static final EXTRA_PERSISTENT:Ljava/lang/String; = "persistent"

.field public static final EXTRA_PHONE_NUMBER:Ljava/lang/String; = "e_phone_number"

.field public static final EXTRA_POINT_1_LAT:Ljava/lang/String; = "e_point_1_lat"

.field public static final EXTRA_POINT_1_LNG:Ljava/lang/String; = "e_point_1_lng"

.field public static final EXTRA_POINT_2_LAT:Ljava/lang/String; = "e_point_2_lat"

.field public static final EXTRA_POINT_2_LNG:Ljava/lang/String; = "e_point_2_lng"

.field public static final EXTRA_QUERY:Ljava/lang/String; = "extra_query"

.field public static final EXTRA_REGION:Ljava/lang/String; = "e_region"

.field public static final EXTRA_SECRET_KEY:Ljava/lang/String; = "e_secret_key"

.field public static final EXTRA_SECRET_TOKEN:Ljava/lang/String; = "e_secret_token"

.field public static final EXTRA_SERVICE_AUTORIZATION_TOKEN:Ljava/lang/String; = "e_service_auth_token"

.field public static final EXTRA_SERVICE_CHANNEL_CURRENT_NAME:Ljava/lang/String; = "e_service_channel_current_name"

.field public static final EXTRA_SERVICE_CHANNEL_ID:Ljava/lang/String; = "e_service_channel_id"

.field public static final EXTRA_SERVICE_CHANNEL_NAME:Ljava/lang/String; = "e_service_channel_name"

.field public static final EXTRA_SERVICE_CHANNEL_TYPE:Ljava/lang/String; = "e_service_channel_type"

.field public static final EXTRA_SESSION_ID:Ljava/lang/String; = "e_session_id"

.field public static final EXTRA_SESSION_TYPE:Ljava/lang/String; = "e_session_type"

.field public static final EXTRA_SIGN_UP:Ljava/lang/String; = "e_sign_up"

.field public static final EXTRA_TIME_ZONE:Ljava/lang/String; = "e_time_zone"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "e_title"

.field public static final EXTRA_TOKEN:Ljava/lang/String; = "e_token"

.field public static final EXTRA_TRACKING_AUTOPLAY:Ljava/lang/String; = "e_autoplay"

.field public static final EXTRA_UNLIMITED_CHAT:Ljava/lang/String; = "e_unlimited_chat"

.field public static final EXTRA_USERNAME:Ljava/lang/String; = "e_username"

.field public static final EXTRA_USER_ID:Ljava/lang/String; = "e_user_id"

.field public static final EXTRA_USER_IDS:Ljava/lang/String; = "e_user_ids"

.field public static final EXTRA_USE_ML:Ljava/lang/String; = "e_use_ml"

.field public static final EXTRA_USE_PERSONAL:Ljava/lang/String; = "e_use_personal"

.field public static final EXTRA_VIEWER_MODERATION:Ljava/lang/String; = "e_viewer_moderation"

.field public static final EXTRA_WIDTH:Ljava/lang/String; = "extra_width"

.field public static final MAX_BATCH_FOLLOW:I = 0x64

.field public static final MAX_GET_BROADCASTS:I = 0x64

.field public static final NUM_RETRIES_NONE:I = 0x0

.field private static final PROFILE_IMAGE_FILENAME:Ljava/lang/String; = "image.jpeg"

.field private static final TAG:Ljava/lang/String; = "PsApi"

.field private static final UPLOAD_TEST_SIZE:I = 0x3d090


# instance fields
.field private final mActionCode:I

.field private final mBundle:Landroid/os/Bundle;

.field private final mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

.field private final mEventBus:Lde/greenrobot/event/c;

.field private final mPublicService:Ltv/periscope/android/api/PublicApiService;

.field private final mRequest:Ltv/periscope/android/api/ApiRequest;

.field private final mRequestId:Ljava/lang/String;

.field private final mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

.field private final mService:Ltv/periscope/android/api/ApiService;

.field private final mSigner:Ltv/periscope/android/signer/SignerService;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 262
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ltv/periscope/android/api/ApiRunnable;->DEADLINE_MS:J

    return-void
.end method

.method constructor <init>(Lde/greenrobot/event/c;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/api/PublicApiService;Ltv/periscope/android/signer/SignerService;Ltv/periscope/android/api/service/channels/ChannelsService;Ltv/periscope/android/api/service/safety/SafetyService;ILjava/lang/String;Ltv/periscope/android/api/ApiRequest;Landroid/os/Bundle;IJ)V
    .locals 2

    .prologue
    .line 378
    sget-wide v0, Ltv/periscope/android/api/ApiRunnable;->DEADLINE_MS:J

    invoke-direct {p0, v0, v1}, Ldcp;-><init>(J)V

    .line 379
    iput-object p1, p0, Ltv/periscope/android/api/ApiRunnable;->mEventBus:Lde/greenrobot/event/c;

    .line 380
    iput-object p2, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    .line 381
    iput-object p3, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    .line 382
    iput-object p4, p0, Ltv/periscope/android/api/ApiRunnable;->mSigner:Ltv/periscope/android/signer/SignerService;

    .line 383
    iput-object p5, p0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    .line 384
    iput-object p6, p0, Ltv/periscope/android/api/ApiRunnable;->mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

    .line 385
    iput p7, p0, Ltv/periscope/android/api/ApiRunnable;->mActionCode:I

    .line 386
    iput-object p8, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    .line 387
    iput-object p9, p0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    .line 388
    iput-object p10, p0, Ltv/periscope/android/api/ApiRunnable;->mBundle:Landroid/os/Bundle;

    .line 389
    return-void
.end method

.method static synthetic access$000(Ltv/periscope/android/api/ApiRunnable;[Ljava/io/File;)Lretrofit/mime/TypedInput;
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0, p1}, Ltv/periscope/android/api/ApiRunnable;->newLogFileForUpload([Ljava/io/File;)Lretrofit/mime/TypedInput;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ltv/periscope/android/api/ApiRunnable;)Ltv/periscope/android/api/ApiService;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    return-object v0
.end method

.method static synthetic access$200(Ltv/periscope/android/api/ApiRunnable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    return-object v0
.end method

.method private accessChat(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1647
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1648
    const-string/jumbo v0, "e_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1649
    const-string/jumbo v1, "e_viewer_moderation"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 1650
    const-string/jumbo v2, "e_unlimited_chat"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 1651
    new-instance v3, Ltv/periscope/android/api/AccessChatRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/AccessChatRequest;-><init>()V

    .line 1652
    const-string/jumbo v4, "e_cookie"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Ltv/periscope/android/api/AccessChatRequest;->cookie:Ljava/lang/String;

    .line 1653
    iput-object v0, v3, Ltv/periscope/android/api/AccessChatRequest;->chatToken:Ljava/lang/String;

    .line 1654
    iput-boolean v2, v3, Ltv/periscope/android/api/AccessChatRequest;->unlimitedChat:Z

    .line 1655
    iput-boolean v1, v3, Ltv/periscope/android/api/AccessChatRequest;->viewerModeration:Z

    .line 1656
    invoke-direct {p0, p1}, Ltv/periscope/android/api/ApiRunnable;->getLanguages(Landroid/os/Bundle;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Ltv/periscope/android/api/AccessChatRequest;->languages:[Ljava/lang/String;

    .line 1658
    :try_start_0
    const-string/jumbo v1, "PsApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "accessing chat for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1659
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->accessChat(Ltv/periscope/android/api/AccessChatRequest;)Ltv/periscope/android/api/AccessChatResponse;

    move-result-object v4

    .line 1660
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "accessChat succeeded"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->u:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-virtual {v4}, Ltv/periscope/android/api/AccessChatResponse;->create()Ltv/periscope/model/u;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1664
    :goto_0
    return-object v0

    .line 1662
    :catch_0
    move-exception v4

    .line 1663
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "accessChat failed"

    invoke-static {v0, v1, v4}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1664
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->u:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private accessChatPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2152
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2153
    new-instance v3, Ltv/periscope/android/api/AccessChatPublicRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/AccessChatPublicRequest;-><init>()V

    .line 2154
    const-string/jumbo v0, "e_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2155
    const-string/jumbo v1, "e_install_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Ltv/periscope/android/api/AccessChatPublicRequest;->installId:Ljava/lang/String;

    .line 2156
    iput-object v0, v3, Ltv/periscope/android/api/AccessChatPublicRequest;->chatToken:Ljava/lang/String;

    .line 2157
    invoke-direct {p0, p1}, Ltv/periscope/android/api/ApiRunnable;->getLanguages(Landroid/os/Bundle;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Ltv/periscope/android/api/AccessChatPublicRequest;->languages:[Ljava/lang/String;

    .line 2159
    :try_start_0
    const-string/jumbo v1, "PsApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "accessing public chat for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2160
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/PublicApiService;->getAccessChatPublic(Ltv/periscope/android/api/AccessChatPublicRequest;)Ltv/periscope/android/api/AccessChatResponse;

    move-result-object v4

    .line 2161
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "accessChatPublic succeeded"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->u:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-virtual {v4}, Ltv/periscope/android/api/AccessChatResponse;->create()Ltv/periscope/model/u;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2165
    :goto_0
    return-object v0

    .line 2163
    :catch_0
    move-exception v4

    .line 2164
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "accessChatPublic failed"

    invoke-static {v0, v1, v4}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2165
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->u:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private accessVideo(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1627
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1628
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1629
    const-string/jumbo v1, "e_token"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1630
    new-instance v3, Ltv/periscope/android/api/AccessVideoRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/AccessVideoRequest;-><init>()V

    .line 1631
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Ltv/periscope/android/api/AccessVideoRequest;->cookie:Ljava/lang/String;

    .line 1632
    iput-object v1, v3, Ltv/periscope/android/api/AccessVideoRequest;->lifeCycleToken:Ljava/lang/String;

    .line 1633
    iput-object v0, v3, Ltv/periscope/android/api/AccessVideoRequest;->broadcastId:Ljava/lang/String;

    .line 1635
    :try_start_0
    const-string/jumbo v1, "PsApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "accessing video for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1636
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->accessVideo(Ltv/periscope/android/api/AccessVideoRequest;)Ltv/periscope/android/api/AccessVideoResponse;

    move-result-object v4

    .line 1637
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "accessVideo succeeded"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1638
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->t:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-virtual {v4}, Ltv/periscope/android/api/AccessVideoResponse;->create()Ltv/periscope/model/af;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1641
    :goto_0
    return-object v0

    .line 1639
    :catch_0
    move-exception v4

    .line 1640
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "accessVideo failed"

    invoke-static {v0, v1, v4}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1641
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->t:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private accessVideoPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 8

    .prologue
    .line 2118
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2122
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2123
    const-string/jumbo v1, "e_install_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2124
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2125
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2126
    new-instance v4, Ltv/periscope/android/api/GetBroadcastsPublicRequest;

    invoke-direct {v4}, Ltv/periscope/android/api/GetBroadcastsPublicRequest;-><init>()V

    .line 2127
    iput-object v1, v4, Ltv/periscope/android/api/GetBroadcastsPublicRequest;->installId:Ljava/lang/String;

    .line 2128
    iput-object v2, v4, Ltv/periscope/android/api/GetBroadcastsPublicRequest;->ids:Ljava/util/ArrayList;

    .line 2130
    new-instance v3, Ltv/periscope/android/api/AccessVideoPublicRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/AccessVideoPublicRequest;-><init>()V

    .line 2131
    iput-object v1, v3, Ltv/periscope/android/api/AccessVideoPublicRequest;->installId:Ljava/lang/String;

    .line 2132
    iput-object v0, v3, Ltv/periscope/android/api/AccessVideoPublicRequest;->broadcastId:Ljava/lang/String;

    .line 2134
    :try_start_0
    iget-object v1, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v1, v4}, Ltv/periscope/android/api/PublicApiService;->getBroadcastsPublic(Ltv/periscope/android/api/GetBroadcastsPublicRequest;)Ljava/util/List;

    move-result-object v1

    .line 2135
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2136
    const-string/jumbo v2, "PsApi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "accessing public video  for "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2137
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/PublicApiService;->getAccessVideoPublic(Ltv/periscope/android/api/AccessVideoPublicRequest;)Ltv/periscope/android/api/AccessVideoResponse;

    move-result-object v4

    .line 2138
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsBroadcast;

    iput-object v0, v4, Ltv/periscope/android/api/AccessVideoResponse;->broadcast:Ltv/periscope/android/api/PsBroadcast;

    .line 2139
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "accessVideoPublic succeeded"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2140
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->t:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-virtual {v4}, Ltv/periscope/android/api/AccessVideoResponse;->create()Ltv/periscope/model/af;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V

    .line 2147
    :goto_0
    return-object v0

    .line 2142
    :cond_0
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->t:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const-string/jumbo v4, "getBroadcastsPublic"

    new-instance v6, Ljava/lang/Exception;

    const-string/jumbo v7, "accessVideoPublic returned no results"

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v6}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2145
    :catch_0
    move-exception v4

    .line 2146
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "accessVideoPublic failed"

    invoke-static {v0, v1, v4}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2147
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->t:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private activeJuror(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 14

    .prologue
    .line 2299
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2300
    const-string/jumbo v0, "e_service_auth_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2301
    const-string/jumbo v1, "tv.periscope.android.api.service.safety.EXTRA_ACTIVE_JUROR_REQUEST"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-static {v1}, Lorg/parceler/c;->a(Landroid/os/Parcelable;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ltv/periscope/android/api/service/safety/ActiveJurorRequest;

    .line 2303
    :try_start_0
    invoke-direct {p0, v0}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 2304
    iget-object v1, p0, Ltv/periscope/android/api/ApiRunnable;->mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

    invoke-interface {v1, v0, v3}, Ltv/periscope/android/api/service/safety/SafetyService;->juror(Ljava/lang/String;Ltv/periscope/android/api/service/safety/ActiveJurorRequest;)Ltv/periscope/android/api/service/safety/ActiveJurorResponse;

    move-result-object v4

    .line 2305
    new-instance v0, Ltv/periscope/android/event/ServiceEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aq:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2307
    :goto_0
    return-object v0

    .line 2306
    :catch_0
    move-exception v11

    .line 2307
    new-instance v6, Ltv/periscope/android/event/ServiceEvent;

    sget-object v7, Ltv/periscope/android/event/ApiEvent$Type;->aq:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v8, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v10, Ltv/periscope/android/api/BackendServiceName;->SAFETY:Ltv/periscope/android/api/BackendServiceName;

    move-object v9, v3

    move v12, v5

    move-object v13, p0

    invoke-direct/range {v6 .. v13}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v0, v6

    goto :goto_0
.end method

.method private adjustBroadcastRank(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1962
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1963
    new-instance v3, Ltv/periscope/android/api/AdjustBroadcastRankRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/AdjustBroadcastRankRequest;-><init>()V

    .line 1964
    const-string/jumbo v0, "e_cookie"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/AdjustBroadcastRankRequest;->cookie:Ljava/lang/String;

    .line 1965
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/AdjustBroadcastRankRequest;->broadcastId:Ljava/lang/String;

    .line 1966
    const-string/jumbo v0, "e_increase_rank"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v3, Ltv/periscope/android/api/AdjustBroadcastRankRequest;->increase:Z

    .line 1967
    const-string/jumbo v0, "e_decrease_rank"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v3, Ltv/periscope/android/api/AdjustBroadcastRankRequest;->decrease:Z

    .line 1969
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->adjustBroadcastRank(Ltv/periscope/android/api/AdjustBroadcastRankRequest;)Ltv/periscope/android/api/AdjustBroadcastRankResponse;

    move-result-object v4

    .line 1970
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->O:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1972
    :goto_0
    return-object v0

    .line 1971
    :catch_0
    move-exception v4

    .line 1972
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->O:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private associateTweetWithBroadcast(Z)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2058
    iget-object v3, p0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    check-cast v3, Ltv/periscope/android/api/AssociateTweetWithBroadcastRequest;

    .line 2060
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->associateTweetWithBroadcast(Ltv/periscope/android/api/AssociateTweetWithBroadcastRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v4

    .line 2061
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aI:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2063
    :goto_0
    return-object v0

    .line 2062
    :catch_0
    move-exception v4

    .line 2063
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aI:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private block(Ljava/lang/String;Ltv/periscope/android/api/BlockRequest;Z)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1978
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, p2}, Ltv/periscope/android/api/ApiService;->block(Ltv/periscope/android/api/BlockRequest;)Ltv/periscope/android/api/BlockResponse;

    move-result-object v4

    .line 1979
    iget-object v0, p2, Ltv/periscope/android/api/BlockRequest;->userId:Ljava/lang/String;

    iput-object v0, v4, Ltv/periscope/android/api/BlockResponse;->userId:Ljava/lang/String;

    .line 1980
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->X:Ltv/periscope/android/event/ApiEvent$Type;

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1982
    :goto_0
    return-object v0

    .line 1981
    :catch_0
    move-exception v4

    .line 1982
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->X:Ltv/periscope/android/event/ApiEvent$Type;

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private blockPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2253
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2254
    new-instance v3, Ltv/periscope/android/api/BlockPublicRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/BlockPublicRequest;-><init>()V

    .line 2255
    const-string/jumbo v0, "e_user_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/BlockPublicRequest;->userId:Ljava/lang/String;

    .line 2256
    const-string/jumbo v0, "e_session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/BlockPublicRequest;->session:Ljava/lang/String;

    .line 2257
    const-string/jumbo v0, "e_install_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/BlockPublicRequest;->installId:Ljava/lang/String;

    .line 2259
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/PublicApiService;->blockPublic(Ltv/periscope/android/api/BlockPublicRequest;)Ltv/periscope/android/api/BlockResponse;

    move-result-object v4

    .line 2260
    iget-object v0, v3, Ltv/periscope/android/api/BlockPublicRequest;->userId:Ljava/lang/String;

    iput-object v0, v4, Ltv/periscope/android/api/BlockResponse;->userId:Ljava/lang/String;

    .line 2261
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->X:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2263
    :goto_0
    return-object v0

    .line 2262
    :catch_0
    move-exception v4

    .line 2263
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->X:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private broadcastSearch(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1611
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1612
    const-string/jumbo v0, "extra_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1613
    new-instance v3, Ltv/periscope/android/api/BroadcastSearchRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/BroadcastSearchRequest;-><init>()V

    .line 1614
    const-string/jumbo v1, "e_cookie"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Ltv/periscope/android/api/BroadcastSearchRequest;->cookie:Ljava/lang/String;

    .line 1615
    iput-object v0, v3, Ltv/periscope/android/api/BroadcastSearchRequest;->query:Ljava/lang/String;

    iput-object v0, v3, Ltv/periscope/android/api/BroadcastSearchRequest;->search:Ljava/lang/String;

    .line 1616
    const/4 v0, 0x1

    iput-boolean v0, v3, Ltv/periscope/android/api/BroadcastSearchRequest;->includeReplay:Z

    .line 1618
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->broadcastSearch(Ltv/periscope/android/api/BroadcastSearchRequest;)Ljava/util/List;

    move-result-object v4

    .line 1619
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->y:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct {p0, v4}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1621
    :goto_0
    return-object v0

    .line 1620
    :catch_0
    move-exception v4

    .line 1621
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->y:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private convert(Ljava/util/Collection;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ltv/periscope/android/api/PsBroadcast;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/p;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2002
    if-nez p1, :cond_0

    .line 2003
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 2010
    :goto_0
    return-object v0

    .line 2006
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2007
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsBroadcast;

    .line 2008
    invoke-virtual {v0}, Ltv/periscope/android/api/PsBroadcast;->create()Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 2010
    goto :goto_0
.end method

.method private convertBids(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/service/channels/PsGetBroadcastsForChannelResponse$Bid;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1588
    if-nez p1, :cond_0

    .line 1589
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 1596
    :goto_0
    return-object v0

    .line 1592
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1593
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/service/channels/PsGetBroadcastsForChannelResponse$Bid;

    .line 1594
    iget-object v0, v0, Ltv/periscope/android/api/service/channels/PsGetBroadcastsForChannelResponse$Bid;->bid:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 1596
    goto :goto_0
.end method

.method private createExternalEncoder(Z)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2377
    iget-object v3, p0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    check-cast v3, Ltv/periscope/android/api/CreateExternalEncoderRequest;

    .line 2379
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->createExternalEncoder(Ltv/periscope/android/api/CreateExternalEncoderRequest;)Ltv/periscope/android/api/CreateExternalEncoderResponse;

    move-result-object v4

    .line 2380
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aK:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2382
    :goto_0
    return-object v0

    .line 2381
    :catch_0
    move-exception v4

    .line 2382
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aK:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private deleteBroadcast(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1913
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1914
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1915
    new-instance v3, Ltv/periscope/android/api/DeleteBroadcastRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/DeleteBroadcastRequest;-><init>()V

    .line 1916
    const-string/jumbo v0, "e_cookie"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/DeleteBroadcastRequest;->cookie:Ljava/lang/String;

    .line 1917
    iput-object v4, v3, Ltv/periscope/android/api/DeleteBroadcastRequest;->broadcastId:Ljava/lang/String;

    .line 1919
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->deleteBroadcast(Ltv/periscope/android/api/DeleteBroadcastRequest;)Ltv/periscope/android/api/PsResponse;

    .line 1920
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->L:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    .line 1921
    invoke-static {v4}, Ltv/periscope/model/x;->a(Ljava/lang/String;)Ltv/periscope/model/x;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1923
    :goto_0
    return-object v0

    .line 1922
    :catch_0
    move-exception v4

    .line 1923
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->L:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private deleteExternalEncoder(Z)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2355
    iget-object v3, p0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    check-cast v3, Ltv/periscope/android/api/DeleteExternalEncoderRequest;

    .line 2357
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->deleteExternalEncoder(Ltv/periscope/android/api/DeleteExternalEncoderRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v4

    .line 2358
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aM:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2360
    :goto_0
    return-object v0

    .line 2359
    :catch_0
    move-exception v4

    .line 2360
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aM:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private endBroadcast(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 5

    .prologue
    .line 1867
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1868
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1869
    const-string/jumbo v2, "e_logger_name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1870
    const-string/jumbo v3, "e_cookie"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1871
    new-instance v4, Ltv/periscope/android/api/ApiRunnable$5;

    invoke-direct {v4, p0, v3, v1, v0}, Ltv/periscope/android/api/ApiRunnable$5;-><init>(Ltv/periscope/android/api/ApiRunnable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1886
    invoke-virtual {v4, v2}, Ltv/periscope/android/api/ApiRequestWithLogs;->execute(Ljava/lang/String;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v0

    return-object v0
.end method

.method private endWatching(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 8

    .prologue
    .line 1804
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 1805
    const-string/jumbo v0, "e_session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1806
    const-string/jumbo v0, "e_logger_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1807
    const-string/jumbo v0, "e_num_hearts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 1808
    const-string/jumbo v0, "e_n_comments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 1809
    const-string/jumbo v0, "e_cookie"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1810
    new-instance v0, Ltv/periscope/android/api/ApiRunnable$3;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/api/ApiRunnable$3;-><init>(Ltv/periscope/android/api/ApiRunnable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1825
    invoke-virtual {v0, v7}, Ltv/periscope/android/api/ApiRequestWithLogs;->execute(Ljava/lang/String;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v0

    return-object v0
.end method

.method private endWatchingPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2207
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2208
    new-instance v3, Ltv/periscope/android/api/EndWatchingPublicRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/EndWatchingPublicRequest;-><init>()V

    .line 2209
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/EndWatchingPublicRequest;->broadcastId:Ljava/lang/String;

    .line 2210
    const-string/jumbo v0, "e_session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/EndWatchingPublicRequest;->session:Ljava/lang/String;

    .line 2211
    const-string/jumbo v0, "e_install_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/EndWatchingPublicRequest;->installId:Ljava/lang/String;

    .line 2213
    :try_start_0
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "stop watching"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/PublicApiService;->endWatchingPublic(Ltv/periscope/android/api/EndWatchingPublicRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v4

    .line 2215
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "endWatching succeeded"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2216
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->Q:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2219
    :goto_0
    return-object v0

    .line 2217
    :catch_0
    move-exception v4

    .line 2218
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "endWatching failed"

    invoke-static {v0, v1, v4}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2219
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->Q:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private getBlocked(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2023
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2024
    new-instance v3, Ltv/periscope/android/api/PsRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 2025
    const-string/jumbo v0, "e_cookie"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    .line 2027
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->getBlocked(Ltv/periscope/android/api/PsRequest;)Ljava/util/List;

    move-result-object v4

    .line 2028
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->Z:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2030
    :goto_0
    return-object v0

    .line 2029
    :catch_0
    move-exception v4

    .line 2030
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->Z:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private getBroadcastForExternalEncoder(Z)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2344
    iget-object v3, p0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    check-cast v3, Ltv/periscope/android/api/GetBroadcastForExternalEncoderRequest;

    .line 2346
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->getBroadcastForExternalEncoder(Ltv/periscope/android/api/GetBroadcastForExternalEncoderRequest;)Ltv/periscope/android/api/GetBroadcastForExternalEncoderResponse;

    move-result-object v4

    .line 2347
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aO:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2349
    :goto_0
    return-object v0

    .line 2348
    :catch_0
    move-exception v4

    .line 2349
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aO:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private getBroadcastPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2069
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2070
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2072
    :try_start_0
    iget-object v1, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v1, v0}, Ltv/periscope/android/api/PublicApiService;->getBroadcastPublic(Ljava/lang/String;)Ltv/periscope/android/api/GetBroadcastPublicResponse;

    move-result-object v0

    .line 2073
    iget-object v1, v0, Ltv/periscope/android/api/GetBroadcastPublicResponse;->broadcast:Ltv/periscope/android/api/PsBroadcast;

    invoke-virtual {v1}, Ltv/periscope/android/api/PsBroadcast;->create()Ltv/periscope/model/p;

    move-result-object v1

    .line 2074
    iget-object v0, v0, Ltv/periscope/android/api/GetBroadcastPublicResponse;->numWatched:Ljava/lang/Long;

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->b(Ljava/lang/Long;)V

    .line 2075
    new-instance v4, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2076
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2077
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->B:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2079
    :goto_0
    return-object v0

    .line 2078
    :catch_0
    move-exception v4

    .line 2079
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->B:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object v3, v6

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private getBroadcastsPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2084
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2085
    const-string/jumbo v0, "extra_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2086
    new-instance v3, Ltv/periscope/android/api/GetBroadcastsPublicRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/GetBroadcastsPublicRequest;-><init>()V

    .line 2087
    const-string/jumbo v1, "e_install_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Ltv/periscope/android/api/GetBroadcastsPublicRequest;->installId:Ljava/lang/String;

    .line 2088
    iput-object v0, v3, Ltv/periscope/android/api/GetBroadcastsPublicRequest;->ids:Ljava/util/ArrayList;

    .line 2090
    :try_start_0
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "getting broadcasts"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2091
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/PublicApiService;->getBroadcastsPublic(Ltv/periscope/android/api/GetBroadcastsPublicRequest;)Ljava/util/List;

    move-result-object v4

    .line 2092
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "getBroadcasts succeeded"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->B:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct {p0, v4}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2096
    :goto_0
    return-object v0

    .line 2094
    :catch_0
    move-exception v4

    .line 2095
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "getBroadcasts failed"

    invoke-static {v0, v1, v4}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2096
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->B:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private getExternalEncoders(Z)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2333
    iget-object v3, p0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    check-cast v3, Ltv/periscope/android/api/PsRequest;

    .line 2335
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->getExternalEncoders(Ltv/periscope/android/api/PsRequest;)Ltv/periscope/android/api/GetExternalEncodersResponse;

    move-result-object v4

    .line 2336
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aN:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2338
    :goto_0
    return-object v0

    .line 2337
    :catch_0
    move-exception v4

    .line 2338
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aN:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private getLanguages(Landroid/os/Bundle;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 1573
    const-string/jumbo v0, "e_languages"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1575
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1576
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 1577
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1583
    :goto_0
    return-object v0

    .line 1580
    :cond_0
    invoke-static {}, Ltv/periscope/android/util/o;->b()Ljava/util/List;

    move-result-object v1

    .line 1581
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 1582
    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto :goto_0
.end method

.method private getMapBroadcastFeed(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1734
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1735
    new-instance v3, Ltv/periscope/android/api/MapGeoBroadcastFeedRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/MapGeoBroadcastFeedRequest;-><init>()V

    .line 1736
    const-string/jumbo v0, "e_cookie"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/MapGeoBroadcastFeedRequest;->cookie:Ljava/lang/String;

    .line 1737
    const-string/jumbo v0, "e_point_1_lat"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, v3, Ltv/periscope/android/api/MapGeoBroadcastFeedRequest;->p1Lat:F

    .line 1738
    const-string/jumbo v0, "e_point_1_lng"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, v3, Ltv/periscope/android/api/MapGeoBroadcastFeedRequest;->p1Lng:F

    .line 1739
    const-string/jumbo v0, "e_point_2_lat"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, v3, Ltv/periscope/android/api/MapGeoBroadcastFeedRequest;->p2Lat:F

    .line 1740
    const-string/jumbo v0, "e_point_2_lng"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, v3, Ltv/periscope/android/api/MapGeoBroadcastFeedRequest;->p2Lng:F

    .line 1741
    const/4 v0, 0x1

    iput-boolean v0, v3, Ltv/periscope/android/api/MapGeoBroadcastFeedRequest;->includeReplay:Z

    .line 1743
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->mapGeoBroadcastFeed(Ltv/periscope/android/api/MapGeoBroadcastFeedRequest;)Ljava/util/List;

    move-result-object v4

    .line 1744
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->H:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct {p0, v4}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1746
    :goto_0
    return-object v0

    .line 1745
    :catch_0
    move-exception v4

    .line 1746
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->H:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private getRankedBroadcastsRequest(Landroid/os/Bundle;)Ltv/periscope/android/api/RankedBroadcastsRequest;
    .locals 2

    .prologue
    .line 1601
    new-instance v0, Ltv/periscope/android/api/RankedBroadcastsRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/RankedBroadcastsRequest;-><init>()V

    .line 1602
    const-string/jumbo v1, "e_cookie"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/RankedBroadcastsRequest;->cookie:Ljava/lang/String;

    .line 1603
    invoke-direct {p0, p1}, Ltv/periscope/android/api/ApiRunnable;->getLanguages(Landroid/os/Bundle;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/RankedBroadcastsRequest;->languages:[Ljava/lang/String;

    .line 1604
    const-string/jumbo v1, "e_use_personal"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Ltv/periscope/android/api/RankedBroadcastsRequest;->usePersonal:Z

    .line 1605
    const-string/jumbo v1, "e_use_ml"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Ltv/periscope/android/api/RankedBroadcastsRequest;->useML:Z

    .line 1606
    return-object v0
.end method

.method private getUserPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2102
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2103
    new-instance v3, Ltv/periscope/android/api/GetUserRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/GetUserRequest;-><init>()V

    .line 2104
    const-string/jumbo v0, "e_user_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2105
    const-string/jumbo v0, "e_user_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/GetUserRequest;->userId:Ljava/lang/String;

    .line 2110
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/PublicApiService;->getUserPublic(Ltv/periscope/android/api/GetUserRequest;)Ltv/periscope/android/api/GetUserResponse;

    move-result-object v4

    .line 2111
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->g:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2113
    :goto_1
    return-object v0

    .line 2106
    :cond_1
    const-string/jumbo v0, "e_username"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107
    const-string/jumbo v0, "e_username"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/GetUserRequest;->username:Ljava/lang/String;

    goto :goto_0

    .line 2112
    :catch_0
    move-exception v4

    .line 2113
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->g:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_1
.end method

.method private getUsers(Ltv/periscope/android/api/GetUsersRequest;Ljava/lang/String;Ljava/util/List;)Ltv/periscope/android/api/GetUsersResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/android/api/GetUsersRequest;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ltv/periscope/android/api/GetUsersResponse;"
        }
    .end annotation

    .prologue
    .line 2326
    iput-object p2, p1, Ltv/periscope/android/api/GetUsersRequest;->cookie:Ljava/lang/String;

    .line 2327
    iput-object p3, p1, Ltv/periscope/android/api/GetUsersRequest;->userIds:Ljava/util/List;

    .line 2328
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiService;->getUsers(Ltv/periscope/android/api/GetUsersRequest;)Ltv/periscope/android/api/GetUsersResponse;

    move-result-object v0

    return-object v0
.end method

.method private getUsers(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2314
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2315
    new-instance v3, Ltv/periscope/android/api/GetUsersRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/GetUsersRequest;-><init>()V

    .line 2317
    :try_start_0
    const-string/jumbo v0, "e_cookie"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "e_user_ids"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v3, v0, v1}, Ltv/periscope/android/api/ApiRunnable;->getUsers(Ltv/periscope/android/api/GetUsersRequest;Ljava/lang/String;Ljava/util/List;)Ltv/periscope/android/api/GetUsersResponse;

    move-result-object v4

    .line 2318
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->h:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2320
    :goto_0
    return-object v0

    .line 2319
    :catch_0
    move-exception v4

    .line 2320
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->h:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private hello(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1720
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1721
    new-instance v3, Ltv/periscope/android/api/HelloRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/HelloRequest;-><init>()V

    .line 1722
    const-string/jumbo v0, "e_cookie"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/HelloRequest;->cookie:Ljava/lang/String;

    .line 1723
    const-string/jumbo v0, "e_locale"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/HelloRequest;->locale:Ljava/util/List;

    .line 1725
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->hello(Ltv/periscope/android/api/HelloRequest;)Ltv/periscope/android/api/HelloResponse;

    move-result-object v4

    .line 1726
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->f:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1728
    :goto_0
    return-object v0

    .line 1727
    :catch_0
    move-exception v4

    .line 1728
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->f:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private markAbusePublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2239
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2240
    new-instance v3, Ltv/periscope/android/api/MarkAbusePublicRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/MarkAbusePublicRequest;-><init>()V

    .line 2241
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/MarkAbusePublicRequest;->broadcastId:Ljava/lang/String;

    .line 2242
    const-string/jumbo v0, "e_abuse_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/MarkAbusePublicRequest;->abuseType:Ljava/lang/String;

    .line 2243
    const-string/jumbo v0, "e_install_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/MarkAbusePublicRequest;->installId:Ljava/lang/String;

    .line 2245
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/PublicApiService;->markAbusePublic(Ltv/periscope/android/api/MarkAbusePublicRequest;)Ltv/periscope/android/api/MarkAbuseResponse;

    move-result-object v4

    .line 2246
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->N:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2248
    :goto_0
    return-object v0

    .line 2247
    :catch_0
    move-exception v4

    .line 2248
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->N:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private newLogFileForUpload([Ljava/io/File;)Lretrofit/mime/TypedInput;
    .locals 3

    .prologue
    .line 1890
    if-eqz p1, :cond_0

    .line 1891
    new-instance v0, Ltv/periscope/android/api/TypedFiles;

    const-string/jumbo v1, "logs.txt"

    invoke-direct {v0, p1, v1}, Ltv/periscope/android/api/TypedFiles;-><init>([Ljava/io/File;Ljava/lang/String;)V

    .line 1893
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ltv/periscope/android/api/TypedFileString;

    const-string/jumbo v1, ""

    const-string/jumbo v2, "logs.txt"

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/api/TypedFileString;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private pingBroadcast(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1829
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1830
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1831
    const-string/jumbo v1, "e_bit_rate"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1832
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1834
    :try_start_0
    const-string/jumbo v3, "PsApi"

    const-string/jumbo v4, "pinging broadcast"

    invoke-static {v3, v4}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1835
    iget-object v3, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v3, v2, v0, v1}, Ltv/periscope/android/api/ApiService;->pingBroadcast(Ljava/lang/String;Ljava/lang/String;I)Ltv/periscope/android/api/PingBroadcastResponse;

    move-result-object v4

    .line 1836
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "pingBroadcast succeeded"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1837
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->V:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1840
    :goto_0
    return-object v0

    .line 1838
    :catch_0
    move-exception v4

    .line 1839
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "pingBroadcast failed"

    invoke-static {v0, v1, v4}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1840
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->V:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object v3, v6

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private pingWatching(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 8

    .prologue
    .line 1779
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 1780
    const-string/jumbo v0, "e_session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1781
    const-string/jumbo v0, "e_logger_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1782
    const-string/jumbo v0, "e_cookie"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1783
    const-string/jumbo v0, "e_num_hearts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 1784
    const-string/jumbo v0, "e_n_comments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 1785
    new-instance v0, Ltv/periscope/android/api/ApiRunnable$2;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/api/ApiRunnable$2;-><init>(Ltv/periscope/android/api/ApiRunnable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1800
    invoke-virtual {v0, v7}, Ltv/periscope/android/api/ApiRequestWithLogs;->execute(Ljava/lang/String;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v0

    return-object v0
.end method

.method private pingWatchingPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2190
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2191
    new-instance v3, Ltv/periscope/android/api/PingPublicRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/PingPublicRequest;-><init>()V

    .line 2192
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/PingPublicRequest;->broadcastId:Ljava/lang/String;

    .line 2193
    const-string/jumbo v0, "e_session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/PingPublicRequest;->session:Ljava/lang/String;

    .line 2194
    const-string/jumbo v0, "e_install_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/PingPublicRequest;->installId:Ljava/lang/String;

    .line 2196
    :try_start_0
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "ping watching"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/PublicApiService;->pingPublic(Ltv/periscope/android/api/PingPublicRequest;)Ltv/periscope/android/api/PingWatchingResponse;

    move-result-object v4

    .line 2198
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "pingWatching succeeded"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2199
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->P:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2202
    :goto_0
    return-object v0

    .line 2200
    :catch_0
    move-exception v4

    .line 2201
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "pingWatching failed"

    invoke-static {v0, v1, v4}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2202
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->P:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private replayThumbnailPlaylist(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1898
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1899
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1900
    new-instance v3, Ltv/periscope/android/api/ThumbnailPlaylistRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/ThumbnailPlaylistRequest;-><init>()V

    .line 1901
    const-string/jumbo v1, "e_cookie"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Ltv/periscope/android/api/ThumbnailPlaylistRequest;->cookie:Ljava/lang/String;

    .line 1902
    iput-object v0, v3, Ltv/periscope/android/api/ThumbnailPlaylistRequest;->broadcastId:Ljava/lang/String;

    .line 1904
    :try_start_0
    iget-object v1, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v1, v3}, Ltv/periscope/android/api/ApiService;->replayThumbnailPlayList(Ltv/periscope/android/api/ThumbnailPlaylistRequest;)Ltv/periscope/android/api/ThumbnailPlaylistResponse;

    move-result-object v4

    .line 1905
    iput-object v0, v4, Ltv/periscope/android/api/ThumbnailPlaylistResponse;->broadcastId:Ljava/lang/String;

    .line 1906
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->I:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1908
    :goto_0
    return-object v0

    .line 1907
    :catch_0
    move-exception v4

    .line 1908
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->I:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private replayThumbnailPlaylistPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2224
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2225
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2226
    new-instance v3, Ltv/periscope/android/api/ThumbnailPlaylistPublicRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/ThumbnailPlaylistPublicRequest;-><init>()V

    .line 2227
    iput-object v0, v3, Ltv/periscope/android/api/ThumbnailPlaylistPublicRequest;->broadcastId:Ljava/lang/String;

    .line 2228
    const-string/jumbo v1, "e_install_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Ltv/periscope/android/api/ThumbnailPlaylistPublicRequest;->installId:Ljava/lang/String;

    .line 2230
    :try_start_0
    iget-object v1, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v1, v3}, Ltv/periscope/android/api/PublicApiService;->replayThumbnailPlaylistPublic(Ltv/periscope/android/api/ThumbnailPlaylistPublicRequest;)Ltv/periscope/android/api/ThumbnailPlaylistResponse;

    move-result-object v4

    .line 2231
    iput-object v0, v4, Ltv/periscope/android/api/ThumbnailPlaylistResponse;->broadcastId:Ljava/lang/String;

    .line 2232
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->I:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2234
    :goto_0
    return-object v0

    .line 2233
    :catch_0
    move-exception v4

    .line 2234
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->I:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private reportBroadcast(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1945
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1946
    new-instance v3, Ltv/periscope/android/api/MarkAbuseRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/MarkAbuseRequest;-><init>()V

    .line 1947
    const-string/jumbo v0, "e_cookie"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/MarkAbuseRequest;->cookie:Ljava/lang/String;

    .line 1948
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/MarkAbuseRequest;->broadcastId:Ljava/lang/String;

    .line 1949
    const-string/jumbo v0, "e_abuse_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/MarkAbuseRequest;->abuseType:Ljava/lang/String;

    .line 1951
    :try_start_0
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "reporting broadcast"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1952
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->markAbuse(Ltv/periscope/android/api/MarkAbuseRequest;)Ltv/periscope/android/api/MarkAbuseResponse;

    move-result-object v4

    .line 1953
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "report Broadcast succeeded"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1954
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->N:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1957
    :goto_0
    return-object v0

    .line 1955
    :catch_0
    move-exception v4

    .line 1956
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "report Broadcast failed"

    invoke-static {v0, v1, v4}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1957
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->N:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private reportComment(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 14

    .prologue
    .line 2269
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2270
    const-string/jumbo v0, "e_service_auth_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2271
    const-string/jumbo v1, "tv.periscope.android.api.service.safety.EXTRA_REPORT_COMMENT_REQUEST"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-static {v1}, Lorg/parceler/c;->a(Landroid/os/Parcelable;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ltv/periscope/android/api/service/safety/ReportCommentRequest;

    .line 2273
    :try_start_0
    invoke-direct {p0, v0}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 2274
    iget-object v1, p0, Ltv/periscope/android/api/ApiRunnable;->mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

    invoke-interface {v1, v0, v3}, Ltv/periscope/android/api/service/safety/SafetyService;->report(Ljava/lang/String;Ltv/periscope/android/api/service/safety/ReportCommentRequest;)Ltv/periscope/android/api/service/safety/ReportCommentResponse;

    move-result-object v4

    .line 2275
    new-instance v0, Ltv/periscope/android/event/ServiceEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->ao:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2277
    :goto_0
    return-object v0

    .line 2276
    :catch_0
    move-exception v11

    .line 2277
    new-instance v6, Ltv/periscope/android/event/ServiceEvent;

    sget-object v7, Ltv/periscope/android/event/ApiEvent$Type;->ao:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v8, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v10, Ltv/periscope/android/api/BackendServiceName;->SAFETY:Ltv/periscope/android/api/BackendServiceName;

    move-object v9, v3

    move v12, v5

    move-object v13, p0

    invoke-direct/range {v6 .. v13}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v0, v6

    goto :goto_0
.end method

.method private retweetBroadcast(Z)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2047
    iget-object v3, p0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    check-cast v3, Ltv/periscope/android/api/TweetBroadcastRequest;

    .line 2049
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->retweetBroadcast(Ltv/periscope/android/api/TweetBroadcastRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v4

    .line 2050
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aH:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2052
    :goto_0
    return-object v0

    .line 2051
    :catch_0
    move-exception v4

    .line 2052
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aH:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private setExternalEncoderName(Z)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2366
    iget-object v3, p0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    check-cast v3, Ltv/periscope/android/api/SetExternalEncoderNameRequest;

    .line 2368
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->setExternalEncoderName(Ltv/periscope/android/api/SetExternalEncoderNameRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v4

    .line 2369
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aL:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2371
    :goto_0
    return-object v0

    .line 2370
    :catch_0
    move-exception v4

    .line 2371
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aL:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private shareBroadcast(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1928
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1929
    new-instance v3, Ltv/periscope/android/api/ShareBroadcastRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/ShareBroadcastRequest;-><init>()V

    .line 1930
    const-string/jumbo v0, "e_cookie"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/ShareBroadcastRequest;->cookie:Ljava/lang/String;

    .line 1931
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/ShareBroadcastRequest;->broadcastId:Ljava/lang/String;

    .line 1932
    const-string/jumbo v0, "e_user_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v3, Ltv/periscope/android/api/ShareBroadcastRequest;->userIds:Ljava/util/ArrayList;

    .line 1934
    :try_start_0
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "sharing broadcast"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1935
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->shareBroadcast(Ltv/periscope/android/api/ShareBroadcastRequest;)Ltv/periscope/android/api/ShareBroadcastResponse;

    move-result-object v4

    .line 1936
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "shareBroadcast succeeded"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1937
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->M:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1940
    :goto_0
    return-object v0

    .line 1938
    :catch_0
    move-exception v4

    .line 1939
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "shareBroadcast failed"

    invoke-static {v0, v1, v4}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1940
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->M:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private startWatching(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1670
    const-string/jumbo v0, "e_autoplay"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1671
    const-string/jumbo v1, "e_background"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1672
    const-string/jumbo v1, "e_token"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1673
    new-instance v3, Ltv/periscope/android/api/StartWatchingRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/StartWatchingRequest;-><init>()V

    .line 1674
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Ltv/periscope/android/api/StartWatchingRequest;->cookie:Ljava/lang/String;

    .line 1675
    iput-object v1, v3, Ltv/periscope/android/api/StartWatchingRequest;->lifeCycleToken:Ljava/lang/String;

    .line 1676
    iput-boolean v0, v3, Ltv/periscope/android/api/StartWatchingRequest;->autoplay:Z

    .line 1678
    :try_start_0
    const-string/jumbo v0, "PsApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start watching for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1679
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->startWatching(Ltv/periscope/android/api/StartWatchingRequest;)Ltv/periscope/android/api/StartWatchingResponse;

    move-result-object v4

    .line 1680
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "startWatching succeeded"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1681
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->v:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1684
    :goto_0
    return-object v0

    .line 1682
    :catch_0
    move-exception v4

    .line 1683
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "startWatching failed"

    invoke-static {v0, v1, v4}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1684
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->v:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private startWatchingPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2170
    const-string/jumbo v0, "e_autoplay"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 2171
    const-string/jumbo v1, "e_background"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2172
    new-instance v3, Ltv/periscope/android/api/StartWatchingPublicRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/StartWatchingPublicRequest;-><init>()V

    .line 2173
    const-string/jumbo v1, "e_token"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2174
    const-string/jumbo v2, "e_install_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Ltv/periscope/android/api/StartWatchingPublicRequest;->installId:Ljava/lang/String;

    .line 2175
    iput-object v1, v3, Ltv/periscope/android/api/StartWatchingPublicRequest;->lifeCycleToken:Ljava/lang/String;

    .line 2176
    iput-boolean v0, v3, Ltv/periscope/android/api/StartWatchingPublicRequest;->autoplay:Z

    .line 2178
    :try_start_0
    const-string/jumbo v0, "PsApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start watching for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2179
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/PublicApiService;->startWatchingPublic(Ltv/periscope/android/api/StartWatchingPublicRequest;)Ltv/periscope/android/api/StartWatchingResponse;

    move-result-object v4

    .line 2180
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "startWatchingPublic succeeded"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2181
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->v:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2184
    :goto_0
    return-object v0

    .line 2182
    :catch_0
    move-exception v4

    .line 2183
    const-string/jumbo v0, "PsApi"

    const-string/jumbo v1, "startWatchingPublic failed"

    invoke-static {v0, v1, v4}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2184
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->v:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private tweetBroadcastPublished(Z)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 2036
    iget-object v3, p0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    check-cast v3, Ltv/periscope/android/api/TweetBroadcastRequest;

    .line 2038
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->tweetBroadcastPublished(Ltv/periscope/android/api/TweetBroadcastRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v4

    .line 2039
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aG:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2041
    :goto_0
    return-object v0

    .line 2040
    :catch_0
    move-exception v4

    .line 2041
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->aG:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private unblock(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1987
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1988
    const-string/jumbo v0, "e_user_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1989
    new-instance v3, Ltv/periscope/android/api/BlockRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/BlockRequest;-><init>()V

    .line 1990
    const-string/jumbo v1, "e_cookie"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Ltv/periscope/android/api/BlockRequest;->cookie:Ljava/lang/String;

    .line 1991
    iput-object v0, v3, Ltv/periscope/android/api/BlockRequest;->userId:Ljava/lang/String;

    .line 1993
    :try_start_0
    iget-object v1, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v1, v3}, Ltv/periscope/android/api/ApiService;->unblock(Ltv/periscope/android/api/BlockRequest;)Ltv/periscope/android/api/BlockResponse;

    move-result-object v4

    .line 1994
    iput-object v0, v4, Ltv/periscope/android/api/BlockResponse;->userId:Ljava/lang/String;

    .line 1995
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->Y:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1997
    :goto_0
    return-object v0

    .line 1996
    :catch_0
    move-exception v4

    .line 1997
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->Y:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private updateProfileDescription(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1765
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1766
    const-string/jumbo v0, "e_description"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1767
    new-instance v3, Ltv/periscope/android/api/UpdateDescriptionRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/UpdateDescriptionRequest;-><init>()V

    .line 1768
    const-string/jumbo v1, "e_cookie"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Ltv/periscope/android/api/UpdateDescriptionRequest;->cookie:Ljava/lang/String;

    .line 1769
    iput-object v0, v3, Ltv/periscope/android/api/UpdateDescriptionRequest;->description:Ljava/lang/String;

    .line 1771
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->updateDescription(Ltv/periscope/android/api/UpdateDescriptionRequest;)Ltv/periscope/android/api/UpdateDescriptionResponse;

    move-result-object v4

    .line 1772
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->ah:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1774
    :goto_0
    return-object v0

    .line 1773
    :catch_0
    move-exception v4

    .line 1774
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->ah:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private updateProfileDisplayName(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 6

    .prologue
    .line 1751
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1752
    const-string/jumbo v0, "e_display_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1753
    new-instance v3, Ltv/periscope/android/api/UpdateDisplayNameRequest;

    invoke-direct {v3}, Ltv/periscope/android/api/UpdateDisplayNameRequest;-><init>()V

    .line 1754
    const-string/jumbo v1, "e_cookie"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Ltv/periscope/android/api/UpdateDisplayNameRequest;->cookie:Ljava/lang/String;

    .line 1755
    iput-object v0, v3, Ltv/periscope/android/api/UpdateDisplayNameRequest;->displayName:Ljava/lang/String;

    .line 1757
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiService;->updateDisplayName(Ltv/periscope/android/api/UpdateDisplayNameRequest;)Ltv/periscope/android/api/UpdateDisplayNameResponse;

    move-result-object v4

    .line 1758
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->ag:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1760
    :goto_0
    return-object v0

    .line 1759
    :catch_0
    move-exception v4

    .line 1760
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->ag:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0
.end method

.method private uploadBroadcasterLogs(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 5

    .prologue
    .line 1845
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1846
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1847
    const-string/jumbo v2, "e_logger_name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1848
    const-string/jumbo v3, "e_cookie"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1849
    new-instance v4, Ltv/periscope/android/api/ApiRunnable$4;

    invoke-direct {v4, p0, v3, v1, v0}, Ltv/periscope/android/api/ApiRunnable$4;-><init>(Ltv/periscope/android/api/ApiRunnable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1863
    invoke-virtual {v4, v2}, Ltv/periscope/android/api/ApiRequestWithLogs;->execute(Ljava/lang/String;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v0

    return-object v0
.end method

.method private uploadToast(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1690
    const-string/jumbo v1, "e_background"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1693
    :try_start_0
    const-string/jumbo v1, "e_file_dir"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1694
    new-instance v2, Ljava/io/File;

    const-string/jumbo v3, "padding.padding"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1695
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1696
    new-instance v6, Ljava/io/OutputStreamWriter;

    invoke-direct {v6, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1697
    const v0, 0x3d090

    :try_start_1
    new-array v0, v0, [C

    .line 1698
    invoke-virtual {v6, v0}, Ljava/io/OutputStreamWriter;->write([C)V

    .line 1699
    new-instance v0, Lretrofit/mime/TypedFile;

    const-string/jumbo v1, "multipart/form-data"

    invoke-direct {v0, v1, v2}, Lretrofit/mime/TypedFile;-><init>(Ljava/lang/String;Ljava/io/File;)V

    .line 1700
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1702
    const-string/jumbo v1, "e_cookie"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 1704
    :try_start_2
    iget-object v4, p0, Ltv/periscope/android/api/ApiRunnable;->mSigner:Ltv/periscope/android/signer/SignerService;

    invoke-interface {v4, v1, v0}, Ltv/periscope/android/signer/SignerService;->uploadPadding(Ljava/lang/String;Lretrofit/mime/TypedFile;)Ltv/periscope/android/api/UploadTestResponse;

    move-result-object v4

    .line 1705
    invoke-virtual {v6}, Ljava/io/OutputStreamWriter;->flush()V

    .line 1706
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v2

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 1707
    const v1, 0x48742400    # 250000.0f

    div-float v0, v1, v0

    iput v0, v4, Ltv/periscope/android/api/UploadTestResponse;->byteRateSeconds:F

    .line 1708
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->ab:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_2
    .catch Lretrofit/RetrofitError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1715
    invoke-static {v6}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 1713
    :goto_0
    return-object v0

    .line 1709
    :catch_0
    move-exception v4

    .line 1710
    :try_start_3
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->ab:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1715
    invoke-static {v6}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 1712
    :catch_1
    move-exception v4

    move-object v6, v0

    .line 1713
    :goto_1
    :try_start_4
    new-instance v0, Ltv/periscope/android/event/ApiEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->ab:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1715
    invoke-static {v6}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    :goto_2
    invoke-static {v6}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1712
    :catch_2
    move-exception v4

    goto :goto_1
.end method

.method private verifyAuthToken(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lretrofit/RetrofitError;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2394
    invoke-static {p1}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2395
    new-instance v0, Lretrofit/client/Response;

    const-string/jumbo v1, ""

    const/16 v2, 0x191

    const-string/jumbo v3, "auth token is empty"

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct/range {v0 .. v5}, Lretrofit/client/Response;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Lretrofit/mime/TypedInput;)V

    .line 2397
    invoke-static {v5, v0, v5, v5}, Lretrofit/RetrofitError;->httpError(Ljava/lang/String;Lretrofit/client/Response;Lretrofit/converter/Converter;Ljava/lang/reflect/Type;)Lretrofit/RetrofitError;

    move-result-object v0

    throw v0

    .line 2399
    :cond_0
    return-void
.end method

.method private vote(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;
    .locals 14

    .prologue
    .line 2284
    const-string/jumbo v0, "e_background"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 2285
    const-string/jumbo v0, "e_service_auth_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2286
    const-string/jumbo v1, "tv.periscope.android.api.service.safety.EXTRA_VOTE_REQUEST"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-static {v1}, Lorg/parceler/c;->a(Landroid/os/Parcelable;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ltv/periscope/android/api/service/safety/VoteRequest;

    .line 2288
    :try_start_0
    invoke-direct {p0, v0}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 2289
    iget-object v1, p0, Ltv/periscope/android/api/ApiRunnable;->mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

    invoke-interface {v1, v0, v3}, Ltv/periscope/android/api/service/safety/SafetyService;->vote(Ljava/lang/String;Ltv/periscope/android/api/service/safety/VoteRequest;)Ltv/periscope/android/api/service/safety/VoteResponse;

    move-result-object v4

    .line 2290
    new-instance v0, Ltv/periscope/android/event/ServiceEvent;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->ap:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v2, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2292
    :goto_0
    return-object v0

    .line 2291
    :catch_0
    move-exception v11

    .line 2292
    new-instance v6, Ltv/periscope/android/event/ServiceEvent;

    sget-object v7, Ltv/periscope/android/event/ApiEvent$Type;->ap:Ltv/periscope/android/event/ApiEvent$Type;

    iget-object v8, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v10, Ltv/periscope/android/api/BackendServiceName;->SAFETY:Ltv/periscope/android/api/BackendServiceName;

    move-object v9, v3

    move v12, v5

    move-object v13, p0

    invoke-direct/range {v6 .. v13}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v0, v6

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic canRetry(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 77
    check-cast p1, Ltv/periscope/android/event/ApiEvent;

    invoke-virtual {p0, p1}, Ltv/periscope/android/api/ApiRunnable;->canRetry(Ltv/periscope/android/event/ApiEvent;)Z

    move-result v0

    return v0
.end method

.method protected canRetry(Ltv/periscope/android/event/ApiEvent;)Z
    .locals 6

    .prologue
    .line 408
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    invoke-static {v0}, Lcwz;->a(Lretrofit/RetrofitError;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 409
    :goto_0
    if-eqz v0, :cond_0

    .line 410
    const-string/jumbo v1, "PsApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "A problem was detected for action code "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Ltv/periscope/android/api/ApiRunnable;->mActionCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ". Retrying. Num retries left: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 411
    invoke-virtual {p0}, Ltv/periscope/android/api/ApiRunnable;->numRetries()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ". Current backoff: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ltv/periscope/android/api/ApiRunnable;->currentBackoff()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 410
    invoke-static {v1, v2}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    :cond_0
    return v0

    .line 408
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public convert(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/SuperfanJsonModel;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2015
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2016
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/SuperfanJsonModel;

    .line 2017
    invoke-virtual {v0}, Ltv/periscope/android/api/SuperfanJsonModel;->create()Ltv/periscope/model/ac$a;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/ac$a;->a()Ltv/periscope/model/ac;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2019
    :cond_0
    return-object v1
.end method

.method protected bridge synthetic execute()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Ltv/periscope/android/api/ApiRunnable;->execute()Ltv/periscope/android/event/ApiEvent;

    move-result-object v0

    return-object v0
.end method

.method protected execute()Ltv/periscope/android/event/ApiEvent;
    .locals 16

    .prologue
    const-wide/16 v12, 0x0

    const/16 v9, 0x64

    const/4 v3, 0x0

    const/4 v14, 0x0

    .line 446
    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mBundle:Landroid/os/Bundle;

    .line 447
    move-object/from16 v0, p0

    iget-object v8, v0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    .line 448
    const-string/jumbo v2, "e_background"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    .line 449
    move-object/from16 v0, p0

    iget v2, v0, Ltv/periscope/android/api/ApiRunnable;->mActionCode:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v2, v14

    .line 1568
    :goto_0
    return-object v2

    .line 451
    :pswitch_1
    const-string/jumbo v2, "e_secret_key"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 452
    const-string/jumbo v3, "e_secret_token"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 453
    const-string/jumbo v5, "e_username"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 454
    const-string/jumbo v5, "e_user_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 455
    const-string/jumbo v5, "e_phone_number"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 456
    const-string/jumbo v5, "e_install_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 457
    const-string/jumbo v5, "e_session_type"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 458
    new-instance v5, Ltv/periscope/android/api/TwitterLoginRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/TwitterLoginRequest;-><init>()V

    .line 459
    iput-object v3, v5, Ltv/periscope/android/api/TwitterLoginRequest;->sessionKey:Ljava/lang/String;

    .line 460
    iput-object v2, v5, Ltv/periscope/android/api/TwitterLoginRequest;->sessionSecret:Ljava/lang/String;

    .line 461
    iput-object v6, v5, Ltv/periscope/android/api/TwitterLoginRequest;->userName:Ljava/lang/String;

    .line 462
    iput-object v8, v5, Ltv/periscope/android/api/TwitterLoginRequest;->userId:Ljava/lang/String;

    .line 463
    iput-object v9, v5, Ltv/periscope/android/api/TwitterLoginRequest;->phoneNumber:Ljava/lang/String;

    .line 464
    iput-object v10, v5, Ltv/periscope/android/api/TwitterLoginRequest;->installId:Ljava/lang/String;

    .line 465
    const-string/jumbo v2, "e_time_zone"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/TwitterLoginRequest;->timeZone:Ljava/lang/String;

    .line 467
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->login(Ltv/periscope/android/api/TwitterLoginRequest;)Ltv/periscope/android/api/TwitterLoginResponse;

    move-result-object v6

    .line 468
    invoke-static {v11}, Ltv/periscope/android/session/Session$Type;->valueOf(Ljava/lang/String;)Ltv/periscope/android/session/Session$Type;

    move-result-object v2

    iput-object v2, v6, Ltv/periscope/android/api/TwitterLoginResponse;->sessionType:Ltv/periscope/android/session/Session$Type;

    .line 469
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->a:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 470
    :catch_0
    move-exception v6

    .line 471
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->a:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto :goto_0

    .line 475
    :pswitch_2
    new-instance v5, Ltv/periscope/android/api/TwitterTokenLoginRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/TwitterTokenLoginRequest;-><init>()V

    .line 476
    const-string/jumbo v2, "e_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/TwitterTokenLoginRequest;->jwt:Ljava/lang/String;

    .line 477
    const-string/jumbo v2, "e_install_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/TwitterTokenLoginRequest;->installId:Ljava/lang/String;

    .line 478
    const-string/jumbo v2, "e_create_user"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v5, Ltv/periscope/android/api/TwitterTokenLoginRequest;->createUser:Z

    .line 479
    const-string/jumbo v2, "e_time_zone"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/TwitterTokenLoginRequest;->timeZone:Ljava/lang/String;

    .line 481
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->loginTwitterToken(Ltv/periscope/android/api/TwitterTokenLoginRequest;)Ltv/periscope/android/api/TwitterTokenLoginResponse;

    move-result-object v6

    .line 482
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->b:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_1
    .catch Lretrofit/RetrofitError; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 483
    :catch_1
    move-exception v6

    .line 484
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->b:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 488
    :pswitch_3
    new-instance v5, Ltv/periscope/android/api/AuthorizeTokenRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/AuthorizeTokenRequest;-><init>()V

    .line 489
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/AuthorizeTokenRequest;->cookie:Ljava/lang/String;

    .line 490
    const-string/jumbo v2, "e_service_name"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/AuthorizeTokenRequest;->service:Ljava/lang/String;

    .line 492
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getAuthorizationTokenForService(Ltv/periscope/android/api/AuthorizeTokenRequest;)Ltv/periscope/android/api/AuthorizeTokenResponse;

    move-result-object v6

    .line 493
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->c:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_2
    .catch Lretrofit/RetrofitError; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 494
    :catch_2
    move-exception v6

    .line 495
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->c:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 499
    :pswitch_4
    new-instance v5, Ltv/periscope/android/api/ValidateUsernameRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/ValidateUsernameRequest;-><init>()V

    .line 500
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/ValidateUsernameRequest;->cookie:Ljava/lang/String;

    .line 501
    const-string/jumbo v2, "e_username"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/ValidateUsernameRequest;->username:Ljava/lang/String;

    .line 502
    const-string/jumbo v2, "e_secret_key"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/ValidateUsernameRequest;->sessionKey:Ljava/lang/String;

    .line 503
    const-string/jumbo v2, "e_secret_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/ValidateUsernameRequest;->sessionSecret:Ljava/lang/String;

    .line 505
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    .line 506
    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->validateUsername(Ltv/periscope/android/api/ValidateUsernameRequest;)Ltv/periscope/android/api/ValidateUsernameResponse;

    move-result-object v6

    .line 507
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->d:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_3
    .catch Lretrofit/RetrofitError; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 509
    :catch_3
    move-exception v13

    .line 511
    :try_start_4
    new-instance v8, Ltv/periscope/android/event/ApiEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->d:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const-class v2, Ltv/periscope/android/api/ValidateUsernameError;

    .line 512
    invoke-virtual {v13, v2}, Lretrofit/RetrofitError;->getBodyAs(Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v12

    move-object v11, v5

    move v14, v7

    invoke-direct/range {v8 .. v14}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;Z)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4

    move-object v2, v8

    .line 511
    goto/16 :goto_0

    .line 513
    :catch_4
    move-exception v2

    .line 514
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->d:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object v6, v13

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 519
    :pswitch_5
    new-instance v5, Ltv/periscope/android/api/AssociateDigitsAccountRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/AssociateDigitsAccountRequest;-><init>()V

    .line 520
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/AssociateDigitsAccountRequest;->cookie:Ljava/lang/String;

    .line 521
    const-string/jumbo v2, "e_secret_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/AssociateDigitsAccountRequest;->sessionKey:Ljava/lang/String;

    .line 522
    const-string/jumbo v2, "e_secret_key"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/AssociateDigitsAccountRequest;->sessionSecret:Ljava/lang/String;

    .line 524
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    .line 525
    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->associateDigitsAccount(Ltv/periscope/android/api/AssociateDigitsAccountRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v6

    .line 526
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->au:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_5
    .catch Lretrofit/RetrofitError; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    .line 528
    :catch_5
    move-exception v6

    .line 529
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->au:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 533
    :pswitch_6
    new-instance v5, Ltv/periscope/android/api/VerifyUsernameRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/VerifyUsernameRequest;-><init>()V

    .line 534
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/VerifyUsernameRequest;->cookie:Ljava/lang/String;

    .line 535
    const-string/jumbo v2, "e_username"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/VerifyUsernameRequest;->username:Ljava/lang/String;

    .line 536
    const-string/jumbo v2, "e_display_name"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/VerifyUsernameRequest;->displayName:Ljava/lang/String;

    .line 537
    const-string/jumbo v2, "e_secret_key"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/VerifyUsernameRequest;->sessionKey:Ljava/lang/String;

    .line 538
    const-string/jumbo v2, "e_secret_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/VerifyUsernameRequest;->sessionSecret:Ljava/lang/String;

    .line 540
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->verifyUsername(Ltv/periscope/android/api/VerifyUsernameRequest;)Ltv/periscope/android/api/VerifyUsernameResponse;

    move-result-object v6

    .line 541
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->e:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_6
    .catch Lretrofit/RetrofitError; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    .line 543
    :catch_6
    move-exception v13

    .line 545
    :try_start_7
    new-instance v8, Ltv/periscope/android/event/ApiEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->e:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const-class v2, Ltv/periscope/android/api/ValidateUsernameError;

    .line 546
    invoke-virtual {v13, v2}, Lretrofit/RetrofitError;->getBodyAs(Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v12

    move-object v11, v5

    move v14, v7

    invoke-direct/range {v8 .. v14}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;Z)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_7

    move-object v2, v8

    .line 545
    goto/16 :goto_0

    .line 547
    :catch_7
    move-exception v2

    .line 548
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->e:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object v6, v13

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 553
    :pswitch_7
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->hello(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 556
    :pswitch_8
    new-instance v5, Ltv/periscope/android/api/GetUserRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/GetUserRequest;-><init>()V

    .line 557
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/GetUserRequest;->cookie:Ljava/lang/String;

    .line 558
    const-string/jumbo v2, "e_my_user_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/GetUserRequest;->userId:Ljava/lang/String;

    .line 560
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getUser(Ltv/periscope/android/api/GetUserRequest;)Ltv/periscope/android/api/GetUserResponse;

    move-result-object v6

    .line 561
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->g:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_8
    .catch Lretrofit/RetrofitError; {:try_start_8 .. :try_end_8} :catch_8

    goto/16 :goto_0

    .line 562
    :catch_8
    move-exception v6

    .line 563
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->g:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 567
    :pswitch_9
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getUsers(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 571
    :pswitch_a
    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    check-cast v2, Ltv/periscope/android/api/SuperfansRequest;

    .line 572
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v3, v2}, Ltv/periscope/android/api/ApiService;->fetchSuperfans(Ltv/periscope/android/api/SuperfansRequest;)Ltv/periscope/android/api/SuperfansResponse;

    move-result-object v3

    .line 573
    new-instance v6, Ltv/periscope/model/ad;

    iget-object v2, v2, Ltv/periscope/android/api/SuperfansRequest;->userId:Ljava/lang/String;

    invoke-direct {v6, v2}, Ltv/periscope/model/ad;-><init>(Ljava/lang/String;)V

    .line 574
    iget-object v2, v3, Ltv/periscope/android/api/SuperfansResponse;->mySuperfans:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 575
    iget-object v2, v3, Ltv/periscope/android/api/SuperfansResponse;->mySuperfans:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v6, v2}, Ltv/periscope/model/ad;->a(Ljava/util/List;)V

    .line 577
    :cond_1
    iget-object v2, v3, Ltv/periscope/android/api/SuperfansResponse;->superfanOf:Ljava/util/List;

    if-eqz v2, :cond_2

    .line 578
    iget-object v2, v3, Ltv/periscope/android/api/SuperfansResponse;->superfanOf:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v6, v2}, Ltv/periscope/model/ad;->b(Ljava/util/List;)V

    .line 580
    :cond_2
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aJ:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_9
    .catch Lretrofit/RetrofitError; {:try_start_9 .. :try_end_9} :catch_9

    goto/16 :goto_0

    .line 581
    :catch_9
    move-exception v6

    .line 582
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aJ:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 586
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    check-cast v5, Ltv/periscope/android/api/GetUserStatsRequest;

    .line 588
    :try_start_a
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getUserStats(Ltv/periscope/android/api/GetUserStatsRequest;)Ltv/periscope/android/api/GetUserStatsResponse;

    move-result-object v6

    .line 589
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->i:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    iget-object v8, v5, Ltv/periscope/android/api/GetUserStatsRequest;->userId:Ljava/lang/String;

    iget-boolean v6, v6, Ltv/periscope/android/api/GetUserStatsResponse;->lowBroadcastCount:Z

    .line 590
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v8, v6}, Ltv/periscope/model/user/f;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ltv/periscope/model/user/f;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_a
    .catch Lretrofit/RetrofitError; {:try_start_a .. :try_end_a} :catch_a

    goto/16 :goto_0

    .line 591
    :catch_a
    move-exception v6

    .line 592
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->i:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 596
    :pswitch_c
    const-string/jumbo v2, "e_my_user_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 597
    new-instance v5, Ltv/periscope/android/api/GetFollowersRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/GetFollowersRequest;-><init>()V

    .line 598
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/GetFollowersRequest;->cookie:Ljava/lang/String;

    .line 599
    iput-object v8, v5, Ltv/periscope/android/api/GetFollowersRequest;->userId:Ljava/lang/String;

    .line 601
    :try_start_b
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getFollowers(Ltv/periscope/android/api/GetFollowersRequest;)Ljava/util/List;

    move-result-object v9

    .line 602
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->j:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    new-instance v6, Ltv/periscope/android/api/FetchUsersResponse;

    invoke-direct {v6, v8, v9}, Ltv/periscope/android/api/FetchUsersResponse;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_b
    .catch Lretrofit/RetrofitError; {:try_start_b .. :try_end_b} :catch_b

    goto/16 :goto_0

    .line 604
    :catch_b
    move-exception v6

    .line 605
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->j:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 609
    :pswitch_d
    const-string/jumbo v2, "e_my_user_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 610
    new-instance v5, Ltv/periscope/android/api/GetFollowingRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/GetFollowingRequest;-><init>()V

    .line 611
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/GetFollowingRequest;->cookie:Ljava/lang/String;

    .line 612
    iput-object v8, v5, Ltv/periscope/android/api/GetFollowingRequest;->userId:Ljava/lang/String;

    .line 614
    :try_start_c
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getFollowing(Ltv/periscope/android/api/GetFollowingRequest;)Ljava/util/List;

    move-result-object v9

    .line 615
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->k:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    new-instance v6, Ltv/periscope/android/api/FetchUsersResponse;

    invoke-direct {v6, v8, v9}, Ltv/periscope/android/api/FetchUsersResponse;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_c
    .catch Lretrofit/RetrofitError; {:try_start_c .. :try_end_c} :catch_c

    goto/16 :goto_0

    .line 617
    :catch_c
    move-exception v6

    .line 618
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->k:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 622
    :pswitch_e
    const-string/jumbo v2, "e_my_user_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 623
    new-instance v5, Ltv/periscope/android/api/GetFollowingRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/GetFollowingRequest;-><init>()V

    .line 624
    const-string/jumbo v3, "e_cookie"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Ltv/periscope/android/api/GetFollowingRequest;->cookie:Ljava/lang/String;

    .line 625
    iput-object v2, v5, Ltv/periscope/android/api/GetFollowingRequest;->userId:Ljava/lang/String;

    .line 626
    const/4 v2, 0x1

    iput-boolean v2, v5, Ltv/periscope/android/api/GetFollowingRequest;->onlyIds:Z

    .line 628
    :try_start_d
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getFollowingIdsOnly(Ltv/periscope/android/api/GetFollowingRequest;)Ljava/util/List;

    move-result-object v6

    .line 629
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->l:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_d
    .catch Lretrofit/RetrofitError; {:try_start_d .. :try_end_d} :catch_d

    goto/16 :goto_0

    .line 631
    :catch_d
    move-exception v6

    .line 632
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->l:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 636
    :pswitch_f
    new-instance v5, Ltv/periscope/android/api/PsRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 637
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    .line 639
    :try_start_e
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getMutualFollows(Ltv/periscope/android/api/PsRequest;)Ljava/util/List;

    move-result-object v6

    .line 640
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->m:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_e
    .catch Lretrofit/RetrofitError; {:try_start_e .. :try_end_e} :catch_e

    goto/16 :goto_0

    .line 641
    :catch_e
    move-exception v6

    .line 642
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->m:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 646
    :pswitch_10
    const-string/jumbo v2, "e_user_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 647
    new-instance v5, Ltv/periscope/android/api/FollowRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/FollowRequest;-><init>()V

    .line 648
    const-string/jumbo v3, "e_cookie"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Ltv/periscope/android/api/FollowRequest;->cookie:Ljava/lang/String;

    .line 649
    iput-object v2, v5, Ltv/periscope/android/api/FollowRequest;->userId:Ljava/lang/String;

    .line 651
    :try_start_f
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v3, v5}, Ltv/periscope/android/api/ApiService;->follow(Ltv/periscope/android/api/FollowRequest;)Ltv/periscope/android/api/FollowResponse;

    move-result-object v6

    .line 652
    iput-object v2, v6, Ltv/periscope/android/api/FollowResponse;->userId:Ljava/lang/String;

    .line 654
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->n:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_f
    .catch Lretrofit/RetrofitError; {:try_start_f .. :try_end_f} :catch_f

    goto/16 :goto_0

    .line 655
    :catch_f
    move-exception v6

    .line 656
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->n:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 660
    :pswitch_11
    const-string/jumbo v2, "e_user_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 661
    new-instance v5, Ltv/periscope/android/api/MuteRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/MuteRequest;-><init>()V

    .line 662
    const-string/jumbo v3, "e_cookie"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Ltv/periscope/android/api/MuteRequest;->cookie:Ljava/lang/String;

    .line 663
    iput-object v2, v5, Ltv/periscope/android/api/MuteRequest;->userId:Ljava/lang/String;

    .line 665
    :try_start_10
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v3, v5}, Ltv/periscope/android/api/ApiService;->mute(Ltv/periscope/android/api/MuteRequest;)Ltv/periscope/android/api/MuteResponse;

    move-result-object v6

    .line 666
    iput-object v2, v6, Ltv/periscope/android/api/MuteResponse;->userId:Ljava/lang/String;

    .line 668
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->o:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_10
    .catch Lretrofit/RetrofitError; {:try_start_10 .. :try_end_10} :catch_10

    goto/16 :goto_0

    .line 669
    :catch_10
    move-exception v6

    .line 670
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->o:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 674
    :pswitch_12
    const-string/jumbo v2, "e_user_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 675
    new-instance v5, Ltv/periscope/android/api/UnMuteRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/UnMuteRequest;-><init>()V

    .line 676
    const-string/jumbo v3, "e_cookie"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Ltv/periscope/android/api/UnMuteRequest;->cookie:Ljava/lang/String;

    .line 677
    iput-object v2, v5, Ltv/periscope/android/api/UnMuteRequest;->userId:Ljava/lang/String;

    .line 679
    :try_start_11
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v3, v5}, Ltv/periscope/android/api/ApiService;->unmute(Ltv/periscope/android/api/UnMuteRequest;)Ltv/periscope/android/api/UnMuteResponse;

    move-result-object v6

    .line 680
    iput-object v2, v6, Ltv/periscope/android/api/UnMuteResponse;->userId:Ljava/lang/String;

    .line 682
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->p:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_11
    .catch Lretrofit/RetrofitError; {:try_start_11 .. :try_end_11} :catch_11

    goto/16 :goto_0

    .line 683
    :catch_11
    move-exception v6

    .line 684
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->p:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 689
    :pswitch_13
    const-string/jumbo v2, "extra_ids"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 690
    array-length v5, v2

    if-le v5, v9, :cond_13

    .line 691
    invoke-static {v2, v3, v9}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object v8, v2

    .line 693
    :goto_1
    array-length v11, v8

    .line 695
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 696
    array-length v13, v8

    move v10, v3

    move v9, v3

    :goto_2
    if-ge v10, v13, :cond_0

    aget-object v2, v8, v10

    .line 697
    new-instance v5, Ltv/periscope/android/api/FollowRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/FollowRequest;-><init>()V

    .line 698
    iput-object v12, v5, Ltv/periscope/android/api/FollowRequest;->cookie:Ljava/lang/String;

    .line 699
    iput-object v2, v5, Ltv/periscope/android/api/FollowRequest;->userId:Ljava/lang/String;

    .line 701
    :try_start_12
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->follow(Ltv/periscope/android/api/FollowRequest;)Ltv/periscope/android/api/FollowResponse;

    move-result-object v6

    .line 703
    add-int/lit8 v9, v9, 0x1

    .line 704
    if-ne v9, v11, :cond_3

    .line 705
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->E:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_12
    .catch Lretrofit/RetrofitError; {:try_start_12 .. :try_end_12} :catch_12

    goto/16 :goto_0

    .line 707
    :catch_12
    move-exception v6

    .line 709
    add-int/lit8 v2, v9, 0x1

    .line 710
    if-ne v2, v11, :cond_4

    .line 711
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->E:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    :cond_3
    move v2, v9

    .line 696
    :cond_4
    add-int/lit8 v3, v10, 0x1

    move v10, v3

    move v9, v2

    goto :goto_2

    .line 718
    :pswitch_14
    const-string/jumbo v2, "e_user_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 719
    new-instance v5, Ltv/periscope/android/api/UnfollowRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/UnfollowRequest;-><init>()V

    .line 720
    const-string/jumbo v3, "e_cookie"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Ltv/periscope/android/api/UnfollowRequest;->cookie:Ljava/lang/String;

    .line 721
    iput-object v2, v5, Ltv/periscope/android/api/UnfollowRequest;->userId:Ljava/lang/String;

    .line 723
    :try_start_13
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v3, v5}, Ltv/periscope/android/api/ApiService;->unfollow(Ltv/periscope/android/api/UnfollowRequest;)Ltv/periscope/android/api/UnfollowResponse;

    move-result-object v6

    .line 724
    iput-object v2, v6, Ltv/periscope/android/api/UnfollowResponse;->userId:Ljava/lang/String;

    .line 726
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->q:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_13
    .catch Lretrofit/RetrofitError; {:try_start_13 .. :try_end_13} :catch_13

    goto/16 :goto_0

    .line 727
    :catch_13
    move-exception v6

    .line 728
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->q:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 732
    :pswitch_15
    new-instance v5, Ltv/periscope/android/api/PsRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 733
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    .line 735
    :try_start_14
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->unban(Ltv/periscope/android/api/PsRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v6

    .line 736
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->r:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_14
    .catch Lretrofit/RetrofitError; {:try_start_14 .. :try_end_14} :catch_14

    goto/16 :goto_0

    .line 737
    :catch_14
    move-exception v6

    .line 738
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->r:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 742
    :pswitch_16
    new-instance v5, Ltv/periscope/android/api/PsRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 743
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    .line 745
    :try_start_15
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->deactivateAccount(Ltv/periscope/android/api/PsRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v6

    .line 746
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->s:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_15
    .catch Lretrofit/RetrofitError; {:try_start_15 .. :try_end_15} :catch_15

    goto/16 :goto_0

    .line 747
    :catch_15
    move-exception v6

    .line 748
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->s:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 752
    :pswitch_17
    const-string/jumbo v2, "e_user_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 753
    new-instance v5, Ltv/periscope/android/api/GetUserRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/GetUserRequest;-><init>()V

    .line 754
    const-string/jumbo v3, "e_cookie"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Ltv/periscope/android/api/GetUserRequest;->cookie:Ljava/lang/String;

    .line 755
    iput-object v2, v5, Ltv/periscope/android/api/GetUserRequest;->userId:Ljava/lang/String;

    .line 757
    :try_start_16
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getUser(Ltv/periscope/android/api/GetUserRequest;)Ltv/periscope/android/api/GetUserResponse;

    move-result-object v6

    .line 758
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->g:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_16
    .catch Lretrofit/RetrofitError; {:try_start_16 .. :try_end_16} :catch_16

    goto/16 :goto_0

    .line 759
    :catch_16
    move-exception v6

    .line 760
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->g:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 764
    :pswitch_18
    const-string/jumbo v2, "e_username"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 765
    new-instance v5, Ltv/periscope/android/api/GetUserRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/GetUserRequest;-><init>()V

    .line 766
    const-string/jumbo v3, "e_cookie"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Ltv/periscope/android/api/GetUserRequest;->cookie:Ljava/lang/String;

    .line 767
    iput-object v2, v5, Ltv/periscope/android/api/GetUserRequest;->username:Ljava/lang/String;

    .line 769
    :try_start_17
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getUser(Ltv/periscope/android/api/GetUserRequest;)Ltv/periscope/android/api/GetUserResponse;

    move-result-object v6

    .line 770
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->g:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_17
    .catch Lretrofit/RetrofitError; {:try_start_17 .. :try_end_17} :catch_17

    goto/16 :goto_0

    .line 771
    :catch_17
    move-exception v6

    .line 772
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->g:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 776
    :pswitch_19
    const-string/jumbo v2, "e_user_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 777
    new-instance v5, Ltv/periscope/android/api/GetFollowersRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/GetFollowersRequest;-><init>()V

    .line 778
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/GetFollowersRequest;->cookie:Ljava/lang/String;

    .line 779
    iput-object v8, v5, Ltv/periscope/android/api/GetFollowersRequest;->userId:Ljava/lang/String;

    .line 781
    :try_start_18
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getFollowers(Ltv/periscope/android/api/GetFollowersRequest;)Ljava/util/List;

    move-result-object v9

    .line 782
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->j:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    new-instance v6, Ltv/periscope/android/api/FetchUsersResponse;

    invoke-direct {v6, v8, v9}, Ltv/periscope/android/api/FetchUsersResponse;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_18
    .catch Lretrofit/RetrofitError; {:try_start_18 .. :try_end_18} :catch_18

    goto/16 :goto_0

    .line 784
    :catch_18
    move-exception v6

    .line 785
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->j:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 789
    :pswitch_1a
    const-string/jumbo v2, "e_user_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 790
    new-instance v5, Ltv/periscope/android/api/GetFollowingRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/GetFollowingRequest;-><init>()V

    .line 791
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/GetFollowingRequest;->cookie:Ljava/lang/String;

    .line 792
    iput-object v8, v5, Ltv/periscope/android/api/GetFollowingRequest;->userId:Ljava/lang/String;

    .line 794
    :try_start_19
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getFollowing(Ltv/periscope/android/api/GetFollowingRequest;)Ljava/util/List;

    move-result-object v9

    .line 795
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->k:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    new-instance v6, Ltv/periscope/android/api/FetchUsersResponse;

    invoke-direct {v6, v8, v9}, Ltv/periscope/android/api/FetchUsersResponse;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_19
    .catch Lretrofit/RetrofitError; {:try_start_19 .. :try_end_19} :catch_19

    goto/16 :goto_0

    .line 797
    :catch_19
    move-exception v6

    .line 798
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->k:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 802
    :pswitch_1b
    const-string/jumbo v2, "e_secret_key"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 803
    const-string/jumbo v5, "e_secret_token"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 804
    const-string/jumbo v5, "e_sign_up"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 805
    const-string/jumbo v5, "e_digits_ids"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 806
    new-instance v5, Ltv/periscope/android/api/SuggestedPeopleRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/SuggestedPeopleRequest;-><init>()V

    .line 807
    const-string/jumbo v9, "e_cookie"

    invoke-virtual {v4, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Ltv/periscope/android/api/SuggestedPeopleRequest;->cookie:Ljava/lang/String;

    .line 808
    iput-object v2, v5, Ltv/periscope/android/api/SuggestedPeopleRequest;->twitterSessionKey:Ljava/lang/String;

    .line 809
    iput-object v6, v5, Ltv/periscope/android/api/SuggestedPeopleRequest;->twitterSessionSecret:Ljava/lang/String;

    .line 810
    iput-boolean v3, v5, Ltv/periscope/android/api/SuggestedPeopleRequest;->signup:Z

    .line 811
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getLanguages(Landroid/os/Bundle;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/SuggestedPeopleRequest;->languages:[Ljava/lang/String;

    .line 812
    if-eqz v8, :cond_5

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 813
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, v5, Ltv/periscope/android/api/SuggestedPeopleRequest;->digitsIds:[Ljava/lang/String;

    .line 814
    iget-object v2, v5, Ltv/periscope/android/api/SuggestedPeopleRequest;->digitsIds:[Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 817
    :cond_5
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->suggestedPeople(Ltv/periscope/android/api/SuggestedPeopleRequest;)Ltv/periscope/android/api/SuggestedPeopleResponse;

    move-result-object v6

    .line 818
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->w:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_1a
    .catch Lretrofit/RetrofitError; {:try_start_1a .. :try_end_1a} :catch_1a

    goto/16 :goto_0

    .line 819
    :catch_1a
    move-exception v6

    .line 820
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->w:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 824
    :pswitch_1c
    const-string/jumbo v2, "extra_query"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 825
    new-instance v5, Ltv/periscope/android/api/UserSearchRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/UserSearchRequest;-><init>()V

    .line 826
    const-string/jumbo v3, "e_cookie"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Ltv/periscope/android/api/UserSearchRequest;->cookie:Ljava/lang/String;

    .line 827
    iput-object v2, v5, Ltv/periscope/android/api/UserSearchRequest;->search:Ljava/lang/String;

    .line 829
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->userSearch(Ltv/periscope/android/api/UserSearchRequest;)Ljava/util/List;

    move-result-object v6

    .line 830
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->x:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_1b
    .catch Lretrofit/RetrofitError; {:try_start_1b .. :try_end_1b} :catch_1b

    goto/16 :goto_0

    .line 831
    :catch_1b
    move-exception v6

    .line 832
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->x:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 836
    :pswitch_1d
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->broadcastSearch(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 839
    :pswitch_1e
    new-instance v5, Ltv/periscope/android/api/MainBroadcastFollowingRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/MainBroadcastFollowingRequest;-><init>()V

    .line 840
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/MainBroadcastFollowingRequest;->cookie:Ljava/lang/String;

    .line 842
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->followingBroadcastFeed(Ltv/periscope/android/api/MainBroadcastFollowingRequest;)Ljava/util/List;

    move-result-object v6

    .line 844
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->A:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_1c
    .catch Lretrofit/RetrofitError; {:try_start_1c .. :try_end_1c} :catch_1c

    goto/16 :goto_0

    .line 845
    :catch_1c
    move-exception v6

    .line 846
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->A:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 850
    :pswitch_1f
    new-instance v5, Ltv/periscope/android/api/MainBroadcastFeaturedRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/MainBroadcastFeaturedRequest;-><init>()V

    .line 851
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/MainBroadcastFeaturedRequest;->cookie:Ljava/lang/String;

    .line 852
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getLanguages(Landroid/os/Bundle;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/MainBroadcastFeaturedRequest;->languages:[Ljava/lang/String;

    .line 854
    :try_start_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->featuredBroadcastFeed(Ltv/periscope/android/api/MainBroadcastFeaturedRequest;)Ljava/util/List;

    move-result-object v6

    .line 856
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->z:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_1d
    .catch Lretrofit/RetrofitError; {:try_start_1d .. :try_end_1d} :catch_1d

    goto/16 :goto_0

    .line 857
    :catch_1d
    move-exception v6

    .line 858
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->z:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 863
    :pswitch_20
    new-instance v5, Ltv/periscope/android/api/service/channels/GetSuggestedChannelsRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/service/channels/GetSuggestedChannelsRequest;-><init>()V

    .line 864
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/service/channels/GetSuggestedChannelsRequest;->cookie:Ljava/lang/String;

    .line 865
    const-string/jumbo v2, "e_service_auth_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 866
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getLanguages(Landroid/os/Bundle;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 868
    :try_start_1e
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 869
    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    invoke-interface {v4, v2, v3}, Ltv/periscope/android/api/service/channels/ChannelsService;->getChannels(Ljava/lang/String;Ljava/util/List;)Ltv/periscope/android/api/service/channels/PsGetChannelsResponse;

    move-result-object v6

    .line 870
    new-instance v2, Ltv/periscope/android/event/ServiceEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->am:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    iget-object v6, v6, Ltv/periscope/android/api/service/channels/PsGetChannelsResponse;->channels:Ljava/util/List;

    .line 871
    invoke-static {v6}, Ltv/periscope/android/api/service/channels/PsChannel;->toChannels(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_1e
    .catch Lretrofit/RetrofitError; {:try_start_1e .. :try_end_1e} :catch_1e

    goto/16 :goto_0

    .line 872
    :catch_1e
    move-exception v13

    .line 873
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->am:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move-object v11, v5

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 878
    :pswitch_21
    new-instance v5, Ltv/periscope/android/api/service/channels/CreateChannelRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/service/channels/CreateChannelRequest;-><init>()V

    .line 879
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/service/channels/CreateChannelRequest;->cookie:Ljava/lang/String;

    .line 880
    const-string/jumbo v2, "e_service_channel_name"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/service/channels/CreateChannelRequest;->name:Ljava/lang/String;

    .line 881
    const-string/jumbo v2, "e_service_channel_type"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v5, Ltv/periscope/android/api/service/channels/CreateChannelRequest;->type:I

    .line 882
    const-string/jumbo v2, "e_service_auth_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 884
    :try_start_1f
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 885
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    invoke-interface {v3, v2, v5}, Ltv/periscope/android/api/service/channels/ChannelsService;->createChannel(Ljava/lang/String;Ltv/periscope/android/api/service/channels/CreateChannelRequest;)Ltv/periscope/android/api/service/channels/PsCreateChannelResponse;

    move-result-object v6

    .line 886
    new-instance v2, Ltv/periscope/android/event/ServiceEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aD:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    iget-object v6, v6, Ltv/periscope/android/api/service/channels/PsCreateChannelResponse;->channel:Ltv/periscope/android/api/service/channels/PsChannel;

    invoke-virtual {v6}, Ltv/periscope/android/api/service/channels/PsChannel;->create()Ltv/periscope/model/r;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_1f
    .catch Lretrofit/RetrofitError; {:try_start_1f .. :try_end_1f} :catch_1f

    goto/16 :goto_0

    .line 888
    :catch_1f
    move-exception v13

    .line 889
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->aD:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move-object v11, v5

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 894
    :pswitch_22
    const-string/jumbo v2, "e_service_channel_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 895
    const-string/jumbo v3, "e_service_auth_token"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 896
    new-instance v5, Ltv/periscope/android/api/service/channels/PatchChannelRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/service/channels/PatchChannelRequest;-><init>()V

    .line 897
    const-string/jumbo v6, "e_service_channel_name"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Ltv/periscope/android/api/service/channels/PatchChannelRequest;->name:Ljava/lang/String;

    .line 898
    const-string/jumbo v6, "e_service_channel_current_name"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Ltv/periscope/android/api/service/channels/PatchChannelRequest;->currentName:Ljava/lang/String;

    .line 899
    iput-object v2, v5, Ltv/periscope/android/api/service/channels/PatchChannelRequest;->channelId:Ljava/lang/String;

    .line 901
    :try_start_20
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 902
    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    invoke-interface {v4, v3, v2, v5}, Ltv/periscope/android/api/service/channels/ChannelsService;->patchChannel(Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/api/service/channels/PatchChannelRequest;)Ltv/periscope/android/api/service/channels/PsCreateChannelResponse;

    move-result-object v6

    .line 903
    new-instance v2, Ltv/periscope/android/event/ServiceEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aE:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    iget-object v6, v6, Ltv/periscope/android/api/service/channels/PsCreateChannelResponse;->channel:Ltv/periscope/android/api/service/channels/PsChannel;

    invoke-virtual {v6}, Ltv/periscope/android/api/service/channels/PsChannel;->create()Ltv/periscope/model/r;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_20
    .catch Lretrofit/RetrofitError; {:try_start_20 .. :try_end_20} :catch_20

    goto/16 :goto_0

    .line 904
    :catch_20
    move-exception v13

    .line 905
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->aE:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move-object v11, v5

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 910
    :pswitch_23
    new-instance v5, Ltv/periscope/android/api/PsRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 911
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    .line 912
    const-string/jumbo v2, "e_service_auth_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 913
    const-string/jumbo v2, "e_user_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 915
    const/4 v2, 0x0

    .line 919
    :try_start_21
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 920
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    const/4 v8, 0x0

    invoke-interface {v3, v6, v4, v8, v2}, Ltv/periscope/android/api/service/channels/ChannelsService;->getChannelsForMember(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ltv/periscope/android/api/service/channels/PsGetChannelsForMemberResponse;

    move-result-object v14

    .line 921
    iget-boolean v2, v14, Ltv/periscope/android/api/service/channels/PsGetChannelsForMemberResponse;->hasMore:Z

    .line 922
    iget-object v3, v14, Ltv/periscope/android/api/service/channels/PsGetChannelsForMemberResponse;->cursor:Ljava/lang/String;

    .line 923
    :goto_3
    if-eqz v2, :cond_6

    invoke-static {v3}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 924
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    const/4 v8, 0x0

    invoke-interface {v2, v6, v4, v8, v3}, Ltv/periscope/android/api/service/channels/ChannelsService;->getChannelsForMember(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ltv/periscope/android/api/service/channels/PsGetChannelsForMemberResponse;

    move-result-object v3

    .line 925
    iget-object v2, v14, Ltv/periscope/android/api/service/channels/PsGetChannelsForMemberResponse;->channels:Ljava/util/List;

    iget-object v8, v3, Ltv/periscope/android/api/service/channels/PsGetChannelsForMemberResponse;->channels:Ljava/util/List;

    invoke-interface {v2, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 926
    iget-boolean v2, v3, Ltv/periscope/android/api/service/channels/PsGetChannelsForMemberResponse;->hasMore:Z

    .line 927
    iget-object v3, v3, Ltv/periscope/android/api/service/channels/PsGetChannelsForMemberResponse;->cursor:Ljava/lang/String;

    goto :goto_3

    .line 929
    :cond_6
    new-instance v2, Ltv/periscope/android/event/ServiceEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ax:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    iget-object v6, v14, Ltv/periscope/android/api/service/channels/PsGetChannelsForMemberResponse;->channels:Ljava/util/List;

    .line 930
    invoke-static {v6}, Ltv/periscope/android/api/service/channels/PsChannel;->toChannels(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_21
    .catch Lretrofit/RetrofitError; {:try_start_21 .. :try_end_21} :catch_21

    goto/16 :goto_0

    .line 931
    :catch_21
    move-exception v13

    .line 932
    if-eqz v14, :cond_7

    iget-object v2, v14, Ltv/periscope/android/api/service/channels/PsGetChannelsForMemberResponse;->channels:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 933
    new-instance v2, Ltv/periscope/android/event/ServiceEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ax:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    iget-object v6, v14, Ltv/periscope/android/api/service/channels/PsGetChannelsForMemberResponse;->channels:Ljava/util/List;

    .line 934
    invoke-static {v6}, Ltv/periscope/android/api/service/channels/PsChannel;->toChannels(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    .line 936
    :cond_7
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->ax:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move-object v11, v5

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 941
    :pswitch_24
    new-instance v11, Ltv/periscope/android/api/PsRequest;

    invoke-direct {v11}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 942
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v11, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    .line 943
    const-string/jumbo v2, "e_service_auth_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 944
    const-string/jumbo v2, "e_service_channel_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 946
    const/4 v2, 0x0

    .line 950
    :try_start_22
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 951
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    const/4 v6, 0x0

    invoke-interface {v3, v5, v4, v6, v2}, Ltv/periscope/android/api/service/channels/ChannelsService;->getChannelMembers(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;
    :try_end_22
    .catch Lretrofit/RetrofitError; {:try_start_22 .. :try_end_22} :catch_22

    move-result-object v12

    .line 952
    :try_start_23
    iput-object v4, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;->channelId:Ljava/lang/String;

    .line 953
    iget-boolean v2, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;->hasMore:Z

    .line 954
    iget-object v3, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;->cursor:Ljava/lang/String;

    .line 955
    :goto_4
    if-eqz v2, :cond_8

    invoke-static {v3}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 956
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    const/4 v6, 0x0

    invoke-interface {v2, v5, v4, v6, v3}, Ltv/periscope/android/api/service/channels/ChannelsService;->getChannelMembers(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;

    move-result-object v3

    .line 957
    iget-object v2, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;->members:Ljava/util/List;

    iget-object v6, v3, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;->members:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 958
    iget-boolean v2, v3, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;->hasMore:Z

    .line 959
    iget-object v3, v3, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;->cursor:Ljava/lang/String;
    :try_end_23
    .catch Lretrofit/RetrofitError; {:try_start_23 .. :try_end_23} :catch_45

    goto :goto_4

    .line 961
    :catch_22
    move-exception v13

    move-object v12, v14

    .line 962
    :goto_5
    if-nez v12, :cond_8

    .line 963
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->ay:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 969
    :cond_8
    iget-object v2, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;->members:Ljava/util/List;

    invoke-static {v2}, Ltv/periscope/android/api/service/channels/PsChannelMember;->toUserIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2, v9}, Ltv/periscope/android/util/s;->a(Ljava/util/List;I)Ljava/util/List;

    move-result-object v2

    .line 971
    :try_start_24
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 972
    new-instance v4, Ltv/periscope/android/api/GetUsersRequest;

    invoke-direct {v4}, Ltv/periscope/android/api/GetUsersRequest;-><init>()V

    iget-object v5, v11, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v2}, Ltv/periscope/android/api/ApiRunnable;->getUsers(Ltv/periscope/android/api/GetUsersRequest;Ljava/lang/String;Ljava/util/List;)Ltv/periscope/android/api/GetUsersResponse;

    move-result-object v2

    .line 973
    iget-object v4, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;->users:Ljava/util/List;

    if-nez v4, :cond_9

    .line 974
    iget-object v2, v2, Ltv/periscope/android/api/GetUsersResponse;->users:Ljava/util/List;

    iput-object v2, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;->users:Ljava/util/List;
    :try_end_24
    .catch Lretrofit/RetrofitError; {:try_start_24 .. :try_end_24} :catch_23

    goto :goto_6

    .line 980
    :catch_23
    move-exception v13

    .line 981
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->ay:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 976
    :cond_9
    :try_start_25
    iget-object v4, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;->users:Ljava/util/List;

    iget-object v2, v2, Ltv/periscope/android/api/GetUsersResponse;->users:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6

    .line 979
    :cond_a
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->ay:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v13, v7

    invoke-direct/range {v8 .. v13}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_25
    .catch Lretrofit/RetrofitError; {:try_start_25 .. :try_end_25} :catch_23

    move-object v2, v8

    goto/16 :goto_0

    .line 986
    :pswitch_25
    new-instance v5, Ltv/periscope/android/api/service/channels/AddMembersToChannelRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/service/channels/AddMembersToChannelRequest;-><init>()V

    .line 987
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/service/channels/AddMembersToChannelRequest;->cookie:Ljava/lang/String;

    .line 988
    const-string/jumbo v2, "e_service_auth_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 989
    const-string/jumbo v3, "e_service_channel_id"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 990
    const-string/jumbo v6, "e_user_ids"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v5, Ltv/periscope/android/api/service/channels/AddMembersToChannelRequest;->addedMembers:Ljava/util/List;

    .line 991
    iput-object v3, v5, Ltv/periscope/android/api/service/channels/AddMembersToChannelRequest;->channelId:Ljava/lang/String;

    .line 993
    :try_start_26
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 994
    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    invoke-interface {v4, v2, v3, v5}, Ltv/periscope/android/api/service/channels/ChannelsService;->addMembersToChannel(Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/api/service/channels/AddMembersToChannelRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v6

    .line 995
    new-instance v2, Ltv/periscope/android/event/ServiceEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->az:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_26
    .catch Lretrofit/RetrofitError; {:try_start_26 .. :try_end_26} :catch_24

    goto/16 :goto_0

    .line 996
    :catch_24
    move-exception v13

    .line 997
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->az:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move-object v11, v5

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 1002
    :pswitch_26
    new-instance v5, Ltv/periscope/android/api/PsRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 1003
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    .line 1004
    const-string/jumbo v2, "e_service_auth_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1005
    const-string/jumbo v3, "e_service_channel_id"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1006
    const-string/jumbo v6, "e_user_id"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1007
    new-instance v6, Ltv/periscope/android/api/service/channels/DeleteChannelMemberData;

    invoke-direct {v6}, Ltv/periscope/android/api/service/channels/DeleteChannelMemberData;-><init>()V

    .line 1008
    iput-object v3, v6, Ltv/periscope/android/api/service/channels/DeleteChannelMemberData;->channelId:Ljava/lang/String;

    .line 1009
    iput-object v4, v6, Ltv/periscope/android/api/service/channels/DeleteChannelMemberData;->userId:Ljava/lang/String;

    .line 1011
    :try_start_27
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 1012
    move-object/from16 v0, p0

    iget-object v8, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    invoke-interface {v8, v2, v3, v4}, Ltv/periscope/android/api/service/channels/ChannelsService;->deleteChannelMember(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/android/api/PsResponse;

    .line 1013
    new-instance v2, Ltv/periscope/android/event/ServiceEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aA:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_27
    .catch Lretrofit/RetrofitError; {:try_start_27 .. :try_end_27} :catch_25

    goto/16 :goto_0

    .line 1014
    :catch_25
    move-exception v13

    .line 1015
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->aA:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object v11, v5

    move-object v12, v6

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 1019
    :pswitch_27
    new-instance v5, Ltv/periscope/android/api/service/channels/PatchChannelMemberRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/service/channels/PatchChannelMemberRequest;-><init>()V

    .line 1020
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/service/channels/PatchChannelMemberRequest;->cookie:Ljava/lang/String;

    .line 1021
    const-string/jumbo v2, "e_service_auth_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1022
    const-string/jumbo v3, "e_service_channel_id"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1023
    const-string/jumbo v6, "e_user_id"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1024
    const-string/jumbo v8, "e_channel_member_muted"

    invoke-virtual {v4, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v5, Ltv/periscope/android/api/service/channels/PatchChannelMemberRequest;->mute:Z

    .line 1025
    iput-object v6, v5, Ltv/periscope/android/api/service/channels/PatchChannelMemberRequest;->userId:Ljava/lang/String;

    .line 1026
    iput-object v3, v5, Ltv/periscope/android/api/service/channels/PatchChannelMemberRequest;->channelId:Ljava/lang/String;

    .line 1028
    :try_start_28
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 1029
    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    invoke-interface {v4, v2, v3, v6, v5}, Ltv/periscope/android/api/service/channels/ChannelsService;->patchChannelMember(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/api/service/channels/PatchChannelMemberRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v6

    .line 1030
    new-instance v2, Ltv/periscope/android/event/ServiceEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aB:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_28
    .catch Lretrofit/RetrofitError; {:try_start_28 .. :try_end_28} :catch_26

    goto/16 :goto_0

    .line 1031
    :catch_26
    move-exception v13

    .line 1032
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->aB:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move-object v11, v5

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 1037
    :pswitch_28
    new-instance v5, Ltv/periscope/android/api/service/channels/GetBroadcastsForChannelRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/service/channels/GetBroadcastsForChannelRequest;-><init>()V

    .line 1038
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/service/channels/GetBroadcastsForChannelRequest;->cookie:Ljava/lang/String;

    .line 1039
    const-string/jumbo v2, "e_service_auth_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1040
    const-string/jumbo v3, "e_service_channel_id"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1042
    :try_start_29
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 1043
    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    invoke-interface {v4, v2, v3}, Ltv/periscope/android/api/service/channels/ChannelsService;->getBroadcastsForChannel(Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/android/api/service/channels/PsGetBroadcastsForChannelResponse;

    move-result-object v2

    .line 1046
    new-instance v6, Ltv/periscope/android/api/service/channels/GetBroadcastsForChannelData;

    iget-object v2, v2, Ltv/periscope/android/api/service/channels/PsGetBroadcastsForChannelResponse;->bids:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ltv/periscope/android/api/ApiRunnable;->convertBids(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v6, v3, v2}, Ltv/periscope/android/api/service/channels/GetBroadcastsForChannelData;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 1047
    new-instance v2, Ltv/periscope/android/event/ServiceEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ar:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_29
    .catch Lretrofit/RetrofitError; {:try_start_29 .. :try_end_29} :catch_27

    goto/16 :goto_0

    .line 1048
    :catch_27
    move-exception v13

    .line 1049
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->ar:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move-object v11, v5

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 1054
    :pswitch_29
    const-string/jumbo v2, "e_service_auth_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1055
    const-string/jumbo v3, "e_service_channel_name"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1056
    const-string/jumbo v5, "e_languages"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 1058
    :try_start_2a
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 1059
    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    invoke-interface {v5, v2, v3, v4}, Ltv/periscope/android/api/service/channels/ChannelsService;->searchChannels(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ltv/periscope/android/api/service/channels/PsGetChannelSearchResponse;

    move-result-object v6

    .line 1060
    new-instance v2, Ltv/periscope/android/event/ServiceEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->as:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, v6, Ltv/periscope/android/api/service/channels/PsGetChannelSearchResponse;->channels:Ljava/util/List;

    invoke-static {v6}, Ltv/periscope/android/api/service/channels/PsChannel;->toChannels(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_2a
    .catch Lretrofit/RetrofitError; {:try_start_2a .. :try_end_2a} :catch_28

    goto/16 :goto_0

    .line 1061
    :catch_28
    move-exception v13

    .line 1062
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->as:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move-object v11, v14

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 1067
    :pswitch_2a
    const-string/jumbo v2, "e_service_auth_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1068
    const-string/jumbo v3, "e_service_channel_id"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1070
    :try_start_2b
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 1071
    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    invoke-interface {v4, v2, v3}, Ltv/periscope/android/api/service/channels/ChannelsService;->getChannelInfo(Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/android/api/service/channels/PsGetChannelInfoResponse;

    move-result-object v6

    .line 1072
    new-instance v2, Ltv/periscope/android/event/ServiceEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->at:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, v6, Ltv/periscope/android/api/service/channels/PsGetChannelInfoResponse;->channel:Ltv/periscope/android/api/service/channels/PsChannel;

    invoke-virtual {v6}, Ltv/periscope/android/api/service/channels/PsChannel;->create()Ltv/periscope/model/r;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_2b
    .catch Lretrofit/RetrofitError; {:try_start_2b .. :try_end_2b} :catch_29

    goto/16 :goto_0

    .line 1074
    :catch_29
    move-exception v13

    .line 1075
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->at:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move-object v11, v14

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 1080
    :pswitch_2b
    new-instance v11, Ltv/periscope/android/api/PsRequest;

    invoke-direct {v11}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 1081
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v11, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    .line 1082
    const-string/jumbo v2, "e_service_auth_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1083
    const-string/jumbo v2, "e_service_channel_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1088
    :try_start_2c
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V
    :try_end_2c
    .catch Lretrofit/RetrofitError; {:try_start_2c .. :try_end_2c} :catch_43

    move-object v12, v14

    .line 1091
    :goto_7
    :try_start_2d
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5, v14}, Ltv/periscope/android/api/service/channels/ChannelsService;->getChannelActions(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelActionsResponse;
    :try_end_2d
    .catch Lretrofit/RetrofitError; {:try_start_2d .. :try_end_2d} :catch_2b

    move-result-object v14

    .line 1092
    if-nez v12, :cond_d

    .line 1094
    :try_start_2e
    iput-object v4, v14, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelActionsResponse;->channelId:Ljava/lang/String;
    :try_end_2e
    .catch Lretrofit/RetrofitError; {:try_start_2e .. :try_end_2e} :catch_44

    move-object v12, v14

    .line 1098
    :goto_8
    :try_start_2f
    iget-object v2, v14, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelActionsResponse;->cursor:Ljava/lang/String;

    .line 1099
    iget-boolean v5, v14, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelActionsResponse;->hasMore:Z

    if-eqz v5, :cond_b

    invoke-static {v2}, Ldcq;->b(Ljava/lang/CharSequence;)Z
    :try_end_2f
    .catch Lretrofit/RetrofitError; {:try_start_2f .. :try_end_2f} :catch_2b

    move-result v5

    if-nez v5, :cond_12

    .line 1108
    :cond_b
    iget-object v2, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelActionsResponse;->actions:Ljava/util/List;

    invoke-static {v2}, Ltv/periscope/android/api/service/channels/PsChannelAction;->toUserIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2, v9}, Ltv/periscope/android/util/s;->a(Ljava/util/List;I)Ljava/util/List;

    move-result-object v2

    .line 1110
    :try_start_30
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 1111
    new-instance v4, Ltv/periscope/android/api/GetUsersRequest;

    invoke-direct {v4}, Ltv/periscope/android/api/GetUsersRequest;-><init>()V

    iget-object v5, v11, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v2}, Ltv/periscope/android/api/ApiRunnable;->getUsers(Ltv/periscope/android/api/GetUsersRequest;Ljava/lang/String;Ljava/util/List;)Ltv/periscope/android/api/GetUsersResponse;

    move-result-object v2

    .line 1112
    iget-object v4, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelActionsResponse;->users:Ljava/util/List;

    if-nez v4, :cond_e

    .line 1113
    iget-object v2, v2, Ltv/periscope/android/api/GetUsersResponse;->users:Ljava/util/List;

    iput-object v2, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelActionsResponse;->users:Ljava/util/List;
    :try_end_30
    .catch Lretrofit/RetrofitError; {:try_start_30 .. :try_end_30} :catch_2a

    goto :goto_9

    .line 1118
    :catch_2a
    move-exception v2

    .line 1122
    :cond_c
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->aF:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move v13, v7

    invoke-direct/range {v8 .. v13}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V

    move-object v2, v8

    goto/16 :goto_0

    .line 1096
    :cond_d
    :try_start_31
    iget-object v2, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelActionsResponse;->actions:Ljava/util/List;

    iget-object v5, v14, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelActionsResponse;->actions:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_31
    .catch Lretrofit/RetrofitError; {:try_start_31 .. :try_end_31} :catch_2b

    goto :goto_8

    .line 1100
    :catch_2b
    move-exception v13

    .line 1101
    :goto_a
    if-nez v12, :cond_b

    .line 1102
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->aF:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 1115
    :cond_e
    :try_start_32
    iget-object v4, v12, Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelActionsResponse;->users:Ljava/util/List;

    iget-object v2, v2, Ltv/periscope/android/api/GetUsersResponse;->users:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_32
    .catch Lretrofit/RetrofitError; {:try_start_32 .. :try_end_32} :catch_2a

    goto :goto_9

    .line 1125
    :pswitch_2c
    new-instance v5, Ltv/periscope/android/api/PsRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 1126
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    .line 1127
    const-string/jumbo v2, "e_service_auth_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1128
    const-string/jumbo v3, "e_service_channel_id"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1130
    :try_start_33
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ltv/periscope/android/api/ApiRunnable;->verifyAuthToken(Ljava/lang/String;)V

    .line 1131
    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    invoke-interface {v4, v2, v3}, Ltv/periscope/android/api/service/channels/ChannelsService;->deleteChannel(Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/android/api/PsResponse;

    .line 1132
    new-instance v6, Ltv/periscope/android/api/service/channels/DeleteChannelData;

    invoke-direct {v6}, Ltv/periscope/android/api/service/channels/DeleteChannelData;-><init>()V

    .line 1133
    iput-object v3, v6, Ltv/periscope/android/api/service/channels/DeleteChannelData;->channelId:Ljava/lang/String;

    .line 1134
    new-instance v2, Ltv/periscope/android/event/ServiceEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aC:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_33
    .catch Lretrofit/RetrofitError; {:try_start_33 .. :try_end_33} :catch_2c

    goto/16 :goto_0

    .line 1135
    :catch_2c
    move-exception v13

    .line 1136
    new-instance v8, Ltv/periscope/android/event/ServiceEvent;

    sget-object v9, Ltv/periscope/android/event/ApiEvent$Type;->aC:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    sget-object v12, Ltv/periscope/android/api/BackendServiceName;->CHANNELS:Ltv/periscope/android/api/BackendServiceName;

    move-object v11, v5

    move v14, v7

    move-object/from16 v15, p0

    invoke-direct/range {v8 .. v15}, Ltv/periscope/android/event/ServiceEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;ZLtv/periscope/android/api/ApiRunnable;)V

    move-object v2, v8

    goto/16 :goto_0

    .line 1141
    :pswitch_2d
    const-string/jumbo v2, "extra_ids"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1142
    new-instance v5, Ltv/periscope/android/api/GetBroadcastsRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/GetBroadcastsRequest;-><init>()V

    .line 1143
    const-string/jumbo v3, "e_cookie"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Ltv/periscope/android/api/GetBroadcastsRequest;->cookie:Ljava/lang/String;

    .line 1144
    iput-object v2, v5, Ltv/periscope/android/api/GetBroadcastsRequest;->ids:Ljava/util/ArrayList;

    .line 1145
    const-string/jumbo v2, "e_only_public_publish"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v5, Ltv/periscope/android/api/GetBroadcastsRequest;->onlyPublicPublish:Z

    .line 1149
    const-string/jumbo v2, "e_event_type"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1150
    if-eqz v2, :cond_f

    .line 1151
    invoke-static {v2}, Ltv/periscope/android/event/ApiEvent$Type;->valueOf(Ljava/lang/String;)Ltv/periscope/android/event/ApiEvent$Type;

    move-result-object v3

    .line 1156
    :goto_b
    :try_start_34
    const-string/jumbo v2, "PsApi"

    const-string/jumbo v4, "getting broadcasts"

    invoke-static {v2, v4}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1157
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getBroadcasts(Ltv/periscope/android/api/GetBroadcastsRequest;)Ljava/util/List;

    move-result-object v6

    .line 1158
    const-string/jumbo v2, "PsApi"

    const-string/jumbo v4, "getBroadcasts succeeded"

    invoke-static {v2, v4}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_34
    .catch Lretrofit/RetrofitError; {:try_start_34 .. :try_end_34} :catch_2d

    goto/16 :goto_0

    .line 1160
    :catch_2d
    move-exception v6

    .line 1161
    const-string/jumbo v2, "PsApi"

    const-string/jumbo v4, "getBroadcasts failed"

    invoke-static {v2, v4, v6}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1162
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1153
    :cond_f
    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->B:Ltv/periscope/android/event/ApiEvent$Type;

    goto :goto_b

    .line 1166
    :pswitch_2e
    new-instance v5, Ltv/periscope/android/api/service/highlights/GetBroadcastTrailerRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/service/highlights/GetBroadcastTrailerRequest;-><init>()V

    .line 1167
    const-string/jumbo v2, "e_broadcast_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/service/highlights/GetBroadcastTrailerRequest;->broadcastId:Ljava/lang/String;

    .line 1168
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/service/highlights/GetBroadcastTrailerRequest;->cookie:Ljava/lang/String;

    .line 1171
    :try_start_35
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getBroadcastReplayTrailer(Ltv/periscope/android/api/service/highlights/GetBroadcastTrailerRequest;)Ltv/periscope/android/api/service/highlights/GetBroadcastTrailerResponse;

    move-result-object v6

    .line 1172
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aw:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_35
    .catch Lretrofit/RetrofitError; {:try_start_35 .. :try_end_35} :catch_2e

    goto/16 :goto_0

    .line 1173
    :catch_2e
    move-exception v6

    .line 1174
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aw:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1178
    :pswitch_2f
    const-string/jumbo v2, "e_broadcast_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1179
    const-string/jumbo v3, "e_user_id"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1180
    new-instance v5, Ltv/periscope/android/api/GetBroadcastViewersRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/GetBroadcastViewersRequest;-><init>()V

    .line 1181
    const-string/jumbo v6, "e_cookie"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Ltv/periscope/android/api/GetBroadcastViewersRequest;->cookie:Ljava/lang/String;

    .line 1182
    iput-object v2, v5, Ltv/periscope/android/api/GetBroadcastViewersRequest;->id:Ljava/lang/String;

    .line 1184
    :try_start_36
    const-string/jumbo v4, "PsApi"

    const-string/jumbo v6, "getting broadcast viewers"

    invoke-static {v4, v6}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1185
    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v4, v5}, Ltv/periscope/android/api/ApiService;->getBroadcastViewers(Ltv/periscope/android/api/GetBroadcastViewersRequest;)Ltv/periscope/android/api/GetBroadcastViewersResponse;

    move-result-object v6

    .line 1186
    const-string/jumbo v4, "PsApi"

    const-string/jumbo v8, "getBroadcastViewers succeeded"

    invoke-static {v4, v8}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1187
    iput-object v2, v6, Ltv/periscope/android/api/GetBroadcastViewersResponse;->broadcastId:Ljava/lang/String;

    .line 1188
    iput-object v3, v6, Ltv/periscope/android/api/GetBroadcastViewersResponse;->broadcasterId:Ljava/lang/String;

    .line 1189
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->D:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_36
    .catch Lretrofit/RetrofitError; {:try_start_36 .. :try_end_36} :catch_2f

    goto/16 :goto_0

    .line 1190
    :catch_2f
    move-exception v6

    .line 1191
    const-string/jumbo v2, "PsApi"

    const-string/jumbo v3, "getBroadcastViewers failed"

    invoke-static {v2, v3, v6}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1192
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->D:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1196
    :pswitch_30
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getRankedBroadcastsRequest(Landroid/os/Bundle;)Ltv/periscope/android/api/RankedBroadcastsRequest;

    move-result-object v5

    .line 1198
    :try_start_37
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->rankedBroadcastFeed(Ltv/periscope/android/api/RankedBroadcastsRequest;)Ljava/util/List;

    move-result-object v6

    .line 1199
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->F:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_37
    .catch Lretrofit/RetrofitError; {:try_start_37 .. :try_end_37} :catch_30

    goto/16 :goto_0

    .line 1200
    :catch_30
    move-exception v6

    .line 1201
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->F:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1205
    :pswitch_31
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getRankedBroadcastsRequest(Landroid/os/Bundle;)Ltv/periscope/android/api/RankedBroadcastsRequest;

    move-result-object v5

    .line 1207
    :try_start_38
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->recentBroadcastFeed(Ltv/periscope/android/api/RankedBroadcastsRequest;)Ljava/util/List;

    move-result-object v6

    .line 1208
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->G:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_38
    .catch Lretrofit/RetrofitError; {:try_start_38 .. :try_end_38} :catch_31

    goto/16 :goto_0

    .line 1209
    :catch_31
    move-exception v6

    .line 1210
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->G:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1214
    :pswitch_32
    new-instance v5, Ltv/periscope/android/api/RankedBroadcastsRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/RankedBroadcastsRequest;-><init>()V

    .line 1215
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/RankedBroadcastsRequest;->cookie:Ljava/lang/String;

    .line 1216
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getLanguages(Landroid/os/Bundle;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/RankedBroadcastsRequest;->languages:[Ljava/lang/String;

    .line 1217
    const-string/jumbo v2, "e_use_personal"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v5, Ltv/periscope/android/api/RankedBroadcastsRequest;->usePersonal:Z

    .line 1218
    const-string/jumbo v2, "e_use_ml"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v5, Ltv/periscope/android/api/RankedBroadcastsRequest;->useML:Z

    .line 1220
    :try_start_39
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->rankedBroadcastFeed(Ltv/periscope/android/api/RankedBroadcastsRequest;)Ljava/util/List;

    move-result-object v6

    .line 1221
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->al:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    .line 1222
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_39
    .catch Lretrofit/RetrofitError; {:try_start_39 .. :try_end_39} :catch_32

    goto/16 :goto_0

    .line 1223
    :catch_32
    move-exception v6

    .line 1224
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->al:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1229
    :pswitch_33
    :try_start_3a
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2}, Ltv/periscope/android/api/ApiService;->getTrendingLocations()Ljava/util/List;

    move-result-object v6

    .line 1230
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->an:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_3a
    .catch Lretrofit/RetrofitError; {:try_start_3a .. :try_end_3a} :catch_33

    goto/16 :goto_0

    .line 1231
    :catch_33
    move-exception v6

    .line 1232
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->an:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object v5, v14

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1236
    :pswitch_34
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getMapBroadcastFeed(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1240
    :pswitch_35
    :try_start_3b
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    const-string/jumbo v3, ""

    invoke-interface {v2, v3}, Ltv/periscope/android/api/ApiService;->supportedLanguages(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1241
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->S:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_3b
    .catch Lretrofit/RetrofitError; {:try_start_3b .. :try_end_3b} :catch_34

    goto/16 :goto_0

    .line 1242
    :catch_34
    move-exception v6

    .line 1243
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->S:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object v5, v14

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1247
    :pswitch_36
    const-string/jumbo v2, "extra_width"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 1248
    const-string/jumbo v3, "extra_height"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1249
    const-string/jumbo v5, "e_region"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1250
    const-string/jumbo v5, "persistent"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 1251
    const-string/jumbo v5, "e_has_moderation"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    .line 1253
    new-instance v5, Ltv/periscope/android/api/CreateBroadcastPersistenceRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/CreateBroadcastPersistenceRequest;-><init>()V

    .line 1254
    const-string/jumbo v10, "e_cookie"

    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v5, Ltv/periscope/android/api/CreateBroadcastPersistenceRequest;->cookie:Ljava/lang/String;

    .line 1255
    iput-wide v12, v5, Ltv/periscope/android/api/CreateBroadcastPersistenceRequest;->lat:D

    .line 1256
    iput-wide v12, v5, Ltv/periscope/android/api/CreateBroadcastPersistenceRequest;->lng:D

    .line 1257
    iput v2, v5, Ltv/periscope/android/api/CreateBroadcastPersistenceRequest;->width:I

    .line 1258
    iput v3, v5, Ltv/periscope/android/api/CreateBroadcastPersistenceRequest;->height:I

    .line 1259
    iput-object v6, v5, Ltv/periscope/android/api/CreateBroadcastPersistenceRequest;->region:Ljava/lang/String;

    .line 1260
    iput-boolean v8, v5, Ltv/periscope/android/api/CreateBroadcastPersistenceRequest;->persistent:Z

    .line 1262
    const/4 v2, 0x3

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 1263
    iput-object v2, v5, Ltv/periscope/android/api/CreateBroadcastPersistenceRequest;->pspVersion:[I

    .line 1264
    iput-boolean v9, v5, Ltv/periscope/android/api/CreateBroadcastPersistenceRequest;->hasModeration:Z

    .line 1265
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getLanguages(Landroid/os/Bundle;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/CreateBroadcastPersistenceRequest;->languages:[Ljava/lang/String;

    .line 1268
    :try_start_3c
    const-string/jumbo v2, "PsApi"

    const-string/jumbo v3, "creating Broadcast with persistence"

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1269
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->startBroadcast(Ltv/periscope/android/api/CreateBroadcastRequest;)Ltv/periscope/android/api/CreateBroadcastResponse;

    move-result-object v6

    .line 1270
    const-string/jumbo v2, "PsApi"

    const-string/jumbo v3, "createBroadcast with persistence succeeded"

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->J:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-virtual {v6}, Ltv/periscope/android/api/CreateBroadcastResponse;->create()Ltv/periscope/model/w;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_3c
    .catch Lretrofit/RetrofitError; {:try_start_3c .. :try_end_3c} :catch_35

    goto/16 :goto_0

    .line 1273
    :catch_35
    move-exception v6

    .line 1274
    const-string/jumbo v2, "PsApi"

    const-string/jumbo v3, "createBroadcast with persistence failed "

    invoke-static {v2, v3, v6}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1275
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->J:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1279
    :pswitch_37
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->replayThumbnailPlaylist(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1282
    :pswitch_38
    const-string/jumbo v2, "e_broadcast_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1283
    const-string/jumbo v3, "e_title"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1284
    const-string/jumbo v5, "e_locked_ids"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 1285
    const-string/jumbo v5, "e_locked_private_channel_ids"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 1286
    const-string/jumbo v5, "e_has_loc"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    .line 1287
    const-string/jumbo v5, "e_lat"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v10

    .line 1288
    const-string/jumbo v5, "e_long"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v11

    .line 1289
    const-string/jumbo v5, "e_following_only_chat"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    .line 1290
    new-instance v5, Ltv/periscope/android/api/PublishBroadcastRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/PublishBroadcastRequest;-><init>()V

    .line 1291
    const-string/jumbo v13, "e_cookie"

    invoke-virtual {v4, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v5, Ltv/periscope/android/api/PublishBroadcastRequest;->cookie:Ljava/lang/String;

    .line 1292
    iput-object v2, v5, Ltv/periscope/android/api/PublishBroadcastRequest;->broadcastId:Ljava/lang/String;

    .line 1293
    iput-object v3, v5, Ltv/periscope/android/api/PublishBroadcastRequest;->title:Ljava/lang/String;

    .line 1294
    iput-object v6, v5, Ltv/periscope/android/api/PublishBroadcastRequest;->lockIds:Ljava/util/ArrayList;

    .line 1295
    iput-object v8, v5, Ltv/periscope/android/api/PublishBroadcastRequest;->lockPrivateChannelIds:Ljava/util/ArrayList;

    .line 1296
    iput-boolean v9, v5, Ltv/periscope/android/api/PublishBroadcastRequest;->hasLocation:Z

    .line 1297
    iput v10, v5, Ltv/periscope/android/api/PublishBroadcastRequest;->lat:F

    .line 1298
    iput v11, v5, Ltv/periscope/android/api/PublishBroadcastRequest;->lng:F

    .line 1299
    iput-boolean v12, v5, Ltv/periscope/android/api/PublishBroadcastRequest;->followingOnlyChat:Z

    .line 1300
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/PublishBroadcastRequest;->locale:Ljava/lang/String;

    .line 1301
    const-string/jumbo v2, "e_bit_rate"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v5, Ltv/periscope/android/api/PublishBroadcastRequest;->bitRate:I

    .line 1302
    const-string/jumbo v2, "e_camera_rotation"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v5, Ltv/periscope/android/api/PublishBroadcastRequest;->cameraRotation:I

    .line 1304
    :try_start_3d
    const-string/jumbo v2, "PsApi"

    const-string/jumbo v3, "publishing broadcast"

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1305
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->publishBroadcast(Ltv/periscope/android/api/PublishBroadcastRequest;)Ltv/periscope/android/api/PublishBroadcastResponse;

    move-result-object v6

    .line 1306
    const-string/jumbo v2, "PsApi"

    const-string/jumbo v3, "publishBroadcast succeeded"

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1307
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->K:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_3d
    .catch Lretrofit/RetrofitError; {:try_start_3d .. :try_end_3d} :catch_36

    goto/16 :goto_0

    .line 1309
    :catch_36
    move-exception v6

    .line 1310
    const-string/jumbo v2, "PsApi"

    const-string/jumbo v3, "publishBroadcast failed"

    invoke-static {v2, v3, v6}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1311
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->K:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1315
    :pswitch_39
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->deleteBroadcast(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1318
    :pswitch_3a
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->pingWatching(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1321
    :pswitch_3b
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->endWatching(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1324
    :pswitch_3c
    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/android/api/ApiRunnable;->mRequest:Ltv/periscope/android/api/ApiRequest;

    check-cast v5, Ltv/periscope/android/api/SetSettingsRequest;

    .line 1326
    :try_start_3e
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->setSettings(Ltv/periscope/android/api/SetSettingsRequest;)Ltv/periscope/android/api/SetSettingsResponse;

    move-result-object v6

    .line 1327
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->T:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_3e
    .catch Lretrofit/RetrofitError; {:try_start_3e .. :try_end_3e} :catch_37

    goto/16 :goto_0

    .line 1328
    :catch_37
    move-exception v6

    .line 1329
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->T:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1333
    :pswitch_3d
    new-instance v5, Ltv/periscope/android/api/GetSettingsRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/GetSettingsRequest;-><init>()V

    .line 1334
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/GetSettingsRequest;->cookie:Ljava/lang/String;

    .line 1336
    :try_start_3f
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->getSettings(Ltv/periscope/android/api/GetSettingsRequest;)Ltv/periscope/android/api/GetSettingsResponse;

    move-result-object v6

    .line 1337
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->R:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_3f
    .catch Lretrofit/RetrofitError; {:try_start_3f .. :try_end_3f} :catch_38

    goto/16 :goto_0

    .line 1338
    :catch_38
    move-exception v6

    .line 1339
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->R:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1343
    :pswitch_3e
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->endBroadcast(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1346
    :pswitch_3f
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->pingBroadcast(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1349
    :pswitch_40
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->uploadBroadcasterLogs(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1352
    :pswitch_41
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->shareBroadcast(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1355
    :pswitch_42
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->reportBroadcast(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1358
    :pswitch_43
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->adjustBroadcastRank(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1361
    :pswitch_44
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object v2, v8

    check-cast v2, Ltv/periscope/android/api/BlockRequest;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v7}, Ltv/periscope/android/api/ApiRunnable;->block(Ljava/lang/String;Ltv/periscope/android/api/BlockRequest;Z)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1364
    :pswitch_45
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->unblock(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1367
    :pswitch_46
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getBlocked(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1370
    :pswitch_47
    const-string/jumbo v2, "e_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1371
    new-instance v5, Ltv/periscope/android/api/BroadcastIdForTokenRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/BroadcastIdForTokenRequest;-><init>()V

    .line 1372
    const-string/jumbo v3, "e_cookie"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Ltv/periscope/android/api/BroadcastIdForTokenRequest;->cookie:Ljava/lang/String;

    .line 1373
    iput-object v2, v5, Ltv/periscope/android/api/BroadcastIdForTokenRequest;->token:Ljava/lang/String;

    .line 1375
    :try_start_40
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aa:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    .line 1376
    invoke-interface {v6, v5}, Ltv/periscope/android/api/ApiService;->getBroadcastIdForShareToken(Ltv/periscope/android/api/BroadcastIdForTokenRequest;)Ltv/periscope/android/api/BroadcastResponse;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_40
    .catch Lretrofit/RetrofitError; {:try_start_40 .. :try_end_40} :catch_39

    goto/16 :goto_0

    .line 1377
    :catch_39
    move-exception v6

    .line 1378
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aa:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1382
    :pswitch_48
    const-string/jumbo v2, "e_token"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1384
    :try_start_41
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/ApiRunnable;->mPublicService:Ltv/periscope/android/api/PublicApiService;

    invoke-interface {v3, v2}, Ltv/periscope/android/api/PublicApiService;->getBroadcastPublic(Ljava/lang/String;)Ltv/periscope/android/api/GetBroadcastPublicResponse;

    move-result-object v2

    .line 1385
    new-instance v6, Ltv/periscope/android/api/BroadcastResponse;

    invoke-direct {v6}, Ltv/periscope/android/api/BroadcastResponse;-><init>()V

    .line 1387
    iget-object v2, v2, Ltv/periscope/android/api/GetBroadcastPublicResponse;->broadcast:Ltv/periscope/android/api/PsBroadcast;

    iget-object v2, v2, Ltv/periscope/android/api/PsBroadcast;->id:Ljava/lang/String;

    iput-object v2, v6, Ltv/periscope/android/api/BroadcastResponse;->broadcastId:Ljava/lang/String;

    .line 1388
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aa:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_41
    .catch Lretrofit/RetrofitError; {:try_start_41 .. :try_end_41} :catch_3a

    goto/16 :goto_0

    .line 1389
    :catch_3a
    move-exception v6

    .line 1390
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aa:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object v5, v14

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1394
    :pswitch_49
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->uploadToast(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    :pswitch_4a
    move-object v5, v8

    .line 1397
    check-cast v5, Ltv/periscope/android/api/UserBroadcastsRequest;

    .line 1399
    :try_start_42
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->userBroadcasts(Ltv/periscope/android/api/UserBroadcastsRequest;)Ljava/util/List;

    move-result-object v6

    .line 1400
    iget-object v2, v5, Ltv/periscope/android/api/UserBroadcastsRequest;->userId:Ljava/lang/String;

    invoke-static {v2}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1401
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ac:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    iget-object v8, v5, Ltv/periscope/android/api/UserBroadcastsRequest;->userId:Ljava/lang/String;

    .line 1402
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v6

    invoke-static {v8, v6}, Ltv/periscope/model/ae;->a(Ljava/lang/String;Ljava/util/List;)Ltv/periscope/model/ae;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_42
    .catch Lretrofit/RetrofitError; {:try_start_42 .. :try_end_42} :catch_3b

    goto/16 :goto_0

    .line 1407
    :catch_3b
    move-exception v6

    .line 1408
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ac:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1403
    :cond_10
    :try_start_43
    iget-object v2, v5, Ltv/periscope/android/api/UserBroadcastsRequest;->username:Ljava/lang/String;

    invoke-static {v2}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1404
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ac:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    iget-object v8, v5, Ltv/periscope/android/api/UserBroadcastsRequest;->username:Ljava/lang/String;

    .line 1405
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v6

    invoke-static {v8, v6}, Ltv/periscope/model/ae;->b(Ljava/lang/String;Ljava/util/List;)Ltv/periscope/model/ae;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_43
    .catch Lretrofit/RetrofitError; {:try_start_43 .. :try_end_43} :catch_3b

    goto/16 :goto_0

    .line 1413
    :cond_11
    :pswitch_4b
    :try_start_44
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    move-object v0, v8

    check-cast v0, Ltv/periscope/android/api/PsRequest;

    move-object v2, v0

    invoke-interface {v3, v2}, Ltv/periscope/android/api/ApiService;->recentlyWatchedBroadcasts(Ltv/periscope/android/api/PsRequest;)Ljava/util/List;

    move-result-object v5

    .line 1414
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ad:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    .line 1415
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Ltv/periscope/android/api/ApiRunnable;->convert(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v6

    move-object v5, v8

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_44
    .catch Lretrofit/RetrofitError; {:try_start_44 .. :try_end_44} :catch_3c

    goto/16 :goto_0

    .line 1416
    :catch_3c
    move-exception v6

    .line 1417
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ad:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object v5, v8

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1422
    :pswitch_4c
    :try_start_45
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ae:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    move-object v0, v8

    check-cast v0, Ltv/periscope/android/api/ClearHistoryBroadcastFeedRequest;

    move-object v5, v0

    .line 1423
    invoke-interface {v6, v5}, Ltv/periscope/android/api/ApiService;->clearRecentlyWatchedHistory(Ltv/periscope/android/api/ClearHistoryBroadcastFeedRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v6

    move-object v5, v8

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_45
    .catch Lretrofit/RetrofitError; {:try_start_45 .. :try_end_45} :catch_3d

    goto/16 :goto_0

    .line 1424
    :catch_3d
    move-exception v6

    .line 1425
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ae:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object v5, v8

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1430
    :pswitch_4d
    const-string/jumbo v2, "e_file_dir"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1431
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1432
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1433
    new-instance v4, Ltv/periscope/android/api/ApiRunnable$1;

    const-string/jumbo v5, "image/jpeg"

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5, v3}, Ltv/periscope/android/api/ApiRunnable$1;-><init>(Ltv/periscope/android/api/ApiRunnable;Ljava/lang/String;Ljava/io/File;)V

    .line 1441
    :try_start_46
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v3, v2, v4}, Ltv/periscope/android/api/ApiService;->uploadProfileImage(Ljava/lang/String;Lretrofit/mime/TypedFile;)Ltv/periscope/android/api/UploadProfileImageResponse;

    move-result-object v6

    .line 1442
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->af:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_46
    .catch Lretrofit/RetrofitError; {:try_start_46 .. :try_end_46} :catch_3e

    goto/16 :goto_0

    .line 1443
    :catch_3e
    move-exception v6

    .line 1444
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->af:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    move-object v5, v14

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1448
    :pswitch_4e
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->updateProfileDisplayName(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1451
    :pswitch_4f
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->updateProfileDescription(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1454
    :pswitch_50
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Ltv/periscope/android/api/ApiRunnable;->createExternalEncoder(Z)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1457
    :pswitch_51
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Ltv/periscope/android/api/ApiRunnable;->setExternalEncoderName(Z)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1460
    :pswitch_52
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Ltv/periscope/android/api/ApiRunnable;->deleteExternalEncoder(Z)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1463
    :pswitch_53
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Ltv/periscope/android/api/ApiRunnable;->getExternalEncoders(Z)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1466
    :pswitch_54
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Ltv/periscope/android/api/ApiRunnable;->getBroadcastForExternalEncoder(Z)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    :pswitch_55
    move-object v5, v8

    .line 1469
    check-cast v5, Ltv/periscope/android/api/PlaybackMetaRequest;

    .line 1471
    :try_start_47
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->livePlaybackMeta(Ltv/periscope/android/api/PlaybackMetaRequest;)Ltv/periscope/android/api/PlaybackMetaResponse;

    move-result-object v6

    .line 1472
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ai:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_47
    .catch Lretrofit/RetrofitError; {:try_start_47 .. :try_end_47} :catch_3f

    goto/16 :goto_0

    .line 1473
    :catch_3f
    move-exception v6

    .line 1474
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ai:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    :pswitch_56
    move-object v5, v8

    .line 1478
    check-cast v5, Ltv/periscope/android/api/PlaybackMetaRequest;

    .line 1480
    :try_start_48
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->replayPlaybackMeta(Ltv/periscope/android/api/PlaybackMetaRequest;)Ltv/periscope/android/api/PlaybackMetaResponse;

    move-result-object v6

    .line 1481
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aj:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_48
    .catch Lretrofit/RetrofitError; {:try_start_48 .. :try_end_48} :catch_40

    goto/16 :goto_0

    .line 1482
    :catch_40
    move-exception v6

    .line 1483
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->aj:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    :pswitch_57
    move-object v5, v8

    .line 1487
    check-cast v5, Ltv/periscope/android/api/BroadcastMetaRequest;

    .line 1489
    :try_start_49
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->broadcastMeta(Ltv/periscope/android/api/BroadcastMetaRequest;)Ltv/periscope/android/api/BroadcastMetaResponse;

    move-result-object v6

    .line 1490
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ak:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_49
    .catch Lretrofit/RetrofitError; {:try_start_49 .. :try_end_49} :catch_41

    goto/16 :goto_0

    .line 1491
    :catch_41
    move-exception v6

    .line 1492
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->ak:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1496
    :pswitch_58
    new-instance v5, Ltv/periscope/android/api/PersistBroadcastRequest;

    invoke-direct {v5}, Ltv/periscope/android/api/PersistBroadcastRequest;-><init>()V

    .line 1497
    const-string/jumbo v2, "e_cookie"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/PersistBroadcastRequest;->cookie:Ljava/lang/String;

    .line 1498
    const-string/jumbo v2, "e_broadcast_id"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Ltv/periscope/android/api/PersistBroadcastRequest;->broadcastId:Ljava/lang/String;

    .line 1500
    :try_start_4a
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/ApiRunnable;->mService:Ltv/periscope/android/api/ApiService;

    invoke-interface {v2, v5}, Ltv/periscope/android/api/ApiService;->persistBroadcast(Ltv/periscope/android/api/PersistBroadcastRequest;)Ltv/periscope/android/api/PsResponse;

    move-result-object v6

    .line 1501
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->av:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    :try_end_4a
    .catch Lretrofit/RetrofitError; {:try_start_4a .. :try_end_4a} :catch_42

    goto/16 :goto_0

    .line 1502
    :catch_42
    move-exception v6

    .line 1503
    new-instance v2, Ltv/periscope/android/event/ApiEvent;

    sget-object v3, Ltv/periscope/android/event/ApiEvent$Type;->av:Ltv/periscope/android/event/ApiEvent$Type;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V

    goto/16 :goto_0

    .line 1507
    :pswitch_59
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->accessVideo(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1510
    :pswitch_5a
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->accessChat(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1513
    :pswitch_5b
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->startWatching(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1516
    :pswitch_5c
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->reportComment(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1519
    :pswitch_5d
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->vote(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1522
    :pswitch_5e
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->activeJuror(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1525
    :pswitch_5f
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Ltv/periscope/android/api/ApiRunnable;->tweetBroadcastPublished(Z)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1528
    :pswitch_60
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Ltv/periscope/android/api/ApiRunnable;->retweetBroadcast(Z)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1531
    :pswitch_61
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Ltv/periscope/android/api/ApiRunnable;->associateTweetWithBroadcast(Z)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1535
    :pswitch_62
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getBroadcastPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1538
    :pswitch_63
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getBroadcastsPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1541
    :pswitch_64
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->getUserPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1544
    :pswitch_65
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->accessVideoPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1547
    :pswitch_66
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->accessChatPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1550
    :pswitch_67
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->startWatchingPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1553
    :pswitch_68
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->pingWatchingPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1556
    :pswitch_69
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->endWatchingPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1559
    :pswitch_6a
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->replayThumbnailPlaylistPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1562
    :pswitch_6b
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->markAbusePublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1565
    :pswitch_6c
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ltv/periscope/android/api/ApiRunnable;->blockPublic(Landroid/os/Bundle;)Ltv/periscope/android/event/ApiEvent;

    move-result-object v2

    goto/16 :goto_0

    .line 1100
    :catch_43
    move-exception v13

    move-object v12, v14

    goto/16 :goto_a

    :catch_44
    move-exception v13

    move-object v12, v14

    goto/16 :goto_a

    .line 961
    :catch_45
    move-exception v13

    goto/16 :goto_5

    :cond_12
    move-object v14, v2

    goto/16 :goto_7

    :cond_13
    move-object v8, v2

    goto/16 :goto_1

    .line 449
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_6
        :pswitch_30
        :pswitch_8
        :pswitch_c
        :pswitch_d
        :pswitch_10
        :pswitch_14
        :pswitch_17
        :pswitch_1a
        :pswitch_19
        :pswitch_0
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1e
        :pswitch_1f
        :pswitch_2d
        :pswitch_2f
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_38
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_0
        :pswitch_0
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_41
        :pswitch_47
        :pswitch_49
        :pswitch_42
        :pswitch_0
        :pswitch_39
        :pswitch_4a
        :pswitch_4d
        :pswitch_4f
        :pswitch_4e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_34
        :pswitch_55
        :pswitch_57
        :pswitch_11
        :pswitch_12
        :pswitch_35
        :pswitch_43
        :pswitch_7
        :pswitch_f
        :pswitch_18
        :pswitch_37
        :pswitch_15
        :pswitch_32
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
        :pswitch_0
        :pswitch_1d
        :pswitch_20
        :pswitch_3
        :pswitch_33
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
        :pswitch_28
        :pswitch_58
        :pswitch_36
        :pswitch_16
        :pswitch_29
        :pswitch_5
        :pswitch_2e
        :pswitch_2a
        :pswitch_2
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_b
        :pswitch_2c
        :pswitch_21
        :pswitch_22
        :pswitch_2b
        :pswitch_e
        :pswitch_56
        :pswitch_5f
        :pswitch_60
        :pswitch_31
        :pswitch_9
        :pswitch_a
        :pswitch_4b
        :pswitch_4c
        :pswitch_61
        :pswitch_40
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_63
        :pswitch_0
        :pswitch_68
        :pswitch_69
        :pswitch_0
        :pswitch_0
        :pswitch_6a
        :pswitch_6b
        :pswitch_6c
        :pswitch_65
        :pswitch_66
        :pswitch_67
        :pswitch_64
        :pswitch_48
        :pswitch_62
    .end packed-switch

    .line 1262
    :array_0
    .array-data 4
        0x1
        0x0
        0x0
    .end array-data
.end method

.method protected bridge synthetic finish(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 77
    check-cast p1, Ltv/periscope/android/event/ApiEvent;

    invoke-virtual {p0, p1}, Ltv/periscope/android/api/ApiRunnable;->finish(Ltv/periscope/android/event/ApiEvent;)V

    return-void
.end method

.method protected finish(Ltv/periscope/android/event/ApiEvent;)V
    .locals 4

    .prologue
    .line 432
    const-string/jumbo v0, "PsApi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No problems detected for action code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/api/ApiRunnable;->mActionCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". Num retries left: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 433
    invoke-virtual {p0}, Ltv/periscope/android/api/ApiRunnable;->numRetries()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". Current backoff: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ltv/periscope/android/api/ApiRunnable;->currentBackoff()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 432
    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    if-eqz p1, :cond_0

    .line 435
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mEventBus:Lde/greenrobot/event/c;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 437
    :cond_0
    return-void
.end method

.method public getRequestId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mRequestId:Ljava/lang/String;

    return-object v0
.end method

.method protected id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 441
    iget v0, p0, Ltv/periscope/android/api/ApiRunnable;->mActionCode:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic noRetriesLeft(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 77
    check-cast p1, Ltv/periscope/android/event/ApiEvent;

    invoke-virtual {p0, p1}, Ltv/periscope/android/api/ApiRunnable;->noRetriesLeft(Ltv/periscope/android/event/ApiEvent;)V

    return-void
.end method

.method protected noRetriesLeft(Ltv/periscope/android/event/ApiEvent;)V
    .locals 3

    .prologue
    .line 419
    const-string/jumbo v0, "PsApi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No retries remaining for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/api/ApiRunnable;->mActionCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". Posting error."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    if-eqz p1, :cond_0

    .line 421
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mEventBus:Lde/greenrobot/event/c;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 423
    :cond_0
    return-void
.end method

.method protected bridge synthetic retry(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 77
    check-cast p1, Ltv/periscope/android/event/ApiEvent;

    invoke-virtual {p0, p1}, Ltv/periscope/android/api/ApiRunnable;->retry(Ltv/periscope/android/event/ApiEvent;)V

    return-void
.end method

.method protected retry(Ltv/periscope/android/event/ApiEvent;)V
    .locals 2

    .prologue
    .line 427
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mEventBus:Lde/greenrobot/event/c;

    new-instance v1, Ltv/periscope/android/event/RetryEvent;

    invoke-direct {v1, p0}, Ltv/periscope/android/event/RetryEvent;-><init>(Ltv/periscope/android/api/ApiRunnable;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 428
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 398
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 399
    invoke-super {p0}, Ldcp;->run()V

    .line 400
    return-void
.end method

.method public setAuthorizationHeader(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Ltv/periscope/android/api/ApiRunnable;->mBundle:Landroid/os/Bundle;

    const-string/jumbo v1, "e_service_auth_token"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    return-void
.end method
