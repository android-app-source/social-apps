.class final enum Ltv/periscope/android/api/FollowAction$FollowActionType;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/api/FollowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "FollowActionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/android/api/FollowAction$FollowActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Ltv/periscope/android/api/FollowAction$FollowActionType;

.field public static final enum Block:Ltv/periscope/android/api/FollowAction$FollowActionType;

.field public static final enum Follow:Ltv/periscope/android/api/FollowAction$FollowActionType;

.field public static final enum Mute:Ltv/periscope/android/api/FollowAction$FollowActionType;

.field public static final enum Unblock:Ltv/periscope/android/api/FollowAction$FollowActionType;

.field public static final enum Unfollow:Ltv/periscope/android/api/FollowAction$FollowActionType;

.field public static final enum Unmute:Ltv/periscope/android/api/FollowAction$FollowActionType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Ltv/periscope/android/api/FollowAction$FollowActionType;

    const-string/jumbo v1, "Follow"

    invoke-direct {v0, v1, v3}, Ltv/periscope/android/api/FollowAction$FollowActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/api/FollowAction$FollowActionType;->Follow:Ltv/periscope/android/api/FollowAction$FollowActionType;

    .line 7
    new-instance v0, Ltv/periscope/android/api/FollowAction$FollowActionType;

    const-string/jumbo v1, "Unfollow"

    invoke-direct {v0, v1, v4}, Ltv/periscope/android/api/FollowAction$FollowActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/api/FollowAction$FollowActionType;->Unfollow:Ltv/periscope/android/api/FollowAction$FollowActionType;

    .line 8
    new-instance v0, Ltv/periscope/android/api/FollowAction$FollowActionType;

    const-string/jumbo v1, "Block"

    invoke-direct {v0, v1, v5}, Ltv/periscope/android/api/FollowAction$FollowActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/api/FollowAction$FollowActionType;->Block:Ltv/periscope/android/api/FollowAction$FollowActionType;

    .line 9
    new-instance v0, Ltv/periscope/android/api/FollowAction$FollowActionType;

    const-string/jumbo v1, "Unblock"

    invoke-direct {v0, v1, v6}, Ltv/periscope/android/api/FollowAction$FollowActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/api/FollowAction$FollowActionType;->Unblock:Ltv/periscope/android/api/FollowAction$FollowActionType;

    .line 10
    new-instance v0, Ltv/periscope/android/api/FollowAction$FollowActionType;

    const-string/jumbo v1, "Mute"

    invoke-direct {v0, v1, v7}, Ltv/periscope/android/api/FollowAction$FollowActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/api/FollowAction$FollowActionType;->Mute:Ltv/periscope/android/api/FollowAction$FollowActionType;

    .line 11
    new-instance v0, Ltv/periscope/android/api/FollowAction$FollowActionType;

    const-string/jumbo v1, "Unmute"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/api/FollowAction$FollowActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/api/FollowAction$FollowActionType;->Unmute:Ltv/periscope/android/api/FollowAction$FollowActionType;

    .line 5
    const/4 v0, 0x6

    new-array v0, v0, [Ltv/periscope/android/api/FollowAction$FollowActionType;

    sget-object v1, Ltv/periscope/android/api/FollowAction$FollowActionType;->Follow:Ltv/periscope/android/api/FollowAction$FollowActionType;

    aput-object v1, v0, v3

    sget-object v1, Ltv/periscope/android/api/FollowAction$FollowActionType;->Unfollow:Ltv/periscope/android/api/FollowAction$FollowActionType;

    aput-object v1, v0, v4

    sget-object v1, Ltv/periscope/android/api/FollowAction$FollowActionType;->Block:Ltv/periscope/android/api/FollowAction$FollowActionType;

    aput-object v1, v0, v5

    sget-object v1, Ltv/periscope/android/api/FollowAction$FollowActionType;->Unblock:Ltv/periscope/android/api/FollowAction$FollowActionType;

    aput-object v1, v0, v6

    sget-object v1, Ltv/periscope/android/api/FollowAction$FollowActionType;->Mute:Ltv/periscope/android/api/FollowAction$FollowActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ltv/periscope/android/api/FollowAction$FollowActionType;->Unmute:Ltv/periscope/android/api/FollowAction$FollowActionType;

    aput-object v2, v0, v1

    sput-object v0, Ltv/periscope/android/api/FollowAction$FollowActionType;->$VALUES:[Ltv/periscope/android/api/FollowAction$FollowActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/android/api/FollowAction$FollowActionType;
    .locals 1

    .prologue
    .line 5
    const-class v0, Ltv/periscope/android/api/FollowAction$FollowActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/FollowAction$FollowActionType;

    return-object v0
.end method

.method public static values()[Ltv/periscope/android/api/FollowAction$FollowActionType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Ltv/periscope/android/api/FollowAction$FollowActionType;->$VALUES:[Ltv/periscope/android/api/FollowAction$FollowActionType;

    invoke-virtual {v0}, [Ltv/periscope/android/api/FollowAction$FollowActionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/android/api/FollowAction$FollowActionType;

    return-object v0
.end method
