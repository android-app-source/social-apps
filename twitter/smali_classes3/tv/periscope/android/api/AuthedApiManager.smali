.class public abstract Ltv/periscope/android/api/AuthedApiManager;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/api/ApiManager;


# instance fields
.field private final mApiService:Ltv/periscope/android/api/ApiService;

.field protected final mBackendServiceManager:Lcym;

.field protected final mBroadcastCache:Lcyn;

.field private final mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

.field protected final mContext:Landroid/content/Context;

.field protected final mEventBus:Lde/greenrobot/event/c;

.field protected final mLocalEventBus:Lde/greenrobot/event/c;

.field private final mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

.field protected final mSessionCache:Ltv/periscope/android/session/a;

.field private final mSignerService:Ltv/periscope/android/signer/SignerService;

.field private final mUserCache:Lcyw;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lde/greenrobot/event/c;Lcyw;Lcyn;Ltv/periscope/android/session/a;Ltv/periscope/android/api/ApiService;Ltv/periscope/android/signer/SignerService;Ltv/periscope/android/api/service/channels/ChannelsService;Ltv/periscope/android/api/service/safety/SafetyService;Lcym;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Ltv/periscope/android/api/AuthedApiManager;->mContext:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Ltv/periscope/android/api/AuthedApiManager;->mEventBus:Lde/greenrobot/event/c;

    .line 62
    iput-object p3, p0, Ltv/periscope/android/api/AuthedApiManager;->mUserCache:Lcyw;

    .line 63
    iput-object p5, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    .line 64
    iput-object p4, p0, Ltv/periscope/android/api/AuthedApiManager;->mBroadcastCache:Lcyn;

    .line 65
    invoke-static {}, Lde/greenrobot/event/c;->b()Lde/greenrobot/event/d;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lde/greenrobot/event/d;->a(Z)Lde/greenrobot/event/d;

    move-result-object v0

    invoke-virtual {v0}, Lde/greenrobot/event/d;->a()Lde/greenrobot/event/c;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mLocalEventBus:Lde/greenrobot/event/c;

    .line 66
    iput-object p6, p0, Ltv/periscope/android/api/AuthedApiManager;->mApiService:Ltv/periscope/android/api/ApiService;

    .line 67
    iput-object p7, p0, Ltv/periscope/android/api/AuthedApiManager;->mSignerService:Ltv/periscope/android/signer/SignerService;

    .line 68
    iput-object p8, p0, Ltv/periscope/android/api/AuthedApiManager;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    .line 69
    iput-object p9, p0, Ltv/periscope/android/api/AuthedApiManager;->mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

    .line 70
    iput-object p10, p0, Ltv/periscope/android/api/AuthedApiManager;->mBackendServiceManager:Lcym;

    .line 71
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mBackendServiceManager:Lcym;

    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mLocalEventBus:Lde/greenrobot/event/c;

    invoke-virtual {v0, v1}, Lcym;->a(Lde/greenrobot/event/c;)V

    .line 72
    return-void
.end method

.method private buildPlaybackMeta(Ljava/lang/String;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ltv/periscope/android/api/PlaybackMetaRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ltv/periscope/android/api/ChatStats;",
            ")",
            "Ltv/periscope/android/api/PlaybackMetaRequest;"
        }
    .end annotation

    .prologue
    .line 524
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->getVersionData()Ljava/util/HashMap;

    move-result-object v0

    .line 525
    invoke-virtual {p2, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 526
    new-instance v0, Ltv/periscope/android/api/PlaybackMetaRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/PlaybackMetaRequest;-><init>()V

    .line 527
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/PlaybackMetaRequest;->cookie:Ljava/lang/String;

    .line 528
    iput-object p1, v0, Ltv/periscope/android/api/PlaybackMetaRequest;->broadcastId:Ljava/lang/String;

    .line 529
    iput-object p2, v0, Ltv/periscope/android/api/PlaybackMetaRequest;->stats:Ljava/util/HashMap;

    .line 530
    iput-object p3, v0, Ltv/periscope/android/api/PlaybackMetaRequest;->chatStats:Ltv/periscope/android/api/ChatStats;

    .line 531
    return-object v0
.end method

.method private createBundleForGetBroadcast(Ljava/util/ArrayList;ZLtv/periscope/android/event/ApiEvent$Type;)Landroid/os/Bundle;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Ltv/periscope/android/event/ApiEvent$Type;",
            ")",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 388
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 389
    const-string/jumbo v1, "extra_ids"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 390
    const-string/jumbo v1, "e_only_public_publish"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 391
    const-string/jumbo v1, "e_event_type"

    invoke-virtual {p3}, Ltv/periscope/android/event/ApiEvent$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    return-object v0
.end method


# virtual methods
.method public activeJuror(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 915
    new-instance v0, Ltv/periscope/android/api/service/safety/ActiveJurorRequest;

    invoke-direct {v0, p1}, Ltv/periscope/android/api/service/safety/ActiveJurorRequest;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lorg/parceler/c;->a(Ljava/lang/Object;)Landroid/os/Parcelable;

    move-result-object v0

    .line 916
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 917
    const-string/jumbo v2, "e_service_auth_token"

    iget-object v3, p0, Ltv/periscope/android/api/AuthedApiManager;->mBackendServiceManager:Lcym;

    sget-object v4, Ltv/periscope/android/api/BackendServiceName;->SAFETY:Ltv/periscope/android/api/BackendServiceName;

    invoke-virtual {v3, v4}, Lcym;->b(Ltv/periscope/android/api/BackendServiceName;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    const-string/jumbo v2, "tv.periscope.android.api.service.safety.EXTRA_ACTIVE_JUROR_REQUEST"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 919
    const/16 v0, 0x4b

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public associateDigitsAccount(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 133
    const-string/jumbo v1, "e_secret_key"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string/jumbo v1, "e_secret_token"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const/16 v1, 0x51

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public associateTweetWithBroadcast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 788
    new-instance v0, Ltv/periscope/android/api/AssociateTweetWithBroadcastRequest;

    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    .line 789
    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, p1, p2, v2}, Ltv/periscope/android/api/AssociateTweetWithBroadcastRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 790
    const/16 v1, 0x68

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public block(Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/Message;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 859
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mUserCache:Lcyw;

    invoke-interface {v0, p1}, Lcyw;->j(Ljava/lang/String;)V

    .line 860
    new-instance v1, Ltv/periscope/android/api/BlockRequest;

    invoke-direct {v1}, Ltv/periscope/android/api/BlockRequest;-><init>()V

    .line 861
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v0}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Ltv/periscope/android/api/BlockRequest;->cookie:Ljava/lang/String;

    .line 862
    iput-object p1, v1, Ltv/periscope/android/api/BlockRequest;->userId:Ljava/lang/String;

    .line 863
    iput-object p2, v1, Ltv/periscope/android/api/BlockRequest;->broadcastId:Ljava/lang/String;

    .line 864
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ltv/periscope/model/chat/Message;->J()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, v1, Ltv/periscope/android/api/BlockRequest;->chatmanReason:Ljava/lang/String;

    .line 865
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->eventHandler()Ltv/periscope/android/api/DefaultAuthedEventHandler;

    move-result-object v0

    sget-object v2, Ltv/periscope/android/api/FollowAction$FollowActionType;->Block:Ltv/periscope/android/api/FollowAction$FollowActionType;

    invoke-virtual {v0, p1, v2, v1}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->addFollowAction(Ljava/lang/String;Ltv/periscope/android/api/FollowAction$FollowActionType;Ltv/periscope/android/api/PsRequest;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 866
    invoke-virtual {p0, v1}, Ltv/periscope/android/api/AuthedApiManager;->executeBlock(Ltv/periscope/android/api/BlockRequest;)Ljava/lang/String;

    move-result-object v0

    .line 868
    :goto_1
    return-object v0

    .line 864
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 868
    :cond_1
    invoke-static {}, Ltv/periscope/android/util/ak;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public broadcastMeta(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ltv/periscope/android/api/ChatStats;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 538
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->getVersionData()Ljava/util/HashMap;

    move-result-object v0

    .line 539
    invoke-virtual {p2, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 541
    new-instance v0, Ltv/periscope/android/api/BroadcastMetaRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/BroadcastMetaRequest;-><init>()V

    .line 542
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/BroadcastMetaRequest;->cookie:Ljava/lang/String;

    .line 543
    iput-object p1, v0, Ltv/periscope/android/api/BroadcastMetaRequest;->broadcastId:Ljava/lang/String;

    .line 544
    iput-object p2, v0, Ltv/periscope/android/api/BroadcastMetaRequest;->stats:Ljava/util/HashMap;

    .line 545
    iput-object p3, v0, Ltv/periscope/android/api/BroadcastMetaRequest;->behaviorStats:Ljava/util/HashMap;

    .line 546
    iput-object p4, v0, Ltv/periscope/android/api/BroadcastMetaRequest;->chatStats:Ltv/periscope/android/api/ChatStats;

    .line 548
    const/16 v1, 0x36

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public broadcastSearch(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 336
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 337
    const-string/jumbo v0, "extra_query"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    const/16 v1, 0x45

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;IJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public clearHistoryBroadcastFeed(Ljava/lang/Long;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 670
    new-instance v0, Ltv/periscope/android/api/ClearHistoryBroadcastFeedRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/ClearHistoryBroadcastFeedRequest;-><init>()V

    .line 671
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/ClearHistoryBroadcastFeedRequest;->cookie:Ljava/lang/String;

    .line 673
    if-eqz p1, :cond_0

    .line 674
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 675
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 676
    invoke-virtual {v1, v4}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/ClearHistoryBroadcastFeedRequest;->time:Ljava/lang/String;

    .line 678
    :cond_0
    const/16 v1, 0x67

    invoke-virtual {p0, v1, v0, v4}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public createBroadcast(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 344
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 347
    const-string/jumbo v1, "extra_width"

    const/16 v2, 0x140

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 348
    const-string/jumbo v1, "extra_height"

    const/16 v2, 0x238

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 349
    const-string/jumbo v1, "e_region"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const-string/jumbo v1, "e_has_moderation"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 351
    const-string/jumbo v1, "persistent"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 352
    const/16 v1, 0x4e

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public createExternalEncoder(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 925
    new-instance v0, Ltv/periscope/android/api/CreateExternalEncoderRequest;

    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ltv/periscope/android/api/CreateExternalEncoderRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    const/16 v1, 0x6a

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deactivateAccount()V
    .locals 2

    .prologue
    .line 228
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 229
    const/16 v1, 0x4f

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    .line 230
    return-void
.end method

.method public decreaseBroadcastRank(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 310
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 311
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const-string/jumbo v1, "e_decrease_rank"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 313
    const/16 v1, 0x3a

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deleteBroadcast(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 436
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 437
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const/16 v1, 0x2b

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deleteExternalEncoder(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 940
    new-instance v0, Ltv/periscope/android/api/DeleteExternalEncoderRequest;

    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ltv/periscope/android/api/DeleteExternalEncoderRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    const/16 v1, 0x6c

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public endBroadcast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 423
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mBroadcastCache:Lcyn;

    invoke-interface {v0, p1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mBroadcastCache:Lcyn;

    invoke-interface {v0, p1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v0, v1}, Ltv/periscope/model/p;->a(Z)V

    .line 425
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mBroadcastCache:Lcyn;

    invoke-interface {v0, p1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v0, v1}, Ltv/periscope/model/p;->b(Z)V

    .line 428
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 429
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const-string/jumbo v1, "e_logger_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    const/16 v1, 0x1f

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public endWatching(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;
    .locals 3

    .prologue
    .line 489
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 490
    const-string/jumbo v1, "e_session_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    const-string/jumbo v1, "e_logger_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    const-string/jumbo v1, "e_num_hearts"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 493
    const-string/jumbo v1, "e_n_comments"

    invoke-virtual {v0, v1, p5, p6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 494
    const/16 v1, 0x1c

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract eventHandler()Ltv/periscope/android/api/DefaultAuthedEventHandler;
.end method

.method execute(ILandroid/os/Bundle;)Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 592
    invoke-static {}, Ltv/periscope/android/util/ak;->a()Ljava/lang/String;

    move-result-object v0

    .line 594
    new-instance v1, Ltv/periscope/android/api/ApiRunnable$Builder;

    invoke-direct {v1}, Ltv/periscope/android/api/ApiRunnable$Builder;-><init>()V

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mLocalEventBus:Lde/greenrobot/event/c;

    .line 595
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->eventBus(Lde/greenrobot/event/c;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mApiService:Ltv/periscope/android/api/ApiService;

    .line 596
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->service(Ltv/periscope/android/api/ApiService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mSignerService:Ltv/periscope/android/signer/SignerService;

    .line 597
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->signer(Ltv/periscope/android/signer/SignerService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    .line 598
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->channelService(Ltv/periscope/android/api/service/channels/ChannelsService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

    .line 599
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->safetyService(Ltv/periscope/android/api/service/safety/SafetyService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 600
    invoke-virtual {v1, p1}, Ltv/periscope/android/api/ApiRunnable$Builder;->actionCode(I)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 601
    invoke-virtual {v1, v0}, Ltv/periscope/android/api/ApiRunnable$Builder;->requestId(Ljava/lang/String;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 602
    invoke-virtual {v1, p2}, Ltv/periscope/android/api/ApiRunnable$Builder;->bundle(Landroid/os/Bundle;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 603
    invoke-virtual {v1}, Ltv/periscope/android/api/ApiRunnable$Builder;->build()Ltv/periscope/android/api/ApiRunnable;

    move-result-object v1

    .line 594
    invoke-virtual {p0, v1}, Ltv/periscope/android/api/AuthedApiManager;->queueAndExecuteRequest(Ltv/periscope/android/api/ApiRunnable;)V

    .line 605
    return-object v0
.end method

.method protected execute(ILandroid/os/Bundle;IJ)Ljava/lang/String;
    .locals 4

    .prologue
    .line 552
    invoke-static {}, Ltv/periscope/android/util/ak;->a()Ljava/lang/String;

    move-result-object v0

    .line 554
    new-instance v1, Ltv/periscope/android/api/ApiRunnable$Builder;

    invoke-direct {v1}, Ltv/periscope/android/api/ApiRunnable$Builder;-><init>()V

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mLocalEventBus:Lde/greenrobot/event/c;

    .line 555
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->eventBus(Lde/greenrobot/event/c;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mApiService:Ltv/periscope/android/api/ApiService;

    .line 556
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->service(Ltv/periscope/android/api/ApiService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mSignerService:Ltv/periscope/android/signer/SignerService;

    .line 557
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->signer(Ltv/periscope/android/signer/SignerService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    .line 558
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->channelService(Ltv/periscope/android/api/service/channels/ChannelsService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

    .line 559
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->safetyService(Ltv/periscope/android/api/service/safety/SafetyService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 560
    invoke-virtual {v1, p1}, Ltv/periscope/android/api/ApiRunnable$Builder;->actionCode(I)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 561
    invoke-virtual {v1, v0}, Ltv/periscope/android/api/ApiRunnable$Builder;->requestId(Ljava/lang/String;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 562
    invoke-virtual {v1, p2}, Ltv/periscope/android/api/ApiRunnable$Builder;->bundle(Landroid/os/Bundle;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 563
    invoke-virtual {v1, p3}, Ltv/periscope/android/api/ApiRunnable$Builder;->numRetries(I)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 564
    invoke-virtual {v1, p4, p5}, Ltv/periscope/android/api/ApiRunnable$Builder;->backoffInterval(J)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 565
    invoke-virtual {v1}, Ltv/periscope/android/api/ApiRunnable$Builder;->build()Ltv/periscope/android/api/ApiRunnable;

    move-result-object v1

    .line 554
    invoke-virtual {p0, v1}, Ltv/periscope/android/api/AuthedApiManager;->queueAndExecuteRequest(Ltv/periscope/android/api/ApiRunnable;)V

    .line 567
    return-object v0
.end method

.method execute(ILandroid/os/Bundle;IJZ)Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 572
    invoke-static {}, Ltv/periscope/android/util/ak;->a()Ljava/lang/String;

    move-result-object v0

    .line 573
    const-string/jumbo v1, "e_background"

    invoke-virtual {p2, v1, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 575
    new-instance v1, Ltv/periscope/android/api/ApiRunnable$Builder;

    invoke-direct {v1}, Ltv/periscope/android/api/ApiRunnable$Builder;-><init>()V

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mLocalEventBus:Lde/greenrobot/event/c;

    .line 576
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->eventBus(Lde/greenrobot/event/c;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mApiService:Ltv/periscope/android/api/ApiService;

    .line 577
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->service(Ltv/periscope/android/api/ApiService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mSignerService:Ltv/periscope/android/signer/SignerService;

    .line 578
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->signer(Ltv/periscope/android/signer/SignerService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

    .line 579
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->safetyService(Ltv/periscope/android/api/service/safety/SafetyService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 580
    invoke-virtual {v1, p1}, Ltv/periscope/android/api/ApiRunnable$Builder;->actionCode(I)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 581
    invoke-virtual {v1, v0}, Ltv/periscope/android/api/ApiRunnable$Builder;->requestId(Ljava/lang/String;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 582
    invoke-virtual {v1, p2}, Ltv/periscope/android/api/ApiRunnable$Builder;->bundle(Landroid/os/Bundle;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 583
    invoke-virtual {v1, p3}, Ltv/periscope/android/api/ApiRunnable$Builder;->numRetries(I)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 584
    invoke-virtual {v1, p4, p5}, Ltv/periscope/android/api/ApiRunnable$Builder;->backoffInterval(J)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 585
    invoke-virtual {v1}, Ltv/periscope/android/api/ApiRunnable$Builder;->build()Ltv/periscope/android/api/ApiRunnable;

    move-result-object v1

    .line 575
    invoke-virtual {p0, v1}, Ltv/periscope/android/api/AuthedApiManager;->queueAndExecuteRequest(Ltv/periscope/android/api/ApiRunnable;)V

    .line 587
    return-object v0
.end method

.method execute(ILandroid/os/Bundle;Z)Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 610
    invoke-static {}, Ltv/periscope/android/util/ak;->a()Ljava/lang/String;

    move-result-object v0

    .line 611
    const-string/jumbo v1, "e_background"

    invoke-virtual {p2, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 613
    new-instance v1, Ltv/periscope/android/api/ApiRunnable$Builder;

    invoke-direct {v1}, Ltv/periscope/android/api/ApiRunnable$Builder;-><init>()V

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mLocalEventBus:Lde/greenrobot/event/c;

    .line 614
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->eventBus(Lde/greenrobot/event/c;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mApiService:Ltv/periscope/android/api/ApiService;

    .line 615
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->service(Ltv/periscope/android/api/ApiService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mSignerService:Ltv/periscope/android/signer/SignerService;

    .line 616
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->signer(Ltv/periscope/android/signer/SignerService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

    .line 617
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->safetyService(Ltv/periscope/android/api/service/safety/SafetyService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 618
    invoke-virtual {v1, p1}, Ltv/periscope/android/api/ApiRunnable$Builder;->actionCode(I)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 619
    invoke-virtual {v1, v0}, Ltv/periscope/android/api/ApiRunnable$Builder;->requestId(Ljava/lang/String;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 620
    invoke-virtual {v1, p2}, Ltv/periscope/android/api/ApiRunnable$Builder;->bundle(Landroid/os/Bundle;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 621
    invoke-virtual {v1}, Ltv/periscope/android/api/ApiRunnable$Builder;->build()Ltv/periscope/android/api/ApiRunnable;

    move-result-object v1

    .line 613
    invoke-virtual {p0, v1}, Ltv/periscope/android/api/AuthedApiManager;->queueAndExecuteRequest(Ltv/periscope/android/api/ApiRunnable;)V

    .line 623
    return-object v0
.end method

.method protected execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 648
    invoke-static {}, Ltv/periscope/android/util/ak;->a()Ljava/lang/String;

    move-result-object v0

    .line 649
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 650
    const-string/jumbo v2, "e_background"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 652
    new-instance v2, Ltv/periscope/android/api/ApiRunnable$Builder;

    invoke-direct {v2}, Ltv/periscope/android/api/ApiRunnable$Builder;-><init>()V

    iget-object v3, p0, Ltv/periscope/android/api/AuthedApiManager;->mLocalEventBus:Lde/greenrobot/event/c;

    .line 653
    invoke-virtual {v2, v3}, Ltv/periscope/android/api/ApiRunnable$Builder;->eventBus(Lde/greenrobot/event/c;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/api/AuthedApiManager;->mApiService:Ltv/periscope/android/api/ApiService;

    .line 654
    invoke-virtual {v2, v3}, Ltv/periscope/android/api/ApiRunnable$Builder;->service(Ltv/periscope/android/api/ApiService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/api/AuthedApiManager;->mSignerService:Ltv/periscope/android/signer/SignerService;

    .line 655
    invoke-virtual {v2, v3}, Ltv/periscope/android/api/ApiRunnable$Builder;->signer(Ltv/periscope/android/signer/SignerService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/api/AuthedApiManager;->mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

    .line 656
    invoke-virtual {v2, v3}, Ltv/periscope/android/api/ApiRunnable$Builder;->safetyService(Ltv/periscope/android/api/service/safety/SafetyService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v2

    .line 657
    invoke-virtual {v2, v1}, Ltv/periscope/android/api/ApiRunnable$Builder;->bundle(Landroid/os/Bundle;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 658
    invoke-virtual {v1, p1}, Ltv/periscope/android/api/ApiRunnable$Builder;->actionCode(I)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 659
    invoke-virtual {v1, v0}, Ltv/periscope/android/api/ApiRunnable$Builder;->requestId(Ljava/lang/String;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 660
    invoke-virtual {v1, p2}, Ltv/periscope/android/api/ApiRunnable$Builder;->request(Ltv/periscope/android/api/ApiRequest;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 661
    invoke-virtual {v1}, Ltv/periscope/android/api/ApiRunnable$Builder;->build()Ltv/periscope/android/api/ApiRunnable;

    move-result-object v1

    .line 652
    invoke-virtual {p0, v1}, Ltv/periscope/android/api/AuthedApiManager;->queueAndExecuteRequest(Ltv/periscope/android/api/ApiRunnable;)V

    .line 663
    return-object v0
.end method

.method protected execute(Lde/greenrobot/event/c;ILandroid/os/Bundle;IJ)Ljava/lang/String;
    .locals 3

    .prologue
    .line 628
    invoke-static {}, Ltv/periscope/android/util/ak;->a()Ljava/lang/String;

    move-result-object v0

    .line 630
    new-instance v1, Ltv/periscope/android/api/ApiRunnable$Builder;

    invoke-direct {v1}, Ltv/periscope/android/api/ApiRunnable$Builder;-><init>()V

    .line 631
    invoke-virtual {v1, p1}, Ltv/periscope/android/api/ApiRunnable$Builder;->eventBus(Lde/greenrobot/event/c;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mApiService:Ltv/periscope/android/api/ApiService;

    .line 632
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->service(Ltv/periscope/android/api/ApiService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mSignerService:Ltv/periscope/android/signer/SignerService;

    .line 633
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->signer(Ltv/periscope/android/signer/SignerService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mChannelsService:Ltv/periscope/android/api/service/channels/ChannelsService;

    .line 634
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->channelService(Ltv/periscope/android/api/service/channels/ChannelsService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mSafetyService:Ltv/periscope/android/api/service/safety/SafetyService;

    .line 635
    invoke-virtual {v1, v2}, Ltv/periscope/android/api/ApiRunnable$Builder;->safetyService(Ltv/periscope/android/api/service/safety/SafetyService;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 636
    invoke-virtual {v1, p2}, Ltv/periscope/android/api/ApiRunnable$Builder;->actionCode(I)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 637
    invoke-virtual {v1, v0}, Ltv/periscope/android/api/ApiRunnable$Builder;->requestId(Ljava/lang/String;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 638
    invoke-virtual {v1, p3}, Ltv/periscope/android/api/ApiRunnable$Builder;->bundle(Landroid/os/Bundle;)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 639
    invoke-virtual {v1, p4}, Ltv/periscope/android/api/ApiRunnable$Builder;->numRetries(I)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 640
    invoke-virtual {v1, p5, p6}, Ltv/periscope/android/api/ApiRunnable$Builder;->backoffInterval(J)Ltv/periscope/android/api/ApiRunnable$Builder;

    move-result-object v1

    .line 641
    invoke-virtual {v1}, Ltv/periscope/android/api/ApiRunnable$Builder;->build()Ltv/periscope/android/api/ApiRunnable;

    move-result-object v1

    .line 630
    invoke-virtual {p0, v1}, Ltv/periscope/android/api/AuthedApiManager;->queueAndExecuteRequest(Ltv/periscope/android/api/ApiRunnable;)V

    .line 643
    return-object v0
.end method

.method executeBlock(Ltv/periscope/android/api/BlockRequest;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 873
    const/16 v0, 0x23

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method executeFollow(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 804
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 805
    const-string/jumbo v1, "e_user_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    const/16 v1, 0x8

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method executeMute(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 836
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 837
    const-string/jumbo v1, "e_user_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    const/16 v1, 0x37

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method executeUnblock(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 887
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 888
    const-string/jumbo v1, "e_user_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    const/16 v1, 0x24

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method executeUnfollow(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 820
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 821
    const-string/jumbo v1, "e_user_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    const/16 v1, 0x9

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method executeUnmute(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 852
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 853
    const-string/jumbo v1, "e_user_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    const/16 v1, 0x38

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public follow(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 795
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mUserCache:Lcyw;

    invoke-interface {v0, p1}, Lcyw;->f(Ljava/lang/String;)V

    .line 796
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->eventHandler()Ltv/periscope/android/api/DefaultAuthedEventHandler;

    move-result-object v0

    sget-object v1, Ltv/periscope/android/api/FollowAction$FollowActionType;->Follow:Ltv/periscope/android/api/FollowAction$FollowActionType;

    invoke-virtual {v0, p1, v1}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->addFollowAction(Ljava/lang/String;Ltv/periscope/android/api/FollowAction$FollowActionType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 797
    invoke-virtual {p0, p1}, Ltv/periscope/android/api/AuthedApiManager;->executeFollow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 799
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ltv/periscope/android/util/ak;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getAccessChat(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 261
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 262
    const-string/jumbo v1, "e_token"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string/jumbo v1, "e_viewer_moderation"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 264
    const/16 v1, 0x42

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAccessChatNoRetry(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 269
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 270
    const-string/jumbo v0, "e_token"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const-string/jumbo v0, "e_viewer_moderation"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 272
    const/16 v1, 0x42

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;IJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAccessVideo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 253
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 254
    const-string/jumbo v1, "e_token"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const/16 v1, 0x41

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApiService()Ltv/periscope/android/api/ApiService;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mApiService:Ltv/periscope/android/api/ApiService;

    return-object v0
.end method

.method public getAuthorizeTokenForBackendService(Ltv/periscope/android/api/BackendServiceName;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 114
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 115
    const-string/jumbo v1, "e_service_name"

    invoke-virtual {p1}, Ltv/periscope/android/api/BackendServiceName;->getServiceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string/jumbo v1, "e_background"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 117
    const/16 v1, 0x47

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBlocked()Ljava/lang/String;
    .locals 2

    .prologue
    .line 248
    const/16 v0, 0x25

    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastForExternalEncoder(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 956
    new-instance v0, Ltv/periscope/android/api/GetBroadcastForExternalEncoderRequest;

    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    .line 957
    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ltv/periscope/android/api/GetBroadcastForExternalEncoderRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 958
    const/16 v1, 0x6e

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastIdForShareToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 684
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 685
    const-string/jumbo v1, "e_token"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    const/16 v1, 0x27

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastTrailer(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 756
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 757
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    const/16 v1, 0x52

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;IJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastViewers(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 318
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 319
    const-string/jumbo v1, "e_user_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    const/16 v1, 0x14

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcasts(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 375
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ltv/periscope/android/api/AuthedApiManager;->getBroadcasts(Ljava/util/ArrayList;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcasts(Ljava/util/ArrayList;Z)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 380
    sget-object v0, Ltv/periscope/android/event/ApiEvent$Type;->B:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/api/AuthedApiManager;->createBundleForGetBroadcast(Ljava/util/ArrayList;ZLtv/periscope/android/event/ApiEvent$Type;)Landroid/os/Bundle;

    move-result-object v2

    .line 381
    const/16 v1, 0x13

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;IJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastsByPolling(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 368
    sget-object v0, Ltv/periscope/android/event/ApiEvent$Type;->C:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-direct {p0, p1, v3, v0}, Ltv/periscope/android/api/AuthedApiManager;->createBundleForGetBroadcast(Ljava/util/ArrayList;ZLtv/periscope/android/event/ApiEvent$Type;)Landroid/os/Bundle;

    move-result-object v2

    .line 369
    const/16 v1, 0x13

    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;IJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExternalEncoders()Ljava/lang/String;
    .locals 3

    .prologue
    .line 948
    new-instance v0, Ltv/periscope/android/api/PsRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 949
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    .line 950
    const/16 v1, 0x6d

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFollowers()Ljava/lang/String;
    .locals 3

    .prologue
    .line 196
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 197
    const-string/jumbo v1, "e_my_user_id"

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mUserCache:Lcyw;

    invoke-interface {v2}, Lcyw;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFollowersById(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 241
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 242
    const-string/jumbo v1, "e_user_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const/16 v1, 0xc

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFollowing()Ljava/lang/String;
    .locals 3

    .prologue
    .line 203
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 204
    const-string/jumbo v1, "e_my_user_id"

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mUserCache:Lcyw;

    invoke-interface {v2}, Lcyw;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const/4 v1, 0x7

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFollowingById(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 235
    const-string/jumbo v1, "e_user_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const/16 v1, 0xb

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFollowingIdsOnly()Ljava/lang/String;
    .locals 3

    .prologue
    .line 210
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 211
    const-string/jumbo v1, "e_my_user_id"

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mUserCache:Lcyw;

    invoke-interface {v2}, Lcyw;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const/16 v1, 0x5f

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGlobalMap(FFFF)Ljava/lang/String;
    .locals 6

    .prologue
    .line 357
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 358
    const-string/jumbo v0, "e_point_1_lat"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 359
    const-string/jumbo v0, "e_point_1_lng"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 360
    const-string/jumbo v0, "e_point_2_lat"

    invoke-virtual {v2, v0, p3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 361
    const-string/jumbo v0, "e_point_2_lng"

    invoke-virtual {v2, v0, p4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 362
    const/16 v1, 0x34

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;IJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMutualFollows()Ljava/lang/String;
    .locals 2

    .prologue
    .line 217
    const/16 v0, 0x3c

    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMyUserBroadcasts()V
    .locals 3

    .prologue
    .line 699
    new-instance v0, Ltv/periscope/android/api/UserBroadcastsRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/UserBroadcastsRequest;-><init>()V

    .line 700
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/UserBroadcastsRequest;->cookie:Ljava/lang/String;

    .line 701
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mUserCache:Lcyw;

    invoke-interface {v1}, Lcyw;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/UserBroadcastsRequest;->userId:Ljava/lang/String;

    .line 702
    const/4 v1, 0x1

    iput-boolean v1, v0, Ltv/periscope/android/api/UserBroadcastsRequest;->all:Z

    .line 703
    const/16 v1, 0x2c

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    .line 704
    return-void
.end method

.method public getRecentlyWatchedBroadcasts()V
    .locals 3

    .prologue
    .line 724
    new-instance v0, Ltv/periscope/android/api/PsRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 725
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/PsRequest;->cookie:Ljava/lang/String;

    .line 726
    const/16 v1, 0x66

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    .line 727
    return-void
.end method

.method public getSettings()Ljava/lang/String;
    .locals 2

    .prologue
    .line 451
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 452
    const/16 v1, 0x1e

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUser()Ljava/lang/String;
    .locals 3

    .prologue
    .line 164
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 165
    const-string/jumbo v1, "e_my_user_id"

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mUserCache:Lcyw;

    invoke-interface {v2}, Lcyw;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserBroadcastsByUserId(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 708
    new-instance v0, Ltv/periscope/android/api/UserBroadcastsRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/UserBroadcastsRequest;-><init>()V

    .line 709
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/UserBroadcastsRequest;->cookie:Ljava/lang/String;

    .line 710
    iput-object p1, v0, Ltv/periscope/android/api/UserBroadcastsRequest;->userId:Ljava/lang/String;

    .line 711
    const/16 v1, 0x2c

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    .line 712
    return-void
.end method

.method public getUserBroadcastsByUsername(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 716
    new-instance v0, Ltv/periscope/android/api/UserBroadcastsRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/UserBroadcastsRequest;-><init>()V

    .line 717
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/UserBroadcastsRequest;->cookie:Ljava/lang/String;

    .line 718
    iput-object p1, v0, Ltv/periscope/android/api/UserBroadcastsRequest;->username:Ljava/lang/String;

    .line 719
    const/16 v1, 0x2c

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    .line 720
    return-void
.end method

.method public getUserById(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 172
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 173
    const-string/jumbo v1, "e_user_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const/16 v1, 0xa

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserByUsername(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 180
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 181
    const-string/jumbo v1, "e_username"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const/16 v1, 0x3d

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserStats(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 188
    new-instance v0, Ltv/periscope/android/api/GetUserStatsRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/GetUserStatsRequest;-><init>()V

    .line 189
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/GetUserStatsRequest;->cookie:Ljava/lang/String;

    .line 190
    iput-object p1, v0, Ltv/periscope/android/api/GetUserStatsRequest;->userId:Ljava/lang/String;

    .line 191
    const/16 v1, 0x5a

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getVersionData()Ljava/util/HashMap;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 500
    const-string/jumbo v1, "platform"

    const-string/jumbo v2, "Android"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    const-string/jumbo v1, "platform_version"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    const-string/jumbo v1, "app_version"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/api/AuthedApiManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Ltv/periscope/android/util/ak;->b(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 503
    const-string/jumbo v1, "device"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    return-object v0
.end method

.method public hello()Ljava/lang/String;
    .locals 3

    .prologue
    .line 150
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 151
    const-string/jumbo v2, "e_locale"

    invoke-static {}, Ltv/periscope/android/util/o;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 152
    const/16 v0, 0x3b

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public increaseBroadcastRank(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 302
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 303
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const-string/jumbo v1, "e_increase_rank"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 305
    const/16 v1, 0x3a

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public livePlaybackMeta(Ljava/lang/String;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ltv/periscope/android/api/ChatStats;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 510
    invoke-direct {p0, p1, p2, p3}, Ltv/periscope/android/api/AuthedApiManager;->buildPlaybackMeta(Ljava/lang/String;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ltv/periscope/android/api/PlaybackMetaRequest;

    move-result-object v0

    .line 511
    const/16 v1, 0x35

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/session/Session$Type;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 89
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 90
    const-string/jumbo v1, "e_secret_key"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string/jumbo v1, "e_secret_token"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string/jumbo v1, "e_username"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string/jumbo v1, "e_user_id"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string/jumbo v1, "e_phone_number"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string/jumbo v1, "e_install_id"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string/jumbo v1, "e_session_type"

    invoke-virtual {p7}, Ltv/periscope/android/session/Session$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string/jumbo v1, "e_time_zone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loginTwitterToken(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 103
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 104
    const-string/jumbo v1, "e_token"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string/jumbo v1, "e_install_id"

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Ltv/periscope/android/util/j;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string/jumbo v1, "e_create_user"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 107
    const-string/jumbo v1, "e_time_zone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const/16 v1, 0x54

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public markBroadcastPersistent(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 740
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mBroadcastCache:Lcyn;

    invoke-interface {v0, p1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 741
    if-eqz v0, :cond_0

    .line 742
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Ltv/periscope/model/p;->a(I)V

    .line 745
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 746
    const-string/jumbo v0, "e_cookie"

    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    const-string/jumbo v0, "e_broadcast_id"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    const/16 v1, 0x4d

    const-wide/16 v4, 0x0

    move-object v0, p0

    move v6, v3

    invoke-virtual/range {v0 .. v6}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;IJZ)Ljava/lang/String;

    .line 751
    return-void
.end method

.method public mute(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 827
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mUserCache:Lcyw;

    invoke-interface {v0, p1}, Lcyw;->h(Ljava/lang/String;)V

    .line 828
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->eventHandler()Ltv/periscope/android/api/DefaultAuthedEventHandler;

    move-result-object v0

    sget-object v1, Ltv/periscope/android/api/FollowAction$FollowActionType;->Mute:Ltv/periscope/android/api/FollowAction$FollowActionType;

    invoke-virtual {v0, p1, v1}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->addFollowAction(Ljava/lang/String;Ltv/periscope/android/api/FollowAction$FollowActionType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 829
    invoke-virtual {p0, p1}, Ltv/periscope/android/api/AuthedApiManager;->executeMute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 831
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ltv/periscope/android/util/ak;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method newApiBundle()Landroid/os/Bundle;
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 157
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 158
    const-string/jumbo v1, "e_cookie"

    iget-object v2, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v2}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    return-object v0
.end method

.method public performUploadTest()V
    .locals 7

    .prologue
    .line 691
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 692
    const-string/jumbo v0, "e_file_dir"

    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    const/16 v1, 0x28

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;IJZ)Ljava/lang/String;

    .line 695
    return-void
.end method

.method public pingBroadcast(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 463
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 464
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    const-string/jumbo v1, "e_bit_rate"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 466
    const/16 v1, 0x20

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public pingWatching(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;
    .locals 3

    .prologue
    .line 479
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 480
    const-string/jumbo v1, "e_session_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    const-string/jumbo v1, "e_logger_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    const-string/jumbo v1, "e_num_hearts"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 483
    const-string/jumbo v1, "e_n_comments"

    invoke-virtual {v0, v1, p5, p6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 484
    const/16 v1, 0x1b

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public publishBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;ZFFZII)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;ZFFZII)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 407
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 408
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    const-string/jumbo v1, "e_title"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string/jumbo v1, "e_locked_ids"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 411
    const-string/jumbo v1, "e_locked_private_channel_ids"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 412
    const-string/jumbo v1, "e_has_loc"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 413
    const-string/jumbo v1, "e_lat"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 414
    const-string/jumbo v1, "e_long"

    invoke-virtual {v0, v1, p7}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 415
    const-string/jumbo v1, "e_following_only_chat"

    invoke-virtual {v0, v1, p8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 416
    const-string/jumbo v1, "e_bit_rate"

    invoke-virtual {v0, v1, p9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 417
    const-string/jumbo v1, "e_camera_rotation"

    invoke-virtual {v0, v1, p10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 418
    const/16 v1, 0x1a

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract queueAndExecuteRequest(Ltv/periscope/android/api/ApiRunnable;)V
.end method

.method protected registerApiEventHandler(Ltv/periscope/android/api/ApiEventHandler;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mLocalEventBus:Lde/greenrobot/event/c;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 76
    return-void
.end method

.method public replayPlaybackMeta(Ljava/lang/String;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ltv/periscope/android/api/ChatStats;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 517
    invoke-direct {p0, p1, p2, p3}, Ltv/periscope/android/api/AuthedApiManager;->buildPlaybackMeta(Ljava/lang/String;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ltv/periscope/android/api/PlaybackMetaRequest;

    move-result-object v0

    .line 518
    const/16 v1, 0x60

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public replayThumbnailPlaylist(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 397
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 398
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    const/16 v1, 0x3e

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public reportBroadcast(Ljava/lang/String;Ltv/periscope/model/AbuseType;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 294
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 295
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const-string/jumbo v1, "e_abuse_type"

    invoke-virtual {p2}, Ltv/periscope/model/AbuseType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const/16 v1, 0x29

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public reportComment(Ltv/periscope/model/chat/Message;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$ReportType;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 895
    new-instance v0, Ltv/periscope/android/api/service/safety/ReportCommentRequest;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->J()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2, p3}, Ltv/periscope/android/api/service/safety/ReportCommentRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$ReportType;)V

    invoke-static {v0}, Lorg/parceler/c;->a(Ljava/lang/Object;)Landroid/os/Parcelable;

    move-result-object v0

    .line 896
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 897
    const-string/jumbo v2, "e_service_auth_token"

    iget-object v3, p0, Ltv/periscope/android/api/AuthedApiManager;->mBackendServiceManager:Lcym;

    sget-object v4, Ltv/periscope/android/api/BackendServiceName;->SAFETY:Ltv/periscope/android/api/BackendServiceName;

    invoke-virtual {v3, v4}, Lcym;->b(Ltv/periscope/android/api/BackendServiceName;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    const-string/jumbo v2, "tv.periscope.android.api.service.safety.EXTRA_REPORT_COMMENT_REQUEST"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 899
    const/16 v0, 0x49

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public retweetBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 777
    new-instance v0, Ltv/periscope/android/api/TweetBroadcastRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/TweetBroadcastRequest;-><init>()V

    .line 778
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/TweetBroadcastRequest;->cookie:Ljava/lang/String;

    .line 779
    iput-object p1, v0, Ltv/periscope/android/api/TweetBroadcastRequest;->broadcastId:Ljava/lang/String;

    .line 780
    iput-object p2, v0, Ltv/periscope/android/api/TweetBroadcastRequest;->oauthToken:Ljava/lang/String;

    .line 781
    iput-object p3, v0, Ltv/periscope/android/api/TweetBroadcastRequest;->oauthTokenSecret:Ljava/lang/String;

    .line 782
    const/16 v1, 0x62

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setExternalEncoderName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 932
    new-instance v0, Ltv/periscope/android/api/SetExternalEncoderNameRequest;

    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Ltv/periscope/android/api/SetExternalEncoderNameRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    const/16 v1, 0x6b

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setProfileImage(Ljava/io/File;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 731
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 732
    const-string/jumbo v1, "e_file_dir"

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    const-string/jumbo v1, "e_cache_dir"

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    const/16 v1, 0x2d

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    .line 735
    return-void
.end method

.method public setSettings(Ltv/periscope/android/api/PsSettings;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 443
    new-instance v0, Ltv/periscope/android/api/SetSettingsRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/SetSettingsRequest;-><init>()V

    .line 444
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/SetSettingsRequest;->cookie:Ljava/lang/String;

    .line 445
    iput-object p1, v0, Ltv/periscope/android/api/SetSettingsRequest;->settings:Ltv/periscope/android/api/PsSettings;

    .line 446
    const/16 v1, 0x1d

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public shareBroadcast(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 286
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 287
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const-string/jumbo v1, "e_user_ids"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 289
    const/16 v1, 0x26

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public startWatching(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 278
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 279
    const-string/jumbo v1, "e_token"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const-string/jumbo v1, "e_autoplay"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 281
    const/16 v1, 0x43

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public supportedLanguages()Ljava/lang/String;
    .locals 2

    .prologue
    .line 457
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 458
    const/16 v1, 0x39

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public tweetBroadcastPublished(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 765
    new-instance v0, Ltv/periscope/android/api/TweetBroadcastRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/TweetBroadcastRequest;-><init>()V

    .line 766
    iget-object v1, p0, Ltv/periscope/android/api/AuthedApiManager;->mSessionCache:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/TweetBroadcastRequest;->cookie:Ljava/lang/String;

    .line 767
    iput-object p1, v0, Ltv/periscope/android/api/TweetBroadcastRequest;->broadcastId:Ljava/lang/String;

    .line 768
    iput-object p2, v0, Ltv/periscope/android/api/TweetBroadcastRequest;->oauthToken:Ljava/lang/String;

    .line 769
    iput-object p3, v0, Ltv/periscope/android/api/TweetBroadcastRequest;->oauthTokenSecret:Ljava/lang/String;

    .line 770
    const/16 v1, 0x61

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILtv/periscope/android/api/PsRequest;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unban()Ljava/lang/String;
    .locals 2

    .prologue
    .line 222
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 223
    const/16 v1, 0x3f

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unblock(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 878
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mUserCache:Lcyw;

    invoke-interface {v0, p1}, Lcyw;->k(Ljava/lang/String;)V

    .line 879
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->eventHandler()Ltv/periscope/android/api/DefaultAuthedEventHandler;

    move-result-object v0

    sget-object v1, Ltv/periscope/android/api/FollowAction$FollowActionType;->Block:Ltv/periscope/android/api/FollowAction$FollowActionType;

    invoke-virtual {v0, p1, v1}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->addFollowAction(Ljava/lang/String;Ltv/periscope/android/api/FollowAction$FollowActionType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 880
    invoke-virtual {p0, p1}, Ltv/periscope/android/api/AuthedApiManager;->executeUnblock(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 882
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ltv/periscope/android/util/ak;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public unfollow(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 811
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mUserCache:Lcyw;

    invoke-interface {v0, p1}, Lcyw;->g(Ljava/lang/String;)V

    .line 812
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->eventHandler()Ltv/periscope/android/api/DefaultAuthedEventHandler;

    move-result-object v0

    sget-object v1, Ltv/periscope/android/api/FollowAction$FollowActionType;->Unfollow:Ltv/periscope/android/api/FollowAction$FollowActionType;

    invoke-virtual {v0, p1, v1}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->addFollowAction(Ljava/lang/String;Ltv/periscope/android/api/FollowAction$FollowActionType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 813
    invoke-virtual {p0, p1}, Ltv/periscope/android/api/AuthedApiManager;->executeUnfollow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 815
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ltv/periscope/android/util/ak;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public unmute(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 843
    iget-object v0, p0, Ltv/periscope/android/api/AuthedApiManager;->mUserCache:Lcyw;

    invoke-interface {v0, p1}, Lcyw;->i(Ljava/lang/String;)V

    .line 844
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->eventHandler()Ltv/periscope/android/api/DefaultAuthedEventHandler;

    move-result-object v0

    sget-object v1, Ltv/periscope/android/api/FollowAction$FollowActionType;->Unmute:Ltv/periscope/android/api/FollowAction$FollowActionType;

    invoke-virtual {v0, p1, v1}, Ltv/periscope/android/api/DefaultAuthedEventHandler;->addFollowAction(Ljava/lang/String;Ltv/periscope/android/api/FollowAction$FollowActionType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 845
    invoke-virtual {p0, p1}, Ltv/periscope/android/api/AuthedApiManager;->executeUnmute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 847
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ltv/periscope/android/util/ak;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public uploadBroadcasterLogs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 471
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 472
    const-string/jumbo v1, "e_broadcast_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const-string/jumbo v1, "e_logger_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    const/16 v1, 0x69

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userSearch(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 327
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 328
    const-string/jumbo v0, "extra_query"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    const/16 v1, 0xf

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;IJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public validateUsername(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 123
    const-string/jumbo v1, "e_username"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string/jumbo v1, "e_secret_key"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string/jumbo v1, "e_secret_token"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public verifyUsername(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 140
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 141
    const-string/jumbo v1, "e_username"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string/jumbo v1, "e_display_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string/jumbo v1, "e_secret_key"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string/jumbo v1, "e_secret_token"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public vote(Ljava/lang/String;Ltv/periscope/model/chat/MessageType$VoteType;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 905
    new-instance v0, Ltv/periscope/android/api/service/safety/VoteRequest;

    invoke-direct {v0, p1, p2}, Ltv/periscope/android/api/service/safety/VoteRequest;-><init>(Ljava/lang/String;Ltv/periscope/model/chat/MessageType$VoteType;)V

    invoke-static {v0}, Lorg/parceler/c;->a(Ljava/lang/Object;)Landroid/os/Parcelable;

    move-result-object v0

    .line 906
    invoke-virtual {p0}, Ltv/periscope/android/api/AuthedApiManager;->newApiBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 907
    const-string/jumbo v2, "e_service_auth_token"

    iget-object v3, p0, Ltv/periscope/android/api/AuthedApiManager;->mBackendServiceManager:Lcym;

    sget-object v4, Ltv/periscope/android/api/BackendServiceName;->SAFETY:Ltv/periscope/android/api/BackendServiceName;

    invoke-virtual {v3, v4}, Lcym;->b(Ltv/periscope/android/api/BackendServiceName;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    const-string/jumbo v2, "tv.periscope.android.api.service.safety.EXTRA_VOTE_REQUEST"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 909
    const/16 v0, 0x4a

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Ltv/periscope/android/api/AuthedApiManager;->execute(ILandroid/os/Bundle;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
