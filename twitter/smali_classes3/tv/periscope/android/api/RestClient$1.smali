.class Ltv/periscope/android/api/RestClient$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lretrofit/RequestInterceptor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/api/RestClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Class;Lretrofit/RestAdapter$LogLevel;Lretrofit/converter/Converter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ltv/periscope/android/api/RestClient;

.field final synthetic val$appContext:Landroid/content/Context;

.field final synthetic val$userAgent:Ljava/lang/String;


# direct methods
.method constructor <init>(Ltv/periscope/android/api/RestClient;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Ltv/periscope/android/api/RestClient$1;->this$0:Ltv/periscope/android/api/RestClient;

    iput-object p2, p0, Ltv/periscope/android/api/RestClient$1;->val$userAgent:Ljava/lang/String;

    iput-object p3, p0, Ltv/periscope/android/api/RestClient$1;->val$appContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public intercept(Lretrofit/RequestInterceptor$RequestFacade;)V
    .locals 2

    .prologue
    .line 61
    const-string/jumbo v0, "User-Agent"

    iget-object v1, p0, Ltv/periscope/android/api/RestClient$1;->val$userAgent:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Ltv/periscope/android/api/RestClient$1;->val$appContext:Landroid/content/Context;

    # invokes: Ltv/periscope/android/api/RestClient;->getLanguageTags(Landroid/content/Context;)Ljava/lang/String;
    invoke-static {v0}, Ltv/periscope/android/api/RestClient;->access$000(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    const-string/jumbo v1, "Accept-Language"

    invoke-interface {p1, v1, v0}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_0
    const-string/jumbo v0, "package"

    iget-object v1, p0, Ltv/periscope/android/api/RestClient$1;->val$appContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string/jumbo v0, "build"

    const-string/jumbo v1, "1d2b209"

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string/jumbo v0, "locale"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string/jumbo v0, "install_id"

    iget-object v1, p0, Ltv/periscope/android/api/RestClient$1;->val$appContext:Landroid/content/Context;

    invoke-static {v1}, Ltv/periscope/android/util/j;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string/jumbo v0, "os"

    invoke-static {}, Ltv/periscope/android/util/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    return-void
.end method
