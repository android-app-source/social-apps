.class public abstract Ltv/periscope/android/api/DefaultEventHandler;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/api/ApiEventHandler;


# instance fields
.field protected final mAppContext:Landroid/content/Context;

.field protected final mBroadcastCache:Lcyn;

.field protected final mEventBus:Lde/greenrobot/event/c;

.field protected final mUserCache:Lcyw;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcyn;Lcyw;Lde/greenrobot/event/c;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/DefaultEventHandler;->mAppContext:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Ltv/periscope/android/api/DefaultEventHandler;->mBroadcastCache:Lcyn;

    .line 38
    iput-object p3, p0, Ltv/periscope/android/api/DefaultEventHandler;->mUserCache:Lcyw;

    .line 39
    iput-object p4, p0, Ltv/periscope/android/api/DefaultEventHandler;->mEventBus:Lde/greenrobot/event/c;

    .line 40
    return-void
.end method

.method private handleApiEvent(Ltv/periscope/android/event/ApiEvent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 74
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    invoke-virtual {v0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->c()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 97
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Ltv/periscope/android/api/DefaultEventHandler;->handleEvent(Ltv/periscope/android/event/ApiEvent;)V

    .line 100
    iget-object v0, p0, Ltv/periscope/android/api/DefaultEventHandler;->mEventBus:Lde/greenrobot/event/c;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 101
    :goto_1
    return-void

    .line 77
    :sswitch_0
    invoke-virtual {p0, p1}, Ltv/periscope/android/api/DefaultEventHandler;->handleUnauthorizedApiResponse(Ltv/periscope/android/event/ApiEvent;)V

    goto :goto_1

    .line 81
    :sswitch_1
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->d()Ltv/periscope/android/api/ErrorResponse;

    move-result-object v0

    .line 82
    iget-boolean v1, p1, Ltv/periscope/android/event/ApiEvent;->f:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, v0, Ltv/periscope/android/api/ErrorResponse;->error:Ltv/periscope/android/api/ErrorResponseItem;

    iget v0, v0, Ltv/periscope/android/api/ErrorResponseItem;->code:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 84
    iget-object v0, p0, Ltv/periscope/android/api/DefaultEventHandler;->mAppContext:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__blocked_user:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 89
    :sswitch_2
    iget-boolean v0, p1, Ltv/periscope/android/event/ApiEvent;->f:Z

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Ltv/periscope/android/api/DefaultEventHandler;->mAppContext:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__rate_limited:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 75
    :sswitch_data_0
    .sparse-switch
        0x191 -> :sswitch_0
        0x193 -> :sswitch_1
        0x1ad -> :sswitch_2
    .end sparse-switch
.end method

.method private handleServiceEvent(Ltv/periscope/android/event/ServiceEvent;)V
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p1}, Ltv/periscope/android/event/ServiceEvent;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ltv/periscope/android/event/ServiceEvent;->c()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    .line 63
    invoke-virtual {p0, p1}, Ltv/periscope/android/api/DefaultEventHandler;->handleUnauthorizedServiceResponse(Ltv/periscope/android/event/ServiceEvent;)V

    .line 71
    :goto_0
    return-void

    .line 67
    :cond_0
    invoke-virtual {p0, p1}, Ltv/periscope/android/api/DefaultEventHandler;->handleEvent(Ltv/periscope/android/event/ApiEvent;)V

    .line 70
    iget-object v0, p0, Ltv/periscope/android/api/DefaultEventHandler;->mEventBus:Lde/greenrobot/event/c;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected handleEvent(Ltv/periscope/android/event/ApiEvent;)V
    .locals 4
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 113
    sget-object v0, Ltv/periscope/android/api/DefaultEventHandler$1;->$SwitchMap$tv$periscope$android$event$ApiEvent$Type:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 116
    :pswitch_0
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 120
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 121
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltv/periscope/model/p;

    .line 122
    invoke-virtual {v1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 127
    :cond_1
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 128
    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->c:Ltv/periscope/android/api/ApiRequest;

    instance-of v1, v1, Ltv/periscope/android/api/GetBroadcastsRequest;

    if-eqz v1, :cond_4

    .line 129
    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->c:Ltv/periscope/android/api/ApiRequest;

    check-cast v1, Ltv/periscope/android/api/GetBroadcastsRequest;

    iget-object v1, v1, Ltv/periscope/android/api/GetBroadcastsRequest;->ids:Ljava/util/ArrayList;

    invoke-interface {v3, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 133
    :cond_2
    :goto_2
    invoke-interface {v3, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 134
    iget-object v1, p0, Ltv/periscope/android/api/DefaultEventHandler;->mBroadcastCache:Lcyn;

    invoke-interface {v1, v3}, Lcyn;->a(Ljava/util/Collection;)V

    .line 136
    :cond_3
    iget-object v1, p0, Ltv/periscope/android/api/DefaultEventHandler;->mBroadcastCache:Lcyn;

    invoke-interface {v1, v0}, Lcyn;->a(Ljava/util/List;)V

    goto :goto_0

    .line 130
    :cond_4
    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->c:Ltv/periscope/android/api/ApiRequest;

    instance-of v1, v1, Ltv/periscope/android/api/GetBroadcastsPublicRequest;

    if-eqz v1, :cond_2

    .line 131
    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->c:Ltv/periscope/android/api/ApiRequest;

    check-cast v1, Ltv/periscope/android/api/GetBroadcastsPublicRequest;

    iget-object v1, v1, Ltv/periscope/android/api/GetBroadcastsPublicRequest;->ids:Ljava/util/ArrayList;

    invoke-interface {v3, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 141
    :pswitch_1
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/model/af;

    .line 143
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 144
    invoke-virtual {v0}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    iget-object v0, p0, Ltv/periscope/android/api/DefaultEventHandler;->mBroadcastCache:Lcyn;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/util/List;)V

    goto :goto_0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected handleUnauthorizedApiResponse(Ltv/periscope/android/event/ApiEvent;)V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method protected handleUnauthorizedServiceResponse(Ltv/periscope/android/event/ServiceEvent;)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 2

    .prologue
    .line 44
    sget-object v0, Ltv/periscope/android/api/DefaultEventHandler$1;->$SwitchMap$tv$periscope$android$event$ApiEvent$EventType:[I

    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->e()Ltv/periscope/android/event/ApiEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 59
    :goto_0
    return-void

    .line 46
    :pswitch_0
    invoke-direct {p0, p1}, Ltv/periscope/android/api/DefaultEventHandler;->handleApiEvent(Ltv/periscope/android/event/ApiEvent;)V

    goto :goto_0

    .line 50
    :pswitch_1
    check-cast p1, Ltv/periscope/android/event/ServiceEvent;

    .line 51
    invoke-direct {p0, p1}, Ltv/periscope/android/api/DefaultEventHandler;->handleServiceEvent(Ltv/periscope/android/event/ServiceEvent;)V

    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
