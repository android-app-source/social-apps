.class public final enum Ltv/periscope/android/api/PsUser$VipBadge;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/api/PsUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VipBadge"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/android/api/PsUser$VipBadge;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Ltv/periscope/android/api/PsUser$VipBadge;

.field public static final enum BRONZE:Ltv/periscope/android/api/PsUser$VipBadge;

.field public static final enum GOLD:Ltv/periscope/android/api/PsUser$VipBadge;

.field public static final enum NONE:Ltv/periscope/android/api/PsUser$VipBadge;

.field public static final enum SILVER:Ltv/periscope/android/api/PsUser$VipBadge;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 241
    new-instance v0, Ltv/periscope/android/api/PsUser$VipBadge;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/api/PsUser$VipBadge;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/api/PsUser$VipBadge;->NONE:Ltv/periscope/android/api/PsUser$VipBadge;

    .line 242
    new-instance v0, Ltv/periscope/android/api/PsUser$VipBadge;

    const-string/jumbo v1, "BRONZE"

    invoke-direct {v0, v1, v3}, Ltv/periscope/android/api/PsUser$VipBadge;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/api/PsUser$VipBadge;->BRONZE:Ltv/periscope/android/api/PsUser$VipBadge;

    .line 243
    new-instance v0, Ltv/periscope/android/api/PsUser$VipBadge;

    const-string/jumbo v1, "SILVER"

    invoke-direct {v0, v1, v4}, Ltv/periscope/android/api/PsUser$VipBadge;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/api/PsUser$VipBadge;->SILVER:Ltv/periscope/android/api/PsUser$VipBadge;

    .line 244
    new-instance v0, Ltv/periscope/android/api/PsUser$VipBadge;

    const-string/jumbo v1, "GOLD"

    invoke-direct {v0, v1, v5}, Ltv/periscope/android/api/PsUser$VipBadge;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/api/PsUser$VipBadge;->GOLD:Ltv/periscope/android/api/PsUser$VipBadge;

    .line 240
    const/4 v0, 0x4

    new-array v0, v0, [Ltv/periscope/android/api/PsUser$VipBadge;

    sget-object v1, Ltv/periscope/android/api/PsUser$VipBadge;->NONE:Ltv/periscope/android/api/PsUser$VipBadge;

    aput-object v1, v0, v2

    sget-object v1, Ltv/periscope/android/api/PsUser$VipBadge;->BRONZE:Ltv/periscope/android/api/PsUser$VipBadge;

    aput-object v1, v0, v3

    sget-object v1, Ltv/periscope/android/api/PsUser$VipBadge;->SILVER:Ltv/periscope/android/api/PsUser$VipBadge;

    aput-object v1, v0, v4

    sget-object v1, Ltv/periscope/android/api/PsUser$VipBadge;->GOLD:Ltv/periscope/android/api/PsUser$VipBadge;

    aput-object v1, v0, v5

    sput-object v0, Ltv/periscope/android/api/PsUser$VipBadge;->$VALUES:[Ltv/periscope/android/api/PsUser$VipBadge;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 240
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Ltv/periscope/android/api/PsUser$VipBadge;
    .locals 2

    .prologue
    .line 247
    invoke-static {p0}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    sget-object v0, Ltv/periscope/android/api/PsUser$VipBadge;->NONE:Ltv/periscope/android/api/PsUser$VipBadge;

    .line 262
    :goto_0
    return-object v0

    .line 251
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 262
    sget-object v0, Ltv/periscope/android/api/PsUser$VipBadge;->NONE:Ltv/periscope/android/api/PsUser$VipBadge;

    goto :goto_0

    .line 251
    :sswitch_0
    const-string/jumbo v1, "bronze"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v1, "silver"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v1, "gold"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    .line 253
    :pswitch_0
    sget-object v0, Ltv/periscope/android/api/PsUser$VipBadge;->BRONZE:Ltv/periscope/android/api/PsUser$VipBadge;

    goto :goto_0

    .line 256
    :pswitch_1
    sget-object v0, Ltv/periscope/android/api/PsUser$VipBadge;->SILVER:Ltv/periscope/android/api/PsUser$VipBadge;

    goto :goto_0

    .line 259
    :pswitch_2
    sget-object v0, Ltv/periscope/android/api/PsUser$VipBadge;->GOLD:Ltv/periscope/android/api/PsUser$VipBadge;

    goto :goto_0

    .line 251
    nop

    :sswitch_data_0
    .sparse-switch
        -0x524a7a66 -> :sswitch_0
        -0x35c82cf3 -> :sswitch_1
        0x308060 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/android/api/PsUser$VipBadge;
    .locals 1

    .prologue
    .line 240
    const-class v0, Ltv/periscope/android/api/PsUser$VipBadge;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser$VipBadge;

    return-object v0
.end method

.method public static values()[Ltv/periscope/android/api/PsUser$VipBadge;
    .locals 1

    .prologue
    .line 240
    sget-object v0, Ltv/periscope/android/api/PsUser$VipBadge;->$VALUES:[Ltv/periscope/android/api/PsUser$VipBadge;

    invoke-virtual {v0}, [Ltv/periscope/android/api/PsUser$VipBadge;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/android/api/PsUser$VipBadge;

    return-object v0
.end method
