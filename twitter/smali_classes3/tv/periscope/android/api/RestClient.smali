.class public Ltv/periscope/android/api/RestClient;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final DEFAULT_LANGUAGE_TAGS_CAPACITY:I = 0xc

.field private static final LOGCAT_MAX_LENGTH:I = 0xf6e

.field private static final TAG:Ljava/lang/String; = "PsRetrofit"


# instance fields
.field private final mApiService:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mRestAdapter:Lretrofit/RestAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Class;Lretrofit/RestAdapter$LogLevel;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lretrofit/RestAdapter$LogLevel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v6, Lretrofit/converter/GsonConverter;

    new-instance v0, Lcom/google/gson/e;

    invoke-direct {v0}, Lcom/google/gson/e;-><init>()V

    invoke-direct {v6, v0}, Lretrofit/converter/GsonConverter;-><init>(Lcom/google/gson/e;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/api/RestClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Class;Lretrofit/RestAdapter$LogLevel;Lretrofit/converter/Converter;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Class;Lretrofit/RestAdapter$LogLevel;Lretrofit/converter/Converter;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lretrofit/RestAdapter$LogLevel;",
            "Lretrofit/converter/Converter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 48
    sget-object v1, Ltv/periscope/android/library/b;->a:Ldca;

    invoke-interface {v1, v0}, Ldca;->a(Lokhttp3/OkHttpClient$Builder;)V

    .line 49
    new-instance v1, Ljava/net/CookieManager;

    invoke-direct {v1}, Ljava/net/CookieManager;-><init>()V

    .line 50
    sget-object v2, Ljava/net/CookiePolicy;->ACCEPT_ORIGINAL_SERVER:Ljava/net/CookiePolicy;

    invoke-virtual {v1, v2}, Ljava/net/CookieManager;->setCookiePolicy(Ljava/net/CookiePolicy;)V

    .line 51
    invoke-static {v1}, Ljava/net/CookieHandler;->setDefault(Ljava/net/CookieHandler;)V

    .line 52
    new-instance v2, Lokhttp3/JavaNetCookieJar;

    invoke-direct {v2, v1}, Lokhttp3/JavaNetCookieJar;-><init>(Ljava/net/CookieHandler;)V

    invoke-virtual {v0, v2}, Lokhttp3/OkHttpClient$Builder;->cookieJar(Lokhttp3/CookieJar;)Lokhttp3/OkHttpClient$Builder;

    .line 53
    invoke-static {}, Ldcn;->a()Lokhttp3/CertificatePinner;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->certificatePinner(Lokhttp3/CertificatePinner;)Lokhttp3/OkHttpClient$Builder;

    .line 54
    new-instance v1, Lla;

    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    invoke-direct {v1, v0}, Lla;-><init>(Lokhttp3/OkHttpClient;)V

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 57
    invoke-static {p1}, Ltv/periscope/android/network/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 58
    new-instance v3, Ltv/periscope/android/api/RestClient$1;

    invoke-direct {v3, p0, v2, v0}, Ltv/periscope/android/api/RestClient$1;-><init>(Ltv/periscope/android/api/RestClient;Ljava/lang/String;Landroid/content/Context;)V

    .line 74
    new-instance v0, Lretrofit/RestAdapter$Builder;

    invoke-direct {v0}, Lretrofit/RestAdapter$Builder;-><init>()V

    .line 75
    invoke-virtual {v0, p3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    new-instance v2, Lretrofit/android/MainThreadExecutor;

    invoke-direct {v2}, Lretrofit/android/MainThreadExecutor;-><init>()V

    .line 76
    invoke-virtual {v0, p2, v2}, Lretrofit/RestAdapter$Builder;->setExecutors(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    .line 77
    invoke-virtual {v0, v1}, Lretrofit/RestAdapter$Builder;->setClient(Lretrofit/client/Client;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    .line 78
    invoke-virtual {v0, p6}, Lretrofit/RestAdapter$Builder;->setConverter(Lretrofit/converter/Converter;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    .line 79
    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setRequestInterceptor(Lretrofit/RequestInterceptor;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    .line 80
    invoke-virtual {v0, p5}, Lretrofit/RestAdapter$Builder;->setLogLevel(Lretrofit/RestAdapter$LogLevel;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    new-instance v1, Ltv/periscope/android/api/RestClient$2;

    invoke-direct {v1, p0}, Ltv/periscope/android/api/RestClient$2;-><init>(Ltv/periscope/android/api/RestClient;)V

    .line 81
    invoke-virtual {v0, v1}, Lretrofit/RestAdapter$Builder;->setLog(Lretrofit/RestAdapter$Log;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/RestClient;->mRestAdapter:Lretrofit/RestAdapter;

    .line 88
    iget-object v0, p0, Ltv/periscope/android/api/RestClient;->mRestAdapter:Lretrofit/RestAdapter;

    invoke-virtual {v0, p4}, Lretrofit/RestAdapter;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/RestClient;->mApiService:Ljava/lang/Object;

    .line 89
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    invoke-static {p0}, Ltv/periscope/android/api/RestClient;->getLanguageTags(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-static {p0}, Ltv/periscope/android/api/RestClient;->dumpMessage(Ljava/lang/String;)V

    return-void
.end method

.method private static dumpMessage(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/16 v2, 0xf6e

    .line 111
    invoke-static {}, Ltv/periscope/android/util/t;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 116
    const-string/jumbo v0, "PsRetrofit"

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/api/RestClient;->dumpMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_1
    const-string/jumbo v0, "PsRetrofit"

    invoke-static {v0, p0}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getLanguageTags(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 93
    invoke-static {p0}, Ltv/periscope/android/util/o;->a(Landroid/content/Context;)Ljava/util/Collection;

    move-result-object v1

    .line 94
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x0

    .line 107
    :goto_0
    return-object v0

    .line 98
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v0, 0xc

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 99
    const/4 v0, 0x0

    .line 100
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 101
    if-lez v1, :cond_1

    .line 102
    const-string/jumbo v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    :cond_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 106
    goto :goto_1

    .line 107
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getAdapter()Lretrofit/RestAdapter;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Ltv/periscope/android/api/RestClient;->mRestAdapter:Lretrofit/RestAdapter;

    return-object v0
.end method

.method public getService()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Ltv/periscope/android/api/RestClient;->mApiService:Ljava/lang/Object;

    return-object v0
.end method
