.class public Ltv/periscope/android/ui/view/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/b;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field protected final a:Ltv/periscope/android/view/RootDragLayout;

.field protected final b:Ltv/periscope/android/view/ActionSheet;


# direct methods
.method public constructor <init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/ActionSheet;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Ltv/periscope/android/ui/view/a;->a:Ltv/periscope/android/view/RootDragLayout;

    .line 24
    iput-object p2, p0, Ltv/periscope/android/ui/view/a;->b:Ltv/periscope/android/view/ActionSheet;

    .line 25
    iget-object v0, p0, Ltv/periscope/android/ui/view/a;->a:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/view/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->c(Landroid/view/View;)V

    .line 26
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List",
            "<+",
            "Ltv/periscope/android/view/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Ltv/periscope/android/ui/view/a;->a(Ljava/lang/CharSequence;Ljava/util/List;I)V

    .line 31
    return-void
.end method

.method public a(Ljava/lang/CharSequence;Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List",
            "<+",
            "Ltv/periscope/android/view/a;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Ltv/periscope/android/ui/view/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-virtual {v0, p1, p2, p3}, Ltv/periscope/android/view/ActionSheet;->a(Ljava/lang/CharSequence;Ljava/util/List;I)V

    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/view/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-virtual {v0}, Ltv/periscope/android/view/ActionSheet;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 49
    new-instance v1, Ltv/periscope/android/ui/view/a$2;

    invoke-direct {v1, p0, v0}, Ltv/periscope/android/ui/view/a$2;-><init>(Ltv/periscope/android/ui/view/a;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 57
    iget-object v0, p0, Ltv/periscope/android/ui/view/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-virtual {v0}, Ltv/periscope/android/view/ActionSheet;->requestLayout()V

    .line 58
    return-void
.end method

.method public a(Ljava/lang/CharSequence;Ljava/util/List;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List",
            "<+",
            "Ltv/periscope/android/view/a;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Ltv/periscope/android/ui/view/a;->a:Ltv/periscope/android/view/RootDragLayout;

    new-instance v1, Ltv/periscope/android/ui/view/a$1;

    invoke-direct {v1, p0, p1, p2}, Ltv/periscope/android/ui/view/a$1;-><init>(Ltv/periscope/android/ui/view/a;Ljava/lang/CharSequence;Ljava/util/List;)V

    invoke-virtual {v0, v1, p3, p4}, Ltv/periscope/android/view/RootDragLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 44
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Ltv/periscope/android/ui/view/a;->a:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/view/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->c(Landroid/view/View;)V

    .line 63
    return-void
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Ltv/periscope/android/ui/view/a;->a:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/view/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method
