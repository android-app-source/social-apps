.class public Ltv/periscope/android/ui/c;
.super Ltv/periscope/android/ui/a;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ltv/periscope/android/api/ApiManager;Lcyw;Ltv/periscope/android/view/ab$a;Lcsa;Ldae;Landroid/view/ViewGroup;Lde/greenrobot/event/c;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct/range {p0 .. p8}, Ltv/periscope/android/ui/a;-><init>(Landroid/content/Context;Ltv/periscope/android/api/ApiManager;Lcyw;Ltv/periscope/android/view/ab$a;Lcsa;Ldae;Landroid/view/ViewGroup;Lde/greenrobot/event/c;)V

    .line 33
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ltv/periscope/android/ui/d;)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 27
    invoke-super {p0, p1}, Ltv/periscope/android/ui/a;->a(Ltv/periscope/android/ui/d;)V

    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/user/g;)V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0, p1}, Ltv/periscope/android/ui/a;->a(Ltv/periscope/android/ui/user/g;)V

    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ldbx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 53
    invoke-super {p0, p1}, Ltv/periscope/android/ui/a;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 54
    iget-object v0, p0, Ltv/periscope/android/ui/c;->d:Lcyw;

    invoke-interface {v0, p1}, Lcyw;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 68
    :goto_0
    return-object v0

    .line 58
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/c;->d:Lcyw;

    invoke-interface {v0, p1}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 59
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ltv/periscope/android/ui/c;->h()Ltv/periscope/android/ui/user/g;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_1
    move-object v0, v1

    .line 60
    goto :goto_0

    .line 63
    :cond_2
    iget-boolean v0, v0, Ltv/periscope/android/api/PsUser;->isBlocked:Z

    if-eqz v0, :cond_3

    .line 64
    new-instance v0, Ldby;

    iget-object v2, p0, Ltv/periscope/android/ui/c;->a:Landroid/content/Context;

    invoke-virtual {p0}, Ltv/periscope/android/ui/c;->h()Ltv/periscope/android/ui/user/g;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3}, Ldby;-><init>(Ltv/periscope/android/view/aj;Landroid/content/Context;Ltv/periscope/android/ui/user/g;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    move-object v0, v1

    .line 68
    goto :goto_0

    .line 66
    :cond_3
    new-instance v0, Ldbu;

    invoke-virtual {p0}, Ltv/periscope/android/ui/c;->h()Ltv/periscope/android/ui/user/g;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Ldbu;-><init>(Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public b()Ltv/periscope/android/view/s;
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Ltv/periscope/android/ui/c;->e:Ltv/periscope/android/view/BaseProfileSheet;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Ltv/periscope/android/view/SimpleProfileSheet;

    iget-object v1, p0, Ltv/periscope/android/ui/c;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Ltv/periscope/android/view/SimpleProfileSheet;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/c;->a(Ltv/periscope/android/view/BaseProfileSheet;)V

    .line 41
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/c;->e:Ltv/periscope/android/view/BaseProfileSheet;

    return-object v0
.end method

.method public bridge synthetic c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0, p1}, Ltv/periscope/android/ui/a;->c(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic d()Ldae;
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Ltv/periscope/android/ui/a;->d()Ldae;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Ltv/periscope/android/ui/c;->a:Landroid/content/Context;

    const-string/jumbo v1, "https://www.periscope.tv"

    invoke-static {v0, v1, p1}, Ltv/periscope/android/util/ab;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public bridge synthetic e()Lcyw;
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Ltv/periscope/android/ui/a;->e()Lcyw;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public bridge synthetic f()V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0}, Ltv/periscope/android/ui/a;->f()V

    return-void
.end method

.method public bridge synthetic g()V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0}, Ltv/periscope/android/ui/a;->g()V

    return-void
.end method

.method public bridge synthetic h()Ltv/periscope/android/ui/user/g;
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Ltv/periscope/android/ui/a;->h()Ltv/periscope/android/ui/user/g;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic i()V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0}, Ltv/periscope/android/ui/a;->i()V

    return-void
.end method

.method public bridge synthetic j()Z
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Ltv/periscope/android/ui/a;->j()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0, p1}, Ltv/periscope/android/ui/a;->onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V

    return-void
.end method

.method public bridge synthetic onEventMainThread(Ltv/periscope/android/event/CacheEvent;)V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0, p1}, Ltv/periscope/android/ui/a;->onEventMainThread(Ltv/periscope/android/event/CacheEvent;)V

    return-void
.end method
