.class abstract Ltv/periscope/android/ui/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/t;


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Ltv/periscope/android/api/ApiManager;

.field protected final c:Landroid/view/ViewGroup;

.field protected final d:Lcyw;

.field protected e:Ltv/periscope/android/view/BaseProfileSheet;

.field private final f:Lcsa;

.field private final g:Ldae;

.field private final h:Lde/greenrobot/event/c;

.field private i:Ltv/periscope/android/ui/user/g;

.field private j:Ltv/periscope/android/view/ab$a;


# direct methods
.method constructor <init>(Landroid/content/Context;Ltv/periscope/android/api/ApiManager;Lcyw;Ltv/periscope/android/view/ab$a;Lcsa;Ldae;Landroid/view/ViewGroup;Lde/greenrobot/event/c;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Ltv/periscope/android/ui/a;->a:Landroid/content/Context;

    .line 53
    iput-object p2, p0, Ltv/periscope/android/ui/a;->b:Ltv/periscope/android/api/ApiManager;

    .line 54
    iput-object p3, p0, Ltv/periscope/android/ui/a;->d:Lcyw;

    .line 55
    iput-object p4, p0, Ltv/periscope/android/ui/a;->j:Ltv/periscope/android/view/ab$a;

    .line 56
    iput-object p5, p0, Ltv/periscope/android/ui/a;->f:Lcsa;

    .line 57
    iput-object p6, p0, Ltv/periscope/android/ui/a;->g:Ldae;

    .line 58
    iput-object p7, p0, Ltv/periscope/android/ui/a;->c:Landroid/view/ViewGroup;

    .line 59
    iput-object p8, p0, Ltv/periscope/android/ui/a;->h:Lde/greenrobot/event/c;

    .line 60
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 37
    check-cast p1, Ltv/periscope/android/ui/d;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/a;->a(Ltv/periscope/android/ui/d;)V

    return-void
.end method

.method public a(Ltv/periscope/android/ui/d;)V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 191
    if-nez p1, :cond_0

    .line 218
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/a;->f:Lcsa;

    invoke-interface {v0}, Lcsa;->e()V

    .line 196
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->b()Ltv/periscope/android/view/s;

    move-result-object v1

    .line 198
    iget-object v0, p1, Ltv/periscope/android/ui/d;->a:Ljava/lang/String;

    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 199
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 200
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/a;->b(Ltv/periscope/android/ui/d;)V

    .line 214
    :cond_1
    :goto_1
    iget-object v0, p0, Ltv/periscope/android/ui/a;->j:Ltv/periscope/android/view/ab$a;

    if-eqz v0, :cond_2

    .line 215
    iget-object v0, p0, Ltv/periscope/android/ui/a;->j:Ltv/periscope/android/view/ab$a;

    invoke-interface {v0}, Ltv/periscope/android/view/ab$a;->a()V

    .line 217
    :cond_2
    invoke-interface {v1}, Ltv/periscope/android/view/s;->h()V

    goto :goto_0

    .line 202
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/a;->b:Ltv/periscope/android/api/ApiManager;

    iget-object v2, p1, Ltv/periscope/android/ui/d;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ltv/periscope/android/api/ApiManager;->getUserById(Ljava/lang/String;)Ljava/lang/String;

    .line 203
    invoke-interface {v1}, Ltv/periscope/android/view/s;->e()V

    .line 204
    iget-object v0, p0, Ltv/periscope/android/ui/a;->d:Lcyw;

    iget-object v2, p1, Ltv/periscope/android/ui/d;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    invoke-interface {v1, v0}, Ltv/periscope/android/view/s;->a(Ltv/periscope/android/api/PsUser;)V

    goto :goto_1

    .line 206
    :cond_4
    iget-object v0, p1, Ltv/periscope/android/ui/d;->b:Ljava/lang/String;

    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    invoke-interface {v1}, Ltv/periscope/android/view/s;->e()V

    .line 208
    iget-object v0, p0, Ltv/periscope/android/ui/a;->b:Ltv/periscope/android/api/ApiManager;

    iget-object v2, p1, Ltv/periscope/android/ui/d;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ltv/periscope/android/api/ApiManager;->getUserByUsername(Ljava/lang/String;)Ljava/lang/String;

    .line 209
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/a;->b(Ltv/periscope/android/ui/d;)V

    goto :goto_1
.end method

.method public a(Ltv/periscope/android/ui/user/g;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Ltv/periscope/android/ui/a;->i:Ltv/periscope/android/ui/user/g;

    .line 144
    return-void
.end method

.method protected a(Ltv/periscope/android/view/BaseProfileSheet;)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Ltv/periscope/android/ui/a;->e:Ltv/periscope/android/view/BaseProfileSheet;

    if-eq v0, p1, :cond_1

    .line 64
    iget-object v0, p0, Ltv/periscope/android/ui/a;->e:Ltv/periscope/android/view/BaseProfileSheet;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Ltv/periscope/android/ui/a;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Ltv/periscope/android/ui/a;->e:Ltv/periscope/android/view/BaseProfileSheet;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 67
    :cond_0
    iput-object p1, p0, Ltv/periscope/android/ui/a;->e:Ltv/periscope/android/view/BaseProfileSheet;

    .line 68
    iget-object v0, p0, Ltv/periscope/android/ui/a;->e:Ltv/periscope/android/view/BaseProfileSheet;

    invoke-virtual {v0, p0}, Ltv/periscope/android/view/BaseProfileSheet;->setDelegate(Ltv/periscope/android/view/t;)V

    .line 69
    iget-object v0, p0, Ltv/periscope/android/ui/a;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Ltv/periscope/android/ui/a;->e:Ltv/periscope/android/view/BaseProfileSheet;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 71
    :cond_1
    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ldbx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Ltv/periscope/android/ui/a;->d:Lcyw;

    invoke-interface {v0, p1}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 95
    if-nez v0, :cond_0

    .line 96
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 104
    :goto_0
    return-object v0

    .line 98
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 99
    iget-boolean v0, v0, Ltv/periscope/android/api/PsUser;->isBluebirdUser:Z

    if-eqz v0, :cond_1

    .line 100
    new-instance v0, Ldbw;

    invoke-direct {v0, p0}, Ldbw;-><init>(Ltv/periscope/android/view/aj;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    move-object v0, v1

    .line 104
    goto :goto_0

    .line 102
    :cond_1
    new-instance v0, Ldbv;

    invoke-direct {v0, p0}, Ldbv;-><init>(Ltv/periscope/android/view/aj;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public abstract b()Ltv/periscope/android/view/s;
.end method

.method b(Ltv/periscope/android/ui/d;)V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Ltv/periscope/android/ui/a;->j:Ltv/periscope/android/view/ab$a;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Ltv/periscope/android/ui/a;->j:Ltv/periscope/android/view/ab$a;

    invoke-interface {v0}, Ltv/periscope/android/view/ab$a;->b()V

    .line 224
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->b()Ltv/periscope/android/view/s;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/view/s;->a(Ltv/periscope/android/ui/d;)V

    .line 225
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Ltv/periscope/android/ui/a;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Ltv/periscope/android/util/ab;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 177
    return-void
.end method

.method public d()Ldae;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Ltv/periscope/android/ui/a;->g:Ldae;

    return-object v0
.end method

.method public e()Lcyw;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ltv/periscope/android/ui/a;->d:Lcyw;

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Ltv/periscope/android/ui/a;->h:Lde/greenrobot/event/c;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->b(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    iget-object v0, p0, Ltv/periscope/android/ui/a;->h:Lde/greenrobot/event/c;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 128
    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Ltv/periscope/android/ui/a;->h:Lde/greenrobot/event/c;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 133
    return-void
.end method

.method public h()Ltv/periscope/android/ui/user/g;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Ltv/periscope/android/ui/a;->i:Ltv/periscope/android/ui/user/g;

    return-object v0
.end method

.method public i()V
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/a;->b(Ltv/periscope/android/ui/d;)V

    .line 232
    :cond_0
    return-void
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Ltv/periscope/android/ui/a;->e:Ltv/periscope/android/view/BaseProfileSheet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/a;->e:Ltv/periscope/android/view/BaseProfileSheet;

    invoke-virtual {v0}, Ltv/periscope/android/view/BaseProfileSheet;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 3

    .prologue
    .line 283
    sget-object v0, Ltv/periscope/android/ui/a$1;->b:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 285
    :pswitch_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/api/GetUserResponse;

    .line 288
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->b()Ltv/periscope/android/view/s;

    move-result-object v1

    invoke-interface {v1}, Ltv/periscope/android/view/s;->getCurrentUserId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Ltv/periscope/android/api/GetUserResponse;->user:Ltv/periscope/android/api/PsUser;

    iget-object v1, v1, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->b()Ltv/periscope/android/view/s;

    move-result-object v2

    invoke-interface {v2}, Ltv/periscope/android/view/s;->getCurrentUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 289
    :cond_1
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->b()Ltv/periscope/android/view/s;

    move-result-object v1

    iget-object v0, v0, Ltv/periscope/android/api/GetUserResponse;->user:Ltv/periscope/android/api/PsUser;

    invoke-interface {v1, v0}, Ltv/periscope/android/view/s;->a(Ltv/periscope/android/api/PsUser;)V

    goto :goto_0

    .line 292
    :cond_2
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->b()Ltv/periscope/android/view/s;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/view/s;->f()V

    .line 293
    iget-object v0, p0, Ltv/periscope/android/ui/a;->a:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__profile_viewer_not_found:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 299
    :pswitch_1
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->b()Ltv/periscope/android/view/s;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/view/s;->k()V

    goto :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onEventMainThread(Ltv/periscope/android/event/CacheEvent;)V
    .locals 2

    .prologue
    .line 256
    sget-object v0, Ltv/periscope/android/ui/a$1;->a:[I

    invoke-virtual {p1}, Ltv/periscope/android/event/CacheEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 260
    :pswitch_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->b()Ltv/periscope/android/view/s;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/view/s;->k()V

    goto :goto_0

    .line 273
    :pswitch_1
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    invoke-virtual {p0}, Ltv/periscope/android/ui/a;->b()Ltv/periscope/android/view/s;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/view/s;->d()V

    goto :goto_0

    .line 256
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
