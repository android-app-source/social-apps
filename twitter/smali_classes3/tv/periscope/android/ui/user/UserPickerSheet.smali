.class public Ltv/periscope/android/ui/user/UserPickerSheet;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ltv/periscope/android/ui/user/j;
.implements Ltv/periscope/android/ui/user/k$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/user/UserPickerSheet$a;
    }
.end annotation


# static fields
.field private static final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcyz;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/TextView;

.field private i:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private j:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private k:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private l:Z

.field private m:I

.field private n:Ltv/periscope/android/ui/user/UserPickerSheet$a;

.field private o:Ltv/periscope/android/ui/user/k;

.field private p:Landroid/support/v7/widget/RecyclerView;

.field private q:Z

.field private r:Z

.field private s:I

.field private t:Ltv/periscope/android/view/v;

.field private u:Ltv/periscope/android/view/v;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Ltv/periscope/android/ui/user/UserPickerSheet;->b:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 83
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/user/UserPickerSheet;->a(Landroid/content/Context;)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 88
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/user/UserPickerSheet;->a(Landroid/content/Context;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 93
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/user/UserPickerSheet;->a(Landroid/content/Context;)V

    .line 94
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/user/UserPickerSheet;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 97
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__user_picker_activity:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 99
    sget v0, Ltv/periscope/android/library/f$g;->title:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->c:Landroid/widget/TextView;

    .line 100
    sget v0, Ltv/periscope/android/library/f$g;->description:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->d:Landroid/widget/TextView;

    .line 101
    sget v0, Ltv/periscope/android/library/f$g;->search_query:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    .line 103
    sget v0, Ltv/periscope/android/library/f$g;->list:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->p:Landroid/support/v7/widget/RecyclerView;

    .line 104
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->p:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v1, p1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 106
    sget v0, Ltv/periscope/android/library/f$g;->search_or_close:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->f:Landroid/widget/ImageView;

    .line 107
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    sget v0, Ltv/periscope/android/library/f$g;->close_or_back:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->g:Landroid/widget/ImageView;

    .line 109
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    sget v0, Ltv/periscope/android/library/f$g;->done_count:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->h:Landroid/widget/TextView;

    .line 111
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    iput v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->s:I

    .line 115
    new-instance v0, Ltv/periscope/android/ui/user/UserPickerSheet$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/user/UserPickerSheet$1;-><init>(Ltv/periscope/android/ui/user/UserPickerSheet;)V

    iput-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->t:Ltv/periscope/android/view/v;

    .line 130
    new-instance v0, Ltv/periscope/android/ui/user/UserPickerSheet$2;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/user/UserPickerSheet$2;-><init>(Ltv/periscope/android/ui/user/UserPickerSheet;)V

    iput-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->u:Ltv/periscope/android/view/v;

    .line 143
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->setVisibility(I)V

    .line 146
    invoke-super {p0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    return-void
.end method

.method private a(Ltv/periscope/model/user/UserItem;)V
    .locals 2

    .prologue
    .line 430
    invoke-interface {p1}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/user/UserItem$Type;->b:Ltv/periscope/model/user/UserItem$Type;

    if-ne v0, v1, :cond_1

    .line 431
    check-cast p1, Ltv/periscope/android/api/PsUser;

    iget-object v0, p1, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-static {v0}, Ltv/periscope/model/user/e;->a(Ljava/lang/String;)Ltv/periscope/model/user/e;

    move-result-object p1

    .line 435
    :cond_0
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/user/k;->a(Ltv/periscope/model/user/UserItem;)V

    .line 436
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->m()V

    .line 437
    return-void

    .line 432
    :cond_1
    invoke-interface {p1}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/user/UserItem$Type;->h:Ltv/periscope/model/user/UserItem$Type;

    if-ne v0, v1, :cond_0

    .line 433
    check-cast p1, Ltv/periscope/model/r;

    invoke-virtual {p1}, Ltv/periscope/model/r;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ltv/periscope/model/user/d;->a(Ljava/lang/String;Ltv/periscope/model/ChannelType;)Ltv/periscope/model/user/d;

    move-result-object p1

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/ui/user/UserPickerSheet;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->r:Z

    return p1
.end method

.method static synthetic b(Ltv/periscope/android/ui/user/UserPickerSheet;)Ltv/periscope/android/ui/user/k;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    return-object v0
.end method

.method static synthetic b(Ltv/periscope/android/ui/user/UserPickerSheet;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->q:Z

    return p1
.end method

.method static synthetic c(Ltv/periscope/android/ui/user/UserPickerSheet;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->k()V

    return-void
.end method

.method private e(I)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 440
    if-nez p1, :cond_0

    .line 441
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 450
    :goto_0
    return-void

    .line 443
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 444
    iget-boolean v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->l:Z

    if-eqz v0, :cond_1

    .line 445
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->j:I

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 447
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->i:I

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 202
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 206
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 207
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->g:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_back:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 208
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->f:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_close:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 209
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    invoke-static {v0}, Ltv/periscope/android/util/n;->b(Landroid/view/View;)V

    .line 210
    return-void
.end method

.method private getCheckedUserIds()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 375
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/k;->f()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    sget-object v0, Ltv/periscope/android/ui/user/UserPickerSheet;->b:Ljava/util/ArrayList;

    .line 378
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v1}, Ltv/periscope/android/ui/user/k;->f()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method private h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 213
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 214
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 216
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 218
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->g:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_close:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 219
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->f:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_search:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 220
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    invoke-static {v0}, Ltv/periscope/android/util/n;->a(Landroid/view/View;)V

    .line 221
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    new-instance v1, Ltv/periscope/android/ui/user/UserPickerSheet$3;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/user/UserPickerSheet$3;-><init>(Ltv/periscope/android/ui/user/UserPickerSheet;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 298
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 330
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 331
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->k:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$l;->ps__dialog_btn_no:I

    const/4 v2, 0x0

    .line 332
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$l;->ps__dialog_btn_yes:I

    new-instance v2, Ltv/periscope/android/ui/user/UserPickerSheet$4;

    invoke-direct {v2, p0}, Ltv/periscope/android/ui/user/UserPickerSheet$4;-><init>(Ltv/periscope/android/ui/user/UserPickerSheet;)V

    .line 333
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 339
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 340
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    .line 344
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->e()V

    .line 345
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->n:Ltv/periscope/android/ui/user/UserPickerSheet$a;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->n:Ltv/periscope/android/ui/user/UserPickerSheet$a;

    sget-object v1, Ltv/periscope/android/ui/user/UserPickerSheet;->b:Ljava/util/ArrayList;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Ltv/periscope/android/ui/user/UserPickerSheet$a;->a(Ljava/util/ArrayList;ZZ)V

    .line 348
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->l()V

    .line 349
    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    .line 352
    iget-boolean v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->r:Z

    if-eqz v0, :cond_0

    .line 363
    :goto_0
    return-void

    .line 356
    :cond_0
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->s:I

    int-to-float v3, v3

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 357
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/android/view/e;->a(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 358
    iget-object v1, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->u:Ltv/periscope/android/view/v;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 360
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 361
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 362
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/k;->notifyDataSetChanged()V

    .line 409
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/k;->f()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->e(I)V

    .line 413
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->e:Landroid/widget/EditText;

    invoke-static {v0}, Ltv/periscope/android/util/n;->a(Landroid/view/View;)V

    .line 414
    return-void
.end method


# virtual methods
.method public a(I)Ltv/periscope/android/ui/user/UserPickerSheet;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 224
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 225
    return-object p0
.end method

.method public a(II)Ltv/periscope/android/ui/user/UserPickerSheet;
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 249
    iput p1, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->i:I

    .line 250
    iput p2, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->j:I

    .line 251
    return-object p0
.end method

.method public a(Ltv/periscope/android/ui/user/UserPickerSheet$a;)Ltv/periscope/android/ui/user/UserPickerSheet;
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->n:Ltv/periscope/android/ui/user/UserPickerSheet$a;

    .line 256
    return-object p0
.end method

.method public a(Ltv/periscope/model/user/UserType;)Ltv/periscope/android/ui/user/UserPickerSheet;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->a:Lcyz;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->a:Lcyz;

    invoke-virtual {v0, p1}, Lcyz;->a(Ltv/periscope/model/user/UserType;)V

    .line 162
    :cond_0
    return-object p0
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 301
    iget-boolean v2, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->r:Z

    if-eqz v2, :cond_0

    .line 318
    :goto_0
    return-void

    .line 305
    :cond_0
    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    iget v4, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->s:I

    int-to-float v4, v4

    aput v4, v3, v1

    const/4 v4, 0x0

    aput v4, v3, v0

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 306
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Ltv/periscope/android/view/e;->b(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 307
    iget-object v3, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->t:Ltv/periscope/android/view/v;

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 309
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 310
    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 311
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 315
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->getCheckedUserIds()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 316
    if-lez v2, :cond_1

    :goto_1
    iput-boolean v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->l:Z

    .line 317
    invoke-direct {p0, v2}, Ltv/periscope/android/ui/user/UserPickerSheet;->e(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 316
    goto :goto_1
.end method

.method public a(ILandroid/view/View;Ltv/periscope/model/user/UserItem;)V
    .locals 0

    .prologue
    .line 385
    invoke-direct {p0, p3}, Ltv/periscope/android/ui/user/UserPickerSheet;->a(Ltv/periscope/model/user/UserItem;)V

    .line 386
    return-void
.end method

.method public a(IZLtv/periscope/model/user/UserItem;)V
    .locals 0

    .prologue
    .line 424
    invoke-direct {p0, p3}, Ltv/periscope/android/ui/user/UserPickerSheet;->a(Ltv/periscope/model/user/UserItem;)V

    .line 425
    return-void
.end method

.method public a(Lcyz;Lcyw;Ldae;)V
    .locals 6

    .prologue
    .line 151
    iput-object p1, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->a:Lcyz;

    .line 152
    new-instance v0, Ltv/periscope/android/ui/user/k;

    invoke-virtual {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->a:Lcyz;

    move-object v3, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/user/k;-><init>(Landroid/content/Context;Lcyz;Ltv/periscope/android/ui/user/j;Lcyw;Ldae;)V

    iput-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    .line 153
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->p:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 154
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->i()V

    .line 155
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 399
    if-eqz p1, :cond_0

    .line 400
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/k;->c()V

    .line 404
    :goto_0
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->m()V

    .line 405
    return-void

    .line 402
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/k;->d()V

    goto :goto_0
.end method

.method public b(I)Ltv/periscope/android/ui/user/UserPickerSheet;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 229
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 230
    return-object p0
.end method

.method public b(Z)Ltv/periscope/android/ui/user/UserPickerSheet;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0, p1, p0}, Ltv/periscope/android/ui/user/k;->a(ZLtv/periscope/android/ui/user/k$a;)V

    .line 240
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/k;->notifyDataSetChanged()V

    .line 241
    return-object p0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 321
    iget-boolean v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->l:Z

    if-eqz v0, :cond_0

    iget v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->k:I

    if-eqz v0, :cond_0

    .line 322
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->j()V

    .line 326
    :goto_0
    return-void

    .line 324
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->k()V

    goto :goto_0
.end method

.method public b(ILandroid/view/View;Ltv/periscope/model/user/UserItem;)V
    .locals 0

    .prologue
    .line 391
    return-void
.end method

.method public c(I)Ltv/periscope/android/ui/user/UserPickerSheet;
    .locals 0

    .prologue
    .line 234
    iput p1, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->m:I

    .line 235
    return-object p0
.end method

.method public c(ILandroid/view/View;Ltv/periscope/model/user/UserItem;)V
    .locals 0

    .prologue
    .line 395
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 366
    iget-boolean v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->q:Z

    return v0
.end method

.method public d(I)Ltv/periscope/android/ui/user/UserPickerSheet;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 245
    invoke-virtual {p0, p1, p1}, Ltv/periscope/android/ui/user/UserPickerSheet;->a(II)Ltv/periscope/android/ui/user/UserPickerSheet;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->a:Lcyz;

    invoke-virtual {v0}, Lcyz;->b()V

    .line 371
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/k;->notifyDataSetChanged()V

    .line 372
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 457
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 458
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/k;->d()V

    .line 418
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->m()V

    .line 419
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->h()V

    .line 420
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 175
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 176
    sget v2, Ltv/periscope/android/library/f$g;->close_or_back:I

    if-ne v0, v2, :cond_2

    .line 177
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->h()V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->b()V

    goto :goto_0

    .line 182
    :cond_2
    sget v2, Ltv/periscope/android/library/f$g;->done_count:I

    if-ne v0, v2, :cond_5

    .line 183
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->n:Ltv/periscope/android/ui/user/UserPickerSheet$a;

    if-eqz v0, :cond_3

    .line 184
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->getCheckedUserIds()Ljava/util/ArrayList;

    move-result-object v2

    .line 185
    iget-object v3, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->n:Ltv/periscope/android/ui/user/UserPickerSheet$a;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v4, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v4}, Ltv/periscope/android/ui/user/k;->getItemCount()I

    move-result v4

    if-ne v0, v4, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v3, v2, v0, v1}, Ltv/periscope/android/ui/user/UserPickerSheet$a;->a(Ljava/util/ArrayList;ZZ)V

    .line 187
    :cond_3
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->l()V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 185
    goto :goto_1

    .line 188
    :cond_5
    sget v1, Ltv/periscope/android/library/f$g;->search_or_close:I

    if-ne v0, v1, :cond_0

    .line 189
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->f()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 190
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->h()V

    goto :goto_0

    .line 192
    :cond_6
    invoke-direct {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->g()V

    goto :goto_0
.end method

.method public setFilterOutList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 272
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 273
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 274
    invoke-static {v0}, Ltv/periscope/model/user/e;->a(Ljava/lang/String;)Ltv/periscope/model/user/e;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 276
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->o:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/user/k;->a(Ljava/util/List;)V

    .line 277
    iget-object v0, p0, Ltv/periscope/android/ui/user/UserPickerSheet;->a:Lcyz;

    invoke-virtual {v0, v1}, Lcyz;->a(Ljava/util/List;)V

    .line 278
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/UserPickerSheet;->d()V

    .line 279
    return-void
.end method
