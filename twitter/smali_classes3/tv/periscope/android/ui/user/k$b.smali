.class Ltv/periscope/android/ui/user/k$b;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/user/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Ltv/periscope/android/view/PsCheckButton;

.field final synthetic c:Ltv/periscope/android/ui/user/k;


# direct methods
.method public constructor <init>(Ltv/periscope/android/ui/user/k;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 304
    iput-object p1, p0, Ltv/periscope/android/ui/user/k$b;->c:Ltv/periscope/android/ui/user/k;

    .line 305
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 306
    sget v0, Ltv/periscope/android/library/f$g;->select_all:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/k$b;->a:Landroid/widget/TextView;

    .line 307
    sget v0, Ltv/periscope/android/library/f$g;->check:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsCheckButton;

    iput-object v0, p0, Ltv/periscope/android/ui/user/k$b;->b:Ltv/periscope/android/view/PsCheckButton;

    .line 308
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 309
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Ltv/periscope/android/ui/user/k$b;->b:Ltv/periscope/android/view/PsCheckButton;

    invoke-virtual {v0}, Ltv/periscope/android/view/PsCheckButton;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 315
    :goto_0
    iget-object v1, p0, Ltv/periscope/android/ui/user/k$b;->b:Ltv/periscope/android/view/PsCheckButton;

    invoke-virtual {v1, v0}, Ltv/periscope/android/view/PsCheckButton;->setChecked(Z)V

    .line 316
    iget-object v1, p0, Ltv/periscope/android/ui/user/k$b;->c:Ltv/periscope/android/ui/user/k;

    invoke-static {v1}, Ltv/periscope/android/ui/user/k;->c(Ltv/periscope/android/ui/user/k;)Ltv/periscope/android/ui/user/k$a;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 317
    iget-object v1, p0, Ltv/periscope/android/ui/user/k$b;->c:Ltv/periscope/android/ui/user/k;

    invoke-static {v1}, Ltv/periscope/android/ui/user/k;->c(Ltv/periscope/android/ui/user/k;)Ltv/periscope/android/ui/user/k$a;

    move-result-object v1

    invoke-interface {v1, v0}, Ltv/periscope/android/ui/user/k$a;->a(Z)V

    .line 319
    :cond_0
    return-void

    .line 314
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
