.class public Ltv/periscope/android/ui/user/c;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final a:Landroid/widget/ImageView;

.field public final b:Landroid/widget/TextView;

.field public final c:Ltv/periscope/android/view/PsCheckButton;

.field public d:Ltv/periscope/model/r;

.field private final e:Ltv/periscope/android/ui/user/j;


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/user/j;I)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Ltv/periscope/android/ui/user/c;-><init>(Landroid/view/View;Ltv/periscope/android/ui/user/j;IZ)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/user/j;IZ)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 30
    sget v0, Ltv/periscope/android/library/f$g;->thumbnail:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/c;->a:Landroid/widget/ImageView;

    .line 31
    iget-object v0, p0, Ltv/periscope/android/ui/user/c;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    sget v0, Ltv/periscope/android/library/f$g;->thumbnail_info_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 33
    sget v0, Ltv/periscope/android/library/f$g;->name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/c;->b:Landroid/widget/TextView;

    .line 34
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsCheckButton;

    iput-object v0, p0, Ltv/periscope/android/ui/user/c;->c:Ltv/periscope/android/view/PsCheckButton;

    .line 35
    iget-object v0, p0, Ltv/periscope/android/ui/user/c;->c:Ltv/periscope/android/view/PsCheckButton;

    invoke-virtual {v0, p0}, Ltv/periscope/android/view/PsCheckButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    iput-object p2, p0, Ltv/periscope/android/ui/user/c;->e:Ltv/periscope/android/ui/user/j;

    .line 39
    return-void

    .line 32
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 43
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/c;->getAdapterPosition()I

    move-result v1

    .line 44
    const/4 v0, -0x1

    if-ne v1, v0, :cond_1

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/user/c;->d:Ltv/periscope/model/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/user/c;->e:Ltv/periscope/android/ui/user/j;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Ltv/periscope/android/ui/user/c;->c:Ltv/periscope/android/view/PsCheckButton;

    if-ne p1, v0, :cond_3

    .line 51
    iget-object v0, p0, Ltv/periscope/android/ui/user/c;->c:Ltv/periscope/android/view/PsCheckButton;

    invoke-virtual {v0}, Ltv/periscope/android/view/PsCheckButton;->a()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 52
    :goto_1
    iget-object v2, p0, Ltv/periscope/android/ui/user/c;->e:Ltv/periscope/android/ui/user/j;

    iget-object v3, p0, Ltv/periscope/android/ui/user/c;->d:Ltv/periscope/model/r;

    invoke-interface {v2, v1, v0, v3}, Ltv/periscope/android/ui/user/j;->a(IZLtv/periscope/model/user/UserItem;)V

    .line 53
    iget-object v1, p0, Ltv/periscope/android/ui/user/c;->c:Ltv/periscope/android/view/PsCheckButton;

    invoke-virtual {v1, v0}, Ltv/periscope/android/view/PsCheckButton;->setChecked(Z)V

    goto :goto_0

    .line 51
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 54
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/user/c;->a:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_4

    .line 55
    iget-object v0, p0, Ltv/periscope/android/ui/user/c;->e:Ltv/periscope/android/ui/user/j;

    iget-object v2, p0, Ltv/periscope/android/ui/user/c;->d:Ltv/periscope/model/r;

    invoke-interface {v0, v1, p1, v2}, Ltv/periscope/android/ui/user/j;->b(ILandroid/view/View;Ltv/periscope/model/user/UserItem;)V

    goto :goto_0

    .line 56
    :cond_4
    iget-object v0, p0, Ltv/periscope/android/ui/user/c;->itemView:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 57
    iget-object v0, p0, Ltv/periscope/android/ui/user/c;->e:Ltv/periscope/android/ui/user/j;

    iget-object v2, p0, Ltv/periscope/android/ui/user/c;->d:Ltv/periscope/model/r;

    invoke-interface {v0, v1, p1, v2}, Ltv/periscope/android/ui/user/j;->a(ILandroid/view/View;Ltv/periscope/model/user/UserItem;)V

    goto :goto_0
.end method
