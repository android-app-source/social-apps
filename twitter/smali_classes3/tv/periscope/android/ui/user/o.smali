.class public Ltv/periscope/android/ui/user/o;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final a:Landroid/widget/ImageView;

.field public final b:Landroid/widget/ImageView;

.field public final c:Landroid/widget/TextView;

.field public final d:Ltv/periscope/android/view/PsImageView;

.field public final e:Ltv/periscope/android/view/PsCheckButton;

.field public final f:Landroid/widget/TextView;

.field public g:Z

.field public h:Ltv/periscope/android/api/PsUser;

.field private final i:Ltv/periscope/android/ui/user/j;


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/user/j;I)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 31
    sget v0, Ltv/periscope/android/library/f$g;->profile_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/o;->a:Landroid/widget/ImageView;

    .line 32
    sget v0, Ltv/periscope/android/library/f$g;->delete_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/o;->b:Landroid/widget/ImageView;

    .line 33
    sget v0, Ltv/periscope/android/library/f$g;->name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/o;->c:Landroid/widget/TextView;

    .line 34
    sget v0, Ltv/periscope/android/library/f$g;->muted:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/o;->d:Ltv/periscope/android/view/PsImageView;

    .line 35
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsCheckButton;

    iput-object v0, p0, Ltv/periscope/android/ui/user/o;->e:Ltv/periscope/android/view/PsCheckButton;

    .line 36
    sget v0, Ltv/periscope/android/library/f$g;->rank:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/user/o;->f:Landroid/widget/TextView;

    .line 37
    iget-object v0, p0, Ltv/periscope/android/ui/user/o;->e:Ltv/periscope/android/view/PsCheckButton;

    invoke-virtual {v0, p0}, Ltv/periscope/android/view/PsCheckButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 41
    iput-object p2, p0, Ltv/periscope/android/ui/user/o;->i:Ltv/periscope/android/ui/user/j;

    .line 42
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 46
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/o;->getAdapterPosition()I

    move-result v1

    .line 47
    const/4 v0, -0x1

    if-ne v1, v0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/user/o;->h:Ltv/periscope/android/api/PsUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/user/o;->i:Ltv/periscope/android/ui/user/j;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Ltv/periscope/android/ui/user/o;->e:Ltv/periscope/android/view/PsCheckButton;

    if-ne p1, v0, :cond_3

    .line 54
    iget-object v0, p0, Ltv/periscope/android/ui/user/o;->e:Ltv/periscope/android/view/PsCheckButton;

    invoke-virtual {v0}, Ltv/periscope/android/view/PsCheckButton;->a()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 55
    :goto_1
    iget-object v2, p0, Ltv/periscope/android/ui/user/o;->i:Ltv/periscope/android/ui/user/j;

    iget-object v3, p0, Ltv/periscope/android/ui/user/o;->h:Ltv/periscope/android/api/PsUser;

    invoke-interface {v2, v1, v0, v3}, Ltv/periscope/android/ui/user/j;->a(IZLtv/periscope/model/user/UserItem;)V

    .line 56
    iget-object v1, p0, Ltv/periscope/android/ui/user/o;->e:Ltv/periscope/android/view/PsCheckButton;

    invoke-virtual {v1, v0}, Ltv/periscope/android/view/PsCheckButton;->setChecked(Z)V

    goto :goto_0

    .line 54
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 57
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/user/o;->itemView:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 58
    iget-object v0, p0, Ltv/periscope/android/ui/user/o;->i:Ltv/periscope/android/ui/user/j;

    iget-object v2, p0, Ltv/periscope/android/ui/user/o;->h:Ltv/periscope/android/api/PsUser;

    invoke-interface {v0, v1, p1, v2}, Ltv/periscope/android/ui/user/j;->a(ILandroid/view/View;Ltv/periscope/model/user/UserItem;)V

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 65
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/o;->getAdapterPosition()I

    move-result v1

    .line 66
    const/4 v0, -0x1

    if-ne v1, v0, :cond_1

    .line 74
    :cond_0
    :goto_0
    return v3

    .line 70
    :cond_1
    iget-boolean v0, p0, Ltv/periscope/android/ui/user/o;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/user/o;->h:Ltv/periscope/android/api/PsUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/user/o;->i:Ltv/periscope/android/ui/user/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/user/o;->itemView:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 71
    iget-object v2, p0, Ltv/periscope/android/ui/user/o;->b:Landroid/widget/ImageView;

    iget-object v0, p0, Ltv/periscope/android/ui/user/o;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Ltv/periscope/android/ui/user/o;->i:Ltv/periscope/android/ui/user/j;

    iget-object v2, p0, Ltv/periscope/android/ui/user/o;->h:Ltv/periscope/android/api/PsUser;

    invoke-interface {v0, v1, p1, v2}, Ltv/periscope/android/ui/user/j;->c(ILandroid/view/View;Ltv/periscope/model/user/UserItem;)V

    goto :goto_0

    .line 71
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
