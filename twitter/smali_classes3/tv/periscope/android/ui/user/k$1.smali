.class Ltv/periscope/android/ui/user/k$1;
.super Ltv/periscope/android/util/e;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/user/k;-><init>(Landroid/content/Context;Lcyz;Ltv/periscope/android/ui/user/j;Lcyw;Ldae;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/util/e",
        "<",
        "Ltv/periscope/model/user/UserItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/user/k;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/user/k;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Ltv/periscope/android/ui/user/k$1;->a:Ltv/periscope/android/ui/user/k;

    invoke-direct {p0}, Ltv/periscope/android/util/e;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Ltv/periscope/android/ui/user/k$1;->a:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/k;->getItemCount()I

    move-result v0

    return v0
.end method

.method protected a(I)Z
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Ltv/periscope/android/ui/user/k$1;->a:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/user/k;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/user/k$1;->a:Ltv/periscope/android/ui/user/k;

    .line 70
    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/user/k;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 69
    :goto_0
    return v0

    .line 70
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(I)Ltv/periscope/model/user/UserItem;
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Ltv/periscope/android/ui/user/k$1;->a:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/user/k;->a(I)Ltv/periscope/model/user/UserItem;

    move-result-object v0

    .line 76
    invoke-interface {v0}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/user/UserItem$Type;->b:Ltv/periscope/model/user/UserItem$Type;

    if-ne v1, v2, :cond_1

    .line 77
    check-cast v0, Ltv/periscope/android/api/PsUser;

    iget-object v0, v0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-static {v0}, Ltv/periscope/model/user/e;->a(Ljava/lang/String;)Ltv/periscope/model/user/e;

    move-result-object v0

    .line 81
    :cond_0
    :goto_0
    return-object v0

    .line 78
    :cond_1
    invoke-interface {v0}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/user/UserItem$Type;->h:Ltv/periscope/model/user/UserItem$Type;

    if-ne v1, v2, :cond_0

    .line 79
    check-cast v0, Ltv/periscope/model/r;

    invoke-virtual {v0}, Ltv/periscope/model/r;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ltv/periscope/model/user/d;->a(Ljava/lang/String;Ltv/periscope/model/ChannelType;)Ltv/periscope/model/user/d;

    move-result-object v0

    goto :goto_0
.end method

.method protected synthetic c(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/user/k$1;->b(I)Ltv/periscope/model/user/UserItem;

    move-result-object v0

    return-object v0
.end method
