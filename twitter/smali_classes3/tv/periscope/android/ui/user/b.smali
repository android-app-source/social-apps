.class public abstract Ltv/periscope/android/ui/user/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/ak;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltv/periscope/android/view/ak",
        "<",
        "Ltv/periscope/android/ui/user/c;",
        "Ltv/periscope/model/r;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ldae;


# direct methods
.method public constructor <init>(Ldae;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Ltv/periscope/android/ui/user/b;->a:Ldae;

    .line 15
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Landroid/support/v7/widget/RecyclerView$ViewHolder;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Ltv/periscope/android/ui/user/c;

    check-cast p2, Ltv/periscope/model/r;

    invoke-virtual {p0, p1, p2, p3}, Ltv/periscope/android/ui/user/b;->a(Ltv/periscope/android/ui/user/c;Ltv/periscope/model/r;I)V

    return-void
.end method

.method public a(Ltv/periscope/android/ui/user/c;Ltv/periscope/model/r;I)V
    .locals 8

    .prologue
    .line 19
    iput-object p2, p1, Ltv/periscope/android/ui/user/c;->d:Ltv/periscope/model/r;

    .line 20
    iget-object v0, p1, Ltv/periscope/android/ui/user/c;->b:Landroid/widget/TextView;

    invoke-virtual {p2}, Ltv/periscope/model/r;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 21
    iget-object v0, p1, Ltv/periscope/android/ui/user/c;->c:Ltv/periscope/android/view/PsCheckButton;

    invoke-virtual {p0, p2}, Ltv/periscope/android/ui/user/b;->a(Ltv/periscope/model/r;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsCheckButton;->setChecked(Z)V

    .line 23
    iget-object v0, p1, Ltv/periscope/android/ui/user/c;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/user/b;->a:Ldae;

    iget-object v3, p1, Ltv/periscope/android/ui/user/c;->a:Landroid/widget/ImageView;

    .line 24
    invoke-virtual {p2}, Ltv/periscope/model/r;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p2}, Ltv/periscope/model/r;->i()Ljava/lang/String;

    move-result-object v5

    int-to-long v6, p3

    .line 23
    invoke-static/range {v1 .. v7}, Ltv/periscope/android/util/b;->a(Landroid/content/Context;Ldae;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;J)V

    .line 25
    return-void

    .line 24
    :cond_0
    invoke-virtual {p2}, Ltv/periscope/model/r;->f()Ljava/util/List;

    move-result-object v0

    const/4 v4, 0x0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/t;

    invoke-virtual {v0}, Ltv/periscope/model/t;->d()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public abstract a(Ltv/periscope/model/r;)Z
.end method
