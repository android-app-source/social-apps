.class public Ltv/periscope/android/ui/user/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/ak;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltv/periscope/android/view/ak",
        "<",
        "Ltv/periscope/android/ui/user/e;",
        "Ltv/periscope/model/user/UserItem$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Landroid/support/v7/widget/RecyclerView$ViewHolder;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Ltv/periscope/android/ui/user/e;

    check-cast p2, Ltv/periscope/model/user/UserItem$a;

    invoke-virtual {p0, p1, p2, p3}, Ltv/periscope/android/ui/user/d;->a(Ltv/periscope/android/ui/user/e;Ltv/periscope/model/user/UserItem$a;I)V

    return-void
.end method

.method public a(Ltv/periscope/android/ui/user/e;Ltv/periscope/model/user/UserItem$a;I)V
    .locals 3

    .prologue
    .line 14
    iget-object v0, p1, Ltv/periscope/android/ui/user/e;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 15
    iget-object v1, p1, Ltv/periscope/android/ui/user/e;->a:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 16
    sget-object v1, Ltv/periscope/android/ui/user/d$1;->a:[I

    iget-object v2, p2, Ltv/periscope/model/user/UserItem$a;->a:Ltv/periscope/model/user/UserType;

    invoke-virtual {v2}, Ltv/periscope/model/user/UserType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 62
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Unsupported user type!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :pswitch_0
    iget-object v1, p1, Ltv/periscope/android/ui/user/e;->a:Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__followers:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    :goto_0
    return-void

    .line 22
    :pswitch_1
    iget-object v1, p1, Ltv/periscope/android/ui/user/e;->a:Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__following:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 26
    :pswitch_2
    iget-object v1, p1, Ltv/periscope/android/ui/user/e;->a:Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__mutual_follow:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 30
    :pswitch_3
    iget-object v1, p1, Ltv/periscope/android/ui/user/e;->a:Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__blocked:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 34
    :pswitch_4
    iget-object v1, p1, Ltv/periscope/android/ui/user/e;->a:Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__featured_twitter:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 38
    :pswitch_5
    iget-object v1, p1, Ltv/periscope/android/ui/user/e;->a:Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__featured_users:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 42
    :pswitch_6
    iget-object v1, p1, Ltv/periscope/android/ui/user/e;->a:Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__featured_most_loved:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 46
    :pswitch_7
    iget-object v1, p1, Ltv/periscope/android/ui/user/e;->a:Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__featured_popular:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 50
    :pswitch_8
    iget-object v1, p1, Ltv/periscope/android/ui/user/e;->a:Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__featured_digits:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 54
    :pswitch_9
    iget-object v0, p1, Ltv/periscope/android/ui/user/e;->a:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 58
    :pswitch_a
    iget-object v1, p1, Ltv/periscope/android/ui/user/e;->a:Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__channels_private_divider:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 16
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
