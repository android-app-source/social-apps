.class Ltv/periscope/android/ui/user/k$c;
.super Landroid/widget/Filter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/user/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/user/k;


# direct methods
.method private constructor <init>(Ltv/periscope/android/ui/user/k;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Ltv/periscope/android/ui/user/k$c;->a:Ltv/periscope/android/ui/user/k;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ltv/periscope/android/ui/user/k;Ltv/periscope/android/ui/user/k$1;)V
    .locals 0

    .prologue
    .line 246
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/user/k$c;-><init>(Ltv/periscope/android/ui/user/k;)V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 8

    .prologue
    .line 250
    new-instance v3, Landroid/widget/Filter$FilterResults;

    invoke-direct {v3}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 251
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    const/4 v0, 0x0

    iput-object v0, v3, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 255
    iget-object v0, p0, Ltv/periscope/android/ui/user/k$c;->a:Ltv/periscope/android/ui/user/k;

    invoke-static {v0}, Ltv/periscope/android/ui/user/k;->b(Ltv/periscope/android/ui/user/k;)I

    move-result v0

    iput v0, v3, Landroid/widget/Filter$FilterResults;->count:I

    .line 278
    :goto_0
    return-object v3

    .line 257
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 258
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 259
    iget-object v0, p0, Ltv/periscope/android/ui/user/k$c;->a:Ltv/periscope/android/ui/user/k;

    invoke-static {v0}, Ltv/periscope/android/ui/user/k;->b(Ltv/periscope/android/ui/user/k;)I

    move-result v6

    .line 260
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v6, :cond_4

    .line 261
    iget-object v0, p0, Ltv/periscope/android/ui/user/k$c;->a:Ltv/periscope/android/ui/user/k;

    invoke-static {v0, v2}, Ltv/periscope/android/ui/user/k;->a(Ltv/periscope/android/ui/user/k;I)Ltv/periscope/model/user/UserItem;

    move-result-object v0

    .line 262
    invoke-interface {v0}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v1

    sget-object v7, Ltv/periscope/model/user/UserItem$Type;->b:Ltv/periscope/model/user/UserItem$Type;

    if-ne v1, v7, :cond_3

    .line 263
    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 264
    iget-object v1, v0, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    .line 265
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 266
    :cond_1
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    :cond_2
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 268
    :cond_3
    invoke-interface {v0}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v1

    sget-object v7, Ltv/periscope/model/user/UserItem$Type;->h:Ltv/periscope/model/user/UserItem$Type;

    if-ne v1, v7, :cond_2

    move-object v1, v0

    .line 269
    check-cast v1, Ltv/periscope/model/r;

    .line 270
    invoke-virtual {v1}, Ltv/periscope/model/r;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 271
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 275
    :cond_4
    iput-object v5, v3, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 276
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v3, Landroid/widget/Filter$FilterResults;->count:I

    goto :goto_0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 283
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-nez v0, :cond_0

    .line 284
    iget-object v0, p0, Ltv/periscope/android/ui/user/k$c;->a:Ltv/periscope/android/ui/user/k;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/ui/user/k;->a(Ltv/periscope/android/ui/user/k;Ljava/util/List;)Ljava/util/List;

    .line 285
    iget-object v0, p0, Ltv/periscope/android/ui/user/k$c;->a:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/k;->notifyDataSetChanged()V

    .line 295
    :goto_0
    return-void

    .line 286
    :cond_0
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 289
    iget-object v0, p0, Ltv/periscope/android/ui/user/k$c;->a:Ltv/periscope/android/ui/user/k;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ltv/periscope/android/ui/user/k;->a(Ltv/periscope/android/ui/user/k;Ljava/util/List;)Ljava/util/List;

    .line 290
    iget-object v0, p0, Ltv/periscope/android/ui/user/k$c;->a:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/k;->notifyDataSetChanged()V

    goto :goto_0

    .line 292
    :cond_1
    iget-object v1, p0, Ltv/periscope/android/ui/user/k$c;->a:Ltv/periscope/android/ui/user/k;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0}, Ltv/periscope/android/ui/user/k;->a(Ltv/periscope/android/ui/user/k;Ljava/util/List;)Ljava/util/List;

    .line 293
    iget-object v0, p0, Ltv/periscope/android/ui/user/k$c;->a:Ltv/periscope/android/ui/user/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/k;->notifyDataSetChanged()V

    goto :goto_0
.end method
