.class public Ltv/periscope/android/ui/user/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/user/l;


# instance fields
.field private final a:Ltv/periscope/android/api/ApiManager;

.field private final b:Ltv/periscope/android/ui/user/UserPickerSheet;

.field private final c:Ltv/periscope/model/user/UserType;

.field private final d:Ltv/periscope/model/ChannelType;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ltv/periscope/android/api/ApiManager;Ltv/periscope/android/ui/user/UserPickerSheet;Ltv/periscope/model/user/UserType;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 22
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/user/m;-><init>(Ltv/periscope/android/api/ApiManager;Ltv/periscope/android/ui/user/UserPickerSheet;Ltv/periscope/model/user/UserType;Ltv/periscope/model/ChannelType;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Ltv/periscope/android/api/ApiManager;Ltv/periscope/android/ui/user/UserPickerSheet;Ltv/periscope/model/user/UserType;Ltv/periscope/model/ChannelType;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Ltv/periscope/android/ui/user/m;->a:Ltv/periscope/android/api/ApiManager;

    .line 28
    iput-object p2, p0, Ltv/periscope/android/ui/user/m;->b:Ltv/periscope/android/ui/user/UserPickerSheet;

    .line 29
    iput-object p3, p0, Ltv/periscope/android/ui/user/m;->c:Ltv/periscope/model/user/UserType;

    .line 30
    iput-object p4, p0, Ltv/periscope/android/ui/user/m;->d:Ltv/periscope/model/ChannelType;

    .line 31
    iput-object p5, p0, Ltv/periscope/android/ui/user/m;->e:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 36
    sget-object v0, Ltv/periscope/android/ui/user/m$1;->a:[I

    iget-object v1, p0, Ltv/periscope/android/ui/user/m;->c:Ltv/periscope/model/user/UserType;

    invoke-virtual {v1}, Ltv/periscope/model/user/UserType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 44
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/user/m;->d:Ltv/periscope/model/ChannelType;

    if-eqz v0, :cond_0

    .line 45
    sget-object v0, Ltv/periscope/android/ui/user/m$1;->b:[I

    iget-object v1, p0, Ltv/periscope/android/ui/user/m;->d:Ltv/periscope/model/ChannelType;

    invoke-virtual {v1}, Ltv/periscope/model/ChannelType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 51
    :cond_0
    :goto_1
    iget-object v0, p0, Ltv/periscope/android/ui/user/m;->b:Ltv/periscope/android/ui/user/UserPickerSheet;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->a()V

    .line 52
    return-void

    .line 38
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/user/m;->a:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getFollowers()Ljava/lang/String;

    goto :goto_0

    .line 41
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/user/m;->a:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getMutualFollows()Ljava/lang/String;

    goto :goto_0

    .line 47
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/user/m;->a:Ltv/periscope/android/api/ApiManager;

    iget-object v1, p0, Ltv/periscope/android/ui/user/m;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Ltv/periscope/android/api/ApiManager;->getChannelsForMember(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 36
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 45
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ltv/periscope/android/ui/user/UserPickerSheet$a;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Ltv/periscope/android/ui/user/m;->b:Ltv/periscope/android/ui/user/UserPickerSheet;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/user/UserPickerSheet;->a(Ltv/periscope/android/ui/user/UserPickerSheet$a;)Ltv/periscope/android/ui/user/UserPickerSheet;

    .line 72
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ltv/periscope/android/ui/user/m;->b:Ltv/periscope/android/ui/user/UserPickerSheet;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->e()V

    .line 67
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 2

    .prologue
    .line 86
    sget-object v0, Ltv/periscope/android/ui/user/m$1;->c:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 90
    :pswitch_0
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Ltv/periscope/android/ui/user/m;->b:Ltv/periscope/android/ui/user/UserPickerSheet;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->d()V

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
