.class public abstract Ltv/periscope/android/ui/user/i;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Holder:",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        "Binder::",
        "Ltv/periscope/android/view/ak;",
        ">",
        "Landroid/support/v7/widget/RecyclerView$Adapter;"
    }
.end annotation


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Lcyu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcyu",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;"
        }
    .end annotation
.end field

.field protected final c:Ltv/periscope/android/view/aj;

.field private final d:Ltv/periscope/android/ui/user/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcyu;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcyu",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;",
            "Ltv/periscope/android/view/aj;",
            "Ltv/periscope/android/ui/user/d;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 36
    iput-object p1, p0, Ltv/periscope/android/ui/user/i;->a:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Ltv/periscope/android/ui/user/i;->b:Lcyu;

    .line 38
    iput-object p3, p0, Ltv/periscope/android/ui/user/i;->c:Ltv/periscope/android/view/aj;

    .line 39
    iput-object p4, p0, Ltv/periscope/android/ui/user/i;->d:Ltv/periscope/android/ui/user/d;

    .line 40
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")THolder;"
        }
    .end annotation
.end method

.method protected abstract a()Ltv/periscope/android/view/ak;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBinder;"
        }
    .end annotation
.end method

.method protected a(I)Ltv/periscope/model/user/UserItem;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Ltv/periscope/android/ui/user/i;->b:Lcyu;

    invoke-virtual {v0, p1}, Lcyu;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/user/UserItem;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Ltv/periscope/android/ui/user/i;->b:Lcyu;

    invoke-virtual {v0}, Lcyu;->a()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/user/i;->a(I)Ltv/periscope/model/user/UserItem;

    move-result-object v0

    .line 91
    invoke-interface {v0}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/user/UserItem$Type;->d:Ltv/periscope/model/user/UserItem$Type;

    if-ne v0, v1, :cond_0

    .line 92
    const/4 v0, 0x1

    .line 94
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 75
    :goto_0
    return-void

    .line 64
    :pswitch_0
    invoke-virtual {p0, p2}, Ltv/periscope/android/ui/user/i;->a(I)Ltv/periscope/model/user/UserItem;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/user/UserItem$a;

    .line 65
    check-cast p1, Ltv/periscope/android/ui/user/e;

    .line 66
    iget-object v1, p0, Ltv/periscope/android/ui/user/i;->d:Ltv/periscope/android/ui/user/d;

    invoke-virtual {v1, p1, v0, p2}, Ltv/periscope/android/ui/user/d;->a(Ltv/periscope/android/ui/user/e;Ltv/periscope/model/user/UserItem$a;I)V

    goto :goto_0

    .line 70
    :pswitch_1
    invoke-virtual {p0, p2}, Ltv/periscope/android/ui/user/i;->a(I)Ltv/periscope/model/user/UserItem;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 71
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/i;->a()Ltv/periscope/android/view/ak;

    move-result-object v1

    invoke-interface {v1, p1, v0, p2}, Ltv/periscope/android/view/ak;->a(Landroid/support/v7/widget/RecyclerView$ViewHolder;Ljava/lang/Object;I)V

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 3

    .prologue
    .line 48
    packed-switch p2, :pswitch_data_0

    .line 55
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/user/i;->a(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    :goto_0
    return-object v0

    .line 50
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/user/i;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__list_divider:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 51
    new-instance v0, Ltv/periscope/android/ui/user/e;

    invoke-direct {v0, v1}, Ltv/periscope/android/ui/user/e;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
