.class public Ltv/periscope/android/ui/user/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/user/g;


# instance fields
.field private final a:Ltv/periscope/android/api/ApiManager;

.field private final b:Lcsa;

.field private final c:Ldbr;


# direct methods
.method public constructor <init>(Ltv/periscope/android/api/ApiManager;Lcsa;Ldbr;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Ltv/periscope/android/ui/user/h;->a:Ltv/periscope/android/api/ApiManager;

    .line 24
    iput-object p2, p0, Ltv/periscope/android/ui/user/h;->b:Lcsa;

    .line 25
    iput-object p3, p0, Ltv/periscope/android/ui/user/h;->c:Ldbr;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Ltv/periscope/android/ui/user/h;->a:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->unblock(Ljava/lang/String;)Ljava/lang/String;

    .line 50
    iget-object v0, p0, Ltv/periscope/android/ui/user/h;->c:Ldbr;

    if-eqz v0, :cond_0

    invoke-static {p2}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Ltv/periscope/android/ui/user/h;->c:Ldbr;

    invoke-interface {v0, p2}, Ldbr;->b(Ljava/lang/String;)V

    .line 54
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/Message;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Ltv/periscope/android/ui/user/h;->a:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v0, p1, p4, p5}, Ltv/periscope/android/api/ApiManager;->block(Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/Message;)Ljava/lang/String;

    .line 39
    iget-object v0, p0, Ltv/periscope/android/ui/user/h;->b:Lcsa;

    invoke-interface {v0}, Lcsa;->d()V

    .line 40
    iget-object v0, p0, Ltv/periscope/android/ui/user/h;->c:Ldbr;

    if-eqz v0, :cond_0

    invoke-static {p2}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Ltv/periscope/android/ui/user/h;->c:Ldbr;

    invoke-interface {v0, p2}, Ldbr;->a(Ljava/lang/String;)V

    .line 44
    :cond_0
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$ReportType;Ljava/lang/String;)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 32
    return-void
.end method
