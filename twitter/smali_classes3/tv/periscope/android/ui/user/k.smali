.class public Ltv/periscope/android/ui/user/k;
.super Ltv/periscope/android/ui/user/i;
.source "Twttr"

# interfaces
.implements Landroid/widget/Filterable;
.implements Ltv/periscope/android/util/ag;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/user/k$b;,
        Ltv/periscope/android/ui/user/k$c;,
        Ltv/periscope/android/ui/user/k$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/user/i",
        "<",
        "Ltv/periscope/android/ui/user/o;",
        "Ltv/periscope/android/ui/user/n;",
        ">;",
        "Landroid/widget/Filterable;",
        "Ltv/periscope/android/util/ag",
        "<",
        "Ltv/periscope/model/user/UserItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:Ltv/periscope/model/user/UserItem$b;


# instance fields
.field private final e:Ltv/periscope/android/ui/user/j;

.field private final f:Ltv/periscope/android/util/ag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ltv/periscope/android/util/ag",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ltv/periscope/android/ui/user/n;

.field private final h:Ltv/periscope/android/ui/user/b;

.field private i:Ltv/periscope/android/ui/user/k$c;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z

.field private m:Ltv/periscope/android/ui/user/k$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Ltv/periscope/model/user/UserItem$b;

    invoke-direct {v0}, Ltv/periscope/model/user/UserItem$b;-><init>()V

    sput-object v0, Ltv/periscope/android/ui/user/k;->d:Ltv/periscope/model/user/UserItem$b;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcyz;Ltv/periscope/android/ui/user/j;Lcyw;Ldae;)V
    .locals 2

    .prologue
    .line 59
    const/4 v0, 0x0

    new-instance v1, Ltv/periscope/android/ui/user/d;

    invoke-direct {v1}, Ltv/periscope/android/ui/user/d;-><init>()V

    invoke-direct {p0, p1, p2, v0, v1}, Ltv/periscope/android/ui/user/i;-><init>(Landroid/content/Context;Lcyu;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/d;)V

    .line 60
    iput-object p3, p0, Ltv/periscope/android/ui/user/k;->e:Ltv/periscope/android/ui/user/j;

    .line 61
    new-instance v0, Ltv/periscope/android/ui/user/k$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/user/k$1;-><init>(Ltv/periscope/android/ui/user/k;)V

    iput-object v0, p0, Ltv/periscope/android/ui/user/k;->f:Ltv/periscope/android/util/ag;

    .line 85
    new-instance v0, Ltv/periscope/android/ui/user/k$2;

    invoke-direct {v0, p0, p4, p5}, Ltv/periscope/android/ui/user/k$2;-><init>(Ltv/periscope/android/ui/user/k;Lcyw;Ldae;)V

    iput-object v0, p0, Ltv/periscope/android/ui/user/k;->g:Ltv/periscope/android/ui/user/n;

    .line 92
    new-instance v0, Ltv/periscope/android/ui/user/k$3;

    invoke-direct {v0, p0, p5}, Ltv/periscope/android/ui/user/k$3;-><init>(Ltv/periscope/android/ui/user/k;Ldae;)V

    iput-object v0, p0, Ltv/periscope/android/ui/user/k;->h:Ltv/periscope/android/ui/user/b;

    .line 98
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/user/k;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Ltv/periscope/android/ui/user/k;->j:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Ltv/periscope/android/ui/user/k;)Ltv/periscope/android/util/ag;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->f:Ltv/periscope/android/util/ag;

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/user/k;I)Ltv/periscope/model/user/UserItem;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/user/k;->b(I)Ltv/periscope/model/user/UserItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ltv/periscope/android/ui/user/k;)I
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ltv/periscope/android/ui/user/k;->g()I

    move-result v0

    return v0
.end method

.method private b(I)Ltv/periscope/model/user/UserItem;
    .locals 2

    .prologue
    .line 193
    iget-boolean v0, p0, Ltv/periscope/android/ui/user/k;->l:Z

    if-eqz v0, :cond_1

    .line 194
    if-nez p1, :cond_0

    .line 195
    sget-object v0, Ltv/periscope/android/ui/user/k;->d:Ltv/periscope/model/user/UserItem$b;

    .line 200
    :goto_0
    return-object v0

    .line 197
    :cond_0
    const/4 v0, 0x0

    add-int/lit8 v1, p1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 200
    :cond_1
    invoke-super {p0, p1}, Ltv/periscope/android/ui/user/i;->a(I)Ltv/periscope/model/user/UserItem;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic c(Ltv/periscope/android/ui/user/k;)Ltv/periscope/android/ui/user/k$a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->m:Ltv/periscope/android/ui/user/k$a;

    return-object v0
.end method

.method private g()I
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Ltv/periscope/android/ui/user/k;->l:Z

    if-eqz v0, :cond_0

    .line 177
    invoke-super {p0}, Ltv/periscope/android/ui/user/i;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 179
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Ltv/periscope/android/ui/user/i;->getItemCount()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public synthetic a(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/user/k;->b(Landroid/view/ViewGroup;)Ltv/periscope/android/ui/user/o;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic a()Ltv/periscope/android/view/ak;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Ltv/periscope/android/ui/user/k;->b()Ltv/periscope/android/ui/user/n;

    move-result-object v0

    return-object v0
.end method

.method protected a(I)Ltv/periscope/model/user/UserItem;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/user/UserItem;

    .line 188
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/user/k;->b(I)Ltv/periscope/model/user/UserItem;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Ltv/periscope/model/user/UserItem;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/user/k;->a(Ltv/periscope/model/user/UserItem;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 220
    iput-object p1, p0, Ltv/periscope/android/ui/user/k;->k:Ljava/util/List;

    .line 221
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->f:Ltv/periscope/android/util/ag;

    invoke-interface {v0, p1}, Ltv/periscope/android/util/ag;->a(Ljava/util/List;)V

    .line 222
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->j:Ljava/util/List;

    iget-object v1, p0, Ltv/periscope/android/ui/user/k;->k:Ljava/util/List;

    invoke-static {v0, v1}, Ltv/periscope/android/util/i;->a(Ljava/util/Collection;Ljava/util/Collection;)V

    .line 223
    return-void
.end method

.method public a(Ltv/periscope/model/user/UserItem;)V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->f:Ltv/periscope/android/util/ag;

    invoke-interface {v0, p1}, Ltv/periscope/android/util/ag;->a(Ljava/lang/Object;)V

    .line 206
    return-void
.end method

.method public a(ZLtv/periscope/android/ui/user/k$a;)V
    .locals 0

    .prologue
    .line 101
    iput-boolean p1, p0, Ltv/periscope/android/ui/user/k;->l:Z

    .line 102
    iput-object p2, p0, Ltv/periscope/android/ui/user/k;->m:Ltv/periscope/android/ui/user/k$a;

    .line 103
    return-void
.end method

.method protected b()Ltv/periscope/android/ui/user/n;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->g:Ltv/periscope/android/ui/user/n;

    return-object v0
.end method

.method public b(Landroid/view/ViewGroup;)Ltv/periscope/android/ui/user/o;
    .locals 4

    .prologue
    .line 148
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__user_row_checked:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 150
    new-instance v1, Ltv/periscope/android/ui/user/o;

    iget-object v2, p0, Ltv/periscope/android/ui/user/k;->e:Ltv/periscope/android/ui/user/j;

    sget v3, Ltv/periscope/android/library/f$g;->check:I

    invoke-direct {v1, v0, v2, v3}, Ltv/periscope/android/ui/user/o;-><init>(Landroid/view/View;Ltv/periscope/android/ui/user/j;I)V

    return-object v1
.end method

.method public c()V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->f:Ltv/periscope/android/util/ag;

    invoke-interface {v0}, Ltv/periscope/android/util/ag;->c()V

    .line 216
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->f:Ltv/periscope/android/util/ag;

    invoke-interface {v0}, Ltv/periscope/android/util/ag;->d()V

    .line 228
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->f:Ltv/periscope/android/util/ag;

    invoke-interface {v0}, Ltv/periscope/android/util/ag;->e()Z

    move-result v0

    return v0
.end method

.method public f()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->f:Ltv/periscope/android/util/ag;

    invoke-interface {v0}, Ltv/periscope/android/util/ag;->f()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->i:Ltv/periscope/android/ui/user/k$c;

    if-nez v0, :cond_0

    .line 161
    new-instance v0, Ltv/periscope/android/ui/user/k$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ltv/periscope/android/ui/user/k$c;-><init>(Ltv/periscope/android/ui/user/k;Ltv/periscope/android/ui/user/k$1;)V

    iput-object v0, p0, Ltv/periscope/android/ui/user/k;->i:Ltv/periscope/android/ui/user/k$c;

    .line 163
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->i:Ltv/periscope/android/ui/user/k$c;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 171
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/user/k;->g()I

    move-result v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    .line 134
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/user/k;->a(I)Ltv/periscope/model/user/UserItem;

    move-result-object v0

    .line 135
    invoke-interface {v0}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/user/UserItem$Type;->d:Ltv/periscope/model/user/UserItem$Type;

    if-ne v1, v2, :cond_0

    .line 136
    const/4 v0, 0x1

    .line 142
    :goto_0
    return v0

    .line 137
    :cond_0
    invoke-interface {v0}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/user/UserItem$Type;->b:Ltv/periscope/model/user/UserItem$Type;

    if-ne v1, v2, :cond_1

    .line 138
    const/4 v0, 0x2

    goto :goto_0

    .line 139
    :cond_1
    invoke-interface {v0}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/user/UserItem$Type;->h:Ltv/periscope/model/user/UserItem$Type;

    if-ne v0, v1, :cond_2

    .line 140
    const/4 v0, 0x4

    goto :goto_0

    .line 142
    :cond_2
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 2

    .prologue
    .line 120
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 121
    check-cast p1, Ltv/periscope/android/ui/user/c;

    .line 122
    invoke-virtual {p0, p2}, Ltv/periscope/android/ui/user/k;->a(I)Ltv/periscope/model/user/UserItem;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/r;

    .line 123
    iget-object v1, p0, Ltv/periscope/android/ui/user/k;->h:Ltv/periscope/android/ui/user/b;

    invoke-virtual {v1, p1, v0, p2}, Ltv/periscope/android/ui/user/b;->a(Ltv/periscope/android/ui/user/c;Ltv/periscope/model/r;I)V

    .line 130
    :goto_0
    return-void

    .line 124
    :cond_0
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 125
    check-cast p1, Ltv/periscope/android/ui/user/k$b;

    .line 126
    iget-object v0, p1, Ltv/periscope/android/ui/user/k$b;->b:Ltv/periscope/android/view/PsCheckButton;

    iget-object v1, p0, Ltv/periscope/android/ui/user/k;->f:Ltv/periscope/android/util/ag;

    invoke-interface {v1}, Ltv/periscope/android/util/ag;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsCheckButton;->setChecked(Z)V

    goto :goto_0

    .line 128
    :cond_1
    invoke-super {p0, p1, p2}, Ltv/periscope/android/ui/user/i;->onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V

    goto :goto_0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 107
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 108
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__channel_row_checked:I

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 109
    new-instance v0, Ltv/periscope/android/ui/user/c;

    iget-object v2, p0, Ltv/periscope/android/ui/user/k;->e:Ltv/periscope/android/ui/user/j;

    sget v3, Ltv/periscope/android/library/f$g;->check:I

    invoke-direct {v0, v1, v2, v3}, Ltv/periscope/android/ui/user/c;-><init>(Landroid/view/View;Ltv/periscope/android/ui/user/j;I)V

    .line 114
    :goto_0
    return-object v0

    .line 110
    :cond_0
    const/4 v0, 0x5

    if-ne p2, v0, :cond_1

    .line 111
    iget-object v0, p0, Ltv/periscope/android/ui/user/k;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__select_all_row:I

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 112
    new-instance v0, Ltv/periscope/android/ui/user/k$b;

    invoke-direct {v0, p0, v1}, Ltv/periscope/android/ui/user/k$b;-><init>(Ltv/periscope/android/ui/user/k;Landroid/view/View;)V

    goto :goto_0

    .line 114
    :cond_1
    invoke-super {p0, p1, p2}, Ltv/periscope/android/ui/user/i;->onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    goto :goto_0
.end method
