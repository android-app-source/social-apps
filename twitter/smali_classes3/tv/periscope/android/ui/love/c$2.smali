.class Ltv/periscope/android/ui/love/c$2;
.super Ltv/periscope/android/view/v;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/love/c;->a(Ltv/periscope/android/ui/love/HeartView;Landroid/view/ViewGroup;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/love/HeartView;

.field final synthetic b:Landroid/view/ViewGroup;

.field final synthetic c:Ltv/periscope/android/ui/love/c;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/love/c;Ltv/periscope/android/ui/love/HeartView;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Ltv/periscope/android/ui/love/c$2;->c:Ltv/periscope/android/ui/love/c;

    iput-object p2, p0, Ltv/periscope/android/ui/love/c$2;->a:Ltv/periscope/android/ui/love/HeartView;

    iput-object p3, p0, Ltv/periscope/android/ui/love/c$2;->b:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ltv/periscope/android/view/v;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/love/c$2;->c:Ltv/periscope/android/ui/love/c;

    invoke-static {v0}, Ltv/periscope/android/ui/love/c;->c(Ltv/periscope/android/ui/love/c;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Ltv/periscope/android/ui/love/c$2$1;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/love/c$2$1;-><init>(Ltv/periscope/android/ui/love/c$2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 89
    iget-object v0, p0, Ltv/periscope/android/ui/love/c$2;->c:Ltv/periscope/android/ui/love/c;

    invoke-static {v0}, Ltv/periscope/android/ui/love/c;->b(Ltv/periscope/android/ui/love/c;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 90
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Ltv/periscope/android/ui/love/c$2;->c:Ltv/periscope/android/ui/love/c;

    invoke-static {v0}, Ltv/periscope/android/ui/love/c;->b(Ltv/periscope/android/ui/love/c;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 72
    iget-object v0, p0, Ltv/periscope/android/ui/love/c$2;->c:Ltv/periscope/android/ui/love/c;

    iget-object v0, v0, Ltv/periscope/android/ui/love/c;->a:Ltv/periscope/android/ui/love/a$a;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Ltv/periscope/android/ui/love/c$2;->c:Ltv/periscope/android/ui/love/c;

    iget-object v0, v0, Ltv/periscope/android/ui/love/c;->a:Ltv/periscope/android/ui/love/a$a;

    iget-object v1, p0, Ltv/periscope/android/ui/love/c$2;->a:Ltv/periscope/android/ui/love/HeartView;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/love/a$a;->a(Ltv/periscope/android/ui/love/HeartView;)V

    .line 75
    :cond_0
    return-void
.end method
