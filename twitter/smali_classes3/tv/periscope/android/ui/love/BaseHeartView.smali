.class public abstract Ltv/periscope/android/ui/love/BaseHeartView;
.super Landroid/widget/ImageView;
.source "Twttr"


# static fields
.field private static a:Landroid/graphics/Paint;

.field private static b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/PorterDuffColorFilter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private d:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Ltv/periscope/android/ui/love/BaseHeartView;->a:Landroid/graphics/Paint;

    .line 22
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Ltv/periscope/android/ui/love/BaseHeartView;->b:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 32
    invoke-direct {p0}, Ltv/periscope/android/ui/love/BaseHeartView;->a()V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    invoke-direct {p0}, Ltv/periscope/android/ui/love/BaseHeartView;->a()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    invoke-direct {p0}, Ltv/periscope/android/ui/love/BaseHeartView;->a()V

    .line 43
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Ltv/periscope/android/ui/love/BaseHeartView;->getBorderDrawable()I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/love/BaseHeartView;->c:I

    .line 47
    invoke-virtual {p0}, Ltv/periscope/android/ui/love/BaseHeartView;->getFillDrawable()I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/love/BaseHeartView;->d:I

    .line 48
    iget v0, p0, Ltv/periscope/android/ui/love/BaseHeartView;->c:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/love/BaseHeartView;->setImageResource(I)V

    .line 49
    return-void
.end method

.method private b(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 88
    iput p1, p0, Ltv/periscope/android/ui/love/BaseHeartView;->g:I

    .line 89
    invoke-virtual {p0}, Ltv/periscope/android/ui/love/BaseHeartView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 91
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 93
    sget-object v3, Ltv/periscope/android/ui/love/BaseHeartView;->a:Landroid/graphics/Paint;

    .line 95
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int/2addr v0, v4

    int-to-float v0, v0

    div-float v4, v0, v7

    .line 96
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sub-int/2addr v0, v5

    int-to-float v0, v0

    div-float v5, v0, v7

    .line 98
    invoke-virtual {v2, p2, v6, v6, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 99
    sget-object v0, Ltv/periscope/android/ui/love/BaseHeartView;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PorterDuffColorFilter;

    .line 100
    if-nez v0, :cond_0

    .line 101
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, p1, v6}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 102
    sget-object v6, Ltv/periscope/android/ui/love/BaseHeartView;->b:Landroid/util/SparseArray;

    invoke-virtual {v6, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 104
    :cond_0
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 106
    invoke-virtual {v2, p3, v4, v5, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 107
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 109
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/love/BaseHeartView;->e:I

    .line 110
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/love/BaseHeartView;->f:I

    .line 111
    return-object v1
.end method


# virtual methods
.method public a(III)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 61
    iput p2, p0, Ltv/periscope/android/ui/love/BaseHeartView;->c:I

    .line 62
    iput p3, p0, Ltv/periscope/android/ui/love/BaseHeartView;->d:I

    .line 63
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/love/BaseHeartView;->setColor(I)V

    .line 64
    return-void
.end method

.method public a(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Ltv/periscope/android/ui/love/BaseHeartView;->b(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 57
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Ltv/periscope/android/ui/love/BaseHeartView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v1}, Ltv/periscope/android/ui/love/BaseHeartView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 58
    return-void
.end method

.method protected abstract getBorderDrawable()I
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Ltv/periscope/android/ui/love/BaseHeartView;->g:I

    return v0
.end method

.method protected abstract getFillDrawable()I
.end method

.method public getHeartHeight()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Ltv/periscope/android/ui/love/BaseHeartView;->f:I

    return v0
.end method

.method public getHeartWidth()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Ltv/periscope/android/ui/love/BaseHeartView;->e:I

    return v0
.end method

.method public setColor(I)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 67
    invoke-virtual {p0}, Ltv/periscope/android/ui/love/BaseHeartView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 68
    iget v1, p0, Ltv/periscope/android/ui/love/BaseHeartView;->c:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/love/BaseHeartView;->d:I

    .line 69
    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 68
    invoke-direct {p0, p1, v1, v0}, Ltv/periscope/android/ui/love/BaseHeartView;->b(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 70
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Ltv/periscope/android/ui/love/BaseHeartView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v1}, Ltv/periscope/android/ui/love/BaseHeartView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 71
    return-void
.end method
