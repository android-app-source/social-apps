.class public Ltv/periscope/android/ui/love/SmallHeartView;
.super Ltv/periscope/android/ui/love/BaseHeartView;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/love/BaseHeartView;-><init>(Landroid/content/Context;)V

    .line 11
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/love/BaseHeartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, Ltv/periscope/android/ui/love/BaseHeartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    return-void
.end method


# virtual methods
.method protected getBorderDrawable()I
    .locals 1

    .prologue
    .line 23
    sget v0, Ltv/periscope/android/library/f$f;->ps__small_heart_border:I

    return v0
.end method

.method protected getFillDrawable()I
    .locals 1

    .prologue
    .line 28
    sget v0, Ltv/periscope/android/library/f$f;->ps__small_heart_fill:I

    return v0
.end method
