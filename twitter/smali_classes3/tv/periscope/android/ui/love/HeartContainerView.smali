.class public Ltv/periscope/android/ui/love/HeartContainerView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# instance fields
.field private a:Ltv/periscope/android/ui/love/a;

.field private b:Ltv/periscope/android/ui/love/d;

.field private c:Ltv/periscope/android/ui/love/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-direct {p0}, Ltv/periscope/android/ui/love/HeartContainerView;->a()V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    invoke-direct {p0}, Ltv/periscope/android/ui/love/HeartContainerView;->a()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    invoke-direct {p0}, Ltv/periscope/android/ui/love/HeartContainerView;->a()V

    .line 36
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/love/HeartContainerView;)Ltv/periscope/android/ui/love/e;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Ltv/periscope/android/ui/love/HeartContainerView;->c:Ltv/periscope/android/ui/love/e;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 39
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 40
    new-instance v0, Ltv/periscope/android/ui/love/c;

    invoke-virtual {p0}, Ltv/periscope/android/ui/love/HeartContainerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ltv/periscope/android/ui/love/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ltv/periscope/android/ui/love/HeartContainerView;->a:Ltv/periscope/android/ui/love/a;

    .line 45
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/love/HeartContainerView;->a:Ltv/periscope/android/ui/love/a;

    new-instance v1, Ltv/periscope/android/ui/love/HeartContainerView$1;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/love/HeartContainerView$1;-><init>(Ltv/periscope/android/ui/love/HeartContainerView;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/love/a;->a(Ltv/periscope/android/ui/love/a$a;)V

    .line 56
    new-instance v0, Ltv/periscope/android/ui/love/d;

    invoke-virtual {p0}, Ltv/periscope/android/ui/love/HeartContainerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ltv/periscope/android/ui/love/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ltv/periscope/android/ui/love/HeartContainerView;->b:Ltv/periscope/android/ui/love/d;

    .line 57
    new-instance v0, Ltv/periscope/android/ui/love/e;

    const/16 v1, 0xa

    iget-object v2, p0, Ltv/periscope/android/ui/love/HeartContainerView;->b:Ltv/periscope/android/ui/love/d;

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/love/e;-><init>(ILtv/periscope/android/ui/love/d;)V

    iput-object v0, p0, Ltv/periscope/android/ui/love/HeartContainerView;->c:Ltv/periscope/android/ui/love/e;

    .line 58
    return-void

    .line 42
    :cond_0
    new-instance v0, Ltv/periscope/android/ui/love/b;

    invoke-virtual {p0}, Ltv/periscope/android/ui/love/HeartContainerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ltv/periscope/android/ui/love/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ltv/periscope/android/ui/love/HeartContainerView;->a:Ltv/periscope/android/ui/love/a;

    goto :goto_0
.end method


# virtual methods
.method public a(IZ)V
    .locals 3

    .prologue
    .line 89
    new-instance v0, Ltv/periscope/android/ui/love/HeartView;

    invoke-virtual {p0}, Ltv/periscope/android/ui/love/HeartContainerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ltv/periscope/android/ui/love/HeartView;-><init>(Landroid/content/Context;)V

    .line 90
    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_screenshot_border:I

    sget v2, Ltv/periscope/android/library/f$f;->ps__ic_screenshot_fill:I

    invoke-virtual {v0, p1, v1, v2}, Ltv/periscope/android/ui/love/HeartView;->a(III)V

    .line 91
    iget-object v1, p0, Ltv/periscope/android/ui/love/HeartContainerView;->a:Ltv/periscope/android/ui/love/a;

    invoke-virtual {v1, v0, p0, p2}, Ltv/periscope/android/ui/love/a;->a(Ltv/periscope/android/ui/love/HeartView;Landroid/view/ViewGroup;Z)V

    .line 92
    return-void
.end method

.method public a(IZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/love/HeartContainerView;->c:Ltv/periscope/android/ui/love/e;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/love/e;->a(I)Ltv/periscope/android/ui/love/HeartView;

    move-result-object v0

    .line 81
    if-eqz p3, :cond_0

    .line 82
    invoke-virtual {v0, p3}, Ltv/periscope/android/ui/love/HeartView;->setTag(Ljava/lang/Object;)V

    .line 85
    :cond_0
    iget-object v1, p0, Ltv/periscope/android/ui/love/HeartContainerView;->a:Ltv/periscope/android/ui/love/a;

    invoke-virtual {v1, v0, p0, p2}, Ltv/periscope/android/ui/love/a;->a(Ltv/periscope/android/ui/love/HeartView;Landroid/view/ViewGroup;Z)V

    .line 86
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Ltv/periscope/android/ui/love/HeartContainerView;->b:Ltv/periscope/android/ui/love/d;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/love/d;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 68
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 63
    iget-object v0, p0, Ltv/periscope/android/ui/love/HeartContainerView;->a:Ltv/periscope/android/ui/love/a;

    invoke-virtual {v0}, Ltv/periscope/android/ui/love/a;->a()V

    .line 64
    return-void
.end method

.method public setImageLoader(Ldae;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Ltv/periscope/android/ui/love/HeartContainerView;->b:Ltv/periscope/android/ui/love/d;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/love/d;->a(Ldae;)V

    .line 72
    return-void
.end method
