.class public Ltv/periscope/android/ui/chat/FriendsWatchingLayoutManager;
.super Landroid/support/v7/widget/LinearLayoutManager;
.source "Twttr"


# instance fields
.field private final a:F


# direct methods
.method public constructor <init>(Landroid/content/Context;ZF)V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 12
    iput p3, p0, Ltv/periscope/android/ui/chat/FriendsWatchingLayoutManager;->a:F

    .line 13
    return-void
.end method


# virtual methods
.method public getPaddingBottom()I
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    return v0
.end method

.method public getPaddingTop()I
    .locals 2

    .prologue
    .line 17
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/FriendsWatchingLayoutManager;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingLayoutManager;->a:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method
