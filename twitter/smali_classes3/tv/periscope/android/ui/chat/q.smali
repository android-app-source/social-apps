.class public Ltv/periscope/android/ui/chat/q;
.super Ltv/periscope/android/ui/chat/j;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Landroid/widget/ImageView;

.field public final c:Ltv/periscope/android/view/PsImageView;

.field public d:Ltv/periscope/android/ui/chat/h;


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/chat/j;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    .line 21
    sget v0, Ltv/periscope/android/library/f$g;->status_item:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/q;->a:Landroid/widget/TextView;

    .line 22
    sget v0, Ltv/periscope/android/library/f$g;->status_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/q;->b:Landroid/widget/ImageView;

    .line 23
    sget v0, Ltv/periscope/android/library/f$g;->superfan_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/q;->c:Ltv/periscope/android/view/PsImageView;

    .line 24
    if-eqz p2, :cond_0

    .line 25
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Ltv/periscope/android/ui/chat/q;->d:Ltv/periscope/android/ui/chat/h;

    if-nez v0, :cond_0

    .line 39
    :goto_0
    return-void

    .line 34
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/q;->d:Ltv/periscope/android/ui/chat/h;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/q;->b(Ltv/periscope/android/ui/chat/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35
    iget-object v0, p0, Ltv/periscope/android/ui/chat/q;->m:Ltv/periscope/android/ui/chat/k;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/q;->d:Ltv/periscope/android/ui/chat/h;

    iget-object v1, v1, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/k;->a(Ltv/periscope/model/chat/Message;)V

    goto :goto_0

    .line 37
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/q;->m:Ltv/periscope/android/ui/chat/k;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/k;->a()V

    goto :goto_0
.end method
