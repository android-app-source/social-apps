.class Ltv/periscope/android/ui/chat/j$a$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/chat/j$a;->a(Landroid/view/View;Ltv/periscope/android/ui/chat/h;)Landroid/animation/ValueAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/chat/h;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Ltv/periscope/android/ui/chat/j$a;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/chat/j$a;Ltv/periscope/android/ui/chat/h;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Ltv/periscope/android/ui/chat/j$a$1;->c:Ltv/periscope/android/ui/chat/j$a;

    iput-object p2, p0, Ltv/periscope/android/ui/chat/j$a$1;->a:Ltv/periscope/android/ui/chat/h;

    iput-object p3, p0, Ltv/periscope/android/ui/chat/j$a$1;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 82
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 83
    iget-object v1, p0, Ltv/periscope/android/ui/chat/j$a$1;->a:Ltv/periscope/android/ui/chat/h;

    const v2, 0x458ca000    # 4500.0f

    mul-float/2addr v2, v0

    float-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ltv/periscope/android/ui/chat/h;->a(J)V

    .line 84
    iget-object v1, p0, Ltv/periscope/android/ui/chat/j$a$1;->a:Ltv/periscope/android/ui/chat/h;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/chat/h;->a(F)V

    .line 85
    iget-object v1, p0, Ltv/periscope/android/ui/chat/j$a$1;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 86
    return-void
.end method
