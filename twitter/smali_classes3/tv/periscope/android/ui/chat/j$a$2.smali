.class Ltv/periscope/android/ui/chat/j$a$2;
.super Ltv/periscope/android/view/v;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/chat/j$a;->a(Landroid/view/View;Ltv/periscope/android/ui/chat/h;)Landroid/animation/ValueAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/chat/j$a;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/chat/j$a;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Ltv/periscope/android/ui/chat/j$a$2;->a:Ltv/periscope/android/ui/chat/j$a;

    invoke-direct {p0}, Ltv/periscope/android/view/v;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 91
    iget-object v0, p0, Ltv/periscope/android/ui/chat/j$a$2;->a:Ltv/periscope/android/ui/chat/j$a;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/j$a;->a(Ltv/periscope/android/ui/chat/j$a;)Ltv/periscope/android/ui/chat/k;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/j$a$2;->a:Ltv/periscope/android/ui/chat/j$a;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/j$a;->b(Ltv/periscope/android/ui/chat/j$a;)Ltv/periscope/android/ui/chat/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Ltv/periscope/android/ui/chat/j$a$2;->a:Ltv/periscope/android/ui/chat/j$a;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/j$a;->a(Ltv/periscope/android/ui/chat/j$a;)Ltv/periscope/android/ui/chat/k;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/chat/j$a$2;->a:Ltv/periscope/android/ui/chat/j$a;

    invoke-static {v1}, Ltv/periscope/android/ui/chat/j$a;->b(Ltv/periscope/android/ui/chat/j$a;)Ltv/periscope/android/ui/chat/h;

    move-result-object v1

    iget-object v1, v1, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/k;->b(Ltv/periscope/model/chat/Message;)V

    .line 94
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/j$a$2;->a:Ltv/periscope/android/ui/chat/j$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ltv/periscope/android/ui/chat/j$a;->a(Ltv/periscope/android/ui/chat/j$a;Z)Z

    .line 95
    iget-object v0, p0, Ltv/periscope/android/ui/chat/j$a$2;->a:Ltv/periscope/android/ui/chat/j$a;

    invoke-static {v0, v2}, Ltv/periscope/android/ui/chat/j$a;->a(Ltv/periscope/android/ui/chat/j$a;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    .line 96
    iget-object v0, p0, Ltv/periscope/android/ui/chat/j$a$2;->a:Ltv/periscope/android/ui/chat/j$a;

    invoke-static {v0, v2}, Ltv/periscope/android/ui/chat/j$a;->a(Ltv/periscope/android/ui/chat/j$a;Ltv/periscope/android/ui/chat/h;)Ltv/periscope/android/ui/chat/h;

    .line 97
    iget-object v0, p0, Ltv/periscope/android/ui/chat/j$a$2;->a:Ltv/periscope/android/ui/chat/j$a;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/j$a;->c(Ltv/periscope/android/ui/chat/j$a;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Ltv/periscope/android/ui/chat/j$a$2;->a:Ltv/periscope/android/ui/chat/j$a;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/j$a;->c(Ltv/periscope/android/ui/chat/j$a;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 99
    iget-object v0, p0, Ltv/periscope/android/ui/chat/j$a$2;->a:Ltv/periscope/android/ui/chat/j$a;

    invoke-static {v0, v2}, Ltv/periscope/android/ui/chat/j$a;->a(Ltv/periscope/android/ui/chat/j$a;Landroid/view/View;)Landroid/view/View;

    .line 101
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/j$a$2;->a:Ltv/periscope/android/ui/chat/j$a;

    invoke-static {v0, v2}, Ltv/periscope/android/ui/chat/j$a;->a(Ltv/periscope/android/ui/chat/j$a;Ltv/periscope/android/ui/chat/k;)Ltv/periscope/android/ui/chat/k;

    .line 102
    return-void
.end method
