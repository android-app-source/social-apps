.class public Ltv/periscope/android/ui/chat/e;
.super Ltv/periscope/android/ui/chat/j;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Ltv/periscope/android/view/UsernameBadgeView;

.field public final c:Landroid/widget/TextView;

.field public final d:Ltv/periscope/android/view/MaskImageView;

.field public final e:Landroid/widget/ImageView;

.field public final f:Landroid/view/View;

.field public final g:Landroid/view/View;

.field public final h:Landroid/widget/ImageView;

.field public final i:Landroid/widget/TextView;

.field public final j:Landroid/support/v7/widget/AppCompatImageView;

.field public final k:Ltv/periscope/android/view/PsImageView;

.field public l:Ltv/periscope/android/ui/chat/h;


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/chat/j;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    .line 34
    sget v0, Ltv/periscope/android/library/f$g;->username_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/chat/e;->a:Landroid/view/View;

    .line 35
    sget v0, Ltv/periscope/android/library/f$g;->username_badge_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/UsernameBadgeView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/e;->b:Ltv/periscope/android/view/UsernameBadgeView;

    .line 36
    sget v0, Ltv/periscope/android/library/f$g;->chat_body:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/e;->c:Landroid/widget/TextView;

    .line 37
    sget v0, Ltv/periscope/android/library/f$g;->masked_avatar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/MaskImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/e;->d:Ltv/periscope/android/view/MaskImageView;

    .line 38
    sget v0, Ltv/periscope/android/library/f$g;->reply_indicator:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/e;->e:Landroid/widget/ImageView;

    .line 39
    sget v0, Ltv/periscope/android/library/f$g;->block_indicator:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/chat/e;->f:Landroid/view/View;

    .line 40
    sget v0, Ltv/periscope/android/library/f$g;->chat_text_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/chat/e;->g:Landroid/view/View;

    .line 41
    sget v0, Ltv/periscope/android/library/f$g;->block_count_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/e;->h:Landroid/widget/ImageView;

    .line 42
    sget v0, Ltv/periscope/android/library/f$g;->block_count:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/e;->i:Landroid/widget/TextView;

    .line 43
    sget v0, Ltv/periscope/android/library/f$g;->following_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/AppCompatImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/e;->j:Landroid/support/v7/widget/AppCompatImageView;

    .line 44
    iget-object v0, p0, Ltv/periscope/android/ui/chat/e;->h:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$d;->ps__light_grey:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 45
    sget v0, Ltv/periscope/android/library/f$g;->superfan_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/e;->k:Ltv/periscope/android/view/PsImageView;

    .line 47
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$e;->ps__card_corner_radius:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 48
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/android/util/ac;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    iget-object v1, p0, Ltv/periscope/android/ui/chat/e;->d:Ltv/periscope/android/view/MaskImageView;

    const/4 v2, 0x4

    new-array v2, v2, [F

    aput v3, v2, v4

    aput v0, v2, v5

    aput v0, v2, v6

    aput v3, v2, v7

    invoke-virtual {v1, v2}, Ltv/periscope/android/view/MaskImageView;->setCornerRadius([F)V

    .line 54
    :goto_0
    if-eqz p2, :cond_0

    .line 55
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    :cond_0
    return-void

    .line 51
    :cond_1
    iget-object v1, p0, Ltv/periscope/android/ui/chat/e;->d:Ltv/periscope/android/view/MaskImageView;

    const/4 v2, 0x4

    new-array v2, v2, [F

    aput v0, v2, v4

    aput v3, v2, v5

    aput v3, v2, v6

    aput v0, v2, v7

    invoke-virtual {v1, v2}, Ltv/periscope/android/view/MaskImageView;->setCornerRadius([F)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Ltv/periscope/android/ui/chat/e;->l:Ltv/periscope/android/ui/chat/h;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/e;->b(Ltv/periscope/android/ui/chat/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Ltv/periscope/android/ui/chat/e;->m:Ltv/periscope/android/ui/chat/k;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/e;->l:Ltv/periscope/android/ui/chat/h;

    iget-object v1, v1, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/k;->a(Ltv/periscope/model/chat/Message;)V

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/e;->m:Ltv/periscope/android/ui/chat/k;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/k;->a()V

    goto :goto_0
.end method
