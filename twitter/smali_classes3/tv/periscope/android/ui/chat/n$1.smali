.class Ltv/periscope/android/ui/chat/n$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/chat/n;->b(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ldcd;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcxu;

.field final synthetic d:Ltv/periscope/android/ui/chat/n;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/chat/n;Ldcd;Ljava/lang/String;Lcxu;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Ltv/periscope/android/ui/chat/n$1;->d:Ltv/periscope/android/ui/chat/n;

    iput-object p2, p0, Ltv/periscope/android/ui/chat/n$1;->a:Ldcd;

    iput-object p3, p0, Ltv/periscope/android/ui/chat/n$1;->b:Ljava/lang/String;

    iput-object p4, p0, Ltv/periscope/android/ui/chat/n$1;->c:Lcxu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 255
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n$1;->d:Ltv/periscope/android/ui/chat/n;

    iget-object v0, v0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n$1;->d:Ltv/periscope/android/ui/chat/n;

    sget-object v1, Ltv/periscope/model/chat/MessageType;->b:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n$1;->d:Ltv/periscope/android/ui/chat/n;

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/n;->i()I

    move-result v0

    if-gtz v0, :cond_0

    .line 256
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n$1;->d:Ltv/periscope/android/ui/chat/n;

    iget-object v0, v0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/chat/MessageType;->E:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/chat/n$1;->a:Ldcd;

    .line 257
    invoke-virtual {v2}, Ldcd;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/chat/n$1;->d:Ltv/periscope/android/ui/chat/n;

    iget-object v2, v2, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/n$1;->a:Ldcd;

    .line 258
    invoke-virtual {v3}, Ldcd;->c()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Ltv/periscope/android/ui/chat/n$1;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->h(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    .line 259
    invoke-virtual {v1}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v1

    .line 256
    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/r;->c(Ltv/periscope/model/chat/Message;)V

    .line 261
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n$1;->d:Ltv/periscope/android/ui/chat/n;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/android/ui/chat/n;)Ltv/periscope/android/ui/chat/w;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n$1;->a:Ldcd;

    invoke-virtual {v1}, Ldcd;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/w;->e(Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n$1;->c:Lcxu;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n$1;->a:Ldcd;

    invoke-virtual {v1}, Ldcd;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcxu;->a(Ljava/lang/String;)V

    .line 265
    :cond_0
    return-void
.end method
