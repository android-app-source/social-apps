.class public Ltv/periscope/android/ui/chat/g;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/chat/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/chat/g$b;,
        Ltv/periscope/android/ui/chat/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Ltv/periscope/android/ui/chat/j;",
        ">;",
        "Ltv/periscope/android/ui/chat/l;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/res/Resources;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ltv/periscope/android/ui/chat/h;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/os/Handler;

.field private final e:Ltv/periscope/android/ui/chat/k;

.field private final f:Ltv/periscope/android/ui/chat/g$a;

.field private final g:Ltv/periscope/android/view/ak;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ltv/periscope/android/view/ak",
            "<",
            "Ltv/periscope/android/ui/chat/e;",
            "Ltv/periscope/model/chat/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ltv/periscope/android/ui/chat/y;

.field private final i:Ltv/periscope/android/ui/chat/ak;

.field private j:Z

.field private k:J

.field private l:Ltv/periscope/model/chat/MessageType;

.field private m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ltv/periscope/android/ui/chat/k;Ltv/periscope/android/view/ak;Ltv/periscope/android/ui/chat/y;Ltv/periscope/android/ui/chat/ak;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ltv/periscope/android/ui/chat/k;",
            "Ltv/periscope/android/view/ak",
            "<",
            "Ltv/periscope/android/ui/chat/e;",
            "Ltv/periscope/model/chat/Message;",
            ">;",
            "Ltv/periscope/android/ui/chat/y;",
            "Ltv/periscope/android/ui/chat/ak;",
            ")V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    .line 70
    const/4 v0, -0x1

    iput v0, p0, Ltv/periscope/android/ui/chat/g;->m:I

    .line 75
    iput-object p1, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/g;->d:Landroid/os/Handler;

    .line 78
    iput-object p2, p0, Ltv/periscope/android/ui/chat/g;->e:Ltv/periscope/android/ui/chat/k;

    .line 79
    new-instance v0, Ltv/periscope/android/ui/chat/g$a;

    const/4 v1, 0x4

    invoke-direct {v0, p0, v1}, Ltv/periscope/android/ui/chat/g$a;-><init>(Ltv/periscope/android/ui/chat/g;I)V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/g;->f:Ltv/periscope/android/ui/chat/g$a;

    .line 80
    iput-object p3, p0, Ltv/periscope/android/ui/chat/g;->g:Ltv/periscope/android/view/ak;

    .line 81
    iput-object p4, p0, Ltv/periscope/android/ui/chat/g;->h:Ltv/periscope/android/ui/chat/y;

    .line 82
    iput-object p5, p0, Ltv/periscope/android/ui/chat/g;->i:Ltv/periscope/android/ui/chat/ak;

    .line 83
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/g;->setHasStableIds(Z)V

    .line 84
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/chat/g;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->d:Landroid/os/Handler;

    return-object v0
.end method

.method private a(Ltv/periscope/model/chat/MessageType$ReportType;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 316
    if-nez p1, :cond_0

    .line 325
    :goto_0
    return-object v0

    .line 319
    :cond_0
    sget-object v1, Ltv/periscope/android/ui/chat/g$1;->c:[I

    invoke-virtual {p1}, Ltv/periscope/model/chat/MessageType$ReportType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 321
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    sget v1, Ltv/periscope/android/library/f$l;->ps__moderator_negative_spam:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 323
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    sget v1, Ltv/periscope/android/library/f$l;->ps__convicted_abuse:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 319
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/widget/TextView;II)V
    .locals 3
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 383
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 384
    invoke-virtual {p1}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 385
    return-void
.end method

.method private a(Ltv/periscope/android/ui/chat/e;Ltv/periscope/android/ui/chat/h;I)V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->g:Ltv/periscope/android/view/ak;

    iget-object v1, p2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-interface {v0, p1, v1, p3}, Ltv/periscope/android/view/ak;->a(Landroid/support/v7/widget/RecyclerView$ViewHolder;Ljava/lang/Object;I)V

    .line 445
    iput-object p2, p1, Ltv/periscope/android/ui/chat/e;->l:Ltv/periscope/android/ui/chat/h;

    .line 446
    return-void
.end method

.method private d(Ltv/periscope/model/chat/Message;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 331
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->A()Ltv/periscope/model/chat/MessageType$ReportType;

    move-result-object v1

    invoke-direct {p0, v1}, Ltv/periscope/android/ui/chat/g;->a(Ltv/periscope/model/chat/MessageType$ReportType;)Ljava/lang/String;

    move-result-object v1

    .line 332
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->C()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 333
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 334
    sget-object v3, Ltv/periscope/android/ui/chat/g$1;->d:[I

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->H()Ltv/periscope/model/chat/MessageType$SentenceType;

    move-result-object v4

    invoke-virtual {v4}, Ltv/periscope/model/chat/MessageType$SentenceType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 377
    :goto_0
    return-object v0

    .line 336
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v3, Ltv/periscope/android/library/f$l;->ps__local_prompt_conviction_broadcast_suspended_with_body_and_reason:I

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v2, v4, v5

    aput-object v1, v4, v6

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 339
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v3, Ltv/periscope/android/library/f$l;->ps__local_prompt_conviction_broadcast_disabled_with_body_and_reason:I

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v2, v4, v5

    aput-object v1, v4, v6

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 342
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v3, Ltv/periscope/android/library/f$l;->ps__local_prompt_conviction_global_suspended_with_body_and_reason:I

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v2, v4, v5

    aput-object v1, v4, v6

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 345
    :pswitch_3
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v3, Ltv/periscope/android/library/f$l;->ps__local_prompt_conviction_global_disabled_with_body_and_reason:I

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v2, v4, v5

    aput-object v1, v4, v6

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 351
    :cond_0
    if-eqz v1, :cond_1

    .line 352
    sget-object v2, Ltv/periscope/android/ui/chat/g$1;->d:[I

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->H()Ltv/periscope/model/chat/MessageType$SentenceType;

    move-result-object v3

    invoke-virtual {v3}, Ltv/periscope/model/chat/MessageType$SentenceType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 354
    :pswitch_4
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v2, Ltv/periscope/android/library/f$l;->ps__local_prompt_conviction_broadcast_suspended_with_reason:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 356
    :pswitch_5
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v2, Ltv/periscope/android/library/f$l;->ps__local_prompt_conviction_broadcast_disabled_with_reason:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 358
    :pswitch_6
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v2, Ltv/periscope/android/library/f$l;->ps__local_prompt_conviction_global_suspended_with_reason:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 360
    :pswitch_7
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v2, Ltv/periscope/android/library/f$l;->ps__local_prompt_conviction_global_disabled_with_reason:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 366
    :cond_1
    sget-object v1, Ltv/periscope/android/ui/chat/g$1;->d:[I

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->H()Ltv/periscope/model/chat/MessageType$SentenceType;

    move-result-object v2

    invoke-virtual {v2}, Ltv/periscope/model/chat/MessageType$SentenceType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    goto/16 :goto_0

    .line 368
    :pswitch_8
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__local_prompt_conviction_broadcast_suspended:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 370
    :pswitch_9
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__local_prompt_conviction_broadcast_disabled:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 372
    :pswitch_a
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__local_prompt_conviction_global_suspended:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 374
    :pswitch_b
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__local_prompt_conviction_global_disabled:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 334
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 352
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 366
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method


# virtual methods
.method public a(Ltv/periscope/model/chat/Message;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 388
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->j()Ljava/lang/String;

    move-result-object v0

    .line 389
    invoke-static {v0}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 390
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->k()Ljava/lang/String;

    move-result-object v0

    .line 392
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v2, Ltv/periscope/android/library/f$l;->ps__username_format:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Ltv/periscope/android/ui/chat/h;
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/chat/h;

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;I)Ltv/periscope/android/ui/chat/j;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 97
    packed-switch p2, :pswitch_data_0

    .line 138
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__chat_row:I

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 139
    new-instance v0, Ltv/periscope/android/ui/chat/e;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/g;->e:Ltv/periscope/android/ui/chat/k;

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/chat/e;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    :goto_0
    return-object v0

    .line 99
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__chat_row_join:I

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 100
    new-instance v0, Ltv/periscope/android/ui/chat/q;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/g;->e:Ltv/periscope/android/ui/chat/k;

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/chat/q;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    goto :goto_0

    .line 103
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_started_locally:I

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 105
    new-instance v0, Ltv/periscope/android/ui/chat/j;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/g;->e:Ltv/periscope/android/ui/chat/k;

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/chat/j;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    goto :goto_0

    .line 108
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__local_prompt_with_icon:I

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 109
    new-instance v0, Ltv/periscope/android/ui/chat/f;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/g;->e:Ltv/periscope/android/ui/chat/k;

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/chat/f;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    goto :goto_0

    .line 112
    :pswitch_3
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__channel_info_prompt:I

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 113
    sget v0, Ltv/periscope/android/library/f$g;->text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 114
    iget-object v2, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v3, Ltv/periscope/android/library/f$l;->ps__replay_skip_tip:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    new-instance v0, Ltv/periscope/android/ui/chat/j;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/g;->e:Ltv/periscope/android/ui/chat/k;

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/chat/j;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    goto :goto_0

    .line 118
    :pswitch_4
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__chat_row_verdict:I

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 120
    new-instance v0, Ltv/periscope/android/ui/chat/j;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/g;->e:Ltv/periscope/android/ui/chat/k;

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/chat/j;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    goto :goto_0

    .line 123
    :pswitch_5
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__channel_info_prompt:I

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 124
    new-instance v0, Ltv/periscope/android/ui/chat/j;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/g;->e:Ltv/periscope/android/ui/chat/k;

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/chat/j;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    goto/16 :goto_0

    .line 127
    :pswitch_6
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__chat_broadcast_tip:I

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 128
    new-instance v0, Ltv/periscope/android/ui/chat/j;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/g;->e:Ltv/periscope/android/ui/chat/k;

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/chat/j;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    goto/16 :goto_0

    .line 131
    :pswitch_7
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->h:Ltv/periscope/android/ui/chat/y;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/chat/y;->a(Landroid/view/ViewGroup;)Ltv/periscope/android/ui/chat/j;

    move-result-object v0

    goto/16 :goto_0

    .line 134
    :pswitch_8
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->i:Ltv/periscope/android/ui/chat/ak;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/chat/ak;->a(Landroid/view/ViewGroup;)Ltv/periscope/android/ui/chat/j;

    move-result-object v0

    goto/16 :goto_0

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public a()V
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 521
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->f:Ltv/periscope/android/ui/chat/g$a;

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/g$a;->b()V

    .line 522
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/chat/g;->l:Ltv/periscope/model/chat/MessageType;

    .line 523
    const/4 v0, -0x1

    iput v0, p0, Ltv/periscope/android/ui/chat/g;->m:I

    .line 524
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/j;I)V
    .locals 13

    .prologue
    const/16 v12, 0x8

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 147
    if-gez p2, :cond_0

    .line 312
    :goto_0
    return-void

    .line 150
    :cond_0
    invoke-virtual {p0, p2}, Ltv/periscope/android/ui/chat/g;->a(I)Ltv/periscope/android/ui/chat/h;

    move-result-object v2

    .line 151
    sget-object v0, Ltv/periscope/android/ui/chat/g$1;->b:[I

    iget-object v1, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/chat/MessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 301
    :cond_1
    :goto_1
    iget-object v0, p1, Ltv/periscope/android/ui/chat/j;->n:Ltv/periscope/model/y;

    if-eqz v0, :cond_2

    .line 302
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->f:Ltv/periscope/android/ui/chat/g$a;

    iget-object v1, p1, Ltv/periscope/android/ui/chat/j;->n:Ltv/periscope/model/y;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/g$a;->a(Ltv/periscope/model/y;)V

    .line 305
    :cond_2
    iget-object v0, p1, Ltv/periscope/android/ui/chat/j;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Ltv/periscope/android/ui/chat/h;->a()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 307
    new-instance v0, Ltv/periscope/android/ui/chat/j$a;

    iget-object v1, p1, Ltv/periscope/android/ui/chat/j;->itemView:Landroid/view/View;

    iget-object v3, p1, Ltv/periscope/android/ui/chat/j;->m:Ltv/periscope/android/ui/chat/k;

    invoke-direct {v0, v1, v2, v3}, Ltv/periscope/android/ui/chat/j$a;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/h;Ltv/periscope/android/ui/chat/k;)V

    .line 308
    invoke-virtual {p1}, Ltv/periscope/android/ui/chat/j;->getItemId()J

    move-result-wide v4

    invoke-virtual {v2}, Ltv/periscope/android/ui/chat/h;->c()I

    move-result v1

    invoke-static {v4, v5, v1}, Ltv/periscope/model/y;->a(JI)Ltv/periscope/model/y;

    move-result-object v1

    .line 309
    iput-object v1, p1, Ltv/periscope/android/ui/chat/j;->n:Ltv/periscope/model/y;

    .line 310
    iget-object v2, p0, Ltv/periscope/android/ui/chat/g;->f:Ltv/periscope/android/ui/chat/g$a;

    invoke-virtual {v2, v1, v0}, Ltv/periscope/android/ui/chat/g$a;->a(Ltv/periscope/model/y;Ltv/periscope/android/ui/chat/j$a;)V

    goto :goto_0

    :pswitch_0
    move-object v0, p1

    .line 153
    check-cast v0, Ltv/periscope/android/ui/chat/q;

    .line 154
    iget-object v1, v0, Ltv/periscope/android/ui/chat/q;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v3, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    sget v4, Ltv/periscope/android/library/f$d;->ps__dark_grey:I

    .line 155
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 156
    iget-object v1, v0, Ltv/periscope/android/ui/chat/q;->a:Landroid/widget/TextView;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v4, Ltv/periscope/android/library/f$l;->ps__broadcaster_kick_block:I

    new-array v5, v11, [Ljava/lang/Object;

    iget-object v6, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v7, Ltv/periscope/android/library/f$l;->ps__username_format:I

    new-array v8, v11, [Ljava/lang/Object;

    iget-object v9, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    .line 157
    invoke-virtual {v9}, Ltv/periscope/model/chat/Message;->u()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    .line 156
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, v0, Ltv/periscope/android/ui/chat/q;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :pswitch_1
    move-object v0, p1

    .line 162
    check-cast v0, Ltv/periscope/android/ui/chat/q;

    .line 163
    iget-object v1, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    iget-object v3, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v3}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v1

    .line 164
    iget-object v3, v0, Ltv/periscope/android/ui/chat/q;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v1, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 165
    iget-object v1, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->r()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_3

    .line 166
    iget-object v1, v0, Ltv/periscope/android/ui/chat/q;->a:Landroid/widget/TextView;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    sget v4, Ltv/periscope/android/library/f$j;->ps__invited_num_followers_embolden:I

    iget-object v5, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    .line 167
    invoke-virtual {v5}, Ltv/periscope/model/chat/Message;->r()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->intValue()I

    move-result v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    .line 168
    invoke-virtual {p0, v7}, Ltv/periscope/android/ui/chat/g;->a(Ltv/periscope/model/chat/Message;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    iget-object v7, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    iget-object v8, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v8}, Ltv/periscope/model/chat/Message;->r()Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v7, v8, v9, v11}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    .line 166
    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    :goto_2
    iget-object v1, v0, Ltv/periscope/android/ui/chat/q;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 175
    iput-object v2, v0, Ltv/periscope/android/ui/chat/q;->d:Ltv/periscope/android/ui/chat/h;

    goto/16 :goto_1

    .line 171
    :cond_3
    iget-object v1, v0, Ltv/periscope/android/ui/chat/q;->a:Landroid/widget/TextView;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    sget v4, Ltv/periscope/android/library/f$l;->ps__invited_followers:I

    new-array v5, v11, [Ljava/lang/Object;

    iget-object v6, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    .line 172
    invoke-virtual {p0, v6}, Ltv/periscope/android/ui/chat/g;->a(Ltv/periscope/model/chat/Message;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    .line 171
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :pswitch_2
    move-object v0, p1

    .line 179
    check-cast v0, Ltv/periscope/android/ui/chat/q;

    .line 180
    iget-object v1, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    iget-object v3, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v3}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v1

    .line 181
    iget-object v3, v0, Ltv/periscope/android/ui/chat/q;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v1, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 182
    iget-object v1, v0, Ltv/periscope/android/ui/chat/q;->a:Landroid/widget/TextView;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    sget v4, Ltv/periscope/android/library/f$l;->ps__posted_on_twitter:I

    new-array v5, v11, [Ljava/lang/Object;

    iget-object v6, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    .line 183
    invoke-virtual {p0, v6}, Ltv/periscope/android/ui/chat/g;->a(Ltv/periscope/model/chat/Message;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    .line 182
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iput-object v2, v0, Ltv/periscope/android/ui/chat/q;->d:Ltv/periscope/android/ui/chat/h;

    .line 185
    iget-object v0, v0, Ltv/periscope/android/ui/chat/q;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    :pswitch_3
    move-object v0, p1

    .line 189
    check-cast v0, Ltv/periscope/android/ui/chat/q;

    .line 190
    iget-object v1, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    iget-object v3, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v3}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v1

    .line 191
    iget-object v3, v0, Ltv/periscope/android/ui/chat/q;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v1, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 192
    iget-object v1, v0, Ltv/periscope/android/ui/chat/q;->a:Landroid/widget/TextView;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    sget v4, Ltv/periscope/android/library/f$l;->ps__retweeted_on_twitter:I

    new-array v5, v11, [Ljava/lang/Object;

    iget-object v6, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    .line 193
    invoke-virtual {p0, v6}, Ltv/periscope/android/ui/chat/g;->a(Ltv/periscope/model/chat/Message;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    .line 192
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    iput-object v2, v0, Ltv/periscope/android/ui/chat/q;->d:Ltv/periscope/android/ui/chat/h;

    .line 195
    iget-object v0, v0, Ltv/periscope/android/ui/chat/q;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    :pswitch_4
    move-object v0, p1

    .line 199
    check-cast v0, Ltv/periscope/android/ui/chat/q;

    .line 201
    invoke-virtual {v2}, Ltv/periscope/android/ui/chat/h;->c()I

    move-result v1

    if-lez v1, :cond_4

    .line 202
    iget-object v1, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    sget v3, Ltv/periscope/android/library/f$d;->ps__dark_grey:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 206
    :goto_3
    iget-object v3, v0, Ltv/periscope/android/ui/chat/q;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v1, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 207
    iget-boolean v1, p0, Ltv/periscope/android/ui/chat/g;->j:Z

    if-eqz v1, :cond_5

    iget-object v1, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->R()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 208
    iget-object v1, v0, Ltv/periscope/android/ui/chat/q;->a:Landroid/widget/TextView;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v4, Ltv/periscope/android/library/f$l;->ps__chat_join_new_user:I

    new-array v5, v11, [Ljava/lang/Object;

    iget-object v6, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    .line 209
    invoke-virtual {p0, v6}, Ltv/periscope/android/ui/chat/g;->a(Ltv/periscope/model/chat/Message;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    .line 208
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    :goto_4
    iput-object v2, v0, Ltv/periscope/android/ui/chat/q;->d:Ltv/periscope/android/ui/chat/h;

    .line 215
    iget-object v1, v0, Ltv/periscope/android/ui/chat/q;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 217
    iget-object v1, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->L()Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->L()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 218
    iget-object v0, v0, Ltv/periscope/android/ui/chat/q;->c:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v0, v10}, Ltv/periscope/android/view/PsImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 204
    :cond_4
    iget-object v1, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    iget-object v3, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v3}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v1

    goto :goto_3

    .line 211
    :cond_5
    iget-object v1, v0, Ltv/periscope/android/ui/chat/q;->a:Landroid/widget/TextView;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v4, Ltv/periscope/android/library/f$l;->ps__chat_join:I

    new-array v5, v11, [Ljava/lang/Object;

    iget-object v6, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    .line 212
    invoke-virtual {p0, v6}, Ltv/periscope/android/ui/chat/g;->a(Ltv/periscope/model/chat/Message;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    .line 211
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 220
    :cond_6
    iget-object v0, v0, Ltv/periscope/android/ui/chat/q;->c:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v0, v12}, Ltv/periscope/android/view/PsImageView;->setVisibility(I)V

    goto/16 :goto_1

    :pswitch_5
    move-object v0, p1

    .line 225
    check-cast v0, Ltv/periscope/android/ui/chat/e;

    .line 226
    invoke-direct {p0, v0, v2, p2}, Ltv/periscope/android/ui/chat/g;->a(Ltv/periscope/android/ui/chat/e;Ltv/periscope/android/ui/chat/h;I)V

    goto/16 :goto_1

    :pswitch_6
    move-object v0, p1

    .line 230
    check-cast v0, Ltv/periscope/android/ui/chat/f;

    .line 231
    iget-object v1, v0, Ltv/periscope/android/ui/chat/f;->a:Landroid/widget/TextView;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v4, Ltv/periscope/android/library/f$l;->ps__chat_share_screenshot_twitter:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v1, v0, Ltv/periscope/android/ui/chat/f;->b:Landroid/widget/ImageView;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/g;->b:Landroid/content/res/Resources;

    sget v4, Ltv/periscope/android/library/f$f;->ps__chat_twitter:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 233
    iput-object v2, v0, Ltv/periscope/android/ui/chat/f;->c:Ltv/periscope/android/ui/chat/h;

    goto/16 :goto_1

    .line 238
    :pswitch_7
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->h:Ltv/periscope/android/ui/chat/y;

    invoke-interface {v0, p1, v2}, Ltv/periscope/android/ui/chat/y;->a(Ltv/periscope/android/ui/chat/j;Ltv/periscope/android/ui/chat/h;)V

    goto/16 :goto_1

    .line 243
    :pswitch_8
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->i:Ltv/periscope/android/ui/chat/ak;

    invoke-interface {v0, p1, v2}, Ltv/periscope/android/ui/chat/ak;->a(Ltv/periscope/android/ui/chat/j;Ltv/periscope/android/ui/chat/h;)V

    goto/16 :goto_1

    .line 248
    :pswitch_9
    iget-object v0, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->E()Ltv/periscope/model/chat/MessageType$VerdictType;

    move-result-object v3

    .line 249
    iget-object v0, p1, Ltv/periscope/android/ui/chat/j;->itemView:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->moderation_verdict:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 250
    iget-object v1, p1, Ltv/periscope/android/ui/chat/j;->itemView:Landroid/view/View;

    sget v4, Ltv/periscope/android/library/f$g;->consequence:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 251
    sget-object v4, Ltv/periscope/android/ui/chat/g$1;->a:[I

    invoke-virtual {v3}, Ltv/periscope/model/chat/MessageType$VerdictType;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_1

    .line 269
    sget v3, Ltv/periscope/android/library/f$l;->ps__moderator_neutral:I

    sget v4, Ltv/periscope/android/library/f$d;->ps__grey:I

    invoke-direct {p0, v0, v3, v4}, Ltv/periscope/android/ui/chat/g;->a(Landroid/widget/TextView;II)V

    .line 270
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 253
    :pswitch_a
    sget v3, Ltv/periscope/android/library/f$l;->ps__moderator_positive:I

    sget v4, Ltv/periscope/android/library/f$d;->ps__blue:I

    invoke-direct {p0, v0, v3, v4}, Ltv/periscope/android/ui/chat/g;->a(Landroid/widget/TextView;II)V

    .line 254
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 258
    :pswitch_b
    sget v3, Ltv/periscope/android/library/f$l;->ps__moderator_negative:I

    sget v4, Ltv/periscope/android/library/f$d;->ps__red:I

    invoke-direct {p0, v0, v3, v4}, Ltv/periscope/android/ui/chat/g;->a(Landroid/widget/TextView;II)V

    .line 259
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 263
    :pswitch_c
    sget v3, Ltv/periscope/android/library/f$l;->ps__moderator_negative_spam:I

    sget v4, Ltv/periscope/android/library/f$d;->ps__red:I

    invoke-direct {p0, v0, v3, v4}, Ltv/periscope/android/ui/chat/g;->a(Landroid/widget/TextView;II)V

    .line 264
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 277
    :pswitch_d
    iget-object v0, p1, Ltv/periscope/android/ui/chat/j;->itemView:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 278
    iget-object v1, p0, Ltv/periscope/android/ui/chat/g;->a:Landroid/content/Context;

    sget v3, Ltv/periscope/android/library/f$l;->ps__local_prompt_moderator_feedback:I

    new-array v4, v11, [Ljava/lang/Object;

    iget-object v5, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v5}, Ltv/periscope/model/chat/Message;->D()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 282
    :pswitch_e
    iget-object v1, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    .line 283
    if-eqz v1, :cond_1

    .line 286
    iget-object v0, p1, Ltv/periscope/android/ui/chat/j;->itemView:Landroid/view/View;

    sget v3, Ltv/periscope/android/library/f$g;->text:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v1}, Ltv/periscope/android/ui/chat/g;->d(Ltv/periscope/model/chat/Message;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 290
    :pswitch_f
    iget-object v0, p1, Ltv/periscope/android/ui/chat/j;->itemView:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 291
    iget-object v1, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 295
    :pswitch_10
    iget-object v0, p1, Ltv/periscope/android/ui/chat/j;->itemView:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->broadcast_tip:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    .line 296
    iget-object v1, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;->setHtmlText(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 151
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 251
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Ltv/periscope/android/ui/chat/g;->j:Z

    .line 88
    return-void
.end method

.method public b(Ltv/periscope/model/chat/Message;)I
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 455
    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_1

    .line 456
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/chat/h;

    iget-object v0, v0, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 460
    :goto_1
    return v0

    .line 455
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 460
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public c(Ltv/periscope/model/chat/Message;)V
    .locals 6

    .prologue
    const v2, 0x3e4ccccd    # 0.2f

    .line 468
    sget-object v0, Ltv/periscope/android/ui/chat/g$1;->b:[I

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/chat/MessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 514
    :cond_0
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    new-instance v1, Ltv/periscope/android/ui/chat/h;

    iget-wide v2, p0, Ltv/periscope/android/ui/chat/g;->k:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, p0, Ltv/periscope/android/ui/chat/g;->k:J

    invoke-direct {v1, p1, v2, v3}, Ltv/periscope/android/ui/chat/h;-><init>(Ltv/periscope/model/chat/Message;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 515
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/g;->notifyItemInserted(I)V

    .line 516
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/chat/g;->l:Ltv/periscope/model/chat/MessageType;

    .line 517
    :cond_1
    :goto_1
    return-void

    .line 471
    :sswitch_0
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->d:Ltv/periscope/model/chat/MessageType;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/chat/g;->l:Ltv/periscope/model/chat/MessageType;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/chat/g;->j:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->R()Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 473
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/chat/h;

    .line 474
    new-instance v2, Ltv/periscope/android/ui/chat/h;

    iget-wide v4, v0, Ltv/periscope/android/ui/chat/h;->b:J

    invoke-direct {v2, p1, v4, v5}, Ltv/periscope/android/ui/chat/h;-><init>(Ltv/periscope/model/chat/Message;J)V

    .line 475
    iget-object v3, p0, Ltv/periscope/android/ui/chat/g;->f:Ltv/periscope/android/ui/chat/g$a;

    invoke-virtual {v3}, Ltv/periscope/android/ui/chat/g$a;->c()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Ltv/periscope/android/ui/chat/g;->f:Ltv/periscope/android/ui/chat/g$a;

    invoke-virtual {v3}, Ltv/periscope/android/ui/chat/g$a;->a()Ltv/periscope/android/ui/chat/j$a;

    move-result-object v3

    invoke-virtual {v3}, Ltv/periscope/android/ui/chat/j$a;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 476
    iget-wide v4, v0, Ltv/periscope/android/ui/chat/h;->b:J

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/h;->c()I

    move-result v3

    invoke-static {v4, v5, v3}, Ltv/periscope/model/y;->a(JI)Ltv/periscope/model/y;

    move-result-object v3

    .line 477
    iget-object v4, p0, Ltv/periscope/android/ui/chat/g;->f:Ltv/periscope/android/ui/chat/g$a;

    invoke-virtual {v4, v3}, Ltv/periscope/android/ui/chat/g$a;->a(Ltv/periscope/model/y;)V

    .line 479
    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/h;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Ltv/periscope/android/ui/chat/h;->a(I)V

    .line 480
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 481
    invoke-virtual {p0, v1}, Ltv/periscope/android/ui/chat/g;->notifyItemChanged(I)V

    .line 482
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/chat/g;->l:Ltv/periscope/model/chat/MessageType;

    goto :goto_1

    .line 491
    :sswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->l:Ltv/periscope/model/chat/MessageType;

    sget-object v1, Ltv/periscope/model/chat/MessageType;->q:Ltv/periscope/model/chat/MessageType;

    if-ne v0, v1, :cond_0

    .line 492
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 493
    iget-object v1, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/chat/h;

    .line 494
    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/h;->a()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    goto/16 :goto_1

    .line 503
    :sswitch_2
    iget v0, p0, Ltv/periscope/android/ui/chat/g;->m:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    iget v0, p0, Ltv/periscope/android/ui/chat/g;->m:I

    if-ltz v0, :cond_3

    iget v0, p0, Ltv/periscope/android/ui/chat/g;->m:I

    iget-object v1, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 504
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    iget v1, p0, Ltv/periscope/android/ui/chat/g;->m:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/chat/h;

    .line 505
    iget-object v1, v0, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    if-ne v1, p1, :cond_3

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/h;->a()F

    move-result v0

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 509
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/chat/g;->m:I

    goto/16 :goto_0

    .line 468
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x5 -> :sswitch_0
        0x7 -> :sswitch_1
        0xf -> :sswitch_2
    .end sparse-switch
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Ltv/periscope/android/ui/chat/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/chat/h;

    iget-wide v0, v0, Ltv/periscope/android/ui/chat/h;->b:J

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 398
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/chat/g;->a(I)Ltv/periscope/android/ui/chat/h;

    move-result-object v0

    .line 399
    sget-object v1, Ltv/periscope/android/ui/chat/g$1;->b:[I

    iget-object v0, v0, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/chat/MessageType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 438
    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 405
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 408
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 412
    :pswitch_3
    const/16 v0, 0x9

    goto :goto_0

    .line 416
    :pswitch_4
    const/16 v0, 0xa

    goto :goto_0

    .line 419
    :pswitch_5
    const/4 v0, 0x4

    goto :goto_0

    .line 422
    :pswitch_6
    const/4 v0, 0x5

    goto :goto_0

    .line 426
    :pswitch_7
    const/4 v0, 0x6

    goto :goto_0

    .line 431
    :pswitch_8
    const/4 v0, 0x7

    goto :goto_0

    .line 434
    :pswitch_9
    const/16 v0, 0x8

    goto :goto_0

    .line 399
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_2
        :pswitch_6
    .end packed-switch
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 40
    check-cast p1, Ltv/periscope/android/ui/chat/j;

    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/chat/g;->a(Ltv/periscope/android/ui/chat/j;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/chat/g;->a(Landroid/view/ViewGroup;I)Ltv/periscope/android/ui/chat/j;

    move-result-object v0

    return-object v0
.end method
