.class Ltv/periscope/android/ui/chat/i$5;
.super Ltv/periscope/android/view/v;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/chat/i;->a(Ltv/periscope/android/ui/chat/i$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/chat/i$a;

.field final synthetic b:Landroid/view/ViewPropertyAnimator;

.field final synthetic c:Landroid/view/View;

.field final synthetic d:Ltv/periscope/android/ui/chat/i;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/chat/i;Ltv/periscope/android/ui/chat/i$a;Landroid/view/ViewPropertyAnimator;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Ltv/periscope/android/ui/chat/i$5;->d:Ltv/periscope/android/ui/chat/i;

    iput-object p2, p0, Ltv/periscope/android/ui/chat/i$5;->a:Ltv/periscope/android/ui/chat/i$a;

    iput-object p3, p0, Ltv/periscope/android/ui/chat/i$5;->b:Landroid/view/ViewPropertyAnimator;

    iput-object p4, p0, Ltv/periscope/android/ui/chat/i$5;->c:Landroid/view/View;

    invoke-direct {p0}, Ltv/periscope/android/view/v;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 348
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$5;->b:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 349
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$5;->c:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 350
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$5;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 351
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$5;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 352
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$5;->d:Ltv/periscope/android/ui/chat/i;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/i$5;->a:Ltv/periscope/android/ui/chat/i$a;

    iget-object v1, v1, Ltv/periscope/android/ui/chat/i$a;->b:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/chat/i;->dispatchChangeFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    .line 353
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$5;->d:Ltv/periscope/android/ui/chat/i;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/i;->g(Ltv/periscope/android/ui/chat/i;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/chat/i$5;->a:Ltv/periscope/android/ui/chat/i$a;

    iget-object v1, v1, Ltv/periscope/android/ui/chat/i$a;->b:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 354
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$5;->d:Ltv/periscope/android/ui/chat/i;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/i;->f(Ltv/periscope/android/ui/chat/i;)V

    .line 355
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 343
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$5;->d:Ltv/periscope/android/ui/chat/i;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/i$5;->a:Ltv/periscope/android/ui/chat/i$a;

    iget-object v1, v1, Ltv/periscope/android/ui/chat/i$a;->b:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/chat/i;->dispatchChangeStarting(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    .line 344
    return-void
.end method
