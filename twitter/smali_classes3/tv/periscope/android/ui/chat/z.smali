.class public Ltv/periscope/android/ui/chat/z;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/chat/z$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Ltv/periscope/android/ui/chat/ad;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ltv/periscope/android/ui/chat/ac;

.field private c:Ldae;

.field private d:Ltv/periscope/android/ui/chat/z$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ltv/periscope/android/ui/chat/ac;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 40
    iput-object p1, p0, Ltv/periscope/android/ui/chat/z;->a:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Ltv/periscope/android/ui/chat/z;->b:Ltv/periscope/android/ui/chat/ac;

    .line 42
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/chat/z;)Ltv/periscope/android/ui/chat/z$a;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Ltv/periscope/android/ui/chat/z;->d:Ltv/periscope/android/ui/chat/z$a;

    return-object v0
.end method

.method private a(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 262
    if-nez p1, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    invoke-virtual {p1}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    goto :goto_0
.end method

.method private b(Ltv/periscope/android/ui/chat/ad;I)V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p1, Ltv/periscope/android/ui/chat/ad;->d:Landroid/animation/Animator;

    .line 125
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 129
    :cond_0
    iget-object v1, p1, Ltv/periscope/android/ui/chat/ad;->c:Ltv/periscope/android/ui/love/SmallHeartView;

    .line 131
    if-nez v0, :cond_1

    .line 132
    const/4 v0, 0x4

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 133
    new-instance v2, Ltv/periscope/android/ui/chat/z$2;

    invoke-direct {v2, p0, v1}, Ltv/periscope/android/ui/chat/z$2;-><init>(Ltv/periscope/android/ui/chat/z;Ltv/periscope/android/ui/love/SmallHeartView;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 143
    new-instance v2, Ltv/periscope/android/ui/chat/z$3;

    invoke-direct {v2, p0, v1}, Ltv/periscope/android/ui/chat/z$3;-><init>(Ltv/periscope/android/ui/chat/z;Ltv/periscope/android/ui/love/SmallHeartView;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 152
    iput-object v0, p1, Ltv/periscope/android/ui/chat/ad;->d:Landroid/animation/Animator;

    .line 156
    :cond_1
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 157
    iget-object v0, p1, Ltv/periscope/android/ui/chat/ad;->e:Landroid/animation/Animator;

    if-nez v0, :cond_2

    .line 158
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/chat/z;->c(Ltv/periscope/android/ui/chat/ad;I)V

    .line 160
    :cond_2
    return-void

    .line 132
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f4ccccd    # 0.8f
        0x3f99999a    # 1.2f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private c(Ltv/periscope/android/ui/chat/ad;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 248
    invoke-virtual {p1}, Ltv/periscope/android/ui/chat/ad;->b()V

    .line 250
    iget-object v0, p1, Ltv/periscope/android/ui/chat/ad;->d:Landroid/animation/Animator;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/chat/z;->a(Landroid/animation/Animator;)V

    .line 251
    iget-object v0, p1, Ltv/periscope/android/ui/chat/ad;->e:Landroid/animation/Animator;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/chat/z;->a(Landroid/animation/Animator;)V

    .line 252
    iget-object v0, p1, Ltv/periscope/android/ui/chat/ad;->f:Landroid/animation/Animator;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/chat/z;->a(Landroid/animation/Animator;)V

    .line 254
    iput-object v1, p1, Ltv/periscope/android/ui/chat/ad;->d:Landroid/animation/Animator;

    .line 255
    iput-object v1, p1, Ltv/periscope/android/ui/chat/ad;->e:Landroid/animation/Animator;

    .line 256
    iput-object v1, p1, Ltv/periscope/android/ui/chat/ad;->f:Landroid/animation/Animator;

    .line 258
    iget-object v0, p1, Ltv/periscope/android/ui/chat/ad;->b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->a()V

    .line 259
    return-void
.end method

.method private c(Ltv/periscope/android/ui/chat/ad;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 163
    iget-object v0, p0, Ltv/periscope/android/ui/chat/z;->b:Ltv/periscope/android/ui/chat/ac;

    invoke-virtual {v0, p2}, Ltv/periscope/android/ui/chat/ac;->b(I)Ldce;

    move-result-object v0

    .line 164
    iget-object v1, p1, Ltv/periscope/android/ui/chat/ad;->c:Ltv/periscope/android/ui/love/SmallHeartView;

    .line 166
    iget-object v2, p1, Ltv/periscope/android/ui/chat/ad;->f:Landroid/animation/Animator;

    invoke-direct {p0, v2}, Ltv/periscope/android/ui/chat/z;->a(Landroid/animation/Animator;)V

    .line 168
    invoke-virtual {v1}, Ltv/periscope/android/ui/love/SmallHeartView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 193
    :goto_0
    return-void

    .line 172
    :cond_0
    iget-object v2, p0, Ltv/periscope/android/ui/chat/z;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0}, Ldce;->c()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v0

    .line 173
    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/love/SmallHeartView;->setColor(I)V

    .line 175
    const/4 v0, 0x2

    new-array v0, v0, [F

    invoke-virtual {v1}, Ltv/periscope/android/ui/love/SmallHeartView;->getAlpha()F

    move-result v2

    aput v2, v0, v6

    const/4 v2, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v0, v2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 176
    new-instance v2, Ltv/periscope/android/ui/chat/z$4;

    invoke-direct {v2, p0, v1}, Ltv/periscope/android/ui/chat/z$4;-><init>(Ltv/periscope/android/ui/chat/z;Ltv/periscope/android/ui/love/SmallHeartView;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 185
    new-instance v2, Ltv/periscope/android/view/m;

    invoke-direct {v2, v1}, Ltv/periscope/android/view/m;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 186
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 188
    invoke-virtual {v1}, Ltv/periscope/android/ui/love/SmallHeartView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 189
    invoke-virtual {v1, v6}, Ltv/periscope/android/ui/love/SmallHeartView;->setVisibility(I)V

    .line 192
    :cond_1
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method private d(Ltv/periscope/android/ui/chat/ad;I)V
    .locals 4

    .prologue
    .line 196
    iget-object v0, p1, Ltv/periscope/android/ui/chat/ad;->c:Ltv/periscope/android/ui/love/SmallHeartView;

    .line 198
    const/4 v1, 0x0

    iput-object v1, p1, Ltv/periscope/android/ui/chat/ad;->e:Landroid/animation/Animator;

    .line 200
    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 201
    new-instance v2, Ltv/periscope/android/ui/chat/z$5;

    invoke-direct {v2, p0, v0}, Ltv/periscope/android/ui/chat/z$5;-><init>(Ltv/periscope/android/ui/chat/z;Ltv/periscope/android/ui/love/SmallHeartView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 210
    new-instance v2, Ltv/periscope/android/ui/chat/z$6;

    invoke-direct {v2, p0, v0, v0}, Ltv/periscope/android/ui/chat/z$6;-><init>(Ltv/periscope/android/ui/chat/z;Landroid/view/View;Ltv/periscope/android/ui/love/SmallHeartView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 231
    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 233
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 234
    return-void

    .line 200
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Ltv/periscope/android/ui/chat/ad;
    .locals 4

    .prologue
    .line 58
    iget-object v0, p0, Ltv/periscope/android/ui/chat/z;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 59
    sget v1, Ltv/periscope/android/library/f$i;->ps__friends_watching_cell:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 60
    new-instance v1, Ltv/periscope/android/ui/chat/ad;

    invoke-direct {v1, v0}, Ltv/periscope/android/ui/chat/ad;-><init>(Landroid/view/View;)V

    .line 61
    iget-object v2, v1, Ltv/periscope/android/ui/chat/ad;->b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/z;->c:Ldae;

    invoke-virtual {v2, v3}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->setImageUrlLoader(Ldae;)V

    .line 62
    new-instance v2, Ltv/periscope/android/ui/chat/z$1;

    invoke-direct {v2, p0, v1}, Ltv/periscope/android/ui/chat/z$1;-><init>(Ltv/periscope/android/ui/chat/z;Ltv/periscope/android/ui/chat/ad;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    return-object v1
.end method

.method public a(Ldae;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Ltv/periscope/android/ui/chat/z;->c:Ldae;

    .line 54
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/ad;)V
    .locals 0

    .prologue
    .line 238
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/chat/z;->c(Ltv/periscope/android/ui/chat/ad;)V

    .line 239
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/ad;I)V
    .locals 4

    .prologue
    .line 75
    invoke-virtual {p1}, Ltv/periscope/android/ui/chat/ad;->a()V

    .line 76
    iget-object v0, p0, Ltv/periscope/android/ui/chat/z;->b:Ltv/periscope/android/ui/chat/ac;

    invoke-virtual {v0, p2}, Ltv/periscope/android/ui/chat/ac;->b(I)Ldce;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Ldce;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Ltv/periscope/android/ui/chat/ad;->g:Ljava/lang/String;

    .line 79
    invoke-virtual {v0}, Ldce;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ltv/periscope/android/util/aa;->a(J)I

    move-result v1

    .line 80
    iget-object v2, p1, Ltv/periscope/android/ui/chat/ad;->b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

    .line 81
    invoke-virtual {v2}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->a()V

    .line 82
    invoke-virtual {v0}, Ldce;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->a(Ljava/lang/String;)V

    .line 83
    invoke-virtual {v2, v1}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->setAvatarColor(I)V

    .line 84
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/ad;ILjava/util/List;)V
    .locals 3

    .prologue
    .line 94
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    :cond_0
    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/chat/z;->a(Ltv/periscope/android/ui/chat/ad;I)V

    .line 121
    :cond_1
    return-void

    .line 99
    :cond_2
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 100
    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 104
    check-cast v0, Ljava/lang/Integer;

    .line 106
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 108
    :pswitch_0
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/chat/z;->b(Ltv/periscope/android/ui/chat/ad;I)V

    goto :goto_0

    .line 112
    :pswitch_1
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/chat/z;->d(Ltv/periscope/android/ui/chat/ad;I)V

    goto :goto_0

    .line 106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Ltv/periscope/android/ui/chat/z$a;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Ltv/periscope/android/ui/chat/z;->d:Ltv/periscope/android/ui/chat/z$a;

    .line 50
    return-void
.end method

.method public b(Ltv/periscope/android/ui/chat/ad;)Z
    .locals 1

    .prologue
    .line 243
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/chat/z;->c(Ltv/periscope/android/ui/chat/ad;)V

    .line 244
    const/4 v0, 0x1

    return v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Ltv/periscope/android/ui/chat/z;->b:Ltv/periscope/android/ui/chat/ac;

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/ac;->a()I

    move-result v0

    return v0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Ltv/periscope/android/ui/chat/ad;

    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/chat/z;->a(Ltv/periscope/android/ui/chat/ad;I)V

    return-void
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Ltv/periscope/android/ui/chat/ad;

    invoke-virtual {p0, p1, p2, p3}, Ltv/periscope/android/ui/chat/z;->a(Ltv/periscope/android/ui/chat/ad;ILjava/util/List;)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/chat/z;->a(Landroid/view/ViewGroup;I)Ltv/periscope/android/ui/chat/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic onFailedToRecycleView(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 1

    .prologue
    .line 25
    check-cast p1, Ltv/periscope/android/ui/chat/ad;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/chat/z;->b(Ltv/periscope/android/ui/chat/ad;)Z

    move-result v0

    return v0
.end method

.method public synthetic onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Ltv/periscope/android/ui/chat/ad;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/chat/z;->a(Ltv/periscope/android/ui/chat/ad;)V

    return-void
.end method
