.class public Ltv/periscope/android/ui/chat/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/chat/a;


# instance fields
.field private final a:Lcyw;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private d:Ltv/periscope/android/ui/chat/o;

.field private e:Ltv/periscope/android/ui/broadcast/moderator/f;


# direct methods
.method public constructor <init>(Lcyw;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 23
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/chat/b;-><init>(Lcyw;Ljava/lang/String;Ltv/periscope/android/ui/chat/o;Ltv/periscope/android/ui/broadcast/moderator/f;Ljava/util/Set;)V

    .line 24
    return-void
.end method

.method constructor <init>(Lcyw;Ljava/lang/String;Ltv/periscope/android/ui/chat/o;Ltv/periscope/android/ui/broadcast/moderator/f;Ljava/util/Set;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcyw;",
            "Ljava/lang/String;",
            "Ltv/periscope/android/ui/chat/o;",
            "Ltv/periscope/android/ui/broadcast/moderator/f;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Ltv/periscope/android/ui/chat/b;->a:Lcyw;

    .line 46
    iput-object p2, p0, Ltv/periscope/android/ui/chat/b;->c:Ljava/lang/String;

    .line 47
    iput-object p4, p0, Ltv/periscope/android/ui/chat/b;->e:Ltv/periscope/android/ui/broadcast/moderator/f;

    .line 48
    iput-object p5, p0, Ltv/periscope/android/ui/chat/b;->b:Ljava/util/Set;

    .line 49
    invoke-virtual {p0, p3}, Ltv/periscope/android/ui/chat/b;->a(Ltv/periscope/android/ui/chat/o;)V

    .line 50
    return-void
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/moderator/f;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Ltv/periscope/android/ui/chat/b;->e:Ltv/periscope/android/ui/broadcast/moderator/f;

    .line 37
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/o;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Ltv/periscope/android/ui/chat/b;->d:Ltv/periscope/android/ui/chat/o;

    .line 33
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Ltv/periscope/android/ui/chat/b;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Ltv/periscope/android/ui/chat/b;->a:Lcyw;

    invoke-interface {v0, p1, p2}, Lcyw;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/b;->e:Ltv/periscope/android/ui/broadcast/moderator/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/chat/b;->e:Ltv/periscope/android/ui/broadcast/moderator/f;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/b;->c:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ltv/periscope/android/ui/broadcast/moderator/f;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Ltv/periscope/android/ui/chat/b;->d:Ltv/periscope/android/ui/chat/o;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Ltv/periscope/android/ui/chat/b;->d:Ltv/periscope/android/ui/chat/o;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/chat/o;->a(Ljava/lang/String;)I

    move-result v0

    .line 71
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Ltv/periscope/android/ui/chat/b;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method
