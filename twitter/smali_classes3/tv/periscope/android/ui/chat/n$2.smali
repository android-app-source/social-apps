.class Ltv/periscope/android/ui/chat/n$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/chat/n;->a(Ljava/lang/String;Ltv/periscope/android/analytics/summary/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ltv/periscope/android/analytics/summary/b;

.field final synthetic c:Ltv/periscope/android/ui/chat/n;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/chat/n;Ljava/lang/String;Ltv/periscope/android/analytics/summary/b;)V
    .locals 0

    .prologue
    .line 532
    iput-object p1, p0, Ltv/periscope/android/ui/chat/n$2;->c:Ltv/periscope/android/ui/chat/n;

    iput-object p2, p0, Ltv/periscope/android/ui/chat/n$2;->a:Ljava/lang/String;

    iput-object p3, p0, Ltv/periscope/android/ui/chat/n$2;->b:Ltv/periscope/android/analytics/summary/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 535
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n$2;->c:Ltv/periscope/android/ui/chat/n;

    iget-object v0, v0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-nez v0, :cond_1

    .line 546
    :cond_0
    :goto_0
    return-void

    .line 539
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n$2;->c:Ltv/periscope/android/ui/chat/n;

    iget-object v0, v0, Ltv/periscope/android/ui/chat/n;->e:Lcyw;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n$2;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 540
    if-eqz v0, :cond_0

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n$2;->c:Ltv/periscope/android/ui/chat/n;

    invoke-static {v1}, Ltv/periscope/android/ui/chat/n;->b(Ltv/periscope/android/ui/chat/n;)Ltv/periscope/android/ui/chat/x;

    move-result-object v1

    iget-object v2, v0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    iget-object v3, v0, Ltv/periscope/android/api/PsUser;->twitterId:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ltv/periscope/android/ui/chat/x;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 544
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n$2;->b:Ltv/periscope/android/analytics/summary/b;

    invoke-virtual {v1}, Ltv/periscope/android/analytics/summary/b;->A()V

    .line 545
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n$2;->c:Ltv/periscope/android/ui/chat/n;

    iget-object v1, v1, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/n$2;->c:Ltv/periscope/android/ui/chat/n;

    invoke-static {v2}, Ltv/periscope/android/ui/chat/n;->b(Ltv/periscope/android/ui/chat/n;)Ltv/periscope/android/ui/chat/x;

    move-result-object v2

    invoke-interface {v2, v0}, Ltv/periscope/android/ui/chat/x;->a(Ltv/periscope/android/api/PsUser;)Ltv/periscope/model/chat/Message;

    move-result-object v0

    invoke-interface {v1, v0}, Ltv/periscope/android/ui/chat/r;->c(Ltv/periscope/model/chat/Message;)V

    goto :goto_0
.end method
