.class Ltv/periscope/android/ui/chat/i$3;
.super Ltv/periscope/android/view/v;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/chat/i;->a(Ltv/periscope/android/ui/chat/i$b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/chat/i$b;

.field final synthetic b:Landroid/support/v7/widget/RecyclerView$ViewHolder;

.field final synthetic c:I

.field final synthetic d:Landroid/view/View;

.field final synthetic e:I

.field final synthetic f:Ltv/periscope/android/ui/chat/i;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/chat/i;Ltv/periscope/android/ui/chat/i$b;Landroid/support/v7/widget/RecyclerView$ViewHolder;ILandroid/view/View;I)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Ltv/periscope/android/ui/chat/i$3;->f:Ltv/periscope/android/ui/chat/i;

    iput-object p2, p0, Ltv/periscope/android/ui/chat/i$3;->a:Ltv/periscope/android/ui/chat/i$b;

    iput-object p3, p0, Ltv/periscope/android/ui/chat/i$3;->b:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    iput p4, p0, Ltv/periscope/android/ui/chat/i$3;->c:I

    iput-object p5, p0, Ltv/periscope/android/ui/chat/i$3;->d:Landroid/view/View;

    iput p6, p0, Ltv/periscope/android/ui/chat/i$3;->e:I

    invoke-direct {p0}, Ltv/periscope/android/view/v;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 243
    iget v0, p0, Ltv/periscope/android/ui/chat/i$3;->c:I

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$3;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 246
    :cond_0
    iget v0, p0, Ltv/periscope/android/ui/chat/i$3;->e:I

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$3;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 249
    :cond_1
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 253
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 254
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$3;->a:Ltv/periscope/android/ui/chat/i$b;

    iget-boolean v0, v0, Ltv/periscope/android/ui/chat/i$b;->f:Z

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$3;->f:Ltv/periscope/android/ui/chat/i;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/i$3;->b:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/i;->dispatchAddFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 256
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$3;->f:Ltv/periscope/android/ui/chat/i;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/i;->d(Ltv/periscope/android/ui/chat/i;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/chat/i$3;->b:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 261
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$3;->f:Ltv/periscope/android/ui/chat/i;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/i;->f(Ltv/periscope/android/ui/chat/i;)V

    .line 262
    return-void

    .line 258
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$3;->f:Ltv/periscope/android/ui/chat/i;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/i$3;->b:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/i;->dispatchMoveFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 259
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$3;->f:Ltv/periscope/android/ui/chat/i;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/i;->e(Ltv/periscope/android/ui/chat/i;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/chat/i$3;->b:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$3;->a:Ltv/periscope/android/ui/chat/i$b;

    iget-boolean v0, v0, Ltv/periscope/android/ui/chat/i$b;->f:Z

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$3;->f:Ltv/periscope/android/ui/chat/i;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/i$3;->b:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/i;->dispatchAddStarting(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 239
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$3;->f:Ltv/periscope/android/ui/chat/i;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/i$3;->b:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/i;->dispatchMoveStarting(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    goto :goto_0
.end method
