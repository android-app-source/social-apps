.class public Ltv/periscope/android/ui/chat/al;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/chat/m;
.implements Ltv/periscope/android/ui/chat/o$b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Ltv/periscope/android/ui/chat/r;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/chat/al;->a(Ltv/periscope/android/ui/chat/r;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public a(Ljava/lang/String;Ltv/periscope/android/analytics/summary/b;)V
    .locals 0

    .prologue
    .line 125
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/chatman/api/Occupant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    return-void
.end method

.method public a(Ltv/periscope/android/chat/f;)V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public a(Ltv/periscope/android/chat/g;)V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method public a(Ltv/periscope/android/player/PlayMode;)V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/r;)V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method public a(Ltv/periscope/model/StreamType;Ltv/periscope/model/u;)V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;)V
    .locals 0

    .prologue
    .line 115
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$ReportType;)V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;Z)V
    .locals 0

    .prologue
    .line 155
    return-void
.end method

.method public a(Ltv/periscope/model/chat/MessageType;)Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public b(Ltv/periscope/model/chat/Message;)V
    .locals 0

    .prologue
    .line 150
    return-void
.end method

.method public b(Ltv/periscope/model/chat/Message;Z)V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method public b(Ltv/periscope/model/chat/MessageType;)V
    .locals 0

    .prologue
    .line 110
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public c(Ltv/periscope/model/chat/Message;)V
    .locals 0

    .prologue
    .line 170
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public d(Ltv/periscope/model/chat/Message;)V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 100
    return-void
.end method

.method public j()V
    .locals 0

    .prologue
    .line 135
    return-void
.end method
