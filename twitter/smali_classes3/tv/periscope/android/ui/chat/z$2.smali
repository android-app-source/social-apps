.class Ltv/periscope/android/ui/chat/z$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/chat/z;->b(Ltv/periscope/android/ui/chat/ad;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/love/SmallHeartView;

.field final synthetic b:Ltv/periscope/android/ui/chat/z;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/chat/z;Ltv/periscope/android/ui/love/SmallHeartView;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Ltv/periscope/android/ui/chat/z$2;->b:Ltv/periscope/android/ui/chat/z;

    iput-object p2, p0, Ltv/periscope/android/ui/chat/z$2;->a:Ltv/periscope/android/ui/love/SmallHeartView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 136
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 137
    iget-object v1, p0, Ltv/periscope/android/ui/chat/z$2;->a:Ltv/periscope/android/ui/love/SmallHeartView;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/love/SmallHeartView;->setScaleX(F)V

    .line 138
    iget-object v1, p0, Ltv/periscope/android/ui/chat/z$2;->a:Ltv/periscope/android/ui/love/SmallHeartView;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/love/SmallHeartView;->setScaleY(F)V

    .line 139
    iget-object v0, p0, Ltv/periscope/android/ui/chat/z$2;->a:Ltv/periscope/android/ui/love/SmallHeartView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/love/SmallHeartView;->requestLayout()V

    .line 140
    return-void
.end method
