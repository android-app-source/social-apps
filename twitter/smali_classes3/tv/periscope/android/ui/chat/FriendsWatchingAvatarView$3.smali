.class Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldae$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->a(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$3;->b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

    iput-object p2, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$3;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 134
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$3;->a:Ljava/lang/String;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$3;->b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

    invoke-static {v1}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->a(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$3;->b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

    invoke-static {v0, p1}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->a(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 139
    new-instance v0, Landroid/graphics/BitmapShader;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$3;->b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

    invoke-static {v1}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->b(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 140
    iget-object v1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$3;->b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

    invoke-static {v1}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->c(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 142
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$3;->b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->d(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;)V

    goto :goto_0
.end method
