.class public Ltv/periscope/android/ui/chat/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/chat/m;
.implements Ltv/periscope/android/ui/chat/o$b;
.implements Ltv/periscope/android/ui/chat/p;


# static fields
.field static final a:J
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field static final b:J

.field static c:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field private A:Ljava/lang/String;

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:Z

.field private G:Z

.field private H:Z

.field private final I:Ljava/lang/Runnable;

.field final d:Landroid/content/res/Resources;

.field final e:Lcyw;

.field f:Ltv/periscope/android/ui/chat/r;

.field g:Ltv/periscope/model/u;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field h:Ljava/lang/String;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field i:Ltv/periscope/model/StreamType;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final j:Landroid/os/Handler;

.field private final k:Ltv/periscope/android/api/ApiManager;

.field private l:Ltv/periscope/android/chat/f;

.field private final m:Ltv/periscope/android/chat/a;

.field private final n:Ltv/periscope/android/player/d;

.field private final o:Ltv/periscope/android/player/e;

.field private final p:Ltv/periscope/android/api/PsUser;

.field private final q:Z

.field private final r:Ltv/periscope/android/ui/chat/ae$a;

.field private final s:Ltv/periscope/android/ui/broadcast/ar;

.field private final t:Ltv/periscope/android/ui/chat/w;

.field private final u:Ldbd;

.field private final v:Lcxm;

.field private final w:Ltv/periscope/android/ui/chat/x;

.field private final x:Ltv/periscope/android/ui/chat/aj;

.field private y:Ltv/periscope/android/player/PlayMode;

.field private z:Ltv/periscope/android/chat/g;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 47
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ltv/periscope/android/ui/chat/n;->a:J

    .line 48
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ltv/periscope/android/ui/chat/n;->b:J

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/os/Handler;Ltv/periscope/android/api/ApiManager;Ltv/periscope/android/chat/a;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Ltv/periscope/android/api/PsUser;ZLtv/periscope/android/ui/chat/ae$a;Ltv/periscope/android/ui/broadcast/ar;Ltv/periscope/android/ui/chat/w;Lcyw;Ldbd;Lcxm;Ltv/periscope/android/ui/chat/x;Ltv/periscope/android/ui/chat/aj;)V
    .locals 2

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const/4 v1, 0x1

    iput-boolean v1, p0, Ltv/periscope/android/ui/chat/n;->F:Z

    .line 613
    new-instance v1, Ltv/periscope/android/ui/chat/n$5;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/chat/n$5;-><init>(Ltv/periscope/android/ui/chat/n;)V

    iput-object v1, p0, Ltv/periscope/android/ui/chat/n;->I:Ljava/lang/Runnable;

    .line 104
    iput-object p1, p0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    .line 105
    iput-object p2, p0, Ltv/periscope/android/ui/chat/n;->j:Landroid/os/Handler;

    .line 106
    iput-object p3, p0, Ltv/periscope/android/ui/chat/n;->k:Ltv/periscope/android/api/ApiManager;

    .line 107
    iput-object p4, p0, Ltv/periscope/android/ui/chat/n;->m:Ltv/periscope/android/chat/a;

    .line 108
    iput-object p5, p0, Ltv/periscope/android/ui/chat/n;->n:Ltv/periscope/android/player/d;

    .line 109
    iput-object p6, p0, Ltv/periscope/android/ui/chat/n;->o:Ltv/periscope/android/player/e;

    .line 110
    iput-object p7, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    .line 111
    iput-boolean p8, p0, Ltv/periscope/android/ui/chat/n;->q:Z

    .line 112
    iput-object p9, p0, Ltv/periscope/android/ui/chat/n;->r:Ltv/periscope/android/ui/chat/ae$a;

    .line 113
    iput-object p10, p0, Ltv/periscope/android/ui/chat/n;->s:Ltv/periscope/android/ui/broadcast/ar;

    .line 114
    iput-object p11, p0, Ltv/periscope/android/ui/chat/n;->t:Ltv/periscope/android/ui/chat/w;

    .line 115
    iput-object p12, p0, Ltv/periscope/android/ui/chat/n;->e:Lcyw;

    .line 116
    iput-object p13, p0, Ltv/periscope/android/ui/chat/n;->u:Ldbd;

    .line 117
    move-object/from16 v0, p14

    iput-object v0, p0, Ltv/periscope/android/ui/chat/n;->v:Lcxm;

    .line 118
    move-object/from16 v0, p15

    iput-object v0, p0, Ltv/periscope/android/ui/chat/n;->w:Ltv/periscope/android/ui/chat/x;

    .line 119
    move-object/from16 v0, p16

    iput-object v0, p0, Ltv/periscope/android/ui/chat/n;->x:Ltv/periscope/android/ui/chat/aj;

    .line 120
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/chat/n;)Ltv/periscope/android/ui/chat/w;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->t:Ltv/periscope/android/ui/chat/w;

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/chat/n;Ltv/periscope/model/chat/Message;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/model/chat/Message;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ltv/periscope/model/chat/Message;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 624
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 625
    iget-boolean v0, p0, Ltv/periscope/android/ui/chat/n;->F:Z

    if-nez v0, :cond_0

    .line 642
    :goto_0
    return-void

    .line 628
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/n;->q()V

    .line 631
    :cond_1
    iget v0, p0, Ltv/periscope/android/ui/chat/n;->E:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltv/periscope/android/ui/chat/n;->E:I

    .line 632
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/chat/n;->d(Ltv/periscope/model/chat/Message;)V

    .line 633
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->t:Ltv/periscope/android/ui/chat/w;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/w;->P()V

    .line 634
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->u:Ldbd;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ldbd;->a(Ljava/lang/String;)Z

    .line 636
    sget-object v0, Ltv/periscope/model/chat/MessageType;->b:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 637
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "send chat #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/chat/n;->E:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->l:Ltv/periscope/android/chat/f;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/chat/f;->a(Ltv/periscope/model/chat/Message;Ljava/lang/String;)V

    .line 639
    const/4 v0, 0x1

    sput-boolean v0, Ltv/periscope/android/ui/chat/n;->c:Z

    .line 641
    :cond_2
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/chat/n;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/ui/chat/n;Z)Z
    .locals 0

    .prologue
    .line 44
    iput-boolean p1, p0, Ltv/periscope/android/ui/chat/n;->F:Z

    return p1
.end method

.method static synthetic b(Ltv/periscope/android/ui/chat/n;)Ltv/periscope/android/ui/chat/x;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->w:Ltv/periscope/android/ui/chat/x;

    return-object v0
.end method

.method private b(J)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 239
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->A:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 240
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->e:Lcyw;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/n;->A:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 241
    if-eqz v0, :cond_1

    iget-object v0, v0, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    .line 245
    :goto_0
    sget-object v1, Ltv/periscope/model/chat/MessageType;->b:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {p0, v1}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-boolean v1, Ltv/periscope/android/ui/chat/n;->c:Z

    if-nez v1, :cond_0

    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 246
    new-instance v1, Lcxu;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/n;->v:Lcxm;

    invoke-direct {v1, v2}, Lcxu;-><init>(Lcxm;)V

    .line 247
    invoke-virtual {v1, p1, p2}, Lcxu;->a(J)Ldcd;

    move-result-object v2

    .line 248
    if-eqz v2, :cond_0

    .line 249
    iget-object v3, p0, Ltv/periscope/android/ui/chat/n;->j:Landroid/os/Handler;

    new-instance v4, Ltv/periscope/android/ui/chat/n$1;

    invoke-direct {v4, p0, v2, v0, v1}, Ltv/periscope/android/ui/chat/n$1;-><init>(Ltv/periscope/android/ui/chat/n;Ldcd;Ljava/lang/String;Lcxu;)V

    sget-wide v0, Ltv/periscope/android/ui/chat/n;->a:J

    invoke-virtual {v3, v4, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 269
    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    .line 241
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 243
    goto :goto_0
.end method

.method private c(J)I
    .locals 5
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 354
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/n;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    invoke-static {v0, p1, p2}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v0

    .line 358
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v0

    goto :goto_0
.end method

.method private m()Z
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->z:Ltv/periscope/android/chat/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->z:Ltv/periscope/android/chat/g;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    invoke-virtual {v1}, Ltv/periscope/model/u;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/chat/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 3

    .prologue
    .line 304
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-nez v0, :cond_0

    .line 329
    :goto_0
    return-void

    .line 308
    :cond_0
    sget-object v0, Ltv/periscope/android/ui/chat/n$7;->a:[I

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->i:Ltv/periscope/model/StreamType;

    invoke-virtual {v1}, Ltv/periscope/model/StreamType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 323
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    sget-object v1, Ltv/periscope/android/ui/chat/ChatState;->a:Ltv/periscope/android/ui/chat/ChatState;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/r;->a(Ltv/periscope/android/ui/chat/ChatState;)V

    .line 328
    :goto_1
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "State="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/chat/n;->i:Ltv/periscope/model/StreamType;

    invoke-virtual {v2}, Ltv/periscope/model/StreamType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    sget-object v1, Ltv/periscope/android/ui/chat/ChatState;->b:Ltv/periscope/android/ui/chat/ChatState;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/r;->a(Ltv/periscope/android/ui/chat/ChatState;)V

    goto :goto_1

    .line 314
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    sget-object v1, Ltv/periscope/android/ui/chat/ChatState;->c:Ltv/periscope/android/ui/chat/ChatState;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/r;->a(Ltv/periscope/android/ui/chat/ChatState;)V

    goto :goto_1

    .line 318
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    sget-object v1, Ltv/periscope/android/ui/chat/ChatState;->d:Ltv/periscope/android/ui/chat/ChatState;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/r;->a(Ltv/periscope/android/ui/chat/ChatState;)V

    goto :goto_1

    .line 308
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private o()Z
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->y:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Ltv/periscope/model/chat/Message;
    .locals 8

    .prologue
    .line 395
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v0, v0, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v1, v1, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v2, v2, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    .line 399
    invoke-virtual {v3}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v3

    .line 400
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/n;->l()J

    move-result-wide v4

    .line 401
    invoke-static {}, Ldaf;->b()J

    move-result-wide v6

    .line 395
    invoke-static/range {v0 .. v7}, Ltv/periscope/model/chat/Message;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JJ)Ltv/periscope/model/chat/Message;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 4

    .prologue
    .line 605
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/r;->M()V

    .line 608
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->j:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->I:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 609
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/chat/n;->F:Z

    .line 610
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->j:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->I:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 611
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    if-nez v0, :cond_1

    .line 374
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->s:Ltv/periscope/android/ui/broadcast/ar;

    if-eqz v0, :cond_0

    .line 372
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/n;->p()Ltv/periscope/model/chat/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/n;->d(Ltv/periscope/model/chat/Message;)V

    goto :goto_0
.end method

.method a(IZ)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 670
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->n:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/chat/r;->a(IZ)V

    .line 673
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 230
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/chat/n;->b(J)V

    .line 231
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p1, Ltv/periscope/android/ui/chat/r;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/android/ui/chat/r;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Ltv/periscope/android/ui/chat/n;->A:Ljava/lang/String;

    .line 161
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 553
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-nez v0, :cond_1

    .line 591
    :cond_0
    :goto_0
    return-void

    .line 557
    :cond_1
    sget-object v0, Ltv/periscope/model/chat/MessageType;->b:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v1, v0, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v2, v0, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v3, v0, Ltv/periscope/android/api/PsUser;->initials:Ljava/lang/String;

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v4, v0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    .line 564
    invoke-virtual {v0}, Ltv/periscope/android/api/PsUser;->getProfileUrlMedium()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v6, v0, Ltv/periscope/android/api/PsUser;->vipBadge:Ljava/lang/String;

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    .line 566
    invoke-virtual {v0}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v7

    .line 567
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/n;->l()J

    move-result-wide v8

    .line 568
    invoke-static {}, Ldaf;->b()J

    move-result-wide v10

    move-object v0, p1

    .line 558
    invoke-static/range {v0 .. v11}, Ltv/periscope/model/chat/Message;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JJ)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 573
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->h:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->m:Ltv/periscope/android/chat/a;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/n;->h:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Ltv/periscope/android/chat/a;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 574
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/n;->d(Ltv/periscope/model/chat/Message;)V

    goto :goto_0

    .line 575
    :cond_2
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->m:Ltv/periscope/android/chat/a;

    invoke-interface {v1, p1}, Ltv/periscope/android/chat/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 576
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    new-instance v2, Ltv/periscope/android/ui/chat/n$3;

    invoke-direct {v2, p0, v0, p2}, Ltv/periscope/android/ui/chat/n$3;-><init>(Ltv/periscope/android/ui/chat/n;Ltv/periscope/model/chat/Message;Ljava/lang/String;)V

    new-instance v3, Ltv/periscope/android/ui/chat/n$4;

    invoke-direct {v3, p0}, Ltv/periscope/android/ui/chat/n$4;-><init>(Ltv/periscope/android/ui/chat/n;)V

    invoke-interface {v1, v0, v2, v3}, Ltv/periscope/android/ui/chat/r;->a(Ltv/periscope/model/chat/Message;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    .line 588
    :cond_3
    invoke-direct {p0, v0, p2}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/model/chat/Message;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ltv/periscope/android/analytics/summary/b;)V
    .locals 4

    .prologue
    .line 528
    iget-boolean v0, p0, Ltv/periscope/android/ui/chat/n;->H:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->e:Lcyw;

    invoke-interface {v0, p1}, Lcyw;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 548
    :cond_0
    :goto_0
    return-void

    .line 531
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/chat/n;->H:Z

    .line 532
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->j:Landroid/os/Handler;

    new-instance v1, Ltv/periscope/android/ui/chat/n$2;

    invoke-direct {v1, p0, p1, p2}, Ltv/periscope/android/ui/chat/n$2;-><init>(Ltv/periscope/android/ui/chat/n;Ljava/lang/String;Ltv/periscope/android/analytics/summary/b;)V

    iget-object v2, p0, Ltv/periscope/android/ui/chat/n;->w:Ltv/periscope/android/ui/chat/x;

    .line 547
    invoke-interface {v2}, Ltv/periscope/android/ui/chat/x;->a()J

    move-result-wide v2

    .line 532
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 406
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    if-nez v2, :cond_1

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/chat/n;->k:Ltv/periscope/android/api/ApiManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    invoke-virtual {v3}, Ltv/periscope/model/u;->b()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v2, v3, v0}, Ltv/periscope/android/api/ApiManager;->shareBroadcast(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    .line 411
    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    const/4 v2, 0x1

    move/from16 v16, v2

    .line 412
    :goto_1
    if-eqz v16, :cond_5

    .line 413
    const-string/jumbo v2, "CM"

    const-string/jumbo v3, "share with all followers"

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :goto_2
    sget-object v2, Ltv/periscope/model/chat/MessageType;->g:Ltv/periscope/model/chat/MessageType;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 420
    if-eqz v16, :cond_6

    .line 421
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    sget v3, Ltv/periscope/android/library/f$l;->ps__invited_followers:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 422
    const-wide/16 v2, 0x0

    move-wide v14, v2

    move-object v3, v4

    .line 429
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v4, v2, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v5, v2, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v6, v2, Ltv/periscope/android/api/PsUser;->initials:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v7, v2, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    .line 435
    invoke-virtual {v2}, Ltv/periscope/android/api/PsUser;->getProfileUrlMedium()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    .line 436
    invoke-virtual {v2}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v9

    .line 437
    invoke-virtual/range {p0 .. p0}, Ltv/periscope/android/ui/chat/n;->l()J

    move-result-wide v10

    .line 438
    invoke-static {}, Ldaf;->b()J

    move-result-wide v12

    .line 439
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    .line 429
    invoke-static/range {v3 .. v14}, Ltv/periscope/model/chat/Message;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JJLjava/lang/Long;)Ltv/periscope/model/chat/Message;

    move-result-object v2

    .line 441
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/chat/n;->d(Ltv/periscope/model/chat/Message;)V

    .line 442
    move-object/from16 v0, p0

    iget v3, v0, Ltv/periscope/android/ui/chat/n;->B:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 448
    if-eqz v16, :cond_3

    .line 449
    move-object/from16 v0, p0

    iget v3, v0, Ltv/periscope/android/ui/chat/n;->B:I

    or-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Ltv/periscope/android/ui/chat/n;->B:I

    .line 451
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/chat/n;->l:Ltv/periscope/android/chat/f;

    invoke-virtual {v3, v2}, Ltv/periscope/android/chat/f;->a(Ltv/periscope/model/chat/Message;)V

    goto/16 :goto_0

    .line 411
    :cond_4
    const/4 v2, 0x0

    move/from16 v16, v2

    goto/16 :goto_1

    .line 415
    :cond_5
    const-string/jumbo v2, "CM"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "share with "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " followers"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 424
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    sget v3, Ltv/periscope/android/library/f$j;->ps__invited_num_followers:I

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v7, v7, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    .line 425
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    int-to-long v8, v8

    const/4 v10, 0x1

    invoke-static {v7, v8, v9, v10}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 424
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 426
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    int-to-long v2, v2

    move-wide v14, v2

    move-object v3, v4

    goto/16 :goto_3
.end method

.method public a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/chatman/api/Occupant;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 273
    iget-boolean v0, p0, Ltv/periscope/android/ui/chat/n;->G:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 274
    iput-boolean v5, p0, Ltv/periscope/android/ui/chat/n;->G:Z

    .line 275
    const/4 v0, 0x0

    .line 276
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v4, :cond_2

    .line 277
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x3

    .line 278
    iget-object v2, p0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    sget v3, Ltv/periscope/android/library/f$l;->ps__four_plus_following_in_chat:I

    new-array v4, v4, [Ljava/lang/Object;

    .line 279
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    iget-object v0, v0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    aput-object v0, v4, v6

    .line 280
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    iget-object v0, v0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    aput-object v0, v4, v5

    .line 281
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    iget-object v0, v0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    aput-object v0, v4, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v8

    .line 278
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 299
    :cond_0
    :goto_0
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    sget-object v3, Ltv/periscope/model/chat/MessageType;->y:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {v2, v3}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v2

    invoke-virtual {v2, v0}, Ltv/periscope/model/chat/Message$a;->h(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    invoke-interface {v1, v0}, Ltv/periscope/android/ui/chat/r;->c(Ltv/periscope/model/chat/Message;)V

    .line 301
    :cond_1
    return-void

    .line 282
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v4, :cond_3

    .line 283
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    sget v2, Ltv/periscope/android/library/f$l;->ps__four_following_in_chat:I

    new-array v3, v8, [Ljava/lang/Object;

    .line 284
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    iget-object v0, v0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    aput-object v0, v3, v6

    .line 285
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    iget-object v0, v0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    aput-object v0, v3, v5

    .line 286
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    iget-object v0, v0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    aput-object v0, v3, v7

    .line 283
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 287
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v8, :cond_4

    .line 288
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    sget v2, Ltv/periscope/android/library/f$l;->ps__three_following_in_chat:I

    new-array v3, v8, [Ljava/lang/Object;

    .line 289
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    iget-object v0, v0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    aput-object v0, v3, v6

    .line 290
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    iget-object v0, v0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    aput-object v0, v3, v5

    .line 291
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    iget-object v0, v0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    aput-object v0, v3, v7

    .line 288
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 292
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v7, :cond_5

    .line 293
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    sget v2, Ltv/periscope/android/library/f$l;->ps__two_following_in_chat:I

    new-array v3, v7, [Ljava/lang/Object;

    .line 294
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    iget-object v0, v0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    aput-object v0, v3, v6

    .line 295
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    iget-object v0, v0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    aput-object v0, v3, v5

    .line 293
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 296
    :cond_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 297
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    sget v2, Ltv/periscope/android/library/f$l;->ps__one_following_in_chat:I

    new-array v3, v5, [Ljava/lang/Object;

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    iget-object v0, v0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public a(Ltv/periscope/android/chat/f;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Ltv/periscope/android/ui/chat/n;->l:Ltv/periscope/android/chat/f;

    .line 124
    return-void
.end method

.method public a(Ltv/periscope/android/chat/g;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Ltv/periscope/android/ui/chat/n;->z:Ltv/periscope/android/chat/g;

    .line 156
    return-void
.end method

.method public a(Ltv/periscope/android/player/PlayMode;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Ltv/periscope/android/ui/chat/n;->y:Ltv/periscope/android/player/PlayMode;

    .line 144
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/r;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    .line 129
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/n;->k()V

    .line 130
    return-void
.end method

.method public a(Ltv/periscope/model/StreamType;Ltv/periscope/model/u;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Ltv/periscope/android/ui/chat/n;->i:Ltv/periscope/model/StreamType;

    .line 149
    iput-object p2, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    .line 150
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/n;->n()V

    .line 151
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;)V
    .locals 12

    .prologue
    .line 483
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-nez v0, :cond_0

    .line 512
    :goto_0
    return-void

    .line 487
    :cond_0
    sget-object v0, Ltv/periscope/model/chat/MessageType;->b:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 489
    iget-boolean v0, p0, Ltv/periscope/android/ui/chat/n;->q:Z

    if-eqz v0, :cond_2

    .line 491
    invoke-static {}, Ldaf;->b()J

    move-result-wide v0

    .line 492
    invoke-static {}, Ldaf;->b()J

    move-result-wide v2

    .line 493
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v4

    .line 494
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v5

    .line 495
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->j()Ljava/lang/String;

    move-result-object v6

    .line 490
    invoke-static/range {v0 .. v6}, Ltv/periscope/model/chat/Message;->a(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 509
    :goto_1
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->l:Ltv/periscope/android/chat/f;

    invoke-virtual {v1, v0}, Ltv/periscope/android/chat/f;->a(Ltv/periscope/model/chat/Message;)V

    .line 511
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/r;->N()V

    goto :goto_0

    .line 497
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v0, v0, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v1, v1, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v2, v2, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    .line 501
    invoke-virtual {v3}, Ltv/periscope/android/api/PsUser;->getProfileUrlMedium()Ljava/lang/String;

    move-result-object v3

    .line 502
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/n;->l()J

    move-result-wide v4

    .line 503
    invoke-static {}, Ldaf;->b()J

    move-result-wide v6

    .line 504
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v8

    .line 505
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v9

    .line 506
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v10

    .line 507
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->j()Ljava/lang/String;

    move-result-object v11

    .line 497
    invoke-static/range {v0 .. v11}, Ltv/periscope/model/chat/Message;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/model/chat/Message;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$ReportType;)V
    .locals 2

    .prologue
    .line 516
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    if-nez v0, :cond_1

    .line 524
    :cond_0
    :goto_0
    return-void

    .line 520
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    invoke-virtual {v0}, Ltv/periscope/model/u;->b()Ljava/lang/String;

    move-result-object v0

    .line 521
    sget-object v1, Ltv/periscope/model/chat/MessageType;->b:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {p0, v1}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 522
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->k:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v1, p1, v0, p2}, Ltv/periscope/android/api/ApiManager;->reportComment(Ltv/periscope/model/chat/Message;Ljava/lang/String;Ltv/periscope/model/chat/MessageType$ReportType;)Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ltv/periscope/model/chat/Message;Z)V
    .locals 4

    .prologue
    .line 664
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v0

    invoke-virtual {p0, v0, p2}, Ltv/periscope/android/ui/chat/n;->a(IZ)V

    .line 665
    return-void
.end method

.method public a(Ltv/periscope/model/chat/MessageType;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 168
    sget-object v2, Ltv/periscope/model/chat/MessageType;->d:Ltv/periscope/model/chat/MessageType;

    if-ne v2, p1, :cond_2

    .line 170
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/n;->m()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Ltv/periscope/model/StreamType;->b:Ltv/periscope/model/StreamType;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/n;->i:Ltv/periscope/model/StreamType;

    invoke-virtual {v2, v3}, Ltv/periscope/model/StreamType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 173
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 170
    goto :goto_0

    .line 173
    :cond_2
    iget-object v2, p0, Ltv/periscope/android/ui/chat/n;->n:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Ltv/periscope/android/ui/chat/n;->m()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Ltv/periscope/model/StreamType;->b:Ltv/periscope/model/StreamType;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/n;->i:Ltv/periscope/model/StreamType;

    invoke-virtual {v2, v3}, Ltv/periscope/model/StreamType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->l:Ltv/periscope/android/chat/f;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->z:Ltv/periscope/android/chat/g;

    invoke-virtual {v0, v1}, Ltv/periscope/android/chat/f;->a(Ltv/periscope/android/chat/g;)V

    .line 200
    return-void
.end method

.method b(IZ)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 683
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->n:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/chat/r;->b(IZ)V

    .line 686
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 647
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-nez v0, :cond_0

    .line 659
    :goto_0
    return-void

    .line 651
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->j:Landroid/os/Handler;

    new-instance v1, Ltv/periscope/android/ui/chat/n$6;

    invoke-direct {v1, p0, p1}, Ltv/periscope/android/ui/chat/n$6;-><init>(Ltv/periscope/android/ui/chat/n;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public b(Ltv/periscope/model/chat/Message;)V
    .locals 0

    .prologue
    .line 601
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/chat/n;->d(Ltv/periscope/model/chat/Message;)V

    .line 602
    return-void
.end method

.method public b(Ltv/periscope/model/chat/Message;Z)V
    .locals 4

    .prologue
    .line 677
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->d:Landroid/content/res/Resources;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v0

    invoke-virtual {p0, v0, p2}, Ltv/periscope/android/ui/chat/n;->b(IZ)V

    .line 678
    return-void
.end method

.method public b(Ltv/periscope/model/chat/MessageType;)V
    .locals 9

    .prologue
    .line 457
    sget-object v0, Ltv/periscope/model/chat/MessageType;->n:Ltv/periscope/model/chat/MessageType;

    if-eq p1, v0, :cond_0

    sget-object v0, Ltv/periscope/model/chat/MessageType;->G:Ltv/periscope/model/chat/MessageType;

    if-eq p1, v0, :cond_0

    .line 458
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid MessageType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 460
    :cond_0
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 461
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v0, v0, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v1, v1, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v2, v2, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    iget-object v3, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    .line 465
    invoke-virtual {v3}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v3

    .line 466
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/n;->l()J

    move-result-wide v4

    .line 467
    invoke-static {}, Ldaf;->b()J

    move-result-wide v6

    move-object v8, p1

    .line 461
    invoke-static/range {v0 .. v8}, Ltv/periscope/model/chat/Message;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JJLtv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 470
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/n;->d(Ltv/periscope/model/chat/Message;)V

    .line 471
    iget v1, p0, Ltv/periscope/android/ui/chat/n;->B:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 479
    :cond_1
    :goto_0
    return-void

    .line 476
    :cond_2
    iget v1, p0, Ltv/periscope/android/ui/chat/n;->B:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Ltv/periscope/android/ui/chat/n;->B:I

    .line 477
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->l:Ltv/periscope/android/chat/f;

    invoke-virtual {v1, v0}, Ltv/periscope/android/chat/f;->a(Ltv/periscope/model/chat/Message;)V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-eqz v0, :cond_0

    .line 205
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "Chat State Changed: Connecting"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    sget-object v1, Ltv/periscope/android/ui/chat/ChatState;->a:Ltv/periscope/android/ui/chat/ChatState;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/r;->a(Ltv/periscope/android/ui/chat/ChatState;)V

    .line 208
    :cond_0
    return-void
.end method

.method public c(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/r;->N()V

    .line 706
    :cond_0
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 212
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/n;->n()V

    .line 213
    return-void
.end method

.method public d(Ltv/periscope/model/chat/Message;)V
    .locals 3

    .prologue
    .line 690
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->H:Ltv/periscope/model/chat/MessageType;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->w:Ltv/periscope/android/ui/chat/x;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/ui/chat/x;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 699
    :cond_0
    :goto_0
    return-void

    .line 693
    :cond_1
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->I:Ltv/periscope/model/chat/MessageType;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->x:Ltv/periscope/android/ui/chat/aj;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/aj;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 696
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->n:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->u:Ldbd;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ldbd;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 697
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/chat/r;->c(Ltv/periscope/model/chat/Message;)V

    goto :goto_0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/n;->n()V

    .line 226
    return-void
.end method

.method public f()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 333
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    if-nez v0, :cond_1

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    iget v0, p0, Ltv/periscope/android/ui/chat/n;->C:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltv/periscope/android/ui/chat/n;->C:I

    .line 339
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    invoke-virtual {v0}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/chat/n;->c(J)I

    move-result v0

    invoke-virtual {p0, v0, v4}, Ltv/periscope/android/ui/chat/n;->a(IZ)V

    .line 340
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->t:Ltv/periscope/android/ui/chat/w;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v1, v1, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    invoke-virtual {v2}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3, v4}, Ltv/periscope/android/ui/chat/w;->b(Ljava/lang/String;JZ)V

    .line 342
    sget-object v0, Ltv/periscope/model/chat/MessageType;->c:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->r:Ltv/periscope/android/ui/chat/ae$a;

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/ae$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->p:Ltv/periscope/android/api/PsUser;

    iget-object v0, v0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    .line 345
    invoke-virtual {v1}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v1

    .line 346
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/n;->l()J

    move-result-wide v2

    .line 347
    invoke-static {}, Ldaf;->b()J

    move-result-wide v4

    .line 343
    invoke-static/range {v0 .. v5}, Ltv/periscope/model/chat/Message;->a(Ljava/lang/String;Ljava/lang/Long;JJ)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 348
    iget-object v1, p0, Ltv/periscope/android/ui/chat/n;->l:Ltv/periscope/android/chat/f;

    invoke-virtual {v1, v0}, Ltv/periscope/android/chat/f;->a(Ltv/periscope/model/chat/Message;)V

    goto :goto_0
.end method

.method public g()V
    .locals 3

    .prologue
    .line 378
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    if-nez v0, :cond_1

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 382
    :cond_1
    iget v0, p0, Ltv/periscope/android/ui/chat/n;->D:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltv/periscope/android/ui/chat/n;->D:I

    .line 384
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    invoke-virtual {v0}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/chat/n;->c(J)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/ui/chat/n;->b(IZ)V

    .line 385
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->t:Ltv/periscope/android/ui/chat/w;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/w;->O()V

    .line 387
    sget-object v0, Ltv/periscope/model/chat/MessageType;->q:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    const-string/jumbo v0, "CM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "send screenshot #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/chat/n;->D:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->l:Ltv/periscope/android/chat/f;

    invoke-direct {p0}, Ltv/periscope/android/ui/chat/n;->p()Ltv/periscope/model/chat/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/chat/f;->a(Ltv/periscope/model/chat/Message;)V

    goto :goto_0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Ltv/periscope/android/ui/chat/n;->C:I

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Ltv/periscope/android/ui/chat/n;->E:I

    return v0
.end method

.method public j()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 134
    iput-object v0, p0, Ltv/periscope/android/ui/chat/n;->f:Ltv/periscope/android/ui/chat/r;

    .line 135
    iput-object v0, p0, Ltv/periscope/android/ui/chat/n;->y:Ltv/periscope/android/player/PlayMode;

    .line 136
    iput-object v0, p0, Ltv/periscope/android/ui/chat/n;->g:Ltv/periscope/model/u;

    .line 137
    iput-object v0, p0, Ltv/periscope/android/ui/chat/n;->z:Ltv/periscope/android/chat/g;

    .line 138
    iput-object v0, p0, Ltv/periscope/android/ui/chat/n;->h:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 192
    iput v0, p0, Ltv/periscope/android/ui/chat/n;->C:I

    .line 193
    iput v0, p0, Ltv/periscope/android/ui/chat/n;->D:I

    .line 194
    iput v0, p0, Ltv/periscope/android/ui/chat/n;->E:I

    .line 195
    return-void
.end method

.method l()J
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 595
    iget-object v0, p0, Ltv/periscope/android/ui/chat/n;->o:Ltv/periscope/android/player/e;

    invoke-interface {v0}, Ltv/periscope/android/player/e;->m()J

    move-result-wide v0

    .line 596
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    invoke-static {}, Ldaf;->b()J

    move-result-wide v0

    :cond_0
    return-wide v0
.end method
