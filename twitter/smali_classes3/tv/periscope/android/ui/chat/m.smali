.class public interface abstract Ltv/periscope/android/ui/chat/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/b;
.implements Ltv/periscope/android/ui/chat/ag;
.implements Ltv/periscope/android/ui/chat/ah;
.implements Ltv/periscope/android/ui/chat/ai;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltv/periscope/android/ui/b",
        "<",
        "Ltv/periscope/android/ui/chat/r;",
        ">;",
        "Ltv/periscope/android/ui/chat/ag;",
        "Ltv/periscope/android/ui/chat/ah;",
        "Ltv/periscope/android/ui/chat/ai;"
    }
.end annotation


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(J)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;Ltv/periscope/android/analytics/summary/b;)V
.end method

.method public abstract a(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/chatman/api/Occupant;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Ltv/periscope/android/chat/f;)V
.end method

.method public abstract a(Ltv/periscope/android/chat/g;)V
.end method

.method public abstract a(Ltv/periscope/android/player/PlayMode;)V
.end method

.method public abstract a(Ltv/periscope/model/StreamType;Ltv/periscope/model/u;)V
.end method

.method public abstract a(Ltv/periscope/model/chat/Message;)V
.end method

.method public abstract a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$ReportType;)V
.end method

.method public abstract a(Ltv/periscope/model/chat/MessageType;)Z
.end method

.method public abstract b()V
.end method

.method public abstract b(Ltv/periscope/model/chat/MessageType;)V
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()V
.end method

.method public abstract g()V
.end method
