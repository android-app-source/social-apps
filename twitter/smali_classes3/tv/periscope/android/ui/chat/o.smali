.class public Ltv/periscope/android/ui/chat/o;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/chat/o$c;,
        Ltv/periscope/android/ui/chat/o$a;,
        Ltv/periscope/android/ui/chat/o$b;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/ui/chat/o$b;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/ui/chat/o$c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/ui/chat/o$a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:Lcyw;

.field private final f:Lcyn;

.field private final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ltv/periscope/android/ui/chat/ae$a;

.field private final i:Z


# direct methods
.method public constructor <init>(Lcyw;Lcyn;ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/o;->g:Ljava/util/HashMap;

    .line 85
    iput-object p1, p0, Ltv/periscope/android/ui/chat/o;->e:Lcyw;

    .line 86
    iput-object p2, p0, Ltv/periscope/android/ui/chat/o;->f:Lcyn;

    .line 87
    iput-boolean p3, p0, Ltv/periscope/android/ui/chat/o;->i:Z

    .line 88
    iput-object p4, p0, Ltv/periscope/android/ui/chat/o;->d:Ljava/lang/String;

    .line 89
    new-instance v0, Ltv/periscope/android/ui/chat/ae$a;

    const/16 v1, 0x19

    const/16 v2, 0x1f4

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Ltv/periscope/android/ui/chat/ae$a;-><init>(III)V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/o;->h:Ltv/periscope/android/ui/chat/ae$a;

    .line 91
    return-void
.end method

.method private a(Ljava/util/List;Ltv/periscope/android/ui/chat/o$c;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/chatman/api/Occupant;",
            ">;",
            "Ltv/periscope/android/ui/chat/o$c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 197
    if-eqz p1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->f:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/o;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v1

    .line 199
    if-nez v1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 203
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/api/Occupant;

    .line 204
    iget-object v4, v0, Ltv/periscope/chatman/api/Occupant;->userId:Ljava/lang/String;

    invoke-virtual {v1}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Ltv/periscope/android/ui/chat/o;->e:Lcyw;

    iget-object v5, v0, Ltv/periscope/chatman/api/Occupant;->userId:Ljava/lang/String;

    iget-object v6, v0, Ltv/periscope/chatman/api/Occupant;->twitterId:Ljava/lang/String;

    .line 205
    invoke-interface {v4, v5, v6}, Lcyw;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 206
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 209
    :cond_3
    invoke-interface {p2, v2}, Ltv/periscope/android/ui/chat/o$c;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method private a(Ltv/periscope/model/chat/Message;Ltv/periscope/android/ui/chat/o$a;)V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->e:Lcyw;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->t()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcyw;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    invoke-interface {p2, p1}, Ltv/periscope/android/ui/chat/o$a;->e(Ltv/periscope/model/chat/Message;)V

    .line 193
    :cond_0
    return-void
.end method

.method private a(Ltv/periscope/model/chat/Message;Ltv/periscope/android/ui/chat/o$b;)V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->h:Ltv/periscope/android/ui/chat/ae$a;

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/ae$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Ltv/periscope/android/ui/chat/o$b;->a(Ltv/periscope/model/chat/Message;Z)V

    .line 292
    :cond_0
    return-void
.end method

.method private a(Ltv/periscope/model/chat/Message;Ltv/periscope/android/ui/chat/o$c;)V
    .locals 4

    .prologue
    .line 281
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->e:Lcyw;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcyw;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    :goto_0
    return-void

    .line 285
    :cond_0
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-interface {p2, v0, v2, v3, v1}, Ltv/periscope/android/ui/chat/o$c;->a(Ljava/lang/String;JZ)V

    goto :goto_0
.end method

.method private b()Ltv/periscope/android/ui/chat/o$b;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/chat/o$b;

    .line 110
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ltv/periscope/model/chat/Message;Ltv/periscope/android/ui/chat/o$b;)V
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->e:Lcyw;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcyw;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    :goto_0
    return-void

    .line 299
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Ltv/periscope/android/ui/chat/o$b;->b(Ltv/periscope/model/chat/Message;Z)V

    goto :goto_0
.end method

.method private c()Ltv/periscope/android/ui/chat/o$c;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/chat/o$c;

    .line 118
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Ltv/periscope/android/ui/chat/o$a;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/chat/o$a;

    .line 126
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 275
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 131
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/o$a;)V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/o;->c:Ljava/lang/ref/WeakReference;

    .line 103
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/o$b;)V
    .locals 1

    .prologue
    .line 94
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/o;->a:Ljava/lang/ref/WeakReference;

    .line 95
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/o$c;)V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/o;->b:Ljava/lang/ref/WeakReference;

    .line 99
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/chatman/model/Join;)V
    .locals 11

    .prologue
    const-wide/16 v6, 0x0

    .line 214
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/o;->c()Ltv/periscope/android/ui/chat/o$c;

    move-result-object v0

    .line 215
    if-nez v0, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    invoke-virtual {p1}, Ltv/periscope/chatman/model/Join;->b()Ltv/periscope/chatman/api/Sender;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/chat/o;->e:Lcyw;

    invoke-virtual {p1}, Ltv/periscope/chatman/model/Join;->b()Ltv/periscope/chatman/api/Sender;

    move-result-object v3

    iget-object v3, v3, Ltv/periscope/chatman/api/Sender;->userId:Ljava/lang/String;

    .line 220
    invoke-virtual {p1}, Ltv/periscope/chatman/model/Join;->b()Ltv/periscope/chatman/api/Sender;

    move-result-object v4

    iget-object v4, v4, Ltv/periscope/chatman/api/Sender;->twitterId:Ljava/lang/String;

    .line 219
    invoke-interface {v2, v3, v4}, Lcyw;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/ui/chat/o$c;->a(Ltv/periscope/chatman/api/Sender;Z)V

    .line 222
    invoke-virtual {p1}, Ltv/periscope/chatman/model/Join;->b()Ltv/periscope/chatman/api/Sender;

    move-result-object v5

    .line 223
    iget-object v0, v5, Ltv/periscope/chatman/api/Sender;->participantIndex:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 226
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/o;->b()Ltv/periscope/android/ui/chat/o$b;

    move-result-object v10

    .line 227
    if-eqz v10, :cond_0

    .line 231
    iget-object v0, v5, Ltv/periscope/chatman/api/Sender;->displayName:Ljava/lang/String;

    invoke-static {v0}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v2, ""

    .line 232
    :goto_1
    iget-object v0, v5, Ltv/periscope/chatman/api/Sender;->username:Ljava/lang/String;

    iget-object v1, v5, Ltv/periscope/chatman/api/Sender;->displayName:Ljava/lang/String;

    iget-object v3, v5, Ltv/periscope/chatman/api/Sender;->userId:Ljava/lang/String;

    iget-object v4, v5, Ltv/periscope/chatman/api/Sender;->profileImageUrl:Ljava/lang/String;

    iget-object v5, v5, Ltv/periscope/chatman/api/Sender;->participantIndex:Ljava/lang/Long;

    move-wide v8, v6

    invoke-static/range {v0 .. v9}, Ltv/periscope/model/chat/Message;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;JJ)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 235
    invoke-interface {v10, v0}, Ltv/periscope/android/ui/chat/o$b;->d(Ltv/periscope/model/chat/Message;)V

    goto :goto_0

    .line 231
    :cond_2
    iget-object v0, v5, Ltv/periscope/chatman/api/Sender;->displayName:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public onEventMainThread(Ltv/periscope/chatman/model/Leave;)V
    .locals 5

    .prologue
    .line 239
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/o;->c()Ltv/periscope/android/ui/chat/o$c;

    move-result-object v0

    .line 240
    if-nez v0, :cond_0

    .line 246
    :goto_0
    return-void

    .line 244
    :cond_0
    invoke-virtual {p1}, Ltv/periscope/chatman/model/Leave;->b()Ltv/periscope/chatman/api/Sender;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/chat/o;->e:Lcyw;

    invoke-virtual {p1}, Ltv/periscope/chatman/model/Leave;->b()Ltv/periscope/chatman/api/Sender;

    move-result-object v3

    iget-object v3, v3, Ltv/periscope/chatman/api/Sender;->userId:Ljava/lang/String;

    .line 245
    invoke-virtual {p1}, Ltv/periscope/chatman/model/Leave;->b()Ltv/periscope/chatman/api/Sender;

    move-result-object v4

    iget-object v4, v4, Ltv/periscope/chatman/api/Sender;->twitterId:Ljava/lang/String;

    .line 244
    invoke-interface {v2, v3, v4}, Lcyw;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/ui/chat/o$c;->b(Ltv/periscope/chatman/api/Sender;Z)V

    goto :goto_0
.end method

.method public onEventMainThread(Ltv/periscope/chatman/model/Presence;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    const-wide/16 v4, 0x0

    .line 260
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/o;->c()Ltv/periscope/android/ui/chat/o$c;

    move-result-object v0

    .line 261
    if-nez v0, :cond_0

    .line 269
    :goto_0
    return-void

    .line 265
    :cond_0
    invoke-virtual {p1}, Ltv/periscope/chatman/model/Presence;->a()J

    move-result-wide v2

    sub-long/2addr v2, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 266
    invoke-interface {v0, v2, v3}, Ltv/periscope/android/ui/chat/o$c;->c_(J)V

    .line 267
    invoke-virtual {p1}, Ltv/periscope/chatman/model/Presence;->b()J

    move-result-wide v2

    sub-long/2addr v2, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 268
    invoke-interface {v0, v2, v3}, Ltv/periscope/android/ui/chat/o$c;->b(J)V

    goto :goto_0
.end method

.method public onEventMainThread(Ltv/periscope/chatman/model/Roster;)V
    .locals 2

    .prologue
    .line 250
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/o;->c()Ltv/periscope/android/ui/chat/o$c;

    move-result-object v0

    .line 251
    if-nez v0, :cond_0

    .line 256
    :goto_0
    return-void

    .line 254
    :cond_0
    invoke-virtual {p1}, Ltv/periscope/chatman/model/Roster;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/o$c;->b(Ljava/util/List;)V

    .line 255
    invoke-virtual {p1}, Ltv/periscope/chatman/model/Roster;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Ltv/periscope/android/ui/chat/o;->a(Ljava/util/List;Ltv/periscope/android/ui/chat/o$c;)V

    goto :goto_0
.end method

.method public onEventMainThread(Ltv/periscope/model/chat/Message;)V
    .locals 5

    .prologue
    .line 135
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/o;->b()Ltv/periscope/android/ui/chat/o$b;

    move-result-object v1

    .line 136
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/o;->c()Ltv/periscope/android/ui/chat/o$c;

    move-result-object v0

    .line 137
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/o;->d()Ltv/periscope/android/ui/chat/o$a;

    move-result-object v2

    .line 138
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    if-nez v2, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    sget-object v3, Ltv/periscope/android/ui/chat/o$1;->a:[I

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v4

    invoke-virtual {v4}, Ltv/periscope/model/chat/MessageType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 149
    :pswitch_0
    invoke-interface {v1, p1}, Ltv/periscope/android/ui/chat/o$b;->d(Ltv/periscope/model/chat/Message;)V

    goto :goto_0

    .line 153
    :pswitch_1
    invoke-direct {p0, p1, v1}, Ltv/periscope/android/ui/chat/o;->a(Ltv/periscope/model/chat/Message;Ltv/periscope/android/ui/chat/o$b;)V

    .line 154
    invoke-direct {p0, p1, v0}, Ltv/periscope/android/ui/chat/o;->a(Ltv/periscope/model/chat/Message;Ltv/periscope/android/ui/chat/o$c;)V

    goto :goto_0

    .line 158
    :pswitch_2
    invoke-direct {p0, p1, v1}, Ltv/periscope/android/ui/chat/o;->b(Ltv/periscope/model/chat/Message;Ltv/periscope/android/ui/chat/o$b;)V

    goto :goto_0

    .line 162
    :pswitch_3
    invoke-interface {v2}, Ltv/periscope/android/ui/chat/o$a;->j()V

    goto :goto_0

    .line 166
    :pswitch_4
    invoke-interface {v1, p1}, Ltv/periscope/android/ui/chat/o$b;->d(Ltv/periscope/model/chat/Message;)V

    .line 167
    invoke-direct {p0, p1, v2}, Ltv/periscope/android/ui/chat/o;->a(Ltv/periscope/model/chat/Message;Ltv/periscope/android/ui/chat/o$a;)V

    goto :goto_0

    .line 171
    :pswitch_5
    iget-boolean v0, p0, Ltv/periscope/android/ui/chat/o;->i:Z

    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v2

    .line 173
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 174
    iget-object v3, p0, Ltv/periscope/android/ui/chat/o;->g:Ljava/util/HashMap;

    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    :goto_1
    invoke-interface {v1, p1}, Ltv/periscope/android/ui/chat/o$b;->c(Ltv/periscope/model/chat/Message;)V

    goto :goto_0

    .line 176
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/chat/o;->g:Ljava/util/HashMap;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
