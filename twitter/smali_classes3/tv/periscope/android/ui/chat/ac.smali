.class public Ltv/periscope/android/ui/chat/ac;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcyq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcyq",
        "<",
        "Ldce;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldce;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/ac;->a:Ljava/util/List;

    .line 18
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/ac;->b:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ltv/periscope/android/ui/chat/ac;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Ltv/periscope/android/ui/chat/ac;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 77
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 78
    iget-object v0, p0, Ltv/periscope/android/ui/chat/ac;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldce;

    .line 79
    invoke-virtual {v0}, Ldce;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 84
    :goto_1
    return v0

    .line 77
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/chat/ac;->b(I)Ldce;

    move-result-object v0

    return-object v0
.end method

.method public a(Ldce;)V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Ltv/periscope/android/ui/chat/ac;->b:Ljava/util/Set;

    invoke-virtual {p1}, Ldce;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    iget-object v0, p0, Ltv/periscope/android/ui/chat/ac;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    iget-object v0, p0, Ltv/periscope/android/ui/chat/ac;->b:Ljava/util/Set;

    invoke-virtual {p1}, Ldce;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_0
    return-void
.end method

.method public b(I)Ldce;
    .locals 1

    .prologue
    .line 23
    if-ltz p1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/ac;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 27
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/ac;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldce;

    goto :goto_0
.end method

.method public b(Ldce;)V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Ltv/periscope/android/ui/chat/ac;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 72
    iget-object v0, p0, Ltv/periscope/android/ui/chat/ac;->b:Ljava/util/Set;

    invoke-virtual {p1}, Ldce;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 73
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/ac;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/chat/ac;->b(I)Ldce;

    move-result-object v0

    .line 63
    if-nez v0, :cond_0

    .line 68
    :goto_0
    return-void

    .line 67
    :cond_0
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/chat/ac;->b(Ldce;)V

    goto :goto_0
.end method
