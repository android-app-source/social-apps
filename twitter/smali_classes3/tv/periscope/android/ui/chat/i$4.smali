.class Ltv/periscope/android/ui/chat/i$4;
.super Ltv/periscope/android/view/v;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/chat/i;->a(Ltv/periscope/android/ui/chat/i$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/chat/i$a;

.field final synthetic b:Landroid/view/ViewPropertyAnimator;

.field final synthetic c:Landroid/view/View;

.field final synthetic d:Ltv/periscope/android/ui/chat/i;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/chat/i;Ltv/periscope/android/ui/chat/i$a;Landroid/view/ViewPropertyAnimator;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Ltv/periscope/android/ui/chat/i$4;->d:Ltv/periscope/android/ui/chat/i;

    iput-object p2, p0, Ltv/periscope/android/ui/chat/i$4;->a:Ltv/periscope/android/ui/chat/i$a;

    iput-object p3, p0, Ltv/periscope/android/ui/chat/i$4;->b:Landroid/view/ViewPropertyAnimator;

    iput-object p4, p0, Ltv/periscope/android/ui/chat/i$4;->c:Landroid/view/View;

    invoke-direct {p0}, Ltv/periscope/android/view/v;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 326
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$4;->b:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 327
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$4;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 328
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$4;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 329
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$4;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 330
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$4;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setScaleX(F)V

    .line 331
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$4;->d:Ltv/periscope/android/ui/chat/i;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/i$4;->a:Ltv/periscope/android/ui/chat/i$a;

    iget-object v1, v1, Ltv/periscope/android/ui/chat/i$a;->a:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/chat/i;->dispatchChangeFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    .line 332
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$4;->d:Ltv/periscope/android/ui/chat/i;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/i;->g(Ltv/periscope/android/ui/chat/i;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/chat/i$4;->a:Ltv/periscope/android/ui/chat/i$a;

    iget-object v1, v1, Ltv/periscope/android/ui/chat/i$a;->a:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 333
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$4;->d:Ltv/periscope/android/ui/chat/i;

    invoke-static {v0}, Ltv/periscope/android/ui/chat/i;->f(Ltv/periscope/android/ui/chat/i;)V

    .line 334
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 321
    iget-object v0, p0, Ltv/periscope/android/ui/chat/i$4;->d:Ltv/periscope/android/ui/chat/i;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/i$4;->a:Ltv/periscope/android/ui/chat/i$a;

    iget-object v1, v1, Ltv/periscope/android/ui/chat/i$a;->a:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/chat/i;->dispatchChangeStarting(Landroid/support/v7/widget/RecyclerView$ViewHolder;Z)V

    .line 322
    return-void
.end method
