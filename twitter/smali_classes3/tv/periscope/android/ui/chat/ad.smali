.class public Ltv/periscope/android/ui/chat/ad;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "Twttr"


# instance fields
.field public final a:Landroid/view/ViewGroup;

.field public final b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

.field public final c:Ltv/periscope/android/ui/love/SmallHeartView;

.field public d:Landroid/animation/Animator;

.field public e:Landroid/animation/Animator;

.field public f:Landroid/animation/Animator;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 27
    sget v0, Ltv/periscope/android/library/f$g;->avatar_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/ad;->a:Landroid/view/ViewGroup;

    .line 28
    sget v0, Ltv/periscope/android/library/f$g;->avatar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/ad;->b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

    .line 29
    sget v0, Ltv/periscope/android/library/f$g;->heart:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/love/SmallHeartView;

    iput-object v0, p0, Ltv/periscope/android/ui/chat/ad;->c:Ltv/periscope/android/ui/love/SmallHeartView;

    .line 30
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/chat/ad;->c:Ltv/periscope/android/ui/love/SmallHeartView;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/love/SmallHeartView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 34
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Ltv/periscope/android/ui/chat/ad;->c:Ltv/periscope/android/ui/love/SmallHeartView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/love/SmallHeartView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 38
    return-void
.end method
