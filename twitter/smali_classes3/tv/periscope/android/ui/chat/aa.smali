.class public Ltv/periscope/android/ui/chat/aa;
.super Landroid/support/v7/widget/SimpleItemAnimator;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/animation/OvershootInterpolator;

.field private final b:Landroid/view/animation/AccelerateInterpolator;

.field private final c:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v7/widget/SimpleItemAnimator;-><init>()V

    .line 20
    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v0}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/aa;->a:Landroid/view/animation/OvershootInterpolator;

    .line 21
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/aa;->b:Landroid/view/animation/AccelerateInterpolator;

    .line 26
    iput p1, p0, Ltv/periscope/android/ui/chat/aa;->c:I

    .line 27
    return-void
.end method


# virtual methods
.method public animateAdd(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 50
    check-cast p1, Ltv/periscope/android/ui/chat/ad;

    .line 51
    iget-object v0, p1, Ltv/periscope/android/ui/chat/ad;->a:Landroid/view/ViewGroup;

    .line 52
    iget-object v1, p1, Ltv/periscope/android/ui/chat/ad;->b:Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;

    .line 53
    iget v2, p0, Ltv/periscope/android/ui/chat/aa;->c:I

    invoke-virtual {v1}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->getHeight()I

    move-result v1

    sub-int v1, v2, v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 55
    sget-object v2, Landroid/view/View;->Y:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    iget v4, p0, Ltv/periscope/android/ui/chat/aa;->c:I

    int-to-float v4, v4

    aput v4, v3, v5

    const/4 v4, 0x1

    aput v1, v3, v4

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 56
    new-instance v1, Ltv/periscope/android/ui/chat/aa$2;

    invoke-direct {v1, p0, p1}, Ltv/periscope/android/ui/chat/aa$2;-><init>(Ltv/periscope/android/ui/chat/aa;Ltv/periscope/android/ui/chat/ad;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 62
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/aa;->getAddDuration()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 63
    iget-object v1, p0, Ltv/periscope/android/ui/chat/aa;->a:Landroid/view/animation/OvershootInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 64
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 66
    return v5
.end method

.method public animateChange(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)Z
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method public animateMove(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 76
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 77
    int-to-float v1, p2

    iget-object v2, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationX()F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 78
    sub-int v2, p4, v1

    .line 79
    if-nez v2, :cond_0

    .line 80
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/chat/aa;->dispatchMoveFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 93
    :goto_0
    return v5

    .line 84
    :cond_0
    sget-object v2, Landroid/view/View;->X:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    int-to-float v1, v1

    aput v1, v3, v5

    const/4 v1, 0x1

    int-to-float v4, p4

    aput v4, v3, v1

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 85
    new-instance v1, Ltv/periscope/android/ui/chat/aa$3;

    invoke-direct {v1, p0, p1}, Ltv/periscope/android/ui/chat/aa$3;-><init>(Ltv/periscope/android/ui/chat/aa;Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 91
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/aa;->getMoveDuration()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 92
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method public animateRemove(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 31
    check-cast p1, Ltv/periscope/android/ui/chat/ad;

    .line 32
    iget-object v0, p1, Ltv/periscope/android/ui/chat/ad;->a:Landroid/view/ViewGroup;

    .line 34
    sget-object v1, Landroid/view/View;->Y:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getY()F

    move-result v3

    aput v3, v2, v5

    const/4 v3, 0x1

    iget v4, p0, Ltv/periscope/android/ui/chat/aa;->c:I

    int-to-float v4, v4

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 35
    new-instance v1, Ltv/periscope/android/ui/chat/aa$1;

    invoke-direct {v1, p0, p1}, Ltv/periscope/android/ui/chat/aa$1;-><init>(Ltv/periscope/android/ui/chat/aa;Ltv/periscope/android/ui/chat/ad;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 42
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/aa;->getRemoveDuration()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 43
    iget-object v1, p0, Ltv/periscope/android/ui/chat/aa;->b:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 44
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 45
    return v5
.end method

.method public canReuseUpdatedViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;Ljava/util/List;)Z
    .locals 1
    .param p1    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 71
    const/4 v0, 0x1

    return v0
.end method

.method public endAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public endAnimations()V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public runPendingAnimations()V
    .locals 0

    .prologue
    .line 104
    return-void
.end method
