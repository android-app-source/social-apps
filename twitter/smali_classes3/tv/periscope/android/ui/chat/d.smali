.class public Ltv/periscope/android/ui/chat/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/ak;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltv/periscope/android/view/ak",
        "<",
        "Ltv/periscope/android/ui/chat/e;",
        "Ltv/periscope/model/chat/Message;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Ljava/lang/String;

.field private final e:Ltv/periscope/android/ui/chat/a;

.field private final f:Ldae;

.field private final g:Lcyw;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;ZZLtv/periscope/android/ui/chat/a;Ldae;Lcyw;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean p4, p0, Ltv/periscope/android/ui/chat/d;->a:Z

    .line 39
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    invoke-static {p1, p2}, Ltv/periscope/android/util/ab;->a(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/chat/d;->b:Ljava/lang/String;

    .line 44
    :goto_0
    iput-boolean p5, p0, Ltv/periscope/android/ui/chat/d;->c:Z

    .line 45
    iput-object p3, p0, Ltv/periscope/android/ui/chat/d;->d:Ljava/lang/String;

    .line 46
    iput-object p6, p0, Ltv/periscope/android/ui/chat/d;->e:Ltv/periscope/android/ui/chat/a;

    .line 47
    iput-object p7, p0, Ltv/periscope/android/ui/chat/d;->f:Ldae;

    .line 48
    iput-object p8, p0, Ltv/periscope/android/ui/chat/d;->g:Lcyw;

    .line 49
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/chat/d;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;ZZLtv/periscope/android/ui/chat/a;Ldae;Lcyw;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct/range {p0 .. p8}, Ltv/periscope/android/ui/chat/d;-><init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;ZZLtv/periscope/android/ui/chat/a;Ldae;Lcyw;)V

    .line 55
    iput-object p9, p0, Ltv/periscope/android/ui/chat/d;->h:Ljava/lang/String;

    .line 56
    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Ltv/periscope/android/ui/chat/d;->e:Ltv/periscope/android/ui/chat/a;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Ltv/periscope/android/ui/chat/d;->e:Ltv/periscope/android/ui/chat/a;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/chat/a;->b(Ljava/lang/String;)I

    move-result v0

    .line 159
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ltv/periscope/model/chat/Message;Ltv/periscope/android/ui/chat/e;I)V
    .locals 3
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 137
    iget-object v0, p0, Ltv/periscope/android/ui/chat/d;->g:Lcyw;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcyw;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Ltv/periscope/android/ui/chat/d;->g:Lcyw;

    iget-object v1, p0, Ltv/periscope/android/ui/chat/d;->g:Lcyw;

    invoke-interface {v1}, Lcyw;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/chat/d;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcyw;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 142
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p2, Ltv/periscope/android/ui/chat/e;->k:Ltv/periscope/android/view/PsImageView;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p3, v1}, Ltv/periscope/android/view/PsImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 144
    iget-object v0, p2, Ltv/periscope/android/ui/chat/e;->k:Ltv/periscope/android/view/PsImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsImageView;->setVisibility(I)V

    .line 148
    :goto_1
    return-void

    .line 140
    :cond_0
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->L()Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 146
    :cond_1
    iget-object v0, p2, Ltv/periscope/android/ui/chat/e;->k:Ltv/periscope/android/view/PsImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Ltv/periscope/android/ui/chat/d;->e:Ltv/periscope/android/ui/chat/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/chat/d;->e:Ltv/periscope/android/ui/chat/a;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/chat/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/d;->e:Ltv/periscope/android/ui/chat/a;

    .line 152
    invoke-interface {v0, p2, p3}, Ltv/periscope/android/ui/chat/a;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    .line 152
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Landroid/support/v7/widget/RecyclerView$ViewHolder;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Ltv/periscope/android/ui/chat/e;

    check-cast p2, Ltv/periscope/model/chat/Message;

    invoke-virtual {p0, p1, p2, p3}, Ltv/periscope/android/ui/chat/d;->a(Ltv/periscope/android/ui/chat/e;Ltv/periscope/model/chat/Message;I)V

    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/e;Ltv/periscope/model/chat/Message;I)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 61
    iget-object v0, p1, Ltv/periscope/android/ui/chat/e;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 66
    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/chat/d;->a(Ljava/lang/String;)I

    move-result v0

    .line 67
    iget-boolean v2, p0, Ltv/periscope/android/ui/chat/d;->c:Z

    if-eqz v2, :cond_0

    if-lez v0, :cond_0

    .line 68
    iget-object v2, p1, Ltv/periscope/android/ui/chat/e;->h:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 69
    iget-object v2, p1, Ltv/periscope/android/ui/chat/e;->i:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 70
    iget-object v2, p1, Ltv/periscope/android/ui/chat/e;->i:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    :goto_0
    iget-object v0, p1, Ltv/periscope/android/ui/chat/e;->b:Ltv/periscope/android/view/UsernameBadgeView;

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Ltv/periscope/android/util/ab;->a(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ltv/periscope/android/view/UsernameBadgeView;->setText(Ljava/lang/String;)V

    .line 77
    iget-object v0, p1, Ltv/periscope/android/ui/chat/e;->b:Ltv/periscope/android/view/UsernameBadgeView;

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->K()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ltv/periscope/android/api/PsUser$VipBadge;->fromString(Ljava/lang/String;)Ltv/periscope/android/api/PsUser$VipBadge;

    move-result-object v2

    invoke-virtual {v0, v2}, Ltv/periscope/android/view/UsernameBadgeView;->setVipStatus(Ltv/periscope/android/api/PsUser$VipBadge;)V

    .line 79
    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    sget-object v2, Ltv/periscope/model/chat/MessageType;->m:Ltv/periscope/model/chat/MessageType;

    if-ne v0, v2, :cond_1

    .line 80
    iget-object v0, p1, Ltv/periscope/android/ui/chat/e;->c:Landroid/widget/TextView;

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Ltv/periscope/android/ui/chat/d;->d:Ljava/lang/String;

    .line 86
    :goto_1
    iget-object v2, p0, Ltv/periscope/android/ui/chat/d;->b:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/ui/chat/d;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 87
    iget-object v2, p1, Ltv/periscope/android/ui/chat/e;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 92
    :goto_2
    sget v2, Ltv/periscope/android/library/f$d;->ps__light_grey:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 93
    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->d()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v3, v5, v6}, Ltv/periscope/android/ui/chat/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 94
    iget-object v2, p1, Ltv/periscope/android/ui/chat/e;->f:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 95
    iget-object v2, p1, Ltv/periscope/android/ui/chat/e;->g:Landroid/view/View;

    sget v3, Ltv/periscope/android/library/f$f;->ps_bg_chat_blocked:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 96
    iget-object v2, p1, Ltv/periscope/android/ui/chat/e;->b:Ltv/periscope/android/view/UsernameBadgeView;

    sget v3, Ltv/periscope/android/library/f$d;->ps__white:I

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ltv/periscope/android/view/UsernameBadgeView;->setTextColor(I)V

    .line 97
    iget-object v2, p1, Ltv/periscope/android/ui/chat/e;->c:Landroid/widget/TextView;

    sget v3, Ltv/periscope/android/library/f$d;->ps__white_30:I

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 106
    :goto_3
    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_4

    const-wide/16 v2, 0x0

    .line 107
    :goto_4
    iget-object v5, p1, Ltv/periscope/android/ui/chat/e;->d:Ltv/periscope/android/view/MaskImageView;

    invoke-virtual {v5, v11}, Ltv/periscope/android/view/MaskImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 108
    iget-object v5, p1, Ltv/periscope/android/ui/chat/e;->d:Ltv/periscope/android/view/MaskImageView;

    invoke-virtual {v5}, Ltv/periscope/android/view/MaskImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-static {v4, v2, v3}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v6

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v5, v6, v7}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 112
    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->d()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v5, v6, v7}, Ltv/periscope/android/ui/chat/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 113
    iget-object v5, p1, Ltv/periscope/android/ui/chat/e;->d:Ltv/periscope/android/view/MaskImageView;

    sget v6, Ltv/periscope/android/library/f$d;->ps__light_grey_90:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ltv/periscope/android/view/MaskImageView;->setColorFilter(I)V

    .line 121
    :goto_5
    invoke-static {v4, v2, v3}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v2

    .line 122
    iget-object v3, p0, Ltv/periscope/android/ui/chat/d;->g:Lcyw;

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcyw;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 123
    new-instance v3, Landroid/content/res/ColorStateList;

    new-array v4, v10, [[I

    new-array v5, v8, [I

    aput-object v5, v4, v8

    new-array v5, v10, [I

    aput v2, v5, v8

    invoke-direct {v3, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 124
    iget-object v4, p1, Ltv/periscope/android/ui/chat/e;->j:Landroid/support/v7/widget/AppCompatImageView;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/AppCompatImageView;->setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 125
    iget-object v3, p1, Ltv/periscope/android/ui/chat/e;->j:Landroid/support/v7/widget/AppCompatImageView;

    invoke-virtual {v3, v8}, Landroid/support/v7/widget/AppCompatImageView;->setVisibility(I)V

    .line 130
    :goto_6
    invoke-direct {p0, p2, p1, v2}, Ltv/periscope/android/ui/chat/d;->a(Ltv/periscope/model/chat/Message;Ltv/periscope/android/ui/chat/e;I)V

    .line 132
    iget-object v2, p0, Ltv/periscope/android/ui/chat/d;->f:Ldae;

    iget-object v3, p1, Ltv/periscope/android/ui/chat/e;->d:Ltv/periscope/android/view/MaskImageView;

    invoke-interface {v2, v1, v0, v3}, Ldae;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 133
    return-void

    .line 72
    :cond_0
    iget-object v0, p1, Ltv/periscope/android/ui/chat/e;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 73
    iget-object v0, p1, Ltv/periscope/android/ui/chat/e;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 83
    :cond_1
    iget-object v0, p1, Ltv/periscope/android/ui/chat/e;->c:Landroid/widget/TextView;

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->m()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 89
    :cond_2
    iget-object v2, p1, Ltv/periscope/android/ui/chat/e;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 99
    :cond_3
    iget-object v3, p1, Ltv/periscope/android/ui/chat/e;->f:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    .line 100
    iget-object v3, p1, Ltv/periscope/android/ui/chat/e;->g:Landroid/view/View;

    sget v5, Ltv/periscope/android/library/f$f;->ps__bg_chat:I

    invoke-virtual {v3, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 101
    iget-object v3, p1, Ltv/periscope/android/ui/chat/e;->b:Ltv/periscope/android/view/UsernameBadgeView;

    invoke-virtual {v3, v2}, Ltv/periscope/android/view/UsernameBadgeView;->setTextColor(I)V

    .line 102
    iget-object v2, p1, Ltv/periscope/android/ui/chat/e;->c:Landroid/widget/TextView;

    sget v3, Ltv/periscope/android/library/f$d;->ps__dark_grey:I

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    .line 106
    :cond_4
    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->e()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto/16 :goto_4

    .line 114
    :cond_5
    iget-boolean v5, p0, Ltv/periscope/android/ui/chat/d;->a:Z

    if-eqz v5, :cond_6

    .line 115
    iget-object v5, p1, Ltv/periscope/android/ui/chat/e;->d:Ltv/periscope/android/view/MaskImageView;

    .line 116
    invoke-static {v4, v2, v3}, Ltv/periscope/android/util/aa;->b(Landroid/content/res/Resources;J)I

    move-result v6

    .line 115
    invoke-virtual {v5, v6}, Ltv/periscope/android/view/MaskImageView;->setColorFilter(I)V

    goto/16 :goto_5

    .line 118
    :cond_6
    iget-object v5, p1, Ltv/periscope/android/ui/chat/e;->d:Ltv/periscope/android/view/MaskImageView;

    invoke-virtual {v5, v11}, Ltv/periscope/android/view/MaskImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto/16 :goto_5

    .line 127
    :cond_7
    iget-object v3, p1, Ltv/periscope/android/ui/chat/e;->j:Landroid/support/v7/widget/AppCompatImageView;

    invoke-virtual {v3, v9}, Landroid/support/v7/widget/AppCompatImageView;->setVisibility(I)V

    goto :goto_6
.end method
