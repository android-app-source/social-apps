.class public Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;
.super Landroid/widget/ImageView;
.source "Twttr"


# static fields
.field private static final a:Landroid/view/animation/DecelerateInterpolator;


# instance fields
.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private d:Landroid/animation/Animator;

.field private e:Ldae;

.field private f:Landroid/graphics/Bitmap;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:F

.field private j:F

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->a:Landroid/view/animation/DecelerateInterpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->b:Landroid/graphics/Paint;

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->c:Landroid/graphics/Paint;

    .line 52
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->b()V

    .line 53
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;F)F
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->j:F

    return p1
.end method

.method static synthetic a(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->f:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->k:Z

    return p1
.end method

.method static synthetic b(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->f:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 56
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 57
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 58
    return-void
.end method

.method static synthetic c(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->c:Landroid/graphics/Paint;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->d:Landroid/animation/Animator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->d:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 104
    :goto_0
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->e()V

    .line 105
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->getUnveilAnimator()Landroid/animation/Animator;

    move-result-object v1

    iput-object v1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->d:Landroid/animation/Animator;

    .line 106
    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->d:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 109
    :cond_0
    return-void

    .line 103
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 156
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->e()V

    .line 157
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->d:Landroid/animation/Animator;

    if-nez v0, :cond_0

    .line 158
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->getUnveilAnimator()Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->d:Landroid/animation/Animator;

    .line 160
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->k:Z

    .line 161
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->d:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 162
    return-void
.end method

.method static synthetic d(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->d()V

    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->d:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->d:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->d:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 168
    :cond_0
    return-void
.end method

.method private getUnveilAnimator()Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 62
    iget v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->i:F

    .line 63
    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    const/4 v2, 0x1

    aput v0, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 65
    new-instance v2, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$1;

    invoke-direct {v2, p0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$1;-><init>(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 74
    sget-object v2, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->a:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 75
    const-wide/16 v2, 0x44c

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 77
    new-instance v2, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$2;

    invoke-direct {v2, p0, v0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$2;-><init>(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;F)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 91
    return-object v1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 171
    const/4 v0, 0x0

    iput v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->j:F

    .line 172
    const/4 v0, 0x0

    iput v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->h:I

    .line 173
    iput-object v1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->f:Landroid/graphics/Bitmap;

    .line 174
    iput-object v1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->g:Ljava/lang/String;

    .line 175
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->k:Z

    .line 176
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->e()V

    .line 177
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 121
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->e:Ldae;

    if-nez v0, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    iput-object p1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->g:Ljava/lang/String;

    .line 131
    new-instance v5, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$3;

    invoke-direct {v5, p0, p1}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView$3;-><init>(Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;Ljava/lang/String;)V

    .line 151
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 152
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->e:Ldae;

    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Ldae;->a(Landroid/content/Context;Ljava/lang/String;IILdae$a;)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 181
    iget-boolean v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->k:Z

    if-eqz v0, :cond_0

    .line 182
    iget v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->i:F

    iget v1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->i:F

    iget v2, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->i:F

    iget-object v3, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 184
    :cond_0
    iget v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->i:F

    iget v1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->i:F

    iget v2, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->j:F

    iget-object v3, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 185
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 96
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 98
    int-to-float v0, p1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->i:F

    .line 99
    invoke-direct {p0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->c()V

    .line 100
    return-void
.end method

.method public setAvatarColor(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 116
    invoke-virtual {p0}, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->h:I

    .line 117
    iget-object v0, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->b:Landroid/graphics/Paint;

    iget v1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->h:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 118
    return-void
.end method

.method public setImageUrlLoader(Ldae;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Ltv/periscope/android/ui/chat/FriendsWatchingAvatarView;->e:Ldae;

    .line 113
    return-void
.end method
