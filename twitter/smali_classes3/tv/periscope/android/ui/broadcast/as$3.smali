.class Ltv/periscope/android/ui/broadcast/as$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/util/n$a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/as;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/as;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/as;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/as$3;->a:Ltv/periscope/android/ui/broadcast/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 72
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as$3;->a:Ltv/periscope/android/ui/broadcast/as;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/as;->d(Ltv/periscope/android/ui/broadcast/as;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 73
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as$3;->a:Ltv/periscope/android/ui/broadcast/as;

    invoke-static {v0, v6}, Ltv/periscope/android/ui/broadcast/as;->b(Ltv/periscope/android/ui/broadcast/as;Z)Z

    .line 74
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as$3;->a:Ltv/periscope/android/ui/broadcast/as;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/as;->e(Ltv/periscope/android/ui/broadcast/as;)F

    move-result v0

    int-to-float v1, p1

    sub-float/2addr v0, v1

    .line 76
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/as$3;->a:Ltv/periscope/android/ui/broadcast/as;

    invoke-static {v1}, Ltv/periscope/android/ui/broadcast/as;->f(Ltv/periscope/android/ui/broadcast/as;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    .line 77
    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr v0, v2

    neg-float v0, v0

    .line 80
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/as$3;->a:Ltv/periscope/android/ui/broadcast/as;

    invoke-static {v2}, Ltv/periscope/android/ui/broadcast/as;->f(Ltv/periscope/android/ui/broadcast/as;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v0

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 81
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as$3;->a:Ltv/periscope/android/ui/broadcast/as;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/as;->f(Ltv/periscope/android/ui/broadcast/as;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 85
    :cond_0
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/as$3;->a:Ltv/periscope/android/ui/broadcast/as;

    invoke-static {v2}, Ltv/periscope/android/ui/broadcast/as;->f(Ltv/periscope/android/ui/broadcast/as;)Landroid/view/View;

    move-result-object v2

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v1, v4, v5

    aput v0, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 87
    new-instance v2, Ltv/periscope/android/ui/broadcast/as$3$1;

    invoke-direct {v2, p0, v0}, Ltv/periscope/android/ui/broadcast/as$3$1;-><init>(Ltv/periscope/android/ui/broadcast/as$3;F)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 93
    const-wide/16 v2, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 94
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 96
    :cond_1
    return-void
.end method

.method public b(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 100
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as$3;->a:Ltv/periscope/android/ui/broadcast/as;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/as;->d(Ltv/periscope/android/ui/broadcast/as;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as$3;->a:Ltv/periscope/android/ui/broadcast/as;

    invoke-static {v0, v4}, Ltv/periscope/android/ui/broadcast/as;->b(Ltv/periscope/android/ui/broadcast/as;Z)Z

    .line 102
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as$3;->a:Ltv/periscope/android/ui/broadcast/as;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/as;->f(Ltv/periscope/android/ui/broadcast/as;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    .line 103
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/as$3;->a:Ltv/periscope/android/ui/broadcast/as;

    invoke-static {v1}, Ltv/periscope/android/ui/broadcast/as;->f(Ltv/periscope/android/ui/broadcast/as;)Landroid/view/View;

    move-result-object v1

    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    aput v0, v3, v4

    const/4 v0, 0x1

    const/4 v4, 0x0

    aput v4, v3, v0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 105
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 106
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 108
    :cond_0
    return-void
.end method
