.class public Ltv/periscope/android/ui/broadcast/g;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "Twttr"

# interfaces
.implements Lcyq$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Ltv/periscope/android/ui/broadcast/i;",
        ">;",
        "Lcyq$a;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ltv/periscope/android/ui/broadcast/h;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/ui/broadcast/i$h;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcyw;

.field private final e:Ldae;

.field private f:Ltv/periscope/android/ui/broadcast/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ltv/periscope/android/ui/broadcast/j;Ltv/periscope/android/ui/broadcast/h;Lcyw;Ldae;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 31
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/g;->f:Ltv/periscope/android/ui/broadcast/j;

    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->f:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0, p0}, Ltv/periscope/android/ui/broadcast/j;->a(Lcyq$a;)V

    .line 34
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    .line 35
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/g;->d:Lcyw;

    .line 36
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/g;->e:Ldae;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->c:Ljava/util/List;

    .line 38
    return-void
.end method


# virtual methods
.method public a(I)Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->f:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/j;->b(I)Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;I)Ltv/periscope/android/ui/broadcast/i;
    .locals 4

    .prologue
    .line 102
    invoke-static {}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;->values()[Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;

    move-result-object v0

    aget-object v0, v0, p2

    .line 105
    sget-object v1, Ltv/periscope/android/ui/broadcast/g$1;->a:[I

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 155
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Unsupported view type"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/g;->d:Lcyw;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/g;->e:Ldae;

    invoke-static {v0, p1, v1, v2, v3}, Ltv/periscope/android/ui/broadcast/i$b;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;Lcyw;Ldae;)Ltv/periscope/android/ui/broadcast/i$b;

    move-result-object v0

    .line 152
    :goto_0
    return-object v0

    .line 110
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/i$d;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$d;

    move-result-object v0

    goto :goto_0

    .line 113
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/i$h;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$h;

    move-result-object v0

    .line 115
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 119
    :pswitch_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/i$a;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$a;

    move-result-object v0

    goto :goto_0

    .line 122
    :pswitch_4
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/i$k;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$k;

    move-result-object v0

    goto :goto_0

    .line 125
    :pswitch_5
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/i$e;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$e;

    move-result-object v0

    goto :goto_0

    .line 128
    :pswitch_6
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/g;->e:Ldae;

    invoke-static {v0, p1, v1, v2}, Ltv/periscope/android/ui/broadcast/i$m;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;Ldae;)Ltv/periscope/android/ui/broadcast/i$m;

    move-result-object v0

    goto :goto_0

    .line 131
    :pswitch_7
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/i$i;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$i;

    move-result-object v0

    goto :goto_0

    .line 134
    :pswitch_8
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/i$f;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$f;

    move-result-object v0

    goto :goto_0

    .line 137
    :pswitch_9
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/i$j;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$j;

    move-result-object v0

    goto :goto_0

    .line 140
    :pswitch_a
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/i$c;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$c;

    move-result-object v0

    goto :goto_0

    .line 143
    :pswitch_b
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/i$g;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$g;

    move-result-object v0

    goto :goto_0

    .line 146
    :pswitch_c
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/i$l;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$l;

    move-result-object v0

    goto :goto_0

    .line 149
    :pswitch_d
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/t;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/t;

    move-result-object v0

    goto :goto_0

    .line 152
    :pswitch_e
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->a:Landroid/content/Context;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/g;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/ae;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/ae;

    move-result-object v0

    goto/16 :goto_0

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public a()V
    .locals 0

    .prologue
    .line 184
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/g;->notifyDataSetChanged()V

    .line 185
    return-void
.end method

.method a(Ltv/periscope/android/player/e;Ljava/util/TimeZone;J)V
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/i$h;

    .line 52
    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/i$h;->c:Ltv/periscope/android/ui/view/LocalTimeView;

    invoke-virtual {v2, p1, p2, p3, p4}, Ltv/periscope/android/ui/view/LocalTimeView;->a(Ltv/periscope/android/player/e;Ljava/util/TimeZone;J)V

    .line 53
    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/i$h;->c:Ltv/periscope/android/ui/view/LocalTimeView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/view/LocalTimeView;->setVisibility(I)V

    goto :goto_0

    .line 55
    :cond_0
    return-void
.end method

.method public a(Ltv/periscope/android/ui/broadcast/i;I)V
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0, p2}, Ltv/periscope/android/ui/broadcast/g;->a(I)Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;

    move-result-object v0

    invoke-virtual {p1, v0}, Ltv/periscope/android/ui/broadcast/i;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V

    .line 163
    return-void
.end method

.method public a(Ltv/periscope/android/ui/broadcast/j;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->f:Ltv/periscope/android/ui/broadcast/j;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->f:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0, p0}, Ltv/periscope/android/ui/broadcast/j;->b(Lcyq$a;)V

    .line 46
    :cond_0
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/g;->f:Ltv/periscope/android/ui/broadcast/j;

    .line 47
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->f:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0, p0}, Ltv/periscope/android/ui/broadcast/j;->a(Lcyq$a;)V

    .line 48
    return-void
.end method

.method b()V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/i$h;

    .line 59
    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/i$h;->c:Ltv/periscope/android/ui/view/LocalTimeView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/view/LocalTimeView;->setVisibility(I)V

    goto :goto_0

    .line 61
    :cond_0
    return-void
.end method

.method c()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/i$h;

    .line 65
    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/i$h;->c:Ltv/periscope/android/ui/view/LocalTimeView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/view/LocalTimeView;->a()V

    goto :goto_0

    .line 67
    :cond_0
    return-void
.end method

.method d()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/i$h;

    .line 71
    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/i$h;->c:Ltv/periscope/android/ui/view/LocalTimeView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/view/LocalTimeView;->b()V

    goto :goto_0

    .line 73
    :cond_0
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/i$h;

    .line 77
    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/i$h;->b:Ltv/periscope/android/view/q;

    invoke-virtual {v0}, Ltv/periscope/android/view/q;->b()V

    goto :goto_0

    .line 79
    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/i$h;

    .line 83
    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/i$h;->b:Ltv/periscope/android/view/q;

    invoke-virtual {v0}, Ltv/periscope/android/view/q;->a()V

    goto :goto_0

    .line 85
    :cond_0
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/i$h;

    .line 89
    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/i$h;->b:Ltv/periscope/android/view/q;

    invoke-virtual {v0}, Ltv/periscope/android/view/q;->c()V

    goto :goto_0

    .line 91
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 92
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->f:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->a()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/g;->a(I)Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;

    move-result-object v0

    .line 179
    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;->a()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;->ordinal()I

    move-result v0

    return v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/i$h;

    .line 96
    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/i$h;->b:Ltv/periscope/android/view/q;

    invoke-virtual {v0}, Ltv/periscope/android/view/q;->d()V

    goto :goto_0

    .line 98
    :cond_0
    return-void
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Ltv/periscope/android/ui/broadcast/i;

    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/g;->a(Ltv/periscope/android/ui/broadcast/i;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/g;->a(Landroid/view/ViewGroup;I)Ltv/periscope/android/ui/broadcast/i;

    move-result-object v0

    return-object v0
.end method
