.class public Ltv/periscope/android/ui/broadcast/an;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ltv/periscope/android/ui/broadcast/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/an$a;
    }
.end annotation


# instance fields
.field private final a:Ltv/periscope/android/ui/broadcast/an$a;

.field private b:Landroid/widget/ImageView;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Ltv/periscope/android/ui/broadcast/an$a;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/an;->d:Z

    .line 23
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/an;->a:Ltv/periscope/android/ui/broadcast/an$a;

    .line 24
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 41
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/an;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 44
    :cond_0
    return-void
.end method

.method public a(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 28
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    .line 29
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_as_retweet:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 30
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__bg_bottom_tray_item_background:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 31
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/an;->c:Z

    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/an;->c()V

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/an;->d()V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 51
    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/an;->c:Z

    .line 67
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$d;->ps__retweet_green:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 70
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/an;->c:Z

    .line 75
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 78
    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/an;->d:Z

    .line 89
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/an;->b()V

    .line 90
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/an;->c:Z

    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/an;->d()V

    .line 57
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->a:Ltv/periscope/android/ui/broadcast/an$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/an$a;->e()V

    .line 62
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/an;->c()V

    .line 60
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/an;->a:Ltv/periscope/android/ui/broadcast/an$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/an$a;->d()V

    goto :goto_0
.end method
