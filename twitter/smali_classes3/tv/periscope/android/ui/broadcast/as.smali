.class Ltv/periscope/android/ui/broadcast/as;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/ar;
.implements Ltv/periscope/android/view/TweetSheet$a;


# instance fields
.field private final a:Ldae;

.field private final b:Landroid/view/ViewGroup;

.field private c:Ltv/periscope/android/view/TweetSheet;

.field private d:Landroid/view/View;

.field private final e:Ltv/periscope/android/view/ai;

.field private final f:Ljava/lang/String;

.field private final g:F

.field private final h:Ltv/periscope/android/util/n$a;

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private final l:Landroid/animation/Animator$AnimatorListener;

.field private final m:Landroid/animation/Animator$AnimatorListener;

.field private final n:Ltv/periscope/android/util/n$a$a;


# direct methods
.method constructor <init>(Ltv/periscope/android/util/n$a;Ltv/periscope/android/view/ai;Ldae;Landroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ltv/periscope/android/ui/broadcast/as$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/as$1;-><init>(Ltv/periscope/android/ui/broadcast/as;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->l:Landroid/animation/Animator$AnimatorListener;

    .line 51
    new-instance v0, Ltv/periscope/android/ui/broadcast/as$2;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/as$2;-><init>(Ltv/periscope/android/ui/broadcast/as;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->m:Landroid/animation/Animator$AnimatorListener;

    .line 68
    new-instance v0, Ltv/periscope/android/ui/broadcast/as$3;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/as$3;-><init>(Ltv/periscope/android/ui/broadcast/as;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->n:Ltv/periscope/android/util/n$a$a;

    .line 114
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/as;->e:Ltv/periscope/android/view/ai;

    .line 115
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/as;->b:Landroid/view/ViewGroup;

    .line 116
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/as;->f:Ljava/lang/String;

    .line 117
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/as;->g:F

    .line 118
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/as;->h:Ltv/periscope/android/util/n$a;

    .line 119
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/as;->a:Ldae;

    .line 120
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/as;)Ltv/periscope/android/view/TweetSheet;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/as;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/as;->j:Z

    return p1
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/as;)Ltv/periscope/android/util/n$a$a;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->n:Ltv/periscope/android/util/n$a$a;

    return-object v0
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/as;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/as;->i:Z

    return p1
.end method

.method static synthetic c(Ltv/periscope/android/ui/broadcast/as;)Ltv/periscope/android/util/n$a;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->h:Ltv/periscope/android/util/n$a;

    return-object v0
.end method

.method static synthetic d(Ltv/periscope/android/ui/broadcast/as;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/as;->i:Z

    return v0
.end method

.method static synthetic e(Ltv/periscope/android/ui/broadcast/as;)F
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Ltv/periscope/android/ui/broadcast/as;->g:F

    return v0
.end method

.method static synthetic f(Ltv/periscope/android/ui/broadcast/as;)Landroid/view/View;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->d:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 175
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/as;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    if-nez v0, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    invoke-virtual {v0}, Ltv/periscope/android/view/TweetSheet;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/util/n;->a(Landroid/view/View;)V

    .line 179
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    invoke-virtual {v0}, Ltv/periscope/android/view/TweetSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    .line 180
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/as;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    .line 181
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/as;->d:Landroid/view/View;

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v4, v6, [F

    const/4 v5, 0x0

    aput v1, v4, v5

    const/4 v1, 0x1

    aput v0, v4, v1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 182
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    invoke-virtual {v1}, Ltv/periscope/android/view/TweetSheet;->getScrim()Landroid/view/View;

    move-result-object v1

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 183
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 185
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 186
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 190
    const-wide/16 v0, 0x12c

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 191
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 192
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 193
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 182
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public a(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 162
    if-eqz p1, :cond_1

    .line 163
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->k:Ljava/lang/String;

    .line 168
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/as;->a:Ldae;

    invoke-virtual {v0, v1, p1}, Ltv/periscope/android/view/TweetSheet;->a(Ldae;Ljava/io/File;)V

    .line 171
    :cond_0
    return-void

    .line 165
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->k:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 124
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    if-nez v0, :cond_0

    .line 125
    new-instance v0, Ltv/periscope/android/view/TweetSheet;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/as;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ltv/periscope/android/view/TweetSheet;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    .line 126
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    sget v1, Ltv/periscope/android/library/f$g;->container:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/TweetSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->d:Landroid/view/View;

    .line 127
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    invoke-virtual {v0, p0}, Ltv/periscope/android/view/TweetSheet;->setCallbackListener(Ltv/periscope/android/view/TweetSheet$a;)V

    .line 128
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 130
    :cond_0
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/as;->j:Z

    if-eqz v0, :cond_1

    .line 158
    :goto_0
    return-void

    .line 133
    :cond_1
    iput-boolean v9, p0, Ltv/periscope/android/ui/broadcast/as;->j:Z

    .line 134
    iput-boolean v8, p0, Ltv/periscope/android/ui/broadcast/as;->i:Z

    .line 135
    if-eqz p3, :cond_2

    .line 136
    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->k:Ljava/lang/String;

    .line 141
    :goto_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    invoke-virtual {v0}, Ltv/periscope/android/view/TweetSheet;->getScrim()Landroid/view/View;

    move-result-object v6

    .line 143
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/as;->a:Ldae;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/as;->f:Ljava/lang/String;

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Ltv/periscope/android/view/TweetSheet;->a(Ldae;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->d:Landroid/view/View;

    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v2, v7, [F

    iget v3, p0, Ltv/periscope/android/ui/broadcast/as;->g:F

    aput v3, v2, v8

    const/4 v3, 0x0

    aput v3, v2, v9

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 146
    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v7, [F

    fill-array-data v2, :array_0

    invoke-static {v6, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 148
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 149
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 150
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 154
    const-wide/16 v0, 0x12c

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 155
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 156
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->m:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 157
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 138
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->k:Ljava/lang/String;

    goto :goto_1

    .line 146
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/as;->j:Z

    return v0
.end method

.method public c()V
    .locals 4

    .prologue
    .line 203
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/as;->e:Ltv/periscope/android/view/ai;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    invoke-virtual {v1}, Ltv/periscope/android/view/TweetSheet;->getText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/as;->c:Ltv/periscope/android/view/TweetSheet;

    invoke-virtual {v2}, Ltv/periscope/android/view/TweetSheet;->getUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/as;->k:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Ltv/periscope/android/view/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 208
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/as;->a()V

    .line 209
    return-void
.end method
