.class public Ltv/periscope/android/ui/broadcast/FriendsWatchingView;
.super Landroid/widget/FrameLayout;
.source "Twttr"


# instance fields
.field private final a:Landroid/support/v7/widget/RecyclerView;

.field private b:Landroid/animation/Animator;

.field private c:Landroid/animation/Animator;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput-boolean v6, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->d:Z

    .line 42
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__friends_watching:I

    invoke-virtual {v0, v1, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 43
    sget v0, Ltv/periscope/android/library/f$g;->friends_watching:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a:Landroid/support/v7/widget/RecyclerView;

    .line 45
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$e;->ps__friends_watching_item_margin_start:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 46
    new-instance v1, Ltv/periscope/android/ui/chat/FriendsWatchingLayoutManager;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Ltv/periscope/android/library/f$e;->ps__friends_watching_avatar_size:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Ltv/periscope/android/ui/chat/FriendsWatchingLayoutManager;-><init>(Landroid/content/Context;ZF)V

    .line 47
    new-instance v2, Ltv/periscope/android/ui/chat/ab;

    invoke-direct {v2, v0}, Ltv/periscope/android/ui/chat/ab;-><init>(I)V

    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 49
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 50
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 51
    return-void
.end method

.method private a(II)Landroid/animation/Animator;
    .locals 5

    .prologue
    .line 111
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a:Landroid/support/v7/widget/RecyclerView;

    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    int-to-float v4, p1

    aput v4, v2, v3

    const/4 v3, 0x1

    int-to-float v4, p2

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 112
    new-instance v1, Ltv/periscope/android/ui/broadcast/FriendsWatchingView$3;

    invoke-direct {v1, p0, p2}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView$3;-><init>(Ltv/periscope/android/ui/broadcast/FriendsWatchingView;I)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 119
    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/FriendsWatchingView;)Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method private a(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 72
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    .line 75
    :cond_0
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/FriendsWatchingView;Z)Z
    .locals 0

    .prologue
    .line 22
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->d:Z

    return p1
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->d:Z

    return v0
.end method

.method public getHideAnimator()Landroid/animation/Animator;
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->c:Landroid/animation/Animator;

    if-nez v0, :cond_0

    .line 96
    const/4 v0, 0x0

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a(II)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->c:Landroid/animation/Animator;

    .line 97
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->c:Landroid/animation/Animator;

    new-instance v1, Ltv/periscope/android/ui/broadcast/FriendsWatchingView$2;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView$2;-><init>(Ltv/periscope/android/ui/broadcast/FriendsWatchingView;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 106
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->c:Landroid/animation/Animator;

    return-object v0
.end method

.method public getShowAnimator()Landroid/animation/Animator;
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->b:Landroid/animation/Animator;

    if-nez v0, :cond_0

    .line 80
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->getHeight()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a(II)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->b:Landroid/animation/Animator;

    .line 81
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->b:Landroid/animation/Animator;

    new-instance v1, Ltv/periscope/android/ui/broadcast/FriendsWatchingView$1;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView$1;-><init>(Ltv/periscope/android/ui/broadcast/FriendsWatchingView;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 90
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->b:Landroid/animation/Animator;

    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 55
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 57
    new-instance v0, Ltv/periscope/android/ui/chat/aa;

    invoke-direct {v0, p2}, Ltv/periscope/android/ui/chat/aa;-><init>(I)V

    .line 58
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 60
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->d:Z

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a:Landroid/support/v7/widget/RecyclerView;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setTranslationY(F)V

    .line 64
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->b:Landroid/animation/Animator;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a(Landroid/animation/Animator;)V

    .line 65
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->c:Landroid/animation/Animator;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a(Landroid/animation/Animator;)V

    .line 67
    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->b:Landroid/animation/Animator;

    .line 68
    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->c:Landroid/animation/Animator;

    .line 69
    return-void
.end method

.method public setAdapter(Ltv/periscope/android/ui/chat/z;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 124
    return-void
.end method
