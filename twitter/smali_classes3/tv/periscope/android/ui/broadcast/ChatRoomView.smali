.class public Ltv/periscope/android/ui/broadcast/ChatRoomView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/chat/z$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;,
        Ltv/periscope/android/ui/broadcast/ChatRoomView$a;
    }
.end annotation


# instance fields
.field private A:Landroid/view/View$OnTouchListener;

.field private B:Z

.field private C:I

.field private final D:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$c;

.field private a:Landroid/view/View;

.field private b:Ltv/periscope/android/ui/love/HeartContainerView;

.field private c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

.field private d:[Landroid/view/View;

.field private e:Ltv/periscope/android/ui/broadcast/BottomTray;

.field private f:Ltv/periscope/android/ui/broadcast/ParticipantCountView;

.field private final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ltv/periscope/android/ui/broadcast/FriendsWatchingView;

.field private i:Ltv/periscope/android/ui/chat/ac;

.field private j:Ltv/periscope/android/ui/chat/z;

.field private k:Landroid/view/View;

.field private l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

.field private m:Landroid/animation/Animator;

.field private n:Landroid/animation/Animator;

.field private o:Landroid/animation/Animator;

.field private p:Landroid/animation/Animator;

.field private q:Ltv/periscope/model/chat/Message;

.field private r:Ltv/periscope/android/util/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ltv/periscope/android/util/x",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

.field private u:Ltv/periscope/android/ui/broadcast/ChatRoomView$a;

.field private v:Ltv/periscope/android/ui/broadcast/d;

.field private w:Ltv/periscope/android/ui/broadcast/ah;

.field private final x:Ljava/lang/Runnable;

.field private y:Z

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 124
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->g:Ljava/util/HashMap;

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->s:Ljava/util/Map;

    .line 79
    sget-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->a:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->t:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    .line 92
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView$1;-><init>(Ltv/periscope/android/ui/broadcast/ChatRoomView;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->x:Ljava/lang/Runnable;

    .line 100
    iput v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->z:I

    .line 102
    iput-boolean v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->B:Z

    .line 115
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$2;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView$2;-><init>(Ltv/periscope/android/ui/broadcast/ChatRoomView;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->D:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$c;

    .line 125
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 129
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->g:Ljava/util/HashMap;

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->s:Ljava/util/Map;

    .line 79
    sget-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->a:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->t:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    .line 92
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView$1;-><init>(Ltv/periscope/android/ui/broadcast/ChatRoomView;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->x:Ljava/lang/Runnable;

    .line 100
    iput v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->z:I

    .line 102
    iput-boolean v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->B:Z

    .line 115
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$2;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView$2;-><init>(Ltv/periscope/android/ui/broadcast/ChatRoomView;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->D:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$c;

    .line 130
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 131
    return-void
.end method

.method private A()V
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->i()V

    .line 466
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->p()V

    .line 467
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->m()V

    .line 468
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->l()V

    .line 469
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->k()V

    .line 470
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->D()V

    .line 471
    return-void
.end method

.method private B()V
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->i()V

    .line 475
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->p()V

    .line 476
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->o()V

    .line 477
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->k()V

    .line 478
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->E()V

    .line 479
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->D()V

    .line 480
    return-void
.end method

.method private C()V
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->i()V

    .line 484
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->p()V

    .line 485
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->n()V

    .line 486
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->k()V

    .line 487
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->E()V

    .line 488
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->D()V

    .line 489
    return-void
.end method

.method private D()V
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->v:Ltv/periscope/android/ui/broadcast/d;

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->v:Ltv/periscope/android/ui/broadcast/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/d;->a()V

    .line 495
    :cond_0
    return-void
.end method

.method private E()V
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->w:Ltv/periscope/android/ui/broadcast/ah;

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->w:Ltv/periscope/android/ui/broadcast/ah;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ah;->a()V

    .line 501
    :cond_0
    return-void
.end method

.method private F()V
    .locals 2

    .prologue
    .line 749
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->q:Ltv/periscope/model/chat/Message;

    if-eqz v0, :cond_0

    .line 750
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->q:Ltv/periscope/model/chat/Message;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/ChatMessageContainerView;->a(Ltv/periscope/model/chat/Message;)V

    .line 751
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->q:Ltv/periscope/model/chat/Message;

    .line 753
    :cond_0
    return-void
.end method

.method private G()V
    .locals 2

    .prologue
    .line 840
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->j()V

    .line 841
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->e()V

    .line 842
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    sget-object v1, Ltv/periscope/android/ui/chat/ChatState;->f:Ltv/periscope/android/ui/chat/ChatState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/BottomTray;->setChatState(Ltv/periscope/android/ui/chat/ChatState;)V

    .line 843
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 134
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__chatroom_view:I

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 136
    sget-object v0, Ltv/periscope/android/library/f$n;->ChatRoomView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 137
    sget v0, Ltv/periscope/android/library/f$n;->ChatRoomView_ps__heartsMarginFactor:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->z:I

    .line 139
    sget v0, Ltv/periscope/android/library/f$n;->ChatRoomView_ps__includeModeration:I

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 140
    if-eqz v0, :cond_0

    .line 141
    sget v0, Ltv/periscope/android/library/f$g;->moderator_container:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 144
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 146
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$e;->ps__standard_spacing_20:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->C:I

    .line 148
    sget v0, Ltv/periscope/android/library/f$g;->chat_container:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a:Landroid/view/View;

    .line 149
    sget v0, Ltv/periscope/android/library/f$g;->hearts_view:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/love/HeartContainerView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b:Ltv/periscope/android/ui/love/HeartContainerView;

    .line 150
    sget v0, Ltv/periscope/android/library/f$g;->chat_messages_view:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    .line 152
    sget v0, Ltv/periscope/android/library/f$g;->moderator_overlay:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->k:Landroid/view/View;

    .line 154
    sget v0, Ltv/periscope/android/library/f$g;->bottom_tray:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/BottomTray;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    .line 155
    sget v0, Ltv/periscope/android/library/f$g;->participants:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/ParticipantCountView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f:Ltv/periscope/android/ui/broadcast/ParticipantCountView;

    .line 156
    sget v0, Ltv/periscope/android/library/f$g;->friends_watching_view:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->h:Ltv/periscope/android/ui/broadcast/FriendsWatchingView;

    .line 158
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->d:[Landroid/view/View;

    .line 159
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->d:[Landroid/view/View;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b:Ltv/periscope/android/ui/love/HeartContainerView;

    aput-object v1, v0, v3

    .line 160
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->d:[Landroid/view/View;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f:Ltv/periscope/android/ui/broadcast/ParticipantCountView;

    aput-object v1, v0, v2

    .line 162
    new-instance v0, Ltv/periscope/android/ui/chat/ac;

    invoke-direct {v0}, Ltv/periscope/android/ui/chat/ac;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i:Ltv/periscope/android/ui/chat/ac;

    .line 163
    new-instance v0, Ltv/periscope/android/ui/chat/z;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i:Ltv/periscope/android/ui/chat/ac;

    invoke-direct {v0, p1, v1}, Ltv/periscope/android/ui/chat/z;-><init>(Landroid/content/Context;Ltv/periscope/android/ui/chat/ac;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->j:Ltv/periscope/android/ui/chat/z;

    .line 164
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->h:Ltv/periscope/android/ui/broadcast/FriendsWatchingView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->j:Ltv/periscope/android/ui/chat/z;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->setAdapter(Ltv/periscope/android/ui/chat/z;)V

    .line 165
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->j:Ltv/periscope/android/ui/chat/z;

    invoke-virtual {v0, p0}, Ltv/periscope/android/ui/chat/z;->a(Ltv/periscope/android/ui/chat/z$a;)V

    .line 167
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->w()V

    .line 169
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$3;

    invoke-direct {v0, p0, p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView$3;-><init>(Ltv/periscope/android/ui/broadcast/ChatRoomView;Landroid/view/View;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->r:Ltv/periscope/android/util/x;

    .line 176
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/ChatRoomView;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->F()V

    return-void
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Ltv/periscope/android/ui/broadcast/BottomTray;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    return-object v0
.end method

.method static synthetic c(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Ltv/periscope/android/ui/chat/z;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->j:Ltv/periscope/android/ui/chat/z;

    return-object v0
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 353
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->s:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 354
    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->r:Ltv/periscope/android/util/x;

    invoke-virtual {v0, p1}, Ltv/periscope/android/util/x;->removeMessages(I)V

    .line 357
    :cond_0
    return-void
.end method

.method static synthetic d(Ltv/periscope/android/ui/broadcast/ChatRoomView;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->C:I

    return v0
.end method

.method private d(I)V
    .locals 4

    .prologue
    .line 360
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->r:Ltv/periscope/android/util/x;

    invoke-virtual {v0}, Ltv/periscope/android/util/x;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 361
    iput p1, v0, Landroid/os/Message;->what:I

    .line 362
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->r:Ltv/periscope/android/util/x;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v0, v2, v3}, Ltv/periscope/android/util/x;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 363
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->s:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    return-void
.end method

.method private e(I)Landroid/animation/Animator;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x2

    .line 620
    .line 621
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->k:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v8, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 622
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v8, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 623
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f:Ltv/periscope/android/ui/broadcast/ParticipantCountView;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v8, [F

    fill-array-data v4, :array_2

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 626
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a:Landroid/view/View;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v5, v8, [F

    const/4 v6, 0x0

    aput v6, v5, v9

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    .line 627
    invoke-virtual {v6}, Ltv/periscope/android/ui/broadcast/BottomTray;->getMeasuredHeight()I

    move-result v6

    iget v7, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->C:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    aput v6, v5, v10

    .line 626
    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 628
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 629
    int-to-long v6, p1

    invoke-virtual {v4, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 630
    const/4 v5, 0x4

    new-array v5, v5, [Landroid/animation/Animator;

    aput-object v0, v5, v9

    aput-object v1, v5, v10

    aput-object v2, v5, v8

    const/4 v0, 0x3

    aput-object v3, v5, v0

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 631
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$4;

    invoke-direct {v0, p0, v3}, Ltv/periscope/android/ui/broadcast/ChatRoomView$4;-><init>(Ltv/periscope/android/ui/broadcast/ChatRoomView;Landroid/animation/ObjectAnimator;)V

    invoke-virtual {v4, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 648
    return-object v4

    .line 621
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 622
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 623
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method static synthetic e(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->k:Landroid/view/View;

    return-object v0
.end method

.method private f(I)Landroid/animation/Animator;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x2

    .line 652
    .line 654
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->k:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v8, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 655
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v8, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 656
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f:Ltv/periscope/android/ui/broadcast/ParticipantCountView;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v8, [F

    fill-array-data v4, :array_2

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 657
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a:Landroid/view/View;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v5, v8, [F

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    .line 658
    invoke-virtual {v6}, Ltv/periscope/android/ui/broadcast/BottomTray;->getMeasuredHeight()I

    move-result v6

    iget v7, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->C:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    aput v6, v5, v9

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    invoke-virtual {v6}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->getInfoContainer()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    neg-int v6, v6

    int-to-float v6, v6

    aput v6, v5, v10

    .line 657
    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 659
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 660
    int-to-long v6, p1

    invoke-virtual {v4, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 661
    const/4 v5, 0x4

    new-array v5, v5, [Landroid/animation/Animator;

    aput-object v0, v5, v9

    aput-object v1, v5, v10

    aput-object v2, v5, v8

    const/4 v0, 0x3

    aput-object v3, v5, v0

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 663
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;

    invoke-direct {v0, p0, v3}, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;-><init>(Ltv/periscope/android/ui/broadcast/ChatRoomView;Landroid/animation/ObjectAnimator;)V

    invoke-virtual {v4, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 680
    return-object v4

    .line 654
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 655
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 656
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic f(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Ltv/periscope/android/ui/broadcast/ParticipantCountView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f:Ltv/periscope/android/ui/broadcast/ParticipantCountView;

    return-object v0
.end method

.method private g(I)Landroid/animation/Animator;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 684
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->k:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v6, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 685
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f:Ltv/periscope/android/ui/broadcast/ParticipantCountView;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 686
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a:Landroid/view/View;

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v4, v6, [F

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    .line 687
    invoke-virtual {v5}, Ltv/periscope/android/ui/broadcast/BottomTray;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    aput v5, v4, v7

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v5}, Ltv/periscope/android/ui/broadcast/BottomTray;->getMeasuredHeight()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    aput v5, v4, v8

    .line 686
    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 688
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 689
    int-to-long v4, p1

    invoke-virtual {v3, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 690
    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    aput-object v0, v4, v7

    aput-object v1, v4, v8

    aput-object v2, v4, v6

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 691
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$6;

    invoke-direct {v0, p0, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView$6;-><init>(Ltv/periscope/android/ui/broadcast/ChatRoomView;Landroid/animation/ObjectAnimator;)V

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 707
    return-object v3

    .line 684
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 685
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic g(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a:Landroid/view/View;

    return-object v0
.end method

.method private h(I)Landroid/animation/Animator;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x2

    .line 711
    .line 713
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v8, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 714
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v8, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 715
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f:Ltv/periscope/android/ui/broadcast/ParticipantCountView;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v8, [F

    fill-array-data v4, :array_2

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 716
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a:Landroid/view/View;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v5, v8, [F

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    .line 717
    invoke-virtual {v6}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->getInfoContainer()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    neg-int v6, v6

    int-to-float v6, v6

    aput v6, v5, v9

    const/4 v6, 0x0

    aput v6, v5, v10

    .line 716
    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 718
    int-to-long v4, p1

    invoke-virtual {v0, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 719
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 720
    int-to-long v6, p1

    invoke-virtual {v4, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 721
    const/4 v5, 0x4

    new-array v5, v5, [Landroid/animation/Animator;

    aput-object v0, v5, v9

    aput-object v3, v5, v10

    aput-object v1, v5, v8

    const/4 v0, 0x3

    aput-object v2, v5, v0

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 722
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$7;

    invoke-direct {v0, p0, v3}, Ltv/periscope/android/ui/broadcast/ChatRoomView$7;-><init>(Ltv/periscope/android/ui/broadcast/ChatRoomView;Landroid/animation/ObjectAnimator;)V

    invoke-virtual {v4, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 745
    return-object v4

    .line 713
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 714
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 715
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic h(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    return-object v0
.end method

.method static synthetic i(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->x:Ljava/lang/Runnable;

    return-object v0
.end method

.method private setComposerSendEnabled(Z)V
    .locals 1

    .prologue
    .line 536
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 541
    :goto_0
    return-void

    .line 539
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/BottomTray;->setSendEnabled(Z)V

    .line 540
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->a()V

    goto :goto_0
.end method

.method private w()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 233
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 234
    sget v1, Ltv/periscope/android/library/f$g;->moderator_container:I

    invoke-virtual {v0, v5, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 236
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Ltv/periscope/android/library/f$e;->ps__standard_spacing_16:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 239
    sget-object v3, Ltv/periscope/android/ui/broadcast/ChatRoomView$9;->a:[I

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->t:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v4}, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 274
    :goto_0
    return-void

    .line 242
    :pswitch_0
    invoke-virtual {v0, v5, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    move v2, v1

    move v3, v1

    .line 265
    :goto_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    invoke-virtual {v1}, Ltv/periscope/android/ui/chat/ChatMessageContainerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 266
    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 268
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b:Ltv/periscope/android/ui/love/HeartContainerView;

    invoke-virtual {v2}, Ltv/periscope/android/ui/love/HeartContainerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 269
    iget v4, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->z:I

    mul-int/2addr v3, v4

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 271
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 272
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/ChatMessageContainerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 273
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b:Ltv/periscope/android/ui/love/HeartContainerView;

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/love/HeartContainerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 253
    :pswitch_1
    sget v1, Ltv/periscope/android/library/f$g;->bottom_tray:I

    invoke-virtual {v0, v5, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    move v3, v2

    .line 256
    goto :goto_1

    .line 239
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private x()V
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->i()V

    .line 445
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->l()V

    .line 446
    return-void
.end method

.method private y()V
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->i()V

    .line 450
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->p()V

    .line 451
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->l()V

    .line 452
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->k()V

    .line 453
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->j()V

    .line 454
    return-void
.end method

.method private z()V
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->i()V

    .line 458
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->p()V

    .line 459
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->l()V

    .line 460
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->k()V

    .line 461
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->D()V

    .line 462
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/16 v1, 0x1f4

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    .line 180
    sget v0, Ltv/periscope/android/library/f$g;->moderator_view:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    .line 181
    invoke-direct {p0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->h(I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->m:Landroid/animation/Animator;

    .line 182
    invoke-direct {p0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f(I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->o:Landroid/animation/Animator;

    .line 183
    invoke-direct {p0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->g(I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->p:Landroid/animation/Animator;

    .line 184
    invoke-direct {p0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e(I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->n:Landroid/animation/Animator;

    .line 185
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 192
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a:Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v2}, Ltv/periscope/android/ui/broadcast/BottomTray;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 193
    return-void
.end method

.method public a(IZ)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 324
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(IZLjava/lang/String;)V

    .line 325
    return-void
.end method

.method public a(IZLjava/lang/String;)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 328
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 329
    if-eqz v0, :cond_0

    .line 330
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 333
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b:Ltv/periscope/android/ui/love/HeartContainerView;

    invoke-virtual {v0, p1, p2, p3}, Ltv/periscope/android/ui/love/HeartContainerView;->a(IZLjava/lang/String;)V

    .line 334
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->u:Ltv/periscope/android/ui/broadcast/ChatRoomView$a;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->u:Ltv/periscope/android/ui/broadcast/ChatRoomView$a;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcast/ChatRoomView$a;->a(Ljava/lang/String;)V

    .line 208
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 397
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 401
    :goto_0
    return-void

    .line 400
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/BottomTray;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 591
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->h:Ltv/periscope/android/ui/broadcast/FriendsWatchingView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->h:Ltv/periscope/android/ui/broadcast/FriendsWatchingView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->getShowAnimator()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 595
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->g:Ljava/util/HashMap;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 596
    invoke-static {p1, p2, p3, p4}, Ldce;->a(Ljava/lang/String;Ljava/lang/String;J)Ldce;

    move-result-object v0

    .line 597
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i:Ltv/periscope/android/ui/chat/ac;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/chat/ac;->a(Ldce;)V

    .line 598
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->j:Ltv/periscope/android/ui/chat/z;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i:Ltv/periscope/android/ui/chat/ac;

    invoke-virtual {v1}, Ltv/periscope/android/ui/chat/ac;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/z;->notifyItemInserted(I)V

    .line 599
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b:Ltv/periscope/android/ui/love/HeartContainerView;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/love/HeartContainerView;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 296
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/chat/ChatMessageContainerView;->a(Ltv/periscope/model/chat/Message;)V

    .line 379
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 197
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 846
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 854
    :goto_0
    return-void

    .line 849
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->G()V

    .line 851
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->q()V

    .line 852
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->setVisibility(I)V

    .line 853
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->D:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$c;

    invoke-virtual {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->a(ILtv/periscope/android/ui/broadcast/moderator/ModeratorView$c;)V

    goto :goto_0
.end method

.method public b(IZ)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 367
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b:Ltv/periscope/android/ui/love/HeartContainerView;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/love/HeartContainerView;->a(IZ)V

    .line 368
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 337
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i:Ltv/periscope/android/ui/chat/ac;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/chat/ac;->a(Ljava/lang/String;)I

    move-result v0

    .line 338
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i:Ltv/periscope/android/ui/chat/ac;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/chat/ac;->b(I)Ldce;

    move-result-object v1

    .line 343
    if-eqz v1, :cond_0

    .line 347
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->j:Ltv/periscope/android/ui/chat/z;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ltv/periscope/android/ui/chat/z;->notifyItemChanged(ILjava/lang/Object;)V

    .line 348
    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c(I)V

    .line 349
    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->d(I)V

    goto :goto_0
.end method

.method public b(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 793
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 800
    :goto_0
    return-void

    .line 796
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->a(Ltv/periscope/model/chat/Message;)V

    .line 797
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->q()V

    .line 798
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->n:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 799
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->n:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 201
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 602
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 603
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i:Ltv/periscope/android/ui/chat/ac;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/chat/ac;->a(Ljava/lang/String;)I

    move-result v0

    .line 604
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i:Ltv/periscope/android/ui/chat/ac;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/chat/ac;->c(I)V

    .line 605
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->j:Ltv/periscope/android/ui/chat/z;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/chat/z;->notifyItemRemoved(I)V

    .line 607
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i:Ltv/periscope/android/ui/chat/ac;

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/ac;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->h:Ltv/periscope/android/ui/broadcast/FriendsWatchingView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 608
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->h:Ltv/periscope/android/ui/broadcast/FriendsWatchingView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/FriendsWatchingView;->getHideAnimator()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 610
    :cond_0
    return-void
.end method

.method public c(Ltv/periscope/model/chat/Message;)V
    .locals 3

    .prologue
    .line 803
    const-string/jumbo v0, "ChatRoomView"

    const-string/jumbo v1, "showModeratorVerdict"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_1

    .line 829
    :cond_0
    :goto_0
    return-void

    .line 808
    :cond_1
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->q:Ltv/periscope/model/chat/Message;

    .line 809
    sget-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$9;->b:[I

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/chat/MessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 825
    const-string/jumbo v0, "ChatRoomView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid message type received for verdict: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v2

    invoke-virtual {v2}, Ltv/periscope/model/chat/MessageType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 812
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->i()V

    .line 813
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 814
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 818
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 820
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->F()V

    goto :goto_0

    .line 809
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 299
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->B:Z

    .line 300
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/ChatMessageContainerView;->setVisibility(I)V

    .line 301
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b:Ltv/periscope/android/ui/love/HeartContainerView;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/love/HeartContainerView;->setVisibility(I)V

    .line 302
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 305
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->B:Z

    .line 306
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->g()V

    .line 307
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/ChatMessageContainerView;->setVisibility(I)V

    .line 308
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b:Ltv/periscope/android/ui/love/HeartContainerView;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/love/HeartContainerView;->setVisibility(I)V

    .line 309
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 313
    iput-boolean v2, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->B:Z

    .line 314
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->g()V

    .line 315
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/ChatMessageContainerView;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b:Ltv/periscope/android/ui/love/HeartContainerView;

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/love/HeartContainerView;->setVisibility(I)V

    .line 317
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 320
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->B:Z

    return v0
.end method

.method public getRightAlignedViews()[Landroid/view/View;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->d:[Landroid/view/View;

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/ChatMessageContainerView;->b()V

    .line 383
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/ChatMessageContainerView;->a()V

    .line 387
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 511
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 515
    :goto_0
    return-void

    .line 514
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->b()V

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 518
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 519
    const/4 v0, 0x0

    .line 521
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->h()Z

    move-result v0

    goto :goto_0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 525
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 529
    :goto_0
    return-void

    .line 528
    :cond_0
    sget-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->e:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    goto :goto_0
.end method

.method public m()V
    .locals 1

    .prologue
    .line 532
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setComposerSendEnabled(Z)V

    .line 533
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    .line 544
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setComposerSendEnabled(Z)V

    .line 545
    return-void
.end method

.method public o()V
    .locals 1

    .prologue
    .line 548
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 552
    :goto_0
    return-void

    .line 551
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-static {v0}, Ltv/periscope/android/util/n;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->A:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->A:Landroid/view/View$OnTouchListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 224
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public p()V
    .locals 1

    .prologue
    .line 756
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 762
    :goto_0
    return-void

    .line 759
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->a()V

    .line 760
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->o:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 761
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->o:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method public q()V
    .locals 2

    .prologue
    .line 765
    const-string/jumbo v0, "ChatRoomView"

    const-string/jumbo v1, "showModeratorTimedOut"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 772
    :goto_0
    return-void

    .line 769
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->c()V

    .line 770
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->p:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 771
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->p:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method public r()V
    .locals 3

    .prologue
    .line 775
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 790
    :goto_0
    return-void

    .line 778
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->i()V

    .line 779
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 780
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 782
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 783
    new-instance v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$8;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-direct {v1, p0, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView$8;-><init>(Ltv/periscope/android/ui/broadcast/ChatRoomView;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 789
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 782
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public s()V
    .locals 2

    .prologue
    .line 832
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 837
    :goto_0
    return-void

    .line 835
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->d()V

    .line 836
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setAvatarImageLoader(Ldae;)V
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->j:Ltv/periscope/android/ui/chat/z;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/chat/z;->a(Ldae;)V

    .line 287
    return-void
.end method

.method public setBottomTrayActionButtonPresenter(Ltv/periscope/android/ui/broadcast/d;)V
    .locals 2

    .prologue
    .line 577
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->v:Ltv/periscope/android/ui/broadcast/d;

    .line 578
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->v:Ltv/periscope/android/ui/broadcast/d;

    sget v0, Ltv/periscope/android/library/f$g;->action_button:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-interface {v1, v0}, Ltv/periscope/android/ui/broadcast/d;->a(Landroid/widget/ImageView;)V

    .line 579
    return-void
.end method

.method public setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V
    .locals 4

    .prologue
    .line 404
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->t:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    .line 405
    sget-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$9;->a:[I

    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 435
    const-string/jumbo v0, "Unknown Composer State"

    .line 436
    const-string/jumbo v0, "ChatRoomView"

    const-string/jumbo v1, "Unknown Composer State"

    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v3, "Unknown Composer State"

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Ltv/periscope/android/util/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 440
    :goto_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->w()V

    .line 441
    return-void

    .line 407
    :pswitch_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->x()V

    goto :goto_0

    .line 411
    :pswitch_1
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y()V

    goto :goto_0

    .line 415
    :pswitch_2
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->z()V

    goto :goto_0

    .line 419
    :pswitch_3
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->A()V

    goto :goto_0

    .line 423
    :pswitch_4
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->B()V

    goto :goto_0

    .line 427
    :pswitch_5
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->C()V

    goto :goto_0

    .line 431
    :pswitch_6
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->i()V

    goto :goto_0

    .line 405
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setChatAlpha(F)V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b:Ltv/periscope/android/ui/love/HeartContainerView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/love/HeartContainerView;->setAlpha(F)V

    .line 278
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/chat/ChatMessageContainerView;->setAlpha(F)V

    .line 279
    return-void
.end method

.method public setChatMessageAdapter(Ltv/periscope/android/ui/chat/g;)V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c:Ltv/periscope/android/ui/chat/ChatMessageContainerView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/chat/ChatMessageContainerView;->setChatMessageAdapter(Ltv/periscope/android/ui/chat/g;)V

    .line 283
    return-void
.end method

.method public setChatState(Ltv/periscope/android/ui/chat/ChatState;)V
    .locals 1

    .prologue
    .line 390
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 394
    :goto_0
    return-void

    .line 393
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/BottomTray;->setChatState(Ltv/periscope/android/ui/chat/ChatState;)V

    goto :goto_0
.end method

.method public setFriendsWatchingListener(Ltv/periscope/android/ui/broadcast/ChatRoomView$a;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->u:Ltv/periscope/android/ui/broadcast/ChatRoomView$a;

    .line 189
    return-void
.end method

.method public setHeartsMarginFactor(I)V
    .locals 0

    .prologue
    .line 228
    iput p1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->z:I

    .line 229
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->w()V

    .line 230
    return-void
.end method

.method public setImageLoader(Ldae;)V
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b:Ltv/periscope/android/ui/love/HeartContainerView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/love/HeartContainerView;->setImageLoader(Ldae;)V

    .line 291
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/BottomTray;->setImageLoader(Ldae;)V

    .line 292
    return-void
.end method

.method public setLocalPunishmentPrompt(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 371
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 375
    :goto_0
    return-void

    .line 374
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/BottomTray;->setLocalPunishmentPrompt(Ltv/periscope/model/chat/Message;)V

    goto :goto_0
.end method

.method public setModeratorSelectionListener(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$b;)V
    .locals 1

    .prologue
    .line 613
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 617
    :goto_0
    return-void

    .line 616
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->setModeratorSelectionListener(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$b;)V

    goto :goto_0
.end method

.method public setOnInterceptTouchEventListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->A:Landroid/view/View$OnTouchListener;

    .line 217
    return-void
.end method

.method public setOverflowClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/BottomTray;->setOverflowClickListener(Landroid/view/View$OnClickListener;)V

    .line 574
    return-void
.end method

.method public setParticipantClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f:Ltv/periscope/android/ui/broadcast/ParticipantCountView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ParticipantCountView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 570
    return-void
.end method

.method public setParticipantCount(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f:Ltv/periscope/android/ui/broadcast/ParticipantCountView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ParticipantCountView;->setNumParticipants(Ljava/lang/String;)V

    .line 588
    return-void
.end method

.method public setPlaytimePresenter(Ltv/periscope/android/ui/broadcast/ah;)V
    .locals 2

    .prologue
    .line 582
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->w:Ltv/periscope/android/ui/broadcast/ah;

    .line 583
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->w:Ltv/periscope/android/ui/broadcast/ah;

    sget v0, Ltv/periscope/android/library/f$g;->play_time:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/ah;->a(Landroid/widget/TextView;)V

    .line 584
    return-void
.end method

.method setPunishmentStatusDelegate(Ltv/periscope/android/ui/chat/ag;)V
    .locals 1

    .prologue
    .line 562
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 566
    :goto_0
    return-void

    .line 565
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/BottomTray;->setPunishmentStatusDelegate(Ltv/periscope/android/ui/chat/ag;)V

    goto :goto_0
.end method

.method setSendCommentDelegate(Ltv/periscope/android/ui/chat/ai;)V
    .locals 1

    .prologue
    .line 555
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 559
    :goto_0
    return-void

    .line 558
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/BottomTray;->setSendCommentDelegate(Ltv/periscope/android/ui/chat/ai;)V

    goto :goto_0
.end method

.method public setUpComposerReply(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 504
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 508
    :goto_0
    return-void

    .line 507
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/BottomTray;->setUpReply(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public t()V
    .locals 3

    .prologue
    .line 857
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 862
    :goto_0
    return-void

    .line 860
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->G()V

    .line 861
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__chat_status_moderation_disabled:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/BottomTray;->setChatStatus(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public u()V
    .locals 3

    .prologue
    .line 865
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 870
    :goto_0
    return-void

    .line 868
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->G()V

    .line 869
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__chat_status_moderation_disabled_global:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/BottomTray;->setChatStatus(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public v()V
    .locals 1

    .prologue
    .line 873
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->y:Z

    if-nez v0, :cond_0

    .line 879
    :goto_0
    return-void

    .line 876
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->f()V

    .line 877
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->p()V

    .line 878
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->d()V

    goto :goto_0
.end method
