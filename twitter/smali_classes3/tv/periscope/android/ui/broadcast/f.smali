.class public Ltv/periscope/android/ui/broadcast/f;
.super Ltv/periscope/android/ui/broadcast/a;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/RootDragLayout$b;


# instance fields
.field private final c:Ltv/periscope/android/ui/broadcast/ChatRoomView;

.field private final d:I


# direct methods
.method public constructor <init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/f;Ltv/periscope/android/view/ActionSheet;Ltv/periscope/android/ui/broadcast/ChatRoomView;Z)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0, p1, p3, p5}, Ltv/periscope/android/ui/broadcast/a;-><init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/ActionSheet;Z)V

    .line 24
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/f;->c:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    .line 25
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/f;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-virtual {v0, p0}, Ltv/periscope/android/view/RootDragLayout;->setListener(Ltv/periscope/android/view/RootDragLayout$b;)V

    .line 26
    invoke-virtual {p4}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$e;->ps__standard_spacing_10:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/f;->d:I

    .line 27
    invoke-virtual {p3, p2}, Ltv/periscope/android/view/ActionSheet;->setActionAdapter(Ltv/periscope/android/view/f;)V

    .line 28
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;
    .locals 1

    .prologue
    .line 16
    invoke-super {p0, p1, p2, p3}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method a()Ltv/periscope/android/ui/broadcast/b;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/view/View;IIII)V
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/f;->b:Ltv/periscope/android/view/ActionSheet;

    if-ne p1, v0, :cond_0

    .line 51
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/f;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-virtual {v0}, Ltv/periscope/android/view/RootDragLayout;->getHeight()I

    move-result v0

    sub-int/2addr v0, p3

    .line 52
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/f;->c:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget v2, p0, Ltv/periscope/android/ui/broadcast/f;->d:I

    add-int/2addr v2, v0

    mul-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(I)V

    .line 53
    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/f;->c:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b()V

    .line 57
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Lcyw;Ldae;)V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/a;->a(Lcyw;Ldae;)V

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List",
            "<+",
            "Ltv/periscope/android/view/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-super {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/f;->c:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c()V

    .line 34
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/CharSequence;Ljava/util/List;I)V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0, p1, p2, p3}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/CharSequence;Ljava/util/List;I)V

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Ljava/util/List;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List",
            "<+",
            "Ltv/periscope/android/view/a;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-super {p0, p1, p2, p3, p4}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/CharSequence;Ljava/util/List;J)V

    .line 39
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/f;->c:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c()V

    .line 40
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/String;Ltv/periscope/model/chat/Message;IZ)V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0, p1, p2, p3, p4}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;IZ)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/String;Ltv/periscope/model/chat/Message;IZLjava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 16
    invoke-super/range {p0 .. p5}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;IZLjava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/chat/ah;)V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0, p1}, Ltv/periscope/android/ui/broadcast/a;->a(Ltv/periscope/android/ui/chat/ah;)V

    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/view/CarouselView$a;)V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0, p1}, Ltv/periscope/android/ui/broadcast/a;->a(Ltv/periscope/android/view/CarouselView$a;)V

    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/view/i;)V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0, p1}, Ltv/periscope/android/ui/broadcast/a;->a(Ltv/periscope/android/view/i;)V

    return-void
.end method

.method public bridge synthetic b()V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0}, Ltv/periscope/android/ui/broadcast/a;->b()V

    return-void
.end method

.method public bridge synthetic c()V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0}, Ltv/periscope/android/ui/broadcast/a;->c()V

    return-void
.end method

.method public bridge synthetic d()Z
    .locals 1

    .prologue
    .line 16
    invoke-super {p0}, Ltv/periscope/android/ui/broadcast/a;->d()Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Ltv/periscope/android/ui/broadcast/a;->e()V

    .line 45
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/f;->c:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b()V

    .line 46
    return-void
.end method
