.class public Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "More"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem",
        "<",
        "Ltv/periscope/model/q;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ltv/periscope/android/ui/broadcast/j;

.field private final b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;

.field private final c:I


# direct methods
.method public constructor <init>(Ltv/periscope/android/ui/broadcast/j;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;I)V
    .locals 0

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;->a:Ltv/periscope/android/ui/broadcast/j;

    .line 179
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;

    .line 180
    iput p3, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;->c:I

    .line 181
    return-void
.end method


# virtual methods
.method public a()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;
    .locals 1

    .prologue
    .line 185
    sget-object v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;->g:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;

    return-object v0
.end method

.method public b()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;->c:I

    return v0
.end method

.method public d()Ltv/periscope/model/q;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;->a:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->p()Ltv/periscope/model/q;

    move-result-object v0

    return-object v0
.end method
