.class public Ltv/periscope/android/ui/broadcast/k;
.super Ltv/periscope/android/ui/broadcast/j;
.source "Twttr"


# instance fields
.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcyw;Lcyn;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/android/ui/broadcast/y;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-direct/range {p0 .. p5}, Ltv/periscope/android/ui/broadcast/j;-><init>(Landroid/content/Context;Lcyw;Lcyn;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/android/ui/broadcast/y;)V

    .line 22
    iput-boolean p6, p0, Ltv/periscope/android/ui/broadcast/k;->k:Z

    .line 23
    return-void
.end method

.method private c(Ltv/periscope/model/p;)V
    .locals 5

    .prologue
    .line 53
    invoke-virtual {p1}, Ltv/periscope/model/p;->L()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/k;->k:Z

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/k;->b:Ljava/util/List;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$l;

    new-instance v2, Ldau;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/k;->f:Ljava/lang/String;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/k;->a:Ltv/periscope/android/ui/broadcast/h;

    invoke-direct {v2, v3, v4}, Ldau;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    invoke-direct {v1, v2}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$l;-><init>(Ldaj;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    :cond_0
    invoke-virtual {p1}, Ltv/periscope/model/p;->F()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 58
    invoke-static {p1}, Ltv/periscope/model/p;->a(Ltv/periscope/model/p;)I

    move-result v0

    if-lez v0, :cond_1

    invoke-static {p1}, Ltv/periscope/model/p;->a(Ltv/periscope/model/p;)I

    move-result v0

    const/16 v1, 0x18

    if-gt v0, v1, :cond_1

    .line 61
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/k;->b:Ljava/util/List;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$l;

    new-instance v2, Ldak;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/k;->a:Ltv/periscope/android/ui/broadcast/h;

    invoke-direct {v2, p1, v3}, Ldak;-><init>(Ltv/periscope/model/p;Ltv/periscope/android/ui/broadcast/h;)V

    invoke-direct {v1, v2}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$l;-><init>(Ldaj;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    :cond_1
    :goto_0
    return-void

    .line 65
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/k;->b:Ljava/util/List;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$l;

    new-instance v2, Ldam;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/k;->f:Ljava/lang/String;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/k;->a:Ltv/periscope/android/ui/broadcast/h;

    invoke-direct {v2, v3, v4}, Ldam;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    invoke-direct {v1, v2}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$l;-><init>(Ldaj;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method protected a(Ltv/periscope/model/p;)V
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/k;->j()V

    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/k;->a(Ltv/periscope/model/p;I)V

    .line 35
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/k;->j:Z

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/k;->c(Ltv/periscope/model/p;I)V

    .line 39
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 40
    invoke-virtual {p1}, Ltv/periscope/model/p;->L()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Ltv/periscope/model/p;->M()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 41
    :cond_1
    invoke-virtual {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/k;->b(Ltv/periscope/model/p;I)V

    .line 44
    :cond_2
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/k;->c(Ltv/periscope/model/p;)V

    .line 45
    return-void
.end method

.method protected b()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;->a:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    return-object v0
.end method

.method protected b(Ltv/periscope/model/p;)V
    .locals 0

    .prologue
    .line 50
    return-void
.end method
