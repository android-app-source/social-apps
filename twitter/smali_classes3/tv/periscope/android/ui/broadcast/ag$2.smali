.class Ltv/periscope/android/ui/broadcast/ag$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/ag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/ag;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/ag;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ag$2;->a:Ltv/periscope/android/ui/broadcast/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag$2;->a:Ltv/periscope/android/ui/broadcast/ag;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ag;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag$2;->a:Ltv/periscope/android/ui/broadcast/ag;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    const-string/jumbo v0, "PlayerHelper"

    const-string/jumbo v1, "Buffering for more than 1000, showing loading bars."

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag$2;->a:Ltv/periscope/android/ui/broadcast/ag;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/ag;->c:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag$2;->a:Ltv/periscope/android/ui/broadcast/ag;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag$2;->a:Ltv/periscope/android/ui/broadcast/ag;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/ag;->e()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__trying_to_reconnect:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->a(Ljava/lang/String;)V

    .line 205
    :cond_1
    :goto_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag$2;->a:Ltv/periscope/android/ui/broadcast/ag;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ltv/periscope/android/ui/broadcast/ag;->a(Ltv/periscope/android/ui/broadcast/ag;Z)Z

    goto :goto_0

    .line 202
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag$2;->a:Ltv/periscope/android/ui/broadcast/ag;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag$2;->a:Ltv/periscope/android/ui/broadcast/ag;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/ag;->e()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__loading:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
