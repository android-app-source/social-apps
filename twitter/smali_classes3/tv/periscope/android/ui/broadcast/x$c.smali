.class final Ltv/periscope/android/ui/broadcast/x$c;
.super Ltv/periscope/android/util/x;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/util/x",
        "<",
        "Ltv/periscope/android/ui/broadcast/x;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/x;


# direct methods
.method public constructor <init>(Ltv/periscope/android/ui/broadcast/x;Ltv/periscope/android/ui/broadcast/x;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/x$c;->a:Ltv/periscope/android/ui/broadcast/x;

    .line 68
    invoke-direct {p0, p2, p3}, Ltv/periscope/android/util/x;-><init>(Ljava/lang/Object;Landroid/os/Looper;)V

    .line 69
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 62
    check-cast p2, Ltv/periscope/android/ui/broadcast/x;

    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/x$c;->a(Landroid/os/Message;Ltv/periscope/android/ui/broadcast/x;)V

    return-void
.end method

.method public a(Landroid/os/Message;Ltv/periscope/android/ui/broadcast/x;)V
    .locals 4

    .prologue
    .line 73
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 75
    :pswitch_0
    const-string/jumbo v0, "ThreadSafetyHandler"

    const-string/jumbo v1, "Writing Graph points to internal storage"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/ui/broadcast/x$a;

    .line 77
    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/x$a;->b:Ljava/lang/String;

    .line 78
    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/x$a;->c:Ljava/util/List;

    .line 79
    if-eqz v0, :cond_0

    .line 81
    :try_start_0
    invoke-static {p2, v1, v0}, Ltv/periscope/android/ui/broadcast/x;->a(Ltv/periscope/android/ui/broadcast/x;Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/x$c;->a:Ltv/periscope/android/ui/broadcast/x;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/x;->a(Ltv/periscope/android/ui/broadcast/x;)Ltv/periscope/android/ui/broadcast/x$b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/x$c;->a:Ltv/periscope/android/ui/broadcast/x;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/x;->a(Ltv/periscope/android/ui/broadcast/x;)Ltv/periscope/android/ui/broadcast/x$b;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/x$b;->a()V

    goto :goto_0

    .line 91
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ltv/periscope/android/ui/broadcast/x$a;

    if-eqz v0, :cond_0

    .line 92
    const-string/jumbo v0, "ThreadSafetyHandler"

    const-string/jumbo v1, "Reading graph points from internal storage"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/ui/broadcast/x$a;

    .line 94
    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/x$a;->b:Ljava/lang/String;

    .line 95
    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/x$a;->a:Ltv/periscope/android/ui/broadcast/y$b;

    .line 96
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/x$c;->a:Ltv/periscope/android/ui/broadcast/x;

    invoke-static {v2, v1}, Ltv/periscope/android/ui/broadcast/x;->a(Ltv/periscope/android/ui/broadcast/x;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 99
    if-eqz v2, :cond_1

    .line 100
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/x$c;->a:Ltv/periscope/android/ui/broadcast/x;

    invoke-static {v3}, Ltv/periscope/android/ui/broadcast/x;->b(Ltv/periscope/android/ui/broadcast/x;)Ltv/periscope/android/ui/broadcast/y;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ltv/periscope/android/ui/broadcast/y;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 103
    :cond_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/x$c;->a:Ltv/periscope/android/ui/broadcast/x;

    iget-object v1, v1, Ltv/periscope/android/ui/broadcast/x;->a:Landroid/os/Handler;

    new-instance v3, Ltv/periscope/android/ui/broadcast/x$c$1;

    invoke-direct {v3, p0, v0, v2}, Ltv/periscope/android/ui/broadcast/x$c$1;-><init>(Ltv/periscope/android/ui/broadcast/x$c;Ltv/periscope/android/ui/broadcast/y$b;Ljava/util/List;)V

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
