.class public Ltv/periscope/android/ui/broadcast/z;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/SettingsDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/DialogInterface$OnDismissListener;

.field private d:Landroid/support/v7/app/AlertDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface$OnDismissListener;Ljava/lang/ref/WeakReference;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/DialogInterface$OnDismissListener;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/SettingsDelegate;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/z;->b:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/z;->c:Landroid/content/DialogInterface$OnDismissListener;

    .line 39
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/z;->a:Ljava/lang/ref/WeakReference;

    .line 40
    return-void
.end method

.method private a(Landroid/content/Context;)Landroid/support/v7/app/AlertDialog;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 44
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__learn_about_moderation_dialog:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 45
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Ltv/periscope/android/library/f$i;->ps__learn_about_moderation_dialog_title:I

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 47
    sget v0, Ltv/periscope/android/library/f$g;->body:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Ltv/periscope/android/library/f$l;->ps__dialog_moderator_learn_more_body:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 52
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 53
    sget v0, Ltv/periscope/android/library/f$g;->disable:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 54
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 55
    sget v4, Ltv/periscope/android/library/f$l;->ps__dialog_moderator_learn_more_disable:I

    new-array v5, v9, [Ljava/lang/Object;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "*1$"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Ltv/periscope/android/library/f$l;->ps__settings:I

    .line 56
    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "*"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    .line 55
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 57
    sget v5, Ltv/periscope/android/library/f$d;->ps__blue:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    new-array v5, v9, [Landroid/view/View$OnClickListener;

    new-instance v6, Ltv/periscope/android/ui/broadcast/z$1;

    invoke-direct {v6, p0}, Ltv/periscope/android/ui/broadcast/z$1;-><init>(Ltv/periscope/android/ui/broadcast/z;)V

    aput-object v6, v5, v8

    invoke-static {v0, v4, v3, v5}, Ltv/periscope/android/util/r;->a(Landroid/widget/TextView;Ljava/lang/String;I[Landroid/view/View$OnClickListener;)V

    .line 68
    :cond_0
    sget v0, Ltv/periscope/android/library/f$g;->positive:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 69
    new-instance v3, Ltv/periscope/android/ui/broadcast/z$2;

    invoke-direct {v3, p0}, Ltv/periscope/android/ui/broadcast/z$2;-><init>(Ltv/periscope/android/ui/broadcast/z;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 77
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 78
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 79
    invoke-virtual {v0, v8}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/z;->c:Landroid/content/DialogInterface$OnDismissListener;

    .line 80
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 76
    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->d:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->d:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 96
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->d:Landroid/support/v7/app/AlertDialog;

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->b:Landroid/content/Context;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/z;->a(Landroid/content/Context;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->d:Landroid/support/v7/app/AlertDialog;

    .line 88
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->d:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$m;->ps__DialogTransitions:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 89
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/z;->d()V

    .line 90
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->d:Landroid/support/v7/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->d:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->d:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->dismiss()V

    .line 102
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->d:Landroid/support/v7/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->d:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->d:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->dismiss()V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/z;->d:Landroid/support/v7/app/AlertDialog;

    .line 108
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/z;->d()V

    .line 110
    :cond_0
    return-void
.end method
