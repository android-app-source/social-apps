.class public Ltv/periscope/android/ui/broadcast/y;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/y$b;,
        Ltv/periscope/android/ui/broadcast/y$a;
    }
.end annotation


# instance fields
.field private final a:Ltv/periscope/android/ui/broadcast/y$a;

.field private final b:Ltv/periscope/android/ui/broadcast/y$a;

.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->c:Ljava/util/HashMap;

    .line 35
    new-instance v0, Ltv/periscope/android/ui/broadcast/x;

    invoke-direct {v0, p1, p0}, Ltv/periscope/android/ui/broadcast/x;-><init>(Landroid/content/Context;Ltv/periscope/android/ui/broadcast/y;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->a:Ltv/periscope/android/ui/broadcast/y$a;

    .line 36
    new-instance v0, Ltv/periscope/android/ui/broadcast/ab;

    invoke-direct {v0}, Ltv/periscope/android/ui/broadcast/ab;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->b:Ltv/periscope/android/ui/broadcast/y$a;

    .line 37
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/y;)Ltv/periscope/android/ui/broadcast/y$a;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->a:Ltv/periscope/android/ui/broadcast/y$a;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/ui/broadcast/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->b:Ltv/periscope/android/ui/broadcast/y$a;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcast/y$a;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/ui/broadcast/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->b:Ltv/periscope/android/ui/broadcast/y$a;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/y$a;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 68
    return-void
.end method

.method public a(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/n;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->b:Ltv/periscope/android/ui/broadcast/y$a;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/y$a;->a(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/n;)V

    .line 72
    return-void
.end method

.method public a(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/x$b;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->a:Ltv/periscope/android/ui/broadcast/y$a;

    check-cast v0, Ltv/periscope/android/ui/broadcast/x;

    invoke-virtual {v0, p2}, Ltv/periscope/android/ui/broadcast/x;->a(Ltv/periscope/android/ui/broadcast/x$b;)V

    .line 63
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->a:Ltv/periscope/android/ui/broadcast/y$a;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/y;->b:Ltv/periscope/android/ui/broadcast/y$a;

    invoke-interface {v1, p1}, Ltv/periscope/android/ui/broadcast/y$a;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/y$a;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 64
    return-void
.end method

.method public a(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/y$b;)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->b:Ltv/periscope/android/ui/broadcast/y$a;

    new-instance v1, Ltv/periscope/android/ui/broadcast/y$1;

    invoke-direct {v1, p0, p1, p2}, Ltv/periscope/android/ui/broadcast/y$1;-><init>(Ltv/periscope/android/ui/broadcast/y;Ljava/lang/String;Ltv/periscope/android/ui/broadcast/y$b;)V

    invoke-interface {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/y$a;->a(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/y$b;)V

    .line 55
    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/y;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method
