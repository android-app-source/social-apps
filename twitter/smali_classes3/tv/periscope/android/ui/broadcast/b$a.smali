.class Ltv/periscope/android/ui/broadcast/b$a;
.super Ltv/periscope/android/ui/broadcast/b$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private final c:Lcrz;

.field private final d:Ltv/periscope/android/ui/user/g;


# direct methods
.method constructor <init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Lcrz;Ltv/periscope/android/ui/user/g;)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0, p1, p2, p3, p4}, Ltv/periscope/android/ui/broadcast/b$b;-><init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;)V

    .line 188
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/b$a;->c:Lcrz;

    .line 189
    iput-object p6, p0, Ltv/periscope/android/ui/broadcast/b$a;->d:Ltv/periscope/android/ui/user/g;

    .line 190
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 194
    sget v0, Ltv/periscope/android/library/f$f;->ps__ic_block:I

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    sget v0, Ltv/periscope/android/library/f$l;->ps__action_sheet_label_block:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 199
    sget v0, Ltv/periscope/android/library/f$d;->ps__light_grey:I

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x1

    return v0
.end method

.method public f()Ltv/periscope/android/view/c;
    .locals 1

    .prologue
    .line 220
    sget-object v0, Ltv/periscope/android/view/c;->c:Ltv/periscope/android/view/c;

    return-object v0
.end method

.method protected g()V
    .locals 6

    .prologue
    .line 214
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/b$a;->c:Lcrz;

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->c(Lcrz;)V

    .line 215
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/b$a;->d:Ltv/periscope/android/ui/user/g;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/b$a;->b:Ltv/periscope/model/chat/Message;

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/b$a;->b:Ltv/periscope/model/chat/Message;

    invoke-virtual {v2}, Ltv/periscope/model/chat/Message;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/b$a;->b:Ltv/periscope/model/chat/Message;

    invoke-virtual {v3}, Ltv/periscope/model/chat/Message;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/b$a;->a:Ljava/lang/String;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/b$a;->b:Ltv/periscope/model/chat/Message;

    invoke-interface/range {v0 .. v5}, Ltv/periscope/android/ui/user/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/Message;)V

    .line 216
    return-void
.end method
