.class public Ltv/periscope/android/ui/broadcast/i$g;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Ltv/periscope/android/ui/broadcast/LiveStatsView;


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 1

    .prologue
    .line 774
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 776
    sget v0, Ltv/periscope/android/library/f$g;->presence_count:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/LiveStatsView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$g;->b:Ltv/periscope/android/ui/broadcast/LiveStatsView;

    .line 777
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$g;
    .locals 3

    .prologue
    .line 769
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_viewer_count:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 770
    new-instance v1, Ltv/periscope/android/ui/broadcast/i$g;

    invoke-direct {v1, v0, p2}, Ltv/periscope/android/ui/broadcast/i$g;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    return-object v1
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;)V
    .locals 4

    .prologue
    .line 781
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;->c()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    move-result-object v0

    sget-object v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;->a:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    if-ne v0, v1, :cond_1

    .line 782
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$g;->b:Ltv/periscope/android/ui/broadcast/LiveStatsView;

    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/LiveStatsView;->a(Ljava/lang/Long;)V

    .line 785
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$g;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 787
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;->c()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    move-result-object v1

    sget-object v2, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    if-ne v1, v2, :cond_2

    .line 788
    sget v1, Ltv/periscope/android/library/f$d;->ps__app_background_secondary:I

    .line 789
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$g;->b:Ltv/periscope/android/ui/broadcast/LiveStatsView;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Ltv/periscope/android/ui/broadcast/LiveStatsView;->setBackgroundColor(I)V

    .line 791
    :cond_2
    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 763
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/i$g;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;)V

    return-void
.end method
