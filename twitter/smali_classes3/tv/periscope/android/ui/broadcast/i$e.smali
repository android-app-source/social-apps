.class public Ltv/periscope/android/ui/broadcast/i$e;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$g;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 1

    .prologue
    .line 372
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 373
    sget v0, Ltv/periscope/android/library/f$g;->divider_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$e;->b:Landroid/widget/TextView;

    .line 374
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$e;
    .locals 3

    .prologue
    .line 364
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__list_divider:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 366
    new-instance v1, Ltv/periscope/android/ui/broadcast/i$e;

    invoke-direct {v1, v0, p2}, Ltv/periscope/android/ui/broadcast/i$e;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    return-object v1
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$g;)V
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$e;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$g;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 361
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$g;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/i$e;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$g;)V

    return-void
.end method
