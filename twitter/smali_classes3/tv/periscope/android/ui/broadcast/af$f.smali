.class final Ltv/periscope/android/ui/broadcast/af$f;
.super Ltv/periscope/android/ui/user/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/af;


# direct methods
.method public constructor <init>(Ltv/periscope/android/ui/broadcast/af;Ltv/periscope/android/api/ApiManager;Lcsa;Ldbr;)V
    .locals 0

    .prologue
    .line 2928
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af$f;->a:Ltv/periscope/android/ui/broadcast/af;

    .line 2929
    invoke-direct {p0, p2, p3, p4}, Ltv/periscope/android/ui/user/h;-><init>(Ltv/periscope/android/api/ApiManager;Lcsa;Ldbr;)V

    .line 2930
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/Message;)V
    .locals 2

    .prologue
    .line 2960
    invoke-super/range {p0 .. p5}, Ltv/periscope/android/ui/user/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/Message;)V

    .line 2961
    if-eqz p5, :cond_0

    invoke-virtual {p5}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2962
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$f;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/af;->f(Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/android/ui/chat/b;

    move-result-object v0

    invoke-virtual {p5}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/b;->c(Ljava/lang/String;)V

    .line 2963
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$f;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0, p5}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/model/chat/Message;)V

    .line 2965
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$f;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/af;->h(Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/model/af;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$f;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/af;->h(Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/model/af;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2968
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$f;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/af;->i(Ltv/periscope/android/ui/broadcast/af;)V

    .line 2970
    :cond_1
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$ReportType;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2935
    invoke-super {p0, p1, p2, p3}, Ltv/periscope/android/ui/user/h;->a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$ReportType;Ljava/lang/String;)V

    .line 2936
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2955
    :cond_0
    :goto_0
    return-void

    .line 2940
    :cond_1
    sget-object v0, Ltv/periscope/android/ui/broadcast/af$8;->e:[I

    invoke-virtual {p2}, Ltv/periscope/model/chat/MessageType$ReportType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2952
    :goto_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$f;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/af;->f(Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/android/ui/chat/b;

    move-result-object v0

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/chat/b;->c(Ljava/lang/String;)V

    .line 2953
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$f;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/model/chat/Message;->a(Ljava/lang/String;)Ltv/periscope/model/chat/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ltv/periscope/model/chat/Message;)V

    .line 2954
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$f;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/af;->g(Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/android/ui/broadcast/moderator/f;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$f;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, v1, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Ltv/periscope/android/ui/broadcast/moderator/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2943
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$f;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$ReportType;)V

    goto :goto_1

    .line 2940
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
