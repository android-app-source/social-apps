.class public Ltv/periscope/android/ui/broadcast/i$j;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "j"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$j;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Landroid/view/View;

.field private c:Ltv/periscope/model/q;

.field private final d:Ltv/periscope/android/ui/broadcast/StatsView;

.field private final e:Ltv/periscope/android/ui/broadcast/StatsView;

.field private final f:Ltv/periscope/android/ui/broadcast/StatsView;

.field private g:Landroid/view/View;

.field private final h:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field private final i:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field private final j:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field private k:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field private l:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field private m:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 3

    .prologue
    .line 639
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 640
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/i$j;->b:Landroid/view/View;

    .line 641
    sget v0, Ltv/periscope/android/library/f$g;->time_watched:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/StatsView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->d:Ltv/periscope/android/ui/broadcast/StatsView;

    .line 642
    sget v0, Ltv/periscope/android/library/f$g;->time_per_user:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/StatsView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->e:Ltv/periscope/android/ui/broadcast/StatsView;

    .line 643
    sget v0, Ltv/periscope/android/library/f$g;->duration:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/StatsView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->f:Ltv/periscope/android/ui/broadcast/StatsView;

    .line 644
    sget v0, Ltv/periscope/android/library/f$g;->custom_stats_padding:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->g:Landroid/view/View;

    .line 645
    sget v0, Ltv/periscope/android/library/f$d;->ps__primary_text:I

    iput v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->h:I

    .line 646
    sget v0, Ltv/periscope/android/library/f$d;->ps__red:I

    iput v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->i:I

    .line 647
    sget v0, Ltv/periscope/android/library/f$d;->ps__blue:I

    iput v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->j:I

    .line 649
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->d:Ltv/periscope/android/ui/broadcast/StatsView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__total_time_watched:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->h:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsView;->a(II)V

    .line 650
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->e:Ltv/periscope/android/ui/broadcast/StatsView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__time_per_viewer:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->h:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsView;->a(II)V

    .line 651
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->f:Ltv/periscope/android/ui/broadcast/StatsView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__duration:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->h:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsView;->a(II)V

    .line 652
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$j;
    .locals 3

    .prologue
    .line 655
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_stats_list:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 657
    new-instance v1, Ltv/periscope/android/ui/broadcast/i$j;

    invoke-direct {v1, v0, p2}, Ltv/periscope/android/ui/broadcast/i$j;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    return-object v1
.end method

.method private a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;Ltv/periscope/model/q;)V
    .locals 4

    .prologue
    .line 686
    if-nez p2, :cond_0

    .line 712
    :goto_0
    return-void

    .line 689
    :cond_0
    sget-object v0, Ltv/periscope/android/ui/broadcast/i$1;->b:[I

    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 691
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->d:Ltv/periscope/android/ui/broadcast/StatsView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$j;->c:Ltv/periscope/model/q;

    invoke-virtual {v1}, Ltv/periscope/model/q;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/StatsView;->setTime(J)V

    .line 692
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->d:Ltv/periscope/android/ui/broadcast/StatsView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__total_time_watched:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->h:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsView;->a(II)V

    .line 693
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->e:Ltv/periscope/android/ui/broadcast/StatsView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$j;->c:Ltv/periscope/model/q;

    invoke-virtual {v1}, Ltv/periscope/model/q;->h()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/StatsView;->setTime(J)V

    .line 694
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->e:Ltv/periscope/android/ui/broadcast/StatsView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__time_per_viewer:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->h:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsView;->a(II)V

    goto :goto_0

    .line 698
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->d:Ltv/periscope/android/ui/broadcast/StatsView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$j;->c:Ltv/periscope/model/q;

    invoke-virtual {v1}, Ltv/periscope/model/q;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/StatsView;->setTime(J)V

    .line 699
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->d:Ltv/periscope/android/ui/broadcast/StatsView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__total_time_watched_live:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->i:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsView;->a(II)V

    .line 700
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->e:Ltv/periscope/android/ui/broadcast/StatsView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$j;->c:Ltv/periscope/model/q;

    invoke-virtual {v1}, Ltv/periscope/model/q;->d()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/StatsView;->setTime(J)V

    .line 701
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->e:Ltv/periscope/android/ui/broadcast/StatsView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__time_per_viewer_live:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->i:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsView;->a(II)V

    goto :goto_0

    .line 705
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->d:Ltv/periscope/android/ui/broadcast/StatsView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$j;->c:Ltv/periscope/model/q;

    invoke-virtual {v1}, Ltv/periscope/model/q;->e()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/StatsView;->setTime(J)V

    .line 706
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->d:Ltv/periscope/android/ui/broadcast/StatsView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__total_time_watched_replay:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->j:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsView;->a(II)V

    .line 707
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->e:Ltv/periscope/android/ui/broadcast/StatsView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$j;->c:Ltv/periscope/model/q;

    invoke-virtual {v1}, Ltv/periscope/model/q;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/StatsView;->setTime(J)V

    .line 708
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->e:Ltv/periscope/android/ui/broadcast/StatsView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__time_per_viewer_replay:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->j:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsView;->a(II)V

    goto/16 :goto_0

    .line 689
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ltv/periscope/model/p;)V
    .locals 4

    .prologue
    .line 716
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0}, Ltv/periscope/model/p;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 717
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 718
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->f:Ltv/periscope/android/ui/broadcast/StatsView;

    invoke-static {v0, v1}, Ldag;->b(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ltv/periscope/android/ui/broadcast/StatsView;->setTime(Ljava/lang/String;)V

    .line 725
    :goto_0
    return-void

    .line 720
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Received negative duration for broadcast "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 721
    invoke-virtual {p1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", start: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/model/p;->m()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", end: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 722
    invoke-virtual {p1}, Ltv/periscope/model/p;->H()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 720
    invoke-static {v0}, Lf;->a(Ljava/lang/Throwable;)V

    .line 723
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->f:Ltv/periscope/android/ui/broadcast/StatsView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__abbrev_not_applicable:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/StatsView;->setTime(J)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$j;)V
    .locals 4

    .prologue
    .line 662
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 664
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$j;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 665
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$j;->d()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    move-result-object v2

    sget-object v3, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    if-ne v2, v3, :cond_0

    .line 666
    sget v2, Ltv/periscope/android/library/f$d;->ps__app_background_secondary:I

    iput v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->k:I

    .line 667
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->b:Landroid/view/View;

    iget v3, p0, Ltv/periscope/android/ui/broadcast/i$j;->k:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 669
    sget v2, Ltv/periscope/android/library/f$e;->ps__broadcast_info_margin_stats_viewer:I

    iput v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->m:I

    .line 670
    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->k:I

    iput v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->l:I

    .line 676
    :goto_0
    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->m:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 677
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$j;->g:Landroid/view/View;

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->l:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 679
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$j;->b()Ltv/periscope/model/q;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$j;->c:Ltv/periscope/model/q;

    .line 680
    iget-object v0, p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$j;->a:Ltv/periscope/model/p;

    .line 681
    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/i$j;->a(Ltv/periscope/model/p;)V

    .line 682
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$j;->c()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$j;->c:Ltv/periscope/model/q;

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/i$j;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;Ltv/periscope/model/q;)V

    .line 683
    return-void

    .line 672
    :cond_0
    sget v2, Ltv/periscope/android/library/f$e;->ps__broadcast_info_margin_stats_owner:I

    iput v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->m:I

    .line 673
    sget v2, Ltv/periscope/android/library/f$d;->ps__section_divider:I

    iput v2, p0, Ltv/periscope/android/ui/broadcast/i$j;->l:I

    goto :goto_0
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 621
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$j;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/i$j;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$j;)V

    return-void
.end method
