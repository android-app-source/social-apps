.class public Ltv/periscope/android/ui/broadcast/t;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/StatsGraphView$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$m;",
        ">;",
        "Ltv/periscope/android/ui/broadcast/StatsGraphView$a;"
    }
.end annotation


# instance fields
.field private final b:Ltv/periscope/android/ui/broadcast/StatsGraphView;

.field private c:Ltv/periscope/android/ui/broadcast/j;

.field private d:Ltv/periscope/model/p;


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 31
    sget v0, Ltv/periscope/android/library/f$g;->stats_graph:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/StatsGraphView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/t;->b:Ltv/periscope/android/ui/broadcast/StatsGraphView;

    .line 32
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/t;
    .locals 3

    .prologue
    .line 25
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_stats_graph_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 26
    new-instance v1, Ltv/periscope/android/ui/broadcast/t;

    invoke-direct {v1, v0, p2}, Ltv/periscope/android/ui/broadcast/t;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    return-object v1
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 63
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/t;->c:Ltv/periscope/android/ui/broadcast/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/t;->d:Ltv/periscope/model/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/t;->d:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/t;->d:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v0

    .line 67
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/t;->c:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/j;->f()Ltv/periscope/android/ui/broadcast/n;

    move-result-object v1

    .line 68
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ltv/periscope/android/ui/broadcast/n;->a(J)V

    .line 70
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/t;->b:Ltv/periscope/android/ui/broadcast/StatsGraphView;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/t;->c:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v3, v0}, Ltv/periscope/android/ui/broadcast/j;->c(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(Ltv/periscope/android/ui/broadcast/n;Ljava/lang/Long;)V

    .line 72
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/t;->c:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v2, v0, v1}, Ltv/periscope/android/ui/broadcast/j;->a(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/n;)V

    goto :goto_0
.end method

.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$m;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 36
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$m;->b()Ltv/periscope/android/ui/broadcast/j;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/t;->c:Ltv/periscope/android/ui/broadcast/j;

    .line 37
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/t;->c:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->c()Ltv/periscope/model/p;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/t;->d:Ltv/periscope/model/p;

    .line 38
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/t;->d:Ltv/periscope/model/p;

    if-nez v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/t;->b:Ltv/periscope/android/ui/broadcast/StatsGraphView;

    invoke-virtual {v0, p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->setDelegate(Ltv/periscope/android/ui/broadcast/StatsGraphView$a;)V

    .line 43
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/t;->c:Ltv/periscope/android/ui/broadcast/j;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/t;->d:Ltv/periscope/model/p;

    invoke-virtual {v1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/j;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 44
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/t;->d:Ltv/periscope/model/p;

    invoke-virtual {v1}, Ltv/periscope/model/p;->L()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/t;->d:Ltv/periscope/model/p;

    invoke-virtual {v1}, Ltv/periscope/model/p;->M()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 49
    :cond_2
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/t;->b:Ltv/periscope/android/ui/broadcast/StatsGraphView;

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->setBroadcastEnded(Z)V

    .line 50
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/t;->b:Ltv/periscope/android/ui/broadcast/StatsGraphView;

    invoke-virtual {v1, v0, v2}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(Ljava/util/List;Z)V

    goto :goto_0

    .line 54
    :cond_3
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/t;->b:Ltv/periscope/android/ui/broadcast/StatsGraphView;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    .line 57
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/t;->b:Ltv/periscope/android/ui/broadcast/StatsGraphView;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(Ljava/util/List;Z)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$m;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/t;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$m;)V

    return-void
.end method
