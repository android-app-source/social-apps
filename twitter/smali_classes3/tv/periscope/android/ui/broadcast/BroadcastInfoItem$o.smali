.class public Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "o"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem",
        "<",
        "Lcyw;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:Ljava/lang/String;

.field private final e:Ltv/periscope/android/ui/broadcast/j;


# direct methods
.method public constructor <init>(Ltv/periscope/android/ui/broadcast/j;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->e:Ltv/periscope/android/ui/broadcast/j;

    .line 214
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->a:Ljava/lang/String;

    .line 215
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->b:Ljava/lang/String;

    .line 216
    iput-boolean p4, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->c:Z

    .line 217
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->d:Ljava/lang/String;

    .line 218
    return-void
.end method


# virtual methods
.method public a()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;
    .locals 1

    .prologue
    .line 222
    sget-object v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;->f:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;

    return-object v0
.end method

.method public b()Lcyw;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->e:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->o()Lcyw;

    move-result-object v0

    return-object v0
.end method
