.class Ltv/periscope/android/ui/broadcast/am$d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/am;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/am;

.field private b:I

.field private c:F

.field private d:F

.field private e:F

.field private f:F


# direct methods
.method private constructor <init>(Ltv/periscope/android/ui/broadcast/am;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/am$d;->a:Ltv/periscope/android/ui/broadcast/am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ltv/periscope/android/ui/broadcast/am;Ltv/periscope/android/ui/broadcast/am$1;)V
    .locals 0

    .prologue
    .line 356
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/am$d;-><init>(Ltv/periscope/android/ui/broadcast/am;)V

    return-void
.end method


# virtual methods
.method a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 365
    if-eqz p1, :cond_0

    .line 366
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/am$d;->b:I

    .line 367
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/am$d;->c:F

    .line 368
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/am$d;->d:F

    .line 374
    :goto_0
    iget v0, p0, Ltv/periscope/android/ui/broadcast/am$d;->c:F

    iput v0, p0, Ltv/periscope/android/ui/broadcast/am$d;->e:F

    .line 375
    iget v0, p0, Ltv/periscope/android/ui/broadcast/am$d;->d:F

    iput v0, p0, Ltv/periscope/android/ui/broadcast/am$d;->f:F

    .line 376
    return-void

    .line 370
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/am$d;->b:I

    .line 371
    iput v1, p0, Ltv/periscope/android/ui/broadcast/am$d;->c:F

    .line 372
    iput v1, p0, Ltv/periscope/android/ui/broadcast/am$d;->d:F

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, -0x1

    const/high16 v5, 0x3f800000    # 1.0f

    .line 380
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/am$d;->a:Ltv/periscope/android/ui/broadcast/am;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/am;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 381
    const/4 v0, 0x0

    .line 421
    :cond_0
    :goto_0
    return v0

    .line 383
    :cond_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/am$d;->a:Ltv/periscope/android/ui/broadcast/am;

    invoke-static {v1}, Ltv/periscope/android/ui/broadcast/am;->a(Ltv/periscope/android/ui/broadcast/am;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 388
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 390
    :pswitch_0
    invoke-virtual {p0, p2}, Ltv/periscope/android/ui/broadcast/am$d;->a(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 394
    :pswitch_1
    iget v1, p0, Ltv/periscope/android/ui/broadcast/am$d;->b:I

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    .line 395
    if-eq v1, v2, :cond_0

    .line 396
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    .line 397
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    .line 399
    iget v3, p0, Ltv/periscope/android/ui/broadcast/am$d;->e:F

    sub-float/2addr v3, v2

    .line 400
    iget v4, p0, Ltv/periscope/android/ui/broadcast/am$d;->f:F

    sub-float/2addr v4, v1

    .line 401
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v5

    if-gez v3, :cond_2

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v5

    if-ltz v3, :cond_0

    .line 402
    :cond_2
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/am$d;->a:Ltv/periscope/android/ui/broadcast/am;

    iget v4, p0, Ltv/periscope/android/ui/broadcast/am$d;->d:F

    invoke-static {v3, v4, v1}, Ltv/periscope/android/ui/broadcast/am;->a(Ltv/periscope/android/ui/broadcast/am;FF)Z

    .line 403
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/am$d;->a:Ltv/periscope/android/ui/broadcast/am;

    iget v3, p0, Ltv/periscope/android/ui/broadcast/am$d;->e:F

    invoke-static {v1, v3, v2}, Ltv/periscope/android/ui/broadcast/am;->b(Ltv/periscope/android/ui/broadcast/am;FF)V

    .line 404
    iput v2, p0, Ltv/periscope/android/ui/broadcast/am$d;->e:F

    goto :goto_0

    .line 411
    :pswitch_2
    iget v1, p0, Ltv/periscope/android/ui/broadcast/am$d;->b:I

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    .line 412
    if-eq v1, v2, :cond_0

    .line 413
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/am$d;->a:Ltv/periscope/android/ui/broadcast/am;

    invoke-static {v1}, Ltv/periscope/android/ui/broadcast/am;->b(Ltv/periscope/android/ui/broadcast/am;)V

    goto :goto_0

    .line 388
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
