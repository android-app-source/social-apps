.class Ltv/periscope/android/ui/broadcast/aw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcya$b;
.implements Ltv/periscope/android/ui/broadcast/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/aw$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ltv/periscope/android/api/ApiManager;

.field private final d:Lcyn;

.field private final e:Ltv/periscope/android/view/b;

.field private final f:Ltv/periscope/android/view/aj;

.field private final g:Ltv/periscope/android/ui/user/g;

.field private final h:Ltv/periscope/android/player/e;

.field private final i:Ltv/periscope/android/ui/broadcast/ao;

.field private final j:Ltv/periscope/android/ui/broadcast/u;

.field private k:Z

.field private l:Z

.field private final m:Lcrz;

.field private final n:Ltv/periscope/android/ui/broadcast/ap;

.field private final o:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/SettingsDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lcxq;


# direct methods
.method constructor <init>(Ljava/lang/ref/WeakReference;Ltv/periscope/android/api/ApiManager;Lcyn;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;Ltv/periscope/android/ui/broadcast/ao;Ltv/periscope/android/ui/broadcast/u;Lcrz;Ltv/periscope/android/ui/broadcast/ap;Ljava/lang/ref/WeakReference;Lcxq;Ltv/periscope/android/player/e;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;",
            "Ltv/periscope/android/api/ApiManager;",
            "Lcyn;",
            "Ltv/periscope/android/view/b;",
            "Ltv/periscope/android/view/aj;",
            "Ltv/periscope/android/ui/user/g;",
            "Ltv/periscope/android/ui/broadcast/ao;",
            "Ltv/periscope/android/ui/broadcast/u;",
            "Lcrz;",
            "Ltv/periscope/android/ui/broadcast/ap;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/SettingsDelegate;",
            ">;",
            "Lcxq;",
            "Ltv/periscope/android/player/e;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/aw;->b:Ljava/lang/ref/WeakReference;

    .line 85
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    .line 86
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/aw;->c:Ltv/periscope/android/api/ApiManager;

    .line 87
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/aw;->d:Lcyn;

    .line 88
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/aw;->e:Ltv/periscope/android/view/b;

    .line 89
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/aw;->f:Ltv/periscope/android/view/aj;

    .line 90
    iput-object p6, p0, Ltv/periscope/android/ui/broadcast/aw;->g:Ltv/periscope/android/ui/user/g;

    .line 91
    iput-object p7, p0, Ltv/periscope/android/ui/broadcast/aw;->i:Ltv/periscope/android/ui/broadcast/ao;

    .line 92
    iput-object p8, p0, Ltv/periscope/android/ui/broadcast/aw;->j:Ltv/periscope/android/ui/broadcast/u;

    .line 93
    iput-object p9, p0, Ltv/periscope/android/ui/broadcast/aw;->m:Lcrz;

    .line 94
    iput-object p10, p0, Ltv/periscope/android/ui/broadcast/aw;->n:Ltv/periscope/android/ui/broadcast/ap;

    .line 95
    iput-object p11, p0, Ltv/periscope/android/ui/broadcast/aw;->o:Ljava/lang/ref/WeakReference;

    .line 96
    iput-object p12, p0, Ltv/periscope/android/ui/broadcast/aw;->p:Lcxq;

    .line 97
    iput-object p13, p0, Ltv/periscope/android/ui/broadcast/aw;->h:Ltv/periscope/android/player/e;

    .line 98
    iput-boolean p14, p0, Ltv/periscope/android/ui/broadcast/aw;->l:Z

    .line 99
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/aw;)Ltv/periscope/android/api/ApiManager;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->c:Ltv/periscope/android/api/ApiManager;

    return-object v0
.end method

.method private a(IILjava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 212
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 213
    if-nez v0, :cond_0

    .line 222
    :goto_0
    return-void

    .line 216
    :cond_0
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v1, v0, p1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 217
    invoke-virtual {v1, p2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 218
    invoke-virtual {v0, p3}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 219
    invoke-virtual {v0, p4, p5}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    .line 220
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ltv/periscope/model/AbuseType;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 226
    new-instance v0, Ltv/periscope/android/ui/broadcast/aw$1;

    invoke-direct {v0, p0, p1, p2}, Ltv/periscope/android/ui/broadcast/aw$1;-><init>(Ltv/periscope/android/ui/broadcast/aw;Ljava/lang/String;Ltv/periscope/model/AbuseType;)V

    return-object v0
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/aw;)Lcyn;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->d:Lcyn;

    return-object v0
.end method

.method private b(Landroid/net/Uri;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 402
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 403
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 407
    :cond_0
    :goto_0
    return-void

    .line 406
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method static synthetic c(Ltv/periscope/android/ui/broadcast/aw;)Ltv/periscope/android/ui/user/g;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->g:Ltv/periscope/android/ui/user/g;

    return-object v0
.end method

.method static synthetic d(Ltv/periscope/android/ui/broadcast/aw;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/aw;->l()V

    return-void
.end method

.method static synthetic e(Ltv/periscope/android/ui/broadcast/aw;)Lcxq;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->p:Lcxq;

    return-object v0
.end method

.method static synthetic f(Ltv/periscope/android/ui/broadcast/aw;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->o:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private i(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 198
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 199
    new-instance v0, Ldas;

    sget-object v1, Ltv/periscope/model/AbuseType;->a:Ltv/periscope/model/AbuseType;

    sget v2, Ltv/periscope/android/library/f$l;->ps__report_broadcast_reason_self_harm:I

    invoke-direct {v0, p1, p0, v1, v2}, Ldas;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/model/AbuseType;I)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    new-instance v0, Ldas;

    sget-object v1, Ltv/periscope/model/AbuseType;->b:Ltv/periscope/model/AbuseType;

    sget v2, Ltv/periscope/android/library/f$l;->ps__report_broadcast_reason_violence:I

    invoke-direct {v0, p1, p0, v1, v2}, Ldas;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/model/AbuseType;I)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    new-instance v0, Ldas;

    sget-object v1, Ltv/periscope/model/AbuseType;->c:Ltv/periscope/model/AbuseType;

    sget v2, Ltv/periscope/android/library/f$l;->ps__report_broadcast_reason_sexual_content:I

    invoke-direct {v0, p1, p0, v1, v2}, Ldas;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/model/AbuseType;I)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    new-instance v0, Ldas;

    sget-object v3, Ltv/periscope/model/AbuseType;->d:Ltv/periscope/model/AbuseType;

    sget v4, Ltv/periscope/android/library/f$l;->ps__report_broadcast_reason_dont_like:I

    sget v5, Ltv/periscope/android/library/f$d;->ps__main_primary:I

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Ldas;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/model/AbuseType;II)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->e:Ltv/periscope/android/view/b;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    sget v2, Ltv/periscope/android/library/f$l;->ps__report_broadcast_reason_snippet:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-interface {v0, v1, v6, v2, v3}, Ltv/periscope/android/view/b;->a(Ljava/lang/CharSequence;Ljava/util/List;J)V

    .line 208
    return-void
.end method

.method private j(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 235
    new-instance v0, Ltv/periscope/android/ui/broadcast/aw$2;

    invoke-direct {v0, p0, p1}, Ltv/periscope/android/ui/broadcast/aw$2;-><init>(Ltv/periscope/android/ui/broadcast/aw;Ljava/lang/String;)V

    return-object v0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->m:Lcrz;

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->d(Lcrz;)V

    .line 113
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->e:Ltv/periscope/android/view/b;

    invoke-interface {v0}, Ltv/periscope/android/view/b;->e()V

    .line 114
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->j:Ltv/periscope/android/ui/broadcast/u;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/u;->l()V

    .line 119
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/aw;->l()V

    .line 120
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 397
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/aw;->b(Landroid/net/Uri;)V

    .line 398
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/aw;->l()V

    .line 104
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->p:Lcxq;

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->p:Lcxq;

    invoke-interface {v0, p1}, Lcxq;->a(Ljava/lang/String;)V

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->i:Ltv/periscope/android/ui/broadcast/ao;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->i:Ltv/periscope/android/ui/broadcast/ao;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcast/ao;->f(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)V
    .locals 8

    .prologue
    .line 351
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 352
    new-instance v1, Ldan;

    invoke-direct {v1, p1, p0}, Ldan;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    .line 353
    new-instance v2, Ldao;

    invoke-direct {v2, p1, p0}, Ldao;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    .line 354
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    sget v2, Ltv/periscope/android/library/f$l;->ps__token:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 357
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Ltv/periscope/android/library/f$d;->ps__secondary_text:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 358
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Ltv/periscope/android/library/f$d;->ps__blue:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 359
    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    sget v5, Ltv/periscope/android/library/f$l;->ps__action_autodelete_confirmation_title:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 360
    new-instance v5, Ltv/periscope/android/ui/broadcast/aw$a;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Ltv/periscope/android/ui/broadcast/aw$a;-><init>(Ltv/periscope/android/ui/broadcast/aw;Ltv/periscope/android/ui/broadcast/aw$1;)V

    invoke-static {v1, v2, v3, v4, v5}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;IILjava/lang/String;Landroid/text/style/ClickableSpan;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 361
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/aw;->e:Ltv/periscope/android/view/b;

    invoke-interface {v2, v1, v0}, Ltv/periscope/android/view/b;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 362
    return-void
.end method

.method public a(Ljava/lang/String;Ltv/periscope/model/AbuseType;)V
    .locals 7

    .prologue
    .line 145
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->p:Lcxq;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->p:Lcxq;

    invoke-interface {v0, p1}, Lcxq;->b(Ljava/lang/String;)V

    .line 195
    :goto_0
    return-void

    .line 150
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/aw;->l()V

    .line 151
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->m:Lcrz;

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->e(Lcrz;)V

    .line 152
    if-nez p2, :cond_1

    .line 153
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/aw;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 157
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 158
    sget-object v1, Ltv/periscope/android/ui/broadcast/aw$4;->a:[I

    invoke-virtual {p2}, Ltv/periscope/model/AbuseType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 185
    sget v1, Ltv/periscope/android/library/f$m;->ps__ReportDialogStyle_Other:I

    sget v2, Ltv/periscope/android/library/f$l;->ps__report_broadcast_reason_dont_like:I

    const-string/jumbo v3, "%s\n\n%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget v6, Ltv/periscope/android/library/f$l;->ps__report_broadcast_dialog_dont_like_message:I

    .line 188
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget v6, Ltv/periscope/android/library/f$l;->ps__report_broadcast_dialog_dont_like_message_extra:I

    .line 189
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 187
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget v4, Ltv/periscope/android/library/f$l;->ps__block_dialog_btn_confirm:I

    .line 191
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/aw;->j(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v5

    move-object v0, p0

    .line 185
    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/broadcast/aw;->a(IILjava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    .line 160
    :pswitch_0
    sget v1, Ltv/periscope/android/library/f$m;->ps__ReportDialogStyle:I

    sget v2, Ltv/periscope/android/library/f$l;->ps__report_broadcast_dialog_self_harm_title:I

    sget v3, Ltv/periscope/android/library/f$l;->ps__report_broadcast_dialog_self_harm_message:I

    .line 162
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Ltv/periscope/android/library/f$l;->ps__report_broadcast_confirm:I

    sget-object v0, Ltv/periscope/model/AbuseType;->a:Ltv/periscope/model/AbuseType;

    .line 164
    invoke-direct {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/aw;->b(Ljava/lang/String;Ltv/periscope/model/AbuseType;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v5

    move-object v0, p0

    .line 160
    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/broadcast/aw;->a(IILjava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    .line 168
    :pswitch_1
    sget v1, Ltv/periscope/android/library/f$m;->ps__ReportDialogStyle:I

    sget v2, Ltv/periscope/android/library/f$l;->ps__report_broadcast_dialog_violence_title:I

    sget v3, Ltv/periscope/android/library/f$l;->ps__report_broadcast_dialog_violence_message:I

    .line 170
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Ltv/periscope/android/library/f$l;->ps__report_broadcast_confirm:I

    sget-object v0, Ltv/periscope/model/AbuseType;->b:Ltv/periscope/model/AbuseType;

    .line 172
    invoke-direct {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/aw;->b(Ljava/lang/String;Ltv/periscope/model/AbuseType;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v5

    move-object v0, p0

    .line 168
    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/broadcast/aw;->a(IILjava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    .line 176
    :pswitch_2
    sget v1, Ltv/periscope/android/library/f$m;->ps__ReportDialogStyle:I

    sget v2, Ltv/periscope/android/library/f$l;->ps__report_broadcast_dialog_sexual_content_title:I

    sget v3, Ltv/periscope/android/library/f$l;->ps__report_broadcast_dialog_sexual_content_message:I

    .line 178
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Ltv/periscope/android/library/f$l;->ps__report_broadcast_confirm:I

    sget-object v0, Ltv/periscope/model/AbuseType;->c:Ltv/periscope/model/AbuseType;

    .line 180
    invoke-direct {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/aw;->b(Ljava/lang/String;Ltv/periscope/model/AbuseType;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v5

    move-object v0, p0

    .line 176
    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/broadcast/aw;->a(IILjava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ltv/periscope/android/view/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 318
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->e:Ltv/periscope/android/view/b;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ltv/periscope/android/view/b;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 319
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 303
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/aw;->k:Z

    .line 304
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->j:Ltv/periscope/android/ui/broadcast/u;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/u;->m()V

    .line 125
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/aw;->l()V

    .line 126
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->c:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->increaseBroadcastRank(Ljava/lang/String;)Ljava/lang/String;

    .line 251
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/aw;->l()V

    .line 252
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->c:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->decreaseBroadcastRank(Ljava/lang/String;)Ljava/lang/String;

    .line 257
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/aw;->l()V

    .line 258
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->j:Ltv/periscope/android/ui/broadcast/u;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/u;->n()Z

    move-result v0

    return v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 262
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 263
    if-nez v0, :cond_0

    .line 287
    :goto_0
    return-void

    .line 266
    :cond_0
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    sget v3, Ltv/periscope/android/library/f$l;->ps__delete_broadcast_dialog:I

    .line 267
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__delete_broadcast_cancel:I

    const/4 v3, 0x0

    .line 268
    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__delete_broadcast_confirm:I

    new-instance v3, Ltv/periscope/android/ui/broadcast/aw$3;

    invoke-direct {v3, p0, p1, v0}, Ltv/periscope/android/ui/broadcast/aw$3;-><init>(Ltv/periscope/android/ui/broadcast/aw;Ljava/lang/String;Landroid/app/Activity;)V

    .line 269
    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 286
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 298
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/aw;->k:Z

    return v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 291
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/aw;->a(Z)V

    .line 292
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Ltv/periscope/android/util/g;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 293
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/aw;->l()V

    .line 294
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/aw;->l:Z

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->n:Ltv/periscope/android/ui/broadcast/ap;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->n:Ltv/periscope/android/ui/broadcast/ap;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ap;->R()V

    .line 326
    :cond_0
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 313
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->f:Ltv/periscope/android/view/aj;

    new-instance v1, Ltv/periscope/android/ui/d;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Ltv/periscope/android/ui/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltv/periscope/android/view/aj;->a(Ljava/lang/Object;)V

    .line 314
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->n:Ltv/periscope/android/ui/broadcast/ap;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->n:Ltv/periscope/android/ui/broadcast/ap;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ap;->S()V

    .line 333
    :cond_0
    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->c:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->markBroadcastPersistent(Ljava/lang/String;)V

    .line 382
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/aw;->l()V

    .line 383
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->n:Ltv/periscope/android/ui/broadcast/ap;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->n:Ltv/periscope/android/ui/broadcast/ap;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ap;->T()V

    .line 340
    :cond_0
    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 387
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    invoke-static {v0}, Ltv/periscope/android/util/j;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/aw;->h:Ltv/periscope/android/player/e;

    .line 389
    invoke-interface {v1}, Ltv/periscope/android/player/e;->l()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Ltv/periscope/android/library/e;->a(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    .line 388
    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/aw;->b(Landroid/net/Uri;)V

    .line 393
    :goto_0
    return-void

    .line 391
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    const-string/jumbo v3, "broadcast_info_cta_deeplink"

    new-instance v4, Ltv/periscope/android/branch/api/BranchApiClient;

    invoke-direct {v4}, Ltv/periscope/android/branch/api/BranchApiClient;-><init>()V

    move-object v2, v1

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Ltv/periscope/android/library/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/branch/api/BranchApiClient;Lcya$b;)V

    goto :goto_0
.end method

.method public i()V
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->n:Ltv/periscope/android/ui/broadcast/ap;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->n:Ltv/periscope/android/ui/broadcast/ap;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ap;->U()V

    .line 347
    :cond_0
    return-void
.end method

.method public j()V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 411
    sget-object v0, Ltv/periscope/android/ui/broadcast/aw$4;->b:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 440
    :goto_0
    return-void

    .line 413
    :pswitch_0
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__report_broadcast_success:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 416
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__report_broadcast_error:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 421
    :pswitch_1
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__delete_broadcast_success:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 423
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->c:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->megaBroadcastCall()Ljava/lang/String;

    goto :goto_0

    .line 425
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__delete_broadcast_error:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 430
    :pswitch_2
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 431
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/api/AdjustBroadcastRankResponse;

    .line 432
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    iget-object v0, v0, Ltv/periscope/android/api/AdjustBroadcastRankResponse;->summary:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 435
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw;->a:Landroid/content/Context;

    const-string/jumbo v1, "Sorry, we could not adjust the broadcast rank"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 411
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
