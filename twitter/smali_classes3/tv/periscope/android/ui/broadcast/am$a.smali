.class Ltv/periscope/android/ui/broadcast/am$a;
.super Ltv/periscope/android/ui/broadcast/am$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/am;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/am;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/am;Ltv/periscope/android/api/ThumbnailPlaylistItem;)V
    .locals 0

    .prologue
    .line 447
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/am$a;->a:Ltv/periscope/android/ui/broadcast/am;

    .line 448
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/am$b;-><init>(Ltv/periscope/android/ui/broadcast/am;Ltv/periscope/android/api/ThumbnailPlaylistItem;)V

    .line 449
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 453
    invoke-super {p0, p1}, Ltv/periscope/android/ui/broadcast/am$b;->a(Landroid/graphics/drawable/Drawable;)V

    .line 456
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am$a;->a:Ltv/periscope/android/ui/broadcast/am;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/am;->a(Ltv/periscope/android/ui/broadcast/am;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am$a;->a:Ltv/periscope/android/ui/broadcast/am;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/am;->d(Ltv/periscope/android/ui/broadcast/am;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am$a;->a:Ltv/periscope/android/ui/broadcast/am;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/am;->d(Ltv/periscope/android/ui/broadcast/am;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/am$a;->b:Ltv/periscope/android/api/ThumbnailPlaylistItem;

    iget-object v1, v1, Ltv/periscope/android/api/ThumbnailPlaylistItem;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 457
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am$a;->a:Ltv/periscope/android/ui/broadcast/am;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/am;->e(Ltv/periscope/android/ui/broadcast/am;)Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    move-result-object v0

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 459
    :cond_1
    return-void
.end method
