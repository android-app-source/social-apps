.class Ltv/periscope/android/ui/broadcast/BottomTray$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/broadcast/BottomTray;->a(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/BottomTray;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/BottomTray;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/BottomTray$1;->a:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray$1;->a:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->a(Ltv/periscope/android/ui/broadcast/BottomTray;)Ltv/periscope/android/ui/chat/ChatState;

    move-result-object v0

    if-nez v0, :cond_1

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    sget-object v0, Ltv/periscope/android/ui/broadcast/BottomTray$5;->a:[I

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray$1;->a:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-static {v1}, Ltv/periscope/android/ui/broadcast/BottomTray;->a(Ltv/periscope/android/ui/broadcast/BottomTray;)Ltv/periscope/android/ui/chat/ChatState;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/android/ui/chat/ChatState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 153
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray$1;->a:Ltv/periscope/android/ui/broadcast/BottomTray;

    sget v1, Ltv/periscope/android/library/f$l;->ps__broadcast_too_full_dialog_title:I

    sget v2, Ltv/periscope/android/library/f$l;->ps__broadcast_too_full_dialog_message:I

    invoke-static {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/BottomTray;->a(Ltv/periscope/android/ui/broadcast/BottomTray;II)V

    goto :goto_0

    .line 158
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray$1;->a:Ltv/periscope/android/ui/broadcast/BottomTray;

    sget v1, Ltv/periscope/android/library/f$l;->ps__broadcast_limited_dialog_title:I

    sget v2, Ltv/periscope/android/library/f$l;->ps__broadcast_limited_dialog_message:I

    invoke-static {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/BottomTray;->a(Ltv/periscope/android/ui/broadcast/BottomTray;II)V

    goto :goto_0

    .line 163
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray$1;->a:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->c()V

    goto :goto_0

    .line 167
    :pswitch_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray$1;->a:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->b(Ltv/periscope/android/ui/broadcast/BottomTray;)Ltv/periscope/android/ui/chat/ag;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray$1;->a:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->c(Ltv/periscope/android/ui/broadcast/BottomTray;)Ltv/periscope/model/chat/Message;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray$1;->a:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->b(Ltv/periscope/android/ui/broadcast/BottomTray;)Ltv/periscope/android/ui/chat/ag;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray$1;->a:Ltv/periscope/android/ui/broadcast/BottomTray;

    invoke-static {v1}, Ltv/periscope/android/ui/broadcast/BottomTray;->c(Ltv/periscope/android/ui/broadcast/BottomTray;)Ltv/periscope/model/chat/Message;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/ag;->b(Ltv/periscope/model/chat/Message;)V

    goto :goto_0

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
