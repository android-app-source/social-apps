.class public final enum Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/BottomTray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ComposeState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

.field public static final enum b:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

.field public static final enum c:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

.field private static final synthetic d:[Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 462
    new-instance v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    const-string/jumbo v1, "Hidden"

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->a:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    .line 463
    new-instance v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    const-string/jumbo v1, "Close"

    invoke-direct {v0, v1, v3}, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->b:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    .line 464
    new-instance v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    const-string/jumbo v1, "Send"

    invoke-direct {v0, v1, v4}, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->c:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    .line 461
    const/4 v0, 0x3

    new-array v0, v0, [Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    sget-object v1, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->a:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    aput-object v1, v0, v2

    sget-object v1, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->b:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    aput-object v1, v0, v3

    sget-object v1, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->c:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    aput-object v1, v0, v4

    sput-object v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->d:[Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 461
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;
    .locals 1

    .prologue
    .line 461
    const-class v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    return-object v0
.end method

.method public static values()[Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;
    .locals 1

    .prologue
    .line 461
    sget-object v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->d:[Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    invoke-virtual {v0}, [Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    return-object v0
.end method
