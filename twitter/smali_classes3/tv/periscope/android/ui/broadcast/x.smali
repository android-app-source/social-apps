.class public Ltv/periscope/android/ui/broadcast/x;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/y$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/x$a;,
        Ltv/periscope/android/ui/broadcast/x$c;,
        Ltv/periscope/android/ui/broadcast/x$b;
    }
.end annotation


# instance fields
.field a:Landroid/os/Handler;

.field private final b:Landroid/content/Context;

.field private final c:Ltv/periscope/android/ui/broadcast/x$c;

.field private final d:Ltv/periscope/android/ui/broadcast/y;

.field private e:Ltv/periscope/android/ui/broadcast/x$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ltv/periscope/android/ui/broadcast/y;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/x;->b:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/x;->d:Ltv/periscope/android/ui/broadcast/y;

    .line 46
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "Read/write Thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 47
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 49
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 50
    new-instance v1, Ltv/periscope/android/ui/broadcast/x$c;

    invoke-direct {v1, p0, p0, v0}, Ltv/periscope/android/ui/broadcast/x$c;-><init>(Ltv/periscope/android/ui/broadcast/x;Ltv/periscope/android/ui/broadcast/x;Landroid/os/Looper;)V

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/x;->c:Ltv/periscope/android/ui/broadcast/x$c;

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/x;->a:Landroid/os/Handler;

    .line 52
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/x;Ljava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/x;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/x;)Ltv/periscope/android/ui/broadcast/x$b;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/x;->e:Ltv/periscope/android/ui/broadcast/x$b;

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/x;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/x;->b(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/ui/broadcast/n;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/16 v8, 0x66

    .line 205
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 209
    :try_start_0
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/x;->b:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 210
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 212
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 213
    invoke-static {}, Ltv/periscope/android/ui/broadcast/n;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v4

    if-nez v4, :cond_0

    .line 229
    invoke-static {v2}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 230
    invoke-static {v3}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 231
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/x;->c:Ltv/periscope/android/ui/broadcast/x$c;

    invoke-virtual {v1, v8}, Ltv/periscope/android/ui/broadcast/x$c;->removeMessages(I)V

    .line 233
    :goto_0
    return-object v0

    .line 217
    :cond_0
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 218
    const-string/jumbo v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 219
    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 220
    const/4 v5, 0x1

    aget-object v0, v0, v5

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 221
    new-instance v0, Ltv/periscope/android/ui/broadcast/n;

    int-to-long v4, v4

    invoke-direct {v0, v4, v5, v6, v7}, Ltv/periscope/android/ui/broadcast/n;-><init>(JJ)V

    .line 222
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_1

    .line 224
    :catch_0
    move-exception v0

    move-object v0, v2

    move-object v2, v3

    .line 225
    :goto_2
    :try_start_4
    const-string/jumbo v3, "DiskPointCache"

    const-string/jumbo v4, "Broadcast file with graph data points was not found"

    invoke-static {v3, v4}, Ltv/periscope/android/util/t;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 229
    invoke-static {v0}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 230
    invoke-static {v2}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 231
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/x;->c:Ltv/periscope/android/ui/broadcast/x$c;

    invoke-virtual {v0, v8}, Ltv/periscope/android/ui/broadcast/x$c;->removeMessages(I)V

    :goto_3
    move-object v0, v1

    .line 233
    goto :goto_0

    .line 229
    :cond_1
    invoke-static {v2}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 230
    invoke-static {v3}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 231
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/x;->c:Ltv/periscope/android/ui/broadcast/x$c;

    invoke-virtual {v0, v8}, Ltv/periscope/android/ui/broadcast/x$c;->removeMessages(I)V

    goto :goto_3

    .line 226
    :catch_1
    move-exception v2

    move-object v2, v0

    move-object v3, v0

    .line 227
    :goto_4
    :try_start_5
    const-string/jumbo v0, "DiskPointCache"

    const-string/jumbo v4, "Unexpected error occurred"

    invoke-static {v0, v4}, Ltv/periscope/android/util/t;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 229
    invoke-static {v2}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 230
    invoke-static {v3}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 231
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/x;->c:Ltv/periscope/android/ui/broadcast/x$c;

    invoke-virtual {v0, v8}, Ltv/periscope/android/ui/broadcast/x$c;->removeMessages(I)V

    goto :goto_3

    .line 229
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    :goto_5
    invoke-static {v2}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 230
    invoke-static {v3}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 231
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/x;->c:Ltv/periscope/android/ui/broadcast/x$c;

    invoke-virtual {v1, v8}, Ltv/periscope/android/ui/broadcast/x$c;->removeMessages(I)V

    throw v0

    .line 229
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_5

    :catchall_2
    move-exception v0

    goto :goto_5

    :catchall_3
    move-exception v1

    move-object v3, v2

    move-object v2, v0

    move-object v0, v1

    goto :goto_5

    .line 226
    :catch_2
    move-exception v2

    move-object v2, v0

    goto :goto_4

    :catch_3
    move-exception v0

    goto :goto_4

    .line 224
    :catch_4
    move-exception v2

    move-object v2, v0

    goto :goto_2

    :catch_5
    move-exception v2

    move-object v2, v3

    goto :goto_2
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/x;)Ltv/periscope/android/ui/broadcast/y;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/x;->d:Ltv/periscope/android/ui/broadcast/y;

    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/ui/broadcast/n;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 170
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/x;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "graph"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 171
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 172
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 173
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 175
    :cond_0
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 177
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 182
    :try_start_0
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/x;->b:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 183
    :try_start_1
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/OutputStreamWriter;

    invoke-direct {v3, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 185
    :try_start_2
    invoke-static {}, Ltv/periscope/android/ui/broadcast/n;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 186
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->newLine()V

    .line 188
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 189
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/n;

    .line 191
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/n;->c()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/n;->b()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 192
    invoke-virtual {v1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 193
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->newLine()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    move-object v0, v1

    move-object v1, v2

    .line 196
    :goto_1
    :try_start_3
    const-string/jumbo v2, "DiskPointCache"

    const-string/jumbo v3, "Broadcast file with graph data points could not be found"

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 198
    invoke-static {v0}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 199
    invoke-static {v1}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 201
    :goto_2
    return-void

    .line 198
    :cond_1
    invoke-static {v1}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 199
    invoke-static {v2}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    goto :goto_2

    .line 198
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    :goto_3
    invoke-static {v1}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 199
    invoke-static {v2}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    throw v0

    .line 198
    :catchall_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_3

    :catchall_2
    move-exception v0

    goto :goto_3

    :catchall_3
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_3

    .line 195
    :catch_1
    move-exception v1

    move-object v1, v0

    goto :goto_1

    :catch_2
    move-exception v1

    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/ui/broadcast/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/x;->c:Ltv/periscope/android/ui/broadcast/x$c;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/x$c;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 139
    const/16 v1, 0x66

    iput v1, v0, Landroid/os/Message;->what:I

    .line 140
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 141
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 142
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/ui/broadcast/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/x;->e:Ltv/periscope/android/ui/broadcast/x$b;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/x;->e:Ltv/periscope/android/ui/broadcast/x$b;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/x$b;->a()V

    .line 133
    :cond_1
    :goto_0
    return-void

    .line 129
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/x;->c:Ltv/periscope/android/ui/broadcast/x$c;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/x$c;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 130
    const/16 v1, 0x65

    iput v1, v0, Landroid/os/Message;->what:I

    .line 131
    new-instance v1, Ltv/periscope/android/ui/broadcast/x$a;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v1, p1, v2}, Ltv/periscope/android/ui/broadcast/x$a;-><init>(Ljava/lang/String;Ljava/util/List;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 132
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/n;)V
    .locals 0

    .prologue
    .line 239
    return-void
.end method

.method public a(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/y$b;)V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/x;->c:Ltv/periscope/android/ui/broadcast/x$c;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/x$c;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 148
    const/16 v1, 0x66

    iput v1, v0, Landroid/os/Message;->what:I

    .line 149
    new-instance v1, Ltv/periscope/android/ui/broadcast/x$a;

    invoke-direct {v1, p1, p2}, Ltv/periscope/android/ui/broadcast/x$a;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/y$b;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 150
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 151
    return-void
.end method

.method public a(Ltv/periscope/android/ui/broadcast/x$b;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/x;->e:Ltv/periscope/android/ui/broadcast/x$b;

    .line 60
    return-void
.end method
