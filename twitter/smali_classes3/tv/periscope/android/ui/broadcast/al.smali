.class Ltv/periscope/android/ui/broadcast/al;
.super Ltv/periscope/android/ui/broadcast/av;
.source "Twttr"


# static fields
.field private static final g:J


# instance fields
.field private final h:Ltv/periscope/android/player/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ltv/periscope/android/ui/broadcast/al;->g:J

    return-void
.end method

.method constructor <init>(Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/video/StreamMode;Ltv/periscope/android/player/d;)V
    .locals 12

    .prologue
    .line 31
    sget-object v9, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v1 .. v11}, Ltv/periscope/android/ui/broadcast/av;-><init>(Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/video/StreamMode;)V

    .line 33
    move-object/from16 v0, p10

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/al;->h:Ltv/periscope/android/player/d;

    .line 34
    return-void
.end method


# virtual methods
.method a(Ltv/periscope/model/af;)V
    .locals 5

    .prologue
    .line 49
    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Ltv/periscope/model/p;->L()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ltv/periscope/model/p;->N()Z

    move-result v1

    if-nez v1, :cond_0

    .line 51
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/al;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/av$a;->Q()V

    .line 56
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-virtual {p1}, Ltv/periscope/model/af;->k()Ltv/periscope/model/StreamType;

    move-result-object v1

    sget-object v2, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/al;->f:Lretrofit/RestAdapter$LogLevel;

    invoke-virtual {v0}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v1, v2, v3, v4}, Ltv/periscope/android/ui/broadcast/al;->a(Ltv/periscope/model/StreamType;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ljava/lang/String;)V

    .line 54
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/al;->d:Ltv/periscope/android/api/ApiManager;

    invoke-virtual {v0}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ltv/periscope/android/api/ApiManager;->replayThumbnailPlaylist(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/al;->c:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/al;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 39
    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/al;->a(Ltv/periscope/model/p;)V

    .line 41
    invoke-virtual {v0}, Ltv/periscope/model/p;->N()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    invoke-virtual {v0}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/al;->a(Ljava/lang/String;)V

    .line 45
    :cond_0
    return-void
.end method

.method public h()V
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/al;->h:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->d()J

    move-result-wide v0

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/al;->h:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->l()J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget-wide v2, Ltv/periscope/android/ui/broadcast/al;->g:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 63
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/al;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/av$a;->Q()V

    .line 65
    :cond_0
    return-void
.end method
