.class Ltv/periscope/android/ui/broadcast/l$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/l;

.field private b:Z


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/l;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/l$a;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/l$a;Z)Z
    .locals 0

    .prologue
    .line 212
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/l$a;->b:Z

    return p1
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 220
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l$a;->b:Z

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l$a;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/l;->d(Ltv/periscope/android/ui/broadcast/l;)V

    .line 225
    :goto_0
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l$a;->a:Ltv/periscope/android/ui/broadcast/l;

    monitor-enter v1

    .line 226
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l$a;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/l;->e(Ltv/periscope/android/ui/broadcast/l;)V

    .line 227
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l$a;->a:Ltv/periscope/android/ui/broadcast/l;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ltv/periscope/android/ui/broadcast/l;->b(Ltv/periscope/android/ui/broadcast/l;Z)Z

    .line 228
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l$a;->a:Ltv/periscope/android/ui/broadcast/l;

    const/4 v2, 0x0

    iput-boolean v2, v0, Ltv/periscope/android/ui/broadcast/l;->a:Z

    .line 229
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l$a;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/l;->f(Ltv/periscope/android/ui/broadcast/l;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l$a;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/l;->g(Ltv/periscope/android/ui/broadcast/l;)V

    .line 231
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l$a;->a:Ltv/periscope/android/ui/broadcast/l;

    const-wide/16 v2, 0x0

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/l$a;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-static {v4}, Ltv/periscope/android/ui/broadcast/l;->h(Ltv/periscope/android/ui/broadcast/l;)Z

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/periscope/android/ui/broadcast/l;->a(JZ)V

    .line 233
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    return-void

    .line 223
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l$a;->a:Ltv/periscope/android/ui/broadcast/l;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ltv/periscope/android/ui/broadcast/l;->a(Ltv/periscope/android/ui/broadcast/l;Z)Z

    goto :goto_0

    .line 233
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
