.class Ltv/periscope/android/ui/broadcast/af$7;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Ltv/periscope/android/time/api/TimeZoneClient$a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/af;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/af;)V
    .locals 0

    .prologue
    .line 2907
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af$7;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ltv/periscope/android/time/api/TimeZoneClient$a;Lretrofit/client/Response;)V
    .locals 5

    .prologue
    .line 2911
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$7;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ltv/periscope/android/time/api/TimeZoneClient$a;->a()Ljava/util/TimeZone;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2912
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$7;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    .line 2913
    :goto_0
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af$7;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v2, v2, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af$7;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v3, v3, Ltv/periscope/android/ui/broadcast/af;->b:Ltv/periscope/android/player/e;

    invoke-virtual {p1}, Ltv/periscope/android/time/api/TimeZoneClient$a;->a()Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0, v1}, Ltv/periscope/android/ui/broadcast/g;->a(Ltv/periscope/android/player/e;Ljava/util/TimeZone;J)V

    .line 2917
    :goto_1
    return-void

    .line 2912
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$7;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->m()J

    move-result-wide v0

    goto :goto_0

    .line 2915
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$7;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->b()V

    goto :goto_1
.end method

.method public failure(Lretrofit/RetrofitError;)V
    .locals 1

    .prologue
    .line 2921
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$7;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->b()V

    .line 2922
    return-void
.end method

.method public synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 2907
    check-cast p1, Ltv/periscope/android/time/api/TimeZoneClient$a;

    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/af$7;->a(Ltv/periscope/android/time/api/TimeZoneClient$a;Lretrofit/client/Response;)V

    return-void
.end method
