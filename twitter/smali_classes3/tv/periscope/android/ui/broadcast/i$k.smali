.class public Ltv/periscope/android/ui/broadcast/i$k;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "k"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$l;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/TextView;

.field private f:Ldaj;


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 2

    .prologue
    .line 320
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 321
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/i$k;->b:Landroid/view/View;

    .line 322
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->b:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->icon:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->d:Landroid/widget/ImageView;

    .line 323
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->b:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->label:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->c:Landroid/widget/TextView;

    .line 324
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->b:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->description:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->e:Landroid/widget/TextView;

    .line 325
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$k;
    .locals 3

    .prologue
    .line 314
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_action_item:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 316
    new-instance v1, Ltv/periscope/android/ui/broadcast/i$k;

    invoke-direct {v1, v0, p2}, Ltv/periscope/android/ui/broadcast/i$k;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    return-object v1
.end method

.method private a(Ldaj;)V
    .locals 3

    .prologue
    .line 347
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldaj;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldaj;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 349
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$k;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 350
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 355
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$k;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, Ldaj;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 356
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$k;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, Ldaj;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 357
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->c:Landroid/widget/TextView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$k;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Ldaj;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    return-void

    .line 352
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$l;)V
    .locals 1

    .prologue
    .line 330
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$l;->b()Ldaj;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->f:Ldaj;

    .line 331
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->f:Ldaj;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->f:Ldaj;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/i$k;->a(Ldaj;)V

    .line 334
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 303
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$l;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/i$k;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$l;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 338
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 339
    sget v1, Ltv/periscope/android/library/f$g;->broadcast_action_item:I

    if-ne v0, v1, :cond_0

    .line 340
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->f:Ldaj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->f:Ldaj;

    invoke-virtual {v0}, Ldaj;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$k;->f:Ldaj;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/i$k;->a(Ldaj;)V

    .line 344
    :cond_0
    return-void
.end method
