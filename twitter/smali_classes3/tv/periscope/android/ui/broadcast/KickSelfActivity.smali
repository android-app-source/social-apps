.class public Ltv/periscope/android/ui/broadcast/KickSelfActivity;
.super Ltv/periscope/android/ui/BaseActivity;
.source "Twttr"


# instance fields
.field private a:Ldbp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ltv/periscope/android/ui/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/KickSelfActivity;Ldbp;)Ldbp;
    .locals 0

    .prologue
    .line 15
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/KickSelfActivity;->a:Ldbp;

    return-object p1
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string/jumbo v0, "Kick Self"

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 23
    invoke-super {p0, p1}, Ltv/periscope/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/KickSelfActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "e_message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ltv/periscope/model/chat/Message;

    .line 27
    new-instance v5, Ltv/periscope/android/ui/broadcast/KickSelfActivity$1;

    invoke-direct {v5, p0}, Ltv/periscope/android/ui/broadcast/KickSelfActivity$1;-><init>(Ltv/periscope/android/ui/broadcast/KickSelfActivity;)V

    .line 33
    new-instance v6, Ltv/periscope/android/ui/broadcast/KickSelfActivity$2;

    invoke-direct {v6, p0}, Ltv/periscope/android/ui/broadcast/KickSelfActivity$2;-><init>(Ltv/periscope/android/ui/broadcast/KickSelfActivity;)V

    .line 41
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-static {}, Ltv/periscope/android/library/d;->a()Ltv/periscope/android/library/d;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/library/d;->c()Ltv/periscope/android/library/c;

    move-result-object v1

    .line 43
    new-instance v0, Ldbn;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v2

    invoke-interface {v1}, Ltv/periscope/android/library/c;->v()Ldae;

    move-result-object v3

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Ldbn;-><init>(Landroid/content/Context;Lcyw;Ldae;Ltv/periscope/model/chat/Message;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/KickSelfActivity;->a:Ldbp;

    .line 48
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/KickSelfActivity;->a:Ldbp;

    invoke-virtual {v0}, Ldbp;->a()V

    .line 49
    return-void

    .line 46
    :cond_0
    new-instance v0, Ldbm;

    invoke-direct {v0, p0, v5, v6}, Ldbm;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/KickSelfActivity;->a:Ldbp;

    goto :goto_0
.end method
