.class public final enum Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/ChatRoomView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BottomTrayState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

.field public static final enum b:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

.field public static final enum c:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

.field public static final enum d:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

.field public static final enum e:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

.field public static final enum f:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

.field public static final enum g:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

.field private static final synthetic h:[Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 106
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v3}, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->a:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    .line 107
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    const-string/jumbo v1, "PARTICIPANTS_ONLY"

    invoke-direct {v0, v1, v4}, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    .line 108
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    const-string/jumbo v1, "BROADCASTER"

    invoke-direct {v0, v1, v5}, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->c:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    .line 109
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    const-string/jumbo v1, "NO_COMPOSER"

    invoke-direct {v0, v1, v6}, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->d:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    .line 110
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    const-string/jumbo v1, "CHAT_DEFAULT"

    invoke-direct {v0, v1, v7}, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->e:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    .line 111
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    const-string/jumbo v1, "REPLAY_PLAYING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    .line 112
    new-instance v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    const-string/jumbo v1, "REPLAY_PAUSED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->g:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    .line 105
    const/4 v0, 0x7

    new-array v0, v0, [Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->a:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    aput-object v1, v0, v3

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    aput-object v1, v0, v4

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->c:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    aput-object v1, v0, v5

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->d:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    aput-object v1, v0, v6

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->e:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->g:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    aput-object v2, v0, v1

    sput-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->h:[Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;
    .locals 1

    .prologue
    .line 105
    const-class v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    return-object v0
.end method

.method public static values()[Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->h:[Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0}, [Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    return-object v0
.end method
