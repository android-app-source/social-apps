.class abstract Ltv/periscope/android/ui/broadcast/b$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "b"
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ltv/periscope/model/chat/Message;

.field private final c:Ltv/periscope/android/view/b;

.field private final d:Ltv/periscope/android/view/aj;


# direct methods
.method constructor <init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/b$b;->a:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/b$b;->b:Ltv/periscope/model/chat/Message;

    .line 55
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/b$b;->c:Ltv/periscope/android/view/b;

    .line 56
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/b$b;->d:Ltv/periscope/android/view/aj;

    .line 57
    return-void
.end method


# virtual methods
.method public c()I
    .locals 1

    .prologue
    .line 65
    sget v0, Ltv/periscope/android/library/f$d;->ps__primary_text:I

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/b$b;->g()V

    .line 71
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/b$b;->c:Ltv/periscope/android/view/b;

    invoke-interface {v0}, Ltv/periscope/android/view/b;->e()V

    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract g()V
.end method

.method h()Ltv/periscope/android/view/aj;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/b$b;->d:Ltv/periscope/android/view/aj;

    return-object v0
.end method
