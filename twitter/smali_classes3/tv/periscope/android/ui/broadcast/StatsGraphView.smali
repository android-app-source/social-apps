.class public Ltv/periscope/android/ui/broadcast/StatsGraphView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/StatsGraphView$b;,
        Ltv/periscope/android/ui/broadcast/StatsGraphView$a;
    }
.end annotation


# instance fields
.field a:Z

.field protected b:Ltv/periscope/android/ui/broadcast/StatsGraphView$a;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ltv/periscope/android/ui/broadcast/n;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/github/mikephil/charting/charts/LineChart;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Ljava/lang/Long;

.field private h:Ltv/periscope/android/ui/broadcast/CustomMarkerView;

.field private i:Ltv/periscope/android/ui/broadcast/StatsGraphView$b;

.field private j:F

.field private k:F

.field private l:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->c:Ljava/util/ArrayList;

    .line 64
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(Landroid/content/Context;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->c:Ljava/util/ArrayList;

    .line 69
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(Landroid/content/Context;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->c:Ljava/util/ArrayList;

    .line 74
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method private a(F)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 298
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/j;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/data/j;->a(I)Lgc;

    move-result-object v0

    check-cast v0, Lgd;

    .line 299
    if-nez v0, :cond_0

    move v0, v1

    .line 308
    :goto_0
    return v0

    .line 302
    :cond_0
    invoke-interface {v0}, Lgd;->s()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_1
    if-ltz v2, :cond_2

    .line 304
    invoke-interface {v0, v2}, Lgd;->f(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v3

    cmpl-float v3, v3, p1

    if-nez v3, :cond_1

    move v0, v2

    .line 305
    goto :goto_0

    .line 302
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 308
    goto :goto_0
.end method

.method private a(Ljava/util/List;)Lcom/github/mikephil/charting/data/LineDataSet;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/github/mikephil/charting/data/Entry;",
            ">;)",
            "Lcom/github/mikephil/charting/data/LineDataSet;"
        }
    .end annotation

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 315
    new-instance v0, Lcom/github/mikephil/charting/data/LineDataSet;

    const-string/jumbo v1, "broadcast_viewer_count"

    invoke-direct {v0, p1, v1}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 318
    sget-object v1, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)V

    .line 319
    invoke-static {}, Lhg;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->c(I)V

    .line 320
    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/data/LineDataSet;->b(F)V

    .line 321
    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/data/LineDataSet;->b(F)V

    .line 322
    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/data/LineDataSet;->b(Z)V

    .line 324
    const/16 v1, 0x41

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->i(I)V

    .line 325
    sget-object v1, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->c:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->a(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V

    .line 326
    invoke-static {}, Lhg;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->h(I)V

    .line 327
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$d;->ps__light_grey:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->a(I)V

    .line 328
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$d;->ps__black:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->d(I)V

    .line 329
    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/data/LineDataSet;->a(Z)V

    .line 330
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/LineDataSet;->c(Z)V

    .line 331
    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/data/LineDataSet;->d(Z)V

    .line 332
    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/data/LineDataSet;->e(Z)V

    .line 333
    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 231
    new-instance v0, Lfj;

    int-to-float v1, p1

    invoke-direct {v0, v1, v3, v3}, Lfj;-><init>(FII)V

    .line 232
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    const/4 v2, 0x1

    new-array v2, v2, [Lfj;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/github/mikephil/charting/charts/LineChart;->a([Lfj;)V

    .line 233
    return-void
.end method

.method private a(II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 236
    if-nez p2, :cond_0

    .line 242
    :goto_0
    return-void

    .line 239
    :cond_0
    new-instance v0, Lfj;

    int-to-float v1, p1

    invoke-direct {v0, v1, v4, v4}, Lfj;-><init>(FII)V

    .line 240
    new-instance v1, Lfj;

    int-to-float v2, p2

    invoke-direct {v1, v2, v4, v4}, Lfj;-><init>(FII)V

    .line 241
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    const/4 v3, 0x2

    new-array v3, v3, [Lfj;

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-virtual {v2, v3}, Lcom/github/mikephil/charting/charts/LineChart;->a([Lfj;)V

    goto :goto_0
.end method

.method private a(J)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 245
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 246
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->f:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 248
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__end_broadcast:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 258
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long v2, p1, v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    .line 256
    invoke-static {v0, v1}, Ldag;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 257
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(JJ)V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->h:Ltv/periscope/android/ui/broadcast/CustomMarkerView;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->h:Ltv/periscope/android/ui/broadcast/CustomMarkerView;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->setCurrentValue(J)V

    .line 226
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->h:Ltv/periscope/android/ui/broadcast/CustomMarkerView;

    invoke-virtual {v0, p3, p4}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->setPeakValue(J)V

    .line 228
    :cond_0
    return-void
.end method

.method private a(Ltv/periscope/android/ui/broadcast/n;)V
    .locals 2

    .prologue
    .line 178
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->b(Ltv/periscope/android/ui/broadcast/n;)V

    .line 181
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/n;->b()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(J)V

    .line 182
    return-void
.end method

.method private b(Ltv/periscope/android/ui/broadcast/n;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 261
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/j;

    .line 262
    if-nez v0, :cond_0

    .line 295
    :goto_0
    return-void

    .line 265
    :cond_0
    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/data/j;->a(I)Lgc;

    move-result-object v1

    if-nez v1, :cond_1

    .line 268
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(Ljava/util/List;)Lcom/github/mikephil/charting/data/LineDataSet;

    move-result-object v1

    .line 269
    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/j;->a(Lgc;)V

    .line 272
    :cond_1
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/n;->c()J

    move-result-wide v2

    .line 273
    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/data/j;->a(I)Lgc;

    move-result-object v1

    check-cast v1, Lgd;

    .line 277
    new-instance v4, Lcom/github/mikephil/charting/data/Entry;

    invoke-interface {v1}, Lgd;->s()I

    move-result v5

    int-to-float v5, v5

    long-to-float v6, v2

    invoke-direct {v4, v5, v6}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    .line 278
    invoke-virtual {v0, v4, v7}, Lcom/github/mikephil/charting/data/j;->a(Lcom/github/mikephil/charting/data/Entry;I)V

    .line 281
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/j;->j()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 283
    invoke-interface {v1}, Lgd;->w()F

    move-result v1

    .line 284
    invoke-direct {p0, v1}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(F)I

    move-result v4

    .line 286
    float-to-int v1, v1

    int-to-long v6, v1

    invoke-direct {p0, v2, v3, v6, v7}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(JJ)V

    .line 288
    const/4 v1, -0x1

    if-eq v4, v1, :cond_2

    .line 289
    invoke-direct {p0, v4, v0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(II)V

    .line 292
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->j:F

    iget v2, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->l:F

    iget v3, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->k:F

    iget v4, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->j:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/github/mikephil/charting/charts/LineChart;->b(FFFF)V

    .line 294
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->h()V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 160
    new-instance v0, Lcom/github/mikephil/charting/data/j;

    invoke-direct {v0}, Lcom/github/mikephil/charting/data/j;-><init>()V

    .line 161
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/j;->b(I)V

    .line 164
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v1, v0}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/h;)V

    .line 165
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setDrawMarkerViews(Z)V

    .line 171
    new-instance v0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$i;->ps__marker_pop_up:I

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->h:Ltv/periscope/android/ui/broadcast/CustomMarkerView;

    .line 174
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->h:Ltv/periscope/android/ui/broadcast/CustomMarkerView;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setMarkerView(Lcom/github/mikephil/charting/components/d;)V

    .line 175
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    const/16 v1, 0x3e9

    .line 351
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->i:Ltv/periscope/android/ui/broadcast/StatsGraphView$b;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/StatsGraphView$b;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 352
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->i:Ltv/periscope/android/ui/broadcast/StatsGraphView$b;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/StatsGraphView$b;->sendEmptyMessage(I)Z

    .line 354
    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->i:Ltv/periscope/android/ui/broadcast/StatsGraphView$b;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/StatsGraphView$b;->removeMessages(I)V

    .line 358
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 112
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v0

    .line 113
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/components/XAxis;->b(I)V

    .line 114
    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/components/XAxis;->e(Z)V

    .line 115
    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/components/XAxis;->d(Z)V

    .line 116
    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/components/XAxis;->b(Z)V

    .line 117
    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/components/XAxis;->e(F)V

    .line 118
    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/components/XAxis;->a(Z)V

    .line 119
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 78
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_stats_line_chart:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 79
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Ltv/periscope/android/library/f$e;->ps__graph_zero_offset:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->j:F

    .line 80
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Ltv/periscope/android/library/f$e;->ps__graph_top_offset:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->l:F

    .line 81
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Ltv/periscope/android/library/f$e;->ps__graph_right_offset:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->k:F

    .line 84
    sget v0, Ltv/periscope/android/library/f$g;->line_chart:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/charts/LineChart;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    .line 89
    sget v0, Ltv/periscope/android/library/f$g;->current_time:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->e:Landroid/widget/TextView;

    .line 90
    sget v0, Ltv/periscope/android/library/f$g;->end_time:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->f:Landroid/widget/TextView;

    .line 93
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->j:F

    iget v2, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->l:F

    iget v3, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->k:F

    iget v4, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->j:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/github/mikephil/charting/charts/LineChart;->b(FFFF)V

    .line 95
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->e()V

    .line 96
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->c()V

    .line 97
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a()V

    .line 98
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->b()V

    .line 99
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->f()V

    .line 100
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->setLegend(Z)V

    .line 101
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->g:Ljava/lang/Long;

    .line 103
    new-instance v0, Ltv/periscope/android/ui/broadcast/StatsGraphView$b;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView$b;-><init>(Ltv/periscope/android/ui/broadcast/StatsGraphView;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->i:Ltv/periscope/android/ui/broadcast/StatsGraphView$b;

    .line 104
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->g()V

    .line 105
    return-void
.end method

.method public a(Ljava/util/List;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/ui/broadcast/n;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 185
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 187
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 188
    new-instance v4, Lcom/github/mikephil/charting/data/Entry;

    int-to-float v5, v1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/n;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/n;->c()J

    move-result-wide v6

    long-to-float v0, v6

    invoke-direct {v4, v5, v0}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 190
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/j;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/data/j;->a(I)Lgc;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 191
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/j;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/data/j;->a(I)Lgc;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/LineDataSet;

    .line 192
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/LineDataSet;->t()Ljava/util/List;

    move-result-object v0

    .line 193
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 194
    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 201
    :goto_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/j;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/data/j;->a(I)Lgc;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/LineDataSet;

    .line 204
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/LineDataSet;->w()F

    move-result v1

    .line 207
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->h:Ltv/periscope/android/ui/broadcast/CustomMarkerView;

    if-eqz v2, :cond_1

    .line 209
    invoke-virtual {v0, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->d(Z)V

    .line 210
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->h:Ltv/periscope/android/ui/broadcast/CustomMarkerView;

    float-to-int v2, v1

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->setPeakValue(J)V

    .line 211
    invoke-direct {p0, v1}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(F)I

    move-result v0

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(I)V

    .line 214
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->j:F

    iget v2, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->l:F

    iget v3, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->j:F

    iget v4, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->j:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/github/mikephil/charting/charts/LineChart;->b(FFFF)V

    .line 216
    if-eqz p2, :cond_3

    const-wide/16 v0, -0x1

    .line 217
    :goto_2
    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(J)V

    .line 220
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->h()V

    .line 221
    return-void

    .line 196
    :cond_2
    invoke-direct {p0, v3}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(Ljava/util/List;)Lcom/github/mikephil/charting/data/LineDataSet;

    move-result-object v0

    .line 197
    new-instance v1, Lcom/github/mikephil/charting/data/j;

    new-array v3, v8, [Lgd;

    aput-object v0, v3, v2

    invoke-direct {v1, v3}, Lcom/github/mikephil/charting/data/j;-><init>([Lgd;)V

    .line 198
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/h;)V

    goto :goto_1

    .line 216
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_2
.end method

.method public declared-synchronized a(Ltv/periscope/android/ui/broadcast/n;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 342
    monitor-enter p0

    if-nez p2, :cond_0

    .line 348
    :goto_0
    monitor-exit p0

    return-void

    .line 345
    :cond_0
    :try_start_0
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->g:Ljava/lang/Long;

    .line 346
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a(Ltv/periscope/android/ui/broadcast/n;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 342
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 122
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v0

    .line 123
    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/components/YAxis;->c(Z)V

    .line 124
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/components/YAxis;->b(I)V

    .line 125
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/components/YAxis;->b(F)V

    .line 126
    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/components/YAxis;->a(Z)V

    .line 127
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/components/YAxis;->e(Z)V

    .line 128
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisRight()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v0

    .line 129
    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/components/YAxis;->d(Z)V

    .line 130
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 134
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setHighlightPerTapEnabled(Z)V

    .line 135
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setDoubleTapToZoomEnabled(Z)V

    .line 136
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setPinchZoom(Z)V

    .line 137
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setTouchEnabled(Z)V

    .line 138
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/charts/LineChart;->setDragEnabled(Z)V

    .line 142
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setScaleXEnabled(Z)V

    .line 143
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/charts/LineChart;->setAutoScaleMinMaxEnabled(Z)V

    .line 146
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setLogEnabled(Z)V

    .line 148
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/charts/LineChart;->setDescription(Lcom/github/mikephil/charting/components/c;)V

    .line 149
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/charts/LineChart;->setNoDataText(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/charts/LineChart;->setScaleEnabled(Z)V

    .line 152
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setDrawGridBackground(Z)V

    .line 153
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setDrawBorders(Z)V

    .line 155
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->setBackgroundColor(I)V

    .line 156
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStatsDelegate()Ltv/periscope/android/ui/broadcast/StatsGraphView$a;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->b:Ltv/periscope/android/ui/broadcast/StatsGraphView$a;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 368
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 369
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->g()V

    .line 370
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 362
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->h()V

    .line 363
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 364
    return-void
.end method

.method public setBroadcastEnded(Z)V
    .locals 0

    .prologue
    .line 382
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a:Z

    .line 383
    return-void
.end method

.method public setDelegate(Ltv/periscope/android/ui/broadcast/StatsGraphView$a;)V
    .locals 0

    .prologue
    .line 373
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->b:Ltv/periscope/android/ui/broadcast/StatsGraphView$a;

    .line 374
    return-void
.end method

.method public setLegend(Z)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/StatsGraphView;->d:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/LineChart;->getLegend()Lcom/github/mikephil/charting/components/Legend;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/github/mikephil/charting/components/Legend;->d(Z)V

    .line 109
    return-void
.end method
