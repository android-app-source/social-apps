.class public Ltv/periscope/android/ui/broadcast/af;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;
.implements Landroid/content/ComponentCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Ltv/periscope/android/chat/h;
.implements Ltv/periscope/android/chat/i$a;
.implements Ltv/periscope/android/player/a;
.implements Ltv/periscope/android/ui/broadcast/ac;
.implements Ltv/periscope/android/ui/broadcast/an$a;
.implements Ltv/periscope/android/ui/broadcast/ao;
.implements Ltv/periscope/android/ui/broadcast/ap;
.implements Ltv/periscope/android/ui/broadcast/av$a;
.implements Ltv/periscope/android/ui/broadcast/u;
.implements Ltv/periscope/android/ui/chat/k;
.implements Ltv/periscope/android/ui/chat/o$a;
.implements Ltv/periscope/android/ui/chat/o$c;
.implements Ltv/periscope/android/ui/chat/r;
.implements Ltv/periscope/android/ui/chat/w;
.implements Ltv/periscope/android/util/ae$a;
.implements Ltv/periscope/android/view/ai;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/af$f;,
        Ltv/periscope/android/ui/broadcast/af$d;,
        Ltv/periscope/android/ui/broadcast/af$c;,
        Ltv/periscope/android/ui/broadcast/af$i;,
        Ltv/periscope/android/ui/broadcast/af$h;,
        Ltv/periscope/android/ui/broadcast/af$e;,
        Ltv/periscope/android/ui/broadcast/af$g;,
        Ltv/periscope/android/ui/broadcast/af$a;,
        Ltv/periscope/android/ui/broadcast/af$b;
    }
.end annotation


# instance fields
.field A:Z

.field B:Z

.field C:Z

.field private final D:Ltv/periscope/android/library/c;

.field private final E:Ltv/periscope/android/time/api/TimeZoneClient;

.field private final F:Landroid/content/SharedPreferences;

.field private final G:Ltv/periscope/android/util/ae;

.field private final H:Ltv/periscope/android/util/n$a;

.field private final I:Ltv/periscope/android/network/ConnectivityChangeReceiver;

.field private final J:Ltv/periscope/android/ui/broadcast/aq;

.field private final K:Lcyw;

.field private final L:Lcyt;

.field private final M:Lcyn;

.field private final N:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final O:Ljava/lang/String;

.field private final P:Landroid/view/ViewGroup;

.field private final Q:Ltv/periscope/android/view/t;

.field private final R:Ltv/periscope/android/analytics/summary/b;

.field private final S:Ltv/periscope/android/video/StreamMode;

.field private final T:Ltv/periscope/android/ui/chat/x;

.field private final U:Ltv/periscope/android/ui/chat/y;

.field private final V:Ltv/periscope/android/ui/chat/aj;

.field private final W:Ltv/periscope/android/ui/chat/ak;

.field private final X:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lczy;",
            ">;"
        }
    .end annotation
.end field

.field private final Y:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/SettingsDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final Z:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/permissions/a;",
            ">;"
        }
    .end annotation
.end field

.field final a:Ltv/periscope/android/player/d;

.field private aA:Ltv/periscope/model/u;

.field private aB:Ltv/periscope/android/chat/g;

.field private aC:Lcxc;

.field private aD:Ljava/lang/Boolean;

.field private final aE:Z

.field private final aF:Z

.field private final aG:Z

.field private final aH:Z

.field private final aI:Z

.field private final aJ:Z

.field private final aK:Z

.field private final aL:Z

.field private final aM:Z

.field private final aN:Z

.field private final aO:Z

.field private aP:Z

.field private aQ:Z

.field private aR:Z

.field private aS:Z

.field private aT:Z

.field private aU:Z

.field private aV:Z

.field private aW:Z

.field private aX:Z

.field private aY:Z

.field private aZ:Z

.field private final aa:Ltv/periscope/android/ui/broadcast/d;

.field private final ab:Ltv/periscope/android/view/o;

.field private final ac:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcxe;",
            ">;"
        }
    .end annotation
.end field

.field private final ad:Lcxa;

.field private final ae:Ljava/lang/String;

.field private final af:Lcxq;

.field private ag:Ltv/periscope/android/ui/broadcast/au;

.field private ah:Ltv/periscope/android/ui/broadcast/f;

.field private ai:Ltv/periscope/android/ui/broadcast/b;

.field private aj:Ltv/periscope/android/ui/broadcast/moderator/h;

.field private ak:Ltv/periscope/android/ui/broadcast/moderator/f;

.field private al:Ltv/periscope/android/ui/user/UserPickerSheet;

.field private am:Ltv/periscope/android/ui/broadcast/j;

.field private an:Ltv/periscope/android/ui/broadcast/h;

.field private ao:Lczz;

.field private ap:Ljava/lang/Runnable;

.field private aq:Ltv/periscope/android/ui/broadcast/ah;

.field private ar:Ltv/periscope/android/player/PlayMode;

.field private as:Ltv/periscope/android/ui/chat/g;

.field private at:Ltv/periscope/android/ui/chat/m;

.field private au:Lcxv;

.field private av:Landroid/os/Handler;

.field private aw:Ltv/periscope/android/ui/chat/b;

.field private ax:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ay:Ljava/io/File;

.field private az:Ltv/periscope/model/af;

.field final b:Ltv/periscope/android/player/e;

.field private ba:Z

.field private bb:Z

.field private bc:Z

.field private bd:Z

.field private be:J

.field private bf:J

.field private bg:J

.field private bh:J

.field private final bi:Lretrofit/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit/Callback",
            "<",
            "Ltv/periscope/android/time/api/TimeZoneClient$a;",
            ">;"
        }
    .end annotation
.end field

.field final c:Landroid/os/Handler;

.field final d:Landroid/content/Context;

.field e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

.field f:Ltv/periscope/android/ui/broadcast/g;

.field g:Ltv/periscope/android/ui/user/l;

.field h:Ltv/periscope/android/ui/broadcast/am;

.field i:Ltv/periscope/android/view/RootDragLayout;

.field j:Landroid/widget/TextView;

.field k:Landroid/widget/TextView;

.field l:Landroid/widget/TextView;

.field m:Landroid/widget/TextView;

.field n:Ltv/periscope/android/ui/broadcast/av;

.field o:Ltv/periscope/android/player/c;

.field p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

.field q:Ltv/periscope/android/ui/broadcast/ar;

.field r:Ltv/periscope/android/ui/broadcast/v;

.field s:Ltv/periscope/android/player/PlayMode;

.field t:Ljava/lang/Runnable;

.field u:Ljava/lang/Runnable;

.field v:Ljava/lang/Runnable;

.field w:Ldbo;

.field x:Ltv/periscope/model/p;

.field y:Ljava/lang/String;

.field final z:Z


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;Ltv/periscope/android/library/c;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Landroid/view/ViewGroup;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ltv/periscope/android/view/t;Lcxq;Ltv/periscope/android/ui/broadcast/d;Ltv/periscope/android/ui/chat/x;Ltv/periscope/android/ui/chat/y;Ltv/periscope/android/view/o;Ltv/periscope/android/ui/chat/aj;Ltv/periscope/android/ui/chat/ak;Ltv/periscope/android/analytics/summary/b;Lcxa;Ljava/util/ArrayList;Ltv/periscope/android/video/StreamMode;JJLjava/lang/String;ZZZZZZZZZZZZZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;",
            "Ltv/periscope/android/library/c;",
            "Ltv/periscope/android/player/d;",
            "Ltv/periscope/android/player/e;",
            "Landroid/view/ViewGroup;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcxe;",
            ">;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/permissions/a;",
            ">;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lczy;",
            ">;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/SettingsDelegate;",
            ">;",
            "Ltv/periscope/android/view/t;",
            "Lcxq;",
            "Ltv/periscope/android/ui/broadcast/d;",
            "Ltv/periscope/android/ui/chat/x;",
            "Ltv/periscope/android/ui/chat/y;",
            "Ltv/periscope/android/view/o;",
            "Ltv/periscope/android/ui/chat/aj;",
            "Ltv/periscope/android/ui/chat/ak;",
            "Ltv/periscope/android/analytics/summary/b;",
            "Lcxa;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ltv/periscope/android/video/StreamMode;",
            "JJ",
            "Ljava/lang/String;",
            "ZZZZZZZZZZZZZ)V"
        }
    .end annotation

    .prologue
    .line 408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    .line 235
    sget-object v2, Ltv/periscope/android/player/PlayMode;->a:Ltv/periscope/android/player/PlayMode;

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    .line 253
    sget-object v2, Ltv/periscope/android/player/PlayMode;->a:Ltv/periscope/android/player/PlayMode;

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->ar:Ltv/periscope/android/player/PlayMode;

    .line 288
    const/4 v2, 0x1

    iput-boolean v2, p0, Ltv/periscope/android/ui/broadcast/af;->aU:Z

    .line 303
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Ltv/periscope/android/ui/broadcast/af;->bh:J

    .line 2907
    new-instance v2, Ltv/periscope/android/ui/broadcast/af$7;

    invoke-direct {v2, p0}, Ltv/periscope/android/ui/broadcast/af$7;-><init>(Ltv/periscope/android/ui/broadcast/af;)V

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->bi:Lretrofit/Callback;

    .line 409
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    .line 410
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    .line 411
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 412
    invoke-interface {p2}, Ltv/periscope/android/library/c;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->O:Ljava/lang/String;

    .line 413
    invoke-interface {p2}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v2

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    .line 414
    invoke-interface {p2}, Ltv/periscope/android/library/c;->g()Lcyt;

    move-result-object v2

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->L:Lcyt;

    .line 415
    invoke-interface {p2}, Ltv/periscope/android/library/c;->h()Lcyn;

    move-result-object v2

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    .line 416
    invoke-interface {p2}, Ltv/periscope/android/library/c;->p()Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->F:Landroid/content/SharedPreferences;

    .line 417
    invoke-interface {p2}, Ltv/periscope/android/library/c;->n()Ltv/periscope/android/ui/broadcast/aq;

    move-result-object v2

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->J:Ltv/periscope/android/ui/broadcast/aq;

    .line 418
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    .line 419
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/af;->b:Ltv/periscope/android/player/e;

    .line 420
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    .line 421
    iput-object p6, p0, Ltv/periscope/android/ui/broadcast/af;->ac:Ljava/lang/ref/WeakReference;

    .line 422
    iput-object p7, p0, Ltv/periscope/android/ui/broadcast/af;->Z:Ljava/lang/ref/WeakReference;

    .line 423
    iput-object p8, p0, Ltv/periscope/android/ui/broadcast/af;->X:Ljava/lang/ref/WeakReference;

    .line 424
    iput-object p9, p0, Ltv/periscope/android/ui/broadcast/af;->Y:Ljava/lang/ref/WeakReference;

    .line 425
    move-object/from16 v0, p12

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aa:Ltv/periscope/android/ui/broadcast/d;

    .line 426
    move-object/from16 v0, p13

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->T:Ltv/periscope/android/ui/chat/x;

    .line 427
    move-object/from16 v0, p14

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->U:Ltv/periscope/android/ui/chat/y;

    .line 428
    move-object/from16 v0, p15

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ab:Ltv/periscope/android/view/o;

    .line 429
    move-object/from16 v0, p16

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->V:Ltv/periscope/android/ui/chat/aj;

    .line 430
    move-object/from16 v0, p17

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->W:Ltv/periscope/android/ui/chat/ak;

    .line 431
    move-object/from16 v0, p18

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->R:Ltv/periscope/android/analytics/summary/b;

    .line 432
    move-object/from16 v0, p19

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ad:Lcxa;

    .line 433
    if-eqz p10, :cond_0

    :goto_0
    move-object/from16 v0, p10

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->Q:Ltv/periscope/android/view/t;

    .line 434
    move-object/from16 v0, p11

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->af:Lcxq;

    .line 435
    new-instance v2, Ltv/periscope/android/time/api/TimeZoneClient;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v3}, Ltv/periscope/android/library/c;->o()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-direct {v2, v3}, Ltv/periscope/android/time/api/TimeZoneClient;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->E:Ltv/periscope/android/time/api/TimeZoneClient;

    .line 436
    new-instance v2, Ltv/periscope/android/util/ae;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    invoke-direct {v2, v3, v4}, Ltv/periscope/android/util/ae;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->G:Ltv/periscope/android/util/ae;

    .line 437
    new-instance v3, Ltv/periscope/android/network/ConnectivityChangeReceiver;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->Z()Lde/greenrobot/event/c;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Ltv/periscope/android/network/ConnectivityChangeReceiver;-><init>(Landroid/app/Activity;Lde/greenrobot/event/c;)V

    iput-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->I:Ltv/periscope/android/network/ConnectivityChangeReceiver;

    .line 438
    new-instance v2, Ltv/periscope/android/util/n$a;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    .line 439
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Ltv/periscope/android/library/f$e;->ps__keyboard_height_threshold:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-direct {v2, v3, v4}, Ltv/periscope/android/util/n$a;-><init>(Landroid/view/View;I)V

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->H:Ltv/periscope/android/util/n$a;

    .line 441
    move-object/from16 v0, p20

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ax:Ljava/util/ArrayList;

    .line 442
    move-object/from16 v0, p21

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->S:Ltv/periscope/android/video/StreamMode;

    .line 443
    move-wide/from16 v0, p22

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/af;->bh:J

    .line 444
    move-wide/from16 v0, p24

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/af;->bg:J

    .line 445
    move-object/from16 v0, p26

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ae:Ljava/lang/String;

    .line 446
    move/from16 v0, p27

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aF:Z

    .line 447
    move/from16 v0, p29

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aE:Z

    .line 448
    move/from16 v0, p28

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aP:Z

    .line 449
    move/from16 v0, p30

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aG:Z

    .line 450
    move/from16 v0, p31

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aH:Z

    .line 451
    move/from16 v0, p33

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aI:Z

    .line 452
    move/from16 v0, p32

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->z:Z

    .line 453
    move/from16 v0, p34

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aJ:Z

    .line 454
    move/from16 v0, p35

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aK:Z

    .line 455
    move/from16 v0, p36

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aL:Z

    .line 456
    move/from16 v0, p37

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aM:Z

    .line 457
    move/from16 v0, p38

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aN:Z

    .line 458
    move/from16 v0, p39

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aO:Z

    .line 460
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-direct {p0, v2}, Ltv/periscope/android/ui/broadcast/af;->a(Landroid/app/Activity;)V

    .line 461
    return-void

    .line 433
    :cond_0
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-direct {p0, v2}, Ltv/periscope/android/ui/broadcast/af;->b(Landroid/app/Activity;)Ltv/periscope/android/view/t;

    move-result-object p10

    goto/16 :goto_0
.end method

.method private X()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 311
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-lt v0, v2, :cond_1

    .line 312
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 313
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 315
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 313
    goto :goto_0

    :cond_1
    move v0, v1

    .line 315
    goto :goto_0
.end method

.method private Y()V
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 320
    if-eqz v0, :cond_0

    .line 321
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 323
    :cond_0
    return-void
.end method

.method private Z()Lde/greenrobot/event/c;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/af;Ltv/periscope/model/chat/Message;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 172
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/af;->d(Ltv/periscope/model/chat/Message;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/android/ui/broadcast/au;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ag:Ltv/periscope/android/ui/broadcast/au;

    return-object v0
.end method

.method private a(Landroid/app/Activity;)V
    .locals 26

    .prologue
    .line 464
    invoke-direct/range {p0 .. p0}, Ltv/periscope/android/ui/broadcast/af;->aa()V

    .line 465
    invoke-direct/range {p0 .. p0}, Ltv/periscope/android/ui/broadcast/af;->ad()V

    .line 466
    invoke-direct/range {p0 .. p1}, Ltv/periscope/android/ui/broadcast/af;->c(Landroid/app/Activity;)V

    .line 468
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 469
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 471
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 472
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/app/Application;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 474
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->j()Ltv/periscope/android/session/a;

    move-result-object v1

    invoke-interface {v1}, Ltv/periscope/android/session/a;->a()Ltv/periscope/android/session/Session;

    move-result-object v1

    if-eqz v1, :cond_4

    const/4 v10, 0x1

    .line 476
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v2, Ltv/periscope/android/library/f$g;->root:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/view/RootDragLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    .line 477
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ltv/periscope/android/view/RootDragLayout;->setDraggable(Z)V

    .line 478
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    new-instance v2, Ltv/periscope/android/ui/broadcast/af$b;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Ltv/periscope/android/ui/broadcast/af$b;-><init>(Ltv/periscope/android/ui/broadcast/af;)V

    invoke-virtual {v1, v2}, Ltv/periscope/android/view/RootDragLayout;->setOnViewDragListener(Ltv/periscope/android/view/RootDragLayout$c;)V

    .line 479
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    new-instance v2, Ltv/periscope/android/ui/broadcast/af$a;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Ltv/periscope/android/ui/broadcast/af$a;-><init>(Ltv/periscope/android/ui/broadcast/af;)V

    invoke-virtual {v1, v2}, Ltv/periscope/android/view/RootDragLayout;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 480
    move-object/from16 v0, p0

    iget-boolean v1, v0, Ltv/periscope/android/ui/broadcast/af;->aO:Z

    if-eqz v1, :cond_0

    .line 481
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    new-instance v2, Ltv/periscope/android/view/ad;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Ltv/periscope/android/view/ad;-><init>(Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v1, v2}, Ltv/periscope/android/view/RootDragLayout;->setSwipeToDismissCallback(Ltv/periscope/android/view/ae$a;)V

    .line 484
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v2, Ltv/periscope/android/library/f$g;->player_view:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/view/PlayerView;

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    .line 485
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->w()Ldae;

    move-result-object v2

    invoke-interface {v1, v2}, Ltv/periscope/android/player/c;->setThumbImageUrlLoader(Ldae;)V

    .line 487
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v1}, Ltv/periscope/android/player/c;->getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    .line 488
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setParticipantClickListener(Landroid/view/View$OnClickListener;)V

    .line 489
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setOverflowClickListener(Landroid/view/View$OnClickListener;)V

    .line 490
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setAvatarImageLoader(Ldae;)V

    .line 491
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->v()Ldae;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setImageLoader(Ldae;)V

    .line 492
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->j()Ltv/periscope/android/session/a;

    move-result-object v1

    invoke-interface {v1}, Ltv/periscope/android/session/a;->a()Ltv/periscope/android/session/Session;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 493
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a()V

    .line 495
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->aa:Ltv/periscope/android/ui/broadcast/d;

    if-eqz v1, :cond_2

    .line 496
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->aa:Ltv/periscope/android/ui/broadcast/d;

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayActionButtonPresenter(Ltv/periscope/android/ui/broadcast/d;)V

    .line 498
    :cond_2
    new-instance v1, Ltv/periscope/android/ui/broadcast/ah;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    invoke-direct {v1, v2, v3}, Ltv/periscope/android/ui/broadcast/ah;-><init>(Ltv/periscope/android/player/d;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->aq:Ltv/periscope/android/ui/broadcast/ah;

    .line 499
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->aq:Ltv/periscope/android/ui/broadcast/ah;

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setPlaytimePresenter(Ltv/periscope/android/ui/broadcast/ah;)V

    .line 501
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v2, Ltv/periscope/android/library/f$g;->close:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 503
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v2, Ltv/periscope/android/library/f$g;->play_highlights:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->l:Landroid/widget/TextView;

    .line 504
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->l:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 506
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v2, Ltv/periscope/android/library/f$g;->tweak_video_info:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->m:Landroid/widget/TextView;

    .line 508
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v2, Ltv/periscope/android/library/f$g;->bottom_drag_child:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    .line 509
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 511
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    new-instance v2, Ltv/periscope/android/ui/broadcast/af$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Ltv/periscope/android/ui/broadcast/af$1;-><init>(Ltv/periscope/android/ui/broadcast/af;)V

    invoke-interface {v1, v2}, Ltv/periscope/android/player/c;->setPlayPauseClickListener(Landroid/view/View$OnClickListener;)V

    .line 518
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v2, Ltv/periscope/android/library/f$g;->replay_scrubber:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    .line 519
    new-instance v2, Ltv/periscope/android/ui/broadcast/am;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v3}, Ltv/periscope/android/library/c;->w()Ldae;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ltv/periscope/android/ui/broadcast/am;-><init>(Ltv/periscope/android/ui/broadcast/ReplayScrubView;Ldae;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    .line 520
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/am;->a(Ltv/periscope/android/player/d;)V

    .line 521
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    new-instance v2, Ltv/periscope/android/ui/broadcast/af$g;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Ltv/periscope/android/ui/broadcast/af$g;-><init>(Ltv/periscope/android/ui/broadcast/af;)V

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/am;->a(Ltv/periscope/android/ui/broadcast/am$c;)V

    .line 523
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v1}, Ltv/periscope/android/player/c;->getPreview()Landroid/view/ViewGroup;

    move-result-object v1

    .line 524
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setHapticFeedbackEnabled(Z)V

    .line 525
    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 526
    new-instance v2, Ltv/periscope/android/view/x;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    new-instance v4, Ltv/periscope/android/ui/broadcast/af$9;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v1}, Ltv/periscope/android/ui/broadcast/af$9;-><init>(Ltv/periscope/android/ui/broadcast/af;Landroid/view/View;)V

    invoke-direct {v2, v3, v4}, Ltv/periscope/android/view/x;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 537
    new-instance v11, Ltv/periscope/android/ui/broadcast/af$h;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    invoke-direct {v11, v2, v3}, Ltv/periscope/android/ui/broadcast/af$h;-><init>(Ltv/periscope/android/view/x;Ltv/periscope/android/ui/broadcast/am;)V

    .line 539
    new-instance v2, Ltv/periscope/android/ui/broadcast/af$c;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View$OnTouchListener;

    const/4 v4, 0x0

    aput-object v11, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ltv/periscope/android/ui/broadcast/af$i;

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-direct {v5, v6}, Ltv/periscope/android/ui/broadcast/af$i;-><init>(Ltv/periscope/android/player/d;)V

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Ltv/periscope/android/ui/broadcast/af$c;-><init>([Landroid/view/View$OnTouchListener;)V

    .line 543
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 545
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v2, Ltv/periscope/android/library/f$g;->user_picker_sheet:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/ui/user/UserPickerSheet;

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->al:Ltv/periscope/android/ui/user/UserPickerSheet;

    .line 546
    new-instance v1, Lcyz;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->L:Lcyt;

    invoke-direct {v1, v2, v3}, Lcyz;-><init>(Lcyw;Lcyt;)V

    .line 547
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->al:Ltv/periscope/android/ui/user/UserPickerSheet;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v4}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Ltv/periscope/android/ui/user/UserPickerSheet;->a(Lcyz;Lcyw;Ldae;)V

    .line 548
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->al:Ltv/periscope/android/ui/user/UserPickerSheet;

    sget v2, Ltv/periscope/android/library/f$l;->ps__share_broadcast_title:I

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/user/UserPickerSheet;->a(I)Ltv/periscope/android/ui/user/UserPickerSheet;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__share_broadcast_description:I

    .line 549
    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/user/UserPickerSheet;->b(I)Ltv/periscope/android/ui/user/UserPickerSheet;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$g;->check:I

    .line 550
    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/user/UserPickerSheet;->c(I)Ltv/periscope/android/ui/user/UserPickerSheet;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__share_broadcast_count:I

    .line 551
    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/user/UserPickerSheet;->d(I)Ltv/periscope/android/ui/user/UserPickerSheet;

    move-result-object v1

    const/4 v2, 0x1

    .line 552
    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/user/UserPickerSheet;->b(Z)Ltv/periscope/android/ui/user/UserPickerSheet;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/user/UserType;->c:Ltv/periscope/model/user/UserType;

    .line 553
    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/user/UserPickerSheet;->a(Ltv/periscope/model/user/UserType;)Ltv/periscope/android/ui/user/UserPickerSheet;

    .line 554
    new-instance v1, Ltv/periscope/android/ui/user/m;

    invoke-virtual/range {p0 .. p0}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->al:Ltv/periscope/android/ui/user/UserPickerSheet;

    sget-object v4, Ltv/periscope/model/user/UserType;->c:Ltv/periscope/model/user/UserType;

    invoke-direct {v1, v2, v3, v4}, Ltv/periscope/android/ui/user/m;-><init>(Ltv/periscope/android/api/ApiManager;Ltv/periscope/android/ui/user/UserPickerSheet;Ltv/periscope/model/user/UserType;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->g:Ltv/periscope/android/ui/user/l;

    .line 556
    new-instance v5, Ltv/periscope/android/ui/broadcast/af$f;

    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 557
    invoke-interface {v1}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->R:Ltv/periscope/android/analytics/summary/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v3}, Ltv/periscope/android/library/c;->r()Ldbr;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v1, v2, v3}, Ltv/periscope/android/ui/broadcast/af$f;-><init>(Ltv/periscope/android/ui/broadcast/af;Ltv/periscope/android/api/ApiManager;Lcsa;Ldbr;)V

    .line 558
    new-instance v1, Ltv/periscope/android/ui/broadcast/au;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v4, Ltv/periscope/android/library/f$g;->viewer_action_sheet:I

    .line 559
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Ltv/periscope/android/view/ActionSheet;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/ui/broadcast/af;->Q:Ltv/periscope/android/view/t;

    invoke-virtual/range {p0 .. p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    move-object/from16 v0, p0

    iget-object v8, v0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    move-object/from16 v0, p0

    iget-object v9, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 560
    invoke-interface {v9}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v9

    invoke-direct/range {v1 .. v10}, Ltv/periscope/android/ui/broadcast/au;-><init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/ActionSheet;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;Lcrz;Lcyw;Lcyn;Ldae;Z)V

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->ag:Ltv/periscope/android/ui/broadcast/au;

    .line 561
    if-eqz v10, :cond_3

    .line 562
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->Q:Ltv/periscope/android/view/t;

    invoke-interface {v1, v5}, Ltv/periscope/android/view/t;->a(Ltv/periscope/android/ui/user/g;)V

    .line 570
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1, v11}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 571
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1, v11}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setOnInterceptTouchEventListener(Landroid/view/View$OnTouchListener;)V

    .line 573
    new-instance v11, Ltv/periscope/android/ui/broadcast/f;

    move-object/from16 v0, p0

    iget-object v12, v0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    new-instance v13, Ltv/periscope/android/view/f;

    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->ab:Ltv/periscope/android/view/o;

    invoke-direct {v13, v1}, Ltv/periscope/android/view/f;-><init>(Ltv/periscope/android/view/o;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v2, Ltv/periscope/android/library/f$g;->broadcast__action_sheet:I

    .line 574
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Ltv/periscope/android/view/ActionSheet;

    move-object/from16 v0, p0

    iget-object v15, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move/from16 v16, v10

    invoke-direct/range {v11 .. v16}, Ltv/periscope/android/ui/broadcast/f;-><init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/f;Ltv/periscope/android/view/ActionSheet;Ltv/periscope/android/ui/broadcast/ChatRoomView;Z)V

    move-object/from16 v0, p0

    iput-object v11, v0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    .line 577
    new-instance v11, Ltv/periscope/android/ui/broadcast/aw;

    move-object/from16 v0, p0

    iget-object v12, v0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual/range {p0 .. p0}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    move-object/from16 v0, p0

    iget-object v15, v0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->Q:Ltv/periscope/android/view/t;

    move-object/from16 v16, v0

    .line 578
    invoke-virtual/range {p0 .. p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->Y:Ljava/lang/ref/WeakReference;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->af:Lcxq;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->b:Ltv/periscope/android/player/e;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/ui/broadcast/af;->aL:Z

    move/from16 v25, v0

    move-object/from16 v17, v5

    move-object/from16 v18, p0

    move-object/from16 v19, p0

    move-object/from16 v21, p0

    invoke-direct/range {v11 .. v25}, Ltv/periscope/android/ui/broadcast/aw;-><init>(Ljava/lang/ref/WeakReference;Ltv/periscope/android/api/ApiManager;Lcyn;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;Ltv/periscope/android/ui/broadcast/ao;Ltv/periscope/android/ui/broadcast/u;Lcrz;Ltv/periscope/android/ui/broadcast/ap;Ljava/lang/ref/WeakReference;Lcxq;Ltv/periscope/android/player/e;Z)V

    move-object/from16 v0, p0

    iput-object v11, v0, Ltv/periscope/android/ui/broadcast/af;->an:Ltv/periscope/android/ui/broadcast/h;

    .line 581
    new-instance v1, Ltv/periscope/android/ui/broadcast/as;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->H:Ltv/periscope/android/util/n$a;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 582
    invoke-interface {v3}, Ltv/periscope/android/library/c;->w()Ldae;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->J:Ltv/periscope/android/ui/broadcast/aq;

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v6}, Ltv/periscope/android/library/c;->j()Ltv/periscope/android/session/a;

    move-result-object v6

    invoke-interface {v6}, Ltv/periscope/android/session/a;->a()Ltv/periscope/android/session/Session;

    move-result-object v6

    invoke-interface {v3, v6}, Ltv/periscope/android/ui/broadcast/aq;->b(Ltv/periscope/android/session/Session;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v3, p0

    invoke-direct/range {v1 .. v6}, Ltv/periscope/android/ui/broadcast/as;-><init>(Ltv/periscope/android/util/n$a;Ltv/periscope/android/view/ai;Ldae;Landroid/view/ViewGroup;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->q:Ltv/periscope/android/ui/broadcast/ar;

    .line 584
    new-instance v1, Ltv/periscope/android/ui/broadcast/moderator/h;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual/range {p0 .. p0}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/android/ui/broadcast/af;->Y:Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v2, v3, v4, v5}, Ltv/periscope/android/ui/broadcast/moderator/h;-><init>(Ljava/lang/ref/WeakReference;Ltv/periscope/android/ui/broadcast/ChatRoomView;Ltv/periscope/android/api/ApiManager;Ljava/lang/ref/WeakReference;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->aj:Ltv/periscope/android/ui/broadcast/moderator/h;

    .line 585
    new-instance v1, Ltv/periscope/android/ui/broadcast/moderator/j;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->aj:Ltv/periscope/android/ui/broadcast/moderator/h;

    new-instance v3, Ltv/periscope/android/ui/broadcast/af$e;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/ui/broadcast/af;->H:Ltv/periscope/android/util/n$a;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Ltv/periscope/android/ui/broadcast/af$e;-><init>(Ltv/periscope/android/ui/broadcast/af;Ltv/periscope/android/util/n$a;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    .line 586
    invoke-interface {v4}, Lcyw;->c()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ldbe;

    invoke-direct {v5}, Ldbe;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-interface {v6}, Lcyw;->g()Ldbb;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Ltv/periscope/android/ui/broadcast/moderator/j;-><init>(Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/c;Ljava/lang/String;Ldbd;Ldbb;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->ak:Ltv/periscope/android/ui/broadcast/moderator/f;

    .line 587
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->aj:Ltv/periscope/android/ui/broadcast/moderator/h;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->ak:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/moderator/h;->a(Ltv/periscope/android/ui/broadcast/moderator/f;)V

    .line 589
    new-instance v1, Ltv/periscope/android/ui/broadcast/e;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v3}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/ui/broadcast/af;->an:Ltv/periscope/android/ui/broadcast/h;

    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/android/ui/broadcast/af;->aj:Ltv/periscope/android/ui/broadcast/moderator/h;

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    move-object/from16 v0, p0

    iget-object v7, v0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    move-object/from16 v0, p0

    iget-object v8, v0, Ltv/periscope/android/ui/broadcast/af;->ab:Ltv/periscope/android/view/o;

    invoke-direct/range {v1 .. v8}, Ltv/periscope/android/ui/broadcast/e;-><init>(Ltv/periscope/android/view/b;Ldae;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/android/ui/broadcast/moderator/g;Lcyw;Lcyn;Ltv/periscope/android/view/o;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->ai:Ltv/periscope/android/ui/broadcast/b;

    .line 592
    new-instance v1, Ltv/periscope/android/ui/broadcast/q;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/android/ui/broadcast/af;->an:Ltv/periscope/android/ui/broadcast/h;

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 593
    invoke-interface {v6}, Ltv/periscope/android/library/c;->i()Ltv/periscope/android/ui/broadcast/y;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v7, v0, Ltv/periscope/android/ui/broadcast/af;->aJ:Z

    invoke-direct/range {v1 .. v7}, Ltv/periscope/android/ui/broadcast/q;-><init>(Landroid/content/Context;Lcyw;Lcyn;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/android/ui/broadcast/y;Z)V

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    .line 594
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Ltv/periscope/android/ui/broadcast/af;->aM:Z

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/j;->a(Z)V

    .line 595
    new-instance v1, Ltv/periscope/android/ui/broadcast/g;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/ui/broadcast/af;->an:Ltv/periscope/android/ui/broadcast/h;

    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 596
    invoke-interface {v6}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Ltv/periscope/android/ui/broadcast/g;-><init>(Landroid/content/Context;Ltv/periscope/android/ui/broadcast/j;Ltv/periscope/android/ui/broadcast/h;Lcyw;Ldae;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    .line 598
    new-instance v5, Ltv/periscope/android/ui/broadcast/v;

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p0

    iget-object v7, v0, Ltv/periscope/android/ui/broadcast/af;->ak:Ltv/periscope/android/ui/broadcast/moderator/f;

    move-object/from16 v0, p0

    iget-object v8, v0, Ltv/periscope/android/ui/broadcast/af;->aj:Ltv/periscope/android/ui/broadcast/moderator/h;

    move-object/from16 v0, p0

    iget-boolean v9, v0, Ltv/periscope/android/ui/broadcast/af;->aH:Z

    invoke-direct/range {v5 .. v10}, Ltv/periscope/android/ui/broadcast/v;-><init>(Ljava/lang/ref/WeakReference;Ltv/periscope/android/ui/broadcast/moderator/f;Ltv/periscope/android/ui/broadcast/moderator/g;ZZ)V

    move-object/from16 v0, p0

    iput-object v5, v0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    .line 603
    new-instance v1, Ltv/periscope/android/ui/broadcast/ad;

    invoke-virtual/range {p0 .. p0}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, Ltv/periscope/android/ui/broadcast/af;->Z()Lde/greenrobot/event/c;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    move-object/from16 v0, p0

    iget-object v7, v0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 604
    invoke-interface {v2}, Ltv/periscope/android/library/c;->b()Lretrofit/RestAdapter$LogLevel;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/ui/broadcast/af;->S:Ltv/periscope/android/video/StreamMode;

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    invoke-direct/range {v1 .. v10}, Ltv/periscope/android/ui/broadcast/ad;-><init>(Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/video/StreamMode;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->n:Ltv/periscope/android/ui/broadcast/av;

    .line 606
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->G:Ltv/periscope/android/util/ae;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Ltv/periscope/android/util/ae;->a(Ltv/periscope/android/util/ae$a;)V

    .line 607
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/periscope/android/ui/broadcast/af;->I:Ltv/periscope/android/network/ConnectivityChangeReceiver;

    invoke-virtual {v1}, Ltv/periscope/android/network/ConnectivityChangeReceiver;->a()V

    .line 608
    return-void

    .line 474
    :cond_4
    const/4 v10, 0x0

    goto/16 :goto_0
.end method

.method private a(Ljava/io/File;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2598
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    if-eqz v0, :cond_2

    .line 2599
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    invoke-virtual {v0}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v1

    .line 2600
    invoke-virtual {v1}, Ltv/periscope/model/p;->d()Ljava/lang/String;

    move-result-object v0

    .line 2601
    invoke-static {v0}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2602
    const-string/jumbo v0, ""

    .line 2605
    :cond_0
    invoke-virtual {v1}, Ltv/periscope/model/p;->v()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-interface {v3}, Lcyw;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2606
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Ltv/periscope/android/library/f$l;->ps__share_post_tweet_text_username:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    .line 2607
    invoke-virtual {v1}, Ltv/periscope/model/p;->v()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    .line 2606
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2610
    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x40

    if-ne v1, v2, :cond_1

    .line 2611
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2618
    :cond_1
    :goto_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->al()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2619
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->q:Ltv/periscope/android/ui/broadcast/ar;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    invoke-virtual {v2}, Ltv/periscope/model/af;->i()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2, p1}, Ltv/periscope/android/ui/broadcast/ar;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 2622
    :cond_2
    return-void

    .line 2616
    :cond_3
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__share_post_tweet_text:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ltv/periscope/android/event/ApiEvent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 1524
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1525
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1526
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 1527
    if-nez v0, :cond_1

    .line 1530
    invoke-virtual {p0, v2, v3}, Ltv/periscope/android/ui/broadcast/af;->a(ILjava/lang/String;)V

    .line 1554
    :cond_0
    :goto_0
    return-void

    .line 1534
    :cond_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v2, Ltv/periscope/android/player/PlayMode;->a:Ltv/periscope/android/player/PlayMode;

    if-ne v1, v2, :cond_5

    .line 1535
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ar:Ltv/periscope/android/player/PlayMode;

    sget-object v2, Ltv/periscope/android/player/PlayMode;->a:Ltv/periscope/android/player/PlayMode;

    if-eq v1, v2, :cond_3

    .line 1536
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ar:Ltv/periscope/android/player/PlayMode;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/android/player/PlayMode;)V

    .line 1548
    :cond_2
    :goto_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ak:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/f;->a()V

    .line 1549
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->s()V

    goto :goto_0

    .line 1538
    :cond_3
    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    :goto_2
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/android/player/PlayMode;)V

    goto :goto_1

    :cond_4
    sget-object v0, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    goto :goto_2

    .line 1540
    :cond_5
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v2, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v1}, Ltv/periscope/android/player/d;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1542
    invoke-virtual {v0}, Ltv/periscope/model/p;->L()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1543
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "BroadcastPlayer is buffering but API told us it ended."

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 1544
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->Q()V

    .line 1545
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aw()V

    goto :goto_1

    .line 1552
    :cond_6
    invoke-virtual {p0, v2, v3}, Ltv/periscope/android/ui/broadcast/af;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/af;J)V
    .locals 1

    .prologue
    .line 172
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/af;->d(J)V

    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/af;Z)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/af;->f(Z)V

    return-void
.end method

.method private a(Ltv/periscope/android/ui/broadcast/av;Ljava/lang/String;)V
    .locals 17

    .prologue
    .line 739
    new-instance v5, Ltv/periscope/android/chat/b;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->q()Lcyr;

    move-result-object v2

    invoke-direct {v5, v2}, Ltv/periscope/android/chat/b;-><init>(Lcyr;)V

    .line 740
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v2

    .line 742
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-interface {v3}, Lcyw;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v8, 0x1

    .line 744
    :goto_0
    invoke-virtual/range {p0 .. p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/ui/broadcast/af;->q:Ltv/periscope/android/ui/broadcast/ar;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    .line 745
    invoke-interface {v2}, Lcyw;->b()Ltv/periscope/android/api/PsUser;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    move-object/from16 v0, p0

    iget-object v11, v0, Ltv/periscope/android/ui/broadcast/af;->b:Ltv/periscope/android/player/e;

    move-object/from16 v0, p0

    iget-object v13, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v9, v0, Ltv/periscope/android/ui/broadcast/af;->F:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v12, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    move-object/from16 v0, p0

    iget-object v14, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 746
    invoke-interface {v14}, Ltv/periscope/android/library/c;->s()Lcxi;

    move-result-object v14

    invoke-static {v2, v9, v12, v14}, Lcxn;->a(Landroid/content/Context;Landroid/content/SharedPreferences;Lcyw;Lcxi;)Lcxm;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Ltv/periscope/android/ui/broadcast/af;->T:Ltv/periscope/android/ui/chat/x;

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->V:Ltv/periscope/android/ui/chat/aj;

    move-object/from16 v16, v0

    move-object/from16 v2, p1

    move-object/from16 v9, p0

    move-object/from16 v12, p0

    .line 744
    invoke-virtual/range {v2 .. v16}, Ltv/periscope/android/ui/broadcast/av;->a(Landroid/content/res/Resources;Landroid/os/Handler;Ltv/periscope/android/chat/a;Ltv/periscope/android/ui/broadcast/ar;Ltv/periscope/android/api/PsUser;ZLtv/periscope/android/ui/chat/r;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Ltv/periscope/android/ui/chat/w;Lcyw;Lcxm;Ltv/periscope/android/ui/chat/x;Ltv/periscope/android/ui/chat/aj;)Ltv/periscope/android/ui/chat/m;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    .line 749
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    invoke-virtual {v2, v3}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setSendCommentDelegate(Ltv/periscope/android/ui/chat/ai;)V

    .line 750
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    invoke-virtual {v2, v3}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setPunishmentStatusDelegate(Ltv/periscope/android/ui/chat/ag;)V

    .line 751
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->ag:Ltv/periscope/android/ui/broadcast/au;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v4}, Ltv/periscope/android/library/c;->v()Ldae;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ltv/periscope/android/ui/broadcast/au;->a(Lcyw;Ldae;)V

    .line 752
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->ag:Ltv/periscope/android/ui/broadcast/au;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    invoke-virtual {v2, v3}, Ltv/periscope/android/ui/broadcast/au;->a(Ltv/periscope/android/ui/chat/ah;)V

    .line 753
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/af;->ag:Ltv/periscope/android/ui/broadcast/au;

    new-instance v3, Ltv/periscope/android/ui/broadcast/af$15;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v1}, Ltv/periscope/android/ui/broadcast/af$15;-><init>(Ltv/periscope/android/ui/broadcast/af;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ltv/periscope/android/ui/broadcast/au;->a(Ltv/periscope/android/view/CarouselView$a;)V

    .line 761
    return-void

    .line 742
    :cond_0
    const/4 v8, 0x0

    goto/16 :goto_0
.end method

.method private aA()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n",
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 1649
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->m:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1650
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->r()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1664
    :goto_0
    return-void

    .line 1652
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->m:Landroid/widget/TextView;

    const-string/jumbo v1, "LHLS"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1656
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->m:Landroid/widget/TextView;

    const-string/jumbo v1, "RTMP"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1660
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->m:Landroid/widget/TextView;

    const-string/jumbo v1, "HLS"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1650
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private aB()V
    .locals 3

    .prologue
    .line 1832
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->X:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->X:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1842
    :cond_0
    :goto_0
    return-void

    .line 1835
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ax:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 1836
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ax:Ljava/util/ArrayList;

    .line 1838
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ax:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1839
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ax:Ljava/util/ArrayList;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1841
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->X:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczy;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->ax:Ljava/util/ArrayList;

    invoke-interface {v0, v1, v2}, Lczy;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private aC()V
    .locals 4

    .prologue
    .line 2393
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aX:Z

    if-eqz v0, :cond_1

    .line 2406
    :cond_0
    :goto_0
    return-void

    .line 2397
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    if-eqz v0, :cond_2

    .line 2398
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/af;->bf:J

    invoke-static {v0, v1, v2, v3}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/summary/b;Ltv/periscope/model/af;J)V

    .line 2401
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aX:Z

    .line 2402
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->n:Ltv/periscope/android/ui/broadcast/av;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/av;->j()V

    .line 2403
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v0, :cond_0

    .line 2404
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/af;->be:J

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->b:Ltv/periscope/android/player/e;

    invoke-interface {v2}, Ltv/periscope/android/player/e;->l()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/af;->be:J

    goto :goto_0
.end method

.method private aD()V
    .locals 2

    .prologue
    .line 2457
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->t()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2474
    :cond_0
    :goto_0
    return-void

    .line 2460
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 2462
    :goto_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v1}, Ltv/periscope/android/player/d;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v1}, Ltv/periscope/android/player/d;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2465
    :cond_2
    if-eqz v0, :cond_5

    .line 2466
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aI()V

    .line 2473
    :cond_3
    :goto_2
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aE()V

    goto :goto_0

    .line 2460
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2467
    :cond_5
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    if-eqz v0, :cond_3

    .line 2468
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->b(Ltv/periscope/model/p;)V

    .line 2469
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->R()V

    goto :goto_2
.end method

.method private aE()V
    .locals 2

    .prologue
    .line 2477
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2483
    :goto_0
    return-void

    .line 2480
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->V()V

    .line 2481
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->b(Landroid/view/View;)V

    .line 2482
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->d(Z)V

    goto :goto_0
.end method

.method private aF()V
    .locals 2

    .prologue
    .line 2489
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2490
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->V()V

    .line 2492
    :cond_0
    return-void
.end method

.method private aG()V
    .locals 0

    .prologue
    .line 2503
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aH()V

    .line 2504
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->W()V

    .line 2505
    return-void
.end method

.method private aH()V
    .locals 2

    .prologue
    .line 2508
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->c(Landroid/view/View;)V

    .line 2509
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->d(Z)V

    .line 2510
    return-void
.end method

.method private aI()V
    .locals 2

    .prologue
    .line 2520
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2533
    :goto_0
    return-void

    .line 2525
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->q()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2526
    const/high16 v0, 0x42b40000    # 90.0f

    .line 2532
    :goto_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v1, v0}, Ltv/periscope/android/player/c;->a(F)V

    goto :goto_0

    .line 2527
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->q()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 2528
    const/high16 v0, 0x43870000    # 270.0f

    goto :goto_1

    .line 2530
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private aJ()Z
    .locals 1

    .prologue
    .line 2536
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    invoke-virtual {v0}, Ltv/periscope/model/af;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    .line 2537
    invoke-virtual {v0}, Ltv/periscope/model/p;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2536
    :goto_0
    return v0

    .line 2537
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aK()V
    .locals 2

    .prologue
    .line 2541
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2577
    :cond_0
    :goto_0
    return-void

    .line 2545
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2546
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->a:Ltv/periscope/android/player/PlayMode;

    if-eq v0, v1, :cond_0

    .line 2551
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v0, :cond_5

    .line 2552
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2553
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aG()V

    goto :goto_0

    .line 2554
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aJ()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2555
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->c(Z)V

    .line 2556
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->J()V

    goto :goto_0

    .line 2557
    :cond_4
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2558
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aG()V

    goto :goto_0

    .line 2561
    :cond_5
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2562
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aG()V

    goto :goto_0

    .line 2566
    :cond_6
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_7

    .line 2568
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->k()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    if-eqz v0, :cond_0

    .line 2570
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->h()V

    goto :goto_0

    .line 2574
    :cond_7
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->h()V

    goto :goto_0
.end method

.method private aL()V
    .locals 1

    .prologue
    .line 2593
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->d(Ltv/periscope/android/analytics/summary/b;)V

    .line 2594
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ay:Ljava/io/File;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->a(Ljava/io/File;)V

    .line 2595
    return-void
.end method

.method private aa()V
    .locals 2

    .prologue
    .line 678
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aK:Z

    if-eqz v0, :cond_0

    .line 679
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v1, Ltv/periscope/android/library/f$g;->tweaks:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 680
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v1, Ltv/periscope/android/library/f$g;->heart_tweaks:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->j:Landroid/widget/TextView;

    .line 681
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v1, Ltv/periscope/android/library/f$g;->comment_tweaks:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->k:Landroid/widget/TextView;

    .line 682
    new-instance v0, Ltv/periscope/android/ui/broadcast/af$d;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/af$d;-><init>(Ltv/periscope/android/ui/broadcast/af;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->av:Landroid/os/Handler;

    .line 686
    :goto_0
    return-void

    .line 684
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    sget v1, Ltv/periscope/android/library/f$g;->tweaks:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private ab()V
    .locals 2

    .prologue
    .line 710
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->Q:Ltv/periscope/android/view/t;

    invoke-interface {v0}, Ltv/periscope/android/view/t;->f()V

    .line 711
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aZ:Z

    if-eqz v0, :cond_1

    .line 723
    :cond_0
    :goto_0
    return-void

    .line 714
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-static {v0}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 718
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aZ:Z

    .line 719
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->Z()Lde/greenrobot/event/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 720
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->Z()Lde/greenrobot/event/c;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->an:Ltv/periscope/android/ui/broadcast/h;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 721
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->Z()Lde/greenrobot/event/c;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->g:Ltv/periscope/android/ui/user/l;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 722
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->Z()Lde/greenrobot/event/c;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ak:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private ac()V
    .locals 2

    .prologue
    .line 726
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->Q:Ltv/periscope/android/view/t;

    invoke-interface {v0}, Ltv/periscope/android/view/t;->g()V

    .line 727
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->Z()Lde/greenrobot/event/c;

    move-result-object v0

    .line 728
    iget-boolean v1, p0, Ltv/periscope/android/ui/broadcast/af;->aZ:Z

    if-nez v1, :cond_0

    .line 736
    :goto_0
    return-void

    .line 731
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Ltv/periscope/android/ui/broadcast/af;->aZ:Z

    .line 732
    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 733
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->an:Ltv/periscope/android/ui/broadcast/h;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 734
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->g:Ltv/periscope/android/ui/user/l;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 735
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ak:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private ad()V
    .locals 6

    .prologue
    .line 766
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PeriscopePlayer"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 767
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    .line 768
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "periscope_player_logs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 769
    new-instance v2, Lczz;

    new-instance v3, Ldaa$a;

    const-string/jumbo v4, "periscope_player_log"

    const-string/jumbo v5, ".txt"

    invoke-direct {v3, v0, v4, v5, v1}, Ldaa$a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lczz;-><init>(Ldaa$a;)V

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    .line 771
    invoke-static {}, Ldac;->a()Ldac;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    invoke-virtual {v0, v1}, Ldac;->a(Ldad;)Ldad;

    .line 772
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    invoke-static {v0}, Ltv/periscope/android/util/t;->a(Ldad;)V

    .line 773
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "=================================================="

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 774
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Android OS Version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ltv/periscope/android/util/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 775
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Model Info: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ltv/periscope/android/util/j;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 776
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "App Version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    invoke-static {v2}, Ltv/periscope/android/util/ak;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 777
    return-void
.end method

.method private ae()V
    .locals 4

    .prologue
    .line 847
    new-instance v0, Ltv/periscope/android/view/j;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->as:Ltv/periscope/android/ui/chat/g;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->aw:Ltv/periscope/android/ui/chat/b;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-direct {v0, v1, v2, v3}, Ltv/periscope/android/view/j;-><init>(Ltv/periscope/android/ui/chat/l;Ltv/periscope/android/ui/chat/a;Lcyw;)V

    .line 849
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ag:Ltv/periscope/android/ui/broadcast/au;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/au;->a(Ltv/periscope/android/view/i;)V

    .line 850
    return-void
.end method

.method private af()V
    .locals 7

    .prologue
    .line 854
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 855
    if-eqz v0, :cond_0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-virtual {v0}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcyw;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    instance-of v0, v0, Ltv/periscope/android/ui/broadcast/k;

    if-nez v0, :cond_0

    .line 857
    new-instance v0, Ltv/periscope/android/ui/broadcast/k;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/af;->an:Ltv/periscope/android/ui/broadcast/h;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 858
    invoke-interface {v5}, Ltv/periscope/android/library/c;->i()Ltv/periscope/android/ui/broadcast/y;

    move-result-object v5

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/af;->ae:Ljava/lang/String;

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    :goto_0
    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/ui/broadcast/k;-><init>(Landroid/content/Context;Lcyw;Lcyn;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/android/ui/broadcast/y;Z)V

    .line 859
    iget-boolean v1, p0, Ltv/periscope/android/ui/broadcast/af;->aM:Z

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/k;->a(Z)V

    .line 860
    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    .line 861
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/g;->a(Ltv/periscope/android/ui/broadcast/j;)V

    .line 863
    new-instance v0, Lcxx;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 864
    invoke-interface {v2}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->an:Ltv/periscope/android/ui/broadcast/h;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    invoke-direct {v0, v1, v2, v3, v4}, Lcxx;-><init>(Ltv/periscope/android/view/b;Ldae;Ltv/periscope/android/ui/broadcast/h;Lcyn;)V

    .line 865
    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ai:Ltv/periscope/android/ui/broadcast/b;

    .line 867
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->an:Ltv/periscope/android/ui/broadcast/h;

    iget-boolean v1, p0, Ltv/periscope/android/ui/broadcast/af;->aF:Z

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/h;->a(Z)V

    .line 868
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/j;->d(Ljava/lang/String;)V

    .line 869
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->n()V

    .line 870
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_1

    .line 871
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aj()V

    .line 873
    :cond_1
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aP:Z

    if-eqz v0, :cond_3

    .line 874
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aE()V

    .line 875
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->R()V

    .line 879
    :goto_1
    return-void

    .line 858
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 877
    :cond_3
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aH()V

    goto :goto_1
.end method

.method private ag()V
    .locals 5

    .prologue
    .line 895
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/af;->bh:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 896
    new-instance v0, Lcxv;

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/af;->bh:J

    invoke-direct {v0, v2, v3}, Lcxv;-><init>(J)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->au:Lcxv;

    .line 898
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    iget-boolean v2, p0, Ltv/periscope/android/ui/broadcast/af;->aN:Z

    invoke-interface {v0, v1, p0, v2}, Ltv/periscope/android/player/d;->a(Landroid/content/Context;Ltv/periscope/android/player/a;Z)V

    .line 899
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->autoPlay:Z

    if-eqz v0, :cond_1

    .line 901
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/af;->bg:J

    invoke-interface {v0, v2, v3}, Ltv/periscope/android/player/d;->a(J)V

    .line 903
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aC:Lcxc;

    if-nez v0, :cond_2

    .line 904
    new-instance v0, Lcxc;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->aA:Ltv/periscope/model/u;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/af;->aB:Ltv/periscope/android/chat/g;

    invoke-direct {v0, v1, v2, v3, v4}, Lcxc;-><init>(Ltv/periscope/model/p;Ltv/periscope/model/af;Ltv/periscope/model/u;Ltv/periscope/android/chat/g;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aC:Lcxc;

    .line 906
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->b:Ltv/periscope/android/player/e;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->aC:Lcxc;

    invoke-interface {v0, v1}, Ltv/periscope/android/player/e;->a(Lcxc;)V

    .line 907
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->c(Z)V

    .line 908
    return-void
.end method

.method private ah()V
    .locals 2

    .prologue
    .line 1025
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->G:Ltv/periscope/android/util/ae;

    invoke-virtual {v0}, Ltv/periscope/android/util/ae;->b()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ay:Ljava/io/File;

    .line 1026
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ay:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 1029
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->j()V

    .line 1030
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->q:Ltv/periscope/android/ui/broadcast/ar;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ar;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1032
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->q:Ltv/periscope/android/ui/broadcast/ar;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ay:Ljava/io/File;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/ar;->a(Ljava/io/File;)V

    .line 1035
    :cond_0
    return-void
.end method

.method private ai()V
    .locals 2

    .prologue
    .line 1082
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->setActionButtonVisibility(Z)V

    .line 1083
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d()V

    .line 1084
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->W()V

    .line 1085
    return-void
.end method

.method private aj()V
    .locals 1

    .prologue
    .line 1185
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->Q()V

    .line 1186
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->c()V

    .line 1187
    return-void
.end method

.method private ak()V
    .locals 7

    .prologue
    .line 1255
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->b:Ltv/periscope/android/player/e;

    iget-boolean v4, p0, Ltv/periscope/android/ui/broadcast/af;->aK:Z

    if-eqz v4, :cond_0

    move-object v5, p0

    :goto_0
    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    .line 1256
    invoke-interface {v4}, Ltv/periscope/android/player/d;->o()Z

    move-result v6

    move-object v4, p0

    .line 1255
    invoke-virtual/range {v0 .. v6}, Ltv/periscope/android/ui/broadcast/v;->a(Lcyw;Ltv/periscope/android/player/PlayMode;Ltv/periscope/android/player/e;Ltv/periscope/android/chat/h;Ltv/periscope/android/chat/i$a;Z)V

    .line 1257
    return-void

    .line 1255
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private al()Z
    .locals 1

    .prologue
    .line 1260
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->d()Z

    move-result v0

    return v0
.end method

.method private am()Z
    .locals 1

    .prologue
    .line 1265
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aA:Ltv/periscope/model/u;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private an()V
    .locals 6

    .prologue
    .line 1269
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ad:Lcxa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    .line 1270
    invoke-interface {v0}, Ltv/periscope/android/player/d;->l()J

    move-result-wide v0

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->d()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 1271
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ad:Lcxa;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->l()J

    move-result-wide v2

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v4}, Ltv/periscope/android/player/d;->t()D

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcxa;->a(Ljava/lang/String;JD)V

    .line 1273
    :cond_0
    return-void
.end method

.method private ao()V
    .locals 2

    .prologue
    .line 1375
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1376
    return-void
.end method

.method private ap()V
    .locals 2

    .prologue
    .line 1382
    invoke-static {}, Ldac;->a()Ldac;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    invoke-virtual {v0, v1}, Ldac;->b(Ldad;)Z

    .line 1383
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    invoke-static {v0}, Ltv/periscope/android/util/t;->b(Ldad;)Z

    .line 1384
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    invoke-virtual {v0}, Lczz;->a()V

    .line 1385
    return-void
.end method

.method private aq()Z
    .locals 1

    .prologue
    .line 1417
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->q:Ltv/periscope/android/ui/broadcast/ar;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ar;->b()Z

    move-result v0

    return v0
.end method

.method private ar()Z
    .locals 1

    .prologue
    .line 1421
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->Q:Ltv/periscope/android/view/t;

    invoke-interface {v0}, Ltv/periscope/android/view/t;->j()Z

    move-result v0

    return v0
.end method

.method private as()Z
    .locals 1

    .prologue
    .line 1425
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->al:Ltv/periscope/android/ui/user/UserPickerSheet;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->c()Z

    move-result v0

    return v0
.end method

.method private at()Z
    .locals 1

    .prologue
    .line 1429
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ag:Ltv/periscope/android/ui/broadcast/au;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/au;->f()Z

    move-result v0

    return v0
.end method

.method private au()Z
    .locals 1

    .prologue
    .line 1433
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/f;->f()Z

    move-result v0

    return v0
.end method

.method private av()Z
    .locals 1

    .prologue
    .line 1437
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->at()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ag:Ltv/periscope/android/ui/broadcast/au;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/au;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aw()V
    .locals 2

    .prologue
    .line 1557
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ap:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1558
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aY:Z

    .line 1559
    return-void
.end method

.method private ax()V
    .locals 1

    .prologue
    .line 1571
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->n:Ltv/periscope/android/ui/broadcast/av;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/av;->i()V

    .line 1572
    return-void
.end method

.method private ay()Lcxe;
    .locals 1

    .prologue
    .line 1613
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ac:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1614
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ac:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxe;

    .line 1616
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private az()Ltv/periscope/android/permissions/a;
    .locals 1

    .prologue
    .line 1621
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->Z:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1622
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->Z:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/permissions/a;

    .line 1624
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/android/ui/chat/m;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    return-object v0
.end method

.method private b(Landroid/app/Activity;)Ltv/periscope/android/view/t;
    .locals 10

    .prologue
    .line 612
    new-instance v0, Ltv/periscope/android/ui/broadcast/af$10;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v3

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    const/4 v5, 0x0

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/af;->R:Ltv/periscope/android/analytics/summary/b;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v7

    iget-object v8, p0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 613
    invoke-interface {v1}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v9

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Ltv/periscope/android/ui/broadcast/af$10;-><init>(Ltv/periscope/android/ui/broadcast/af;Landroid/content/Context;Ltv/periscope/android/api/ApiManager;Lcyw;Ltv/periscope/android/view/ab$a;Lcsa;Ldae;Landroid/view/ViewGroup;Lde/greenrobot/event/c;)V

    .line 612
    return-object v0
.end method

.method private b(Ltv/periscope/model/p;)V
    .locals 3

    .prologue
    .line 1141
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ad:Lcxa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ad:Lcxa;

    invoke-virtual {p1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcxa;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ad:Lcxa;

    invoke-virtual {p1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ltv/periscope/android/ui/broadcast/af$2;

    invoke-direct {v2, p0}, Ltv/periscope/android/ui/broadcast/af$2;-><init>(Ltv/periscope/android/ui/broadcast/af;)V

    invoke-virtual {v0, v1, v2}, Lcxa;->a(Ljava/lang/String;Ltv/periscope/android/util/f$a;)V

    .line 1153
    :goto_0
    return-void

    .line 1151
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-virtual {p1}, Ltv/periscope/model/p;->r()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->setThumbnail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Ltv/periscope/android/ui/broadcast/af;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private c(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 623
    new-instance v0, Ltv/periscope/android/ui/broadcast/af$11;

    invoke-direct {v0, p0, p1}, Ltv/periscope/android/ui/broadcast/af$11;-><init>(Ltv/periscope/android/ui/broadcast/af;Landroid/app/Activity;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->t:Ljava/lang/Runnable;

    .line 630
    new-instance v0, Ltv/periscope/android/ui/broadcast/af$12;

    invoke-direct {v0, p0, p1}, Ltv/periscope/android/ui/broadcast/af$12;-><init>(Ltv/periscope/android/ui/broadcast/af;Landroid/app/Activity;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->u:Ljava/lang/Runnable;

    .line 637
    new-instance v0, Ltv/periscope/android/ui/broadcast/af$13;

    invoke-direct {v0, p0, p1}, Ltv/periscope/android/ui/broadcast/af$13;-><init>(Ltv/periscope/android/ui/broadcast/af;Landroid/app/Activity;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ap:Ljava/lang/Runnable;

    .line 647
    new-instance v0, Ltv/periscope/android/ui/broadcast/af$14;

    invoke-direct {v0, p0, p1}, Ltv/periscope/android/ui/broadcast/af$14;-><init>(Ltv/periscope/android/ui/broadcast/af;Landroid/app/Activity;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->v:Ljava/lang/Runnable;

    .line 675
    return-void
.end method

.method private c(Ltv/periscope/android/player/PlayMode;)V
    .locals 11

    .prologue
    .line 780
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->n:Ltv/periscope/android/ui/broadcast/av;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/av;->k()V

    .line 781
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v2

    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->Z()Lde/greenrobot/event/c;

    move-result-object v3

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 782
    invoke-interface {v0}, Ltv/periscope/android/library/c;->b()Lretrofit/RestAdapter$LogLevel;

    move-result-object v8

    iget-object v9, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    iget-object v10, p0, Ltv/periscope/android/ui/broadcast/af;->S:Ltv/periscope/android/video/StreamMode;

    move-object v0, p0

    move-object v1, p0

    move-object v7, p1

    .line 781
    invoke-static/range {v0 .. v10}, Ltv/periscope/android/ui/broadcast/av;->a(Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/player/d;Ltv/periscope/android/video/StreamMode;)Ltv/periscope/android/ui/broadcast/av;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->n:Ltv/periscope/android/ui/broadcast/av;

    .line 784
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->n:Ltv/periscope/android/ui/broadcast/av;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/android/ui/broadcast/av;Ljava/lang/String;)V

    .line 786
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->n:Ltv/periscope/android/ui/broadcast/av;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    iget-boolean v2, p0, Ltv/periscope/android/ui/broadcast/af;->aG:Z

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/af;->aw:Ltv/periscope/android/ui/chat/b;

    move-object v3, p0

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Ltv/periscope/android/ui/broadcast/av;->a(Lcyw;ZLtv/periscope/android/ui/chat/o$c;Ltv/periscope/android/ui/chat/o$a;Ltv/periscope/android/ui/chat/b;)V

    .line 788
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setModeratorSelectionListener(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$b;)V

    .line 789
    return-void
.end method

.method private c(Ltv/periscope/model/p;)V
    .locals 6

    .prologue
    .line 1174
    if-eqz p1, :cond_1

    .line 1175
    invoke-virtual {p1}, Ltv/periscope/model/p;->I()J

    move-result-wide v0

    .line 1176
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v2

    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-nez v3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :cond_0
    invoke-static {v2, v0, v1}, Ldag;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    .line 1180
    :goto_0
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v1, v0}, Ltv/periscope/android/player/c;->b(Ljava/lang/String;)V

    .line 1181
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a(Ljava/lang/String;)V

    .line 1182
    return-void

    .line 1178
    :cond_1
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Ldag;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic d(Ltv/periscope/android/ui/broadcast/af;)Lcyn;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    return-object v0
.end method

.method private d(Ltv/periscope/model/chat/Message;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1191
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1199
    :cond_0
    :goto_0
    return-object v0

    .line 1196
    :cond_1
    const-string/jumbo v1, "*"

    .line 1197
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1198
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Ltv/periscope/android/library/f$l;->ps__superfan_of_action_sheet_text:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1199
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->L()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private d(J)V
    .locals 1

    .prologue
    .line 1587
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aX:Z

    .line 1588
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/player/d;->b(J)V

    .line 1591
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->B:Z

    .line 1592
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->c()V

    .line 1593
    return-void
.end method

.method private d(Ltv/periscope/android/player/PlayMode;)V
    .locals 2

    .prologue
    .line 882
    sget-object v0, Ltv/periscope/android/ui/broadcast/af$8;->a:[I

    invoke-virtual {p1}, Ltv/periscope/android/player/PlayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 892
    :goto_0
    return-void

    .line 884
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->e:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    .line 885
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/chat/ChatState;->a:Ltv/periscope/android/ui/chat/ChatState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setChatState(Ltv/periscope/android/ui/chat/ChatState;)V

    goto :goto_0

    .line 889
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->g:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    goto :goto_0

    .line 882
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic e(Ltv/periscope/android/ui/broadcast/af;)Lcyw;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    return-object v0
.end method

.method private e(Z)V
    .locals 2

    .prologue
    .line 1156
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aD:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aD:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 1157
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aD:Ljava/lang/Boolean;

    .line 1159
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_2

    .line 1160
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ai()V

    .line 1170
    :cond_1
    :goto_0
    return-void

    .line 1163
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->setActionButtonVisibility(Z)V

    .line 1164
    if-eqz p1, :cond_3

    .line 1165
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->V()V

    goto :goto_0

    .line 1167
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->W()V

    goto :goto_0
.end method

.method static synthetic f(Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/android/ui/chat/b;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aw:Ltv/periscope/android/ui/chat/b;

    return-object v0
.end method

.method private f(Z)V
    .locals 1

    .prologue
    .line 1575
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1576
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0, p1}, Ltv/periscope/android/player/d;->a(Z)V

    .line 1578
    :cond_0
    if-eqz p1, :cond_1

    .line 1579
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->b()V

    .line 1583
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->d()V

    .line 1584
    return-void

    .line 1581
    :cond_1
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aC()V

    goto :goto_0
.end method

.method static synthetic g(Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/android/ui/broadcast/moderator/f;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ak:Ltv/periscope/android/ui/broadcast/moderator/f;

    return-object v0
.end method

.method static synthetic h(Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/model/af;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    return-object v0
.end method

.method static synthetic i(Ltv/periscope/android/ui/broadcast/af;)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->Y()V

    return-void
.end method

.method private m(Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 833
    new-instance v0, Ltv/periscope/android/ui/chat/b;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-direct {v0, v1, p1}, Ltv/periscope/android/ui/chat/b;-><init>(Lcyw;Ljava/lang/String;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aw:Ltv/periscope/android/ui/chat/b;

    .line 834
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v9

    .line 835
    new-instance v0, Ltv/periscope/android/ui/chat/d;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    .line 836
    invoke-interface {v2}, Lcyw;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-interface {v3}, Lcyw;->e()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    iget-boolean v5, p0, Ltv/periscope/android/ui/broadcast/af;->aG:Z

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/af;->aw:Ltv/periscope/android/ui/chat/b;

    iget-object v7, p0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    .line 837
    invoke-interface {v7}, Ltv/periscope/android/library/c;->v()Ldae;

    move-result-object v7

    iget-object v8, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-direct/range {v0 .. v9}, Ltv/periscope/android/ui/chat/d;-><init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;ZZLtv/periscope/android/ui/chat/a;Ldae;Lcyw;Ljava/lang/String;)V

    .line 838
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->W:Ltv/periscope/android/ui/chat/ak;

    invoke-interface {v1, p0}, Ltv/periscope/android/ui/chat/ak;->a(Ltv/periscope/android/ui/chat/k;)V

    .line 840
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i()V

    .line 841
    new-instance v1, Ltv/periscope/android/ui/chat/g;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/af;->U:Ltv/periscope/android/ui/chat/y;

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/af;->W:Ltv/periscope/android/ui/chat/ak;

    move-object v3, p0

    move-object v4, v0

    invoke-direct/range {v1 .. v6}, Ltv/periscope/android/ui/chat/g;-><init>(Landroid/content/Context;Ltv/periscope/android/ui/chat/k;Ltv/periscope/android/view/ak;Ltv/periscope/android/ui/chat/y;Ltv/periscope/android/ui/chat/ak;)V

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->as:Ltv/periscope/android/ui/chat/g;

    .line 843
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->as:Ltv/periscope/android/ui/chat/g;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setChatMessageAdapter(Ltv/periscope/android/ui/chat/g;)V

    .line 844
    return-void
.end method


# virtual methods
.method A()V
    .locals 2

    .prologue
    .line 796
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->setSystemUiVisibility(I)V

    .line 797
    return-void
.end method

.method public B()V
    .locals 2

    .prologue
    .line 1354
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1355
    if-eqz v0, :cond_0

    .line 1356
    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 1357
    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/Application;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 1359
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->g()V

    .line 1360
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->n:Ltv/periscope/android/ui/broadcast/av;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/av;->k()V

    .line 1361
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1362
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->s()V

    .line 1364
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aX:Z

    .line 1365
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i()V

    .line 1366
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->setAdapter(Ltv/periscope/android/ui/broadcast/g;)V

    .line 1367
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->G:Ltv/periscope/android/util/ae;

    invoke-virtual {v0}, Ltv/periscope/android/util/ae;->a()V

    .line 1368
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->I:Ltv/periscope/android/network/ConnectivityChangeReceiver;

    invoke-virtual {v0}, Ltv/periscope/android/network/ConnectivityChangeReceiver;->b()V

    .line 1369
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ao()V

    .line 1370
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ac()V

    .line 1371
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ap()V

    .line 1372
    return-void
.end method

.method public C()Z
    .locals 1

    .prologue
    .line 1394
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aq()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1395
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->q:Ltv/periscope/android/ui/broadcast/ar;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ar;->a()V

    .line 1413
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 1396
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ar()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1397
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->Q:Ltv/periscope/android/view/t;

    invoke-interface {v0}, Ltv/periscope/android/view/t;->i()V

    goto :goto_0

    .line 1398
    :cond_1
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->as()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1399
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->al:Ltv/periscope/android/ui/user/UserPickerSheet;

    invoke-virtual {v0}, Ltv/periscope/android/ui/user/UserPickerSheet;->b()V

    goto :goto_0

    .line 1400
    :cond_2
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->au()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1401
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/f;->e()V

    goto :goto_0

    .line 1402
    :cond_3
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->av()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1403
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ag:Ltv/periscope/android/ui/broadcast/au;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/au;->b()V

    goto :goto_0

    .line 1404
    :cond_4
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->at()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1405
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ag:Ltv/periscope/android/ui/broadcast/au;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/au;->e()V

    goto :goto_0

    .line 1406
    :cond_5
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->D()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->i()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->h()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1407
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aG()V

    goto :goto_0

    .line 1408
    :cond_6
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aE:Z

    if-eqz v0, :cond_7

    .line 1409
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->Y()V

    goto :goto_0

    .line 1411
    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public D()Z
    .locals 2

    .prologue
    .line 1441
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method E()Z
    .locals 1

    .prologue
    .line 1445
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aq()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1446
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1447
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->as()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1448
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->au()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1449
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->at()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1450
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1445
    :goto_0
    return v0

    .line 1450
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public F()Z
    .locals 1

    .prologue
    .line 1668
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    return v0
.end method

.method public G()V
    .locals 1

    .prologue
    .line 1703
    sget-object v0, Ltv/periscope/android/ui/chat/ChatState;->g:Ltv/periscope/android/ui/chat/ChatState;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/android/ui/chat/ChatState;)V

    .line 1704
    return-void
.end method

.method public H()V
    .locals 1

    .prologue
    .line 1715
    sget-object v0, Ltv/periscope/android/ui/chat/ChatState;->h:Ltv/periscope/android/ui/chat/ChatState;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/android/ui/chat/ChatState;)V

    .line 1716
    return-void
.end method

.method I()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1805
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v0, :cond_3

    .line 1806
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1807
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aG()V

    .line 1808
    invoke-virtual {p0, v2}, Ltv/periscope/android/ui/broadcast/af;->c(Z)V

    .line 1821
    :cond_0
    :goto_0
    return-void

    .line 1809
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1810
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/summary/b;)V

    .line 1811
    invoke-direct {p0, v1}, Ltv/periscope/android/ui/broadcast/af;->f(Z)V

    .line 1812
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aD()V

    .line 1813
    invoke-virtual {p0, v1}, Ltv/periscope/android/ui/broadcast/af;->c(Z)V

    goto :goto_0

    .line 1815
    :cond_2
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->J()V

    .line 1816
    invoke-virtual {p0, v2}, Ltv/periscope/android/ui/broadcast/af;->c(Z)V

    goto :goto_0

    .line 1818
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_0

    .line 1819
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aG()V

    goto :goto_0
.end method

.method J()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 1848
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->bc:Z

    if-eqz v0, :cond_1

    .line 1870
    :cond_0
    :goto_0
    return-void

    .line 1851
    :cond_1
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->am()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1852
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1855
    :cond_2
    iput-boolean v4, p0, Ltv/periscope/android/ui/broadcast/af;->bc:Z

    .line 1856
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1857
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/af;->bg:J

    invoke-interface {v0, v2, v3}, Ltv/periscope/android/player/d;->a(J)V

    .line 1859
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/af;->bg:J

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/v;->b(J)V

    .line 1862
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->al()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/af;->bg:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_4

    .line 1863
    iput-boolean v4, p0, Ltv/periscope/android/ui/broadcast/af;->B:Z

    .line 1866
    :cond_4
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/af;->bg:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_0

    .line 1868
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/chat/MessageType;->r:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ltv/periscope/model/chat/Message;)V

    goto :goto_0
.end method

.method public K()V
    .locals 1

    .prologue
    .line 1937
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l()V

    .line 1938
    return-void
.end method

.method public L()V
    .locals 1

    .prologue
    .line 1942
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->m()V

    .line 1943
    return-void
.end method

.method public M()V
    .locals 1

    .prologue
    .line 1947
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->n()V

    .line 1948
    return-void
.end method

.method public N()V
    .locals 1

    .prologue
    .line 1952
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->h()V

    .line 1953
    return-void
.end method

.method public O()V
    .locals 1

    .prologue
    .line 1963
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->e(Ltv/periscope/android/analytics/summary/b;)V

    .line 1964
    return-void
.end method

.method public P()V
    .locals 1

    .prologue
    .line 1968
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->f(Ltv/periscope/android/analytics/summary/b;)V

    .line 1969
    return-void
.end method

.method public Q()V
    .locals 0

    .prologue
    .line 2228
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->j()V

    .line 2229
    return-void
.end method

.method public R()V
    .locals 2

    .prologue
    .line 2435
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    sget-object v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/j;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;)V

    .line 2436
    return-void
.end method

.method public S()V
    .locals 2

    .prologue
    .line 2440
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    sget-object v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;->c:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/j;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;)V

    .line 2441
    return-void
.end method

.method public T()V
    .locals 2

    .prologue
    .line 2445
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 2446
    if-eqz v0, :cond_0

    .line 2447
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/j;->b(Ltv/periscope/model/p;)V

    .line 2449
    :cond_0
    return-void
.end method

.method public U()V
    .locals 2

    .prologue
    .line 2453
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    sget-object v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;->a:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/j;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;)V

    .line 2454
    return-void
.end method

.method V()V
    .locals 1

    .prologue
    .line 2499
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->n()V

    .line 2500
    return-void
.end method

.method W()V
    .locals 1

    .prologue
    .line 2513
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aX:Z

    .line 2514
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2515
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->j()V

    .line 2517
    :cond_0
    return-void
.end method

.method public a()V
    .locals 0

    .prologue
    .line 1247
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aK()V

    .line 1248
    return-void
.end method

.method a(I)V
    .locals 1

    .prologue
    .line 2580
    packed-switch p1, :pswitch_data_0

    .line 2590
    :cond_0
    :goto_0
    return-void

    .line 2582
    :pswitch_0
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aR:Z

    if-eqz v0, :cond_0

    .line 2585
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aR:Z

    .line 2586
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->a(Ljava/io/File;)V

    goto :goto_0

    .line 2580
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 1047
    const/16 v0, 0xcee

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1048
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ah()V

    .line 1049
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aL()V

    .line 1051
    :cond_0
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1720
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ay()Lcxe;

    move-result-object v0

    .line 1721
    if-eqz v0, :cond_0

    .line 1722
    new-instance v1, Ltv/periscope/android/library/PeriscopeException;

    invoke-direct {v1, p2}, Ltv/periscope/android/library/PeriscopeException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcxe;->a(Ltv/periscope/android/library/PeriscopeException;)V

    .line 1725
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 1747
    :goto_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->Y()V

    .line 1748
    :goto_1
    return-void

    .line 1729
    :sswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to load broadcast "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 1730
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->O()V

    .line 1731
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    invoke-static {v0, p2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1735
    :sswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__cannot_play:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1736
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "video unplayable, finishing."

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 1737
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Video unplayable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lf;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1741
    :sswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to load broadcast due to 404 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 1742
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->O()V

    .line 1743
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    invoke-static {v0, p2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 1725
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x194 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(IZ)V
    .locals 1

    .prologue
    .line 1879
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(IZ)V

    .line 1880
    return-void
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 2349
    instance-of v0, p1, Lcom/google/android/exoplayer/drm/UnsupportedDrmException;

    if-eqz v0, :cond_0

    .line 2350
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    const-string/jumbo v1, "Unsupported DRM exception"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2352
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1003
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->av:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1005
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->av:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1006
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1007
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->av:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1008
    return-void
.end method

.method public a(Ljava/lang/String;JZ)V
    .locals 8

    .prologue
    .line 2024
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->d:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v6, 0x1

    .line 2025
    :goto_0
    if-eqz v6, :cond_3

    if-nez p4, :cond_3

    .line 2036
    :cond_1
    :goto_1
    return-void

    .line 2024
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 2030
    :cond_3
    if-eqz p1, :cond_1

    .line 2035
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    move-object v3, p1

    move-wide v4, p2

    invoke-interface/range {v1 .. v6}, Lcyw;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 962
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    if-nez v0, :cond_0

    .line 999
    :goto_0
    return-void

    .line 965
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->J:Ltv/periscope/android/ui/broadcast/aq;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v1}, Ltv/periscope/model/p;->D()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ltv/periscope/android/ui/broadcast/af$16;

    invoke-direct {v5, p0, p3}, Ltv/periscope/android/ui/broadcast/af$16;-><init>(Ltv/periscope/android/ui/broadcast/af;Ljava/lang/String;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-interface/range {v0 .. v5}, Ltv/periscope/android/ui/broadcast/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/ui/broadcast/aq$a;)V

    goto :goto_0
.end method

.method a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2071
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/v;->a(Ljava/util/ArrayList;)V

    .line 2072
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/chatman/api/Occupant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2000
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    if-eqz v0, :cond_0

    .line 2001
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/chat/m;->a(Ljava/util/List;)V

    .line 2003
    :cond_0
    return-void
.end method

.method public a(Ltv/periscope/android/player/PlayMode;)V
    .locals 3

    .prologue
    .line 805
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->a:Ltv/periscope/android/player/PlayMode;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    if-ne v0, p1, :cond_1

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aV:Z

    if-eqz v0, :cond_1

    .line 830
    :cond_0
    :goto_0
    return-void

    .line 808
    :cond_1
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ab()V

    .line 809
    sget-object v0, Ltv/periscope/android/player/PlayMode;->a:Ltv/periscope/android/player/PlayMode;

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    if-nez v0, :cond_3

    .line 812
    :cond_2
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af;->ar:Ltv/periscope/android/player/PlayMode;

    .line 813
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 814
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 815
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v1

    invoke-interface {v1, v0}, Ltv/periscope/android/api/ApiManager;->getBroadcasts(Ljava/util/ArrayList;)Ljava/lang/String;

    goto :goto_0

    .line 819
    :cond_3
    const-string/jumbo v0, "PeriscopePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Switching to mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    .line 823
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->m(Ljava/lang/String;)V

    .line 824
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->af()V

    .line 825
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/af;->c(Ltv/periscope/android/player/PlayMode;)V

    .line 826
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/af;->d(Ltv/periscope/android/player/PlayMode;)V

    .line 827
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ag()V

    .line 828
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ak()V

    .line 829
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ae()V

    goto :goto_0
.end method

.method public a(Ltv/periscope/android/ui/chat/ChatState;)V
    .locals 1

    .prologue
    .line 1874
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setChatState(Ltv/periscope/android/ui/chat/ChatState;)V

    .line 1875
    return-void
.end method

.method a(Ltv/periscope/android/ui/d;)V
    .locals 2

    .prologue
    .line 2059
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    invoke-virtual {v0}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Ltv/periscope/android/ui/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2060
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->b(Ltv/periscope/android/analytics/summary/b;)V

    .line 2062
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->j()V

    .line 2063
    return-void
.end method

.method public a(Ltv/periscope/chatman/api/Sender;Z)V
    .locals 0

    .prologue
    .line 2008
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;)V
    .locals 6

    .prologue
    .line 1204
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1243
    :cond_0
    :goto_0
    return-void

    .line 1208
    :cond_1
    sget-object v0, Ltv/periscope/android/ui/broadcast/af$8;->b:[I

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/chat/MessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1238
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->b(Lcrz;)V

    .line 1239
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->Q:Ltv/periscope/android/view/t;

    new-instance v1, Ltv/periscope/android/ui/d;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Ltv/periscope/android/ui/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltv/periscope/android/view/t;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 1210
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ay:Ljava/io/File;

    if-eqz v0, :cond_2

    .line 1211
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aL()V

    goto :goto_0

    .line 1212
    :cond_2
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->az()Ltv/periscope/android/permissions/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    sget-object v1, Ltv/periscope/android/util/ae;->a:[Ljava/lang/String;

    .line 1213
    invoke-static {v0, v1}, Ltv/periscope/android/permissions/b;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1214
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->az()Ltv/periscope/android/permissions/a;

    move-result-object v0

    sget-object v1, Ltv/periscope/android/util/ae;->a:[Ljava/lang/String;

    const/16 v2, 0xcee

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/permissions/a;->a([Ljava/lang/String;I)V

    goto :goto_0

    .line 1219
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcyw;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1220
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->as:Ltv/periscope/android/ui/chat/g;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/chat/g;->b(Ltv/periscope/model/chat/Message;)I

    move-result v3

    .line 1221
    const/4 v0, -0x1

    if-eq v3, v0, :cond_0

    .line 1224
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->f(Lcrz;)V

    .line 1225
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->P:Landroid/view/ViewGroup;

    invoke-static {v0}, Ltv/periscope/android/util/n;->a(Landroid/view/View;)V

    .line 1226
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ag:Ltv/periscope/android/ui/broadcast/au;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    sget-object v4, Ltv/periscope/model/chat/MessageType;->b:Ltv/periscope/model/chat/MessageType;

    .line 1227
    invoke-virtual {v2, v4}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v4

    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/af;->d(Ltv/periscope/model/chat/Message;)Ljava/lang/CharSequence;

    move-result-object v5

    move-object v2, p1

    .line 1226
    invoke-virtual/range {v0 .. v5}, Ltv/periscope/android/ui/broadcast/au;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;IZLjava/lang/CharSequence;)V

    goto :goto_0

    .line 1232
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->an:Ltv/periscope/android/ui/broadcast/h;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/h;->a(Ljava/lang/String;)V

    .line 1233
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    .line 1234
    invoke-virtual {v0}, Ltv/periscope/android/analytics/summary/b;->g()V

    goto/16 :goto_0

    .line 1208
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ltv/periscope/model/chat/Message;I)V
    .locals 0

    .prologue
    .line 2214
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 8

    .prologue
    .line 1898
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 1899
    if-nez v1, :cond_0

    .line 1933
    :goto_0
    return-void

    .line 1902
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->o()V

    .line 1904
    new-instance v0, Ldbq;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v3}, Ltv/periscope/android/library/c;->v()Ldae;

    move-result-object v3

    new-instance v5, Ltv/periscope/android/ui/broadcast/af$3;

    invoke-direct {v5, p0, p2}, Ltv/periscope/android/ui/broadcast/af$3;-><init>(Ltv/periscope/android/ui/broadcast/af;Landroid/content/DialogInterface$OnClickListener;)V

    new-instance v6, Ltv/periscope/android/ui/broadcast/af$4;

    invoke-direct {v6, p0, p3}, Ltv/periscope/android/ui/broadcast/af$4;-><init>(Ltv/periscope/android/ui/broadcast/af;Landroid/content/DialogInterface$OnClickListener;)V

    new-instance v7, Ltv/periscope/android/ui/broadcast/af$5;

    invoke-direct {v7, p0}, Ltv/periscope/android/ui/broadcast/af$5;-><init>(Ltv/periscope/android/ui/broadcast/af;)V

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Ldbq;-><init>(Landroid/content/Context;Lcyw;Ldae;Ltv/periscope/model/chat/Message;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->w:Ldbo;

    .line 1931
    sget-object v0, Ltv/periscope/android/analytics/Event;->ai:Ltv/periscope/android/analytics/Event;

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/Event;)V

    .line 1932
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->w:Ldbo;

    invoke-virtual {v0}, Ldbo;->a()V

    goto :goto_0
.end method

.method public a(Ltv/periscope/model/p;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1089
    if-eqz p1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1138
    :cond_0
    :goto_0
    return-void

    .line 1092
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v3, Ltv/periscope/android/player/PlayMode;->a:Ltv/periscope/android/player/PlayMode;

    if-eq v0, v3, :cond_0

    .line 1096
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-virtual {p1}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_5

    .line 1097
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-virtual {p1}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ltv/periscope/android/api/ApiManager;->getUserById(Ljava/lang/String;)Ljava/lang/String;

    .line 1101
    :goto_1
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    .line 1102
    invoke-virtual {p1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    .line 1103
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->O:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/p;->z()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1104
    invoke-virtual {p1}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p1}, Ltv/periscope/model/p;->N()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->e(Z)V

    .line 1105
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v4, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-eq v0, v4, :cond_2

    invoke-virtual {p1}, Ltv/periscope/model/p;->N()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_2
    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Ltv/periscope/android/view/RootDragLayout;->setDraggable(Z)V

    .line 1106
    invoke-virtual {p0, v2}, Ltv/periscope/android/ui/broadcast/af;->c(Z)V

    .line 1109
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aN:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->E()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1110
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    new-instance v2, Landroid/graphics/RectF;

    const/4 v3, 0x0

    const v4, 0x3e19999a    # 0.15f

    const/high16 v5, 0x3f800000    # 1.0f

    const v6, 0x3f59999a    # 0.85f

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v0, v2}, Ltv/periscope/android/view/RootDragLayout;->setDisableAreaForDrag(Landroid/graphics/RectF;)V

    .line 1116
    :goto_4
    invoke-virtual {p1}, Ltv/periscope/model/p;->P()Ljava/lang/Long;

    move-result-object v0

    .line 1117
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    .line 1118
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5, v1}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setParticipantCount(Ljava/lang/String;)V

    .line 1121
    :cond_3
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/af;->b(Ltv/periscope/model/p;)V

    .line 1124
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->n()V

    .line 1125
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->setAdapter(Ltv/periscope/android/ui/broadcast/g;)V

    .line 1126
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1}, Ltv/periscope/android/util/g;->a(Landroid/content/res/Resources;Ltv/periscope/model/p;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/model/p;->e()Ltv/periscope/model/z;

    move-result-object v2

    invoke-virtual {v2}, Ltv/periscope/model/z;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1127
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_9

    .line 1128
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aj()V

    .line 1133
    :goto_5
    invoke-virtual {p1}, Ltv/periscope/model/p;->w()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Ltv/periscope/model/p;->n()D

    move-result-wide v0

    invoke-virtual {p1}, Ltv/periscope/model/p;->o()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lczr;->a(DD)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1134
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->E:Ltv/periscope/android/time/api/TimeZoneClient;

    invoke-virtual {p1}, Ltv/periscope/model/p;->n()D

    move-result-wide v2

    invoke-virtual {p1}, Ltv/periscope/model/p;->o()D

    move-result-wide v4

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/af;->bi:Lretrofit/Callback;

    invoke-virtual/range {v1 .. v6}, Ltv/periscope/android/time/api/TimeZoneClient;->a(DDLretrofit/Callback;)V

    .line 1137
    :cond_4
    invoke-virtual {p1}, Ltv/periscope/model/p;->B()Z

    move-result v0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aT:Z

    goto/16 :goto_0

    .line 1099
    :cond_5
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->ab:Ltv/periscope/android/view/o;

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-virtual {p1}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    invoke-interface {v3, v0, v4}, Ltv/periscope/android/view/o;->a(Ltv/periscope/android/api/PsUser;Ltv/periscope/android/view/b;)V

    goto/16 :goto_1

    :cond_6
    move v0, v2

    .line 1104
    goto/16 :goto_2

    :cond_7
    move v0, v2

    .line 1105
    goto/16 :goto_3

    .line 1112
    :cond_8
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ltv/periscope/android/view/RootDragLayout;->setDisableAreaForDrag(Landroid/graphics/RectF;)V

    goto/16 :goto_4

    .line 1130
    :cond_9
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/af;->c(Ltv/periscope/model/p;)V

    goto :goto_5
.end method

.method public a(Ltv/periscope/model/u;)V
    .locals 6

    .prologue
    .line 1693
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af;->aA:Ltv/periscope/model/u;

    .line 1694
    invoke-virtual {p1}, Ltv/periscope/model/u;->d()Z

    move-result v0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aS:Z

    .line 1695
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    iget-boolean v1, p0, Ltv/periscope/android/ui/broadcast/af;->aS:Z

    invoke-virtual {v0, v1}, Lczz;->a(Z)V

    .line 1697
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-interface {v1}, Lcyw;->e()Ljava/lang/String;

    move-result-object v1

    .line 1698
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->aA:Ltv/periscope/model/u;

    invoke-virtual {v3}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Ltv/periscope/android/util/aa;->b(Landroid/content/res/Resources;J)I

    move-result v2

    .line 1697
    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ljava/lang/String;I)V

    .line 1699
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 369
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/af;->aV:Z

    .line 370
    return-void
.end method

.method public a(Ltv/periscope/model/af;)Z
    .locals 8

    .prologue
    .line 1629
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1630
    const/4 v0, 0x0

    .line 1644
    :cond_0
    :goto_0
    return v0

    .line 1632
    :cond_1
    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v1

    .line 1633
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->S:Ltv/periscope/android/video/StreamMode;

    invoke-virtual {p1}, Ltv/periscope/model/af;->c()Ljava/lang/String;

    move-result-object v3

    .line 1634
    invoke-static {p1}, Ltv/periscope/android/util/g;->a(Ltv/periscope/model/af;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Ltv/periscope/model/af;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Ltv/periscope/model/af;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Ltv/periscope/model/af;->h()Ljava/util/List;

    move-result-object v7

    .line 1633
    invoke-interface/range {v0 .. v7}, Ltv/periscope/android/player/d;->a(Ltv/periscope/model/p;Ltv/periscope/android/video/StreamMode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    .line 1635
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v3}, Ltv/periscope/android/player/d;->o()Z

    move-result v3

    invoke-virtual {v2, v3}, Ltv/periscope/android/ui/broadcast/v;->a(Z)V

    .line 1636
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->ad:Lcxa;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->ad:Lcxa;

    invoke-virtual {v1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcxa;->b(Ljava/lang/String;)D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_2

    .line 1637
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->ad:Lcxa;

    invoke-virtual {v1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcxa;->b(Ljava/lang/String;)D

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Ltv/periscope/android/player/d;->a(D)V

    .line 1641
    :goto_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-interface {v1}, Lcyw;->b()Ltv/periscope/android/api/PsUser;

    move-result-object v1

    iget-boolean v1, v1, Ltv/periscope/android/api/PsUser;->isEmployee:Z

    if-eqz v1, :cond_0

    .line 1642
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aA()V

    goto :goto_0

    .line 1639
    :cond_2
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-virtual {v1}, Ltv/periscope/model/p;->A()I

    move-result v1

    int-to-double v4, v1

    invoke-interface {v2, v4, v5}, Ltv/periscope/android/player/d;->a(D)V

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1040
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->i()V

    .line 1041
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    sget-object v1, Ltv/periscope/android/util/ae;->a:[Ljava/lang/String;

    invoke-static {v0, v1}, Ltv/periscope/android/permissions/b;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1042
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ah()V

    .line 1044
    :cond_0
    return-void
.end method

.method public b(IZ)V
    .locals 1

    .prologue
    .line 1884
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b(IZ)V

    .line 1885
    return-void
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 2200
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/j;->b(J)V

    .line 2201
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2202
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->g()V

    .line 2204
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1012
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->av:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1014
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->av:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1015
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1016
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->av:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1017
    return-void
.end method

.method public b(Ljava/lang/String;JZ)V
    .locals 4

    .prologue
    .line 1957
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->g(Ltv/periscope/android/analytics/summary/b;)V

    .line 1958
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    invoke-interface {v0}, Lcyw;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->aA:Ltv/periscope/model/u;

    invoke-virtual {v1}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3, p4}, Ltv/periscope/android/ui/broadcast/af;->a(Ljava/lang/String;JZ)V

    .line 1959
    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/chatman/api/Occupant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2017
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    if-eqz v0, :cond_0

    .line 2018
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v1}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p1}, Lcyw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 2020
    :cond_0
    return-void
.end method

.method public b(Ltv/periscope/android/player/PlayMode;)V
    .locals 5

    .prologue
    .line 1597
    new-instance v0, Ltv/periscope/android/chat/g;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->aA:Ltv/periscope/model/u;

    invoke-virtual {v1}, Ltv/periscope/model/u;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ltv/periscope/android/chat/g;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aB:Ltv/periscope/android/chat/g;

    .line 1598
    new-instance v0, Lcxc;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->aA:Ltv/periscope/model/u;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/af;->aB:Ltv/periscope/android/chat/g;

    invoke-direct {v0, v1, v2, v3, v4}, Lcxc;-><init>(Ltv/periscope/model/p;Ltv/periscope/model/af;Ltv/periscope/model/u;Ltv/periscope/android/chat/g;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aC:Lcxc;

    .line 1599
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->b:Ltv/periscope/android/player/e;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->aC:Lcxc;

    invoke-interface {v0, v1}, Ltv/periscope/android/player/e;->a(Lcxc;)V

    .line 1600
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ay()Lcxe;

    move-result-object v0

    .line 1601
    if-eqz v0, :cond_0

    .line 1602
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcxe;->a(Lcxb;)V

    .line 1605
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->d:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aJ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1606
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->c(Z)V

    .line 1607
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->J()V

    .line 1609
    :cond_1
    return-void
.end method

.method public b(Ltv/periscope/chatman/api/Sender;Z)V
    .locals 0

    .prologue
    .line 2013
    return-void
.end method

.method public b(Ltv/periscope/model/af;)V
    .locals 4

    .prologue
    .line 1673
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    .line 1675
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    invoke-virtual {v0}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v0

    .line 1676
    invoke-virtual {v0}, Ltv/periscope/model/p;->L()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1677
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v1

    invoke-virtual {v0}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ltv/periscope/android/api/ApiManager;->getBroadcastViewers(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 1680
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v1

    invoke-static {v1, p1}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/summary/b;Ltv/periscope/model/af;)V

    .line 1681
    invoke-virtual {v0}, Ltv/periscope/model/p;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1682
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->g(Lcrz;)V

    .line 1685
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/model/p;)V

    .line 1687
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->u:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1688
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->u:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1689
    return-void
.end method

.method public b(Ltv/periscope/model/chat/Message;)V
    .locals 0

    .prologue
    .line 1252
    return-void
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 2410
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2431
    :goto_0
    return-void

    .line 2413
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "onPlaybackReady"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 2414
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/af;->be:J

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->l()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/af;->be:J

    .line 2415
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->O()V

    .line 2416
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aw()V

    .line 2419
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->A:Z

    if-eqz v0, :cond_1

    .line 2420
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->A:Z

    .line 2421
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/am;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2422
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/am;->b()V

    .line 2426
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_2

    .line 2427
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ai()V

    goto :goto_0

    .line 2429
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->replayable:Z

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->e(Z)V

    goto :goto_0
.end method

.method public b_(J)V
    .locals 0

    .prologue
    .line 2209
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 2256
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->t()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    if-nez v0, :cond_1

    .line 2279
    :cond_0
    :goto_0
    return-void

    .line 2259
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aW:Z

    .line 2260
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "onPlaybackStarted"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 2261
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ax()V

    .line 2262
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->O()V

    .line 2263
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->S()V

    .line 2264
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aG()V

    .line 2265
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->c()V

    .line 2266
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v1}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/ui/chat/m;->a(Ljava/lang/String;Ltv/periscope/android/analytics/summary/b;)V

    .line 2267
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aq:Ltv/periscope/android/ui/broadcast/ah;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ah;->b()V

    .line 2268
    sget-object v0, Ltv/periscope/android/ui/broadcast/af$8;->a:[I

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    invoke-virtual {v1}, Ltv/periscope/android/player/PlayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2270
    :pswitch_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    const-string/jumbo v1, "Summary.TimeWatched"

    invoke-virtual {v0, v1}, Ltv/periscope/android/analytics/summary/b;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 2275
    :pswitch_1
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    const-string/jumbo v1, "Summary.ReplayTimeWatched"

    invoke-virtual {v0, v1}, Ltv/periscope/android/analytics/summary/b;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 2268
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public c(J)V
    .locals 7

    .prologue
    .line 2356
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->B:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2357
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->B:Z

    .line 2358
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i()V

    .line 2359
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/v;->a(J)V

    .line 2363
    :cond_0
    const-wide v0, 0x20251fe2400L

    add-long/2addr v0, p1

    .line 2365
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->au:Lcxv;

    if-eqz v2, :cond_1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->au:Lcxv;

    invoke-virtual {v2}, Lcxv;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2366
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->au:Lcxv;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->b:Ltv/periscope/android/player/e;

    invoke-interface {v3}, Ltv/periscope/android/player/e;->l()J

    move-result-wide v4

    invoke-virtual {v2, v0, v1, v4, v5}, Lcxv;->a(JJ)V

    .line 2367
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->au:Lcxv;

    invoke-virtual {v0}, Lcxv;->b()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/af;->d(J)V

    .line 2369
    :cond_1
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 800
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    .line 801
    return-void
.end method

.method public c(Ltv/periscope/model/chat/Message;)V
    .locals 3

    .prologue
    .line 1889
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ak:Ltv/periscope/android/ui/broadcast/moderator/f;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/moderator/f;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1893
    :goto_0
    return-void

    .line 1892
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ltv/periscope/model/chat/Message;)V

    goto :goto_0
.end method

.method c(Z)V
    .locals 2

    .prologue
    .line 1068
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1069
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ai()V

    .line 1079
    :goto_0
    return-void

    .line 1072
    :cond_0
    if-eqz p1, :cond_1

    .line 1073
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    .line 1074
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->b()V

    goto :goto_0

    .line 1076
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->g:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    .line 1077
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a()V

    goto :goto_0
.end method

.method public c_(J)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1978
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aQ:Z

    if-nez v0, :cond_1

    .line 1979
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0, p1, p2}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/summary/b;J)V

    .line 1980
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    if-eqz v0, :cond_0

    .line 1981
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/chat/m;->a(J)V

    .line 1983
    :cond_0
    iput-boolean v2, p0, Ltv/periscope/android/ui/broadcast/af;->aQ:Z

    .line 1986
    :cond_1
    iput-wide p1, p0, Ltv/periscope/android/ui/broadcast/af;->bf:J

    .line 1987
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1, p2, v2}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setParticipantCount(Ljava/lang/String;)V

    .line 1989
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aJ:Z

    if-eqz v0, :cond_2

    .line 1990
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/j;->a(J)V

    .line 1992
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1993
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->e()V

    .line 1996
    :cond_2
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 2626
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    if-eqz v0, :cond_0

    .line 2627
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    sget-object v1, Ltv/periscope/model/chat/MessageType;->G:Ltv/periscope/model/chat/MessageType;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/m;->b(Ltv/periscope/model/chat/MessageType;)V

    .line 2629
    :cond_0
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1021
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setUpComposerReply(Ljava/lang/String;)V

    .line 1022
    return-void
.end method

.method d(Z)V
    .locals 2

    .prologue
    .line 1824
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aI:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aT:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->P()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1825
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->l:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1829
    :goto_0
    return-void

    .line 1827
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->l:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 2632
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1973
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Ltv/periscope/android/analytics/summary/b;->h(Ljava/lang/String;)V

    .line 1974
    return-void
.end method

.method public e(Ltv/periscope/model/chat/Message;)V
    .locals 3

    .prologue
    .line 2040
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->w:Ldbo;

    if-eqz v0, :cond_0

    .line 2041
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->w:Ldbo;

    invoke-virtual {v0}, Ldbo;->b()V

    .line 2044
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->o()V

    .line 2046
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2047
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->f(Z)V

    .line 2050
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2051
    if-eqz v0, :cond_2

    .line 2052
    new-instance v1, Landroid/content/Intent;

    const-class v2, Ltv/periscope/android/ui/broadcast/KickSelfActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "e_message"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    .line 2053
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 2054
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->Y()V

    .line 2056
    :cond_2
    return-void
.end method

.method public f()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2283
    iput-boolean v3, p0, Ltv/periscope/android/ui/broadcast/af;->bc:Z

    .line 2288
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->t()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aV:Z

    if-nez v0, :cond_1

    .line 2325
    :cond_0
    :goto_0
    return-void

    .line 2291
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ad:Lcxa;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2292
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ad:Lcxa;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcxa;->c(Ljava/lang/String;)V

    .line 2294
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "onPlaybackEnded"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 2296
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->d()V

    .line 2300
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/am;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2301
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/am;->b()V

    .line 2304
    :cond_3
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/af;->bg:J

    .line 2306
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aC()V

    .line 2307
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->c(Ltv/periscope/model/p;)V

    .line 2308
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aD()V

    .line 2311
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_4

    .line 2312
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    if-nez v0, :cond_5

    .line 2313
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "playback ended but broadcast is not in our cache"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lf;->a(Ljava/lang/Throwable;)V

    .line 2321
    :goto_1
    iput-boolean v2, p0, Ltv/periscope/android/ui/broadcast/af;->aP:Z

    .line 2322
    sget-object v0, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/android/player/PlayMode;)V

    .line 2324
    :cond_4
    invoke-virtual {p0, v3}, Ltv/periscope/android/ui/broadcast/af;->c(Z)V

    goto :goto_0

    .line 2315
    :cond_5
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v0, v2}, Ltv/periscope/model/p;->b(Z)V

    .line 2319
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v0, v2}, Ltv/periscope/model/p;->a(Z)V

    goto :goto_1
.end method

.method public f(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2076
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    if-nez v0, :cond_0

    .line 2101
    :goto_0
    return-void

    .line 2079
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2080
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    invoke-virtual {v1}, Ltv/periscope/model/af;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 2082
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->J:Ltv/periscope/android/ui/broadcast/aq;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v3}, Ltv/periscope/android/library/c;->j()Ltv/periscope/android/session/a;

    move-result-object v3

    invoke-interface {v3}, Ltv/periscope/android/session/a;->a()Ltv/periscope/android/session/Session;

    move-result-object v3

    invoke-interface {v2, v3}, Ltv/periscope/android/ui/broadcast/aq;->a(Ltv/periscope/android/session/Session;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2083
    if-eqz v1, :cond_1

    .line 2084
    new-instance v2, Ldba;

    invoke-direct {v2, p1, p0}, Ldba;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/ao;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2086
    :cond_1
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    invoke-virtual {v2}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v2

    invoke-virtual {v2}, Ltv/periscope/model/p;->D()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Ltv/periscope/android/ui/broadcast/af;->bd:Z

    if-nez v2, :cond_2

    .line 2087
    new-instance v2, Ldat;

    invoke-direct {v2, p1, p0}, Ldat;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/ao;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2091
    :cond_2
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v3, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v2, v3, :cond_3

    .line 2092
    new-instance v2, Lday;

    invoke-direct {v2, p1, p0}, Lday;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/ao;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2093
    new-instance v2, Ldaz;

    invoke-direct {v2, p1, p0}, Ldaz;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/ao;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2096
    :cond_3
    if-eqz v1, :cond_4

    .line 2097
    new-instance v1, Ldaw;

    invoke-direct {v1, p1, p0}, Ldaw;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/ao;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2098
    new-instance v1, Ldax;

    invoke-direct {v1, p1, p0}, Ldax;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/ao;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2100
    :cond_4
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    const/4 v2, 0x0

    const-wide/16 v4, 0x12c

    invoke-virtual {v1, v2, v0, v4, v5}, Ltv/periscope/android/ui/broadcast/f;->a(Ljava/lang/CharSequence;Ljava/util/List;J)V

    goto :goto_0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 2233
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2239
    :goto_0
    return-void

    .line 2236
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "onPlaybackRequested"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 2237
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->a(Ljava/lang/String;)V

    .line 2238
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->R()V

    goto :goto_0
.end method

.method public g(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2105
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/f;->e()V

    .line 2106
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->g:Ltv/periscope/android/ui/user/l;

    new-instance v1, Ltv/periscope/android/ui/broadcast/af$6;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/af$6;-><init>(Ltv/periscope/android/ui/broadcast/af;)V

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/user/l;->a(Ltv/periscope/android/ui/user/UserPickerSheet$a;)V

    .line 2122
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->g:Ltv/periscope/android/ui/user/l;

    invoke-interface {v0}, Ltv/periscope/android/ui/user/l;->a()V

    .line 2123
    return-void
.end method

.method public h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2243
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->t()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    if-nez v0, :cond_1

    .line 2252
    :cond_0
    :goto_0
    return-void

    .line 2246
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "onPauseRequested"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 2248
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2249
    invoke-direct {p0, v2}, Ltv/periscope/android/ui/broadcast/af;->f(Z)V

    .line 2250
    invoke-virtual {p0, v2}, Ltv/periscope/android/ui/broadcast/af;->c(Z)V

    goto :goto_0
.end method

.method public h(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2127
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->j(Lcrz;)V

    .line 2128
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/f;->e()V

    .line 2129
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->a(Ljava/util/ArrayList;)V

    .line 2130
    return-void
.end method

.method public i()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2329
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2345
    :cond_0
    :goto_0
    return-void

    .line 2332
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "onPlaybackBuffer"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 2333
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->C:Z

    if-nez v0, :cond_0

    .line 2336
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aW:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2337
    const-string/jumbo v0, "PeriscopePlayer"

    const-string/jumbo v1, "Buffering detected."

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2338
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->v:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2339
    iput-boolean v4, p0, Ltv/periscope/android/ui/broadcast/af;->C:Z

    .line 2340
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aY:Z

    if-nez v0, :cond_0

    .line 2341
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ap:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2342
    iput-boolean v4, p0, Ltv/periscope/android/ui/broadcast/af;->aY:Z

    goto :goto_0
.end method

.method public i(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2134
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->i(Lcrz;)V

    .line 2135
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/f;->e()V

    .line 2136
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aR:Z

    .line 2137
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 2218
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2219
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->k()V

    .line 2224
    :goto_0
    return-void

    .line 2222
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->f()V

    goto :goto_0
.end method

.method public j(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2141
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->i(Lcrz;)V

    .line 2142
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/f;->e()V

    .line 2143
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->J:Ltv/periscope/android/ui/broadcast/aq;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcast/aq;->a(Ljava/lang/String;)V

    .line 2144
    return-void
.end method

.method public k()J
    .locals 2

    .prologue
    .line 2067
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/af;->bf:J

    return-wide v0
.end method

.method public k(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2148
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/f;->e()V

    .line 2149
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    if-eqz v0, :cond_0

    .line 2150
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    const-string/jumbo v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 2151
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    invoke-virtual {v1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/p;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    invoke-virtual {v2}, Ltv/periscope/model/af;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 2152
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 2153
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__share_broadcast_copy_link_copied:I

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2157
    :goto_0
    return-void

    .line 2155
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__share_broadcast_copy_link_copied_fail:I

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public l()V
    .locals 2

    .prologue
    .line 2172
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->h(Lcrz;)V

    .line 2173
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aU:Z

    .line 2174
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e()V

    .line 2175
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_0

    .line 2176
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->d:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    .line 2178
    :cond_0
    return-void
.end method

.method public l(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2161
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2162
    if-eqz v0, :cond_0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    if-eqz v1, :cond_0

    .line 2163
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.extra.TEXT"

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    .line 2164
    invoke-virtual {v3}, Ltv/periscope/model/af;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "text/plain"

    .line 2165
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2166
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Ltv/periscope/android/library/f$l;->ps__share_broadcast_title:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 2168
    :cond_0
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    .line 2182
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aU:Z

    .line 2183
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->d()V

    .line 2184
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_0

    .line 2185
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->e:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    .line 2191
    :goto_0
    return-void

    .line 2186
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2187
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    goto :goto_0

    .line 2189
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->g:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 2195
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->g()Z

    move-result v0

    return v0
.end method

.method public o()V
    .locals 1

    .prologue
    .line 2373
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->c(Z)V

    .line 2374
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aq:Ltv/periscope/android/ui/broadcast/ah;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ah;->b()V

    .line 2375
    return-void
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 933
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 1341
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 1351
    :goto_0
    return-void

    .line 1344
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "onDestroy"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 1345
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ae:Ljava/lang/String;

    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->an:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/h;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1346
    const-string/jumbo v0, "PeriscopePlayer"

    const-string/jumbo v1, "Video was not saved to camera, deleting"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1347
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ae:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1348
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1350
    :cond_1
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->B()V

    goto :goto_0
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 1277
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 1283
    :goto_0
    return-void

    .line 1280
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "onPause"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 1281
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->an()V

    .line 1282
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->e()V

    goto :goto_0
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 948
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_1

    .line 958
    :cond_0
    :goto_0
    return-void

    .line 952
    :cond_1
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->X()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 953
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f()V

    .line 954
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ltv/periscope/android/player/d;->a(I)V

    .line 955
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "onResume"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 956
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->f()V

    goto :goto_0
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1330
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 937
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 944
    :goto_0
    return-void

    .line 940
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ab()V

    .line 941
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "onStart"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 942
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->I:Ltv/periscope/android/network/ConnectivityChangeReceiver;

    invoke-virtual {v0}, Ltv/periscope/android/network/ConnectivityChangeReceiver;->a()V

    .line 943
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->G:Ltv/periscope/android/util/ae;

    invoke-virtual {v0, p0}, Ltv/periscope/android/util/ae;->a(Ltv/periscope/android/util/ae$a;)V

    goto :goto_0
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1287
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 1326
    :goto_0
    return-void

    .line 1290
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ac()V

    .line 1291
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "onStop"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 1292
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ak:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/f;->a()V

    .line 1293
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->g()V

    .line 1294
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->I:Ltv/periscope/android/network/ConnectivityChangeReceiver;

    invoke-virtual {v0}, Ltv/periscope/android/network/ConnectivityChangeReceiver;->b()V

    .line 1297
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->b:Ltv/periscope/android/player/e;

    invoke-interface {v0}, Ltv/periscope/android/player/e;->l()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/af;->bg:J

    .line 1298
    invoke-direct {p0, v2}, Ltv/periscope/android/ui/broadcast/af;->f(Z)V

    .line 1299
    invoke-virtual {p0, v2}, Ltv/periscope/android/ui/broadcast/af;->a(Z)V

    .line 1302
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->al()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1303
    iput-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->aA:Ltv/periscope/model/u;

    .line 1304
    iput-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->az:Ltv/periscope/model/af;

    .line 1307
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1308
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->k()V

    .line 1310
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1311
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_5

    .line 1312
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->p()Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v3}, Ltv/periscope/android/ui/broadcast/v;->r()Ltv/periscope/android/api/ChatStats;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Ltv/periscope/android/api/ApiManager;->livePlaybackMeta(Ljava/lang/String;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ljava/lang/String;

    .line 1317
    :cond_3
    :goto_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->G:Ltv/periscope/android/util/ae;

    invoke-virtual {v0}, Ltv/periscope/android/util/ae;->a()V

    .line 1318
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/am;->c()V

    .line 1319
    invoke-static {}, Ltv/periscope/android/util/aa;->a()V

    .line 1320
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_4

    .line 1322
    sget-object v0, Ltv/periscope/android/player/PlayMode;->d:Ltv/periscope/android/player/PlayMode;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    .line 1325
    :cond_4
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->D()Z

    move-result v0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aP:Z

    goto :goto_0

    .line 1313
    :cond_5
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->d:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_3

    .line 1314
    :cond_6
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->p()Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v3}, Ltv/periscope/android/ui/broadcast/v;->r()Ltv/periscope/android/api/ChatStats;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Ltv/periscope/android/api/ApiManager;->replayPlaybackMeta(Ljava/lang/String;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ljava/lang/String;

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1782
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1802
    :cond_0
    :goto_0
    return-void

    .line 1786
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 1787
    sget v1, Ltv/periscope/android/library/f$g;->participants:I

    if-ne v0, v1, :cond_2

    .line 1790
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aD()V

    goto :goto_0

    .line 1791
    :cond_2
    sget v1, Ltv/periscope/android/library/f$g;->close:I

    if-ne v0, v1, :cond_3

    .line 1792
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    .line 1793
    :cond_3
    sget v1, Ltv/periscope/android/library/f$g;->play_highlights:I

    if-ne v0, v1, :cond_4

    .line 1794
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aB()V

    goto :goto_0

    .line 1795
    :cond_4
    sget v1, Ltv/periscope/android/library/f$g;->preview_frame:I

    if-ne v0, v1, :cond_5

    .line 1796
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aK()V

    goto :goto_0

    .line 1797
    :cond_5
    sget v1, Ltv/periscope/android/library/f$g;->action_button:I

    if-ne v0, v1, :cond_6

    .line 1798
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->I()V

    goto :goto_0

    .line 1799
    :cond_6
    sget v1, Ltv/periscope/android/library/f$g;->overflow_button:I

    if-ne v0, v1, :cond_0

    .line 1800
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ai:Ltv/periscope/android/ui/broadcast/b;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ltv/periscope/android/ui/broadcast/b;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ltv/periscope/android/ui/broadcast/f;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 925
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->w:Ldbo;

    if-eqz v0, :cond_0

    .line 926
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->w:Ldbo;

    invoke-virtual {v0}, Ldbo;->c()V

    .line 928
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->aj:Ltv/periscope/android/ui/broadcast/moderator/h;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/h;->h()V

    .line 929
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1455
    sget-object v0, Ltv/periscope/android/ui/broadcast/af$8;->c:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1521
    :cond_0
    :goto_0
    return-void

    .line 1458
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->n()V

    goto :goto_0

    .line 1462
    :pswitch_1
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/android/event/ApiEvent;)V

    goto :goto_0

    .line 1466
    :pswitch_2
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1467
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__failed_to_mark_broadcast_persistent:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1468
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1469
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mark broadcast as persistent error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 1473
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->n()V

    goto :goto_0

    .line 1479
    :pswitch_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1480
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/api/ThumbnailPlaylistResponse;

    .line 1481
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    iget-object v2, v0, Ltv/periscope/android/api/ThumbnailPlaylistResponse;->broadcastId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1482
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    iget-object v0, v0, Ltv/periscope/android/api/ThumbnailPlaylistResponse;->chunks:Ljava/util/List;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/am;->a(Ljava/util/List;)V

    goto :goto_0

    .line 1485
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/am;->a(Ljava/util/List;)V

    goto :goto_0

    .line 1490
    :pswitch_4
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1491
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    sget v1, Ltv/periscope/android/library/f$l;->ps__share_broadcast_failed:I

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1492
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "share w/ followers error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1497
    :pswitch_5
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->ap()V

    goto/16 :goto_0

    .line 1501
    :pswitch_6
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->Y()V

    goto/16 :goto_0

    .line 1505
    :pswitch_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->bd:Z

    .line 1506
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    sget-object v1, Ltv/periscope/model/chat/MessageType;->G:Ltv/periscope/model/chat/MessageType;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/m;->b(Ltv/periscope/model/chat/MessageType;)V

    goto/16 :goto_0

    .line 1510
    :pswitch_8
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1511
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->at:Ltv/periscope/android/ui/chat/m;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v1}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/ui/chat/m;->a(Ljava/lang/String;Ltv/periscope/android/analytics/summary/b;)V

    .line 1513
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->K:Lcyw;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v1}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 1514
    if-eqz v0, :cond_0

    .line 1517
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->ab:Ltv/periscope/android/view/o;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af;->ah:Ltv/periscope/android/ui/broadcast/f;

    invoke-interface {v1, v0, v2}, Ltv/periscope/android/view/o;->a(Ltv/periscope/android/api/PsUser;Ltv/periscope/android/view/b;)V

    goto/16 :goto_0

    .line 1455
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onEventMainThread(Ltv/periscope/android/event/CacheEvent;)V
    .locals 2

    .prologue
    .line 1752
    sget-object v0, Ltv/periscope/android/ui/broadcast/af$8;->d:[I

    invoke-virtual {p1}, Ltv/periscope/android/event/CacheEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1771
    :cond_0
    :goto_0
    return-void

    .line 1754
    :pswitch_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af;->aF()V

    goto :goto_0

    .line 1758
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->M:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 1759
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/model/p;)V

    goto :goto_0

    .line 1765
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->as:Ltv/periscope/android/ui/chat/g;

    if-eqz v0, :cond_0

    .line 1767
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->as:Ltv/periscope/android/ui/chat/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/g;->notifyDataSetChanged()V

    goto :goto_0

    .line 1752
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ParticipantHeartCountEvent;)V
    .locals 2

    .prologue
    .line 1775
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1776
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->am:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/j;->a(Ltv/periscope/android/event/ParticipantHeartCountEvent;)V

    .line 1778
    :cond_0
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/network/NetworkMonitorInfo;)V
    .locals 2

    .prologue
    .line 1563
    if-eqz p1, :cond_0

    .line 1564
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    iget-object v1, p1, Ltv/periscope/android/network/NetworkMonitorInfo;->a:Landroid/os/Bundle;

    invoke-static {v1}, Ltv/periscope/android/network/a;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 1568
    :goto_0
    return-void

    .line 1566
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "Received network activity but info was empty"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 1334
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->bb:Z

    .line 1335
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    const-string/jumbo v1, "onLowMemory"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 1336
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->f:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->h()V

    .line 1337
    return-void
.end method

.method public p()V
    .locals 0

    .prologue
    .line 2380
    return-void
.end method

.method public q()V
    .locals 3

    .prologue
    .line 2384
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__audio_noisy:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2385
    return-void
.end method

.method public r()V
    .locals 1

    .prologue
    .line 2389
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->n:Ltv/periscope/android/ui/broadcast/av;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/av;->h()V

    .line 2390
    return-void
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1056
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aS:Z

    if-eqz v0, :cond_0

    .line 1057
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->ao:Lczz;

    invoke-virtual {v0}, Lczz;->c()Ljava/lang/String;

    move-result-object v0

    .line 1061
    :goto_0
    return-object v0

    .line 1059
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method t()Z
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->N:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 307
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method u()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method v()Ltv/periscope/android/api/ApiManager;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->D:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    return-object v0
.end method

.method w()Ltv/periscope/android/analytics/summary/b;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->R:Ltv/periscope/android/analytics/summary/b;

    return-object v0
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 356
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->aV:Z

    return v0
.end method

.method y()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1388

    .line 689
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->bb:Z

    if-eqz v0, :cond_1

    .line 707
    :cond_0
    :goto_0
    return-void

    .line 693
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->q()I

    move-result v0

    .line 696
    iget-boolean v1, p0, Ltv/periscope/android/ui/broadcast/af;->ba:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->d:Landroid/content/Context;

    invoke-static {v1}, Ltv/periscope/android/util/ad;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    if-eqz v0, :cond_3

    .line 697
    :cond_2
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v1, v0, v2, v3}, Ltv/periscope/android/player/c;->a(IJ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 698
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af;->ba:Z

    .line 705
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->u:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 706
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af;->u:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 701
    :cond_4
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->N()V

    goto :goto_0
.end method

.method z()V
    .locals 2

    .prologue
    .line 792
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->setSystemUiVisibility(I)V

    .line 793
    return-void
.end method
