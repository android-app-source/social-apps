.class public Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem",
        "<",
        "Ltv/periscope/model/q;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ltv/periscope/android/ui/broadcast/j;

.field private final b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

.field private final c:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/j;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;)V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;->a:Ltv/periscope/android/ui/broadcast/j;

    .line 280
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    .line 281
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;->c:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    .line 282
    return-void
.end method


# virtual methods
.method public a()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;
    .locals 1

    .prologue
    .line 286
    sget-object v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;->i:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;

    return-object v0
.end method

.method public b()Ltv/periscope/model/q;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;->a:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->p()Ltv/periscope/model/q;

    move-result-object v0

    return-object v0
.end method

.method public c()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    return-object v0
.end method

.method public d()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;->c:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    return-object v0
.end method
