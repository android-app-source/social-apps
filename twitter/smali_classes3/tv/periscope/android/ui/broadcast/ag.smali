.class Ltv/periscope/android/ui/broadcast/ag;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcxb;
.implements Ltv/periscope/android/chat/h;
.implements Ltv/periscope/android/player/a;
.implements Ltv/periscope/android/ui/broadcast/ac;
.implements Ltv/periscope/android/ui/broadcast/av$a;
.implements Ltv/periscope/android/ui/chat/o$a;
.implements Ltv/periscope/android/ui/chat/o$c;
.implements Ltv/periscope/android/ui/chat/r;
.implements Ltv/periscope/android/ui/chat/w;


# static fields
.field private static final l:Ltv/periscope/android/ui/broadcast/moderator/f;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Ltv/periscope/android/ui/broadcast/av;

.field private D:Z

.field private E:Z

.field private F:Ltv/periscope/android/chat/a;

.field private G:Ltv/periscope/android/ui/broadcast/v;

.field private final H:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final I:Landroid/content/Context;

.field private final J:Lde/greenrobot/event/c;

.field private final K:Ltv/periscope/android/api/ApiManager;

.field private final L:Ljava/lang/String;

.field private final M:Lretrofit/RestAdapter$LogLevel;

.field private final N:Z

.field private final O:Z

.field private final P:Ltv/periscope/android/video/StreamMode;

.field private final Q:Ltv/periscope/android/chat/i$a;

.field private R:Ltv/periscope/android/chat/g;

.field private S:Lcxc;

.field private final T:Ljava/lang/Runnable;

.field private final U:Ljava/lang/Runnable;

.field private final V:Ljava/lang/Runnable;

.field protected a:Ltv/periscope/android/player/c;

.field protected b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

.field protected c:Ltv/periscope/model/p;

.field protected d:Ljava/lang/String;

.field protected e:Ltv/periscope/android/api/PsUser;

.field protected f:Ltv/periscope/android/ui/broadcast/ar;

.field protected g:Ltv/periscope/android/player/d;

.field protected h:Ltv/periscope/android/player/e;

.field protected i:Z

.field protected j:Ltv/periscope/android/player/PlayMode;

.field protected final k:Landroid/os/Handler;

.field private m:Lcyw;

.field private n:Lcyn;

.field private final o:Ldae;

.field private p:Ltv/periscope/model/af;

.field private q:Ltv/periscope/model/u;

.field private r:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcxe;",
            ">;"
        }
    .end annotation
.end field

.field private s:Z

.field private t:Z

.field private u:J

.field private v:J

.field private w:Ljava/lang/Boolean;

.field private x:Z

.field private y:J

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    new-instance v0, Ltv/periscope/android/ui/broadcast/moderator/l;

    invoke-direct {v0}, Ltv/periscope/android/ui/broadcast/moderator/l;-><init>()V

    sput-object v0, Ltv/periscope/android/ui/broadcast/ag;->l:Ltv/periscope/android/ui/broadcast/moderator/f;

    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lde/greenrobot/event/c;Ltv/periscope/android/api/ApiManager;Ljava/lang/String;Lcyw;Lcyn;Ltv/periscope/android/api/PsUser;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Ldae;Ldae;Ltv/periscope/android/chat/a;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/player/c;Ltv/periscope/android/ui/broadcast/ar;ZLtv/periscope/android/video/StreamMode;ZLtv/periscope/android/chat/i$a;)V
    .locals 7

    .prologue
    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    const/4 v1, 0x0

    iput-boolean v1, p0, Ltv/periscope/android/ui/broadcast/ag;->D:Z

    .line 119
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->k:Landroid/os/Handler;

    .line 181
    new-instance v1, Ltv/periscope/android/ui/broadcast/ag$1;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/ag$1;-><init>(Ltv/periscope/android/ui/broadcast/ag;)V

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->T:Ljava/lang/Runnable;

    .line 191
    new-instance v1, Ltv/periscope/android/ui/broadcast/ag$2;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/ag$2;-><init>(Ltv/periscope/android/ui/broadcast/ag;)V

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->U:Ljava/lang/Runnable;

    .line 209
    new-instance v1, Ltv/periscope/android/ui/broadcast/ag$3;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/ag$3;-><init>(Ltv/periscope/android/ui/broadcast/ag;)V

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->V:Ljava/lang/Runnable;

    .line 228
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->H:Ljava/lang/ref/WeakReference;

    .line 229
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->I:Landroid/content/Context;

    .line 230
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/ag;->J:Lde/greenrobot/event/c;

    .line 231
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/ag;->K:Ltv/periscope/android/api/ApiManager;

    .line 232
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/ag;->L:Ljava/lang/String;

    .line 233
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/ag;->m:Lcyw;

    .line 234
    iput-object p6, p0, Ltv/periscope/android/ui/broadcast/ag;->n:Lcyn;

    .line 235
    iput-object p7, p0, Ltv/periscope/android/ui/broadcast/ag;->e:Ltv/periscope/android/api/PsUser;

    .line 236
    iput-object p8, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    .line 237
    move-object/from16 v0, p9

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->h:Ltv/periscope/android/player/e;

    .line 238
    move-object/from16 v0, p10

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->o:Ldae;

    .line 239
    move-object/from16 v0, p14

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    .line 240
    move-object/from16 v0, p15

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->f:Ltv/periscope/android/ui/broadcast/ar;

    .line 241
    move-object/from16 v0, p13

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->M:Lretrofit/RestAdapter$LogLevel;

    .line 242
    move/from16 v0, p18

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->O:Z

    .line 243
    move-object/from16 v0, p17

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->P:Ltv/periscope/android/video/StreamMode;

    .line 244
    move-object/from16 v0, p19

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->Q:Ltv/periscope/android/chat/i$a;

    .line 245
    move-object/from16 v0, p12

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->F:Ltv/periscope/android/chat/a;

    .line 246
    move/from16 v0, p16

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->N:Z

    .line 247
    new-instance v1, Ltv/periscope/android/ui/broadcast/v;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->H:Ljava/lang/ref/WeakReference;

    sget-object v3, Ltv/periscope/android/ui/broadcast/ag;->l:Ltv/periscope/android/ui/broadcast/moderator/f;

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Ltv/periscope/android/ui/broadcast/v;-><init>(Ljava/lang/ref/WeakReference;Ltv/periscope/android/ui/broadcast/moderator/f;Ltv/periscope/android/ui/broadcast/moderator/g;ZZ)V

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->G:Ltv/periscope/android/ui/broadcast/v;

    .line 248
    move-object/from16 v0, p11

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->a(Ldae;)V

    .line 249
    return-void
.end method

.method private X()V
    .locals 15

    .prologue
    .line 318
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->C:Ltv/periscope/android/ui/broadcast/av;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->e()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->k:Landroid/os/Handler;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ag;->F:Ltv/periscope/android/chat/a;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/ag;->f:Ltv/periscope/android/ui/broadcast/ar;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/ag;->e:Ltv/periscope/android/api/PsUser;

    const/4 v6, 0x0

    iget-object v8, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    iget-object v9, p0, Ltv/periscope/android/ui/broadcast/ag;->h:Ltv/periscope/android/player/e;

    iget-object v11, p0, Ltv/periscope/android/ui/broadcast/ag;->m:Lcyw;

    const/4 v12, 0x0

    sget-object v13, Ltv/periscope/android/ui/chat/s;->a:Ltv/periscope/android/ui/chat/s;

    sget-object v14, Ltv/periscope/android/ui/chat/u;->a:Ltv/periscope/android/ui/chat/u;

    move-object v7, p0

    move-object v10, p0

    invoke-virtual/range {v0 .. v14}, Ltv/periscope/android/ui/broadcast/av;->a(Landroid/content/res/Resources;Landroid/os/Handler;Ltv/periscope/android/chat/a;Ltv/periscope/android/ui/broadcast/ar;Ltv/periscope/android/api/PsUser;ZLtv/periscope/android/ui/chat/r;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Ltv/periscope/android/ui/chat/w;Lcyw;Lcxm;Ltv/periscope/android/ui/chat/x;Ltv/periscope/android/ui/chat/aj;)Ltv/periscope/android/ui/chat/m;

    move-result-object v0

    .line 322
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setSendCommentDelegate(Ltv/periscope/android/ui/chat/ai;)V

    .line 323
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setPunishmentStatusDelegate(Ltv/periscope/android/ui/chat/ag;)V

    .line 324
    return-void
.end method

.method private Y()V
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->S()V

    .line 345
    return-void
.end method

.method private Z()V
    .locals 2

    .prologue
    .line 372
    sget-object v0, Ltv/periscope/android/ui/broadcast/ag$4;->a:[I

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    invoke-virtual {v1}, Ltv/periscope/android/player/PlayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 382
    :goto_0
    return-void

    .line 374
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    .line 375
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/chat/ChatState;->a:Ltv/periscope/android/ui/chat/ChatState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setChatState(Ltv/periscope/android/ui/chat/ChatState;)V

    goto :goto_0

    .line 379
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->a:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    goto :goto_0

    .line 372
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/ag;)Ltv/periscope/android/ui/broadcast/av;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->C:Ltv/periscope/android/ui/broadcast/av;

    return-object v0
.end method

.method private a(Ldae;)V
    .locals 13

    .prologue
    .line 304
    new-instance v0, Ltv/periscope/android/ui/broadcast/ad;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->m()Ltv/periscope/android/api/ApiManager;

    move-result-object v3

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->l()Lde/greenrobot/event/c;

    move-result-object v4

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/ag;->G:Ltv/periscope/android/ui/broadcast/v;

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/ag;->n:Lcyn;

    iget-object v7, p0, Ltv/periscope/android/ui/broadcast/ag;->d:Ljava/lang/String;

    sget-object v8, Lretrofit/RestAdapter$LogLevel;->NONE:Lretrofit/RestAdapter$LogLevel;

    iget-object v9, p0, Ltv/periscope/android/ui/broadcast/ag;->P:Ltv/periscope/android/video/StreamMode;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v9}, Ltv/periscope/android/ui/broadcast/ad;-><init>(Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/video/StreamMode;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->C:Ltv/periscope/android/ui/broadcast/av;

    .line 307
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    .line 308
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setImageLoader(Ldae;)V

    .line 309
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->x()Ltv/periscope/android/ui/chat/k;

    move-result-object v10

    .line 310
    new-instance v11, Ltv/periscope/android/ui/chat/g;

    iget-object v12, p0, Ltv/periscope/android/ui/broadcast/ag;->I:Landroid/content/Context;

    new-instance v0, Ltv/periscope/android/ui/chat/d;

    .line 311
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->e()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->e:Ltv/periscope/android/api/PsUser;

    iget-object v2, v2, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ag;->e:Ltv/periscope/android/api/PsUser;

    invoke-virtual {v3}, Ltv/periscope/android/api/PsUser;->getProfileUrlLarge()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    iget-boolean v5, p0, Ltv/periscope/android/ui/broadcast/ag;->N:Z

    new-instance v6, Ltv/periscope/android/ui/chat/b;

    iget-object v7, p0, Ltv/periscope/android/ui/broadcast/ag;->m:Lcyw;

    iget-object v8, p0, Ltv/periscope/android/ui/broadcast/ag;->d:Ljava/lang/String;

    invoke-direct {v6, v7, v8}, Ltv/periscope/android/ui/chat/b;-><init>(Lcyw;Ljava/lang/String;)V

    iget-object v8, p0, Ltv/periscope/android/ui/broadcast/ag;->m:Lcyw;

    const/4 v9, 0x0

    move-object v7, p1

    invoke-direct/range {v0 .. v9}, Ltv/periscope/android/ui/chat/d;-><init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;ZZLtv/periscope/android/ui/chat/a;Ldae;Lcyw;Ljava/lang/String;)V

    new-instance v5, Ltv/periscope/android/ui/chat/t;

    invoke-direct {v5}, Ltv/periscope/android/ui/chat/t;-><init>()V

    new-instance v6, Ltv/periscope/android/ui/chat/v;

    invoke-direct {v6}, Ltv/periscope/android/ui/chat/v;-><init>()V

    move-object v1, v11

    move-object v2, v12

    move-object v3, v10

    move-object v4, v0

    invoke-direct/range {v1 .. v6}, Ltv/periscope/android/ui/chat/g;-><init>(Landroid/content/Context;Ltv/periscope/android/ui/chat/k;Ltv/periscope/android/view/ak;Ltv/periscope/android/ui/chat/y;Ltv/periscope/android/ui/chat/ak;)V

    .line 314
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, v11}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setChatMessageAdapter(Ltv/periscope/android/ui/chat/g;)V

    .line 315
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/ag;Z)Z
    .locals 0

    .prologue
    .line 73
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/ag;->z:Z

    return p1
.end method

.method private aa()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 385
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->I:Landroid/content/Context;

    iget-boolean v2, p0, Ltv/periscope/android/ui/broadcast/ag;->O:Z

    invoke-interface {v0, v1, p0, v2}, Ltv/periscope/android/player/d;->a(Landroid/content/Context;Ltv/periscope/android/player/a;Z)V

    .line 386
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->autoPlay:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-eq v0, v1, :cond_0

    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/ag;->y:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 388
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0, v4, v5}, Ltv/periscope/android/player/d;->a(J)V

    .line 390
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->S:Lcxc;

    if-nez v0, :cond_2

    .line 391
    new-instance v0, Lcxc;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->c:Ltv/periscope/model/p;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->p:Ltv/periscope/model/af;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ag;->q:Ltv/periscope/model/u;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/ag;->R:Ltv/periscope/android/chat/g;

    invoke-direct {v0, v1, v2, v3, v4}, Lcxc;-><init>(Ltv/periscope/model/p;Ltv/periscope/model/af;Ltv/periscope/model/u;Ltv/periscope/android/chat/g;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->S:Lcxc;

    .line 393
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->h:Ltv/periscope/android/player/e;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->S:Lcxc;

    invoke-interface {v0, v1}, Ltv/periscope/android/player/e;->a(Lcxc;)V

    .line 394
    return-void
.end method

.method private ab()V
    .locals 7

    .prologue
    .line 488
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->G:Ltv/periscope/android/ui/broadcast/v;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->m:Lcyw;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ag;->h:Ltv/periscope/android/player/e;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/ag;->Q:Ltv/periscope/android/chat/i$a;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v4}, Ltv/periscope/android/player/d;->o()Z

    move-result v6

    move-object v4, p0

    invoke-virtual/range {v0 .. v6}, Ltv/periscope/android/ui/broadcast/v;->a(Lcyw;Ltv/periscope/android/player/PlayMode;Ltv/periscope/android/player/e;Ltv/periscope/android/chat/h;Ltv/periscope/android/chat/i$a;Z)V

    .line 489
    return-void
.end method

.method private ac()Z
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->p:Ltv/periscope/model/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->q:Ltv/periscope/model/u;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ad()V
    .locals 1

    .prologue
    .line 533
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->ae()V

    .line 535
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->C:Ltv/periscope/android/ui/broadcast/av;

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->C:Ltv/periscope/android/ui/broadcast/av;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/av;->k()V

    .line 538
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->v()V

    .line 540
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->s()V

    .line 543
    :cond_1
    return-void
.end method

.method private ae()V
    .locals 2

    .prologue
    .line 546
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->k:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 547
    return-void
.end method

.method private af()V
    .locals 2

    .prologue
    .line 609
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->k:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->V:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 610
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->A:Z

    .line 611
    return-void
.end method

.method private ag()V
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->C:Ltv/periscope/android/ui/broadcast/av;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/av;->i()V

    .line 615
    return-void
.end method

.method private ah()Lcxe;
    .locals 1

    .prologue
    .line 682
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->r:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 683
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->r:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxe;

    .line 685
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ai()V
    .locals 4

    .prologue
    .line 1001
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->t:Z

    if-eqz v0, :cond_1

    .line 1010
    :cond_0
    :goto_0
    return-void

    .line 1005
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->t:Z

    .line 1006
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->C:Ltv/periscope/android/ui/broadcast/av;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/av;->j()V

    .line 1007
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v0, :cond_0

    .line 1008
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/ag;->u:J

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->h:Ltv/periscope/android/player/e;

    invoke-interface {v2}, Ltv/periscope/android/player/e;->l()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/ag;->u:J

    goto :goto_0
.end method

.method private aj()V
    .locals 2

    .prologue
    .line 1059
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1072
    :goto_0
    return-void

    .line 1064
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->q()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1065
    const/high16 v0, 0x42b40000    # 90.0f

    .line 1071
    :goto_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v1, v0}, Ltv/periscope/android/player/c;->a(F)V

    goto :goto_0

    .line 1066
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->q()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 1067
    const/high16 v0, 0x43870000    # 270.0f

    goto :goto_1

    .line 1069
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Ltv/periscope/model/p;)V
    .locals 6

    .prologue
    .line 478
    if-eqz p1, :cond_1

    .line 479
    invoke-virtual {p1}, Ltv/periscope/model/p;->I()J

    move-result-wide v0

    .line 480
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->e()Landroid/content/res/Resources;

    move-result-object v2

    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-nez v3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :cond_0
    invoke-static {v2, v0, v1}, Ldag;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    .line 484
    :goto_0
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v1, v0}, Ltv/periscope/android/player/c;->b(Ljava/lang/String;)V

    .line 485
    return-void

    .line 482
    :cond_1
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->e()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Ldag;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private d(Ltv/periscope/android/player/PlayMode;)V
    .locals 11

    .prologue
    .line 331
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->C:Ltv/periscope/android/ui/broadcast/av;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->C:Ltv/periscope/android/ui/broadcast/av;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/av;->k()V

    .line 335
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->m()Ltv/periscope/android/api/ApiManager;

    move-result-object v2

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->l()Lde/greenrobot/event/c;

    move-result-object v3

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/ag;->G:Ltv/periscope/android/ui/broadcast/v;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/ag;->n:Lcyn;

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/ag;->d:Ljava/lang/String;

    sget-object v8, Lretrofit/RestAdapter$LogLevel;->NONE:Lretrofit/RestAdapter$LogLevel;

    iget-object v9, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    iget-object v10, p0, Ltv/periscope/android/ui/broadcast/ag;->P:Ltv/periscope/android/video/StreamMode;

    move-object v0, p0

    move-object v1, p0

    move-object v7, p1

    invoke-static/range {v0 .. v10}, Ltv/periscope/android/ui/broadcast/av;->a(Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/player/d;Ltv/periscope/android/video/StreamMode;)Ltv/periscope/android/ui/broadcast/av;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->C:Ltv/periscope/android/ui/broadcast/av;

    .line 338
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->X()V

    .line 340
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->C:Ltv/periscope/android/ui/broadcast/av;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->m:Lcyw;

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v3, p0

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Ltv/periscope/android/ui/broadcast/av;->a(Lcyw;ZLtv/periscope/android/ui/chat/o$c;Ltv/periscope/android/ui/chat/o$a;Ltv/periscope/android/ui/chat/b;)V

    .line 341
    return-void
.end method

.method private e(Z)V
    .locals 2

    .prologue
    .line 461
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->w:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->w:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 462
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->w:Ljava/lang/Boolean;

    .line 464
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_2

    .line 465
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->C()V

    .line 474
    :cond_1
    :goto_0
    return-void

    .line 468
    :cond_2
    if-eqz p1, :cond_3

    .line 469
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->V()V

    goto :goto_0

    .line 471
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->W()V

    goto :goto_0
.end method


# virtual methods
.method A()V
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->U()V

    .line 429
    return-void
.end method

.method B()V
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->T()V

    .line 433
    return-void
.end method

.method C()V
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->W()V

    .line 437
    return-void
.end method

.method D()Z
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->G:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->d()Z

    move-result v0

    return v0
.end method

.method public E()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 501
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->G:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->g()V

    .line 502
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->a(Z)V

    .line 505
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->q:Ltv/periscope/model/u;

    .line 507
    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->p:Ltv/periscope/model/af;

    .line 510
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->d:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_1

    .line 512
    sget-object v0, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    .line 514
    :cond_1
    return-void
.end method

.method public F()Z
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    return v0
.end method

.method public G()V
    .locals 1

    .prologue
    .line 726
    sget-object v0, Ltv/periscope/android/ui/chat/ChatState;->g:Ltv/periscope/android/ui/chat/ChatState;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->a(Ltv/periscope/android/ui/chat/ChatState;)V

    .line 727
    return-void
.end method

.method public H()V
    .locals 1

    .prologue
    .line 736
    sget-object v0, Ltv/periscope/android/ui/chat/ChatState;->h:Ltv/periscope/android/ui/chat/ChatState;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->a(Ltv/periscope/android/ui/chat/ChatState;)V

    .line 737
    return-void
.end method

.method public I()V
    .locals 1

    .prologue
    .line 517
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->R()V

    .line 518
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->k()V

    .line 521
    :cond_0
    invoke-static {}, Ltv/periscope/android/util/aa;->a()V

    .line 522
    return-void
.end method

.method public J()V
    .locals 0

    .prologue
    .line 529
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->ad()V

    .line 530
    return-void
.end method

.method public K()V
    .locals 1

    .prologue
    .line 818
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->l()V

    .line 819
    return-void
.end method

.method public L()V
    .locals 1

    .prologue
    .line 823
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->m()V

    .line 824
    return-void
.end method

.method public M()V
    .locals 1

    .prologue
    .line 828
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->n()V

    .line 829
    return-void
.end method

.method public N()V
    .locals 1

    .prologue
    .line 833
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->h()V

    .line 834
    return-void
.end method

.method public O()V
    .locals 0

    .prologue
    .line 842
    return-void
.end method

.method public P()V
    .locals 0

    .prologue
    .line 846
    return-void
.end method

.method public Q()V
    .locals 0

    .prologue
    .line 908
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->j()V

    .line 909
    return-void
.end method

.method public R()V
    .locals 0

    .prologue
    .line 620
    return-void
.end method

.method S()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 771
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->x:Z

    if-eqz v0, :cond_1

    .line 788
    :cond_0
    :goto_0
    return-void

    .line 774
    :cond_1
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->ac()Z

    move-result v0

    if-nez v0, :cond_2

    .line 775
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->e()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__loading:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 778
    :cond_2
    iput-boolean v4, p0, Ltv/periscope/android/ui/broadcast/ag;->x:Z

    .line 779
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-nez v0, :cond_3

    .line 780
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/ag;->y:J

    invoke-interface {v0, v2, v3}, Ltv/periscope/android/player/d;->a(J)V

    .line 782
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->G:Ltv/periscope/android/ui/broadcast/v;

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/ag;->y:J

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/v;->b(J)V

    .line 785
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/ag;->y:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 786
    iput-boolean v4, p0, Ltv/periscope/android/ui/broadcast/ag;->i:Z

    goto :goto_0
.end method

.method T()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1031
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->d()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->e()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1046
    :cond_0
    :goto_0
    return-void

    .line 1034
    :cond_1
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    iget-boolean v2, v2, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v2, :cond_3

    move v2, v0

    .line 1035
    :goto_1
    if-eqz v2, :cond_4

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->h()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1037
    :goto_2
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v1}, Ltv/periscope/android/player/d;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v1}, Ltv/periscope/android/player/d;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1040
    :cond_2
    if-eqz v0, :cond_5

    .line 1041
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->aj()V

    goto :goto_0

    :cond_3
    move v2, v1

    .line 1034
    goto :goto_1

    :cond_4
    move v0, v1

    .line 1035
    goto :goto_2

    .line 1042
    :cond_5
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->c:Ltv/periscope/model/p;

    if-eqz v0, :cond_0

    .line 1043
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->c:Ltv/periscope/model/p;

    invoke-virtual {v1}, Ltv/periscope/model/p;->r()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->setThumbnail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method U()V
    .locals 0

    .prologue
    .line 1049
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->V()V

    .line 1050
    return-void
.end method

.method V()V
    .locals 1

    .prologue
    .line 1053
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1054
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->j()V

    .line 1056
    :cond_0
    return-void
.end method

.method W()Z
    .locals 1

    .prologue
    .line 1075
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->p:Ltv/periscope/model/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->p:Ltv/periscope/model/af;

    invoke-virtual {v0}, Ltv/periscope/model/af;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->c:Ltv/periscope/model/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->c:Ltv/periscope/model/p;

    .line 1076
    invoke-virtual {v0}, Ltv/periscope/model/p;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1075
    :goto_0
    return v0

    .line 1076
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Lcxe;)Ltv/periscope/android/ui/broadcast/ag;
    .locals 1

    .prologue
    .line 171
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->r:Ljava/lang/ref/WeakReference;

    .line 172
    return-object p0
.end method

.method a(Ljava/lang/String;)Ltv/periscope/android/ui/broadcast/ag;
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ag;->d:Ljava/lang/String;

    .line 157
    return-object p0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->t:Z

    if-eqz v0, :cond_0

    .line 631
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->t:Z

    .line 632
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->S()V

    .line 635
    :cond_0
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 744
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->ah()Lcxe;

    move-result-object v0

    .line 745
    if-eqz v0, :cond_0

    .line 746
    new-instance v1, Ltv/periscope/android/library/PeriscopeException;

    invoke-direct {v1, p2}, Ltv/periscope/android/library/PeriscopeException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcxe;->a(Ltv/periscope/android/library/PeriscopeException;)V

    .line 748
    :cond_0
    return-void
.end method

.method public a(IZ)V
    .locals 1

    .prologue
    .line 797
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(IZ)V

    .line 798
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 658
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->t:Z

    .line 659
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/player/d;->b(J)V

    .line 662
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->i:Z

    .line 663
    return-void
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 966
    instance-of v0, p1, Lcom/google/android/exoplayer/drm/UnsupportedDrmException;

    if-eqz v0, :cond_0

    .line 967
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->I:Landroid/content/Context;

    const-string/jumbo v1, "Unsupported DRM exception"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 969
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;JZ)V
    .locals 0

    .prologue
    .line 870
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/chatman/api/Occupant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 861
    return-void
.end method

.method a(Ltv/periscope/android/event/ApiEvent;)V
    .locals 3

    .prologue
    .line 575
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 576
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->n:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 578
    if-nez v0, :cond_1

    .line 597
    :cond_0
    :goto_0
    return-void

    .line 584
    :cond_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    if-nez v1, :cond_3

    .line 585
    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    :goto_1
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->a(Ltv/periscope/android/player/PlayMode;)V

    .line 586
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->t()V

    goto :goto_0

    .line 585
    :cond_2
    sget-object v0, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    goto :goto_1

    .line 587
    :cond_3
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    sget-object v2, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v1}, Ltv/periscope/android/player/d;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 589
    invoke-virtual {v0}, Ltv/periscope/model/p;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590
    const-string/jumbo v0, "PlayerHelper"

    const-string/jumbo v1, "BroadcastPlayer is buffering but API told us it ended."

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->Q()V

    .line 592
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->af()V

    goto :goto_0
.end method

.method a(Ltv/periscope/android/event/CacheEvent;)V
    .locals 2

    .prologue
    .line 761
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    if-eqz v0, :cond_0

    .line 762
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->n:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 763
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->a(Ltv/periscope/model/p;)V

    .line 765
    :cond_0
    return-void
.end method

.method public a(Ltv/periscope/android/player/PlayMode;)V
    .locals 3

    .prologue
    .line 349
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    if-ne v0, p1, :cond_1

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    const-string/jumbo v0, "PlayerHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Switching to mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    .line 354
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 358
    sget-object v0, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne p1, v0, :cond_2

    .line 359
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->C()V

    .line 362
    :cond_2
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/ag;->d(Ltv/periscope/android/player/PlayMode;)V

    .line 363
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->Z()V

    .line 364
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->aa()V

    .line 365
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->ab()V

    .line 368
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->t()V

    goto :goto_0
.end method

.method public a(Ltv/periscope/android/ui/chat/ChatState;)V
    .locals 1

    .prologue
    .line 792
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setChatState(Ltv/periscope/android/ui/chat/ChatState;)V

    .line 793
    return-void
.end method

.method public a(Ltv/periscope/chatman/api/Sender;Z)V
    .locals 0

    .prologue
    .line 879
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 0

    .prologue
    .line 814
    return-void
.end method

.method public a(Ltv/periscope/model/p;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 441
    if-eqz p1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->c:Ltv/periscope/model/p;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 458
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    if-eqz v0, :cond_0

    .line 448
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ag;->c:Ltv/periscope/model/p;

    .line 449
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->L:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/p;->z()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 450
    invoke-virtual {p1}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ltv/periscope/model/p;->N()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->e(Z)V

    .line 451
    invoke-virtual {p0, v1}, Ltv/periscope/android/ui/broadcast/ag;->c(Z)V

    .line 453
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-virtual {p1}, Ltv/periscope/model/p;->r()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->setThumbnail(Ljava/lang/String;)V

    .line 455
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->d:Ltv/periscope/android/player/PlayMode;

    if-eq v0, v1, :cond_0

    .line 456
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/ag;->b(Ltv/periscope/model/p;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 450
    goto :goto_1
.end method

.method public a(Ltv/periscope/model/u;)V
    .locals 6

    .prologue
    .line 716
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ag;->q:Ltv/periscope/model/u;

    .line 717
    invoke-virtual {p1}, Ltv/periscope/model/u;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->d(Z)V

    .line 720
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->e:Ltv/periscope/android/api/PsUser;

    invoke-virtual {v1}, Ltv/periscope/android/api/PsUser;->getProfileUrlMedium()Ljava/lang/String;

    move-result-object v1

    .line 721
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->e()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ag;->q:Ltv/periscope/model/u;

    invoke-virtual {v3}, Ltv/periscope/model/u;->a()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Ltv/periscope/android/util/aa;->b(Landroid/content/res/Resources;J)I

    move-result v2

    .line 720
    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ljava/lang/String;I)V

    .line 722
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0, p1}, Ltv/periscope/android/player/d;->a(Z)V

    .line 642
    :cond_0
    if-eqz p1, :cond_1

    .line 643
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->G:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->b()V

    .line 647
    :goto_0
    return-void

    .line 645
    :cond_1
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->ai()V

    goto :goto_0
.end method

.method public a(Ltv/periscope/model/af;)Z
    .locals 8

    .prologue
    .line 690
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 691
    const/4 v0, 0x0

    .line 696
    :goto_0
    return v0

    .line 693
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->P:Ltv/periscope/android/video/StreamMode;

    invoke-virtual {p1}, Ltv/periscope/model/af;->c()Ljava/lang/String;

    move-result-object v3

    .line 694
    invoke-static {p1}, Ltv/periscope/android/util/g;->a(Ltv/periscope/model/af;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Ltv/periscope/model/af;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Ltv/periscope/model/af;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Ltv/periscope/model/af;->h()Ljava/util/List;

    move-result-object v7

    .line 693
    invoke-interface/range {v0 .. v7}, Ltv/periscope/android/player/d;->a(Ltv/periscope/model/p;Ltv/periscope/android/video/StreamMode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    .line 695
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->G:Ltv/periscope/android/ui/broadcast/v;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->o()Z

    move-result v2

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/v;->a(Z)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 651
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->t:Z

    .line 652
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->j()V

    .line 653
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->ag()V

    .line 654
    return-void
.end method

.method public b(IZ)V
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b(IZ)V

    .line 803
    return-void
.end method

.method public b(J)V
    .locals 0

    .prologue
    .line 874
    return-void
.end method

.method public b(Ljava/lang/String;JZ)V
    .locals 0

    .prologue
    .line 838
    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/chatman/api/Occupant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 865
    return-void
.end method

.method public b(Ltv/periscope/android/player/PlayMode;)V
    .locals 5

    .prologue
    .line 666
    new-instance v0, Ltv/periscope/android/chat/g;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->q:Ltv/periscope/model/u;

    invoke-virtual {v1}, Ltv/periscope/model/u;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ltv/periscope/android/chat/g;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->R:Ltv/periscope/android/chat/g;

    .line 667
    new-instance v0, Lcxc;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->c:Ltv/periscope/model/p;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->p:Ltv/periscope/model/af;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ag;->q:Ltv/periscope/model/u;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/ag;->R:Ltv/periscope/android/chat/g;

    invoke-direct {v0, v1, v2, v3, v4}, Lcxc;-><init>(Ltv/periscope/model/p;Ltv/periscope/model/af;Ltv/periscope/model/u;Ltv/periscope/android/chat/g;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->S:Lcxc;

    .line 668
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->h:Ltv/periscope/android/player/e;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->S:Lcxc;

    invoke-interface {v0, v1}, Ltv/periscope/android/player/e;->a(Lcxc;)V

    .line 669
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->ah()Lcxe;

    move-result-object v0

    .line 670
    if-eqz v0, :cond_0

    .line 671
    invoke-interface {v0, p0}, Lcxe;->a(Lcxb;)V

    .line 674
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->d:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->W()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 675
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->c(Z)V

    .line 676
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->S()V

    .line 678
    :cond_1
    return-void
.end method

.method public b(Ltv/periscope/chatman/api/Sender;Z)V
    .locals 0

    .prologue
    .line 884
    return-void
.end method

.method public b(Ltv/periscope/model/af;)V
    .locals 2

    .prologue
    .line 706
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ag;->p:Ltv/periscope/model/af;

    .line 708
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->n:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->a(Ltv/periscope/model/p;)V

    .line 710
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->k:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->T:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 711
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->k:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->T:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 712
    return-void
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 1014
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1028
    :cond_0
    :goto_0
    return-void

    .line 1017
    :cond_1
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/ag;->u:J

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->l()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/ag;->u:J

    .line 1018
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->O()V

    .line 1019
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->af()V

    .line 1021
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    if-eqz v0, :cond_0

    .line 1022
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_2

    .line 1023
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->C()V

    goto :goto_0

    .line 1025
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->replayable:Z

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->e(Z)V

    goto :goto_0
.end method

.method c(Ltv/periscope/android/player/PlayMode;)Ltv/periscope/android/ui/broadcast/ag;
    .locals 0

    .prologue
    .line 161
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/ag;->a(Ltv/periscope/android/player/PlayMode;)V

    .line 162
    return-object p0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 923
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->ag()V

    .line 924
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->O()V

    .line 925
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->s:Z

    .line 926
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->Y()V

    .line 927
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->U()V

    .line 928
    return-void
.end method

.method public c(J)V
    .locals 1

    .prologue
    .line 973
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->G:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 974
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->i:Z

    .line 975
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i()V

    .line 976
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->G:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/v;->a(J)V

    .line 978
    :cond_0
    return-void
.end method

.method public c(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 807
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ltv/periscope/model/chat/Message;)V

    .line 808
    return-void
.end method

.method c(Z)V
    .locals 2

    .prologue
    .line 416
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->c:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->C()V

    .line 425
    :goto_0
    return-void

    .line 420
    :cond_0
    if-eqz p1, :cond_1

    .line 421
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->A()V

    goto :goto_0

    .line 423
    :cond_1
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->B()V

    goto :goto_0
.end method

.method public c_(J)V
    .locals 3

    .prologue
    .line 854
    iput-wide p1, p0, Ltv/periscope/android/ui/broadcast/ag;->v:J

    .line 855
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->e()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, p1, p2, v2}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setParticipantCount(Ljava/lang/String;)V

    .line 856
    return-void
.end method

.method d(J)Ltv/periscope/android/ui/broadcast/ag;
    .locals 1

    .prologue
    .line 177
    iput-wide p1, p0, Ltv/periscope/android/ui/broadcast/ag;->y:J

    .line 178
    return-object p0
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setUpComposerReply(Ljava/lang/String;)V

    .line 405
    return-void
.end method

.method protected d(Z)V
    .locals 0

    .prologue
    .line 740
    return-void
.end method

.method d()Z
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->H:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 136
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->I:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 850
    return-void
.end method

.method public e(Ltv/periscope/model/chat/Message;)V
    .locals 0

    .prologue
    .line 889
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 932
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/ag;->y:J

    .line 933
    iput-boolean v2, p0, Ltv/periscope/android/ui/broadcast/ag;->x:Z

    .line 935
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ag;->ai()V

    .line 936
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->c:Ltv/periscope/model/p;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->b(Ltv/periscope/model/p;)V

    .line 937
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->T()V

    .line 940
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->j:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_0

    .line 941
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->c:Ltv/periscope/model/p;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv/periscope/model/p;->b(Z)V

    .line 942
    sget-object v0, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/ag;->a(Ltv/periscope/android/player/PlayMode;)V

    .line 944
    :cond_0
    invoke-virtual {p0, v2}, Ltv/periscope/android/ui/broadcast/ag;->c(Z)V

    .line 945
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 913
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->e()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__loading:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->a(Ljava/lang/String;)V

    .line 914
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 919
    return-void
.end method

.method public i()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 949
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->z:Z

    if-eqz v0, :cond_1

    .line 962
    :cond_0
    :goto_0
    return-void

    .line 952
    :cond_1
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    .line 953
    invoke-interface {v0}, Ltv/periscope/android/player/d;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 954
    const-string/jumbo v0, "PlayerHelper"

    const-string/jumbo v1, "Buffering detected."

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->k:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->U:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 956
    iput-boolean v4, p0, Ltv/periscope/android/ui/broadcast/ag;->z:Z

    .line 957
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->A:Z

    if-nez v0, :cond_0

    .line 958
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->k:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->V:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 959
    iput-boolean v4, p0, Ltv/periscope/android/ui/broadcast/ag;->A:Z

    goto :goto_0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 898
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 899
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->k()V

    .line 904
    :goto_0
    return-void

    .line 902
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->f()V

    goto :goto_0
.end method

.method public k()J
    .locals 2

    .prologue
    .line 893
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/ag;->v:J

    return-wide v0
.end method

.method l()Lde/greenrobot/event/c;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->J:Lde/greenrobot/event/c;

    return-object v0
.end method

.method m()Ltv/periscope/android/api/ApiManager;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->K:Ltv/periscope/android/api/ApiManager;

    return-object v0
.end method

.method protected n()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1388

    .line 252
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->E:Z

    if-eqz v0, :cond_1

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->g:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->q()I

    move-result v0

    .line 259
    iget-boolean v1, p0, Ltv/periscope/android/ui/broadcast/ag;->D:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->I:Landroid/content/Context;

    invoke-static {v1}, Ltv/periscope/android/util/ad;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    if-eqz v0, :cond_3

    .line 260
    :cond_2
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v1, v0, v2, v3}, Ltv/periscope/android/player/c;->a(IJ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 261
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->D:Z

    .line 268
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->k:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->T:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 269
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->k:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ag;->T:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 264
    :cond_4
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->a:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->N()V

    goto :goto_0
.end method

.method public o()V
    .locals 0

    .prologue
    .line 983
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 2

    .prologue
    .line 566
    sget-object v0, Ltv/periscope/android/ui/broadcast/ag$4;->b:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 572
    :goto_0
    return-void

    .line 568
    :pswitch_0
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/ag;->a(Ltv/periscope/android/event/ApiEvent;)V

    goto :goto_0

    .line 566
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onEventMainThread(Ltv/periscope/android/event/CacheEvent;)V
    .locals 2

    .prologue
    .line 752
    sget-object v0, Ltv/periscope/android/ui/broadcast/ag$4;->c:[I

    invoke-virtual {p1}, Ltv/periscope/android/event/CacheEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 758
    :goto_0
    return-void

    .line 754
    :pswitch_0
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/ag;->a(Ltv/periscope/android/event/CacheEvent;)V

    goto :goto_0

    .line 752
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public p()V
    .locals 0

    .prologue
    .line 988
    return-void
.end method

.method public q()V
    .locals 0

    .prologue
    .line 993
    return-void
.end method

.method public r()V
    .locals 0

    .prologue
    .line 998
    return-void
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 409
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final t()V
    .locals 1

    .prologue
    .line 273
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->B:Z

    if-eqz v0, :cond_1

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ag;->d:Ljava/lang/String;

    invoke-static {v0}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->B:Z

    .line 281
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->u()V

    goto :goto_0
.end method

.method protected u()V
    .locals 1

    .prologue
    .line 285
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->l()Lde/greenrobot/event/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 286
    return-void
.end method

.method protected final v()V
    .locals 1

    .prologue
    .line 289
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->B:Z

    if-nez v0, :cond_0

    .line 294
    :goto_0
    return-void

    .line 292
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/ag;->B:Z

    .line 293
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->w()V

    goto :goto_0
.end method

.method protected w()V
    .locals 1

    .prologue
    .line 297
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ag;->l()Lde/greenrobot/event/c;

    move-result-object v0

    .line 298
    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 299
    return-void
.end method

.method protected x()Ltv/periscope/android/ui/chat/k;
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x0

    return-object v0
.end method

.method public y()V
    .locals 0

    .prologue
    .line 397
    return-void
.end method

.method public z()V
    .locals 0

    .prologue
    .line 400
    return-void
.end method
