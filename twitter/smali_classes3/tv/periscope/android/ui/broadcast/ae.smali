.class public Ltv/periscope/android/ui/broadcast/ae;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$k;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private final b:Ltv/periscope/android/view/PsButton;

.field private final c:Landroid/content/Context;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p3}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 30
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ae;->c:Landroid/content/Context;

    .line 31
    sget v0, Ltv/periscope/android/library/f$g;->cta_action_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsButton;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ae;->b:Ltv/periscope/android/view/PsButton;

    .line 32
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ae;->b:Ltv/periscope/android/view/PsButton;

    invoke-virtual {v0, p0}, Ltv/periscope/android/view/PsButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/ae;
    .locals 3

    .prologue
    .line 24
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__periscope_cta_info_item:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 25
    new-instance v1, Ltv/periscope/android/ui/broadcast/ae;

    invoke-direct {v1, v0, p0, p2}, Ltv/periscope/android/ui/broadcast/ae;-><init>(Landroid/view/View;Landroid/content/Context;Ltv/periscope/android/ui/broadcast/h;)V

    return-object v1
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$k;)V
    .locals 3

    .prologue
    .line 37
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$k;->b()Ltv/periscope/android/ui/broadcast/j;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ae;->d:Ljava/lang/String;

    .line 38
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ae;->c:Landroid/content/Context;

    invoke-static {v0}, Ltv/periscope/android/util/j;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ae;->b:Ltv/periscope/android/view/PsButton;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ae;->c:Landroid/content/Context;

    sget v2, Ltv/periscope/android/library/f$l;->ps__cta_open_app:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsButton;->setText(Ljava/lang/CharSequence;)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ae;->b:Ltv/periscope/android/view/PsButton;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ae;->c:Landroid/content/Context;

    sget v2, Ltv/periscope/android/library/f$l;->ps__cta_install_app:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$k;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/ae;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$k;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ae;->d:Ljava/lang/String;

    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ae;->a:Ltv/periscope/android/ui/broadcast/h;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ae;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/h;->h(Ljava/lang/String;)V

    .line 50
    :cond_0
    return-void
.end method
