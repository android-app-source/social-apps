.class public Ltv/periscope/android/ui/broadcast/BottomTray;
.super Landroid/widget/LinearLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;
    }
.end annotation


# instance fields
.field private A:Ltv/periscope/model/chat/Message;

.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/EditText;

.field private h:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Ltv/periscope/android/ui/chat/ai;

.field private l:Ltv/periscope/android/ui/chat/ag;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:Ltv/periscope/android/ui/chat/ChatState;

.field private q:Ltv/periscope/android/ui/chat/ChatState;

.field private r:Landroid/app/Dialog;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Ltv/periscope/android/view/MaskImageView;

.field private v:Landroid/view/View;

.field private w:Landroid/view/View;

.field private x:Landroid/view/View;

.field private y:Ldae;

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->z:Z

    .line 75
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/BottomTray;->a(Landroid/content/Context;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->z:Z

    .line 80
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/BottomTray;->a(Landroid/content/Context;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->z:Z

    .line 85
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/BottomTray;->a(Landroid/content/Context;)V

    .line 86
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/BottomTray;)Ltv/periscope/android/ui/chat/ChatState;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->p:Ltv/periscope/android/ui/chat/ChatState;

    return-object v0
.end method

.method private a(II)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 368
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->s:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 369
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->t:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 370
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->r:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 371
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 112
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__bottom_tray:I

    invoke-virtual {v0, v1, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 114
    sget v0, Ltv/periscope/android/library/f$g;->line:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->w:Landroid/view/View;

    .line 115
    sget v0, Ltv/periscope/android/library/f$g;->button_container:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->a:Landroid/view/View;

    .line 116
    sget v0, Ltv/periscope/android/library/f$g;->btn_play_icon:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->d:Landroid/widget/ImageView;

    .line 117
    sget v0, Ltv/periscope/android/library/f$g;->participants:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->x:Landroid/view/View;

    .line 118
    sget v0, Ltv/periscope/android/library/f$g;->overflow_button:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->b:Landroid/view/View;

    .line 119
    sget v0, Ltv/periscope/android/library/f$g;->action_button:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->c:Landroid/view/View;

    .line 120
    sget v0, Ltv/periscope/android/library/f$g;->cancel_comment:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->e:Landroid/view/View;

    .line 121
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->e:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    sget v0, Ltv/periscope/android/library/f$g;->comment_send:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->f:Landroid/view/View;

    .line 123
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->f:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    sget v0, Ltv/periscope/android/library/f$g;->compose_layout:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->v:Landroid/view/View;

    .line 125
    sget v0, Ltv/periscope/android/library/f$g;->compose_comment:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    .line 126
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    new-array v1, v6, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0x64

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 127
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_private:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->o:Landroid/graphics/drawable/Drawable;

    .line 128
    sget v0, Ltv/periscope/android/library/f$g;->friends_watching_view:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->i:Landroid/view/View;

    .line 129
    sget v0, Ltv/periscope/android/library/f$g;->play_time:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->j:Landroid/view/View;

    .line 130
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->o:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 133
    :cond_0
    sget v0, Ltv/periscope/android/library/f$g;->masked_avatar:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/MaskImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->u:Ltv/periscope/android/view/MaskImageView;

    .line 134
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$e;->ps__card_corner_radius:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 136
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/android/util/ac;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 137
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->u:Ltv/periscope/android/view/MaskImageView;

    const/4 v2, 0x4

    new-array v2, v2, [F

    aput v4, v2, v5

    aput v0, v2, v6

    aput v0, v2, v7

    aput v4, v2, v8

    invoke-virtual {v1, v2}, Ltv/periscope/android/view/MaskImageView;->setCornerRadius([F)V

    .line 142
    :goto_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$f;->ps__bg_bottom_tray_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->n:Landroid/graphics/drawable/Drawable;

    .line 144
    sget v0, Ltv/periscope/android/library/f$g;->chat_status:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    .line 145
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BottomTray$1;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/BottomTray$1;-><init>(Ltv/periscope/android/ui/broadcast/BottomTray;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    sget-object v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->a:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->h:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    .line 181
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 183
    new-instance v0, Ltv/periscope/android/ui/broadcast/BottomTray$2;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/BottomTray$2;-><init>(Ltv/periscope/android/ui/broadcast/BottomTray;)V

    .line 193
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 195
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__chat_state_dialog:I

    .line 196
    invoke-virtual {v0, v1, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 197
    sget v0, Ltv/periscope/android/library/f$g;->title:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->s:Landroid/widget/TextView;

    .line 198
    sget v0, Ltv/periscope/android/library/f$g;->message:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->t:Landroid/widget/TextView;

    .line 199
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 200
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->r:Landroid/app/Dialog;

    .line 202
    return-void

    .line 139
    :cond_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->u:Ltv/periscope/android/view/MaskImageView;

    const/4 v2, 0x4

    new-array v2, v2, [F

    aput v0, v2, v5

    aput v4, v2, v6

    aput v4, v2, v7

    aput v0, v2, v8

    invoke-virtual {v1, v2}, Ltv/periscope/android/view/MaskImageView;->setCornerRadius([F)V

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/BottomTray;II)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/BottomTray;->a(II)V

    return-void
.end method

.method private a(Ltv/periscope/android/ui/chat/ChatState;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 297
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 298
    sget-object v0, Ltv/periscope/android/ui/broadcast/BottomTray$5;->a:[I

    invoke-virtual {p1}, Ltv/periscope/android/ui/chat/ChatState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 333
    :goto_0
    return-void

    .line 300
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__broadcast_too_full:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 301
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 305
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__connecting:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 306
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 310
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__comment_hint:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 311
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 315
    :pswitch_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__broadcast_limited:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 316
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 321
    :pswitch_4
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 322
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$e;->ps__btn_horizontal_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 323
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 328
    :pswitch_5
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__connection_error:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 329
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 298
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 228
    sget-object v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->a:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->h:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    .line 229
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    invoke-static {v0}, Ltv/periscope/android/util/n;->a(Landroid/view/View;)V

    .line 231
    if-eqz p1, :cond_0

    .line 232
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->d()V

    .line 240
    :goto_0
    return-void

    .line 234
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->v:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/BottomTray;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->z:Z

    return p1
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/BottomTray;)Ltv/periscope/android/ui/chat/ag;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->l:Ltv/periscope/android/ui/chat/ag;

    return-object v0
.end method

.method static synthetic c(Ltv/periscope/android/ui/broadcast/BottomTray;)Ltv/periscope/model/chat/Message;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->A:Ltv/periscope/model/chat/Message;

    return-object v0
.end method

.method static synthetic d(Ltv/periscope/android/ui/broadcast/BottomTray;)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->a:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Ltv/periscope/android/ui/broadcast/BottomTray;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Ltv/periscope/android/ui/broadcast/BottomTray;)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->v:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(Ltv/periscope/android/ui/broadcast/BottomTray;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic h(Ltv/periscope/android/ui/broadcast/BottomTray;)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic i(Ltv/periscope/android/ui/broadcast/BottomTray;)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->e:Landroid/view/View;

    return-object v0
.end method

.method private r()V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    invoke-static {v0}, Ltv/periscope/android/util/n;->b(Landroid/view/View;)V

    .line 244
    sget-object v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->b:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->h:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    .line 245
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->a()V

    .line 246
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 209
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->h:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    sget-object v1, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->a:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    if-ne v0, v1, :cond_0

    .line 210
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 221
    :goto_0
    return-void

    .line 212
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->z:Z

    if-eqz v0, :cond_1

    .line 213
    sget-object v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->c:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->h:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    .line 214
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 217
    :cond_1
    sget-object v0, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->b:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->h:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    .line 218
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 219
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)V
    .locals 3
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 424
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->u:Ltv/periscope/android/view/MaskImageView;

    invoke-virtual {v0, p2}, Ltv/periscope/android/view/MaskImageView;->setColorFilter(I)V

    .line 425
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->y:Ldae;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->u:Ltv/periscope/android/view/MaskImageView;

    invoke-interface {v0, v1, p1, v2}, Ldae;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 426
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 225
    return-void
.end method

.method c()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 249
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 250
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->v:Landroid/view/View;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 251
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 252
    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 253
    new-instance v0, Ltv/periscope/android/ui/broadcast/BottomTray$3;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/BottomTray$3;-><init>(Ltv/periscope/android/ui/broadcast/BottomTray;)V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 262
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    .line 263
    return-void

    .line 249
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 250
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method d()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 266
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 267
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->v:Landroid/view/View;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 268
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 269
    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 270
    new-instance v0, Ltv/periscope/android/ui/broadcast/BottomTray$4;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/BottomTray$4;-><init>(Ltv/periscope/android/ui/broadcast/BottomTray;)V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 280
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    .line 281
    return-void

    .line 266
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 267
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public e()V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->p:Ltv/periscope/android/ui/chat/ChatState;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->q:Ltv/periscope/android/ui/chat/ChatState;

    .line 341
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 344
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->q:Ltv/periscope/android/ui/chat/ChatState;

    if-nez v0, :cond_0

    .line 351
    :goto_0
    return-void

    .line 347
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->q:Ltv/periscope/android/ui/chat/ChatState;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->p:Ltv/periscope/android/ui/chat/ChatState;

    .line 348
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->p:Ltv/periscope/android/ui/chat/ChatState;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->a(Ltv/periscope/android/ui/chat/ChatState;)V

    .line 349
    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->q:Ltv/periscope/android/ui/chat/ChatState;

    .line 350
    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->A:Ltv/periscope/model/chat/Message;

    goto :goto_0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->r:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->r:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 357
    :cond_0
    return-void
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->h:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    sget-object v1, Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;->a:Ltv/periscope/android/ui/broadcast/BottomTray$ComposeState;

    if-eq v0, v1, :cond_0

    .line 361
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 362
    const/4 v0, 0x1

    .line 364
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 375
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->w:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 376
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 377
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 378
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->x:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 379
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 380
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 381
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->v:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 382
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 383
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 384
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 385
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 386
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->i:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 390
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 393
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 394
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    .line 397
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->x:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 398
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 402
    return-void
.end method

.method public n()V
    .locals 2

    .prologue
    .line 405
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->d:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_play:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 406
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 407
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    .line 410
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->d:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_pause:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 411
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 412
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 442
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 443
    sget v1, Ltv/periscope/android/library/f$g;->cancel_comment:I

    if-ne v0, v1, :cond_1

    .line 444
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 455
    :cond_0
    :goto_0
    return-void

    .line 445
    :cond_1
    sget v1, Ltv/periscope/android/library/f$g;->comment_send:I

    if-ne v0, v1, :cond_0

    .line 446
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 447
    invoke-static {v0}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 450
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->k:Ltv/periscope/android/ui/chat/ai;

    if-eqz v1, :cond_2

    .line 451
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->k:Ltv/periscope/android/ui/chat/ai;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Ltv/periscope/android/util/o;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ltv/periscope/android/ui/chat/ai;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Ltv/periscope/android/library/f$g;->compose_comment:I

    if-ne v0, v1, :cond_0

    .line 91
    if-eqz p2, :cond_1

    .line 92
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->r()V

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->a(Z)V

    goto :goto_0
.end method

.method public p()V
    .locals 2

    .prologue
    .line 429
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->w:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 430
    return-void
.end method

.method public q()V
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->w:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 434
    return-void
.end method

.method public setChatState(Ltv/periscope/android/ui/chat/ChatState;)V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->p:Ltv/periscope/android/ui/chat/ChatState;

    sget-object v1, Ltv/periscope/android/ui/chat/ChatState;->f:Ltv/periscope/android/ui/chat/ChatState;

    if-ne v0, v1, :cond_1

    .line 287
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->q:Ltv/periscope/android/ui/chat/ChatState;

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->p:Ltv/periscope/android/ui/chat/ChatState;

    if-eq v0, p1, :cond_0

    .line 291
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->p:Ltv/periscope/android/ui/chat/ChatState;

    .line 292
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->p:Ltv/periscope/android/ui/chat/ChatState;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/BottomTray;->a(Ltv/periscope/android/ui/chat/ChatState;)V

    goto :goto_0
.end method

.method public setChatStatus(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->m:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 337
    return-void
.end method

.method public setImageLoader(Ldae;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->y:Ldae;

    .line 101
    return-void
.end method

.method public setLocalPunishmentPrompt(Ltv/periscope/model/chat/Message;)V
    .locals 0

    .prologue
    .line 437
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->A:Ltv/periscope/model/chat/Message;

    .line 438
    return-void
.end method

.method public setOverflowClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 459
    return-void
.end method

.method public setPunishmentStatusDelegate(Ltv/periscope/android/ui/chat/ag;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->l:Ltv/periscope/android/ui/chat/ag;

    .line 109
    return-void
.end method

.method public setSendCommentDelegate(Ltv/periscope/android/ui/chat/ai;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->k:Ltv/periscope/android/ui/chat/ai;

    .line 105
    return-void
.end method

.method public setSendEnabled(Z)V
    .locals 0

    .prologue
    .line 205
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->z:Z

    .line 206
    return-void
.end method

.method public setUpReply(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 415
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->p:Ltv/periscope/android/ui/chat/ChatState;

    sget-object v1, Ltv/periscope/android/ui/chat/ChatState;->e:Ltv/periscope/android/ui/chat/ChatState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->p:Ltv/periscope/android/ui/chat/ChatState;

    sget-object v1, Ltv/periscope/android/ui/chat/ChatState;->f:Ltv/periscope/android/ui/chat/ChatState;

    if-ne v0, v1, :cond_1

    .line 421
    :cond_0
    :goto_0
    return-void

    .line 418
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 419
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BottomTray;->g:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    .line 420
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BottomTray;->c()V

    goto :goto_0
.end method
