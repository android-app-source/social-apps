.class Ltv/periscope/android/ui/broadcast/af$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/RootDragLayout$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/af;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/af;)V
    .locals 0

    .prologue
    .line 2634
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2678
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2687
    :cond_0
    :goto_0
    return-void

    .line 2681
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/af;->d(Ltv/periscope/android/ui/broadcast/af;)Lcyn;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, v1, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 2684
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-static {v1}, Ltv/periscope/android/ui/broadcast/af;->e(Ltv/periscope/android/ui/broadcast/af;)Lcyw;

    move-result-object v1

    invoke-virtual {v0}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcyw;->l(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2685
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v1

    invoke-virtual {v0}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ltv/periscope/android/api/ApiManager;->getSuperfans(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2664
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->z()V

    .line 2666
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Ltv/periscope/android/library/f$g;->bottom_drag_child:I

    if-ne v0, v1, :cond_1

    .line 2667
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v1, Ltv/periscope/android/player/PlayMode;->d:Ltv/periscope/android/player/PlayMode;

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    if-eqz v0, :cond_2

    .line 2668
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->v()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, v1, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v1}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v2, v2, Ltv/periscope/android/ui/broadcast/af;->y:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/api/ApiManager;->getBroadcastViewers(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 2669
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->i(Ltv/periscope/android/analytics/summary/b;)V

    .line 2673
    :goto_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/af$b;->a()V

    .line 2675
    :cond_1
    return-void

    .line 2671
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->r:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->f()V

    goto :goto_0
.end method

.method public a(Landroid/view/View;FIIII)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 2653
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Ltv/periscope/android/library/f$g;->bottom_drag_child:I

    if-ne v0, v1, :cond_0

    .line 2654
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->setVisibility(I)V

    .line 2655
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, p2}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->setTitleAlpha(F)V

    .line 2656
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    sub-float v1, v2, p2

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->setPlayPauseAlpha(F)V

    .line 2657
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sub-float v1, v2, p2

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setChatAlpha(F)V

    .line 2658
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 2660
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2637
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 2638
    sget v1, Ltv/periscope/android/library/f$g;->broadcast__action_sheet:I

    if-ne v0, v1, :cond_1

    .line 2639
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0, p2}, Ltv/periscope/android/ui/broadcast/af;->a(I)V

    .line 2649
    :cond_0
    :goto_0
    return-void

    .line 2640
    :cond_1
    sget v1, Ltv/periscope/android/library/f$g;->bottom_drag_child:I

    if-ne v0, v1, :cond_0

    .line 2643
    if-ne p2, v2, :cond_2

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, v1, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2644
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->V()V

    .line 2647
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/broadcast/af;->d(Z)V

    goto :goto_0
.end method

.method public b(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2691
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->A()V

    .line 2693
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Ltv/periscope/android/library/f$g;->bottom_drag_child:I

    if-ne v0, v1, :cond_0

    .line 2694
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    .line 2695
    invoke-interface {v0}, Ltv/periscope/android/player/d;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2709
    :cond_0
    :goto_0
    return-void

    .line 2699
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2701
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->S()V

    .line 2702
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/broadcast/af;->c(Z)V

    .line 2703
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->W()V

    goto :goto_0

    .line 2704
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2705
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/broadcast/af;->c(Z)V

    .line 2706
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$b;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->J()V

    goto :goto_0
.end method
