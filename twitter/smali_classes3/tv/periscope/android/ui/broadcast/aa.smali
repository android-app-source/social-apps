.class Ltv/periscope/android/ui/broadcast/aa;
.super Ltv/periscope/android/ui/broadcast/av;
.source "Twttr"


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/video/StreamMode;)V
    .locals 11

    .prologue
    .line 26
    sget-object v8, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Ltv/periscope/android/ui/broadcast/av;-><init>(Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/video/StreamMode;)V

    .line 28
    return-void
.end method


# virtual methods
.method public a(Ltv/periscope/model/af;)V
    .locals 4

    .prologue
    .line 41
    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v1

    .line 42
    invoke-virtual {v1}, Ltv/periscope/model/p;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ltv/periscope/model/p;->N()Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aa;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/av$a;->Q()V

    .line 53
    :goto_0
    return-void

    .line 47
    :cond_0
    sget-object v0, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    .line 48
    invoke-virtual {v1}, Ltv/periscope/model/p;->K()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Ltv/periscope/model/p;->N()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    sget-object v0, Ltv/periscope/android/player/PlayMode;->c:Ltv/periscope/android/player/PlayMode;

    .line 50
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/aa;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v1, v0}, Ltv/periscope/android/ui/broadcast/av$a;->a(Ltv/periscope/android/player/PlayMode;)V

    .line 52
    :cond_1
    invoke-virtual {p1}, Ltv/periscope/model/af;->k()Ltv/periscope/model/StreamType;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/aa;->f:Lretrofit/RestAdapter$LogLevel;

    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v3

    invoke-virtual {v3}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v1, v0, v2, v3}, Ltv/periscope/android/ui/broadcast/aa;->a(Ltv/periscope/model/StreamType;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aa;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/aa;->a(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aa;->c:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/aa;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 34
    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/aa;->a(Ltv/periscope/model/p;)V

    .line 37
    :cond_0
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 58
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/aa;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/aa;->d:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v1, v0}, Ltv/periscope/android/api/ApiManager;->getBroadcasts(Ljava/util/ArrayList;)Ljava/lang/String;

    .line 60
    return-void
.end method
