.class abstract Ltv/periscope/android/ui/broadcast/av;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcxd$a;
.implements Ltv/periscope/android/ui/broadcast/v$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/av$a;
    }
.end annotation


# instance fields
.field protected final a:Ltv/periscope/android/ui/broadcast/av$a;

.field protected final b:Ljava/lang/String;

.field protected final c:Lcyn;

.field protected final d:Ltv/periscope/android/api/ApiManager;

.field protected final e:Lde/greenrobot/event/c;

.field final f:Lretrofit/RestAdapter$LogLevel;

.field private g:Lcxd;

.field private final h:Ltv/periscope/android/ui/broadcast/v;

.field private final i:Ltv/periscope/android/video/StreamMode;

.field private j:Z


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/video/StreamMode;)V
    .locals 11

    .prologue
    .line 86
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    invoke-direct/range {v0 .. v9}, Ltv/periscope/android/ui/broadcast/av;-><init>(Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Lcxd;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/video/StreamMode;)V

    .line 87
    new-instance v1, Lcxd;

    const/4 v5, 0x0

    new-instance v6, Ltv/periscope/android/ui/chat/af;

    invoke-direct {v6}, Ltv/periscope/android/ui/chat/af;-><init>()V

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    move-object v2, p3

    move-object v3, p1

    move-object/from16 v4, p8

    invoke-direct/range {v1 .. v10}, Lcxd;-><init>(Ltv/periscope/android/api/ApiManager;Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/player/PlayMode;Lcxd$a;Ltv/periscope/android/ui/chat/p;ZJZ)V

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    .line 89
    return-void
.end method

.method constructor <init>(Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Lcxd;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/video/StreamMode;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    .line 97
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/av;->d:Ltv/periscope/android/api/ApiManager;

    .line 98
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/av;->e:Lde/greenrobot/event/c;

    .line 99
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    .line 100
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/av;->h:Ltv/periscope/android/ui/broadcast/v;

    .line 101
    iput-object p6, p0, Ltv/periscope/android/ui/broadcast/av;->c:Lcyn;

    .line 102
    iput-object p7, p0, Ltv/periscope/android/ui/broadcast/av;->b:Ljava/lang/String;

    .line 103
    iput-object p8, p0, Ltv/periscope/android/ui/broadcast/av;->f:Lretrofit/RestAdapter$LogLevel;

    .line 104
    iput-object p9, p0, Ltv/periscope/android/ui/broadcast/av;->i:Ltv/periscope/android/video/StreamMode;

    .line 105
    return-void
.end method

.method static a(Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/player/d;Ltv/periscope/android/video/StreamMode;)Ltv/periscope/android/ui/broadcast/av;
    .locals 11

    .prologue
    .line 64
    sget-object v0, Ltv/periscope/android/ui/broadcast/av$1;->a:[I

    invoke-virtual/range {p7 .. p7}, Ltv/periscope/android/player/PlayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 73
    new-instance v0, Ltv/periscope/android/ui/broadcast/aa;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p8

    move-object/from16 v9, p10

    invoke-direct/range {v0 .. v9}, Ltv/periscope/android/ui/broadcast/aa;-><init>(Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/video/StreamMode;)V

    .line 78
    :goto_0
    invoke-virtual {p4, v0}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/android/ui/broadcast/v$a;)V

    .line 79
    return-object v0

    .line 67
    :pswitch_0
    new-instance v0, Ltv/periscope/android/ui/broadcast/al;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p8

    move-object/from16 v9, p10

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Ltv/periscope/android/ui/broadcast/al;-><init>(Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/ui/broadcast/av$a;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Ltv/periscope/android/ui/broadcast/v;Lcyn;Ljava/lang/String;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/video/StreamMode;Ltv/periscope/android/player/d;)V

    goto :goto_0

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private b(Ltv/periscope/model/af;)Z
    .locals 3

    .prologue
    .line 265
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcast/av$a;->a(Ltv/periscope/model/af;)Z

    move-result v0

    .line 266
    if-nez v0, :cond_0

    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/p;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/av$a;->a(ILjava/lang/String;)V

    .line 268
    const/4 v0, 0x0

    .line 270
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/content/res/Resources;Landroid/os/Handler;Ltv/periscope/android/chat/a;Ltv/periscope/android/ui/broadcast/ar;Ltv/periscope/android/api/PsUser;ZLtv/periscope/android/ui/chat/r;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Ltv/periscope/android/ui/chat/w;Lcyw;Lcxm;Ltv/periscope/android/ui/chat/x;Ltv/periscope/android/ui/chat/aj;)Ltv/periscope/android/ui/chat/m;
    .locals 18

    .prologue
    .line 131
    new-instance v10, Ltv/periscope/android/ui/chat/ae$a;

    const/16 v1, 0x19

    const/16 v2, 0x1f4

    const/4 v3, 0x4

    invoke-direct {v10, v1, v2, v3}, Ltv/periscope/android/ui/chat/ae$a;-><init>(III)V

    .line 134
    new-instance v1, Ltv/periscope/android/ui/chat/n;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/ui/broadcast/av;->d:Ltv/periscope/android/api/ApiManager;

    new-instance v14, Ldbe;

    invoke-direct {v14}, Ldbe;-><init>()V

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v11, p4

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v15, p12

    move-object/from16 v16, p13

    move-object/from16 v17, p14

    invoke-direct/range {v1 .. v17}, Ltv/periscope/android/ui/chat/n;-><init>(Landroid/content/res/Resources;Landroid/os/Handler;Ltv/periscope/android/api/ApiManager;Ltv/periscope/android/chat/a;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Ltv/periscope/android/api/PsUser;ZLtv/periscope/android/ui/chat/ae$a;Ltv/periscope/android/ui/broadcast/ar;Ltv/periscope/android/ui/chat/w;Lcyw;Ldbd;Lcxm;Ltv/periscope/android/ui/chat/x;Ltv/periscope/android/ui/chat/aj;)V

    .line 137
    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/chat/n;->a(Ltv/periscope/android/ui/chat/r;)V

    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/av;->h:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v2, v1}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/android/ui/chat/m;)V

    .line 139
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v2, v1}, Lcxd;->a(Ltv/periscope/android/ui/chat/p;)V

    .line 140
    return-object v1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/av$a;->G()V

    .line 280
    return-void
.end method

.method public a(Lcyw;ZLtv/periscope/android/ui/chat/o$c;Ltv/periscope/android/ui/chat/o$a;Ltv/periscope/android/ui/chat/b;)V
    .locals 9

    .prologue
    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/av;->j:Z

    .line 117
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->e:Lde/greenrobot/event/c;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 119
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->h:Ltv/periscope/android/ui/broadcast/v;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/av;->c:Lcyn;

    const/4 v6, 0x0

    iget-object v8, p0, Ltv/periscope/android/ui/broadcast/av;->b:Ljava/lang/String;

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v8}, Ltv/periscope/android/ui/broadcast/v;->a(Lcyw;Lcyn;ZLtv/periscope/android/ui/chat/o$c;Ltv/periscope/android/ui/chat/o$a;Ltv/periscope/android/ui/chat/o$b;Ltv/periscope/android/ui/chat/b;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/av;->g()V

    .line 123
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v0, p1}, Lcxd;->a(Ljava/lang/String;)V

    .line 153
    return-void
.end method

.method a(Ltv/periscope/model/StreamType;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->h:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0, p1, p2, p3, p4}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/model/StreamType;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v0, p2}, Ltv/periscope/android/ui/broadcast/av$a;->b(Ltv/periscope/android/player/PlayMode;)V

    .line 169
    return-void
.end method

.method abstract a(Ltv/periscope/model/af;)V
.end method

.method a(Ltv/periscope/model/p;)V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcast/av$a;->a(Ltv/periscope/model/p;)V

    .line 275
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/av$a;->H()V

    .line 285
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v0}, Lcxd;->h()V

    .line 291
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v0}, Lcxd;->h()V

    .line 297
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v0}, Lcxd;->h()V

    .line 303
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v0}, Lcxd;->i()V

    .line 308
    return-void
.end method

.method abstract g()V
.end method

.method abstract h()V
.end method

.method i()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v0}, Lcxd;->d()V

    .line 157
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->h:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->a()V

    .line 158
    return-void
.end method

.method j()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v0}, Lcxd;->e()V

    .line 162
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->h:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->c()V

    .line 163
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v0}, Lcxd;->a()V

    .line 173
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->h:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/v;->e()V

    .line 175
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/av;->j:Z

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->e:Lde/greenrobot/event/c;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 177
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/av;->j:Z

    .line 179
    :cond_0
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 4

    .prologue
    const/16 v3, 0x194

    .line 182
    sget-object v0, Ltv/periscope/android/ui/broadcast/av$1;->b:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 184
    :pswitch_0
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 185
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/model/af;

    .line 186
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/av;->i:Ltv/periscope/android/video/StreamMode;

    sget-object v2, Ltv/periscope/android/video/StreamMode;->c:Ltv/periscope/android/video/StreamMode;

    if-ne v1, v2, :cond_1

    .line 187
    sget-object v1, Ltv/periscope/model/StreamType;->c:Ltv/periscope/model/StreamType;

    invoke-virtual {v0, v1}, Ltv/periscope/model/af;->a(Ltv/periscope/model/StreamType;)V

    .line 190
    :cond_1
    invoke-virtual {v0}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v1

    .line 191
    invoke-virtual {v1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v2

    .line 195
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/av;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 200
    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/av;->b(Ltv/periscope/model/af;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 205
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v2, v0}, Ltv/periscope/android/ui/broadcast/av$a;->b(Ltv/periscope/model/af;)V

    .line 208
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v3}, Ltv/periscope/android/ui/broadcast/av$a;->F()Z

    move-result v3

    invoke-virtual {v2, v0, v1, v3}, Lcxd;->a(Ltv/periscope/model/af;Ltv/periscope/model/p;Z)V

    goto :goto_0

    .line 210
    :cond_2
    const-string/jumbo v0, "VC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to load broadcast "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->c()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 212
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ltv/periscope/android/ui/broadcast/av$a;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 214
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    const/4 v1, 0x1

    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/av$a;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 220
    :pswitch_1
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcxd;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 222
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/model/u;

    .line 223
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/av;->h:Ltv/periscope/android/ui/broadcast/v;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/model/u;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 224
    const-string/jumbo v1, "VC"

    const-string/jumbo v2, "ChatAccess changed."

    invoke-static {v1, v2}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v1}, Lcxd;->f()V

    .line 227
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v1, v0}, Ltv/periscope/android/ui/broadcast/av$a;->a(Ltv/periscope/model/u;)V

    .line 228
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/av;->h:Ltv/periscope/android/ui/broadcast/v;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v2}, Lcxd;->c()Ltv/periscope/model/StreamType;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/model/StreamType;Ltv/periscope/model/u;)V

    .line 229
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v0}, Lcxd;->b()Ltv/periscope/model/af;

    move-result-object v0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/av;->a(Ltv/periscope/model/af;)V

    goto/16 :goto_0

    .line 231
    :cond_4
    const-string/jumbo v0, "VC"

    const-string/jumbo v1, "ChatAccess succeeded but was unchanged."

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    invoke-virtual {v0}, Lcxd;->g()V

    goto/16 :goto_0

    .line 238
    :cond_5
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    if-eqz v0, :cond_6

    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    invoke-virtual {v0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 240
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    invoke-virtual {v1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v1

    invoke-virtual {v1}, Lretrofit/client/Response;->getStatus()I

    move-result v1

    invoke-virtual {v0, v1}, Lcxd;->a(I)V

    goto/16 :goto_0

    .line 243
    :cond_6
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->a:Ltv/periscope/android/ui/broadcast/av$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/av$a;->G()V

    goto/16 :goto_0

    .line 250
    :pswitch_2
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 251
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    iget-object v2, p1, Ltv/periscope/android/event/ApiEvent;->b:Ljava/lang/String;

    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/api/StartWatchingResponse;

    iget-object v0, v0, Ltv/periscope/android/api/StartWatchingResponse;->session:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcxd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 256
    :pswitch_3
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/av;->g:Lcxd;

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcxd;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 182
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
