.class public Ltv/periscope/android/ui/broadcast/o;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcxf;


# static fields
.field private static final a:Ltv/periscope/android/api/PsUser;


# instance fields
.field private final b:Ltv/periscope/android/library/c;

.field private final c:Ltv/periscope/android/api/PsUser;

.field private d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ltv/periscope/android/ui/broadcast/ag;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ltv/periscope/android/api/PsUser;

    invoke-direct {v0}, Ltv/periscope/android/api/PsUser;-><init>()V

    sput-object v0, Ltv/periscope/android/ui/broadcast/o;->a:Ltv/periscope/android/api/PsUser;

    .line 29
    sget-object v0, Ltv/periscope/android/ui/broadcast/o;->a:Ltv/periscope/android/api/PsUser;

    const-string/jumbo v1, ""

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    .line 30
    sget-object v0, Ltv/periscope/android/ui/broadcast/o;->a:Ltv/periscope/android/api/PsUser;

    const-string/jumbo v1, ""

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    .line 31
    sget-object v0, Ltv/periscope/android/ui/broadcast/o;->a:Ltv/periscope/android/api/PsUser;

    const-string/jumbo v1, ""

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public constructor <init>(Ltv/periscope/android/library/c;)V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Ltv/periscope/android/ui/broadcast/o;->a:Ltv/periscope/android/api/PsUser;

    invoke-direct {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/o;-><init>(Ltv/periscope/android/library/c;Ltv/periscope/android/api/PsUser;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Ltv/periscope/android/library/c;Ltv/periscope/android/api/PsUser;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/o;->b:Ltv/periscope/android/library/c;

    .line 50
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/o;->c:Ltv/periscope/android/api/PsUser;

    .line 51
    return-void
.end method

.method private a(Landroid/app/Activity;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Ltv/periscope/android/player/c;)Ltv/periscope/android/ui/broadcast/ag;
    .locals 21

    .prologue
    .line 88
    new-instance v1, Ltv/periscope/android/ui/broadcast/ag;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/o;->b:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/o;->b:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v4

    const-string/jumbo v5, "api.periscope.tv"

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/o;->b:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/o;->b:Ltv/periscope/android/library/c;

    .line 89
    invoke-interface {v2}, Ltv/periscope/android/library/c;->h()Lcyn;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Ltv/periscope/android/ui/broadcast/o;->c:Ltv/periscope/android/api/PsUser;

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/o;->b:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->w()Ldae;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/ui/broadcast/o;->b:Ltv/periscope/android/library/c;

    .line 90
    invoke-interface {v2}, Ltv/periscope/android/library/c;->v()Ldae;

    move-result-object v12

    new-instance v13, Ltv/periscope/android/chat/n;

    invoke-direct {v13}, Ltv/periscope/android/chat/n;-><init>()V

    sget-object v14, Lretrofit/RestAdapter$LogLevel;->BASIC:Lretrofit/RestAdapter$LogLevel;

    const/16 v16, 0x0

    const/16 v17, 0x1

    sget-object v18, Ltv/periscope/android/video/StreamMode;->a:Ltv/periscope/android/video/StreamMode;

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v2, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v15, p4

    invoke-direct/range {v1 .. v20}, Ltv/periscope/android/ui/broadcast/ag;-><init>(Landroid/app/Activity;Lde/greenrobot/event/c;Ltv/periscope/android/api/ApiManager;Ljava/lang/String;Lcyw;Lcyn;Ltv/periscope/android/api/PsUser;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Ldae;Ldae;Ltv/periscope/android/chat/a;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/android/player/c;Ltv/periscope/android/ui/broadcast/ar;ZLtv/periscope/android/video/StreamMode;ZLtv/periscope/android/chat/i$a;)V

    .line 88
    return-object v1
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->b:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->h()Lcyn;

    move-result-object v0

    invoke-interface {v0, p1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 96
    if-nez v0, :cond_0

    .line 98
    invoke-static {}, Ltv/periscope/model/p;->G()Ltv/periscope/model/p$a;

    move-result-object v0

    .line 99
    invoke-virtual {v0, p1}, Ltv/periscope/model/p$a;->a(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    const-string/jumbo v1, ""

    .line 100
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->f(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    const-string/jumbo v1, ""

    .line 101
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->i(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Ltv/periscope/model/p$a;->a()Ltv/periscope/model/p;

    move-result-object v0

    .line 103
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv/periscope/model/p;->b(Z)V

    .line 104
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/o;->b:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->h()Lcyn;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcyn;->a(Ljava/lang/String;Ltv/periscope/model/p;)V

    .line 106
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method public a(Landroid/app/Activity;Ljava/lang/String;Ltv/periscope/android/player/PlayMode;Ltv/periscope/android/player/b;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Lcxe;)V
    .locals 4

    .prologue
    .line 65
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    if-eqz v0, :cond_1

    .line 66
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/o;->f:Z

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ag;->I()V

    .line 69
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ag;->J()V

    .line 71
    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->d:Ljava/lang/ref/WeakReference;

    .line 72
    invoke-direct {p0, p2}, Ltv/periscope/android/ui/broadcast/o;->a(Ljava/lang/String;)V

    .line 74
    invoke-interface {p4}, Ltv/periscope/android/player/b;->a()Ltv/periscope/android/player/c;

    move-result-object v0

    invoke-direct {p0, p1, p5, p6, v0}, Ltv/periscope/android/ui/broadcast/o;->a(Landroid/app/Activity;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Ltv/periscope/android/player/c;)Ltv/periscope/android/ui/broadcast/ag;

    move-result-object v0

    .line 75
    invoke-virtual {v0, p2}, Ltv/periscope/android/ui/broadcast/ag;->a(Ljava/lang/String;)Ltv/periscope/android/ui/broadcast/ag;

    move-result-object v0

    .line 76
    invoke-virtual {v0, p3}, Ltv/periscope/android/ui/broadcast/ag;->c(Ltv/periscope/android/player/PlayMode;)Ltv/periscope/android/ui/broadcast/ag;

    move-result-object v0

    .line 77
    invoke-virtual {v0, p7}, Ltv/periscope/android/ui/broadcast/ag;->a(Lcxe;)Ltv/periscope/android/ui/broadcast/ag;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/o;->f:Z

    .line 80
    iget-boolean v0, p3, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v0, :cond_2

    .line 81
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    invoke-interface {p6}, Ltv/periscope/android/player/e;->l()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/ag;->d(J)Ltv/periscope/android/ui/broadcast/ag;

    .line 83
    :cond_2
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ag;->y()V

    .line 117
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ag;->z()V

    .line 123
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ag;->E()V

    .line 129
    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ag;->I()V

    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/o;->f:Z

    .line 136
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ag;->J()V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/o;->e:Ltv/periscope/android/ui/broadcast/ag;

    .line 143
    :cond_0
    return-void
.end method
