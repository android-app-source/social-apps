.class public Ltv/periscope/android/ui/broadcast/v;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/v$a;
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Ltv/periscope/android/chat/f;

.field private final d:Ltv/periscope/chatman/c;

.field private final e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lde/greenrobot/event/c;

.field private final g:Landroid/os/Handler;

.field private final h:Ltv/periscope/android/ui/broadcast/moderator/f;

.field private final i:Ltv/periscope/android/ui/broadcast/moderator/g;

.field private j:Ltv/periscope/android/ui/chat/m;

.field private k:Ltv/periscope/android/chat/g;

.field private l:Ltv/periscope/model/u;

.field private m:Ltv/periscope/android/chat/d;

.field private n:Ltv/periscope/android/ui/chat/o;

.field private o:Ltv/periscope/android/ui/broadcast/v$a;

.field private p:Z

.field private final q:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;Ltv/periscope/android/ui/broadcast/moderator/f;Ltv/periscope/android/ui/broadcast/moderator/g;ZZ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;",
            "Ltv/periscope/android/ui/broadcast/moderator/f;",
            "Ltv/periscope/android/ui/broadcast/moderator/g;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v2, Ltv/periscope/chatman/c;

    invoke-direct {v2}, Ltv/periscope/chatman/c;-><init>()V

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Ltv/periscope/android/network/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/chat/f;->a(Ljava/lang/String;)Ltv/periscope/android/chat/f;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Ltv/periscope/android/ui/broadcast/v;-><init>(Ljava/lang/ref/WeakReference;Ltv/periscope/chatman/c;Ltv/periscope/android/chat/f;Ltv/periscope/android/ui/broadcast/moderator/f;Ltv/periscope/android/ui/broadcast/moderator/g;ZZ)V

    .line 109
    return-void
.end method

.method constructor <init>(Ljava/lang/ref/WeakReference;Ltv/periscope/chatman/c;Ltv/periscope/android/chat/f;Ltv/periscope/android/ui/broadcast/moderator/f;Ltv/periscope/android/ui/broadcast/moderator/g;ZZ)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;",
            "Ltv/periscope/chatman/c;",
            "Ltv/periscope/android/chat/f;",
            "Ltv/periscope/android/ui/broadcast/moderator/f;",
            "Ltv/periscope/android/ui/broadcast/moderator/g;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->g:Landroid/os/Handler;

    .line 94
    new-instance v0, Ltv/periscope/android/ui/broadcast/v$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/v$1;-><init>(Ltv/periscope/android/ui/broadcast/v;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->q:Ljava/lang/Runnable;

    .line 115
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/v;->e:Ljava/lang/ref/WeakReference;

    .line 116
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/v;->d:Ltv/periscope/chatman/c;

    .line 117
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/v;->c:Ltv/periscope/android/chat/f;

    .line 118
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/v;->h:Ltv/periscope/android/ui/broadcast/moderator/f;

    .line 119
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/v;->i:Ltv/periscope/android/ui/broadcast/moderator/g;

    .line 120
    iput-boolean p6, p0, Ltv/periscope/android/ui/broadcast/v;->a:Z

    .line 121
    iput-boolean p7, p0, Ltv/periscope/android/ui/broadcast/v;->b:Z

    .line 122
    new-instance v0, Lde/greenrobot/event/c;

    invoke-direct {v0}, Lde/greenrobot/event/c;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->f:Lde/greenrobot/event/c;

    .line 123
    new-instance v0, Ltv/periscope/android/ui/chat/al;

    invoke-direct {v0}, Ltv/periscope/android/ui/chat/al;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    .line 124
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/v;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->e:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private a(Ltv/periscope/android/chat/ChatRoomEvent;)V
    .locals 2

    .prologue
    .line 227
    sget-object v0, Ltv/periscope/android/ui/broadcast/v$2;->a:[I

    invoke-virtual {p1}, Ltv/periscope/android/chat/ChatRoomEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 229
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/m;->b()V

    goto :goto_0

    .line 233
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/m;->c()V

    goto :goto_0

    .line 237
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/m;->d()V

    goto :goto_0

    .line 241
    :pswitch_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/m;->e()V

    .line 242
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->o:Ltv/periscope/android/ui/broadcast/v$a;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->o:Ltv/periscope/android/ui/broadcast/v$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/v$a;->f()V

    goto :goto_0

    .line 249
    :pswitch_4
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->o:Ltv/periscope/android/ui/broadcast/v$a;

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->o:Ltv/periscope/android/ui/broadcast/v$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/v$a;->d()V

    goto :goto_0

    .line 256
    :pswitch_5
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->o:Ltv/periscope/android/ui/broadcast/v$a;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->o:Ltv/periscope/android/ui/broadcast/v$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/v$a;->e()V

    goto :goto_0

    .line 263
    :pswitch_6
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->o:Ltv/periscope/android/ui/broadcast/v$a;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->o:Ltv/periscope/android/ui/broadcast/v$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/v$a;->c()V

    goto :goto_0

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private a(Ltv/periscope/model/chat/MessageType$VoteType;)V
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->h:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/f;->a(Ltv/periscope/model/chat/MessageType$VoteType;)V

    .line 398
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->i:Ltv/periscope/android/ui/broadcast/moderator/g;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/g;->a(Ltv/periscope/model/chat/MessageType$VoteType;)V

    .line 399
    return-void
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/v;)Ltv/periscope/android/ui/broadcast/moderator/f;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->h:Ltv/periscope/android/ui/broadcast/moderator/f;

    return-object v0
.end method

.method static synthetic c(Ltv/periscope/android/ui/broadcast/v;)Ltv/periscope/android/ui/broadcast/moderator/g;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->i:Ltv/periscope/android/ui/broadcast/moderator/g;

    return-object v0
.end method

.method private s()Ltv/periscope/android/ui/chat/m;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    invoke-interface {v0}, Ltv/periscope/android/chat/d;->d()V

    .line 172
    :cond_0
    return-void
.end method

.method a(J)V
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/chat/d;->a(J)V

    .line 299
    :cond_0
    return-void
.end method

.method public a(Lcyw;Lcyn;ZLtv/periscope/android/ui/chat/o$c;Ltv/periscope/android/ui/chat/o$a;Ltv/periscope/android/ui/chat/o$b;Ltv/periscope/android/ui/chat/b;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 149
    new-instance v0, Ltv/periscope/android/ui/chat/o;

    invoke-direct {v0, p1, p2, p3, p8}, Ltv/periscope/android/ui/chat/o;-><init>(Lcyw;Lcyn;ZLjava/lang/String;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->n:Ltv/periscope/android/ui/chat/o;

    .line 150
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->n:Ltv/periscope/android/ui/chat/o;

    invoke-virtual {v0, p4}, Ltv/periscope/android/ui/chat/o;->a(Ltv/periscope/android/ui/chat/o$c;)V

    .line 151
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->n:Ltv/periscope/android/ui/chat/o;

    invoke-virtual {v0, p5}, Ltv/periscope/android/ui/chat/o;->a(Ltv/periscope/android/ui/chat/o$a;)V

    .line 152
    if-eqz p6, :cond_1

    .line 153
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->n:Ltv/periscope/android/ui/chat/o;

    invoke-virtual {v0, p6}, Ltv/periscope/android/ui/chat/o;->a(Ltv/periscope/android/ui/chat/o$b;)V

    .line 158
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->f:Lde/greenrobot/event/c;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 159
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->f:Lde/greenrobot/event/c;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->h:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 160
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->f:Lde/greenrobot/event/c;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->n:Ltv/periscope/android/ui/chat/o;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 162
    if-eqz p7, :cond_0

    .line 163
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->n:Ltv/periscope/android/ui/chat/o;

    invoke-virtual {p7, v0}, Ltv/periscope/android/ui/chat/b;->a(Ltv/periscope/android/ui/chat/o;)V

    .line 164
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->h:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-virtual {p7, v0}, Ltv/periscope/android/ui/chat/b;->a(Ltv/periscope/android/ui/broadcast/moderator/f;)V

    .line 166
    :cond_0
    return-void

    .line 155
    :cond_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->n:Ltv/periscope/android/ui/chat/o;

    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/v;->s()Ltv/periscope/android/ui/chat/m;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/chat/o$b;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/chat/o;->a(Ltv/periscope/android/ui/chat/o$b;)V

    goto :goto_0
.end method

.method public a(Lcyw;Ltv/periscope/android/player/PlayMode;Ltv/periscope/android/player/e;Ltv/periscope/android/chat/h;Ltv/periscope/android/chat/i$a;Z)V
    .locals 8

    .prologue
    .line 420
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    invoke-interface {v0}, Ltv/periscope/android/chat/d;->a()V

    .line 423
    :cond_0
    sget-object v0, Ltv/periscope/android/ui/broadcast/v$2;->b:[I

    invoke-virtual {p2}, Ltv/periscope/android/player/PlayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 433
    new-instance v0, Ltv/periscope/android/chat/o;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->f:Lde/greenrobot/event/c;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/v;->c:Ltv/periscope/android/chat/f;

    iget-boolean v7, p0, Ltv/periscope/android/ui/broadcast/v;->a:Z

    move-object v2, p1

    move-object v4, p3

    move v5, p6

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Ltv/periscope/android/chat/o;-><init>(Lde/greenrobot/event/c;Lcyw;Ltv/periscope/android/chat/f;Ltv/periscope/android/player/e;ZLtv/periscope/android/chat/i$a;Z)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    .line 435
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    invoke-interface {v0}, Ltv/periscope/android/chat/d;->b()V

    .line 439
    :goto_0
    return-void

    .line 425
    :pswitch_0
    new-instance v0, Ltv/periscope/android/chat/l;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->f:Lde/greenrobot/event/c;

    iget-boolean v6, p0, Ltv/periscope/android/ui/broadcast/v;->a:Z

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/chat/l;-><init>(Lde/greenrobot/event/c;Lcyw;Ltv/periscope/android/player/e;Ltv/periscope/android/chat/h;Ltv/periscope/android/chat/i$a;Z)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    .line 427
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    invoke-interface {v0}, Ltv/periscope/android/chat/d;->b()V

    goto :goto_0

    .line 423
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 329
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/chat/m;->a(Ljava/util/ArrayList;)V

    .line 330
    return-void
.end method

.method a(Ltv/periscope/android/ui/broadcast/v$a;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/v;->o:Ltv/periscope/android/ui/broadcast/v$a;

    .line 141
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/m;)V
    .locals 2

    .prologue
    .line 135
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    .line 136
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->c:Ltv/periscope/android/chat/f;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/m;->a(Ltv/periscope/android/chat/f;)V

    .line 137
    return-void
.end method

.method public a(Ltv/periscope/model/StreamType;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 274
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/v;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "Already joined on current channel, closing."

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->c:Ltv/periscope/android/chat/f;

    invoke-virtual {v0}, Ltv/periscope/android/chat/f;->a()V

    .line 279
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    if-eqz v0, :cond_1

    .line 280
    new-instance v0, Ltv/periscope/android/chat/g;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    invoke-virtual {v1}, Ltv/periscope/model/u;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ltv/periscope/android/chat/g;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->k:Ltv/periscope/android/chat/g;

    .line 281
    const-string/jumbo v0, "CM"

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->k:Ltv/periscope/android/chat/g;

    invoke-virtual {v1}, Ltv/periscope/android/chat/g;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->k:Ltv/periscope/android/chat/g;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/m;->a(Ltv/periscope/android/chat/g;)V

    .line 284
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0, p2}, Ltv/periscope/android/ui/chat/m;->a(Ltv/periscope/android/player/PlayMode;)V

    .line 285
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0, p4}, Ltv/periscope/android/ui/chat/m;->a(Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->c:Ltv/periscope/android/chat/f;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->f:Lde/greenrobot/event/c;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/v;->d:Ltv/periscope/chatman/c;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v6}, Ltv/periscope/android/chat/f;->a(Lde/greenrobot/event/c;Ltv/periscope/model/u;Ltv/periscope/model/StreamType;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/chatman/c;)V

    .line 289
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->k:Ltv/periscope/android/chat/g;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    invoke-virtual {v2}, Ltv/periscope/model/u;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    invoke-virtual {v3}, Ltv/periscope/model/u;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, p4, v3}, Ltv/periscope/android/chat/d;->a(Ltv/periscope/android/chat/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    :cond_1
    return-void
.end method

.method public a(Ltv/periscope/model/StreamType;Ltv/periscope/model/u;)V
    .locals 1

    .prologue
    .line 185
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    .line 186
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/chat/m;->a(Ltv/periscope/model/StreamType;Ltv/periscope/model/u;)V

    .line 189
    :cond_0
    return-void
.end method

.method a(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/chat/m;->a(Ltv/periscope/model/chat/Message;)V

    .line 322
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 446
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/v;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/m;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->c:Ltv/periscope/android/chat/f;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/chat/f;->a(Ltv/periscope/model/chat/Message;Ljava/lang/String;)V

    .line 449
    :cond_0
    return-void
.end method

.method a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$ReportType;)V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/chat/m;->a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$ReportType;)V

    .line 326
    return-void
.end method

.method a(Z)V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    invoke-interface {v0, p1}, Ltv/periscope/android/chat/d;->a(Z)V

    .line 224
    :cond_0
    return-void
.end method

.method a(Ltv/periscope/model/chat/MessageType;)Z
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/chat/m;->a(Ltv/periscope/model/chat/MessageType;)Z

    move-result v0

    return v0
.end method

.method a(Ltv/periscope/model/u;)Z
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    invoke-interface {v0}, Ltv/periscope/android/chat/d;->c()V

    .line 178
    :cond_0
    return-void
.end method

.method b(J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 340
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    invoke-interface {v0}, Ltv/periscope/android/chat/d;->e()V

    .line 344
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/v;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    if-eqz v0, :cond_1

    .line 347
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/v;->p:Z

    .line 348
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->c:Ltv/periscope/android/chat/f;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/v;->k:Ltv/periscope/android/chat/g;

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    invoke-virtual {v0}, Ltv/periscope/model/u;->e()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v6, ""

    invoke-virtual/range {v1 .. v6}, Ltv/periscope/android/chat/f;->a(Ltv/periscope/android/chat/g;Ljava/lang/String;JLjava/lang/String;)V

    .line 349
    cmp-long v0, p1, v4

    if-eqz v0, :cond_1

    .line 351
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/v;->b()V

    .line 354
    :cond_1
    return-void
.end method

.method public b(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 442
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/model/chat/Message;Ljava/lang/String;)V

    .line 443
    return-void
.end method

.method c()V
    .locals 0

    .prologue
    .line 181
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/v;->b()V

    .line 182
    return-void
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->k:Ltv/periscope/android/chat/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->k:Ltv/periscope/android/chat/g;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    invoke-virtual {v1}, Ltv/periscope/model/u;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/chat/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 200
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    invoke-interface {v0}, Ltv/periscope/android/chat/d;->a()V

    .line 202
    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    .line 204
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->n:Ltv/periscope/android/ui/chat/o;

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->n:Ltv/periscope/android/ui/chat/o;

    invoke-virtual {v0}, Ltv/periscope/android/ui/chat/o;->a()V

    .line 206
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->f:Lde/greenrobot/event/c;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->n:Ltv/periscope/android/ui/chat/o;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 207
    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/v;->n:Ltv/periscope/android/ui/chat/o;

    .line 209
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->c:Ltv/periscope/android/chat/f;

    invoke-virtual {v0}, Ltv/periscope/android/chat/f;->a()V

    .line 210
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->f:Lde/greenrobot/event/c;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->h:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 211
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->f:Lde/greenrobot/event/c;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 212
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 215
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/v;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->c:Ltv/periscope/android/chat/f;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->k:Ltv/periscope/android/chat/g;

    invoke-virtual {v0, v1}, Ltv/periscope/android/chat/f;->a(Ltv/periscope/android/chat/g;)V

    .line 218
    :cond_0
    return-void
.end method

.method g()V
    .locals 1

    .prologue
    .line 302
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/v;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->l:Ltv/periscope/model/u;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->c:Ltv/periscope/android/chat/f;

    invoke-virtual {v0}, Ltv/periscope/android/chat/f;->a()V

    .line 306
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/v;->b()V

    .line 308
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->k:Ltv/periscope/android/chat/g;

    .line 311
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/m;->j()V

    .line 312
    return-void
.end method

.method h()V
    .locals 1

    .prologue
    .line 315
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/v;->b:Z

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/m;->f()V

    .line 318
    :cond_0
    return-void
.end method

.method i()V
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/m;->a()V

    .line 334
    return-void
.end method

.method j()V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->j:Ltv/periscope/android/ui/chat/m;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/m;->g()V

    .line 337
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 359
    sget-object v0, Ltv/periscope/model/chat/MessageType$VoteType;->c:Ltv/periscope/model/chat/MessageType$VoteType;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/model/chat/MessageType$VoteType;)V

    .line 360
    return-void
.end method

.method public l()V
    .locals 1

    .prologue
    .line 365
    sget-object v0, Ltv/periscope/model/chat/MessageType$VoteType;->d:Ltv/periscope/model/chat/MessageType$VoteType;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/model/chat/MessageType$VoteType;)V

    .line 366
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 371
    sget-object v0, Ltv/periscope/model/chat/MessageType$VoteType;->b:Ltv/periscope/model/chat/MessageType$VoteType;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/model/chat/MessageType$VoteType;)V

    .line 372
    return-void
.end method

.method public n()V
    .locals 4

    .prologue
    .line 377
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->i:Ltv/periscope/android/ui/broadcast/moderator/g;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/g;->c()V

    .line 378
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->h:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/f;->b()V

    .line 379
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->g:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->q:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 380
    return-void
.end method

.method public o()V
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->i:Ltv/periscope/android/ui/broadcast/moderator/g;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/g;->d()V

    .line 386
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->h:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/f;->a()V

    .line 387
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/chat/ChatRoomEvent;)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 129
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/v;->a(Ltv/periscope/android/chat/ChatRoomEvent;)V

    .line 132
    :cond_0
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/chat/EventHistory;)V
    .locals 4

    .prologue
    .line 403
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/v;->p:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    iget-wide v2, p1, Ltv/periscope/android/chat/EventHistory;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 404
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/v;->p:Z

    .line 406
    :cond_0
    return-void
.end method

.method public p()V
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->g:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/v;->q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 393
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->i:Ltv/periscope/android/ui/broadcast/moderator/g;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/g;->a()V

    .line 394
    return-void
.end method

.method q()Z
    .locals 1

    .prologue
    .line 409
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/v;->p:Z

    return v0
.end method

.method public r()Ltv/periscope/android/api/ChatStats;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->m:Ltv/periscope/android/chat/d;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/v;->c:Ltv/periscope/android/chat/f;

    invoke-virtual {v0}, Ltv/periscope/android/chat/f;->b()Ltv/periscope/android/api/ChatStats;

    move-result-object v0

    .line 455
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
