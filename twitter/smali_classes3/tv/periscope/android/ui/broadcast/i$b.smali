.class public Ltv/periscope/android/ui/broadcast/i$b;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$c;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Lcyw;

.field private final d:Ldae;

.field private final e:Landroid/widget/ImageView;

.field private final f:Landroid/widget/TextView;

.field private final g:Ltv/periscope/android/view/UsernameBadgeView;

.field private h:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;Lcyw;Ldae;)V
    .locals 1

    .prologue
    .line 195
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 196
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/i$b;->b:Landroid/view/View;

    .line 197
    sget v0, Ltv/periscope/android/library/f$g;->broadcaster:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$b;->f:Landroid/widget/TextView;

    .line 198
    sget v0, Ltv/periscope/android/library/f$g;->header_profile_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$b;->e:Landroid/widget/ImageView;

    .line 199
    sget v0, Ltv/periscope/android/library/f$g;->broadcaster_description:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/UsernameBadgeView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$b;->g:Ltv/periscope/android/view/UsernameBadgeView;

    .line 200
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/i$b;->c:Lcyw;

    .line 201
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/i$b;->d:Ldae;

    .line 202
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;Lcyw;Ldae;)Ltv/periscope/android/ui/broadcast/i$b;
    .locals 3

    .prologue
    .line 180
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcaster_container:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 182
    new-instance v1, Ltv/periscope/android/ui/broadcast/i$b;

    invoke-direct {v1, v0, p2, p3, p4}, Ltv/periscope/android/ui/broadcast/i$b;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;Lcyw;Ldae;)V

    return-object v1
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$c;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 207
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$b;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 208
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 209
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$c;->b()Ltv/periscope/model/p;

    move-result-object v5

    .line 211
    invoke-virtual {v5}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/i$b;->h:Ljava/lang/String;

    .line 212
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$b;->f:Landroid/widget/TextView;

    invoke-virtual {v5}, Ltv/periscope/model/p;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$b;->c:Lcyw;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/i$b;->h:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcyw;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 215
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$b;->b:Landroid/view/View;

    sget v3, Ltv/periscope/android/library/f$d;->ps__app_background_secondary:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 218
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$b;->c:Lcyw;

    invoke-virtual {v5}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 219
    if-eqz v0, :cond_1

    iget-object v2, v0, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    invoke-static {v2}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 220
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$b;->g:Ltv/periscope/android/view/UsernameBadgeView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__broadcaster:I

    invoke-virtual {v0, v2}, Ltv/periscope/android/view/UsernameBadgeView;->setText(I)V

    .line 221
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$b;->g:Ltv/periscope/android/view/UsernameBadgeView;

    invoke-virtual {v0, v4, v4}, Ltv/periscope/android/view/UsernameBadgeView;->a(ZZ)V

    .line 228
    :goto_0
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$b;->d:Ldae;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/i$b;->e:Landroid/widget/ImageView;

    invoke-virtual {v5}, Ltv/periscope/model/p;->u()Ljava/lang/String;

    move-result-object v4

    .line 229
    invoke-virtual {v5}, Ltv/periscope/model/p;->t()Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, 0x0

    .line 228
    invoke-static/range {v1 .. v7}, Ltv/periscope/android/util/b;->a(Landroid/content/Context;Ldae;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;J)V

    .line 230
    return-void

    .line 223
    :cond_2
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$b;->g:Ltv/periscope/android/view/UsernameBadgeView;

    iget-object v3, v0, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ltv/periscope/android/view/UsernameBadgeView;->setUsername(Ljava/lang/String;)V

    .line 224
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$b;->g:Ltv/periscope/android/view/UsernameBadgeView;

    iget-boolean v3, v0, Ltv/periscope/android/api/PsUser;->isVerified:Z

    iget-boolean v4, v0, Ltv/periscope/android/api/PsUser;->isBluebirdUser:Z

    invoke-virtual {v2, v3, v4}, Ltv/periscope/android/view/UsernameBadgeView;->a(ZZ)V

    .line 225
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$b;->g:Ltv/periscope/android/view/UsernameBadgeView;

    invoke-virtual {v0}, Ltv/periscope/android/api/PsUser;->getBadgeStatus()Ltv/periscope/android/api/PsUser$VipBadge;

    move-result-object v0

    invoke-virtual {v2, v0}, Ltv/periscope/android/view/UsernameBadgeView;->setVipStatus(Ltv/periscope/android/api/PsUser$VipBadge;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 175
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$c;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/i$b;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$c;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$b;->a:Ltv/periscope/android/ui/broadcast/h;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$b;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/h;->f(Ljava/lang/String;)V

    .line 235
    return-void
.end method
