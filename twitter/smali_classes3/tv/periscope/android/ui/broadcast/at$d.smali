.class Ltv/periscope/android/ui/broadcast/at$d;
.super Ltv/periscope/android/ui/broadcast/b$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/at;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic c:Ltv/periscope/android/ui/broadcast/at;

.field private final d:Ltv/periscope/android/view/c;

.field private final e:Ltv/periscope/android/ui/user/g;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/at;Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;)V
    .locals 1

    .prologue
    .line 187
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/at$d;->c:Ltv/periscope/android/ui/broadcast/at;

    .line 188
    invoke-direct {p0, p2, p3, p4, p5}, Ltv/periscope/android/ui/broadcast/b$b;-><init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;)V

    .line 189
    iput-object p6, p0, Ltv/periscope/android/ui/broadcast/at$d;->e:Ltv/periscope/android/ui/user/g;

    .line 190
    new-instance v0, Ltv/periscope/android/ui/broadcast/at$d$1;

    invoke-direct {v0, p0, p1}, Ltv/periscope/android/ui/broadcast/at$d$1;-><init>(Ltv/periscope/android/ui/broadcast/at$d;Ltv/periscope/android/ui/broadcast/at;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/at$d;->d:Ltv/periscope/android/view/c;

    .line 197
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    sget v0, Ltv/periscope/android/library/f$l;->ps__action_sheet_report_spam:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 201
    sget v0, Ltv/periscope/android/library/f$d;->ps__red:I

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x1

    return v0
.end method

.method public f()Ltv/periscope/android/view/c;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/at$d;->d:Ltv/periscope/android/view/c;

    return-object v0
.end method

.method protected g()V
    .locals 4

    .prologue
    .line 227
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/at$d;->e:Ltv/periscope/android/ui/user/g;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/at$d;->b:Ltv/periscope/model/chat/Message;

    sget-object v2, Ltv/periscope/model/chat/MessageType$ReportType;->c:Ltv/periscope/model/chat/MessageType$ReportType;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/at$d;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Ltv/periscope/android/ui/user/g;->a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$ReportType;Ljava/lang/String;)V

    .line 228
    return-void
.end method
