.class Ltv/periscope/android/ui/broadcast/b$c;
.super Ltv/periscope/android/ui/broadcast/b$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "c"
.end annotation


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ltv/periscope/android/ui/chat/ah;


# direct methods
.method constructor <init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3, p4}, Ltv/periscope/android/ui/broadcast/b$b;-><init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;)V

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/b$c;->c:Ljava/lang/String;

    .line 86
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 101
    sget v0, Ltv/periscope/android/library/f$f;->ps__ic_as_reply:I

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    sget v0, Ltv/periscope/android/library/f$l;->ps__action_sheet_chat_reply:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method a(Ltv/periscope/android/ui/chat/ah;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/b$c;->d:Ltv/periscope/android/ui/chat/ah;

    .line 90
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 106
    sget v0, Ltv/periscope/android/library/f$d;->ps__blue:I

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method

.method public f()Ltv/periscope/android/view/c;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Ltv/periscope/android/view/c;->c:Ltv/periscope/android/view/c;

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/b$c;->d:Ltv/periscope/android/ui/chat/ah;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/b$c;->d:Ltv/periscope/android/ui/chat/ah;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/b$c;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/chat/ah;->b(Ljava/lang/String;)V

    .line 97
    :cond_0
    return-void
.end method
