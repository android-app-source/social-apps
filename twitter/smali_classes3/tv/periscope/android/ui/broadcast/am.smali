.class Ltv/periscope/android/ui/broadcast/am;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/am$a;,
        Ltv/periscope/android/ui/broadcast/am$b;,
        Ltv/periscope/android/ui/broadcast/am$d;,
        Ltv/periscope/android/ui/broadcast/am$c;
    }
.end annotation


# static fields
.field private static final d:J


# instance fields
.field a:F

.field b:F

.field c:F

.field private final e:Ldae;

.field private final f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

.field private final g:Ltv/periscope/android/ui/broadcast/am$d;

.field private final h:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Ltv/periscope/android/api/ThumbnailPlaylistItem;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/lang/String;

.field private k:Ltv/periscope/android/player/d;

.field private l:Ltv/periscope/android/ui/broadcast/am$c;

.field private m:Z

.field private n:Z

.field private o:J

.field private p:J

.field private q:I

.field private r:J

.field private s:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 54
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ltv/periscope/android/ui/broadcast/am;->d:J

    return-void
.end method

.method constructor <init>(Ltv/periscope/android/ui/broadcast/ReplayScrubView;Ldae;)V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->h:Ljava/util/TreeMap;

    .line 65
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->i:Ljava/util/TreeSet;

    .line 79
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    .line 80
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->setZoomZonePercentage(F)V

    .line 81
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/am;->e:Ldae;

    .line 82
    new-instance v0, Ltv/periscope/android/ui/broadcast/am$d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ltv/periscope/android/ui/broadcast/am$d;-><init>(Ltv/periscope/android/ui/broadcast/am;Ltv/periscope/android/ui/broadcast/am$1;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->g:Ltv/periscope/android/ui/broadcast/am$d;

    .line 83
    return-void
.end method

.method private a(F)I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 97
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 98
    const/4 v0, 0x0

    .line 104
    :goto_0
    return v0

    .line 99
    :cond_0
    const/high16 v0, 0x40400000    # 3.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 100
    sget v0, Ltv/periscope/android/library/f$l;->ps__replay_scrub_zoom_half:I

    goto :goto_0

    .line 101
    :cond_1
    const/high16 v0, 0x40a00000    # 5.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    .line 102
    sget v0, Ltv/periscope/android/library/f$l;->ps__replay_scrub_zoom_quarter:I

    goto :goto_0

    .line 104
    :cond_2
    sget v0, Ltv/periscope/android/library/f$l;->ps__replay_scrub_zoom_fine:I

    goto :goto_0
.end method

.method private a(JLjava/util/NavigableSet;)Ljava/lang/Long;
    .locals 9
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/NavigableSet",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/Long;"
        }
    .end annotation

    .prologue
    .line 146
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 147
    invoke-interface {p3, v2}, Ljava/util/NavigableSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v1, v2

    .line 167
    :cond_0
    :goto_0
    return-object v1

    .line 150
    :cond_1
    invoke-interface {p3, v2}, Ljava/util/NavigableSet;->lower(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 151
    invoke-interface {p3, v2}, Ljava/util/NavigableSet;->higher(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 153
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 155
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    .line 156
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 157
    add-long/2addr v4, v2

    long-to-float v4, v4

    .line 161
    long-to-float v2, v2

    div-float/2addr v2, v4

    const v3, 0x3f4ccccd    # 0.8f

    cmpg-float v2, v2, v3

    if-lez v2, :cond_0

    move-object v1, v0

    goto :goto_0

    .line 162
    :cond_2
    if-nez v0, :cond_3

    if-eqz v1, :cond_5

    .line 163
    :cond_3
    if-eqz v0, :cond_4

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1

    .line 165
    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private a(J)Ltv/periscope/android/api/ThumbnailPlaylistItem;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->h:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->navigableKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/ui/broadcast/am;->a(JLjava/util/NavigableSet;)Ljava/lang/Long;

    move-result-object v0

    .line 129
    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/am;->a(Ljava/lang/Long;)Ltv/periscope/android/api/ThumbnailPlaylistItem;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Long;)Ltv/periscope/android/api/ThumbnailPlaylistItem;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 173
    if-eqz p1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->h:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->h:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/ThumbnailPlaylistItem;

    .line 183
    :goto_0
    return-object v0

    .line 177
    :cond_0
    new-instance v0, Ltv/periscope/android/api/ThumbnailPlaylistItem;

    invoke-direct {v0}, Ltv/periscope/android/api/ThumbnailPlaylistItem;-><init>()V

    .line 178
    const/4 v1, -0x1

    iput v1, v0, Ltv/periscope/android/api/ThumbnailPlaylistItem;->chunk:I

    .line 179
    const/4 v1, 0x0

    iput-object v1, v0, Ltv/periscope/android/api/ThumbnailPlaylistItem;->url:Ljava/lang/String;

    .line 180
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Ltv/periscope/android/api/ThumbnailPlaylistItem;->timeInSecs:D

    .line 181
    const/4 v1, 0x0

    iput v1, v0, Ltv/periscope/android/api/ThumbnailPlaylistItem;->rotation:I

    goto :goto_0
.end method

.method private a(JJ)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 296
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->setDuration(J)V

    .line 297
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->setZoomZonePercentage(F)V

    .line 298
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-direct {p0, v2}, Ltv/periscope/android/ui/broadcast/am;->a(F)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(FI)V

    .line 299
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0, p3, p4}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->setInitialTime(J)V

    .line 300
    invoke-direct {p0, p3, p4}, Ltv/periscope/android/ui/broadcast/am;->a(J)Ltv/periscope/android/api/ThumbnailPlaylistItem;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/am;->a(Ltv/periscope/android/api/ThumbnailPlaylistItem;)V

    .line 301
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/util/List;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/ThumbnailPlaylistItem;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 253
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    .line 254
    const/4 v0, 0x1

    div-int v1, v2, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 255
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 256
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/ThumbnailPlaylistItem;

    .line 257
    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/am;->e:Ldae;

    iget-object v5, v0, Ltv/periscope/android/api/ThumbnailPlaylistItem;->url:Ljava/lang/String;

    new-instance v6, Ltv/periscope/android/ui/broadcast/am$b;

    invoke-direct {v6, p0, v0}, Ltv/periscope/android/ui/broadcast/am$b;-><init>(Ltv/periscope/android/ui/broadcast/am;Ltv/periscope/android/api/ThumbnailPlaylistItem;)V

    invoke-interface {v4, p1, v5, v6}, Ldae;->a(Landroid/content/Context;Ljava/lang/String;Ldae$b;)V

    .line 255
    add-int v0, v1, v3

    move v1, v0

    goto :goto_0

    .line 259
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 335
    return-void
.end method

.method private a(Ltv/periscope/android/api/ThumbnailPlaylistItem;)V
    .locals 4
    .param p1    # Ltv/periscope/android/api/ThumbnailPlaylistItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 133
    iget-object v0, p1, Ltv/periscope/android/api/ThumbnailPlaylistItem;->url:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 134
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 135
    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/am;->j:Ljava/lang/String;

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    iget-object v0, p1, Ltv/periscope/android/api/ThumbnailPlaylistItem;->url:Ljava/lang/String;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/am;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->e:Ldae;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p1, Ltv/periscope/android/api/ThumbnailPlaylistItem;->url:Ljava/lang/String;

    new-instance v3, Ltv/periscope/android/ui/broadcast/am$a;

    invoke-direct {v3, p0, p1}, Ltv/periscope/android/ui/broadcast/am$a;-><init>(Ltv/periscope/android/ui/broadcast/am;Ltv/periscope/android/api/ThumbnailPlaylistItem;)V

    invoke-interface {v0, v1, v2, v3}, Ldae;->a(Landroid/content/Context;Ljava/lang/String;Ldae$b;)V

    .line 138
    iget-object v0, p1, Ltv/periscope/android/api/ThumbnailPlaylistItem;->url:Ljava/lang/String;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->j:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(FF)Z
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {p0, p2, v0}, Ltv/periscope/android/ui/broadcast/am;->b(FI)F

    move-result v0

    .line 87
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getZoom()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/am;->a(F)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(FI)V

    .line 89
    const/4 v0, 0x1

    .line 91
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/am;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/am;->n:Z

    return v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/am;FF)Z
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/am;->a(FF)Z

    move-result v0

    return v0
.end method

.method private b(FI)F
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getZoomZoneStart()F

    move-result v0

    .line 200
    iget v1, p0, Ltv/periscope/android/ui/broadcast/am;->a:F

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_0

    .line 201
    invoke-virtual {p0, v0, p2}, Ltv/periscope/android/ui/broadcast/am;->a(FI)V

    .line 203
    :cond_0
    iget v0, p0, Ltv/periscope/android/ui/broadcast/am;->b:F

    iget v1, p0, Ltv/periscope/android/ui/broadcast/am;->c:F

    invoke-virtual {p0, p1, v0, v1}, Ltv/periscope/android/ui/broadcast/am;->a(FFF)F

    move-result v0

    return v0
.end method

.method private b(FF)V
    .locals 7

    .prologue
    .line 110
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getDuration()J

    move-result-wide v0

    .line 111
    sub-float v2, p1, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    .line 112
    invoke-virtual {v3}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getBarWidth()I

    move-result v3

    .line 111
    invoke-static {v2, v3, v0, v1}, Ldag;->a(FIJ)J

    move-result-wide v2

    .line 116
    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v4}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getSeekTo()J

    move-result-wide v4

    .line 117
    cmpl-float v6, p1, p2

    if-lez v6, :cond_0

    .line 118
    add-long/2addr v2, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 122
    :goto_0
    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/am;->a(J)Ltv/periscope/android/api/ThumbnailPlaylistItem;

    move-result-object v2

    invoke-direct {p0, v2}, Ltv/periscope/android/ui/broadcast/am;->a(Ltv/periscope/android/api/ThumbnailPlaylistItem;)V

    .line 123
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v2, v0, v1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(J)V

    .line 124
    return-void

    .line 120
    :cond_0
    sub-long v0, v4, v2

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/am;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/am;->d()V

    return-void
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/am;FF)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/am;->b(FF)V

    return-void
.end method

.method static synthetic c(Ltv/periscope/android/ui/broadcast/am;)Ljava/util/TreeSet;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->i:Ljava/util/TreeSet;

    return-object v0
.end method

.method static synthetic d(Ltv/periscope/android/ui/broadcast/am;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->j:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 6

    .prologue
    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/am;->n:Z

    .line 188
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getSeekTo()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/am;->p:J

    .line 189
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/am;->p:J

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->setEndTime(J)V

    .line 190
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->l:Ltv/periscope/android/ui/broadcast/am$c;

    if-eqz v0, :cond_0

    .line 191
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/am;->p:J

    sget-wide v4, Ltv/periscope/android/ui/broadcast/am;->d:J

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 192
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/am;->l:Ltv/periscope/android/ui/broadcast/am$c;

    invoke-interface {v2, v0, v1}, Ltv/periscope/android/ui/broadcast/am$c;->a(J)V

    .line 194
    :cond_0
    return-void
.end method

.method static synthetic e(Ltv/periscope/android/ui/broadcast/am;)Ltv/periscope/android/ui/broadcast/ReplayScrubView;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    return-object v0
.end method


# virtual methods
.method a(FFF)F
    .locals 4

    .prologue
    const/high16 v1, 0x40a00000    # 5.0f

    const/high16 v0, 0x3f800000    # 1.0f

    .line 219
    cmpg-float v2, p1, p2

    if-gtz v2, :cond_0

    .line 230
    :goto_0
    return v0

    .line 221
    :cond_0
    cmpl-float v2, p1, p3

    if-ltz v2, :cond_1

    move v0, v1

    .line 222
    goto :goto_0

    .line 224
    :cond_1
    sub-float v2, p3, p2

    .line 225
    const/high16 v3, 0x40800000    # 4.0f

    div-float v2, v3, v2

    .line 226
    sub-float v3, p1, p2

    .line 227
    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0
.end method

.method a(FI)V
    .locals 3

    .prologue
    .line 208
    iput p1, p0, Ltv/periscope/android/ui/broadcast/am;->a:F

    .line 210
    int-to-float v0, p2

    const v1, 0x3dcccccd    # 0.1f

    mul-float/2addr v0, v1

    .line 211
    int-to-float v1, p2

    const v2, 0x3d4ccccd    # 0.05f

    mul-float/2addr v1, v2

    .line 212
    iget v2, p0, Ltv/periscope/android/ui/broadcast/am;->a:F

    add-float/2addr v0, v2

    iput v0, p0, Ltv/periscope/android/ui/broadcast/am;->b:F

    .line 213
    int-to-float v0, p2

    sub-float/2addr v0, v1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/am;->c:F

    .line 214
    return-void
.end method

.method a(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 270
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->k:Ltv/periscope/android/player/d;

    if-nez v0, :cond_0

    .line 271
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "You must call setPlayer before startScrubbing"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 273
    :cond_0
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/am;->m:Z

    if-eqz v0, :cond_2

    .line 293
    :cond_1
    :goto_0
    return-void

    .line 277
    :cond_2
    iget v0, p0, Ltv/periscope/android/ui/broadcast/am;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/am;->q:I

    .line 278
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/am;->r:J

    .line 280
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->k:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->d()J

    move-result-wide v0

    .line 281
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/am;->k:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->l()J

    move-result-wide v2

    iput-wide v2, p0, Ltv/periscope/android/ui/broadcast/am;->o:J

    .line 282
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ltv/periscope/android/ui/broadcast/am;->p:J

    .line 284
    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/am;->o:J

    invoke-direct {p0, v0, v1, v2, v3}, Ltv/periscope/android/ui/broadcast/am;->a(JJ)V

    .line 285
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->g:Ltv/periscope/android/ui/broadcast/am$d;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/am$d;->a(Landroid/view/MotionEvent;)V

    .line 286
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/am;->m:Z

    .line 287
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/am;->n:Z

    .line 288
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a()V

    .line 290
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->l:Ltv/periscope/android/ui/broadcast/am$c;

    if-eqz v0, :cond_1

    .line 291
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->l:Ltv/periscope/android/ui/broadcast/am$c;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/am$c;->a()V

    goto :goto_0
.end method

.method a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/ThumbnailPlaylistItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 238
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->h:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    .line 239
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->i:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    .line 241
    if-eqz p1, :cond_1

    .line 242
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/ThumbnailPlaylistItem;

    .line 243
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/am;->h:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ltv/periscope/android/api/ThumbnailPlaylistItem;->getTimeInMs()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 246
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x14

    invoke-direct {p0, v0, p1, v1}, Ltv/periscope/android/ui/broadcast/am;->a(Landroid/content/Context;Ljava/util/List;I)V

    .line 248
    :cond_1
    return-void
.end method

.method public a(Ltv/periscope/android/player/d;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/am;->k:Ltv/periscope/android/player/d;

    .line 235
    return-void
.end method

.method public a(Ltv/periscope/android/ui/broadcast/am$c;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/am;->l:Ltv/periscope/android/ui/broadcast/am$c;

    .line 263
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/am;->m:Z

    return v0
.end method

.method b()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    .line 304
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/am;->m:Z

    if-nez v0, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 309
    :cond_1
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/am;->r:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    .line 310
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/am;->s:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Ltv/periscope/android/ui/broadcast/am;->r:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/am;->s:J

    .line 311
    iput-wide v6, p0, Ltv/periscope/android/ui/broadcast/am;->r:J

    .line 313
    :cond_2
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/am;->p:J

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/am;->o:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    .line 314
    const-string/jumbo v0, "forward"

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/am;->a(Ljava/lang/String;)V

    .line 318
    :goto_1
    iput-wide v6, p0, Ltv/periscope/android/ui/broadcast/am;->o:J

    .line 319
    iput-wide v6, p0, Ltv/periscope/android/ui/broadcast/am;->p:J

    .line 321
    iput-boolean v8, p0, Ltv/periscope/android/ui/broadcast/am;->m:Z

    .line 322
    iput-boolean v8, p0, Ltv/periscope/android/ui/broadcast/am;->n:Z

    .line 323
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->b()V

    .line 325
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->l:Ltv/periscope/android/ui/broadcast/am$c;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->l:Ltv/periscope/android/ui/broadcast/am$c;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/am$c;->b()V

    goto :goto_0

    .line 316
    :cond_3
    const-string/jumbo v0, "reverse"

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/am;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 353
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/am;->g:Ltv/periscope/android/ui/broadcast/am$d;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/am;->f:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0, v1, p1}, Ltv/periscope/android/ui/broadcast/am$d;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method c()V
    .locals 4

    .prologue
    .line 338
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/am;->s:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-double v0, v0

    .line 339
    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    .line 340
    double-to-int v0, v0

    mul-int/lit8 v0, v0, 0xa

    .line 342
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 343
    const-string/jumbo v2, "n_times"

    iget v3, p0, Ltv/periscope/android/ui/broadcast/am;->q:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    const-string/jumbo v2, "n_duration"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    const/4 v0, 0x0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/am;->q:I

    .line 349
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/am;->s:J

    .line 350
    return-void
.end method
