.class abstract Ltv/periscope/android/ui/broadcast/a;
.super Ltv/periscope/android/ui/view/a;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/c;


# instance fields
.field private c:Ltv/periscope/android/view/i;

.field private final d:Z


# direct methods
.method public constructor <init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/ActionSheet;Z)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/view/a;-><init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/ActionSheet;)V

    .line 30
    iput-boolean p3, p0, Ltv/periscope/android/ui/broadcast/a;->d:Z

    .line 31
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ltv/periscope/model/chat/Message;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/view/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/a;->a()Ltv/periscope/android/ui/broadcast/b;

    move-result-object v0

    .line 72
    if-nez v0, :cond_0

    .line 73
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 75
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Ltv/periscope/android/ui/broadcast/b;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method abstract a()Ltv/periscope/android/ui/broadcast/b;
.end method

.method public a(Lcyw;Ldae;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/view/ActionSheet;->a(Lcyw;Ldae;)V

    .line 80
    return-void
.end method

.method public a(Ljava/lang/CharSequence;Ljava/util/List;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List",
            "<+",
            "Ltv/periscope/android/view/a;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 102
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 103
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/a;

    .line 104
    invoke-interface {v0}, Ltv/periscope/android/view/a;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ltv/periscope/android/view/a;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Ltv/periscope/android/ui/broadcast/a;->d:Z

    if-eqz v3, :cond_0

    .line 105
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 108
    :cond_2
    invoke-super {p0, p1, v1, p3}, Ltv/periscope/android/ui/view/a;->a(Ljava/lang/CharSequence;Ljava/util/List;I)V

    .line 109
    return-void
.end method

.method public a(Ljava/lang/String;Ltv/periscope/model/chat/Message;IZ)V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/a;->c:Ltv/periscope/android/view/i;

    if-nez v0, :cond_0

    .line 53
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/a;->c:Ltv/periscope/android/view/i;

    const/4 v1, 0x5

    invoke-interface {v0, p3, v1}, Ltv/periscope/android/view/i;->a(II)Ljava/util/List;

    move-result-object v0

    .line 51
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-interface {v0, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ltv/periscope/android/view/ActionSheet;->a(Ljava/util/List;I)V

    .line 52
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p4}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v1, v2}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/CharSequence;Ljava/util/List;I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ltv/periscope/model/chat/Message;IZLjava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/a;->c:Ltv/periscope/android/view/i;

    if-nez v0, :cond_0

    .line 64
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/a;->c:Ltv/periscope/android/view/i;

    const/4 v1, 0x5

    invoke-interface {v0, p3, v1}, Ltv/periscope/android/view/i;->a(II)Ljava/util/List;

    move-result-object v0

    .line 62
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-interface {v0, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ltv/periscope/android/view/ActionSheet;->a(Ljava/util/List;I)V

    .line 63
    invoke-virtual {p0, p1, p2, p4}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, p5, v0, v1}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/CharSequence;Ljava/util/List;I)V

    goto :goto_0
.end method

.method public a(Ltv/periscope/android/ui/chat/ah;)V
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/a;->a()Ltv/periscope/android/ui/broadcast/b;

    move-result-object v0

    .line 35
    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/b;->a(Ltv/periscope/android/ui/chat/ah;)V

    .line 38
    :cond_0
    return-void
.end method

.method public a(Ltv/periscope/android/view/CarouselView$a;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/ActionSheet;->setCarouselScrollListener(Ltv/periscope/android/view/CarouselView$a;)V

    .line 84
    return-void
.end method

.method public a(Ltv/periscope/android/view/i;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/a;->c:Ltv/periscope/android/view/i;

    .line 42
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-virtual {v0}, Ltv/periscope/android/view/ActionSheet;->b()V

    .line 89
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-virtual {v0}, Ltv/periscope/android/view/ActionSheet;->a()V

    .line 94
    return-void
.end method

.method public d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 97
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/a;->b:Ltv/periscope/android/view/ActionSheet;

    invoke-virtual {v1}, Ltv/periscope/android/view/ActionSheet;->getScrollPage()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
