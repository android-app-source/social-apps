.class Ltv/periscope/android/ui/broadcast/q;
.super Ltv/periscope/android/ui/broadcast/j;
.source "Twttr"


# instance fields
.field private k:I

.field private l:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcyw;Lcyn;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/android/ui/broadcast/y;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-direct/range {p0 .. p5}, Ltv/periscope/android/ui/broadcast/j;-><init>(Landroid/content/Context;Lcyw;Lcyn;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/android/ui/broadcast/y;)V

    .line 21
    iput-boolean p6, p0, Ltv/periscope/android/ui/broadcast/q;->l:Z

    .line 22
    return-void
.end method


# virtual methods
.method protected a(Ltv/periscope/model/p;)V
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/q;->j()V

    .line 32
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/q;->k()V

    .line 34
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/q;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/q;->a(Ltv/periscope/model/p;I)V

    .line 36
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/q;->l:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/q;->j:Z

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/q;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/q;->c(Ltv/periscope/model/p;I)V

    .line 41
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/q;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/q;->k:I

    .line 42
    invoke-virtual {p1}, Ltv/periscope/model/p;->L()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ltv/periscope/model/p;->M()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 43
    :cond_1
    iget v0, p0, Ltv/periscope/android/ui/broadcast/q;->k:I

    invoke-virtual {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/q;->b(Ltv/periscope/model/p;I)V

    .line 45
    :cond_2
    return-void
.end method

.method protected b()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    return-object v0
.end method

.method protected b(Ltv/periscope/model/p;)V
    .locals 5

    .prologue
    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/q;->i:Z

    .line 50
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/q;->b:Ljava/util/List;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/q;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 51
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/q;->b:Ljava/util/List;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/q;->k:I

    new-instance v2, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$j;

    sget-object v3, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;->a:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    .line 53
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/q;->b()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    move-result-object v4

    invoke-direct {v2, p0, p1, v3, v4}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$j;-><init>(Ltv/periscope/android/ui/broadcast/j;Ltv/periscope/model/p;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;)V

    .line 51
    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 54
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/q;->r()V

    .line 55
    return-void
.end method
