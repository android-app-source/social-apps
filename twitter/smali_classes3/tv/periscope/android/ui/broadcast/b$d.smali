.class Ltv/periscope/android/ui/broadcast/b$d;
.super Ltv/periscope/android/ui/broadcast/b$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "d"
.end annotation


# instance fields
.field private final c:Lcrz;

.field private final d:Ltv/periscope/android/view/c;

.field private final e:Ldae;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Lcrz;Ldae;)V
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0, p1, p2, p3, p4}, Ltv/periscope/android/ui/broadcast/b$b;-><init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;)V

    .line 134
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/b$d;->c:Lcrz;

    .line 135
    iput-object p6, p0, Ltv/periscope/android/ui/broadcast/b$d;->e:Ldae;

    .line 136
    new-instance v0, Ltv/periscope/android/ui/broadcast/b$d$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/b$d$1;-><init>(Ltv/periscope/android/ui/broadcast/b$d;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/b$d;->d:Ltv/periscope/android/view/c;

    .line 146
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/b$d;)Ldae;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/b$d;->e:Ldae;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    sget v0, Ltv/periscope/android/library/f$l;->ps__action_sheet_label_view_profile:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method public f()Ltv/periscope/android/view/c;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/b$d;->d:Ltv/periscope/android/view/c;

    return-object v0
.end method

.method protected g()V
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/b$d;->c:Lcrz;

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->b(Lcrz;)V

    .line 171
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/b$d;->h()Ltv/periscope/android/view/aj;

    move-result-object v0

    new-instance v1, Ltv/periscope/android/ui/d;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/b$d;->b:Ltv/periscope/model/chat/Message;

    invoke-virtual {v2}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Ltv/periscope/android/ui/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltv/periscope/android/view/aj;->a(Ljava/lang/Object;)V

    .line 172
    return-void
.end method
