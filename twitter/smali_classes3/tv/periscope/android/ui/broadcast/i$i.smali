.class public Ltv/periscope/android/ui/broadcast/i$i;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "i"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 1

    .prologue
    .line 495
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 496
    sget v0, Ltv/periscope/android/library/f$g;->more_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$i;->b:Landroid/widget/TextView;

    .line 497
    sget v0, Ltv/periscope/android/library/f$g;->more_total:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$i;->c:Landroid/widget/TextView;

    .line 498
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$i;
    .locals 3

    .prologue
    .line 489
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_info_more:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 491
    new-instance v1, Ltv/periscope/android/ui/broadcast/i$i;

    invoke-direct {v1, v0, p2}, Ltv/periscope/android/ui/broadcast/i$i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    return-object v1
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    .line 502
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;->d()Ltv/periscope/model/q;

    move-result-object v0

    .line 503
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$i;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 504
    sget-object v2, Ltv/periscope/android/ui/broadcast/i$1;->a:[I

    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;->b()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;

    move-result-object v3

    invoke-virtual {v3}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 518
    :goto_0
    return-void

    .line 506
    :pswitch_0
    invoke-virtual {v0}, Ltv/periscope/model/q;->b()J

    move-result-wide v2

    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;->c()I

    move-result v0

    int-to-long v4, v0

    sub-long/2addr v2, v4

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 507
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$i;->c:Landroid/widget/TextView;

    invoke-static {v1, v2, v3, v6}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 508
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$i;->b:Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__more_viewers:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 512
    :pswitch_1
    invoke-virtual {v0}, Ltv/periscope/model/q;->a()J

    move-result-wide v2

    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;->c()I

    move-result v0

    int-to-long v4, v0

    sub-long/2addr v2, v4

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 513
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$i;->c:Landroid/widget/TextView;

    invoke-static {v1, v2, v3, v6}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 514
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$i;->b:Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__more_viewers:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 504
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 484
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/i$i;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;)V

    return-void
.end method
