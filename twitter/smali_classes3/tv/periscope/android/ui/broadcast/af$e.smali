.class Ltv/periscope/android/ui/broadcast/af$e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/moderator/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation


# instance fields
.field a:Z

.field final synthetic b:Ltv/periscope/android/ui/broadcast/af;

.field private final c:Ltv/periscope/android/util/n$a;

.field private final d:Ltv/periscope/android/util/n$a$a;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/af;Ltv/periscope/android/util/n$a;)V
    .locals 2

    .prologue
    .line 2800
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af$e;->b:Ltv/periscope/android/ui/broadcast/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2785
    new-instance v0, Ltv/periscope/android/ui/broadcast/af$e$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/af$e$1;-><init>(Ltv/periscope/android/ui/broadcast/af$e;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/af$e;->d:Ltv/periscope/android/util/n$a$a;

    .line 2801
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/af$e;->c:Ltv/periscope/android/util/n$a;

    .line 2802
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$e;->c:Ltv/periscope/android/util/n$a;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$e;->d:Ltv/periscope/android/util/n$a$a;

    invoke-virtual {v0, v1}, Ltv/periscope/android/util/n$a;->a(Ltv/periscope/android/util/n$a$a;)V

    .line 2803
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2807
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af$e;->b:Ltv/periscope/android/ui/broadcast/af;

    iget-object v2, v2, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    sget-object v3, Ltv/periscope/android/player/PlayMode;->b:Ltv/periscope/android/player/PlayMode;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af$e;->b:Ltv/periscope/android/ui/broadcast/af;

    .line 2808
    invoke-virtual {v2}, Ltv/periscope/android/ui/broadcast/af;->E()Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Ltv/periscope/android/ui/broadcast/af$e;->a:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af$e;->b:Ltv/periscope/android/ui/broadcast/af;

    .line 2810
    invoke-virtual {v2}, Ltv/periscope/android/ui/broadcast/af;->n()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/af$e;->b:Ltv/periscope/android/ui/broadcast/af;

    iget-boolean v2, v2, Ltv/periscope/android/ui/broadcast/af;->z:Z

    if-eqz v2, :cond_1

    move v2, v0

    .line 2813
    :goto_0
    if-nez v2, :cond_0

    .line 2814
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/af$e;->b:Ltv/periscope/android/ui/broadcast/af;

    iget-boolean v3, v3, Ltv/periscope/android/ui/broadcast/af;->z:Z

    if-nez v3, :cond_2

    .line 2815
    :goto_1
    invoke-static {v0}, Ltv/periscope/android/analytics/d;->a(Z)V

    .line 2817
    :cond_0
    return v2

    :cond_1
    move v2, v1

    .line 2810
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2814
    goto :goto_1
.end method
