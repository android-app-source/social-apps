.class Ltv/periscope/android/ui/broadcast/af$g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/am$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "g"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/af;

.field private b:Z


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/af;)V
    .locals 1

    .prologue
    .line 2723
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2725
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->b:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2729
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iput-boolean v4, v1, Ltv/periscope/android/ui/broadcast/af;->A:Z

    iput-boolean v4, v0, Ltv/periscope/android/ui/broadcast/af;->B:Z

    .line 2730
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2733
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->h(Ltv/periscope/android/analytics/summary/b;)V

    .line 2735
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, v1, Ltv/periscope/android/ui/broadcast/af;->v:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2736
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iput-boolean v4, v0, Ltv/periscope/android/ui/broadcast/af;->C:Z

    .line 2737
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, v1, Ltv/periscope/android/ui/broadcast/af;->u:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2739
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/android/ui/broadcast/af;Z)V

    .line 2740
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, v1, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v1}, Ltv/periscope/android/player/d;->q()I

    move-result v1

    const-wide/16 v2, 0x1e

    invoke-interface {v0, v1, v2, v3}, Ltv/periscope/android/player/c;->a(IJ)Z

    .line 2741
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->M()V

    .line 2742
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->W()V

    .line 2743
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->i()V

    .line 2744
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->g()Z

    move-result v0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->b:Z

    .line 2745
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e()V

    .line 2746
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->a:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    .line 2747
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    invoke-virtual {v0, v4}, Ltv/periscope/android/view/RootDragLayout;->setDraggable(Z)V

    .line 2749
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 2753
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2754
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    const/4 v2, 0x1

    iput-boolean v2, v1, Ltv/periscope/android/ui/broadcast/af;->A:Z

    iput-boolean v2, v0, Ltv/periscope/android/ui/broadcast/af;->B:Z

    .line 2755
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->a(Ljava/lang/String;)V

    .line 2756
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-static {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/android/ui/broadcast/af;J)V

    .line 2762
    :goto_0
    return-void

    .line 2760
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/am;->b()V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 2766
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    const/4 v1, 0x0

    iput-boolean v1, v0, Ltv/periscope/android/ui/broadcast/af;->A:Z

    .line 2770
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->y()V

    .line 2771
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->L()V

    .line 2772
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    invoke-interface {v0}, Ltv/periscope/android/player/c;->V()V

    .line 2773
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->b:Z

    if-eqz v0, :cond_0

    .line 2774
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->d()V

    .line 2778
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    .line 2779
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->setDraggable(Z)V

    .line 2780
    return-void

    .line 2776
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$g;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->p:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e()V

    goto :goto_0
.end method
