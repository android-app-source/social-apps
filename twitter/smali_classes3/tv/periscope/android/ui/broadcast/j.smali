.class public abstract Ltv/periscope/android/ui/broadcast/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcyq;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcyq",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;",
        ">;"
    }
.end annotation


# instance fields
.field protected final a:Ltv/periscope/android/ui/broadcast/h;

.field protected final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field protected final c:Lcyn;

.field protected final d:Lcyw;

.field protected final e:Landroid/content/Context;

.field protected f:Ljava/lang/String;

.field protected g:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

.field protected h:Lcyq$a;

.field protected i:Z

.field protected j:Z

.field private final k:Ltv/periscope/android/ui/broadcast/y;

.field private final l:Z

.field private m:Ltv/periscope/android/ui/broadcast/n;

.field private n:Landroid/location/Location;

.field private o:I

.field private p:I

.field private q:J

.field private r:I

.field private s:J

.field private t:Z

.field private u:Z

.field private v:Ltv/periscope/android/ui/broadcast/j$a;

.field private final w:Ltv/periscope/android/util/i$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ltv/periscope/android/util/i$a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcyw;Lcyn;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/android/ui/broadcast/y;)V
    .locals 6

    .prologue
    const/16 v0, 0xf

    const-wide/16 v4, -0x1

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput v0, p0, Ltv/periscope/android/ui/broadcast/j;->o:I

    .line 49
    iput v0, p0, Ltv/periscope/android/ui/broadcast/j;->p:I

    .line 50
    iput-wide v4, p0, Ltv/periscope/android/ui/broadcast/j;->q:J

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/j;->r:I

    .line 52
    iput-wide v4, p0, Ltv/periscope/android/ui/broadcast/j;->s:J

    .line 421
    new-instance v0, Ltv/periscope/android/ui/broadcast/j$2;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/j$2;-><init>(Ltv/periscope/android/ui/broadcast/j;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->w:Ltv/periscope/android/util/i$a;

    .line 68
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/j;->e:Landroid/content/Context;

    .line 69
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/j;->c:Lcyn;

    .line 70
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/j;->a:Ltv/periscope/android/ui/broadcast/h;

    .line 71
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/j;->d:Lcyw;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    .line 73
    invoke-static {p1}, Lczs;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/j;->l:Z

    .line 74
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/j;->k:Ltv/periscope/android/ui/broadcast/y;

    .line 75
    new-instance v0, Ltv/periscope/android/ui/broadcast/n;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v4, v5, v2, v3}, Ltv/periscope/android/ui/broadcast/n;-><init>(JJ)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->m:Ltv/periscope/android/ui/broadcast/n;

    .line 77
    sget-object v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;->a:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->g:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    .line 78
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/j;Z)Z
    .locals 0

    .prologue
    .line 28
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/j;->u:Z

    return p1
.end method

.method private s()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 88
    iget-boolean v2, p0, Ltv/periscope/android/ui/broadcast/j;->j:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Ltv/periscope/android/ui/broadcast/j;->u:Z

    if-eqz v2, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->c()Ltv/periscope/model/p;

    move-result-object v2

    .line 92
    if-nez v2, :cond_2

    .line 93
    iput-boolean v1, p0, Ltv/periscope/android/ui/broadcast/j;->j:Z

    goto :goto_0

    .line 97
    :cond_2
    invoke-virtual {v2}, Ltv/periscope/model/p;->K()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Ltv/periscope/model/p;->M()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 98
    :cond_3
    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/j;->u:Z

    .line 99
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->k:Ltv/periscope/android/ui/broadcast/y;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->f:Ljava/lang/String;

    new-instance v2, Ltv/periscope/android/ui/broadcast/j$1;

    invoke-direct {v2, p0}, Ltv/periscope/android/ui/broadcast/j$1;-><init>(Ltv/periscope/android/ui/broadcast/j;)V

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/y;->a(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/y$b;)V

    goto :goto_0

    .line 113
    :cond_4
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/j;->m:Ltv/periscope/android/ui/broadcast/n;

    invoke-virtual {v2}, Ltv/periscope/android/ui/broadcast/n;->c()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    :goto_1
    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/j;->j:Z

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method private t()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 429
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->d:Lcyw;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyw;->d(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->w:Ltv/periscope/android/util/i$a;

    invoke-static {v0, v1}, Ltv/periscope/android/util/i;->a(Ljava/util/Collection;Ltv/periscope/android/util/i$a;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private u()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 433
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->d:Lcyw;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyw;->d(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->w:Ltv/periscope/android/util/i$a;

    invoke-static {v0, v1}, Ltv/periscope/android/util/i;->b(Ljava/util/Collection;Ltv/periscope/android/util/i$a;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private v()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 438
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->d:Lcyw;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyw;->e(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->w:Ltv/periscope/android/util/i$a;

    invoke-static {v0, v1}, Ltv/periscope/android/util/i;->a(Ljava/util/Collection;Ltv/periscope/android/util/i$a;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private w()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->d:Lcyw;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyw;->e(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->w:Ltv/periscope/android/util/i$a;

    invoke-static {v0, v1}, Ltv/periscope/android/util/i;->b(Ljava/util/Collection;Ltv/periscope/android/util/i$a;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/j;->b(I)Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 194
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/j;->s:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    new-instance v0, Ltv/periscope/android/ui/broadcast/n;

    invoke-direct {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/n;-><init>(J)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->m:Ltv/periscope/android/ui/broadcast/n;

    .line 198
    iput-wide p1, p0, Ltv/periscope/android/ui/broadcast/j;->s:J

    .line 201
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/j;->j:Z

    if-nez v0, :cond_0

    .line 202
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->d()V

    goto :goto_0
.end method

.method public a(Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->n:Landroid/location/Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->n:Landroid/location/Location;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/j;->n:Landroid/location/Location;

    .line 135
    :cond_0
    return-void
.end method

.method public a(Lcyq$a;)V
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/j;->h:Lcyq$a;

    .line 456
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->v:Ltv/periscope/android/ui/broadcast/j$a;

    if-nez v0, :cond_0

    .line 222
    new-instance v0, Ltv/periscope/android/ui/broadcast/j$a;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Ltv/periscope/android/ui/broadcast/j$a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->v:Ltv/periscope/android/ui/broadcast/j$a;

    .line 226
    :cond_0
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/j;->j:Z

    if-eqz v0, :cond_1

    .line 227
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->k:Ltv/periscope/android/ui/broadcast/y;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->v:Ltv/periscope/android/ui/broadcast/j$a;

    invoke-virtual {v0, p1, v1}, Ltv/periscope/android/ui/broadcast/y;->a(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/x$b;)V

    .line 229
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/n;)V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->k:Ltv/periscope/android/ui/broadcast/y;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/y;->a(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/n;)V

    .line 233
    return-void
.end method

.method public a(Ltv/periscope/android/event/ParticipantHeartCountEvent;)V
    .locals 5

    .prologue
    .line 144
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 145
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 146
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;

    .line 148
    iget-object v3, p1, Ltv/periscope/android/event/ParticipantHeartCountEvent;->b:Ljava/lang/String;

    iget-object v4, v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p1, Ltv/periscope/android/event/ParticipantHeartCountEvent;->c:Z

    iget-boolean v0, v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->c:Z

    if-ne v3, v0, :cond_1

    .line 149
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->r()V

    .line 154
    :cond_0
    return-void

    .line 145
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;)V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/j;->i:Z

    .line 279
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/j;->g:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    .line 280
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->n()V

    .line 281
    return-void
.end method

.method protected abstract a(Ltv/periscope/model/p;)V
.end method

.method protected a(Ltv/periscope/model/p;I)V
    .locals 5

    .prologue
    .line 165
    iput p2, p0, Ltv/periscope/android/ui/broadcast/j;->r:I

    .line 166
    invoke-virtual {p1}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ltv/periscope/model/p;->M()Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/j;->r:I

    new-instance v2, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->b()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;-><init>(Ltv/periscope/android/ui/broadcast/j;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;)V

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 173
    :goto_0
    return-void

    .line 168
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->p()Ltv/periscope/model/q;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/j;->r:I

    new-instance v2, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/j;->g:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->b()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    move-result-object v4

    invoke-direct {v2, p0, v3, v4}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;-><init>(Ltv/periscope/android/ui/broadcast/j;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;)V

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 171
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/j;->r:I

    new-instance v2, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$n;

    invoke-direct {v2, p0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$n;-><init>(Ltv/periscope/android/ui/broadcast/j;)V

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/j;->t:Z

    .line 85
    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/ui/broadcast/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->k:Ltv/periscope/android/ui/broadcast/y;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/y;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;
.end method

.method public b(I)Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 266
    iput-wide p1, p0, Ltv/periscope/android/ui/broadcast/j;->q:J

    .line 267
    return-void
.end method

.method public b(Lcyq$a;)V
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->h:Lcyq$a;

    if-ne p1, v0, :cond_0

    .line 461
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->h:Lcyq$a;

    .line 463
    :cond_0
    return-void
.end method

.method protected abstract b(Ltv/periscope/model/p;)V
.end method

.method protected b(Ltv/periscope/model/p;I)V
    .locals 4

    .prologue
    .line 177
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->p()Ltv/periscope/model/q;

    move-result-object v0

    if-nez v0, :cond_0

    .line 190
    :goto_0
    return-void

    .line 180
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->b()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    move-result-object v0

    .line 182
    sget-object v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    if-ne v0, v1, :cond_1

    iget-boolean v1, p0, Ltv/periscope/android/ui/broadcast/j;->i:Z

    if-eqz v1, :cond_2

    .line 183
    :cond_1
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v2, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$j;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/j;->g:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    invoke-direct {v2, p0, p1, v3, v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$j;-><init>(Ltv/periscope/android/ui/broadcast/j;Ltv/periscope/model/p;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;)V

    invoke-interface {v1, p2, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 187
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$d;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/j;->e:Landroid/content/Context;

    .line 188
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Ltv/periscope/android/library/f$l;->ps__show_stats:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Ltv/periscope/android/library/f$d;->ps__app_background_secondary:I

    invoke-direct {v1, v2, v3}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$d;-><init>(Ljava/lang/String;I)V

    .line 187
    invoke-interface {v0, p2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->k:Ltv/periscope/android/ui/broadcast/y;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/y;->b(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public c()Ltv/periscope/model/p;
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->c:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 128
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ltv/periscope/model/p;I)V
    .locals 4

    .prologue
    .line 270
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->k:Ltv/periscope/android/ui/broadcast/y;

    invoke-virtual {p1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/y;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 273
    new-instance v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$m;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$m;-><init>(Ltv/periscope/android/ui/broadcast/j;)V

    .line 274
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 275
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/j;->s()V

    .line 209
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->n()V

    .line 210
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 471
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/j;->f:Ljava/lang/String;

    .line 472
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 217
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->r()V

    .line 218
    return-void
.end method

.method public f()Ltv/periscope/android/ui/broadcast/n;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->m:Ltv/periscope/android/ui/broadcast/n;

    return-object v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 255
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->c()Ltv/periscope/model/p;

    move-result-object v0

    .line 256
    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Ltv/periscope/android/ui/broadcast/j;->r:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->r()V

    goto :goto_0
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 285
    const/4 v0, -0x1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/j;->r:I

    .line 286
    return-void
.end method

.method protected i()V
    .locals 4

    .prologue
    .line 289
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->c()Ltv/periscope/model/p;

    move-result-object v0

    .line 290
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ltv/periscope/model/p;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ltv/periscope/model/p;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 291
    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$f;

    .line 292
    invoke-virtual {v0}, Ltv/periscope/model/p;->j()Ljava/lang/String;

    move-result-object v2

    .line 293
    invoke-virtual {v0}, Ltv/periscope/model/p;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ltv/periscope/model/p;->i()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$f;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v2, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$e;

    invoke-direct {v2, v1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$e;-><init>(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$f;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    :cond_0
    return-void
.end method

.method protected j()V
    .locals 8

    .prologue
    .line 299
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->c()Ltv/periscope/model/p;

    move-result-object v0

    .line 300
    iget-boolean v1, p0, Ltv/periscope/android/ui/broadcast/j;->t:Z

    if-eqz v1, :cond_0

    .line 301
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v2, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$k;

    invoke-direct {v2, p0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$k;-><init>(Ltv/periscope/android/ui/broadcast/j;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 303
    :cond_0
    iget-boolean v1, p0, Ltv/periscope/android/ui/broadcast/j;->l:Z

    if-eqz v1, :cond_1

    .line 304
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ltv/periscope/model/p;->w()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 305
    invoke-virtual {v0}, Ltv/periscope/model/p;->n()D

    move-result-wide v2

    invoke-virtual {v0}, Ltv/periscope/model/p;->o()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lczr;->a(DD)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 306
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v2, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$i;

    invoke-virtual {v0}, Ltv/periscope/model/p;->n()D

    move-result-wide v4

    invoke-virtual {v0}, Ltv/periscope/model/p;->o()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$i;-><init>(DD)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 311
    :cond_1
    :goto_0
    return-void

    .line 307
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->n:Landroid/location/Location;

    invoke-static {v0}, Lczr;->a(Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$i;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/j;->n:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/j;->n:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$i;-><init>(DD)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected k()V
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$c;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$c;-><init>(Ltv/periscope/android/ui/broadcast/j;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 315
    return-void
.end method

.method protected l()V
    .locals 14

    .prologue
    const/16 v3, 0x32

    const/16 v2, 0xf

    const/4 v7, 0x0

    .line 318
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->c()Ltv/periscope/model/p;

    move-result-object v0

    .line 319
    if-nez v0, :cond_3

    const/4 v5, 0x0

    .line 320
    :goto_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 321
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/j;->v()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 322
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/j;->w()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 325
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/j;->v()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 326
    iput v3, p0, Ltv/periscope/android/ui/broadcast/j;->p:I

    .line 329
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 330
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/j;->t()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 331
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/j;->u()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 334
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/j;->t()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    if-le v1, v2, :cond_1

    .line 335
    iput v3, p0, Ltv/periscope/android/ui/broadcast/j;->o:I

    .line 339
    :cond_1
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 391
    :cond_2
    :goto_1
    return-void

    .line 319
    :cond_3
    invoke-virtual {v0}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 343
    :cond_4
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 344
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->p()Ltv/periscope/model/q;

    move-result-object v11

    .line 345
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    .line 347
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$g;

    sget v2, Ltv/periscope/android/library/f$l;->ps__stat_replay_viewers:I

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$g;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 350
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v6, v7

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 351
    iget-object v13, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/j;->f:Ljava/lang/String;

    const/4 v4, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;-><init>(Ltv/periscope/android/ui/broadcast/j;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    add-int/lit8 v0, v6, 0x1

    .line 354
    iget v1, p0, Ltv/periscope/android/ui/broadcast/j;->p:I

    if-lt v0, v1, :cond_8

    .line 358
    :cond_5
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Ltv/periscope/android/ui/broadcast/j;->p:I

    if-le v0, v1, :cond_6

    if-eqz v11, :cond_6

    .line 360
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;

    sget-object v2, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;

    iget v3, p0, Ltv/periscope/android/ui/broadcast/j;->p:I

    invoke-direct {v1, p0, v2, v3}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;-><init>(Ltv/periscope/android/ui/broadcast/j;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    :cond_6
    :goto_3
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 374
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$g;

    sget v2, Ltv/periscope/android/library/f$l;->ps__stat_live_viewers:I

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$g;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v6, v7

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 378
    iget-object v10, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/j;->f:Ljava/lang/String;

    move-object v1, p0

    move v4, v7

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;-><init>(Ltv/periscope/android/ui/broadcast/j;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 379
    add-int/lit8 v0, v6, 0x1

    .line 381
    iget v1, p0, Ltv/periscope/android/ui/broadcast/j;->o:I

    if-lt v0, v1, :cond_a

    .line 385
    :cond_7
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Ltv/periscope/android/ui/broadcast/j;->o:I

    if-le v0, v1, :cond_2

    if-eqz v11, :cond_2

    .line 387
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;

    sget-object v2, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;->a:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;

    iget v3, p0, Ltv/periscope/android/ui/broadcast/j;->o:I

    invoke-direct {v1, p0, v2, v3}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More;-><init>(Ltv/periscope/android/ui/broadcast/j;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$More$MoreType;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_8
    move v6, v0

    .line 357
    goto/16 :goto_2

    .line 364
    :cond_9
    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v0}, Ltv/periscope/model/p;->N()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 367
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->b:Ljava/util/List;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$g;

    sget v2, Ltv/periscope/android/library/f$l;->ps__no_replay_viewers:I

    .line 368
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$g;-><init>(Ljava/lang/String;)V

    .line 367
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_a
    move v6, v0

    .line 384
    goto :goto_4
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->f:Ljava/lang/String;

    return-object v0
.end method

.method public n()V
    .locals 1

    .prologue
    .line 399
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->h()V

    .line 401
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->c()Ltv/periscope/model/p;

    move-result-object v0

    .line 402
    if-nez v0, :cond_0

    .line 404
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->r()V

    .line 415
    :goto_0
    return-void

    .line 408
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->i()V

    .line 410
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/j;->s()V

    .line 411
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/j;->a(Ltv/periscope/model/p;)V

    .line 413
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->l()V

    .line 414
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/j;->r()V

    goto :goto_0
.end method

.method public o()Lcyw;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->d:Lcyw;

    return-object v0
.end method

.method public p()Ltv/periscope/model/q;
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->c:Lcyn;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/j;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->b(Ljava/lang/String;)Ltv/periscope/model/q;

    move-result-object v0

    return-object v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 450
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/j;->q:J

    return-wide v0
.end method

.method r()V
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->h:Lcyq$a;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/j;->h:Lcyq$a;

    invoke-interface {v0}, Lcyq$a;->a()V

    .line 478
    :cond_0
    return-void
.end method
