.class Ltv/periscope/android/ui/broadcast/aw$a;
.super Landroid/text/style/ClickableSpan;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/aw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/aw;


# direct methods
.method private constructor <init>(Ltv/periscope/android/ui/broadcast/aw;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/aw$a;->a:Ltv/periscope/android/ui/broadcast/aw;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ltv/periscope/android/ui/broadcast/aw;Ltv/periscope/android/ui/broadcast/aw$1;)V
    .locals 0

    .prologue
    .line 364
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/aw$a;-><init>(Ltv/periscope/android/ui/broadcast/aw;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 367
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw$a;->a:Ltv/periscope/android/ui/broadcast/aw;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/aw;->f(Ltv/periscope/android/ui/broadcast/aw;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw$a;->a:Ltv/periscope/android/ui/broadcast/aw;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/aw;->f(Ltv/periscope/android/ui/broadcast/aw;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aw$a;->a:Ltv/periscope/android/ui/broadcast/aw;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/aw;->f(Ltv/periscope/android/ui/broadcast/aw;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/SettingsDelegate;

    sget-object v1, Ltv/periscope/android/SettingsDelegate$SettingType;->c:Ltv/periscope/android/SettingsDelegate$SettingType;

    invoke-virtual {v1}, Ltv/periscope/android/SettingsDelegate$SettingType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/SettingsDelegate;->a(Ljava/lang/String;)V

    .line 370
    :cond_0
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 374
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 375
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 376
    return-void
.end method
