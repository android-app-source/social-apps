.class public Ltv/periscope/android/ui/broadcast/i$a;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$a;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private final b:Ltv/periscope/android/view/ActionSheetItem;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private e:Ldaj;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldaj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 1

    .prologue
    .line 254
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 255
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/i$a;->c:Landroid/view/View;

    .line 256
    sget v0, Ltv/periscope/android/library/f$g;->default_action:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/ActionSheetItem;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$a;->b:Ltv/periscope/android/view/ActionSheetItem;

    .line 257
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$a;->b:Ltv/periscope/android/view/ActionSheetItem;

    invoke-virtual {v0, p0}, Ltv/periscope/android/view/ActionSheetItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    sget v0, Ltv/periscope/android/library/f$g;->more:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$a;->d:Landroid/view/View;

    .line 259
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$a;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$a;
    .locals 3

    .prologue
    .line 242
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_info_action:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 244
    new-instance v1, Ltv/periscope/android/ui/broadcast/i$a;

    invoke-direct {v1, v0, p2}, Ltv/periscope/android/ui/broadcast/i$a;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    return-object v1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 282
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$a;->b:Ltv/periscope/android/view/ActionSheetItem;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$a;->e:Ldaj;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$a;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldaj;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$a;->e:Ldaj;

    .line 283
    invoke-virtual {v2}, Ldaj;->g()I

    move-result v2

    .line 282
    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/view/ActionSheetItem;->a(Ljava/lang/CharSequence;I)V

    .line 284
    return-void
.end method

.method private a(Ldaj;)V
    .locals 1

    .prologue
    .line 297
    invoke-virtual {p1}, Ldaj;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/i$a;->a()V

    .line 300
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$a;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 264
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$a;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 265
    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldaj;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$a;->e:Ldaj;

    .line 266
    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/i$a;->f:Ljava/util/List;

    .line 267
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/i$a;->a()V

    .line 269
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$a;->c()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    move-result-object v0

    sget-object v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    if-ne v0, v1, :cond_0

    .line 270
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$a;->c:Landroid/view/View;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$a;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$d;->ps__app_background_secondary:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 273
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$a;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 278
    :goto_0
    return-void

    .line 276
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$a;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 238
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$a;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/i$a;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$a;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 288
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 289
    sget v1, Ltv/periscope/android/library/f$g;->more:I

    if-ne v0, v1, :cond_1

    .line 290
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$a;->a:Ltv/periscope/android/ui/broadcast/h;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$a;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/h;->a(Ljava/util/List;)V

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    sget v1, Ltv/periscope/android/library/f$g;->default_action:I

    if-ne v0, v1, :cond_0

    .line 292
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$a;->e:Ldaj;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/i$a;->a(Ldaj;)V

    goto :goto_0
.end method
