.class public Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ltv/periscope/android/ui/broadcast/j;

.field private final b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/j;Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;)V
    .locals 0

    .prologue
    .line 342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 343
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;->a:Ltv/periscope/android/ui/broadcast/j;

    .line 344
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    .line 345
    return-void
.end method


# virtual methods
.method public a()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;
    .locals 1

    .prologue
    .line 349
    sget-object v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;->l:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$Type;

    return-object v0
.end method

.method public b()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;->a:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->q()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public c()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$h;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    return-object v0
.end method
