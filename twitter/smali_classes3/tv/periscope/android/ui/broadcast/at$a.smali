.class Ltv/periscope/android/ui/broadcast/at$a;
.super Ltv/periscope/android/ui/broadcast/b$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/at;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final c:Ltv/periscope/android/view/c;

.field private final d:Ltv/periscope/android/ui/user/g;


# direct methods
.method constructor <init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0, p1, p2, p3, p4}, Ltv/periscope/android/ui/broadcast/b$b;-><init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;)V

    .line 135
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/at$a;->d:Ltv/periscope/android/ui/user/g;

    .line 136
    new-instance v0, Ltv/periscope/android/ui/broadcast/at$a$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/at$a$1;-><init>(Ltv/periscope/android/ui/broadcast/at$a;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/at$a;->c:Ltv/periscope/android/view/c;

    .line 143
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    sget v0, Ltv/periscope/android/library/f$l;->ps__action_sheet_report_abuse:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 147
    sget v0, Ltv/periscope/android/library/f$d;->ps__red:I

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x1

    return v0
.end method

.method public f()Ltv/periscope/android/view/c;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/at$a;->c:Ltv/periscope/android/view/c;

    return-object v0
.end method

.method protected g()V
    .locals 4

    .prologue
    .line 173
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/at$a;->d:Ltv/periscope/android/ui/user/g;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/at$a;->b:Ltv/periscope/model/chat/Message;

    sget-object v2, Ltv/periscope/model/chat/MessageType$ReportType;->b:Ltv/periscope/model/chat/MessageType$ReportType;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/at$a;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Ltv/periscope/android/ui/user/g;->a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$ReportType;Ljava/lang/String;)V

    .line 174
    return-void
.end method
