.class Ltv/periscope/android/ui/broadcast/af$9;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/broadcast/af;->a(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Ltv/periscope/android/ui/broadcast/af;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/af;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 527
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af$9;->b:Ltv/periscope/android/ui/broadcast/af;

    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/af$9;->a:Landroid/view/View;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 530
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$9;->b:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    iget-boolean v0, v0, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$9;->b:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$9;->b:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->E()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$9;->b:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/am;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 531
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$9;->b:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->h:Ltv/periscope/android/ui/broadcast/am;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/am;->a(Landroid/view/MotionEvent;)V

    .line 532
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$9;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 534
    :cond_0
    return-void
.end method
