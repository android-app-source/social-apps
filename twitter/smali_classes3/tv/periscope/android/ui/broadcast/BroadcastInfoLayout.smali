.class public Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;
.super Landroid/view/ViewGroup;
.source "Twttr"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/support/v7/widget/RecyclerView;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 32
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a(Landroid/content/Context;)V

    .line 43
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_info_layout:I

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 47
    sget v0, Ltv/periscope/android/library/f$g;->action_button:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    .line 48
    sget v0, Ltv/periscope/android/library/f$g;->title_view:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->e:Landroid/view/View;

    .line 49
    sget v0, Ltv/periscope/android/library/f$g;->broadcast_title:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->b:Landroid/widget/TextView;

    .line 50
    sget v0, Ltv/periscope/android/library/f$g;->broadcast_subtitle:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->c:Landroid/widget/TextView;

    .line 51
    sget v0, Ltv/periscope/android/library/f$g;->ended_container:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->f:Landroid/view/View;

    .line 52
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->f:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->ended_time:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->g:Landroid/widget/TextView;

    .line 53
    sget v0, Ltv/periscope/android/library/f$g;->list:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d:Landroid/support/v7/widget/RecyclerView;

    .line 54
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 55
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 56
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 57
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 58
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout$1;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout$1;-><init>(Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addOnItemTouchListener(Landroid/support/v7/widget/RecyclerView$OnItemTouchListener;)V

    .line 83
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_play:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 124
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    invoke-static {p2}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_pause:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 128
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 141
    return-void
.end method

.method public canScrollVertically(I)Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_chat:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 186
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 160
    sub-int v1, p4, p2

    .line 161
    sub-int v0, p5, p3

    .line 163
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    .line 164
    sub-int/2addr v0, v2

    .line 166
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d:Landroid/support/v7/widget/RecyclerView;

    add-int/2addr v2, v0

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/support/v7/widget/RecyclerView;->layout(IIII)V

    .line 168
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->e:Landroid/view/View;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {v2, v4, v3, v1, v0}, Landroid/view/View;->layout(IIII)V

    .line 171
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Ltv/periscope/android/library/f$e;->ps__activity_horizontal_margin:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 172
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    .line 173
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    .line 175
    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->e:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    .line 178
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Ltv/periscope/android/util/ac;->a(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 179
    :goto_0
    div-int/lit8 v1, v3, 0x2

    sub-int v1, v4, v1

    .line 181
    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    add-int/2addr v2, v0

    add-int/2addr v3, v1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 182
    return-void

    .line 178
    :cond_0
    sub-int v0, v1, v0

    sub-int/2addr v0, v2

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p1, p2}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->measureChild(Landroid/view/View;II)V

    .line 146
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->e:Landroid/view/View;

    invoke-virtual {p0, v0, p1, p2}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->measureChild(Landroid/view/View;II)V

    .line 149
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 150
    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 152
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v1, p1, v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->measureChild(Landroid/view/View;II)V

    .line 154
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->getDefaultSize(II)I

    move-result v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->e:Landroid/view/View;

    .line 155
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    .line 154
    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->setMeasuredDimension(II)V

    .line 156
    return-void
.end method

.method public setActionButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 115
    if-eqz p1, :cond_0

    .line 116
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 120
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setAdapter(Ltv/periscope/android/ui/broadcast/g;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 132
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    return-void
.end method

.method public setTitleAlpha(F)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 90
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 91
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 92
    return-void
.end method
