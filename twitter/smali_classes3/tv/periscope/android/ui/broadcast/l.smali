.class public Ltv/periscope/android/ui/broadcast/l;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Lcom/google/android/exoplayer/upstream/BandwidthMeter$EventListener;
.implements Lczi$a;
.implements Lczi$b;
.implements Lczi$e;
.implements Lczk$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/l$c;,
        Ltv/periscope/android/ui/broadcast/l$b;,
        Ltv/periscope/android/ui/broadcast/l$a;
    }
.end annotation


# instance fields
.field private volatile A:Ltv/periscope/android/util/Size;

.field private B:J

.field private C:Landroid/view/TextureView;

.field private D:Ltv/periscope/android/graphics/b;

.field private E:Landroid/view/Surface;

.field private F:Ltv/periscope/android/graphics/i;

.field private G:Ltv/periscope/android/graphics/g;

.field private H:Ltv/periscope/android/graphics/n;

.field private I:Landroid/view/Surface;

.field private J:Landroid/os/Handler;

.field private K:Landroid/os/Handler;

.field private L:Lczi$f;

.field private M:J

.field private N:J

.field private O:I

.field private P:I

.field private Q:I

.field private R:Ltv/periscope/android/video/rtmp/i;

.field private S:J

.field private T:J

.field private U:J

.field private V:Ltv/periscope/android/video/rtmp/i;

.field private W:D

.field private X:Z

.field private Y:D

.field private Z:Landroid/view/OrientationEventListener;

.field protected a:Z

.field private aa:I

.field private ab:I

.field private ac:D

.field private ad:D

.field private ae:Z

.field private af:Z

.field private final ag:Ltv/periscope/android/ui/broadcast/l$a;

.field private ah:Z

.field private final ai:Ljava/lang/Runnable;

.field private aj:I

.field public final b:Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final c:Lcyn;

.field private d:Ldct;

.field private e:Ltv/periscope/model/p;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ltv/periscope/android/ui/broadcast/ai;

.field private final l:Landroid/content/Context;

.field private final m:Ljava/lang/String;

.field private final n:Ltv/periscope/android/util/a;

.field private final o:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private final x:Z

.field private volatile y:Z

.field private volatile z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/TextureView;Ltv/periscope/android/player/a;Ltv/periscope/android/ui/broadcast/ai;Ltv/periscope/android/util/a;Lcyn;Z)V
    .locals 9

    .prologue
    .line 319
    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Ltv/periscope/android/ui/broadcast/l;-><init>(Landroid/content/Context;Landroid/view/TextureView;Ltv/periscope/android/player/a;Ltv/periscope/android/ui/broadcast/ai;Ltv/periscope/android/util/a;Lcyn;ZZ)V

    .line 320
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/TextureView;Ltv/periscope/android/player/a;Ltv/periscope/android/ui/broadcast/ai;Ltv/periscope/android/util/a;Lcyn;ZZ)V
    .locals 4

    .prologue
    .line 324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->j:Z

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->p:Ljava/util/List;

    .line 138
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->K:Landroid/os/Handler;

    .line 151
    const/4 v0, 0x0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/l;->O:I

    .line 152
    const/4 v0, 0x0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/l;->P:I

    .line 153
    const/4 v0, 0x0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/l;->Q:I

    .line 154
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->R:Ltv/periscope/android/video/rtmp/i;

    .line 155
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->S:J

    .line 156
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->T:J

    .line 157
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->U:J

    .line 158
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->V:Ltv/periscope/android/video/rtmp/i;

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->X:Z

    .line 163
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->Y:D

    .line 166
    const/4 v0, 0x0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/l;->aa:I

    .line 167
    const/4 v0, 0x0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/l;->ab:I

    .line 169
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->ac:D

    .line 170
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->ad:D

    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->ae:Z

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->af:Z

    .line 176
    new-instance v0, Ltv/periscope/android/ui/broadcast/l$a;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/l$a;-><init>(Ltv/periscope/android/ui/broadcast/l;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->ag:Ltv/periscope/android/ui/broadcast/l$a;

    .line 179
    new-instance v0, Ltv/periscope/android/ui/broadcast/l$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/l$1;-><init>(Ltv/periscope/android/ui/broadcast/l;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->ai:Ljava/lang/Runnable;

    .line 250
    new-instance v0, Ltv/periscope/android/ui/broadcast/l$2;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/l$2;-><init>(Ltv/periscope/android/ui/broadcast/l;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->b:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 325
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->l:Landroid/content/Context;

    .line 326
    new-instance v0, Ltv/periscope/android/ui/broadcast/l$c;

    invoke-direct {v0, p3}, Ltv/periscope/android/ui/broadcast/l$c;-><init>(Ltv/periscope/android/player/a;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    .line 327
    iput-boolean p8, p0, Ltv/periscope/android/ui/broadcast/l;->x:Z

    .line 328
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    .line 329
    if-eqz p2, :cond_3

    .line 330
    invoke-virtual {p2, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 332
    invoke-virtual {p2}, Landroid/view/TextureView;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    invoke-virtual {p2}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/TextureView;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/view/TextureView;->getHeight()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Ltv/periscope/android/ui/broadcast/l;->a(Landroid/graphics/SurfaceTexture;II)V

    .line 339
    :cond_0
    :goto_0
    invoke-static {p1}, Ltv/periscope/android/network/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->m:Ljava/lang/String;

    .line 340
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    .line 341
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/l;->n:Ltv/periscope/android/util/a;

    .line 342
    iput-object p6, p0, Ltv/periscope/android/ui/broadcast/l;->c:Lcyn;

    .line 343
    new-instance v0, Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    invoke-direct {v0, v1, p0}, Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;-><init>(Landroid/os/Handler;Lcom/google/android/exoplayer/upstream/BandwidthMeter$EventListener;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->o:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    .line 344
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ai;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->t:Z

    .line 345
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->Z:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_1

    if-eqz p7, :cond_1

    .line 346
    new-instance v0, Ltv/periscope/android/ui/broadcast/l$3;

    const/4 v1, 0x3

    invoke-direct {v0, p0, p1, v1}, Ltv/periscope/android/ui/broadcast/l$3;-><init>(Ltv/periscope/android/ui/broadcast/l;Landroid/content/Context;I)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->Z:Landroid/view/OrientationEventListener;

    .line 356
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->Z:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->canDetectOrientation()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 357
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->Z:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 362
    :cond_1
    :goto_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_2

    .line 365
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ai;->i()I

    move-result v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v1}, Ltv/periscope/android/ui/broadcast/ai;->h()I

    move-result v1

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v1, v2, v3}, Ltv/periscope/android/ui/broadcast/l;->a(IIIF)V

    .line 367
    :cond_2
    return-void

    .line 337
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->r:Z

    goto :goto_0

    .line 344
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 359
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->Z:Landroid/view/OrientationEventListener;

    goto :goto_2
.end method

.method private A()V
    .locals 2

    .prologue
    .line 503
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setRotation(F)V

    .line 506
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_1

    .line 507
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ai;->g()V

    .line 508
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    .line 510
    :cond_1
    return-void
.end method

.method private B()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 536
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 537
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->K:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 538
    return-void
.end method

.method private C()V
    .locals 3

    .prologue
    .line 577
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getWidth()I

    move-result v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/l;->a(II)V

    .line 579
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->G:Ltv/periscope/android/graphics/g;

    if-nez v0, :cond_0

    .line 580
    new-instance v0, Ltv/periscope/android/graphics/b;

    invoke-direct {v0}, Ltv/periscope/android/graphics/b;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->D:Ltv/periscope/android/graphics/b;

    .line 581
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->I:Landroid/view/Surface;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->E:Landroid/view/Surface;

    .line 582
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->D:Ltv/periscope/android/graphics/b;

    const/4 v1, 0x0

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->E:Landroid/view/Surface;

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/graphics/b;->a(Ltv/periscope/android/graphics/b;Landroid/view/Surface;)Z

    .line 583
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->D:Ltv/periscope/android/graphics/b;

    new-instance v1, Ltv/periscope/android/ui/broadcast/l$4;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/l$4;-><init>(Ltv/periscope/android/ui/broadcast/l;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/graphics/b;->a(Ltv/periscope/android/graphics/b$c;)Z

    .line 597
    new-instance v0, Ltv/periscope/android/graphics/g;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->D:Ltv/periscope/android/graphics/b;

    new-instance v2, Ltv/periscope/android/ui/broadcast/l$5;

    invoke-direct {v2, p0}, Ltv/periscope/android/ui/broadcast/l$5;-><init>(Ltv/periscope/android/ui/broadcast/l;)V

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/graphics/g;-><init>(Ltv/periscope/android/graphics/b;Ltv/periscope/android/graphics/g$b;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->G:Ltv/periscope/android/graphics/g;

    .line 610
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->G:Ltv/periscope/android/graphics/g;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/g;->a()V

    .line 612
    :cond_0
    return-void
.end method

.method private D()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 615
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->G:Ltv/periscope/android/graphics/g;

    if-eqz v0, :cond_2

    .line 616
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_0

    .line 617
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0, v2}, Ltv/periscope/android/ui/broadcast/ai;->a(Landroid/view/Surface;)V

    .line 619
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->G:Ltv/periscope/android/graphics/g;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/g;->b()V

    .line 620
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->D:Ltv/periscope/android/graphics/b;

    new-instance v1, Ltv/periscope/android/ui/broadcast/l$6;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/l$6;-><init>(Ltv/periscope/android/ui/broadcast/l;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/graphics/b;->a(Ltv/periscope/android/graphics/b$c;)Z

    .line 635
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->E:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 636
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->E:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 637
    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->E:Landroid/view/Surface;

    .line 639
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/n;->a()V

    .line 640
    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    .line 641
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->D:Ltv/periscope/android/graphics/b;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/b;->b()V

    .line 642
    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->D:Ltv/periscope/android/graphics/b;

    .line 643
    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->G:Ltv/periscope/android/graphics/g;

    .line 645
    :cond_2
    return-void
.end method

.method private E()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 718
    monitor-enter p0

    .line 719
    :try_start_0
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->T:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 720
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->S:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Ltv/periscope/android/ui/broadcast/l;->T:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->S:J

    .line 721
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->T:J

    .line 723
    :cond_0
    monitor-exit p0

    .line 724
    return-void

    .line 723
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private F()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 728
    monitor-enter p0

    .line 729
    :try_start_0
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->U:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 730
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->U:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    .line 731
    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->S:J

    long-to-double v2, v2

    sub-double/2addr v2, v0

    double-to-long v2, v2

    iput-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->S:J

    .line 732
    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    .line 733
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->V:Ltv/periscope/android/video/rtmp/i;

    invoke-virtual {v2, v0, v1}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 734
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->U:J

    .line 735
    const-string/jumbo v0, "BroadcastPlayer"

    const-string/jumbo v1, "Stall recovered"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    :cond_0
    monitor-exit p0

    .line 738
    return-void

    .line 737
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private G()V
    .locals 1

    .prologue
    .line 742
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->i:Z

    if-nez v0, :cond_1

    .line 743
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->i:Z

    .line 744
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_0

    .line 745
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ai;->d()V

    .line 747
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->K()V

    .line 749
    :cond_1
    return-void
.end method

.method private H()V
    .locals 7

    .prologue
    .line 905
    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->Y:D

    .line 906
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->X:Z

    if-nez v0, :cond_0

    .line 907
    const-wide/16 v2, 0x0

    .line 911
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->y:Z

    if-nez v0, :cond_1

    .line 912
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    iget v4, p0, Ltv/periscope/android/ui/broadcast/l;->aa:I

    iget v5, p0, Ltv/periscope/android/ui/broadcast/l;->ab:I

    iget-boolean v6, p0, Ltv/periscope/android/ui/broadcast/l;->ae:Z

    invoke-static/range {v1 .. v6}, Lczo;->a(Landroid/view/TextureView;DIIZ)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/l;->aj:I

    .line 918
    :goto_0
    return-void

    .line 913
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    if-eqz v0, :cond_2

    .line 914
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setTransform(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 916
    :cond_2
    invoke-static {v2, v3}, Lczo;->a(D)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/l;->aj:I

    goto :goto_0
.end method

.method private I()V
    .locals 4

    .prologue
    .line 1210
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->ah:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->e:Ltv/periscope/model/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->e:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->ai:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1212
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->ah:Z

    .line 1214
    :cond_0
    return-void
.end method

.method private J()V
    .locals 2

    .prologue
    .line 1311
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1312
    return-void
.end method

.method private K()V
    .locals 2

    .prologue
    .line 1316
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1317
    return-void
.end method

.method private L()V
    .locals 2

    .prologue
    .line 1320
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1321
    return-void
.end method

.method private M()V
    .locals 2

    .prologue
    .line 1324
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1325
    return-void
.end method

.method private N()V
    .locals 2

    .prologue
    .line 1328
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1329
    return-void
.end method

.method private O()V
    .locals 2

    .prologue
    .line 1332
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1333
    return-void
.end method

.method private P()V
    .locals 2

    .prologue
    .line 1341
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1342
    return-void
.end method

.method private Q()V
    .locals 2

    .prologue
    .line 1355
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1356
    return-void
.end method

.method private R()V
    .locals 2

    .prologue
    .line 1360
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1361
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/l;Landroid/view/Surface;)Landroid/view/Surface;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/l;->I:Landroid/view/Surface;

    return-object p1
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/l;Ltv/periscope/android/graphics/i;)Ltv/periscope/android/graphics/i;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/l;->F:Ltv/periscope/android/graphics/i;

    return-object p1
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/l;Ltv/periscope/android/graphics/n;)Ltv/periscope/android/graphics/n;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    return-object p1
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/l;)Ltv/periscope/model/p;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->e:Ltv/periscope/model/p;

    return-object v0
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 566
    invoke-static {p1, p2}, Ltv/periscope/android/util/Size;->a(II)Ltv/periscope/android/util/Size;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->A:Ltv/periscope/android/util/Size;

    .line 567
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->z:Z

    .line 569
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->l:Landroid/content/Context;

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 571
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 572
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    invoke-virtual {v1, v0}, Ltv/periscope/android/graphics/n;->b(I)V

    .line 574
    :cond_0
    return-void
.end method

.method private a(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 551
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->r:Z

    if-nez v0, :cond_0

    .line 552
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->r:Z

    .line 553
    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->I:Landroid/view/Surface;

    .line 554
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->ae:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->y:Z

    if-nez v0, :cond_0

    .line 555
    invoke-direct {p0, p2, p3}, Ltv/periscope/android/ui/broadcast/l;->b(II)V

    .line 558
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 1017
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->i:Z

    if-eqz v0, :cond_0

    .line 1026
    :goto_0
    return-void

    .line 1023
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->ag:Ltv/periscope/android/ui/broadcast/l$a;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1024
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->ag:Ltv/periscope/android/ui/broadcast/l$a;

    invoke-static {v0, p1}, Ltv/periscope/android/ui/broadcast/l$a;->a(Ltv/periscope/android/ui/broadcast/l$a;Z)Z

    .line 1025
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->ag:Ltv/periscope/android/ui/broadcast/l$a;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/l;Z)Z
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/l;->af:Z

    return p1
.end method

.method private b(II)V
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 667
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->y:Z

    if-eqz v0, :cond_1

    .line 689
    :cond_0
    :goto_0
    return-void

    .line 671
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    .line 672
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    invoke-virtual {v2}, Landroid/view/TextureView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    .line 677
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/l;->l:Landroid/content/Context;

    invoke-static {v3}, Ltv/periscope/android/util/ad;->c(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 678
    div-float/2addr v0, v2

    int-to-float v2, p1

    mul-float/2addr v0, v2

    .line 679
    int-to-float v2, p2

    div-float/2addr v0, v2

    .line 685
    :goto_1
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 687
    div-int/lit8 v3, p2, 0x2

    int-to-float v3, v3

    div-int/lit8 v4, p1, 0x2

    int-to-float v4, v4

    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 688
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    invoke-virtual {v0, v2}, Landroid/view/TextureView;->setTransform(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 681
    :cond_2
    div-float v0, v2, v0

    int-to-float v2, p2

    mul-float/2addr v0, v2

    .line 682
    int-to-float v2, p1

    div-float/2addr v0, v2

    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_1
.end method

.method private b(J)V
    .locals 3

    .prologue
    .line 816
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->t:Z

    if-eqz v0, :cond_1

    .line 828
    :cond_0
    :goto_0
    return-void

    .line 819
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_0

    .line 822
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->t:Z

    .line 823
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->J()V

    .line 824
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->W:D

    .line 825
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->w()V

    .line 826
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/ai;->a(J)V

    goto :goto_0
.end method

.method private b(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1345
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1346
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1347
    return-void
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/l;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->R()V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1336
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1337
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1338
    return-void
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/l;Z)Z
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/l;->t:Z

    return p1
.end method

.method static synthetic c(Ltv/periscope/android/ui/broadcast/l;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    return-object v0
.end method

.method private c(J)V
    .locals 3

    .prologue
    .line 1350
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1351
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1352
    return-void
.end method

.method static synthetic c(Ltv/periscope/android/ui/broadcast/l;Z)Z
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/l;->z:Z

    return p1
.end method

.method static synthetic d(Ltv/periscope/android/ui/broadcast/l;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->P()V

    return-void
.end method

.method static synthetic e(Ltv/periscope/android/ui/broadcast/l;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->A()V

    return-void
.end method

.method static synthetic f(Ltv/periscope/android/ui/broadcast/l;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->i:Z

    return v0
.end method

.method static synthetic g(Ltv/periscope/android/ui/broadcast/l;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->z()V

    return-void
.end method

.method static synthetic h(Ltv/periscope/android/ui/broadcast/l;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->w:Z

    return v0
.end method

.method static synthetic i(Ltv/periscope/android/ui/broadcast/l;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->l:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic j(Ltv/periscope/android/ui/broadcast/l;)Ltv/periscope/android/util/a;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->n:Ltv/periscope/android/util/a;

    return-object v0
.end method

.method static synthetic k(Ltv/periscope/android/ui/broadcast/l;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->M()V

    return-void
.end method

.method static synthetic l(Ltv/periscope/android/ui/broadcast/l;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->H()V

    return-void
.end method

.method static synthetic m(Ltv/periscope/android/ui/broadcast/l;)Ltv/periscope/android/graphics/i;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->F:Ltv/periscope/android/graphics/i;

    return-object v0
.end method

.method static synthetic n(Ltv/periscope/android/ui/broadcast/l;)Ltv/periscope/android/graphics/n;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    return-object v0
.end method

.method static synthetic o(Ltv/periscope/android/ui/broadcast/l;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->z:Z

    return v0
.end method

.method static synthetic p(Ltv/periscope/android/ui/broadcast/l;)Ltv/periscope/android/util/Size;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->A:Ltv/periscope/android/util/Size;

    return-object v0
.end method

.method static synthetic q(Ltv/periscope/android/ui/broadcast/l;)I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Ltv/periscope/android/ui/broadcast/l;->aa:I

    return v0
.end method

.method static synthetic r(Ltv/periscope/android/ui/broadcast/l;)I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Ltv/periscope/android/ui/broadcast/l;->ab:I

    return v0
.end method

.method static synthetic s(Ltv/periscope/android/ui/broadcast/l;)Landroid/view/Surface;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->I:Landroid/view/Surface;

    return-object v0
.end method

.method private v()V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->J:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->ai:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->ah:Z

    .line 195
    return-void
.end method

.method private w()V
    .locals 1

    .prologue
    .line 281
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->v:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 282
    :goto_0
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/l;->a(I)V

    .line 283
    return-void

    .line 281
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private x()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-nez v0, :cond_0

    .line 295
    :goto_0
    return-void

    .line 294
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/ai;->a(I)V

    goto :goto_0
.end method

.method private y()Lczi$f;
    .locals 5

    .prologue
    .line 384
    iget v0, p0, Ltv/periscope/android/ui/broadcast/l;->f:I

    packed-switch v0, :pswitch_data_0

    .line 402
    :pswitch_0
    new-instance v0, Lczc;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->l:Landroid/content/Context;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->m:Ljava/lang/String;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/l;->g:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    new-instance v4, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;

    invoke-direct {v4}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;-><init>()V

    invoke-direct {v0, v1, v2, v3, v4}, Lczc;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/exoplayer/extractor/Extractor;)V

    :goto_0
    return-object v0

    .line 386
    :pswitch_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "unsupported type ss"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 389
    :pswitch_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "unsupported type dash"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392
    :pswitch_3
    new-instance v0, Lczd;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->l:Landroid/content/Context;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->m:Ljava/lang/String;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/l;->g:Ljava/lang/String;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/l;->o:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    invoke-direct {v0, v1, v2, v3, v4}, Lczd;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/exoplayer/upstream/BandwidthMeter;)V

    goto :goto_0

    .line 395
    :pswitch_4
    new-instance v0, Lczk;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->l:Landroid/content/Context;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->g:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p0}, Lczk;-><init>(Landroid/content/Context;Ljava/lang/String;Lczk$a;)V

    goto :goto_0

    .line 398
    :pswitch_5
    new-instance v0, Lczf;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->l:Landroid/content/Context;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->m:Ljava/lang/String;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/l;->g:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, p0}, Lczf;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lczk$a;)V

    goto :goto_0

    .line 384
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private declared-synchronized z()V
    .locals 4

    .prologue
    .line 409
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-nez v0, :cond_0

    .line 410
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->s:Z

    .line 411
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->y()Lczi$f;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->L:Lczi$f;

    .line 412
    new-instance v0, Ltv/periscope/android/ui/broadcast/ak;

    new-instance v1, Lczi;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->L:Lczi$f;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/l;->d:Ldct;

    invoke-direct {v1, v2, v3}, Lczi;-><init>(Lczi$f;Ldct;)V

    invoke-direct {v0, v1, p0, p0, p0}, Ltv/periscope/android/ui/broadcast/ak;-><init>(Lczi;Lczi$e;Lczi$b;Lczi$a;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    .line 413
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->v:Z

    if-eqz v0, :cond_0

    .line 414
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->x()V

    .line 417
    :cond_0
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->s:Z

    if-eqz v0, :cond_1

    .line 418
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->N:J

    .line 419
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->M:J

    .line 420
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ai;->c()V

    .line 421
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->s:Z

    .line 424
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->I:Landroid/view/Surface;

    if-eqz v0, :cond_3

    .line 425
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->y:Z

    if-eqz v0, :cond_2

    .line 426
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->C()V

    .line 428
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->I:Landroid/view/Surface;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/ai;->a(Landroid/view/Surface;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    :cond_3
    monitor-exit p0

    return-void

    .line 409
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 996
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/l;->a(Z)V

    .line 997
    return-void
.end method

.method public a(D)V
    .locals 3

    .prologue
    .line 897
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->Y:D

    cmpl-double v0, p1, v0

    if-eqz v0, :cond_0

    .line 898
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->X:Z

    .line 899
    iput-wide p1, p0, Ltv/periscope/android/ui/broadcast/l;->Y:D

    .line 901
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->H()V

    .line 902
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 266
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->w:Z

    if-nez v0, :cond_1

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->n:Ltv/periscope/android/util/a;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->l:Landroid/content/Context;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->b:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/util/a;->a(Landroid/content/Context;Landroid/media/AudioManager$OnAudioFocusChangeListener;)Z

    move-result v0

    .line 270
    if-eqz v0, :cond_2

    .line 271
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->v:Z

    .line 272
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcast/ai;->a(I)V

    goto :goto_0

    .line 275
    :cond_2
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->v:Z

    if-nez v0, :cond_0

    .line 276
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->Q()V

    goto :goto_0
.end method

.method public a(IIIF)V
    .locals 2

    .prologue
    .line 850
    iput p2, p0, Ltv/periscope/android/ui/broadcast/l;->ab:I

    .line 851
    iput p1, p0, Ltv/periscope/android/ui/broadcast/l;->aa:I

    .line 852
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->Y:D

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/l;->a(D)V

    .line 853
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->O()V

    .line 854
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 1109
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->h:Z

    .line 1110
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_0

    .line 1111
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/ai;->a(J)V

    .line 1114
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    if-eqz v0, :cond_1

    .line 1115
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->G:Ltv/periscope/android/graphics/g;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/g;->a()V

    .line 1116
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/n;->b()V

    .line 1118
    :cond_1
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 693
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->t:Z

    if-eqz v0, :cond_1

    .line 706
    :cond_0
    :goto_0
    return-void

    .line 696
    :cond_1
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->u:Z

    if-nez v0, :cond_0

    .line 699
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->u:Z

    .line 700
    iput-wide p1, p0, Ltv/periscope/android/ui/broadcast/l;->B:J

    .line 701
    iput-boolean p3, p0, Ltv/periscope/android/ui/broadcast/l;->w:Z

    .line 702
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->L()V

    .line 703
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->a:Z

    if-eqz v0, :cond_0

    .line 704
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/l;->b(J)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 833
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->a:Z

    if-nez v0, :cond_0

    .line 834
    iget v0, p0, Ltv/periscope/android/ui/broadcast/l;->P:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/l;->P:I

    .line 835
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->N:J

    .line 837
    :cond_0
    instance-of v0, p1, Lcom/google/android/exoplayer/ExoPlaybackException;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/exoplayer/BehindLiveWindowException;

    if-eqz v0, :cond_1

    .line 839
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/l;->b()V

    .line 845
    :goto_0
    return-void

    .line 842
    :cond_1
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/l;->b(Ljava/lang/Exception;)V

    .line 843
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->s:Z

    .line 844
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->I()V

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/metadata/id3/Id3Frame;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 875
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/metadata/id3/Id3Frame;

    .line 876
    instance-of v2, v0, Lcom/google/android/exoplayer/metadata/id3/TextInformationFrame;

    if-eqz v2, :cond_0

    .line 877
    check-cast v0, Lcom/google/android/exoplayer/metadata/id3/TextInformationFrame;

    .line 878
    const-string/jumbo v2, "TIT3"

    iget-object v3, v0, Lcom/google/android/exoplayer/metadata/id3/TextInformationFrame;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 879
    iget-object v0, v0, Lcom/google/android/exoplayer/metadata/id3/TextInformationFrame;->description:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 880
    iget-wide v4, p0, Ltv/periscope/android/ui/broadcast/l;->W:D

    cmpl-double v0, v4, v2

    if-eqz v0, :cond_0

    .line 881
    iput-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->W:D

    .line 882
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/l;->h()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Ltv/periscope/android/ui/broadcast/l;->c(J)V

    goto :goto_0

    .line 884
    :cond_1
    const-string/jumbo v2, "TKEY"

    iget-object v3, v0, Lcom/google/android/exoplayer/metadata/id3/TextInformationFrame;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 885
    iget-object v2, v0, Lcom/google/android/exoplayer/metadata/id3/TextInformationFrame;->description:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 886
    iget-object v0, v0, Lcom/google/android/exoplayer/metadata/id3/TextInformationFrame;->description:Ljava/lang/String;

    invoke-static {v0}, Lczo;->a(Ljava/lang/String;)D

    move-result-wide v2

    .line 888
    invoke-virtual {p0, v2, v3}, Ltv/periscope/android/ui/broadcast/l;->a(D)V

    goto :goto_0

    .line 893
    :cond_2
    return-void
.end method

.method a(Ljava/util/List;Ljava/net/URL;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/v;",
            ">;",
            "Ljava/net/URL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 374
    if-eqz p1, :cond_1

    .line 375
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/v;

    .line 376
    if-eqz v0, :cond_0

    .line 377
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->d:Ldct;

    invoke-virtual {v0}, Ltv/periscope/model/v;->a()Ljava/net/HttpCookie;

    move-result-object v0

    invoke-virtual {v2, v0, p2}, Ldct;->a(Ljava/net/HttpCookie;Ljava/net/URL;)V

    goto :goto_0

    .line 381
    :cond_1
    return-void
.end method

.method public a(Ljava/util/Map;J)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 931
    if-nez p1, :cond_1

    .line 992
    :cond_0
    :goto_0
    return-void

    .line 935
    :cond_1
    const-string/jumbo v0, "ntp"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 938
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->af:Z

    .line 939
    const-string/jumbo v0, "ntp"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 940
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/l;->l()J

    move-result-wide v4

    .line 941
    sub-long v0, p2, v4

    long-to-double v0, v0

    .line 942
    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double v6, v0, v6

    .line 943
    sub-double v0, v2, v6

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->W:D

    .line 945
    invoke-static {}, Ltv/periscope/android/video/rtmp/d;->a()Ltv/periscope/android/video/rtmp/d;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/d;->d()J

    move-result-wide v0

    long-to-double v8, v0

    .line 946
    const-wide v0, 0x408f400000000000L    # 1000.0

    div-double v0, v8, v0

    const-wide v10, 0x41e0754fd0000000L    # 2.2089888E9

    add-double/2addr v0, v10

    .line 947
    iget-wide v10, p0, Ltv/periscope/android/ui/broadcast/l;->W:D

    sub-double v10, v0, v10

    .line 951
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-lez v0, :cond_2

    .line 952
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->R:Ltv/periscope/android/video/rtmp/i;

    invoke-virtual {v0, v10, v11}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 955
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->L:Lczi$f;

    instance-of v0, v0, Lczk;

    if-eqz v0, :cond_7

    .line 956
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->L:Lczi$f;

    check-cast v0, Lczk;

    .line 957
    invoke-virtual {v0, v4, v5}, Lczk;->a(J)V

    .line 963
    :cond_3
    :goto_1
    new-instance v0, Ljava/text/DecimalFormat;

    const-string/jumbo v1, "0.###"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 964
    const-string/jumbo v1, "BroadcastPlayer"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Latency: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v0, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " queued: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " ntp "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->W:D

    const-wide v2, 0x41e0754fd0000000L    # 2.2089888E9

    sub-double/2addr v0, v2

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    .line 967
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 968
    const-string/jumbo v0, "BroadcastPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Capture: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " now: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    double-to-long v6, v8

    invoke-direct {v2, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    const-string/jumbo v0, "width"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "height"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 971
    const-string/jumbo v0, "width"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 972
    const-string/jumbo v0, "height"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 973
    iget-wide v6, p0, Ltv/periscope/android/ui/broadcast/l;->ad:D

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-nez v6, :cond_4

    iget-wide v6, p0, Ltv/periscope/android/ui/broadcast/l;->ac:D

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-nez v6, :cond_4

    .line 974
    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->ad:D

    .line 975
    iput-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->ac:D

    .line 977
    :cond_4
    iget-wide v6, p0, Ltv/periscope/android/ui/broadcast/l;->ac:D

    cmpl-double v6, v2, v6

    if-nez v6, :cond_5

    iget-wide v6, p0, Ltv/periscope/android/ui/broadcast/l;->ad:D

    cmpl-double v6, v0, v6

    if-eqz v6, :cond_6

    .line 978
    :cond_5
    const-string/jumbo v6, "BroadcastPlayer"

    const-string/jumbo v7, "Source Change detected"

    invoke-static {v6, v7}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->ad:D

    .line 980
    iput-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->ac:D

    .line 985
    :cond_6
    const-string/jumbo v0, "rotation"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 986
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->X:Z

    .line 987
    const-string/jumbo v0, "rotation"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 988
    const-string/jumbo v2, "BroadcastPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Broadcast "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, " degrees"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    const-wide/16 v2, 0x0

    sub-long v4, p2, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 990
    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/l;->K:Landroid/os/Handler;

    new-instance v5, Ltv/periscope/android/ui/broadcast/l$b;

    invoke-direct {v5, p0, v0, v1}, Ltv/periscope/android/ui/broadcast/l$b;-><init>(Ltv/periscope/android/ui/broadcast/l;D)V

    invoke-virtual {v4, v5, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 958
    :cond_7
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->L:Lczi$f;

    instance-of v0, v0, Lczf;

    if-eqz v0, :cond_3

    .line 959
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->L:Lczi$f;

    check-cast v0, Lczf;

    .line 960
    invoke-virtual {v0, v4, v5}, Lczf;->a(J)V

    goto/16 :goto_1
.end method

.method public a(ZI)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 754
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-nez v0, :cond_0

    .line 803
    :goto_0
    return-void

    .line 757
    :cond_0
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 785
    :pswitch_0
    monitor-enter p0

    .line 786
    :try_start_0
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->j:Z

    if-eqz v0, :cond_3

    .line 787
    const-string/jumbo v0, "RTMP"

    const-string/jumbo v1, "Stall when end pending"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->G()V

    .line 789
    monitor-exit p0

    goto :goto_0

    .line 796
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 759
    :pswitch_1
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->F()V

    .line 760
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->a:Z

    if-nez v0, :cond_1

    .line 762
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->N:J

    .line 763
    iget v0, p0, Ltv/periscope/android/ui/broadcast/l;->O:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/l;->O:I

    .line 765
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->a:Z

    .line 766
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/l;->b(Z)V

    .line 767
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->v()V

    .line 768
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->u:Z

    if-eqz v0, :cond_2

    .line 769
    iput-boolean v2, p0, Ltv/periscope/android/ui/broadcast/l;->u:Z

    .line 770
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->B:J

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/l;->b(J)V

    goto :goto_0

    .line 772
    :cond_2
    const-string/jumbo v0, "BroadcastPlayer"

    const-string/jumbo v1, "playback was not requested"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 778
    :pswitch_2
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->E()V

    .line 779
    iput-boolean v2, p0, Ltv/periscope/android/ui/broadcast/l;->t:Z

    .line 780
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->K()V

    goto :goto_0

    .line 791
    :cond_3
    :try_start_1
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->U:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->T:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_4

    .line 792
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->U:J

    .line 793
    iget v0, p0, Ltv/periscope/android/ui/broadcast/l;->Q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/l;->Q:I

    .line 794
    const-string/jumbo v0, "BroadcastPlayer"

    const-string/jumbo v1, "Stall"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 797
    const-string/jumbo v0, "RTMP"

    const-string/jumbo v1, "buffering"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->P()V

    .line 799
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->I()V

    goto/16 :goto_0

    .line 757
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    invoke-virtual {v0, p2}, Ltv/periscope/android/graphics/n;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 663
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ltv/periscope/model/p;Ltv/periscope/android/video/StreamMode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/model/p;",
            "Ltv/periscope/android/video/StreamMode;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/v;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1126
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/l;->e:Ltv/periscope/model/p;

    .line 1127
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->x:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->e:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->y:Z

    .line 1129
    sget-object v0, Ltv/periscope/android/ui/broadcast/l$7;->a:[I

    invoke-virtual {p2}, Ltv/periscope/android/video/StreamMode;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 1143
    :goto_1
    :pswitch_0
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ltv/periscope/model/p;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1155
    :cond_0
    invoke-static {p4}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1156
    const-string/jumbo v0, "BroadcastPlayer"

    const-string/jumbo v2, "Using RTMP url."

    invoke-static {v0, v2}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1157
    const/4 v0, 0x4

    iput v0, p0, Ltv/periscope/android/ui/broadcast/l;->f:I

    .line 1158
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/l;->g:Ljava/lang/String;

    .line 1180
    :goto_2
    :try_start_0
    new-instance v0, Ldct;

    invoke-direct {v0}, Ldct;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->d:Ldct;

    .line 1181
    new-instance v0, Ljava/net/URL;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->g:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p7, v0}, Ltv/periscope/android/ui/broadcast/l;->a(Ljava/util/List;Ljava/net/URL;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1185
    :goto_3
    iput-boolean v1, p0, Ltv/periscope/android/ui/broadcast/l;->q:Z

    .line 1187
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->r:Z

    if-eqz v0, :cond_6

    .line 1188
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->z()V

    :goto_4
    move v2, v1

    .line 1192
    :goto_5
    return v2

    :cond_1
    move v0, v2

    .line 1127
    goto :goto_0

    .line 1133
    :pswitch_1
    const/4 p4, 0x0

    .line 1134
    goto :goto_1

    .line 1159
    :cond_2
    invoke-static {p5}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1160
    const-string/jumbo v0, "BroadcastPlayer"

    const-string/jumbo v2, "Using HLS url."

    invoke-static {v0, v2}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1161
    iput v4, p0, Ltv/periscope/android/ui/broadcast/l;->f:I

    .line 1162
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/l;->g:Ljava/lang/String;

    goto :goto_2

    .line 1163
    :cond_3
    invoke-static {p6}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1164
    invoke-static {p6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1165
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, ".m3u8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1166
    const-string/jumbo v0, "BroadcastPlayer"

    const-string/jumbo v2, "Found .m3u8 extension, using HLS."

    invoke-static {v0, v2}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1167
    iput v4, p0, Ltv/periscope/android/ui/broadcast/l;->f:I

    .line 1172
    :goto_6
    iput-object p6, p0, Ltv/periscope/android/ui/broadcast/l;->g:Ljava/lang/String;

    goto :goto_2

    .line 1169
    :cond_4
    const-string/jumbo v0, "BroadcastPlayer"

    const-string/jumbo v2, "Using other url (probably mp4)"

    invoke-static {v0, v2}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    const/4 v0, 0x2

    iput v0, p0, Ltv/periscope/android/ui/broadcast/l;->f:I

    goto :goto_6

    .line 1175
    :cond_5
    const-string/jumbo v0, "BroadcastPlayer"

    const-string/jumbo v1, "Unplayable url"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 1190
    :cond_6
    const-string/jumbo v0, "BroadcastPlayer"

    const-string/jumbo v2, "Texture not ready, we\'ll set up the player once it becomes available."

    invoke-static {v0, v2}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1182
    :catch_0
    move-exception v0

    goto :goto_3

    .line 1129
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 1012
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/l;->a(Z)V

    .line 1013
    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/text/Cue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 871
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 859
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_0

    .line 860
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/ai;->a(Z)V

    .line 862
    :cond_0
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->T:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 863
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->T:J

    .line 865
    :cond_1
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->N()V

    .line 866
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 1001
    monitor-enter p0

    .line 1002
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->j:Z

    .line 1003
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->U:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->t:Z

    if-nez v0, :cond_1

    .line 1004
    :cond_0
    const-string/jumbo v0, "RTMP"

    const-string/jumbo v1, "End signalled when not playing"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->G()V

    .line 1007
    :cond_1
    monitor-exit p0

    .line 1008
    return-void

    .line 1007
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public e()Ljava/util/HashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 434
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 435
    iget v0, p0, Ltv/periscope/android/ui/broadcast/l;->f:I

    const/4 v2, 0x4

    if-eq v0, v2, :cond_0

    iget v0, p0, Ltv/periscope/android/ui/broadcast/l;->f:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_6

    .line 436
    :cond_0
    const/4 v0, 0x0

    .line 437
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->L:Lczi$f;

    instance-of v2, v2, Lczk;

    if-eqz v2, :cond_5

    .line 438
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->L:Lczi$f;

    check-cast v0, Lczk;

    .line 439
    invoke-virtual {v0}, Lczk;->d()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 440
    invoke-virtual {v0}, Lczk;->e()I

    move-result v0

    .line 447
    :cond_1
    :goto_0
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->R:Ltv/periscope/android/video/rtmp/i;

    const-string/jumbo v3, "Latency"

    invoke-virtual {v2, v1, v3}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 449
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->E()V

    .line 450
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->F()V

    .line 451
    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->S:J

    long-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double v4, v2, v4

    .line 452
    const-string/jumbo v2, "DurationWatched"

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    const-string/jumbo v2, "StallCount"

    iget v3, p0, Ltv/periscope/android/ui/broadcast/l;->Q:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->V:Ltv/periscope/android/video/rtmp/i;

    const-string/jumbo v3, "StallDuration"

    invoke-virtual {v2, v1, v3}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 455
    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->S:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_3

    .line 456
    iget v2, p0, Ltv/periscope/android/ui/broadcast/l;->Q:I

    int-to-double v2, v2

    .line 457
    iget v6, p0, Ltv/periscope/android/ui/broadcast/l;->Q:I

    const/4 v7, 0x1

    if-le v6, v7, :cond_2

    .line 458
    iget v2, p0, Ltv/periscope/android/ui/broadcast/l;->Q:I

    int-to-double v2, v2

    const-wide/high16 v6, 0x404e000000000000L    # 60.0

    div-double v6, v4, v6

    div-double/2addr v2, v6

    .line 460
    :cond_2
    const-string/jumbo v6, "StallsPerMinute"

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    :cond_3
    if-lez v0, :cond_4

    .line 464
    const-string/jumbo v2, "KeyframeInterval"

    int-to-double v6, v0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    :cond_4
    :goto_1
    const-string/jumbo v0, "BroadcastPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Playback stats: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    return-object v1

    .line 441
    :cond_5
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->L:Lczi$f;

    instance-of v2, v2, Lczf;

    if-eqz v2, :cond_1

    .line 442
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->L:Lczi$f;

    check-cast v0, Lczf;

    .line 443
    invoke-virtual {v0}, Lczf;->c()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 444
    invoke-virtual {v0}, Lczf;->b()I

    move-result v0

    goto/16 :goto_0

    .line 466
    :cond_6
    iget v0, p0, Ltv/periscope/android/ui/broadcast/l;->f:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_4

    .line 467
    const-string/jumbo v0, "BroadcastPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "HLS Stalls: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Ltv/periscope/android/ui/broadcast/l;->Q:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string/jumbo v0, "observed_bitrate"

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->p:Ljava/util/List;

    invoke-static {v2}, Ltv/periscope/android/util/w;->a(Ljava/util/List;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    const-string/jumbo v0, "observed_bitrate_stddev"

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->p:Ljava/util/List;

    invoke-static {v2}, Ltv/periscope/android/util/w;->b(Ljava/util/List;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    const-string/jumbo v0, "hls_stalls"

    iget v2, p0, Ltv/periscope/android/ui/broadcast/l;->Q:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    const-string/jumbo v0, "hls_start_failures"

    iget v2, p0, Ltv/periscope/android/ui/broadcast/l;->P:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    const-string/jumbo v0, "hls_start_successes"

    iget v2, p0, Ltv/periscope/android/ui/broadcast/l;->O:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->M:J

    cmp-long v0, v2, v6

    if-lez v0, :cond_4

    .line 474
    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->N:J

    cmp-long v0, v2, v6

    if-nez v0, :cond_7

    .line 475
    const-string/jumbo v0, "hls_abandon_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Ltv/periscope/android/ui/broadcast/l;->M:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 476
    :cond_7
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->a:Z

    if-eqz v0, :cond_8

    .line 477
    const-string/jumbo v0, "hls_start_success_time"

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->N:J

    iget-wide v4, p0, Ltv/periscope/android/ui/broadcast/l;->M:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 479
    :cond_8
    const-string/jumbo v0, "hls_start_failure_time"

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/l;->N:J

    iget-wide v4, p0, Ltv/periscope/android/ui/broadcast/l;->M:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1
.end method

.method public f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 514
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->i:Z

    .line 515
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->A()V

    .line 516
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->D()V

    .line 518
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    .line 519
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 520
    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->C:Landroid/view/TextureView;

    .line 522
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->I:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 523
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->I:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 524
    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->I:Landroid/view/Surface;

    .line 526
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->Z:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_2

    .line 527
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->Z:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 528
    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->Z:Landroid/view/OrientationEventListener;

    .line 530
    :cond_2
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->B()V

    .line 531
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->v()V

    .line 532
    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->L:Lczi$f;

    .line 533
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 713
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->t:Z

    return v0
.end method

.method public h()J
    .locals 4

    .prologue
    .line 806
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->W:D

    const-wide v2, 0x41e0754fd0000000L    # 2.2089888E9

    sub-double/2addr v0, v2

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    return-wide v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 810
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ai;->e()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    .line 811
    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ai;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 810
    :goto_0
    return v0

    .line 811
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 926
    iget v0, p0, Ltv/periscope/android/ui/broadcast/l;->aj:I

    return v0
.end method

.method public k()J
    .locals 4

    .prologue
    .line 1029
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->W:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1030
    invoke-static {}, Ldaf;->b()J

    move-result-wide v0

    .line 1033
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/l;->h()J

    move-result-wide v0

    goto :goto_0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 1037
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_0

    .line 1038
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ai;->a()J

    move-result-wide v0

    .line 1040
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public m()J
    .locals 2

    .prologue
    .line 1045
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_0

    .line 1046
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ai;->b()J

    move-result-wide v0

    .line 1048
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public n()V
    .locals 3

    .prologue
    .line 1054
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/l;->d()V

    .line 1055
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->n:Ltv/periscope/android/util/a;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->l:Landroid/content/Context;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->b:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/util/a;->b(Landroid/content/Context;Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    .line 1056
    return-void
.end method

.method public o()V
    .locals 3

    .prologue
    .line 1069
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->h:Z

    .line 1070
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->n:Ltv/periscope/android/util/a;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/l;->l:Landroid/content/Context;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/l;->b:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/util/a;->b(Landroid/content/Context;Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    .line 1071
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_0

    .line 1072
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/ai;->a(Z)V

    .line 1074
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    if-eqz v0, :cond_1

    .line 1075
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->G:Ltv/periscope/android/graphics/g;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/g;->b()V

    .line 1076
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/n;->c()V

    .line 1078
    :cond_1
    return-void
.end method

.method public onBandwidthSample(IJJ)V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->p:Ljava/util/List;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 543
    invoke-direct {p0, p1, p2, p3}, Ltv/periscope/android/ui/broadcast/l;->a(Landroid/graphics/SurfaceTexture;II)V

    .line 544
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->q:Z

    if-eqz v0, :cond_0

    .line 545
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->z()V

    .line 547
    :cond_0
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 649
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/l;->D()V

    .line 650
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 562
    invoke-direct {p0, p2, p3}, Ltv/periscope/android/ui/broadcast/l;->a(II)V

    .line 563
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 656
    return-void
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 1081
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ai;->e()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 1085
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->af:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    .line 1087
    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ai;->e()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    .line 1088
    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ai;->e()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1085
    :goto_0
    return v0

    .line 1088
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 1092
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->h:Z

    return v0
.end method

.method public s()V
    .locals 2

    .prologue
    .line 1097
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/l;->h:Z

    .line 1098
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    if-eqz v0, :cond_0

    .line 1099
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->k:Ltv/periscope/android/ui/broadcast/ai;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/ai;->a(Z)V

    .line 1101
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    if-eqz v0, :cond_1

    .line 1102
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->G:Ltv/periscope/android/graphics/g;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/g;->a()V

    .line 1103
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/l;->H:Ltv/periscope/android/graphics/n;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/n;->b()V

    .line 1105
    :cond_1
    return-void
.end method

.method public t()I
    .locals 1

    .prologue
    .line 1121
    iget v0, p0, Ltv/periscope/android/ui/broadcast/l;->f:I

    return v0
.end method

.method public u()D
    .locals 2

    .prologue
    .line 1204
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/l;->Y:D

    return-wide v0
.end method
