.class public Ltv/periscope/android/ui/broadcast/i$h;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"

# interfaces
.implements Lcom/google/android/gms/maps/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$i;",
        ">;",
        "Lcom/google/android/gms/maps/e;"
    }
.end annotation


# instance fields
.field public final b:Ltv/periscope/android/view/q;

.field public final c:Ltv/periscope/android/ui/view/LocalTimeView;

.field private d:Lcom/google/android/gms/maps/c;

.field private e:Lcom/google/android/gms/maps/model/LatLng;


# direct methods
.method private constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 79
    new-instance v1, Ltv/periscope/android/view/q;

    sget v0, Ltv/periscope/android/library/f$g;->map:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapView;

    invoke-direct {v1, v0}, Ltv/periscope/android/view/q;-><init>(Lcom/google/android/gms/maps/MapView;)V

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/i$h;->b:Ltv/periscope/android/view/q;

    .line 80
    sget v0, Ltv/periscope/android/library/f$g;->broadcast_local_time:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/view/LocalTimeView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->c:Ltv/periscope/android/ui/view/LocalTimeView;

    .line 81
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/i$h;->a()V

    .line 82
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$h;
    .locals 3

    .prologue
    .line 67
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_info_map:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 69
    new-instance v1, Ltv/periscope/android/ui/broadcast/i$h;

    invoke-direct {v1, v0, p2}, Ltv/periscope/android/ui/broadcast/i$h;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    return-object v1
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->b:Ltv/periscope/android/view/q;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->b:Ltv/periscope/android/view/q;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/q;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->b:Ltv/periscope/android/view/q;

    invoke-virtual {v0}, Ltv/periscope/android/view/q;->a()V

    .line 108
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->b:Ltv/periscope/android/view/q;

    invoke-virtual {v0, p0}, Ltv/periscope/android/view/q;->a(Lcom/google/android/gms/maps/e;)V

    .line 118
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->b:Ltv/periscope/android/view/q;

    invoke-virtual {v0}, Ltv/periscope/android/view/q;->e()Lcom/google/android/gms/maps/MapView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/MapView;->setClickable(Z)V

    .line 120
    :cond_0
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->b:Ltv/periscope/android/view/q;

    invoke-virtual {v0}, Ltv/periscope/android/view/q;->e()Lcom/google/android/gms/maps/MapView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/MapView;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->b:Ltv/periscope/android/view/q;

    invoke-virtual {v0}, Ltv/periscope/android/view/q;->e()Lcom/google/android/gms/maps/MapView;

    move-result-object v0

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/MapView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/high16 v2, 0x3f000000    # 0.5f

    .line 123
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/i$h;->e:Lcom/google/android/gms/maps/model/LatLng;

    .line 124
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->d:Lcom/google/android/gms/maps/c;

    if-eqz v0, :cond_0

    .line 125
    if-eqz p1, :cond_1

    .line 127
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->d:Lcom/google/android/gms/maps/c;

    invoke-static {p1}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;)V

    .line 128
    new-instance v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    .line 129
    sget v1, Ltv/periscope/android/library/f$f;->ps__mappin_noheading:I

    invoke-static {v1}, Lcom/google/android/gms/maps/model/b;->a(I)Lcom/google/android/gms/maps/model/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 130
    invoke-virtual {v0, v2, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 131
    invoke-virtual {v0, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->b(Z)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 132
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$h;->d:Lcom/google/android/gms/maps/c;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/d;

    .line 135
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->d:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/maps/c;->a(I)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->d:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->a()V

    .line 138
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->d:Lcom/google/android/gms/maps/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/maps/c;)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/maps/d;->a(Landroid/content/Context;)I

    .line 87
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/i$h;->d:Lcom/google/android/gms/maps/c;

    .line 88
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->d:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->b()Lcom/google/android/gms/maps/h;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/h;->e(Z)V

    .line 89
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->d:Lcom/google/android/gms/maps/c;

    new-instance v1, Ltv/periscope/android/ui/broadcast/i$h$1;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/i$h$1;-><init>(Ltv/periscope/android/ui/broadcast/i$h;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/c$f;)V

    .line 96
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$h;->e:Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/i$h;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 97
    return-void
.end method

.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$i;)V
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$i;->b()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/i$h;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 146
    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 64
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$i;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/i$h;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$i;)V

    return-void
.end method
