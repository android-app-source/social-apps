.class public Ltv/periscope/android/ui/broadcast/i$m;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "m"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field public final b:Landroid/widget/ImageView;

.field final c:Ltv/periscope/android/view/UsernameBadgeView;

.field public final d:Landroid/widget/TextView;

.field public final e:Ltv/periscope/android/ui/love/HeartView;

.field public f:Ltv/periscope/model/aa;

.field public g:Ljava/lang/String;

.field h:Lcyw;

.field private final i:Ldae;


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;Ldae;)V
    .locals 1

    .prologue
    .line 404
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 405
    sget v0, Ltv/periscope/android/library/f$g;->profile_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$m;->b:Landroid/widget/ImageView;

    .line 406
    sget v0, Ltv/periscope/android/library/f$g;->name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/UsernameBadgeView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$m;->c:Ltv/periscope/android/view/UsernameBadgeView;

    .line 407
    sget v0, Ltv/periscope/android/library/f$g;->heart_line:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$m;->d:Landroid/widget/TextView;

    .line 408
    sget v0, Ltv/periscope/android/library/f$g;->baby_heart:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/love/HeartView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$m;->e:Ltv/periscope/android/ui/love/HeartView;

    .line 409
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 410
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/i$m;->i:Ldae;

    .line 411
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;Ldae;)Ltv/periscope/android/ui/broadcast/i$m;
    .locals 3

    .prologue
    .line 387
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_info_viewer:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 389
    new-instance v1, Ltv/periscope/android/ui/broadcast/i$m;

    invoke-direct {v1, v0, p2, p3}, Ltv/periscope/android/ui/broadcast/i$m;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;Ldae;)V

    return-object v1
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;)V
    .locals 13

    .prologue
    const/16 v10, 0x8

    const/4 v12, 0x0

    .line 415
    iget-object v0, p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->b:Ljava/lang/String;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$m;->g:Ljava/lang/String;

    .line 417
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->b()Lcyw;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$m;->h:Lcyw;

    .line 418
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$m;->h:Lcyw;

    iget-object v1, p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 419
    if-nez v0, :cond_0

    .line 420
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Viewer isn\'t in cache"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lf;->a(Ljava/lang/Throwable;)V

    .line 421
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$m;->c:Ltv/periscope/android/view/UsernameBadgeView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/UsernameBadgeView;->setText(Ljava/lang/String;)V

    .line 476
    :goto_0
    return-void

    .line 426
    :cond_0
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$m;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 427
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 428
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$m;->h:Lcyw;

    iget-object v3, p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->a:Ljava/lang/String;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/i$m;->g:Ljava/lang/String;

    iget-boolean v6, p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->c:Z

    invoke-interface {v2, v3, v5, v6}, Lcyw;->a(Ljava/lang/String;Ljava/lang/String;Z)Ltv/periscope/model/aa;

    move-result-object v5

    .line 430
    iput-object v5, p0, Ltv/periscope/android/ui/broadcast/i$m;->f:Ltv/periscope/model/aa;

    .line 434
    invoke-virtual {v0}, Ltv/periscope/android/api/PsUser;->getParticipantIndex()J

    move-result-wide v2

    .line 435
    if-eqz v5, :cond_4

    .line 436
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$m;->b:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 437
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$m;->b:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 439
    :cond_1
    iget-boolean v2, p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->c:Z

    if-nez v2, :cond_2

    .line 440
    iget-wide v2, v5, Ltv/periscope/model/aa;->a:J

    .line 441
    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/i$m;->b:Landroid/widget/ImageView;

    invoke-static {v4, v2, v3}, Ltv/periscope/android/util/aa;->b(Landroid/content/res/Resources;J)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 446
    :goto_1
    iget-wide v6, v5, Ltv/periscope/model/aa;->b:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_3

    .line 447
    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/i$m;->d:Landroid/widget/TextView;

    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 448
    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/i$m;->d:Landroid/widget/TextView;

    sget v7, Ltv/periscope/android/library/f$l;->ps__num_hearts:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    iget-wide v10, v5, Ltv/periscope/model/aa;->b:J

    .line 449
    invoke-static {v4, v10, v11, v12}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v12

    .line 448
    invoke-virtual {v4, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450
    invoke-static {v4, v2, v3}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v5

    .line 451
    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/i$m;->e:Ltv/periscope/android/ui/love/HeartView;

    sget v7, Ltv/periscope/android/library/f$f;->ps__ic_heart_profile_border:I

    sget v8, Ltv/periscope/android/library/f$f;->ps__ic_heart_profile:I

    invoke-virtual {v6, v5, v7, v8}, Ltv/periscope/android/ui/love/HeartView;->a(III)V

    .line 453
    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/i$m;->e:Ltv/periscope/android/ui/love/HeartView;

    invoke-virtual {v5, v12}, Ltv/periscope/android/ui/love/HeartView;->setVisibility(I)V

    move-wide v6, v2

    .line 462
    :goto_2
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$m;->h:Lcyw;

    iget-object v3, v0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    iget-object v5, p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;->d:Ljava/lang/String;

    invoke-interface {v2, v3, v5}, Lcyw;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 463
    if-eqz v2, :cond_5

    .line 464
    invoke-static {v4, v6, v7}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v2

    .line 465
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/i$m;->c:Ltv/periscope/android/view/UsernameBadgeView;

    invoke-virtual {v3, v2}, Ltv/periscope/android/view/UsernameBadgeView;->setSuperfansIcon(I)V

    .line 470
    :goto_3
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$m;->c:Ltv/periscope/android/view/UsernameBadgeView;

    invoke-virtual {v2, v12, v12}, Ltv/periscope/android/view/UsernameBadgeView;->a(ZZ)V

    .line 472
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$m;->c:Ltv/periscope/android/view/UsernameBadgeView;

    iget-object v3, v0, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ltv/periscope/android/view/UsernameBadgeView;->setText(Ljava/lang/String;)V

    .line 474
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$m;->i:Ldae;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/i$m;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Ltv/periscope/android/api/PsUser;->getProfileUrlSmall()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    invoke-static/range {v1 .. v7}, Ltv/periscope/android/util/b;->a(Landroid/content/Context;Ldae;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 443
    :cond_2
    const-wide/16 v2, -0x1

    .line 444
    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/i$m;->b:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->clearColorFilter()V

    goto :goto_1

    .line 455
    :cond_3
    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/i$m;->e:Ltv/periscope/android/ui/love/HeartView;

    invoke-virtual {v5, v10}, Ltv/periscope/android/ui/love/HeartView;->setVisibility(I)V

    .line 456
    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/i$m;->d:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    move-wide v6, v2

    goto :goto_2

    .line 459
    :cond_4
    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/i$m;->d:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    move-wide v6, v2

    goto :goto_2

    .line 467
    :cond_5
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$m;->c:Ltv/periscope/android/view/UsernameBadgeView;

    invoke-virtual {v2}, Ltv/periscope/android/view/UsernameBadgeView;->a()V

    goto :goto_3
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 382
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/i$m;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$o;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 480
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$m;->a:Ltv/periscope/android/ui/broadcast/h;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$m;->g:Ljava/lang/String;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/h;->f(Ljava/lang/String;)V

    .line 481
    return-void
.end method
