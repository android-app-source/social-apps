.class Ltv/periscope/android/ui/broadcast/af$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/af;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/af;)V
    .locals 0

    .prologue
    .line 2712
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af$a;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSystemUiVisibilityChange(I)V
    .locals 6

    .prologue
    .line 2715
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$a;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->i:Ltv/periscope/android/view/RootDragLayout;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$a;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, v1, Ltv/periscope/android/ui/broadcast/af;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$a;->a:Ltv/periscope/android/ui/broadcast/af;

    .line 2716
    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/android/ui/broadcast/au;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/au;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2717
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$a;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$a;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, v1, Ltv/periscope/android/ui/broadcast/af;->t:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2718
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$a;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->c:Landroid/os/Handler;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$a;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, v1, Ltv/periscope/android/ui/broadcast/af;->t:Ljava/lang/Runnable;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x5

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2720
    :cond_0
    return-void
.end method
