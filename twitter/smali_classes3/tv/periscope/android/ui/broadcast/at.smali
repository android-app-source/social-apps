.class Ltv/periscope/android/ui/broadcast/at;
.super Ltv/periscope/android/ui/broadcast/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/at$c;,
        Ltv/periscope/android/ui/broadcast/at$d;,
        Ltv/periscope/android/ui/broadcast/at$a;,
        Ltv/periscope/android/ui/broadcast/at$b;
    }
.end annotation


# instance fields
.field private final d:Ltv/periscope/android/view/aj;

.field private final e:Ltv/periscope/android/ui/user/g;

.field private final f:Lcyw;

.field private final g:Lcyn;

.field private final h:Lcrz;

.field private final i:Ltv/periscope/android/ui/broadcast/c;


# direct methods
.method constructor <init>(Ltv/periscope/android/view/b;Lcrz;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;Ltv/periscope/android/ui/broadcast/c;Lcyw;Lcyn;Ldae;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p8}, Ltv/periscope/android/ui/broadcast/b;-><init>(Ltv/periscope/android/view/b;Ldae;)V

    .line 41
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/at;->h:Lcrz;

    .line 42
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/at;->d:Ltv/periscope/android/view/aj;

    .line 43
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/at;->e:Ltv/periscope/android/ui/user/g;

    .line 44
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/at;->i:Ltv/periscope/android/ui/broadcast/c;

    .line 45
    iput-object p6, p0, Ltv/periscope/android/ui/broadcast/at;->f:Lcyw;

    .line 46
    iput-object p7, p0, Ltv/periscope/android/ui/broadcast/at;->g:Lcyn;

    .line 47
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ltv/periscope/model/chat/Message;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/view/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 53
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/at;->f:Lcyw;

    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcyw;->b(Ljava/lang/String;)Z

    move-result v8

    .line 54
    new-instance v0, Ltv/periscope/android/ui/broadcast/b$d;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/at;->b:Ltv/periscope/android/view/b;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/at;->d:Ltv/periscope/android/view/aj;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/at;->h:Lcrz;

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/at;->c:Ldae;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/ui/broadcast/b$d;-><init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Lcrz;Ldae;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    if-nez v8, :cond_1

    .line 57
    if-eqz p3, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/at;->a:Ltv/periscope/android/ui/chat/ah;

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Ltv/periscope/android/ui/broadcast/b$c;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/at;->b:Ltv/periscope/android/view/b;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/at;->d:Ltv/periscope/android/view/aj;

    invoke-direct {v0, p1, p2, v1, v2}, Ltv/periscope/android/ui/broadcast/b$c;-><init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;)V

    .line 60
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/at;->a:Ltv/periscope/android/ui/chat/ah;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/b$c;->a(Ltv/periscope/android/ui/chat/ah;)V

    .line 61
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/at;->g:Lcyn;

    invoke-interface {v0, p1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_2

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/at;->f:Lcyw;

    invoke-virtual {v0}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcyw;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 65
    :goto_0
    if-eqz v0, :cond_3

    .line 66
    new-instance v0, Ltv/periscope/android/ui/broadcast/b$a;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/at;->b:Ltv/periscope/android/view/b;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/at;->d:Ltv/periscope/android/view/aj;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/at;->h:Lcrz;

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/at;->e:Ltv/periscope/android/ui/user/g;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/ui/broadcast/b$a;-><init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Lcrz;Ltv/periscope/android/ui/user/g;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_1
    :goto_1
    return-object v7

    .line 64
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_3
    new-instance v0, Ltv/periscope/android/ui/broadcast/at$b;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/at;->i:Ltv/periscope/android/ui/broadcast/c;

    invoke-direct {v0, v1}, Ltv/periscope/android/ui/broadcast/at$b;-><init>(Ltv/periscope/android/ui/broadcast/c;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    new-instance v0, Ltv/periscope/android/ui/broadcast/at$a;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/at;->b:Ltv/periscope/android/view/b;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/at;->d:Ltv/periscope/android/view/aj;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/at;->e:Ltv/periscope/android/ui/user/g;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/broadcast/at$a;-><init>(Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    new-instance v0, Ltv/periscope/android/ui/broadcast/at$d;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/at;->b:Ltv/periscope/android/view/b;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/at;->d:Ltv/periscope/android/view/aj;

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/at;->e:Ltv/periscope/android/ui/user/g;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/ui/broadcast/at$d;-><init>(Ltv/periscope/android/ui/broadcast/at;Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    new-instance v0, Ltv/periscope/android/ui/broadcast/at$c;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/at;->b:Ltv/periscope/android/view/b;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/at;->d:Ltv/periscope/android/view/aj;

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/at;->e:Ltv/periscope/android/ui/user/g;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/ui/broadcast/at$c;-><init>(Ltv/periscope/android/ui/broadcast/at;Ljava/lang/String;Ltv/periscope/model/chat/Message;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
