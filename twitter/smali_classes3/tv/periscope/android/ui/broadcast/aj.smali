.class public Ltv/periscope/android/ui/broadcast/aj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/ai;


# instance fields
.field private final a:Lczi;


# direct methods
.method public constructor <init>(Lczi;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/aj;->a:Lczi;

    .line 17
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aj;->a:Lczi;

    invoke-virtual {v0}, Lczi;->getCurrentPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aj;->a:Lczi;

    invoke-virtual {v0}, Lczi;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public e()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aj;->a:Lczi;

    invoke-virtual {v0}, Lczi;->f()I

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/aj;->a:Lczi;

    invoke-virtual {v0}, Lczi;->h()Z

    move-result v0

    return v0
.end method

.method public g()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public h()I
    .locals 1

    .prologue
    .line 76
    const/16 v0, 0x238

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 81
    const/16 v0, 0x140

    return v0
.end method
