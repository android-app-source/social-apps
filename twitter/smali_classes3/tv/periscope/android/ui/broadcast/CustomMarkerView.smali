.class public Ltv/periscope/android/ui/broadcast/CustomMarkerView;
.super Lcom/github/mikephil/charting/components/MarkerView;
.source "Twttr"


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/view/View;

.field private d:F

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/github/mikephil/charting/components/MarkerView;-><init>(Landroid/content/Context;I)V

    .line 24
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->d:F

    .line 30
    sget v0, Ltv/periscope/android/library/f$g;->marker_view:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->c:Landroid/view/View;

    .line 31
    sget v0, Ltv/periscope/android/library/f$g;->count_value:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->a:Landroid/widget/TextView;

    .line 32
    sget v0, Ltv/periscope/android/library/f$g;->peak_title:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->b:Landroid/widget/TextView;

    .line 33
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->d:F

    .line 69
    return-void
.end method


# virtual methods
.method public a(Lcom/github/mikephil/charting/data/Entry;Lfj;)V
    .locals 6

    .prologue
    .line 39
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v0

    float-to-int v0, v0

    .line 42
    int-to-long v2, v0

    iget-wide v4, p0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->e:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 43
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->b:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 44
    sget v1, Ltv/periscope/android/library/f$f;->ps__bg_graph_custom_marker_peak:I

    invoke-virtual {p0, v1}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->setBackgroundResource(I)V

    .line 49
    :cond_0
    :goto_0
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->a:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    invoke-super {p0, p1, p2}, Lcom/github/mikephil/charting/components/MarkerView;->a(Lcom/github/mikephil/charting/data/Entry;Lfj;)V

    .line 51
    return-void

    .line 45
    :cond_1
    int-to-long v2, v0

    iget-wide v4, p0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->f:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 46
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->b:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 47
    sget v1, Ltv/periscope/android/library/f$f;->ps__bg_graph_custom_marker_current:I

    invoke-virtual {p0, v1}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public getOffset()Lhk;
    .locals 3

    .prologue
    .line 63
    new-instance v0, Lhk;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->getWidth()I

    move-result v1

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Lhk;-><init>(FF)V

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1, p2}, Lcom/github/mikephil/charting/components/MarkerView;->onMeasure(II)V

    .line 75
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->a()V

    .line 76
    return-void
.end method

.method public setCurrentValue(J)V
    .locals 1

    .prologue
    .line 58
    iput-wide p1, p0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->f:J

    .line 59
    return-void
.end method

.method public setPeakValue(J)V
    .locals 1

    .prologue
    .line 54
    iput-wide p1, p0, Ltv/periscope/android/ui/broadcast/CustomMarkerView;->e:J

    .line 55
    return-void
.end method
