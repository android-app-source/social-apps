.class public Ltv/periscope/android/ui/broadcast/i$c;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$d;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 3

    .prologue
    .line 740
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 742
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/i$c;->c:Landroid/view/View;

    .line 743
    sget v0, Ltv/periscope/android/library/f$g;->show_more_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$c;->b:Landroid/widget/TextView;

    .line 744
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$c;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$d;->ps__main_primary:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 745
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$c;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 746
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$c;
    .locals 3

    .prologue
    .line 731
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_stats_show_more:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 733
    new-instance v1, Ltv/periscope/android/ui/broadcast/i$c;

    invoke-direct {v1, v0, p2}, Ltv/periscope/android/ui/broadcast/i$c;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    return-object v1
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$d;)V
    .locals 3

    .prologue
    .line 750
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$c;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 751
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$c;->c:Landroid/view/View;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$c;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$d;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 752
    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 728
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$d;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/i$c;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$d;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 756
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 757
    sget v1, Ltv/periscope/android/library/f$g;->show_more_text:I

    if-ne v0, v1, :cond_0

    .line 758
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$c;->a:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/h;->h()V

    .line 760
    :cond_0
    return-void
.end method
