.class public Ltv/periscope/android/ui/broadcast/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/h;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ltv/periscope/android/ui/broadcast/r;

.field private final c:Ltv/periscope/android/view/b;

.field private final d:Ltv/periscope/android/view/aj;

.field private final e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/broadcast/r;Z)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/s;->a:Landroid/content/Context;

    .line 31
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/s;->b:Ltv/periscope/android/ui/broadcast/r;

    .line 32
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/s;->c:Ltv/periscope/android/view/b;

    .line 33
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/s;->d:Ltv/periscope/android/view/aj;

    .line 34
    iput-boolean p5, p0, Ltv/periscope/android/ui/broadcast/s;->e:Z

    .line 35
    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/s;->c:Ltv/periscope/android/view/b;

    invoke-interface {v0}, Ltv/periscope/android/view/b;->e()V

    .line 39
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/s;->b:Ltv/periscope/android/ui/broadcast/r;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/r;->l()V

    .line 49
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/s;->l()V

    .line 50
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 150
    return-void
.end method

.method public a(Ljava/lang/String;Ltv/periscope/model/AbuseType;)V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ltv/periscope/android/view/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/s;->c:Ltv/periscope/android/view/b;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ltv/periscope/android/view/b;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 125
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/s;->f:Z

    .line 110
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/s;->b:Ltv/periscope/android/ui/broadcast/r;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/r;->m()V

    .line 55
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/s;->l()V

    .line 56
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/s;->b:Ltv/periscope/android/ui/broadcast/r;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/r;->n()Z

    move-result v0

    return v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/s;->f:Z

    return v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/s;->a(Z)V

    .line 98
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/s;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Ltv/periscope/android/util/g;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 99
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/s;->l()V

    .line 100
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/s;->e:Z

    return v0
.end method

.method public f()V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 119
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/s;->d:Ltv/periscope/android/view/aj;

    new-instance v1, Ltv/periscope/android/ui/d;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Ltv/periscope/android/ui/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltv/periscope/android/view/aj;->a(Ljava/lang/Object;)V

    .line 120
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 135
    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 155
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/s;->b:Ltv/periscope/android/ui/broadcast/r;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/r;->o()V

    .line 61
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/s;->l()V

    .line 62
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/s;->b:Ltv/periscope/android/ui/broadcast/r;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/r;->p()V

    .line 67
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/s;->l()V

    .line 68
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 0

    .prologue
    .line 165
    return-void
.end method
