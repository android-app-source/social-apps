.class Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;
.super Ltv/periscope/android/view/m;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/broadcast/ReplayScrubView;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/ReplayScrubView;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/ReplayScrubView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;->a:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-direct {p0, p2}, Ltv/periscope/android/view/m;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 96
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;->a:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;->a:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/util/ad;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;->a:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->b(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)V

    .line 101
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;->a:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->e(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;->a:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->f(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)Ltv/periscope/android/view/SegmentedProgressBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/SegmentedProgressBar;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;->a:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->g(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;->a:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->setVisibility(I)V

    .line 106
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;->a:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->d(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;->a:Ltv/periscope/android/ui/broadcast/ReplayScrubView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->c(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method
