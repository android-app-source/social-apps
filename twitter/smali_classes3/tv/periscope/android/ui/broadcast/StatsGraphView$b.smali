.class Ltv/periscope/android/ui/broadcast/StatsGraphView$b;
.super Ltv/periscope/android/util/x;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/StatsGraphView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/util/x",
        "<",
        "Ltv/periscope/android/ui/broadcast/StatsGraphView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ltv/periscope/android/ui/broadcast/StatsGraphView;)V
    .locals 0

    .prologue
    .line 388
    invoke-direct {p0, p1}, Ltv/periscope/android/util/x;-><init>(Ljava/lang/Object;)V

    .line 389
    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 385
    check-cast p2, Ltv/periscope/android/ui/broadcast/StatsGraphView;

    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/StatsGraphView$b;->a(Landroid/os/Message;Ltv/periscope/android/ui/broadcast/StatsGraphView;)V

    return-void
.end method

.method protected a(Landroid/os/Message;Ltv/periscope/android/ui/broadcast/StatsGraphView;)V
    .locals 3

    .prologue
    const/16 v2, 0x3e9

    .line 393
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 411
    :cond_0
    :goto_0
    return-void

    .line 395
    :pswitch_0
    invoke-virtual {p2}, Ltv/periscope/android/ui/broadcast/StatsGraphView;->getStatsDelegate()Ltv/periscope/android/ui/broadcast/StatsGraphView$a;

    move-result-object v0

    .line 396
    if-eqz v0, :cond_0

    .line 399
    invoke-virtual {p0, v2}, Ltv/periscope/android/ui/broadcast/StatsGraphView$b;->removeMessages(I)V

    .line 400
    iget-boolean v1, p2, Ltv/periscope/android/ui/broadcast/StatsGraphView;->a:Z

    if-nez v1, :cond_0

    .line 403
    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/StatsGraphView$a;->a()V

    .line 404
    const-wide/16 v0, 0x3e8

    invoke-virtual {p0, v2, v0, v1}, Ltv/periscope/android/ui/broadcast/StatsGraphView$b;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 393
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method
