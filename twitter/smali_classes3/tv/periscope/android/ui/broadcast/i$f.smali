.class public Ltv/periscope/android/ui/broadcast/i$f;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Ltv/periscope/android/ui/broadcast/StatsMainView;

.field private final d:Ltv/periscope/android/ui/broadcast/StatsMainView;

.field private e:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

.field private final f:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field private final g:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field private final h:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field private final i:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field private j:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 3

    .prologue
    .line 534
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 535
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/i$f;->b:Landroid/view/View;

    .line 536
    sget v0, Ltv/periscope/android/library/f$d;->ps__primary_text:I

    iput v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->f:I

    .line 537
    sget v0, Ltv/periscope/android/library/f$d;->ps__secondary_text:I

    iput v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->g:I

    .line 538
    sget v0, Ltv/periscope/android/library/f$d;->ps__red:I

    iput v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->h:I

    .line 539
    sget v0, Ltv/periscope/android/library/f$d;->ps__blue:I

    iput v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->i:I

    .line 541
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 542
    sget v0, Ltv/periscope/android/library/f$g;->stat_1:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/StatsMainView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->c:Ltv/periscope/android/ui/broadcast/StatsMainView;

    .line 543
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->c:Ltv/periscope/android/ui/broadcast/StatsMainView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__stat_live_viewers:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/broadcast/StatsMainView;->setDescription(Ljava/lang/String;)V

    .line 544
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->c:Ltv/periscope/android/ui/broadcast/StatsMainView;

    invoke-virtual {v0, p0}, Ltv/periscope/android/ui/broadcast/StatsMainView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 546
    sget v0, Ltv/periscope/android/library/f$g;->stat_2:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/StatsMainView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->d:Ltv/periscope/android/ui/broadcast/StatsMainView;

    .line 547
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->d:Ltv/periscope/android/ui/broadcast/StatsMainView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__stat_replay_viewers:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/StatsMainView;->setDescription(Ljava/lang/String;)V

    .line 548
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->d:Ltv/periscope/android/ui/broadcast/StatsMainView;

    invoke-virtual {v0, p0}, Ltv/periscope/android/ui/broadcast/StatsMainView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 549
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$f;
    .locals 3

    .prologue
    .line 552
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_stats_main:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 554
    new-instance v1, Ltv/periscope/android/ui/broadcast/i$f;

    invoke-direct {v1, v0, p2}, Ltv/periscope/android/ui/broadcast/i$f;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    return-object v1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 581
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    if-eqz v0, :cond_0

    .line 582
    sget-object v0, Ltv/periscope/android/ui/broadcast/i$1;->b:[I

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$f;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 584
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->c:Ltv/periscope/android/ui/broadcast/StatsMainView;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/i$f;->f:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$f;->g:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsMainView;->a(II)V

    .line 585
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->d:Ltv/periscope/android/ui/broadcast/StatsMainView;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/i$f;->f:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$f;->g:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsMainView;->a(II)V

    goto :goto_0

    .line 589
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->c:Ltv/periscope/android/ui/broadcast/StatsMainView;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/i$f;->h:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/StatsMainView;->setColor(I)V

    .line 590
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->d:Ltv/periscope/android/ui/broadcast/StatsMainView;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/i$f;->f:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$f;->g:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsMainView;->a(II)V

    goto :goto_0

    .line 594
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->c:Ltv/periscope/android/ui/broadcast/StatsMainView;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/i$f;->f:I

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$f;->g:I

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/StatsMainView;->a(II)V

    .line 595
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->d:Ltv/periscope/android/ui/broadcast/StatsMainView;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/i$f;->i:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/StatsMainView;->setColor(I)V

    goto :goto_0

    .line 582
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 559
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 560
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;->d()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    move-result-object v1

    sget-object v2, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$UserType;

    if-ne v1, v2, :cond_0

    .line 561
    sget v1, Ltv/periscope/android/library/f$d;->ps__app_background_secondary:I

    iput v1, p0, Ltv/periscope/android/ui/broadcast/i$f;->j:I

    .line 562
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$f;->b:Landroid/view/View;

    iget v2, p0, Ltv/periscope/android/ui/broadcast/i$f;->j:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 564
    :cond_0
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;->b()Ltv/periscope/model/q;

    move-result-object v1

    if-nez v1, :cond_1

    .line 578
    :goto_0
    return-void

    .line 567
    :cond_1
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;->b()Ltv/periscope/model/q;

    move-result-object v1

    .line 568
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;->c()Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    move-result-object v2

    iput-object v2, p0, Ltv/periscope/android/ui/broadcast/i$f;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    .line 570
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$f;->c:Ltv/periscope/android/ui/broadcast/StatsMainView;

    sget v3, Ltv/periscope/android/library/f$j;->ps__stats_live_viewers:I

    invoke-virtual {v1}, Ltv/periscope/model/q;->b()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/android/ui/broadcast/StatsMainView;->setDescription(Ljava/lang/String;)V

    .line 571
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$f;->c:Ltv/periscope/android/ui/broadcast/StatsMainView;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/i$f;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v1}, Ltv/periscope/model/q;->b()J

    move-result-wide v4

    invoke-static {v3, v4, v5, v6}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/android/ui/broadcast/StatsMainView;->setValue(Ljava/lang/String;)V

    .line 573
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$f;->d:Ltv/periscope/android/ui/broadcast/StatsMainView;

    sget v3, Ltv/periscope/android/library/f$j;->ps__stats_replay_viewers:I

    invoke-virtual {v1}, Ltv/periscope/model/q;->a()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ltv/periscope/android/ui/broadcast/StatsMainView;->setDescription(Ljava/lang/String;)V

    .line 574
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->d:Ltv/periscope/android/ui/broadcast/StatsMainView;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/i$f;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1}, Ltv/periscope/model/q;->a()J

    move-result-wide v4

    invoke-static {v2, v4, v5, v6}, Ltv/periscope/android/util/z;->a(Landroid/content/res/Resources;JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/StatsMainView;->setValue(Ljava/lang/String;)V

    .line 577
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/i$f;->a()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 521
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/i$f;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$b;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 604
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 605
    sget v1, Ltv/periscope/android/library/f$g;->stat_1:I

    if-ne v0, v1, :cond_2

    .line 606
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    sget-object v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;->b:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    if-ne v0, v1, :cond_1

    .line 607
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->a:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/h;->i()V

    .line 618
    :cond_0
    :goto_0
    return-void

    .line 609
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->a:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/h;->f()V

    goto :goto_0

    .line 611
    :cond_2
    sget v1, Ltv/periscope/android/library/f$g;->stat_2:I

    if-ne v0, v1, :cond_0

    .line 612
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->e:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    sget-object v1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;->c:Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$StatsType;

    if-ne v0, v1, :cond_3

    .line 613
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->a:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/h;->i()V

    goto :goto_0

    .line 615
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$f;->a:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/h;->g()V

    goto :goto_0
.end method
