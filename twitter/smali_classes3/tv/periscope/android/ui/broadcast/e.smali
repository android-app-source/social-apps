.class public Ltv/periscope/android/ui/broadcast/e;
.super Ltv/periscope/android/ui/broadcast/b;
.source "Twttr"


# instance fields
.field private final d:Ltv/periscope/android/ui/broadcast/h;

.field private final e:Lcyn;

.field private final f:Lcyw;

.field private final g:Ltv/periscope/android/ui/broadcast/moderator/g;

.field private final h:Ltv/periscope/android/view/o;


# direct methods
.method public constructor <init>(Ltv/periscope/android/view/b;Ldae;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/android/ui/broadcast/moderator/g;Lcyw;Lcyn;Ltv/periscope/android/view/o;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/b;-><init>(Ltv/periscope/android/view/b;Ldae;)V

    .line 41
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/e;->d:Ltv/periscope/android/ui/broadcast/h;

    .line 42
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/e;->g:Ltv/periscope/android/ui/broadcast/moderator/g;

    .line 43
    iput-object p6, p0, Ltv/periscope/android/ui/broadcast/e;->e:Lcyn;

    .line 44
    iput-object p5, p0, Ltv/periscope/android/ui/broadcast/e;->f:Lcyw;

    .line 45
    iput-object p7, p0, Ltv/periscope/android/ui/broadcast/e;->h:Ltv/periscope/android/view/o;

    .line 46
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ltv/periscope/model/chat/Message;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/view/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/e;->e:Lcyn;

    invoke-interface {v0, p1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v1

    .line 52
    if-nez v1, :cond_1

    .line 53
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 72
    :cond_0
    :goto_0
    return-object v0

    .line 55
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 56
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/e;->h:Ltv/periscope/android/view/o;

    if-eqz v2, :cond_2

    .line 57
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/e;->h:Ltv/periscope/android/view/o;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    :cond_2
    invoke-virtual {v1}, Ltv/periscope/model/p;->q()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v1}, Ltv/periscope/model/p;->K()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Ltv/periscope/model/p;->N()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 61
    :cond_3
    new-instance v2, Ldav;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/e;->d:Ltv/periscope/android/ui/broadcast/h;

    invoke-direct {v2, p1, v3}, Ldav;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_4
    new-instance v2, Ldap;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/e;->d:Ltv/periscope/android/ui/broadcast/h;

    invoke-direct {v2, p1, v3}, Ldap;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/e;->g:Ltv/periscope/android/ui/broadcast/moderator/g;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/e;->g:Ltv/periscope/android/ui/broadcast/moderator/g;

    invoke-interface {v2}, Ltv/periscope/android/ui/broadcast/moderator/g;->e()Z

    move-result v2

    if-nez v2, :cond_5

    .line 66
    new-instance v2, Ldar;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/e;->d:Ltv/periscope/android/ui/broadcast/h;

    invoke-direct {v2, p1, v3}, Ldar;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    :cond_5
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/e;->f:Lcyw;

    invoke-interface {v2}, Lcyw;->b()Ltv/periscope/android/api/PsUser;

    move-result-object v2

    iget-boolean v2, v2, Ltv/periscope/android/api/PsUser;->isEmployee:Z

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ltv/periscope/model/p;->K()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    new-instance v1, Ldaq;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/e;->d:Ltv/periscope/android/ui/broadcast/h;

    invoke-direct {v1, p1, v2}, Ldaq;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    new-instance v1, Ldal;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/e;->d:Ltv/periscope/android/ui/broadcast/h;

    invoke-direct {v1, p1, v2}, Ldal;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
