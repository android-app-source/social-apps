.class public Ltv/periscope/android/ui/broadcast/au;
.super Ltv/periscope/android/ui/broadcast/a;
.source "Twttr"


# instance fields
.field private final c:Ltv/periscope/android/ui/broadcast/b;


# direct methods
.method public constructor <init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/ActionSheet;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;Lcrz;Lcyw;Lcyn;Ldae;Z)V
    .locals 10

    .prologue
    .line 22
    move/from16 v0, p9

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/ui/broadcast/a;-><init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/ActionSheet;Z)V

    .line 23
    new-instance v1, Ltv/periscope/android/ui/broadcast/at;

    move-object v2, p0

    move-object v3, p5

    move-object v4, p3

    move-object v5, p4

    move-object v6, p0

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Ltv/periscope/android/ui/broadcast/at;-><init>(Ltv/periscope/android/view/b;Lcrz;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;Ltv/periscope/android/ui/broadcast/c;Lcyw;Lcyn;Ldae;)V

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/au;->c:Ltv/periscope/android/ui/broadcast/b;

    .line 25
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;
    .locals 1

    .prologue
    .line 15
    invoke-super {p0, p1, p2, p3}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method a()Ltv/periscope/android/ui/broadcast/b;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/au;->c:Ltv/periscope/android/ui/broadcast/b;

    return-object v0
.end method

.method public bridge synthetic a(Lcyw;Ldae;)V
    .locals 0

    .prologue
    .line 15
    invoke-super {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/a;->a(Lcyw;Ldae;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/CharSequence;Ljava/util/List;I)V
    .locals 0

    .prologue
    .line 15
    invoke-super {p0, p1, p2, p3}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/CharSequence;Ljava/util/List;I)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/String;Ltv/periscope/model/chat/Message;IZ)V
    .locals 0

    .prologue
    .line 15
    invoke-super {p0, p1, p2, p3, p4}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;IZ)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/String;Ltv/periscope/model/chat/Message;IZLjava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 15
    invoke-super/range {p0 .. p5}, Ltv/periscope/android/ui/broadcast/a;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;IZLjava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/chat/ah;)V
    .locals 0

    .prologue
    .line 15
    invoke-super {p0, p1}, Ltv/periscope/android/ui/broadcast/a;->a(Ltv/periscope/android/ui/chat/ah;)V

    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/view/CarouselView$a;)V
    .locals 0

    .prologue
    .line 15
    invoke-super {p0, p1}, Ltv/periscope/android/ui/broadcast/a;->a(Ltv/periscope/android/view/CarouselView$a;)V

    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/view/i;)V
    .locals 0

    .prologue
    .line 15
    invoke-super {p0, p1}, Ltv/periscope/android/ui/broadcast/a;->a(Ltv/periscope/android/view/i;)V

    return-void
.end method

.method public bridge synthetic b()V
    .locals 0

    .prologue
    .line 15
    invoke-super {p0}, Ltv/periscope/android/ui/broadcast/a;->b()V

    return-void
.end method

.method public bridge synthetic c()V
    .locals 0

    .prologue
    .line 15
    invoke-super {p0}, Ltv/periscope/android/ui/broadcast/a;->c()V

    return-void
.end method

.method public bridge synthetic d()Z
    .locals 1

    .prologue
    .line 15
    invoke-super {p0}, Ltv/periscope/android/ui/broadcast/a;->d()Z

    move-result v0

    return v0
.end method
