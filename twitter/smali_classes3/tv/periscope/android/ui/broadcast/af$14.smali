.class Ltv/periscope/android/ui/broadcast/af$14;
.super Ltv/periscope/android/util/q;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/broadcast/af;->c(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/util/q",
        "<",
        "Landroid/app/Activity;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/ui/broadcast/af;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/af;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 647
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/af$14;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-direct {p0, p2}, Ltv/periscope/android/util/q;-><init>(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    .line 650
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$14;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    if-nez v0, :cond_0

    .line 673
    :goto_0
    return-void

    .line 653
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$14;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->a:Ltv/periscope/android/player/d;

    invoke-interface {v0}, Ltv/periscope/android/player/d;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 654
    const-string/jumbo v0, "PeriscopePlayer"

    const-string/jumbo v1, "Buffering for more than 1000, showing loading bars."

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$14;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->x:Ltv/periscope/model/p;

    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 656
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$14;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$14;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/af;->u()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__trying_to_reconnect:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->a(Ljava/lang/String;)V

    .line 660
    :goto_1
    sget-object v0, Ltv/periscope/android/ui/broadcast/af$8;->a:[I

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/af$14;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v1, v1, Ltv/periscope/android/ui/broadcast/af;->s:Ltv/periscope/android/player/PlayMode;

    invoke-virtual {v1}, Ltv/periscope/android/player/PlayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 672
    :cond_1
    :goto_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$14;->a:Ltv/periscope/android/ui/broadcast/af;

    const/4 v1, 0x0

    iput-boolean v1, v0, Ltv/periscope/android/ui/broadcast/af;->C:Z

    goto :goto_0

    .line 658
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$14;->a:Ltv/periscope/android/ui/broadcast/af;

    iget-object v0, v0, Ltv/periscope/android/ui/broadcast/af;->o:Ltv/periscope/android/player/c;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ltv/periscope/android/player/c;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 662
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$14;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    const-string/jumbo v1, "Summary.TimeWatched"

    invoke-virtual {v0, v1}, Ltv/periscope/android/analytics/summary/b;->g(Ljava/lang/String;)V

    goto :goto_2

    .line 667
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/af$14;->a:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->w()Ltv/periscope/android/analytics/summary/b;

    move-result-object v0

    const-string/jumbo v1, "Summary.ReplayTimeWatched"

    invoke-virtual {v0, v1}, Ltv/periscope/android/analytics/summary/b;->g(Ljava/lang/String;)V

    goto :goto_2

    .line 660
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
