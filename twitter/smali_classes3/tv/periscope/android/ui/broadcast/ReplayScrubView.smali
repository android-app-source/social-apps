.class public Ltv/periscope/android/ui/broadcast/ReplayScrubView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# static fields
.field private static final a:J

.field private static final b:J

.field private static final c:J


# instance fields
.field private d:Landroid/view/View;

.field private e:Ltv/periscope/android/view/MaskImageView;

.field private f:Landroid/widget/TextView;

.field private g:Ltv/periscope/android/view/SegmentedProgressBar;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/view/View;

.field private k:Landroid/animation/Animator;

.field private l:Landroid/animation/Animator;

.field private m:I

.field private n:J

.field private o:F

.field private p:F

.field private q:J

.field private r:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 33
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a:J

    .line 34
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->b:J

    .line 35
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->c:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 48
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->o:F

    .line 49
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->p:F

    .line 55
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->o:F

    .line 49
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->p:F

    .line 60
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->o:F

    .line 49
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->p:F

    .line 65
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(Landroid/content/Context;)V

    .line 66
    return-void
.end method

.method private a(JF)F
    .locals 1

    .prologue
    .line 272
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->b(J)I

    move-result v0

    .line 273
    int-to-float v0, v0

    div-float v0, p3, v0

    return v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->n:J

    return-wide v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 69
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__replay_scrub_view:I

    invoke-virtual {v0, v1, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 70
    sget v0, Ltv/periscope/android/library/f$g;->thumb_container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->d:Landroid/view/View;

    .line 71
    sget v0, Ltv/periscope/android/library/f$g;->thumb:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/MaskImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->e:Ltv/periscope/android/view/MaskImageView;

    .line 72
    sget v0, Ltv/periscope/android/library/f$g;->time:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->f:Landroid/widget/TextView;

    .line 73
    sget v0, Ltv/periscope/android/library/f$g;->bar:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/SegmentedProgressBar;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->g:Ltv/periscope/android/view/SegmentedProgressBar;

    .line 74
    sget v0, Ltv/periscope/android/library/f$g;->zoom_zone:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->h:Landroid/view/View;

    .line 75
    sget v0, Ltv/periscope/android/library/f$g;->zoom_label:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->i:Landroid/widget/TextView;

    .line 76
    sget v0, Ltv/periscope/android/library/f$g;->control_hints:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->j:Landroid/view/View;

    .line 78
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$e;->ps__replay_thumbnail_corner_radius:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 79
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->e:Ltv/periscope/android/view/MaskImageView;

    const/4 v2, 0x4

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v0, v2, v3

    aput v0, v2, v4

    const/4 v3, 0x2

    aput v0, v2, v3

    const/4 v3, 0x3

    aput v0, v2, v3

    invoke-virtual {v1, v2}, Ltv/periscope/android/view/MaskImageView;->setCornerRadius([F)V

    .line 81
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->c()V

    .line 82
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->d()V

    .line 84
    invoke-static {p1}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 85
    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f400000    # 0.75f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->m:I

    .line 86
    return-void
.end method

.method private a(Landroid/graphics/Point;)V
    .locals 3

    .prologue
    .line 155
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 156
    iget v1, p1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    iget v2, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->p:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 157
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->h:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 158
    return-void
.end method

.method private a(ZLandroid/graphics/Point;)V
    .locals 3

    .prologue
    .line 143
    if-eqz p1, :cond_0

    .line 144
    const/high16 v0, 0x3e800000    # 0.25f

    move v1, v0

    .line 148
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->d:Landroid/view/View;

    .line 149
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 150
    iget v2, p2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 151
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 152
    return-void

    .line 146
    :cond_0
    const v0, 0x3eb33333    # 0.35f

    move v1, v0

    goto :goto_0
.end method

.method private b(J)I
    .locals 5

    .prologue
    .line 277
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->c(J)J

    move-result-wide v0

    .line 278
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 279
    div-long v0, p1, v0

    long-to-int v0, v0

    .line 281
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->f()V

    return-void
.end method

.method private c(J)J
    .locals 5

    .prologue
    .line 286
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v0

    .line 288
    const-wide/16 v2, 0x1

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 290
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 295
    :goto_0
    return-wide v0

    .line 293
    :cond_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 89
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->k:Landroid/animation/Animator;

    .line 90
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->k:Landroid/animation/Animator;

    sget-wide v2, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->b:J

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 91
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->k:Landroid/animation/Animator;

    new-instance v1, Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;

    invoke-direct {v1, p0, p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView$1;-><init>(Ltv/periscope/android/ui/broadcast/ReplayScrubView;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 108
    return-void

    .line 89
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic c(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)Z
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->e()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->j:Landroid/view/View;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 111
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->l:Landroid/animation/Animator;

    .line 112
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->l:Landroid/animation/Animator;

    sget-wide v2, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->c:J

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 113
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->l:Landroid/animation/Animator;

    new-instance v1, Ltv/periscope/android/ui/broadcast/ReplayScrubView$2;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView$2;-><init>(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 128
    return-void

    .line 111
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method static synthetic e(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->d:Landroid/view/View;

    return-object v0
.end method

.method private e()Z
    .locals 4

    .prologue
    .line 190
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/util/ad;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->n:J

    sub-long/2addr v0, v2

    sget-wide v2, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    .line 190
    :goto_0
    return v0

    .line 191
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)Ltv/periscope/android/view/SegmentedProgressBar;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->g:Ltv/periscope/android/view/SegmentedProgressBar;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 305
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 308
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->j:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 309
    new-instance v1, Ltv/periscope/android/view/n;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->j:Landroid/view/View;

    invoke-direct {v1, v2}, Ltv/periscope/android/view/n;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 310
    sget-wide v2, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a:J

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 311
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 313
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->n:J

    .line 314
    return-void

    .line 308
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method static synthetic g(Ltv/periscope/android/ui/broadcast/ReplayScrubView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->i:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->l:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 301
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->k:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 302
    return-void
.end method

.method public a(FI)V
    .locals 4
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 249
    iput p1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->o:F

    .line 251
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getBarWidth()I

    move-result v0

    .line 252
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->g:Ltv/periscope/android/view/SegmentedProgressBar;

    invoke-virtual {v1, v0}, Ltv/periscope/android/view/SegmentedProgressBar;->setBarWidth(I)V

    .line 253
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->g:Ltv/periscope/android/view/SegmentedProgressBar;

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->q:J

    int-to-float v0, v0

    invoke-direct {p0, v2, v3, v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(JF)F

    move-result v0

    invoke-virtual {v1, v0}, Ltv/periscope/android/view/SegmentedProgressBar;->setSegmentSize(F)V

    .line 254
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->g:Ltv/periscope/android/view/SegmentedProgressBar;

    invoke-virtual {v0}, Ltv/periscope/android/view/SegmentedProgressBar;->invalidate()V

    .line 256
    if-lez p2, :cond_0

    .line 257
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    :goto_0
    return-void

    .line 259
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->i:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(J)V
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 210
    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->r:J

    cmp-long v2, v2, p1

    if-eqz v2, :cond_1

    .line 212
    cmp-long v2, p1, v0

    if-gez v2, :cond_2

    move-wide p1, v0

    .line 220
    :cond_0
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->f:Landroid/widget/TextView;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p1, p2}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ldag;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    iput-wide p1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->r:J

    .line 223
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->g:Ltv/periscope/android/view/SegmentedProgressBar;

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->r:J

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/SegmentedProgressBar;->setProgress(I)V

    .line 225
    :cond_1
    return-void

    .line 214
    :cond_2
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->q:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 215
    iget-wide p1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->q:J

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->l:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 319
    return-void
.end method

.method public getBarWidth()I
    .locals 2

    .prologue
    .line 268
    iget v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->m:I

    int-to-float v0, v0

    iget v1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->o:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 201
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->q:J

    return-wide v0
.end method

.method public getSeekTo()J
    .locals 2

    .prologue
    .line 228
    iget-wide v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->r:J

    return-wide v0
.end method

.method public getThumb()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->e:Ltv/periscope/android/view/MaskImageView;

    return-object v0
.end method

.method public getZoom()F
    .locals 1

    .prologue
    .line 264
    iget v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->o:F

    return v0
.end method

.method public getZoomZoneStart()F
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6

    .prologue
    .line 173
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 175
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 176
    invoke-static {v0}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v1

    .line 177
    invoke-static {v0}, Ltv/periscope/android/util/ad;->c(Landroid/content/Context;)Z

    move-result v0

    .line 179
    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(ZLandroid/graphics/Point;)V

    .line 180
    invoke-direct {p0, v1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(Landroid/graphics/Point;)V

    .line 182
    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->n:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 183
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->f()V

    .line 187
    :goto_0
    return-void

    .line 185
    :cond_0
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->j:Landroid/view/View;

    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 132
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 134
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 135
    invoke-static {v0}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v1

    .line 136
    invoke-static {v0}, Ltv/periscope/android/util/ad;->c(Landroid/content/Context;)Z

    move-result v0

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(ZLandroid/graphics/Point;)V

    .line 137
    invoke-direct {p0, v1}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(Landroid/graphics/Point;)V

    .line 138
    return-void
.end method

.method public setDuration(J)V
    .locals 5

    .prologue
    .line 196
    iput-wide p1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->q:J

    .line 197
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->g:Ltv/periscope/android/view/SegmentedProgressBar;

    iget-wide v2, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->q:J

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/SegmentedProgressBar;->setMax(I)V

    .line 198
    return-void
.end method

.method public setEndTime(J)V
    .locals 3

    .prologue
    const/4 v1, 0x4

    .line 240
    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(J)V

    .line 243
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->g:Ltv/periscope/android/view/SegmentedProgressBar;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/SegmentedProgressBar;->setVisibility(I)V

    .line 245
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 246
    return-void
.end method

.method public setInitialTime(J)V
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->g:Ltv/periscope/android/view/SegmentedProgressBar;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/SegmentedProgressBar;->setInitialProgress(I)V

    .line 206
    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(J)V

    .line 207
    return-void
.end method

.method public setThumb(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->e:Ltv/periscope/android/view/MaskImageView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/MaskImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 233
    return-void
.end method

.method public setZoomZonePercentage(F)V
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->p:F

    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    iput p1, p0, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->p:F

    .line 163
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/ReplayScrubView;->a(Landroid/graphics/Point;)V

    .line 165
    :cond_0
    return-void
.end method
