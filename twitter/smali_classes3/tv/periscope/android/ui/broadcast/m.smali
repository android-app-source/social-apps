.class public Ltv/periscope/android/ui/broadcast/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/player/d;
.implements Ltv/periscope/android/player/e;


# instance fields
.field protected a:Ltv/periscope/android/ui/broadcast/l;

.field private final b:Ltv/periscope/android/player/c;

.field private final c:Lcyn;

.field private final d:Ltv/periscope/android/ui/broadcast/ai;

.field private e:Lcxc;


# direct methods
.method public constructor <init>(Ltv/periscope/android/player/c;Ltv/periscope/android/ui/broadcast/ai;Lcyn;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/m;->b:Ltv/periscope/android/player/c;

    .line 36
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/m;->d:Ltv/periscope/android/ui/broadcast/ai;

    .line 37
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/m;->c:Lcyn;

    .line 38
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->e:Lcxc;

    iget-object v0, v0, Lcxc;->d:Ltv/periscope/android/chat/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->e:Lcxc;

    iget-object v0, v0, Lcxc;->b:Ltv/periscope/model/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->e:Lcxc;

    iget-object v0, v0, Lcxc;->d:Ltv/periscope/android/chat/g;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/m;->e:Lcxc;

    iget-object v1, v1, Lcxc;->b:Ltv/periscope/model/af;

    .line 89
    invoke-virtual {v1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/chat/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    .line 89
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(D)V
    .locals 1

    .prologue
    .line 223
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/l;->a(D)V

    .line 226
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 216
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/l;->a(I)V

    .line 219
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 134
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Ltv/periscope/android/ui/broadcast/l;->a(JZ)V

    .line 137
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Ltv/periscope/android/player/a;Z)V
    .locals 8

    .prologue
    .line 119
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->f()V

    .line 122
    :cond_0
    new-instance v0, Ltv/periscope/android/ui/broadcast/l;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/m;->b:Ltv/periscope/android/player/c;

    invoke-interface {v1}, Ltv/periscope/android/player/c;->getTextureView()Landroid/view/TextureView;

    move-result-object v2

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/m;->d:Ltv/periscope/android/ui/broadcast/ai;

    new-instance v5, Ltv/periscope/android/util/a;

    invoke-direct {v5}, Ltv/periscope/android/util/a;-><init>()V

    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/m;->c:Lcyn;

    move-object v1, p1

    move-object v3, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Ltv/periscope/android/ui/broadcast/l;-><init>(Landroid/content/Context;Landroid/view/TextureView;Ltv/periscope/android/player/a;Ltv/periscope/android/ui/broadcast/ai;Ltv/periscope/android/util/a;Lcyn;Z)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    .line 124
    return-void
.end method

.method public a(Lcxc;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/m;->e:Lcxc;

    .line 79
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->o()V

    .line 144
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 238
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/l;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 241
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ltv/periscope/android/chat/g;)Z
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->e:Lcxc;

    iget-object v0, v0, Lcxc;->d:Ltv/periscope/android/chat/g;

    invoke-virtual {v0, p1}, Ltv/periscope/android/chat/g;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    .line 84
    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 83
    :goto_0
    return v0

    .line 84
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ltv/periscope/model/p;Ltv/periscope/android/video/StreamMode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/model/p;",
            "Ltv/periscope/android/video/StreamMode;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/v;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 129
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Ltv/periscope/android/ui/broadcast/l;->a(Ltv/periscope/model/p;Ltv/periscope/android/video/StreamMode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/l;->a(J)V

    .line 158
    :cond_0
    return-void
.end method

.method public d()J
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->m()J

    move-result-wide v0

    .line 54
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 114
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->s()V

    .line 151
    :cond_0
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->n()V

    .line 165
    :cond_0
    return-void
.end method

.method public l()J
    .locals 2

    .prologue
    .line 42
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->l()J

    move-result-wide v0

    .line 45
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public m()J
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->k()J

    move-result-wide v0

    .line 63
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public n()J
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->h()J

    move-result-wide v0

    .line 72
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 169
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->t()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->e()Ljava/util/HashMap;

    move-result-object v0

    .line 178
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    goto :goto_0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 184
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->j()I

    move-result v0

    .line 187
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->t()I

    move-result v0

    .line 196
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public s()V
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->f()V

    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    .line 205
    :cond_0
    return-void
.end method

.method public t()D
    .locals 2

    .prologue
    .line 230
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/m;->a:Ltv/periscope/android/ui/broadcast/l;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/l;->u()D

    move-result-wide v0

    .line 233
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
