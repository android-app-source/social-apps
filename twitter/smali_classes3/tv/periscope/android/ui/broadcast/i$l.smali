.class public Ltv/periscope/android/ui/broadcast/i$l;
.super Ltv/periscope/android/ui/broadcast/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcast/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "l"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/ui/broadcast/i",
        "<",
        "Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$n;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Ltv/periscope/android/ui/broadcast/LiveStatsView;


# direct methods
.method private constructor <init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 1

    .prologue
    .line 805
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/i;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    .line 806
    sget v0, Ltv/periscope/android/library/f$g;->presence_count:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/LiveStatsView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/i$l;->b:Ltv/periscope/android/ui/broadcast/LiveStatsView;

    .line 807
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Ltv/periscope/android/ui/broadcast/h;)Ltv/periscope/android/ui/broadcast/i$l;
    .locals 3

    .prologue
    .line 800
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__broadcast_viewer_count:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 801
    new-instance v1, Ltv/periscope/android/ui/broadcast/i$l;

    invoke-direct {v1, v0, p2}, Ltv/periscope/android/ui/broadcast/i$l;-><init>(Landroid/view/View;Ltv/periscope/android/ui/broadcast/h;)V

    return-object v1
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$n;)V
    .locals 6

    .prologue
    .line 811
    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$n;->b()Ljava/lang/Long;

    move-result-object v0

    .line 812
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 813
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$l;->b:Ltv/periscope/android/ui/broadcast/LiveStatsView;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/LiveStatsView;->a(Ljava/lang/Long;)V

    .line 815
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/i$l;->b:Ltv/periscope/android/ui/broadcast/LiveStatsView;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/i$l;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$d;->ps__app_background_secondary:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/LiveStatsView;->setBackgroundColor(I)V

    .line 816
    return-void
.end method

.method public bridge synthetic a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem;)V
    .locals 0

    .prologue
    .line 794
    check-cast p1, Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$n;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcast/i$l;->a(Ltv/periscope/android/ui/broadcast/BroadcastInfoItem$n;)V

    return-void
.end method
