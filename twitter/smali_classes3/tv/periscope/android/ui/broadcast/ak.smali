.class public Ltv/periscope/android/ui/broadcast/ak;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/ai;


# instance fields
.field private final a:Lczi;

.field private final b:Lczi$e;

.field private final c:Lcza;


# direct methods
.method public constructor <init>(Lczi;Lczi$e;Lczi$b;Lczi$a;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    .line 22
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/ak;->b:Lczi$e;

    .line 23
    new-instance v0, Lcza;

    invoke-direct {v0}, Lcza;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->c:Lcza;

    .line 24
    invoke-direct {p0, p2, p3, p4}, Ltv/periscope/android/ui/broadcast/ak;->a(Lczi$e;Lczi$b;Lczi$a;)V

    .line 25
    return-void
.end method

.method private a(Lczi$e;Lczi$b;Lczi$a;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->c:Lcza;

    invoke-virtual {v0}, Lcza;->a()V

    .line 31
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0, p1}, Lczi;->a(Lczi$e;)V

    .line 32
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0, p3}, Lczi;->a(Lczi$a;)V

    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0, p2}, Lczi;->a(Lczi$b;)V

    .line 35
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->c:Lcza;

    invoke-virtual {v0}, Lcza;->a()V

    .line 36
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ak;->c:Lcza;

    invoke-virtual {v0, v1}, Lczi;->a(Lczi$e;)V

    .line 37
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ak;->c:Lcza;

    invoke-virtual {v0, v1}, Lczi;->a(Lczi$c;)V

    .line 38
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ak;->c:Lcza;

    invoke-virtual {v0, v1}, Lczi;->a(Lczi$d;)V

    .line 39
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0}, Lczi;->getCurrentPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lczi;->a(F)V

    .line 106
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0, p1, p2}, Lczi;->a(J)V

    .line 79
    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0, p1}, Lczi;->a(Landroid/view/Surface;)V

    .line 59
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0, p1}, Lczi;->a(Z)V

    .line 84
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0}, Lczi;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0}, Lczi;->c()V

    .line 54
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0}, Lczi;->d()V

    .line 64
    return-void
.end method

.method public e()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0}, Lczi;->f()I

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0}, Lczi;->h()Z

    move-result v0

    return v0
.end method

.method public g()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 88
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    .line 89
    new-instance v1, Ltv/periscope/android/ui/broadcast/ak$1;

    const-string/jumbo v2, "PsPlayerReadWriteProxy release"

    invoke-static {v2}, Ltv/periscope/android/util/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Ltv/periscope/android/ui/broadcast/ak$1;-><init>(Ltv/periscope/android/ui/broadcast/ak;Ljava/lang/String;Lczi;)V

    .line 94
    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/ak$1;->start()V

    .line 95
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ak;->b:Lczi$e;

    invoke-virtual {v0, v1}, Lczi;->b(Lczi$e;)V

    .line 96
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ak;->c:Lcza;

    invoke-virtual {v0, v1}, Lczi;->b(Lczi$e;)V

    .line 97
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0, v3}, Lczi;->a(Lczi$c;)V

    .line 98
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0, v3}, Lczi;->a(Lczi$d;)V

    .line 99
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0, v3}, Lczi;->a(Lczi$a;)V

    .line 100
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ak;->a:Lczi;

    invoke-virtual {v0, v3}, Lczi;->a(Lczi$b;)V

    .line 101
    return-void
.end method

.method public h()I
    .locals 1

    .prologue
    .line 110
    const/16 v0, 0x238

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 115
    const/16 v0, 0x140

    return v0
.end method
