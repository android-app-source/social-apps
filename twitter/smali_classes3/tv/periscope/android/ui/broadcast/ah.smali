.class public Ltv/periscope/android/ui/broadcast/ah;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private final a:Ltv/periscope/android/player/d;

.field private final b:Landroid/os/Handler;

.field private c:Landroid/widget/TextView;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ltv/periscope/android/player/d;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ltv/periscope/android/ui/broadcast/ah$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/ah$1;-><init>(Ltv/periscope/android/ui/broadcast/ah;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/ah;->d:Ljava/lang/Runnable;

    .line 35
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ah;->a:Ltv/periscope/android/player/d;

    .line 36
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/ah;->b:Landroid/os/Handler;

    .line 37
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ah;->b()V

    .line 38
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ah;->c:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ah;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 49
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/ah;->b()V

    goto :goto_0
.end method

.method public a(Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ah;->c:Landroid/widget/TextView;

    .line 42
    return-void
.end method

.method public b()V
    .locals 8

    .prologue
    const-wide/16 v0, 0x3e8

    .line 53
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ah;->c:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ah;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ah;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->isAttachedToWindow()Z

    move-result v2

    if-nez v2, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ah;->a:Ltv/periscope/android/player/d;

    invoke-interface {v2}, Ltv/periscope/android/player/d;->l()J

    move-result-wide v2

    .line 57
    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/ah;->c:Landroid/widget/TextView;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ldag;->a(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/ah;->b:Landroid/os/Handler;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/ah;->d:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 61
    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/ah;->a:Ltv/periscope/android/player/d;

    invoke-interface {v4}, Ltv/periscope/android/player/d;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/ah;->a:Ltv/periscope/android/player/d;

    invoke-interface {v4}, Ltv/periscope/android/player/d;->i()Z

    move-result v4

    if-nez v4, :cond_0

    .line 63
    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/ah;->a:Ltv/periscope/android/player/d;

    invoke-interface {v4}, Ltv/periscope/android/player/d;->f()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/ah;->a:Ltv/periscope/android/player/d;

    invoke-interface {v4}, Ltv/periscope/android/player/d;->g()Z

    move-result v4

    if-nez v4, :cond_2

    .line 64
    rem-long/2addr v2, v0

    sub-long/2addr v0, v2

    .line 68
    :cond_2
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ah;->b:Landroid/os/Handler;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ah;->d:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
