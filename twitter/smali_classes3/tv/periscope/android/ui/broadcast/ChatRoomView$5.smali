.class Ltv/periscope/android/ui/broadcast/ChatRoomView$5;
.super Ltv/periscope/android/view/v;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/ui/broadcast/ChatRoomView;->f(I)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/animation/ObjectAnimator;

.field final synthetic b:Ltv/periscope/android/ui/broadcast/ChatRoomView;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/ChatRoomView;Landroid/animation/ObjectAnimator;)V
    .locals 0

    .prologue
    .line 663
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->a:Landroid/animation/ObjectAnimator;

    invoke-direct {p0}, Ltv/periscope/android/view/v;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 674
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 675
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Ltv/periscope/android/ui/broadcast/BottomTray;

    move-result-object v0

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/broadcast/BottomTray;->setAlpha(F)V

    .line 676
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Ltv/periscope/android/ui/broadcast/ParticipantCountView;

    move-result-object v0

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/broadcast/ParticipantCountView;->setAlpha(F)V

    .line 677
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->g(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->h(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->getInfoContainer()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 678
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 666
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->a:Landroid/animation/ObjectAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Ltv/periscope/android/ui/broadcast/BottomTray;

    move-result-object v2

    invoke-virtual {v2}, Ltv/periscope/android/ui/broadcast/BottomTray;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {v3}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->d(Ltv/periscope/android/ui/broadcast/ChatRoomView;)I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    aput v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    .line 667
    invoke-static {v3}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->h(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;

    move-result-object v3

    invoke-virtual {v3}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->getInfoContainer()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    aput v3, v1, v2

    .line 666
    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 668
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Ltv/periscope/android/ui/broadcast/BottomTray;

    move-result-object v0

    invoke-virtual {v0, v4}, Ltv/periscope/android/ui/broadcast/BottomTray;->setVisibility(I)V

    .line 669
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/ChatRoomView$5;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->f(Ltv/periscope/android/ui/broadcast/ChatRoomView;)Ltv/periscope/android/ui/broadcast/ParticipantCountView;

    move-result-object v0

    invoke-virtual {v0, v4}, Ltv/periscope/android/ui/broadcast/ParticipantCountView;->setVisibility(I)V

    .line 670
    return-void
.end method
