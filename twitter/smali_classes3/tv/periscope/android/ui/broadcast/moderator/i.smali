.class public Ltv/periscope/android/ui/broadcast/moderator/i;
.super Ltv/periscope/android/util/x;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/util/x",
        "<",
        "Ltv/periscope/android/ui/broadcast/moderator/k;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/moderator/k;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Ltv/periscope/android/util/x;-><init>(Ljava/lang/Object;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 7
    check-cast p2, Ltv/periscope/android/ui/broadcast/moderator/k;

    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/moderator/i;->a(Landroid/os/Message;Ltv/periscope/android/ui/broadcast/moderator/k;)V

    return-void
.end method

.method protected a(Landroid/os/Message;Ltv/periscope/android/ui/broadcast/moderator/k;)V
    .locals 1

    .prologue
    .line 32
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 34
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/model/chat/Message;

    .line 35
    invoke-virtual {p2, v0}, Ltv/periscope/android/ui/broadcast/moderator/k;->a(Ltv/periscope/model/chat/Message;)V

    goto :goto_0

    .line 39
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 40
    invoke-virtual {p2, v0}, Ltv/periscope/android/ui/broadcast/moderator/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {p2}, Ltv/periscope/android/ui/broadcast/moderator/k;->d()V

    goto :goto_0

    .line 32
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 24
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/moderator/i;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 25
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 26
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 27
    int-to-long v2, p2

    invoke-virtual {p0, v0, v2, v3}, Ltv/periscope/android/ui/broadcast/moderator/i;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 28
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;)V
    .locals 2

    .prologue
    .line 17
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/moderator/i;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 18
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 19
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 20
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/moderator/i;->sendMessage(Landroid/os/Message;)Z

    .line 21
    return-void
.end method
