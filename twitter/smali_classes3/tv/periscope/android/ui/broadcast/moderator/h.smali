.class public Ltv/periscope/android/ui/broadcast/moderator/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/moderator/g;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

.field private final c:Ltv/periscope/android/api/ApiManager;

.field private final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/SettingsDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ltv/periscope/android/ui/broadcast/z;

.field private f:Ltv/periscope/android/ui/broadcast/moderator/f;


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;Ltv/periscope/android/ui/broadcast/ChatRoomView;Ltv/periscope/android/api/ApiManager;Ljava/lang/ref/WeakReference;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;",
            "Ltv/periscope/android/ui/broadcast/ChatRoomView;",
            "Ltv/periscope/android/api/ApiManager;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/SettingsDelegate;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->a:Ljava/lang/ref/WeakReference;

    .line 30
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    .line 31
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->c:Ltv/periscope/android/api/ApiManager;

    .line 32
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->d:Ljava/lang/ref/WeakReference;

    .line 33
    return-void
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 41
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Ltv/periscope/android/ui/broadcast/z;
    .locals 4

    .prologue
    .line 137
    new-instance v1, Ltv/periscope/android/ui/broadcast/z;

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    new-instance v2, Ltv/periscope/android/ui/broadcast/moderator/h$1;

    invoke-direct {v2, p0}, Ltv/periscope/android/ui/broadcast/moderator/h$1;-><init>(Ltv/periscope/android/ui/broadcast/moderator/h;)V

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->d:Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0, v2, v3}, Ltv/periscope/android/ui/broadcast/z;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnDismissListener;Ljava/lang/ref/WeakReference;)V

    return-object v1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/moderator/h;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->e:Ltv/periscope/android/ui/broadcast/z;

    if-nez v0, :cond_1

    .line 98
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/moderator/h;->j()Ltv/periscope/android/ui/broadcast/z;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->e:Ltv/periscope/android/ui/broadcast/z;

    .line 100
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->e:Ltv/periscope/android/ui/broadcast/z;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/z;->a()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/moderator/h;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->c:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiManager;->activeJuror(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ltv/periscope/model/chat/MessageType$VoteType;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/moderator/h;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->c:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/api/ApiManager;->vote(Ljava/lang/String;Ltv/periscope/model/chat/MessageType$VoteType;)Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ltv/periscope/android/ui/broadcast/moderator/a;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 72
    iget-object v2, p1, Ltv/periscope/android/ui/broadcast/moderator/a;->a:Ltv/periscope/model/chat/Message;

    .line 73
    iget-object v3, p1, Ltv/periscope/android/ui/broadcast/moderator/a;->b:Ltv/periscope/model/chat/MessageType$SentenceType;

    if-eqz v2, :cond_3

    .line 75
    invoke-virtual {v2}, Ltv/periscope/model/chat/Message;->A()Ltv/periscope/model/chat/MessageType$ReportType;

    move-result-object v1

    :goto_0
    if-eqz v2, :cond_0

    .line 76
    invoke-virtual {v2}, Ltv/periscope/model/chat/Message;->C()Ljava/lang/String;

    move-result-object v0

    .line 73
    :cond_0
    invoke-static {v3, v1, v0}, Ltv/periscope/model/chat/Message;->a(Ltv/periscope/model/chat/MessageType$SentenceType;Ltv/periscope/model/chat/MessageType$ReportType;Ljava/lang/String;)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 80
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setLocalPunishmentPrompt(Ltv/periscope/model/chat/Message;)V

    .line 81
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ltv/periscope/model/chat/Message;)V

    .line 82
    sget-object v0, Ltv/periscope/model/chat/MessageType$SentenceType;->d:Ltv/periscope/model/chat/MessageType$SentenceType;

    iget-object v1, p1, Ltv/periscope/android/ui/broadcast/moderator/a;->b:Ltv/periscope/model/chat/MessageType$SentenceType;

    if-eq v0, v1, :cond_1

    sget-object v0, Ltv/periscope/model/chat/MessageType$SentenceType;->b:Ltv/periscope/model/chat/MessageType$SentenceType;

    iget-object v1, p1, Ltv/periscope/android/ui/broadcast/moderator/a;->b:Ltv/periscope/model/chat/MessageType$SentenceType;

    if-ne v0, v1, :cond_4

    .line 84
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget v1, p1, Ltv/periscope/android/ui/broadcast/moderator/a;->c:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b(I)V

    .line 90
    :cond_2
    :goto_1
    return-void

    :cond_3
    move-object v1, v0

    .line 75
    goto :goto_0

    .line 85
    :cond_4
    sget-object v0, Ltv/periscope/model/chat/MessageType$SentenceType;->c:Ltv/periscope/model/chat/MessageType$SentenceType;

    iget-object v1, p1, Ltv/periscope/android/ui/broadcast/moderator/a;->b:Ltv/periscope/model/chat/MessageType$SentenceType;

    if-ne v0, v1, :cond_5

    .line 86
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->t()V

    goto :goto_1

    .line 87
    :cond_5
    sget-object v0, Ltv/periscope/model/chat/MessageType$SentenceType;->e:Ltv/periscope/model/chat/MessageType$SentenceType;

    iget-object v1, p1, Ltv/periscope/android/ui/broadcast/moderator/a;->b:Ltv/periscope/model/chat/MessageType$SentenceType;

    if-ne v0, v1, :cond_2

    .line 88
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->u()V

    goto :goto_1
.end method

.method public a(Ltv/periscope/android/ui/broadcast/moderator/f;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->f:Ltv/periscope/android/ui/broadcast/moderator/f;

    .line 37
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->b(Ltv/periscope/model/chat/Message;)V

    .line 63
    return-void
.end method

.method public a(Ltv/periscope/model/chat/MessageType$VoteType;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->p()V

    .line 121
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->r()V

    .line 106
    return-void
.end method

.method public b(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->c(Ltv/periscope/model/chat/Message;)V

    .line 68
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->q()V

    .line 111
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->v()V

    .line 116
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->f:Ltv/periscope/android/ui/broadcast/moderator/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->f:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/f;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/moderator/h;->g()V

    .line 132
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->b:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->r()V

    .line 133
    return-void
.end method

.method g()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->f:Ltv/periscope/android/ui/broadcast/moderator/f;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->f:Ltv/periscope/android/ui/broadcast/moderator/f;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/f;->a()V

    .line 151
    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->e:Ltv/periscope/android/ui/broadcast/z;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/h;->e:Ltv/periscope/android/ui/broadcast/z;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/z;->c()V

    .line 157
    :cond_0
    return-void
.end method
