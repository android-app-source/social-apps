.class public Ltv/periscope/android/ui/broadcast/moderator/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/moderator/f;


# instance fields
.field private final a:Ltv/periscope/android/ui/broadcast/moderator/k;


# direct methods
.method public constructor <init>(Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/c;Ljava/lang/String;Ldbd;Ldbb;)V
    .locals 7

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ltv/periscope/android/ui/broadcast/moderator/k;

    invoke-static {}, Ltv/periscope/android/ui/broadcast/moderator/b;->a()Ltv/periscope/android/ui/broadcast/moderator/b;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/ui/broadcast/moderator/k;-><init>(Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/c;Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/b;Ldbd;Ldbb;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/j;->a:Ltv/periscope/android/ui/broadcast/moderator/k;

    .line 19
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/j;->a:Ltv/periscope/android/ui/broadcast/moderator/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/k;->c()V

    .line 23
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/j;->a:Ltv/periscope/android/ui/broadcast/moderator/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/k;->b()Ltv/periscope/android/ui/broadcast/moderator/d;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ltv/periscope/android/ui/broadcast/moderator/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public a(Ltv/periscope/model/chat/MessageType$VoteType;)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/j;->a:Ltv/periscope/android/ui/broadcast/moderator/k;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/k;->a(Ltv/periscope/model/chat/MessageType$VoteType;)V

    .line 27
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/j;->a:Ltv/periscope/android/ui/broadcast/moderator/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/k;->b()Ltv/periscope/android/ui/broadcast/moderator/d;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/moderator/d;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/j;->a:Ltv/periscope/android/ui/broadcast/moderator/k;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/k;->e()V

    .line 31
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/j;->a:Ltv/periscope/android/ui/broadcast/moderator/k;

    const-string/jumbo v1, "UserPunished"

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/moderator/k;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ServiceEvent;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/j;->a:Ltv/periscope/android/ui/broadcast/moderator/k;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/k;->a(Ltv/periscope/android/event/ServiceEvent;)V

    .line 59
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/chatman/model/Ban;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/j;->a:Ltv/periscope/android/ui/broadcast/moderator/k;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/k;->a(Ltv/periscope/chatman/model/Ban;)V

    .line 54
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/model/chat/ChatEvent;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/j;->a:Ltv/periscope/android/ui/broadcast/moderator/k;

    invoke-virtual {p1}, Ltv/periscope/model/chat/ChatEvent;->a()Ltv/periscope/model/chat/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/moderator/k;->a(Ltv/periscope/model/chat/Message;)V

    .line 49
    return-void
.end method
