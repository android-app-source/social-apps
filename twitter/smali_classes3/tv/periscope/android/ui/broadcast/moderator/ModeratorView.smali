.class public Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;,
        Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$a;,
        Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$c;,
        Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$b;
    }
.end annotation


# instance fields
.field private A:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;

.field private a:Landroid/animation/Animator;

.field private b:Landroid/animation/Animator;

.field private c:Landroid/animation/Animator;

.field private d:Landroid/animation/Animator;

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:Z

.field private j:I

.field private k:Landroid/view/View;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/view/View;

.field private p:Landroid/widget/ProgressBar;

.field private q:Landroid/view/View;

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;

.field private t:Landroid/widget/Button;

.field private u:Landroid/widget/Button;

.field private v:Landroid/widget/Button;

.field private final w:I

.field private final x:I

.field private y:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$b;

.field private z:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$c;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 226
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 230
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/16 v3, 0x1f4

    .line 233
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->i:Z

    .line 235
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__moderator_overlay:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 237
    sget v0, Ltv/periscope/android/library/f$g;->info_container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->o:Landroid/view/View;

    .line 238
    sget v0, Ltv/periscope/android/library/f$g;->info:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->m:Landroid/widget/TextView;

    .line 239
    sget v0, Ltv/periscope/android/library/f$g;->timer:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    .line 240
    sget v0, Ltv/periscope/android/library/f$g;->moderator_out_of_time:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->s:Landroid/view/View;

    .line 241
    sget v0, Ltv/periscope/android/library/f$g;->learn_more_about_moderation:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->n:Landroid/widget/TextView;

    .line 242
    sget v0, Ltv/periscope/android/library/f$g;->buttons_container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->q:Landroid/view/View;

    .line 243
    sget v0, Ltv/periscope/android/library/f$g;->negative:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->t:Landroid/widget/Button;

    .line 244
    sget v0, Ltv/periscope/android/library/f$g;->positive:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->u:Landroid/widget/Button;

    .line 245
    sget v0, Ltv/periscope/android/library/f$g;->neutral:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->v:Landroid/widget/Button;

    .line 248
    sget v0, Ltv/periscope/android/library/f$g;->message_container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->r:Landroid/view/View;

    .line 249
    sget v0, Ltv/periscope/android/library/f$g;->message:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->k:Landroid/view/View;

    .line 250
    sget v0, Ltv/periscope/android/library/f$g;->message_moderate_body:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->l:Landroid/widget/TextView;

    .line 252
    new-instance v0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$a;-><init>(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$1;)V

    .line 253
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->t:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 254
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->u:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 255
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->v:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    new-instance v0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;-><init>(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->A:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;

    .line 260
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 262
    sget v1, Ltv/periscope/android/library/f$e;->ps__moderator_timer_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->h:F

    .line 264
    invoke-direct {p0, v3}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->c(I)Landroid/animation/Animator;

    move-result-object v1

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->a:Landroid/animation/Animator;

    .line 265
    const/16 v1, 0x3e8

    invoke-direct {p0, v1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->d(I)Landroid/animation/Animator;

    move-result-object v1

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->b:Landroid/animation/Animator;

    .line 266
    invoke-direct {p0, v3}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->e(I)Landroid/animation/Animator;

    move-result-object v1

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->c:Landroid/animation/Animator;

    .line 267
    invoke-direct {p0, v3}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->f(I)Landroid/animation/Animator;

    move-result-object v1

    iput-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->d:Landroid/animation/Animator;

    .line 269
    sget v1, Ltv/periscope/android/library/f$e;->ps__moderator_timer_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->w:I

    .line 270
    sget v1, Ltv/periscope/android/library/f$e;->ps__moderator_timer_height_small:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->x:I

    .line 272
    sget v1, Ltv/periscope/android/library/f$e;->ps__moderator_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->e:F

    .line 274
    sget v1, Ltv/periscope/android/library/f$e;->ps__moderator_button_min_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sget v2, Ltv/periscope/android/library/f$e;->ps__moderator_ui_component_spacing:I

    .line 275
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->f:F

    .line 276
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->n:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$c;)Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$c;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->z:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$c;

    return-object p1
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 403
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->a(II)V

    .line 404
    return-void
.end method

.method private a(II)V
    .locals 4

    .prologue
    .line 398
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->A:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;

    int-to-long v2, p2

    invoke-virtual {v0, p1, v2, v3}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;->sendEmptyMessageDelayed(IJ)Z

    .line 399
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->A:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;->sendEmptyMessage(I)Z

    .line 400
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;F)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->setCountdownTimerBackgroundAlpha(F)V

    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->b(I)V

    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;Z)Z
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->i:Z

    return p1
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$b;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->y:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$b;

    return-object v0
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 407
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->a(II)V

    .line 408
    return-void
.end method

.method private c(I)Landroid/animation/Animator;
    .locals 12

    .prologue
    .line 445
    .line 452
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->q:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 453
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->m:Landroid/widget/TextView;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 454
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->q:Landroid/view/View;

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v6, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->g:F

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 456
    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v5, 0x2

    new-array v5, v5, [F

    const/4 v6, 0x0

    iget v7, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->g:F

    iget v8, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->h:F

    add-float/2addr v7, v8

    aput v7, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput v7, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 458
    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->r:Landroid/view/View;

    sget-object v5, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v6, 0x2

    new-array v6, v6, [F

    const/4 v7, 0x0

    iget v8, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->g:F

    iget v9, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->h:F

    add-float/2addr v8, v9

    aput v8, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x0

    aput v8, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 460
    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->k:Landroid/view/View;

    sget-object v6, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    iget-object v9, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->k:Landroid/view/View;

    .line 461
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    int-to-float v9, v9

    aput v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x0

    aput v9, v7, v8

    .line 460
    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 462
    iget-object v6, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->o:Landroid/view/View;

    sget-object v7, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    iget v10, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->g:F

    iget v11, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->h:F

    add-float/2addr v10, v11

    iget-object v11, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->k:Landroid/view/View;

    .line 463
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    int-to-float v11, v11

    add-float/2addr v10, v11

    aput v10, v8, v9

    const/4 v9, 0x1

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 462
    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 464
    new-instance v7, Landroid/animation/AnimatorSet;

    invoke-direct {v7}, Landroid/animation/AnimatorSet;-><init>()V

    .line 465
    int-to-long v8, p1

    invoke-virtual {v7, v8, v9}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 466
    const/4 v8, 0x7

    new-array v8, v8, [Landroid/animation/Animator;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    const/4 v0, 0x1

    aput-object v1, v8, v0

    const/4 v0, 0x2

    aput-object v2, v8, v0

    const/4 v0, 0x3

    aput-object v3, v8, v0

    const/4 v0, 0x4

    aput-object v5, v8, v0

    const/4 v0, 0x5

    aput-object v4, v8, v0

    const/4 v0, 0x6

    aput-object v6, v8, v0

    invoke-virtual {v7, v8}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 467
    new-instance v0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$1;

    invoke-direct {v0, p0, v5, v6}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$1;-><init>(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V

    invoke-virtual {v7, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 496
    return-object v7

    .line 452
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 453
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic c(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->i:Z

    return v0
.end method

.method private d(I)Landroid/animation/Animator;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 500
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->m:Landroid/widget/TextView;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v6, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 501
    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 502
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->m:Landroid/widget/TextView;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 503
    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 505
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 506
    new-array v3, v6, [Landroid/animation/Animator;

    aput-object v0, v3, v7

    aput-object v1, v3, v8

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 508
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    fill-array-data v3, :array_2

    invoke-static {v0, v1, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 509
    int-to-long v4, p1

    invoke-virtual {v0, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 510
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v6, [F

    fill-array-data v4, :array_3

    invoke-static {v1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 511
    int-to-long v4, p1

    invoke-virtual {v1, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 513
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 514
    new-array v4, v6, [Landroid/animation/Animator;

    aput-object v0, v4, v7

    aput-object v1, v4, v8

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 516
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 517
    new-array v1, v6, [Landroid/animation/Animator;

    aput-object v3, v1, v7

    aput-object v2, v1, v8

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 518
    new-instance v1, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$3;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$3;-><init>(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 524
    return-object v0

    .line 500
    :array_0
    .array-data 4
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    .line 502
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
    .end array-data

    .line 508
    :array_2
    .array-data 4
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    .line 510
    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
    .end array-data
.end method

.method static synthetic d(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->A:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;

    return-object v0
.end method

.method private e(I)Landroid/animation/Animator;
    .locals 14

    .prologue
    .line 528
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->q:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 529
    new-instance v1, Ltv/periscope/android/view/n;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->q:Landroid/view/View;

    invoke-direct {v1, v2}, Ltv/periscope/android/view/n;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 531
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->q:Landroid/view/View;

    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->g:F

    aput v5, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 532
    new-instance v1, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$4;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$4;-><init>(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)V

    invoke-virtual {v2, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 539
    iget v4, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->h:F

    .line 540
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v5, 0x2

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    const/4 v6, 0x1

    iget v7, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->g:F

    add-float/2addr v7, v4

    aput v7, v5, v6

    invoke-static {v1, v3, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 541
    new-instance v1, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$5;

    invoke-direct {v1, p0, v4}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$5;-><init>(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;F)V

    invoke-virtual {v3, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 548
    iget v6, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->h:F

    .line 549
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->r:Landroid/view/View;

    sget-object v5, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/4 v9, 0x0

    aput v9, v7, v8

    const/4 v8, 0x1

    iget v9, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->g:F

    add-float/2addr v9, v6

    aput v9, v7, v8

    invoke-static {v1, v5, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 550
    new-instance v1, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$6;

    invoke-direct {v1, p0, v6}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$6;-><init>(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;F)V

    invoke-virtual {v5, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 557
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->k:Landroid/view/View;

    sget-object v7, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->k:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    int-to-float v10, v10

    aput v10, v8, v9

    invoke-static {v1, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 558
    new-instance v1, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$7;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$7;-><init>(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)V

    invoke-virtual {v7, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 565
    iget v9, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->h:F

    .line 566
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->o:Landroid/view/View;

    sget-object v8, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/4 v11, 0x0

    const/4 v12, 0x0

    aput v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->k:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    int-to-float v12, v12

    add-float/2addr v12, v9

    iget v13, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->g:F

    add-float/2addr v12, v13

    aput v12, v10, v11

    invoke-static {v1, v8, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    .line 567
    new-instance v1, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$8;

    invoke-direct {v1, p0, v9}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$8;-><init>(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;F)V

    invoke-virtual {v8, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 574
    new-instance v10, Landroid/animation/AnimatorSet;

    invoke-direct {v10}, Landroid/animation/AnimatorSet;-><init>()V

    .line 575
    int-to-long v12, p1

    invoke-virtual {v10, v12, v13}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 576
    const/4 v1, 0x6

    new-array v1, v1, [Landroid/animation/Animator;

    const/4 v11, 0x0

    aput-object v0, v1, v11

    const/4 v0, 0x1

    aput-object v2, v1, v0

    const/4 v0, 0x2

    aput-object v3, v1, v0

    const/4 v0, 0x3

    aput-object v7, v1, v0

    const/4 v0, 0x4

    aput-object v5, v1, v0

    const/4 v0, 0x5

    aput-object v8, v1, v0

    invoke-virtual {v10, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 577
    new-instance v0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$9;

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$9;-><init>(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;FLandroid/animation/ObjectAnimator;FLandroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;F)V

    invoke-virtual {v10, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 594
    return-object v10

    .line 528
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method static synthetic e(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->t:Landroid/widget/Button;

    return-object v0
.end method

.method private f(I)Landroid/animation/Animator;
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 598
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->e(I)Landroid/animation/Animator;

    move-result-object v0

    .line 599
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->o:Landroid/view/View;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 600
    new-instance v2, Ltv/periscope/android/view/n;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->o:Landroid/view/View;

    invoke-direct {v2, v3}, Ltv/periscope/android/view/n;-><init>(Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 602
    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->s:Landroid/view/View;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v6, [F

    fill-array-data v4, :array_1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 603
    new-instance v3, Ltv/periscope/android/view/m;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->s:Landroid/view/View;

    invoke-direct {v3, v4}, Ltv/periscope/android/view/m;-><init>(Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 605
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 606
    int-to-long v4, p1

    invoke-virtual {v3, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 607
    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v2, v4, v1

    aput-object v0, v4, v6

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 608
    new-instance v0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$10;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$10;-><init>(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)V

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 619
    return-object v3

    .line 599
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 602
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic f(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->u:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic g(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->v:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic h(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic i(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->j()V

    return-void
.end method

.method static synthetic j(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$c;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->z:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$c;

    return-object v0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 425
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v0

    .line 426
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getMax()I

    move-result v1

    .line 427
    sub-int v0, v1, v0

    int-to-float v0, v0

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 429
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 430
    instance-of v2, v0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v2, :cond_0

    .line 432
    const/16 v2, 0xff

    const v3, 0x3e4ccccd    # 0.2f

    add-float/2addr v1, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v1, v3

    float-to-int v1, v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 433
    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    const v2, 0x102000d

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 435
    :cond_0
    return-void
.end method

.method static synthetic k(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->o:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->s:Landroid/view/View;

    return-object v0
.end method

.method static synthetic m(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->k:Landroid/view/View;

    return-object v0
.end method

.method static synthetic n(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->g:F

    return v0
.end method

.method static synthetic o(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->h:F

    return v0
.end method

.method static synthetic p(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->m:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic q(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->q:Landroid/view/View;

    return-object v0
.end method

.method static synthetic r(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->r:Landroid/view/View;

    return-object v0
.end method

.method static synthetic s(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->j:I

    return v0
.end method

.method private setChildrenVisibility(I)V
    .locals 3

    .prologue
    .line 391
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->getChildCount()I

    move-result v1

    .line 392
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 393
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 392
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 395
    :cond_0
    return-void
.end method

.method private setCountdownTimerBackgroundAlpha(F)V
    .locals 2

    .prologue
    .line 438
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 439
    instance-of v1, v0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v1, :cond_0

    .line 440
    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    const/high16 v1, 0x1020000

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 442
    :cond_0
    return-void
.end method

.method private setReportType(Ltv/periscope/model/chat/MessageType$ReportType;)V
    .locals 2

    .prologue
    .line 309
    sget-object v0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$2;->b:[I

    invoke-virtual {p1}, Ltv/periscope/model/chat/MessageType$ReportType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 315
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->t:Landroid/widget/Button;

    sget v1, Ltv/periscope/android/library/f$l;->ps__moderator_negative:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 318
    :goto_0
    return-void

    .line 311
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->t:Landroid/widget/Button;

    sget v1, Ltv/periscope/android/library/f$l;->ps__moderator_negative_spam:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 309
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic t(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->b:Landroid/animation/Animator;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 321
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->e()V

    .line 322
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->g()V

    .line 323
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 324
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->c:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 326
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->m:Landroid/widget/TextView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__moderate_wait_for_responses:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 328
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 329
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 330
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->c:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 331
    return-void
.end method

.method public a(ILtv/periscope/android/ui/broadcast/moderator/ModeratorView$c;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 373
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->z:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$c;

    .line 375
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 376
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 377
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 378
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->s:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 380
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 381
    const v0, 0x3e4ccccd    # 0.2f

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->setCountdownTimerBackgroundAlpha(F)V

    .line 382
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setAlpha(F)V

    .line 383
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 384
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 385
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->j()V

    .line 387
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->a(I)V

    .line 388
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;)V
    .locals 2

    .prologue
    .line 289
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->G()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->j:I

    .line 290
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->i()V

    .line 291
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->setVisibility(I)V

    .line 292
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->m:Landroid/widget/TextView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__help_moderate_content:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 294
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    .line 295
    sget-object v1, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$2;->a:[I

    invoke-virtual {v0}, Ltv/periscope/model/chat/MessageType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 301
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->l:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 303
    :goto_0
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->A()Ltv/periscope/model/chat/MessageType$ReportType;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->setReportType(Ltv/periscope/model/chat/MessageType$ReportType;)V

    .line 304
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->a:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 305
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->a:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 306
    return-void

    .line 298
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->l:Landroid/widget/TextView;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->A:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;->b()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->d:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 339
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->d:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 340
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 343
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->i()V

    .line 344
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->setVisibility(I)V

    .line 345
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->m:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 349
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->m:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 353
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 356
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->w:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 357
    invoke-direct {p0, v2}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->setCountdownTimerBackgroundAlpha(F)V

    .line 358
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setAlpha(F)V

    .line 359
    return-void
.end method

.method public getInfoContainer()Landroid/view/View;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->o:Landroid/view/View;

    return-object v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->x:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 363
    return-void
.end method

.method public i()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 411
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 412
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->d:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 413
    const v0, 0x3e4ccccd    # 0.2f

    invoke-direct {p0, v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->setCountdownTimerBackgroundAlpha(F)V

    .line 414
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setAlpha(F)V

    .line 415
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 416
    iput-boolean v2, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->i:Z

    .line 417
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->A:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$d;->a()V

    .line 418
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 419
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->p:Landroid/widget/ProgressBar;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 420
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->j()V

    .line 421
    invoke-direct {p0, v2}, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->setChildrenVisibility(I)V

    .line 422
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 280
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 281
    iget v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->e:F

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->q:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->f:F

    sub-float/2addr v0, v1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->g:F

    .line 282
    return-void
.end method

.method public setModeratorSelectionListener(Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$b;)V
    .locals 0

    .prologue
    .line 366
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/moderator/ModeratorView;->y:Ltv/periscope/android/ui/broadcast/moderator/ModeratorView$b;

    .line 367
    return-void
.end method
