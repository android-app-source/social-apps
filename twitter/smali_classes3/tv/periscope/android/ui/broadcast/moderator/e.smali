.class public Ltv/periscope/android/ui/broadcast/moderator/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcast/moderator/d;


# instance fields
.field final a:Ldbd;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field final b:Ldbb;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private d:Ltv/periscope/model/chat/Message;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ldbd;Ldbb;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p2, p0, Ltv/periscope/android/ui/broadcast/moderator/e;->a:Ldbd;

    .line 29
    iput-object p3, p0, Ltv/periscope/android/ui/broadcast/moderator/e;->b:Ldbb;

    .line 30
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/moderator/e;->c:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/e;->d:Ltv/periscope/model/chat/Message;

    .line 36
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 52
    if-nez p1, :cond_0

    .line 56
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/e;->a:Ldbd;

    invoke-interface {v0, p1}, Ldbd;->a(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/ui/broadcast/moderator/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-virtual {p0, p3}, Ltv/periscope/android/ui/broadcast/moderator/e;->a(Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 40
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/moderator/e;->d:Ltv/periscope/model/chat/Message;

    .line 41
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcast/moderator/e;->a(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 90
    if-eqz p2, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/e;->b:Ldbb;

    invoke-interface {v0, p1}, Ldbb;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/e;->b:Ldbb;

    invoke-interface {v0, p1}, Ldbb;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ltv/periscope/model/chat/Message;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/e;->d:Ltv/periscope/model/chat/Message;

    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 75
    if-nez p2, :cond_0

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/e;->b:Ldbb;

    invoke-interface {v0, p1, p2}, Ldbb;->a(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 60
    if-eqz p1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/e;->a:Ldbd;

    invoke-interface {v0, p1}, Ldbd;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/e;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
