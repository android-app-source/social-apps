.class public Ltv/periscope/android/ui/broadcast/moderator/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcast/moderator/b$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/ui/broadcast/moderator/a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/b;->a:Ljava/util/List;

    .line 24
    return-void
.end method

.method synthetic constructor <init>(Ltv/periscope/android/ui/broadcast/moderator/b$1;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/moderator/b;-><init>()V

    return-void
.end method

.method public static a()Ltv/periscope/android/ui/broadcast/moderator/b;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Ltv/periscope/android/ui/broadcast/moderator/b$a;->a:Ltv/periscope/android/ui/broadcast/moderator/b;

    return-object v0
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/moderator/a;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/moderator/b;->b:Z

    .line 49
    return-void
.end method

.method public b()Ltv/periscope/android/ui/broadcast/moderator/a;
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 31
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/b;->a:Ljava/util/List;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/b;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/moderator/a;

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcast/moderator/b;->b:Z

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcast/moderator/b;->b:Z

    .line 40
    return-void
.end method
