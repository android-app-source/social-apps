.class Ltv/periscope/android/ui/broadcast/moderator/k;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:[Ldbh;

.field private final b:[[I

.field private c:I

.field private final d:Ltv/periscope/android/ui/broadcast/moderator/b;

.field private final e:Ltv/periscope/android/ui/broadcast/moderator/d;

.field private final f:Ltv/periscope/android/ui/broadcast/moderator/i;

.field private final g:Ltv/periscope/android/ui/broadcast/moderator/g;


# direct methods
.method constructor <init>(Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/c;Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/b;Ldbd;Ldbb;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x5

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x6

    new-array v0, v0, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-array v2, v3, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_5

    aput-object v1, v0, v3

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->b:[[I

    .line 72
    iput v4, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    .line 83
    new-instance v0, Ltv/periscope/android/ui/broadcast/moderator/e;

    invoke-direct {v0, p3, p5, p6}, Ltv/periscope/android/ui/broadcast/moderator/e;-><init>(Ljava/lang/String;Ldbd;Ldbb;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->e:Ltv/periscope/android/ui/broadcast/moderator/d;

    .line 84
    new-instance v0, Ltv/periscope/android/ui/broadcast/moderator/i;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/moderator/i;-><init>(Ltv/periscope/android/ui/broadcast/moderator/k;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->f:Ltv/periscope/android/ui/broadcast/moderator/i;

    .line 85
    iput-object p4, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    .line 86
    iput-object p1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->g:Ltv/periscope/android/ui/broadcast/moderator/g;

    .line 89
    invoke-direct {p0, p1, p2, p3}, Ltv/periscope/android/ui/broadcast/moderator/k;->a(Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/c;Ljava/lang/String;)[Ldbh;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->a:[Ldbh;

    .line 90
    return-void

    .line 60
    nop

    :array_0
    .array-data 4
        0x1
        0x0
        0x0
        0x0
        0x3
    .end array-data

    :array_1
    .array-data 4
        0x4
        0x0
        0x4
        0x2
        0x1
    .end array-data

    :array_2
    .array-data 4
        0x5
        0x0
        0x5
        0x2
        0x2
    .end array-data

    :array_3
    .array-data 4
        0x3
        0x0
        0x0
        0x3
        0x3
    .end array-data

    :array_4
    .array-data 4
        0x4
        0x0
        0x4
        0x4
        0x4
    .end array-data

    :array_5
    .array-data 4
        0x0
        0x0
        0x0
        0x5
        0x5
    .end array-data
.end method

.method private a(Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/c;Ljava/lang/String;)[Ldbh;
    .locals 8

    .prologue
    .line 103
    const/4 v0, 0x6

    new-array v6, v0, [Ldbh;

    const/4 v7, 0x0

    new-instance v0, Ldbl;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->e:Ltv/periscope/android/ui/broadcast/moderator/d;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    move-object v1, p3

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Ldbl;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;Ltv/periscope/android/ui/broadcast/moderator/c;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    new-instance v0, Ldbg;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->e:Ltv/periscope/android/ui/broadcast/moderator/d;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->f:Ltv/periscope/android/ui/broadcast/moderator/i;

    move-object v1, p3

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Ldbg;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;Ltv/periscope/android/ui/broadcast/moderator/i;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    new-instance v0, Ldbf;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->e:Ltv/periscope/android/ui/broadcast/moderator/d;

    iget-object v4, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    iget-object v5, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->f:Ltv/periscope/android/ui/broadcast/moderator/i;

    move-object v1, p3

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Ldbf;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;Ltv/periscope/android/ui/broadcast/moderator/i;)V

    aput-object v0, v6, v7

    const/4 v0, 0x3

    new-instance v1, Ldbi;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->e:Ltv/periscope/android/ui/broadcast/moderator/d;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    invoke-direct {v1, p3, v2, p1, v3}, Ldbi;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;)V

    aput-object v1, v6, v0

    const/4 v0, 0x4

    new-instance v1, Ldbk;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->e:Ltv/periscope/android/ui/broadcast/moderator/d;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    invoke-direct {v1, p3, v2, p1, v3}, Ldbk;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;)V

    aput-object v1, v6, v0

    const/4 v0, 0x5

    new-instance v1, Ldbj;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->e:Ltv/periscope/android/ui/broadcast/moderator/d;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    invoke-direct {v1, p3, v2, p1, v3}, Ldbj;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;)V

    aput-object v1, v6, v0

    return-object v6
.end method

.method private f()V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->a:[Ldbh;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ldbh;->d()V

    .line 201
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->b:[[I

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    const/4 v1, 0x4

    aget v0, v0, v1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    .line 202
    return-void
.end method


# virtual methods
.method a()Ldbh;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->a:[Ldbh;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method a(Ltv/periscope/android/event/ServiceEvent;)V
    .locals 3

    .prologue
    .line 155
    sget-object v0, Ltv/periscope/android/ui/broadcast/moderator/k$1;->a:[I

    iget-object v1, p1, Ltv/periscope/android/event/ServiceEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 158
    :pswitch_0
    invoke-virtual {p1}, Ltv/periscope/android/event/ServiceEvent;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    const-string/jumbo v0, "ModeratorStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to complete request: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/android/event/ServiceEvent;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/moderator/k;->d()V

    goto :goto_0

    .line 155
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method a(Ltv/periscope/chatman/model/Ban;)V
    .locals 4

    .prologue
    .line 149
    new-instance v0, Ltv/periscope/android/ui/broadcast/moderator/a;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ltv/periscope/chatman/model/Ban;->a()Ltv/periscope/model/chat/MessageType$SentenceType;

    move-result-object v2

    invoke-virtual {p1}, Ltv/periscope/chatman/model/Ban;->b()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ltv/periscope/android/ui/broadcast/moderator/a;-><init>(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$SentenceType;I)V

    .line 150
    iget-object v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/moderator/b;->a(Ltv/periscope/android/ui/broadcast/moderator/a;)V

    .line 151
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/moderator/k;->f()V

    .line 152
    return-void
.end method

.method a(Ltv/periscope/model/chat/Message;)V
    .locals 3

    .prologue
    .line 129
    const-string/jumbo v0, "ModeratorStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "handleMessage: type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string/jumbo v0, "ModeratorStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "handleMessage: current state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->a:[Ldbh;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Ldbh;->c(Ltv/periscope/model/chat/Message;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    const-string/jumbo v0, "ModeratorStateMachine"

    const-string/jumbo v1, "handleMessage: punishing user"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/moderator/k;->f()V

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->a:[Ldbh;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Ldbh;->a(Ltv/periscope/model/chat/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const-string/jumbo v0, "ModeratorStateMachine"

    const-string/jumbo v1, "handleMessage: handling message"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->a:[Ldbh;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Ldbh;->b(Ltv/periscope/model/chat/Message;)V

    .line 143
    const-string/jumbo v0, "ModeratorStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "handleMessage: previous state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->b:[[I

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    .line 145
    const-string/jumbo v0, "ModeratorStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "handleMessage: new state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method a(Ltv/periscope/model/chat/MessageType$VoteType;)V
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->a:[Ldbh;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Ldbh;->a(Ltv/periscope/model/chat/MessageType$VoteType;)V

    .line 194
    const-string/jumbo v0, "ModeratorStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "userVoted: previous state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->b:[[I

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    const/4 v1, 0x3

    aget v0, v0, v1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    .line 196
    const-string/jumbo v0, "ModeratorStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "userVoted: new state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    return-void
.end method

.method a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcast/moderator/k;->a()Ldbh;

    move-result-object v0

    invoke-virtual {v0}, Ldbh;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method b()Ltv/periscope/android/ui/broadcast/moderator/d;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->e:Ltv/periscope/android/ui/broadcast/moderator/d;

    return-object v0
.end method

.method c()V
    .locals 3

    .prologue
    .line 170
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->f:Ltv/periscope/android/ui/broadcast/moderator/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/moderator/i;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 171
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->a:[Ldbh;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ldbh;->c()V

    .line 172
    const-string/jumbo v0, "ModeratorStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "abort: previous state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->b:[[I

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    .line 174
    const-string/jumbo v0, "ModeratorStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "abort: new state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcast/moderator/k;->f()V

    .line 179
    :cond_0
    return-void
.end method

.method d()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->g:Ltv/periscope/android/ui/broadcast/moderator/g;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/g;->f()V

    .line 183
    return-void
.end method

.method e()V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->a:[Ldbh;

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ldbh;->b()V

    .line 187
    const-string/jumbo v0, "ModeratorStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "timerExpired: previous state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->b:[[I

    iget v1, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    aget-object v0, v0, v1

    const/4 v1, 0x2

    aget v0, v0, v1

    iput v0, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    .line 189
    const-string/jumbo v0, "ModeratorStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "timerExpired: new state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/ui/broadcast/moderator/k;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    return-void
.end method
