.class public Ltv/periscope/android/ui/broadcaster/BroadcasterView;
.super Ltv/periscope/android/view/RootDragLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;
.implements Ltv/periscope/android/util/n$a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcaster/BroadcasterView$a;,
        Ltv/periscope/android/ui/broadcaster/BroadcasterView$b;
    }
.end annotation


# static fields
.field private static final j:J


# instance fields
.field private A:J

.field private B:Z

.field private C:Ltv/periscope/android/util/n$a;

.field private k:Landroid/view/View;

.field private l:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;

.field private m:Landroid/widget/Button;

.field private n:Landroid/view/View;

.field private o:Ltv/periscope/android/ui/broadcast/ChatRoomView;

.field private p:Ltv/periscope/android/ui/broadcaster/CameraPreviewLayout;

.field private q:Landroid/view/View;

.field private r:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

.field private s:Ltv/periscope/android/graphics/GLRenderView;

.field private t:Landroid/view/View;

.field private u:Ljava/lang/Runnable;

.field private v:Ljava/lang/Runnable;

.field private w:Ljava/lang/Runnable;

.field private x:Landroid/graphics/Typeface;

.field private y:Landroid/graphics/Typeface;

.field private z:Ltv/periscope/android/ui/broadcaster/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 39
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->j:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 67
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0, p1, p2, p3}, Ltv/periscope/android/view/RootDragLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    const-string/jumbo v0, "fonts/MuseoSans-500.otf"

    invoke-static {p1, v0}, Ltv/periscope/android/view/z;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->x:Landroid/graphics/Typeface;

    .line 77
    const-string/jumbo v0, "fonts/MuseoSans-700.otf"

    invoke-static {p1, v0}, Ltv/periscope/android/view/z;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->y:Landroid/graphics/Typeface;

    .line 79
    new-instance v0, Ltv/periscope/android/ui/broadcaster/BroadcasterView$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView$1;-><init>(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->u:Ljava/lang/Runnable;

    .line 85
    new-instance v0, Ltv/periscope/android/ui/broadcaster/BroadcasterView$2;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView$2;-><init>(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->v:Ljava/lang/Runnable;

    .line 91
    new-instance v0, Ltv/periscope/android/ui/broadcaster/BroadcasterView$3;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView$3;-><init>(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->w:Ljava/lang/Runnable;

    .line 98
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setDraggable(Z)V

    .line 99
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setFriction(F)V

    .line 100
    invoke-virtual {p0, p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 101
    new-instance v0, Ltv/periscope/android/ui/broadcaster/BroadcasterView$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView$a;-><init>(Ltv/periscope/android/ui/broadcaster/BroadcasterView;Ltv/periscope/android/ui/broadcaster/BroadcasterView$1;)V

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setOnViewDragListener(Ltv/periscope/android/view/RootDragLayout$c;)V

    .line 102
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcaster/BroadcasterView;Landroid/view/View;)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->e(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->t:Landroid/view/View;

    return-object v0
.end method

.method private a(ZILandroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->m:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 253
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->m:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(I)V

    .line 254
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->m:Landroid/widget/Button;

    invoke-virtual {v0, p3}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 255
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->m:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 256
    return-void
.end method

.method static synthetic b(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->i()V

    return-void
.end method

.method static synthetic c(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->j()V

    return-void
.end method

.method static synthetic d(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->B:Z

    return v0
.end method

.method private e(Landroid/view/View;)Landroid/animation/Animator;
    .locals 2

    .prologue
    .line 197
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 198
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/android/view/e;->b(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 199
    new-instance v1, Ltv/periscope/android/ui/broadcaster/BroadcasterView$4;

    invoke-direct {v1, p0, p1, p1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView$4;-><init>(Ltv/periscope/android/ui/broadcaster/BroadcasterView;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 205
    return-object v0

    .line 197
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic e(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->l:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;

    return-object v0
.end method

.method static synthetic f(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->m()V

    return-void
.end method

.method static synthetic g(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->c:I

    return v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setSystemUiVisibility(I)V

    .line 163
    return-void
.end method

.method static synthetic h(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->l()V

    return-void
.end method

.method static synthetic i(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->d:I

    return v0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setSystemUiVisibility(I)V

    .line 167
    return-void
.end method

.method static synthetic j(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)Ltv/periscope/android/ui/broadcaster/a;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->z:Ltv/periscope/android/ui/broadcaster/a;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 178
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 179
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->u:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 180
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 181
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->k()V

    .line 185
    :goto_0
    return-void

    .line 183
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method static synthetic k(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->c:I

    return v0
.end method

.method private k()V
    .locals 4

    .prologue
    .line 188
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->u:Ljava/lang/Runnable;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 189
    return-void
.end method

.method static synthetic l(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->q:Landroid/view/View;

    return-object v0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->u:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 193
    return-void
.end method

.method static synthetic m(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)Ltv/periscope/android/ui/broadcast/ChatRoomView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->o:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    return-object v0
.end method

.method private m()V
    .locals 6

    .prologue
    .line 228
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->z:Ltv/periscope/android/ui/broadcaster/a;

    if-nez v0, :cond_0

    .line 237
    :goto_0
    return-void

    .line 231
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 233
    iget-wide v2, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->A:J

    sub-long v2, v0, v2

    sget-wide v4, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->j:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 234
    iget-object v2, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->z:Ltv/periscope/android/ui/broadcaster/a;

    invoke-interface {v2}, Ltv/periscope/android/ui/broadcaster/a;->h()V

    .line 236
    :cond_1
    iput-wide v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->A:J

    goto :goto_0
.end method

.method static synthetic n(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->d:I

    return v0
.end method

.method static synthetic o(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->h()V

    return-void
.end method

.method static synthetic p(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->e:I

    return v0
.end method

.method static synthetic q(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->c:I

    return v0
.end method

.method static synthetic r(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->k()V

    return-void
.end method

.method static synthetic s(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->d:I

    return v0
.end method

.method static synthetic t(Ltv/periscope/android/ui/broadcaster/BroadcasterView;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->e:I

    return v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 171
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->i()V

    .line 173
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->v:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 174
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->k()V

    .line 175
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 310
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->B:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->m:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getTranslationY()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->m:Landroid/widget/Button;

    neg-int v1, p1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTranslationY(F)V

    .line 313
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->r:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 317
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->B:Z

    if-nez v0, :cond_0

    .line 318
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->m:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTranslationY(F)V

    .line 320
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->r:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->r:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->d(Landroid/view/View;)V

    .line 264
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 275
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 276
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 277
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->k:Landroid/view/View;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->removeView(Landroid/view/View;)V

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->B:Z

    .line 282
    return-void
.end method

.method public getCameraPreview()Ltv/periscope/android/ui/broadcaster/CameraPreviewLayout;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->p:Ltv/periscope/android/ui/broadcaster/CameraPreviewLayout;

    return-object v0
.end method

.method public getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->o:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    return-object v0
.end method

.method public getRenderView()Ltv/periscope/android/graphics/GLRenderView;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->s:Ltv/periscope/android/graphics/GLRenderView;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 148
    invoke-super {p0}, Ltv/periscope/android/view/RootDragLayout;->onAttachedToWindow()V

    .line 149
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->C:Ltv/periscope/android/util/n$a;

    invoke-virtual {v0, p0}, Ltv/periscope/android/util/n$a;->a(Ltv/periscope/android/util/n$a$a;)V

    .line 150
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->z:Ltv/periscope/android/ui/broadcaster/a;

    if-nez v0, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 214
    sget v1, Ltv/periscope/android/library/f$g;->btn_start_broadcast:I

    if-ne v0, v1, :cond_2

    .line 215
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->z:Ltv/periscope/android/ui/broadcaster/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/a;->f()V

    goto :goto_0

    .line 216
    :cond_2
    sget v1, Ltv/periscope/android/library/f$g;->btn_stop_broadcast:I

    if-ne v0, v1, :cond_3

    .line 217
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->z:Ltv/periscope/android/ui/broadcaster/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/a;->g()V

    goto :goto_0

    .line 218
    :cond_3
    sget v1, Ltv/periscope/android/library/f$g;->btn_cameraflip:I

    if-ne v0, v1, :cond_4

    .line 219
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->m()V

    goto :goto_0

    .line 220
    :cond_4
    sget v1, Ltv/periscope/android/library/f$g;->drawer_caret:I

    if-ne v0, v1, :cond_5

    .line 221
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->p:Ltv/periscope/android/ui/broadcaster/CameraPreviewLayout;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->d(Landroid/view/View;)V

    goto :goto_0

    .line 222
    :cond_5
    sget v1, Ltv/periscope/android/library/f$g;->action_button:I

    if-ne v0, v1, :cond_0

    .line 223
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->c()V

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Ltv/periscope/android/view/RootDragLayout;->onDetachedFromWindow()V

    .line 155
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->u:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 156
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->v:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 157
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->w:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 158
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->C:Ltv/periscope/android/util/n$a;

    invoke-virtual {v0, p0}, Ltv/periscope/android/util/n$a;->b(Ltv/periscope/android/util/n$a$a;)V

    .line 159
    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 106
    invoke-super {p0}, Ltv/periscope/android/view/RootDragLayout;->onFinishInflate()V

    .line 108
    sget v0, Ltv/periscope/android/library/f$g;->chatroom_view:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->o:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    .line 109
    sget v0, Ltv/periscope/android/library/f$g;->camera_preview:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcaster/CameraPreviewLayout;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->p:Ltv/periscope/android/ui/broadcaster/CameraPreviewLayout;

    .line 110
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->p:Ltv/periscope/android/ui/broadcaster/CameraPreviewLayout;

    new-instance v1, Ltv/periscope/android/ui/broadcaster/BroadcasterView$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Ltv/periscope/android/ui/broadcaster/BroadcasterView$b;-><init>(Ltv/periscope/android/ui/broadcaster/BroadcasterView;Ltv/periscope/android/ui/broadcaster/BroadcasterView$1;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcaster/CameraPreviewLayout;->setGestureListener(Landroid/view/GestureDetector$SimpleOnGestureListener;)V

    .line 111
    sget v0, Ltv/periscope/android/library/f$g;->stream_details:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->k:Landroid/view/View;

    .line 112
    sget v0, Ltv/periscope/android/library/f$g;->pre_broadcast_details:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->l:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;

    .line 114
    sget v0, Ltv/periscope/android/library/f$g;->playback_controls:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->n:Landroid/view/View;

    .line 115
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->n:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->btn_stop_broadcast:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->n:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->btn_cameraflip:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    sget v0, Ltv/periscope/android/library/f$g;->btn_start_broadcast:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->m:Landroid/widget/Button;

    .line 119
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->m:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    sget v0, Ltv/periscope/android/library/f$g;->drawer_caret:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->q:Landroid/view/View;

    .line 122
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->q:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    sget v0, Ltv/periscope/android/library/f$g;->bottom_drag_child:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->r:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    .line 125
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->r:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, p0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->r:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->setActionButtonVisibility(Z)V

    .line 127
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->r:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->d()V

    .line 128
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->r:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->c(Landroid/view/View;)V

    .line 130
    sget v0, Ltv/periscope/android/library/f$g;->camera_playback:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/graphics/GLRenderView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->s:Ltv/periscope/android/graphics/GLRenderView;

    .line 132
    sget v0, Ltv/periscope/android/library/f$g;->gesture_hints:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->t:Landroid/view/View;

    .line 133
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 134
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->t:Landroid/view/View;

    sget v2, Ltv/periscope/android/library/f$g;->swipe_down_label:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__camera_swipe_down:I

    .line 135
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 136
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->t:Landroid/view/View;

    sget v2, Ltv/periscope/android/library/f$g;->double_tap_label:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Ltv/periscope/android/library/f$l;->ps__camera_double_tap:I

    .line 137
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 140
    sget v0, Ltv/periscope/android/library/f$l;->ps__initializing:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setButtonDisabledMessage(I)V

    .line 142
    new-instance v0, Ltv/periscope/android/util/n$a;

    .line 143
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$e;->ps__keyboard_height_threshold:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-direct {v0, p0, v1}, Ltv/periscope/android/util/n$a;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->C:Ltv/periscope/android/util/n$a;

    .line 144
    return-void
.end method

.method public onSystemUiVisibilityChange(I)V
    .locals 4

    .prologue
    .line 301
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->z:Ltv/periscope/android/ui/broadcaster/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->z:Ltv/periscope/android/ui/broadcaster/a;

    .line 302
    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/a;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 303
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->w:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 304
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->w:Ljava/lang/Runnable;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 306
    :cond_1
    return-void
.end method

.method public setBroadcastInfoAdapter(Ltv/periscope/android/ui/broadcast/g;)V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->r:Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/BroadcastInfoLayout;->setAdapter(Ltv/periscope/android/ui/broadcast/g;)V

    .line 272
    return-void
.end method

.method public setBroadcasterDelegate(Ltv/periscope/android/ui/broadcaster/a;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->z:Ltv/periscope/android/ui/broadcaster/a;

    .line 268
    return-void
.end method

.method public setButtonDisabledMessage(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 244
    const/4 v0, 0x0

    iget-object v1, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->x:Landroid/graphics/Typeface;

    invoke-direct {p0, v0, p1, v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->a(ZILandroid/graphics/Typeface;)V

    .line 245
    return-void
.end method

.method public setButtonEnabledMessage(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 248
    const/4 v0, 0x1

    iget-object v1, p0, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->y:Landroid/graphics/Typeface;

    invoke-direct {p0, v0, p1, v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->a(ZILandroid/graphics/Typeface;)V

    .line 249
    return-void
.end method
