.class public Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;
.super Landroid/widget/LinearLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ltv/periscope/android/ui/broadcaster/prebroadcast/d;


# instance fields
.field final a:Ltv/periscope/android/view/PsImageView;

.field final b:Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

.field c:Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;

.field private final d:Landroid/widget/EditText;

.field private final e:Ltv/periscope/android/view/PsImageView;

.field private final f:Ltv/periscope/android/view/PsImageView;

.field private final g:Ltv/periscope/android/view/PulseAnimationView;

.field private final h:Ltv/periscope/android/view/PsImageView;

.field private final i:Ltv/periscope/android/view/ah;

.field private final j:Landroid/view/View;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/view/View;

.field private final m:Landroid/widget/ImageView;

.field private final n:Landroid/view/View;

.field private final o:I

.field private final p:Ltv/periscope/android/view/y;

.field private q:Ldae;

.field private r:Ljava/lang/Runnable;

.field private s:I

.field private t:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 67
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 75
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    new-instance v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView$1;-><init>(Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;)V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->p:Ltv/periscope/android/view/y;

    .line 77
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__pre_broadcast_details:I

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 78
    invoke-virtual {p0, v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->setOrientation(I)V

    .line 80
    sget v0, Ltv/periscope/android/library/f$g;->edit_broadcast_title:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->d:Landroid/widget/EditText;

    .line 81
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 82
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->d:Landroid/widget/EditText;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->p:Ltv/periscope/android/view/y;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 83
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->p:Ltv/periscope/android/view/y;

    invoke-virtual {v0, v2}, Ltv/periscope/android/view/y;->a(Z)V

    .line 85
    sget v0, Ltv/periscope/android/library/f$g;->broadcast_tip:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->b:Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    .line 86
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->b:Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->b:Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;->setCloseBtnVisibility(I)V

    .line 88
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->b:Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    new-instance v1, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView$2;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView$2;-><init>(Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;->setCloseBtnOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    :cond_0
    sget v0, Ltv/periscope/android/library/f$g;->btn_close:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->e:Ltv/periscope/android/view/PsImageView;

    .line 99
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->e:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v0, p0}, Ltv/periscope/android/view/PsImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    sget v0, Ltv/periscope/android/library/f$g;->location:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->f:Ltv/periscope/android/view/PsImageView;

    .line 102
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->f:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v0, p0}, Ltv/periscope/android/view/PsImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    sget v0, Ltv/periscope/android/library/f$g;->location_container:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PulseAnimationView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->g:Ltv/periscope/android/view/PulseAnimationView;

    .line 105
    sget v0, Ltv/periscope/android/library/f$g;->following_chat:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->h:Ltv/periscope/android/view/PsImageView;

    .line 106
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->h:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v0, p0}, Ltv/periscope/android/view/PsImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    sget v0, Ltv/periscope/android/library/f$g;->tweet:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->a:Ltv/periscope/android/view/PsImageView;

    .line 109
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->a:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v0, p0}, Ltv/periscope/android/view/PsImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    sget v0, Ltv/periscope/android/library/f$g;->provided_location:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->j:Landroid/view/View;

    .line 112
    sget v0, Ltv/periscope/android/library/f$g;->location_name:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->k:Landroid/widget/TextView;

    .line 113
    sget v0, Ltv/periscope/android/library/f$g;->remove_location:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->l:Landroid/view/View;

    .line 114
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->l:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    sget v0, Ltv/periscope/android/library/f$g;->broadcaster_avatar:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->m:Landroid/widget/ImageView;

    .line 117
    sget v0, Ltv/periscope/android/library/f$g;->audience_header_container:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->n:Landroid/view/View;

    .line 118
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->n:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 121
    new-instance v1, Ltv/periscope/android/view/ah;

    invoke-direct {v1, p1}, Ltv/periscope/android/view/ah;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->i:Ltv/periscope/android/view/ah;

    .line 122
    iget-object v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->i:Ltv/periscope/android/view/ah;

    sget v2, Ltv/periscope/android/library/f$e;->ps__tooltip_max_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ltv/periscope/android/view/ah;->a(I)V

    .line 123
    sget v1, Ltv/periscope/android/library/f$e;->ps__tooltip_prebroadcast_offset:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->o:I

    .line 124
    return-void
.end method

.method private a(Z)F
    .locals 1

    .prologue
    .line 351
    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    const v0, 0x3e99999a    # 0.3f

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->j:Landroid/view/View;

    return-object v0
.end method

.method private a(Ltv/periscope/android/view/PsImageView;ZZZ)V
    .locals 2

    .prologue
    .line 333
    invoke-virtual {p1, p2}, Ltv/periscope/android/view/PsImageView;->setActivated(Z)V

    .line 334
    invoke-direct {p0, p2}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->a(Z)F

    move-result v0

    invoke-virtual {p1, v0}, Ltv/periscope/android/view/PsImageView;->setAlpha(F)V

    .line 336
    if-eqz p3, :cond_0

    .line 337
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Ltv/periscope/android/view/PsImageView;->getToolTipOnText()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 338
    const/high16 v1, -0x1000000

    invoke-virtual {p0, p1, v0, v1, p4}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->a(Ltv/periscope/android/view/PsImageView;Ljava/lang/String;IZ)V

    .line 340
    :cond_0
    return-void

    .line 337
    :cond_1
    invoke-virtual {p1}, Ltv/periscope/android/view/PsImageView;->getToolTipOffText()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 158
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->b:Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    if-nez v0, :cond_0

    .line 286
    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->b:Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;->setHtmlText(Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->b:Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;->setVisibility(I)V

    .line 285
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->p:Ltv/periscope/android/view/y;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/y;->a(Z)V

    goto :goto_0
.end method

.method a(Ltv/periscope/android/view/PsImageView;Ljava/lang/String;IZ)V
    .locals 6
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 344
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->i:Ltv/periscope/android/view/ah;

    invoke-virtual {v0}, Ltv/periscope/android/view/ah;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->i:Ltv/periscope/android/view/ah;

    invoke-virtual {v0}, Ltv/periscope/android/view/ah;->b()V

    .line 347
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->i:Ltv/periscope/android/view/ah;

    iget v5, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->o:I

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Ltv/periscope/android/view/ah;->a(Landroid/view/View;Ljava/lang/CharSequence;IZI)V

    .line 348
    return-void
.end method

.method public a(ZZ)V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->f:Ltv/periscope/android/view/PsImageView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->a(Ltv/periscope/android/view/PsImageView;ZZZ)V

    .line 207
    if-eqz p1, :cond_0

    .line 208
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->g:Ltv/periscope/android/view/PulseAnimationView;

    invoke-virtual {v0}, Ltv/periscope/android/view/PulseAnimationView;->a()V

    .line 212
    :goto_0
    return-void

    .line 210
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->g:Ltv/periscope/android/view/PulseAnimationView;

    invoke-virtual {v0}, Ltv/periscope/android/view/PulseAnimationView;->b()V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView$3;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView$3;-><init>(Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 168
    return-void
.end method

.method public b(ZZ)V
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->h:Ltv/periscope/android/view/PsImageView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->a(Ltv/periscope/android/view/PsImageView;ZZZ)V

    .line 217
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->h:Ltv/periscope/android/view/PsImageView;

    iget v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->t:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsImageView;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->a:Ltv/periscope/android/view/PsImageView;

    iget v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->s:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsImageView;->setVisibility(I)V

    .line 266
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 270
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->h:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsImageView;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->a:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsImageView;->setVisibility(I)V

    .line 272
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->i:Ltv/periscope/android/view/ah;

    invoke-virtual {v0}, Ltv/periscope/android/view/ah;->b()V

    .line 273
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->r:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->r:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 276
    :cond_0
    return-void
.end method

.method public e()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 291
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->b:Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    if-nez v0, :cond_0

    .line 309
    :goto_0
    return-void

    .line 294
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->b:Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 295
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->b:Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;->clearAnimation()V

    .line 296
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->b:Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/view/BroadcastTipView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 297
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 298
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 299
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 300
    new-instance v1, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView$4;

    invoke-direct {v1, p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView$4;-><init>(Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 308
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->p:Ltv/periscope/android/view/y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/y;->a(Z)V

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitleView()Landroid/view/View;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->d:Landroid/widget/EditText;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->c:Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;

    if-nez v0, :cond_1

    .line 330
    :cond_0
    :goto_0
    return-void

    .line 317
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->f:Ltv/periscope/android/view/PsImageView;

    if-ne p1, v0, :cond_2

    .line 318
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->c:Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;->p()V

    goto :goto_0

    .line 319
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->h:Ltv/periscope/android/view/PsImageView;

    if-ne p1, v0, :cond_3

    .line 320
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->c:Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;->q()V

    goto :goto_0

    .line 321
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->a:Ltv/periscope/android/view/PsImageView;

    if-ne p1, v0, :cond_4

    .line 322
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->c:Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;->r()V

    goto :goto_0

    .line 323
    :cond_4
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->n:Landroid/view/View;

    if-ne p1, v0, :cond_5

    .line 324
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->c:Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;->s()V

    goto :goto_0

    .line 325
    :cond_5
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->e:Ltv/periscope/android/view/PsImageView;

    if-ne p1, v0, :cond_6

    .line 326
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->c:Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;->o()V

    goto :goto_0

    .line 327
    :cond_6
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->l:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 328
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->c:Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;->t()V

    goto :goto_0
.end method

.method public setAudienceSelectionVisibility(I)V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->n:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 260
    return-void
.end method

.method public setAvatar(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->q:Ldae;

    if-eqz v0, :cond_0

    invoke-static {p1}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->m:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->q:Ldae;

    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->m:Landroid/widget/ImageView;

    invoke-interface {v0, v1, p1, v2}, Ldae;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 176
    :cond_0
    return-void
.end method

.method public setCloseIconPosition(Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 180
    sget v0, Ltv/periscope/android/library/f$g;->btn_container:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 182
    iget-object v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->n:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 183
    iget-object v2, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->e:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v2}, Ltv/periscope/android/view/PsImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 184
    sget-object v3, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView$5;->a:[I

    invoke-virtual {p1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 196
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->e:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v0, v5}, Ltv/periscope/android/view/PsImageView;->setVisibility(I)V

    .line 197
    return-void

    .line 186
    :pswitch_0
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Ltv/periscope/android/library/f$e;->ps__drawable_padding:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    neg-int v3, v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 187
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 188
    sget v0, Ltv/periscope/android/library/f$g;->btn_container:I

    invoke-virtual {v1, v5, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0

    .line 191
    :pswitch_1
    invoke-virtual {p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Ltv/periscope/android/library/f$e;->ps__drawable_padding:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    neg-int v3, v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 192
    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 193
    const/4 v0, 0x1

    sget v2, Ltv/periscope/android/library/f$g;->btn_container:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0

    .line 184
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setFollowingChatVisibility(I)V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->h:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/PsImageView;->setVisibility(I)V

    .line 254
    iput p1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->t:I

    .line 255
    return-void
.end method

.method public setImageUrlLoader(Ldae;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->q:Ldae;

    .line 134
    return-void
.end method

.method public setListener(Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->c:Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;

    .line 129
    return-void
.end method

.method public setLocationName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    return-void
.end method

.method public setLocationVisibility(I)V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->f:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/PsImageView;->setVisibility(I)V

    .line 249
    return-void
.end method

.method public setMaxTitleChars(I)V
    .locals 4

    .prologue
    .line 201
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->d:Landroid/widget/EditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v3, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 202
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->p:Ltv/periscope/android/view/y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/y;->a(Z)V

    .line 139
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->d:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->p:Ltv/periscope/android/view/y;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/y;->a(Z)V

    .line 142
    return-void
.end method

.method public setTwitterVisibility(I)V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->a:Ltv/periscope/android/view/PsImageView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/PsImageView;->setVisibility(I)V

    .line 243
    iput p1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;->s:I

    .line 244
    return-void
.end method
