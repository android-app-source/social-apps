.class public final enum Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CloseIconPosition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

.field public static final enum b:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

.field private static final synthetic c:[Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10
    new-instance v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    const-string/jumbo v1, "Right"

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;->a:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    new-instance v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    const-string/jumbo v1, "Left"

    invoke-direct {v0, v1, v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;->b:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    .line 9
    const/4 v0, 0x2

    new-array v0, v0, [Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    sget-object v1, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;->a:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    aput-object v1, v0, v2

    sget-object v1, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;->b:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    aput-object v1, v0, v3

    sput-object v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;->c:[Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;
    .locals 1

    .prologue
    .line 9
    const-class v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    return-object v0
.end method

.method public static values()[Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;->c:[Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    invoke-virtual {v0}, [Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    return-object v0
.end method
