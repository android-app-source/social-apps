.class public interface abstract Ltv/periscope/android/ui/broadcaster/prebroadcast/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;
    }
.end annotation


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(ZZ)V
.end method

.method public abstract b()V
.end method

.method public abstract b(ZZ)V
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract setAudienceSelectionVisibility(I)V
.end method

.method public abstract setAvatar(Ljava/lang/String;)V
.end method

.method public abstract setCloseIconPosition(Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;)V
.end method

.method public abstract setFollowingChatVisibility(I)V
.end method

.method public abstract setImageUrlLoader(Ldae;)V
.end method

.method public abstract setListener(Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;)V
.end method

.method public abstract setLocationName(Ljava/lang/String;)V
.end method

.method public abstract setLocationVisibility(I)V
.end method

.method public abstract setMaxTitleChars(I)V
.end method

.method public abstract setTitle(Ljava/lang/String;)V
.end method

.method public abstract setTwitterVisibility(I)V
.end method
