.class Ltv/periscope/android/ui/broadcaster/prebroadcast/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/broadcaster/prebroadcast/a;
.implements Ltv/periscope/android/ui/user/UserPickerSheet$a;
.implements Ltv/periscope/android/ui/user/a$a;
.implements Ltv/periscope/android/ui/user/a$b;


# static fields
.field static final a:Ljava/util/ArrayList;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/app/Activity;

.field private final c:Lczq;

.field private final d:Ltv/periscope/android/ui/user/a;

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

.field private final h:Ltv/periscope/android/ui/broadcast/p;

.field private final i:Ldae;

.field private j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

.field private k:Ltv/periscope/android/ui/broadcaster/prebroadcast/a$c;

.field private l:Ltv/periscope/android/ui/broadcaster/prebroadcast/a$a;

.field private m:Lcxr;

.field private n:Ltv/periscope/android/ui/broadcaster/prebroadcast/a$b;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Ldcd;

.field private u:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a:Ljava/util/ArrayList;

    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lczq;Ltv/periscope/android/ui/user/a;Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;Ltv/periscope/android/ui/broadcast/p;Ldae;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->e:Ljava/util/ArrayList;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->f:Ljava/util/ArrayList;

    .line 65
    iput-object p1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->b:Landroid/app/Activity;

    .line 66
    iput-object p2, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->c:Lczq;

    .line 67
    iput-object p3, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->d:Ltv/periscope/android/ui/user/a;

    .line 68
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->d:Ltv/periscope/android/ui/user/a;

    invoke-interface {v0, p0}, Ltv/periscope/android/ui/user/a;->a(Ltv/periscope/android/ui/user/a$b;)V

    .line 69
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->d:Ltv/periscope/android/ui/user/a;

    invoke-interface {v0, p0}, Ltv/periscope/android/ui/user/a;->a(Ltv/periscope/android/ui/user/a$a;)V

    .line 70
    iput-object p4, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    .line 71
    iput-object p5, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->h:Ltv/periscope/android/ui/broadcast/p;

    .line 72
    iput-object p6, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->i:Ldae;

    .line 73
    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 323
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    if-nez v0, :cond_0

    .line 337
    :goto_0
    return-void

    .line 327
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v0, v3

    .line 328
    if-lez v0, :cond_1

    move v0, v1

    .line 329
    :goto_1
    iput-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->o:Z

    .line 330
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->p:Z

    .line 332
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->o:Z

    if-eqz v0, :cond_3

    .line 333
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->d()V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 328
    goto :goto_1

    :cond_2
    move v1, v2

    .line 330
    goto :goto_2

    .line 335
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->c()V

    goto :goto_0
.end method

.method private a(ZZ)V
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    if-nez v0, :cond_0

    .line 371
    :goto_0
    return-void

    .line 360
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->a(ZZ)V

    .line 361
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->r:Z

    .line 362
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->r:Z

    if-eqz v0, :cond_1

    .line 365
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->c:Lczq;

    invoke-virtual {v0}, Lczq;->f()V

    goto :goto_0

    .line 369
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->c:Lczq;

    invoke-virtual {v0}, Lczq;->g()V

    goto :goto_0
.end method

.method private a(ZZZJ)V
    .locals 8

    .prologue
    .line 408
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    if-nez v0, :cond_0

    .line 413
    :goto_0
    return-void

    .line 412
    :cond_0
    iget-object v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    move v3, p1

    move v4, p2

    move v5, p3

    move-wide v6, p4

    invoke-interface/range {v1 .. v7}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->a(Ltv/periscope/android/ui/broadcaster/prebroadcast/d;ZZZJ)V

    goto :goto_0
.end method

.method private b(ZZ)V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    if-nez v0, :cond_0

    .line 386
    :goto_0
    return-void

    .line 384
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->b(ZZ)V

    .line 385
    iput-boolean p1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->s:Z

    goto :goto_0
.end method

.method private u()V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->m:Lcxr;

    if-nez v0, :cond_1

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->e()V

    .line 304
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->t:Ldcd;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->m:Lcxr;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->t:Ldcd;

    invoke-virtual {v1}, Ldcd;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcxr;->a(Ljava/lang/String;)V

    .line 306
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->t:Ldcd;

    goto :goto_0
.end method


# virtual methods
.method public a(Lcxr;)V
    .locals 3

    .prologue
    .line 217
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->t:Ldcd;

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    if-nez v0, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iput-object p1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->m:Lcxr;

    .line 221
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->m:Lcxr;

    invoke-virtual {v0}, Lcxr;->b()Ldcd;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->t:Ldcd;

    .line 222
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->t:Ldcd;

    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->b:Landroid/app/Activity;

    iget-object v2, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->t:Ldcd;

    invoke-virtual {v2}, Ldcd;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->a(Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->t:Ldcd;

    invoke-virtual {v0}, Ldcd;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->u:Ljava/lang/String;

    goto :goto_0

    .line 226
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->e()V

    .line 227
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->u:Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-virtual {p0, p1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a(Ltv/periscope/android/ui/broadcaster/prebroadcast/d;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    if-nez v0, :cond_0

    .line 176
    :goto_0
    return-void

    .line 170
    :cond_0
    invoke-static {p1}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->setLocationName(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->a()V

    goto :goto_0

    .line 174
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->b()V

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;ZZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 122
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 123
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/user/UserItem;

    .line 124
    invoke-interface {v0}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v2

    sget-object v3, Ltv/periscope/model/user/UserItem$Type;->c:Ltv/periscope/model/user/UserItem$Type;

    if-ne v2, v3, :cond_1

    .line 125
    iget-object v2, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->e:Ljava/util/ArrayList;

    check-cast v0, Ltv/periscope/model/user/e;

    invoke-virtual {v0}, Ltv/periscope/model/user/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 126
    :cond_1
    invoke-interface {v0}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v2

    sget-object v3, Ltv/periscope/model/user/UserItem$Type;->i:Ltv/periscope/model/user/UserItem$Type;

    if-ne v2, v3, :cond_0

    .line 127
    iget-object v2, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->f:Ljava/util/ArrayList;

    check-cast v0, Ltv/periscope/model/user/d;

    invoke-virtual {v0}, Ltv/periscope/model/user/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 131
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->e:Ljava/util/ArrayList;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->f:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 132
    iput-boolean p2, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->q:Z

    .line 133
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->k:Ltv/periscope/android/ui/broadcaster/prebroadcast/a$c;

    if-eqz v0, :cond_3

    .line 134
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->e:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 135
    iget-object v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 136
    iget-object v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->k:Ltv/periscope/android/ui/broadcaster/prebroadcast/a$c;

    invoke-interface {v1, v0, p2}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a$c;->a(Ljava/util/List;Z)V

    .line 138
    :cond_3
    return-void
.end method

.method public a(Ltv/periscope/android/ui/broadcaster/prebroadcast/a$a;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->l:Ltv/periscope/android/ui/broadcaster/prebroadcast/a$a;

    .line 112
    return-void
.end method

.method public a(Ltv/periscope/android/ui/broadcaster/prebroadcast/a$b;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->n:Ltv/periscope/android/ui/broadcaster/prebroadcast/a$b;

    .line 117
    return-void
.end method

.method public a(Ltv/periscope/android/ui/broadcaster/prebroadcast/a$c;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->k:Ltv/periscope/android/ui/broadcaster/prebroadcast/a$c;

    .line 107
    return-void
.end method

.method public a(Ltv/periscope/android/ui/broadcaster/prebroadcast/d;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    iput-object p1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    .line 78
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0, p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->setListener(Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;)V

    .line 79
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->i:Ldae;

    invoke-interface {v0, v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->setImageUrlLoader(Ldae;)V

    .line 80
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    invoke-interface {v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->b()I

    move-result v3

    invoke-interface {v0, v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->setTwitterVisibility(I)V

    .line 81
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    invoke-interface {v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->d()I

    move-result v3

    invoke-interface {v0, v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->setLocationVisibility(I)V

    .line 82
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    invoke-interface {v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->e()I

    move-result v3

    invoke-interface {v0, v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->setFollowingChatVisibility(I)V

    .line 83
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    invoke-interface {v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->f()I

    move-result v3

    invoke-interface {v0, v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->setAudienceSelectionVisibility(I)V

    .line 84
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    invoke-interface {v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->h()Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;

    move-result-object v3

    invoke-interface {v0, v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->setCloseIconPosition(Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate$CloseIconPosition;)V

    .line 85
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    iget-object v3, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    invoke-interface {v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->i()I

    move-result v3

    invoke-interface {v0, v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->setMaxTitleChars(I)V

    .line 87
    sget-object v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a:Ljava/util/ArrayList;

    sget-object v3, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 88
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->h:Ltv/periscope/android/ui/broadcast/p;

    iget-boolean v0, v0, Ltv/periscope/android/ui/broadcast/p;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->c:Lczq;

    invoke-virtual {v0}, Lczq;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v2}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a(ZZ)V

    .line 89
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->h:Ltv/periscope/android/ui/broadcast/p;

    iget-boolean v0, v0, Ltv/periscope/android/ui/broadcast/p;->b:Z

    invoke-direct {p0, v0, v2}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->b(ZZ)V

    .line 92
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->h:Ltv/periscope/android/ui/broadcast/p;

    iget-boolean v0, v0, Ltv/periscope/android/ui/broadcast/p;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    .line 93
    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->a()Z

    move-result v2

    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->a()Z

    move-result v3

    const-wide/16 v4, 0x96

    move-object v0, p0

    .line 92
    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a(ZZZJ)V

    .line 94
    return-void

    :cond_1
    move v0, v2

    .line 88
    goto :goto_0

    :cond_2
    move v1, v2

    .line 92
    goto :goto_1
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 157
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a(ZZ)V

    .line 158
    if-eqz p1, :cond_0

    .line 161
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->b:Landroid/app/Activity;

    sget v2, Ltv/periscope/android/library/f$l;->ps__location_settings_off:I

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 163
    :cond_0
    return-void

    .line 157
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->c:Lczq;

    invoke-virtual {v0}, Lczq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    if-nez v0, :cond_0

    .line 184
    :goto_0
    return-void

    .line 183
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->setAvatar(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 243
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->o:Z

    return v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    if-nez v0, :cond_0

    .line 192
    :goto_0
    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->p:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 253
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->o:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->q:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->o:Z

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->e:Ljava/util/ArrayList;

    .line 261
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public f()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 266
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->p:Z

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->f:Ljava/util/ArrayList;

    .line 269
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 277
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->s:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 284
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->u:Ljava/lang/String;

    return-object v0
.end method

.method public j()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 98
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->setListener(Ltv/periscope/android/ui/broadcaster/prebroadcast/d$a;)V

    .line 100
    iput-object v1, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    .line 102
    :cond_0
    return-void
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    if-nez v0, :cond_0

    .line 198
    const/4 v0, 0x0

    .line 200
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->j:Ltv/periscope/android/ui/broadcaster/prebroadcast/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/d;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public l()V
    .locals 0

    .prologue
    .line 233
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->u()V

    .line 234
    return-void
.end method

.method public m()V
    .locals 0

    .prologue
    .line 295
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->u()V

    .line 296
    return-void
.end method

.method public n()V
    .locals 0

    .prologue
    .line 312
    invoke-direct {p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->u()V

    .line 313
    return-void
.end method

.method public o()V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->l:Ltv/periscope/android/ui/broadcaster/prebroadcast/a$a;

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->l:Ltv/periscope/android/ui/broadcaster/prebroadcast/a$a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a$a;->q()V

    .line 320
    :cond_0
    return-void
.end method

.method public p()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 342
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->c:Lczq;

    invoke-virtual {v0}, Lczq;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->c:Lczq;

    invoke-virtual {v0}, Lczq;->b()Z

    move-result v0

    .line 345
    if-nez v0, :cond_0

    .line 346
    invoke-virtual {p0, v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a(Z)V

    .line 353
    :goto_0
    return-void

    .line 352
    :cond_0
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->r:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a(ZZ)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public q()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 376
    iget-boolean v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->s:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->b(ZZ)V

    .line 377
    return-void

    .line 376
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 391
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->g:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->c()Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    :goto_0
    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->a(ZZZJ)V

    .line 392
    return-void

    :cond_0
    move v1, v3

    .line 391
    goto :goto_0
.end method

.method public s()V
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->d:Ltv/periscope/android/ui/user/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/user/a;->a()V

    .line 397
    sget-object v0, Ltv/periscope/android/analytics/Event;->br:Ltv/periscope/android/analytics/Event;

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/Event;)V

    .line 398
    return-void
.end method

.method public t()V
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->n:Ltv/periscope/android/ui/broadcaster/prebroadcast/a$b;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Ltv/periscope/android/ui/broadcaster/prebroadcast/c;->n:Ltv/periscope/android/ui/broadcaster/prebroadcast/a$b;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a$b;->r()V

    .line 405
    :cond_0
    return-void
.end method
