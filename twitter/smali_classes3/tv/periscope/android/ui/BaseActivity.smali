.class public abstract Ltv/periscope/android/ui/BaseActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "Twttr"


# instance fields
.field private a:Lcrw;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 69
    invoke-virtual {p0}, Ltv/periscope/android/ui/BaseActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    invoke-virtual {p0}, Ltv/periscope/android/ui/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 72
    invoke-virtual {p0}, Ltv/periscope/android/ui/BaseActivity;->c()Ljava/lang/String;

    move-result-object v0

    .line 74
    const-string/jumbo v2, "e_previous_screen"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75
    const-string/jumbo v0, "e_previous_screen"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    :cond_0
    invoke-virtual {p0, v0}, Ltv/periscope/android/ui/BaseActivity;->a(Ljava/lang/String;)Lcrw;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/ui/BaseActivity;->a:Lcrw;

    .line 79
    :cond_1
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;)Lcrw;
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcrx;

    invoke-direct {v0}, Lcrx;-><init>()V

    return-object v0
.end method

.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string/jumbo v0, ""

    return-object v0
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Ltv/periscope/android/ui/BaseActivity;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ltv/periscope/android/ui/BaseActivity;->a:Lcrw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/ui/BaseActivity;->a:Lcrw;

    invoke-virtual {v0}, Lcrw;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    invoke-direct {p0}, Ltv/periscope/android/ui/BaseActivity;->e()V

    .line 25
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 46
    iget-object v0, p0, Ltv/periscope/android/ui/BaseActivity;->a:Lcrw;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "e_previous_screen"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const-string/jumbo v0, "e_previous_screen"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    iget-object v1, p0, Ltv/periscope/android/ui/BaseActivity;->a:Lcrw;

    invoke-virtual {v1, v0}, Lcrw;->b(Ljava/lang/String;)V

    .line 50
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    .line 38
    iget-object v0, p0, Ltv/periscope/android/ui/BaseActivity;->a:Lcrw;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ltv/periscope/android/ui/BaseActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Ltv/periscope/android/ui/BaseActivity;->a:Lcrw;

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->a(Lcrz;)V

    .line 41
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 29
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 30
    const-string/jumbo v0, "activity"

    invoke-virtual {p0}, Ltv/periscope/android/ui/BaseActivity;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ltv/periscope/android/ui/BaseActivity;->e()V

    .line 32
    return-void
.end method
