.class public Ltv/periscope/android/network/ConnectivityChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lde/greenrobot/event/c;

.field private final c:Landroid/content/IntentFilter;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lde/greenrobot/event/c;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 26
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltv/periscope/android/network/ConnectivityChangeReceiver;->a:Ljava/lang/ref/WeakReference;

    .line 27
    iput-object p2, p0, Ltv/periscope/android/network/ConnectivityChangeReceiver;->b:Lde/greenrobot/event/c;

    .line 28
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ltv/periscope/android/network/ConnectivityChangeReceiver;->c:Landroid/content/IntentFilter;

    .line 29
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Ltv/periscope/android/network/ConnectivityChangeReceiver;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 33
    if-nez v0, :cond_1

    .line 47
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    iget-boolean v1, p0, Ltv/periscope/android/network/ConnectivityChangeReceiver;->d:Z

    if-nez v1, :cond_0

    .line 41
    :try_start_0
    iget-object v1, p0, Ltv/periscope/android/network/ConnectivityChangeReceiver;->c:Landroid/content/IntentFilter;

    invoke-virtual {v0, p0, v1}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/network/ConnectivityChangeReceiver;->d:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    const-string/jumbo v1, "ConnectivityChangeReceiver"

    const-string/jumbo v2, "Receiver registration failed"

    invoke-static {v1, v2, v0}, Ltv/periscope/android/util/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Ltv/periscope/android/network/ConnectivityChangeReceiver;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 51
    if-nez v0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    iget-boolean v1, p0, Ltv/periscope/android/network/ConnectivityChangeReceiver;->d:Z

    if-eqz v1, :cond_0

    .line 56
    :try_start_0
    invoke-virtual {v0, p0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/network/ConnectivityChangeReceiver;->d:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    const-string/jumbo v1, "ConnectivityChangeReceiver"

    const-string/jumbo v2, "Unregistering receiver failed"

    invoke-static {v1, v2, v0}, Ltv/periscope/android/util/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 66
    const-string/jumbo v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Ltv/periscope/android/network/ConnectivityChangeReceiver;->b:Lde/greenrobot/event/c;

    new-instance v1, Ltv/periscope/android/network/NetworkMonitorInfo;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v1, v2}, Ltv/periscope/android/network/NetworkMonitorInfo;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 69
    :cond_0
    return-void
.end method
