.class public final Ltv/periscope/android/library/f$d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/library/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f11019d

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f11019e

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f11019f

.field public static final abc_color_highlight_material:I = 0x7f1101a0

.field public static final abc_input_method_navigation_guard:I = 0x7f110024

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f1101a1

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f1101a2

.field public static final abc_primary_text_material_dark:I = 0x7f1101a3

.field public static final abc_primary_text_material_light:I = 0x7f1101a4

.field public static final abc_search_url_text:I = 0x7f1101a5

.field public static final abc_search_url_text_normal:I = 0x7f110025

.field public static final abc_search_url_text_pressed:I = 0x7f110026

.field public static final abc_search_url_text_selected:I = 0x7f110027

.field public static final abc_secondary_text_material_dark:I = 0x7f1101a6

.field public static final abc_secondary_text_material_light:I = 0x7f1101a7

.field public static final abc_tint_btn_checkable:I = 0x7f1101a8

.field public static final abc_tint_default:I = 0x7f1101a9

.field public static final abc_tint_edittext:I = 0x7f1101aa

.field public static final abc_tint_seek_thumb:I = 0x7f1101ab

.field public static final abc_tint_spinner:I = 0x7f1101ac

.field public static final abc_tint_switch_thumb:I = 0x7f1101ad

.field public static final abc_tint_switch_track:I = 0x7f1101ae

.field public static final accent_material_dark:I = 0x7f110028

.field public static final accent_material_light:I = 0x7f110029

.field public static final background_floating_material_dark:I = 0x7f11002c

.field public static final background_floating_material_light:I = 0x7f11002d

.field public static final background_material_dark:I = 0x7f11002e

.field public static final background_material_light:I = 0x7f11002f

.field public static final bright_foreground_disabled_material_dark:I = 0x7f110036

.field public static final bright_foreground_disabled_material_light:I = 0x7f110037

.field public static final bright_foreground_inverse_material_dark:I = 0x7f110038

.field public static final bright_foreground_inverse_material_light:I = 0x7f110039

.field public static final bright_foreground_material_dark:I = 0x7f11003a

.field public static final bright_foreground_material_light:I = 0x7f11003b

.field public static final button_material_dark:I = 0x7f110047

.field public static final button_material_light:I = 0x7f110048

.field public static final common_google_signin_btn_text_dark:I = 0x7f1101b9

.field public static final common_google_signin_btn_text_dark_default:I = 0x7f110050

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f110051

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f110052

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f110053

.field public static final common_google_signin_btn_text_light:I = 0x7f1101ba

.field public static final common_google_signin_btn_text_light_default:I = 0x7f110054

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f110055

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f110056

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f110057

.field public static final common_plus_signin_btn_text_dark:I = 0x7f1101bb

.field public static final common_plus_signin_btn_text_dark_default:I = 0x7f110058

.field public static final common_plus_signin_btn_text_dark_disabled:I = 0x7f110059

.field public static final common_plus_signin_btn_text_dark_focused:I = 0x7f11005a

.field public static final common_plus_signin_btn_text_dark_pressed:I = 0x7f11005b

.field public static final common_plus_signin_btn_text_light:I = 0x7f1101bc

.field public static final common_plus_signin_btn_text_light_default:I = 0x7f11005c

.field public static final common_plus_signin_btn_text_light_disabled:I = 0x7f11005d

.field public static final common_plus_signin_btn_text_light_focused:I = 0x7f11005e

.field public static final common_plus_signin_btn_text_light_pressed:I = 0x7f11005f

.field public static final dim_foreground_disabled_material_dark:I = 0x7f110081

.field public static final dim_foreground_disabled_material_light:I = 0x7f110082

.field public static final dim_foreground_material_dark:I = 0x7f110083

.field public static final dim_foreground_material_light:I = 0x7f110084

.field public static final foreground_material_dark:I = 0x7f11009a

.field public static final foreground_material_light:I = 0x7f11009b

.field public static final highlighted_text_material_dark:I = 0x7f1100a7

.field public static final highlighted_text_material_light:I = 0x7f1100a8

.field public static final hint_foreground_material_dark:I = 0x7f1100b3

.field public static final hint_foreground_material_light:I = 0x7f1100b4

.field public static final material_blue_grey_800:I = 0x7f1100cc

.field public static final material_blue_grey_900:I = 0x7f1100cd

.field public static final material_blue_grey_950:I = 0x7f1100ce

.field public static final material_deep_teal_200:I = 0x7f1100cf

.field public static final material_deep_teal_500:I = 0x7f1100d0

.field public static final material_grey_100:I = 0x7f1100d1

.field public static final material_grey_300:I = 0x7f1100d2

.field public static final material_grey_50:I = 0x7f1100d3

.field public static final material_grey_600:I = 0x7f1100d4

.field public static final material_grey_800:I = 0x7f1100d5

.field public static final material_grey_850:I = 0x7f1100d6

.field public static final material_grey_900:I = 0x7f1100d7

.field public static final place_autocomplete_prediction_primary_text:I = 0x7f110111

.field public static final place_autocomplete_prediction_primary_text_highlight:I = 0x7f110112

.field public static final place_autocomplete_prediction_secondary_text:I = 0x7f110113

.field public static final place_autocomplete_search_hint:I = 0x7f110114

.field public static final place_autocomplete_search_text:I = 0x7f110115

.field public static final place_autocomplete_separator:I = 0x7f110116

.field public static final primary_dark_material_dark:I = 0x7f110117

.field public static final primary_dark_material_light:I = 0x7f110118

.field public static final primary_material_dark:I = 0x7f110119

.field public static final primary_material_light:I = 0x7f11011a

.field public static final primary_text_default_material_dark:I = 0x7f11011c

.field public static final primary_text_default_material_light:I = 0x7f11011d

.field public static final primary_text_disabled_material_dark:I = 0x7f11011e

.field public static final primary_text_disabled_material_light:I = 0x7f11011f

.field public static final ps__app_background:I = 0x7f110120

.field public static final ps__app_background_secondary:I = 0x7f110121

.field public static final ps__bg_action_bar_pressable:I = 0x7f1101cd

.field public static final ps__bg_blue:I = 0x7f1101ce

.field public static final ps__bg_button_default:I = 0x7f110122

.field public static final ps__bg_card_outline:I = 0x7f110123

.field public static final ps__bg_heart:I = 0x7f1101cf

.field public static final ps__bg_light_grey:I = 0x7f1101d0

.field public static final ps__bg_moderator_overlay:I = 0x7f110124

.field public static final ps__bg_pre_broadcast:I = 0x7f110125

.field public static final ps__bg_toolbar_btn_pressed:I = 0x7f110126

.field public static final ps__black:I = 0x7f110127

.field public static final ps__black_20:I = 0x7f110128

.field public static final ps__black_25:I = 0x7f110129

.field public static final ps__black_40:I = 0x7f11012a

.field public static final ps__black_50:I = 0x7f11012b

.field public static final ps__black_70:I = 0x7f11012c

.field public static final ps__black_80:I = 0x7f11012d

.field public static final ps__black_scrim:I = 0x7f11012e

.field public static final ps__blue:I = 0x7f11012f

.field public static final ps__border:I = 0x7f110130

.field public static final ps__channel_info_background:I = 0x7f110131

.field public static final ps__collection_cell_background:I = 0x7f110132

.field public static final ps__dark_grey:I = 0x7f110133

.field public static final ps__dark_grey_20:I = 0x7f110134

.field public static final ps__dark_red:I = 0x7f110135

.field public static final ps__featured:I = 0x7f110136

.field public static final ps__grey:I = 0x7f110137

.field public static final ps__hint_text:I = 0x7f110138

.field public static final ps__light_blue:I = 0x7f110139

.field public static final ps__light_grey:I = 0x7f11013a

.field public static final ps__light_grey_20:I = 0x7f11013b

.field public static final ps__light_grey_30:I = 0x7f11013c

.field public static final ps__light_grey_40:I = 0x7f11013d

.field public static final ps__light_grey_50:I = 0x7f11013e

.field public static final ps__light_grey_60:I = 0x7f11013f

.field public static final ps__light_grey_90:I = 0x7f110140

.field public static final ps__live_btn_disabled:I = 0x7f110141

.field public static final ps__live_btn_pressed:I = 0x7f110142

.field public static final ps__main_primary:I = 0x7f110143

.field public static final ps__main_secondary:I = 0x7f110144

.field public static final ps__moderation_button_blue_down:I = 0x7f110145

.field public static final ps__moderation_button_white_down:I = 0x7f110146

.field public static final ps__participant_1:I = 0x7f110147

.field public static final ps__participant_10:I = 0x7f110148

.field public static final ps__participant_11:I = 0x7f110149

.field public static final ps__participant_12:I = 0x7f11014a

.field public static final ps__participant_13:I = 0x7f11014b

.field public static final ps__participant_2:I = 0x7f11014c

.field public static final ps__participant_3:I = 0x7f11014d

.field public static final ps__participant_4:I = 0x7f11014e

.field public static final ps__participant_5:I = 0x7f11014f

.field public static final ps__participant_6:I = 0x7f110150

.field public static final ps__participant_7:I = 0x7f110151

.field public static final ps__participant_8:I = 0x7f110152

.field public static final ps__participant_9:I = 0x7f110153

.field public static final ps__participant_replay:I = 0x7f110154

.field public static final ps__periscope_cta_item_background:I = 0x7f110155

.field public static final ps__primary_text:I = 0x7f110156

.field public static final ps__pyml_rationale:I = 0x7f110157

.field public static final ps__red:I = 0x7f110158

.field public static final ps__red_67:I = 0x7f110159

.field public static final ps__retweet_green:I = 0x7f11015a

.field public static final ps__scrim_center:I = 0x7f11015b

.field public static final ps__scrim_end:I = 0x7f11015c

.field public static final ps__scrim_pressed_center:I = 0x7f11015d

.field public static final ps__scrim_pressed_start:I = 0x7f11015e

.field public static final ps__scrim_start:I = 0x7f11015f

.field public static final ps__secondary_text:I = 0x7f110160

.field public static final ps__section_divider:I = 0x7f110161

.field public static final ps__setting_disabled:I = 0x7f110162

.field public static final ps__splash_primary:I = 0x7f110163

.field public static final ps__splash_secondary:I = 0x7f110164

.field public static final ps__superfan_rank_color:I = 0x7f110165

.field public static final ps__text_activated:I = 0x7f1101d1

.field public static final ps__text_button:I = 0x7f1101d2

.field public static final ps__text_disabled:I = 0x7f110166

.field public static final ps__transparent:I = 0x7f110167

.field public static final ps__twitter_blue:I = 0x7f110168

.field public static final ps__vip_badge_bronze:I = 0x7f110169

.field public static final ps__vip_badge_gold:I = 0x7f11016a

.field public static final ps__vip_badge_silver:I = 0x7f11016b

.field public static final ps__white:I = 0x7f11016c

.field public static final ps__white_20:I = 0x7f11016d

.field public static final ps__white_30:I = 0x7f11016e

.field public static final ps__white_alpha_half:I = 0x7f11016f

.field public static final ps__white_low_alpha:I = 0x7f110170

.field public static final ps__white_tint:I = 0x7f110171

.field public static final ps_moderation_button_red_down:I = 0x7f110172

.field public static final ripple_material_dark:I = 0x7f110174

.field public static final ripple_material_light:I = 0x7f110175

.field public static final secondary_text_default_material_dark:I = 0x7f110178

.field public static final secondary_text_default_material_light:I = 0x7f110179

.field public static final secondary_text_disabled_material_dark:I = 0x7f11017a

.field public static final secondary_text_disabled_material_light:I = 0x7f11017b

.field public static final switch_thumb_disabled_material_dark:I = 0x7f110185

.field public static final switch_thumb_disabled_material_light:I = 0x7f110186

.field public static final switch_thumb_material_dark:I = 0x7f1101d3

.field public static final switch_thumb_material_light:I = 0x7f1101d4

.field public static final switch_thumb_normal_material_dark:I = 0x7f110187

.field public static final switch_thumb_normal_material_light:I = 0x7f110188
