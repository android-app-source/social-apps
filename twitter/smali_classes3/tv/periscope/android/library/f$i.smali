.class public final Ltv/periscope/android/library/f$i;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/library/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "i"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f040001

.field public static final abc_action_bar_up_container:I = 0x7f040002

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f040003

.field public static final abc_action_menu_item_layout:I = 0x7f040004

.field public static final abc_action_menu_layout:I = 0x7f040005

.field public static final abc_action_mode_bar:I = 0x7f040006

.field public static final abc_action_mode_close_item_material:I = 0x7f040007

.field public static final abc_activity_chooser_view:I = 0x7f040008

.field public static final abc_activity_chooser_view_list_item:I = 0x7f040009

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f04000a

.field public static final abc_alert_dialog_material:I = 0x7f04000b

.field public static final abc_dialog_title_material:I = 0x7f04000c

.field public static final abc_expanded_menu_layout:I = 0x7f04000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f04000e

.field public static final abc_list_menu_item_icon:I = 0x7f04000f

.field public static final abc_list_menu_item_layout:I = 0x7f040010

.field public static final abc_list_menu_item_radio:I = 0x7f040011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f040012

.field public static final abc_popup_menu_item_layout:I = 0x7f040013

.field public static final abc_screen_content_include:I = 0x7f040014

.field public static final abc_screen_simple:I = 0x7f040015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f040016

.field public static final abc_screen_toolbar:I = 0x7f040017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f040018

.field public static final abc_search_view:I = 0x7f040019

.field public static final abc_select_dialog_material:I = 0x7f04001a

.field public static final info_window:I = 0x7f040161

.field public static final notification_media_action:I = 0x7f040272

.field public static final notification_media_cancel_action:I = 0x7f040273

.field public static final notification_template_big_media:I = 0x7f040274

.field public static final notification_template_big_media_narrow:I = 0x7f040275

.field public static final notification_template_lines:I = 0x7f040276

.field public static final notification_template_media:I = 0x7f040277

.field public static final notification_template_part_chronometer:I = 0x7f040278

.field public static final notification_template_part_time:I = 0x7f040279

.field public static final place_autocomplete_fragment:I = 0x7f0402a8

.field public static final place_autocomplete_item_powered_by_google:I = 0x7f0402a9

.field public static final place_autocomplete_item_prediction:I = 0x7f0402aa

.field public static final place_autocomplete_progress:I = 0x7f0402ab

.field public static final profile_sheet_bio:I = 0x7f0402e0

.field public static final ps__action_sheet:I = 0x7f0402ea

.field public static final ps__action_sheet_item:I = 0x7f0402eb

.field public static final ps__action_sheet_row:I = 0x7f0402ec

.field public static final ps__audience_selection_divider:I = 0x7f0402ed

.field public static final ps__audience_selection_header_row:I = 0x7f0402ee

.field public static final ps__audience_selection_layout:I = 0x7f0402ef

.field public static final ps__audience_selection_public_row:I = 0x7f0402f0

.field public static final ps__bottom_tray:I = 0x7f0402f1

.field public static final ps__broadcast_action_item:I = 0x7f0402f2

.field public static final ps__broadcast_info_action:I = 0x7f0402f3

.field public static final ps__broadcast_info_layout:I = 0x7f0402f4

.field public static final ps__broadcast_info_map:I = 0x7f0402f5

.field public static final ps__broadcast_info_more:I = 0x7f0402f6

.field public static final ps__broadcast_info_viewer:I = 0x7f0402f7

.field public static final ps__broadcast_started_locally:I = 0x7f0402f8

.field public static final ps__broadcast_stats_graph_view:I = 0x7f0402f9

.field public static final ps__broadcast_stats_line_chart:I = 0x7f0402fa

.field public static final ps__broadcast_stats_list:I = 0x7f0402fb

.field public static final ps__broadcast_stats_main:I = 0x7f0402fc

.field public static final ps__broadcast_stats_show_more:I = 0x7f0402fd

.field public static final ps__broadcast_tip:I = 0x7f0402fe

.field public static final ps__broadcast_title:I = 0x7f0402ff

.field public static final ps__broadcast_viewer_count:I = 0x7f040300

.field public static final ps__broadcaster_blocked:I = 0x7f040301

.field public static final ps__broadcaster_container:I = 0x7f040302

.field public static final ps__broadcaster_view:I = 0x7f040303

.field public static final ps__carousel_chat_row:I = 0x7f040304

.field public static final ps__channel_info_prompt:I = 0x7f040305

.field public static final ps__channel_row_checked:I = 0x7f040306

.field public static final ps__chat_broadcast_tip:I = 0x7f040307

.field public static final ps__chat_row:I = 0x7f040308

.field public static final ps__chat_row_join:I = 0x7f040309

.field public static final ps__chat_row_verdict:I = 0x7f04030a

.field public static final ps__chat_state_dialog:I = 0x7f04030b

.field public static final ps__chat_warning:I = 0x7f04030c

.field public static final ps__chatroom_view:I = 0x7f04030d

.field public static final ps__chats_container_layout:I = 0x7f04030e

.field public static final ps__content_loading:I = 0x7f04030f

.field public static final ps__create_channel_row:I = 0x7f040310

.field public static final ps__dialog_simple_list_item:I = 0x7f040311

.field public static final ps__featured_summary_contianer:I = 0x7f040312

.field public static final ps__friends_watching:I = 0x7f040313

.field public static final ps__friends_watching_cell:I = 0x7f040314

.field public static final ps__fuzzy_ball:I = 0x7f040315

.field public static final ps__fuzzy_balls_import:I = 0x7f040316

.field public static final ps__interstitial_activity:I = 0x7f040317

.field public static final ps__learn_about_moderation_dialog:I = 0x7f040318

.field public static final ps__learn_about_moderation_dialog_title:I = 0x7f040319

.field public static final ps__list_divider:I = 0x7f04031a

.field public static final ps__local_prompt_with_icon:I = 0x7f04031b

.field public static final ps__marker_pop_up:I = 0x7f04031c

.field public static final ps__moderator_container:I = 0x7f04031d

.field public static final ps__moderator_overlay:I = 0x7f04031e

.field public static final ps__periscope_cta_info_item:I = 0x7f04031f

.field public static final ps__periscope_player:I = 0x7f040320

.field public static final ps__permissions:I = 0x7f040321

.field public static final ps__placeholder_row:I = 0x7f040322

.field public static final ps__play_view:I = 0x7f040323

.field public static final ps__pre_broadcast_details:I = 0x7f040324

.field public static final ps__pre_broadcast_options:I = 0x7f040325

.field public static final ps__private_channel_layout:I = 0x7f040326

.field public static final ps__replay_scrub_view:I = 0x7f040327

.field public static final ps__search_row:I = 0x7f040328

.field public static final ps__select_all_row:I = 0x7f040329

.field public static final ps__sheet:I = 0x7f04032a

.field public static final ps__stat:I = 0x7f04032b

.field public static final ps__stat_value_pair:I = 0x7f04032c

.field public static final ps__title_toolbar:I = 0x7f04032d

.field public static final ps__tooltip:I = 0x7f04032e

.field public static final ps__tweet_sheet:I = 0x7f04032f

.field public static final ps__user_picker_activity:I = 0x7f040330

.field public static final ps__user_row_checked:I = 0x7f040331

.field public static final ps__user_row_follow:I = 0x7f040332

.field public static final ps__username:I = 0x7f040333

.field public static final ps__username_view:I = 0x7f040334

.field public static final select_dialog_item_material:I = 0x7f0403ad

.field public static final select_dialog_multichoice_material:I = 0x7f0403ae

.field public static final select_dialog_singlechoice_material:I = 0x7f0403af

.field public static final simple_profile_sheet:I = 0x7f0403b8

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0403de

.field public static final text_bubble:I = 0x7f0403ee

.field public static final viewer_activity_tweaks:I = 0x7f040444

.field public static final vip_badge_tooltip_layout:I = 0x7f040445

.field public static final webview:I = 0x7f04044c
