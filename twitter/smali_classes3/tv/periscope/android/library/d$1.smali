.class Ltv/periscope/android/library/d$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/library/d;->a(Landroid/app/Application;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/library/d;


# direct methods
.method constructor <init>(Ltv/periscope/android/library/d;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/o;->a()Landroid/app/Activity;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 106
    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/o;->f()V

    .line 109
    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0, v1}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;Ltv/periscope/android/ui/broadcast/o;)Ltv/periscope/android/ui/broadcast/o;

    .line 111
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0, v1}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/android/ui/broadcast/af;

    .line 112
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/o;->a()Landroid/app/Activity;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 87
    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/o;->d()V

    .line 89
    :cond_0
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/o;->a()Landroid/app/Activity;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 80
    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/o;->c()V

    .line 82
    :cond_0
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/o;->a()Landroid/app/Activity;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 73
    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/o;->b()V

    .line 75
    :cond_0
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/o;->a()Landroid/app/Activity;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 94
    iget-object v0, p0, Ltv/periscope/android/library/d$1;->a:Ltv/periscope/android/library/d;

    invoke-static {v0}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/o;->e()V

    .line 96
    :cond_0
    return-void
.end method
