.class public Ltv/periscope/android/library/a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/library/a$a;
    }
.end annotation


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:J

.field private M:J

.field private final a:Ltv/periscope/android/library/c;

.field private b:Ltv/periscope/android/ui/chat/x;

.field private c:Ltv/periscope/android/ui/chat/y;

.field private d:Ltv/periscope/android/view/o;

.field private e:Ltv/periscope/android/ui/chat/aj;

.field private f:Ltv/periscope/android/ui/chat/ak;

.field private g:Ltv/periscope/android/player/PlayMode;

.field private h:Ltv/periscope/android/analytics/summary/b;

.field private i:Ltv/periscope/android/video/StreamMode;

.field private j:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ltv/periscope/android/player/d;

.field private l:Ltv/periscope/android/player/e;

.field private m:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lczy;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/SettingsDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/permissions/a;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcxq;

.field private q:Ltv/periscope/android/view/t;

.field private r:Ltv/periscope/android/ui/broadcast/d;

.field private s:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcxe;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcxa;

.field private u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Z

.field private z:Z


# direct methods
.method constructor <init>(Ltv/periscope/android/library/c;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    sget-object v0, Ltv/periscope/android/ui/chat/s;->a:Ltv/periscope/android/ui/chat/s;

    iput-object v0, p0, Ltv/periscope/android/library/a;->b:Ltv/periscope/android/ui/chat/x;

    .line 45
    new-instance v0, Ltv/periscope/android/ui/chat/t;

    invoke-direct {v0}, Ltv/periscope/android/ui/chat/t;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/library/a;->c:Ltv/periscope/android/ui/chat/y;

    .line 47
    sget-object v0, Ltv/periscope/android/ui/chat/u;->a:Ltv/periscope/android/ui/chat/u;

    iput-object v0, p0, Ltv/periscope/android/library/a;->e:Ltv/periscope/android/ui/chat/aj;

    .line 48
    new-instance v0, Ltv/periscope/android/ui/chat/v;

    invoke-direct {v0}, Ltv/periscope/android/ui/chat/v;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/library/a;->f:Ltv/periscope/android/ui/chat/ak;

    .line 49
    sget-object v0, Ltv/periscope/android/player/PlayMode;->a:Ltv/periscope/android/player/PlayMode;

    iput-object v0, p0, Ltv/periscope/android/library/a;->g:Ltv/periscope/android/player/PlayMode;

    .line 50
    new-instance v0, Ltv/periscope/android/analytics/summary/b;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Ltv/periscope/android/analytics/summary/b;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ltv/periscope/android/library/a;->h:Ltv/periscope/android/analytics/summary/b;

    .line 51
    sget-object v0, Ltv/periscope/android/video/StreamMode;->a:Ltv/periscope/android/video/StreamMode;

    iput-object v0, p0, Ltv/periscope/android/library/a;->i:Ltv/periscope/android/video/StreamMode;

    .line 71
    iput-boolean v2, p0, Ltv/periscope/android/library/a;->C:Z

    .line 72
    iput-boolean v2, p0, Ltv/periscope/android/library/a;->D:Z

    .line 77
    iput-boolean v2, p0, Ltv/periscope/android/library/a;->I:Z

    .line 80
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ltv/periscope/android/library/a;->L:J

    .line 84
    iput-object p1, p0, Ltv/periscope/android/library/a;->a:Ltv/periscope/android/library/c;

    .line 85
    return-void
.end method

.method private b(Landroid/view/ViewGroup;)Ltv/periscope/android/ui/broadcast/af;
    .locals 42

    .prologue
    .line 332
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/library/a;->j:Ljava/lang/ref/WeakReference;

    if-nez v2, :cond_0

    .line 333
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "Invalid builder configuration - no Activity provided"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 334
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/library/a;->k:Ltv/periscope/android/player/d;

    if-nez v2, :cond_1

    .line 335
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "Invalid builder configuration - no PlayerWrapper provided"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 336
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/library/a;->l:Ltv/periscope/android/player/e;

    if-nez v2, :cond_2

    .line 337
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "Invalid builder configuration - no PlaytimeProvider provided"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 339
    :cond_2
    new-instance v2, Ltv/periscope/android/ui/broadcast/af;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/library/a;->j:Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/library/a;->a:Ltv/periscope/android/library/c;

    move-object/from16 v0, p0

    iget-object v5, v0, Ltv/periscope/android/library/a;->k:Ltv/periscope/android/player/d;

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/library/a;->l:Ltv/periscope/android/player/e;

    move-object/from16 v0, p0

    iget-object v8, v0, Ltv/periscope/android/library/a;->s:Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p0

    iget-object v9, v0, Ltv/periscope/android/library/a;->o:Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/library/a;->m:Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p0

    iget-object v11, v0, Ltv/periscope/android/library/a;->n:Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p0

    iget-object v12, v0, Ltv/periscope/android/library/a;->q:Ltv/periscope/android/view/t;

    move-object/from16 v0, p0

    iget-object v13, v0, Ltv/periscope/android/library/a;->p:Lcxq;

    move-object/from16 v0, p0

    iget-object v14, v0, Ltv/periscope/android/library/a;->r:Ltv/periscope/android/ui/broadcast/d;

    move-object/from16 v0, p0

    iget-object v15, v0, Ltv/periscope/android/library/a;->b:Ltv/periscope/android/ui/chat/x;

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/library/a;->c:Ltv/periscope/android/ui/chat/y;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/library/a;->d:Ltv/periscope/android/view/o;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/library/a;->e:Ltv/periscope/android/ui/chat/aj;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/library/a;->f:Ltv/periscope/android/ui/chat/ak;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/library/a;->h:Ltv/periscope/android/analytics/summary/b;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/library/a;->t:Lcxa;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/library/a;->u:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/library/a;->i:Ltv/periscope/android/video/StreamMode;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Ltv/periscope/android/library/a;->L:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Ltv/periscope/android/library/a;->M:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/library/a;->v:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->A:Z

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->z:Z

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->y:Z

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->B:Z

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->C:Z

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->D:Z

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->E:Z

    move/from16 v35, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->F:Z

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->G:Z

    move/from16 v37, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->H:Z

    move/from16 v38, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->I:Z

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->J:Z

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/library/a;->K:Z

    move/from16 v41, v0

    move-object/from16 v7, p1

    invoke-direct/range {v2 .. v41}, Ltv/periscope/android/ui/broadcast/af;-><init>(Ljava/lang/ref/WeakReference;Ltv/periscope/android/library/c;Ltv/periscope/android/player/d;Ltv/periscope/android/player/e;Landroid/view/ViewGroup;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ltv/periscope/android/view/t;Lcxq;Ltv/periscope/android/ui/broadcast/d;Ltv/periscope/android/ui/chat/x;Ltv/periscope/android/ui/chat/y;Ltv/periscope/android/view/o;Ltv/periscope/android/ui/chat/aj;Ltv/periscope/android/ui/chat/ak;Ltv/periscope/android/analytics/summary/b;Lcxa;Ljava/util/ArrayList;Ltv/periscope/android/video/StreamMode;JJLjava/lang/String;ZZZZZZZZZZZZZ)V

    .line 347
    invoke-static {}, Ltv/periscope/android/library/d;->a()Ltv/periscope/android/library/d;

    move-result-object v3

    invoke-virtual {v3, v2}, Ltv/periscope/android/library/d;->a(Ltv/periscope/android/ui/broadcast/af;)V

    .line 348
    return-object v2
.end method


# virtual methods
.method public a(Landroid/app/Activity;)Ltv/periscope/android/library/a;
    .locals 1

    .prologue
    .line 89
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltv/periscope/android/library/a;->j:Ljava/lang/ref/WeakReference;

    .line 90
    return-object p0
.end method

.method public a(Lcxe;)Ltv/periscope/android/library/a;
    .locals 1

    .prologue
    .line 182
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltv/periscope/android/library/a;->s:Ljava/lang/ref/WeakReference;

    .line 183
    return-object p0
.end method

.method public a(Lcxq;)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Ltv/periscope/android/library/a;->p:Lcxq;

    .line 138
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Ltv/periscope/android/library/a;->v:Ljava/lang/String;

    .line 195
    return-object p0
.end method

.method public a(Ltv/periscope/android/permissions/a;)Ltv/periscope/android/library/a;
    .locals 1

    .prologue
    .line 131
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltv/periscope/android/library/a;->o:Ljava/lang/ref/WeakReference;

    .line 132
    return-object p0
.end method

.method public a(Ltv/periscope/android/player/PlayMode;)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Ltv/periscope/android/library/a;->g:Ltv/periscope/android/player/PlayMode;

    .line 108
    return-object p0
.end method

.method public a(Ltv/periscope/android/player/d;)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Ltv/periscope/android/library/a;->k:Ltv/periscope/android/player/d;

    .line 96
    return-object p0
.end method

.method public a(Ltv/periscope/android/player/e;)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Ltv/periscope/android/library/a;->l:Ltv/periscope/android/player/e;

    .line 102
    return-object p0
.end method

.method public a(Ltv/periscope/android/ui/broadcast/d;)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Ltv/periscope/android/library/a;->r:Ltv/periscope/android/ui/broadcast/d;

    .line 150
    return-object p0
.end method

.method public a(Ltv/periscope/android/ui/chat/aj;Ltv/periscope/android/ui/chat/ak;)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Ltv/periscope/android/library/a;->e:Ltv/periscope/android/ui/chat/aj;

    .line 171
    iput-object p2, p0, Ltv/periscope/android/library/a;->f:Ltv/periscope/android/ui/chat/ak;

    .line 172
    return-object p0
.end method

.method public a(Ltv/periscope/android/ui/chat/x;Ltv/periscope/android/ui/chat/y;)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Ltv/periscope/android/library/a;->b:Ltv/periscope/android/ui/chat/x;

    .line 157
    iput-object p2, p0, Ltv/periscope/android/library/a;->c:Ltv/periscope/android/ui/chat/y;

    .line 158
    return-object p0
.end method

.method public a(Ltv/periscope/android/view/o;)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Ltv/periscope/android/library/a;->d:Ltv/periscope/android/view/o;

    .line 164
    return-object p0
.end method

.method public a(Ltv/periscope/android/view/t;)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Ltv/periscope/android/library/a;->q:Ltv/periscope/android/view/t;

    .line 144
    return-object p0
.end method

.method public a(Z)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 212
    iput-boolean p1, p0, Ltv/periscope/android/library/a;->z:Z

    .line 213
    return-object p0
.end method

.method public a(Landroid/view/ViewGroup;)Ltv/periscope/android/ui/broadcast/af;
    .locals 6

    .prologue
    .line 319
    invoke-direct {p0, p1}, Ltv/periscope/android/library/a;->b(Landroid/view/ViewGroup;)Ltv/periscope/android/ui/broadcast/af;

    move-result-object v1

    .line 320
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/af;->a(Z)V

    .line 321
    iget-object v0, p0, Ltv/periscope/android/library/a;->w:Ljava/lang/String;

    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322
    iget-object v0, p0, Ltv/periscope/android/library/a;->w:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/af;->c(Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Ltv/periscope/android/library/a;->g:Ltv/periscope/android/player/PlayMode;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/android/player/PlayMode;)V

    .line 327
    :cond_0
    :goto_0
    return-object v1

    .line 324
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/library/a;->x:Ljava/lang/String;

    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    new-instance v0, Ltv/periscope/android/library/a$a;

    iget-object v2, p0, Ltv/periscope/android/library/a;->a:Ltv/periscope/android/library/c;

    iget-object v3, p0, Ltv/periscope/android/library/a;->g:Ltv/periscope/android/player/PlayMode;

    iget-object v4, p0, Ltv/periscope/android/library/a;->x:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/library/a$a;-><init>(Ltv/periscope/android/ui/broadcast/af;Ltv/periscope/android/library/c;Ltv/periscope/android/player/PlayMode;Ljava/lang/String;Ltv/periscope/android/library/a$1;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Ltv/periscope/android/library/a;->w:Ljava/lang/String;

    .line 308
    return-object p0
.end method

.method public b(Z)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 218
    iput-boolean p1, p0, Ltv/periscope/android/library/a;->A:Z

    .line 219
    return-object p0
.end method

.method public c(Z)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 283
    iput-boolean p1, p0, Ltv/periscope/android/library/a;->J:Z

    .line 284
    return-object p0
.end method

.method public d(Z)Ltv/periscope/android/library/a;
    .locals 0

    .prologue
    .line 289
    iput-boolean p1, p0, Ltv/periscope/android/library/a;->K:Z

    .line 290
    return-object p0
.end method
