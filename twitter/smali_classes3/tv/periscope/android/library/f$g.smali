.class public final Ltv/periscope/android/library/f$g;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/library/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "g"
.end annotation


# static fields
.field public static final action0:I = 0x7f1305e1

.field public static final action_bar:I = 0x7f130133

.field public static final action_bar_activity_content:I = 0x7f130001

.field public static final action_bar_container:I = 0x7f130132

.field public static final action_bar_root:I = 0x7f13012e

.field public static final action_bar_spinner:I = 0x7f130002

.field public static final action_bar_subtitle:I = 0x7f130115

.field public static final action_bar_title:I = 0x7f130114

.field public static final action_button:I = 0x7f130003

.field public static final action_context_bar:I = 0x7f130134

.field public static final action_divider:I = 0x7f1305e5

.field public static final action_menu_divider:I = 0x7f130009

.field public static final action_menu_presenter:I = 0x7f13000a

.field public static final action_mode_bar:I = 0x7f130130

.field public static final action_mode_bar_stub:I = 0x7f13012f

.field public static final action_mode_close_button:I = 0x7f130116

.field public static final action_sheet:I = 0x7f13076f

.field public static final action_sheet_divider:I = 0x7f1306c2

.field public static final actions:I = 0x7f130304

.field public static final activity_chooser_view_content:I = 0x7f130117

.field public static final add:I = 0x7f1300bb

.field public static final adjust_height:I = 0x7f1300ed

.field public static final adjust_width:I = 0x7f1300ee

.field public static final alertTitle:I = 0x7f130121

.field public static final always:I = 0x7f1300f5

.field public static final audience_count:I = 0x7f130766

.field public static final audience_header_container:I = 0x7f130763

.field public static final audience_icon:I = 0x7f1306cb

.field public static final audience_search_back:I = 0x7f1306c6

.field public static final audience_search_clear:I = 0x7f1306c8

.field public static final audience_search_header:I = 0x7f1306c5

.field public static final audience_search_query:I = 0x7f1306c7

.field public static final audience_selection:I = 0x7f13070f

.field public static final audience_selection_items:I = 0x7f1306c9

.field public static final audience_subtitle:I = 0x7f130767

.field public static final audience_title:I = 0x7f130765

.field public static final audience_title_container:I = 0x7f130764

.field public static final auto:I = 0x7f1300cc

.field public static final avatar:I = 0x7f1302ef

.field public static final avatar_container:I = 0x7f1306a4

.field public static final baby_heart:I = 0x7f1306e2

.field public static final back:I = 0x7f13035b

.field public static final badge_containers:I = 0x7f130860

.field public static final badge_info:I = 0x7f13085e

.field public static final ball_1:I = 0x7f130737

.field public static final ball_2:I = 0x7f130738

.field public static final ball_3:I = 0x7f130739

.field public static final ball_4:I = 0x7f13073a

.field public static final bar:I = 0x7f130779

.field public static final beginning:I = 0x7f1300ec

.field public static final block_count:I = 0x7f13071e

.field public static final block_count_icon:I = 0x7f13071d

.field public static final block_indicator:I = 0x7f130721

.field public static final body:I = 0x7f130014

.field public static final bottom:I = 0x7f1300a4

.field public static final bottom_drag_child:I = 0x7f130703

.field public static final bottom_tray:I = 0x7f13072d

.field public static final broadcast__action_sheet:I = 0x7f130702

.field public static final broadcast_action_item:I = 0x7f1306c3

.field public static final broadcast_live_label:I = 0x7f1306e4

.field public static final broadcast_local_time:I = 0x7f1306de

.field public static final broadcast_subtitle:I = 0x7f1306f4

.field public static final broadcast_tip:I = 0x7f130716

.field public static final broadcast_tip_close:I = 0x7f1306f1

.field public static final broadcast_tip_image:I = 0x7f1306f0

.field public static final broadcast_tip_text:I = 0x7f1306f2

.field public static final broadcast_title:I = 0x7f1306f3

.field public static final broadcast_viewer_row_container:I = 0x7f1306e1

.field public static final broadcaster:I = 0x7f1306fd

.field public static final broadcaster_avatar:I = 0x7f13075f

.field public static final broadcaster_container:I = 0x7f1306fb

.field public static final broadcaster_description:I = 0x7f1306fe

.field public static final broadcaster_view:I = 0x7f1306ff

.field public static final bronze_badge:I = 0x7f130862

.field public static final bronze_badge_container:I = 0x7f130861

.field public static final bronze_badge_label:I = 0x7f130863

.field public static final btn_cameraflip:I = 0x7f130705

.field public static final btn_close:I = 0x7f130762

.field public static final btn_container:I = 0x7f130760

.field public static final btn_done:I = 0x7f130761

.field public static final btn_play_icon:I = 0x7f1306d1

.field public static final btn_start_broadcast:I = 0x7f13070e

.field public static final btn_stop_broadcast:I = 0x7f130706

.field public static final buttonPanel:I = 0x7f13011c

.field public static final button_container:I = 0x7f1306cf

.field public static final buttons_container:I = 0x7f130144

.field public static final camera_playback:I = 0x7f130707

.field public static final camera_preview:I = 0x7f130700

.field public static final cancel_action:I = 0x7f1305e2

.field public static final cancel_comment:I = 0x7f1306d7

.field public static final caret:I = 0x7f1303ab

.field public static final carousel_container:I = 0x7f1306bd

.field public static final carrot:I = 0x7f13085c

.field public static final channel_members:I = 0x7f130732

.field public static final channel_row_container:I = 0x7f130714

.field public static final chat_body:I = 0x7f13071f

.field public static final chat_container:I = 0x7f130728

.field public static final chat_list:I = 0x7f13072e

.field public static final chat_message_container:I = 0x7f130717

.field public static final chat_messages_view:I = 0x7f13072a

.field public static final chat_row:I = 0x7f1306f9

.field public static final chat_row_container:I = 0x7f130722

.field public static final chat_status:I = 0x7f1306d3

.field public static final chat_text_container:I = 0x7f130718

.field public static final chatroom_view:I = 0x7f130629

.field public static final check:I = 0x7f1306cc

.field public static final checkbox:I = 0x7f13012a

.field public static final chronometer:I = 0x7f1305e7

.field public static final click_jack:I = 0x7f130772

.field public static final close:I = 0x7f1301ba

.field public static final close_button:I = 0x7f1307ab

.field public static final close_or_back:I = 0x7f13078c

.field public static final collapseActionView:I = 0x7f1300f6

.field public static final comment_send:I = 0x7f1306d6

.field public static final comment_tweaks:I = 0x7f13085a

.field public static final compose_comment:I = 0x7f1306d5

.field public static final compose_layout:I = 0x7f1306d4

.field public static final consequence:I = 0x7f130726

.field public static final container:I = 0x7f130155

.field public static final content:I = 0x7f130019

.field public static final contentPanel:I = 0x7f130122

.field public static final content_list:I = 0x7f130770

.field public static final control_hints:I = 0x7f130773

.field public static final count_value:I = 0x7f130743

.field public static final create_channel:I = 0x7f130731

.field public static final create_channel_user_picker_sheet:I = 0x7f130712

.field public static final create_group_row_container:I = 0x7f13072f

.field public static final create_private_channel:I = 0x7f130710

.field public static final cta_action_button:I = 0x7f13074e

.field public static final cta_description:I = 0x7f13074d

.field public static final current_time:I = 0x7f1306e7

.field public static final custom:I = 0x7f130128

.field public static final customPanel:I = 0x7f130127

.field public static final custom_stats_padding:I = 0x7f1306ec

.field public static final dark:I = 0x7f1300ff

.field public static final decor_content_parent:I = 0x7f130131

.field public static final default_action:I = 0x7f1306db

.field public static final default_activity_button:I = 0x7f130119

.field public static final delete_icon:I = 0x7f130791

.field public static final description:I = 0x7f1301e9

.field public static final dev:I = 0x7f13076c

.field public static final did_not_vote:I = 0x7f13074a

.field public static final dim_bg:I = 0x7f13077d

.field public static final disable:I = 0x7f13073e

.field public static final disableHome:I = 0x7f1300b0

.field public static final display_name:I = 0x7f1302ff

.field public static final divider:I = 0x7f130020

.field public static final divider_line:I = 0x7f13073f

.field public static final divider_title:I = 0x7f130740

.field public static final done_count:I = 0x7f13078f

.field public static final double_tap_label:I = 0x7f13070a

.field public static final drag_child:I = 0x7f13074f

.field public static final drawer_caret:I = 0x7f13070b

.field public static final duration:I = 0x7f1306eb

.field public static final edit_broadcast_title:I = 0x7f130768

.field public static final edit_query:I = 0x7f130135

.field public static final end:I = 0x7f1300d0

.field public static final end_padder:I = 0x7f1305eb

.field public static final end_time:I = 0x7f1306e8

.field public static final ended_container:I = 0x7f1306f5

.field public static final ended_time:I = 0x7f1306f7

.field public static final ended_title:I = 0x7f1306f6

.field public static final example:I = 0x7f13073c

.field public static final expand_activities_button:I = 0x7f130118

.field public static final expanded_menu:I = 0x7f130129

.field public static final featured_label:I = 0x7f130733

.field public static final featured_summary:I = 0x7f130734

.field public static final follow:I = 0x7f130793

.field public static final following_chat:I = 0x7f13076d

.field public static final following_icon:I = 0x7f13071a

.field public static final friends_watching:I = 0x7f130735

.field public static final friends_watching_view:I = 0x7f1306d8

.field public static final fuzzy_ball:I = 0x7f13075a

.field public static final fuzzy_balls_stub:I = 0x7f130759

.field public static final gesture_hints:I = 0x7f130708

.field public static final gold_badge:I = 0x7f130868

.field public static final gold_badge_container:I = 0x7f130867

.field public static final gold_badge_label:I = 0x7f130869

.field public static final header:I = 0x7f130041

.field public static final header_container:I = 0x7f130167

.field public static final header_profile_image:I = 0x7f1306fc

.field public static final heart:I = 0x7f130736

.field public static final heart_line:I = 0x7f1306e3

.field public static final heart_tweaks:I = 0x7f13085b

.field public static final hearts:I = 0x7f1306b3

.field public static final hearts_container:I = 0x7f1306b2

.field public static final hearts_view:I = 0x7f130729

.field public static final home:I = 0x7f130043

.field public static final homeAsUp:I = 0x7f1300b1

.field public static final hybrid:I = 0x7f1300ef

.field public static final icon:I = 0x7f1300fa

.field public static final icon_only:I = 0x7f1300fc

.field public static final icon_participants:I = 0x7f130742

.field public static final ifRoom:I = 0x7f1300f7

.field public static final image:I = 0x7f130044

.field public static final image_preview:I = 0x7f130389

.field public static final info:I = 0x7f1305ea

.field public static final info_container:I = 0x7f130746

.field public static final info_snippet:I = 0x7f1306c1

.field public static final item_touch_helper_previous_elevation:I = 0x7f13004b

.field public static final label:I = 0x7f1306c4

.field public static final learn_more:I = 0x7f13085f

.field public static final learn_more_about_moderation:I = 0x7f13074b

.field public static final light:I = 0x7f130100

.field public static final line:I = 0x7f1306ce

.field public static final line1:I = 0x7f1305e6

.field public static final line3:I = 0x7f1305e9

.field public static final line_chart:I = 0x7f1306e6

.field public static final list:I = 0x7f130391

.field public static final listMode:I = 0x7f1300ae

.field public static final list_item:I = 0x7f13011a

.field public static final loading_animation:I = 0x7f13075e

.field public static final loading_text:I = 0x7f1303f0

.field public static final location:I = 0x7f13028e

.field public static final location_container:I = 0x7f13076b

.field public static final location_name:I = 0x7f130464

.field public static final manage_channel_user_picker_sheet:I = 0x7f130713

.field public static final manage_private_channel:I = 0x7f130711

.field public static final map:I = 0x7f1306dd

.field public static final marker_view:I = 0x7f130741

.field public static final masked_avatar:I = 0x7f130626

.field public static final media_actions:I = 0x7f1305e4

.field public static final message:I = 0x7f130197

.field public static final message_carousel:I = 0x7f1306c0

.field public static final message_container:I = 0x7f130747

.field public static final message_moderate_body:I = 0x7f13073d

.field public static final middle:I = 0x7f1300a5

.field public static final moderation_verdict:I = 0x7f130725

.field public static final moderator_container:I = 0x7f13072c

.field public static final moderator_out_of_time:I = 0x7f130749

.field public static final moderator_overlay:I = 0x7f13072b

.field public static final moderator_view:I = 0x7f130745

.field public static final more:I = 0x7f1306da

.field public static final more_text:I = 0x7f1306e0

.field public static final more_total:I = 0x7f1306df

.field public static final multiply:I = 0x7f1300bc

.field public static final muted:I = 0x7f13055c

.field public static final name:I = 0x7f130058

.field public static final negative:I = 0x7f130727

.field public static final neutral:I = 0x7f13074c

.field public static final never:I = 0x7f1300f8

.field public static final none:I = 0x7f1300a6

.field public static final normal:I = 0x7f1300ad

.field public static final overflow_button:I = 0x7f1306d0

.field public static final parentPanel:I = 0x7f13011e

.field public static final participants:I = 0x7f1306d2

.field public static final peak_title:I = 0x7f130744

.field public static final permissions_btn:I = 0x7f130756

.field public static final place_autocomplete_clear_button:I = 0x7f13064b

.field public static final place_autocomplete_powered_by_google:I = 0x7f13064d

.field public static final place_autocomplete_prediction_primary_text:I = 0x7f13064f

.field public static final place_autocomplete_prediction_secondary_text:I = 0x7f130650

.field public static final place_autocomplete_progress:I = 0x7f13064e

.field public static final place_autocomplete_search_button:I = 0x7f130649

.field public static final place_autocomplete_search_input:I = 0x7f13064a

.field public static final place_autocomplete_separator:I = 0x7f13064c

.field public static final placeholder_text:I = 0x7f130757

.field public static final play_highlights:I = 0x7f130752

.field public static final play_time:I = 0x7f1306d9

.field public static final playback_controls:I = 0x7f130704

.field public static final player_view:I = 0x7f130750

.field public static final plus_sign:I = 0x7f130730

.field public static final positive:I = 0x7f1306fa

.field public static final pre_broadcast_details:I = 0x7f13070d

.field public static final presence_count:I = 0x7f1306f8

.field public static final preview_container:I = 0x7f130758

.field public static final preview_frame:I = 0x7f13075b

.field public static final profile_image:I = 0x7f13031a

.field public static final profile_image_badge_layer1:I = 0x7f1306ac

.field public static final profile_image_badge_layer2:I = 0x7f1306ad

.field public static final profile_image_badge_layer3:I = 0x7f1306ae

.field public static final profile_image_badge_layer4:I = 0x7f1306af

.field public static final profile_image_badge_layer5:I = 0x7f1306b0

.field public static final profile_image_container:I = 0x7f1306ab

.field public static final profile_sheet_bio:I = 0x7f1306aa

.field public static final progress_bar:I = 0x7f130064

.field public static final progress_bar_container:I = 0x7f130771

.field public static final progress_circular:I = 0x7f130065

.field public static final progress_horizontal:I = 0x7f130067

.field public static final provided_location:I = 0x7f130769

.field public static final ps__none:I = 0x7f1300f2

.field public static final ps__oval:I = 0x7f1300f3

.field public static final ps__roundRect:I = 0x7f1300f4

.field public static final public_row_container:I = 0x7f1306ca

.field public static final public_text:I = 0x7f1306cd

.field public static final radio:I = 0x7f13012c

.field public static final rank:I = 0x7f130792

.field public static final remove_location:I = 0x7f13076a

.field public static final replay_scrubber:I = 0x7f13075d

.field public static final reply_indicator:I = 0x7f130720

.field public static final report_comment_background:I = 0x7f1306be

.field public static final report_comment_info:I = 0x7f1306bf

.field public static final right_button:I = 0x7f130784

.field public static final right_text_button:I = 0x7f130785

.field public static final root:I = 0x7f13014c

.field public static final row_container:I = 0x7f13077b

.field public static final satellite:I = 0x7f1300f0

.field public static final screen:I = 0x7f1300bd

.field public static final scrollIndicatorDown:I = 0x7f130126

.field public static final scrollIndicatorUp:I = 0x7f130123

.field public static final scrollView:I = 0x7f130124

.field public static final scroll_container:I = 0x7f1306dc

.field public static final search_badge:I = 0x7f130137

.field public static final search_bar:I = 0x7f130136

.field public static final search_button:I = 0x7f130138

.field public static final search_close_btn:I = 0x7f13013d

.field public static final search_edit_frame:I = 0x7f130139

.field public static final search_go_btn:I = 0x7f13013f

.field public static final search_icon:I = 0x7f1304a9

.field public static final search_mag_icon:I = 0x7f13013a

.field public static final search_or_close:I = 0x7f13078e

.field public static final search_plate:I = 0x7f13013b

.field public static final search_query:I = 0x7f13078d

.field public static final search_row_container:I = 0x7f13077a

.field public static final search_src_text:I = 0x7f13013c

.field public static final search_voice_btn:I = 0x7f130140

.field public static final seek_hint:I = 0x7f130774

.field public static final select_all:I = 0x7f13077c

.field public static final select_dialog_listview:I = 0x7f130141

.field public static final sheet_container:I = 0x7f1301b4

.field public static final sheet_content:I = 0x7f13077f

.field public static final sheet_inner:I = 0x7f13077e

.field public static final shortcut:I = 0x7f13012b

.field public static final showCustom:I = 0x7f1300b2

.field public static final showHome:I = 0x7f1300b3

.field public static final showTitle:I = 0x7f1300b4

.field public static final show_more_text:I = 0x7f1306ef

.field public static final silver_badge:I = 0x7f130865

.field public static final silver_badge_container:I = 0x7f130864

.field public static final silver_badge_label:I = 0x7f130866

.field public static final spacer:I = 0x7f13011d

.field public static final split_action_bar:I = 0x7f13007b

.field public static final src_atop:I = 0x7f1300be

.field public static final src_in:I = 0x7f1300bf

.field public static final src_over:I = 0x7f1300c0

.field public static final standard:I = 0x7f1300fd

.field public static final stat_1:I = 0x7f1306ed

.field public static final stat_2:I = 0x7f1306ee

.field public static final stat_label:I = 0x7f130781

.field public static final stat_name:I = 0x7f130782

.field public static final stat_value:I = 0x7f130780

.field public static final stats_graph:I = 0x7f1306e5

.field public static final status_bar_latest_event_content:I = 0x7f1305e3

.field public static final status_icon:I = 0x7f130724

.field public static final status_item:I = 0x7f130723

.field public static final stream_details:I = 0x7f13070c

.field public static final submenuarrow:I = 0x7f13012d

.field public static final submit_area:I = 0x7f13013e

.field public static final subtitle:I = 0x7f1302f2

.field public static final superfan_icon:I = 0x7f13071b

.field public static final superfans_badge:I = 0x7f130796

.field public static final swipe_down_label:I = 0x7f130709

.field public static final tabMode:I = 0x7f1300af

.field public static final terrain:I = 0x7f1300f1

.field public static final text:I = 0x7f13007d

.field public static final text2:I = 0x7f1305e8

.field public static final textSpacerNoButtons:I = 0x7f130125

.field public static final thumb:I = 0x7f13075c

.field public static final thumb_container:I = 0x7f130778

.field public static final thumbnail:I = 0x7f1303bb

.field public static final thumbnail_container:I = 0x7f13052a

.field public static final thumbnail_info_icon:I = 0x7f130715

.field public static final time:I = 0x7f130187

.field public static final time_per_user:I = 0x7f1306ea

.field public static final time_watched:I = 0x7f1306e9

.field public static final timer:I = 0x7f130748

.field public static final title:I = 0x7f13011b

.field public static final title_container:I = 0x7f130783

.field public static final title_template:I = 0x7f130120

.field public static final title_view:I = 0x7f130150

.field public static final toolbar:I = 0x7f13007f

.field public static final tooltip:I = 0x7f130786

.field public static final tooltip_content:I = 0x7f13085d

.field public static final top:I = 0x7f1300a7

.field public static final topPanel:I = 0x7f13011f

.field public static final tos_line:I = 0x7f13073b

.field public static final tweak_video_info:I = 0x7f130751

.field public static final tweaks:I = 0x7f130755

.field public static final tweet:I = 0x7f13076e

.field public static final tweet_button:I = 0x7f1302a3

.field public static final tweet_content_container:I = 0x7f130789

.field public static final tweet_counter:I = 0x7f13078b

.field public static final tweet_sheet:I = 0x7f130754

.field public static final tweet_sheet_scrim:I = 0x7f130787

.field public static final tweet_text:I = 0x7f13027a

.field public static final tweet_url:I = 0x7f13078a

.field public static final twitter_icon:I = 0x7f130788

.field public static final up:I = 0x7f130094

.field public static final useLogo:I = 0x7f1300b5

.field public static final user_picker_sheet:I = 0x7f130753

.field public static final user_row_container:I = 0x7f130790

.field public static final username:I = 0x7f13009a

.field public static final username_badge_view:I = 0x7f13071c

.field public static final username_container:I = 0x7f130719

.field public static final username_text:I = 0x7f130794

.field public static final viewer_action_sheet:I = 0x7f130701

.field public static final vip_badge:I = 0x7f130795

.field public static final watch_live:I = 0x7f1306b1

.field public static final webview:I = 0x7f13055f

.field public static final wide:I = 0x7f1300fe

.field public static final window:I = 0x7f130428

.field public static final withText:I = 0x7f1300f9

.field public static final wrap_content:I = 0x7f1300c1

.field public static final zoom_hint:I = 0x7f130775

.field public static final zoom_label:I = 0x7f130777

.field public static final zoom_zone:I = 0x7f130776
