.class public Ltv/periscope/android/library/d;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static volatile a:Ltv/periscope/android/library/d;


# instance fields
.field private final b:Ltv/periscope/android/library/c;

.field private c:Landroid/app/Application$ActivityLifecycleCallbacks;

.field private d:Ltv/periscope/android/ui/broadcast/o;

.field private e:Ltv/periscope/android/ui/broadcast/af;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ltv/periscope/android/library/c;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p2, p0, Ltv/periscope/android/library/d;->b:Ltv/periscope/android/library/c;

    .line 56
    instance-of v0, p1, Landroid/app/Application;

    if-eqz v0, :cond_0

    .line 57
    check-cast p1, Landroid/app/Application;

    invoke-direct {p0, p1}, Ltv/periscope/android/library/d;->a(Landroid/app/Application;)V

    .line 61
    return-void

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Context must be an instance of Application"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 145
    const-string/jumbo v0, "Periscope Library Prefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static a()Ltv/periscope/android/library/d;
    .locals 2

    .prologue
    .line 30
    sget-object v0, Ltv/periscope/android/library/d;->a:Ltv/periscope/android/library/d;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Must call PeriscopeCore.initialize before calling getInstance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_0
    sget-object v0, Ltv/periscope/android/library/d;->a:Ltv/periscope/android/library/d;

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/library/d;Ltv/periscope/android/ui/broadcast/af;)Ltv/periscope/android/ui/broadcast/af;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Ltv/periscope/android/library/d;->e:Ltv/periscope/android/ui/broadcast/af;

    return-object p1
.end method

.method static synthetic a(Ltv/periscope/android/library/d;)Ltv/periscope/android/ui/broadcast/o;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Ltv/periscope/android/library/d;->d:Ltv/periscope/android/ui/broadcast/o;

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/library/d;Ltv/periscope/android/ui/broadcast/o;)Ltv/periscope/android/ui/broadcast/o;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Ltv/periscope/android/library/d;->d:Ltv/periscope/android/ui/broadcast/o;

    return-object p1
.end method

.method private a(Landroid/app/Application;)V
    .locals 1

    .prologue
    .line 64
    new-instance v0, Ltv/periscope/android/library/d$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/library/d$1;-><init>(Ltv/periscope/android/library/d;)V

    iput-object v0, p0, Ltv/periscope/android/library/d;->c:Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 115
    iget-object v0, p0, Ltv/periscope/android/library/d;->c:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 116
    return-void
.end method

.method public static a(Landroid/content/Context;Ltv/periscope/android/library/c;)V
    .locals 2

    .prologue
    .line 42
    sget-object v0, Ltv/periscope/android/library/d;->a:Ltv/periscope/android/library/d;

    if-nez v0, :cond_1

    .line 43
    const-class v1, Ltv/periscope/android/library/d;

    monitor-enter v1

    .line 44
    :try_start_0
    sget-object v0, Ltv/periscope/android/library/d;->a:Ltv/periscope/android/library/d;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Ltv/periscope/android/library/d;

    invoke-direct {v0, p0, p1}, Ltv/periscope/android/library/d;-><init>(Landroid/content/Context;Ltv/periscope/android/library/c;)V

    sput-object v0, Ltv/periscope/android/library/d;->a:Ltv/periscope/android/library/d;

    .line 47
    :cond_0
    monitor-exit v1

    .line 51
    :goto_0
    return-void

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 49
    :cond_1
    const-string/jumbo v0, "PeriscopeCore"

    const-string/jumbo v1, "PeriscopeCore.initialize has already been called!"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 38
    sget-object v0, Ltv/periscope/android/library/d;->a:Ltv/periscope/android/library/d;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ltv/periscope/android/ui/broadcast/af;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Ltv/periscope/android/library/d;->e:Ltv/periscope/android/ui/broadcast/af;

    .line 133
    return-void
.end method

.method public c()Ltv/periscope/android/library/c;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ltv/periscope/android/library/d;->b:Ltv/periscope/android/library/c;

    return-object v0
.end method

.method public d()Ltv/periscope/android/library/a;
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Ltv/periscope/android/library/d;->e:Ltv/periscope/android/ui/broadcast/af;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Ltv/periscope/android/library/d;->e:Ltv/periscope/android/ui/broadcast/af;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/af;->B()V

    .line 128
    :cond_0
    new-instance v0, Ltv/periscope/android/library/a;

    iget-object v1, p0, Ltv/periscope/android/library/d;->b:Ltv/periscope/android/library/c;

    invoke-direct {v0, v1}, Ltv/periscope/android/library/a;-><init>(Ltv/periscope/android/library/c;)V

    return-object v0
.end method

.method public e()Lcxf;
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Ltv/periscope/android/library/d;->d:Ltv/periscope/android/ui/broadcast/o;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Ltv/periscope/android/ui/broadcast/o;

    iget-object v1, p0, Ltv/periscope/android/library/d;->b:Ltv/periscope/android/library/c;

    invoke-direct {v0, v1}, Ltv/periscope/android/ui/broadcast/o;-><init>(Ltv/periscope/android/library/c;)V

    iput-object v0, p0, Ltv/periscope/android/library/d;->d:Ltv/periscope/android/ui/broadcast/o;

    .line 140
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/library/d;->d:Ltv/periscope/android/ui/broadcast/o;

    return-object v0
.end method
