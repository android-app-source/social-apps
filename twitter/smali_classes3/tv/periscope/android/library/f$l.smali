.class public final Ltv/periscope/android/library/f$l;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/library/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "l"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f0a0000

.field public static final abc_action_bar_home_description_format:I = 0x7f0a0001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f0a0002

.field public static final abc_action_bar_up_description:I = 0x7f0a0003

.field public static final abc_action_menu_overflow_description:I = 0x7f0a0004

.field public static final abc_action_mode_done:I = 0x7f0a0005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0a0006

.field public static final abc_activitychooserview_choose_application:I = 0x7f0a0007

.field public static final abc_capital_off:I = 0x7f0a0008

.field public static final abc_capital_on:I = 0x7f0a0009

.field public static final abc_font_family_body_1_material:I = 0x7f0a0b7d

.field public static final abc_font_family_body_2_material:I = 0x7f0a0b7e

.field public static final abc_font_family_button_material:I = 0x7f0a0b7f

.field public static final abc_font_family_caption_material:I = 0x7f0a0b80

.field public static final abc_font_family_display_1_material:I = 0x7f0a0b81

.field public static final abc_font_family_display_2_material:I = 0x7f0a0b82

.field public static final abc_font_family_display_3_material:I = 0x7f0a0b83

.field public static final abc_font_family_display_4_material:I = 0x7f0a0b84

.field public static final abc_font_family_headline_material:I = 0x7f0a0b85

.field public static final abc_font_family_menu_material:I = 0x7f0a0b86

.field public static final abc_font_family_subhead_material:I = 0x7f0a0b87

.field public static final abc_font_family_title_material:I = 0x7f0a0b88

.field public static final abc_search_hint:I = 0x7f0a000a

.field public static final abc_searchview_description_clear:I = 0x7f0a000b

.field public static final abc_searchview_description_query:I = 0x7f0a000c

.field public static final abc_searchview_description_search:I = 0x7f0a000d

.field public static final abc_searchview_description_submit:I = 0x7f0a000e

.field public static final abc_searchview_description_voice:I = 0x7f0a000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f0a0010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0a0011

.field public static final abc_toolbar_collapse_description:I = 0x7f0a0012

.field public static final common_google_play_services_enable_button:I = 0x7f0a0013

.field public static final common_google_play_services_enable_text:I = 0x7f0a0014

.field public static final common_google_play_services_enable_title:I = 0x7f0a0015

.field public static final common_google_play_services_install_button:I = 0x7f0a0016

.field public static final common_google_play_services_install_text_phone:I = 0x7f0a0017

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0a0018

.field public static final common_google_play_services_install_title:I = 0x7f0a0019

.field public static final common_google_play_services_notification_ticker:I = 0x7f0a001a

.field public static final common_google_play_services_unknown_issue:I = 0x7f0a001b

.field public static final common_google_play_services_unsupported_text:I = 0x7f0a001c

.field public static final common_google_play_services_unsupported_title:I = 0x7f0a001d

.field public static final common_google_play_services_update_button:I = 0x7f0a001e

.field public static final common_google_play_services_update_text:I = 0x7f0a001f

.field public static final common_google_play_services_update_title:I = 0x7f0a0020

.field public static final common_google_play_services_updating_text:I = 0x7f0a0021

.field public static final common_google_play_services_updating_title:I = 0x7f0a0022

.field public static final common_google_play_services_wear_update_text:I = 0x7f0a0023

.field public static final common_open_on_phone:I = 0x7f0a0024

.field public static final common_signin_button_text:I = 0x7f0a0025

.field public static final common_signin_button_text_long:I = 0x7f0a0026

.field public static final place_autocomplete_clear_button:I = 0x7f0a0027

.field public static final place_autocomplete_search_hint:I = 0x7f0a0028

.field public static final ps__abbrev_not_applicable:I = 0x7f0a0a45

.field public static final ps__action_autodelete_confirmation_title:I = 0x7f0a0a46

.field public static final ps__action_change_expiration:I = 0x7f0a0a47

.field public static final ps__action_change_expiration_subtext:I = 0x7f0a0a48

.field public static final ps__action_delete_broadcast_now:I = 0x7f0a0a49

.field public static final ps__action_dont_auto_delete:I = 0x7f0a0a4a

.field public static final ps__action_sheet_chat_reply:I = 0x7f0a0a4b

.field public static final ps__action_sheet_delete_broadcast:I = 0x7f0a0a4c

.field public static final ps__action_sheet_hide_chat:I = 0x7f0a0a4d

.field public static final ps__action_sheet_label_block:I = 0x7f0a0a4e

.field public static final ps__action_sheet_label_report:I = 0x7f0a0a4f

.field public static final ps__action_sheet_label_view_profile:I = 0x7f0a0a50

.field public static final ps__action_sheet_open_twitter_profile:I = 0x7f0a0a51

.field public static final ps__action_sheet_report_abuse:I = 0x7f0a0a52

.field public static final ps__action_sheet_report_other:I = 0x7f0a0a53

.field public static final ps__action_sheet_report_other_example:I = 0x7f0a0a54

.field public static final ps__action_sheet_report_snippet:I = 0x7f0a0a55

.field public static final ps__action_sheet_report_spam:I = 0x7f0a0a56

.field public static final ps__action_sheet_save_to_gallery:I = 0x7f0a0a57

.field public static final ps__action_sheet_saved_to_gallery:I = 0x7f0a0a58

.field public static final ps__action_sheet_show_chat:I = 0x7f0a0a59

.field public static final ps__app_name:I = 0x7f0a0a5a

.field public static final ps__audience_selection_create_channel_with:I = 0x7f0a0a5b

.field public static final ps__audience_selection_current_audience:I = 0x7f0a0a5c

.field public static final ps__audience_selection_done:I = 0x7f0a0a5d

.field public static final ps__audience_selection_header:I = 0x7f0a0a5e

.field public static final ps__audience_selection_public_audience:I = 0x7f0a0a5f

.field public static final ps__audience_selection_search_mutual_followers:I = 0x7f0a0a60

.field public static final ps__audience_selection_tap_to_change_audience:I = 0x7f0a0a61

.field public static final ps__audio_noisy:I = 0x7f0a0a62

.field public static final ps__bitrate_too_low:I = 0x7f0a0a63

.field public static final ps__bitrate_undefined:I = 0x7f0a0a64

.field public static final ps__block_dialog_btn_confirm:I = 0x7f0a0a65

.field public static final ps__block_limit_error:I = 0x7f0a0a66

.field public static final ps__block_unblock_dialog_btn_cancel:I = 0x7f0a0a67

.field public static final ps__blocked:I = 0x7f0a0a68

.field public static final ps__blocked_user:I = 0x7f0a0a69

.field public static final ps__branch_api_key:I = 0x7f0a0bed

.field public static final ps__broadcast_default_title_live:I = 0x7f0a0bee

.field public static final ps__broadcast_default_title_replay:I = 0x7f0a0bef

.field public static final ps__broadcast_following_chat_off:I = 0x7f0a0a6a

.field public static final ps__broadcast_following_chat_on:I = 0x7f0a0a6b

.field public static final ps__broadcast_limited:I = 0x7f0a0a6c

.field public static final ps__broadcast_limited_dialog_message:I = 0x7f0a0a6d

.field public static final ps__broadcast_limited_dialog_title:I = 0x7f0a0a6e

.field public static final ps__broadcast_live_label:I = 0x7f0a0a6f

.field public static final ps__broadcast_location_off:I = 0x7f0a0a70

.field public static final ps__broadcast_location_on:I = 0x7f0a0a71

.field public static final ps__broadcast_private:I = 0x7f0a0a72

.field public static final ps__broadcast_private_count:I = 0x7f0a0a73

.field public static final ps__broadcast_public:I = 0x7f0a0a74

.field public static final ps__broadcast_shared_by:I = 0x7f0a0a75

.field public static final ps__broadcast_title_hint:I = 0x7f0a0bf0

.field public static final ps__broadcast_to_channel:I = 0x7f0a0bf1

.field public static final ps__broadcast_too_full:I = 0x7f0a0a76

.field public static final ps__broadcast_too_full_dialog_message:I = 0x7f0a0a77

.field public static final ps__broadcast_too_full_dialog_title:I = 0x7f0a0a78

.field public static final ps__broadcast_twitter_disabled:I = 0x7f0a0a79

.field public static final ps__broadcast_twitter_post_off:I = 0x7f0a0a7a

.field public static final ps__broadcast_twitter_post_on:I = 0x7f0a0a7b

.field public static final ps__broadcaster:I = 0x7f0a0a7c

.field public static final ps__broadcaster_action_ask_for_follow:I = 0x7f0a0bf2

.field public static final ps__broadcaster_action_ask_for_follow_confirmation:I = 0x7f0a0bf3

.field public static final ps__broadcaster_action_ask_for_share:I = 0x7f0a0bf4

.field public static final ps__broadcaster_action_ask_for_share_confirmation:I = 0x7f0a0bf5

.field public static final ps__broadcaster_audio_muted:I = 0x7f0a0a7d

.field public static final ps__broadcaster_kick_block:I = 0x7f0a0a7e

.field public static final ps__broadcaster_kicked_me:I = 0x7f0a0a7f

.field public static final ps__broadcaster_kicked_me_detailed:I = 0x7f0a0a80

.field public static final ps__broadcaster_kicked_me_explanation:I = 0x7f0a0a81

.field public static final ps__broadcaster_unmute:I = 0x7f0a0a82

.field public static final ps__broadcasting_live:I = 0x7f0a0a83

.field public static final ps__btn_go_live:I = 0x7f0a0a84

.field public static final ps__btn_stop_broadcast:I = 0x7f0a0a85

.field public static final ps__camera_double_tap:I = 0x7f0a0a86

.field public static final ps__camera_swipe_down:I = 0x7f0a0a87

.field public static final ps__cannot_play:I = 0x7f0a0a88

.field public static final ps__channels_action_change_name:I = 0x7f0a0a89

.field public static final ps__channels_action_create:I = 0x7f0a0a8a

.field public static final ps__channels_action_create_anon:I = 0x7f0a0a8b

.field public static final ps__channels_action_delete:I = 0x7f0a0a8c

.field public static final ps__channels_action_leave:I = 0x7f0a0a8d

.field public static final ps__channels_action_member_add:I = 0x7f0a0a8e

.field public static final ps__channels_action_member_add_anon:I = 0x7f0a0a8f

.field public static final ps__channels_action_member_remove:I = 0x7f0a0a90

.field public static final ps__channels_action_member_remove_anon:I = 0x7f0a0a91

.field public static final ps__channels_action_mute_notifications:I = 0x7f0a0a92

.field public static final ps__channels_action_rename:I = 0x7f0a0a93

.field public static final ps__channels_action_rename_anon:I = 0x7f0a0a94

.field public static final ps__channels_action_unmute_notifications:I = 0x7f0a0a95

.field public static final ps__channels_action_view_history:I = 0x7f0a0bf6

.field public static final ps__channels_add_members:I = 0x7f0a0a96

.field public static final ps__channels_add_members_count:I = 0x7f0a0a97

.field public static final ps__channels_add_members_description:I = 0x7f0a0a98

.field public static final ps__channels_create:I = 0x7f0a0a99

.field public static final ps__channels_create_private_channel_description:I = 0x7f0a0bf7

.field public static final ps__channels_delete_warning_message:I = 0x7f0a0a9a

.field public static final ps__channels_delete_warning_ok:I = 0x7f0a0a9b

.field public static final ps__channels_delete_warning_title:I = 0x7f0a0a9c

.field public static final ps__channels_history:I = 0x7f0a0bf8

.field public static final ps__channels_invalid_name:I = 0x7f0a0a9d

.field public static final ps__channels_leave_warning_message:I = 0x7f0a0a9e

.field public static final ps__channels_leave_warning_ok:I = 0x7f0a0a9f

.field public static final ps__channels_leave_warning_title:I = 0x7f0a0aa0

.field public static final ps__channels_manage_private_channel_description:I = 0x7f0a0bf9

.field public static final ps__channels_members:I = 0x7f0a0aa1

.field public static final ps__channels_name_error:I = 0x7f0a0aa2

.field public static final ps__channels_name_hint:I = 0x7f0a0aa3

.field public static final ps__channels_private_divider:I = 0x7f0a0aa4

.field public static final ps__channels_public_divider:I = 0x7f0a0aa5

.field public static final ps__channels_remove_member_dialog_ok:I = 0x7f0a0aa6

.field public static final ps__channels_remove_member_dialog_title:I = 0x7f0a0aa7

.field public static final ps__channels_update_name_dialog_ok:I = 0x7f0a0aa8

.field public static final ps__chat_ask_for_share:I = 0x7f0a0bfa

.field public static final ps__chat_join:I = 0x7f0a0aa9

.field public static final ps__chat_join_new_user:I = 0x7f0a0bfb

.field public static final ps__chat_prompt_follow_broadcaster:I = 0x7f0a0aaa

.field public static final ps__chat_share_screenshot_twitter:I = 0x7f0a0aab

.field public static final ps__chat_status_moderation_disabled:I = 0x7f0a0aac

.field public static final ps__chat_status_moderation_disabled_global:I = 0x7f0a0aad

.field public static final ps__chat_status_moderation_limited:I = 0x7f0a0aae

.field public static final ps__chat_warning_dialog_btn_cancel:I = 0x7f0a0aaf

.field public static final ps__chat_warning_dialog_btn_send:I = 0x7f0a0ab0

.field public static final ps__chat_warning_dialog_message:I = 0x7f0a0ab1

.field public static final ps__chat_warning_dialog_title:I = 0x7f0a0ab2

.field public static final ps__comment_hint:I = 0x7f0a0ab3

.field public static final ps__connecting:I = 0x7f0a0ab4

.field public static final ps__connection_error:I = 0x7f0a0ab5

.field public static final ps__convicted_abuse:I = 0x7f0a0ab6

.field public static final ps__cta_app_name:I = 0x7f0a0ab7

.field public static final ps__cta_install_app:I = 0x7f0a0ab8

.field public static final ps__cta_open_app:I = 0x7f0a0ab9

.field public static final ps__cta_view_broadcasts:I = 0x7f0a0aba

.field public static final ps__days:I = 0x7f0a0abb

.field public static final ps__delete_broadcast_cancel:I = 0x7f0a0abc

.field public static final ps__delete_broadcast_confirm:I = 0x7f0a0abd

.field public static final ps__delete_broadcast_dialog:I = 0x7f0a0abe

.field public static final ps__delete_broadcast_error:I = 0x7f0a0abf

.field public static final ps__delete_broadcast_success:I = 0x7f0a0ac0

.field public static final ps__dialog_btn_cancel_end_broadcast:I = 0x7f0a0ac1

.field public static final ps__dialog_btn_confirm_end_broadcast:I = 0x7f0a0ac2

.field public static final ps__dialog_btn_no:I = 0x7f0a0ac3

.field public static final ps__dialog_btn_yes:I = 0x7f0a0ac4

.field public static final ps__dialog_message_cancel_broadcast:I = 0x7f0a0ac5

.field public static final ps__dialog_message_end_broadcast:I = 0x7f0a0ac6

.field public static final ps__dialog_moderator_learn_more_body:I = 0x7f0a0bfc

.field public static final ps__dialog_moderator_learn_more_button:I = 0x7f0a0ac7

.field public static final ps__dialog_moderator_learn_more_disable:I = 0x7f0a0ac8

.field public static final ps__dialog_moderator_learn_more_example:I = 0x7f0a0ac9

.field public static final ps__dialog_moderator_learn_more_message_body:I = 0x7f0a0aca

.field public static final ps__dialog_moderator_learn_more_title:I = 0x7f0a0acb

.field public static final ps__download_periscope:I = 0x7f0a0acc

.field public static final ps__duration:I = 0x7f0a0acd

.field public static final ps__ellipsis:I = 0x7f0a0ace

.field public static final ps__end_broadcast:I = 0x7f0a0acf

.field public static final ps__ended_broadcast:I = 0x7f0a0ad0

.field public static final ps__failed_to_mark_broadcast_persistent:I = 0x7f0a0ad1

.field public static final ps__featured:I = 0x7f0a0ad2

.field public static final ps__featured_digits:I = 0x7f0a0ad3

.field public static final ps__featured_most_loved:I = 0x7f0a0ad4

.field public static final ps__featured_popular:I = 0x7f0a0ad5

.field public static final ps__featured_twitter:I = 0x7f0a0ad6

.field public static final ps__featured_users:I = 0x7f0a0ad7

.field public static final ps__follow_action:I = 0x7f0a0bfd

.field public static final ps__follow_limit_error:I = 0x7f0a0ad8

.field public static final ps__followers:I = 0x7f0a0ad9

.field public static final ps__following:I = 0x7f0a0ada

.field public static final ps__following_action:I = 0x7f0a0bfe

.field public static final ps__four_following_in_chat:I = 0x7f0a0adb

.field public static final ps__four_plus_following_in_chat:I = 0x7f0a0adc

.field public static final ps__help_moderate_content:I = 0x7f0a0add

.field public static final ps__highlights_btn_play:I = 0x7f0a0ade

.field public static final ps__hours:I = 0x7f0a0adf

.field public static final ps__initializing:I = 0x7f0a0ae0

.field public static final ps__install_description:I = 0x7f0a0ae1

.field public static final ps__install_no_thanks:I = 0x7f0a0ae2

.field public static final ps__interstitial_tos:I = 0x7f0a0ae3

.field public static final ps__invited_followers:I = 0x7f0a0ae4

.field public static final ps__live:I = 0x7f0a0ae5

.field public static final ps__loading:I = 0x7f0a0ae6

.field public static final ps__local_prompt_conviction_broadcast_disabled:I = 0x7f0a0ae7

.field public static final ps__local_prompt_conviction_broadcast_disabled_with_body_and_reason:I = 0x7f0a0ae8

.field public static final ps__local_prompt_conviction_broadcast_disabled_with_reason:I = 0x7f0a0ae9

.field public static final ps__local_prompt_conviction_broadcast_suspended:I = 0x7f0a0aea

.field public static final ps__local_prompt_conviction_broadcast_suspended_with_body_and_reason:I = 0x7f0a0aeb

.field public static final ps__local_prompt_conviction_broadcast_suspended_with_reason:I = 0x7f0a0aec

.field public static final ps__local_prompt_conviction_global_disabled:I = 0x7f0a0aed

.field public static final ps__local_prompt_conviction_global_disabled_with_body_and_reason:I = 0x7f0a0aee

.field public static final ps__local_prompt_conviction_global_disabled_with_reason:I = 0x7f0a0aef

.field public static final ps__local_prompt_conviction_global_suspended:I = 0x7f0a0af0

.field public static final ps__local_prompt_conviction_global_suspended_with_body_and_reason:I = 0x7f0a0af1

.field public static final ps__local_prompt_conviction_global_suspended_with_reason:I = 0x7f0a0af2

.field public static final ps__local_prompt_moderator_feedback:I = 0x7f0a0af3

.field public static final ps__location_settings_off:I = 0x7f0a0af4

.field public static final ps__minutes:I = 0x7f0a0af5

.field public static final ps__moderate_wait_for_responses:I = 0x7f0a0af6

.field public static final ps__moderator_did_not_vote:I = 0x7f0a0af7

.field public static final ps__moderator_learn_more:I = 0x7f0a0af8

.field public static final ps__moderator_negative:I = 0x7f0a0af9

.field public static final ps__moderator_negative_spam:I = 0x7f0a0afa

.field public static final ps__moderator_neutral:I = 0x7f0a0afb

.field public static final ps__moderator_positive:I = 0x7f0a0afc

.field public static final ps__moderator_verdict:I = 0x7f0a0afd

.field public static final ps__moderator_verdict_consequence:I = 0x7f0a0afe

.field public static final ps__months:I = 0x7f0a0aff

.field public static final ps__more_viewers:I = 0x7f0a0b00

.field public static final ps__mutual_follow:I = 0x7f0a0b01

.field public static final ps__no_replay_viewers:I = 0x7f0a0b02

.field public static final ps__no_viewers:I = 0x7f0a0b03

.field public static final ps__now:I = 0x7f0a0b04

.field public static final ps__num_hearts:I = 0x7f0a0b05

.field public static final ps__number_format_million_grouping:I = 0x7f0a0b06

.field public static final ps__number_format_millions:I = 0x7f0a0b07

.field public static final ps__number_format_thousand_grouping:I = 0x7f0a0b08

.field public static final ps__number_format_thousands:I = 0x7f0a0b09

.field public static final ps__one_following_in_chat:I = 0x7f0a0b0a

.field public static final ps__other_languages:I = 0x7f0a0b0b

.field public static final ps__peak_value:I = 0x7f0a0b0c

.field public static final ps__permissions_broadcaster:I = 0x7f0a0b0d

.field public static final ps__permissions_btn_allow:I = 0x7f0a0b0e

.field public static final ps__permissions_btn_settings:I = 0x7f0a0b0f

.field public static final ps__permissions_required:I = 0x7f0a0b10

.field public static final ps__permissions_screenshots:I = 0x7f0a0b11

.field public static final ps__placeholder_for_value:I = 0x7f0a0b12

.field public static final ps__post_broadcast_twitter:I = 0x7f0a0b13

.field public static final ps__posted_on_twitter:I = 0x7f0a0b14

.field public static final ps__pp_url:I = 0x7f0a0bff

.field public static final ps__private_broadcast_count:I = 0x7f0a0b15

.field public static final ps__private_broadcast_description:I = 0x7f0a0b16

.field public static final ps__private_broadcast_dialog:I = 0x7f0a0b17

.field public static final ps__private_broadcast_title:I = 0x7f0a0b18

.field public static final ps__profile_copy_url:I = 0x7f0a0b19

.field public static final ps__profile_copy_url_copied:I = 0x7f0a0b1a

.field public static final ps__profile_sheet_more_options_block:I = 0x7f0a0b1b

.field public static final ps__profile_sheet_more_options_mute:I = 0x7f0a0c00

.field public static final ps__profile_sheet_more_options_unblock:I = 0x7f0a0b1c

.field public static final ps__profile_sheet_more_options_unfollow:I = 0x7f0a0c01

.field public static final ps__profile_viewer_not_found:I = 0x7f0a0b1d

.field public static final ps__rate_limited:I = 0x7f0a0b1e

.field public static final ps__replay_scrub_hint_fine_control:I = 0x7f0a0b1f

.field public static final ps__replay_scrub_hint_seek:I = 0x7f0a0b20

.field public static final ps__replay_scrub_zoom_fine:I = 0x7f0a0b21

.field public static final ps__replay_scrub_zoom_half:I = 0x7f0a0b22

.field public static final ps__replay_scrub_zoom_quarter:I = 0x7f0a0b23

.field public static final ps__replay_skip_tip:I = 0x7f0a0b24

.field public static final ps__report_broadcast_action:I = 0x7f0a0b25

.field public static final ps__report_broadcast_confirm:I = 0x7f0a0b26

.field public static final ps__report_broadcast_dialog_dont_like_message:I = 0x7f0a0b27

.field public static final ps__report_broadcast_dialog_dont_like_message_extra:I = 0x7f0a0b28

.field public static final ps__report_broadcast_dialog_feedback_message:I = 0x7f0a0b29

.field public static final ps__report_broadcast_dialog_feedback_title:I = 0x7f0a0b2a

.field public static final ps__report_broadcast_dialog_self_harm_message:I = 0x7f0a0b2b

.field public static final ps__report_broadcast_dialog_self_harm_title:I = 0x7f0a0b2c

.field public static final ps__report_broadcast_dialog_sexual_content_message:I = 0x7f0a0b2d

.field public static final ps__report_broadcast_dialog_sexual_content_title:I = 0x7f0a0b2e

.field public static final ps__report_broadcast_dialog_violence_message:I = 0x7f0a0b2f

.field public static final ps__report_broadcast_dialog_violence_title:I = 0x7f0a0b30

.field public static final ps__report_broadcast_error:I = 0x7f0a0b31

.field public static final ps__report_broadcast_reason_dont_like:I = 0x7f0a0b32

.field public static final ps__report_broadcast_reason_self_harm:I = 0x7f0a0b33

.field public static final ps__report_broadcast_reason_sexual_content:I = 0x7f0a0b34

.field public static final ps__report_broadcast_reason_snippet:I = 0x7f0a0b35

.field public static final ps__report_broadcast_reason_violence:I = 0x7f0a0b36

.field public static final ps__report_broadcast_success:I = 0x7f0a0b37

.field public static final ps__retweet_broadcast_action:I = 0x7f0a0b38

.field public static final ps__retweeted_on_twitter:I = 0x7f0a0b39

.field public static final ps__save_broadcast_unsuccessful:I = 0x7f0a0b3a

.field public static final ps__search_people_hint:I = 0x7f0a0b3b

.field public static final ps__seconds:I = 0x7f0a0b3c

.field public static final ps__select_all:I = 0x7f0a0b3d

.field public static final ps__settings:I = 0x7f0a0b3e

.field public static final ps__share_broadcast_action:I = 0x7f0a0b3f

.field public static final ps__share_broadcast_all_followers:I = 0x7f0a0b40

.field public static final ps__share_broadcast_copy_link:I = 0x7f0a0b41

.field public static final ps__share_broadcast_copy_link_copied:I = 0x7f0a0b42

.field public static final ps__share_broadcast_copy_link_copied_fail:I = 0x7f0a0b43

.field public static final ps__share_broadcast_count:I = 0x7f0a0b44

.field public static final ps__share_broadcast_description:I = 0x7f0a0b45

.field public static final ps__share_broadcast_failed:I = 0x7f0a0b46

.field public static final ps__share_broadcast_other_app:I = 0x7f0a0b47

.field public static final ps__share_broadcast_subset_followers:I = 0x7f0a0b48

.field public static final ps__share_broadcast_title:I = 0x7f0a0b49

.field public static final ps__share_post_tweet:I = 0x7f0a0b4a

.field public static final ps__share_post_tweet_fail:I = 0x7f0a0b4b

.field public static final ps__share_post_tweet_success:I = 0x7f0a0b4c

.field public static final ps__share_post_tweet_text:I = 0x7f0a0b4d

.field public static final ps__share_post_tweet_text_username:I = 0x7f0a0b4e

.field public static final ps__share_post_tweeting:I = 0x7f0a0b4f

.field public static final ps__show_stats:I = 0x7f0a0b50

.field public static final ps__start_broadcast_error:I = 0x7f0a0b51

.field public static final ps__starting_broadcast:I = 0x7f0a0b52

.field public static final ps__stat_live_viewer_only:I = 0x7f0a0b53

.field public static final ps__stat_live_viewers:I = 0x7f0a0b54

.field public static final ps__stat_replay_viewer_only:I = 0x7f0a0b55

.field public static final ps__stat_replay_viewers:I = 0x7f0a0b56

.field public static final ps__stat_total_viewers:I = 0x7f0a0b57

.field public static final ps__store_graph_to_disk_error:I = 0x7f0a0b58

.field public static final ps__superfan_of_action_sheet_text:I = 0x7f0a0c02

.field public static final ps__superfans_title:I = 0x7f0a0c03

.field public static final ps__three_following_in_chat:I = 0x7f0a0b59

.field public static final ps__time_per_viewer:I = 0x7f0a0b5a

.field public static final ps__time_per_viewer_live:I = 0x7f0a0b5b

.field public static final ps__time_per_viewer_replay:I = 0x7f0a0b5c

.field public static final ps__tip_broadcaster_1:I = 0x7f0a0c04

.field public static final ps__tip_broadcaster_11:I = 0x7f0a0c05

.field public static final ps__tip_broadcaster_2:I = 0x7f0a0c06

.field public static final ps__tip_broadcaster_3:I = 0x7f0a0c07

.field public static final ps__tip_broadcaster_4:I = 0x7f0a0c08

.field public static final ps__tip_broadcaster_5:I = 0x7f0a0c09

.field public static final ps__tip_broadcaster_6:I = 0x7f0a0c0a

.field public static final ps__tip_broadcaster_7:I = 0x7f0a0c0b

.field public static final ps__tip_broadcaster_8:I = 0x7f0a0c0c

.field public static final ps__tip_broadcaster_9:I = 0x7f0a0c0d

.field public static final ps__tip_pre_broadcast_1:I = 0x7f0a0c0e

.field public static final ps__tip_pre_broadcast_2:I = 0x7f0a0c0f

.field public static final ps__tip_pre_broadcast_3:I = 0x7f0a0c10

.field public static final ps__tip_pre_broadcast_4:I = 0x7f0a0c11

.field public static final ps__tip_pre_broadcast_5:I = 0x7f0a0c12

.field public static final ps__tip_pre_broadcast_6:I = 0x7f0a0c13

.field public static final ps__tip_pre_broadcast_8:I = 0x7f0a0c14

.field public static final ps__tip_pre_broadcast_9:I = 0x7f0a0c15

.field public static final ps__tip_viewer_1:I = 0x7f0a0c16

.field public static final ps__tip_viewer_2:I = 0x7f0a0c17

.field public static final ps__tip_viewer_3:I = 0x7f0a0c18

.field public static final ps__tip_viewer_4:I = 0x7f0a0c19

.field public static final ps__token:I = 0x7f0a0b5d

.field public static final ps__tos_url:I = 0x7f0a0c1a

.field public static final ps__total_time_watched:I = 0x7f0a0b5e

.field public static final ps__total_time_watched_live:I = 0x7f0a0b5f

.field public static final ps__total_time_watched_replay:I = 0x7f0a0b60

.field public static final ps__trying_to_reconnect:I = 0x7f0a0b61

.field public static final ps__tweet_broadcast:I = 0x7f0a0b62

.field public static final ps__tweet_broadcast_detailed:I = 0x7f0a0b63

.field public static final ps__tweet_broadcast_failed:I = 0x7f0a0b64

.field public static final ps__two_following_in_chat:I = 0x7f0a0b65

.field public static final ps__unblock_dialog_btn_confirm:I = 0x7f0a0b66

.field public static final ps__unblock_dialog_title:I = 0x7f0a0b67

.field public static final ps__username_format:I = 0x7f0a0b68

.field public static final ps__vip_badge_bronze:I = 0x7f0a0b69

.field public static final ps__vip_badge_description:I = 0x7f0a0b6a

.field public static final ps__vip_badge_gold:I = 0x7f0a0b6b

.field public static final ps__vip_badge_learn_more:I = 0x7f0a0b6c

.field public static final ps__vip_badge_silver:I = 0x7f0a0b6d

.field public static final ps__watch_live:I = 0x7f0a0b6e

.field public static final ps__weeks:I = 0x7f0a0b6f

.field public static final ps__years:I = 0x7f0a0b70

.field public static final search_menu_title:I = 0x7f0a0029

.field public static final status_bar_notification_info_overflow:I = 0x7f0a002a
