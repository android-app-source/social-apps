.class Ltv/periscope/android/library/a$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/library/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/ui/broadcast/af;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ltv/periscope/android/library/c;

.field private final c:Ltv/periscope/android/player/PlayMode;

.field private final d:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ltv/periscope/android/ui/broadcast/af;Ltv/periscope/android/library/c;Ltv/periscope/android/player/PlayMode;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 359
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltv/periscope/android/library/a$a;->a:Ljava/lang/ref/WeakReference;

    .line 360
    iput-object p2, p0, Ltv/periscope/android/library/a$a;->b:Ltv/periscope/android/library/c;

    .line 361
    iput-object p3, p0, Ltv/periscope/android/library/a$a;->c:Ltv/periscope/android/player/PlayMode;

    .line 363
    iget-object v0, p0, Ltv/periscope/android/library/a$a;->b:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 364
    iget-object v0, p0, Ltv/periscope/android/library/a$a;->b:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0, p4}, Ltv/periscope/android/api/ApiManager;->getBroadcastIdForShareToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/library/a$a;->d:Ljava/lang/String;

    .line 365
    return-void
.end method

.method synthetic constructor <init>(Ltv/periscope/android/ui/broadcast/af;Ltv/periscope/android/library/c;Ltv/periscope/android/player/PlayMode;Ljava/lang/String;Ltv/periscope/android/library/a$1;)V
    .locals 0

    .prologue
    .line 351
    invoke-direct {p0, p1, p2, p3, p4}, Ltv/periscope/android/library/a$a;-><init>(Ltv/periscope/android/ui/broadcast/af;Ltv/periscope/android/library/c;Ltv/periscope/android/player/PlayMode;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 3

    .prologue
    .line 369
    sget-object v0, Ltv/periscope/android/library/a$1;->a:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 371
    :pswitch_0
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->b:Ljava/lang/String;

    iget-object v1, p0, Ltv/periscope/android/library/a$a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Ltv/periscope/android/library/a$a;->b:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 373
    iget-object v0, p0, Ltv/periscope/android/library/a$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/af;

    .line 374
    if-eqz v0, :cond_0

    .line 377
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 378
    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v1, Ltv/periscope/android/api/BroadcastResponse;

    .line 379
    iget-object v1, v1, Ltv/periscope/android/api/BroadcastResponse;->broadcastId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/af;->c(Ljava/lang/String;)V

    .line 380
    iget-object v1, p0, Ltv/periscope/android/library/a$a;->c:Ltv/periscope/android/player/PlayMode;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/af;->a(Ltv/periscope/android/player/PlayMode;)V

    goto :goto_0

    .line 382
    :cond_1
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/af;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 369
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
