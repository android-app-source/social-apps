.class Ltv/periscope/android/video/b$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/video/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/video/b;

.field private final b:D

.field private c:D

.field private d:J

.field private e:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ltv/periscope/android/video/b$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ltv/periscope/android/video/b;D)V
    .locals 2

    .prologue
    .line 72
    iput-object p1, p0, Ltv/periscope/android/video/b$b;->a:Ltv/periscope/android/video/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/video/b$b;->c:D

    .line 68
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/video/b$b;->d:J

    .line 69
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/b$b;->e:Ljava/util/Vector;

    .line 73
    iput-wide p2, p0, Ltv/periscope/android/video/b$b;->b:D

    .line 74
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 98
    iget-wide v2, p0, Ltv/periscope/android/video/b$b;->c:D

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    .line 100
    iget-wide v0, p0, Ltv/periscope/android/video/b$b;->d:J

    long-to-double v0, v0

    iget-wide v2, p0, Ltv/periscope/android/video/b$b;->c:D

    div-double/2addr v0, v2

    double-to-int v0, v0

    .line 102
    :cond_0
    return v0
.end method

.method public a(DI)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 78
    iget-object v0, p0, Ltv/periscope/android/video/b$b;->e:Ljava/util/Vector;

    new-instance v1, Ltv/periscope/android/video/b$c;

    iget-object v2, p0, Ltv/periscope/android/video/b$b;->a:Ltv/periscope/android/video/b;

    invoke-direct {v1, v2, p1, p2, p3}, Ltv/periscope/android/video/b$c;-><init>(Ltv/periscope/android/video/b;DI)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 79
    iget-wide v0, p0, Ltv/periscope/android/video/b$b;->d:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Ltv/periscope/android/video/b$b;->d:J

    .line 81
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/video/b$b;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 83
    iget-object v0, p0, Ltv/periscope/android/video/b$b;->e:Ljava/util/Vector;

    invoke-virtual {v0, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/video/b$c;

    .line 84
    iget-wide v2, v0, Ltv/periscope/android/video/b$c;->a:D

    sub-double v2, p1, v2

    iput-wide v2, p0, Ltv/periscope/android/video/b$b;->c:D

    .line 85
    iget-wide v2, p0, Ltv/periscope/android/video/b$b;->c:D

    iget-wide v4, p0, Ltv/periscope/android/video/b$b;->b:D

    cmpg-double v1, v2, v4

    if-gez v1, :cond_1

    .line 93
    :cond_0
    return-void

    .line 90
    :cond_1
    iget-wide v2, p0, Ltv/periscope/android/video/b$b;->d:J

    iget v0, v0, Ltv/periscope/android/video/b$c;->b:I

    int-to-long v0, v0

    sub-long v0, v2, v0

    iput-wide v0, p0, Ltv/periscope/android/video/b$b;->d:J

    .line 91
    iget-object v0, p0, Ltv/periscope/android/video/b$b;->e:Ljava/util/Vector;

    invoke-virtual {v0, v6}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public b()D
    .locals 4

    .prologue
    .line 106
    const-wide/16 v0, 0x0

    .line 107
    iget-object v2, p0, Ltv/periscope/android/video/b$b;->e:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 109
    iget-wide v0, p0, Ltv/periscope/android/video/b$b;->c:D

    iget-object v2, p0, Ltv/periscope/android/video/b$b;->e:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 111
    :cond_0
    return-wide v0
.end method
