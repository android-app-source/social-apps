.class public interface abstract Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/video/lhls/LHLSPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LHLSPlayerListener"
.end annotation


# virtual methods
.method public abstract makeNetRequest(Ljava/lang/String;J)Ltv/periscope/android/video/lhls/HTTPRequest;
.end method

.method public abstract onAudio([BDD)V
.end method

.method public abstract onAudioFormat(II)V
.end method

.method public abstract onDiscontinuity()V
.end method

.method public abstract onEOS()V
.end method

.method public abstract onMetadata([BD)V
.end method

.method public abstract onVideo([BDD)V
.end method

.method public abstract onVideoFormat([B[BII)V
.end method
