.class public Ltv/periscope/android/video/lhls/HTTPRequest;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;,
        Ltv/periscope/android/video/lhls/HTTPRequest$HTTPRequestDelegate;
    }
.end annotation


# instance fields
.field private mConn:Lczb;

.field private final mCookieStore:Ldct;

.field private mDelegate:Ltv/periscope/android/video/lhls/HTTPRequest$HTTPRequestDelegate;

.field private mNativeHandle:J

.field private mShouldCancel:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mTask:Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;

.field private mURL:Ljava/lang/String;

.field private final mUserAgent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLdct;Ljava/lang/String;Ltv/periscope/android/video/lhls/HTTPRequest$HTTPRequestDelegate;)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mNativeHandle:J

    .line 36
    const-string/jumbo v0, ""

    iput-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mURL:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mTask:Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;

    .line 57
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mShouldCancel:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 45
    iput-wide p2, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mNativeHandle:J

    .line 46
    iput-object p1, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mURL:Ljava/lang/String;

    .line 47
    iput-object p6, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mDelegate:Ltv/periscope/android/video/lhls/HTTPRequest$HTTPRequestDelegate;

    .line 48
    iput-object p5, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mUserAgent:Ljava/lang/String;

    .line 49
    iput-object p4, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mCookieStore:Ldct;

    .line 50
    return-void
.end method

.method static synthetic access$000(Ltv/periscope/android/video/lhls/HTTPRequest;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mShouldCancel:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$100(Ltv/periscope/android/video/lhls/HTTPRequest;)Lczb;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mConn:Lczb;

    return-object v0
.end method

.method static synthetic access$102(Ltv/periscope/android/video/lhls/HTTPRequest;Lczb;)Lczb;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mConn:Lczb;

    return-object p1
.end method

.method static synthetic access$200(Ltv/periscope/android/video/lhls/HTTPRequest;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mUserAgent:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Ltv/periscope/android/video/lhls/HTTPRequest;)Ldct;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mCookieStore:Ldct;

    return-object v0
.end method

.method static synthetic access$400(Ltv/periscope/android/video/lhls/HTTPRequest;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ltv/periscope/android/video/lhls/HTTPRequest;->onComplete()V

    return-void
.end method

.method static synthetic access$502(Ltv/periscope/android/video/lhls/HTTPRequest;Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;)Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mTask:Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;

    return-object p1
.end method

.method static synthetic access$600(Ltv/periscope/android/video/lhls/HTTPRequest;[BI)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/video/lhls/HTTPRequest;->signalData([BI)V

    return-void
.end method

.method static synthetic access$700(Ltv/periscope/android/video/lhls/HTTPRequest;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ltv/periscope/android/video/lhls/HTTPRequest;->signalEndOfStream()V

    return-void
.end method

.method static synthetic access$800(Ltv/periscope/android/video/lhls/HTTPRequest;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Ltv/periscope/android/video/lhls/HTTPRequest;->signalError(Ljava/lang/String;)V

    return-void
.end method

.method private onComplete()V
    .locals 2

    .prologue
    .line 151
    .line 152
    monitor-enter p0

    .line 154
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mDelegate:Ltv/periscope/android/video/lhls/HTTPRequest$HTTPRequestDelegate;

    .line 155
    const/4 v1, 0x0

    iput-object v1, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mDelegate:Ltv/periscope/android/video/lhls/HTTPRequest$HTTPRequestDelegate;

    .line 156
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    if-eqz v0, :cond_0

    .line 159
    invoke-interface {v0, p0}, Ltv/periscope/android/video/lhls/HTTPRequest$HTTPRequestDelegate;->onRequestComplete(Ltv/periscope/android/video/lhls/HTTPRequest;)V

    .line 161
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mConn:Lczb;

    if-eqz v0, :cond_1

    .line 164
    :try_start_1
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mConn:Lczb;

    invoke-virtual {v0}, Lczb;->close()V
    :try_end_1
    .catch Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException; {:try_start_1 .. :try_end_1} :catch_0

    .line 169
    :cond_1
    :goto_0
    return-void

    .line 156
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;->printStackTrace()V

    goto :goto_0
.end method

.method private native onData(J[BI)V
.end method

.method private native onEndOfStream(J)V
.end method

.method private native onError(JLjava/lang/String;)V
.end method

.method private signalData([BI)V
    .locals 4

    .prologue
    .line 186
    monitor-enter p0

    .line 188
    :try_start_0
    iget-wide v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mNativeHandle:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 190
    iget-wide v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mNativeHandle:J

    invoke-direct {p0, v0, v1, p1, p2}, Ltv/periscope/android/video/lhls/HTTPRequest;->onData(J[BI)V

    .line 192
    :cond_0
    monitor-exit p0

    .line 193
    return-void

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private signalEndOfStream()V
    .locals 4

    .prologue
    .line 197
    monitor-enter p0

    .line 199
    :try_start_0
    iget-wide v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mNativeHandle:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 201
    iget-wide v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mNativeHandle:J

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/video/lhls/HTTPRequest;->onEndOfStream(J)V

    .line 203
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    invoke-direct {p0}, Ltv/periscope/android/video/lhls/HTTPRequest;->onComplete()V

    .line 205
    return-void

    .line 203
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private signalError(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 173
    monitor-enter p0

    .line 175
    :try_start_0
    const-string/jumbo v0, "LHLS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Socket error for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mShouldCancel:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mNativeHandle:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 178
    iget-wide v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mNativeHandle:J

    invoke-direct {p0, v0, v1, p1}, Ltv/periscope/android/video/lhls/HTTPRequest;->onError(JLjava/lang/String;)V

    .line 180
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/video/lhls/HTTPRequest;->onComplete()V

    .line 181
    monitor-exit p0

    .line 182
    return-void

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public cancelRequest()V
    .locals 2

    .prologue
    .line 142
    monitor-enter p0

    .line 144
    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mNativeHandle:J

    .line 145
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    invoke-virtual {p0}, Ltv/periscope/android/video/lhls/HTTPRequest;->stop()V

    .line 147
    return-void

    .line 145
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public fetch()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 113
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mConn:Lczb;

    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {p0}, Ltv/periscope/android/video/lhls/HTTPRequest;->stop()V

    .line 117
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mShouldCancel:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 118
    new-instance v0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;-><init>(Ltv/periscope/android/video/lhls/HTTPRequest;Ltv/periscope/android/video/lhls/HTTPRequest$1;)V

    iput-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mTask:Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;

    .line 119
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mTask:Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;

    new-array v1, v4, [Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mURL:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 120
    return v4
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mURL:Ljava/lang/String;

    return-object v0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mShouldCancel:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 127
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mTask:Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mTask:Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->cancel(Z)Z

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mConn:Lczb;

    if-eqz v0, :cond_0

    .line 131
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest;->mConn:Lczb;

    invoke-virtual {v0}, Lczb;->close()V
    :try_end_0
    .catch Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 132
    :catch_0
    move-exception v0

    .line 133
    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;->printStackTrace()V

    goto :goto_0
.end method
