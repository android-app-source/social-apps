.class public Ltv/periscope/android/video/lhls/LHLSPlayer;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;
    }
.end annotation


# static fields
.field private static sHasLoaded:Z

.field private static sTriedToLoad:Z


# instance fields
.field private mDelegate:Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;

.field private mNativeHandle:J


# direct methods
.method public constructor <init>(Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mDelegate:Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;

    .line 34
    return-void
.end method

.method private native Play(Ljava/lang/String;)J
.end method

.method private native Stop(J)V
.end method

.method private static declared-synchronized init()V
    .locals 3

    .prologue
    .line 61
    const-class v1, Ltv/periscope/android/video/lhls/LHLSPlayer;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Ltv/periscope/android/video/lhls/LHLSPlayer;->sTriedToLoad:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 63
    :try_start_1
    invoke-static {}, Ltv/periscope/android/video/lhls/LHLSPlayer;->isCpuSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string/jumbo v0, "lhlslib-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 65
    const/4 v0, 0x1

    sput-boolean v0, Ltv/periscope/android/video/lhls/LHLSPlayer;->sHasLoaded:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/LinkageError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 69
    :cond_0
    const/4 v0, 0x1

    :try_start_2
    sput-boolean v0, Ltv/periscope/android/video/lhls/LHLSPlayer;->sTriedToLoad:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 72
    :cond_1
    :goto_0
    monitor-exit v1

    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 69
    :goto_1
    const/4 v0, 0x1

    :try_start_3
    sput-boolean v0, Ltv/periscope/android/video/lhls/LHLSPlayer;->sTriedToLoad:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 69
    :catchall_1
    move-exception v0

    const/4 v2, 0x1

    :try_start_4
    sput-boolean v2, Ltv/periscope/android/video/lhls/LHLSPlayer;->sTriedToLoad:Z

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 67
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private static isCpuSupported()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 75
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_2

    .line 76
    sget-object v2, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 77
    const-string/jumbo v3, "armeabi-v7a"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "arm64-v8a"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "x86"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "x86_64"

    .line 78
    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 80
    :cond_1
    :goto_0
    return v0

    :cond_2
    sget-object v2, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    const-string/jumbo v3, "armeabi-v7a"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    const-string/jumbo v3, "arm64-v8a"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    const-string/jumbo v3, "x86"

    .line 81
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    const-string/jumbo v3, "x86_64"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static isLoaded()Z
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Ltv/periscope/android/video/lhls/LHLSPlayer;->init()V

    .line 57
    sget-boolean v0, Ltv/periscope/android/video/lhls/LHLSPlayer;->sHasLoaded:Z

    return v0
.end method

.method private onAudio([BDD)V
    .locals 6

    .prologue
    .line 103
    iget-object v0, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mDelegate:Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;->onAudio([BDD)V

    .line 104
    return-void
.end method

.method private onAudioFormat(II)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mDelegate:Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;

    invoke-interface {v0, p1, p2}, Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;->onAudioFormat(II)V

    .line 100
    return-void
.end method

.method private onDiscontinuity()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mDelegate:Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;

    invoke-interface {v0}, Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;->onDiscontinuity()V

    .line 112
    return-void
.end method

.method private onEOS()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mDelegate:Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;

    invoke-interface {v0}, Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;->onEOS()V

    .line 116
    return-void
.end method

.method private onMetadata([BD)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mDelegate:Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;

    invoke-interface {v0, p1, p2, p3}, Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;->onMetadata([BD)V

    .line 108
    return-void
.end method

.method private onVideo([BDD)V
    .locals 6

    .prologue
    .line 95
    iget-object v0, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mDelegate:Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;->onVideo([BDD)V

    .line 96
    return-void
.end method

.method private onVideoFormat([B[BII)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mDelegate:Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;

    invoke-interface {v0, p1, p2, p3, p4}, Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;->onVideoFormat([B[BII)V

    .line 92
    return-void
.end method


# virtual methods
.method public StartPlayback(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 38
    iget-wide v2, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mNativeHandle:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v0

    .line 42
    :cond_1
    invoke-direct {p0, p1}, Ltv/periscope/android/video/lhls/LHLSPlayer;->Play(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mNativeHandle:J

    .line 43
    iget-wide v2, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mNativeHandle:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public StopPlayback()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 48
    iget-wide v0, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mNativeHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 50
    iget-wide v0, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mNativeHandle:J

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/video/lhls/LHLSPlayer;->Stop(J)V

    .line 51
    iput-wide v2, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mNativeHandle:J

    .line 53
    :cond_0
    return-void
.end method

.method public makeNetRequest(Ljava/lang/String;J)Ltv/periscope/android/video/lhls/HTTPRequest;
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Ltv/periscope/android/video/lhls/LHLSPlayer;->mDelegate:Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;

    invoke-interface {v0, p1, p2, p3}, Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;->makeNetRequest(Ljava/lang/String;J)Ltv/periscope/android/video/lhls/HTTPRequest;

    move-result-object v0

    return-object v0
.end method
