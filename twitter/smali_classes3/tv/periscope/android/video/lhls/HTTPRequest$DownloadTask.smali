.class Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/video/lhls/HTTPRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Ltv/periscope/android/video/lhls/HTTPRequest;


# direct methods
.method private constructor <init>(Ltv/periscope/android/video/lhls/HTTPRequest;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ltv/periscope/android/video/lhls/HTTPRequest;Ltv/periscope/android/video/lhls/HTTPRequest$1;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;-><init>(Ltv/periscope/android/video/lhls/HTTPRequest;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 59
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 64
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    # getter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mShouldCancel:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$000(Ltv/periscope/android/video/lhls/HTTPRequest;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_3

    .line 65
    const/4 v0, 0x0

    aget-object v8, p1, v0

    .line 66
    iget-object v9, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    new-instance v0, Lczb;

    iget-object v1, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    # getter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mUserAgent:Ljava/lang/String;
    invoke-static {v1}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$200(Ltv/periscope/android/video/lhls/HTTPRequest;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x1f40

    const/16 v5, 0x1f40

    const/4 v6, 0x1

    iget-object v7, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    .line 68
    # getter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mCookieStore:Ldct;
    invoke-static {v7}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$300(Ltv/periscope/android/video/lhls/HTTPRequest;)Ldct;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lczb;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer/util/Predicate;Lcom/google/android/exoplayer/upstream/TransferListener;IIZLdct;)V

    .line 66
    # setter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mConn:Lczb;
    invoke-static {v9, v0}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$102(Ltv/periscope/android/video/lhls/HTTPRequest;Lczb;)Lczb;

    .line 70
    new-instance v0, Lcom/google/android/exoplayer/upstream/DataSpec;

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer/upstream/DataSpec;-><init>(Landroid/net/Uri;I)V

    .line 71
    iget-object v1, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    # getter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mConn:Lczb;
    invoke-static {v1}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$100(Ltv/periscope/android/video/lhls/HTTPRequest;)Lczb;

    move-result-object v1

    invoke-virtual {v1, v0}, Lczb;->open(Lcom/google/android/exoplayer/upstream/DataSpec;)J

    .line 75
    const/16 v0, 0x5e0

    new-array v0, v0, [B

    .line 77
    :goto_0
    invoke-virtual {p0}, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    # getter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mShouldCancel:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v1}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$000(Ltv/periscope/android/video/lhls/HTTPRequest;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 78
    :cond_0
    const-string/jumbo v0, "LHLS"

    const-string/jumbo v1, "Task cancelled"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    # invokes: Ltv/periscope/android/video/lhls/HTTPRequest;->onComplete()V
    invoke-static {v0}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$400(Ltv/periscope/android/video/lhls/HTTPRequest;)V

    .line 80
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    const/4 v1, 0x0

    # setter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mTask:Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;
    invoke-static {v0, v1}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$502(Ltv/periscope/android/video/lhls/HTTPRequest;Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;)Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;

    .line 107
    :goto_1
    return-object v10

    .line 83
    :cond_1
    iget-object v1, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    # getter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mConn:Lczb;
    invoke-static {v1}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$100(Ltv/periscope/android/video/lhls/HTTPRequest;)Lczb;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x5e0

    invoke-virtual {v1, v0, v2, v3}, Lczb;->read([BII)I

    move-result v1

    .line 85
    invoke-virtual {p0}, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    # getter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mShouldCancel:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v2}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$000(Ltv/periscope/android/video/lhls/HTTPRequest;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 86
    :cond_2
    const-string/jumbo v0, "LHLS"

    const-string/jumbo v1, "Task cancelled"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    # invokes: Ltv/periscope/android/video/lhls/HTTPRequest;->onComplete()V
    invoke-static {v0}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$400(Ltv/periscope/android/video/lhls/HTTPRequest;)V

    .line 88
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    const/4 v1, 0x0

    # setter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mTask:Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;
    invoke-static {v0, v1}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$502(Ltv/periscope/android/video/lhls/HTTPRequest;Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;)Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 102
    :catch_0
    move-exception v0

    .line 104
    iget-object v1, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    # invokes: Ltv/periscope/android/video/lhls/HTTPRequest;->signalError(Ljava/lang/String;)V
    invoke-static {v1, v0}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$800(Ltv/periscope/android/video/lhls/HTTPRequest;Ljava/lang/String;)V

    .line 106
    :cond_3
    :goto_2
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    # setter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mTask:Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;
    invoke-static {v0, v10}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$502(Ltv/periscope/android/video/lhls/HTTPRequest;Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;)Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;

    goto :goto_1

    .line 92
    :cond_4
    if-gez v1, :cond_5

    .line 97
    :try_start_1
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    # invokes: Ltv/periscope/android/video/lhls/HTTPRequest;->signalEndOfStream()V
    invoke-static {v0}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$700(Ltv/periscope/android/video/lhls/HTTPRequest;)V

    .line 98
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    # getter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mConn:Lczb;
    invoke-static {v0}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$100(Ltv/periscope/android/video/lhls/HTTPRequest;)Lczb;

    move-result-object v0

    invoke-virtual {v0}, Lczb;->close()V

    .line 99
    iget-object v0, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    const/4 v1, 0x0

    # setter for: Ltv/periscope/android/video/lhls/HTTPRequest;->mConn:Lczb;
    invoke-static {v0, v1}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$102(Ltv/periscope/android/video/lhls/HTTPRequest;Lczb;)Lczb;

    goto :goto_2

    .line 95
    :cond_5
    iget-object v2, p0, Ltv/periscope/android/video/lhls/HTTPRequest$DownloadTask;->this$0:Ltv/periscope/android/video/lhls/HTTPRequest;

    # invokes: Ltv/periscope/android/video/lhls/HTTPRequest;->signalData([BI)V
    invoke-static {v2, v0, v1}, Ltv/periscope/android/video/lhls/HTTPRequest;->access$600(Ltv/periscope/android/video/lhls/HTTPRequest;[BI)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
