.class Ltv/periscope/android/video/b$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/video/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/video/b;

.field private final b:I

.field private c:[D

.field private d:I


# direct methods
.method public constructor <init>(Ltv/periscope/android/video/b;I)V
    .locals 1

    .prologue
    .line 123
    iput-object p1, p0, Ltv/periscope/android/video/b$a;->a:Ltv/periscope/android/video/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput p2, p0, Ltv/periscope/android/video/b$a;->b:I

    .line 125
    iget v0, p0, Ltv/periscope/android/video/b$a;->b:I

    new-array v0, v0, [D

    iput-object v0, p0, Ltv/periscope/android/video/b$a;->c:[D

    .line 126
    const/4 v0, 0x0

    iput v0, p0, Ltv/periscope/android/video/b$a;->d:I

    .line 127
    return-void
.end method


# virtual methods
.method public a()D
    .locals 6

    .prologue
    .line 136
    const-wide/16 v2, 0x0

    .line 137
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Ltv/periscope/android/video/b$a;->b:I

    if-ge v0, v1, :cond_1

    .line 139
    iget-object v1, p0, Ltv/periscope/android/video/b$a;->c:[D

    aget-wide v4, v1, v0

    cmpg-double v1, v4, v2

    if-gez v1, :cond_0

    .line 141
    iget-object v1, p0, Ltv/periscope/android/video/b$a;->c:[D

    aget-wide v2, v1, v0

    .line 137
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_1
    return-wide v2
.end method

.method public a(D)V
    .locals 3

    .prologue
    .line 130
    iget v0, p0, Ltv/periscope/android/video/b$a;->d:I

    iget v1, p0, Ltv/periscope/android/video/b$a;->b:I

    rem-int/2addr v0, v1

    .line 131
    iget-object v1, p0, Ltv/periscope/android/video/b$a;->c:[D

    aput-wide p1, v1, v0

    .line 132
    iget v0, p0, Ltv/periscope/android/video/b$a;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltv/periscope/android/video/b$a;->d:I

    .line 133
    return-void
.end method
