.class public Ltv/periscope/android/video/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/video/b$a;,
        Ltv/periscope/android/video/b$b;,
        Ltv/periscope/android/video/b$c;
    }
.end annotation


# instance fields
.field private A:I

.field private B:D

.field private C:D

.field private D:Ljava/util/Date;

.field private E:Z

.field private F:Z

.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:D

.field private final e:I

.field private final f:D

.field private final g:J

.field private final h:I

.field private final i:I

.field private final j:I

.field private k:Ltv/periscope/android/video/b$b;

.field private l:Ltv/periscope/android/video/b$b;

.field private m:Ltv/periscope/android/video/rtmp/i;

.field private n:Ltv/periscope/android/video/rtmp/i;

.field private o:Ltv/periscope/android/video/rtmp/i;

.field private p:Ltv/periscope/android/video/rtmp/i;

.field private q:Ltv/periscope/android/video/rtmp/i;

.field private r:I

.field private s:Ltv/periscope/android/video/rtmp/i;

.field private t:Ltv/periscope/android/video/b$a;

.field private u:I

.field private v:I

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4038000000000000L    # 24.0

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    const/4 v2, 0x0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const v0, 0xdc00

    iput v0, p0, Ltv/periscope/android/video/b;->a:I

    .line 18
    const v0, 0xa000

    iput v0, p0, Ltv/periscope/android/video/b;->b:I

    .line 19
    const/16 v0, 0x2800

    iput v0, p0, Ltv/periscope/android/video/b;->c:I

    .line 20
    const-wide/high16 v0, 0x4020000000000000L    # 8.0

    iput-wide v0, p0, Ltv/periscope/android/video/b;->d:D

    .line 21
    const/16 v0, 0x280

    iput v0, p0, Ltv/periscope/android/video/b;->e:I

    .line 22
    iput-wide v6, p0, Ltv/periscope/android/video/b;->f:D

    .line 23
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Ltv/periscope/android/video/b;->g:J

    .line 25
    const v0, 0xb400

    iput v0, p0, Ltv/periscope/android/video/b;->h:I

    .line 26
    const/high16 v0, 0x10000

    iput v0, p0, Ltv/periscope/android/video/b;->i:I

    .line 27
    const v0, 0x8000

    iput v0, p0, Ltv/periscope/android/video/b;->j:I

    .line 29
    new-instance v0, Ltv/periscope/android/video/b$b;

    invoke-direct {v0, p0, v4, v5}, Ltv/periscope/android/video/b$b;-><init>(Ltv/periscope/android/video/b;D)V

    iput-object v0, p0, Ltv/periscope/android/video/b;->k:Ltv/periscope/android/video/b$b;

    .line 30
    new-instance v0, Ltv/periscope/android/video/b$b;

    invoke-direct {v0, p0, v4, v5}, Ltv/periscope/android/video/b$b;-><init>(Ltv/periscope/android/video/b;D)V

    iput-object v0, p0, Ltv/periscope/android/video/b;->l:Ltv/periscope/android/video/b$b;

    .line 31
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/b;->m:Ltv/periscope/android/video/rtmp/i;

    .line 32
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/b;->n:Ltv/periscope/android/video/rtmp/i;

    .line 33
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/b;->o:Ltv/periscope/android/video/rtmp/i;

    .line 34
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/b;->p:Ltv/periscope/android/video/rtmp/i;

    .line 35
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/b;->q:Ltv/periscope/android/video/rtmp/i;

    .line 36
    iput v2, p0, Ltv/periscope/android/video/b;->r:I

    .line 37
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/b;->s:Ltv/periscope/android/video/rtmp/i;

    .line 38
    new-instance v0, Ltv/periscope/android/video/b$a;

    const/16 v1, 0xa

    invoke-direct {v0, p0, v1}, Ltv/periscope/android/video/b$a;-><init>(Ltv/periscope/android/video/b;I)V

    iput-object v0, p0, Ltv/periscope/android/video/b;->t:Ltv/periscope/android/video/b$a;

    .line 39
    iput v2, p0, Ltv/periscope/android/video/b;->u:I

    .line 40
    iput v2, p0, Ltv/periscope/android/video/b;->v:I

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/video/b;->w:Z

    .line 42
    iput-boolean v2, p0, Ltv/periscope/android/video/b;->x:Z

    .line 43
    iput-boolean v2, p0, Ltv/periscope/android/video/b;->y:Z

    .line 44
    iput v2, p0, Ltv/periscope/android/video/b;->z:I

    .line 45
    const/16 v0, 0x6400

    iput v0, p0, Ltv/periscope/android/video/b;->A:I

    .line 46
    iput-wide v6, p0, Ltv/periscope/android/video/b;->B:D

    .line 47
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/video/b;->C:D

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/video/b;->D:Ljava/util/Date;

    .line 49
    iput-boolean v2, p0, Ltv/periscope/android/video/b;->E:Z

    .line 50
    iput-boolean v2, p0, Ltv/periscope/android/video/b;->F:Z

    return-void
.end method

.method private j()V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v8, 0x0

    .line 219
    iget-boolean v0, p0, Ltv/periscope/android/video/b;->w:Z

    if-nez v0, :cond_4

    .line 221
    iget v0, p0, Ltv/periscope/android/video/b;->u:I

    .line 222
    iget-boolean v1, p0, Ltv/periscope/android/video/b;->x:Z

    if-eqz v1, :cond_3

    iget v1, p0, Ltv/periscope/android/video/b;->v:I

    if-lez v1, :cond_3

    iget v1, p0, Ltv/periscope/android/video/b;->v:I

    iget v2, p0, Ltv/periscope/android/video/b;->u:I

    if-ge v1, v2, :cond_3

    .line 224
    iget-boolean v1, p0, Ltv/periscope/android/video/b;->y:Z

    if-eqz v1, :cond_2

    .line 226
    iput-boolean v3, p0, Ltv/periscope/android/video/b;->w:Z

    .line 227
    iget v0, p0, Ltv/periscope/android/video/b;->v:I

    .line 247
    :cond_0
    :goto_0
    mul-int/lit8 v0, v0, 0x8

    div-int/lit8 v0, v0, 0xa

    iget-object v1, p0, Ltv/periscope/android/video/b;->l:Ltv/periscope/android/video/b$b;

    invoke-virtual {v1}, Ltv/periscope/android/video/b$b;->a()I

    move-result v1

    sub-int/2addr v0, v1

    .line 248
    mul-int/lit8 v0, v0, 0x41

    div-int/lit8 v0, v0, 0x64

    iput v0, p0, Ltv/periscope/android/video/b;->A:I

    .line 249
    invoke-direct {p0}, Ltv/periscope/android/video/b;->k()V

    .line 250
    invoke-direct {p0}, Ltv/periscope/android/video/b;->l()V

    .line 316
    :cond_1
    :goto_1
    return-void

    .line 231
    :cond_2
    iput-boolean v3, p0, Ltv/periscope/android/video/b;->y:Z

    .line 232
    iput v8, p0, Ltv/periscope/android/video/b;->z:I

    goto :goto_0

    .line 237
    :cond_3
    iget-boolean v1, p0, Ltv/periscope/android/video/b;->y:Z

    if-eqz v1, :cond_0

    .line 240
    iget v1, p0, Ltv/periscope/android/video/b;->z:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Ltv/periscope/android/video/b;->z:I

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    .line 242
    iput-boolean v8, p0, Ltv/periscope/android/video/b;->y:Z

    goto :goto_0

    .line 254
    :cond_4
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 255
    iget-object v0, p0, Ltv/periscope/android/video/b;->D:Ljava/util/Date;

    if-nez v0, :cond_5

    .line 257
    iput-object v2, p0, Ltv/periscope/android/video/b;->D:Ljava/util/Date;

    goto :goto_1

    .line 259
    :cond_5
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iget-object v3, p0, Ltv/periscope/android/video/b;->D:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v0, v4

    const-wide/16 v4, 0x2710

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 261
    iget v0, p0, Ltv/periscope/android/video/b;->v:I

    .line 262
    iget v1, p0, Ltv/periscope/android/video/b;->u:I

    if-lez v1, :cond_6

    iget v1, p0, Ltv/periscope/android/video/b;->u:I

    iget v3, p0, Ltv/periscope/android/video/b;->v:I

    if-ge v1, v3, :cond_6

    .line 265
    iget v0, p0, Ltv/periscope/android/video/b;->u:I

    .line 269
    :cond_6
    iget-object v1, p0, Ltv/periscope/android/video/b;->l:Ltv/periscope/android/video/b$b;

    invoke-virtual {v1}, Ltv/periscope/android/video/b$b;->a()I

    move-result v1

    sub-int/2addr v0, v1

    .line 272
    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0xa

    .line 275
    if-gtz v0, :cond_7

    .line 277
    iget v0, p0, Ltv/periscope/android/video/b;->A:I

    .line 283
    :cond_7
    iget v1, p0, Ltv/periscope/android/video/b;->A:I

    .line 284
    iget-object v3, p0, Ltv/periscope/android/video/b;->k:Ltv/periscope/android/video/b$b;

    invoke-virtual {v3}, Ltv/periscope/android/video/b$b;->a()I

    move-result v3

    .line 285
    mul-int/lit8 v4, v1, 0x7

    div-int/lit8 v4, v4, 0xa

    if-ge v3, v4, :cond_9

    iget-wide v4, p0, Ltv/periscope/android/video/b;->B:D

    const-wide/high16 v6, 0x4038000000000000L    # 24.0

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_9

    .line 287
    const-string/jumbo v0, "RTMP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Target/Actual kbits/s "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    mul-int/lit8 v1, v1, 0x8

    div-int/lit16 v1, v1, 0x400

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x8

    div-int/lit16 v3, v3, 0x400

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :cond_8
    :goto_2
    iput-object v2, p0, Ltv/periscope/android/video/b;->D:Ljava/util/Date;

    .line 313
    iput-boolean v8, p0, Ltv/periscope/android/video/b;->E:Z

    goto/16 :goto_1

    .line 291
    :cond_9
    iget-boolean v1, p0, Ltv/periscope/android/video/b;->E:Z

    if-nez v1, :cond_8

    iget v1, p0, Ltv/periscope/android/video/b;->A:I

    if-gt v0, v1, :cond_8

    .line 294
    add-int/lit16 v1, v0, 0x2800

    .line 297
    iget v0, p0, Ltv/periscope/android/video/b;->u:I

    if-lez v0, :cond_a

    .line 299
    iget v0, p0, Ltv/periscope/android/video/b;->u:I

    mul-int/lit8 v0, v0, 0x8

    div-int/lit8 v0, v0, 0xa

    iget-object v3, p0, Ltv/periscope/android/video/b;->l:Ltv/periscope/android/video/b$b;

    invoke-virtual {v3}, Ltv/periscope/android/video/b$b;->a()I

    move-result v3

    sub-int/2addr v0, v3

    .line 300
    mul-int/lit8 v0, v0, 0x41

    div-int/lit8 v0, v0, 0x64

    .line 301
    if-le v1, v0, :cond_a

    .line 307
    :goto_3
    iput v0, p0, Ltv/periscope/android/video/b;->A:I

    .line 308
    invoke-direct {p0}, Ltv/periscope/android/video/b;->k()V

    .line 309
    invoke-direct {p0}, Ltv/periscope/android/video/b;->l()V

    goto :goto_2

    :cond_a
    move v0, v1

    goto :goto_3
.end method

.method private k()V
    .locals 3

    .prologue
    const/16 v2, 0x2800

    .line 320
    iget-boolean v0, p0, Ltv/periscope/android/video/b;->w:Z

    if-eqz v0, :cond_2

    const v0, 0xa000

    .line 321
    :goto_0
    iget v1, p0, Ltv/periscope/android/video/b;->A:I

    if-le v1, v0, :cond_0

    .line 323
    iput v0, p0, Ltv/periscope/android/video/b;->A:I

    .line 325
    :cond_0
    iget v0, p0, Ltv/periscope/android/video/b;->A:I

    if-ge v0, v2, :cond_1

    .line 327
    iput v2, p0, Ltv/periscope/android/video/b;->A:I

    .line 329
    :cond_1
    return-void

    .line 320
    :cond_2
    const v0, 0xdc00

    goto :goto_0
.end method

.method private l()V
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4020000000000000L    # 8.0

    .line 333
    iget-boolean v0, p0, Ltv/periscope/android/video/b;->w:Z

    if-eqz v0, :cond_0

    iget v0, p0, Ltv/periscope/android/video/b;->v:I

    if-lez v0, :cond_1

    .line 335
    :cond_0
    iget v0, p0, Ltv/periscope/android/video/b;->A:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x40ce000000000000L    # 15360.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 337
    iget v0, p0, Ltv/periscope/android/video/b;->A:I

    div-int/lit16 v0, v0, 0x280

    int-to-double v0, v0

    iput-wide v0, p0, Ltv/periscope/android/video/b;->B:D

    .line 338
    iget-wide v0, p0, Ltv/periscope/android/video/b;->B:D

    cmpg-double v0, v0, v4

    if-gez v0, :cond_1

    .line 340
    iput-wide v4, p0, Ltv/periscope/android/video/b;->B:D

    .line 344
    :cond_1
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Ltv/periscope/android/video/b;->u:I

    return v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 150
    iput p1, p0, Ltv/periscope/android/video/b;->u:I

    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/video/b;->w:Z

    .line 152
    invoke-direct {p0}, Ltv/periscope/android/video/b;->j()V

    .line 153
    return-void
.end method

.method public a(ID)V
    .locals 4

    .prologue
    .line 398
    iget-object v0, p0, Ltv/periscope/android/video/b;->k:Ltv/periscope/android/video/b$b;

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double v2, p2, v2

    invoke-virtual {v0, v2, v3, p1}, Ltv/periscope/android/video/b$b;->a(DI)V

    .line 401
    iget-object v0, p0, Ltv/periscope/android/video/b;->k:Ltv/periscope/android/video/b$b;

    invoke-virtual {v0}, Ltv/periscope/android/video/b$b;->a()I

    move-result v0

    .line 402
    mul-int/lit8 v0, v0, 0x64

    iget v1, p0, Ltv/periscope/android/video/b;->A:I

    div-int/2addr v0, v1

    int-to-double v0, v0

    .line 403
    iget-object v2, p0, Ltv/periscope/android/video/b;->o:Ltv/periscope/android/video/rtmp/i;

    invoke-virtual {v2, v0, v1}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 405
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 162
    mul-int v0, p1, p2

    iput v0, p0, Ltv/periscope/android/video/b;->r:I

    .line 163
    return-void
.end method

.method public a(D)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const-wide/16 v6, 0x0

    .line 378
    iget-wide v2, p0, Ltv/periscope/android/video/b;->B:D

    cmpl-double v1, v2, v6

    if-eqz v1, :cond_0

    iget-wide v2, p0, Ltv/periscope/android/video/b;->B:D

    const-wide/high16 v4, 0x4038000000000000L    # 24.0

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_1

    .line 392
    :cond_0
    :goto_0
    return v0

    .line 382
    :cond_1
    iget-wide v2, p0, Ltv/periscope/android/video/b;->C:D

    cmpl-double v1, v2, v6

    if-lez v1, :cond_2

    .line 384
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p0, Ltv/periscope/android/video/b;->B:D

    div-double/2addr v2, v4

    .line 385
    iget-wide v4, p0, Ltv/periscope/android/video/b;->C:D

    sub-double v4, p1, v4

    .line 386
    cmpl-double v1, v2, v6

    if-lez v1, :cond_2

    cmpl-double v1, v4, v6

    if-lez v1, :cond_2

    cmpg-double v1, v4, v2

    if-gez v1, :cond_2

    .line 388
    const/4 v0, 0x0

    goto :goto_0

    .line 391
    :cond_2
    iput-wide p1, p0, Ltv/periscope/android/video/b;->C:D

    goto :goto_0
.end method

.method public b(ID)V
    .locals 4

    .prologue
    .line 409
    iget-object v0, p0, Ltv/periscope/android/video/b;->l:Ltv/periscope/android/video/b$b;

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double v2, p2, v2

    invoke-virtual {v0, v2, v3, p1}, Ltv/periscope/android/video/b$b;->a(DI)V

    .line 410
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p0, Ltv/periscope/android/video/b;->F:Z

    return v0
.end method

.method public b(II)Z
    .locals 12

    .prologue
    .line 167
    iget v0, p0, Ltv/periscope/android/video/b;->A:I

    .line 168
    iget-wide v2, p0, Ltv/periscope/android/video/b;->B:D

    .line 170
    iget-object v1, p0, Ltv/periscope/android/video/b;->t:Ltv/periscope/android/video/b$a;

    int-to-double v4, p1

    invoke-virtual {v1, v4, v5}, Ltv/periscope/android/video/b$a;->a(D)V

    .line 173
    int-to-double v4, p1

    const-wide/high16 v6, 0x4020000000000000L    # 8.0

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    div-double/2addr v4, v6

    .line 174
    iget-object v1, p0, Ltv/periscope/android/video/b;->m:Ltv/periscope/android/video/rtmp/i;

    invoke-virtual {v1, v4, v5}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 175
    invoke-virtual {p0}, Ltv/periscope/android/video/b;->e()D

    move-result-wide v4

    .line 176
    iget-object v1, p0, Ltv/periscope/android/video/b;->n:Ltv/periscope/android/video/rtmp/i;

    invoke-virtual {v1, v4, v5}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 179
    iget-object v1, p0, Ltv/periscope/android/video/b;->k:Ltv/periscope/android/video/b$b;

    invoke-virtual {v1}, Ltv/periscope/android/video/b$b;->a()I

    move-result v1

    .line 180
    iget-object v6, p0, Ltv/periscope/android/video/b;->p:Ltv/periscope/android/video/rtmp/i;

    int-to-double v8, v1

    const-wide/high16 v10, 0x4020000000000000L    # 8.0

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4090000000000000L    # 1024.0

    div-double/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 183
    iget-object v6, p0, Ltv/periscope/android/video/b;->l:Ltv/periscope/android/video/b$b;

    invoke-virtual {v6}, Ltv/periscope/android/video/b$b;->a()I

    move-result v6

    .line 184
    iget-object v7, p0, Ltv/periscope/android/video/b;->q:Ltv/periscope/android/video/rtmp/i;

    int-to-double v8, v6

    const-wide/high16 v10, 0x4020000000000000L    # 8.0

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4090000000000000L    # 1024.0

    div-double/2addr v8, v10

    invoke-virtual {v7, v8, v9}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 186
    iget v6, p0, Ltv/periscope/android/video/b;->r:I

    if-lez v6, :cond_0

    .line 188
    mul-int/lit8 v1, v1, 0x8

    int-to-double v6, v1

    iget v1, p0, Ltv/periscope/android/video/b;->r:I

    int-to-double v8, v1

    mul-double/2addr v4, v8

    div-double v4, v6, v4

    .line 189
    iget-object v1, p0, Ltv/periscope/android/video/b;->s:Ltv/periscope/android/video/rtmp/i;

    invoke-virtual {v1, v4, v5}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 192
    :cond_0
    iget-object v1, p0, Ltv/periscope/android/video/b;->t:Ltv/periscope/android/video/b$a;

    invoke-virtual {v1}, Ltv/periscope/android/video/b$a;->a()D

    move-result-wide v4

    double-to-int v1, v4

    iput v1, p0, Ltv/periscope/android/video/b;->v:I

    .line 193
    const/4 v1, 0x1

    iput-boolean v1, p0, Ltv/periscope/android/video/b;->x:Z

    .line 194
    if-lez p2, :cond_1

    .line 196
    const/4 v1, 0x0

    iput-boolean v1, p0, Ltv/periscope/android/video/b;->x:Z

    .line 197
    const/4 v1, 0x1

    iput-boolean v1, p0, Ltv/periscope/android/video/b;->E:Z

    .line 198
    const/4 v1, 0x1

    iput-boolean v1, p0, Ltv/periscope/android/video/b;->F:Z

    .line 200
    :cond_1
    invoke-direct {p0}, Ltv/periscope/android/video/b;->j()V

    .line 204
    iget v1, p0, Ltv/periscope/android/video/b;->A:I

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 205
    const/16 v1, 0x400

    if-gt v0, v1, :cond_2

    iget-wide v0, p0, Ltv/periscope/android/video/b;->B:D

    cmpl-double v0, v2, v0

    if-eqz v0, :cond_3

    .line 207
    :cond_2
    const/4 v0, 0x1

    .line 209
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 348
    iget v0, p0, Ltv/periscope/android/video/b;->A:I

    return v0
.end method

.method public d()D
    .locals 2

    .prologue
    .line 353
    iget-wide v0, p0, Ltv/periscope/android/video/b;->B:D

    return-wide v0
.end method

.method public e()D
    .locals 4

    .prologue
    .line 358
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iget-object v2, p0, Ltv/periscope/android/video/b;->k:Ltv/periscope/android/video/b$b;

    invoke-virtual {v2}, Ltv/periscope/android/video/b$b;->b()D

    move-result-wide v2

    div-double/2addr v0, v2

    .line 359
    return-wide v0
.end method

.method public f()D
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, Ltv/periscope/android/video/b;->l:Ltv/periscope/android/video/b$b;

    invoke-virtual {v0}, Ltv/periscope/android/video/b$b;->a()I

    move-result v0

    mul-int/lit8 v0, v0, 0x8

    div-int/lit16 v0, v0, 0x400

    int-to-double v0, v0

    return-wide v0
.end method

.method public g()I
    .locals 2

    .prologue
    .line 369
    iget v0, p0, Ltv/periscope/android/video/b;->A:I

    const v1, 0xb400

    if-le v0, v1, :cond_0

    .line 371
    const/high16 v0, 0x10000

    .line 373
    :goto_0
    return v0

    :cond_0
    const v0, 0x8000

    goto :goto_0
.end method

.method public h()V
    .locals 3

    .prologue
    .line 414
    const-string/jumbo v0, "RTMP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Upload rate (kbits/s): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/video/b;->m:Ltv/periscope/android/video/rtmp/i;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const-string/jumbo v0, "RTMP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "FPS: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/video/b;->n:Ltv/periscope/android/video/rtmp/i;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const-string/jumbo v0, "RTMP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Encoder ratio: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/video/b;->o:Ltv/periscope/android/video/rtmp/i;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    return-void
.end method

.method public i()Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 421
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 423
    iget-object v1, p0, Ltv/periscope/android/video/b;->m:Ltv/periscope/android/video/rtmp/i;

    const-string/jumbo v2, "UploadRate"

    invoke-virtual {v1, v0, v2}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 424
    iget-object v1, p0, Ltv/periscope/android/video/b;->o:Ltv/periscope/android/video/rtmp/i;

    const-string/jumbo v2, "VideoIndexRatio"

    invoke-virtual {v1, v0, v2}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 425
    iget-object v1, p0, Ltv/periscope/android/video/b;->n:Ltv/periscope/android/video/rtmp/i;

    const-string/jumbo v2, "FrameRate"

    invoke-virtual {v1, v0, v2}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 426
    iget-object v1, p0, Ltv/periscope/android/video/b;->p:Ltv/periscope/android/video/rtmp/i;

    const-string/jumbo v2, "VideoOutput"

    invoke-virtual {v1, v0, v2}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 427
    iget-object v1, p0, Ltv/periscope/android/video/b;->s:Ltv/periscope/android/video/rtmp/i;

    const-string/jumbo v2, "BitsPerPixel"

    invoke-virtual {v1, v0, v2}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 428
    iget-object v1, p0, Ltv/periscope/android/video/b;->q:Ltv/periscope/android/video/rtmp/i;

    const-string/jumbo v2, "AudioOutputRate"

    invoke-virtual {v1, v0, v2}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 430
    return-object v0
.end method
