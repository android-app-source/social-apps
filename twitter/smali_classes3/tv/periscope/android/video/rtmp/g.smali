.class public Ltv/periscope/android/video/rtmp/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/video/rtmp/g$a;,
        Ltv/periscope/android/video/rtmp/g$b;
    }
.end annotation


# instance fields
.field private a:Ltv/periscope/android/video/rtmp/g$a;

.field private b:Ljava/lang/Thread;

.field private c:Ljava/net/Socket;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Ljava/io/InputStream;

.field private g:Ltv/periscope/android/video/rtmp/g$b;

.field private h:Z

.field private i:Ltv/periscope/android/video/rtmp/e;

.field private j:Ltv/periscope/android/video/rtmp/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/video/rtmp/g;->h:Z

    .line 34
    new-instance v0, Ltv/periscope/android/video/rtmp/e;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/e;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g;->i:Ltv/periscope/android/video/rtmp/e;

    .line 35
    new-instance v0, Ltv/periscope/android/video/rtmp/e;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/e;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g;->j:Ltv/periscope/android/video/rtmp/e;

    return-void
.end method

.method static synthetic a(Ltv/periscope/android/video/rtmp/g;)Ltv/periscope/android/video/rtmp/g$a;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->a:Ltv/periscope/android/video/rtmp/g$a;

    return-object v0
.end method

.method static synthetic b(Ltv/periscope/android/video/rtmp/g;)Ltv/periscope/android/video/rtmp/e;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->j:Ltv/periscope/android/video/rtmp/e;

    return-object v0
.end method

.method static synthetic c(Ltv/periscope/android/video/rtmp/g;)Ljava/net/Socket;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->c:Ljava/net/Socket;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 207
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/g$b;->a()V

    .line 211
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 212
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    .line 213
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/video/rtmp/g;->b:Ljava/lang/Thread;

    if-eq v0, v1, :cond_1

    .line 214
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :cond_1
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->i:Ltv/periscope/android/video/rtmp/e;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/e;->a()V

    .line 219
    return-void

    .line 216
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;IZLtv/periscope/android/video/rtmp/g$a;)V
    .locals 2

    .prologue
    .line 195
    iput-object p4, p0, Ltv/periscope/android/video/rtmp/g;->a:Ltv/periscope/android/video/rtmp/g$a;

    .line 196
    iput-object p1, p0, Ltv/periscope/android/video/rtmp/g;->d:Ljava/lang/String;

    .line 197
    iput p2, p0, Ltv/periscope/android/video/rtmp/g;->e:I

    .line 198
    iput-boolean p3, p0, Ltv/periscope/android/video/rtmp/g;->h:Z

    .line 200
    new-instance v0, Ljava/lang/Thread;

    const-string/jumbo v1, "NetStream"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g;->b:Ljava/lang/Thread;

    .line 201
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 202
    return-void
.end method

.method public a(Ltv/periscope/android/video/rtmp/f;)V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->i:Ltv/periscope/android/video/rtmp/e;

    invoke-virtual {v0, p1}, Ltv/periscope/android/video/rtmp/e;->a(Ltv/periscope/android/video/rtmp/f;)V

    .line 224
    return-void
.end method

.method public a([BII)V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    invoke-virtual {v0, p1, p2, p3}, Ltv/periscope/android/video/rtmp/g$b;->a([BII)V

    .line 334
    :cond_0
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/g$b;->c()J

    move-result-wide v0

    .line 342
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/g$b;->d()J

    move-result-wide v0

    .line 350
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/util/Date;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/g$b;->e()Ljava/util/Date;

    move-result-object v0

    .line 358
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/g$b;->b()V

    .line 366
    :cond_0
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    const v0, 0x8000

    const/4 v1, 0x0

    .line 227
    .line 228
    new-array v3, v0, [B

    .line 231
    :try_start_0
    iget-boolean v0, p0, Ltv/periscope/android/video/rtmp/g;->h:Z

    if-eqz v0, :cond_1

    .line 233
    invoke-static {}, Ljavax/net/ssl/SSLSocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    .line 242
    iget-object v2, p0, Ltv/periscope/android/video/rtmp/g;->d:Ljava/lang/String;

    iget v4, p0, Ltv/periscope/android/video/rtmp/g;->e:I

    invoke-virtual {v0, v2, v4}, Ljavax/net/SocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    .line 243
    new-instance v2, Ljava/util/concurrent/Semaphore;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 244
    new-instance v4, Ltv/periscope/android/video/rtmp/g$1;

    invoke-direct {v4, p0, v2}, Ltv/periscope/android/video/rtmp/g$1;-><init>(Ltv/periscope/android/video/rtmp/g;Ljava/util/concurrent/Semaphore;)V

    invoke-virtual {v0, v4}, Ljavax/net/ssl/SSLSocket;->addHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    .line 252
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    .line 253
    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 254
    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g;->c:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    :goto_0
    new-instance v0, Ltv/periscope/android/video/rtmp/g$b;

    invoke-direct {v0, p0}, Ltv/periscope/android/video/rtmp/g$b;-><init>(Ltv/periscope/android/video/rtmp/g;)V

    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g;->g:Ltv/periscope/android/video/rtmp/g$b;

    .line 267
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->a:Ltv/periscope/android/video/rtmp/g$a;

    invoke-interface {v0, p0}, Ltv/periscope/android/video/rtmp/g$a;->a(Ltv/periscope/android/video/rtmp/g;)V

    .line 276
    :try_start_1
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->c:Ljava/net/Socket;

    const/16 v2, 0x32

    invoke-virtual {v0, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 277
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g;->f:Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 283
    :cond_0
    :goto_1
    :try_start_2
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->f:Ljava/io/InputStream;

    const/4 v2, 0x0

    const v4, 0x8000

    invoke-virtual {v0, v3, v2, v4}, Ljava/io/InputStream;->read([BII)I
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v0

    move v2, v1

    .line 291
    :goto_2
    if-gtz v0, :cond_2

    .line 292
    if-nez v2, :cond_0

    .line 296
    :try_start_3
    const-string/jumbo v0, "NetStream"

    const-string/jumbo v1, "Socket close assumed"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->a:Ltv/periscope/android/video/rtmp/g$a;

    invoke-interface {v0, p0}, Ltv/periscope/android/video/rtmp/g$a;->b(Ltv/periscope/android/video/rtmp/g;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 326
    :goto_3
    return-void

    .line 258
    :cond_1
    :try_start_4
    new-instance v0, Ljava/net/Socket;

    iget-object v2, p0, Ltv/periscope/android/video/rtmp/g;->d:Ljava/lang/String;

    iget v4, p0, Ltv/periscope/android/video/rtmp/g;->e:I

    invoke-direct {v0, v2, v4}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g;->c:Ljava/net/Socket;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 260
    :catch_0
    move-exception v0

    .line 261
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 262
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->a:Ltv/periscope/android/video/rtmp/g$a;

    invoke-interface {v0, p0}, Ltv/periscope/android/video/rtmp/g$a;->b(Ltv/periscope/android/video/rtmp/g;)V

    goto :goto_3

    .line 285
    :catch_1
    move-exception v0

    .line 287
    const/4 v0, 0x1

    .line 289
    :try_start_5
    iget-object v2, p0, Ltv/periscope/android/video/rtmp/g;->c:Ljava/net/Socket;

    const/16 v4, 0xc8

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    move v2, v0

    move v0, v1

    goto :goto_2

    .line 301
    :cond_2
    iget-object v2, p0, Ltv/periscope/android/video/rtmp/g;->c:Ljava/net/Socket;

    const/16 v4, 0x32

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 304
    :goto_4
    iget-object v2, p0, Ltv/periscope/android/video/rtmp/g;->i:Ltv/periscope/android/video/rtmp/e;

    invoke-virtual {v2, v0}, Ltv/periscope/android/video/rtmp/e;->b(I)Ltv/periscope/android/video/rtmp/f;

    move-result-object v2

    .line 305
    if-eqz v2, :cond_3

    .line 307
    invoke-virtual {v2, v3, v0}, Ltv/periscope/android/video/rtmp/f;->a([BI)V

    .line 308
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->a:Ltv/periscope/android/video/rtmp/g$a;

    invoke-interface {v0, p0, v2}, Ltv/periscope/android/video/rtmp/g$a;->a(Ltv/periscope/android/video/rtmp/g;Ltv/periscope/android/video/rtmp/f;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 321
    :catch_2
    move-exception v0

    .line 322
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 325
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g;->a:Ltv/periscope/android/video/rtmp/g$a;

    invoke-interface {v0, p0}, Ltv/periscope/android/video/rtmp/g$a;->b(Ltv/periscope/android/video/rtmp/g;)V

    goto :goto_3

    .line 313
    :cond_3
    const-wide/16 v4, 0xa

    :try_start_6
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_4

    .line 314
    :catch_3
    move-exception v0

    goto :goto_1
.end method
