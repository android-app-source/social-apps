.class Ltv/periscope/android/video/rtmp/g$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/video/rtmp/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/video/rtmp/g;

.field private b:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ltv/periscope/android/video/rtmp/f;",
            ">;"
        }
    .end annotation
.end field

.field private c:J

.field private d:J

.field private e:Ljava/util/Date;

.field private f:Ljava/lang/Thread;

.field private g:Z

.field private h:J

.field private i:J

.field private j:Ljava/util/Date;

.field private k:Z


# direct methods
.method public constructor <init>(Ltv/periscope/android/video/rtmp/g;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 52
    iput-object p1, p0, Ltv/periscope/android/video/rtmp/g$b;->a:Ltv/periscope/android/video/rtmp/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 46
    iput-wide v2, p0, Ltv/periscope/android/video/rtmp/g$b;->h:J

    .line 47
    iput-wide v2, p0, Ltv/periscope/android/video/rtmp/g$b;->i:J

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->j:Ljava/util/Date;

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/video/rtmp/g$b;->k:Z

    .line 53
    new-instance v0, Ljava/lang/Thread;

    const-string/jumbo v1, "NetStream SendQueue"

    invoke-static {v1}, Ltv/periscope/android/util/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->f:Ljava/lang/Thread;

    .line 54
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 55
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/video/rtmp/g$b;->g:Z

    .line 60
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 62
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public a([BII)V
    .locals 4

    .prologue
    .line 83
    monitor-enter p0

    .line 85
    :try_start_0
    iget-boolean v0, p0, Ltv/periscope/android/video/rtmp/g$b;->k:Z

    if-eqz v0, :cond_0

    .line 88
    monitor-exit p0

    .line 112
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->e:Ljava/util/Date;

    if-nez v0, :cond_1

    .line 92
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->e:Ljava/util/Date;

    .line 93
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/video/rtmp/g$b;->d:J

    .line 99
    :cond_1
    :goto_1
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->a:Ltv/periscope/android/video/rtmp/g;

    invoke-static {v0}, Ltv/periscope/android/video/rtmp/g;->b(Ltv/periscope/android/video/rtmp/g;)Ltv/periscope/android/video/rtmp/e;

    move-result-object v0

    invoke-virtual {v0, p3}, Ltv/periscope/android/video/rtmp/e;->b(I)Ltv/periscope/android/video/rtmp/f;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_2

    .line 102
    invoke-virtual {v0, p1, p2, p3}, Ltv/periscope/android/video/rtmp/f;->a([BII)V

    .line 103
    iget-object v1, p0, Ltv/periscope/android/video/rtmp/g$b;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 104
    iget-wide v0, p0, Ltv/periscope/android/video/rtmp/g$b;->c:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Ltv/periscope/android/video/rtmp/g$b;->c:J

    .line 105
    monitor-exit p0

    goto :goto_0

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 108
    :cond_2
    const-wide/16 v0, 0xa

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 110
    :catch_0
    move-exception v0

    .line 112
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 70
    monitor-enter p0

    .line 72
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Ltv/periscope/android/video/rtmp/g$b;->k:Z

    .line 73
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->a:Ltv/periscope/android/video/rtmp/g;

    invoke-static {v0}, Ltv/periscope/android/video/rtmp/g;->a(Ltv/periscope/android/video/rtmp/g;)Ltv/periscope/android/video/rtmp/g$a;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/video/rtmp/g$a;->b()V

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/video/rtmp/g$b;->k:Z

    .line 78
    :cond_0
    monitor-exit p0

    .line 79
    return-void

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 166
    monitor-enter p0

    .line 168
    :try_start_0
    iget-wide v0, p0, Ltv/periscope/android/video/rtmp/g$b;->c:J

    iput-wide v0, p0, Ltv/periscope/android/video/rtmp/g$b;->h:J

    .line 169
    iget-wide v0, p0, Ltv/periscope/android/video/rtmp/g$b;->d:J

    iput-wide v0, p0, Ltv/periscope/android/video/rtmp/g$b;->i:J

    .line 170
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->e:Ljava/util/Date;

    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->j:Ljava/util/Date;

    .line 171
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->e:Ljava/util/Date;

    .line 172
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/video/rtmp/g$b;->d:J

    .line 174
    iget-wide v0, p0, Ltv/periscope/android/video/rtmp/g$b;->i:J

    monitor-exit p0

    return-wide v0

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 179
    iget-wide v0, p0, Ltv/periscope/android/video/rtmp/g$b;->h:J

    return-wide v0
.end method

.method public e()Ljava/util/Date;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->j:Ljava/util/Date;

    return-object v0
.end method

.method public run()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 119
    .line 121
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->a:Ltv/periscope/android/video/rtmp/g;

    invoke-static {v0}, Ltv/periscope/android/video/rtmp/g;->c(Ltv/periscope/android/video/rtmp/g;)Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 128
    :goto_0
    :try_start_1
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/video/rtmp/f;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 133
    :goto_1
    :try_start_2
    iget-boolean v3, p0, Ltv/periscope/android/video/rtmp/g$b;->g:Z

    if-eqz v3, :cond_0

    .line 162
    :goto_2
    return-void

    .line 129
    :catch_0
    move-exception v0

    .line 131
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    move-object v0, v1

    goto :goto_1

    .line 137
    :cond_0
    if-eqz v0, :cond_1

    .line 139
    monitor-enter p0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 141
    :try_start_3
    iget-wide v4, p0, Ltv/periscope/android/video/rtmp/g$b;->c:J

    iget v3, v0, Ltv/periscope/android/video/rtmp/f;->b:I

    int-to-long v6, v3

    sub-long/2addr v4, v6

    iput-wide v4, p0, Ltv/periscope/android/video/rtmp/g$b;->c:J

    .line 142
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 145
    :cond_1
    :try_start_4
    iget-object v3, v0, Ltv/periscope/android/video/rtmp/f;->a:[B

    const/4 v4, 0x0

    iget v5, v0, Ltv/periscope/android/video/rtmp/f;->b:I

    invoke-virtual {v2, v3, v4, v5}, Ljava/io/OutputStream;->write([BII)V

    .line 146
    iget-wide v4, p0, Ltv/periscope/android/video/rtmp/g$b;->d:J

    iget v3, v0, Ltv/periscope/android/video/rtmp/f;->b:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    iput-wide v4, p0, Ltv/periscope/android/video/rtmp/g$b;->d:J

    .line 147
    iget-object v3, p0, Ltv/periscope/android/video/rtmp/g$b;->a:Ltv/periscope/android/video/rtmp/g;

    invoke-static {v3}, Ltv/periscope/android/video/rtmp/g;->b(Ltv/periscope/android/video/rtmp/g;)Ltv/periscope/android/video/rtmp/e;

    move-result-object v3

    invoke-virtual {v3, v0}, Ltv/periscope/android/video/rtmp/e;->a(Ltv/periscope/android/video/rtmp/f;)V

    .line 149
    monitor-enter p0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 151
    :try_start_5
    iget-boolean v0, p0, Ltv/periscope/android/video/rtmp/g$b;->k:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->a:Ltv/periscope/android/video/rtmp/g;

    invoke-static {v0}, Ltv/periscope/android/video/rtmp/g;->a(Ltv/periscope/android/video/rtmp/g;)Ltv/periscope/android/video/rtmp/g$a;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/video/rtmp/g$a;->b()V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/video/rtmp/g$b;->k:Z

    .line 156
    :cond_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 158
    :catch_1
    move-exception v0

    .line 159
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 160
    iget-object v0, p0, Ltv/periscope/android/video/rtmp/g$b;->a:Ltv/periscope/android/video/rtmp/g;

    invoke-static {v0}, Ltv/periscope/android/video/rtmp/g;->a(Ltv/periscope/android/video/rtmp/g;)Ltv/periscope/android/video/rtmp/g$a;

    move-result-object v0

    invoke-interface {v0, v1}, Ltv/periscope/android/video/rtmp/g$a;->b(Ltv/periscope/android/video/rtmp/g;)V

    goto :goto_2

    .line 142
    :catchall_1
    move-exception v0

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
.end method
