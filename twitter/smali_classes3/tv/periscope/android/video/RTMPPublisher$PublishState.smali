.class public final enum Ltv/periscope/android/video/RTMPPublisher$PublishState;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/video/RTMPPublisher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PublishState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/android/video/RTMPPublisher$PublishState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltv/periscope/android/video/RTMPPublisher$PublishState;

.field public static final enum b:Ltv/periscope/android/video/RTMPPublisher$PublishState;

.field public static final enum c:Ltv/periscope/android/video/RTMPPublisher$PublishState;

.field public static final enum d:Ltv/periscope/android/video/RTMPPublisher$PublishState;

.field private static final synthetic e:[Ltv/periscope/android/video/RTMPPublisher$PublishState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    new-instance v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;

    const-string/jumbo v1, "PS_Connecting"

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/video/RTMPPublisher$PublishState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;->a:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    .line 94
    new-instance v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;

    const-string/jumbo v1, "PS_Publishing"

    invoke-direct {v0, v1, v3}, Ltv/periscope/android/video/RTMPPublisher$PublishState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;->b:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    .line 95
    new-instance v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;

    const-string/jumbo v1, "PS_Reconnecting"

    invoke-direct {v0, v1, v4}, Ltv/periscope/android/video/RTMPPublisher$PublishState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;->c:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    .line 96
    new-instance v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;

    const-string/jumbo v1, "PS_Ended"

    invoke-direct {v0, v1, v5}, Ltv/periscope/android/video/RTMPPublisher$PublishState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;->d:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    .line 91
    const/4 v0, 0x4

    new-array v0, v0, [Ltv/periscope/android/video/RTMPPublisher$PublishState;

    sget-object v1, Ltv/periscope/android/video/RTMPPublisher$PublishState;->a:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    aput-object v1, v0, v2

    sget-object v1, Ltv/periscope/android/video/RTMPPublisher$PublishState;->b:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    aput-object v1, v0, v3

    sget-object v1, Ltv/periscope/android/video/RTMPPublisher$PublishState;->c:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    aput-object v1, v0, v4

    sget-object v1, Ltv/periscope/android/video/RTMPPublisher$PublishState;->d:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    aput-object v1, v0, v5

    sput-object v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;->e:[Ltv/periscope/android/video/RTMPPublisher$PublishState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/android/video/RTMPPublisher$PublishState;
    .locals 1

    .prologue
    .line 91
    const-class v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;

    return-object v0
.end method

.method public static values()[Ltv/periscope/android/video/RTMPPublisher$PublishState;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;->e:[Ltv/periscope/android/video/RTMPPublisher$PublishState;

    invoke-virtual {v0}, [Ltv/periscope/android/video/RTMPPublisher$PublishState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/android/video/RTMPPublisher$PublishState;

    return-object v0
.end method
