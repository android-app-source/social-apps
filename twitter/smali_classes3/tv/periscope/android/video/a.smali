.class public Ltv/periscope/android/video/a;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static final a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Ltv/periscope/android/video/a;->a:[I

    return-void

    :array_0
    .array-data 4
        0x17700
        0x15888
        0xfa00
        0xbb80
        0xac44
        0x7d00
        0x5dc0
        0x5622
        0x3e80
        0x2ee0
        0x2b11
        0x1f40
        0x1cb6
    .end array-data
.end method

.method public static a(II)[B
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 28
    .line 29
    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 31
    const/4 p1, 0x7

    :cond_0
    move v0, v1

    .line 36
    :goto_0
    sget-object v2, Ltv/periscope/android/video/a;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 38
    sget-object v2, Ltv/periscope/android/video/a;->a:[I

    aget v2, v2, v0

    if-ne v2, p0, :cond_1

    .line 45
    :goto_1
    const/4 v2, 0x2

    new-array v2, v2, [B

    .line 46
    shr-int/lit8 v3, v0, 0x1

    and-int/lit8 v3, v3, 0x7

    or-int/lit8 v3, v3, 0x10

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 47
    const/4 v1, 0x1

    and-int/lit8 v0, v0, 0x1

    shl-int/lit8 v0, v0, 0x7

    shl-int/lit8 v3, p1, 0x3

    or-int/2addr v0, v3

    int-to-byte v0, v0

    aput-byte v0, v2, v1

    .line 49
    return-object v2

    .line 36
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
