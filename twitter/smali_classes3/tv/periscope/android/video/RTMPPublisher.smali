.class public Ltv/periscope/android/video/RTMPPublisher;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/video/rtmp/Connection$a;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/video/RTMPPublisher$a;,
        Ltv/periscope/android/video/RTMPPublisher$PublishState;
    }
.end annotation


# instance fields
.field private A:Ljava/lang/String;

.field private B:I

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:J

.field private G:J

.field private H:I

.field private I:D

.field private J:Ltv/periscope/android/video/RTMPPublisher$PublishState;

.field private K:Z

.field private a:Ltv/periscope/android/video/rtmp/Connection;

.field private b:[B

.field private c:[B

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Landroid/media/MediaFormat;

.field private j:Landroid/media/MediaFormat;

.field private k:Z

.field private l:Ltv/periscope/android/video/RTMPPublisher$a;

.field private m:J

.field private n:J

.field private o:D

.field private p:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private r:Z

.field private s:Z

.field private t:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private u:J

.field private v:J

.field private final w:Ltv/periscope/android/video/c;

.field private x:Ljava/util/Timer;

.field private y:J

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    .line 42
    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    .line 44
    iput-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->d:Z

    .line 45
    iput-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->e:Z

    .line 46
    iput-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->f:Z

    .line 47
    iput-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->g:Z

    .line 52
    iput-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->k:Z

    .line 58
    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->p:Ljava/util/HashMap;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->q:Ljava/util/HashMap;

    .line 60
    iput-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->r:Z

    .line 61
    iput-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->s:Z

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->t:Ljava/util/HashMap;

    .line 66
    iput-wide v4, p0, Ltv/periscope/android/video/RTMPPublisher;->v:J

    .line 68
    new-instance v0, Ltv/periscope/android/video/c;

    invoke-direct {v0}, Ltv/periscope/android/video/c;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->w:Ltv/periscope/android/video/c;

    .line 74
    iput-wide v4, p0, Ltv/periscope/android/video/RTMPPublisher;->y:J

    .line 85
    iput-wide v4, p0, Ltv/periscope/android/video/RTMPPublisher;->F:J

    .line 86
    iput-wide v4, p0, Ltv/periscope/android/video/RTMPPublisher;->G:J

    .line 87
    iput v2, p0, Ltv/periscope/android/video/RTMPPublisher;->H:I

    .line 89
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->I:D

    .line 99
    sget-object v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;->a:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->J:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    .line 101
    iput-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->K:Z

    .line 110
    iput-object p1, p0, Ltv/periscope/android/video/RTMPPublisher;->z:Ljava/lang/String;

    .line 111
    iput-object p2, p0, Ltv/periscope/android/video/RTMPPublisher;->A:Ljava/lang/String;

    .line 112
    iput p3, p0, Ltv/periscope/android/video/RTMPPublisher;->B:I

    .line 113
    iput-object p4, p0, Ltv/periscope/android/video/RTMPPublisher;->C:Ljava/lang/String;

    .line 114
    iput-object p5, p0, Ltv/periscope/android/video/RTMPPublisher;->D:Ljava/lang/String;

    .line 115
    iput-object p6, p0, Ltv/periscope/android/video/RTMPPublisher;->E:Ljava/lang/String;

    .line 117
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->l()V

    .line 118
    return-void
.end method

.method private a(Ltv/periscope/android/video/RTMPPublisher$PublishState;)V
    .locals 1

    .prologue
    .line 936
    monitor-enter p0

    .line 938
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->J:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    if-ne p1, v0, :cond_1

    .line 940
    monitor-exit p0

    .line 948
    :cond_0
    :goto_0
    return-void

    .line 942
    :cond_1
    iput-object p1, p0, Ltv/periscope/android/video/RTMPPublisher;->J:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    .line 943
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 944
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->l:Ltv/periscope/android/video/RTMPPublisher$a;

    if-eqz v0, :cond_0

    .line 946
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->l:Ltv/periscope/android/video/RTMPPublisher$a;

    invoke-interface {v0, p1}, Ltv/periscope/android/video/RTMPPublisher$a;->a(Ltv/periscope/android/video/RTMPPublisher$PublishState;)V

    goto :goto_0

    .line 943
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Ltv/periscope/android/video/RTMPPublisher;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->w()V

    return-void
.end method

.method private a(JZ)[B
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 806
    invoke-direct {p0, p1, p2, p3}, Ltv/periscope/android/video/RTMPPublisher;->b(JZ)[B

    move-result-object v3

    .line 815
    array-length v0, v3

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x17

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 817
    array-length v0, v3

    add-int/lit8 v0, v0, 0x10

    .line 818
    const/4 v2, 0x6

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 819
    const/4 v2, 0x5

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 822
    :goto_0
    const/16 v2, 0xff

    if-le v0, v2, :cond_0

    .line 824
    const/4 v2, -0x1

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 825
    add-int/lit16 v0, v0, -0xff

    goto :goto_0

    .line 827
    :cond_0
    int-to-byte v0, v0

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 830
    sget-object v0, Lczm;->a:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->getMostSignificantBits()J

    move-result-wide v6

    .line 831
    invoke-virtual {v4, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 832
    sget-object v0, Lczm;->a:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v6

    .line 833
    invoke-virtual {v4, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move v0, v1

    move v2, v1

    .line 837
    :goto_1
    array-length v5, v3

    if-ge v0, v5, :cond_3

    .line 839
    aget-byte v5, v3, v0

    .line 840
    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 841
    if-nez v5, :cond_2

    .line 843
    if-eqz v2, :cond_1

    .line 846
    const/4 v2, 0x3

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move v2, v1

    .line 837
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 850
    :cond_1
    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    move v2, v1

    .line 854
    goto :goto_2

    .line 859
    :cond_3
    const/16 v0, -0x80

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 861
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    new-array v0, v0, [B

    .line 862
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 863
    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 865
    return-object v0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 692
    and-int/lit16 v0, p0, 0xff

    or-int/lit16 v0, v0, 0x100

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(JZ)[B
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 872
    const-wide/16 v0, 0x0

    .line 873
    monitor-enter p0

    .line 875
    :try_start_0
    iget-wide v2, p0, Ltv/periscope/android/video/RTMPPublisher;->v:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    .line 877
    iget-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->v:J

    iget-wide v2, p0, Ltv/periscope/android/video/RTMPPublisher;->u:J

    sub-long v2, p1, v2

    add-long/2addr v0, v2

    .line 878
    long-to-double v0, v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    const-wide v2, 0x41e0754fd0000000L    # 2.2089888E9

    add-double/2addr v0, v2

    move-wide v2, v0

    .line 882
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->p:Ljava/util/HashMap;

    const-string/jumbo v1, "rotation"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 884
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->p:Ljava/util/HashMap;

    const-string/jumbo v1, "rotation"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->I:D

    .line 886
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 888
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->q:Ljava/util/HashMap;

    const-string/jumbo v1, "ntp"

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 890
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 891
    if-eqz p3, :cond_3

    .line 893
    monitor-enter p0

    .line 895
    :try_start_1
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->p:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    .line 897
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->p:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 899
    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 900
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->q:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 919
    :goto_1
    new-array v1, v7, [Ljava/lang/Object;

    aput-object v0, v1, v6

    .line 920
    invoke-static {v1}, Ltv/periscope/android/video/rtmp/a;->a([Ljava/lang/Object;)[B

    move-result-object v1

    .line 921
    if-eqz p3, :cond_2

    .line 923
    invoke-static {v1, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 924
    const-string/jumbo v3, "Base64"

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 928
    :cond_2
    new-array v2, v8, [Ljava/lang/Object;

    const-string/jumbo v3, "Periscope"

    aput-object v3, v2, v6

    aput-object v0, v2, v7

    .line 929
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0, v2, p1, p2}, Ltv/periscope/android/video/rtmp/Connection;->a([Ljava/lang/Object;J)V

    .line 931
    return-object v1

    .line 886
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 899
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 904
    :cond_3
    const-string/jumbo v1, "ntp"

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 905
    monitor-enter p0

    .line 907
    :try_start_4
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->p:Ljava/util/HashMap;

    if-eqz v1, :cond_4

    .line 909
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->p:Ljava/util/HashMap;

    const-string/jumbo v2, "rotation"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 911
    const-string/jumbo v1, "rotation"

    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->p:Ljava/util/HashMap;

    const-string/jumbo v3, "rotation"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 914
    :cond_4
    monitor-exit p0

    goto :goto_1

    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0

    :cond_5
    move-wide v2, v0

    goto/16 :goto_0
.end method

.method private l()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->m()V

    .line 122
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->u()V

    .line 123
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->n()V

    .line 124
    return-void
.end method

.method private m()V
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->t:Ljava/util/HashMap;

    const-string/jumbo v1, "RtmpConnectSuccess"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->t:Ljava/util/HashMap;

    const-string/jumbo v1, "RtmpConnectTime"

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    return-void
.end method

.method private n()V
    .locals 8

    .prologue
    .line 133
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->y:J

    .line 134
    new-instance v0, Ltv/periscope/android/video/rtmp/Connection;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/Connection;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    .line 135
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    const-wide/32 v2, 0x2625a0

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/video/rtmp/Connection;->a(J)V

    .line 136
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->z:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->A:Ljava/lang/String;

    iget v3, p0, Ltv/periscope/android/video/RTMPPublisher;->B:I

    iget-object v4, p0, Ltv/periscope/android/video/RTMPPublisher;->C:Ljava/lang/String;

    iget-object v5, p0, Ltv/periscope/android/video/RTMPPublisher;->D:Ljava/lang/String;

    iget-object v6, p0, Ltv/periscope/android/video/RTMPPublisher;->E:Ljava/lang/String;

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Ltv/periscope/android/video/rtmp/Connection;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/video/rtmp/Connection$a;)V

    .line 137
    return-void
.end method

.method private o()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 581
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/Connection;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 584
    aput-object v1, v0, v5

    .line 585
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v1}, Ltv/periscope/android/video/rtmp/Connection;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 586
    const-string/jumbo v1, "live"

    aput-object v1, v0, v7

    .line 587
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v1}, Ltv/periscope/android/video/rtmp/Connection;->e()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 588
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    const-string/jumbo v2, "fast-publish"

    invoke-virtual {v1, v2, v0}, Ltv/periscope/android/video/rtmp/Connection;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 601
    :goto_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 602
    const-string/jumbo v1, "connectiondata"

    const-string/jumbo v2, "In IP4 0.0.0.0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 603
    const-string/jumbo v1, "name"

    const-string/jumbo v2, "Live stream from Periscope"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 604
    const-string/jumbo v1, "protocolversion"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 605
    const-string/jumbo v1, "timing"

    const-string/jumbo v2, "0 0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 607
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 608
    const-string/jumbo v2, "rtpsessioninfo"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 610
    new-array v0, v7, [Ljava/lang/Object;

    .line 611
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->p()Ljava/util/Map;

    move-result-object v2

    aput-object v2, v0, v5

    .line 612
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->q()Ljava/util/Map;

    move-result-object v2

    aput-object v2, v0, v6

    .line 613
    const-string/jumbo v2, "trackinfo"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 615
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->i:Landroid/media/MediaFormat;

    const-string/jumbo v2, "width"

    invoke-virtual {v0, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 616
    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->i:Landroid/media/MediaFormat;

    const-string/jumbo v3, "height"

    invoke-virtual {v2, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    .line 617
    const-string/jumbo v3, "videocodecid"

    const-string/jumbo v4, "avc1"

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 618
    const-string/jumbo v3, "width"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 619
    const-string/jumbo v3, "displayWidth"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    const-string/jumbo v3, "frameWidth"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 621
    const-string/jumbo v0, "height"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 622
    const-string/jumbo v0, "displayHeight"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 623
    const-string/jumbo v0, "frameHeight"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 625
    const-string/jumbo v0, "audiocodecid"

    const-string/jumbo v2, "mp4a"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 626
    const-string/jumbo v0, "audiochannels"

    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->j:Landroid/media/MediaFormat;

    const-string/jumbo v3, "channel-count"

    invoke-virtual {v2, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 627
    const-string/jumbo v0, "audiosamplerate"

    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->j:Landroid/media/MediaFormat;

    const-string/jumbo v3, "sample-rate"

    invoke-virtual {v2, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    new-array v0, v7, [Ljava/lang/Object;

    const-string/jumbo v2, "onMetaData"

    aput-object v2, v0, v5

    aput-object v1, v0, v6

    .line 630
    new-instance v1, Ltv/periscope/android/video/rtmp/h;

    const/16 v2, 0x12

    const/4 v3, 0x5

    iget-object v4, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v4}, Ltv/periscope/android/video/rtmp/Connection;->f()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Ltv/periscope/android/video/rtmp/h;-><init>(III)V

    .line 631
    invoke-virtual {v1, v0}, Ltv/periscope/android/video/rtmp/h;->a([Ljava/lang/Object;)V

    .line 632
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0, v1}, Ltv/periscope/android/video/rtmp/Connection;->b(Ltv/periscope/android/video/rtmp/h;)V

    .line 633
    const-string/jumbo v0, "RTMP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Metadata: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    const/16 v1, 0x200

    invoke-virtual {v0, v1}, Ltv/periscope/android/video/rtmp/Connection;->a(I)V

    .line 639
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->r()V

    .line 641
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->s()V

    .line 642
    return-void

    .line 594
    :cond_0
    new-array v0, v2, [Ljava/lang/Object;

    .line 595
    aput-object v1, v0, v5

    .line 596
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v1}, Ltv/periscope/android/video/rtmp/Connection;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 597
    const-string/jumbo v1, "live"

    aput-object v1, v0, v7

    .line 598
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    const-string/jumbo v2, "publish"

    invoke-virtual {v1, v2, v0}, Ltv/periscope/android/video/rtmp/Connection;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private p()Ljava/util/Map;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 647
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 649
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    aget-byte v1, v1, v7

    and-int/lit16 v1, v1, 0xff

    invoke-static {v1}, Ltv/periscope/android/video/RTMPPublisher;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    aget-byte v1, v1, v8

    and-int/lit16 v1, v1, 0xff

    invoke-static {v1}, Ltv/periscope/android/video/RTMPPublisher;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    const/4 v3, 0x3

    aget-byte v1, v1, v3

    and-int/lit16 v1, v1, 0xff

    invoke-static {v1}, Ltv/periscope/android/video/RTMPPublisher;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 650
    const-string/jumbo v0, "profile-level-id"

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 652
    const-string/jumbo v0, "Baseline"

    .line 653
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    aget-byte v1, v1, v7

    const/16 v4, 0x4d

    if-ne v1, v4, :cond_2

    .line 655
    const-string/jumbo v0, "Main"

    .line 661
    :cond_0
    :goto_0
    const-string/jumbo v1, ""

    .line 662
    iget-object v4, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    aget-byte v4, v4, v8

    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_1

    .line 664
    const-string/jumbo v1, "Constrained "

    .line 666
    :cond_1
    const-string/jumbo v4, "RTMP"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Profile-level-id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " profile: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    invoke-static {v1, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    invoke-static {v1, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 669
    const-string/jumbo v1, "sprop-parameter-sets"

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 671
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->i:Landroid/media/MediaFormat;

    const-string/jumbo v1, "width"

    invoke-virtual {v0, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 672
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->i:Landroid/media/MediaFormat;

    const-string/jumbo v3, "height"

    invoke-virtual {v1, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    .line 673
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "{H264CodecConfigInfo: codec:H264, profile:Main, level:2.1, frameSize:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", displaySize:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "x"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", crop: l:0 r:0 t:0 b:0}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 674
    const-string/jumbo v1, "description"

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 675
    const-string/jumbo v0, "language"

    const-string/jumbo v1, "eng"

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 676
    const-string/jumbo v0, "timescale"

    const v1, 0x15f90

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 677
    const-string/jumbo v0, "type"

    const-string/jumbo v1, "video"

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 680
    new-array v0, v7, [Ljava/lang/Object;

    .line 681
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 682
    const-string/jumbo v3, "sampletype"

    const-string/jumbo v4, "H264"

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 683
    const/4 v3, 0x0

    aput-object v1, v0, v3

    .line 684
    const-string/jumbo v1, "sampledescription"

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 686
    const-string/jumbo v0, "RTMP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Video props: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    return-object v2

    .line 657
    :cond_2
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    aget-byte v1, v1, v7

    const/16 v4, 0x64

    if-ne v1, v4, :cond_0

    .line 659
    const-string/jumbo v0, "High"

    goto/16 :goto_0
.end method

.method private q()Ljava/util/Map;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 697
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 698
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->j:Landroid/media/MediaFormat;

    const-string/jumbo v2, "channel-count"

    invoke-virtual {v1, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    .line 699
    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->j:Landroid/media/MediaFormat;

    const-string/jumbo v3, "sample-rate"

    invoke-virtual {v2, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    .line 700
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "{AACFrame: codec:AAC, channels:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", frequency:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", samplesPerFrame:1024, objectType:LC}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 701
    const-string/jumbo v4, "description"

    invoke-virtual {v0, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 702
    const-string/jumbo v3, "language"

    const-string/jumbo v4, "eng"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 703
    const-string/jumbo v3, "timescale"

    const v4, 0x15f90

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 704
    const-string/jumbo v3, "type"

    const-string/jumbo v4, "audio"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 706
    new-array v3, v8, [Ljava/lang/Object;

    .line 707
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 708
    const-string/jumbo v5, "sampletype"

    const-string/jumbo v6, "mpeg4-generic"

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 709
    aput-object v4, v3, v7

    .line 710
    const-string/jumbo v4, "sampledescription"

    invoke-virtual {v0, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 713
    invoke-static {v2, v1}, Ltv/periscope/android/video/a;->a(II)[B

    move-result-object v1

    .line 714
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-byte v3, v1, v7

    invoke-static {v3}, Ltv/periscope/android/video/RTMPPublisher;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-byte v1, v1, v8

    invoke-static {v1}, Ltv/periscope/android/video/RTMPPublisher;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 715
    const-string/jumbo v2, "config"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 717
    const-string/jumbo v1, "RTMP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Audio props: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    return-object v0
.end method

.method private r()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 724
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->t()[B

    move-result-object v0

    .line 725
    array-length v1, v0

    add-int/lit8 v1, v1, 0x5

    new-array v1, v1, [B

    .line 726
    const/16 v2, 0x17

    aput-byte v2, v1, v4

    .line 727
    const/4 v2, 0x5

    array-length v3, v0

    invoke-static {v0, v4, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 729
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ltv/periscope/android/video/rtmp/Connection;->a([BJ)V

    .line 730
    return-void
.end method

.method private s()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 734
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->j:Landroid/media/MediaFormat;

    const-string/jumbo v1, "channel-count"

    invoke-virtual {v0, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 735
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->j:Landroid/media/MediaFormat;

    const-string/jumbo v2, "sample-rate"

    invoke-virtual {v1, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    .line 736
    invoke-static {v1, v0}, Ltv/periscope/android/video/a;->a(II)[B

    move-result-object v0

    .line 737
    const/4 v1, 0x4

    new-array v1, v1, [B

    .line 738
    const/16 v2, -0x51

    aput-byte v2, v1, v3

    .line 739
    const/4 v2, 0x2

    aget-byte v3, v0, v3

    aput-byte v3, v1, v2

    .line 740
    const/4 v2, 0x3

    const/4 v3, 0x1

    aget-byte v0, v0, v3

    aput-byte v0, v1, v2

    .line 741
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ltv/periscope/android/video/rtmp/Connection;->b([BJ)V

    .line 742
    return-void
.end method

.method private t()[B
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 746
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0xb

    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    array-length v1, v1

    add-int/2addr v0, v1

    new-array v0, v0, [B

    .line 747
    aput-byte v4, v0, v5

    .line 748
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    const/4 v2, 0x3

    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 749
    const/4 v1, 0x4

    const/4 v2, -0x1

    aput-byte v2, v0, v1

    .line 750
    const/4 v1, 0x5

    const/16 v2, -0x1f

    aput-byte v2, v0, v1

    .line 751
    const/4 v1, 0x6

    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    array-length v2, v2

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 752
    const/4 v1, 0x7

    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    array-length v2, v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 753
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    const/16 v2, 0x8

    iget-object v3, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    array-length v3, v3

    invoke-static {v1, v5, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 754
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x8

    .line 755
    add-int/lit8 v2, v1, 0x1

    aput-byte v4, v0, v1

    .line 756
    add-int/lit8 v1, v2, 0x1

    iget-object v3, p0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    array-length v3, v3

    shr-int/lit8 v3, v3, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 757
    add-int/lit8 v2, v1, 0x1

    iget-object v3, p0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    array-length v3, v3

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 758
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    iget-object v3, p0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    array-length v3, v3

    invoke-static {v1, v5, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 760
    return-object v0
.end method

.method private u()V
    .locals 6

    .prologue
    const-wide/16 v2, 0xbb8

    .line 1008
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->x:Ljava/util/Timer;

    .line 1009
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->x:Ljava/util/Timer;

    new-instance v1, Ltv/periscope/android/video/RTMPPublisher$1;

    invoke-direct {v1, p0}, Ltv/periscope/android/video/RTMPPublisher$1;-><init>(Ltv/periscope/android/video/RTMPPublisher;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 1017
    return-void
.end method

.method private v()V
    .locals 1

    .prologue
    .line 1021
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->x:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1023
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->x:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1024
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->x:Ljava/util/Timer;

    .line 1026
    :cond_0
    return-void
.end method

.method private w()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1030
    .line 1031
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1032
    monitor-enter p0

    .line 1034
    :try_start_0
    iget-wide v2, p0, Ltv/periscope/android/video/RTMPPublisher;->y:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 1036
    iget-wide v2, p0, Ltv/periscope/android/video/RTMPPublisher;->y:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3a98

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1038
    const-string/jumbo v0, "RTMP"

    const-string/jumbo v1, "Restart on Connect timeout"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1039
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->K:Z

    .line 1040
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->y:J

    .line 1043
    :cond_0
    iget-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->K:Z

    if-eqz v0, :cond_1

    .line 1045
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->K:Z

    .line 1046
    invoke-virtual {p0}, Ltv/periscope/android/video/RTMPPublisher;->j()V

    .line 1048
    :cond_1
    monitor-exit p0

    .line 1049
    return-void

    .line 1048
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private x()V
    .locals 4

    .prologue
    .line 1054
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->J:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    sget-object v1, Ltv/periscope/android/video/RTMPPublisher$PublishState;->a:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->t:Ljava/util/HashMap;

    const-string/jumbo v1, "RtmpConnectTime"

    .line 1055
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1056
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Ltv/periscope/android/video/RTMPPublisher;->y:J

    sub-long/2addr v0, v2

    .line 1057
    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->t:Ljava/util/HashMap;

    const-string/jumbo v3, "RtmpConnectTime"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1059
    :cond_0
    return-void
.end method


# virtual methods
.method public a()D
    .locals 2

    .prologue
    .line 171
    monitor-enter p0

    .line 173
    :try_start_0
    iget-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->I:D

    monitor-exit p0

    return-wide v0

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 487
    if-lez p1, :cond_1

    .line 489
    monitor-enter p0

    .line 491
    :try_start_0
    iget-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->d:Z

    if-nez v0, :cond_0

    .line 493
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->d:Z

    .line 494
    iget-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->f:Z

    if-eqz v0, :cond_0

    .line 496
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->o()V

    .line 500
    :cond_0
    monitor-exit p0

    .line 502
    :cond_1
    return-void

    .line 500
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Landroid/media/MediaFormat;Landroid/media/MediaFormat;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 146
    iput-object p2, p0, Ltv/periscope/android/video/RTMPPublisher;->i:Landroid/media/MediaFormat;

    .line 147
    iput-object p1, p0, Ltv/periscope/android/video/RTMPPublisher;->j:Landroid/media/MediaFormat;

    .line 149
    const/4 v1, 0x0

    .line 150
    monitor-enter p0

    .line 152
    :try_start_0
    iget-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->f:Z

    if-nez v2, :cond_1

    .line 154
    const/4 v2, 0x1

    iput-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->f:Z

    .line 155
    iget-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->d:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->e:Z

    if-eqz v2, :cond_1

    .line 160
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    if-eqz v0, :cond_0

    .line 163
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->o()V

    .line 166
    :cond_0
    const-wide v0, 0x408f400000000000L    # 1000.0

    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->j:Landroid/media/MediaFormat;

    const-string/jumbo v3, "channel-count"

    invoke-virtual {v2, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->j:Landroid/media/MediaFormat;

    const-string/jumbo v3, "sample-rate"

    invoke-virtual {v2, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->o:D

    .line 167
    return-void

    .line 160
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/16 v6, 0xa

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 506
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    if-eqz v0, :cond_0

    .line 509
    new-array v0, v6, [B

    .line 510
    const/16 v1, 0x37

    aput-byte v1, v0, v5

    .line 511
    aput-byte v4, v0, v4

    .line 513
    const-wide/16 v2, 0x1

    const/4 v1, 0x5

    invoke-static {v2, v3, v0, v1}, Ltv/periscope/android/video/rtmp/Connection;->a(J[BI)V

    .line 514
    const/16 v1, 0x9

    aput-byte v6, v0, v1

    .line 515
    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v1, v0, v8, v9}, Ltv/periscope/android/video/rtmp/Connection;->a([BJ)V

    .line 518
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 519
    const-string/jumbo v1, "EndOfBroadcast"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "Periscope"

    aput-object v2, v1, v5

    aput-object v0, v1, v4

    .line 521
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0, v1, v8, v9}, Ltv/periscope/android/video/rtmp/Connection;->a([Ljava/lang/Object;J)V

    .line 524
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0, p1}, Ltv/periscope/android/video/rtmp/Connection;->a(Ljava/lang/Runnable;)V

    .line 526
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 179
    monitor-enter p0

    .line 181
    :try_start_0
    iget-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->F:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 183
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->F:J

    .line 185
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->w:Ltv/periscope/android/video/c;

    iget-object v1, p0, Ltv/periscope/android/video/RTMPPublisher;->j:Landroid/media/MediaFormat;

    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->i:Landroid/media/MediaFormat;

    invoke-virtual {v0, p1, v1, v2}, Ltv/periscope/android/video/c;->a(Ljava/lang/String;Landroid/media/MediaFormat;Landroid/media/MediaFormat;)Z

    move-result v0

    iput-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->h:Z

    .line 187
    return-void

    .line 185
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    const-wide/16 v4, 0x0

    .line 205
    iget-wide v0, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    div-long/2addr v0, v6

    .line 206
    monitor-enter p0

    .line 208
    :try_start_0
    iget-boolean v2, p0, Ltv/periscope/android/video/RTMPPublisher;->g:Z

    if-nez v2, :cond_0

    .line 210
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->m:J

    .line 211
    monitor-exit p0

    .line 241
    :goto_0
    return-void

    .line 213
    :cond_0
    iget-wide v2, p0, Ltv/periscope/android/video/RTMPPublisher;->v:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    cmp-long v2, v0, v4

    if-lez v2, :cond_1

    .line 216
    invoke-static {}, Ltv/periscope/android/video/rtmp/d;->a()Ltv/periscope/android/video/rtmp/d;

    move-result-object v2

    invoke-virtual {v2}, Ltv/periscope/android/video/rtmp/d;->d()J

    move-result-wide v2

    iput-wide v2, p0, Ltv/periscope/android/video/RTMPPublisher;->v:J

    .line 217
    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->u:J

    .line 219
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    iget-wide v2, p0, Ltv/periscope/android/video/RTMPPublisher;->m:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 223
    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->n:J

    .line 228
    :goto_1
    iget-wide v2, p0, Ltv/periscope/android/video/RTMPPublisher;->m:J

    const-wide/16 v4, 0x400

    add-long/2addr v2, v4

    iput-wide v2, p0, Ltv/periscope/android/video/RTMPPublisher;->m:J

    .line 229
    mul-long v2, v0, v6

    iput-wide v2, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    .line 230
    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->w:Ltv/periscope/android/video/c;

    invoke-virtual {v2, p1, p2}, Ltv/periscope/android/video/c;->a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 232
    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    const/16 v3, 0x8

    const/4 v4, 0x6

    iget-object v5, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v5}, Ltv/periscope/android/video/rtmp/Connection;->f()I

    move-result v5

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    invoke-virtual {v2, v3, v4, v5, v6}, Ltv/periscope/android/video/rtmp/Connection;->a(IIII)Ltv/periscope/android/video/rtmp/h;

    move-result-object v2

    .line 233
    invoke-virtual {v2}, Ltv/periscope/android/video/rtmp/h;->e()Ltv/periscope/android/video/rtmp/f;

    move-result-object v3

    .line 234
    const/16 v4, -0x51

    invoke-virtual {v3, v4}, Ltv/periscope/android/video/rtmp/f;->a(B)V

    .line 235
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ltv/periscope/android/video/rtmp/f;->a(B)V

    .line 236
    iget-object v4, v3, Ltv/periscope/android/video/rtmp/f;->a:[B

    iget v5, v3, Ltv/periscope/android/video/rtmp/f;->b:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v6

    invoke-virtual {p1, v4, v5, v6}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 237
    iget v4, v3, Ltv/periscope/android/video/rtmp/f;->b:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v3, Ltv/periscope/android/video/rtmp/f;->b:I

    .line 238
    invoke-virtual {v2, v0, v1}, Ltv/periscope/android/video/rtmp/h;->a(J)V

    .line 239
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0, v2}, Ltv/periscope/android/video/rtmp/Connection;->b(Ltv/periscope/android/video/rtmp/h;)V

    .line 240
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0, v2}, Ltv/periscope/android/video/rtmp/Connection;->a(Ltv/periscope/android/video/rtmp/h;)V

    goto :goto_0

    .line 219
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 226
    :cond_2
    iget-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->n:J

    iget-wide v2, p0, Ltv/periscope/android/video/RTMPPublisher;->m:J

    long-to-double v2, v2

    iget-wide v4, p0, Ltv/periscope/android/video/RTMPPublisher;->o:D

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_1
.end method

.method public a(Ljava/util/HashMap;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 792
    monitor-enter p0

    .line 794
    :try_start_0
    iput-object p1, p0, Ltv/periscope/android/video/RTMPPublisher;->p:Ljava/util/HashMap;

    .line 797
    iput-boolean p2, p0, Ltv/periscope/android/video/RTMPPublisher;->r:Z

    .line 798
    iput-boolean p3, p0, Ltv/periscope/android/video/RTMPPublisher;->s:Z

    .line 799
    monitor-exit p0

    .line 800
    return-void

    .line 799
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ltv/periscope/android/video/RTMPPublisher$a;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Ltv/periscope/android/video/RTMPPublisher;->l:Ltv/periscope/android/video/RTMPPublisher$a;

    .line 142
    return-void
.end method

.method public a(Ltv/periscope/android/video/rtmp/h;)Z
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 550
    invoke-virtual {p1}, Ltv/periscope/android/video/rtmp/h;->b()I

    move-result v0

    const/16 v1, 0x14

    if-ne v0, v1, :cond_0

    .line 552
    invoke-virtual {p1}, Ltv/periscope/android/video/rtmp/h;->h()[Ljava/lang/Object;

    move-result-object v1

    .line 553
    aget-object v0, v1, v3

    check-cast v0, Ljava/lang/String;

    .line 554
    const-string/jumbo v2, "onStatus"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    array-length v0, v1

    if-le v0, v4, :cond_0

    aget-object v0, v1, v4

    instance-of v0, v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 556
    aget-object v0, v1, v4

    check-cast v0, Ljava/util/Map;

    .line 557
    const-string/jumbo v1, "code"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 558
    const-string/jumbo v1, "NetStream.Publish.Start"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 560
    monitor-enter p0

    .line 562
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->k:Z

    .line 563
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->g:Z

    .line 566
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->t:Ljava/util/HashMap;

    const-string/jumbo v1, "RtmpConnectSuccess"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 567
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->x()V

    .line 570
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->y:J

    .line 571
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572
    sget-object v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;->b:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    invoke-direct {p0, v0}, Ltv/periscope/android/video/RTMPPublisher;->a(Ltv/periscope/android/video/RTMPPublisher$PublishState;)V

    .line 576
    :cond_0
    return v3

    .line 571
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 4

    .prologue
    const/16 v3, 0x1bb

    const/16 v2, 0x50

    .line 972
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->x()V

    .line 973
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->J:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    sget-object v1, Ltv/periscope/android/video/RTMPPublisher$PublishState;->d:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    if-eq v0, v1, :cond_0

    .line 976
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/Connection;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Ltv/periscope/android/video/RTMPPublisher;->B:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->z:Ljava/lang/String;

    const-string/jumbo v1, "rtmp"

    .line 978
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 982
    const-string/jumbo v0, "RTMP"

    const-string/jumbo v1, "Reconnecting with RTMPS"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    iput v3, p0, Ltv/periscope/android/video/RTMPPublisher;->B:I

    .line 984
    const-string/jumbo v0, "RTMPS"

    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->z:Ljava/lang/String;

    .line 985
    const-string/jumbo v0, "RTMP"

    const-string/jumbo v1, "Attempt restart with SSL:443"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->K:Z

    .line 1004
    :cond_0
    return-void

    .line 987
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/Connection;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Ltv/periscope/android/video/RTMPPublisher;->B:I

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->z:Ljava/lang/String;

    const-string/jumbo v1, "psp"

    .line 989
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 993
    const-string/jumbo v0, "RTMP"

    const-string/jumbo v1, "Reconnecting with PSPS"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    iput v3, p0, Ltv/periscope/android/video/RTMPPublisher;->B:I

    .line 995
    const-string/jumbo v0, "PSPS"

    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->z:Ljava/lang/String;

    .line 996
    const-string/jumbo v0, "RTMP"

    const-string/jumbo v1, "Attempt restart with SSL:443"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1000
    :cond_2
    const-string/jumbo v0, "RTMP"

    const-string/jumbo v1, "Restart on socket close"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 19

    .prologue
    .line 245
    move-object/from16 v0, p2

    iget-wide v2, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v4, 0x3e8

    div-long v10, v2, v4

    .line 246
    monitor-enter p0

    .line 248
    :try_start_0
    move-object/from16 v0, p0

    iget-wide v2, v0, Ltv/periscope/android/video/RTMPPublisher;->v:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v10, v2

    if-lez v2, :cond_0

    .line 250
    invoke-static {}, Ltv/periscope/android/video/rtmp/d;->a()Ltv/periscope/android/video/rtmp/d;

    move-result-object v2

    invoke-virtual {v2}, Ltv/periscope/android/video/rtmp/d;->d()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Ltv/periscope/android/video/RTMPPublisher;->v:J

    .line 251
    move-object/from16 v0, p0

    iput-wide v10, v0, Ltv/periscope/android/video/RTMPPublisher;->u:J

    .line 253
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 255
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/video/RTMPPublisher;->w:Ltv/periscope/android/video/c;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Ltv/periscope/android/video/c;->b(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 265
    const/4 v7, 0x0

    .line 267
    const/4 v6, 0x0

    .line 268
    const/4 v5, 0x0

    .line 270
    const/4 v4, 0x0

    .line 271
    const/4 v3, 0x0

    .line 272
    const/4 v2, 0x0

    .line 274
    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v8

    add-int/lit8 v8, v8, -0x4

    if-ge v7, v8, :cond_1d

    .line 276
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    if-nez v8, :cond_a

    add-int/lit8 v8, v7, 0x1

    .line 277
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    if-nez v8, :cond_a

    add-int/lit8 v8, v7, 0x2

    .line 278
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_a

    .line 280
    add-int/lit8 v8, v7, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    .line 281
    and-int/lit8 v9, v8, 0x1f

    .line 282
    and-int/lit8 v12, v8, 0x60

    if-eqz v12, :cond_1

    .line 284
    const/4 v3, 0x1

    .line 286
    :cond_1
    const/4 v12, 0x5

    if-ne v9, v12, :cond_2

    .line 288
    const/4 v4, 0x1

    .line 289
    const/4 v2, 0x1

    .line 291
    :cond_2
    const/4 v12, 0x7

    if-ne v9, v12, :cond_3

    .line 293
    const-string/jumbo v12, "RTMP"

    const-string/jumbo v13, "SPS found"

    invoke-static {v12, v13}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :cond_3
    const/4 v12, 0x1

    if-ne v9, v12, :cond_4

    .line 297
    const/4 v2, 0x1

    .line 299
    :cond_4
    if-nez v6, :cond_9

    .line 301
    add-int/lit8 v6, v7, 0x3

    .line 307
    :goto_1
    and-int/lit8 v8, v8, 0x1f

    const/4 v9, 0x7

    if-eq v8, v9, :cond_a

    move v7, v4

    move v8, v5

    move v9, v6

    move v6, v3

    move v4, v2

    .line 316
    :goto_2
    if-lez v8, :cond_b

    .line 318
    add-int/lit8 v2, v8, -0x3

    sub-int v3, v2, v9

    .line 319
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    sub-int/2addr v2, v8

    move v5, v3

    move v3, v2

    .line 326
    :goto_3
    if-lez v8, :cond_7

    .line 328
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit8 v2, v2, 0x1f

    const/4 v12, 0x7

    if-ne v2, v12, :cond_c

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit8 v2, v2, 0x1f

    const/16 v12, 0x8

    if-ne v2, v12, :cond_c

    .line 330
    monitor-enter p0

    .line 333
    :try_start_1
    new-array v2, v5, [B

    move-object/from16 v0, p0

    iput-object v2, v0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    .line 334
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 335
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12, v5}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 337
    new-array v2, v3, [B

    move-object/from16 v0, p0

    iput-object v2, v0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    .line 338
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 339
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8, v3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 341
    const/4 v2, 0x0

    .line 342
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 344
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v8, v0, Ltv/periscope/android/video/RTMPPublisher;->e:Z

    if-nez v8, :cond_5

    .line 346
    const/4 v8, 0x1

    move-object/from16 v0, p0

    iput-boolean v8, v0, Ltv/periscope/android/video/RTMPPublisher;->e:Z

    .line 347
    move-object/from16 v0, p0

    iget-boolean v8, v0, Ltv/periscope/android/video/RTMPPublisher;->d:Z

    if-eqz v8, :cond_5

    move-object/from16 v0, p0

    iget-boolean v8, v0, Ltv/periscope/android/video/RTMPPublisher;->f:Z

    if-eqz v8, :cond_5

    .line 349
    const/4 v2, 0x1

    .line 352
    :cond_5
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 353
    if-eqz v2, :cond_6

    .line 355
    :try_start_3
    invoke-direct/range {p0 .. p0}, Ltv/periscope/android/video/RTMPPublisher;->o()V

    .line 357
    :cond_6
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 364
    :cond_7
    :goto_4
    if-nez v4, :cond_d

    .line 483
    :cond_8
    :goto_5
    return-void

    .line 253
    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    .line 304
    :cond_9
    add-int/lit8 v5, v7, 0x3

    goto/16 :goto_1

    .line 313
    :cond_a
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 322
    :cond_b
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    sub-int v3, v2, v9

    .line 323
    const/4 v2, 0x0

    move v5, v3

    move v3, v2

    goto :goto_3

    .line 352
    :catchall_1
    move-exception v2

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v2

    .line 357
    :catchall_2
    move-exception v2

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v2

    .line 360
    :cond_c
    const-string/jumbo v2, "RTMP"

    const-string/jumbo v8, "Unexpected NALU structure"

    invoke-static {v2, v8}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 369
    :cond_d
    monitor-enter p0

    .line 371
    :try_start_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Ltv/periscope/android/video/RTMPPublisher;->g:Z

    if-nez v2, :cond_e

    .line 373
    monitor-exit p0

    goto :goto_5

    .line 375
    :catchall_3
    move-exception v2

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v2

    :cond_e
    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 377
    move-object/from16 v0, p0

    iget-boolean v2, v0, Ltv/periscope/android/video/RTMPPublisher;->k:Z

    if-nez v2, :cond_f

    .line 379
    if-eqz v7, :cond_8

    .line 384
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Ltv/periscope/android/video/RTMPPublisher;->k:Z

    .line 388
    :cond_f
    if-eqz v7, :cond_11

    .line 391
    monitor-enter p0

    .line 393
    :try_start_9
    move-object/from16 v0, p0

    iget-wide v12, v0, Ltv/periscope/android/video/RTMPPublisher;->F:J

    const-wide/16 v14, 0x0

    cmp-long v2, v12, v14

    if-eqz v2, :cond_10

    .line 395
    move-object/from16 v0, p0

    iget v2, v0, Ltv/periscope/android/video/RTMPPublisher;->H:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Ltv/periscope/android/video/RTMPPublisher;->H:I

    .line 397
    :cond_10
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 401
    :cond_11
    add-int/lit8 v2, v5, 0x4

    .line 402
    if-lez v3, :cond_12

    .line 404
    add-int/lit8 v3, v3, 0x4

    add-int/2addr v2, v3

    .line 406
    :cond_12
    const/4 v3, 0x0

    .line 407
    const/4 v4, 0x0

    .line 408
    monitor-enter p0

    .line 410
    :try_start_a
    move-object/from16 v0, p0

    iget-boolean v8, v0, Ltv/periscope/android/video/RTMPPublisher;->r:Z

    if-nez v8, :cond_13

    move-object/from16 v0, p0

    iget-boolean v8, v0, Ltv/periscope/android/video/RTMPPublisher;->s:Z

    if-eqz v8, :cond_14

    .line 412
    :cond_13
    const/4 v4, 0x1

    .line 413
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iput-boolean v8, v0, Ltv/periscope/android/video/RTMPPublisher;->r:Z

    .line 415
    :cond_14
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 416
    if-nez v7, :cond_15

    if-eqz v4, :cond_1c

    .line 418
    :cond_15
    if-eqz v7, :cond_16

    .line 420
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    array-length v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    array-length v4, v4

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x8

    add-int/2addr v2, v3

    .line 422
    :cond_16
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11, v7}, Ltv/periscope/android/video/RTMPPublisher;->a(JZ)[B

    move-result-object v3

    .line 423
    if-eqz v3, :cond_1b

    .line 425
    array-length v4, v3

    add-int/lit8 v4, v4, 0x4

    add-int/2addr v2, v4

    move-object/from16 v18, v3

    move v3, v2

    move-object/from16 v2, v18

    .line 429
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    const/16 v8, 0x9

    const/4 v12, 0x7

    move-object/from16 v0, p0

    iget-object v13, v0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v13}, Ltv/periscope/android/video/rtmp/Connection;->f()I

    move-result v13

    add-int/lit8 v3, v3, 0x5

    invoke-virtual {v4, v8, v12, v13, v3}, Ltv/periscope/android/video/rtmp/Connection;->a(IIII)Ltv/periscope/android/video/rtmp/h;

    move-result-object v4

    .line 430
    invoke-virtual {v4}, Ltv/periscope/android/video/rtmp/h;->e()Ltv/periscope/android/video/rtmp/f;

    move-result-object v8

    .line 431
    if-eqz v7, :cond_18

    .line 433
    const/16 v3, 0x17

    invoke-virtual {v8, v3}, Ltv/periscope/android/video/rtmp/f;->a(B)V

    .line 441
    :goto_7
    const/4 v3, 0x1

    invoke-virtual {v8, v3}, Ltv/periscope/android/video/rtmp/f;->a(B)V

    .line 444
    const-wide/16 v12, 0x0

    .line 451
    const/16 v3, 0x10

    shr-long v14, v12, v3

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v3, v14

    int-to-byte v3, v3

    invoke-virtual {v8, v3}, Ltv/periscope/android/video/rtmp/f;->a(B)V

    .line 452
    const/16 v3, 0x8

    shr-long v14, v12, v3

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v3, v14

    int-to-byte v3, v3

    invoke-virtual {v8, v3}, Ltv/periscope/android/video/rtmp/f;->a(B)V

    .line 453
    const-wide/16 v14, 0xff

    and-long/2addr v12, v14

    long-to-int v3, v12

    int-to-byte v3, v3

    invoke-virtual {v8, v3}, Ltv/periscope/android/video/rtmp/f;->a(B)V

    .line 455
    const/4 v3, 0x5

    .line 456
    if-eqz v7, :cond_17

    .line 458
    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    array-length v6, v6

    int-to-long v6, v6

    iget-object v12, v8, Ltv/periscope/android/video/rtmp/f;->a:[B

    invoke-static {v6, v7, v12, v3}, Ltv/periscope/android/video/rtmp/Connection;->a(J[BI)V

    .line 459
    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    const/4 v7, 0x0

    iget-object v12, v8, Ltv/periscope/android/video/rtmp/f;->a:[B

    const/16 v13, 0x9

    move-object/from16 v0, p0

    iget-object v14, v0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    array-length v14, v14

    invoke-static {v6, v7, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 460
    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/video/RTMPPublisher;->b:[B

    array-length v6, v6

    add-int/lit8 v6, v6, 0x4

    add-int/2addr v3, v6

    .line 462
    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    array-length v6, v6

    int-to-long v6, v6

    iget-object v12, v8, Ltv/periscope/android/video/rtmp/f;->a:[B

    invoke-static {v6, v7, v12, v3}, Ltv/periscope/android/video/rtmp/Connection;->a(J[BI)V

    .line 463
    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    const/4 v7, 0x0

    iget-object v12, v8, Ltv/periscope/android/video/rtmp/f;->a:[B

    add-int/lit8 v13, v3, 0x4

    move-object/from16 v0, p0

    iget-object v14, v0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    array-length v14, v14

    invoke-static {v6, v7, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 464
    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/video/RTMPPublisher;->c:[B

    array-length v6, v6

    add-int/lit8 v6, v6, 0x4

    add-int/2addr v3, v6

    .line 467
    :cond_17
    if-eqz v2, :cond_1a

    .line 469
    array-length v6, v2

    int-to-long v6, v6

    iget-object v12, v8, Ltv/periscope/android/video/rtmp/f;->a:[B

    invoke-static {v6, v7, v12, v3}, Ltv/periscope/android/video/rtmp/Connection;->a(J[BI)V

    .line 470
    const/4 v6, 0x0

    iget-object v7, v8, Ltv/periscope/android/video/rtmp/f;->a:[B

    add-int/lit8 v12, v3, 0x4

    array-length v13, v2

    invoke-static {v2, v6, v7, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 471
    array-length v2, v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v2, v3

    .line 474
    :goto_8
    int-to-long v6, v5

    iget-object v3, v8, Ltv/periscope/android/video/rtmp/f;->a:[B

    invoke-static {v6, v7, v3, v2}, Ltv/periscope/android/video/rtmp/Connection;->a(J[BI)V

    .line 475
    add-int/lit8 v2, v2, 0x4

    .line 476
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 477
    iget-object v3, v8, Ltv/periscope/android/video/rtmp/f;->a:[B

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2, v5}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 478
    add-int/2addr v2, v5

    .line 479
    iput v2, v8, Ltv/periscope/android/video/rtmp/f;->b:I

    .line 480
    invoke-virtual {v4, v10, v11}, Ltv/periscope/android/video/rtmp/h;->a(J)V

    .line 481
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v2, v4}, Ltv/periscope/android/video/rtmp/Connection;->b(Ltv/periscope/android/video/rtmp/h;)V

    .line 482
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v2, v4}, Ltv/periscope/android/video/rtmp/Connection;->a(Ltv/periscope/android/video/rtmp/h;)V

    goto/16 :goto_5

    .line 397
    :catchall_4
    move-exception v2

    :try_start_b
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    throw v2

    .line 415
    :catchall_5
    move-exception v2

    :try_start_c
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    throw v2

    .line 434
    :cond_18
    if-eqz v6, :cond_19

    .line 436
    const/16 v3, 0x27

    invoke-virtual {v8, v3}, Ltv/periscope/android/video/rtmp/f;->a(B)V

    goto/16 :goto_7

    .line 439
    :cond_19
    const/16 v3, 0x37

    invoke-virtual {v8, v3}, Ltv/periscope/android/video/rtmp/f;->a(B)V

    goto/16 :goto_7

    :cond_1a
    move v2, v3

    goto :goto_8

    :cond_1b
    move-object/from16 v18, v3

    move v3, v2

    move-object/from16 v2, v18

    goto/16 :goto_6

    :cond_1c
    move-object/from16 v18, v3

    move v3, v2

    move-object/from16 v2, v18

    goto/16 :goto_6

    :cond_1d
    move v7, v4

    move v8, v5

    move v9, v6

    move v6, v3

    move v4, v2

    goto/16 :goto_2
.end method

.method public c()V
    .locals 1

    .prologue
    .line 952
    sget-object v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;->d:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    invoke-direct {p0, v0}, Ltv/periscope/android/video/RTMPPublisher;->a(Ltv/periscope/android/video/RTMPPublisher$PublishState;)V

    .line 953
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->h:Z

    return v0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 195
    monitor-enter p0

    .line 197
    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->v:J

    .line 198
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->u:J

    .line 199
    monitor-exit p0

    .line 200
    return-void

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 530
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->v()V

    .line 531
    sget-object v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;->d:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    invoke-direct {p0, v0}, Ltv/periscope/android/video/RTMPPublisher;->a(Ltv/periscope/android/video/RTMPPublisher$PublishState;)V

    .line 532
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->w:Ltv/periscope/android/video/c;

    invoke-virtual {v0}, Ltv/periscope/android/video/c;->a()V

    .line 533
    monitor-enter p0

    .line 535
    :try_start_0
    iget-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->F:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 537
    iget-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->G:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Ltv/periscope/android/video/RTMPPublisher;->F:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->G:J

    .line 538
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->F:J

    .line 540
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 541
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->g:Z

    .line 542
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    if-eqz v0, :cond_1

    .line 544
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/Connection;->i()V

    .line 546
    :cond_1
    return-void

    .line 540
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 765
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    if-nez v0, :cond_0

    .line 767
    const-wide/16 v0, 0x0

    .line 769
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/Connection;->j()J

    move-result-wide v0

    goto :goto_0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 774
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    if-nez v0, :cond_0

    .line 776
    const-wide/16 v0, 0x0

    .line 778
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/Connection;->k()J

    move-result-wide v0

    goto :goto_0
.end method

.method public i()Ljava/util/Date;
    .locals 1

    .prologue
    .line 783
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    if-nez v0, :cond_0

    .line 785
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 787
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/Connection;->l()Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method public j()V
    .locals 2

    .prologue
    .line 957
    const-string/jumbo v0, "RTMP"

    const-string/jumbo v1, "Restarting publish connection"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 958
    sget-object v0, Ltv/periscope/android/video/RTMPPublisher$PublishState;->c:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    invoke-direct {p0, v0}, Ltv/periscope/android/video/RTMPPublisher;->a(Ltv/periscope/android/video/RTMPPublisher$PublishState;)V

    .line 959
    monitor-enter p0

    .line 961
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->d:Z

    .line 962
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/video/RTMPPublisher;->g:Z

    .line 963
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 964
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v0}, Ltv/periscope/android/video/rtmp/Connection;->i()V

    .line 965
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    .line 967
    invoke-direct {p0}, Ltv/periscope/android/video/RTMPPublisher;->n()V

    .line 968
    return-void

    .line 963
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public k()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1063
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    if-eqz v0, :cond_0

    .line 1065
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->t:Ljava/util/HashMap;

    const-string/jumbo v1, "fmsVer"

    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->a:Ltv/periscope/android/video/rtmp/Connection;

    invoke-virtual {v2}, Ltv/periscope/android/video/rtmp/Connection;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1067
    :cond_0
    monitor-enter p0

    .line 1069
    :try_start_0
    iget v0, p0, Ltv/periscope/android/video/RTMPPublisher;->H:I

    if-lez v0, :cond_2

    .line 1071
    iget-wide v0, p0, Ltv/periscope/android/video/RTMPPublisher;->G:J

    .line 1072
    iget-wide v2, p0, Ltv/periscope/android/video/RTMPPublisher;->F:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1074
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Ltv/periscope/android/video/RTMPPublisher;->F:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 1076
    :cond_1
    long-to-double v0, v0

    iget v2, p0, Ltv/periscope/android/video/RTMPPublisher;->H:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 1078
    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    .line 1079
    const-string/jumbo v2, "RTMP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Keyframe interval (secs): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    iget-object v2, p0, Ltv/periscope/android/video/RTMPPublisher;->t:Ljava/util/HashMap;

    const-string/jumbo v3, "KeyframeInterval"

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1082
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1083
    iget-object v0, p0, Ltv/periscope/android/video/RTMPPublisher;->t:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 1082
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
