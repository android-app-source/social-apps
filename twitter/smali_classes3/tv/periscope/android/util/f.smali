.class public Ltv/periscope/android/util/f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/util/f$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/io/FileFilter;

.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ltv/periscope/android/util/f$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Ltv/periscope/android/util/f$1;

    invoke-direct {v0}, Ltv/periscope/android/util/f$1;-><init>()V

    sput-object v0, Ltv/periscope/android/util/f;->a:Ljava/io/FileFilter;

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Ltv/periscope/android/util/f;->b:Ljava/util/Set;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Ltv/periscope/android/util/f;->c:Ljava/util/Map;

    return-void
.end method

.method public static a(Ljava/lang/String;Ltv/periscope/android/util/f$a;)V
    .locals 1

    .prologue
    .line 104
    sget-object v0, Ltv/periscope/android/util/f;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    sget-object v0, Ltv/periscope/android/util/f;->c:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    :goto_0
    return-void

    .line 107
    :cond_0
    invoke-interface {p1, p0}, Ltv/periscope/android/util/f$a;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
