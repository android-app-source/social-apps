.class public Ltv/periscope/android/util/aa;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:[I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field private static final b:I

.field private static final c:I

.field private static final d:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static h:Ltv/periscope/android/util/aa;


# instance fields
.field private final e:[I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final f:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final g:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    const/16 v0, 0xd

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_1:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_2:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_3:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_4:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_5:I

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_6:I

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_7:I

    aput v2, v0, v1

    const/4 v1, 0x7

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_8:I

    aput v2, v0, v1

    const/16 v1, 0x8

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_9:I

    aput v2, v0, v1

    const/16 v1, 0x9

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_10:I

    aput v2, v0, v1

    const/16 v1, 0xa

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_11:I

    aput v2, v0, v1

    const/16 v1, 0xb

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_12:I

    aput v2, v0, v1

    const/16 v1, 0xc

    sget v2, Ltv/periscope/android/library/f$d;->ps__participant_13:I

    aput v2, v0, v1

    sput-object v0, Ltv/periscope/android/util/aa;->a:[I

    .line 37
    sget v0, Ltv/periscope/android/library/f$d;->ps__dark_grey:I

    sput v0, Ltv/periscope/android/util/aa;->b:I

    .line 38
    sget v0, Ltv/periscope/android/library/f$d;->ps__participant_replay:I

    sput v0, Ltv/periscope/android/util/aa;->c:I

    .line 43
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    sput-object v0, Ltv/periscope/android/util/aa;->d:Landroid/support/v4/util/LongSparseArray;

    return-void
.end method

.method private constructor <init>(Landroid/content/res/Resources;)V
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    sget-object v0, Ltv/periscope/android/util/aa;->a:[I

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Ltv/periscope/android/util/aa;->e:[I

    .line 53
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Ltv/periscope/android/util/aa;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 54
    iget-object v1, p0, Ltv/periscope/android/util/aa;->e:[I

    sget-object v2, Ltv/periscope/android/util/aa;->a:[I

    aget v2, v2, v0

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v1, v0

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    :cond_0
    sget v0, Ltv/periscope/android/util/aa;->b:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/util/aa;->f:I

    .line 57
    sget v0, Ltv/periscope/android/util/aa;->c:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/util/aa;->g:I

    .line 58
    return-void
.end method

.method public static a(J)I
    .locals 2
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation

    .prologue
    .line 93
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 94
    sget v0, Ltv/periscope/android/util/aa;->c:I

    .line 99
    :goto_0
    return v0

    .line 95
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    .line 96
    sget v0, Ltv/periscope/android/util/aa;->b:I

    goto :goto_0

    .line 98
    :cond_1
    invoke-static {p0, p1}, Ltv/periscope/android/util/aa;->b(J)I

    move-result v0

    .line 99
    sget-object v1, Ltv/periscope/android/util/aa;->a:[I

    aget v0, v1, v0

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;J)I
    .locals 3

    .prologue
    .line 64
    sget-object v0, Ltv/periscope/android/util/aa;->h:Ltv/periscope/android/util/aa;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Ltv/periscope/android/util/aa;

    invoke-direct {v0, p0}, Ltv/periscope/android/util/aa;-><init>(Landroid/content/res/Resources;)V

    sput-object v0, Ltv/periscope/android/util/aa;->h:Ltv/periscope/android/util/aa;

    .line 67
    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 68
    sget-object v0, Ltv/periscope/android/util/aa;->h:Ltv/periscope/android/util/aa;

    iget v0, v0, Ltv/periscope/android/util/aa;->g:I

    .line 73
    :goto_0
    return v0

    .line 69
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_2

    .line 70
    sget-object v0, Ltv/periscope/android/util/aa;->h:Ltv/periscope/android/util/aa;

    iget v0, v0, Ltv/periscope/android/util/aa;->f:I

    goto :goto_0

    .line 72
    :cond_2
    invoke-static {p1, p2}, Ltv/periscope/android/util/aa;->b(J)I

    move-result v0

    .line 73
    sget-object v1, Ltv/periscope/android/util/aa;->h:Ltv/periscope/android/util/aa;

    iget-object v1, v1, Ltv/periscope/android/util/aa;->e:[I

    aget v0, v1, v0

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;ILjava/lang/String;I)Ltv/periscope/android/view/af;
    .locals 8
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 110
    invoke-static {p2}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    .line 111
    :goto_0
    new-instance v0, Ltv/periscope/android/view/af;

    new-instance v2, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v2}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    sget v6, Ltv/periscope/android/library/f$d;->ps__white:I

    move-object v1, p0

    move v3, p1

    move v4, p1

    move v5, p3

    invoke-direct/range {v0 .. v7}, Ltv/periscope/android/view/af;-><init>(Landroid/content/res/Resources;Landroid/graphics/drawable/shapes/Shape;IIIILjava/lang/CharSequence;)V

    return-object v0

    .line 110
    :cond_0
    const-string/jumbo v7, ""

    goto :goto_0
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 119
    sget-object v0, Ltv/periscope/android/util/aa;->d:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 120
    return-void
.end method

.method private static b(J)I
    .locals 4

    .prologue
    .line 78
    sget-object v0, Ltv/periscope/android/util/aa;->d:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 79
    if-nez v0, :cond_0

    .line 81
    const-wide/16 v0, 0x1

    sub-long v0, p0, v0

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 82
    sget-object v2, Ltv/periscope/android/util/aa;->a:[I

    array-length v2, v2

    int-to-long v2, v2

    rem-long/2addr v0, v2

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 83
    sget-object v1, Ltv/periscope/android/util/aa;->d:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v1, p0, p1, v0}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 85
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/res/Resources;J)I
    .locals 3

    .prologue
    .line 104
    invoke-static {p0, p1, p2}, Ltv/periscope/android/util/aa;->a(Landroid/content/res/Resources;J)I

    move-result v0

    const v1, -0x5f000001

    and-int/2addr v0, v1

    return v0
.end method
