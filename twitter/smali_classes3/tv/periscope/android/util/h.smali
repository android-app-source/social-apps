.class public Ltv/periscope/android/util/h;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "GT-I9505"

    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "GT-I9506"

    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "GT-I9500"

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "SGH-I337"

    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "SGH-M919"

    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "SCH-I545"

    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "SPH-L720"

    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "GT-I9508"

    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "SHV-E300"

    .line 21
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "SCH-R970"

    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "SM-N900"

    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "LG-D801"

    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Ltv/periscope/android/util/h;->a:Z

    .line 12
    return-void

    .line 24
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ltv/periscope/android/util/Size;Ltv/periscope/android/util/Size;)F
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 67
    invoke-virtual {p0}, Ltv/periscope/android/util/Size;->d()F

    move-result v0

    .line 68
    invoke-virtual {p1}, Ltv/periscope/android/util/Size;->d()F

    move-result v4

    .line 69
    cmpg-float v3, v0, v5

    if-gez v3, :cond_1

    move v3, v1

    :goto_0
    cmpg-float v5, v4, v5

    if-gez v5, :cond_2

    :goto_1
    if-eq v3, v1, :cond_0

    .line 70
    invoke-virtual {p0}, Ltv/periscope/android/util/Size;->b()I

    move-result v0

    invoke-virtual {p0}, Ltv/periscope/android/util/Size;->a()I

    move-result v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/Size;->a(II)Ltv/periscope/android/util/Size;

    move-result-object p0

    .line 71
    invoke-virtual {p0}, Ltv/periscope/android/util/Size;->d()F

    move-result v0

    .line 74
    :cond_0
    cmpg-float v0, v0, v4

    if-gez v0, :cond_3

    .line 75
    invoke-virtual {p0}, Ltv/periscope/android/util/Size;->b()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v4

    invoke-virtual {p0}, Ltv/periscope/android/util/Size;->b()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/Size;->a(FF)Ltv/periscope/android/util/Size;

    move-result-object v0

    .line 79
    :goto_2
    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->e()I

    move-result v1

    invoke-virtual {p1}, Ltv/periscope/android/util/Size;->e()I

    move-result v2

    sub-int/2addr v1, v2

    .line 82
    if-lez v1, :cond_4

    .line 84
    int-to-float v1, v1

    .line 89
    :goto_3
    const-string/jumbo v2, "CameraUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Display: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " Camera: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " AR size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " Score: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    return v1

    :cond_1
    move v3, v2

    .line 69
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 77
    :cond_3
    invoke-virtual {p0}, Ltv/periscope/android/util/Size;->a()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Ltv/periscope/android/util/Size;->a()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    invoke-static {v0, v1}, Ltv/periscope/android/util/Size;->a(FF)Ltv/periscope/android/util/Size;

    move-result-object v0

    goto :goto_2

    .line 87
    :cond_4
    neg-int v1, v1

    int-to-float v1, v1

    const/high16 v2, 0x41c80000    # 25.0f

    mul-float/2addr v1, v2

    goto :goto_3
.end method

.method public static a(ILandroid/hardware/Camera$CameraInfo;)Landroid/hardware/Camera;
    .locals 3

    .prologue
    .line 27
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    .line 28
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 29
    invoke-static {v0, p1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 30
    iget v2, p1, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v2, p0, :cond_0

    .line 31
    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    .line 34
    :goto_1
    return-object v0

    .line 28
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34
    :cond_1
    invoke-static {p1}, Ltv/periscope/android/util/h;->a(Landroid/hardware/Camera$CameraInfo;)Landroid/hardware/Camera;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Landroid/hardware/Camera$CameraInfo;)Landroid/hardware/Camera;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-static {v0, p0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 39
    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ltv/periscope/android/util/Size;Ljava/util/List;)Ltv/periscope/android/util/Size;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/android/util/Size;",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;)",
            "Ltv/periscope/android/util/Size;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 94
    if-nez p1, :cond_1

    .line 110
    :cond_0
    return-object v2

    .line 99
    :cond_1
    const/high16 v1, -0x40800000    # -1.0f

    .line 101
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 102
    invoke-static {v0}, Ltv/periscope/android/util/Size;->a(Landroid/hardware/Camera$Size;)Ltv/periscope/android/util/Size;

    move-result-object v3

    .line 103
    invoke-static {p0, v3}, Ltv/periscope/android/util/h;->a(Ltv/periscope/android/util/Size;Ltv/periscope/android/util/Size;)F

    move-result v0

    .line 104
    cmpg-float v5, v0, v1

    if-ltz v5, :cond_2

    const/4 v5, 0x0

    cmpg-float v5, v1, v5

    if-gez v5, :cond_3

    :cond_2
    move-object v1, v3

    :goto_1
    move-object v2, v1

    move v1, v0

    .line 108
    goto :goto_0

    :cond_3
    move v0, v1

    move-object v1, v2

    goto :goto_1
.end method

.method public static a(Landroid/hardware/Camera;Landroid/hardware/Camera$Parameters;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 114
    const-string/jumbo v0, ""

    .line 116
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v2

    .line 118
    if-nez v2, :cond_0

    move v0, v1

    .line 139
    :goto_0
    return v0

    .line 122
    :cond_0
    sget-boolean v3, Ltv/periscope/android/util/h;->a:Z

    if-eqz v3, :cond_2

    .line 123
    const-string/jumbo v0, "auto"

    .line 129
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v2

    .line 131
    :try_start_0
    const-string/jumbo v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 132
    invoke-virtual {p1, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 133
    invoke-virtual {p0, p1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    const/4 v0, 0x1

    goto :goto_0

    .line 124
    :cond_2
    const-string/jumbo v3, "continuous-video"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 125
    const-string/jumbo v0, "continuous-video"

    goto :goto_1

    .line 126
    :cond_3
    const-string/jumbo v3, "infinity"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 127
    const-string/jumbo v0, "infinity"

    goto :goto_1

    .line 136
    :catch_0
    move-exception v0

    .line 137
    invoke-virtual {p1, v2}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    :cond_4
    move v0, v1

    .line 139
    goto :goto_0
.end method

.method public static a(ILjava/util/List;)[I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<[I>;)[I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 43
    if-nez p1, :cond_1

    .line 63
    :cond_0
    return-object v1

    .line 48
    :cond_1
    const v4, 0x7fffffff

    .line 51
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v6

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 52
    aget v5, v0, v6

    .line 53
    const/4 v3, 0x1

    aget v3, v0, v3

    .line 54
    if-lt v3, p0, :cond_3

    if-gt v5, p0, :cond_3

    .line 55
    if-lt v5, v4, :cond_2

    if-ne v5, v4, :cond_3

    if-le v3, v2, :cond_3

    :cond_2
    move v1, v3

    move v2, v5

    :goto_1
    move v4, v2

    move v2, v1

    move-object v1, v0

    .line 62
    goto :goto_0

    :cond_3
    move-object v0, v1

    move v1, v2

    move v2, v4

    goto :goto_1
.end method
