.class public abstract Ltv/periscope/android/util/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/util/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ltv/periscope/android/util/ag",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/util/e;->a:Ljava/util/Set;

    .line 17
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/util/e;->b:Ljava/util/Set;

    .line 18
    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Ltv/periscope/android/util/e;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Ltv/periscope/android/util/e;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 27
    :goto_0
    return-void

    .line 25
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/util/e;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Ltv/periscope/android/util/e;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 47
    iget-object v0, p0, Ltv/periscope/android/util/e;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 48
    iget-object v0, p0, Ltv/periscope/android/util/e;->a:Ljava/util/Set;

    iget-object v1, p0, Ltv/periscope/android/util/e;->b:Ljava/util/Set;

    invoke-static {v0, v1}, Ltv/periscope/android/util/i;->a(Ljava/util/Collection;Ljava/util/Collection;)V

    .line 49
    return-void
.end method

.method protected abstract a(I)Z
.end method

.method protected abstract c(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation
.end method

.method public c()V
    .locals 4

    .prologue
    .line 36
    invoke-virtual {p0}, Ltv/periscope/android/util/e;->a()I

    move-result v1

    .line 37
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 38
    invoke-virtual {p0, v0}, Ltv/periscope/android/util/e;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    iget-object v2, p0, Ltv/periscope/android/util/e;->a:Ljava/util/Set;

    invoke-virtual {p0, v0}, Ltv/periscope/android/util/e;->c(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 37
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_1
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ltv/periscope/android/util/e;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 60
    return-void
.end method

.method public e()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    .line 65
    invoke-virtual {p0}, Ltv/periscope/android/util/e;->a()I

    move-result v4

    move v3, v2

    move v0, v2

    .line 67
    :goto_0
    if-ge v3, v4, :cond_2

    .line 68
    invoke-virtual {p0, v3}, Ltv/periscope/android/util/e;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 69
    add-int/lit8 v0, v0, 0x1

    .line 70
    iget-object v5, p0, Ltv/periscope/android/util/e;->a:Ljava/util/Set;

    invoke-virtual {p0, v3}, Ltv/periscope/android/util/e;->c(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    move v3, v1

    .line 76
    :goto_1
    if-lez v0, :cond_1

    if-nez v3, :cond_1

    move v0, v1

    :goto_2
    return v0

    .line 67
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 76
    goto :goto_2

    :cond_2
    move v3, v2

    goto :goto_1
.end method

.method public f()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Ltv/periscope/android/util/e;->a:Ljava/util/Set;

    return-object v0
.end method
