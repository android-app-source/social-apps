.class public Ltv/periscope/android/util/ae;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/util/ae$b;,
        Ltv/periscope/android/util/ae$a;
    }
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field b:Ltv/periscope/android/util/ae$a;

.field private final c:Ltv/periscope/android/util/ae$b;

.field private final d:Landroid/content/Context;

.field private e:Ljava/lang/String;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Ltv/periscope/android/util/ae;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/util/ae;->d:Landroid/content/Context;

    .line 42
    new-instance v0, Ltv/periscope/android/util/ae$b;

    invoke-direct {v0, p0, p2}, Ltv/periscope/android/util/ae$b;-><init>(Ltv/periscope/android/util/ae;Landroid/os/Handler;)V

    iput-object v0, p0, Ltv/periscope/android/util/ae;->c:Ltv/periscope/android/util/ae$b;

    .line 43
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/util/ae;->b:Ltv/periscope/android/util/ae$a;

    .line 56
    iget-boolean v0, p0, Ltv/periscope/android/util/ae;->f:Z

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Ltv/periscope/android/util/ae;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/util/ae;->c:Ltv/periscope/android/util/ae$b;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/util/ae;->f:Z

    .line 60
    :cond_0
    return-void
.end method

.method public a(Ltv/periscope/android/util/ae$a;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 46
    iput-object p1, p0, Ltv/periscope/android/util/ae;->b:Ltv/periscope/android/util/ae$a;

    .line 47
    iget-boolean v0, p0, Ltv/periscope/android/util/ae;->f:Z

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Ltv/periscope/android/util/ae;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Ltv/periscope/android/util/ae;->c:Ltv/periscope/android/util/ae$b;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 50
    iput-boolean v3, p0, Ltv/periscope/android/util/ae;->f:Z

    .line 52
    :cond_0
    return-void
.end method

.method public b()Ljava/io/File;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 68
    iget-object v0, p0, Ltv/periscope/android/util/ae;->d:Landroid/content/Context;

    sget-object v1, Ltv/periscope/android/util/ae;->a:[Ljava/lang/String;

    invoke-static {v0, v1}, Ltv/periscope/android/permissions/b;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-object v2

    .line 72
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/util/ae;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v3, "_display_name like \'Screenshot%\'"

    const-string/jumbo v5, "date_added desc limit 1"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 74
    if-eqz v3, :cond_4

    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 75
    const-string/jumbo v0, "_data"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 76
    iget-object v0, p0, Ltv/periscope/android/util/ae;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 77
    iput-object v1, p0, Ltv/periscope/android/util/ae;->e:Ljava/lang/String;

    .line 78
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 81
    if-eqz v3, :cond_2

    if-eqz v2, :cond_3

    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    :goto_1
    move-object v2, v0

    .line 78
    goto :goto_0

    .line 81
    :catch_0
    move-exception v1

    invoke-virtual {v2, v1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_4
    if-eqz v3, :cond_0

    if-eqz v2, :cond_5

    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v2, v0}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_5
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 72
    :catch_2
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 81
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    if-eqz v3, :cond_6

    if-eqz v2, :cond_7

    :try_start_4
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    :cond_6
    :goto_3
    throw v0

    :catch_3
    move-exception v1

    invoke-virtual {v2, v1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_7
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2
.end method
