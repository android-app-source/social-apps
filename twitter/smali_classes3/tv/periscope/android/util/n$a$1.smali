.class Ltv/periscope/android/util/n$a$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/util/n$a;-><init>(Landroid/view/View;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/util/n$a;


# direct methods
.method constructor <init>(Ltv/periscope/android/util/n$a;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 6

    .prologue
    .line 54
    iget-object v0, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iget-object v0, v0, Ltv/periscope/android/util/n$a;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iget-object v0, v0, Ltv/periscope/android/util/n$a;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 61
    iget-object v0, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iget-object v0, v0, Ltv/periscope/android/util/n$a;->c:Landroid/view/View;

    iget-object v1, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iget-object v1, v1, Ltv/periscope/android/util/n$a;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 63
    iget-object v0, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iget-object v0, v0, Ltv/periscope/android/util/n$a;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iget-object v1, v1, Ltv/periscope/android/util/n$a;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iget-object v2, v2, Ltv/periscope/android/util/n$a;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    sub-int v2, v0, v1

    .line 64
    iget-object v0, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iget v0, v0, Ltv/periscope/android/util/n$a;->d:I

    if-ge v2, v0, :cond_1

    .line 66
    iget-object v0, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iput v2, v0, Ltv/periscope/android/util/n$a;->e:I

    .line 68
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iget v0, v0, Ltv/periscope/android/util/n$a;->e:I

    sub-int v3, v2, v0

    .line 70
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 72
    iget-object v0, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iget-object v0, v0, Ltv/periscope/android/util/n$a;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 73
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/util/n$a$a;

    .line 74
    if-nez v1, :cond_2

    .line 75
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 79
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iget v0, v0, Ltv/periscope/android/util/n$a;->d:I

    if-le v2, v0, :cond_3

    .line 80
    invoke-interface {v1, v3}, Ltv/periscope/android/util/n$a$a;->a(I)V

    goto :goto_1

    .line 82
    :cond_3
    invoke-interface {v1, v3}, Ltv/periscope/android/util/n$a$a;->b(I)V

    goto :goto_1

    .line 86
    :cond_4
    iget-object v0, p0, Ltv/periscope/android/util/n$a$1;->a:Ltv/periscope/android/util/n$a;

    iget-object v0, v0, Ltv/periscope/android/util/n$a;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
