.class Ltv/periscope/android/chat/p$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/chat/p;->a(Ltv/periscope/android/chat/g;Ljava/lang/String;JLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ltv/periscope/android/chat/g;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ltv/periscope/android/chat/p;


# direct methods
.method constructor <init>(Ltv/periscope/android/chat/p;JLjava/lang/String;Ltv/periscope/android/chat/g;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Ltv/periscope/android/chat/p$1;->e:Ltv/periscope/android/chat/p;

    iput-wide p2, p0, Ltv/periscope/android/chat/p$1;->a:J

    iput-object p4, p0, Ltv/periscope/android/chat/p$1;->b:Ljava/lang/String;

    iput-object p5, p0, Ltv/periscope/android/chat/p$1;->c:Ltv/periscope/android/chat/g;

    iput-object p6, p0, Ltv/periscope/android/chat/p$1;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 159
    const-string/jumbo v0, "ReplayConsumer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fetching more from channel with this token "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Ltv/periscope/android/chat/p$1;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", cursor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/chat/p$1;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Ltv/periscope/android/chat/p$1;->e:Ltv/periscope/android/chat/p;

    invoke-static {v0}, Ltv/periscope/android/chat/p;->a(Ltv/periscope/android/chat/p;)Ltv/periscope/android/player/e;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/chat/p$1;->c:Ltv/periscope/android/chat/g;

    invoke-interface {v0, v1}, Ltv/periscope/android/player/e;->a(Ltv/periscope/android/chat/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/chat/p$1;->e:Ltv/periscope/android/chat/p;

    invoke-static {v0}, Ltv/periscope/android/chat/p;->b(Ltv/periscope/android/chat/p;)Ltv/periscope/android/chat/f;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/chat/p$1;->d:Ljava/lang/String;

    iget-wide v2, p0, Ltv/periscope/android/chat/p$1;->a:J

    iget-object v4, p0, Ltv/periscope/android/chat/p$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Ltv/periscope/android/chat/f;->a(Ljava/lang/String;JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :cond_0
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 164
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Could not call history on: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/chat/p$1;->c:Ltv/periscope/android/chat/g;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " with cursor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/chat/p$1;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->a(Ljava/lang/String;)V

    .line 165
    throw v0
.end method
