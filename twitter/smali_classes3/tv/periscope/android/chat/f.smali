.class public Ltv/periscope/android/chat/f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/chat/f$c;,
        Ltv/periscope/android/chat/f$b;,
        Ltv/periscope/android/chat/f$d;,
        Ltv/periscope/android/chat/f$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/codahale/metrics/Histogram;

.field private final b:Ljava/lang/String;

.field private c:Ltv/periscope/chatman/a;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/codahale/metrics/Histogram;

    new-instance v1, Lcom/codahale/metrics/UniformReservoir;

    invoke-direct {v1}, Lcom/codahale/metrics/UniformReservoir;-><init>()V

    invoke-direct {v0, v1}, Lcom/codahale/metrics/Histogram;-><init>(Lcom/codahale/metrics/Reservoir;)V

    iput-object v0, p0, Ltv/periscope/android/chat/f;->a:Lcom/codahale/metrics/Histogram;

    .line 50
    iput-object p1, p0, Ltv/periscope/android/chat/f;->b:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public static a(Ljava/lang/String;)Ltv/periscope/android/chat/f;
    .locals 1

    .prologue
    .line 54
    new-instance v0, Ltv/periscope/android/chat/f;

    invoke-direct {v0, p0}, Ltv/periscope/android/chat/f;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    invoke-virtual {v0}, Ltv/periscope/chatman/a;->a()V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    .line 142
    :cond_0
    return-void
.end method

.method public a(Lde/greenrobot/event/c;Ltv/periscope/model/u;Ltv/periscope/model/StreamType;Ltv/periscope/android/player/PlayMode;Lretrofit/RestAdapter$LogLevel;Ltv/periscope/chatman/c;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-virtual {p2}, Ltv/periscope/model/u;->i()I

    move-result v3

    .line 63
    iget-boolean v1, p4, Ltv/periscope/android/player/PlayMode;->replayable:Z

    if-eqz v1, :cond_4

    .line 64
    and-int/lit8 v3, v3, -0x2

    .line 65
    invoke-virtual {p2}, Ltv/periscope/model/u;->f()Ljava/lang/String;

    move-result-object v1

    .line 66
    invoke-virtual {p2}, Ltv/periscope/model/u;->h()Ljava/lang/String;

    move-result-object v0

    .line 69
    :goto_0
    invoke-static {v1}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    invoke-virtual {p2}, Ltv/periscope/model/u;->e()Ljava/lang/String;

    move-result-object v1

    .line 72
    :cond_0
    invoke-static {v0}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 73
    invoke-virtual {p2}, Ltv/periscope/model/u;->g()Ljava/lang/String;

    move-result-object v2

    .line 75
    :goto_1
    invoke-virtual {p2}, Ltv/periscope/model/u;->b()Ljava/lang/String;

    move-result-object v7

    .line 77
    if-lez v3, :cond_1

    .line 78
    invoke-static {v1}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v2}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v7}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    new-instance v0, Ltv/periscope/android/chat/f$a;

    iget-object v4, p0, Ltv/periscope/android/chat/f;->a:Lcom/codahale/metrics/Histogram;

    invoke-direct {v0, p1, v4}, Ltv/periscope/android/chat/f$a;-><init>(Lde/greenrobot/event/c;Lcom/codahale/metrics/Histogram;)V

    .line 86
    iget-object v5, p0, Ltv/periscope/android/chat/f;->b:Ljava/lang/String;

    move-object v4, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Ltv/periscope/chatman/a;->a(Ltv/periscope/chatman/a$b;Ljava/lang/String;Ljava/lang/String;ILretrofit/RestAdapter$LogLevel;Ljava/lang/String;Ltv/periscope/chatman/c;)Ltv/periscope/chatman/a;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    .line 88
    iget-object v0, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    invoke-virtual {v0, v7}, Ltv/periscope/chatman/a;->a(Ljava/lang/String;)V

    .line 89
    const-string/jumbo v0, "CM"

    const-string/jumbo v4, "Subscribed to ChatMan: YES"

    invoke-static {v0, v4}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string/jumbo v0, "CM"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "ChatMan: joining room "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "room="

    .line 96
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, ", endpoint="

    .line 97
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ", token="

    .line 98
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", perms={cm="

    .line 99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", subs={cm="

    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", stream-type="

    .line 101
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 102
    const-string/jumbo v1, "CM"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    return-void

    .line 92
    :cond_1
    const-string/jumbo v0, "CM"

    const-string/jumbo v4, "Subscribed to ChatMan: NO"

    invoke-static {v0, v4}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 100
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    move-object v2, v0

    goto/16 :goto_1

    :cond_4
    move-object v1, v0

    goto/16 :goto_0
.end method

.method a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_0

    if-eqz p4, :cond_1

    .line 113
    :cond_0
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "ChatMan: fetching history"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    invoke-virtual {v0, p1, p2, p3, p4}, Ltv/periscope/chatman/a;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 116
    :cond_1
    return-void
.end method

.method public a(Ltv/periscope/android/chat/g;)V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    if-eqz v0, :cond_0

    .line 132
    const-string/jumbo v0, "CM"

    const-string/jumbo v1, "ChatMan: roster"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    iget-object v1, p1, Ltv/periscope/android/chat/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ltv/periscope/chatman/a;->b(Ljava/lang/String;)V

    .line 135
    :cond_0
    return-void
.end method

.method public a(Ltv/periscope/android/chat/g;Ljava/lang/String;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0, p2, p3, p4, p5}, Ltv/periscope/android/chat/f;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 107
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ltv/periscope/android/chat/f;->a(Ltv/periscope/model/chat/Message;Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 123
    new-instance v0, Ltv/periscope/android/api/PsMessage;

    invoke-direct {v0, p1}, Ltv/periscope/android/api/PsMessage;-><init>(Ltv/periscope/model/chat/Message;)V

    .line 125
    iget-object v1, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    if-eqz v1, :cond_0

    .line 126
    iget-object v1, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    invoke-virtual {v1, v0, p2}, Ltv/periscope/chatman/a;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    :cond_0
    return-void
.end method

.method public b()Ltv/periscope/android/api/ChatStats;
    .locals 6

    .prologue
    .line 146
    iget-object v0, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    if-eqz v0, :cond_0

    .line 148
    iget-object v1, p0, Ltv/periscope/android/chat/f;->a:Lcom/codahale/metrics/Histogram;

    monitor-enter v1

    .line 149
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/chat/f;->a:Lcom/codahale/metrics/Histogram;

    invoke-virtual {v0}, Lcom/codahale/metrics/Histogram;->getSnapshot()Lcom/codahale/metrics/Snapshot;

    move-result-object v2

    .line 150
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    new-instance v0, Ltv/periscope/android/api/ChatStats;

    invoke-direct {v0}, Ltv/periscope/android/api/ChatStats;-><init>()V

    .line 153
    iget-object v1, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    invoke-virtual {v1}, Ltv/periscope/chatman/a;->c()I

    move-result v1

    int-to-long v4, v1

    iput-wide v4, v0, Ltv/periscope/android/api/ChatStats;->sent:J

    .line 154
    iget-object v1, p0, Ltv/periscope/android/chat/f;->c:Ltv/periscope/chatman/a;

    invoke-virtual {v1}, Ltv/periscope/chatman/a;->b()I

    move-result v1

    int-to-long v4, v1

    iput-wide v4, v0, Ltv/periscope/android/api/ChatStats;->received:J

    .line 155
    invoke-virtual {v2}, Lcom/codahale/metrics/Snapshot;->getMin()J

    move-result-wide v4

    long-to-double v4, v4

    iput-wide v4, v0, Ltv/periscope/android/api/ChatStats;->latencyMin:D

    .line 156
    invoke-virtual {v2}, Lcom/codahale/metrics/Snapshot;->getMax()J

    move-result-wide v4

    long-to-double v4, v4

    iput-wide v4, v0, Ltv/periscope/android/api/ChatStats;->latencyMax:D

    .line 157
    invoke-virtual {v2}, Lcom/codahale/metrics/Snapshot;->getMedian()D

    move-result-wide v4

    iput-wide v4, v0, Ltv/periscope/android/api/ChatStats;->latencyMedian:D

    .line 158
    invoke-virtual {v2}, Lcom/codahale/metrics/Snapshot;->getMean()D

    move-result-wide v4

    iput-wide v4, v0, Ltv/periscope/android/api/ChatStats;->latencyMean:D

    .line 159
    invoke-virtual {v2}, Lcom/codahale/metrics/Snapshot;->getStdDev()D

    move-result-wide v4

    iput-wide v4, v0, Ltv/periscope/android/api/ChatStats;->latencyStdDev:D

    .line 160
    invoke-virtual {v2}, Lcom/codahale/metrics/Snapshot;->get95thPercentile()D

    move-result-wide v4

    iput-wide v4, v0, Ltv/periscope/android/api/ChatStats;->latencyP95:D

    .line 161
    invoke-virtual {v2}, Lcom/codahale/metrics/Snapshot;->get99thPercentile()D

    move-result-wide v2

    iput-wide v2, v0, Ltv/periscope/android/api/ChatStats;->latencyP99:D

    .line 165
    :goto_0
    return-object v0

    .line 150
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 165
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
