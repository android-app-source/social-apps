.class public abstract Ltv/periscope/android/chat/q;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:J

.field private final b:Ljava/math/BigInteger;

.field private final c:Ljava/math/BigInteger;

.field private d:J


# direct methods
.method protected constructor <init>(JLjava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-wide p1, p0, Ltv/periscope/android/chat/q;->a:J

    .line 20
    iput-object p3, p0, Ltv/periscope/android/chat/q;->b:Ljava/math/BigInteger;

    .line 21
    iput-object p4, p0, Ltv/periscope/android/chat/q;->c:Ljava/math/BigInteger;

    .line 22
    return-void
.end method


# virtual methods
.method abstract a()Ltv/periscope/model/chat/MessageType;
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 51
    iput-wide p1, p0, Ltv/periscope/android/chat/q;->d:J

    .line 52
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Ltv/periscope/android/chat/q;->a:J

    return-wide v0
.end method

.method public c()J
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Ltv/periscope/android/chat/q;->b:Ljava/math/BigInteger;

    .line 36
    if-eqz v0, :cond_0

    sget-object v1, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    .line 37
    invoke-virtual {v1, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v1

    const/4 v2, -0x1

    if-gt v1, v2, :cond_0

    .line 38
    invoke-static {v0}, Ldaf;->a(Ljava/math/BigInteger;)J

    move-result-wide v0

    .line 42
    :goto_0
    return-wide v0

    .line 39
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/chat/q;->c:Ljava/math/BigInteger;

    if-eqz v0, :cond_1

    .line 40
    iget-object v0, p0, Ltv/periscope/android/chat/q;->c:Ljava/math/BigInteger;

    invoke-static {v0}, Ldaf;->a(Ljava/math/BigInteger;)J

    move-result-wide v0

    goto :goto_0

    .line 42
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Ltv/periscope/android/chat/q;->d:J

    return-wide v0
.end method
