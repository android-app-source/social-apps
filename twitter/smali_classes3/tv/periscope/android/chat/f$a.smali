.class Ltv/periscope/android/chat/f$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/chatman/a$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/chat/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Lde/greenrobot/event/c;

.field private final b:Lcom/codahale/metrics/Histogram;


# direct methods
.method public constructor <init>(Lde/greenrobot/event/c;Lcom/codahale/metrics/Histogram;)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    iput-object p1, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    .line 174
    iput-object p2, p0, Ltv/periscope/android/chat/f$a;->b:Lcom/codahale/metrics/Histogram;

    .line 175
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/chat/ChatRoomEvent;->e:Ltv/periscope/android/chat/ChatRoomEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 180
    return-void
.end method

.method public a(Ltv/periscope/chatman/model/Ban;)V
    .locals 1
    .param p1    # Ltv/periscope/chatman/model/Ban;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 247
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 248
    return-void
.end method

.method public a(Ltv/periscope/chatman/model/Join;)V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    invoke-static {p1}, Ltv/periscope/model/chat/JoinEvent;->a(Ltv/periscope/chatman/model/Join;)Ltv/periscope/model/chat/JoinEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 253
    return-void
.end method

.method public a(Ltv/periscope/chatman/model/Leave;)V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 258
    return-void
.end method

.method public a(Ltv/periscope/chatman/model/Presence;)V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 243
    return-void
.end method

.method public a(Ltv/periscope/chatman/model/Roster;)V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 238
    return-void
.end method

.method public a(Ltv/periscope/chatman/model/j;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 214
    sget-object v0, Ldco;->a:Lcom/google/gson/e;

    invoke-virtual {p1}, Ltv/periscope/chatman/model/j;->b()Ljava/lang/String;

    move-result-object v1

    const-class v2, Ltv/periscope/android/api/PsMessage;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsMessage;

    .line 215
    invoke-virtual {v0, p1}, Ltv/periscope/android/api/PsMessage;->toMessage(Ltv/periscope/chatman/model/j;)Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 217
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->v()Ljava/math/BigInteger;

    move-result-object v1

    .line 218
    if-eqz v1, :cond_0

    .line 219
    invoke-static {}, Ldaf;->a()J

    move-result-wide v2

    .line 220
    cmp-long v4, v2, v6

    if-lez v4, :cond_0

    .line 221
    invoke-static {v1}, Ldaf;->a(Ljava/math/BigInteger;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 222
    const-string/jumbo v1, "CM"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "received message, latency="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    cmp-long v1, v2, v6

    if-lez v1, :cond_0

    .line 225
    iget-object v1, p0, Ltv/periscope/android/chat/f$a;->b:Lcom/codahale/metrics/Histogram;

    monitor-enter v1

    .line 226
    :try_start_0
    iget-object v4, p0, Ltv/periscope/android/chat/f$a;->b:Lcom/codahale/metrics/Histogram;

    invoke-virtual {v4, v2, v3}, Lcom/codahale/metrics/Histogram;->update(J)V

    .line 227
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    :cond_0
    iget-object v1, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    invoke-static {v0}, Ltv/periscope/model/chat/ChatEvent;->a(Ltv/periscope/model/chat/Message;)Ltv/periscope/model/chat/ChatEvent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 233
    return-void

    .line 227
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ltv/periscope/chatman/model/k;ZZ)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 262
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 264
    if-eqz p3, :cond_1

    new-instance v0, Ltv/periscope/android/chat/f$b;

    invoke-direct {v0, v1}, Ltv/periscope/android/chat/f$b;-><init>(Ltv/periscope/android/chat/f$1;)V

    move-object v1, v0

    .line 267
    :goto_0
    invoke-virtual {p1}, Ltv/periscope/chatman/model/k;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/chatman/model/l;

    .line 268
    invoke-interface {v1, v0}, Ltv/periscope/android/chat/f$d;->a(Ltv/periscope/chatman/model/l;)Ltv/periscope/android/chat/q;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 264
    :cond_1
    new-instance v0, Ltv/periscope/android/chat/f$c;

    invoke-direct {v0, v1}, Ltv/periscope/android/chat/f$c;-><init>(Ltv/periscope/android/chat/f$1;)V

    move-object v1, v0

    goto :goto_0

    .line 274
    :cond_2
    new-instance v1, Ltv/periscope/android/chat/EventHistory;

    .line 275
    invoke-virtual {p1}, Ltv/periscope/chatman/model/k;->b()J

    move-result-wide v4

    invoke-virtual {p1}, Ltv/periscope/chatman/model/k;->c()Ljava/lang/String;

    move-result-object v6

    .line 276
    invoke-virtual {p1}, Ltv/periscope/chatman/model/k;->d()Ljava/lang/String;

    move-result-object v7

    move v3, p2

    invoke-direct/range {v1 .. v7}, Ltv/periscope/android/chat/EventHistory;-><init>(Ljava/util/List;ZJLjava/lang/String;Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 278
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/chat/ChatRoomEvent;->a:Ltv/periscope/android/chat/ChatRoomEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 185
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/chat/ChatRoomEvent;->c:Ltv/periscope/android/chat/ChatRoomEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 190
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/chat/ChatRoomEvent;->b:Ltv/periscope/android/chat/ChatRoomEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 195
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/chat/ChatRoomEvent;->d:Ltv/periscope/android/chat/ChatRoomEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 200
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/chat/ChatRoomEvent;->f:Ltv/periscope/android/chat/ChatRoomEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 205
    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Ltv/periscope/android/chat/f$a;->a:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/chat/ChatRoomEvent;->g:Ltv/periscope/android/chat/ChatRoomEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 210
    return-void
.end method
