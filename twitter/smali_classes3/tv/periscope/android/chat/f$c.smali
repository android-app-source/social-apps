.class Ltv/periscope/android/chat/f$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/chat/f$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/chat/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ltv/periscope/android/chat/f$1;)V
    .locals 0

    .prologue
    .line 327
    invoke-direct {p0}, Ltv/periscope/android/chat/f$c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ltv/periscope/chatman/model/l;)Ltv/periscope/android/chat/q;
    .locals 6

    .prologue
    .line 333
    invoke-interface {p1}, Ltv/periscope/chatman/model/l;->d()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 334
    check-cast p1, Ltv/periscope/chatman/model/j;

    .line 335
    sget-object v0, Ldco;->a:Lcom/google/gson/e;

    invoke-virtual {p1}, Ltv/periscope/chatman/model/j;->b()Ljava/lang/String;

    move-result-object v1

    const-class v2, Ltv/periscope/android/api/PsMessage;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsMessage;

    .line 336
    invoke-virtual {v0, p1}, Ltv/periscope/android/api/PsMessage;->toMessage(Ltv/periscope/chatman/model/j;)Ltv/periscope/model/chat/Message;

    move-result-object v5

    .line 337
    invoke-virtual {v5}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->d:Ltv/periscope/model/chat/MessageType;

    if-ne v0, v1, :cond_0

    .line 339
    invoke-static {}, Ltv/periscope/chatman/model/Join;->c()Ltv/periscope/chatman/model/Join$a;

    move-result-object v0

    .line 340
    invoke-virtual {p1}, Ltv/periscope/chatman/model/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/chatman/model/Join$a;->a(Ljava/lang/String;)Ltv/periscope/chatman/model/Join$a;

    move-result-object v0

    .line 341
    invoke-virtual {p1}, Ltv/periscope/chatman/model/j;->c()Ltv/periscope/chatman/api/Sender;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/chatman/model/Join$a;->a(Ltv/periscope/chatman/api/Sender;)Ltv/periscope/chatman/model/Join$a;

    move-result-object v0

    .line 342
    invoke-virtual {v0}, Ltv/periscope/chatman/model/Join$a;->a()Ltv/periscope/chatman/model/Join;

    move-result-object v1

    .line 343
    new-instance v0, Ltv/periscope/android/chat/k;

    invoke-virtual {p1}, Ltv/periscope/chatman/model/j;->bz_()J

    move-result-wide v2

    invoke-virtual {v5}, Ltv/periscope/model/chat/Message;->v()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v5}, Ltv/periscope/model/chat/Message;->f()Ljava/math/BigInteger;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/chat/k;-><init>(Ltv/periscope/chatman/model/Join;JLjava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 348
    :goto_0
    return-object v0

    .line 345
    :cond_0
    new-instance v0, Ltv/periscope/android/chat/e;

    invoke-virtual {p1}, Ltv/periscope/chatman/model/j;->bz_()J

    move-result-wide v2

    invoke-direct {v0, v5, v2, v3}, Ltv/periscope/android/chat/e;-><init>(Ltv/periscope/model/chat/Message;J)V

    goto :goto_0

    .line 348
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
