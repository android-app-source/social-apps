.class Ltv/periscope/android/chat/m$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/chat/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ltv/periscope/model/chat/Message;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/Message;)I
    .locals 2

    .prologue
    .line 209
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->f()Ljava/math/BigInteger;

    move-result-object v0

    .line 210
    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->f()Ljava/math/BigInteger;

    move-result-object v1

    .line 211
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 212
    const/4 v0, 0x0

    .line 218
    :goto_0
    return v0

    .line 213
    :cond_0
    if-nez v0, :cond_1

    .line 214
    const/4 v0, -0x1

    goto :goto_0

    .line 215
    :cond_1
    if-nez v1, :cond_2

    .line 216
    const/4 v0, 0x1

    goto :goto_0

    .line 218
    :cond_2
    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/Message;)I
    .locals 1

    .prologue
    .line 205
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/chat/m$a;->b(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/Message;)I

    move-result v0

    return v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 202
    check-cast p1, Ltv/periscope/model/chat/Message;

    check-cast p2, Ltv/periscope/model/chat/Message;

    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/chat/m$a;->a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/Message;)I

    move-result v0

    return v0
.end method
