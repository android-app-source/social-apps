.class Ltv/periscope/android/chat/p;
.super Ltv/periscope/android/chat/i;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/chat/p$a;,
        Ltv/periscope/android/chat/p$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ltv/periscope/android/chat/i",
        "<",
        "Ltv/periscope/android/chat/q;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:J


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:Lde/greenrobot/event/c;

.field private final d:Lcyw;

.field private final e:Ltv/periscope/android/chat/f;

.field private final f:Z

.field private g:Ltv/periscope/android/player/e;

.field private h:Ltv/periscope/android/chat/r;

.field private volatile i:Z

.field private volatile j:I

.field private k:Ltv/periscope/android/chat/g;

.field private l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 39
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ltv/periscope/android/chat/p;->a:J

    return-void
.end method

.method public constructor <init>(Lde/greenrobot/event/c;Lcyw;Ltv/periscope/android/chat/f;Ltv/periscope/android/player/e;ZLtv/periscope/android/chat/i$a;Z)V
    .locals 2

    .prologue
    .line 66
    if-eqz p5, :cond_0

    new-instance v0, Ltv/periscope/android/chat/p$a;

    invoke-direct {v0}, Ltv/periscope/android/chat/p$a;-><init>()V

    :goto_0
    invoke-direct {p0, v0, p6, p7}, Ltv/periscope/android/chat/i;-><init>(Ljava/util/Comparator;Ltv/periscope/android/chat/i$a;Z)V

    .line 41
    new-instance v0, Landroid/os/Handler;

    .line 42
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ltv/periscope/android/chat/p;->b:Landroid/os/Handler;

    .line 67
    iput-object p1, p0, Ltv/periscope/android/chat/p;->c:Lde/greenrobot/event/c;

    .line 68
    iput-object p2, p0, Ltv/periscope/android/chat/p;->d:Lcyw;

    .line 69
    iput-object p3, p0, Ltv/periscope/android/chat/p;->e:Ltv/periscope/android/chat/f;

    .line 70
    iput-boolean p5, p0, Ltv/periscope/android/chat/p;->f:Z

    .line 72
    iput-object p4, p0, Ltv/periscope/android/chat/p;->g:Ltv/periscope/android/player/e;

    .line 73
    return-void

    .line 66
    :cond_0
    new-instance v0, Ltv/periscope/android/chat/p$b;

    invoke-direct {v0}, Ltv/periscope/android/chat/p$b;-><init>()V

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/chat/p;)Ltv/periscope/android/player/e;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Ltv/periscope/android/chat/p;->g:Ltv/periscope/android/player/e;

    return-object v0
.end method

.method private a(Ltv/periscope/android/chat/e;)V
    .locals 2

    .prologue
    .line 226
    sget-object v0, Ltv/periscope/android/chat/p$2;->a:[I

    invoke-virtual {p1}, Ltv/periscope/android/chat/e;->a()Ltv/periscope/model/chat/MessageType;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/chat/MessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 255
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 235
    :pswitch_1
    iget-object v0, p1, Ltv/periscope/android/chat/e;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Ltv/periscope/android/chat/e;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/chat/p;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    invoke-virtual {p0, p1}, Ltv/periscope/android/chat/p;->c(Ltv/periscope/android/chat/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Ltv/periscope/android/chat/p;->c:Lde/greenrobot/event/c;

    iget-object v1, p1, Ltv/periscope/android/chat/e;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    goto :goto_0

    .line 242
    :cond_1
    invoke-virtual {p0, p1}, Ltv/periscope/android/chat/p;->b(Ltv/periscope/android/chat/q;)V

    goto :goto_0

    .line 226
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(Ltv/periscope/android/chat/g;Ljava/lang/String;JLjava/lang/String;)V
    .locals 9

    .prologue
    .line 156
    iget-object v7, p0, Ltv/periscope/android/chat/p;->b:Landroid/os/Handler;

    new-instance v0, Ltv/periscope/android/chat/p$1;

    move-object v1, p0

    move-wide v2, p3

    move-object v4, p5

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/chat/p$1;-><init>(Ltv/periscope/android/chat/p;JLjava/lang/String;Ltv/periscope/android/chat/g;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 170
    return-void
.end method

.method private a(Ltv/periscope/android/chat/k;)V
    .locals 4

    .prologue
    .line 210
    iget v0, p0, Ltv/periscope/android/chat/p;->j:I

    const/16 v1, 0x14

    if-gt v0, v1, :cond_0

    invoke-virtual {p1}, Ltv/periscope/android/chat/k;->d()J

    move-result-wide v0

    sget-wide v2, Ltv/periscope/android/chat/p;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    iget-object v0, p1, Ltv/periscope/android/chat/k;->a:Ltv/periscope/chatman/model/Join;

    invoke-virtual {v0}, Ltv/periscope/chatman/model/Join;->b()Ltv/periscope/chatman/api/Sender;

    move-result-object v0

    .line 215
    iget-object v1, v0, Ltv/periscope/chatman/api/Sender;->userId:Ljava/lang/String;

    iget-object v0, v0, Ltv/periscope/chatman/api/Sender;->twitterId:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Ltv/periscope/android/chat/p;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 216
    iget-object v0, p0, Ltv/periscope/android/chat/p;->c:Lde/greenrobot/event/c;

    iget-object v1, p1, Ltv/periscope/android/chat/k;->a:Ltv/periscope/chatman/model/Join;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 217
    iget v0, p0, Ltv/periscope/android/chat/p;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltv/periscope/android/chat/p;->j:I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 222
    if-eqz p1, :cond_0

    iget-object v0, p0, Ltv/periscope/android/chat/p;->d:Lcyw;

    invoke-interface {v0, p1, p2}, Lcyw;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Ltv/periscope/android/chat/p;)Ltv/periscope/android/chat/f;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Ltv/periscope/android/chat/p;->e:Ltv/periscope/android/chat/f;

    return-object v0
.end method

.method private b(Ltv/periscope/android/chat/g;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 173
    if-eqz p1, :cond_0

    invoke-static {p2}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/chat/p;->h:Ltv/periscope/android/chat/r;

    invoke-virtual {v0}, Ltv/periscope/android/chat/r;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Ltv/periscope/android/chat/p;->h:Ltv/periscope/android/chat/r;

    invoke-virtual {v0}, Ltv/periscope/android/chat/r;->b()J

    move-result-wide v4

    iget-object v0, p0, Ltv/periscope/android/chat/p;->h:Ltv/periscope/android/chat/r;

    invoke-virtual {v0}, Ltv/periscope/android/chat/r;->c()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Ltv/periscope/android/chat/p;->a(Ltv/periscope/android/chat/g;Ljava/lang/String;JLjava/lang/String;)V

    .line 175
    iget-object v0, p0, Ltv/periscope/android/chat/p;->h:Ltv/periscope/android/chat/r;

    invoke-virtual {v0, v7, v7}, Ltv/periscope/android/chat/r;->a(ZZ)V

    .line 177
    :cond_0
    return-void
.end method

.method private d(Ltv/periscope/android/chat/q;)V
    .locals 1

    .prologue
    .line 202
    instance-of v0, p1, Ltv/periscope/android/chat/k;

    if-eqz v0, :cond_1

    .line 203
    check-cast p1, Ltv/periscope/android/chat/k;

    invoke-direct {p0, p1}, Ltv/periscope/android/chat/p;->a(Ltv/periscope/android/chat/k;)V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    instance-of v0, p1, Ltv/periscope/android/chat/e;

    if-eqz v0, :cond_0

    .line 205
    check-cast p1, Ltv/periscope/android/chat/e;

    invoke-direct {p0, p1}, Ltv/periscope/android/chat/p;->a(Ltv/periscope/android/chat/e;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/Map;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ltv/periscope/model/chat/MessageType$Throttle;",
            "Ltv/periscope/android/chat/j",
            "<",
            "Ltv/periscope/android/chat/q;",
            ">;>;",
            "Ljava/util/Map",
            "<",
            "Ltv/periscope/model/chat/MessageType$Throttle;",
            "Ltv/periscope/android/chat/j",
            "<",
            "Ltv/periscope/android/chat/q;",
            ">;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    const-wide/16 v2, 0x64

    .line 107
    iget-boolean v0, p0, Ltv/periscope/android/chat/p;->i:Z

    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {p0}, Ltv/periscope/android/chat/p;->f()V

    .line 111
    :cond_0
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/chat/j;

    .line 112
    iget-object v1, v0, Ltv/periscope/android/chat/j;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/chat/q;

    .line 113
    iget-object v0, v0, Ltv/periscope/android/chat/j;->b:Ltv/periscope/model/chat/MessageType$Throttle;

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/chat/p;->a(Ljava/lang/Object;Ltv/periscope/model/chat/MessageType$Throttle;)V

    goto :goto_0

    .line 118
    :cond_1
    monitor-enter p0

    .line 119
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/chat/p;->k:Ltv/periscope/android/chat/g;

    .line 120
    iget-object v1, p0, Ltv/periscope/android/chat/p;->l:Ljava/lang/String;

    .line 121
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    iget-object v4, p0, Ltv/periscope/android/chat/p;->g:Ltv/periscope/android/player/e;

    invoke-interface {v4, v0}, Ltv/periscope/android/player/e;->a(Ltv/periscope/android/chat/g;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 124
    invoke-direct {p0, v0, v1}, Ltv/periscope/android/chat/p;->b(Ltv/periscope/android/chat/g;Ljava/lang/String;)V

    .line 125
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/chat/j;

    .line 126
    iget-object v1, v0, Ltv/periscope/android/chat/j;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/chat/q;

    .line 127
    iget-object v5, v0, Ltv/periscope/android/chat/j;->b:Ltv/periscope/model/chat/MessageType$Throttle;

    invoke-virtual {p0, v5}, Ltv/periscope/android/chat/p;->a(Ltv/periscope/model/chat/MessageType$Throttle;)V

    .line 128
    if-eqz v1, :cond_5

    .line 129
    invoke-virtual {v0}, Ltv/periscope/android/chat/j;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 130
    invoke-direct {p0, v1}, Ltv/periscope/android/chat/p;->d(Ltv/periscope/android/chat/q;)V

    .line 131
    invoke-virtual {v0}, Ltv/periscope/android/chat/j;->c()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    :goto_2
    move-wide v2, v0

    .line 137
    goto :goto_1

    .line 121
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 133
    :cond_2
    iget-object v5, v0, Ltv/periscope/android/chat/j;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v5, v1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 134
    invoke-virtual {v0}, Ltv/periscope/android/chat/j;->b()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_2

    .line 139
    :cond_3
    const-wide/16 v2, 0x1f4

    .line 142
    :cond_4
    invoke-virtual {p0, v2, v3}, Ltv/periscope/android/chat/p;->a(J)V

    .line 143
    return-void

    :cond_5
    move-wide v0, v2

    goto :goto_2
.end method

.method public declared-synchronized a(Ltv/periscope/android/chat/g;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Ltv/periscope/android/chat/p;->k:Ltv/periscope/android/chat/g;

    .line 99
    iput-object p2, p0, Ltv/periscope/android/chat/p;->l:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ltv/periscope/android/chat/r;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Ltv/periscope/android/chat/p;->h:Ltv/periscope/android/chat/r;

    .line 77
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 26
    check-cast p1, Ltv/periscope/android/chat/q;

    invoke-virtual {p0, p1}, Ltv/periscope/android/chat/p;->a(Ltv/periscope/android/chat/q;)Z

    move-result v0

    return v0
.end method

.method public a(Ltv/periscope/android/chat/q;)Z
    .locals 2

    .prologue
    .line 147
    invoke-virtual {p1}, Ltv/periscope/android/chat/q;->a()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->b:Ltv/periscope/model/chat/MessageType;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Ltv/periscope/android/chat/q;->a()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->c:Ltv/periscope/model/chat/MessageType;

    if-eq v0, v1, :cond_0

    .line 149
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Ltv/periscope/android/chat/p;->c(Ltv/periscope/android/chat/q;)Z

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ltv/periscope/model/chat/MessageType$Throttle;Ljava/util/Queue;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 26
    check-cast p3, Ltv/periscope/android/chat/q;

    invoke-virtual {p0, p1, p2, p3}, Ltv/periscope/android/chat/p;->a(Ltv/periscope/model/chat/MessageType$Throttle;Ljava/util/Queue;Ltv/periscope/android/chat/q;)Z

    move-result v0

    return v0
.end method

.method public a(Ltv/periscope/model/chat/MessageType$Throttle;Ljava/util/Queue;Ltv/periscope/android/chat/q;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/model/chat/MessageType$Throttle;",
            "Ljava/util/Queue",
            "<",
            "Ltv/periscope/android/chat/q;",
            ">;",
            "Ltv/periscope/android/chat/q;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 181
    sget-object v1, Ltv/periscope/model/chat/MessageType$Throttle;->c:Ltv/periscope/model/chat/MessageType$Throttle;

    if-eq p1, v1, :cond_1

    sget-object v1, Ltv/periscope/model/chat/MessageType$Throttle;->b:Ltv/periscope/model/chat/MessageType$Throttle;

    if-eq p1, v1, :cond_1

    .line 194
    :cond_0
    :goto_0
    return v0

    .line 186
    :cond_1
    invoke-interface {p2}, Ljava/util/Queue;->size()I

    move-result v1

    iget v2, p1, Ltv/periscope/model/chat/MessageType$Throttle;->minQueueSizeForDrop:I

    if-le v1, v2, :cond_0

    .line 187
    invoke-virtual {p3}, Ltv/periscope/android/chat/q;->c()J

    move-result-wide v2

    iget-object v1, p0, Ltv/periscope/android/chat/p;->g:Ltv/periscope/android/player/e;

    invoke-interface {v1}, Ltv/periscope/android/player/e;->n()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 192
    iget-wide v4, p1, Ltv/periscope/model/chat/MessageType$Throttle;->deliveryThresholdMs:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-super {p0}, Ltv/periscope/android/chat/i;->b()V

    .line 83
    iget-object v0, p0, Ltv/periscope/android/chat/p;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 84
    iput-object v1, p0, Ltv/periscope/android/chat/p;->g:Ltv/periscope/android/player/e;

    .line 85
    iput-object v1, p0, Ltv/periscope/android/chat/p;->h:Ltv/periscope/android/chat/r;

    .line 86
    return-void
.end method

.method public b(Ltv/periscope/android/chat/q;)V
    .locals 1

    .prologue
    .line 198
    invoke-virtual {p1}, Ltv/periscope/android/chat/q;->a()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    iget-object v0, v0, Ltv/periscope/model/chat/MessageType;->throttle:Ltv/periscope/model/chat/MessageType$Throttle;

    invoke-virtual {p0, v0, p1}, Ltv/periscope/android/chat/p;->b(Ltv/periscope/model/chat/MessageType$Throttle;Ljava/lang/Object;)V

    .line 199
    return-void
.end method

.method protected c(Ltv/periscope/android/chat/q;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 259
    iget-boolean v2, p0, Ltv/periscope/android/chat/p;->f:Z

    if-eqz v2, :cond_2

    .line 260
    iget-object v2, p0, Ltv/periscope/android/chat/p;->g:Ltv/periscope/android/player/e;

    invoke-interface {v2}, Ltv/periscope/android/player/e;->n()J

    move-result-wide v2

    .line 261
    invoke-virtual {p1}, Ltv/periscope/android/chat/q;->c()J

    move-result-wide v4

    .line 263
    const-wide/16 v6, 0x0

    cmp-long v6, v6, v2

    if-eqz v6, :cond_1

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 265
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 263
    goto :goto_0

    .line 265
    :cond_2
    invoke-virtual {p1}, Ltv/periscope/android/chat/q;->d()J

    move-result-wide v2

    iget-object v4, p0, Ltv/periscope/android/chat/p;->g:Ltv/periscope/android/player/e;

    invoke-interface {v4}, Ltv/periscope/android/player/e;->l()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 271
    invoke-super {p0}, Ltv/periscope/android/chat/i;->d()V

    .line 272
    const/4 v0, 0x0

    iput v0, p0, Ltv/periscope/android/chat/p;->j:I

    .line 273
    iget-object v0, p0, Ltv/periscope/android/chat/p;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 274
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/chat/p;->i:Z

    .line 90
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/chat/p;->i:Z

    .line 94
    invoke-virtual {p0}, Ltv/periscope/android/chat/p;->g()V

    .line 95
    return-void
.end method
