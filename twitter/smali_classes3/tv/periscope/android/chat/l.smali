.class public Ltv/periscope/android/chat/l;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/chat/d;


# instance fields
.field private final a:Lde/greenrobot/event/c;

.field private final b:Lcyw;

.field private c:Ltv/periscope/android/chat/m;

.field private d:Ltv/periscope/android/chat/h;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lde/greenrobot/event/c;Lcyw;Ltv/periscope/android/player/e;Ltv/periscope/android/chat/h;Ltv/periscope/android/chat/i$a;Z)V
    .locals 6

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Ltv/periscope/android/chat/l;->a:Lde/greenrobot/event/c;

    .line 40
    iput-object p2, p0, Ltv/periscope/android/chat/l;->b:Lcyw;

    .line 41
    new-instance v0, Ltv/periscope/android/chat/m;

    iget-object v1, p0, Ltv/periscope/android/chat/l;->b:Lcyw;

    invoke-interface {v1}, Lcyw;->c()Ljava/lang/String;

    move-result-object v4

    move-object v1, p3

    move-object v2, p1

    move-object v3, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/chat/m;-><init>(Ltv/periscope/android/player/e;Lde/greenrobot/event/c;Ltv/periscope/android/chat/i$a;Ljava/lang/String;Z)V

    iput-object v0, p0, Ltv/periscope/android/chat/l;->c:Ltv/periscope/android/chat/m;

    .line 42
    iput-object p4, p0, Ltv/periscope/android/chat/l;->d:Ltv/periscope/android/chat/h;

    .line 43
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    iget-object v0, p0, Ltv/periscope/android/chat/l;->a:Lde/greenrobot/event/c;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 70
    iget-object v0, p0, Ltv/periscope/android/chat/l;->c:Ltv/periscope/android/chat/m;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Ltv/periscope/android/chat/l;->c:Ltv/periscope/android/chat/m;

    invoke-virtual {v0}, Ltv/periscope/android/chat/m;->c()V

    .line 72
    iput-object v1, p0, Ltv/periscope/android/chat/l;->c:Ltv/periscope/android/chat/m;

    .line 74
    :cond_0
    iput-object v1, p0, Ltv/periscope/android/chat/l;->d:Ltv/periscope/android/chat/h;

    .line 75
    return-void
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public a(Ltv/periscope/android/chat/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 85
    iput-object p3, p0, Ltv/periscope/android/chat/l;->e:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ltv/periscope/android/chat/l;->c:Ltv/periscope/android/chat/m;

    invoke-virtual {v0}, Ltv/periscope/android/chat/m;->start()V

    .line 48
    iget-object v0, p0, Ltv/periscope/android/chat/l;->a:Lde/greenrobot/event/c;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 49
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/chat/l;->c:Ltv/periscope/android/chat/m;

    invoke-virtual {v0}, Ltv/periscope/android/chat/m;->d()V

    .line 80
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/model/chat/ChatEvent;)V
    .locals 4

    .prologue
    .line 107
    invoke-virtual {p1}, Ltv/periscope/model/chat/ChatEvent;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 109
    iget-object v1, p0, Ltv/periscope/android/chat/l;->b:Lcyw;

    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcyw;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 110
    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v1

    iget-object v1, v1, Ltv/periscope/model/chat/MessageType;->controlType:Ltv/periscope/model/chat/MessageType$ControlType;

    sget-object v2, Ltv/periscope/model/chat/MessageType$ControlType;->c:Ltv/periscope/model/chat/MessageType$ControlType;

    if-eq v1, v2, :cond_0

    .line 111
    const-string/jumbo v1, "ChatQueue"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Dropping message from blocked user: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :goto_0
    :pswitch_0
    return-void

    .line 116
    :cond_0
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v1

    iget-object v1, v1, Ltv/periscope/model/chat/MessageType;->controlType:Ltv/periscope/model/chat/MessageType$ControlType;

    sget-object v2, Ltv/periscope/model/chat/MessageType$ControlType;->c:Ltv/periscope/model/chat/MessageType$ControlType;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Ltv/periscope/android/chat/l;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ltv/periscope/android/chat/l;->e:Ljava/lang/String;

    .line 117
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 118
    const-string/jumbo v1, "ChatQueue"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Received broadcast message from non-broadcaster: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 122
    :cond_1
    sget-object v1, Ltv/periscope/android/chat/l$1;->a:[I

    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v2

    invoke-virtual {v2}, Ltv/periscope/model/chat/MessageType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 144
    iget-object v1, p0, Ltv/periscope/android/chat/l;->c:Ltv/periscope/android/chat/m;

    invoke-virtual {v1, v0}, Ltv/periscope/android/chat/m;->c(Ltv/periscope/model/chat/Message;)V

    goto :goto_0

    .line 124
    :pswitch_1
    iget-object v1, p0, Ltv/periscope/android/chat/l;->c:Ltv/periscope/android/chat/m;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ltv/periscope/android/chat/m;->a(Z)V

    .line 125
    iget-object v1, p0, Ltv/periscope/android/chat/l;->c:Ltv/periscope/android/chat/m;

    invoke-virtual {v1, v0}, Ltv/periscope/android/chat/m;->c(Ltv/periscope/model/chat/Message;)V

    goto :goto_0

    .line 136
    :pswitch_2
    iget-object v1, p0, Ltv/periscope/android/chat/l;->c:Ltv/periscope/android/chat/m;

    invoke-virtual {v1, v0}, Ltv/periscope/android/chat/m;->d(Ltv/periscope/model/chat/Message;)V

    goto :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onEventMainThread(Ltv/periscope/model/chat/JoinEvent;)V
    .locals 6

    .prologue
    .line 98
    invoke-virtual {p1}, Ltv/periscope/model/chat/JoinEvent;->a()Ltv/periscope/chatman/model/Join;

    move-result-object v0

    .line 99
    iget-object v1, p0, Ltv/periscope/android/chat/l;->d:Ltv/periscope/android/chat/h;

    invoke-interface {v1}, Ltv/periscope/android/chat/h;->k()J

    move-result-wide v2

    const-wide/16 v4, 0x14

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    iget-object v1, p0, Ltv/periscope/android/chat/l;->b:Lcyw;

    .line 100
    invoke-virtual {v0}, Ltv/periscope/chatman/model/Join;->b()Ltv/periscope/chatman/api/Sender;

    move-result-object v2

    iget-object v2, v2, Ltv/periscope/chatman/api/Sender;->userId:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcyw;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 101
    :cond_0
    iget-object v1, p0, Ltv/periscope/android/chat/l;->a:Lde/greenrobot/event/c;

    invoke-virtual {v1, v0}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 103
    :cond_1
    return-void
.end method
