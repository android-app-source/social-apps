.class Ltv/periscope/android/chat/f$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/chat/f$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/chat/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private a:J

.field private b:Ljava/math/BigInteger;

.field private c:Ljava/math/BigInteger;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ltv/periscope/android/chat/f$1;)V
    .locals 0

    .prologue
    .line 287
    invoke-direct {p0}, Ltv/periscope/android/chat/f$b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ltv/periscope/chatman/model/l;)Ltv/periscope/android/chat/q;
    .locals 6

    .prologue
    .line 297
    invoke-interface {p1}, Ltv/periscope/chatman/model/l;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 323
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 299
    :pswitch_0
    check-cast p1, Ltv/periscope/chatman/model/j;

    .line 300
    sget-object v0, Ldco;->a:Lcom/google/gson/e;

    invoke-virtual {p1}, Ltv/periscope/chatman/model/j;->b()Ljava/lang/String;

    move-result-object v1

    const-class v2, Ltv/periscope/android/api/PsMessage;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsMessage;

    .line 301
    invoke-virtual {v0, p1}, Ltv/periscope/android/api/PsMessage;->toMessage(Ltv/periscope/chatman/model/j;)Ltv/periscope/model/chat/Message;

    move-result-object v1

    .line 302
    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->h()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Ltv/periscope/android/chat/f$b;->a:J

    .line 303
    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->v()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/chat/f$b;->b:Ljava/math/BigInteger;

    .line 304
    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->f()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/chat/f$b;->c:Ljava/math/BigInteger;

    .line 306
    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    sget-object v2, Ltv/periscope/model/chat/MessageType;->d:Ltv/periscope/model/chat/MessageType;

    if-eq v0, v2, :cond_0

    .line 307
    new-instance v0, Ltv/periscope/android/chat/e;

    invoke-virtual {p1}, Ltv/periscope/chatman/model/j;->bz_()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Ltv/periscope/android/chat/e;-><init>(Ltv/periscope/model/chat/Message;J)V

    goto :goto_0

    .line 312
    :pswitch_1
    instance-of v0, p1, Ltv/periscope/chatman/model/Join;

    if-eqz v0, :cond_0

    .line 315
    new-instance v0, Ltv/periscope/android/chat/k;

    move-object v1, p1

    check-cast v1, Ltv/periscope/chatman/model/Join;

    iget-wide v2, p0, Ltv/periscope/android/chat/f$b;->a:J

    iget-object v4, p0, Ltv/periscope/android/chat/f$b;->b:Ljava/math/BigInteger;

    iget-object v5, p0, Ltv/periscope/android/chat/f$b;->c:Ljava/math/BigInteger;

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/chat/k;-><init>(Ltv/periscope/chatman/model/Join;JLjava/math/BigInteger;Ljava/math/BigInteger;)V

    goto :goto_0

    .line 297
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
