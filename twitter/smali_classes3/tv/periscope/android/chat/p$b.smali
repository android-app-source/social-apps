.class Ltv/periscope/android/chat/p$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/chat/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ltv/periscope/android/chat/q;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ltv/periscope/android/chat/q;Ltv/periscope/android/chat/q;)I
    .locals 4

    .prologue
    .line 279
    invoke-virtual {p1}, Ltv/periscope/android/chat/q;->b()J

    move-result-wide v0

    invoke-virtual {p2}, Ltv/periscope/android/chat/q;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 280
    const/4 v0, -0x1

    .line 285
    :goto_0
    return v0

    .line 282
    :cond_0
    invoke-virtual {p1}, Ltv/periscope/android/chat/q;->b()J

    move-result-wide v0

    invoke-virtual {p2}, Ltv/periscope/android/chat/q;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 283
    const/4 v0, 0x1

    goto :goto_0

    .line 285
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 276
    check-cast p1, Ltv/periscope/android/chat/q;

    check-cast p2, Ltv/periscope/android/chat/q;

    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/chat/p$b;->a(Ltv/periscope/android/chat/q;Ltv/periscope/android/chat/q;)I

    move-result v0

    return v0
.end method
