.class public final Ltv/periscope/android/analytics/d;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static volatile a:Ltv/periscope/android/analytics/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Ltv/periscope/android/analytics/b;

    new-instance v1, Ltv/periscope/android/analytics/c;

    invoke-direct {v1}, Ltv/periscope/android/analytics/c;-><init>()V

    invoke-direct {v0, v1}, Ltv/periscope/android/analytics/b;-><init>(Ltv/periscope/android/analytics/e;)V

    sput-object v0, Ltv/periscope/android/analytics/d;->a:Ltv/periscope/android/analytics/a;

    return-void
.end method

.method public static a(Ltv/periscope/android/analytics/summary/SummaryType;Ljava/lang/String;)Lcrw;
    .locals 1

    .prologue
    .line 246
    invoke-static {}, Ltv/periscope/android/analytics/d;->b()Ltv/periscope/android/analytics/a;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Ltv/periscope/android/analytics/a;->a(Ltv/periscope/android/analytics/summary/SummaryType;Ljava/lang/String;)Lcrw;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lcrz;
    .locals 2

    .prologue
    .line 241
    sget-object v0, Ltv/periscope/android/analytics/summary/SummaryType;->b:Ltv/periscope/android/analytics/summary/SummaryType;

    const-string/jumbo v1, "CREATE_BROADCAST"

    invoke-static {v0, v1}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/summary/SummaryType;Ljava/lang/String;)Lcrw;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Ltv/periscope/android/analytics/d;->b()Ltv/periscope/android/analytics/a;

    move-result-object v0

    invoke-interface {v0, p0}, Ltv/periscope/android/analytics/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcrz;)V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Ltv/periscope/android/analytics/d;->b()Ltv/periscope/android/analytics/a;

    move-result-object v0

    invoke-interface {v0, p0}, Ltv/periscope/android/analytics/a;->a(Lcrz;)V

    .line 58
    return-void
.end method

.method public static a(Ltv/periscope/android/analytics/Event;)V
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Ltv/periscope/android/analytics/d;->b()Ltv/periscope/android/analytics/a;

    move-result-object v0

    invoke-interface {v0, p0}, Ltv/periscope/android/analytics/a;->a(Ltv/periscope/android/analytics/Event;)V

    .line 46
    return-void
.end method

.method public static a(Ltv/periscope/android/analytics/Event;Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/android/analytics/Event;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-static {}, Ltv/periscope/android/analytics/d;->b()Ltv/periscope/android/analytics/a;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Ltv/periscope/android/analytics/a;->a(Ltv/periscope/android/analytics/Event;Ljava/util/HashMap;)V

    .line 54
    return-void
.end method

.method public static a(Ltv/periscope/android/analytics/summary/b;)V
    .locals 0

    .prologue
    .line 165
    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->o()V

    .line 166
    return-void
.end method

.method public static a(Ltv/periscope/android/analytics/summary/b;J)V
    .locals 1

    .prologue
    .line 157
    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/analytics/summary/b;->a(J)V

    .line 158
    return-void
.end method

.method public static a(Ltv/periscope/android/analytics/summary/b;Ltv/periscope/model/af;)V
    .locals 0

    .prologue
    .line 161
    invoke-virtual {p0, p1}, Ltv/periscope/android/analytics/summary/b;->a(Ltv/periscope/model/af;)V

    .line 162
    return-void
.end method

.method public static a(Ltv/periscope/android/analytics/summary/b;Ltv/periscope/model/af;J)V
    .locals 0

    .prologue
    .line 202
    invoke-virtual {p0, p1, p2, p3}, Ltv/periscope/android/analytics/summary/b;->a(Ltv/periscope/model/af;J)V

    .line 203
    return-void
.end method

.method public static a(Ltv/periscope/android/analytics/summary/c;)V
    .locals 0

    .prologue
    .line 218
    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/c;->l()V

    .line 219
    return-void
.end method

.method public static a(Ltv/periscope/android/analytics/summary/c;Ljava/lang/String;Lcyd;ZLtv/periscope/android/session/Session;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 214
    invoke-virtual/range {p0 .. p6}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Lcyd;ZLtv/periscope/android/session/Session;ZLjava/lang/String;)V

    .line 215
    return-void
.end method

.method public static a(Ltv/periscope/android/analytics/summary/c;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 231
    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/analytics/summary/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    return-void
.end method

.method public static a(Ltv/periscope/android/analytics/summary/c;ZZ)V
    .locals 0

    .prologue
    .line 226
    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/analytics/summary/c;->a(ZZ)V

    .line 227
    return-void
.end method

.method public static a(Z)V
    .locals 3

    .prologue
    .line 206
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 207
    const-string/jumbo v2, "Opt Out"

    if-eqz p0, :cond_0

    const-string/jumbo v0, "YES"

    :goto_0
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Ltv/periscope/android/analytics/Event;->aO:Ltv/periscope/android/analytics/Event;

    invoke-static {v0, v1}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/Event;Ljava/util/HashMap;)V

    .line 209
    return-void

    .line 207
    :cond_0
    const-string/jumbo v0, "NO"

    goto :goto_0
.end method

.method private static b()Ltv/periscope/android/analytics/a;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Ltv/periscope/android/analytics/d;->a:Ltv/periscope/android/analytics/a;

    return-object v0
.end method

.method public static b(Lcrz;)V
    .locals 1

    .prologue
    .line 69
    instance-of v0, p0, Ltv/periscope/android/analytics/summary/a;

    if-eqz v0, :cond_0

    .line 70
    check-cast p0, Ltv/periscope/android/analytics/summary/a;

    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/a;->i()V

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-static {}, Ltv/periscope/android/analytics/d;->c()V

    goto :goto_0
.end method

.method public static b(Ltv/periscope/android/analytics/summary/b;)V
    .locals 0

    .prologue
    .line 169
    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->s()V

    .line 170
    return-void
.end method

.method public static b(Ltv/periscope/android/analytics/summary/c;)V
    .locals 0

    .prologue
    .line 222
    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/c;->m()V

    .line 223
    return-void
.end method

.method private static c()V
    .locals 0

    .prologue
    .line 263
    return-void
.end method

.method public static c(Lcrz;)V
    .locals 1

    .prologue
    .line 77
    instance-of v0, p0, Ltv/periscope/android/analytics/summary/a;

    if-eqz v0, :cond_0

    .line 78
    check-cast p0, Ltv/periscope/android/analytics/summary/a;

    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/a;->j()V

    .line 82
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-static {}, Ltv/periscope/android/analytics/d;->c()V

    goto :goto_0
.end method

.method public static c(Ltv/periscope/android/analytics/summary/b;)V
    .locals 0

    .prologue
    .line 173
    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->q()V

    .line 174
    return-void
.end method

.method public static d(Lcrz;)V
    .locals 1

    .prologue
    .line 85
    instance-of v0, p0, Ltv/periscope/android/analytics/summary/a;

    if-eqz v0, :cond_0

    .line 86
    check-cast p0, Ltv/periscope/android/analytics/summary/a;

    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/a;->k()V

    .line 90
    :goto_0
    return-void

    .line 88
    :cond_0
    invoke-static {}, Ltv/periscope/android/analytics/d;->c()V

    goto :goto_0
.end method

.method public static d(Ltv/periscope/android/analytics/summary/b;)V
    .locals 0

    .prologue
    .line 177
    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->p()V

    .line 178
    return-void
.end method

.method public static e(Lcrz;)V
    .locals 1

    .prologue
    .line 93
    instance-of v0, p0, Ltv/periscope/android/analytics/summary/b;

    if-eqz v0, :cond_0

    .line 94
    check-cast p0, Ltv/periscope/android/analytics/summary/b;

    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->y()V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-static {}, Ltv/periscope/android/analytics/d;->c()V

    goto :goto_0
.end method

.method public static e(Ltv/periscope/android/analytics/summary/b;)V
    .locals 0

    .prologue
    .line 181
    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->n()V

    .line 182
    return-void
.end method

.method public static f(Lcrz;)V
    .locals 1

    .prologue
    .line 101
    instance-of v0, p0, Ltv/periscope/android/analytics/summary/a;

    if-eqz v0, :cond_0

    .line 102
    check-cast p0, Ltv/periscope/android/analytics/summary/a;

    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/a;->h()V

    .line 106
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-static {}, Ltv/periscope/android/analytics/d;->c()V

    goto :goto_0
.end method

.method public static f(Ltv/periscope/android/analytics/summary/b;)V
    .locals 1

    .prologue
    .line 185
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->i(Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public static g(Lcrz;)V
    .locals 1

    .prologue
    .line 109
    instance-of v0, p0, Ltv/periscope/android/analytics/summary/b;

    if-eqz v0, :cond_0

    .line 110
    check-cast p0, Ltv/periscope/android/analytics/summary/b;

    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->z()V

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_0
    invoke-static {}, Ltv/periscope/android/analytics/d;->c()V

    goto :goto_0
.end method

.method public static g(Ltv/periscope/android/analytics/summary/b;)V
    .locals 0

    .prologue
    .line 189
    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->m()V

    .line 190
    return-void
.end method

.method public static h(Lcrz;)V
    .locals 1

    .prologue
    .line 117
    instance-of v0, p0, Ltv/periscope/android/analytics/summary/b;

    if-eqz v0, :cond_0

    .line 118
    check-cast p0, Ltv/periscope/android/analytics/summary/b;

    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->x()V

    .line 124
    :goto_0
    return-void

    .line 119
    :cond_0
    instance-of v0, p0, Ltv/periscope/android/analytics/summary/c;

    if-eqz v0, :cond_1

    .line 120
    check-cast p0, Ltv/periscope/android/analytics/summary/c;

    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/c;->n()V

    goto :goto_0

    .line 122
    :cond_1
    invoke-static {}, Ltv/periscope/android/analytics/d;->c()V

    goto :goto_0
.end method

.method public static h(Ltv/periscope/android/analytics/summary/b;)V
    .locals 0

    .prologue
    .line 193
    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->l()V

    .line 194
    return-void
.end method

.method public static i(Lcrz;)V
    .locals 1

    .prologue
    .line 127
    instance-of v0, p0, Ltv/periscope/android/analytics/summary/b;

    if-eqz v0, :cond_0

    .line 128
    check-cast p0, Ltv/periscope/android/analytics/summary/b;

    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->t()V

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-static {}, Ltv/periscope/android/analytics/d;->c()V

    goto :goto_0
.end method

.method public static i(Ltv/periscope/android/analytics/summary/b;)V
    .locals 0

    .prologue
    .line 197
    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->r()V

    .line 198
    return-void
.end method

.method public static j(Lcrz;)V
    .locals 1

    .prologue
    .line 135
    instance-of v0, p0, Ltv/periscope/android/analytics/summary/b;

    if-eqz v0, :cond_0

    .line 136
    check-cast p0, Ltv/periscope/android/analytics/summary/b;

    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->u()V

    .line 138
    :cond_0
    return-void
.end method

.method public static k(Lcrz;)V
    .locals 1

    .prologue
    .line 141
    instance-of v0, p0, Ltv/periscope/android/analytics/summary/b;

    if-eqz v0, :cond_0

    .line 142
    check-cast p0, Ltv/periscope/android/analytics/summary/b;

    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->v()V

    .line 146
    :goto_0
    return-void

    .line 144
    :cond_0
    invoke-static {}, Ltv/periscope/android/analytics/d;->c()V

    goto :goto_0
.end method

.method public static l(Lcrz;)V
    .locals 1

    .prologue
    .line 149
    instance-of v0, p0, Ltv/periscope/android/analytics/summary/b;

    if-eqz v0, :cond_0

    .line 150
    check-cast p0, Ltv/periscope/android/analytics/summary/b;

    invoke-virtual {p0}, Ltv/periscope/android/analytics/summary/b;->w()V

    .line 154
    :goto_0
    return-void

    .line 152
    :cond_0
    invoke-static {}, Ltv/periscope/android/analytics/d;->c()V

    goto :goto_0
.end method
