.class public Ltv/periscope/android/analytics/summary/b;
.super Ltv/periscope/android/analytics/summary/a;
.source "Twttr"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, -0x1

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    invoke-direct {p0, p1}, Ltv/periscope/android/analytics/summary/a;-><init>(Ljava/lang/String;)V

    .line 64
    const-string/jumbo v0, "Summary.State"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string/jumbo v0, "Summary.BroadcasterFollowed"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string/jumbo v0, "Summary.LiveViewersAtJoin"

    invoke-virtual {p0, v0, v2}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;I)V

    .line 67
    const-string/jumbo v0, "Summary.LiveViewersAtExit"

    invoke-virtual {p0, v0, v2}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;I)V

    .line 69
    new-instance v0, Lcry;

    const-string/jumbo v1, "Summary.TimeWatched"

    invoke-direct {v0, v1}, Lcry;-><init>(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->a(Lcry;)V

    .line 72
    const-string/jumbo v0, "Summary.ReplayDuration"

    invoke-virtual {p0, v0, v2}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;I)V

    .line 73
    const-string/jumbo v0, "Summary.ReplayTimeWatched"

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;F)V

    .line 74
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "Summary.ReplayPauses"

    aput-object v1, v0, v3

    const-string/jumbo v1, "Summary.ReplayScrubs"

    aput-object v1, v0, v4

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->b([Ljava/lang/String;)V

    .line 77
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Chat.ChatCommentTapped"

    aput-object v1, v0, v3

    const-string/jumbo v1, "Chat.ChatCommentReplied"

    aput-object v1, v0, v4

    const-string/jumbo v1, "Chat.ChatCommentViewProfile"

    aput-object v1, v0, v5

    const-string/jumbo v1, "Chat.ChatCommentBlockUser"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string/jumbo v2, "Chat.ChatCommentCancel"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->b([Ljava/lang/String;)V

    .line 80
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Chat.Hearts"

    aput-object v1, v0, v3

    const-string/jumbo v1, "Chat.Comments"

    aput-object v1, v0, v4

    const-string/jumbo v1, "Chat.Screenshots"

    aput-object v1, v0, v5

    const-string/jumbo v1, "Chat.ScreenshotTwitterPrompt"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string/jumbo v2, "Chat.ScreenshotTwitterPost"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->b([Ljava/lang/String;)V

    .line 81
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, "Chat.FollowBroadcaster"

    aput-object v1, v0, v3

    const-string/jumbo v1, "Chat.PromptedToFollowBroadcaster"

    aput-object v1, v0, v4

    const-string/jumbo v1, "Chat.Share"

    aput-object v1, v0, v5

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->a([Ljava/lang/String;)V

    .line 83
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "InfoPanel.SwipeIn"

    aput-object v1, v0, v3

    const-string/jumbo v1, "InfoPanel.BroadcasterProfileViewed"

    aput-object v1, v0, v4

    const-string/jumbo v1, "InfoPanel.MapZoom"

    aput-object v1, v0, v5

    const-string/jumbo v1, "InfoPanel.ShareTwitter"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string/jumbo v2, "InfoPanel.ShareAllFollowers"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "InfoPanel.ShareSpecificFollowers"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "InfoPanel.ShareCopyLink"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "InfoPanel.ShareFacebook"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "InfoPanel.ShareMutuals"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "InfoPanel.HideChat"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "InfoPanel.Reported"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "InfoPanel.LiveViewerProfileViewed"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "InfoPanel.ReplayViewerProfileViewed"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->b([Ljava/lang/String;)V

    .line 87
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "InfoPanel.LocationAvailable"

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->a([Ljava/lang/String;)V

    .line 88
    return-void
.end method


# virtual methods
.method public A()V
    .locals 2

    .prologue
    .line 194
    const-string/jumbo v0, "Chat.PromptedToFollowBroadcaster"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;Z)V

    .line 195
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 114
    const-string/jumbo v0, "Summary.LiveViewersAtJoin"

    invoke-virtual {p0, v0, p1, p2}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;J)V

    .line 115
    return-void
.end method

.method public a(Ltv/periscope/model/af;)V
    .locals 2
    .param p1    # Ltv/periscope/model/af;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 102
    const-string/jumbo v0, "Summary.BroadcasterUserID"

    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string/jumbo v0, "Summary.BroadcastID"

    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    const-string/jumbo v0, "Summary.State"

    const-string/jumbo v1, "Live"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :goto_0
    const-string/jumbo v1, "Summary.Type"

    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/p;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "Private"

    :goto_1
    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void

    .line 107
    :cond_0
    const-string/jumbo v0, "Summary.State"

    const-string/jumbo v1, "Replay"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :cond_1
    const-string/jumbo v0, "Public"

    goto :goto_1
.end method

.method public a(Ltv/periscope/model/af;J)V
    .locals 4
    .param p1    # Ltv/periscope/model/af;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 118
    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const-string/jumbo v0, "Summary.LiveViewersAtExit"

    invoke-virtual {p0, v0, p2, p3}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;J)V

    .line 124
    :goto_0
    return-void

    .line 121
    :cond_0
    const-string/jumbo v0, "Summary.ReplayDuration"

    invoke-virtual {p1}, Ltv/periscope/model/af;->g()Ltv/periscope/model/p;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2}, Ltv/periscope/model/p;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;J)V

    .line 122
    const-string/jumbo v0, "Summary.ReplayTimeWatched"

    const-string/jumbo v1, "Summary.TimeWatched"

    invoke-virtual {p0, v1}, Ltv/periscope/android/analytics/summary/b;->e(Ljava/lang/String;)Lcry;

    move-result-object v1

    invoke-virtual {v1}, Lcry;->e()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Ltv/periscope/android/analytics/summary/b;->a(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 93
    const-string/jumbo v0, "Broadcast Viewed"

    return-object v0
.end method

.method public i(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 139
    const-string/jumbo v0, "Chat.Comments"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 140
    invoke-static {p1}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    .line 141
    const-string/jumbo v0, "Chat.ChatCommentReplied"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 143
    :cond_0
    return-void
.end method

.method public l()V
    .locals 1

    .prologue
    .line 127
    const-string/jumbo v0, "Summary.ReplayScrubs"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 131
    const-string/jumbo v0, "Chat.Hearts"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    .line 135
    const-string/jumbo v0, "Chat.Screenshots"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method public o()V
    .locals 1

    .prologue
    .line 146
    const-string/jumbo v0, "Summary.ReplayPauses"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 147
    return-void
.end method

.method public p()V
    .locals 1

    .prologue
    .line 150
    const-string/jumbo v0, "Chat.ScreenshotTwitterPrompt"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method public q()V
    .locals 1

    .prologue
    .line 154
    const-string/jumbo v0, "Chat.ScreenshotTwitterPost"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 155
    return-void
.end method

.method public r()V
    .locals 1

    .prologue
    .line 158
    const-string/jumbo v0, "InfoPanel.SwipeIn"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method public s()V
    .locals 1

    .prologue
    .line 162
    const-string/jumbo v0, "InfoPanel.BroadcasterProfileViewed"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 163
    return-void
.end method

.method public t()V
    .locals 1

    .prologue
    .line 166
    const-string/jumbo v0, "InfoPanel.ShareTwitter"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 167
    return-void
.end method

.method public u()V
    .locals 1

    .prologue
    .line 170
    const-string/jumbo v0, "InfoPanel.ShareAllFollowers"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method public v()V
    .locals 1

    .prologue
    .line 174
    const-string/jumbo v0, "InfoPanel.ShareSpecificFollowers"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method public w()V
    .locals 1

    .prologue
    .line 178
    const-string/jumbo v0, "InfoPanel.ShareMutuals"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 179
    return-void
.end method

.method public x()V
    .locals 1

    .prologue
    .line 182
    const-string/jumbo v0, "InfoPanel.HideChat"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method public y()V
    .locals 1

    .prologue
    .line 186
    const-string/jumbo v0, "InfoPanel.Reported"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 187
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 190
    const-string/jumbo v0, "InfoPanel.LocationAvailable"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/b;->d(Ljava/lang/String;)V

    .line 191
    return-void
.end method
