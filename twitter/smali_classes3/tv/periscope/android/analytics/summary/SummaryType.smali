.class public final enum Ltv/periscope/android/analytics/summary/SummaryType;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/android/analytics/summary/SummaryType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltv/periscope/android/analytics/summary/SummaryType;

.field public static final enum b:Ltv/periscope/android/analytics/summary/SummaryType;

.field public static final enum c:Ltv/periscope/android/analytics/summary/SummaryType;

.field public static final enum d:Ltv/periscope/android/analytics/summary/SummaryType;

.field private static final synthetic e:[Ltv/periscope/android/analytics/summary/SummaryType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Ltv/periscope/android/analytics/summary/SummaryType;

    const-string/jumbo v1, "BroadcastViewedSummary"

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/analytics/summary/SummaryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/analytics/summary/SummaryType;->a:Ltv/periscope/android/analytics/summary/SummaryType;

    .line 5
    new-instance v0, Ltv/periscope/android/analytics/summary/SummaryType;

    const-string/jumbo v1, "CreateBroadcastSummary"

    invoke-direct {v0, v1, v3}, Ltv/periscope/android/analytics/summary/SummaryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/analytics/summary/SummaryType;->b:Ltv/periscope/android/analytics/summary/SummaryType;

    .line 6
    new-instance v0, Ltv/periscope/android/analytics/summary/SummaryType;

    const-string/jumbo v1, "GlobalChannelCellViewedSummary"

    invoke-direct {v0, v1, v4}, Ltv/periscope/android/analytics/summary/SummaryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/analytics/summary/SummaryType;->c:Ltv/periscope/android/analytics/summary/SummaryType;

    .line 7
    new-instance v0, Ltv/periscope/android/analytics/summary/SummaryType;

    const-string/jumbo v1, "HighlightsViewedSummary"

    invoke-direct {v0, v1, v5}, Ltv/periscope/android/analytics/summary/SummaryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/analytics/summary/SummaryType;->d:Ltv/periscope/android/analytics/summary/SummaryType;

    .line 3
    const/4 v0, 0x4

    new-array v0, v0, [Ltv/periscope/android/analytics/summary/SummaryType;

    sget-object v1, Ltv/periscope/android/analytics/summary/SummaryType;->a:Ltv/periscope/android/analytics/summary/SummaryType;

    aput-object v1, v0, v2

    sget-object v1, Ltv/periscope/android/analytics/summary/SummaryType;->b:Ltv/periscope/android/analytics/summary/SummaryType;

    aput-object v1, v0, v3

    sget-object v1, Ltv/periscope/android/analytics/summary/SummaryType;->c:Ltv/periscope/android/analytics/summary/SummaryType;

    aput-object v1, v0, v4

    sget-object v1, Ltv/periscope/android/analytics/summary/SummaryType;->d:Ltv/periscope/android/analytics/summary/SummaryType;

    aput-object v1, v0, v5

    sput-object v0, Ltv/periscope/android/analytics/summary/SummaryType;->e:[Ltv/periscope/android/analytics/summary/SummaryType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/android/analytics/summary/SummaryType;
    .locals 1

    .prologue
    .line 3
    const-class v0, Ltv/periscope/android/analytics/summary/SummaryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/analytics/summary/SummaryType;

    return-object v0
.end method

.method public static values()[Ltv/periscope/android/analytics/summary/SummaryType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Ltv/periscope/android/analytics/summary/SummaryType;->e:[Ltv/periscope/android/analytics/summary/SummaryType;

    invoke-virtual {v0}, [Ltv/periscope/android/analytics/summary/SummaryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/android/analytics/summary/SummaryType;

    return-object v0
.end method
