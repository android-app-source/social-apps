.class public abstract Ltv/periscope/android/analytics/summary/a;
.super Lcrw;
.source "Twttr"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcrw;-><init>(Ljava/lang/String;)V

    .line 36
    const-string/jumbo v0, "Summary.BroadcastID"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    const-string/jumbo v0, "Summary.BroadcasterUserID"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string/jumbo v0, "Summary.BroadcastTipViewed"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method


# virtual methods
.method public g()V
    .locals 1

    .prologue
    .line 46
    const-string/jumbo v0, "BroadcasterAction.ViewerShareClick"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/a;->d(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 50
    const-string/jumbo v0, "Chat.ChatCommentTapped"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/a;->d(Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    const-string/jumbo v0, "Summary.BroadcastTipViewed"

    invoke-virtual {p0, v0, p1}, Ltv/periscope/android/analytics/summary/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 54
    const-string/jumbo v0, "Chat.ChatCommentViewProfile"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/a;->d(Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 58
    const-string/jumbo v0, "Chat.ChatCommentBlockUser"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/a;->d(Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 62
    const-string/jumbo v0, "Chat.ChatCommentCancel"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/a;->d(Ljava/lang/String;)V

    .line 63
    return-void
.end method
