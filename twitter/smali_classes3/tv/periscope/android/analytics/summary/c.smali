.class public Ltv/periscope/android/analytics/summary/c;
.super Ltv/periscope/android/analytics/summary/a;
.source "Twttr"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 73
    invoke-direct {p0, p1}, Ltv/periscope/android/analytics/summary/a;-><init>(Ljava/lang/String;)V

    .line 76
    const-string/jumbo v0, "PreBroadcast.Title"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string/jumbo v0, "PreBroadcast.Cancelled"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/c;->c(Ljava/lang/String;)V

    .line 78
    const-string/jumbo v0, "PreBroadcast.Location"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/c;->c(Ljava/lang/String;)V

    .line 79
    const-string/jumbo v0, "PreBroadcast.Type"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string/jumbo v0, "PreBroadcast.Mutuals"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string/jumbo v0, "PreBroadcast.MutualsNumber"

    invoke-virtual {p0, v0, v3}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;I)V

    .line 82
    const-string/jumbo v0, "PreBroadcast.ChatSetting"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string/jumbo v0, "PreBroadcast.LoginType"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string/jumbo v0, "PreBroadcast.DeeplinkSource"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string/jumbo v0, "PreBroadcast.BroadcastTipViewed"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, "PreBroadcast.Twitter"

    aput-object v1, v0, v4

    const-string/jumbo v1, "PreBroadcast.StartBroadcast"

    aput-object v1, v0, v5

    const-string/jumbo v1, "PreBroadcast.Verified"

    aput-object v1, v0, v6

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/c;->a([Ljava/lang/String;)V

    .line 91
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Broadcast.CameraFlip"

    aput-object v1, v0, v4

    const-string/jumbo v1, "Broadcast.AudioMuted"

    aput-object v1, v0, v5

    const-string/jumbo v1, "Broadcast.HideChat"

    aput-object v1, v0, v6

    const-string/jumbo v1, "Broadcast.ViewerBlock"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "Broadcast.ViewerProfileViewed"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "Broadcast.ViewerFollowed"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "Broadcast.ViewerCancel"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "Broadcast.StopBroadcast"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/c;->b([Ljava/lang/String;)V

    .line 102
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "PostBroadcast.SaveCameraRoll"

    aput-object v1, v0, v4

    const-string/jumbo v1, "PostBroadcast.PlayReplay"

    aput-object v1, v0, v5

    const-string/jumbo v1, "PostBroadcast.DeleteReplay"

    aput-object v1, v0, v6

    const-string/jumbo v1, "PostBroadcast.DeleteBroadcast"

    aput-object v1, v0, v7

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/c;->a([Ljava/lang/String;)V

    .line 107
    const-string/jumbo v0, "Metrics.TotalLiveViewers"

    invoke-virtual {p0, v0, v3}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;I)V

    .line 108
    const-string/jumbo v0, "Metrics.TotalLiveAppViewers"

    invoke-virtual {p0, v0, v3}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;I)V

    .line 109
    const-string/jumbo v0, "Metrics.TotalLiveWebViewers"

    invoke-virtual {p0, v0, v3}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;I)V

    .line 110
    const-string/jumbo v0, "Metrics.TotalLiveTvViewers"

    invoke-virtual {p0, v0, v3}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;I)V

    .line 111
    const-string/jumbo v0, "Metrics.TotalLiveTwitterViewers"

    invoke-virtual {p0, v0, v3}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;I)V

    .line 112
    const-string/jumbo v0, "Metrics.TotalReplayViewers"

    invoke-virtual {p0, v0, v3}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;I)V

    .line 113
    const-string/jumbo v0, "Metrics.Retention"

    invoke-virtual {p0, v0, v3}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;I)V

    .line 114
    const-string/jumbo v0, "Metrics.TimeWatched(sec)"

    invoke-virtual {p0, v0, v3}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;I)V

    .line 115
    const-string/jumbo v0, "Metrics.Duration(sec)"

    invoke-virtual {p0, v0, v3}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;I)V

    .line 116
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcyd;ZLtv/periscope/android/session/Session;ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 136
    const-string/jumbo v0, "PreBroadcast.Title"

    invoke-virtual {p0, v0, p1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string/jumbo v0, "PreBroadcast.Location"

    invoke-interface {p2}, Lcyd;->a()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Z)V

    .line 138
    const-string/jumbo v1, "PreBroadcast.Type"

    invoke-interface {p2}, Lcyd;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "Group"

    :goto_0
    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-interface {p2}, Lcyd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const-string/jumbo v1, "PreBroadcast.Mutuals"

    invoke-interface {p2}, Lcyd;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "All"

    :goto_1
    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_0
    const-string/jumbo v0, "PreBroadcast.MutualsNumber"

    invoke-interface {p2}, Lcyd;->e()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;I)V

    .line 146
    const-string/jumbo v1, "PreBroadcast.ChatSetting"

    invoke-interface {p2}, Lcyd;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "All"

    :goto_2
    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string/jumbo v0, "PreBroadcast.Twitter"

    invoke-interface {p2}, Lcyd;->h()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Z)V

    .line 149
    const-string/jumbo v0, "PreBroadcast.BroadcastTipViewed"

    invoke-interface {p2}, Lcyd;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string/jumbo v0, "PreBroadcast.StartBroadcast"

    invoke-virtual {p0, v0, p3}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Z)V

    .line 152
    const-string/jumbo v0, "Unknown"

    .line 153
    if-eqz p4, :cond_1

    .line 154
    invoke-virtual {p4}, Ltv/periscope/android/session/Session;->b()Ltv/periscope/android/session/Session$Type;

    move-result-object v1

    sget-object v2, Ltv/periscope/android/session/Session$Type;->b:Ltv/periscope/android/session/Session$Type;

    if-ne v1, v2, :cond_6

    .line 155
    const-string/jumbo v0, "Twitter"

    .line 160
    :cond_1
    :goto_3
    const-string/jumbo v1, "PreBroadcast.LoginType"

    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string/jumbo v0, "PreBroadcast.Verified"

    invoke-virtual {p0, v0, p5}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Z)V

    .line 162
    const-string/jumbo v0, "PreBroadcast.DeeplinkSource"

    invoke-virtual {p0, v0, p6}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    return-void

    .line 139
    :cond_2
    invoke-interface {p2}, Lcyd;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "Private"

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "Public"

    goto :goto_0

    .line 142
    :cond_4
    const-string/jumbo v0, "Individuals"

    goto :goto_1

    .line 146
    :cond_5
    const-string/jumbo v0, "Follower-Only"

    goto :goto_2

    .line 156
    :cond_6
    invoke-virtual {p4}, Ltv/periscope/android/session/Session;->b()Ltv/periscope/android/session/Session$Type;

    move-result-object v1

    sget-object v2, Ltv/periscope/android/session/Session$Type;->c:Ltv/periscope/android/session/Session$Type;

    if-ne v1, v2, :cond_1

    .line 157
    const-string/jumbo v0, "Digits"

    goto :goto_3
.end method

.method public a(ZZ)V
    .locals 2

    .prologue
    .line 193
    if-nez p1, :cond_0

    .line 194
    const-string/jumbo v0, "PostBroadcast.SaveCameraRoll"

    const-string/jumbo v1, "No"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :goto_0
    return-void

    .line 196
    :cond_0
    const-string/jumbo v1, "PostBroadcast.SaveCameraRoll"

    if-eqz p2, :cond_1

    const-string/jumbo v0, "Autosaved"

    :goto_1
    invoke-virtual {p0, v1, v0}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "Saved"

    goto :goto_1
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 130
    const-string/jumbo v0, "Summary.BroadcastID"

    invoke-virtual {p0, v0, p1}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string/jumbo v0, "Summary.BroadcasterUserID"

    invoke-virtual {p0, v0, p2}, Ltv/periscope/android/analytics/summary/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 179
    const-string/jumbo v0, "Broadcast.ViewerBlock"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/c;->d(Ljava/lang/String;)V

    .line 180
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 184
    const-string/jumbo v0, "Broadcast.ViewerProfileViewed"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/c;->d(Ljava/lang/String;)V

    .line 185
    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    const-string/jumbo v0, "BroadcastCreated"

    return-object v0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 166
    const-string/jumbo v0, "PreBroadcast.Cancelled"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/c;->c(Ljava/lang/String;)V

    .line 167
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 170
    const-string/jumbo v0, "Broadcast.CameraFlip"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/c;->d(Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    .line 174
    const-string/jumbo v0, "Broadcast.HideChat"

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/c;->d(Ljava/lang/String;)V

    .line 175
    return-void
.end method
