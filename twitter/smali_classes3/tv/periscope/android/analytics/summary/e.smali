.class public Ltv/periscope/android/analytics/summary/e;
.super Lcrw;
.source "Twttr"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    invoke-direct {p0, p1}, Lcrw;-><init>(Ljava/lang/String;)V

    .line 28
    const-string/jumbo v0, "Highlights.PreviousScreen"

    const-string/jumbo v1, "Undefined"

    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/analytics/summary/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Highlights.NumViewed"

    aput-object v1, v0, v2

    const-string/jumbo v1, "Highlights.NumSwiped"

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/e;->b([Ljava/lang/String;)V

    .line 30
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "Highlights.DidEnterBroadcast"

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Ltv/periscope/android/analytics/summary/e;->a([Ljava/lang/String;)V

    .line 31
    return-void
.end method


# virtual methods
.method public f()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 52
    const-string/jumbo v0, "Highlights Viewed"

    return-object v0
.end method
