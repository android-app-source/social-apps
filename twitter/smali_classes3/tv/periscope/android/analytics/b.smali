.class public Ltv/periscope/android/analytics/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/analytics/a;


# instance fields
.field private final a:Lcom/xspotlivin/analytics/a;

.field private final b:Ltv/periscope/android/analytics/summary/f;

.field private final c:Ltv/periscope/android/analytics/e;


# direct methods
.method public constructor <init>(Ltv/periscope/android/analytics/e;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ltv/periscope/android/analytics/b$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/analytics/b$1;-><init>(Ltv/periscope/android/analytics/b;)V

    iput-object v0, p0, Ltv/periscope/android/analytics/b;->a:Lcom/xspotlivin/analytics/a;

    .line 40
    iput-object p1, p0, Ltv/periscope/android/analytics/b;->c:Ltv/periscope/android/analytics/e;

    .line 41
    new-instance v0, Ltv/periscope/android/analytics/summary/f;

    invoke-direct {v0}, Ltv/periscope/android/analytics/summary/f;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/analytics/b;->b:Ltv/periscope/android/analytics/summary/f;

    .line 42
    return-void
.end method


# virtual methods
.method public a(Ltv/periscope/android/analytics/summary/SummaryType;Ljava/lang/String;)Lcrw;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Ltv/periscope/android/analytics/b;->b:Ltv/periscope/android/analytics/summary/f;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/analytics/summary/f;->a(Ltv/periscope/android/analytics/summary/SummaryType;Ljava/lang/String;)Lcrw;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 56
    const-string/jumbo v0, "DebugAnalytics"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "trackScreen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public a(Lcrz;)V
    .locals 5

    .prologue
    .line 77
    invoke-interface {p1}, Lcrz;->b()Ljava/util/Map;

    move-result-object v0

    .line 78
    invoke-interface {p1}, Lcrz;->f()Ljava/lang/String;

    move-result-object v1

    .line 79
    const-string/jumbo v2, "DebugAnalytics"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "reportSummary \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "\': "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public a(Ltv/periscope/android/analytics/Event;)V
    .locals 2

    .prologue
    .line 62
    const-string/jumbo v0, "DebugAnalytics"

    iget-object v1, p1, Ltv/periscope/android/analytics/Event;->eventName:Ljava/lang/String;

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public a(Ltv/periscope/android/analytics/Event;Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/android/analytics/Event;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    const-string/jumbo v0, "DebugAnalytics"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Ltv/periscope/android/analytics/Event;->eventName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    return-void
.end method
