.class public Ltv/periscope/android/view/PsFollowButton;
.super Ltv/periscope/android/view/PsCheckButton;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1}, Ltv/periscope/android/view/PsCheckButton;-><init>(Landroid/content/Context;)V

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/view/PsCheckButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Ltv/periscope/android/view/PsCheckButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    return-void
.end method


# virtual methods
.method protected getDefaultCheckedColorFilterId()I
    .locals 1

    .prologue
    .line 29
    sget v0, Ltv/periscope/android/library/f$d;->ps__blue:I

    return v0
.end method

.method protected getDefaultCheckedResId()I
    .locals 1

    .prologue
    .line 24
    sget v0, Ltv/periscope/android/library/f$f;->ps__ic_check:I

    return v0
.end method

.method protected getDefaultUncheckedColorFilterId()I
    .locals 1

    .prologue
    .line 39
    sget v0, Ltv/periscope/android/library/f$d;->ps__blue:I

    return v0
.end method

.method protected getDefaultUncheckedResId()I
    .locals 1

    .prologue
    .line 34
    sget v0, Ltv/periscope/android/library/f$f;->ps__btn_follow:I

    return v0
.end method
