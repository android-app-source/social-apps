.class public Ltv/periscope/android/view/RootDragLayout;
.super Landroid/view/ViewGroup;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/view/RootDragLayout$b;,
        Ltv/periscope/android/view/RootDragLayout$a;,
        Ltv/periscope/android/view/RootDragLayout$d;,
        Ltv/periscope/android/view/RootDragLayout$LayoutParams;,
        Ltv/periscope/android/view/RootDragLayout$c;
    }
.end annotation


# static fields
.field private static final j:[I


# instance fields
.field public a:F

.field public b:F

.field protected c:I
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation
.end field

.field protected d:I
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation
.end field

.field protected e:I
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation
.end field

.field protected f:I
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation
.end field

.field protected g:I
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation
.end field

.field protected h:I
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation
.end field

.field i:Ltv/periscope/android/view/RootDragLayout$b;

.field private k:Ltv/periscope/android/view/RootDragLayout$d;

.field private l:Landroid/support/v4/widget/ViewDragHelper;

.field private m:Landroid/view/View;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/view/l;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/view/l;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private q:Landroid/graphics/Paint;

.field private r:Ltv/periscope/android/view/RootDragLayout$c;

.field private s:Ltv/periscope/android/view/ae;

.field private t:Landroid/graphics/RectF;

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100b3

    aput v2, v0, v1

    sput-object v0, Ltv/periscope/android/view/RootDragLayout;->j:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 64
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Ltv/periscope/android/view/RootDragLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/view/RootDragLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 72
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->n:Ljava/util/List;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->o:Ljava/util/List;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->p:Ljava/util/ArrayList;

    .line 51
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->q:Landroid/graphics/Paint;

    .line 58
    iput-boolean v1, p0, Ltv/periscope/android/view/RootDragLayout;->w:Z

    .line 73
    new-instance v0, Ltv/periscope/android/view/RootDragLayout$d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ltv/periscope/android/view/RootDragLayout$d;-><init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/RootDragLayout$1;)V

    iput-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->k:Ltv/periscope/android/view/RootDragLayout$d;

    .line 74
    iput v2, p0, Ltv/periscope/android/view/RootDragLayout;->x:F

    .line 75
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$e;->ps__playback_control_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout;->a:F

    .line 76
    iget v0, p0, Ltv/periscope/android/view/RootDragLayout;->a:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout;->b:F

    .line 78
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 79
    const/high16 v1, 0x43c80000    # 400.0f

    mul-float/2addr v0, v1

    .line 80
    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout;->k:Ltv/periscope/android/view/RootDragLayout$d;

    invoke-static {p0, v2, v1}, Landroid/support/v4/widget/ViewDragHelper;->create(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ViewDragHelper$Callback;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v1

    iput-object v1, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    .line 81
    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/ViewDragHelper;->setMinVelocity(F)V

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Ltv/periscope/android/library/f$n;->RootDragLayout:[I

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v1, p3, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 85
    :try_start_0
    sget v0, Ltv/periscope/android/library/f$n;->RootDragLayout_ps__dragChild:I

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout;->c:I

    .line 86
    sget v0, Ltv/periscope/android/library/f$n;->RootDragLayout_ps__bottomDragChild:I

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout;->d:I

    .line 87
    sget v0, Ltv/periscope/android/library/f$n;->RootDragLayout_ps__actionSheet:I

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout;->e:I

    .line 88
    sget v0, Ltv/periscope/android/library/f$n;->RootDragLayout_ps__extrasActionSheet:I

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout;->f:I

    .line 89
    sget v0, Ltv/periscope/android/library/f$n;->RootDragLayout_ps__bottomSheet:I

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout;->g:I

    .line 90
    sget v0, Ltv/periscope/android/library/f$n;->RootDragLayout_ps__swipeDismissChild:I

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 94
    return-void

    .line 92
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private a(Ljava/util/List;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/view/l;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 577
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/l;

    .line 578
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Ltv/periscope/android/view/l;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 579
    iget-object v0, v0, Ltv/periscope/android/view/l;->a:Landroid/view/View;

    .line 582
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/view/RootDragLayout;)Ltv/periscope/android/view/RootDragLayout$c;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->r:Ltv/periscope/android/view/RootDragLayout$c;

    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    .line 249
    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout;->m:Landroid/view/View;

    .line 250
    if-nez v1, :cond_0

    .line 258
    :goto_0
    return-void

    .line 253
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    .line 254
    iget v2, p0, Ltv/periscope/android/view/RootDragLayout;->a:F

    iget v3, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->a:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 255
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 256
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 257
    iget v5, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->leftMargin:I

    iget v0, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->leftMargin:I

    add-int/2addr v0, v3

    add-int v3, v2, v4

    invoke-virtual {v1, v5, v2, v0, v3}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method private a(IF)V
    .locals 2

    .prologue
    .line 550
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/widget/ViewDragHelper;->checkTouchSlop(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0}, Landroid/support/v4/widget/ViewDragHelper;->getCapturedView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 552
    iget v0, p0, Ltv/periscope/android/view/RootDragLayout;->y:F

    sub-float v0, p2, v0

    .line 553
    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 554
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->m:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->m:Landroid/view/View;

    invoke-direct {p0, v0}, Ltv/periscope/android/view/RootDragLayout;->f(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 555
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout;->m:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/widget/ViewDragHelper;->captureChildView(Landroid/view/View;I)V

    .line 574
    :cond_0
    :goto_0
    return-void

    .line 557
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->o:Ljava/util/List;

    invoke-direct {p0, v0}, Ltv/periscope/android/view/RootDragLayout;->b(Ljava/util/List;)Landroid/view/View;

    move-result-object v0

    .line 558
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Ltv/periscope/android/view/RootDragLayout;->f(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 559
    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v1, v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->captureChildView(Landroid/view/View;I)V

    goto :goto_0

    .line 563
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->m:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->m:Landroid/view/View;

    invoke-direct {p0, v0}, Ltv/periscope/android/view/RootDragLayout;->f(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 564
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout;->m:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/widget/ViewDragHelper;->captureChildView(Landroid/view/View;I)V

    goto :goto_0

    .line 566
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->o:Ljava/util/List;

    invoke-direct {p0, v0}, Ltv/periscope/android/view/RootDragLayout;->a(Ljava/util/List;)Landroid/view/View;

    move-result-object v0

    .line 567
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Ltv/periscope/android/view/RootDragLayout;->f(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 568
    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v1, v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->captureChildView(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private a(IIIIZ)V
    .locals 13

    .prologue
    .line 298
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getChildCount()I

    move-result v3

    .line 300
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingLeft()I

    move-result v4

    .line 301
    sub-int v0, p3, p1

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingRight()I

    move-result v1

    sub-int v5, v0, v1

    .line 303
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingTop()I

    move-result v6

    .line 304
    sub-int v0, p4, p2

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingBottom()I

    move-result v1

    sub-int v7, v0, v1

    .line 306
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_4

    .line 307
    invoke-virtual {p0, v2}, Ltv/periscope/android/view/RootDragLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 308
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->m:Landroid/view/View;

    if-eq v8, v0, :cond_0

    invoke-direct {p0, v8}, Ltv/periscope/android/view/RootDragLayout;->e(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 306
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 311
    :cond_1
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    .line 313
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 314
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 319
    iget v1, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->c:I

    .line 320
    const/4 v11, -0x1

    if-ne v1, v11, :cond_2

    .line 321
    const v1, 0x800033

    .line 324
    :cond_2
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getLayoutDirection()I

    move-result v11

    .line 325
    invoke-static {v1, v11}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    move-result v11

    .line 326
    and-int/lit8 v12, v1, 0x70

    .line 328
    and-int/lit8 v1, v11, 0x7

    sparse-switch v1, :sswitch_data_0

    .line 340
    :cond_3
    iget v1, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->leftMargin:I

    add-int/2addr v1, v4

    .line 343
    :goto_2
    sparse-switch v12, :sswitch_data_1

    .line 355
    iget v0, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->topMargin:I

    add-int/2addr v0, v6

    .line 358
    :goto_3
    add-int/2addr v9, v1

    add-int/2addr v10, v0

    invoke-virtual {v8, v1, v0, v9, v10}, Landroid/view/View;->layout(IIII)V

    goto :goto_1

    .line 330
    :sswitch_0
    sub-int v1, v5, v4

    sub-int/2addr v1, v9

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v4

    iget v11, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->leftMargin:I

    add-int/2addr v1, v11

    iget v11, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v1, v11

    .line 332
    goto :goto_2

    .line 334
    :sswitch_1
    if-nez p5, :cond_3

    .line 335
    sub-int v1, v5, v9

    iget v11, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v1, v11

    .line 336
    goto :goto_2

    .line 345
    :sswitch_2
    iget v0, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->topMargin:I

    add-int/2addr v0, v6

    .line 346
    goto :goto_3

    .line 348
    :sswitch_3
    sub-int v11, v7, v6

    sub-int/2addr v11, v10

    div-int/lit8 v11, v11, 0x2

    add-int/2addr v11, v6

    iget v12, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->topMargin:I

    add-int/2addr v11, v12

    iget v0, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->bottomMargin:I

    sub-int v0, v11, v0

    .line 350
    goto :goto_3

    .line 352
    :sswitch_4
    sub-int v11, v7, v10

    iget v0, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->bottomMargin:I

    sub-int v0, v11, v0

    .line 353
    goto :goto_3

    .line 360
    :cond_4
    return-void

    .line 328
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch

    .line 343
    :sswitch_data_1
    .sparse-switch
        0x10 -> :sswitch_3
        0x30 -> :sswitch_2
        0x50 -> :sswitch_4
    .end sparse-switch
.end method

.method private a(IZZ)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 140
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/RootDragLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_0

    .line 142
    new-instance v1, Ltv/periscope/android/view/RootDragLayout$a;

    invoke-direct {v1, p0}, Ltv/periscope/android/view/RootDragLayout$a;-><init>(Ltv/periscope/android/view/RootDragLayout;)V

    .line 143
    iput-object v0, v1, Ltv/periscope/android/view/l;->a:Landroid/view/View;

    .line 144
    iput-boolean p2, v1, Ltv/periscope/android/view/l;->b:Z

    .line 145
    iput-boolean p3, v1, Ltv/periscope/android/view/l;->c:Z

    .line 146
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;II)V
    .locals 8

    .prologue
    .line 261
    if-nez p1, :cond_1

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    sub-int v1, p3, p2

    .line 265
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 266
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 267
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    .line 268
    int-to-float v4, v2

    iget v5, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->a:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    sub-int v4, v1, v4

    .line 269
    sub-int/2addr v1, v4

    int-to-float v1, v1

    int-to-float v5, v2

    div-float v5, v1, v5

    .line 270
    iget v1, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->a:F

    cmpl-float v1, v5, v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 272
    :goto_1
    iget v6, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->leftMargin:I

    iget v7, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->leftMargin:I

    add-int/2addr v3, v7

    add-int/2addr v2, v4

    invoke-virtual {p1, v6, v4, v3, v2}, Landroid/view/View;->layout(IIII)V

    .line 274
    if-eqz v1, :cond_0

    .line 275
    iput v5, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->a:F

    goto :goto_0

    .line 270
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->r:Ltv/periscope/android/view/RootDragLayout$c;

    if-eqz v0, :cond_0

    .line 706
    if-eqz p2, :cond_1

    .line 707
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->r:Ltv/periscope/android/view/RootDragLayout$c;

    invoke-interface {v0, p1}, Ltv/periscope/android/view/RootDragLayout$c;->b(Landroid/view/View;)V

    .line 712
    :cond_0
    :goto_0
    return-void

    .line 709
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->r:Ltv/periscope/android/view/RootDragLayout$c;

    invoke-interface {v0, p1}, Ltv/periscope/android/view/RootDragLayout$c;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/view/RootDragLayout;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;Z)V

    return-void
.end method

.method private a(Ljava/util/List;Landroid/view/View;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/view/l;",
            ">;",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 284
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/view/RootDragLayout;->b(Ljava/util/List;Landroid/view/View;)Ltv/periscope/android/view/l;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/view/RootDragLayout;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Ltv/periscope/android/view/RootDragLayout;->e(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Ltv/periscope/android/view/RootDragLayout;)Landroid/support/v4/widget/ViewDragHelper;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    return-object v0
.end method

.method private b()Landroid/view/View;
    .locals 4

    .prologue
    .line 453
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/l;

    .line 454
    iget-boolean v1, v0, Ltv/periscope/android/view/l;->c:Z

    if-eqz v1, :cond_0

    .line 455
    iget-object v1, v0, Ltv/periscope/android/view/l;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    iget v1, v1, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->a:F

    .line 456
    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    .line 457
    iget-object v0, v0, Ltv/periscope/android/view/l;->a:Landroid/view/View;

    .line 461
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/util/List;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/view/l;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 586
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/l;

    .line 587
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ltv/periscope/android/view/l;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 588
    iget-object v0, v0, Ltv/periscope/android/view/l;->a:Landroid/view/View;

    .line 591
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/util/List;Landroid/view/View;)Ltv/periscope/android/view/l;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/view/l;",
            ">;",
            "Landroid/view/View;",
            ")",
            "Ltv/periscope/android/view/l;"
        }
    .end annotation

    .prologue
    .line 288
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/l;

    .line 289
    iget-object v2, v0, Ltv/periscope/android/view/l;->a:Landroid/view/View;

    if-ne v2, p2, :cond_0

    .line 293
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Ltv/periscope/android/view/RootDragLayout;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Ltv/periscope/android/view/RootDragLayout;->f(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method private c()F
    .locals 1

    .prologue
    .line 465
    invoke-direct {p0}, Ltv/periscope/android/view/RootDragLayout;->b()Landroid/view/View;

    move-result-object v0

    .line 466
    if-eqz v0, :cond_0

    .line 467
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    iget v0, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->a:F

    .line 469
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Ltv/periscope/android/view/RootDragLayout;)F
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Ltv/periscope/android/view/RootDragLayout;->x:F

    return v0
.end method

.method static synthetic d(Ltv/periscope/android/view/RootDragLayout;)F
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Ltv/periscope/android/view/RootDragLayout;->y:F

    return v0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/l;

    .line 534
    iget-object v0, v0, Ltv/periscope/android/view/l;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    const/4 v0, 0x1

    .line 538
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Ltv/periscope/android/view/RootDragLayout;)F
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Ltv/periscope/android/view/RootDragLayout;->z:F

    return v0
.end method

.method private e(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->o:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Ltv/periscope/android/view/RootDragLayout;->a(Ljava/util/List;Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic f(Ltv/periscope/android/view/RootDragLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->m:Landroid/view/View;

    return-object v0
.end method

.method private f(Landroid/view/View;)Z
    .locals 3

    .prologue
    .line 521
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/l;

    .line 522
    iget-object v2, v0, Ltv/periscope/android/view/l;->a:Landroid/view/View;

    if-eq p1, v2, :cond_0

    .line 525
    iget-object v0, v0, Ltv/periscope/android/view/l;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526
    const/4 v0, 0x1

    .line 529
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g()[I
    .locals 1

    .prologue
    .line 29
    sget-object v0, Ltv/periscope/android/view/RootDragLayout;->j:[I

    return-object v0
.end method


# virtual methods
.method public a(Landroid/util/AttributeSet;)Ltv/periscope/android/view/RootDragLayout$LayoutParams;
    .locals 2

    .prologue
    .line 622
    new-instance v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ltv/periscope/android/view/RootDragLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public a(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 626
    if-nez p1, :cond_0

    .line 630
    :goto_0
    return v1

    .line 629
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    .line 630
    iget v0, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->a:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 634
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    .line 635
    iget-boolean v1, p0, Ltv/periscope/android/view/RootDragLayout;->w:Z

    if-nez v1, :cond_0

    iget-boolean v1, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->b:Z

    if-nez v1, :cond_3

    .line 636
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget v2, p0, Ltv/periscope/android/view/RootDragLayout;->c:I

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout;->n:Ljava/util/List;

    invoke-direct {p0, v1, p1}, Ltv/periscope/android/view/RootDragLayout;->a(Ljava/util/List;Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 637
    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->a:F

    .line 638
    const/4 v1, 0x1

    iput-boolean v1, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->b:Z

    .line 654
    :goto_0
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->invalidate()V

    .line 655
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;Z)V

    .line 656
    return-void

    .line 640
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Not a drag child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 643
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 644
    iget v1, p0, Ltv/periscope/android/view/RootDragLayout;->c:I

    if-ne v0, v1, :cond_4

    .line 645
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v2, p0, Ltv/periscope/android/view/RootDragLayout;->a:F

    float-to-int v2, v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    goto :goto_0

    .line 647
    :cond_4
    invoke-direct {p0, p1}, Ltv/periscope/android/view/RootDragLayout;->e(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 648
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    goto :goto_0

    .line 650
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Not a drag child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 659
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    .line 660
    iget-boolean v1, p0, Ltv/periscope/android/view/RootDragLayout;->w:Z

    if-nez v1, :cond_0

    iget-boolean v1, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->b:Z

    if-nez v1, :cond_3

    .line 661
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget v2, p0, Ltv/periscope/android/view/RootDragLayout;->c:I

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout;->n:Ljava/util/List;

    invoke-direct {p0, v1, p1}, Ltv/periscope/android/view/RootDragLayout;->a(Ljava/util/List;Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 662
    :cond_1
    const/4 v1, 0x0

    iput v1, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->a:F

    .line 663
    iput-boolean v3, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->b:Z

    .line 679
    :goto_0
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->invalidate()V

    .line 680
    invoke-direct {p0, p1, v3}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;Z)V

    .line 681
    return-void

    .line 665
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Not a drag child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 668
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 669
    iget v1, p0, Ltv/periscope/android/view/RootDragLayout;->c:I

    if-ne v0, v1, :cond_4

    .line 670
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    goto :goto_0

    .line 672
    :cond_4
    invoke-direct {p0, p1}, Ltv/periscope/android/view/RootDragLayout;->e(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 673
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getHeight()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    goto :goto_0

    .line 675
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Not a drag child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 617
    instance-of v0, p1, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 2

    .prologue
    .line 596
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/ViewDragHelper;->continueSettling(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->postInvalidateOnAnimation()V

    .line 599
    :cond_0
    return-void
.end method

.method public d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 684
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 685
    iget v1, p0, Ltv/periscope/android/view/RootDragLayout;->c:I

    if-ne v0, v1, :cond_1

    .line 686
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    if-gtz v0, :cond_0

    .line 687
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/RootDragLayout;->b(Landroid/view/View;)V

    .line 702
    :goto_0
    return-void

    .line 689
    :cond_0
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/RootDragLayout;->c(Landroid/view/View;)V

    goto :goto_0

    .line 692
    :cond_1
    invoke-direct {p0, p1}, Ltv/periscope/android/view/RootDragLayout;->e(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 693
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getHeight()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 694
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/RootDragLayout;->b(Landroid/view/View;)V

    goto :goto_0

    .line 696
    :cond_2
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/RootDragLayout;->c(Landroid/view/View;)V

    goto :goto_0

    .line 699
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Not a drag child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 716
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->n:Ljava/util/List;

    invoke-direct {p0, v0, p2}, Ltv/periscope/android/view/RootDragLayout;->b(Ljava/util/List;Landroid/view/View;)Ltv/periscope/android/view/l;

    move-result-object v0

    .line 717
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Ltv/periscope/android/view/l;->c:Z

    if-eqz v0, :cond_0

    .line 718
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    .line 719
    iget v2, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->a:F

    .line 720
    cmpl-float v3, v2, v1

    if-lez v3, :cond_0

    .line 723
    const/high16 v3, 0x43190000    # 153.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 724
    shl-int/lit8 v2, v2, 0x18

    or-int/lit8 v2, v2, 0x0

    .line 725
    iget-object v3, p0, Ltv/periscope/android/view/RootDragLayout;->q:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 727
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getWidth()I

    move-result v2

    int-to-float v3, v2

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v2

    iget v0, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->topMargin:I

    sub-int v0, v2, v0

    int-to-float v4, v0

    iget-object v5, p0, Ltv/periscope/android/view/RootDragLayout;->q:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 730
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    return v0
.end method

.method protected f()Ltv/periscope/android/view/RootDragLayout$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 603
    new-instance v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Ltv/periscope/android/view/RootDragLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->f()Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/util/AttributeSet;)Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 608
    instance-of v0, p1, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    if-eqz v0, :cond_0

    new-instance v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    check-cast p1, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    invoke-direct {v0, p1}, Ltv/periscope/android/view/RootDragLayout$LayoutParams;-><init>(Ltv/periscope/android/view/RootDragLayout$LayoutParams;)V

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    new-instance v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Ltv/periscope/android/view/RootDragLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    invoke-direct {v0, p1}, Ltv/periscope/android/view/RootDragLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 377
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 378
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/view/RootDragLayout;->w:Z

    .line 379
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 371
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 372
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/view/RootDragLayout;->w:Z

    .line 373
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 131
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 132
    iget v0, p0, Ltv/periscope/android/view/RootDragLayout;->c:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/RootDragLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->m:Landroid/view/View;

    .line 133
    iget v0, p0, Ltv/periscope/android/view/RootDragLayout;->d:I

    invoke-direct {p0, v0, v2, v1}, Ltv/periscope/android/view/RootDragLayout;->a(IZZ)V

    .line 134
    iget v0, p0, Ltv/periscope/android/view/RootDragLayout;->e:I

    invoke-direct {p0, v0, v1, v2}, Ltv/periscope/android/view/RootDragLayout;->a(IZZ)V

    .line 135
    iget v0, p0, Ltv/periscope/android/view/RootDragLayout;->f:I

    invoke-direct {p0, v0, v1, v2}, Ltv/periscope/android/view/RootDragLayout;->a(IZZ)V

    .line 136
    iget v0, p0, Ltv/periscope/android/view/RootDragLayout;->g:I

    invoke-direct {p0, v0, v1, v1}, Ltv/periscope/android/view/RootDragLayout;->a(IZZ)V

    .line 137
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 386
    .line 388
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 393
    :goto_0
    iget-boolean v2, p0, Ltv/periscope/android/view/RootDragLayout;->u:Z

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-le v2, v3, :cond_1

    .line 446
    :cond_0
    :goto_1
    return v1

    .line 389
    :catch_0
    move-exception v0

    .line 391
    new-instance v2, Ljava/lang/Exception;

    const-string/jumbo v4, "Crash while attempting to intercept touch event"

    invoke-direct {v2, v4, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v2}, Lf;->a(Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_0

    .line 397
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    .line 398
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v6

    .line 400
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 401
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    .line 403
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getWidth()I

    move-result v2

    if-nez v2, :cond_3

    move v2, v3

    :goto_2
    int-to-float v2, v2

    div-float v8, v4, v2

    .line 404
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getHeight()I

    move-result v2

    if-nez v2, :cond_4

    move v2, v3

    :goto_3
    int-to-float v2, v2

    div-float v9, v7, v2

    .line 406
    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    float-to-int v4, v4

    float-to-int v10, v7

    invoke-virtual {v2, v4, v10}, Landroid/support/v4/widget/ViewDragHelper;->findTopChildUnder(II)Landroid/view/View;

    move-result-object v10

    .line 409
    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout;->s:Ltv/periscope/android/view/ae;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout;->s:Ltv/periscope/android/view/ae;

    invoke-virtual {v2}, Ltv/periscope/android/view/ae;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v3

    .line 410
    :goto_4
    iget-object v4, p0, Ltv/periscope/android/view/RootDragLayout;->t:Landroid/graphics/RectF;

    if-eqz v4, :cond_6

    iget-object v4, p0, Ltv/periscope/android/view/RootDragLayout;->t:Landroid/graphics/RectF;

    invoke-virtual {v4, v8, v9}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v3

    .line 411
    :goto_5
    invoke-direct {p0}, Ltv/periscope/android/view/RootDragLayout;->d()Z

    move-result v8

    if-nez v8, :cond_2

    if-nez v2, :cond_2

    if-nez v4, :cond_0

    .line 415
    :cond_2
    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout;->s:Ltv/periscope/android/view/ae;

    if-eqz v2, :cond_8

    .line 416
    if-eqz v10, :cond_8

    invoke-virtual {v10}, Landroid/view/View;->getId()I

    move-result v2

    iget v4, p0, Ltv/periscope/android/view/RootDragLayout;->h:I

    if-ne v2, v4, :cond_8

    invoke-direct {p0}, Ltv/periscope/android/view/RootDragLayout;->d()Z

    move-result v2

    if-nez v2, :cond_8

    .line 417
    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout;->s:Ltv/periscope/android/view/ae;

    invoke-virtual {v2, v10, p1}, Ltv/periscope/android/view/ae;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_7

    move v1, v3

    .line 418
    goto :goto_1

    .line 403
    :cond_3
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getWidth()I

    move-result v2

    goto :goto_2

    .line 404
    :cond_4
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getHeight()I

    move-result v2

    goto :goto_3

    :cond_5
    move v2, v1

    .line 409
    goto :goto_4

    :cond_6
    move v4, v1

    .line 410
    goto :goto_5

    .line 420
    :cond_7
    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout;->s:Ltv/periscope/android/view/ae;

    invoke-virtual {v2, v10}, Ltv/periscope/android/view/ae;->a(Landroid/view/View;)V

    .line 425
    :cond_8
    iput v7, p0, Ltv/periscope/android/view/RootDragLayout;->z:F

    .line 429
    packed-switch v5, :pswitch_data_0

    :cond_9
    :goto_6
    :pswitch_0
    move v2, v1

    .line 446
    :goto_7
    if-nez v0, :cond_a

    if-eqz v2, :cond_0

    :cond_a
    move v1, v3

    goto/16 :goto_1

    .line 431
    :pswitch_1
    iput v7, p0, Ltv/periscope/android/view/RootDragLayout;->y:F

    .line 432
    invoke-direct {p0}, Ltv/periscope/android/view/RootDragLayout;->c()F

    move-result v2

    .line 433
    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-lez v2, :cond_9

    invoke-direct {p0}, Ltv/periscope/android/view/RootDragLayout;->b()Landroid/view/View;

    move-result-object v2

    if-eq v10, v2, :cond_9

    move v2, v3

    .line 434
    goto :goto_7

    .line 439
    :pswitch_2
    if-eqz v10, :cond_b

    iget v2, p0, Ltv/periscope/android/view/RootDragLayout;->y:F

    sub-float/2addr v2, v7

    float-to-int v2, v2

    invoke-virtual {v10, v2}, Landroid/view/View;->canScrollVertically(I)Z

    move-result v2

    if-nez v2, :cond_c

    .line 440
    :cond_b
    invoke-direct {p0, v6, v7}, Ltv/periscope/android/view/RootDragLayout;->a(IF)V

    .line 442
    :cond_c
    iput v7, p0, Ltv/periscope/android/view/RootDragLayout;->y:F

    goto :goto_6

    .line 429
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 235
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/view/RootDragLayout;->v:Z

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    .line 237
    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/view/RootDragLayout;->a(IIIIZ)V

    .line 238
    invoke-direct {p0}, Ltv/periscope/android/view/RootDragLayout;->a()V

    .line 240
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/l;

    .line 241
    iget-object v0, v0, Ltv/periscope/android/view/l;->a:Landroid/view/View;

    invoke-direct {p0, v0, p3, p5}, Ltv/periscope/android/view/RootDragLayout;->a(Landroid/view/View;II)V

    goto :goto_0

    .line 244
    :cond_0
    iput-boolean v5, p0, Ltv/periscope/android/view/RootDragLayout;->v:Z

    .line 245
    iput-boolean v5, p0, Ltv/periscope/android/view/RootDragLayout;->w:Z

    .line 246
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    .line 153
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getChildCount()I

    move-result v11

    .line 156
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_0

    .line 157
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    if-eq v0, v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    move v6, v0

    .line 158
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 160
    const/4 v9, 0x0

    .line 161
    const/4 v8, 0x0

    .line 162
    const/4 v7, 0x0

    .line 164
    const/4 v0, 0x0

    move v10, v0

    :goto_1
    if-ge v10, v11, :cond_4

    .line 165
    invoke-virtual {p0, v10}, Ltv/periscope/android/view/RootDragLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 166
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_8

    .line 167
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v2, p1

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Ltv/periscope/android/view/RootDragLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 168
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    .line 170
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget v3, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->leftMargin:I

    add-int/2addr v2, v3

    iget v3, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->rightMargin:I

    add-int/2addr v2, v3

    .line 169
    invoke-static {v8, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 172
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v4, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->topMargin:I

    add-int/2addr v2, v4

    iget v4, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v2, v4

    .line 171
    invoke-static {v9, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 173
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredState()I

    move-result v2

    invoke-static {v7, v2}, Ltv/periscope/android/view/RootDragLayout;->combineMeasuredStates(II)I

    move-result v2

    .line 174
    if-eqz v6, :cond_2

    .line 175
    iget v5, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->width:I

    const/4 v7, -0x1

    if-eq v5, v7, :cond_1

    iget v0, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->height:I

    const/4 v5, -0x1

    if-ne v0, v5, :cond_2

    .line 177
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    move v0, v2

    move v1, v3

    move v2, v4

    .line 164
    :goto_2
    add-int/lit8 v3, v10, 0x1

    move v10, v3

    move v7, v0

    move v8, v1

    move v9, v2

    goto :goto_1

    .line 157
    :cond_3
    const/4 v0, 0x0

    move v6, v0

    goto :goto_0

    .line 184
    :cond_4
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, v8

    .line 185
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, v9

    .line 188
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 189
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 191
    invoke-static {v0, p1, v7}, Ltv/periscope/android/view/RootDragLayout;->resolveSizeAndState(III)I

    move-result v0

    shl-int/lit8 v2, v7, 0x10

    .line 192
    invoke-static {v1, p2, v2}, Ltv/periscope/android/view/RootDragLayout;->resolveSizeAndState(III)I

    move-result v1

    .line 191
    invoke-virtual {p0, v0, v1}, Ltv/periscope/android/view/RootDragLayout;->setMeasuredDimension(II)V

    .line 195
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 196
    const/4 v0, 0x1

    if-le v4, v0, :cond_7

    .line 197
    const/4 v0, 0x0

    move v3, v0

    :goto_3
    if-ge v3, v4, :cond_7

    .line 198
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 200
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 204
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    const/4 v5, -0x1

    if-ne v2, v5, :cond_5

    .line 205
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getMeasuredWidth()I

    move-result v2

    .line 206
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v2, v5

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingRight()I

    move-result v5

    sub-int/2addr v2, v5

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v2, v5

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v2, v5

    const/high16 v5, 0x40000000    # 2.0f

    .line 205
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 216
    :goto_4
    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_6

    .line 217
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getMeasuredHeight()I

    move-result v5

    .line 218
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingTop()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v5, v6

    iget v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v5, v6

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v1, v5, v1

    const/high16 v5, 0x40000000    # 2.0f

    .line 217
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 228
    :goto_5
    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    .line 197
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 211
    :cond_5
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingRight()I

    move-result v5

    add-int/2addr v2, v5

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v2, v5

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v5

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 210
    invoke-static {p1, v2, v5}, Ltv/periscope/android/view/RootDragLayout;->getChildMeasureSpec(III)I

    move-result v2

    goto :goto_4

    .line 223
    :cond_6
    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingTop()I

    move-result v5

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v5, v6

    iget v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v5, v6

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 222
    invoke-static {p2, v5, v1}, Ltv/periscope/android/view/RootDragLayout;->getChildMeasureSpec(III)I

    move-result v1

    goto :goto_5

    .line 231
    :cond_7
    return-void

    :cond_8
    move v0, v7

    move v1, v8

    move v2, v9

    goto/16 :goto_2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 474
    iget-boolean v2, p0, Ltv/periscope/android/view/RootDragLayout;->u:Z

    if-nez v2, :cond_1

    .line 517
    :cond_0
    :goto_0
    return v0

    .line 480
    :cond_1
    :try_start_0
    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v2, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-gt v2, v1, :cond_0

    .line 489
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 490
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 492
    iput v3, p0, Ltv/periscope/android/view/RootDragLayout;->z:F

    .line 494
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    .line 495
    packed-switch v4, :pswitch_data_0

    :cond_2
    :goto_2
    move v0, v1

    .line 517
    goto :goto_0

    .line 481
    :catch_0
    move-exception v2

    .line 483
    new-instance v3, Ljava/lang/Exception;

    const-string/jumbo v4, "Crash while processing touch event"

    invoke-direct {v3, v4, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v3}, Lf;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 497
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout;->y:F

    goto :goto_2

    .line 501
    :pswitch_1
    iget-object v4, p0, Ltv/periscope/android/view/RootDragLayout;->l:Landroid/support/v4/widget/ViewDragHelper;

    float-to-int v2, v2

    float-to-int v5, v3

    invoke-virtual {v4, v2, v5}, Landroid/support/v4/widget/ViewDragHelper;->findTopChildUnder(II)Landroid/view/View;

    move-result-object v2

    .line 502
    if-eqz v2, :cond_3

    iget v4, p0, Ltv/periscope/android/view/RootDragLayout;->y:F

    sub-float/2addr v4, v3

    float-to-int v4, v4

    invoke-virtual {v2, v4}, Landroid/view/View;->canScrollVertically(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 503
    :cond_3
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 504
    invoke-direct {p0, v0, v3}, Ltv/periscope/android/view/RootDragLayout;->a(IF)V

    .line 506
    :cond_4
    iput v3, p0, Ltv/periscope/android/view/RootDragLayout;->y:F

    goto :goto_2

    .line 510
    :pswitch_2
    invoke-direct {p0}, Ltv/periscope/android/view/RootDragLayout;->b()Landroid/view/View;

    move-result-object v0

    .line 511
    if-eqz v0, :cond_2

    .line 512
    invoke-virtual {p0, v0}, Ltv/periscope/android/view/RootDragLayout;->c(Landroid/view/View;)V

    goto :goto_2

    .line 495
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 364
    iget-boolean v0, p0, Ltv/periscope/android/view/RootDragLayout;->v:Z

    if-nez v0, :cond_0

    .line 365
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 367
    :cond_0
    return-void
.end method

.method public setDisableAreaForDrag(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Ltv/periscope/android/view/RootDragLayout;->t:Landroid/graphics/RectF;

    .line 127
    return-void
.end method

.method public setDraggable(Z)V
    .locals 0

    .prologue
    .line 110
    iput-boolean p1, p0, Ltv/periscope/android/view/RootDragLayout;->u:Z

    .line 111
    return-void
.end method

.method public setFriction(F)V
    .locals 0

    .prologue
    .line 117
    iput p1, p0, Ltv/periscope/android/view/RootDragLayout;->x:F

    .line 118
    return-void
.end method

.method public setListener(Ltv/periscope/android/view/RootDragLayout$b;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Ltv/periscope/android/view/RootDragLayout;->i:Ltv/periscope/android/view/RootDragLayout$b;

    .line 98
    return-void
.end method

.method public setOnViewDragListener(Ltv/periscope/android/view/RootDragLayout$c;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Ltv/periscope/android/view/RootDragLayout;->r:Ltv/periscope/android/view/RootDragLayout$c;

    .line 102
    return-void
.end method

.method public setSwipeToDismissCallback(Ltv/periscope/android/view/ae$a;)V
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ltv/periscope/android/view/ae;

    invoke-virtual {p0}, Ltv/periscope/android/view/RootDragLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ltv/periscope/android/view/ae;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->s:Ltv/periscope/android/view/ae;

    .line 106
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout;->s:Ltv/periscope/android/view/ae;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/ae;->a(Ltv/periscope/android/view/ae$a;)V

    .line 107
    return-void
.end method
