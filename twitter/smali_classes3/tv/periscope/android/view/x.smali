.class public Ltv/periscope/android/view/x;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/view/x$a;
    }
.end annotation


# static fields
.field private static final f:I

.field private static final g:I

.field private static final h:I


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private final i:Landroid/os/Handler;

.field private final j:Landroid/view/GestureDetector$OnGestureListener;

.field private k:Landroid/view/GestureDetector$OnDoubleTapListener;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Landroid/view/MotionEvent;

.field private r:Landroid/view/MotionEvent;

.field private s:Z

.field private t:F

.field private u:F

.field private v:F

.field private w:F

.field private x:Z

.field private y:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, Ltv/periscope/android/view/x;->f:I

    .line 46
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Ltv/periscope/android/view/x;->g:I

    .line 47
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Ltv/periscope/android/view/x;->h:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/view/x;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    .line 135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    if-eqz p3, :cond_1

    .line 151
    new-instance v0, Ltv/periscope/android/view/x$a;

    invoke-direct {v0, p0, p3}, Ltv/periscope/android/view/x$a;-><init>(Ltv/periscope/android/view/x;Landroid/os/Handler;)V

    iput-object v0, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    .line 155
    :goto_0
    iput-object p2, p0, Ltv/periscope/android/view/x;->j:Landroid/view/GestureDetector$OnGestureListener;

    .line 156
    instance-of v0, p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_0

    .line 157
    check-cast p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-virtual {p0, p2}, Ltv/periscope/android/view/x;->a(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 159
    :cond_0
    invoke-direct {p0, p1}, Ltv/periscope/android/view/x;->a(Landroid/content/Context;)V

    .line 160
    return-void

    .line 153
    :cond_1
    new-instance v0, Ltv/periscope/android/view/x$a;

    invoke-direct {v0, p0}, Ltv/periscope/android/view/x$a;-><init>(Ltv/periscope/android/view/x;)V

    iput-object v0, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/view/x;)Landroid/view/MotionEvent;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 434
    iget-object v0, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 435
    iget-object v0, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 436
    iget-object v0, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 437
    iget-object v0, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 438
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    .line 439
    iput-boolean v2, p0, Ltv/periscope/android/view/x;->s:Z

    .line 440
    iput-boolean v2, p0, Ltv/periscope/android/view/x;->l:Z

    .line 441
    iput-boolean v2, p0, Ltv/periscope/android/view/x;->o:Z

    .line 442
    iput-boolean v2, p0, Ltv/periscope/android/view/x;->p:Z

    .line 443
    iput-boolean v2, p0, Ltv/periscope/android/view/x;->m:Z

    .line 444
    iget-boolean v0, p0, Ltv/periscope/android/view/x;->n:Z

    if-eqz v0, :cond_0

    .line 445
    iput-boolean v2, p0, Ltv/periscope/android/view/x;->n:Z

    .line 447
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 181
    iget-object v0, p0, Ltv/periscope/android/view/x;->j:Landroid/view/GestureDetector$OnGestureListener;

    if-nez v0, :cond_0

    .line 182
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "OnGestureListener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/view/x;->x:Z

    .line 188
    if-nez p1, :cond_1

    .line 190
    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v0

    .line 192
    const/16 v1, 0x64

    .line 194
    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, Ltv/periscope/android/view/x;->d:I

    .line 195
    invoke-static {}, Landroid/view/ViewConfiguration;->getMaximumFlingVelocity()I

    move-result v2

    iput v2, p0, Ltv/periscope/android/view/x;->e:I

    move v2, v0

    .line 204
    :goto_0
    mul-int/2addr v2, v2

    iput v2, p0, Ltv/periscope/android/view/x;->a:I

    .line 205
    mul-int/2addr v0, v0

    iput v0, p0, Ltv/periscope/android/view/x;->b:I

    .line 206
    mul-int v0, v1, v1

    iput v0, p0, Ltv/periscope/android/view/x;->c:I

    .line 207
    return-void

    .line 197
    :cond_1
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    .line 198
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    .line 200
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v1

    .line 201
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, Ltv/periscope/android/view/x;->d:I

    .line 202
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v2

    iput v2, p0, Ltv/periscope/android/view/x;->e:I

    move v2, v0

    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 464
    iget-boolean v1, p0, Ltv/periscope/android/view/x;->p:Z

    if-nez v1, :cond_1

    .line 475
    :cond_0
    :goto_0
    return v0

    .line 468
    :cond_1
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 469
    sget v1, Ltv/periscope/android/view/x;->h:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    const-wide/16 v4, 0x28

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 473
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    .line 474
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    .line 475
    mul-int/2addr v1, v1

    mul-int/2addr v2, v2

    add-int/2addr v1, v2

    iget v2, p0, Ltv/periscope/android/view/x;->c:I

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Ltv/periscope/android/view/x;Z)Z
    .locals 0

    .prologue
    .line 31
    iput-boolean p1, p0, Ltv/periscope/android/view/x;->m:Z

    return p1
.end method

.method static synthetic b(Ltv/periscope/android/view/x;)Landroid/view/GestureDetector$OnGestureListener;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ltv/periscope/android/view/x;->j:Landroid/view/GestureDetector$OnGestureListener;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 450
    iget-object v0, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 451
    iget-object v0, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 452
    iget-object v0, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 453
    iput-boolean v2, p0, Ltv/periscope/android/view/x;->s:Z

    .line 454
    iput-boolean v2, p0, Ltv/periscope/android/view/x;->o:Z

    .line 455
    iput-boolean v2, p0, Ltv/periscope/android/view/x;->p:Z

    .line 456
    iput-boolean v2, p0, Ltv/periscope/android/view/x;->m:Z

    .line 457
    iget-boolean v0, p0, Ltv/periscope/android/view/x;->n:Z

    if-eqz v0, :cond_0

    .line 458
    iput-boolean v2, p0, Ltv/periscope/android/view/x;->n:Z

    .line 460
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 479
    iget-object v0, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 480
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/view/x;->m:Z

    .line 481
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/view/x;->n:Z

    .line 482
    iget-object v0, p0, Ltv/periscope/android/view/x;->j:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v1, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    .line 483
    return-void
.end method

.method static synthetic c(Ltv/periscope/android/view/x;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ltv/periscope/android/view/x;->c()V

    return-void
.end method

.method static synthetic d(Ltv/periscope/android/view/x;)Landroid/view/GestureDetector$OnDoubleTapListener;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ltv/periscope/android/view/x;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    return-object v0
.end method

.method static synthetic e(Ltv/periscope/android/view/x;)Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Ltv/periscope/android/view/x;->l:Z

    return v0
.end method


# virtual methods
.method public a(Landroid/view/GestureDetector$OnDoubleTapListener;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Ltv/periscope/android/view/x;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    .line 218
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x3

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 249
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    .line 251
    iget-object v0, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 252
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    .line 254
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 256
    and-int/lit16 v0, v9, 0xff

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    move v7, v8

    .line 258
    :goto_0
    if-eqz v7, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 262
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    move v5, v3

    move v1, v6

    move v2, v6

    .line 263
    :goto_2
    if-ge v5, v4, :cond_4

    .line 264
    if-ne v0, v5, :cond_3

    .line 263
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_1
    move v7, v3

    .line 256
    goto :goto_0

    .line 258
    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    .line 265
    :cond_3
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    add-float/2addr v2, v10

    .line 266
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v10

    add-float/2addr v1, v10

    goto :goto_3

    .line 268
    :cond_4
    if-eqz v7, :cond_6

    add-int/lit8 v0, v4, -0x1

    .line 269
    :goto_4
    int-to-float v5, v0

    div-float/2addr v2, v5

    .line 270
    int-to-float v0, v0

    div-float/2addr v1, v0

    .line 274
    and-int/lit16 v0, v9, 0xff

    packed-switch v0, :pswitch_data_0

    .line 430
    :cond_5
    :goto_5
    :pswitch_0
    return v3

    :cond_6
    move v0, v4

    .line 268
    goto :goto_4

    .line 276
    :pswitch_1
    iput v2, p0, Ltv/periscope/android/view/x;->t:F

    iput v2, p0, Ltv/periscope/android/view/x;->v:F

    .line 277
    iput v1, p0, Ltv/periscope/android/view/x;->u:F

    iput v1, p0, Ltv/periscope/android/view/x;->w:F

    .line 279
    invoke-direct {p0}, Ltv/periscope/android/view/x;->b()V

    goto :goto_5

    .line 283
    :pswitch_2
    iput v2, p0, Ltv/periscope/android/view/x;->t:F

    iput v2, p0, Ltv/periscope/android/view/x;->v:F

    .line 284
    iput v1, p0, Ltv/periscope/android/view/x;->u:F

    iput v1, p0, Ltv/periscope/android/view/x;->w:F

    .line 288
    iget-object v0, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, Ltv/periscope/android/view/x;->e:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 289
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    .line 290
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 291
    iget-object v2, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v2

    .line 292
    iget-object v5, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v5, v0}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v5

    move v0, v3

    .line 293
    :goto_6
    if-ge v0, v4, :cond_5

    .line 294
    if-ne v0, v1, :cond_8

    .line 293
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 296
    :cond_8
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v7

    .line 297
    iget-object v8, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v8, v7}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v8

    mul-float/2addr v8, v2

    .line 298
    iget-object v9, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v9, v7}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v7

    mul-float/2addr v7, v5

    .line 300
    add-float/2addr v7, v8

    .line 301
    cmpg-float v7, v7, v6

    if-gez v7, :cond_7

    .line 302
    iget-object v0, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_5

    .line 309
    :pswitch_3
    iget-object v0, p0, Ltv/periscope/android/view/x;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_d

    .line 310
    iget-object v0, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    .line 311
    if-eqz v0, :cond_9

    iget-object v4, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    invoke-virtual {v4, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 312
    :cond_9
    iget-object v4, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    if-eqz v4, :cond_c

    iget-object v4, p0, Ltv/periscope/android/view/x;->r:Landroid/view/MotionEvent;

    if-eqz v4, :cond_c

    if-eqz v0, :cond_c

    iget-object v0, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    iget-object v4, p0, Ltv/periscope/android/view/x;->r:Landroid/view/MotionEvent;

    .line 313
    invoke-direct {p0, v0, v4, p1}, Ltv/periscope/android/view/x;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 315
    iput-boolean v8, p0, Ltv/periscope/android/view/x;->s:Z

    .line 317
    iget-object v0, p0, Ltv/periscope/android/view/x;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    iget-object v4, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    invoke-interface {v0, v4}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/2addr v0, v3

    .line 319
    iget-object v4, p0, Ltv/periscope/android/view/x;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v4, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    or-int/2addr v0, v4

    .line 326
    :goto_7
    iput v2, p0, Ltv/periscope/android/view/x;->t:F

    iput v2, p0, Ltv/periscope/android/view/x;->v:F

    .line 327
    iput v1, p0, Ltv/periscope/android/view/x;->u:F

    iput v1, p0, Ltv/periscope/android/view/x;->w:F

    .line 328
    iget-object v1, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    if-eqz v1, :cond_a

    .line 329
    iget-object v1, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 331
    :cond_a
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    .line 332
    iput-boolean v8, p0, Ltv/periscope/android/view/x;->o:Z

    .line 333
    iput-boolean v8, p0, Ltv/periscope/android/view/x;->p:Z

    .line 334
    iput-boolean v8, p0, Ltv/periscope/android/view/x;->l:Z

    .line 335
    iput-boolean v3, p0, Ltv/periscope/android/view/x;->n:Z

    .line 336
    iput-boolean v3, p0, Ltv/periscope/android/view/x;->m:Z

    .line 338
    iget-boolean v1, p0, Ltv/periscope/android/view/x;->x:Z

    if-eqz v1, :cond_b

    .line 339
    iget-object v1, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 341
    iget-object v1, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    iget-object v2, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    const-wide/16 v4, 0xfa

    add-long/2addr v2, v4

    invoke-virtual {v1, v12, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 344
    :cond_b
    iget-object v1, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    iget-object v2, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sget v4, Ltv/periscope/android/view/x;->g:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v8, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 345
    iget-object v1, p0, Ltv/periscope/android/view/x;->j:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v1, p1}, Landroid/view/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int v3, v0, v1

    .line 346
    goto/16 :goto_5

    .line 322
    :cond_c
    iget-object v0, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    sget v4, Ltv/periscope/android/view/x;->h:I

    int-to-long v4, v4

    invoke-virtual {v0, v11, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_d
    move v0, v3

    goto :goto_7

    .line 349
    :pswitch_4
    iget-boolean v0, p0, Ltv/periscope/android/view/x;->n:Z

    if-nez v0, :cond_5

    .line 352
    iget v0, p0, Ltv/periscope/android/view/x;->t:F

    sub-float/2addr v0, v2

    .line 353
    iget v4, p0, Ltv/periscope/android/view/x;->u:F

    sub-float/2addr v4, v1

    .line 354
    iget-boolean v5, p0, Ltv/periscope/android/view/x;->s:Z

    if-eqz v5, :cond_e

    .line 356
    iget-object v0, p0, Ltv/periscope/android/view/x;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/2addr v3, v0

    goto/16 :goto_5

    .line 357
    :cond_e
    iget-boolean v5, p0, Ltv/periscope/android/view/x;->o:Z

    if-eqz v5, :cond_10

    .line 358
    iget v5, p0, Ltv/periscope/android/view/x;->v:F

    sub-float v5, v2, v5

    float-to-int v5, v5

    .line 359
    iget v6, p0, Ltv/periscope/android/view/x;->w:F

    sub-float v6, v1, v6

    float-to-int v6, v6

    .line 360
    mul-int/2addr v5, v5

    mul-int/2addr v6, v6

    add-int/2addr v5, v6

    .line 361
    iget v6, p0, Ltv/periscope/android/view/x;->a:I

    mul-int/lit8 v6, v6, 0x2

    if-le v5, v6, :cond_1a

    .line 362
    iget-object v6, p0, Ltv/periscope/android/view/x;->j:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v7, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    invoke-interface {v6, v7, p1, v0, v4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    .line 363
    iput v2, p0, Ltv/periscope/android/view/x;->t:F

    .line 364
    iput v1, p0, Ltv/periscope/android/view/x;->u:F

    .line 365
    iput-boolean v3, p0, Ltv/periscope/android/view/x;->o:Z

    .line 366
    iget-object v1, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    invoke-virtual {v1, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 367
    iget-object v1, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 368
    iget-object v1, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 370
    :goto_8
    iget v1, p0, Ltv/periscope/android/view/x;->b:I

    mul-int/lit8 v1, v1, 0x2

    if-le v5, v1, :cond_f

    .line 371
    iput-boolean v3, p0, Ltv/periscope/android/view/x;->p:Z

    :cond_f
    move v3, v0

    .line 373
    goto/16 :goto_5

    :cond_10
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    if-gez v5, :cond_11

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_5

    .line 374
    :cond_11
    iget-object v3, p0, Ltv/periscope/android/view/x;->j:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v5, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    invoke-interface {v3, v5, p1, v0, v4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v3

    .line 375
    iput v2, p0, Ltv/periscope/android/view/x;->t:F

    .line 376
    iput v1, p0, Ltv/periscope/android/view/x;->u:F

    goto/16 :goto_5

    .line 381
    :pswitch_5
    iput-boolean v3, p0, Ltv/periscope/android/view/x;->l:Z

    .line 382
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 383
    iget-boolean v0, p0, Ltv/periscope/android/view/x;->s:Z

    if-eqz v0, :cond_15

    .line 385
    iget-object v0, p0, Ltv/periscope/android/view/x;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/2addr v0, v3

    .line 408
    :cond_12
    :goto_9
    iget-object v2, p0, Ltv/periscope/android/view/x;->r:Landroid/view/MotionEvent;

    if-eqz v2, :cond_13

    .line 409
    iget-object v2, p0, Ltv/periscope/android/view/x;->r:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 412
    :cond_13
    iput-object v1, p0, Ltv/periscope/android/view/x;->r:Landroid/view/MotionEvent;

    .line 413
    iget-object v1, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_14

    .line 416
    iget-object v1, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 417
    const/4 v1, 0x0

    iput-object v1, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    .line 419
    :cond_14
    iput-boolean v3, p0, Ltv/periscope/android/view/x;->s:Z

    .line 420
    iput-boolean v3, p0, Ltv/periscope/android/view/x;->m:Z

    .line 421
    iget-object v1, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 422
    iget-object v1, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    move v3, v0

    .line 423
    goto/16 :goto_5

    .line 386
    :cond_15
    iget-boolean v0, p0, Ltv/periscope/android/view/x;->n:Z

    if-eqz v0, :cond_16

    .line 387
    iget-object v0, p0, Ltv/periscope/android/view/x;->i:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 388
    iput-boolean v3, p0, Ltv/periscope/android/view/x;->n:Z

    move v0, v3

    goto :goto_9

    .line 389
    :cond_16
    iget-boolean v0, p0, Ltv/periscope/android/view/x;->o:Z

    if-eqz v0, :cond_17

    .line 390
    iget-object v0, p0, Ltv/periscope/android/view/x;->j:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 391
    iget-boolean v2, p0, Ltv/periscope/android/view/x;->m:Z

    if-eqz v2, :cond_12

    iget-object v2, p0, Ltv/periscope/android/view/x;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v2, :cond_12

    .line 392
    iget-object v2, p0, Ltv/periscope/android/view/x;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v2, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    goto :goto_9

    .line 397
    :cond_17
    iget-object v0, p0, Ltv/periscope/android/view/x;->y:Landroid/view/VelocityTracker;

    .line 398
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    .line 399
    const/16 v4, 0x3e8

    iget v5, p0, Ltv/periscope/android/view/x;->e:I

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 400
    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v4

    .line 401
    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    .line 403
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v5, p0, Ltv/periscope/android/view/x;->d:I

    int-to-float v5, v5

    cmpl-float v2, v2, v5

    if-gtz v2, :cond_18

    .line 404
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v5, p0, Ltv/periscope/android/view/x;->d:I

    int-to-float v5, v5

    cmpl-float v2, v2, v5

    if-lez v2, :cond_19

    .line 405
    :cond_18
    iget-object v2, p0, Ltv/periscope/android/view/x;->j:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v5, p0, Ltv/periscope/android/view/x;->q:Landroid/view/MotionEvent;

    invoke-interface {v2, v5, p1, v0, v4}, Landroid/view/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto/16 :goto_9

    .line 426
    :pswitch_6
    invoke-direct {p0}, Ltv/periscope/android/view/x;->a()V

    goto/16 :goto_5

    :cond_19
    move v0, v3

    goto/16 :goto_9

    :cond_1a
    move v0, v3

    goto/16 :goto_8

    .line 274
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
