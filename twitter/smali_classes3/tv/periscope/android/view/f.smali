.class public Ltv/periscope/android/view/f;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Ltv/periscope/android/view/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ltv/periscope/android/view/o;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ltv/periscope/android/view/f;-><init>(Ltv/periscope/android/view/o;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Ltv/periscope/android/view/o;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/view/f;->a:Ljava/util/List;

    .line 29
    iput-object p1, p0, Ltv/periscope/android/view/f;->b:Ltv/periscope/android/view/o;

    .line 30
    return-void
.end method


# virtual methods
.method public a(I)Ltv/periscope/android/view/a;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Ltv/periscope/android/view/f;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/a;

    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ltv/periscope/android/view/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    iput-object p1, p0, Ltv/periscope/android/view/f;->a:Ljava/util/List;

    .line 34
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Ltv/periscope/android/view/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/f;->a(I)Ltv/periscope/android/view/a;

    move-result-object v0

    .line 39
    if-eqz v0, :cond_0

    iget-object v1, p0, Ltv/periscope/android/view/f;->b:Ltv/periscope/android/view/o;

    if-ne v0, v1, :cond_0

    .line 40
    const/4 v0, 0x2

    .line 42
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p0, p2}, Ltv/periscope/android/view/f;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 69
    invoke-virtual {p0, p2}, Ltv/periscope/android/view/f;->a(I)Ltv/periscope/android/view/a;

    move-result-object v0

    .line 70
    invoke-interface {v0}, Ltv/periscope/android/view/a;->f()Ltv/periscope/android/view/c;

    move-result-object v1

    check-cast p1, Ltv/periscope/android/view/d;

    invoke-virtual {v1, p1, v0, p2}, Ltv/periscope/android/view/c;->a(Ltv/periscope/android/view/d;Ltv/periscope/android/view/a;I)V

    .line 74
    :goto_0
    return-void

    .line 64
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/view/f;->b:Ltv/periscope/android/view/o;

    invoke-interface {v0, p1}, Ltv/periscope/android/view/o;->a(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    goto :goto_0

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 3

    .prologue
    .line 47
    packed-switch p2, :pswitch_data_0

    .line 53
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 54
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__action_sheet_row:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 55
    new-instance v0, Ltv/periscope/android/view/d;

    invoke-direct {v0, v1}, Ltv/periscope/android/view/d;-><init>(Landroid/view/View;)V

    :goto_0
    return-object v0

    .line 49
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/view/f;->b:Ltv/periscope/android/view/o;

    invoke-interface {v0, p1}, Ltv/periscope/android/view/o;->a(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
