.class public Ltv/periscope/android/view/TweetSheet;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/view/TweetSheet$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/EditText;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/Button;

.field private g:Ltv/periscope/android/view/TweetSheet$a;

.field private h:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 49
    invoke-direct {p0}, Ltv/periscope/android/view/TweetSheet;->c()V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    invoke-direct {p0}, Ltv/periscope/android/view/TweetSheet;->c()V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    invoke-direct {p0}, Ltv/periscope/android/view/TweetSheet;->c()V

    .line 60
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 83
    invoke-virtual {p0}, Ltv/periscope/android/view/TweetSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__tweet_sheet:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 85
    invoke-virtual {p0}, Ltv/periscope/android/view/TweetSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 86
    sget v0, Ltv/periscope/android/library/f$g;->twitter_icon:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/TweetSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v2, Ltv/periscope/android/library/f$d;->ps__twitter_blue:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 88
    sget v0, Ltv/periscope/android/library/f$g;->close:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/TweetSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 89
    sget v2, Ltv/periscope/android/library/f$d;->ps__bg_button_default:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 90
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    sget v0, Ltv/periscope/android/library/f$g;->container:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/TweetSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    sget v0, Ltv/periscope/android/library/f$g;->username:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/TweetSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/view/TweetSheet;->a:Landroid/widget/TextView;

    .line 95
    sget v0, Ltv/periscope/android/library/f$g;->tweet_text:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/TweetSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Ltv/periscope/android/view/TweetSheet;->d:Landroid/widget/EditText;

    .line 96
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 97
    sget v0, Ltv/periscope/android/library/f$g;->image_preview:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/TweetSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/view/TweetSheet;->e:Landroid/widget/ImageView;

    .line 98
    sget v0, Ltv/periscope/android/library/f$g;->tweet_url:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/TweetSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/view/TweetSheet;->b:Landroid/widget/TextView;

    .line 99
    sget v0, Ltv/periscope/android/library/f$g;->tweet_counter:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/TweetSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/view/TweetSheet;->c:Landroid/widget/TextView;

    .line 100
    sget v0, Ltv/periscope/android/library/f$g;->tweet_sheet_scrim:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/TweetSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/TweetSheet;->h:Landroid/view/View;

    .line 101
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->h:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    sget v0, Ltv/periscope/android/library/f$g;->tweet_button:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/TweetSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Ltv/periscope/android/view/TweetSheet;->f:Landroid/widget/Button;

    .line 103
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->f:Landroid/widget/Button;

    sget v1, Ltv/periscope/android/library/f$l;->ps__share_post_tweet:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 105
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 169
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->d:Landroid/widget/EditText;

    invoke-static {v0}, Ltv/periscope/android/util/n;->b(Landroid/view/View;)V

    .line 170
    return-void
.end method

.method public a(Ldae;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 122
    if-nez p2, :cond_0

    .line 123
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 124
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->e:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 129
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 127
    invoke-virtual {p0}, Ltv/periscope/android/view/TweetSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/view/TweetSheet;->e:Landroid/widget/ImageView;

    invoke-interface {p1, v0, p2, v1}, Ldae;->a(Landroid/content/Context;Ljava/io/File;Landroid/widget/ImageView;)V

    goto :goto_0
.end method

.method public a(Ldae;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 113
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->f:Landroid/widget/Button;

    sget v1, Ltv/periscope/android/library/f$l;->ps__share_post_tweet:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 114
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->a:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->d:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p3}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    invoke-virtual {p0, p1, p4}, Ltv/periscope/android/view/TweetSheet;->a(Ldae;Ljava/io/File;)V

    .line 119
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x17

    add-int/lit8 v0, v0, 0x1

    .line 152
    rsub-int v0, v0, 0x8c

    .line 153
    iget-object v1, p0, Ltv/periscope/android/view/TweetSheet;->c:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    if-gez v0, :cond_0

    .line 155
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Ltv/periscope/android/view/TweetSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$d;->ps__dark_red:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 156
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->f:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 161
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Ltv/periscope/android/view/TweetSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$d;->ps__light_grey:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 159
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->f:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->d:Landroid/widget/EditText;

    invoke-static {v0}, Ltv/periscope/android/util/n;->a(Landroid/view/View;)V

    .line 174
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 175
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public getScrim()Landroid/view/View;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->h:Landroid/view/View;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 179
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 180
    sget v1, Ltv/periscope/android/library/f$g;->close:I

    if-eq v0, v1, :cond_0

    sget v1, Ltv/periscope/android/library/f$g;->tweet_sheet_scrim:I

    if-ne v0, v1, :cond_2

    .line 181
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->g:Ltv/periscope/android/view/TweetSheet$a;

    invoke-interface {v0}, Ltv/periscope/android/view/TweetSheet$a;->d()V

    .line 187
    :cond_1
    :goto_0
    return-void

    .line 182
    :cond_2
    sget v1, Ltv/periscope/android/library/f$g;->tweet_button:I

    if-ne v0, v1, :cond_1

    .line 183
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->f:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 184
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->f:Landroid/widget/Button;

    sget v1, Ltv/periscope/android/library/f$l;->ps__share_post_tweeting:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 185
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->g:Ltv/periscope/android/view/TweetSheet$a;

    invoke-interface {v0}, Ltv/periscope/android/view/TweetSheet$a;->c()V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x3

    .line 64
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 65
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->f:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 66
    iget-object v1, p0, Ltv/periscope/android/view/TweetSheet;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 67
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 68
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 69
    sget v2, Ltv/periscope/android/library/f$g;->tweet_url:I

    invoke-virtual {v0, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 70
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 71
    sget v2, Ltv/periscope/android/library/f$g;->tweet_url:I

    invoke-virtual {v1, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 78
    :goto_0
    iget-object v2, p0, Ltv/periscope/android/view/TweetSheet;->f:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    iget-object v0, p0, Ltv/periscope/android/view/TweetSheet;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    return-void

    .line 73
    :cond_0
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 74
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 75
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 76
    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method public setCallbackListener(Ltv/periscope/android/view/TweetSheet$a;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Ltv/periscope/android/view/TweetSheet;->g:Ltv/periscope/android/view/TweetSheet$a;

    .line 109
    return-void
.end method
