.class Ltv/periscope/android/view/RootDragLayout$d;
.super Landroid/support/v4/widget/ViewDragHelper$Callback;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/view/RootDragLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/view/RootDragLayout;


# direct methods
.method private constructor <init>(Ltv/periscope/android/view/RootDragLayout;)V
    .locals 0

    .prologue
    .line 777
    iput-object p1, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-direct {p0}, Landroid/support/v4/widget/ViewDragHelper$Callback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/RootDragLayout$1;)V
    .locals 0

    .prologue
    .line 777
    invoke-direct {p0, p1}, Ltv/periscope/android/view/RootDragLayout$d;-><init>(Ltv/periscope/android/view/RootDragLayout;)V

    return-void
.end method


# virtual methods
.method public clampViewPositionVertical(Landroid/view/View;II)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 835
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 836
    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    iget v2, v2, Ltv/periscope/android/view/RootDragLayout;->c:I

    if-ne v1, v2, :cond_2

    .line 837
    int-to-float v1, p2

    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    iget v2, v2, Ltv/periscope/android/view/RootDragLayout;->a:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 838
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    iget v0, v0, Ltv/periscope/android/view/RootDragLayout;->a:F

    float-to-int v0, v0

    .line 849
    :cond_0
    :goto_0
    return v0

    .line 839
    :cond_1
    if-ltz p2, :cond_0

    .line 842
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v1, p3

    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v2}, Ltv/periscope/android/view/RootDragLayout;->c(Ltv/periscope/android/view/RootDragLayout;)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 845
    :cond_2
    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v1, p1}, Ltv/periscope/android/view/RootDragLayout;->a(Ltv/periscope/android/view/RootDragLayout;Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 846
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-virtual {v0}, Ltv/periscope/android/view/RootDragLayout;->getHeight()I

    move-result v0

    .line 847
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    sub-int v1, v0, v1

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public getOrderedChildIndex(I)I
    .locals 3

    .prologue
    .line 780
    move v0, p1

    :goto_0
    if-ltz v0, :cond_0

    .line 781
    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-virtual {v1, v0}, Ltv/periscope/android/view/RootDragLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 782
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    move p1, v0

    .line 786
    :cond_0
    return p1

    .line 780
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public getViewVerticalDragRange(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 822
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 823
    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    iget v1, v1, Ltv/periscope/android/view/RootDragLayout;->c:I

    if-ne v0, v1, :cond_0

    .line 824
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    iget v0, v0, Ltv/periscope/android/view/RootDragLayout;->a:F

    float-to-int v0, v0

    .line 829
    :goto_0
    return v0

    .line 826
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v0, p1}, Ltv/periscope/android/view/RootDragLayout;->a(Ltv/periscope/android/view/RootDragLayout;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 827
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_0

    .line 829
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onViewDragStateChanged(I)V
    .locals 2

    .prologue
    .line 791
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v0}, Ltv/periscope/android/view/RootDragLayout;->a(Ltv/periscope/android/view/RootDragLayout;)Ltv/periscope/android/view/RootDragLayout$c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 792
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v0}, Ltv/periscope/android/view/RootDragLayout;->a(Ltv/periscope/android/view/RootDragLayout;)Ltv/periscope/android/view/RootDragLayout$c;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v1}, Ltv/periscope/android/view/RootDragLayout;->b(Ltv/periscope/android/view/RootDragLayout;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/widget/ViewDragHelper;->getCapturedView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ltv/periscope/android/view/RootDragLayout$c;->a(Landroid/view/View;I)V

    .line 794
    :cond_0
    return-void
.end method

.method public onViewPositionChanged(Landroid/view/View;IIII)V
    .locals 7

    .prologue
    .line 798
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    iget-object v0, v0, Ltv/periscope/android/view/RootDragLayout;->i:Ltv/periscope/android/view/RootDragLayout$b;

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    iget-object v0, v0, Ltv/periscope/android/view/RootDragLayout;->i:Ltv/periscope/android/view/RootDragLayout$b;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Ltv/periscope/android/view/RootDragLayout$b;->a(Landroid/view/View;IIII)V

    .line 801
    :cond_0
    const/4 v2, 0x0

    .line 802
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 803
    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    iget v1, v1, Ltv/periscope/android/view/RootDragLayout;->c:I

    if-ne v0, v1, :cond_3

    .line 804
    int-to-float v0, p3

    iget-object v1, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    iget v1, v1, Ltv/periscope/android/view/RootDragLayout;->a:F

    div-float v2, v0, v1

    .line 812
    :cond_1
    :goto_0
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v0}, Ltv/periscope/android/view/RootDragLayout;->a(Ltv/periscope/android/view/RootDragLayout;)Ltv/periscope/android/view/RootDragLayout$c;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 813
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v0}, Ltv/periscope/android/view/RootDragLayout;->a(Ltv/periscope/android/view/RootDragLayout;)Ltv/periscope/android/view/RootDragLayout$c;

    move-result-object v0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Ltv/periscope/android/view/RootDragLayout$c;->a(Landroid/view/View;FIIII)V

    .line 815
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;

    .line 816
    iput v2, v0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->a:F

    .line 817
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-virtual {v0}, Ltv/periscope/android/view/RootDragLayout;->invalidate()V

    .line 818
    return-void

    .line 806
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v0, p1}, Ltv/periscope/android/view/RootDragLayout;->a(Ltv/periscope/android/view/RootDragLayout;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 807
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-virtual {v0}, Ltv/periscope/android/view/RootDragLayout;->getHeight()I

    move-result v0

    .line 808
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 809
    sub-int/2addr v0, p3

    int-to-float v0, v0

    int-to-float v1, v1

    div-float v2, v0, v1

    goto :goto_0
.end method

.method public onViewReleased(Landroid/view/View;FF)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 866
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v0}, Ltv/periscope/android/view/RootDragLayout;->f(Ltv/periscope/android/view/RootDragLayout;)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_4

    .line 867
    cmpl-float v0, p3, v4

    if-gtz v0, :cond_0

    cmpl-float v0, p3, v4

    if-nez v0, :cond_2

    .line 868
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    iget-object v3, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    iget v3, v3, Ltv/periscope/android/view/RootDragLayout;->b:F

    float-to-int v3, v3

    if-le v0, v3, :cond_2

    :cond_0
    move v3, v2

    .line 869
    :goto_0
    if-eqz v3, :cond_3

    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    iget v0, v0, Ltv/periscope/android/view/RootDragLayout;->a:F

    float-to-int v0, v0

    .line 870
    :goto_1
    iget-object v4, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v4}, Ltv/periscope/android/view/RootDragLayout;->b(Ltv/periscope/android/view/RootDragLayout;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {v4, v5, v0}, Landroid/support/v4/widget/ViewDragHelper;->settleCapturedViewAt(II)Z

    .line 871
    if-nez v3, :cond_1

    move v1, v2

    .line 880
    :cond_1
    :goto_2
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-virtual {v0}, Ltv/periscope/android/view/RootDragLayout;->invalidate()V

    .line 881
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v0, p1, v1}, Ltv/periscope/android/view/RootDragLayout;->a(Ltv/periscope/android/view/RootDragLayout;Landroid/view/View;Z)V

    .line 882
    return-void

    :cond_2
    move v3, v1

    .line 868
    goto :goto_0

    :cond_3
    move v0, v1

    .line 869
    goto :goto_1

    .line 873
    :cond_4
    iget-object v0, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-virtual {v0}, Ltv/periscope/android/view/RootDragLayout;->getHeight()I

    move-result v0

    .line 874
    cmpl-float v3, p3, v4

    if-gtz v3, :cond_5

    cmpl-float v3, p3, v4

    if-nez v3, :cond_6

    .line 875
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    div-int/lit8 v4, v0, 0x2

    if-le v3, v4, :cond_6

    :cond_5
    move v1, v2

    .line 876
    :cond_6
    if-eqz v1, :cond_7

    .line 877
    :goto_3
    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v2}, Ltv/periscope/android/view/RootDragLayout;->b(Ltv/periscope/android/view/RootDragLayout;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/support/v4/widget/ViewDragHelper;->settleCapturedViewAt(II)Z

    goto :goto_2

    .line 876
    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    goto :goto_3
.end method

.method public tryCaptureView(Landroid/view/View;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 855
    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v2, p1}, Ltv/periscope/android/view/RootDragLayout;->a(Ltv/periscope/android/view/RootDragLayout;Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 856
    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v2, p1}, Ltv/periscope/android/view/RootDragLayout;->b(Ltv/periscope/android/view/RootDragLayout;Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    .line 857
    invoke-static {v2}, Ltv/periscope/android/view/RootDragLayout;->d(Ltv/periscope/android/view/RootDragLayout;)F

    move-result v2

    iget-object v3, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v3}, Ltv/periscope/android/view/RootDragLayout;->e(Ltv/periscope/android/view/RootDragLayout;)F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {p1, v2}, Landroid/view/View;->canScrollVertically(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 859
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 857
    goto :goto_0

    .line 859
    :cond_2
    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v2, p1}, Ltv/periscope/android/view/RootDragLayout;->b(Ltv/periscope/android/view/RootDragLayout;Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Ltv/periscope/android/view/RootDragLayout$d;->a:Ltv/periscope/android/view/RootDragLayout;

    invoke-static {v2}, Ltv/periscope/android/view/RootDragLayout;->f(Ltv/periscope/android/view/RootDragLayout;)Landroid/view/View;

    move-result-object v2

    if-eq p1, v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
