.class public Ltv/periscope/android/view/UsernameBadgeView;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# instance fields
.field final a:Landroid/widget/ImageView;

.field final b:Landroid/widget/ImageView;

.field private final c:Ltv/periscope/android/view/PsTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ltv/periscope/android/view/UsernameBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/view/UsernameBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__username_view:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 47
    sget v0, Ltv/periscope/android/library/f$g;->username_text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsTextView;

    iput-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->c:Ltv/periscope/android/view/PsTextView;

    .line 48
    sget v0, Ltv/periscope/android/library/f$g;->vip_badge:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->a:Landroid/widget/ImageView;

    .line 49
    sget v0, Ltv/periscope/android/library/f$g;->superfans_badge:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->b:Landroid/widget/ImageView;

    .line 50
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->c:Ltv/periscope/android/view/PsTextView;

    invoke-static {p1, p2, v0}, Ltv/periscope/android/view/UsernameBadgeView;->a(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/widget/TextView;)V

    .line 51
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/widget/TextView;)V
    .locals 4

    .prologue
    .line 55
    invoke-static {p0, p1, p2}, Ltv/periscope/android/view/z;->a(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/widget/TextView;)V

    .line 57
    sget-object v0, Ltv/periscope/android/library/f$n;->UsernameBadgeView:[I

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 58
    sget v1, Ltv/periscope/android/library/f$n;->UsernameBadgeView_ps__usernameTextColor:I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Ltv/periscope/android/library/f$d;->ps__secondary_text:I

    .line 59
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 58
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 60
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 61
    sget v1, Ltv/periscope/android/library/f$n;->UsernameBadgeView_ps__usernameTextSize:I

    .line 62
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Ltv/periscope/android/library/f$e;->ps__standard_text_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 61
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    int-to-float v1, v1

    .line 63
    const/4 v2, 0x0

    invoke-virtual {p2, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 65
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 66
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->b:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 104
    return-void
.end method

.method public a(ZZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 89
    iget-object v3, p0, Ltv/periscope/android/view/UsernameBadgeView;->c:Ltv/periscope/android/view/PsTextView;

    if-eqz p2, :cond_0

    sget v0, Ltv/periscope/android/library/f$f;->ps__ic_bluebird_user:I

    move v2, v0

    :goto_0
    if-eqz p1, :cond_1

    sget v0, Ltv/periscope/android/library/f$f;->ps__ic_verified:I

    :goto_1
    invoke-virtual {v3, v2, v1, v0, v1}, Ltv/periscope/android/view/PsTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 94
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->c:Ltv/periscope/android/view/PsTextView;

    invoke-virtual {p0}, Ltv/periscope/android/view/UsernameBadgeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$e;->ps__drawable_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsTextView;->setCompoundDrawablePadding(I)V

    .line 95
    return-void

    :cond_0
    move v2, v1

    .line 89
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public getBadgeView()Landroid/view/View;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->a:Landroid/widget/ImageView;

    return-object v0
.end method

.method public setSuperfansIcon(I)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 98
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->b:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, p1, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 100
    return-void
.end method

.method public setText(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 69
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->c:Ltv/periscope/android/view/PsTextView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/PsTextView;->setText(I)V

    .line 70
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->c:Ltv/periscope/android/view/PsTextView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/PsTextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    return-void
.end method

.method public setTextColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 77
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->c:Ltv/periscope/android/view/PsTextView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/PsTextView;->setTextColor(I)V

    .line 78
    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 81
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->c:Ltv/periscope/android/view/PsTextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsTextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->c:Ltv/periscope/android/view/PsTextView;

    invoke-virtual {p0}, Ltv/periscope/android/view/UsernameBadgeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1}, Ltv/periscope/android/util/ab;->a(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setVipStatus(Ltv/periscope/android/api/PsUser$VipBadge;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 107
    sget-object v0, Ltv/periscope/android/view/UsernameBadgeView$1;->a:[I

    invoke-virtual {p1}, Ltv/periscope/android/api/PsUser$VipBadge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 125
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->a:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 129
    :goto_0
    return-void

    .line 109
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, Ltv/periscope/android/view/UsernameBadgeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$f;->ps__gold_badge:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 114
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, Ltv/periscope/android/view/UsernameBadgeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$f;->ps__silver_badge:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 119
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Ltv/periscope/android/view/UsernameBadgeView;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, Ltv/periscope/android/view/UsernameBadgeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$f;->ps__bronze_badge:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
