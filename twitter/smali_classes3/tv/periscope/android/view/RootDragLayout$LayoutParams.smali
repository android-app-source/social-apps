.class public Ltv/periscope/android/view/RootDragLayout$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/view/RootDragLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field a:F

.field b:Z

.field public c:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 761
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 750
    const/4 v0, -0x1

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->c:I

    .line 762
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 753
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 750
    const/4 v0, -0x1

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->c:I

    .line 755
    invoke-static {}, Ltv/periscope/android/view/RootDragLayout;->g()[I

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 756
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->c:I

    .line 757
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 758
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 769
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 750
    const/4 v0, -0x1

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->c:I

    .line 770
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 1

    .prologue
    .line 773
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 750
    const/4 v0, -0x1

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->c:I

    .line 774
    return-void
.end method

.method public constructor <init>(Ltv/periscope/android/view/RootDragLayout$LayoutParams;)V
    .locals 1

    .prologue
    .line 765
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 750
    const/4 v0, -0x1

    iput v0, p0, Ltv/periscope/android/view/RootDragLayout$LayoutParams;->c:I

    .line 766
    return-void
.end method
