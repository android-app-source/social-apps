.class public Ltv/periscope/android/view/PlayerView;
.super Landroid/widget/FrameLayout;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/player/c;


# instance fields
.field private a:Ltv/periscope/android/view/FuzzyBalls;

.field private b:Landroid/view/TextureView;

.field private c:Landroid/widget/FrameLayout;

.field private d:Landroid/widget/ImageView;

.field private e:Ltv/periscope/android/view/v;

.field private f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

.field private g:Ltv/periscope/android/view/PsLoading;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/ImageView;

.field private j:Z

.field private k:Ldae;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/view/PlayerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/view/PlayerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/view/PlayerView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 58
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__play_view:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 60
    sget v0, Ltv/periscope/android/library/f$g;->preview_frame:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/PlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Ltv/periscope/android/view/PlayerView;->c:Landroid/widget/FrameLayout;

    .line 61
    sget v0, Ltv/periscope/android/library/f$g;->thumb:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/PlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/view/PlayerView;->d:Landroid/widget/ImageView;

    .line 62
    new-instance v0, Ltv/periscope/android/view/PlayerView$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/view/PlayerView$1;-><init>(Ltv/periscope/android/view/PlayerView;)V

    iput-object v0, p0, Ltv/periscope/android/view/PlayerView;->e:Ltv/periscope/android/view/v;

    .line 69
    sget v0, Ltv/periscope/android/library/f$g;->chatroom_view:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/PlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iput-object v0, p0, Ltv/periscope/android/view/PlayerView;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    .line 71
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setHeartsMarginFactor(I)V

    .line 73
    sget v0, Ltv/periscope/android/library/f$g;->loading_animation:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/PlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsLoading;

    iput-object v0, p0, Ltv/periscope/android/view/PlayerView;->g:Ltv/periscope/android/view/PsLoading;

    .line 74
    sget v0, Ltv/periscope/android/library/f$g;->loading_text:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/PlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/view/PlayerView;->h:Landroid/widget/TextView;

    .line 76
    sget v0, Ltv/periscope/android/library/f$g;->btn_play_icon:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/PlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/view/PlayerView;->i:Landroid/widget/ImageView;

    .line 77
    return-void
.end method


# virtual methods
.method public L()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->c:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 126
    return-void
.end method

.method public M()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->c:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 131
    return-void
.end method

.method public N()V
    .locals 8

    .prologue
    .line 167
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->a:Ltv/periscope/android/view/FuzzyBalls;

    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {p0}, Ltv/periscope/android/view/PlayerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$d;->ps__dark_grey:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 169
    iget-object v1, p0, Ltv/periscope/android/view/PlayerView;->a:Ltv/periscope/android/view/FuzzyBalls;

    const-wide/16 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v1 .. v7}, Ltv/periscope/android/view/FuzzyBalls;->a(IIIIJ)V

    .line 171
    :cond_0
    return-void
.end method

.method public O()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->g:Ltv/periscope/android/view/PsLoading;

    invoke-virtual {v0}, Ltv/periscope/android/view/PsLoading;->c()V

    .line 185
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    return-void
.end method

.method public P()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Q()V
    .locals 0

    .prologue
    .line 200
    return-void
.end method

.method public R()V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->d:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 236
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 237
    return-void
.end method

.method public S()V
    .locals 4

    .prologue
    .line 241
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xb4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/view/PlayerView;->e:Ltv/periscope/android/view/v;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 242
    return-void
.end method

.method public T()V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->i:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_play:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 247
    invoke-virtual {p0}, Ltv/periscope/android/view/PlayerView;->V()V

    .line 248
    return-void
.end method

.method public U()V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->i:Landroid/widget/ImageView;

    sget v1, Ltv/periscope/android/library/f$f;->ps__ic_pause:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 253
    invoke-virtual {p0}, Ltv/periscope/android/view/PlayerView;->V()V

    .line 254
    return-void
.end method

.method public V()V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->i:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 259
    return-void
.end method

.method public W()V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 264
    return-void
.end method

.method public a(F)V
    .locals 4

    .prologue
    .line 204
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->b:Landroid/view/TextureView;

    if-nez v0, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->b:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {p0}, Ltv/periscope/android/view/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x19

    new-instance v3, Ltv/periscope/android/view/PlayerView$2;

    invoke-direct {v3, p0}, Ltv/periscope/android/view/PlayerView$2;-><init>(Ltv/periscope/android/view/PlayerView;)V

    invoke-static {v1, v0, p1, v2, v3}, Ltv/periscope/android/util/m;->a(Landroid/content/Context;Landroid/graphics/Bitmap;FILtv/periscope/android/util/m$b;)V

    .line 220
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 223
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->g:Ltv/periscope/android/view/PsLoading;

    invoke-virtual {v0}, Ltv/periscope/android/view/PsLoading;->b()V

    .line 176
    invoke-static {p1}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 180
    :cond_0
    return-void
.end method

.method public a(IJ)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 135
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->b:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/view/PlayerView;->j:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 162
    :goto_0
    return v0

    .line 139
    :cond_1
    const/4 v3, 0x0

    .line 141
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->a:Ltv/periscope/android/view/FuzzyBalls;

    if-nez v0, :cond_2

    .line 143
    sget v0, Ltv/periscope/android/library/f$g;->fuzzy_balls_stub:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/PlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/FuzzyBalls;

    iput-object v0, p0, Ltv/periscope/android/view/PlayerView;->a:Ltv/periscope/android/view/FuzzyBalls;

    .line 146
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->b:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 147
    if-eqz v0, :cond_4

    .line 148
    :try_start_1
    iget-object v3, p0, Ltv/periscope/android/view/PlayerView;->a:Ltv/periscope/android/view/FuzzyBalls;

    invoke-virtual {v3, v0, p1, p2, p3}, Ltv/periscope/android/view/FuzzyBalls;->a(Landroid/graphics/Bitmap;IJ)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 157
    :goto_1
    if-eqz v0, :cond_3

    .line 158
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    move v0, v1

    .line 152
    goto :goto_0

    .line 150
    :cond_4
    :try_start_2
    invoke-virtual {p0}, Ltv/periscope/android/view/PlayerView;->N()V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 154
    :catch_0
    move-exception v1

    .line 155
    :goto_2
    const/4 v1, 0x1

    :try_start_3
    iput-boolean v1, p0, Ltv/periscope/android/view/PlayerView;->j:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 157
    if-eqz v0, :cond_5

    .line 158
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    move v0, v2

    .line 162
    goto :goto_0

    .line 157
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v3, :cond_6

    .line 158
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    :cond_6
    throw v0

    .line 157
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_3

    .line 154
    :catch_1
    move-exception v0

    move-object v0, v3

    goto :goto_2
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 195
    invoke-virtual {p0}, Ltv/periscope/android/view/PlayerView;->O()V

    .line 196
    return-void
.end method

.method public getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->f:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    return-object v0
.end method

.method public getPreview()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->c:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getTextureView()Landroid/view/TextureView;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->b:Landroid/view/TextureView;

    return-object v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 83
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->a:Ltv/periscope/android/view/FuzzyBalls;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->a:Ltv/periscope/android/view/FuzzyBalls;

    invoke-virtual {v0}, Ltv/periscope/android/view/FuzzyBalls;->a()V

    .line 86
    :cond_0
    return-void
.end method

.method public setPlayPauseAlpha(F)V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 269
    return-void
.end method

.method public setPlayPauseClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    return-void
.end method

.method public setTextureView(Landroid/view/TextureView;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 97
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 98
    iput-object p1, p0, Ltv/periscope/android/view/PlayerView;->b:Landroid/view/TextureView;

    .line 99
    return-void
.end method

.method public setThumbImageUrlLoader(Ldae;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Ltv/periscope/android/view/PlayerView;->k:Ldae;

    .line 116
    return-void
.end method

.method public setThumbnail(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->k:Ldae;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Ltv/periscope/android/view/PlayerView;->k:Ldae;

    invoke-virtual {p0}, Ltv/periscope/android/view/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/view/PlayerView;->d:Landroid/widget/ImageView;

    invoke-interface {v0, v1, p1, v2}, Ldae;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 231
    :cond_0
    return-void
.end method
