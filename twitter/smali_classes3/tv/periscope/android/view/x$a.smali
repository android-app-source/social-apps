.class Ltv/periscope/android/view/x$a;
.super Landroid/os/Handler;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/view/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/view/x;


# direct methods
.method constructor <init>(Ltv/periscope/android/view/x;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Ltv/periscope/android/view/x$a;->a:Ltv/periscope/android/view/x;

    .line 87
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 88
    return-void
.end method

.method constructor <init>(Ltv/periscope/android/view/x;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 90
    iput-object p1, p0, Ltv/periscope/android/view/x$a;->a:Ltv/periscope/android/view/x;

    .line 91
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 92
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 96
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 117
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :pswitch_0
    iget-object v0, p0, Ltv/periscope/android/view/x$a;->a:Ltv/periscope/android/view/x;

    invoke-static {v0}, Ltv/periscope/android/view/x;->b(Ltv/periscope/android/view/x;)Landroid/view/GestureDetector$OnGestureListener;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/view/x$a;->a:Ltv/periscope/android/view/x;

    invoke-static {v1}, Ltv/periscope/android/view/x;->a(Ltv/periscope/android/view/x;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onShowPress(Landroid/view/MotionEvent;)V

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 102
    :pswitch_1
    iget-object v0, p0, Ltv/periscope/android/view/x$a;->a:Ltv/periscope/android/view/x;

    invoke-static {v0}, Ltv/periscope/android/view/x;->c(Ltv/periscope/android/view/x;)V

    goto :goto_0

    .line 107
    :pswitch_2
    iget-object v0, p0, Ltv/periscope/android/view/x$a;->a:Ltv/periscope/android/view/x;

    invoke-static {v0}, Ltv/periscope/android/view/x;->d(Ltv/periscope/android/view/x;)Landroid/view/GestureDetector$OnDoubleTapListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Ltv/periscope/android/view/x$a;->a:Ltv/periscope/android/view/x;

    invoke-static {v0}, Ltv/periscope/android/view/x;->e(Ltv/periscope/android/view/x;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 109
    iget-object v0, p0, Ltv/periscope/android/view/x$a;->a:Ltv/periscope/android/view/x;

    invoke-static {v0}, Ltv/periscope/android/view/x;->d(Ltv/periscope/android/view/x;)Landroid/view/GestureDetector$OnDoubleTapListener;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/view/x$a;->a:Ltv/periscope/android/view/x;

    invoke-static {v1}, Ltv/periscope/android/view/x;->a(Ltv/periscope/android/view/x;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 111
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/view/x$a;->a:Ltv/periscope/android/view/x;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ltv/periscope/android/view/x;->a(Ltv/periscope/android/view/x;Z)Z

    goto :goto_0

    .line 96
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
