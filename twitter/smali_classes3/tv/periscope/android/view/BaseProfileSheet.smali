.class public abstract Ltv/periscope/android/view/BaseProfileSheet;
.super Landroid/widget/FrameLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ltv/periscope/android/view/s;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/view/BaseProfileSheet$a;
    }
.end annotation


# instance fields
.field final a:Landroid/view/View;

.field final b:Landroid/widget/TextView;

.field final c:Ltv/periscope/android/view/UsernameBadgeView;

.field final d:Landroid/widget/TextView;

.field final e:Landroid/view/View;

.field protected final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbx;",
            ">;"
        }
    .end annotation
.end field

.field g:Ltv/periscope/android/view/t;

.field h:Ltv/periscope/android/api/PsUser;

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field j:Z

.field k:Z

.field private final l:Landroid/widget/TextView;

.field private final m:Landroid/widget/ImageView;

.field private final n:Landroid/widget/ImageView;

.field private final o:Landroid/view/View;

.field private final p:Landroid/view/View;

.field private final q:Ltv/periscope/android/view/v;

.field private final r:Ltv/periscope/android/view/v;

.field private s:Landroid/view/animation/Animation;

.field private t:Landroid/app/Dialog;

.field private u:Ltv/periscope/android/ui/d;

.field private v:Z

.field private w:Z

.field private x:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 70
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Ltv/periscope/android/view/BaseProfileSheet;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/view/BaseProfileSheet;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->f:Ljava/util/List;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->i:Ljava/util/List;

    .line 79
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/BaseProfileSheet;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->a:Landroid/view/View;

    .line 80
    sget v0, Ltv/periscope/android/library/f$g;->sheet_inner:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/BaseProfileSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 83
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->o:Landroid/view/View;

    .line 87
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->a:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->display_name:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->b:Landroid/widget/TextView;

    .line 88
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->a:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->username:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/UsernameBadgeView;

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->c:Ltv/periscope/android/view/UsernameBadgeView;

    .line 89
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->a:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->description:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->d:Landroid/widget/TextView;

    .line 90
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->d:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 91
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->a:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->profile_image_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->e:Landroid/view/View;

    .line 92
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->e:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->a:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->profile_image:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->m:Landroid/widget/ImageView;

    .line 94
    sget v0, Ltv/periscope/android/library/f$g;->more:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/BaseProfileSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->n:Landroid/widget/ImageView;

    .line 95
    sget v0, Ltv/periscope/android/library/f$g;->dim_bg:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/BaseProfileSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->p:Landroid/view/View;

    .line 96
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->a:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->watch_live:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->l:Landroid/widget/TextView;

    .line 98
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Ltv/periscope/android/view/BaseProfileSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ltv/periscope/android/library/f$l;->ps__watch_live:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    invoke-virtual {p0}, Ltv/periscope/android/view/BaseProfileSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    iput v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->x:I

    .line 101
    iget-object v1, p0, Ltv/periscope/android/view/BaseProfileSheet;->i:Ljava/util/List;

    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->a:Landroid/view/View;

    sget v2, Ltv/periscope/android/library/f$g;->profile_image_badge_layer1:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    iget-object v1, p0, Ltv/periscope/android/view/BaseProfileSheet;->i:Ljava/util/List;

    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->a:Landroid/view/View;

    sget v2, Ltv/periscope/android/library/f$g;->profile_image_badge_layer2:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v1, p0, Ltv/periscope/android/view/BaseProfileSheet;->i:Ljava/util/List;

    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->a:Landroid/view/View;

    sget v2, Ltv/periscope/android/library/f$g;->profile_image_badge_layer3:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object v1, p0, Ltv/periscope/android/view/BaseProfileSheet;->i:Ljava/util/List;

    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->a:Landroid/view/View;

    sget v2, Ltv/periscope/android/library/f$g;->profile_image_badge_layer4:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v1, p0, Ltv/periscope/android/view/BaseProfileSheet;->i:Ljava/util/List;

    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->a:Landroid/view/View;

    sget v2, Ltv/periscope/android/library/f$g;->profile_image_badge_layer5:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    new-instance v0, Ltv/periscope/android/view/BaseProfileSheet$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/view/BaseProfileSheet$1;-><init>(Ltv/periscope/android/view/BaseProfileSheet;)V

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->q:Ltv/periscope/android/view/v;

    .line 125
    new-instance v0, Ltv/periscope/android/view/BaseProfileSheet$2;

    invoke-direct {v0, p0}, Ltv/periscope/android/view/BaseProfileSheet$2;-><init>(Ltv/periscope/android/view/BaseProfileSheet;)V

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->r:Ltv/periscope/android/view/v;

    .line 155
    invoke-direct {p0}, Ltv/periscope/android/view/BaseProfileSheet;->m()V

    .line 156
    invoke-direct {p0}, Ltv/periscope/android/view/BaseProfileSheet;->l()V

    .line 158
    invoke-virtual {p0, v3}, Ltv/periscope/android/view/BaseProfileSheet;->setClipChildren(Z)V

    .line 159
    invoke-virtual {p0, v3}, Ltv/periscope/android/view/BaseProfileSheet;->setClipToPadding(Z)V

    .line 161
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/BaseProfileSheet;->setVisibility(I)V

    .line 162
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/view/BaseProfileSheet;Ltv/periscope/android/ui/d;)Ltv/periscope/android/ui/d;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Ltv/periscope/android/view/BaseProfileSheet;->u:Ltv/periscope/android/ui/d;

    return-object p1
.end method

.method static synthetic a(Ltv/periscope/android/view/BaseProfileSheet;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->w:Z

    return v0
.end method

.method static synthetic a(Ltv/periscope/android/view/BaseProfileSheet;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Ltv/periscope/android/view/BaseProfileSheet;->v:Z

    return p1
.end method

.method static synthetic b(Ltv/periscope/android/view/BaseProfileSheet;)Ltv/periscope/android/ui/d;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->u:Ltv/periscope/android/ui/d;

    return-object v0
.end method

.method static synthetic b(Ltv/periscope/android/view/BaseProfileSheet;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Ltv/periscope/android/view/BaseProfileSheet;->w:Z

    return p1
.end method

.method static synthetic c(Ltv/periscope/android/view/BaseProfileSheet;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->l:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Ltv/periscope/android/view/BaseProfileSheet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->p:Landroid/view/View;

    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 191
    invoke-virtual {p0}, Ltv/periscope/android/view/BaseProfileSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$a;->ps__overshoot_from_bottom:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->s:Landroid/view/animation/Animation;

    .line 192
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->s:Landroid/view/animation/Animation;

    new-instance v1, Ltv/periscope/android/view/BaseProfileSheet$3;

    invoke-direct {v1, p0}, Ltv/periscope/android/view/BaseProfileSheet$3;-><init>(Ltv/periscope/android/view/BaseProfileSheet;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 200
    return-void
.end method

.method private m()V
    .locals 4

    .prologue
    .line 203
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    invoke-virtual {p0}, Ltv/periscope/android/view/BaseProfileSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 206
    new-instance v1, Ltv/periscope/android/view/BaseProfileSheet$a;

    sget v2, Ltv/periscope/android/library/f$i;->ps__dialog_simple_list_item:I

    iget-object v3, p0, Ltv/periscope/android/view/BaseProfileSheet;->f:Ljava/util/List;

    invoke-direct {v1, v0, v2, v3}, Ltv/periscope/android/view/BaseProfileSheet$a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 207
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Ltv/periscope/android/view/BaseProfileSheet;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Ltv/periscope/android/library/f$m;->ps__BaseDialogStyle:I

    invoke-direct {v0, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    new-instance v2, Ltv/periscope/android/view/BaseProfileSheet$4;

    invoke-direct {v2, p0}, Ltv/periscope/android/view/BaseProfileSheet$4;-><init>(Ltv/periscope/android/view/BaseProfileSheet;)V

    .line 208
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->t:Landroid/app/Dialog;

    .line 223
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/content/Context;)Landroid/view/View;
.end method

.method protected abstract a()V
.end method

.method public a(Ltv/periscope/android/api/PsUser;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 263
    if-nez p1, :cond_0

    .line 281
    :goto_0
    return-void

    .line 266
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ltv/periscope/android/view/BaseProfileSheet;->a(Ltv/periscope/android/api/PsUser;I)V

    .line 267
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->c:Ltv/periscope/android/view/UsernameBadgeView;

    iget-boolean v1, p1, Ltv/periscope/android/api/PsUser;->isVerified:Z

    iget-boolean v2, p1, Ltv/periscope/android/api/PsUser;->isBluebirdUser:Z

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/view/UsernameBadgeView;->a(ZZ)V

    .line 269
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->b:Landroid/widget/TextView;

    iget-object v1, p1, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->c:Ltv/periscope/android/view/UsernameBadgeView;

    iget-object v1, p1, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/UsernameBadgeView;->setUsername(Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->d:Landroid/widget/TextView;

    iget-object v1, p1, Ltv/periscope/android/api/PsUser;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    invoke-virtual {v0}, Ltv/periscope/android/api/PsUser;->getProfileUrlLarge()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    .line 274
    invoke-virtual {v0}, Ltv/periscope/android/api/PsUser;->getProfileUrlLarge()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ltv/periscope/android/api/PsUser;->getProfileUrlLarge()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 280
    :cond_1
    :goto_1
    iput-object p1, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    goto :goto_0

    .line 276
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->g:Ltv/periscope/android/view/t;

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->g:Ltv/periscope/android/view/t;

    invoke-interface {v0}, Ltv/periscope/android/view/t;->d()Ldae;

    move-result-object v0

    invoke-virtual {p0}, Ltv/periscope/android/view/BaseProfileSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/android/api/PsUser;->getProfileUrlLarge()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/view/BaseProfileSheet;->m:Landroid/widget/ImageView;

    invoke-interface {v0, v1, v2, v3}, Ldae;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V

    goto :goto_1
.end method

.method protected a(Ltv/periscope/android/api/PsUser;I)V
    .locals 2

    .prologue
    .line 284
    if-eqz p1, :cond_1

    iget-object v0, p1, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    .line 285
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/BaseProfileSheet;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 286
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->n:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 290
    :goto_1
    return-void

    .line 284
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 288
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Ltv/periscope/android/ui/d;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 321
    iget-boolean v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->k:Z

    if-eqz v0, :cond_1

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 325
    :cond_1
    iput-object p1, p0, Ltv/periscope/android/view/BaseProfileSheet;->u:Ltv/periscope/android/ui/d;

    .line 327
    invoke-virtual {p0}, Ltv/periscope/android/view/BaseProfileSheet;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->p:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 331
    new-instance v1, Ltv/periscope/android/view/n;

    iget-object v2, p0, Ltv/periscope/android/view/BaseProfileSheet;->p:Landroid/view/View;

    invoke-direct {v1, v2}, Ltv/periscope/android/view/n;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 333
    iget-object v1, p0, Ltv/periscope/android/view/BaseProfileSheet;->o:Landroid/view/View;

    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Ltv/periscope/android/view/BaseProfileSheet;->x:I

    int-to-float v5, v5

    aput v5, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 334
    invoke-virtual {p0}, Ltv/periscope/android/view/BaseProfileSheet;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Ltv/periscope/android/view/e;->a(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 335
    iget-object v2, p0, Ltv/periscope/android/view/BaseProfileSheet;->r:Ltv/periscope/android/view/v;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 337
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 338
    invoke-virtual {v2, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 339
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 330
    nop

    :array_0
    .array-data 4
        0x3f19999a    # 0.6f
        0x0
    .end array-data
.end method

.method protected a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->g:Ltv/periscope/android/view/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->g:Ltv/periscope/android/view/t;

    invoke-interface {v0}, Ltv/periscope/android/view/t;->e()Lcyw;

    move-result-object v0

    invoke-interface {v0}, Lcyw;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    if-nez v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 232
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->f:Ljava/util/List;

    iget-object v1, p0, Ltv/periscope/android/view/BaseProfileSheet;->g:Ltv/periscope/android/view/t;

    iget-object v2, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    iget-object v2, v2, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ltv/periscope/android/view/t;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    if-eqz v0, :cond_0

    .line 237
    invoke-virtual {p0}, Ltv/periscope/android/view/BaseProfileSheet;->b()V

    .line 238
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->t:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 240
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->g:Ltv/periscope/android/view/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->g:Ltv/periscope/android/view/t;

    invoke-interface {v0}, Ltv/periscope/android/view/t;->e()Lcyw;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    iget-object v1, v1, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/BaseProfileSheet;->a(Ltv/periscope/android/api/PsUser;)V

    .line 250
    :cond_0
    return-void
.end method

.method public e()V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 255
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 256
    iput-object v1, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    .line 257
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->w:Z

    .line 345
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/BaseProfileSheet;->a(Ltv/periscope/android/ui/d;)V

    .line 346
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->v:Z

    return v0
.end method

.method public getCurrentUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    if-nez v0, :cond_0

    .line 358
    const/4 v0, 0x0

    .line 360
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    iget-object v0, v0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    goto :goto_0
.end method

.method public h()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    .line 370
    iget-boolean v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->k:Z

    if-eqz v0, :cond_0

    .line 395
    :goto_0
    return-void

    .line 374
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->p:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 375
    new-instance v1, Ltv/periscope/android/view/BaseProfileSheet$5;

    invoke-direct {v1, p0}, Ltv/periscope/android/view/BaseProfileSheet$5;-><init>(Ltv/periscope/android/view/BaseProfileSheet;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 382
    iget-object v1, p0, Ltv/periscope/android/view/BaseProfileSheet;->o:Landroid/view/View;

    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v3, v3, [F

    const/4 v4, 0x0

    iget v5, p0, Ltv/periscope/android/view/BaseProfileSheet;->x:I

    int-to-float v5, v5

    aput v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput v5, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 383
    invoke-virtual {p0}, Ltv/periscope/android/view/BaseProfileSheet;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Ltv/periscope/android/view/e;->b(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 384
    iget-object v2, p0, Ltv/periscope/android/view/BaseProfileSheet;->q:Ltv/periscope/android/view/v;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 386
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 387
    new-instance v3, Ltv/periscope/android/view/BaseProfileSheet$6;

    invoke-direct {v3, p0}, Ltv/periscope/android/view/BaseProfileSheet$6;-><init>(Ltv/periscope/android/view/BaseProfileSheet;)V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 393
    invoke-virtual {v2, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 394
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 374
    nop

    :array_0
    .array-data 4
        0x0
        0x3f19999a    # 0.6f
    .end array-data
.end method

.method public i()V
    .locals 1

    .prologue
    .line 399
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/BaseProfileSheet;->a(Ltv/periscope/android/ui/d;)V

    .line 400
    return-void
.end method

.method j()V
    .locals 0

    .prologue
    .line 404
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->g:Ltv/periscope/android/view/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->h:Ltv/periscope/android/api/PsUser;

    if-nez v0, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 174
    sget v1, Ltv/periscope/android/library/f$g;->dim_bg:I

    if-ne v0, v1, :cond_2

    .line 175
    invoke-virtual {p0}, Ltv/periscope/android/view/BaseProfileSheet;->i()V

    goto :goto_0

    .line 176
    :cond_2
    sget v1, Ltv/periscope/android/library/f$g;->more:I

    if-ne v0, v1, :cond_3

    .line 177
    invoke-virtual {p0}, Ltv/periscope/android/view/BaseProfileSheet;->c()V

    goto :goto_0

    .line 178
    :cond_3
    sget v1, Ltv/periscope/android/library/f$g;->profile_image_container:I

    if-ne v0, v1, :cond_0

    .line 179
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->l:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 182
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet;->l:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/p;

    .line 183
    if-eqz v0, :cond_0

    .line 184
    iget-object v1, p0, Ltv/periscope/android/view/BaseProfileSheet;->g:Ltv/periscope/android/view/t;

    invoke-virtual {v0}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ltv/periscope/android/view/t;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDelegate(Ltv/periscope/android/view/t;)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 351
    iput-object p1, p0, Ltv/periscope/android/view/BaseProfileSheet;->g:Ltv/periscope/android/view/t;

    .line 352
    return-void
.end method
