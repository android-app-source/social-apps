.class public Ltv/periscope/android/view/ag;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/a;


# instance fields
.field private final a:Ltv/periscope/android/view/a;

.field private final b:Ltv/periscope/android/view/a;

.field private c:Ltv/periscope/android/view/a;


# direct methods
.method public constructor <init>(Ltv/periscope/android/view/a;Ltv/periscope/android/view/a;)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Ltv/periscope/android/view/ag;->a:Ltv/periscope/android/view/a;

    .line 13
    iput-object p2, p0, Ltv/periscope/android/view/ag;->b:Ltv/periscope/android/view/a;

    .line 15
    iget-object v0, p0, Ltv/periscope/android/view/ag;->a:Ltv/periscope/android/view/a;

    iput-object v0, p0, Ltv/periscope/android/view/ag;->c:Ltv/periscope/android/view/a;

    .line 16
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Ltv/periscope/android/view/ag;->c:Ltv/periscope/android/view/a;

    iget-object v1, p0, Ltv/periscope/android/view/ag;->a:Ltv/periscope/android/view/a;

    if-ne v0, v1, :cond_0

    .line 20
    iget-object v0, p0, Ltv/periscope/android/view/ag;->b:Ltv/periscope/android/view/a;

    iput-object v0, p0, Ltv/periscope/android/view/ag;->c:Ltv/periscope/android/view/a;

    .line 24
    :goto_0
    return-void

    .line 22
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/ag;->a:Ltv/periscope/android/view/a;

    iput-object v0, p0, Ltv/periscope/android/view/ag;->c:Ltv/periscope/android/view/a;

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Ltv/periscope/android/view/ag;->c:Ltv/periscope/android/view/a;

    invoke-interface {v0}, Ltv/periscope/android/view/a;->a()I

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ltv/periscope/android/view/ag;->c:Ltv/periscope/android/view/a;

    invoke-interface {v0, p1}, Ltv/periscope/android/view/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ltv/periscope/android/view/ag;->c:Ltv/periscope/android/view/a;

    invoke-interface {v0}, Ltv/periscope/android/view/a;->b()I

    move-result v0

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ltv/periscope/android/view/ag;->c:Ltv/periscope/android/view/a;

    invoke-interface {v0}, Ltv/periscope/android/view/a;->c()I

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Ltv/periscope/android/view/ag;->c:Ltv/periscope/android/view/a;

    invoke-interface {v0}, Ltv/periscope/android/view/a;->e()Z

    .line 54
    invoke-direct {p0}, Ltv/periscope/android/view/ag;->g()V

    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public f()Ltv/periscope/android/view/c;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Ltv/periscope/android/view/ag;->c:Ltv/periscope/android/view/a;

    invoke-interface {v0}, Ltv/periscope/android/view/a;->f()Ltv/periscope/android/view/c;

    move-result-object v0

    return-object v0
.end method
