.class public Ltv/periscope/android/view/ah;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/view/ah$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Landroid/widget/PopupWindow;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/animation/AnimatorSet;

.field private final f:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .prologue
    .line 45
    sget v0, Ltv/periscope/android/library/f$i;->ps__tooltip:I

    invoke-direct {p0, p1, v0}, Ltv/periscope/android/view/ah;-><init>(Landroid/content/Context;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 5
    .param p2    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-direct {v0, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ltv/periscope/android/view/ah;->b:Landroid/widget/PopupWindow;

    .line 50
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/ah;->c:Landroid/view/View;

    .line 51
    iget-object v0, p0, Ltv/periscope/android/view/ah;->c:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 53
    iget-object v0, p0, Ltv/periscope/android/view/ah;->c:Landroid/view/View;

    sget v1, Ltv/periscope/android/library/f$g;->tooltip:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/view/ah;->d:Landroid/widget/TextView;

    .line 54
    new-instance v0, Ltv/periscope/android/view/ah$a;

    iget-object v1, p0, Ltv/periscope/android/view/ah;->b:Landroid/widget/PopupWindow;

    iget-object v2, p0, Ltv/periscope/android/view/ah;->c:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/view/ah$a;-><init>(Landroid/widget/PopupWindow;Landroid/view/View;)V

    iput-object v0, p0, Ltv/periscope/android/view/ah;->a:Landroid/os/Handler;

    .line 55
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/view/ah;->e:Landroid/animation/AnimatorSet;

    .line 57
    iget-object v0, p0, Ltv/periscope/android/view/ah;->c:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/ah;->f:Landroid/animation/ObjectAnimator;

    .line 58
    iget-object v0, p0, Ltv/periscope/android/view/ah;->f:Landroid/animation/ObjectAnimator;

    new-instance v1, Ltv/periscope/android/view/n;

    iget-object v2, p0, Ltv/periscope/android/view/ah;->c:Landroid/view/View;

    invoke-direct {v1, v2}, Ltv/periscope/android/view/n;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 60
    iget-object v0, p0, Ltv/periscope/android/view/ah;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 61
    iget-object v0, p0, Ltv/periscope/android/view/ah;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 62
    iget-object v0, p0, Ltv/periscope/android/view/ah;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 63
    iget-object v0, p0, Ltv/periscope/android/view/ah;->b:Landroid/widget/PopupWindow;

    iget-object v1, p0, Ltv/periscope/android/view/ah;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 64
    return-void

    .line 57
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Ltv/periscope/android/view/ah;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 68
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/CharSequence;IZI)V
    .locals 5
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/view/ah;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p3, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 80
    iget-object v0, p0, Ltv/periscope/android/view/ah;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 83
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 85
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 86
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 87
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v3, v1

    .line 88
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-direct {v2, v1, v0, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 90
    iget-object v0, p0, Ltv/periscope/android/view/ah;->c:Landroid/view/View;

    const/4 v1, -0x2

    const/4 v3, -0x2

    invoke-virtual {v0, v1, v3}, Landroid/view/View;->measure(II)V

    .line 94
    iget-object v0, p0, Ltv/periscope/android/view/ah;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 95
    iget-object v1, p0, Ltv/periscope/android/view/ah;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 96
    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    div-int/lit8 v0, v0, 0x2

    sub-int/2addr v3, v0

    .line 97
    iget v0, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    add-int v2, v0, p5

    .line 99
    iget-object v4, p0, Ltv/periscope/android/view/ah;->f:Landroid/animation/ObjectAnimator;

    if-eqz p4, :cond_0

    const-wide/16 v0, 0x157c

    :goto_0
    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 101
    iget-object v0, p0, Ltv/periscope/android/view/ah;->b:Landroid/widget/PopupWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, v3, v2}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 102
    iget-object v0, p0, Ltv/periscope/android/view/ah;->e:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Ltv/periscope/android/view/ah;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 103
    iget-object v0, p0, Ltv/periscope/android/view/ah;->e:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 105
    iget-object v2, p0, Ltv/periscope/android/view/ah;->a:Landroid/os/Handler;

    const/16 v3, 0x64

    if-eqz p4, :cond_1

    const-wide/16 v0, 0x157c

    :goto_1
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 106
    return-void

    .line 99
    :cond_0
    const-wide/16 v0, 0x9c4

    goto :goto_0

    .line 105
    :cond_1
    const-wide/16 v0, 0x9c4

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ltv/periscope/android/view/ah;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/view/ah;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Ltv/periscope/android/view/ah;->a:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 114
    iget-object v0, p0, Ltv/periscope/android/view/ah;->e:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 115
    iget-object v0, p0, Ltv/periscope/android/view/ah;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 116
    return-void
.end method
