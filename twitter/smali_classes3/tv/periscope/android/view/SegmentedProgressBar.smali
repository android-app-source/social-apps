.class public Ltv/periscope/android/view/SegmentedProgressBar;
.super Landroid/widget/ProgressBar;
.source "Twttr"


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/Paint;

.field private c:F

.field private d:I

.field private e:I

.field private f:F

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/view/SegmentedProgressBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/view/SegmentedProgressBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method private a()F
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0}, Ltv/periscope/android/view/SegmentedProgressBar;->getInitialOffset()F

    move-result v0

    invoke-virtual {p0}, Ltv/periscope/android/view/SegmentedProgressBar;->getProgress()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Ltv/periscope/android/view/SegmentedProgressBar;->a(F)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method private a(F)F
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Ltv/periscope/android/view/SegmentedProgressBar;->getMax()I

    move-result v0

    if-lez v0, :cond_0

    .line 95
    invoke-virtual {p0}, Ltv/periscope/android/view/SegmentedProgressBar;->getMax()I

    move-result v0

    int-to-float v0, v0

    div-float v0, p1, v0

    iget v1, p0, Ltv/periscope/android/view/SegmentedProgressBar;->h:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 97
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 41
    sget-object v1, Ltv/periscope/android/library/f$n;->SegmentedProgressBar:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 43
    :try_start_0
    sget v2, Ltv/periscope/android/library/f$n;->SegmentedProgressBar_ps__barHeight:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Ltv/periscope/android/view/SegmentedProgressBar;->g:I

    .line 44
    sget v2, Ltv/periscope/android/library/f$n;->SegmentedProgressBar_ps__dotRadius:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Ltv/periscope/android/view/SegmentedProgressBar;->c:F

    .line 45
    sget v2, Ltv/periscope/android/library/f$n;->SegmentedProgressBar_ps__dotMargin:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Ltv/periscope/android/view/SegmentedProgressBar;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 50
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Ltv/periscope/android/view/SegmentedProgressBar;->a:Landroid/graphics/Paint;

    .line 51
    iget-object v1, p0, Ltv/periscope/android/view/SegmentedProgressBar;->a:Landroid/graphics/Paint;

    sget v2, Ltv/periscope/android/library/f$d;->ps__black_50:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 52
    iget-object v1, p0, Ltv/periscope/android/view/SegmentedProgressBar;->a:Landroid/graphics/Paint;

    sget v2, Ltv/periscope/android/library/f$e;->ps__progress_stroke_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 53
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/view/SegmentedProgressBar;->b:Landroid/graphics/Paint;

    .line 54
    iget-object v0, p0, Ltv/periscope/android/view/SegmentedProgressBar;->b:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 55
    invoke-static {p1}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    iput v0, p0, Ltv/periscope/android/view/SegmentedProgressBar;->h:I

    .line 56
    return-void

    .line 47
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private getInitialOffset()F
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p0}, Ltv/periscope/android/view/SegmentedProgressBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 67
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Ltv/periscope/android/view/SegmentedProgressBar;->a()F

    move-result v0

    .line 68
    invoke-virtual {p0}, Ltv/periscope/android/view/SegmentedProgressBar;->getTranslationY()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 70
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->onDraw(Landroid/graphics/Canvas;)V

    .line 72
    invoke-virtual {p0}, Ltv/periscope/android/view/SegmentedProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v6

    .line 75
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v7

    .line 78
    iget v0, p0, Ltv/periscope/android/view/SegmentedProgressBar;->f:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 79
    iget v1, p0, Ltv/periscope/android/view/SegmentedProgressBar;->f:F

    :goto_0
    int-to-float v0, v7

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    .line 80
    const/4 v2, 0x0

    int-to-float v4, v6

    iget-object v5, p0, Ltv/periscope/android/view/SegmentedProgressBar;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 79
    iget v0, p0, Ltv/periscope/android/view/SegmentedProgressBar;->f:F

    add-float/2addr v1, v0

    goto :goto_0

    .line 85
    :cond_0
    iget v0, p0, Ltv/periscope/android/view/SegmentedProgressBar;->e:I

    int-to-float v0, v0

    invoke-direct {p0, v0}, Ltv/periscope/android/view/SegmentedProgressBar;->a(F)F

    move-result v0

    iget v1, p0, Ltv/periscope/android/view/SegmentedProgressBar;->d:I

    add-int/2addr v1, v6

    int-to-float v1, v1

    iget v2, p0, Ltv/periscope/android/view/SegmentedProgressBar;->c:F

    iget-object v3, p0, Ltv/periscope/android/view/SegmentedProgressBar;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    monitor-exit p0

    return-void

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Ltv/periscope/android/view/SegmentedProgressBar;->h:I

    .line 61
    iget v1, p0, Ltv/periscope/android/view/SegmentedProgressBar;->g:I

    .line 62
    invoke-super {p0, v0, v1, p3, p4}, Landroid/widget/ProgressBar;->onSizeChanged(IIII)V

    .line 63
    return-void
.end method

.method public setBarWidth(I)V
    .locals 4

    .prologue
    .line 110
    iput p1, p0, Ltv/periscope/android/view/SegmentedProgressBar;->h:I

    .line 111
    invoke-virtual {p0}, Ltv/periscope/android/view/SegmentedProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 112
    iget v1, p0, Ltv/periscope/android/view/SegmentedProgressBar;->h:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v3, p0, Ltv/periscope/android/view/SegmentedProgressBar;->g:I

    invoke-virtual {p0, v1, v2, v0, v3}, Ltv/periscope/android/view/SegmentedProgressBar;->onSizeChanged(IIII)V

    .line 113
    return-void
.end method

.method public setInitialProgress(I)V
    .locals 0

    .prologue
    .line 106
    iput p1, p0, Ltv/periscope/android/view/SegmentedProgressBar;->e:I

    .line 107
    return-void
.end method

.method public setSegmentSize(F)V
    .locals 0

    .prologue
    .line 116
    iput p1, p0, Ltv/periscope/android/view/SegmentedProgressBar;->f:F

    .line 117
    return-void
.end method
