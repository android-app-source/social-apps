.class public Ltv/periscope/android/view/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/i;


# instance fields
.field private final a:Ltv/periscope/android/ui/chat/l;

.field private final b:Ltv/periscope/android/ui/chat/a;

.field private final c:Lcyw;


# direct methods
.method public constructor <init>(Ltv/periscope/android/ui/chat/l;Ltv/periscope/android/ui/chat/a;Lcyw;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Ltv/periscope/android/view/j;->a:Ltv/periscope/android/ui/chat/l;

    .line 23
    iput-object p2, p0, Ltv/periscope/android/view/j;->b:Ltv/periscope/android/ui/chat/a;

    .line 24
    iput-object p3, p0, Ltv/periscope/android/view/j;->c:Lcyw;

    .line 25
    return-void
.end method

.method private a(Ljava/util/List;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/chat/Message;",
            ">;II)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 54
    sub-int v1, p2, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 56
    :goto_0
    if-ge v0, p3, :cond_0

    if-ge v1, p2, :cond_0

    .line 57
    add-int/lit8 v2, v1, 0x1

    invoke-direct {p0, p1, v1}, Ltv/periscope/android/view/j;->a(Ljava/util/List;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 61
    :cond_0
    return-void

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method private a(Ljava/util/List;III)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/chat/Message;",
            ">;III)V"
        }
    .end annotation

    .prologue
    .line 64
    add-int/lit8 v1, p2, 0x1

    .line 65
    const/4 v0, 0x0

    .line 66
    :goto_0
    if-ge v0, p3, :cond_0

    if-ge v1, p4, :cond_0

    .line 67
    add-int/lit8 v2, v1, 0x1

    invoke-direct {p0, p1, v1}, Ltv/periscope/android/view/j;->a(Ljava/util/List;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 71
    :cond_0
    return-void

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method private a(Ljava/util/List;I)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/chat/Message;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Ltv/periscope/android/view/j;->a:Ltv/periscope/android/ui/chat/l;

    invoke-interface {v0, p2}, Ltv/periscope/android/ui/chat/l;->a(I)Ltv/periscope/android/ui/chat/h;

    move-result-object v0

    iget-object v0, v0, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    .line 75
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/chat/MessageType;->b:Ltv/periscope/model/chat/MessageType;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Ltv/periscope/android/view/j;->c:Lcyw;

    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcyw;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ltv/periscope/android/view/j;->b:Ltv/periscope/android/ui/chat/a;

    .line 76
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ltv/periscope/android/ui/chat/a;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    const/4 v0, 0x1

    .line 80
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(II)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/chat/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 31
    iget-object v1, p0, Ltv/periscope/android/view/j;->a:Ltv/periscope/android/ui/chat/l;

    invoke-interface {v1}, Ltv/periscope/android/ui/chat/l;->getItemCount()I

    move-result v1

    .line 32
    if-nez v1, :cond_0

    .line 50
    :goto_0
    return-object v0

    .line 35
    :cond_0
    iget-object v2, p0, Ltv/periscope/android/view/j;->a:Ltv/periscope/android/ui/chat/l;

    invoke-interface {v2, p1}, Ltv/periscope/android/ui/chat/l;->a(I)Ltv/periscope/android/ui/chat/h;

    move-result-object v2

    .line 38
    add-int/lit8 v3, v1, -0x1

    .line 39
    if-nez p1, :cond_1

    .line 40
    iget-object v2, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-direct {p0, v0, p1, v2, v1}, Ltv/periscope/android/view/j;->a(Ljava/util/List;III)V

    goto :goto_0

    .line 42
    :cond_1
    if-ne p1, v3, :cond_2

    .line 43
    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {p0, v0, p1, v1}, Ltv/periscope/android/view/j;->a(Ljava/util/List;II)V

    .line 44
    iget-object v1, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 46
    :cond_2
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-direct {p0, v0, p1, v3}, Ltv/periscope/android/view/j;->a(Ljava/util/List;II)V

    .line 47
    iget-object v2, v2, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    sub-int v2, v1, p1

    invoke-static {v2, p2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-direct {p0, v0, p1, v2, v1}, Ltv/periscope/android/view/j;->a(Ljava/util/List;III)V

    goto :goto_0
.end method
