.class public Ltv/periscope/android/view/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/ak;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltv/periscope/android/view/ak",
        "<",
        "Ltv/periscope/android/view/d;",
        "Ltv/periscope/android/view/a;",
        ">;"
    }
.end annotation


# static fields
.field public static c:Ltv/periscope/android/view/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    new-instance v0, Ltv/periscope/android/view/c;

    invoke-direct {v0}, Ltv/periscope/android/view/c;-><init>()V

    sput-object v0, Ltv/periscope/android/view/c;->c:Ltv/periscope/android/view/c;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Landroid/support/v7/widget/RecyclerView$ViewHolder;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 5
    check-cast p1, Ltv/periscope/android/view/d;

    check-cast p2, Ltv/periscope/android/view/a;

    invoke-virtual {p0, p1, p2, p3}, Ltv/periscope/android/view/c;->a(Ltv/periscope/android/view/d;Ltv/periscope/android/view/a;I)V

    return-void
.end method

.method public a(Ltv/periscope/android/view/d;Ltv/periscope/android/view/a;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 11
    iput-object p2, p1, Ltv/periscope/android/view/d;->b:Ltv/periscope/android/view/a;

    .line 12
    iget-object v0, p1, Ltv/periscope/android/view/d;->a:Ltv/periscope/android/view/ActionSheetItem;

    invoke-interface {p2}, Ltv/periscope/android/view/a;->a()I

    move-result v1

    invoke-interface {p2}, Ltv/periscope/android/view/a;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/view/ActionSheetItem;->a(II)V

    .line 13
    iget-object v0, p1, Ltv/periscope/android/view/d;->a:Ltv/periscope/android/view/ActionSheetItem;

    iget-object v1, p1, Ltv/periscope/android/view/d;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p2, v1}, Ltv/periscope/android/view/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 14
    invoke-interface {p2}, Ltv/periscope/android/view/a;->c()I

    move-result v2

    .line 13
    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/view/ActionSheetItem;->a(Ljava/lang/CharSequence;I)V

    .line 15
    iget-object v0, p1, Ltv/periscope/android/view/d;->a:Ltv/periscope/android/view/ActionSheetItem;

    invoke-virtual {v0, v3}, Ltv/periscope/android/view/ActionSheetItem;->setIconVisibility(I)V

    .line 16
    iget-object v0, p1, Ltv/periscope/android/view/d;->a:Ltv/periscope/android/view/ActionSheetItem;

    invoke-virtual {v0, v3}, Ltv/periscope/android/view/ActionSheetItem;->setLabelVisibility(I)V

    .line 17
    iget-object v0, p1, Ltv/periscope/android/view/d;->a:Ltv/periscope/android/view/ActionSheetItem;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/ActionSheetItem;->setDescriptionVisibility(I)V

    .line 18
    return-void
.end method
