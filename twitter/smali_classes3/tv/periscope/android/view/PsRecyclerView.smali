.class public Ltv/periscope/android/view/PsRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source "Twttr"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/view/PsRecyclerView;->a:Z

    .line 13
    invoke-direct {p0}, Ltv/periscope/android/view/PsRecyclerView;->a()V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/view/PsRecyclerView;->a:Z

    .line 18
    invoke-direct {p0}, Ltv/periscope/android/view/PsRecyclerView;->a()V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/view/PsRecyclerView;->a:Z

    .line 23
    invoke-direct {p0}, Ltv/periscope/android/view/PsRecyclerView;->a()V

    .line 24
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/PsRecyclerView;->setOverScrollMode(I)V

    .line 28
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 36
    iget-boolean v1, p0, Ltv/periscope/android/view/PsRecyclerView;->a:Z

    if-nez v1, :cond_0

    .line 37
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    .line 38
    packed-switch v1, :pswitch_data_0

    .line 44
    :cond_0
    iget-boolean v1, p0, Ltv/periscope/android/view/PsRecyclerView;->a:Z

    if-eqz v1, :cond_1

    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    :pswitch_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public setAllowScroll(Z)V
    .locals 0

    .prologue
    .line 31
    iput-boolean p1, p0, Ltv/periscope/android/view/PsRecyclerView;->a:Z

    .line 32
    return-void
.end method
