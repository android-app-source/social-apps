.class public Ltv/periscope/android/view/ActionSheetItem;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Ltv/periscope/android/view/PsTextView;

.field private c:Ltv/periscope/android/view/PsTextView;

.field private d:Z

.field private e:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private f:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Ltv/periscope/android/view/ActionSheetItem;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Ltv/periscope/android/view/ActionSheetItem;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    invoke-virtual {p0, p1, p2, p3}, Ltv/periscope/android/view/ActionSheetItem;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 81
    iget-boolean v0, p0, Ltv/periscope/android/view/ActionSheetItem;->d:Z

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 86
    :cond_0
    :goto_0
    if-lez p1, :cond_2

    .line 87
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 91
    :goto_1
    return-void

    .line 83
    :cond_1
    if-lez p2, :cond_0

    .line 84
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, Ltv/periscope/android/view/ActionSheetItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_0

    .line 89
    :cond_2
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Ltv/periscope/android/library/f$i;->ps__action_sheet_item:I

    const/4 v3, 0x1

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 42
    sget v0, Ltv/periscope/android/library/f$g;->icon:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/ActionSheetItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->a:Landroid/widget/ImageView;

    .line 43
    sget v0, Ltv/periscope/android/library/f$g;->label:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/ActionSheetItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsTextView;

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->b:Ltv/periscope/android/view/PsTextView;

    .line 44
    sget v0, Ltv/periscope/android/library/f$g;->description:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/ActionSheetItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsTextView;

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->c:Ltv/periscope/android/view/PsTextView;

    .line 46
    sget-object v0, Ltv/periscope/android/library/f$n;->ActionSheetItem:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 48
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    move v0, v1

    .line 49
    :goto_0
    if-ge v0, v3, :cond_8

    .line 50
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    .line 51
    sget v5, Ltv/periscope/android/library/f$n;->ActionSheetItem_ps__icon:I

    if-ne v4, v5, :cond_1

    .line 52
    iget-object v5, p0, Ltv/periscope/android/view/ActionSheetItem;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 49
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_1
    sget v5, Ltv/periscope/android/library/f$n;->ActionSheetItem_ps__tint:I

    if-ne v4, v5, :cond_2

    .line 54
    iget-object v5, p0, Ltv/periscope/android/view/ActionSheetItem;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_1

    .line 55
    :cond_2
    sget v5, Ltv/periscope/android/library/f$n;->ActionSheetItem_ps__label:I

    if-ne v4, v5, :cond_3

    .line 56
    iget-object v5, p0, Ltv/periscope/android/view/ActionSheetItem;->b:Ltv/periscope/android/view/PsTextView;

    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ltv/periscope/android/view/PsTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 57
    :cond_3
    sget v5, Ltv/periscope/android/library/f$n;->ActionSheetItem_ps__labelTextSize:I

    if-ne v4, v5, :cond_4

    .line 58
    iget-object v5, p0, Ltv/periscope/android/view/ActionSheetItem;->b:Ltv/periscope/android/view/PsTextView;

    const/16 v6, 0x10

    invoke-virtual {v2, v4, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v5, v1, v4}, Ltv/periscope/android/view/PsTextView;->setTextSize(IF)V

    goto :goto_1

    .line 59
    :cond_4
    sget v5, Ltv/periscope/android/library/f$n;->ActionSheetItem_ps__labelFont:I

    if-ne v4, v5, :cond_5

    .line 60
    iget-object v4, p0, Ltv/periscope/android/view/ActionSheetItem;->b:Ltv/periscope/android/view/PsTextView;

    invoke-static {p1, p2, v4}, Ltv/periscope/android/view/z;->a(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/widget/TextView;)V

    goto :goto_1

    .line 61
    :cond_5
    sget v5, Ltv/periscope/android/library/f$n;->ActionSheetItem_ps__primaryTextColor:I

    if-ne v4, v5, :cond_6

    .line 62
    invoke-virtual {p0}, Ltv/periscope/android/view/ActionSheetItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Ltv/periscope/android/library/f$d;->ps__primary_text:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Ltv/periscope/android/view/ActionSheetItem;->e:I

    goto :goto_1

    .line 63
    :cond_6
    sget v5, Ltv/periscope/android/library/f$n;->ActionSheetItem_ps__secondaryTextColor:I

    if-ne v4, v5, :cond_7

    .line 64
    invoke-virtual {p0}, Ltv/periscope/android/view/ActionSheetItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Ltv/periscope/android/library/f$d;->ps__secondary_text:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Ltv/periscope/android/view/ActionSheetItem;->f:I

    goto :goto_1

    .line 65
    :cond_7
    sget v5, Ltv/periscope/android/library/f$n;->ActionSheetItem_ps__darkTheme:I

    if-ne v4, v5, :cond_0

    .line 66
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Ltv/periscope/android/view/ActionSheetItem;->d:Z

    goto :goto_1

    .line 69
    :cond_8
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 70
    return-void
.end method

.method public a(Ljava/lang/CharSequence;I)V
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 98
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->b:Ltv/periscope/android/view/PsTextView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/PsTextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-boolean v0, p0, Ltv/periscope/android/view/ActionSheetItem;->d:Z

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->b:Ltv/periscope/android/view/PsTextView;

    iget v1, p0, Ltv/periscope/android/view/ActionSheetItem;->e:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsTextView;->setTextColor(I)V

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->b:Ltv/periscope/android/view/PsTextView;

    invoke-virtual {p0}, Ltv/periscope/android/view/ActionSheetItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsTextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public b(Ljava/lang/CharSequence;I)V
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 111
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->c:Ltv/periscope/android/view/PsTextView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/PsTextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-boolean v0, p0, Ltv/periscope/android/view/ActionSheetItem;->d:Z

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->b:Ltv/periscope/android/view/PsTextView;

    iget v1, p0, Ltv/periscope/android/view/ActionSheetItem;->f:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsTextView;->setTextColor(I)V

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->b:Ltv/periscope/android/view/PsTextView;

    invoke-virtual {p0}, Ltv/periscope/android/view/ActionSheetItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/PsTextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public getIcon()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->a:Landroid/widget/ImageView;

    return-object v0
.end method

.method public setDescriptionVisibility(I)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->c:Ltv/periscope/android/view/PsTextView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/PsTextView;->setVisibility(I)V

    .line 108
    return-void
.end method

.method public setIconVisibility(I)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 78
    return-void
.end method

.method public setLabelVisibility(I)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheetItem;->b:Ltv/periscope/android/view/PsTextView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/PsTextView;->setVisibility(I)V

    .line 95
    return-void
.end method
