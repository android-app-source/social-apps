.class public Ltv/periscope/android/view/ActionSheet;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/view/ActionSheet$b;,
        Ltv/periscope/android/view/ActionSheet$a;
    }
.end annotation


# instance fields
.field private a:Landroid/view/View;

.field private b:Ltv/periscope/android/view/CarouselView;

.field private c:Ltv/periscope/android/view/g;

.field private d:Ltv/periscope/android/view/f;

.field private e:Ltv/periscope/android/view/r;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/view/View;

.field private i:Landroid/support/v7/widget/RecyclerView;

.field private j:Ltv/periscope/android/view/ActionSheet$a;

.field private k:I

.field private l:Landroid/support/v7/widget/RecyclerView$SmoothScroller;

.field private m:Landroid/animation/Animator;

.field private n:Landroid/animation/Animator;

.field private o:I

.field private p:I

.field private final q:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 182
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 174
    new-instance v0, Ltv/periscope/android/view/ActionSheet$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/view/ActionSheet$1;-><init>(Ltv/periscope/android/view/ActionSheet;)V

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->q:Ljava/lang/Runnable;

    .line 183
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/ActionSheet;->a(Landroid/content/Context;)V

    .line 184
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 174
    new-instance v0, Ltv/periscope/android/view/ActionSheet$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/view/ActionSheet$1;-><init>(Ltv/periscope/android/view/ActionSheet;)V

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->q:Ljava/lang/Runnable;

    .line 188
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/ActionSheet;->a(Landroid/content/Context;)V

    .line 189
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 174
    new-instance v0, Ltv/periscope/android/view/ActionSheet$1;

    invoke-direct {v0, p0}, Ltv/periscope/android/view/ActionSheet$1;-><init>(Ltv/periscope/android/view/ActionSheet;)V

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->q:Ljava/lang/Runnable;

    .line 193
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/ActionSheet;->a(Landroid/content/Context;)V

    .line 194
    return-void
.end method

.method private a(I)Landroid/animation/Animator;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x2

    .line 309
    invoke-virtual {p0}, Ltv/periscope/android/view/ActionSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 311
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 312
    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 314
    new-array v2, v8, [I

    iget v3, p0, Ltv/periscope/android/view/ActionSheet;->o:I

    aput v3, v2, v9

    iget v3, p0, Ltv/periscope/android/view/ActionSheet;->p:I

    aput v3, v2, v10

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 315
    new-instance v3, Ltv/periscope/android/view/ActionSheet$8;

    invoke-direct {v3, p0}, Ltv/periscope/android/view/ActionSheet$8;-><init>(Ltv/periscope/android/view/ActionSheet;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 322
    new-instance v3, Ltv/periscope/android/view/ActionSheet$9;

    invoke-direct {v3, p0}, Ltv/periscope/android/view/ActionSheet$9;-><init>(Ltv/periscope/android/view/ActionSheet;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 330
    new-array v3, v8, [I

    fill-array-data v3, :array_0

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 331
    new-instance v4, Ltv/periscope/android/view/ActionSheet$10;

    invoke-direct {v4, p0, v3}, Ltv/periscope/android/view/ActionSheet$10;-><init>(Ltv/periscope/android/view/ActionSheet;Landroid/animation/ValueAnimator;)V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 344
    new-instance v4, Ltv/periscope/android/view/ActionSheet$11;

    invoke-direct {v4, p0}, Ltv/periscope/android/view/ActionSheet$11;-><init>(Ltv/periscope/android/view/ActionSheet;)V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 352
    sget v4, Ltv/periscope/android/library/f$e;->ps__message_carousel_vertical_translation:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 353
    iget-object v4, p0, Ltv/periscope/android/view/ActionSheet;->b:Ltv/periscope/android/view/CarouselView;

    sget-object v5, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v6, v8, [F

    const/4 v7, 0x0

    aput v7, v6, v9

    neg-int v7, v0

    int-to-float v7, v7

    aput v7, v6, v10

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 354
    new-instance v5, Ltv/periscope/android/view/ActionSheet$12;

    invoke-direct {v5, p0, v0}, Ltv/periscope/android/view/ActionSheet$12;-><init>(Ltv/periscope/android/view/ActionSheet;I)V

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 361
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->f:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v8, [F

    fill-array-data v6, :array_1

    invoke-static {v0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 362
    new-instance v5, Ltv/periscope/android/view/ActionSheet$13;

    iget-object v6, p0, Ltv/periscope/android/view/ActionSheet;->f:Landroid/widget/TextView;

    invoke-direct {v5, p0, v6}, Ltv/periscope/android/view/ActionSheet$13;-><init>(Ltv/periscope/android/view/ActionSheet;Landroid/view/View;)V

    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 369
    const/4 v5, 0x4

    new-array v5, v5, [Landroid/animation/Animator;

    aput-object v2, v5, v9

    aput-object v4, v5, v10

    aput-object v0, v5, v8

    const/4 v0, 0x3

    aput-object v3, v5, v0

    invoke-virtual {v1, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 370
    new-instance v0, Ltv/periscope/android/view/ActionSheet$14;

    invoke-direct {v0, p0}, Ltv/periscope/android/view/ActionSheet$14;-><init>(Ltv/periscope/android/view/ActionSheet;)V

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 376
    return-object v1

    .line 330
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 361
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic a(Ltv/periscope/android/view/ActionSheet;)Ltv/periscope/android/view/ActionSheet$a;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->j:Ltv/periscope/android/view/ActionSheet$a;

    return-object v0
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 291
    iput p1, p0, Ltv/periscope/android/view/ActionSheet;->k:I

    .line 292
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->l:Landroid/support/v7/widget/RecyclerView$SmoothScroller;

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/RecyclerView$SmoothScroller;->setTargetPosition(I)V

    .line 293
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->j:Ltv/periscope/android/view/ActionSheet$a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/ActionSheet$a;->a(Z)V

    .line 294
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->j:Ltv/periscope/android/view/ActionSheet$a;

    invoke-virtual {v0}, Ltv/periscope/android/view/ActionSheet$a;->requestLayout()V

    .line 295
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->j:Ltv/periscope/android/view/ActionSheet$a;

    iget-object v1, p0, Ltv/periscope/android/view/ActionSheet;->l:Landroid/support/v7/widget/RecyclerView$SmoothScroller;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/ActionSheet$a;->startSmoothScroll(Landroid/support/v7/widget/RecyclerView$SmoothScroller;)V

    .line 296
    invoke-direct {p0}, Ltv/periscope/android/view/ActionSheet;->d()V

    .line 297
    return-void
.end method

.method private b(I)Landroid/animation/Animator;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 380
    invoke-virtual {p0}, Ltv/periscope/android/view/ActionSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 382
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 383
    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 385
    new-array v2, v7, [I

    iget v3, p0, Ltv/periscope/android/view/ActionSheet;->p:I

    aput v3, v2, v8

    iget v3, p0, Ltv/periscope/android/view/ActionSheet;->o:I

    aput v3, v2, v9

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 386
    new-instance v3, Ltv/periscope/android/view/ActionSheet$2;

    invoke-direct {v3, p0}, Ltv/periscope/android/view/ActionSheet$2;-><init>(Ltv/periscope/android/view/ActionSheet;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 394
    new-array v3, v7, [I

    fill-array-data v3, :array_0

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 395
    new-instance v4, Ltv/periscope/android/view/ActionSheet$3;

    invoke-direct {v4, p0, v3}, Ltv/periscope/android/view/ActionSheet$3;-><init>(Ltv/periscope/android/view/ActionSheet;Landroid/animation/ValueAnimator;)V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 408
    new-instance v4, Ltv/periscope/android/view/ActionSheet$4;

    invoke-direct {v4, p0}, Ltv/periscope/android/view/ActionSheet$4;-><init>(Ltv/periscope/android/view/ActionSheet;)V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 416
    sget v4, Ltv/periscope/android/library/f$e;->ps__message_carousel_vertical_translation:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 417
    iget-object v4, p0, Ltv/periscope/android/view/ActionSheet;->b:Ltv/periscope/android/view/CarouselView;

    sget-object v5, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v6, v7, [F

    neg-int v0, v0

    int-to-float v0, v0

    aput v0, v6, v8

    const/4 v0, 0x0

    aput v0, v6, v9

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 419
    iget-object v4, p0, Ltv/periscope/android/view/ActionSheet;->f:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 420
    new-instance v5, Ltv/periscope/android/view/ActionSheet$5;

    invoke-direct {v5, p0}, Ltv/periscope/android/view/ActionSheet$5;-><init>(Ltv/periscope/android/view/ActionSheet;)V

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 427
    const/4 v5, 0x4

    new-array v5, v5, [Landroid/animation/Animator;

    aput-object v2, v5, v8

    aput-object v0, v5, v9

    aput-object v4, v5, v7

    const/4 v0, 0x3

    aput-object v3, v5, v0

    invoke-virtual {v1, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 428
    new-instance v0, Ltv/periscope/android/view/ActionSheet$6;

    invoke-direct {v0, p0}, Ltv/periscope/android/view/ActionSheet$6;-><init>(Ltv/periscope/android/view/ActionSheet;)V

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 439
    return-object v1

    .line 394
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 419
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method static synthetic b(Ltv/periscope/android/view/ActionSheet;)Ltv/periscope/android/view/CarouselView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->b:Ltv/periscope/android/view/CarouselView;

    return-object v0
.end method

.method static synthetic c(Ltv/periscope/android/view/ActionSheet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->h:Landroid/view/View;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 263
    iput v2, p0, Ltv/periscope/android/view/ActionSheet;->k:I

    .line 264
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->b:Ltv/periscope/android/view/CarouselView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/CarouselView;->setTranslationY(F)V

    .line 267
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Ltv/periscope/android/view/ActionSheet;->o:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 268
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 269
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->b:Ltv/periscope/android/view/CarouselView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/CarouselView;->a(Z)V

    .line 270
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->e:Ltv/periscope/android/view/r;

    invoke-virtual {v0, v2}, Ltv/periscope/android/view/r;->a(I)V

    .line 271
    return-void
.end method

.method static synthetic d(Ltv/periscope/android/view/ActionSheet;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Ltv/periscope/android/view/ActionSheet;->p:I

    return v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 300
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->q:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/ActionSheet;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 301
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->q:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Ltv/periscope/android/view/ActionSheet;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 302
    return-void
.end method

.method static synthetic e(Ltv/periscope/android/view/ActionSheet;)Ltv/periscope/android/view/r;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->e:Ltv/periscope/android/view/r;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->n:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 444
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 445
    return-void
.end method

.method static synthetic f(Ltv/periscope/android/view/ActionSheet;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 449
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->n:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 450
    return-void
.end method

.method private setInfoText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 247
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->g:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 250
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 281
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Ltv/periscope/android/view/ActionSheet;->a(II)V

    .line 282
    invoke-direct {p0}, Ltv/periscope/android/view/ActionSheet;->e()V

    .line 283
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/16 v6, 0xfa

    const/high16 v5, 0x3f800000    # 1.0f

    const v4, 0x3f4ccccd    # 0.8f

    .line 197
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    .line 200
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Ltv/periscope/android/library/f$i;->ps__action_sheet:I

    const/4 v3, 0x1

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 201
    sget v0, Ltv/periscope/android/library/f$g;->carousel_container:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/ActionSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->a:Landroid/view/View;

    .line 202
    sget v0, Ltv/periscope/android/library/f$g;->message_carousel:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/ActionSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/CarouselView;

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->b:Ltv/periscope/android/view/CarouselView;

    .line 203
    new-instance v0, Ltv/periscope/android/view/r;

    invoke-direct {v0, v4, v5, v4, v5}, Ltv/periscope/android/view/r;-><init>(FFFF)V

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->e:Ltv/periscope/android/view/r;

    .line 204
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->b:Ltv/periscope/android/view/CarouselView;

    iget-object v2, p0, Ltv/periscope/android/view/ActionSheet;->e:Ltv/periscope/android/view/r;

    invoke-virtual {v0, v2}, Ltv/periscope/android/view/CarouselView;->setItemTransformer(Ltv/periscope/android/view/CarouselView$b;)V

    .line 205
    sget v0, Ltv/periscope/android/library/f$g;->report_comment_info:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/ActionSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->f:Landroid/widget/TextView;

    .line 206
    sget v0, Ltv/periscope/android/library/f$g;->report_comment_background:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/ActionSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->h:Landroid/view/View;

    .line 207
    sget v0, Ltv/periscope/android/library/f$g;->info_snippet:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/ActionSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->g:Landroid/widget/TextView;

    .line 208
    sget v0, Ltv/periscope/android/library/f$g;->actions:I

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/ActionSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->i:Landroid/support/v7/widget/RecyclerView;

    .line 209
    new-instance v0, Ltv/periscope/android/view/ActionSheet$a;

    invoke-direct {v0, p1, p0}, Ltv/periscope/android/view/ActionSheet$a;-><init>(Landroid/content/Context;Ltv/periscope/android/view/ActionSheet;)V

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->j:Ltv/periscope/android/view/ActionSheet$a;

    .line 210
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->i:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Ltv/periscope/android/view/ActionSheet;->j:Ltv/periscope/android/view/ActionSheet$a;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 211
    new-instance v0, Ltv/periscope/android/view/ActionSheet$b;

    invoke-virtual {p0}, Ltv/periscope/android/view/ActionSheet;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Ltv/periscope/android/view/ActionSheet;->j:Ltv/periscope/android/view/ActionSheet$a;

    invoke-direct {v0, v2, v3}, Ltv/periscope/android/view/ActionSheet$b;-><init>(Landroid/content/Context;Ltv/periscope/android/view/ActionSheet$a;)V

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->l:Landroid/support/v7/widget/RecyclerView$SmoothScroller;

    .line 213
    invoke-virtual {p0}, Ltv/periscope/android/view/ActionSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 214
    sget v2, Ltv/periscope/android/library/f$e;->ps__report_bg_start_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Ltv/periscope/android/view/ActionSheet;->o:I

    .line 215
    sget v2, Ltv/periscope/android/library/f$e;->ps__report_bg_end_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/ActionSheet;->p:I

    .line 217
    invoke-direct {p0, v6}, Ltv/periscope/android/view/ActionSheet;->a(I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->m:Landroid/animation/Animator;

    .line 218
    invoke-direct {p0, v6}, Ltv/periscope/android/view/ActionSheet;->b(I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/ActionSheet;->n:Landroid/animation/Animator;

    .line 220
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->b:Ltv/periscope/android/view/CarouselView;

    new-instance v2, Ltv/periscope/android/view/p;

    invoke-direct {v2, v1}, Ltv/periscope/android/view/p;-><init>(I)V

    invoke-virtual {v0, v2}, Ltv/periscope/android/view/CarouselView;->addOnItemTouchListener(Landroid/support/v7/widget/RecyclerView$OnItemTouchListener;)V

    .line 222
    new-instance v0, Ltv/periscope/android/view/f;

    invoke-direct {v0}, Ltv/periscope/android/view/f;-><init>()V

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/ActionSheet;->setActionAdapter(Ltv/periscope/android/view/f;)V

    .line 223
    return-void
.end method

.method public a(Lcyw;Ldae;)V
    .locals 11

    .prologue
    .line 226
    new-instance v9, Ltv/periscope/android/view/ActionSheet$7;

    invoke-direct {v9, p0}, Ltv/periscope/android/view/ActionSheet$7;-><init>(Ltv/periscope/android/view/ActionSheet;)V

    .line 232
    new-instance v10, Ltv/periscope/android/view/g;

    new-instance v0, Ltv/periscope/android/ui/chat/d;

    invoke-virtual {p0}, Ltv/periscope/android/view/ActionSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 233
    invoke-interface {p1}, Lcyw;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcyw;->e()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v7, p2

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Ltv/periscope/android/ui/chat/d;-><init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;ZZLtv/periscope/android/ui/chat/a;Ldae;Lcyw;)V

    invoke-direct {v10, v0, v9}, Ltv/periscope/android/view/g;-><init>(Ltv/periscope/android/view/ak;Ltv/periscope/android/view/k;)V

    iput-object v10, p0, Ltv/periscope/android/view/ActionSheet;->c:Ltv/periscope/android/view/g;

    .line 235
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->b:Ltv/periscope/android/view/CarouselView;

    iget-object v1, p0, Ltv/periscope/android/view/ActionSheet;->c:Ltv/periscope/android/view/g;

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/CarouselView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 236
    return-void
.end method

.method public a(Ljava/lang/CharSequence;Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List",
            "<+",
            "Ltv/periscope/android/view/a;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 255
    invoke-direct {p0}, Ltv/periscope/android/view/ActionSheet;->c()V

    .line 256
    invoke-direct {p0, p1}, Ltv/periscope/android/view/ActionSheet;->setInfoText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->j:Ltv/periscope/android/view/ActionSheet$a;

    invoke-virtual {v0, p3}, Ltv/periscope/android/view/ActionSheet$a;->a(I)V

    .line 258
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->d:Ltv/periscope/android/view/f;

    invoke-virtual {v0, p2}, Ltv/periscope/android/view/f;->a(Ljava/util/List;)V

    .line 259
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->i:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Ltv/periscope/android/view/ActionSheet;->d:Ltv/periscope/android/view/f;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 260
    return-void
.end method

.method public a(Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/chat/Message;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 274
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 275
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->c:Ltv/periscope/android/view/g;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/g;->a(Ljava/util/List;)V

    .line 276
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->b:Ltv/periscope/android/view/CarouselView;

    invoke-virtual {v0, p2}, Ltv/periscope/android/view/CarouselView;->a(I)V

    .line 277
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 286
    invoke-direct {p0, v0, v0}, Ltv/periscope/android/view/ActionSheet;->a(II)V

    .line 287
    invoke-direct {p0}, Ltv/periscope/android/view/ActionSheet;->f()V

    .line 288
    return-void
.end method

.method public getScrollPage()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Ltv/periscope/android/view/ActionSheet;->k:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 455
    return-void
.end method

.method public setActionAdapter(Ltv/periscope/android/view/f;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Ltv/periscope/android/view/ActionSheet;->d:Ltv/periscope/android/view/f;

    .line 240
    return-void
.end method

.method public setCarouselScrollListener(Ltv/periscope/android/view/CarouselView$a;)V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Ltv/periscope/android/view/ActionSheet;->b:Ltv/periscope/android/view/CarouselView;

    invoke-virtual {v0, p1}, Ltv/periscope/android/view/CarouselView;->setCarouselScrollListener(Ltv/periscope/android/view/CarouselView$a;)V

    .line 244
    return-void
.end method
