.class public Ltv/periscope/android/view/VipBadgeTooltip;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/view/VipBadgeTooltip$a;
    }
.end annotation


# instance fields
.field a:Ltv/periscope/android/view/VipBadgeTooltip$a;

.field b:Z

.field private final c:Ltv/periscope/android/view/PsTextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/ImageView;

.field private final f:Landroid/animation/Animator;

.field private final g:Landroid/animation/Animator;

.field private final h:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ltv/periscope/android/view/VipBadgeTooltip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ltv/periscope/android/view/VipBadgeTooltip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Ltv/periscope/android/library/f$i;->vip_badge_tooltip_layout:I

    const/4 v3, 0x1

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 50
    sget v0, Ltv/periscope/android/library/f$g;->learn_more:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/PsTextView;

    iput-object v0, p0, Ltv/periscope/android/view/VipBadgeTooltip;->c:Ltv/periscope/android/view/PsTextView;

    .line 51
    iget-object v0, p0, Ltv/periscope/android/view/VipBadgeTooltip;->c:Ltv/periscope/android/view/PsTextView;

    const/16 v3, 0x9

    invoke-virtual {v0, v3}, Ltv/periscope/android/view/PsTextView;->setPaintFlags(I)V

    .line 53
    sget v0, Ltv/periscope/android/library/f$g;->tooltip_content:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/VipBadgeTooltip;->h:Landroid/view/View;

    .line 55
    sget v0, Ltv/periscope/android/library/f$g;->close_button:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/view/VipBadgeTooltip;->e:Landroid/widget/ImageView;

    .line 56
    iget-object v0, p0, Ltv/periscope/android/view/VipBadgeTooltip;->e:Landroid/widget/ImageView;

    new-instance v3, Ltv/periscope/android/view/VipBadgeTooltip$1;

    invoke-direct {v3, p0}, Ltv/periscope/android/view/VipBadgeTooltip$1;-><init>(Ltv/periscope/android/view/VipBadgeTooltip;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    sget v0, Ltv/periscope/android/library/f$g;->carrot:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ltv/periscope/android/view/VipBadgeTooltip;->d:Landroid/widget/ImageView;

    .line 65
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v4, [F

    fill-array-data v2, :array_0

    invoke-static {p0, v0, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/VipBadgeTooltip;->f:Landroid/animation/Animator;

    .line 67
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v4, [F

    fill-array-data v2, :array_1

    invoke-static {p0, v0, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/view/VipBadgeTooltip;->g:Landroid/animation/Animator;

    .line 68
    iget-object v0, p0, Ltv/periscope/android/view/VipBadgeTooltip;->g:Landroid/animation/Animator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 69
    iget-object v0, p0, Ltv/periscope/android/view/VipBadgeTooltip;->g:Landroid/animation/Animator;

    new-instance v2, Ltv/periscope/android/view/VipBadgeTooltip$2;

    invoke-direct {v2, p0}, Ltv/periscope/android/view/VipBadgeTooltip$2;-><init>(Ltv/periscope/android/view/VipBadgeTooltip;)V

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 76
    invoke-virtual {p0}, Ltv/periscope/android/view/VipBadgeTooltip;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 78
    sget v2, Ltv/periscope/android/library/f$e;->ps__vip_badge_tooltip_border_radius:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 80
    iget-object v3, p0, Ltv/periscope/android/view/VipBadgeTooltip;->c:Ltv/periscope/android/view/PsTextView;

    new-instance v4, Ltv/periscope/android/view/VipBadgeTooltip$3;

    invoke-direct {v4, p0}, Ltv/periscope/android/view/VipBadgeTooltip$3;-><init>(Ltv/periscope/android/view/VipBadgeTooltip;)V

    invoke-virtual {v3, v4}, Ltv/periscope/android/view/PsTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v7, p0, Ltv/periscope/android/view/VipBadgeTooltip;->h:Landroid/view/View;

    sget v3, Ltv/periscope/android/library/f$d;->ps__black:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    int-to-float v3, v2

    int-to-float v4, v2

    int-to-float v5, v2

    int-to-float v6, v2

    move v2, v1

    invoke-static/range {v0 .. v6}, Ltv/periscope/android/util/m;->a(IIIFFFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 91
    invoke-virtual {p0, v1}, Ltv/periscope/android/view/VipBadgeTooltip;->setClipChildren(Z)V

    .line 92
    invoke-virtual {p0, v1}, Ltv/periscope/android/view/VipBadgeTooltip;->setClipToPadding(Z)V

    .line 93
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/VipBadgeTooltip;->setVisibility(I)V

    .line 94
    return-void

    .line 65
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 67
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/VipBadgeTooltip;->setVisibility(I)V

    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/view/VipBadgeTooltip;->b:Z

    .line 151
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ltv/periscope/android/view/VipBadgeTooltip;->setAlpha(F)V

    .line 152
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 137
    if-eqz p1, :cond_0

    .line 138
    iget-object v0, p0, Ltv/periscope/android/view/VipBadgeTooltip;->g:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-virtual {p0}, Ltv/periscope/android/view/VipBadgeTooltip;->a()V

    goto :goto_0
.end method

.method public setListener(Ltv/periscope/android/view/VipBadgeTooltip$a;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Ltv/periscope/android/view/VipBadgeTooltip;->a:Ltv/periscope/android/view/VipBadgeTooltip$a;

    .line 156
    return-void
.end method
