.class public Ltv/periscope/android/view/ae;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/view/ae$a;
    }
.end annotation


# instance fields
.field protected final a:I

.field protected b:F

.field private final c:F

.field private final d:F

.field private e:Ltv/periscope/android/view/ae$a;

.field private f:F

.field private g:F

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/ae;->a:I

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    iput v0, p0, Ltv/periscope/android/view/ae;->c:F

    .line 37
    iget v0, p0, Ltv/periscope/android/view/ae;->c:F

    const v1, 0x3e4ccccd    # 0.2f

    mul-float/2addr v0, v1

    iput v0, p0, Ltv/periscope/android/view/ae;->d:F

    .line 38
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/view/ae;)Ltv/periscope/android/view/ae$a;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Ltv/periscope/android/view/ae;->e:Ltv/periscope/android/view/ae$a;

    return-object v0
.end method

.method private a(Landroid/view/View;F)V
    .locals 6

    .prologue
    .line 160
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    .line 161
    float-to-double v2, p2

    invoke-direct {p0, v0}, Ltv/periscope/android/view/ae;->b(F)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    .line 162
    add-float/2addr v0, v1

    invoke-direct {p0, v0}, Ltv/periscope/android/view/ae;->c(F)F

    move-result v0

    .line 163
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 164
    iget-object v1, p0, Ltv/periscope/android/view/ae;->e:Ltv/periscope/android/view/ae$a;

    if-eqz v1, :cond_0

    .line 165
    iget-object v1, p0, Ltv/periscope/android/view/ae;->e:Ltv/periscope/android/view/ae$a;

    invoke-interface {v1, v0}, Ltv/periscope/android/view/ae$a;->a(F)V

    .line 167
    :cond_0
    return-void
.end method

.method private a(FF)Z
    .locals 2

    .prologue
    .line 116
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 120
    iget v1, p0, Ltv/periscope/android/view/ae;->h:I

    if-ltz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(F)D
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 172
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 173
    iget v1, p0, Ltv/periscope/android/view/ae;->d:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    .line 174
    float-to-double v2, v0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    .line 175
    float-to-double v0, v1

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 176
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    div-double v0, v2, v0

    sub-double v0, v4, v0

    .line 177
    return-wide v0
.end method

.method private b(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 129
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    .line 130
    iget v1, p0, Ltv/periscope/android/view/ae;->d:F

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_0

    iget v1, p0, Ltv/periscope/android/view/ae;->d:F

    neg-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 131
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/ae;->e:Ltv/periscope/android/view/ae$a;

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Ltv/periscope/android/view/ae;->e:Ltv/periscope/android/view/ae$a;

    invoke-interface {v0}, Ltv/periscope/android/view/ae$a;->a()V

    .line 135
    :cond_1
    const/4 v0, 0x1

    .line 138
    :goto_0
    return v0

    .line 137
    :cond_2
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/ae;->a(Landroid/view/View;)V

    .line 138
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(F)F
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Ltv/periscope/android/view/ae;->c:F

    neg-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 182
    iget v0, p0, Ltv/periscope/android/view/ae;->c:F

    neg-float p1, v0

    .line 186
    :cond_0
    :goto_0
    return p1

    .line 183
    :cond_1
    iget v0, p0, Ltv/periscope/android/view/ae;->c:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 184
    iget p1, p0, Ltv/periscope/android/view/ae;->c:F

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 143
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_0

    .line 144
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput v3, v1, v2

    .line 145
    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 146
    new-instance v1, Ltv/periscope/android/view/ae$1;

    invoke-direct {v1, p0}, Ltv/periscope/android/view/ae$1;-><init>(Ltv/periscope/android/view/ae;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 155
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 157
    :cond_0
    return-void
.end method

.method public a(Ltv/periscope/android/view/ae$a;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Ltv/periscope/android/view/ae;->e:Ltv/periscope/android/view/ae$a;

    .line 191
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Ltv/periscope/android/view/ae;->i:Z

    return v0
.end method

.method protected a(F)Z
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Ltv/periscope/android/view/ae;->a:I

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 48
    .line 50
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 95
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 52
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/ae;->f:F

    .line 53
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/ae;->g:F

    iput v0, p0, Ltv/periscope/android/view/ae;->b:F

    .line 54
    iput-boolean v1, p0, Ltv/periscope/android/view/ae;->i:Z

    .line 55
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/view/ae;->h:I

    goto :goto_0

    .line 59
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    .line 60
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    .line 61
    iget v3, p0, Ltv/periscope/android/view/ae;->b:F

    sub-float v3, v2, v3

    .line 62
    iget v4, p0, Ltv/periscope/android/view/ae;->f:F

    sub-float v4, v0, v4

    .line 63
    iget v5, p0, Ltv/periscope/android/view/ae;->g:F

    sub-float v5, v2, v5

    .line 64
    iput v0, p0, Ltv/periscope/android/view/ae;->f:F

    .line 65
    iput v2, p0, Ltv/periscope/android/view/ae;->g:F

    .line 66
    invoke-direct {p0, p2}, Ltv/periscope/android/view/ae;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/view/ae;->i:Z

    if-nez v0, :cond_1

    .line 67
    invoke-virtual {p0, v3}, Ltv/periscope/android/view/ae;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-direct {p0, v4, v5}, Ltv/periscope/android/view/ae;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/view/ae;->i:Z

    .line 70
    invoke-direct {p0, p1, v5}, Ltv/periscope/android/view/ae;->a(Landroid/view/View;F)V

    goto :goto_0

    .line 75
    :pswitch_3
    invoke-virtual {p0, p1}, Ltv/periscope/android/view/ae;->a(Landroid/view/View;)V

    .line 76
    iput-boolean v1, p0, Ltv/periscope/android/view/ae;->i:Z

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Ltv/periscope/android/view/ae;->h:I

    goto :goto_0

    .line 82
    :pswitch_4
    invoke-direct {p0, p2}, Ltv/periscope/android/view/ae;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83
    iget-boolean v0, p0, Ltv/periscope/android/view/ae;->i:Z

    if-eqz v0, :cond_2

    .line 84
    invoke-direct {p0, p1}, Ltv/periscope/android/view/ae;->b(Landroid/view/View;)Z

    move-result v0

    .line 87
    :goto_1
    iput-boolean v1, p0, Ltv/periscope/android/view/ae;->i:Z

    move v1, v0

    .line 88
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 50
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
