.class Ltv/periscope/android/view/BaseProfileSheet$2;
.super Ltv/periscope/android/view/v;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltv/periscope/android/view/BaseProfileSheet;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/view/BaseProfileSheet;


# direct methods
.method constructor <init>(Ltv/periscope/android/view/BaseProfileSheet;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    invoke-direct {p0}, Ltv/periscope/android/view/v;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 133
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    iput-boolean v1, v0, Ltv/periscope/android/view/BaseProfileSheet;->k:Z

    .line 134
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    invoke-static {v0, v1}, Ltv/periscope/android/view/BaseProfileSheet;->a(Ltv/periscope/android/view/BaseProfileSheet;Z)Z

    .line 135
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ltv/periscope/android/view/BaseProfileSheet;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    iget-boolean v0, v0, Ltv/periscope/android/view/BaseProfileSheet;->j:Z

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    invoke-virtual {v0}, Ltv/periscope/android/view/BaseProfileSheet;->a()V

    .line 142
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    iget-object v0, v0, Ltv/periscope/android/view/BaseProfileSheet;->g:Ltv/periscope/android/view/t;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    invoke-static {v0}, Ltv/periscope/android/view/BaseProfileSheet;->b(Ltv/periscope/android/view/BaseProfileSheet;)Ltv/periscope/android/ui/d;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 143
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    invoke-static {v0}, Ltv/periscope/android/view/BaseProfileSheet;->b(Ltv/periscope/android/view/BaseProfileSheet;)Ltv/periscope/android/ui/d;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    invoke-virtual {v0}, Ltv/periscope/android/view/BaseProfileSheet;->e()V

    .line 145
    iget-object v1, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    iget-object v0, v0, Ltv/periscope/android/view/BaseProfileSheet;->g:Ltv/periscope/android/view/t;

    invoke-interface {v0}, Ltv/periscope/android/view/t;->e()Lcyw;

    move-result-object v0

    iget-object v2, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    invoke-static {v2}, Ltv/periscope/android/view/BaseProfileSheet;->b(Ltv/periscope/android/view/BaseProfileSheet;)Ltv/periscope/android/ui/d;

    move-result-object v2

    iget-object v2, v2, Ltv/periscope/android/ui/d;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    invoke-virtual {v1, v0}, Ltv/periscope/android/view/BaseProfileSheet;->a(Ltv/periscope/android/api/PsUser;)V

    .line 149
    :cond_1
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ltv/periscope/android/view/BaseProfileSheet;->a(Ltv/periscope/android/view/BaseProfileSheet;Ltv/periscope/android/ui/d;)Ltv/periscope/android/ui/d;

    .line 150
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    invoke-virtual {v0}, Ltv/periscope/android/view/BaseProfileSheet;->h()V

    .line 152
    :cond_2
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Ltv/periscope/android/view/BaseProfileSheet$2;->a:Ltv/periscope/android/view/BaseProfileSheet;

    const/4 v1, 0x1

    iput-boolean v1, v0, Ltv/periscope/android/view/BaseProfileSheet;->k:Z

    .line 129
    return-void
.end method
