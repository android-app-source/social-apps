.class public Ltv/periscope/android/view/g;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/view/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Ltv/periscope/android/view/g$a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/chat/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ltv/periscope/android/view/ak;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ltv/periscope/android/view/ak",
            "<",
            "Ltv/periscope/android/ui/chat/e;",
            "Ltv/periscope/model/chat/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ltv/periscope/android/view/k;


# direct methods
.method public constructor <init>(Ltv/periscope/android/view/ak;Ltv/periscope/android/view/k;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/android/view/ak",
            "<",
            "Ltv/periscope/android/ui/chat/e;",
            "Ltv/periscope/model/chat/Message;",
            ">;",
            "Ltv/periscope/android/view/k;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 47
    iput-object p1, p0, Ltv/periscope/android/view/g;->b:Ltv/periscope/android/view/ak;

    .line 48
    iput-object p2, p0, Ltv/periscope/android/view/g;->c:Ltv/periscope/android/view/k;

    .line 49
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Ltv/periscope/android/view/g$a;
    .locals 3

    .prologue
    .line 58
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$i;->ps__carousel_chat_row:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 59
    new-instance v1, Ltv/periscope/android/view/g$a;

    iget-object v2, p0, Ltv/periscope/android/view/g;->c:Ltv/periscope/android/view/k;

    invoke-direct {v1, v0, v2}, Ltv/periscope/android/view/g$a;-><init>(Landroid/view/View;Ltv/periscope/android/view/k;)V

    return-object v1
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/chat/Message;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    iput-object p1, p0, Ltv/periscope/android/view/g;->a:Ljava/util/List;

    .line 53
    invoke-virtual {p0}, Ltv/periscope/android/view/g;->notifyDataSetChanged()V

    .line 54
    return-void
.end method

.method public a(Ltv/periscope/android/view/g$a;I)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Ltv/periscope/android/view/g;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/chat/Message;

    .line 65
    iget-object v1, p0, Ltv/periscope/android/view/g;->b:Ltv/periscope/android/view/ak;

    invoke-interface {v1, p1, v0, p2}, Ltv/periscope/android/view/ak;->a(Landroid/support/v7/widget/RecyclerView$ViewHolder;Ljava/lang/Object;I)V

    .line 66
    iput-object v0, p1, Ltv/periscope/android/view/g$a;->o:Ltv/periscope/model/chat/Message;

    .line 67
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Ltv/periscope/android/view/g;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 74
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ltv/periscope/android/view/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Ltv/periscope/android/view/g$a;

    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/view/g;->a(Ltv/periscope/android/view/g$a;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1, p2}, Ltv/periscope/android/view/g;->a(Landroid/view/ViewGroup;I)Ltv/periscope/android/view/g$a;

    move-result-object v0

    return-object v0
.end method
