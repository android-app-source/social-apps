.class public Ltv/periscope/android/graphics/n;
.super Ltv/periscope/android/graphics/o;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/graphics/n$a;
    }
.end annotation


# static fields
.field private static final c:Lczt;

.field private static final d:Lczt;

.field private static final e:Lczt;

.field private static final f:Lczw;

.field private static final g:Lczw;

.field private static final h:Lczw;

.field private static final i:Lczw;


# instance fields
.field private final j:Ltv/periscope/android/graphics/d;

.field private final k:Ltv/periscope/android/graphics/c;

.field private l:[F

.field private m:Lczw;

.field private n:Lczt;

.field private o:Lczt;

.field private final p:Ltv/periscope/android/graphics/m;

.field private final q:Ltv/periscope/android/graphics/n$a;

.field private r:J


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const v4, 0x3fc90fdb

    const v3, -0x4036f025

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 25
    new-instance v0, Lczt;

    invoke-direct {v0, v2, v1, v1}, Lczt;-><init>(FFF)V

    sput-object v0, Ltv/periscope/android/graphics/n;->c:Lczt;

    .line 26
    new-instance v0, Lczt;

    invoke-direct {v0, v1, v2, v1}, Lczt;-><init>(FFF)V

    sput-object v0, Ltv/periscope/android/graphics/n;->d:Lczt;

    .line 27
    new-instance v0, Lczt;

    invoke-direct {v0, v1, v1, v2}, Lczt;-><init>(FFF)V

    sput-object v0, Ltv/periscope/android/graphics/n;->e:Lczt;

    .line 28
    sget-object v0, Ltv/periscope/android/graphics/n;->e:Lczt;

    invoke-static {v3, v0}, Lczw;->a(FLczt;)Lczw;

    move-result-object v0

    sput-object v0, Ltv/periscope/android/graphics/n;->f:Lczw;

    .line 29
    sget-object v0, Ltv/periscope/android/graphics/n;->d:Lczt;

    invoke-static {v4, v0}, Lczw;->a(FLczt;)Lczw;

    move-result-object v0

    sput-object v0, Ltv/periscope/android/graphics/n;->g:Lczw;

    .line 30
    sget-object v0, Ltv/periscope/android/graphics/n;->e:Lczt;

    invoke-static {v3, v0}, Lczw;->a(FLczt;)Lczw;

    move-result-object v0

    sput-object v0, Ltv/periscope/android/graphics/n;->h:Lczw;

    .line 31
    sget-object v0, Ltv/periscope/android/graphics/n;->e:Lczt;

    invoke-static {v4, v0}, Lczw;->a(FLczt;)Lczw;

    move-result-object v0

    sput-object v0, Ltv/periscope/android/graphics/n;->i:Lczw;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0, p1}, Ltv/periscope/android/graphics/o;-><init>(Landroid/content/Context;)V

    .line 36
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Ltv/periscope/android/graphics/n;->l:[F

    .line 37
    sget-object v0, Lczw;->a:Lczw;

    iput-object v0, p0, Ltv/periscope/android/graphics/n;->m:Lczw;

    .line 38
    new-instance v0, Lczt;

    invoke-direct {v0}, Lczt;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/n;->n:Lczt;

    .line 39
    new-instance v0, Lczt;

    invoke-direct {v0}, Lczt;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/n;->o:Lczt;

    .line 49
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/graphics/n;->r:J

    .line 51
    new-instance v0, Ltv/periscope/android/graphics/d;

    invoke-direct {v0}, Ltv/periscope/android/graphics/d;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/n;->j:Ltv/periscope/android/graphics/d;

    .line 52
    new-instance v0, Ltv/periscope/android/graphics/c;

    sget v1, Ltv/periscope/android/library/f$k;->video_360_vs:I

    sget v2, Ltv/periscope/android/library/f$k;->video_360_fs:I

    invoke-direct {v0, p1, v1, v2}, Ltv/periscope/android/graphics/c;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Ltv/periscope/android/graphics/n;->k:Ltv/periscope/android/graphics/c;

    .line 53
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->l:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 55
    new-instance v0, Ltv/periscope/android/graphics/m;

    invoke-direct {v0, p1}, Ltv/periscope/android/graphics/m;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ltv/periscope/android/graphics/n;->p:Ltv/periscope/android/graphics/m;

    .line 56
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->p:Ltv/periscope/android/graphics/m;

    new-instance v1, Ltv/periscope/android/graphics/n$1;

    invoke-direct {v1, p0}, Ltv/periscope/android/graphics/n$1;-><init>(Ltv/periscope/android/graphics/n;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/graphics/m;->a(Ltv/periscope/android/graphics/m$a;)V

    .line 62
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->p:Ltv/periscope/android/graphics/m;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/m;->a()V

    .line 64
    new-instance v0, Ltv/periscope/android/graphics/n$a;

    new-instance v1, Ltv/periscope/android/graphics/n$2;

    invoke-direct {v1, p0}, Ltv/periscope/android/graphics/n$2;-><init>(Ltv/periscope/android/graphics/n;)V

    invoke-direct {v0, p1, v1}, Ltv/periscope/android/graphics/n$a;-><init>(Landroid/content/Context;Ltv/periscope/android/graphics/n$a$a;)V

    iput-object v0, p0, Ltv/periscope/android/graphics/n;->q:Ltv/periscope/android/graphics/n$a;

    .line 80
    return-void
.end method

.method static synthetic a(Ltv/periscope/android/graphics/n;)Lczt;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->n:Lczt;

    return-object v0
.end method

.method static synthetic a(Ltv/periscope/android/graphics/n;Lczt;)Lczt;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Ltv/periscope/android/graphics/n;->o:Lczt;

    return-object p1
.end method

.method private a(Lczw;I)Lczw;
    .locals 1

    .prologue
    .line 155
    packed-switch p2, :pswitch_data_0

    .line 165
    :goto_0
    :pswitch_0
    return-object p1

    .line 160
    :pswitch_1
    sget-object v0, Ltv/periscope/android/graphics/n;->h:Lczw;

    invoke-virtual {p1, v0}, Lczw;->b(Lczw;)Lczw;

    move-result-object p1

    goto :goto_0

    .line 162
    :pswitch_2
    sget-object v0, Ltv/periscope/android/graphics/n;->i:Lczw;

    invoke-virtual {p1, v0}, Lczw;->b(Lczw;)Lczw;

    move-result-object p1

    goto :goto_0

    .line 155
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private declared-synchronized a(Lczw;)V
    .locals 2

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 144
    monitor-enter p0

    .line 145
    :try_start_0
    iget v0, p1, Lczw;->b:F

    mul-float/2addr v0, v1

    iput v0, p1, Lczw;->b:F

    .line 146
    iget v0, p1, Lczw;->c:F

    mul-float/2addr v0, v1

    iput v0, p1, Lczw;->c:F

    .line 147
    invoke-virtual {p1}, Lczw;->d()Lczw;

    move-result-object v0

    .line 148
    sget-object v1, Ltv/periscope/android/graphics/n;->g:Lczw;

    invoke-virtual {v1, v0}, Lczw;->b(Lczw;)Lczw;

    move-result-object v0

    .line 149
    sget-object v1, Ltv/periscope/android/graphics/n;->f:Lczw;

    invoke-virtual {v1, v0}, Lczw;->b(Lczw;)Lczw;

    move-result-object v0

    .line 151
    iput-object v0, p0, Ltv/periscope/android/graphics/n;->m:Lczw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    monitor-exit p0

    return-void

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Ltv/periscope/android/graphics/n;Lczw;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Ltv/periscope/android/graphics/n;->a(Lczw;)V

    return-void
.end method

.method private declared-synchronized a(F)[F
    .locals 4

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Ltv/periscope/android/graphics/n;->d()Lczw;

    move-result-object v0

    .line 132
    invoke-direct {p0, p1}, Ltv/periscope/android/graphics/n;->c(F)V

    .line 133
    iget-object v1, p0, Ltv/periscope/android/graphics/n;->n:Lczt;

    iget v1, v1, Lczt;->a:F

    sget-object v2, Ltv/periscope/android/graphics/n;->d:Lczt;

    invoke-static {v1, v2}, Lczw;->a(FLczt;)Lczw;

    move-result-object v1

    .line 134
    iget-object v2, p0, Ltv/periscope/android/graphics/n;->n:Lczt;

    iget v2, v2, Lczt;->b:F

    sget-object v3, Ltv/periscope/android/graphics/n;->c:Lczt;

    invoke-static {v2, v3}, Lczw;->a(FLczt;)Lczw;

    move-result-object v2

    .line 136
    invoke-virtual {v0, v2}, Lczw;->b(Lczw;)Lczw;

    move-result-object v0

    invoke-virtual {v1, v0}, Lczw;->b(Lczw;)Lczw;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Lczw;->d()Lczw;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lczw;->e()[F

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/n;->l:[F

    .line 140
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->l:[F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(F)F
    .locals 4

    .prologue
    .line 173
    const-wide v0, 0x3feb333340000000L    # 0.8500000238418579

    const/high16 v2, 0x42700000    # 60.0f

    mul-float/2addr v2, p1

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method static synthetic b(Ltv/periscope/android/graphics/n;Lczt;)Lczt;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Ltv/periscope/android/graphics/n;->n:Lczt;

    return-object p1
.end method

.method private c(F)V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->n:Lczt;

    iget-object v1, p0, Ltv/periscope/android/graphics/n;->o:Lczt;

    invoke-virtual {p0, v1, p1}, Ltv/periscope/android/graphics/n;->b(Lczt;F)Lczt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lczt;->a(Lczt;)Lczt;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/n;->n:Lczt;

    .line 191
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->o:Lczt;

    invoke-virtual {p0, v0, p1}, Ltv/periscope/android/graphics/n;->a(Lczt;F)Lczt;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/n;->o:Lczt;

    .line 192
    return-void
.end method

.method private d()Lczw;
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->m:Lczw;

    iget v1, p0, Ltv/periscope/android/graphics/n;->a:I

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/graphics/n;->a(Lczw;I)Lczw;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a(Lczt;F)Lczt;
    .locals 1

    .prologue
    .line 178
    invoke-direct {p0, p2}, Ltv/periscope/android/graphics/n;->b(F)F

    move-result v0

    invoke-virtual {p1, v0}, Lczt;->a(F)Lczt;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->q:Ltv/periscope/android/graphics/n$a;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/n$a;->c()V

    .line 85
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->p:Ltv/periscope/android/graphics/m;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/m;->b()V

    .line 86
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->j:Ltv/periscope/android/graphics/d;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/d;->a()V

    .line 87
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->k:Ltv/periscope/android/graphics/c;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/c;->c()V

    .line 89
    invoke-super {p0}, Ltv/periscope/android/graphics/o;->a()V

    .line 90
    return-void
.end method

.method public a(Ltv/periscope/android/graphics/i;)V
    .locals 6

    .prologue
    .line 109
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Ltv/periscope/android/graphics/n;->r:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    .line 110
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Ltv/periscope/android/graphics/n;->r:J

    .line 112
    invoke-static {}, Ltv/periscope/android/graphics/k;->a()Ltv/periscope/android/util/Size;

    move-result-object v1

    .line 113
    invoke-virtual {p1}, Ltv/periscope/android/graphics/i;->d()Landroid/graphics/SurfaceTexture;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 115
    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v0, v2

    invoke-direct {p0, v0}, Ltv/periscope/android/graphics/n;->a(F)[F

    move-result-object v0

    .line 117
    iget-object v2, p0, Ltv/periscope/android/graphics/n;->j:Ltv/periscope/android/graphics/d;

    iget-object v3, p0, Ltv/periscope/android/graphics/n;->k:Ltv/periscope/android/graphics/c;

    invoke-virtual {v2, v3}, Ltv/periscope/android/graphics/d;->a(Ltv/periscope/android/graphics/c;)V

    .line 118
    iget-object v2, p0, Ltv/periscope/android/graphics/n;->j:Ltv/periscope/android/graphics/d;

    const-string/jumbo v3, "texture"

    const/16 v4, 0x2601

    const v5, 0x812f

    invoke-virtual {v2, v3, p1, v4, v5}, Ltv/periscope/android/graphics/d;->a(Ljava/lang/String;Ltv/periscope/android/graphics/j;II)V

    .line 119
    iget-object v2, p0, Ltv/periscope/android/graphics/n;->j:Ltv/periscope/android/graphics/d;

    invoke-virtual {v1}, Ltv/periscope/android/util/Size;->a()I

    move-result v3

    invoke-virtual {v1}, Ltv/periscope/android/util/Size;->b()I

    move-result v1

    invoke-virtual {v2, v0, v3, v1}, Ltv/periscope/android/graphics/d;->a([FII)V

    .line 120
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->j:Ltv/periscope/android/graphics/d;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/d;->c()V

    .line 121
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->q:Ltv/periscope/android/graphics/n$a;

    invoke-virtual {v0, p1}, Ltv/periscope/android/graphics/n$a;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method b(Lczt;F)Lczt;
    .locals 6

    .prologue
    const-wide v4, 0x3feb333340000000L    # 0.8500000238418579

    .line 184
    const/high16 v0, 0x42700000    # 60.0f

    mul-float/2addr v0, p2

    float-to-double v0, v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v0, v2

    .line 185
    const-wide/high16 v2, 0x404e000000000000L    # 60.0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    .line 186
    div-double/2addr v0, v2

    double-to-float v0, v0

    invoke-virtual {p1, v0}, Lczt;->a(F)Lczt;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 93
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ltv/periscope/android/graphics/n;->r:J

    .line 94
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->p:Ltv/periscope/android/graphics/m;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/m;->a()V

    .line 95
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->q:Ltv/periscope/android/graphics/n$a;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/n$a;->a()V

    .line 96
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->q:Ltv/periscope/android/graphics/n$a;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/n$a;->b()V

    .line 100
    iget-object v0, p0, Ltv/periscope/android/graphics/n;->p:Ltv/periscope/android/graphics/m;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/m;->b()V

    .line 101
    return-void
.end method
