.class public Ltv/periscope/android/graphics/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/graphics/b$a;,
        Ltv/periscope/android/graphics/b$b;,
        Ltv/periscope/android/graphics/b$c;
    }
.end annotation


# instance fields
.field private a:Landroid/opengl/EGLSurface;

.field private b:Landroid/opengl/EGLDisplay;

.field private c:Landroid/opengl/EGLConfig;

.field private d:Landroid/opengl/EGLContext;

.field private e:Landroid/view/Surface;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/graphics/b;->e:Landroid/view/Surface;

    .line 34
    return-void
.end method


# virtual methods
.method public a()Landroid/opengl/EGLContext;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->d:Landroid/opengl/EGLContext;

    return-object v0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, Ltv/periscope/android/graphics/b;->a:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1, p1, p2}, Landroid/opengl/EGLExt;->eglPresentationTimeANDROID(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;J)Z

    .line 181
    return-void
.end method

.method public declared-synchronized a(Landroid/view/Surface;)V
    .locals 4

    .prologue
    .line 169
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x3038

    aput v2, v0, v1

    .line 170
    iget-object v1, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, Ltv/periscope/android/graphics/b;->a:Landroid/opengl/EGLSurface;

    invoke-static {v1, v2}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 171
    iget-object v1, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, Ltv/periscope/android/graphics/b;->c:Landroid/opengl/EGLConfig;

    const/4 v3, 0x0

    invoke-static {v1, v2, p1, v0, v3}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/b;->a:Landroid/opengl/EGLSurface;

    .line 173
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->e:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->e:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 176
    :cond_0
    iput-object p1, p0, Ltv/periscope/android/graphics/b;->e:Landroid/view/Surface;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    monitor-exit p0

    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ltv/periscope/android/graphics/b$c;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 124
    monitor-enter p0

    move v0, v1

    move v2, v1

    .line 126
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    const/16 v3, 0xa

    if-ge v0, v3, :cond_1

    .line 127
    :try_start_0
    iget-object v2, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    iget-object v3, p0, Ltv/periscope/android/graphics/b;->a:Landroid/opengl/EGLSurface;

    iget-object v4, p0, Ltv/periscope/android/graphics/b;->a:Landroid/opengl/EGLSurface;

    iget-object v5, p0, Ltv/periscope/android/graphics/b;->d:Landroid/opengl/EGLContext;

    invoke-static {v2, v3, v4, v5}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v2

    .line 129
    if-nez v2, :cond_0

    .line 130
    iget-object v3, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    sget-object v4, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v5, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v6, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v3, v4, v5, v6}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    const-wide/16 v4, 0xa

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :cond_1
    if-eqz v2, :cond_2

    .line 140
    :try_start_2
    invoke-interface {p1}, Ltv/periscope/android/graphics/b$c;->a()V

    .line 141
    invoke-static {}, Landroid/opengl/GLES20;->glFinish()V

    .line 142
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v4, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v2, v3, v4}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 147
    :goto_2
    monitor-exit p0

    return v1

    .line 145
    :cond_2
    :try_start_3
    invoke-interface {p1}, Ltv/periscope/android/graphics/b$c;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 134
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public declared-synchronized a(Ltv/periscope/android/graphics/b;Landroid/view/Surface;)Z
    .locals 10

    .prologue
    const/16 v0, 0x3038

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 37
    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {v1}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v1

    iput-object v1, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    .line 38
    iget-object v1, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 116
    :cond_0
    :goto_0
    monitor-exit p0

    return v9

    .line 43
    :cond_1
    const/16 v1, 0xf

    :try_start_1
    new-array v1, v1, [I

    const/4 v2, 0x0

    const/16 v3, 0x3024

    aput v3, v1, v2

    const/4 v2, 0x1

    const/16 v3, 0x8

    aput v3, v1, v2

    const/4 v2, 0x2

    const/16 v3, 0x3023

    aput v3, v1, v2

    const/4 v2, 0x3

    const/16 v3, 0x8

    aput v3, v1, v2

    const/4 v2, 0x4

    const/16 v3, 0x3022

    aput v3, v1, v2

    const/4 v2, 0x5

    const/16 v3, 0x8

    aput v3, v1, v2

    const/4 v2, 0x6

    const/16 v3, 0x3021

    aput v3, v1, v2

    const/4 v2, 0x7

    const/4 v3, 0x0

    aput v3, v1, v2

    const/16 v2, 0x8

    const/16 v3, 0x3033

    aput v3, v1, v2

    const/16 v2, 0x9

    const/4 v3, 0x5

    aput v3, v1, v2

    const/16 v2, 0xa

    const/16 v3, 0x3040

    aput v3, v1, v2

    const/16 v2, 0xb

    const/4 v3, 0x4

    aput v3, v1, v2

    const/16 v3, 0xc

    if-eqz p2, :cond_4

    const/16 v2, 0x3142

    :goto_1
    aput v2, v1, v3

    const/16 v2, 0xd

    if-eqz p2, :cond_2

    move v0, v8

    :cond_2
    aput v0, v1, v2

    const/16 v0, 0xe

    const/16 v2, 0x3038

    aput v2, v1, v0

    .line 54
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 55
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 56
    iget-object v3, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v2, v4, v0, v5}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const/4 v0, 0x1

    new-array v3, v0, [Landroid/opengl/EGLConfig;

    .line 60
    const/4 v0, 0x1

    new-array v6, v0, [I

    .line 61
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, 0x0

    aget-object v0, v3, v0

    iput-object v0, p0, Ltv/periscope/android/graphics/b;->c:Landroid/opengl/EGLConfig;

    .line 66
    if-eqz p2, :cond_3

    .line 68
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 69
    iget-object v1, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, Ltv/periscope/android/graphics/b;->c:Landroid/opengl/EGLConfig;

    const/16 v4, 0x3142

    const/4 v5, 0x0

    invoke-static {v1, v2, v4, v0, v5}, Landroid/opengl/EGL14;->eglGetConfigAttrib(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;I[II)Z

    .line 70
    const/4 v1, 0x0

    aget v0, v0, v1

    if-nez v0, :cond_3

    .line 73
    const/16 v0, 0xd

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    .line 82
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const/4 v0, 0x0

    aget-object v0, v3, v0

    iput-object v0, p0, Ltv/periscope/android/graphics/b;->c:Landroid/opengl/EGLConfig;

    .line 89
    :cond_3
    const/4 v0, 0x3

    new-array v1, v0, [I

    fill-array-data v1, :array_1

    .line 93
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ltv/periscope/android/graphics/b;->a()Landroid/opengl/EGLContext;

    move-result-object v0

    .line 94
    :goto_2
    iget-object v2, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    iget-object v3, p0, Ltv/periscope/android/graphics/b;->c:Landroid/opengl/EGLConfig;

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v1, v4}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v0

    .line 97
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v1

    const/16 v2, 0x3000

    if-ne v1, v2, :cond_0

    .line 101
    iput-object v0, p0, Ltv/periscope/android/graphics/b;->d:Landroid/opengl/EGLContext;

    .line 103
    if-eqz p2, :cond_6

    .line 104
    iput-object p2, p0, Ltv/periscope/android/graphics/b;->e:Landroid/view/Surface;

    .line 105
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x3038

    aput v2, v0, v1

    .line 106
    iget-object v1, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, Ltv/periscope/android/graphics/b;->c:Landroid/opengl/EGLConfig;

    const/4 v3, 0x0

    invoke-static {v1, v2, p2, v0, v3}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/b;->a:Landroid/opengl/EGLSurface;

    .line 116
    :goto_3
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->a:Landroid/opengl/EGLSurface;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-eq v0, v1, :cond_7

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v0

    const/16 v1, 0x3000

    if-ne v0, v1, :cond_7

    move v0, v8

    :goto_4
    move v9, v0

    goto/16 :goto_0

    :cond_4
    move v2, v0

    .line 43
    goto/16 :goto_1

    .line 93
    :cond_5
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    goto :goto_2

    .line 108
    :cond_6
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    .line 113
    iget-object v1, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, Ltv/periscope/android/graphics/b;->c:Landroid/opengl/EGLConfig;

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Landroid/opengl/EGL14;->eglCreatePbufferSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;[II)Landroid/opengl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/b;->a:Landroid/opengl/EGLSurface;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    move v0, v9

    .line 116
    goto :goto_4

    .line 73
    :array_0
    .array-data 4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x0
        0x3033
        0x5
        0x3040
        0x4
        0x3038
    .end array-data

    .line 89
    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data

    .line 108
    :array_2
    .array-data 4
        0x3057
        0x4
        0x3056
        0x4
        0x3038
    .end array-data
.end method

.method public declared-synchronized b()V
    .locals 4

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 154
    :cond_1
    :try_start_1
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 156
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, Ltv/periscope/android/graphics/b;->a:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 157
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, Ltv/periscope/android/graphics/b;->d:Landroid/opengl/EGLContext;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    .line 158
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    invoke-static {v0}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/graphics/b;->d:Landroid/opengl/EGLContext;

    .line 160
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/graphics/b;->a:Landroid/opengl/EGLSurface;

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    .line 162
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->e:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->e:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/graphics/b;->e:Landroid/view/Surface;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Ltv/periscope/android/graphics/b;->b:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, Ltv/periscope/android/graphics/b;->a:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 185
    return-void
.end method

.method public d()Ltv/periscope/android/graphics/GLRenderView$f;
    .locals 1

    .prologue
    .line 188
    new-instance v0, Ltv/periscope/android/graphics/b$b;

    invoke-direct {v0, p0}, Ltv/periscope/android/graphics/b$b;-><init>(Ltv/periscope/android/graphics/b;)V

    return-object v0
.end method

.method public e()Ltv/periscope/android/graphics/GLRenderView$e;
    .locals 1

    .prologue
    .line 192
    new-instance v0, Ltv/periscope/android/graphics/b$a;

    invoke-direct {v0}, Ltv/periscope/android/graphics/b$a;-><init>()V

    return-object v0
.end method
