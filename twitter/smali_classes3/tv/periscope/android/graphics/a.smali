.class public Ltv/periscope/android/graphics/a;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/graphics/f;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:Ltv/periscope/android/util/Size;

.field private e:Ltv/periscope/android/util/Size;

.field private f:Ltv/periscope/android/util/Size;

.field private g:I

.field private h:Z

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/a;->a:Ljava/util/List;

    .line 27
    const-string/jumbo v0, ""

    iput-object v0, p0, Ltv/periscope/android/graphics/a;->i:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public a(Ltv/periscope/android/graphics/c;Ltv/periscope/android/graphics/c;Ltv/periscope/android/graphics/e;Ltv/periscope/android/graphics/j;)Ltv/periscope/android/graphics/f;
    .locals 10

    .prologue
    const v9, 0x812f

    const/16 v8, 0x2601

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 86
    const/4 v0, 0x0

    .line 87
    iget-object v1, p0, Ltv/periscope/android/graphics/a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v6, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/graphics/f;

    .line 88
    invoke-virtual {p3, v0}, Ltv/periscope/android/graphics/e;->a(Ltv/periscope/android/graphics/f;)V

    .line 89
    instance-of v1, p4, Ltv/periscope/android/graphics/i;

    if-eqz v1, :cond_0

    move-object v1, p2

    .line 90
    :goto_1
    invoke-virtual {p3, v1}, Ltv/periscope/android/graphics/e;->a(Ltv/periscope/android/graphics/c;)V

    .line 91
    const-string/jumbo v5, "Random"

    invoke-virtual {v1, v5}, Ltv/periscope/android/graphics/c;->b(Ljava/lang/String;)I

    move-result v1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    double-to-float v5, v6

    invoke-static {v1, v5}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 92
    const-string/jumbo v1, "Texture0"

    invoke-virtual {p3, v1, p4, v8, v9}, Ltv/periscope/android/graphics/e;->a(Ljava/lang/String;Ltv/periscope/android/graphics/j;II)V

    .line 93
    invoke-virtual {p3}, Ltv/periscope/android/graphics/e;->b()V

    .line 94
    invoke-virtual {v0}, Ltv/periscope/android/graphics/f;->b()Ltv/periscope/android/graphics/j;

    move-result-object p4

    .line 96
    invoke-virtual {p3}, Ltv/periscope/android/graphics/e;->c()V

    .line 97
    invoke-virtual {p3}, Ltv/periscope/android/graphics/e;->d()V

    move-object v6, v0

    .line 98
    goto :goto_0

    :cond_0
    move-object v1, p1

    .line 89
    goto :goto_1

    .line 99
    :cond_1
    iget-boolean v0, p0, Ltv/periscope/android/graphics/a;->h:Z

    if-eqz v0, :cond_2

    move v0, v2

    .line 102
    :goto_2
    iget-object v1, p0, Ltv/periscope/android/graphics/a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_5

    .line 104
    iget-boolean v0, p0, Ltv/periscope/android/graphics/a;->h:Z

    if-eqz v0, :cond_3

    .line 106
    :goto_3
    instance-of v0, p4, Ltv/periscope/android/graphics/i;

    if-eqz v0, :cond_4

    .line 107
    :goto_4
    invoke-virtual {p3, p2}, Ltv/periscope/android/graphics/e;->a(Ltv/periscope/android/graphics/c;)V

    .line 108
    const-string/jumbo v0, "Texture0"

    invoke-virtual {p3, v0, p4, v8, v9}, Ltv/periscope/android/graphics/e;->a(Ljava/lang/String;Ltv/periscope/android/graphics/j;II)V

    .line 109
    const-string/jumbo v0, "Mirror"

    invoke-virtual {p2, v0}, Ltv/periscope/android/graphics/c;->b(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 110
    const-string/jumbo v0, "Random"

    invoke-virtual {p2, v0}, Ltv/periscope/android/graphics/c;->b(Ljava/lang/String;)I

    move-result v0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 111
    iget v1, p0, Ltv/periscope/android/graphics/a;->b:I

    iget v2, p0, Ltv/periscope/android/graphics/a;->c:I

    iget-object v0, p0, Ltv/periscope/android/graphics/a;->d:Ltv/periscope/android/util/Size;

    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->a()I

    move-result v3

    iget-object v0, p0, Ltv/periscope/android/graphics/a;->d:Ltv/periscope/android/util/Size;

    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->b()I

    move-result v4

    iget v5, p0, Ltv/periscope/android/graphics/a;->g:I

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, Ltv/periscope/android/graphics/e;->a(IIIII)V

    .line 112
    invoke-virtual {p3}, Ltv/periscope/android/graphics/e;->c()V

    .line 113
    return-object v6

    :cond_2
    move v0, v3

    .line 99
    goto :goto_2

    :cond_3
    move v3, v2

    .line 104
    goto :goto_3

    :cond_4
    move-object p2, p1

    .line 106
    goto :goto_4

    :cond_5
    move v3, v0

    goto :goto_3
.end method

.method a()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/graphics/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/graphics/f;

    .line 80
    invoke-virtual {v0}, Ltv/periscope/android/graphics/f;->a()V

    goto :goto_0

    .line 82
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/graphics/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 83
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Ltv/periscope/android/graphics/a;->i:Ljava/lang/String;

    .line 31
    return-void
.end method

.method a(Ltv/periscope/android/util/Size;Ltv/periscope/android/util/Size;IZ)V
    .locals 8

    .prologue
    .line 34
    iput-boolean p4, p0, Ltv/periscope/android/graphics/a;->h:Z

    .line 36
    iget-object v0, p0, Ltv/periscope/android/graphics/a;->e:Ltv/periscope/android/util/Size;

    invoke-virtual {p1, v0}, Ltv/periscope/android/util/Size;->a(Ltv/periscope/android/util/Size;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/graphics/a;->f:Ltv/periscope/android/util/Size;

    invoke-virtual {p2, v0}, Ltv/periscope/android/util/Size;->a(Ltv/periscope/android/util/Size;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Ltv/periscope/android/graphics/a;->g:I

    if-ne p3, v0, :cond_1

    .line 76
    :cond_0
    return-void

    .line 40
    :cond_1
    invoke-virtual {p0}, Ltv/periscope/android/graphics/a;->a()V

    .line 42
    invoke-virtual {p2}, Ltv/periscope/android/util/Size;->d()F

    move-result v0

    .line 43
    invoke-virtual {p1, p3}, Ltv/periscope/android/util/Size;->a(I)Ltv/periscope/android/util/Size;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/android/util/Size;->d()F

    move-result v1

    .line 47
    const/4 v2, 0x0

    iput v2, p0, Ltv/periscope/android/graphics/a;->c:I

    iput v2, p0, Ltv/periscope/android/graphics/a;->b:I

    .line 48
    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 50
    invoke-virtual {p2}, Ltv/periscope/android/util/Size;->b()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-virtual {p2}, Ltv/periscope/android/util/Size;->b()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/Size;->a(FF)Ltv/periscope/android/util/Size;

    move-result-object v0

    .line 51
    invoke-virtual {p2}, Ltv/periscope/android/util/Size;->a()I

    move-result v1

    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->a()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Ltv/periscope/android/graphics/a;->b:I

    .line 57
    :goto_0
    const-string/jumbo v1, "Decimator"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Ltv/periscope/android/graphics/a;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Dest "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Source "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Rot: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Crop: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Ltv/periscope/android/graphics/a;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Ltv/periscope/android/graphics/a;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p1}, Ltv/periscope/android/util/Size;->a()I

    move-result v2

    .line 61
    invoke-virtual {p1}, Ltv/periscope/android/util/Size;->b()I

    move-result v1

    .line 62
    invoke-virtual {v0, p3}, Ltv/periscope/android/util/Size;->a(I)Ltv/periscope/android/util/Size;

    move-result-object v3

    invoke-virtual {v3}, Ltv/periscope/android/util/Size;->a()I

    move-result v3

    .line 63
    invoke-virtual {v0, p3}, Ltv/periscope/android/util/Size;->a(I)Ltv/periscope/android/util/Size;

    move-result-object v4

    invoke-virtual {v4}, Ltv/periscope/android/util/Size;->b()I

    move-result v4

    .line 65
    iput-object v0, p0, Ltv/periscope/android/graphics/a;->d:Ltv/periscope/android/util/Size;

    .line 66
    iput p3, p0, Ltv/periscope/android/graphics/a;->g:I

    .line 67
    iput-object p2, p0, Ltv/periscope/android/graphics/a;->f:Ltv/periscope/android/util/Size;

    .line 68
    iput-object p1, p0, Ltv/periscope/android/graphics/a;->e:Ltv/periscope/android/util/Size;

    move v0, v1

    move v1, v2

    .line 71
    :goto_1
    div-int/lit8 v2, v1, 0x2

    if-le v2, v3, :cond_0

    div-int/lit8 v2, v0, 0x2

    if-le v2, v4, :cond_0

    .line 72
    div-int/lit8 v1, v1, 0x2

    .line 73
    div-int/lit8 v0, v0, 0x2

    .line 74
    iget-object v2, p0, Ltv/periscope/android/graphics/a;->a:Ljava/util/List;

    new-instance v5, Ltv/periscope/android/graphics/f;

    const/16 v6, 0x1907

    const/16 v7, 0x1401

    invoke-direct {v5, v1, v0, v6, v7}, Ltv/periscope/android/graphics/f;-><init>(IIII)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 53
    :cond_2
    invoke-virtual {p2}, Ltv/periscope/android/util/Size;->a()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Ltv/periscope/android/util/Size;->a()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/Size;->a(FF)Ltv/periscope/android/util/Size;

    move-result-object v0

    .line 54
    invoke-virtual {p2}, Ltv/periscope/android/util/Size;->b()I

    move-result v1

    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->b()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Ltv/periscope/android/graphics/a;->c:I

    goto/16 :goto_0
.end method
