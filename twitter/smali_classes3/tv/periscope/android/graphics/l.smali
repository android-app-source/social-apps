.class public Ltv/periscope/android/graphics/l;
.super Ltv/periscope/android/graphics/o;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Ltv/periscope/android/graphics/o;-><init>(Landroid/content/Context;)V

    .line 9
    return-void
.end method

.method private b()I
    .locals 2

    .prologue
    .line 42
    iget v0, p0, Ltv/periscope/android/graphics/l;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 43
    iget v0, p0, Ltv/periscope/android/graphics/l;->a:I

    add-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    .line 44
    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    .line 48
    :goto_0
    return v0

    .line 46
    :cond_0
    iget v0, p0, Ltv/periscope/android/graphics/l;->a:I

    rsub-int/lit8 v0, v0, 0x5a

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0
.end method


# virtual methods
.method protected a(I)I
    .locals 2

    .prologue
    .line 17
    iget v0, p0, Ltv/periscope/android/graphics/l;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 18
    invoke-direct {p0}, Ltv/periscope/android/graphics/l;->b()I

    move-result v0

    .line 19
    iget v1, p0, Ltv/periscope/android/graphics/l;->b:I

    packed-switch v1, :pswitch_data_0

    .line 30
    invoke-direct {p0}, Ltv/periscope/android/graphics/l;->b()I

    move-result v0

    .line 37
    :goto_0
    return v0

    .line 21
    :pswitch_0
    rsub-int/lit8 v1, p1, 0x5a

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 25
    :pswitch_1
    rsub-int v1, p1, 0x10e

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0

    :cond_0
    move v0, p1

    .line 35
    goto :goto_0

    .line 19
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
