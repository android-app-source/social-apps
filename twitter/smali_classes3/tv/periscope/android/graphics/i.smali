.class public Ltv/periscope/android/graphics/i;
.super Ltv/periscope/android/graphics/j;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private c:Landroid/graphics/SurfaceTexture;

.field private volatile d:Z

.field private e:I

.field private final f:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ltv/periscope/android/graphics/j;-><init>(Ltv/periscope/android/util/Size;)V

    .line 20
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget v1, p0, Ltv/periscope/android/graphics/i;->a:I

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/graphics/i;->f:Z

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ltv/periscope/android/graphics/j;-><init>(Ltv/periscope/android/util/Size;)V

    .line 26
    iget v0, p0, Ltv/periscope/android/graphics/i;->a:I

    invoke-virtual {p1, v0}, Landroid/graphics/SurfaceTexture;->attachToGLContext(I)V

    .line 27
    iput-object p1, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/graphics/i;->f:Z

    .line 29
    return-void
.end method


# virtual methods
.method public a(II)I
    .locals 2

    .prologue
    const v1, 0x8d65

    .line 44
    iget v0, p0, Ltv/periscope/android/graphics/i;->a:I

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 45
    const/16 v0, 0x2801

    invoke-static {v1, v0, p1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 46
    const/16 v0, 0x2800

    invoke-static {v1, v0, p1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 47
    const/16 v0, 0x2802

    invoke-static {v1, v0, p2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 48
    const/16 v0, 0x2803

    invoke-static {v1, v0, p2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 49
    const/4 v0, 0x3

    return v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 32
    invoke-super {p0}, Ltv/periscope/android/graphics/j;->a()V

    .line 33
    iget-object v0, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->releaseTexImage()V

    .line 34
    iget-boolean v0, p0, Ltv/periscope/android/graphics/i;->f:Z

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 36
    iget-object v1, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    monitor-enter v1

    .line 37
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 38
    monitor-exit v1

    .line 40
    :cond_0
    return-void

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ltv/periscope/android/util/Size;I)V
    .locals 2

    .prologue
    .line 57
    iget-object v1, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    monitor-enter v1

    .line 58
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 59
    iput-object p1, p0, Ltv/periscope/android/graphics/i;->b:Ltv/periscope/android/util/Size;

    .line 60
    iput p2, p0, Ltv/periscope/android/graphics/i;->e:I

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/graphics/i;->d:Z

    .line 62
    iget-object v0, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 63
    monitor-exit v1

    .line 64
    return-void

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v0

    return-wide v0
.end method

.method public c()I
    .locals 2

    .prologue
    .line 67
    iget-object v1, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    monitor-enter v1

    .line 68
    :try_start_0
    iget v0, p0, Ltv/periscope/android/graphics/i;->e:I

    monitor-exit v1

    return v0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()Landroid/graphics/SurfaceTexture;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 77
    iget-object v1, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    monitor-enter v1

    .line 78
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 79
    monitor-exit v1

    .line 80
    return-void

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 83
    iget-object v2, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    monitor-enter v2

    .line 84
    :try_start_0
    iget-boolean v3, p0, Ltv/periscope/android/graphics/i;->d:Z

    if-eqz v3, :cond_0

    .line 85
    const/4 v1, 0x0

    iput-boolean v1, p0, Ltv/periscope/android/graphics/i;->d:Z

    .line 86
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :goto_0
    return v0

    .line 89
    :cond_0
    :try_start_1
    iget-object v3, p0, Ltv/periscope/android/graphics/i;->c:Landroid/graphics/SurfaceTexture;

    const-wide/16 v4, 0x64

    invoke-virtual {v3, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    :goto_1
    :try_start_2
    iget-boolean v3, p0, Ltv/periscope/android/graphics/i;->d:Z

    if-eqz v3, :cond_1

    .line 93
    const/4 v1, 0x0

    iput-boolean v1, p0, Ltv/periscope/android/graphics/i;->d:Z

    .line 94
    monitor-exit v2

    goto :goto_0

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 97
    goto :goto_0

    .line 90
    :catch_0
    move-exception v3

    goto :goto_1
.end method
