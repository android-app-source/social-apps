.class public Ltv/periscope/android/graphics/j;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected a:I

.field protected b:Ltv/periscope/android/util/Size;


# direct methods
.method public constructor <init>(Ltv/periscope/android/util/Size;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Ltv/periscope/android/graphics/j;->b:Ltv/periscope/android/util/Size;

    .line 13
    new-array v0, v2, [I

    .line 14
    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 15
    aget v0, v0, v1

    iput v0, p0, Ltv/periscope/android/graphics/j;->a:I

    .line 16
    return-void
.end method


# virtual methods
.method public a(II)I
    .locals 2

    .prologue
    const/16 v1, 0xde1

    .line 27
    iget v0, p0, Ltv/periscope/android/graphics/j;->a:I

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 28
    const/16 v0, 0x2801

    invoke-static {v1, v0, p1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 29
    const/16 v0, 0x2800

    invoke-static {v1, v0, p1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 30
    const/16 v0, 0x2802

    invoke-static {v1, v0, p2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 31
    const/16 v0, 0x2803

    invoke-static {v1, v0, p2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 32
    const/4 v0, 0x1

    return v0
.end method

.method a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    new-array v0, v3, [I

    iget v1, p0, Ltv/periscope/android/graphics/j;->a:I

    aput v1, v0, v2

    .line 37
    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 38
    return-void
.end method

.method public g()Ltv/periscope/android/util/Size;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Ltv/periscope/android/graphics/j;->b:Ltv/periscope/android/util/Size;

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Ltv/periscope/android/graphics/j;->a:I

    return v0
.end method
