.class public Ltv/periscope/android/graphics/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/graphics/m$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/hardware/SensorManager;

.field private final b:Landroid/hardware/Sensor;

.field private final c:Landroid/hardware/Sensor;

.field private d:Ltv/periscope/android/graphics/m$a;

.field private e:Z

.field private f:F

.field private g:I

.field private h:Lczt;

.field private i:I

.field private j:J

.field private k:J

.field private l:F

.field private m:Lczw;

.field private n:Lczt;

.field private o:Lczv;

.field private p:Lczv;

.field private q:Lczv;

.field private r:Lczt;

.field private s:Lczt;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lczt;

    invoke-direct {v0}, Lczt;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->h:Lczt;

    .line 54
    new-instance v0, Lczw;

    invoke-direct {v0}, Lczw;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->m:Lczw;

    .line 55
    new-instance v0, Lczt;

    invoke-direct {v0}, Lczt;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->n:Lczt;

    .line 56
    new-instance v0, Lczv;

    invoke-direct {v0}, Lczv;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    .line 57
    new-instance v0, Lczv;

    invoke-direct {v0}, Lczv;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->p:Lczv;

    .line 58
    new-instance v0, Lczv;

    invoke-direct {v0}, Lczv;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->q:Lczv;

    .line 59
    new-instance v0, Lczt;

    invoke-direct {v0}, Lczt;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->r:Lczt;

    .line 60
    new-instance v0, Lczt;

    invoke-direct {v0}, Lczt;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->s:Lczt;

    .line 63
    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->a:Landroid/hardware/SensorManager;

    .line 64
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->a:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->b:Landroid/hardware/Sensor;

    .line 65
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->a:Landroid/hardware/SensorManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->c:Landroid/hardware/Sensor;

    .line 66
    invoke-direct {p0}, Ltv/periscope/android/graphics/m;->c()V

    .line 67
    return-void
.end method

.method private a(F)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 92
    mul-float v0, p1, p1

    .line 93
    mul-float v1, v0, p1

    .line 94
    const v2, 0x33d6bf95    # 1.0E-7f

    mul-float/2addr v2, p1

    const v3, 0x2abba5eb

    mul-float/2addr v1, v3

    add-float/2addr v1, v2

    .line 95
    const v2, 0x2b8cbccc    # 1.0E-12f

    mul-float/2addr v2, p1

    .line 96
    const v3, 0x2b0cbccc    # 5.0E-13f

    mul-float/2addr v0, v3

    .line 98
    iget-object v3, p0, Ltv/periscope/android/graphics/m;->p:Lczv;

    iget-object v3, v3, Lczv;->a:[[Lczu;

    aget-object v3, v3, v5

    new-instance v4, Lczu;

    invoke-direct {v4, v1}, Lczu;-><init>(F)V

    aput-object v4, v3, v5

    .line 99
    iget-object v1, p0, Ltv/periscope/android/graphics/m;->p:Lczv;

    iget-object v1, v1, Lczv;->a:[[Lczu;

    aget-object v1, v1, v6

    new-instance v3, Lczu;

    neg-float v4, v0

    invoke-direct {v3, v4}, Lczu;-><init>(F)V

    aput-object v3, v1, v5

    .line 100
    iget-object v1, p0, Ltv/periscope/android/graphics/m;->p:Lczv;

    iget-object v1, v1, Lczv;->a:[[Lczu;

    aget-object v1, v1, v5

    new-instance v3, Lczu;

    neg-float v0, v0

    invoke-direct {v3, v0}, Lczu;-><init>(F)V

    aput-object v3, v1, v6

    .line 101
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->p:Lczv;

    iget-object v0, v0, Lczv;->a:[[Lczu;

    aget-object v0, v0, v6

    new-instance v1, Lczu;

    invoke-direct {v1, v2}, Lczu;-><init>(F)V

    aput-object v1, v0, v6

    .line 102
    return-void
.end method

.method private a(Lczt;F)V
    .locals 18

    .prologue
    .line 151
    move-object/from16 v0, p0

    iget v2, v0, Ltv/periscope/android/graphics/m;->l:F

    const v3, 0x3f733333    # 0.95f

    mul-float/2addr v2, v3

    const v3, 0x3d4ccccd    # 0.05f

    mul-float v3, v3, p2

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Ltv/periscope/android/graphics/m;->l:F

    .line 152
    move-object/from16 v0, p0

    iget v2, v0, Ltv/periscope/android/graphics/m;->l:F

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ltv/periscope/android/graphics/m;->a(F)V

    .line 153
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/graphics/m;->m:Lczw;

    .line 154
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/graphics/m;->n:Lczt;

    .line 155
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lczt;->b(Lczt;)Lczt;

    move-result-object v2

    .line 156
    invoke-virtual {v2}, Lczt;->b()F

    move-result v4

    const v5, 0x38722a55

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    .line 157
    iget v2, v2, Lczt;->a:F

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-lez v2, :cond_2

    new-instance v2, Lczt;

    const v4, 0x38722a55

    invoke-direct {v2, v4}, Lczt;-><init>(F)V

    .line 159
    :cond_0
    :goto_0
    new-instance v4, Lczu;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v4, v5}, Lczu;-><init>(F)V

    .line 160
    new-instance v5, Lczu;

    move/from16 v0, p2

    invoke-direct {v5, v0}, Lczu;-><init>(F)V

    .line 161
    const/4 v6, 0x0

    invoke-static {v2, v6}, Lczu;->a(Lczt;F)Lczu;

    move-result-object v6

    .line 162
    invoke-virtual {v6, v6}, Lczu;->a(Lczu;)Lczu;

    move-result-object v7

    .line 163
    invoke-virtual {v2}, Lczt;->b()F

    move-result v8

    mul-float v8, v8, p2

    .line 164
    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v9, v8

    .line 165
    const/high16 v10, 0x3f800000    # 1.0f

    invoke-virtual {v2}, Lczt;->b()F

    move-result v11

    div-float/2addr v10, v11

    .line 166
    const/high16 v11, 0x3f800000    # 1.0f

    float-to-double v12, v8

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    double-to-float v12, v12

    sub-float/2addr v11, v12

    mul-float v12, v10, v10

    mul-float/2addr v11, v12

    .line 167
    float-to-double v12, v8

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    double-to-float v12, v12

    .line 168
    float-to-double v14, v9

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    double-to-float v13, v14

    .line 169
    float-to-double v14, v9

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    double-to-float v9, v14

    mul-float/2addr v9, v10

    invoke-virtual {v2, v9}, Lczt;->a(F)Lczt;

    move-result-object v2

    .line 170
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v2, v9}, Lczt;->a(F)Lczt;

    move-result-object v9

    invoke-static {v9, v13}, Lczu;->a(Lczt;F)Lczu;

    move-result-object v9

    .line 171
    const/16 v14, 0x10

    new-array v14, v14, [F

    const/4 v15, 0x0

    iget-object v0, v9, Lczu;->a:[F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aget v16, v16, v17

    aput v16, v14, v15

    const/4 v15, 0x1

    iget-object v0, v9, Lczu;->a:[F

    move-object/from16 v16, v0

    const/16 v17, 0x1

    aget v16, v16, v17

    aput v16, v14, v15

    const/4 v15, 0x2

    iget-object v0, v9, Lczu;->a:[F

    move-object/from16 v16, v0

    const/16 v17, 0x2

    aget v16, v16, v17

    aput v16, v14, v15

    const/4 v15, 0x3

    iget v0, v2, Lczt;->a:F

    move/from16 v16, v0

    move/from16 v0, v16

    neg-float v0, v0

    move/from16 v16, v0

    aput v16, v14, v15

    const/4 v15, 0x4

    iget-object v0, v9, Lczu;->a:[F

    move-object/from16 v16, v0

    const/16 v17, 0x3

    aget v16, v16, v17

    aput v16, v14, v15

    const/4 v15, 0x5

    iget-object v0, v9, Lczu;->a:[F

    move-object/from16 v16, v0

    const/16 v17, 0x4

    aget v16, v16, v17

    aput v16, v14, v15

    const/4 v15, 0x6

    iget-object v0, v9, Lczu;->a:[F

    move-object/from16 v16, v0

    const/16 v17, 0x5

    aget v16, v16, v17

    aput v16, v14, v15

    const/4 v15, 0x7

    iget v0, v2, Lczt;->b:F

    move/from16 v16, v0

    move/from16 v0, v16

    neg-float v0, v0

    move/from16 v16, v0

    aput v16, v14, v15

    const/16 v15, 0x8

    iget-object v0, v9, Lczu;->a:[F

    move-object/from16 v16, v0

    const/16 v17, 0x6

    aget v16, v16, v17

    aput v16, v14, v15

    const/16 v15, 0x9

    iget-object v0, v9, Lczu;->a:[F

    move-object/from16 v16, v0

    const/16 v17, 0x7

    aget v16, v16, v17

    aput v16, v14, v15

    const/16 v15, 0xa

    iget-object v9, v9, Lczu;->a:[F

    const/16 v16, 0x8

    aget v9, v9, v16

    aput v9, v14, v15

    const/16 v9, 0xb

    iget v15, v2, Lczt;->c:F

    neg-float v15, v15

    aput v15, v14, v9

    const/16 v9, 0xc

    iget v15, v2, Lczt;->a:F

    aput v15, v14, v9

    const/16 v9, 0xd

    iget v15, v2, Lczt;->b:F

    aput v15, v14, v9

    const/16 v9, 0xe

    iget v2, v2, Lczt;->c:F

    aput v2, v14, v9

    const/16 v2, 0xf

    aput v13, v14, v2

    .line 176
    new-instance v2, Lczx;

    invoke-direct {v2, v14}, Lczx;-><init>([F)V

    .line 177
    move-object/from16 v0, p0

    iget-object v9, v0, Ltv/periscope/android/graphics/m;->q:Lczv;

    iget-object v9, v9, Lczv;->a:[[Lczu;

    const/4 v13, 0x0

    aget-object v9, v9, v13

    const/4 v13, 0x0

    mul-float v14, v12, v10

    invoke-virtual {v6, v14}, Lczu;->a(F)Lczu;

    move-result-object v14

    invoke-virtual {v4, v14}, Lczu;->c(Lczu;)Lczu;

    move-result-object v4

    invoke-virtual {v7, v11}, Lczu;->a(F)Lczu;

    move-result-object v14

    invoke-virtual {v4, v14}, Lczu;->b(Lczu;)Lczu;

    move-result-object v4

    aput-object v4, v9, v13

    .line 178
    invoke-virtual {v6, v11}, Lczu;->a(F)Lczu;

    move-result-object v4

    .line 179
    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/graphics/m;->q:Lczv;

    iget-object v6, v6, Lczv;->a:[[Lczu;

    const/4 v9, 0x1

    aget-object v6, v6, v9

    const/4 v9, 0x0

    invoke-virtual {v4, v5}, Lczu;->c(Lczu;)Lczu;

    move-result-object v4

    mul-float v5, v10, v10

    mul-float/2addr v5, v10

    invoke-virtual {v7, v5}, Lczu;->a(F)Lczu;

    move-result-object v5

    sub-float v7, v8, v12

    invoke-virtual {v5, v7}, Lczu;->a(F)Lczu;

    move-result-object v5

    invoke-virtual {v4, v5}, Lczu;->c(Lczu;)Lczu;

    move-result-object v4

    aput-object v4, v6, v9

    .line 180
    invoke-virtual {v2, v3}, Lczx;->a(Lczw;)Lczw;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Ltv/periscope/android/graphics/m;->m:Lczw;

    .line 181
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/graphics/m;->m:Lczw;

    iget v2, v2, Lczw;->e:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/graphics/m;->m:Lczw;

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v2, v3}, Lczw;->a(F)Lczw;

    .line 184
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/graphics/m;->q:Lczv;

    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/graphics/m;->o:Lczv;

    invoke-virtual {v2, v3}, Lczv;->b(Lczv;)Lczv;

    move-result-object v2

    .line 185
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/graphics/m;->q:Lczv;

    invoke-virtual {v3}, Lczv;->a()Lczv;

    move-result-object v3

    .line 186
    invoke-virtual {v2, v3}, Lczv;->b(Lczv;)Lczv;

    move-result-object v2

    .line 187
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/graphics/m;->p:Lczv;

    invoke-virtual {v2, v3}, Lczv;->a(Lczv;)Lczv;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Ltv/periscope/android/graphics/m;->o:Lczv;

    .line 188
    invoke-direct/range {p0 .. p0}, Ltv/periscope/android/graphics/m;->e()V

    .line 189
    return-void

    .line 157
    :cond_2
    new-instance v2, Lczt;

    const v4, -0x478dd5ab

    invoke-direct {v2, v4}, Lczt;-><init>(F)V

    goto/16 :goto_0
.end method

.method private a(Lczt;Lczt;F)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 192
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->m:Lczw;

    .line 193
    invoke-static {v0}, Lczu;->a(Lczw;)Lczu;

    move-result-object v1

    .line 194
    invoke-virtual {v1, p2}, Lczu;->a(Lczt;)Lczt;

    move-result-object v1

    .line 195
    invoke-static {v1, v10}, Lczu;->a(Lczt;F)Lczu;

    move-result-object v2

    .line 196
    new-instance v3, Lczu;

    mul-float v4, p3, p3

    invoke-direct {v3, v4}, Lczu;-><init>(F)V

    .line 197
    iget-object v4, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v4, v4, Lczv;->a:[[Lczu;

    aget-object v4, v4, v8

    aget-object v4, v4, v8

    invoke-virtual {v2, v4}, Lczu;->d(Lczu;)Lczu;

    move-result-object v4

    invoke-virtual {v4, v3}, Lczu;->b(Lczu;)Lczu;

    move-result-object v3

    .line 198
    invoke-virtual {v3}, Lczu;->c()Lczu;

    move-result-object v3

    .line 199
    invoke-virtual {v2}, Lczu;->a()Lczu;

    move-result-object v4

    invoke-virtual {v4, v3}, Lczu;->a(Lczu;)Lczu;

    move-result-object v3

    .line 200
    iget-object v4, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v4, v4, Lczv;->a:[[Lczu;

    aget-object v4, v4, v8

    aget-object v4, v4, v8

    invoke-virtual {v4, v3}, Lczu;->a(Lczu;)Lczu;

    move-result-object v4

    .line 201
    iget-object v5, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v5, v5, Lczv;->a:[[Lczu;

    aget-object v5, v5, v9

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lczu;->a()Lczu;

    move-result-object v5

    invoke-virtual {v5, v3}, Lczu;->a(Lczu;)Lczu;

    move-result-object v3

    .line 202
    invoke-virtual {v4, v2}, Lczu;->a(Lczu;)Lczu;

    move-result-object v5

    .line 203
    invoke-virtual {v3, v2}, Lczu;->a(Lczu;)Lczu;

    move-result-object v2

    .line 204
    iget-object v3, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v3, v3, Lczv;->a:[[Lczu;

    aget-object v3, v3, v8

    iget-object v6, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v6, v6, Lczv;->a:[[Lczu;

    aget-object v6, v6, v8

    aget-object v6, v6, v8

    iget-object v7, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v7, v7, Lczv;->a:[[Lczu;

    aget-object v7, v7, v8

    aget-object v7, v7, v8

    invoke-virtual {v5, v7}, Lczu;->a(Lczu;)Lczu;

    move-result-object v7

    invoke-virtual {v6, v7}, Lczu;->c(Lczu;)Lczu;

    move-result-object v6

    aput-object v6, v3, v8

    .line 205
    iget-object v3, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v3, v3, Lczv;->a:[[Lczu;

    aget-object v3, v3, v9

    iget-object v6, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v6, v6, Lczv;->a:[[Lczu;

    aget-object v6, v6, v9

    aget-object v6, v6, v9

    iget-object v7, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v7, v7, Lczv;->a:[[Lczu;

    aget-object v7, v7, v9

    aget-object v7, v7, v8

    invoke-virtual {v2, v7}, Lczu;->a(Lczu;)Lczu;

    move-result-object v2

    invoke-virtual {v6, v2}, Lczu;->c(Lczu;)Lczu;

    move-result-object v2

    aput-object v2, v3, v9

    .line 206
    iget-object v2, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v2, v2, Lczv;->a:[[Lczu;

    aget-object v2, v2, v9

    iget-object v3, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v3, v3, Lczv;->a:[[Lczu;

    aget-object v3, v3, v9

    aget-object v3, v3, v8

    iget-object v6, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v6, v6, Lczv;->a:[[Lczu;

    aget-object v6, v6, v9

    aget-object v6, v6, v8

    invoke-virtual {v5, v6}, Lczu;->a(Lczu;)Lczu;

    move-result-object v5

    invoke-virtual {v3, v5}, Lczu;->c(Lczu;)Lczu;

    move-result-object v3

    aput-object v3, v2, v8

    .line 207
    iget-object v2, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v2, v2, Lczv;->a:[[Lczu;

    aget-object v2, v2, v8

    iget-object v3, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v3, v3, Lczv;->a:[[Lczu;

    aget-object v3, v3, v9

    aget-object v3, v3, v8

    invoke-virtual {v3}, Lczu;->a()Lczu;

    move-result-object v3

    aput-object v3, v2, v9

    .line 208
    invoke-virtual {p1, v1}, Lczt;->b(Lczt;)Lczt;

    move-result-object v1

    .line 209
    invoke-virtual {v4, v1}, Lczu;->a(Lczt;)Lczt;

    move-result-object v1

    .line 210
    const/16 v2, 0x10

    new-array v2, v2, [F

    iget v3, v0, Lczw;->e:F

    aput v3, v2, v8

    iget v3, v0, Lczw;->d:F

    aput v3, v2, v9

    const/4 v3, 0x2

    iget v4, v0, Lczw;->c:F

    neg-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x3

    iget v4, v0, Lczw;->b:F

    neg-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x4

    iget v4, v0, Lczw;->d:F

    neg-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x5

    iget v4, v0, Lczw;->e:F

    aput v4, v2, v3

    const/4 v3, 0x6

    iget v4, v0, Lczw;->b:F

    aput v4, v2, v3

    const/4 v3, 0x7

    iget v4, v0, Lczw;->c:F

    neg-float v4, v4

    aput v4, v2, v3

    const/16 v3, 0x8

    iget v4, v0, Lczw;->c:F

    aput v4, v2, v3

    const/16 v3, 0x9

    iget v4, v0, Lczw;->b:F

    neg-float v4, v4

    aput v4, v2, v3

    const/16 v3, 0xa

    iget v4, v0, Lczw;->e:F

    aput v4, v2, v3

    const/16 v3, 0xb

    iget v4, v0, Lczw;->d:F

    neg-float v4, v4

    aput v4, v2, v3

    const/16 v3, 0xc

    aput v10, v2, v3

    const/16 v3, 0xd

    aput v10, v2, v3

    const/16 v3, 0xe

    aput v10, v2, v3

    const/16 v3, 0xf

    aput v10, v2, v3

    .line 215
    new-instance v3, Lczx;

    invoke-direct {v3, v2}, Lczx;-><init>([F)V

    .line 216
    new-instance v2, Lczw;

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual {v1, v4}, Lczt;->a(F)Lczt;

    move-result-object v1

    invoke-direct {v2, v1, v10}, Lczw;-><init>(Lczt;F)V

    invoke-virtual {v3, v2}, Lczx;->a(Lczw;)Lczw;

    move-result-object v1

    .line 217
    invoke-virtual {v0, v1}, Lczw;->a(Lczw;)Lczw;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->m:Lczw;

    .line 218
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->m:Lczw;

    invoke-virtual {v0}, Lczw;->d()Lczw;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->m:Lczw;

    .line 219
    invoke-direct {p0}, Ltv/periscope/android/graphics/m;->e()V

    .line 220
    return-void
.end method

.method private a(Lczw;F)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    iput-object p1, p0, Ltv/periscope/android/graphics/m;->m:Lczw;

    .line 106
    new-instance v0, Lczt;

    invoke-direct {v0, v1, v1, v1}, Lczt;-><init>(FFF)V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->n:Lczt;

    .line 107
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    invoke-virtual {v0, v1}, Lczv;->a(F)V

    .line 108
    iput p2, p0, Ltv/periscope/android/graphics/m;->l:F

    .line 109
    invoke-direct {p0, p2}, Ltv/periscope/android/graphics/m;->a(F)V

    .line 110
    return-void
.end method

.method private b(Lczt;F)V
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Ltv/periscope/android/graphics/m;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltv/periscope/android/graphics/m;->g:I

    .line 224
    iget v0, p0, Ltv/periscope/android/graphics/m;->f:F

    add-float/2addr v0, p2

    iput v0, p0, Ltv/periscope/android/graphics/m;->f:F

    .line 225
    iget-boolean v0, p0, Ltv/periscope/android/graphics/m;->e:Z

    if-eqz v0, :cond_0

    .line 226
    invoke-direct {p0, p1, p2}, Ltv/periscope/android/graphics/m;->a(Lczt;F)V

    .line 228
    :cond_0
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 83
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->q:Lczv;

    iget-object v0, v0, Lczv;->a:[[Lczu;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    new-instance v1, Lczu;

    invoke-direct {v1, v2}, Lczu;-><init>(F)V

    aput-object v1, v0, v4

    .line 84
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->q:Lczv;

    iget-object v0, v0, Lczv;->a:[[Lczu;

    aget-object v0, v0, v4

    new-instance v1, Lczu;

    invoke-direct {v1, v3}, Lczu;-><init>(F)V

    aput-object v1, v0, v4

    .line 85
    new-instance v0, Lczt;

    invoke-direct {v0, v2, v2, v3}, Lczt;-><init>(FFF)V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->r:Lczt;

    .line 86
    new-instance v0, Lczt;

    invoke-direct {v0, v2, v3, v2}, Lczt;-><init>(FFF)V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->s:Lczt;

    .line 87
    new-instance v0, Lczw;

    invoke-direct {v0}, Lczw;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->m:Lczw;

    .line 88
    new-instance v0, Lczt;

    invoke-direct {v0, v2}, Lczt;-><init>(F)V

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->n:Lczt;

    .line 89
    return-void
.end method

.method private c(Lczt;F)V
    .locals 6

    .prologue
    .line 231
    iget-boolean v0, p0, Ltv/periscope/android/graphics/m;->e:Z

    if-nez v0, :cond_2

    .line 232
    iget v0, p0, Ltv/periscope/android/graphics/m;->i:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 233
    iget v0, p0, Ltv/periscope/android/graphics/m;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltv/periscope/android/graphics/m;->i:I

    .line 234
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->h:Lczt;

    invoke-virtual {v0, p1}, Lczt;->a(Lczt;)Lczt;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/m;->h:Lczt;

    .line 236
    :cond_0
    invoke-direct {p0}, Ltv/periscope/android/graphics/m;->d()V

    .line 250
    :cond_1
    :goto_0
    return-void

    .line 238
    :cond_2
    invoke-virtual {p1}, Lczt;->b()F

    move-result v0

    .line 239
    const v1, 0x3f7b22d2    # 0.98100007f

    cmpg-float v1, v0, v1

    if-ltz v1, :cond_1

    .line 242
    invoke-direct {p0}, Ltv/periscope/android/graphics/m;->f()Lczu;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/graphics/m;->s:Lczt;

    invoke-virtual {v1, v2}, Lczu;->a(Lczt;)Lczt;

    move-result-object v1

    .line 243
    iget-object v2, p0, Ltv/periscope/android/graphics/m;->s:Lczt;

    const v3, 0x3dcccccd    # 0.1f

    invoke-direct {p0, v1, v2, v3}, Ltv/periscope/android/graphics/m;->a(Lczt;Lczt;F)V

    .line 244
    const/high16 v1, 0x3f800000    # 1.0f

    div-float/2addr v1, v0

    .line 245
    invoke-virtual {p1, v1}, Lczt;->a(F)Lczt;

    move-result-object v2

    .line 246
    const v3, 0x411cf5c3    # 9.81f

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v0, v4

    .line 247
    const v3, 0x3c75c28f    # 0.015f

    mul-float/2addr v1, v3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    double-to-float v0, v4

    mul-float/2addr v0, v1

    .line 248
    iget-object v1, p0, Ltv/periscope/android/graphics/m;->r:Lczt;

    invoke-direct {p0, v2, v1, v0}, Ltv/periscope/android/graphics/m;->a(Lczt;Lczt;F)V

    goto :goto_0
.end method

.method private d()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 113
    iget v0, p0, Ltv/periscope/android/graphics/m;->i:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_1

    .line 114
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->h:Lczt;

    iget v1, p0, Ltv/periscope/android/graphics/m;->i:I

    int-to-float v1, v1

    div-float v1, v5, v1

    invoke-virtual {v0, v1}, Lczt;->a(F)Lczt;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Lczt;->c()Lczt;

    move-result-object v2

    .line 116
    new-instance v1, Lczt;

    invoke-direct {v1, v4, v4, v5}, Lczt;-><init>(FFF)V

    .line 117
    invoke-virtual {v2, v1}, Lczt;->d(Lczt;)Lczt;

    move-result-object v0

    invoke-virtual {v0}, Lczt;->c()Lczt;

    move-result-object v0

    .line 118
    invoke-virtual {v2, v1}, Lczt;->c(Lczt;)F

    move-result v1

    const v3, 0x3f666666    # 0.9f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    .line 119
    new-instance v0, Lczt;

    invoke-direct {v0, v5, v4, v4}, Lczt;-><init>(FFF)V

    .line 120
    invoke-virtual {v0, v2}, Lczt;->d(Lczt;)Lczt;

    move-result-object v0

    invoke-virtual {v0}, Lczt;->c()Lczt;

    move-result-object v1

    .line 121
    invoke-virtual {v2, v1}, Lczt;->d(Lczt;)Lczt;

    move-result-object v0

    .line 125
    :goto_0
    invoke-virtual {v1}, Lczt;->c()Lczt;

    move-result-object v1

    invoke-virtual {v0}, Lczt;->c()Lczt;

    move-result-object v0

    invoke-virtual {v2}, Lczt;->c()Lczt;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lczw;->a(Lczt;Lczt;Lczt;)Lczw;

    move-result-object v0

    .line 126
    iget v1, p0, Ltv/periscope/android/graphics/m;->g:I

    const/16 v2, 0x8

    if-le v1, v2, :cond_0

    .line 127
    iget v1, p0, Ltv/periscope/android/graphics/m;->f:F

    iget v2, p0, Ltv/periscope/android/graphics/m;->g:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-direct {p0, v0, v1}, Ltv/periscope/android/graphics/m;->a(Lczw;F)V

    .line 128
    const/4 v1, 0x1

    iput-boolean v1, p0, Ltv/periscope/android/graphics/m;->e:Z

    .line 130
    :cond_0
    iget-object v1, p0, Ltv/periscope/android/graphics/m;->d:Ltv/periscope/android/graphics/m$a;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Ltv/periscope/android/graphics/m;->e:Z

    if-nez v1, :cond_1

    .line 131
    iget-object v1, p0, Ltv/periscope/android/graphics/m;->d:Ltv/periscope/android/graphics/m$a;

    new-instance v2, Lczw;

    iget v3, v0, Lczw;->b:F

    iget v4, v0, Lczw;->c:F

    iget v5, v0, Lczw;->d:F

    iget v0, v0, Lczw;->e:F

    invoke-direct {v2, v3, v4, v5, v0}, Lczw;-><init>(FFFF)V

    invoke-interface {v1, v2}, Ltv/periscope/android/graphics/m$a;->a(Lczw;)V

    .line 134
    :cond_1
    return-void

    .line 123
    :cond_2
    invoke-virtual {v0, v2}, Lczt;->d(Lczt;)Lczt;

    move-result-object v1

    goto :goto_0
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const v1, 0x2edbe6ff    # 1.0E-10f

    .line 137
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v0, v0, Lczv;->a:[[Lczu;

    aget-object v0, v0, v2

    aget-object v0, v0, v2

    invoke-virtual {v0, v1}, Lczu;->b(F)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    iget-object v0, v0, Lczv;->a:[[Lczu;

    aget-object v0, v0, v3

    aget-object v0, v0, v3

    invoke-virtual {v0, v1}, Lczu;->b(F)Z

    move-result v0

    if-nez v0, :cond_1

    .line 138
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->o:Lczv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lczv;->a(F)V

    .line 140
    :cond_1
    return-void
.end method

.method private f()Lczu;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->m:Lczw;

    invoke-static {v0}, Lczu;->a(Lczw;)Lczu;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 74
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->a:Landroid/hardware/SensorManager;

    iget-object v1, p0, Ltv/periscope/android/graphics/m;->b:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 75
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->a:Landroid/hardware/SensorManager;

    iget-object v1, p0, Ltv/periscope/android/graphics/m;->c:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 76
    return-void
.end method

.method public a(Ltv/periscope/android/graphics/m$a;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Ltv/periscope/android/graphics/m;->d:Ltv/periscope/android/graphics/m$a;

    .line 71
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->a:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 80
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 255
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v9, 0x0

    const-wide v10, 0x41cdcd6500000000L    # 1.0E9

    const/4 v8, 0x1

    const-wide/16 v6, 0x0

    .line 259
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 260
    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v2, p0, Ltv/periscope/android/graphics/m;->j:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    iget-wide v0, p0, Ltv/periscope/android/graphics/m;->j:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 261
    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v2, p0, Ltv/periscope/android/graphics/m;->j:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    div-double/2addr v0, v10

    .line 262
    new-instance v2, Lczt;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v9

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v8

    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v5, v5, v12

    invoke-direct {v2, v3, v4, v5}, Lczt;-><init>(FFF)V

    .line 263
    double-to-float v0, v0

    invoke-direct {p0, v2, v0}, Ltv/periscope/android/graphics/m;->b(Lczt;F)V

    .line 265
    :cond_0
    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v0, p0, Ltv/periscope/android/graphics/m;->j:J

    .line 267
    :cond_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    if-ne v0, v8, :cond_3

    .line 268
    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v2, p0, Ltv/periscope/android/graphics/m;->k:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_2

    iget-wide v0, p0, Ltv/periscope/android/graphics/m;->k:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    .line 269
    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v2, p0, Ltv/periscope/android/graphics/m;->k:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    div-double/2addr v0, v10

    .line 270
    new-instance v2, Lczt;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v9

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v8

    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v5, v5, v12

    invoke-direct {v2, v3, v4, v5}, Lczt;-><init>(FFF)V

    .line 271
    double-to-float v0, v0

    invoke-direct {p0, v2, v0}, Ltv/periscope/android/graphics/m;->c(Lczt;F)V

    .line 273
    :cond_2
    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v0, p0, Ltv/periscope/android/graphics/m;->k:J

    .line 275
    :cond_3
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->d:Ltv/periscope/android/graphics/m$a;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Ltv/periscope/android/graphics/m;->e:Z

    if-eqz v0, :cond_4

    .line 276
    iget-object v0, p0, Ltv/periscope/android/graphics/m;->d:Ltv/periscope/android/graphics/m$a;

    new-instance v1, Lczw;

    iget-object v2, p0, Ltv/periscope/android/graphics/m;->m:Lczw;

    iget v2, v2, Lczw;->b:F

    iget-object v3, p0, Ltv/periscope/android/graphics/m;->m:Lczw;

    iget v3, v3, Lczw;->c:F

    iget-object v4, p0, Ltv/periscope/android/graphics/m;->m:Lczw;

    iget v4, v4, Lczw;->d:F

    iget-object v5, p0, Ltv/periscope/android/graphics/m;->m:Lczw;

    iget v5, v5, Lczw;->e:F

    invoke-direct {v1, v2, v3, v4, v5}, Lczw;-><init>(FFFF)V

    invoke-interface {v0, v1}, Ltv/periscope/android/graphics/m$a;->a(Lczw;)V

    .line 278
    :cond_4
    return-void
.end method
