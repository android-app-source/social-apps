.class Ltv/periscope/android/graphics/g$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/graphics/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/graphics/g;

.field private volatile b:Z

.field private volatile c:Z

.field private final d:Ljava/lang/Object;

.field private e:Ljava/lang/Thread;


# direct methods
.method private constructor <init>(Ltv/periscope/android/graphics/g;)V
    .locals 1

    .prologue
    .line 32
    iput-object p1, p0, Ltv/periscope/android/graphics/g$a;->a:Ltv/periscope/android/graphics/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/g$a;->d:Ljava/lang/Object;

    return-void
.end method

.method synthetic constructor <init>(Ltv/periscope/android/graphics/g;Ltv/periscope/android/graphics/g$1;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Ltv/periscope/android/graphics/g$a;-><init>(Ltv/periscope/android/graphics/g;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Ltv/periscope/android/graphics/g$a$1;

    invoke-direct {v1, p0}, Ltv/periscope/android/graphics/g$a$1;-><init>(Ltv/periscope/android/graphics/g$a;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Ltv/periscope/android/graphics/g$a;->e:Ljava/lang/Thread;

    .line 45
    iget-object v0, p0, Ltv/periscope/android/graphics/g$a;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 46
    iget-object v1, p0, Ltv/periscope/android/graphics/g$a;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 47
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Ltv/periscope/android/graphics/g$a;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ltv/periscope/android/graphics/g$a;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 49
    :try_start_1
    iget-object v0, p0, Ltv/periscope/android/graphics/g$a;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 50
    :catch_0
    move-exception v0

    goto :goto_0

    .line 53
    :cond_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 54
    iget-object v0, p0, Ltv/periscope/android/graphics/g$a;->e:Ljava/lang/Thread;

    const-string/jumbo v1, "GLRenderThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 55
    return-void

    .line 53
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/graphics/g$a;->b:Z

    .line 60
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/graphics/g$a;->e:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Ltv/periscope/android/graphics/g$a;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/graphics/g$a;->e:Ljava/lang/Thread;

    .line 66
    return-void

    .line 63
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method c()V
    .locals 2

    .prologue
    .line 69
    iget-object v1, p0, Ltv/periscope/android/graphics/g$a;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 70
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Ltv/periscope/android/graphics/g$a;->c:Z

    .line 71
    iget-object v0, p0, Ltv/periscope/android/graphics/g$a;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 72
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    :goto_0
    iget-boolean v0, p0, Ltv/periscope/android/graphics/g$a;->b:Z

    if-nez v0, :cond_0

    .line 75
    iget-object v0, p0, Ltv/periscope/android/graphics/g$a;->a:Ltv/periscope/android/graphics/g;

    invoke-static {v0}, Ltv/periscope/android/graphics/g;->b(Ltv/periscope/android/graphics/g;)Ltv/periscope/android/graphics/b;

    move-result-object v0

    new-instance v1, Ltv/periscope/android/graphics/g$a$2;

    invoke-direct {v1, p0}, Ltv/periscope/android/graphics/g$a$2;-><init>(Ltv/periscope/android/graphics/g$a;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/graphics/b;->a(Ltv/periscope/android/graphics/b$c;)Z

    goto :goto_0

    .line 72
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 87
    :cond_0
    return-void
.end method
