.class public Ltv/periscope/android/graphics/e;
.super Ltv/periscope/android/graphics/h;
.source "Twttr"


# static fields
.field private static d:[F


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/16 v0, 0x8

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Ltv/periscope/android/graphics/e;->d:[F

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ltv/periscope/android/graphics/h;-><init>()V

    .line 24
    sget-object v0, Ltv/periscope/android/graphics/e;->d:[F

    invoke-static {v0}, Ltv/periscope/android/graphics/e;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/graphics/e;->a(Ljava/nio/Buffer;)I

    move-result v0

    iput v0, p0, Ltv/periscope/android/graphics/e;->e:I

    .line 25
    return-void
.end method

.method private static a(Ljava/nio/Buffer;)I
    .locals 5

    .prologue
    const v4, 0x8892

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 60
    new-array v0, v1, [I

    .line 61
    invoke-static {v1, v0, v3}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 62
    aget v1, v0, v3

    invoke-static {v4, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 63
    invoke-virtual {p0}, Ljava/nio/Buffer;->capacity()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    const v2, 0x88e4

    invoke-static {v4, v1, p0, v2}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 64
    aget v0, v0, v3

    return v0
.end method

.method private static a([F)Ljava/nio/FloatBuffer;
    .locals 2

    .prologue
    .line 68
    array-length v0, p0

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 69
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 70
    invoke-virtual {v0, p0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 71
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 72
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-array v0, v3, [I

    iget v1, p0, Ltv/periscope/android/graphics/e;->e:I

    aput v1, v0, v2

    .line 29
    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    .line 30
    return-void
.end method

.method public a(IIIII)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 38
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    const v0, 0x8892

    iget v1, p0, Ltv/periscope/android/graphics/e;->e:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 43
    iget-object v0, p0, Ltv/periscope/android/graphics/e;->a:Ltv/periscope/android/graphics/c;

    const-string/jumbo v1, "Position"

    invoke-virtual {v0, v1}, Ltv/periscope/android/graphics/c;->a(Ljava/lang/String;)I

    move-result v0

    .line 44
    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 45
    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/16 v4, 0x8

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 47
    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 48
    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 49
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 50
    const/16 v0, 0x207

    invoke-static {v0}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    .line 52
    invoke-static {}, Ltv/periscope/android/graphics/k;->a()Ltv/periscope/android/util/Size;

    move-result-object v0

    .line 53
    iget-object v1, p0, Ltv/periscope/android/graphics/e;->a:Ltv/periscope/android/graphics/c;

    const-string/jumbo v2, "OffsetScale"

    invoke-virtual {v1, v2}, Ltv/periscope/android/graphics/c;->b(Ljava/lang/String;)I

    move-result v1

    int-to-float v2, p1

    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->a()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    .line 54
    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->b()I

    move-result v4

    div-int v4, p2, v4

    int-to-float v4, v4

    int-to-float v5, p3

    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->a()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    int-to-float v6, p4

    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->b()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v6, v0

    .line 53
    invoke-static {v1, v2, v4, v5, v0}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 55
    iget-object v0, p0, Ltv/periscope/android/graphics/e;->a:Ltv/periscope/android/graphics/c;

    const-string/jumbo v1, "Rotation"

    invoke-virtual {v0, v1}, Ltv/periscope/android/graphics/c;->b(Ljava/lang/String;)I

    move-result v0

    int-to-float v1, p5

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 56
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-static {v0, v3, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    goto :goto_0
.end method

.method public b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-static {}, Ltv/periscope/android/graphics/k;->a()Ltv/periscope/android/util/Size;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->a()I

    move-result v3

    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->b()I

    move-result v4

    move-object v0, p0

    move v2, v1

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Ltv/periscope/android/graphics/e;->a(IIIII)V

    .line 35
    return-void
.end method
