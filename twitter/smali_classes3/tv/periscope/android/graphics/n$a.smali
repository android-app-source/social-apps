.class Ltv/periscope/android/graphics/n$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/graphics/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/graphics/n$a$a;
    }
.end annotation


# instance fields
.field private final a:Ltv/periscope/android/graphics/n$a$a;

.field private b:Landroid/view/VelocityTracker;

.field private c:I

.field private d:F

.field private e:F

.field private f:Z

.field private g:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Ltv/periscope/android/graphics/n$a$a;)V
    .locals 1

    .prologue
    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    iput-object p2, p0, Ltv/periscope/android/graphics/n$a;->a:Ltv/periscope/android/graphics/n$a$a;

    .line 210
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    .line 211
    mul-int/2addr v0, v0

    iput v0, p0, Ltv/periscope/android/graphics/n$a;->c:I

    .line 212
    invoke-virtual {p0}, Ltv/periscope/android/graphics/n$a;->a()V

    .line 213
    return-void
.end method

.method private a(Landroid/view/MotionEvent;FF)V
    .locals 1

    .prologue
    .line 277
    iput p2, p0, Ltv/periscope/android/graphics/n$a;->d:F

    .line 278
    iput p3, p0, Ltv/periscope/android/graphics/n$a;->e:F

    .line 279
    iget-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 281
    iget-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 283
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/graphics/n$a;->a:Ltv/periscope/android/graphics/n$a$a;

    invoke-interface {v0}, Ltv/periscope/android/graphics/n$a$a;->a()V

    .line 284
    return-void
.end method

.method private b(Landroid/view/MotionEvent;FF)V
    .locals 4

    .prologue
    const v2, -0x42dc28f6    # -0.04f

    .line 287
    iget-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 290
    :cond_0
    iget v0, p0, Ltv/periscope/android/graphics/n$a;->d:F

    sub-float v0, p2, v0

    mul-float/2addr v0, v2

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 291
    iget v1, p0, Ltv/periscope/android/graphics/n$a;->e:F

    sub-float v1, p3, v1

    mul-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 292
    iget-object v2, p0, Ltv/periscope/android/graphics/n$a;->a:Ltv/periscope/android/graphics/n$a$a;

    invoke-interface {v2, v0, v1}, Ltv/periscope/android/graphics/n$a$a;->a(FF)V

    .line 293
    iput p2, p0, Ltv/periscope/android/graphics/n$a;->d:F

    .line 294
    iput p3, p0, Ltv/periscope/android/graphics/n$a;->e:F

    .line 295
    return-void
.end method

.method private b(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 235
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Ltv/periscope/android/graphics/n$a;->d:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 236
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Ltv/periscope/android/graphics/n$a;->e:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 237
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    .line 238
    iget v1, p0, Ltv/periscope/android/graphics/n$a;->c:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 5

    .prologue
    const/high16 v4, -0x40200000    # -1.75f

    .line 298
    iget-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 300
    iget-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr v0, v4

    .line 301
    iget-object v1, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v1, v4

    .line 302
    iget-object v2, p0, Ltv/periscope/android/graphics/n$a;->a:Ltv/periscope/android/graphics/n$a$a;

    invoke-interface {v2, v0, v1}, Ltv/periscope/android/graphics/n$a$a;->b(FF)V

    .line 304
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/periscope/android/graphics/n$a;->f:Z

    .line 217
    iget-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 218
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    .line 220
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 242
    iget-boolean v2, p0, Ltv/periscope/android/graphics/n$a;->f:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-le v2, v1, :cond_1

    .line 273
    :cond_0
    :goto_0
    return v0

    .line 246
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 248
    :pswitch_0
    iput-boolean v0, p0, Ltv/periscope/android/graphics/n$a;->g:Z

    .line 249
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {p0, p1, v1, v2}, Ltv/periscope/android/graphics/n$a;->a(Landroid/view/MotionEvent;FF)V

    goto :goto_0

    .line 252
    :pswitch_1
    invoke-direct {p0, p1}, Ltv/periscope/android/graphics/n$a;->b(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 256
    iput-boolean v1, p0, Ltv/periscope/android/graphics/n$a;->g:Z

    .line 257
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v2

    .line 258
    :goto_1
    if-ge v0, v2, :cond_2

    .line 259
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v4

    invoke-direct {p0, p1, v3, v4}, Ltv/periscope/android/graphics/n$a;->b(Landroid/view/MotionEvent;FF)V

    .line 258
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 261
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {p0, p1, v0, v2}, Ltv/periscope/android/graphics/n$a;->b(Landroid/view/MotionEvent;FF)V

    move v0, v1

    .line 262
    goto :goto_0

    .line 264
    :pswitch_2
    iget-boolean v0, p0, Ltv/periscope/android/graphics/n$a;->g:Z

    if-eqz v0, :cond_3

    .line 265
    invoke-direct {p0}, Ltv/periscope/android/graphics/n$a;->d()V

    .line 267
    :cond_3
    iget-boolean v0, p0, Ltv/periscope/android/graphics/n$a;->g:Z

    goto :goto_0

    .line 269
    :pswitch_3
    iput-boolean v0, p0, Ltv/periscope/android/graphics/n$a;->g:Z

    goto :goto_0

    .line 246
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/periscope/android/graphics/n$a;->f:Z

    .line 224
    iget-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 226
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/periscope/android/graphics/n$a;->b:Landroid/view/VelocityTracker;

    .line 228
    :cond_0
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 231
    invoke-virtual {p0}, Ltv/periscope/android/graphics/n$a;->b()V

    .line 232
    return-void
.end method
