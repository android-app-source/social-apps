.class public Ltv/periscope/android/graphics/f;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Ltv/periscope/android/graphics/j;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput p1, p0, Ltv/periscope/android/graphics/f;->c:I

    .line 40
    iput p2, p0, Ltv/periscope/android/graphics/f;->d:I

    .line 41
    iput p3, p0, Ltv/periscope/android/graphics/f;->b:I

    .line 42
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 11

    .prologue
    const/16 v4, 0x2601

    const v10, 0x8d40

    const/4 v3, 0x1

    const/16 v0, 0xde1

    const/4 v1, 0x0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput p1, p0, Ltv/periscope/android/graphics/f;->c:I

    .line 15
    iput p2, p0, Ltv/periscope/android/graphics/f;->d:I

    .line 17
    new-array v2, v3, [I

    .line 18
    invoke-static {v3, v2, v1}, Landroid/opengl/GLES20;->glGenFramebuffers(I[II)V

    .line 19
    aget v2, v2, v1

    iput v2, p0, Ltv/periscope/android/graphics/f;->b:I

    .line 21
    new-array v9, v3, [I

    .line 22
    const v2, 0x8ca6

    invoke-static {v2, v9, v1}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 23
    iget v2, p0, Ltv/periscope/android/graphics/f;->b:I

    invoke-static {v10, v2}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 25
    new-instance v2, Ltv/periscope/android/graphics/j;

    invoke-static {p1, p2}, Ltv/periscope/android/util/Size;->a(II)Ltv/periscope/android/util/Size;

    move-result-object v3

    invoke-direct {v2, v3}, Ltv/periscope/android/graphics/j;-><init>(Ltv/periscope/android/util/Size;)V

    iput-object v2, p0, Ltv/periscope/android/graphics/f;->a:Ltv/periscope/android/graphics/j;

    .line 27
    iget-object v2, p0, Ltv/periscope/android/graphics/f;->a:Ltv/periscope/android/graphics/j;

    invoke-virtual {v2}, Ltv/periscope/android/graphics/j;->h()I

    move-result v2

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 28
    const/16 v2, 0x2802

    const v3, 0x812f

    invoke-static {v0, v2, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 29
    const/16 v2, 0x2803

    const v3, 0x812f

    invoke-static {v0, v2, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 30
    const/16 v2, 0x2801

    invoke-static {v0, v2, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 31
    const/16 v2, 0x2800

    invoke-static {v0, v2, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 32
    const/4 v8, 0x0

    move v2, p3

    move v3, p1

    move v4, p2

    move v5, v1

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 33
    const v2, 0x8ce0

    iget-object v3, p0, Ltv/periscope/android/graphics/f;->a:Ltv/periscope/android/graphics/j;

    invoke-virtual {v3}, Ltv/periscope/android/graphics/j;->h()I

    move-result v3

    invoke-static {v10, v2, v0, v3, v1}, Landroid/opengl/GLES20;->glFramebufferTexture2D(IIIII)V

    .line 34
    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 35
    aget v0, v9, v1

    invoke-static {v10, v0}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 36
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    iget-object v0, p0, Ltv/periscope/android/graphics/f;->a:Ltv/periscope/android/graphics/j;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/j;->a()V

    .line 46
    new-array v0, v3, [I

    iget v1, p0, Ltv/periscope/android/graphics/f;->b:I

    aput v1, v0, v2

    .line 47
    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteFramebuffers(I[II)V

    .line 48
    return-void
.end method

.method public b()Ltv/periscope/android/graphics/j;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Ltv/periscope/android/graphics/f;->a:Ltv/periscope/android/graphics/j;

    return-object v0
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    const v0, 0x8d40

    iget v1, p0, Ltv/periscope/android/graphics/f;->b:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 60
    iget v0, p0, Ltv/periscope/android/graphics/f;->c:I

    iget v1, p0, Ltv/periscope/android/graphics/f;->d:I

    invoke-static {v2, v2, v0, v1}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 61
    return-void
.end method
