.class public Ltv/periscope/android/graphics/h;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected a:Ltv/periscope/android/graphics/c;

.field protected b:I

.field protected c:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ltv/periscope/android/graphics/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/h;->c:Ljava/util/Stack;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ltv/periscope/android/graphics/j;II)V
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Ltv/periscope/android/graphics/h;->a:Ltv/periscope/android/graphics/c;

    invoke-virtual {v0, p1}, Ltv/periscope/android/graphics/c;->b(Ljava/lang/String;)I

    move-result v0

    .line 52
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 53
    const v1, 0x84c0

    iget v2, p0, Ltv/periscope/android/graphics/h;->b:I

    add-int/2addr v1, v2

    invoke-static {v1}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 54
    invoke-virtual {p2, p3, p4}, Ltv/periscope/android/graphics/j;->a(II)I

    move-result v1

    .line 55
    iget v2, p0, Ltv/periscope/android/graphics/h;->b:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 56
    iget v0, p0, Ltv/periscope/android/graphics/h;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Ltv/periscope/android/graphics/h;->b:I

    .line 58
    :cond_0
    return-void
.end method

.method public a(Ltv/periscope/android/graphics/c;)V
    .locals 1

    .prologue
    .line 15
    iput-object p1, p0, Ltv/periscope/android/graphics/h;->a:Ltv/periscope/android/graphics/c;

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Ltv/periscope/android/graphics/h;->b:I

    .line 17
    iget-object v0, p0, Ltv/periscope/android/graphics/h;->a:Ltv/periscope/android/graphics/c;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/c;->a()V

    .line 18
    return-void
.end method

.method a(Ltv/periscope/android/graphics/f;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 31
    iget-object v0, p0, Ltv/periscope/android/graphics/h;->c:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 32
    const/4 v0, 0x4

    new-array v0, v0, [I

    .line 33
    const/16 v1, 0xba2

    invoke-static {v1, v0, v6}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 34
    const/4 v1, 0x1

    new-array v1, v1, [I

    .line 35
    const v2, 0x8ca6

    invoke-static {v2, v1, v6}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 36
    iget-object v2, p0, Ltv/periscope/android/graphics/h;->c:Ljava/util/Stack;

    new-instance v3, Ltv/periscope/android/graphics/f;

    const/4 v4, 0x2

    aget v4, v0, v4

    const/4 v5, 0x3

    aget v0, v0, v5

    aget v1, v1, v6

    invoke-direct {v3, v4, v0, v1}, Ltv/periscope/android/graphics/f;-><init>(III)V

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/graphics/h;->c:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    invoke-virtual {p1}, Ltv/periscope/android/graphics/f;->c()V

    .line 40
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 21
    iget-object v0, p0, Ltv/periscope/android/graphics/h;->a:Ltv/periscope/android/graphics/c;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/c;->b()V

    .line 22
    const v0, 0x8892

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    move v0, v1

    .line 23
    :goto_0
    iget v2, p0, Ltv/periscope/android/graphics/h;->b:I

    if-ge v0, v2, :cond_0

    .line 24
    const v2, 0x84c0

    add-int/2addr v2, v0

    invoke-static {v2}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 25
    const/16 v2, 0xde1

    invoke-static {v2, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 26
    const v2, 0x8d65

    invoke-static {v2, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 23
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    :cond_0
    return-void
.end method

.method d()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Ltv/periscope/android/graphics/h;->c:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 44
    iget-object v0, p0, Ltv/periscope/android/graphics/h;->c:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/graphics/f;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/f;->c()V

    .line 45
    iget-object v0, p0, Ltv/periscope/android/graphics/h;->c:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 46
    iget-object v0, p0, Ltv/periscope/android/graphics/h;->c:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 48
    :cond_0
    return-void
.end method
