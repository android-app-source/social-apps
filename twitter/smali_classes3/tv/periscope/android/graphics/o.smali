.class public Ltv/periscope/android/graphics/o;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected a:I

.field protected b:I

.field private c:Ltv/periscope/android/graphics/e;

.field private d:Ltv/periscope/android/graphics/c;

.field private e:Ltv/periscope/android/graphics/c;

.field private f:Ltv/periscope/android/graphics/a;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput v0, p0, Ltv/periscope/android/graphics/o;->a:I

    .line 13
    iput v0, p0, Ltv/periscope/android/graphics/o;->b:I

    .line 22
    new-instance v0, Ltv/periscope/android/graphics/e;

    invoke-direct {v0}, Ltv/periscope/android/graphics/e;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/o;->c:Ltv/periscope/android/graphics/e;

    .line 23
    new-instance v0, Ltv/periscope/android/graphics/c;

    sget v1, Ltv/periscope/android/library/f$k;->pass_vs:I

    sget v2, Ltv/periscope/android/library/f$k;->pass_fs:I

    invoke-direct {v0, p1, v1, v2}, Ltv/periscope/android/graphics/c;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Ltv/periscope/android/graphics/o;->e:Ltv/periscope/android/graphics/c;

    .line 24
    new-instance v0, Ltv/periscope/android/graphics/c;

    sget v1, Ltv/periscope/android/library/f$k;->pass_vs:I

    sget v2, Ltv/periscope/android/library/f$k;->pass_oes_fs:I

    invoke-direct {v0, p1, v1, v2}, Ltv/periscope/android/graphics/c;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Ltv/periscope/android/graphics/o;->d:Ltv/periscope/android/graphics/c;

    .line 25
    new-instance v0, Ltv/periscope/android/graphics/a;

    invoke-direct {v0}, Ltv/periscope/android/graphics/a;-><init>()V

    iput-object v0, p0, Ltv/periscope/android/graphics/o;->f:Ltv/periscope/android/graphics/a;

    .line 26
    return-void
.end method


# virtual methods
.method protected a(I)I
    .locals 0

    .prologue
    .line 68
    return p1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ltv/periscope/android/graphics/o;->f:Ltv/periscope/android/graphics/a;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Ltv/periscope/android/graphics/o;->f:Ltv/periscope/android/graphics/a;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/a;->a()V

    .line 49
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/graphics/o;->c:Ltv/periscope/android/graphics/e;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/e;->a()V

    .line 50
    iget-object v0, p0, Ltv/periscope/android/graphics/o;->e:Ltv/periscope/android/graphics/c;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/c;->c()V

    .line 51
    iget-object v0, p0, Ltv/periscope/android/graphics/o;->d:Ltv/periscope/android/graphics/c;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/c;->c()V

    .line 52
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ltv/periscope/android/graphics/o;->f:Ltv/periscope/android/graphics/a;

    invoke-virtual {v0, p1}, Ltv/periscope/android/graphics/a;->a(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method public a(Ltv/periscope/android/graphics/i;)V
    .locals 5

    .prologue
    .line 55
    invoke-virtual {p1}, Ltv/periscope/android/graphics/i;->g()Ltv/periscope/android/util/Size;

    move-result-object v0

    .line 56
    if-nez v0, :cond_0

    .line 65
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-virtual {p1}, Ltv/periscope/android/graphics/i;->c()I

    move-result v1

    invoke-virtual {p0, v1}, Ltv/periscope/android/graphics/o;->a(I)I

    move-result v1

    .line 61
    invoke-static {}, Ltv/periscope/android/graphics/k;->a()Ltv/periscope/android/util/Size;

    move-result-object v2

    .line 63
    iget-object v3, p0, Ltv/periscope/android/graphics/o;->f:Ltv/periscope/android/graphics/a;

    iget-boolean v4, p0, Ltv/periscope/android/graphics/o;->g:Z

    invoke-virtual {v3, v0, v2, v1, v4}, Ltv/periscope/android/graphics/a;->a(Ltv/periscope/android/util/Size;Ltv/periscope/android/util/Size;IZ)V

    .line 64
    iget-object v0, p0, Ltv/periscope/android/graphics/o;->f:Ltv/periscope/android/graphics/a;

    iget-object v1, p0, Ltv/periscope/android/graphics/o;->e:Ltv/periscope/android/graphics/c;

    iget-object v2, p0, Ltv/periscope/android/graphics/o;->d:Ltv/periscope/android/graphics/c;

    iget-object v3, p0, Ltv/periscope/android/graphics/o;->c:Ltv/periscope/android/graphics/e;

    invoke-virtual {v0, v1, v2, v3, p1}, Ltv/periscope/android/graphics/a;->a(Ltv/periscope/android/graphics/c;Ltv/periscope/android/graphics/c;Ltv/periscope/android/graphics/e;Ltv/periscope/android/graphics/j;)Ltv/periscope/android/graphics/f;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Ltv/periscope/android/graphics/o;->g:Z

    .line 35
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 38
    iput p1, p0, Ltv/periscope/android/graphics/o;->a:I

    .line 39
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Ltv/periscope/android/graphics/o;->b:I

    .line 43
    return-void
.end method
