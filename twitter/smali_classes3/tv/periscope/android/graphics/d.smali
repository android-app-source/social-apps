.class public Ltv/periscope/android/graphics/d;
.super Ltv/periscope/android/graphics/h;
.source "Twttr"


# static fields
.field private static d:[F

.field private static final e:[S


# instance fields
.field private f:Ljava/nio/FloatBuffer;

.field private g:Ljava/nio/ShortBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/16 v0, 0x8

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Ltv/periscope/android/graphics/d;->d:[F

    .line 19
    const/4 v0, 0x6

    new-array v0, v0, [S

    fill-array-data v0, :array_1

    sput-object v0, Ltv/periscope/android/graphics/d;->e:[S

    return-void

    .line 13
    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 19
    :array_1
    .array-data 2
        0x0s
        0x1s
        0x2s
        0x1s
        0x3s
        0x2s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-direct {p0}, Ltv/periscope/android/graphics/h;-><init>()V

    .line 26
    sget-object v0, Ltv/periscope/android/graphics/d;->d:[F

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 27
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 28
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/d;->f:Ljava/nio/FloatBuffer;

    .line 29
    iget-object v0, p0, Ltv/periscope/android/graphics/d;->f:Ljava/nio/FloatBuffer;

    sget-object v1, Ltv/periscope/android/graphics/d;->d:[F

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 30
    iget-object v0, p0, Ltv/periscope/android/graphics/d;->f:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 32
    sget-object v0, Ltv/periscope/android/graphics/d;->e:[S

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 33
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 34
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/graphics/d;->g:Ljava/nio/ShortBuffer;

    .line 35
    iget-object v0, p0, Ltv/periscope/android/graphics/d;->g:Ljava/nio/ShortBuffer;

    sget-object v1, Ltv/periscope/android/graphics/d;->e:[S

    invoke-virtual {v0, v1}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    .line 36
    iget-object v0, p0, Ltv/periscope/android/graphics/d;->g:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 37
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public a([FII)V
    .locals 8

    .prologue
    const v1, 0x3f8ccccd    # 1.1f

    const/4 v3, 0x0

    .line 45
    iget-object v0, p0, Ltv/periscope/android/graphics/d;->a:Ltv/periscope/android/graphics/c;

    const-string/jumbo v2, "viewMatrix"

    invoke-virtual {v0, v2}, Ltv/periscope/android/graphics/c;->b(Ljava/lang/String;)I

    move-result v2

    .line 46
    iget-object v0, p0, Ltv/periscope/android/graphics/d;->a:Ltv/periscope/android/graphics/c;

    const-string/jumbo v4, "nearPlane"

    invoke-virtual {v0, v4}, Ltv/periscope/android/graphics/c;->b(Ljava/lang/String;)I

    move-result v4

    .line 47
    iget-object v0, p0, Ltv/periscope/android/graphics/d;->a:Ltv/periscope/android/graphics/c;

    const-string/jumbo v5, "aspectRatio"

    invoke-virtual {v0, v5}, Ltv/periscope/android/graphics/c;->b(Ljava/lang/String;)I

    move-result v5

    .line 50
    iget-object v0, p0, Ltv/periscope/android/graphics/d;->a:Ltv/periscope/android/graphics/c;

    const-string/jumbo v6, "vs_Position"

    invoke-virtual {v0, v6}, Ltv/periscope/android/graphics/c;->a(Ljava/lang/String;)I

    move-result v0

    .line 52
    int-to-float v6, p2

    int-to-float v7, p3

    div-float/2addr v6, v7

    .line 53
    const/4 v7, 0x1

    invoke-static {v2, v7, v3, p1, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 54
    if-le p2, p3, :cond_0

    mul-float/2addr v1, v6

    :cond_0
    invoke-static {v4, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 55
    invoke-static {v5, v6}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 57
    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 58
    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/16 v4, 0x8

    iget-object v5, p0, Ltv/periscope/android/graphics/d;->f:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 62
    const/4 v1, 0x4

    sget-object v2, Ltv/periscope/android/graphics/d;->e:[S

    array-length v2, v2

    const/16 v3, 0x1403

    iget-object v4, p0, Ltv/periscope/android/graphics/d;->g:Ljava/nio/ShortBuffer;

    invoke-static {v1, v2, v3, v4}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    .line 65
    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 67
    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 68
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 69
    const/16 v0, 0xc11

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 70
    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 71
    const/16 v0, 0x207

    invoke-static {v0}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    .line 72
    return-void
.end method
