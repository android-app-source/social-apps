.class public final enum Ltv/periscope/android/event/AppEvent$Type;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/event/AppEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/android/event/AppEvent$Type;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltv/periscope/android/event/AppEvent$Type;

.field public static final enum b:Ltv/periscope/android/event/AppEvent$Type;

.field public static final enum c:Ltv/periscope/android/event/AppEvent$Type;

.field public static final enum d:Ltv/periscope/android/event/AppEvent$Type;

.field public static final enum e:Ltv/periscope/android/event/AppEvent$Type;

.field public static final enum f:Ltv/periscope/android/event/AppEvent$Type;

.field public static final enum g:Ltv/periscope/android/event/AppEvent$Type;

.field public static final enum h:Ltv/periscope/android/event/AppEvent$Type;

.field public static final enum i:Ltv/periscope/android/event/AppEvent$Type;

.field private static final synthetic j:[Ltv/periscope/android/event/AppEvent$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Ltv/periscope/android/event/AppEvent$Type;

    const-string/jumbo v1, "OnNormalLogout"

    invoke-direct {v0, v1, v3}, Ltv/periscope/android/event/AppEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/event/AppEvent$Type;->a:Ltv/periscope/android/event/AppEvent$Type;

    .line 9
    new-instance v0, Ltv/periscope/android/event/AppEvent$Type;

    const-string/jumbo v1, "OnUnauthorizedLogout"

    invoke-direct {v0, v1, v4}, Ltv/periscope/android/event/AppEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/event/AppEvent$Type;->b:Ltv/periscope/android/event/AppEvent$Type;

    .line 10
    new-instance v0, Ltv/periscope/android/event/AppEvent$Type;

    const-string/jumbo v1, "OnBannedUserLogout"

    invoke-direct {v0, v1, v5}, Ltv/periscope/android/event/AppEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/event/AppEvent$Type;->c:Ltv/periscope/android/event/AppEvent$Type;

    .line 11
    new-instance v0, Ltv/periscope/android/event/AppEvent$Type;

    const-string/jumbo v1, "OnBannedCopyrightUserLogout"

    invoke-direct {v0, v1, v6}, Ltv/periscope/android/event/AppEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/event/AppEvent$Type;->d:Ltv/periscope/android/event/AppEvent$Type;

    .line 12
    new-instance v0, Ltv/periscope/android/event/AppEvent$Type;

    const-string/jumbo v1, "OnBannedRectifiableUserLogout"

    invoke-direct {v0, v1, v7}, Ltv/periscope/android/event/AppEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/event/AppEvent$Type;->e:Ltv/periscope/android/event/AppEvent$Type;

    .line 13
    new-instance v0, Ltv/periscope/android/event/AppEvent$Type;

    const-string/jumbo v1, "OnDeactivateAccount"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/event/AppEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/event/AppEvent$Type;->f:Ltv/periscope/android/event/AppEvent$Type;

    .line 14
    new-instance v0, Ltv/periscope/android/event/AppEvent$Type;

    const-string/jumbo v1, "OnUnban"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/event/AppEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/event/AppEvent$Type;->g:Ltv/periscope/android/event/AppEvent$Type;

    .line 15
    new-instance v0, Ltv/periscope/android/event/AppEvent$Type;

    const-string/jumbo v1, "OnLoggedIn"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/event/AppEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/event/AppEvent$Type;->h:Ltv/periscope/android/event/AppEvent$Type;

    .line 16
    new-instance v0, Ltv/periscope/android/event/AppEvent$Type;

    const-string/jumbo v1, "OnAppNotification"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/event/AppEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/event/AppEvent$Type;->i:Ltv/periscope/android/event/AppEvent$Type;

    .line 7
    const/16 v0, 0x9

    new-array v0, v0, [Ltv/periscope/android/event/AppEvent$Type;

    sget-object v1, Ltv/periscope/android/event/AppEvent$Type;->a:Ltv/periscope/android/event/AppEvent$Type;

    aput-object v1, v0, v3

    sget-object v1, Ltv/periscope/android/event/AppEvent$Type;->b:Ltv/periscope/android/event/AppEvent$Type;

    aput-object v1, v0, v4

    sget-object v1, Ltv/periscope/android/event/AppEvent$Type;->c:Ltv/periscope/android/event/AppEvent$Type;

    aput-object v1, v0, v5

    sget-object v1, Ltv/periscope/android/event/AppEvent$Type;->d:Ltv/periscope/android/event/AppEvent$Type;

    aput-object v1, v0, v6

    sget-object v1, Ltv/periscope/android/event/AppEvent$Type;->e:Ltv/periscope/android/event/AppEvent$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ltv/periscope/android/event/AppEvent$Type;->f:Ltv/periscope/android/event/AppEvent$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ltv/periscope/android/event/AppEvent$Type;->g:Ltv/periscope/android/event/AppEvent$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ltv/periscope/android/event/AppEvent$Type;->h:Ltv/periscope/android/event/AppEvent$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ltv/periscope/android/event/AppEvent$Type;->i:Ltv/periscope/android/event/AppEvent$Type;

    aput-object v2, v0, v1

    sput-object v0, Ltv/periscope/android/event/AppEvent$Type;->j:[Ltv/periscope/android/event/AppEvent$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/android/event/AppEvent$Type;
    .locals 1

    .prologue
    .line 7
    const-class v0, Ltv/periscope/android/event/AppEvent$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/event/AppEvent$Type;

    return-object v0
.end method

.method public static values()[Ltv/periscope/android/event/AppEvent$Type;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Ltv/periscope/android/event/AppEvent$Type;->j:[Ltv/periscope/android/event/AppEvent$Type;

    invoke-virtual {v0}, [Ltv/periscope/android/event/AppEvent$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/android/event/AppEvent$Type;

    return-object v0
.end method
