.class public final enum Ltv/periscope/android/event/ApiEvent$EventType;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/android/event/ApiEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EventType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/android/event/ApiEvent$EventType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltv/periscope/android/event/ApiEvent$EventType;

.field public static final enum b:Ltv/periscope/android/event/ApiEvent$EventType;

.field private static final synthetic c:[Ltv/periscope/android/event/ApiEvent$EventType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 103
    new-instance v0, Ltv/periscope/android/event/ApiEvent$EventType;

    const-string/jumbo v1, "API"

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/event/ApiEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/event/ApiEvent$EventType;->a:Ltv/periscope/android/event/ApiEvent$EventType;

    .line 104
    new-instance v0, Ltv/periscope/android/event/ApiEvent$EventType;

    const-string/jumbo v1, "SERVICE"

    invoke-direct {v0, v1, v3}, Ltv/periscope/android/event/ApiEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltv/periscope/android/event/ApiEvent$EventType;->b:Ltv/periscope/android/event/ApiEvent$EventType;

    .line 102
    const/4 v0, 0x2

    new-array v0, v0, [Ltv/periscope/android/event/ApiEvent$EventType;

    sget-object v1, Ltv/periscope/android/event/ApiEvent$EventType;->a:Ltv/periscope/android/event/ApiEvent$EventType;

    aput-object v1, v0, v2

    sget-object v1, Ltv/periscope/android/event/ApiEvent$EventType;->b:Ltv/periscope/android/event/ApiEvent$EventType;

    aput-object v1, v0, v3

    sput-object v0, Ltv/periscope/android/event/ApiEvent$EventType;->c:[Ltv/periscope/android/event/ApiEvent$EventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/android/event/ApiEvent$EventType;
    .locals 1

    .prologue
    .line 102
    const-class v0, Ltv/periscope/android/event/ApiEvent$EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/event/ApiEvent$EventType;

    return-object v0
.end method

.method public static values()[Ltv/periscope/android/event/ApiEvent$EventType;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Ltv/periscope/android/event/ApiEvent$EventType;->c:[Ltv/periscope/android/event/ApiEvent$EventType;

    invoke-virtual {v0}, [Ltv/periscope/android/event/ApiEvent$EventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/android/event/ApiEvent$EventType;

    return-object v0
.end method
