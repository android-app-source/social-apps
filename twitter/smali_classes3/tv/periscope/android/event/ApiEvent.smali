.class public Ltv/periscope/android/event/ApiEvent;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/event/ApiEvent$Type;,
        Ltv/periscope/android/event/ApiEvent$EventType;
    }
.end annotation


# instance fields
.field public final a:Ltv/periscope/android/event/ApiEvent$Type;

.field public final b:Ljava/lang/String;

.field public final c:Ltv/periscope/android/api/ApiRequest;

.field public final d:Ljava/lang/Object;

.field public final e:Lretrofit/RetrofitError;

.field public final f:Z


# direct methods
.method public constructor <init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;Z)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    .line 51
    iput-object p2, p0, Ltv/periscope/android/event/ApiEvent;->b:Ljava/lang/String;

    .line 52
    iput-object p3, p0, Ltv/periscope/android/event/ApiEvent;->c:Ltv/periscope/android/api/ApiRequest;

    .line 53
    iput-object p5, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    .line 54
    iput-object p4, p0, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    .line 55
    iput-boolean p6, p0, Ltv/periscope/android/event/ApiEvent;->f:Z

    .line 56
    return-void
.end method

.method public constructor <init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Z)V
    .locals 7

    .prologue
    .line 39
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;Z)V

    .line 40
    return-void
.end method

.method public constructor <init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Lretrofit/RetrofitError;Z)V
    .locals 7

    .prologue
    .line 44
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/event/ApiEvent;-><init>(Ltv/periscope/android/event/ApiEvent$Type;Ljava/lang/String;Ltv/periscope/android/api/ApiRequest;Ljava/lang/Object;Lretrofit/RetrofitError;Z)V

    .line 45
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    invoke-virtual {v0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    invoke-virtual {v0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    invoke-virtual {v0}, Lretrofit/client/Response;->getReason()Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    invoke-virtual {v0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    invoke-virtual {v0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    invoke-virtual {v0}, Lretrofit/client/Response;->getStatus()I

    move-result v0

    .line 78
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x208

    goto :goto_0
.end method

.method public d()Ltv/periscope/android/api/ErrorResponse;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    invoke-virtual {v0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    invoke-virtual {v0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    invoke-virtual {v0}, Lretrofit/client/Response;->getBody()Lretrofit/mime/TypedInput;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "application/json"

    iget-object v1, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    .line 85
    invoke-virtual {v1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v1

    invoke-virtual {v1}, Lretrofit/client/Response;->getBody()Lretrofit/mime/TypedInput;

    move-result-object v1

    invoke-interface {v1}, Lretrofit/mime/TypedInput;->mimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    :try_start_0
    iget-object v0, p0, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    const-class v1, Ltv/periscope/android/api/ErrorResponse;

    invoke-virtual {v0, v1}, Lretrofit/RetrofitError;->getBodyAs(Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/ErrorResponse;

    .line 89
    iget-object v1, v0, Ltv/periscope/android/api/ErrorResponse;->error:Ltv/periscope/android/api/ErrorResponseItem;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    .line 95
    :goto_0
    return-object v0

    .line 92
    :catch_0
    move-exception v0

    .line 95
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Ltv/periscope/android/event/ApiEvent$EventType;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Ltv/periscope/android/event/ApiEvent$EventType;->a:Ltv/periscope/android/event/ApiEvent$EventType;

    return-object v0
.end method
