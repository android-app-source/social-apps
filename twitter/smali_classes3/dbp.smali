.class public abstract Ldbp;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/DialogInterface$OnClickListener;

.field private final c:Landroid/content/DialogInterface$OnClickListener;

.field private final d:Landroid/content/DialogInterface$OnDismissListener;

.field private e:Landroid/support/v7/app/AlertDialog;

.field private f:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Ldbp;->a:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Ldbp;->b:Landroid/content/DialogInterface$OnClickListener;

    .line 34
    iput-object p3, p0, Ldbp;->c:Landroid/content/DialogInterface$OnClickListener;

    .line 35
    iput-object p4, p0, Ldbp;->d:Landroid/content/DialogInterface$OnDismissListener;

    .line 36
    return-void
.end method

.method private b(Landroid/content/Context;)Landroid/support/v7/app/AlertDialog;
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Ldbp;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    .line 43
    sget v0, Ltv/periscope/android/library/f$g;->positive:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 44
    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    :cond_0
    sget v0, Ltv/periscope/android/library/f$g;->negative:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 48
    if-eqz v0, :cond_1

    .line 49
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    :cond_1
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 53
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 54
    invoke-virtual {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 55
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 52
    return-object v0
.end method


# virtual methods
.method protected abstract a(Landroid/content/Context;)Landroid/view/View;
.end method

.method public a()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Ldbp;->a:Landroid/content/Context;

    invoke-direct {p0, v0}, Ldbp;->b(Landroid/content/Context;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    .line 64
    :cond_0
    iget-object v0, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    iget-object v0, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 67
    :cond_1
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbp;->f:Z

    .line 72
    iget-object v0, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->dismiss()V

    .line 74
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbp;->f:Z

    .line 81
    iget-object v0, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->dismiss()V

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    .line 84
    invoke-virtual {p0}, Ldbp;->a()V

    .line 86
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 92
    sget v1, Ltv/periscope/android/library/f$g;->positive:I

    if-ne v0, v1, :cond_1

    .line 93
    iget-object v0, p0, Ldbp;->b:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Ldbp;->b:Landroid/content/DialogInterface$OnClickListener;

    iget-object v1, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    sget v1, Ltv/periscope/android/library/f$g;->negative:I

    if-ne v0, v1, :cond_0

    .line 97
    iget-object v0, p0, Ldbp;->c:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Ldbp;->c:Landroid/content/DialogInterface$OnClickListener;

    iget-object v1, p0, Ldbp;->e:Landroid/support/v7/app/AlertDialog;

    const/4 v2, -0x2

    invoke-interface {v0, v1, v2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Ldbp;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbp;->d:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Ldbp;->d:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 111
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbp;->f:Z

    .line 112
    return-void
.end method
