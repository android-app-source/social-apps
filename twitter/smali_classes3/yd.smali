.class public Lyd;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laun;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laun",
        "<",
        "Ljava/util/Collection",
        "<",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Long;",
        "Lcep;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lyf;


# direct methods
.method public constructor <init>(Lyf;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lyd;->a:Lyf;

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/twitter/model/core/Tweet;)Lceo;
    .locals 1

    .prologue
    .line 32
    invoke-static {p0}, Lyd;->b(Lcom/twitter/model/core/Tweet;)Lceo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lyd;)Lyf;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lyd;->a:Lyf;

    return-object v0
.end method

.method private static b(Lcom/twitter/model/core/Tweet;)Lceo;
    .locals 6

    .prologue
    .line 44
    new-instance v0, Lceo$a;

    invoke-direct {v0}, Lceo$a;-><init>()V

    .line 45
    invoke-static {p0}, Lyd;->c(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/moments/MomentPageType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lceo$a;->a(Lcom/twitter/model/moments/MomentPageType;)Lceo$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->G:J

    .line 46
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lceo$a;->a(Ljava/lang/String;)Lceo$a;

    move-result-object v0

    .line 47
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    new-instance v1, Lcem;

    .line 49
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->N()Lcom/twitter/model/core/MediaEntity;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/model/core/MediaEntity;->c:J

    sget-object v4, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    const-string/jumbo v5, ""

    invoke-direct {v1, v2, v3, v4, v5}, Lcem;-><init>(JLcom/twitter/util/math/Size;Ljava/lang/String;)V

    .line 48
    invoke-virtual {v0, v1}, Lceo$a;->a(Lcem;)Lceo$a;

    .line 51
    :cond_0
    invoke-virtual {v0}, Lceo$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lceo;

    return-object v0
.end method

.method private static c(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/moments/MomentPageType;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    sget-object v0, Lcom/twitter/model/moments/MomentPageType;->f:Lcom/twitter/model/moments/MomentPageType;

    .line 68
    :goto_0
    return-object v0

    .line 59
    :cond_0
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->c(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    sget-object v0, Lcom/twitter/model/moments/MomentPageType;->g:Lcom/twitter/model/moments/MomentPageType;

    goto :goto_0

    .line 61
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    sget-object v0, Lcom/twitter/model/moments/MomentPageType;->e:Lcom/twitter/model/moments/MomentPageType;

    goto :goto_0

    .line 63
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->K()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 64
    sget-object v0, Lcom/twitter/model/moments/MomentPageType;->c:Lcom/twitter/model/moments/MomentPageType;

    goto :goto_0

    .line 65
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 66
    sget-object v0, Lcom/twitter/model/moments/MomentPageType;->j:Lcom/twitter/model/moments/MomentPageType;

    goto :goto_0

    .line 68
    :cond_4
    sget-object v0, Lcom/twitter/model/moments/MomentPageType;->a:Lcom/twitter/model/moments/MomentPageType;

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Lrx/g;
    .locals 1

    .prologue
    .line 31
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lyd;->a(Ljava/util/Collection;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Collection;)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/g",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 76
    invoke-static {p1}, Lrx/c;->a(Ljava/lang/Iterable;)Lrx/c;

    move-result-object v0

    new-instance v1, Lyd$4;

    invoke-direct {v1, p0}, Lyd$4;-><init>(Lyd;)V

    .line 77
    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lyd$3;

    invoke-direct {v1, p0}, Lyd$3;-><init>(Lyd;)V

    .line 82
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lyd$2;

    invoke-direct {v1, p0}, Lyd$2;-><init>(Lyd;)V

    .line 87
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 92
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v1

    new-instance v2, Lyd$1;

    invoke-direct {v2, p0}, Lyd$1;-><init>(Lyd;)V

    invoke-virtual {v0, v1, v2}, Lrx/c;->a(Ljava/lang/Object;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lrx/c;->b()Lrx/g;

    move-result-object v0

    .line 76
    return-object v0
.end method
