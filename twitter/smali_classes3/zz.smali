.class public Lzz;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field private final a:J

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/bs;

.field private final c:Lbse;

.field private d:Lcom/twitter/model/moments/viewmodels/MomentPage;

.field private e:Lcom/twitter/model/moments/viewmodels/a;

.field private f:Lcom/twitter/model/moments/Moment;

.field private g:I


# direct methods
.method public constructor <init>(JLcom/twitter/android/moments/ui/fullscreen/bs;Lbse;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-wide p1, p0, Lzz;->a:J

    .line 40
    iput-object p3, p0, Lzz;->b:Lcom/twitter/android/moments/ui/fullscreen/bs;

    .line 41
    iput-object p4, p0, Lzz;->c:Lbse;

    .line 42
    return-void
.end method

.method static synthetic a(Lzz;)J
    .locals 2

    .prologue
    .line 23
    iget-wide v0, p0, Lzz;->a:J

    return-wide v0
.end method

.method private static a(JLcom/twitter/model/moments/viewmodels/a;Lcom/twitter/model/moments/viewmodels/MomentPage;I)Lcom/twitter/analytics/feature/model/MomentScribeDetails;
    .locals 4

    .prologue
    .line 110
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 111
    invoke-virtual {v0, p0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;

    invoke-direct {v1, p4}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;-><init>(I)V

    .line 112
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v1

    .line 113
    instance-of v0, p3, Lcom/twitter/model/moments/viewmodels/n;

    if-eqz v0, :cond_0

    move-object v0, p3

    .line 114
    check-cast v0, Lcom/twitter/model/moments/viewmodels/n;

    .line 115
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/n;->t()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 117
    :cond_0
    invoke-static {p3, p2}, Lzy;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/viewmodels/a;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 118
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    return-object v0
.end method

.method private a(I)V
    .locals 6

    .prologue
    .line 99
    iget-object v0, p0, Lzz;->d:Lcom/twitter/model/moments/viewmodels/MomentPage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzz;->d:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->g()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lzz;->a:J

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "moments:capsule:::close"

    aput-object v5, v1, v4

    invoke-direct {v0, v2, v3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    iget-object v1, p0, Lzz;->d:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 102
    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->g()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, p0, Lzz;->e:Lcom/twitter/model/moments/viewmodels/a;

    iget-object v4, p0, Lzz;->d:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-static {v2, v3, v1, v4, p1}, Lzz;->a(JLcom/twitter/model/moments/viewmodels/a;Lcom/twitter/model/moments/viewmodels/MomentPage;I)Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    move-result-object v1

    .line 101
    invoke-static {v1}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 100
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 105
    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/Moment;)V
    .locals 4

    .prologue
    .line 65
    iget-object v0, p0, Lzz;->c:Lbse;

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v0, v2, v3}, Lbse;->a(J)Lrx/c;

    move-result-object v0

    new-instance v1, Lzz$1;

    invoke-direct {v1, p0, p1, p2}, Lzz$1;-><init>(Lzz;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/Moment;)V

    .line 66
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 84
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lzz;->a(I)V

    .line 92
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lzz;->a(I)V

    .line 96
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public onPageSelected(I)V
    .locals 6

    .prologue
    .line 50
    iget-object v0, p0, Lzz;->b:Lcom/twitter/android/moments/ui/fullscreen/bs;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bs;->d(I)Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v0

    iput-object v0, p0, Lzz;->e:Lcom/twitter/model/moments/viewmodels/a;

    .line 51
    iget-object v0, p0, Lzz;->b:Lcom/twitter/android/moments/ui/fullscreen/bs;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bs;->c(I)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    iput-object v0, p0, Lzz;->d:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 52
    iget v0, p0, Lzz;->g:I

    .line 53
    iput p1, p0, Lzz;->g:I

    .line 54
    iget v1, p0, Lzz;->g:I

    if-le v1, v0, :cond_1

    const/4 v0, 0x1

    .line 55
    :goto_0
    iget-object v1, p0, Lzz;->e:Lcom/twitter/model/moments/viewmodels/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lzz;->d:Lcom/twitter/model/moments/viewmodels/MomentPage;

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, p0, Lzz;->f:Lcom/twitter/model/moments/Moment;

    .line 57
    iget-object v2, p0, Lzz;->e:Lcom/twitter/model/moments/viewmodels/a;

    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v2

    iput-object v2, p0, Lzz;->f:Lcom/twitter/model/moments/Moment;

    .line 58
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    iget-object v0, p0, Lzz;->f:Lcom/twitter/model/moments/Moment;

    iget-wide v4, v0, Lcom/twitter/model/moments/Moment;->b:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lzz;->f:Lcom/twitter/model/moments/Moment;

    invoke-direct {p0, v1, v0}, Lzz;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/Moment;)V

    .line 62
    :cond_0
    return-void

    .line 54
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
