.class Lrw$2;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lrw;->a(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/library/client/p;Lbbt$a;)Lcom/twitter/util/concurrent/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lcom/twitter/util/concurrent/ObservablePromise;

.field final synthetic d:Lrw;


# direct methods
.method constructor <init>(Lrw;JLjava/util/List;Lcom/twitter/util/concurrent/ObservablePromise;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lrw$2;->d:Lrw;

    iput-wide p2, p0, Lrw$2;->a:J

    iput-object p4, p0, Lrw$2;->b:Ljava/util/List;

    iput-object p5, p0, Lrw$2;->c:Lcom/twitter/util/concurrent/ObservablePromise;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 107
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lrw$2;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 110
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 115
    iget-object v1, p0, Lrw$2;->d:Lrw;

    invoke-static {v1}, Lrw;->a(Lrw;)Ljava/util/Map;

    move-result-object v1

    iget-wide v2, p0, Lrw$2;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget-object v0, p0, Lrw$2;->d:Lrw;

    invoke-static {v0}, Lrw;->a(Lrw;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lrw$2;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lrw$2;->c:Lcom/twitter/util/concurrent/ObservablePromise;

    iget-object v1, p0, Lrw$2;->d:Lrw;

    invoke-static {v1}, Lrw;->a(Lrw;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/concurrent/ObservablePromise;->set(Ljava/lang/Object;)V

    .line 119
    :cond_0
    return-void
.end method
