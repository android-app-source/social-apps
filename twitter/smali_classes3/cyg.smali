.class public Lcyg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;
.implements Lcyf;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcyg$f;,
        Lcyg$d;,
        Lcyg$g;,
        Lcyg$a;,
        Lcyg$c;,
        Lcyg$b;,
        Lcyg$e;
    }
.end annotation


# instance fields
.field A:Z

.field B:I

.field C:J

.field D:J

.field private E:Ltv/periscope/android/graphics/b;

.field private F:Lcyg$d;

.field private G:Lcyg$g;

.field private H:Lcyg$c;

.field private I:Lcyg$a;

.field private J:Z

.field private K:I

.field private L:I

.field private M:Landroid/os/Handler;

.field private N:I

.field private O:I

.field a:Landroid/hardware/Camera;

.field b:Landroid/hardware/Camera$CameraInfo;

.field c:Landroid/media/AudioRecord;

.field d:Landroid/media/MediaCodec;

.field e:Landroid/media/MediaCodec;

.field f:Lcyf$a;

.field g:Ltv/periscope/android/graphics/GLRenderView;

.field h:Ltv/periscope/android/graphics/i;

.field i:Ltv/periscope/android/graphics/b;

.field j:Ltv/periscope/android/graphics/o;

.field k:Ltv/periscope/android/graphics/o;

.field l:J

.field m:J

.field n:Ljava/util/concurrent/ArrayBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ArrayBlockingQueue",
            "<",
            "Lcyg$b;",
            ">;"
        }
    .end annotation
.end field

.field o:Ljava/util/concurrent/ArrayBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ArrayBlockingQueue",
            "<",
            "Lcyg$b;",
            ">;"
        }
    .end annotation
.end field

.field p:Z

.field q:Z

.field r:I

.field s:I

.field t:I

.field u:I

.field v:I

.field w:Ltv/periscope/android/util/Size;

.field x:Z

.field y:Landroid/media/MediaFormat;

.field final z:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/16 v1, 0x2d

    const/4 v2, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    iput-object v0, p0, Lcyg;->b:Landroid/hardware/Camera$CameraInfo;

    .line 86
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcyg;->n:Ljava/util/concurrent/ArrayBlockingQueue;

    .line 88
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcyg;->o:Ljava/util/concurrent/ArrayBlockingQueue;

    .line 93
    const v0, 0x64000

    iput v0, p0, Lcyg;->K:I

    .line 94
    const/16 v0, 0x18

    iput v0, p0, Lcyg;->L:I

    .line 95
    iput v2, p0, Lcyg;->r:I

    .line 96
    iput v2, p0, Lcyg;->s:I

    .line 100
    sget-object v0, Ltv/periscope/android/util/Size;->a:Ltv/periscope/android/util/Size;

    iput-object v0, p0, Lcyg;->w:Ltv/periscope/android/util/Size;

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcyg;->x:Z

    .line 102
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcyg;->M:Landroid/os/Handler;

    .line 103
    const v0, 0x8000

    iput v0, p0, Lcyg;->N:I

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->y:Landroid/media/MediaFormat;

    .line 105
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcyg;->z:Ljava/lang/Object;

    .line 106
    iput-boolean v2, p0, Lcyg;->A:Z

    .line 107
    iput v2, p0, Lcyg;->O:I

    .line 751
    iput-wide v4, p0, Lcyg;->C:J

    .line 752
    iput-wide v4, p0, Lcyg;->D:J

    return-void
.end method

.method static synthetic a(Lcyg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Lcyg;->s()V

    return-void
.end method

.method private l()V
    .locals 6

    .prologue
    const v2, 0xac44

    const/16 v3, 0x10

    const/4 v4, 0x2

    .line 141
    invoke-static {v2, v3, v4}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v0

    mul-int/lit8 v5, v0, 0xc

    .line 143
    new-instance v0, Landroid/media/AudioRecord;

    const/4 v1, 0x1

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcyg;->c:Landroid/media/AudioRecord;

    .line 146
    div-int/lit8 v0, v5, 0x2

    int-to-long v0, v0

    iput-wide v0, p0, Lcyg;->C:J

    .line 147
    iget-wide v0, p0, Lcyg;->C:J

    const-wide/32 v2, 0x3b9aca00

    mul-long/2addr v0, v2

    const-wide/32 v2, 0xac44

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcyg;->D:J

    .line 148
    return-void
.end method

.method private m()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 415
    const-string/jumbo v1, "CameraBroadcaster"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "resetAudioEncoder "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lcyg;->J:Z

    if-eqz v0, :cond_5

    const-string/jumbo v0, "Encoding"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    iget-boolean v0, p0, Lcyg;->J:Z

    if-eqz v0, :cond_3

    .line 417
    iget-object v0, p0, Lcyg;->F:Lcyg$d;

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcyg;->F:Lcyg$d;

    invoke-virtual {v0}, Lcyg$d;->d()V

    .line 419
    iput-object v3, p0, Lcyg;->F:Lcyg$d;

    .line 421
    :cond_0
    iget-object v0, p0, Lcyg;->I:Lcyg$a;

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lcyg;->I:Lcyg$a;

    invoke-virtual {v0}, Lcyg$a;->d()V

    .line 423
    iput-object v3, p0, Lcyg;->I:Lcyg$a;

    .line 425
    :cond_1
    iget-object v0, p0, Lcyg;->H:Lcyg$c;

    if-eqz v0, :cond_2

    .line 426
    iget-object v0, p0, Lcyg;->H:Lcyg$c;

    invoke-virtual {v0}, Lcyg$c;->d()V

    .line 427
    iput-object v3, p0, Lcyg;->H:Lcyg$c;

    .line 429
    :cond_2
    iget-object v0, p0, Lcyg;->e:Landroid/media/MediaCodec;

    if-eqz v0, :cond_3

    .line 430
    iget-object v0, p0, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 431
    iget-object v0, p0, Lcyg;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    .line 432
    iput-object v3, p0, Lcyg;->e:Landroid/media/MediaCodec;

    .line 436
    :cond_3
    iput-object v3, p0, Lcyg;->y:Landroid/media/MediaFormat;

    .line 439
    :try_start_0
    invoke-direct {p0}, Lcyg;->p()Landroid/media/MediaCodec;

    move-result-object v0

    iput-object v0, p0, Lcyg;->e:Landroid/media/MediaCodec;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 444
    :goto_1
    iget-object v0, p0, Lcyg;->e:Landroid/media/MediaCodec;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcyg;->J:Z

    if-eqz v0, :cond_4

    .line 445
    const-string/jumbo v0, "CameraBroadcaster"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Audio bitrate change to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcyg;->N:I

    div-int/lit16 v2, v2, 0x400

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " kbits/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    invoke-direct {p0}, Lcyg;->l()V

    .line 449
    new-instance v0, Lcyg$a;

    invoke-direct {v0, p0}, Lcyg$a;-><init>(Lcyg;)V

    iput-object v0, p0, Lcyg;->I:Lcyg$a;

    .line 450
    iget-object v0, p0, Lcyg;->I:Lcyg$a;

    invoke-virtual {v0}, Lcyg$a;->b()V

    .line 451
    iget-object v0, p0, Lcyg;->I:Lcyg$a;

    invoke-virtual {v0}, Lcyg$a;->c()V

    .line 453
    new-instance v0, Lcyg$c;

    invoke-direct {v0, p0}, Lcyg$c;-><init>(Lcyg;)V

    iput-object v0, p0, Lcyg;->H:Lcyg$c;

    .line 454
    iget-object v0, p0, Lcyg;->H:Lcyg$c;

    invoke-virtual {v0}, Lcyg$c;->b()V

    .line 456
    new-instance v0, Lcyg$d;

    invoke-direct {v0, p0}, Lcyg$d;-><init>(Lcyg;)V

    iput-object v0, p0, Lcyg;->F:Lcyg$d;

    .line 457
    iget-object v0, p0, Lcyg;->F:Lcyg$d;

    invoke-virtual {v0}, Lcyg$d;->b()V

    .line 460
    :cond_4
    return-void

    .line 415
    :cond_5
    const-string/jumbo v0, "Not encoding"

    goto/16 :goto_0

    .line 440
    :catch_0
    move-exception v0

    .line 441
    invoke-static {v0}, Lf;->a(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private n()V
    .locals 3

    .prologue
    .line 463
    iget-object v1, p0, Lcyg;->z:Ljava/lang/Object;

    monitor-enter v1

    .line 465
    :try_start_0
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 467
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 468
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 473
    :cond_0
    :try_start_1
    iget v0, p0, Lcyg;->r:I

    iget-object v2, p0, Lcyg;->b:Landroid/hardware/Camera$CameraInfo;

    invoke-static {v0, v2}, Ltv/periscope/android/util/h;->a(ILandroid/hardware/Camera$CameraInfo;)Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    .line 474
    monitor-exit v1

    .line 475
    return-void

    .line 470
    :catch_0
    move-exception v0

    .line 471
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 474
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private declared-synchronized o()V
    .locals 2

    .prologue
    .line 478
    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "CameraBroadcaster"

    const-string/jumbo v1, "resetCamera"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    iget-object v0, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 487
    :goto_0
    monitor-exit p0

    return-void

    .line 483
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcyg;->B:I

    .line 484
    invoke-direct {p0}, Lcyg;->n()V

    .line 485
    invoke-direct {p0}, Lcyg;->r()V

    .line 486
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcyg;->A:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 478
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private p()Landroid/media/MediaCodec;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 526
    new-instance v0, Landroid/media/MediaFormat;

    invoke-direct {v0}, Landroid/media/MediaFormat;-><init>()V

    .line 527
    const-string/jumbo v1, "mime"

    const-string/jumbo v2, "audio/mp4a-latm"

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    const-string/jumbo v1, "aac-profile"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 529
    const-string/jumbo v1, "sample-rate"

    const v2, 0xac44

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 530
    const-string/jumbo v1, "channel-count"

    invoke-virtual {v0, v1, v3}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 531
    const-string/jumbo v1, "bitrate"

    iget v2, p0, Lcyg;->N:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 532
    const-string/jumbo v1, "max-input-size"

    const/16 v2, 0x800

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 534
    const-string/jumbo v1, "audio/mp4a-latm"

    invoke-static {v1}, Landroid/media/MediaCodec;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v1

    .line 535
    invoke-virtual {v1, v0, v4, v4, v3}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 536
    return-object v1
.end method

.method private q()Landroid/media/MediaCodec;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x140

    const/4 v2, 0x0

    const/4 v10, 0x2

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 541
    const-string/jumbo v0, "video/avc"

    invoke-static {v0}, Landroid/media/MediaCodec;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    .line 543
    const-string/jumbo v4, "video/avc"

    const/16 v5, 0x238

    invoke-static {v4, v11, v5}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v5

    .line 544
    const-string/jumbo v4, "bitrate"

    iget v6, p0, Lcyg;->K:I

    invoke-virtual {v5, v4, v6}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 545
    const-string/jumbo v4, "frame-rate"

    iget v6, p0, Lcyg;->L:I

    invoke-virtual {v5, v4, v6}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 546
    const-string/jumbo v4, "i-frame-interval"

    invoke-virtual {v5, v4, v10}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 547
    const-string/jumbo v4, "color-format"

    const v6, 0x7f000789

    invoke-virtual {v5, v4, v6}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 548
    const-string/jumbo v4, "level"

    const/16 v6, 0x40

    invoke-virtual {v5, v4, v6}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 551
    invoke-virtual {v0}, Landroid/media/MediaCodec;->getName()Ljava/lang/String;

    move-result-object v6

    .line 552
    const-string/jumbo v4, "CameraBroadcaster"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Encoder: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    new-array v7, v10, [Ljava/lang/String;

    const-string/jumbo v4, "OMX.Exynos.AVC.Encoder"

    aput-object v4, v7, v2

    const-string/jumbo v4, "OMX.qcom.video.encoder.avc"

    aput-object v4, v7, v3

    .line 558
    array-length v8, v7

    move v4, v2

    :goto_0
    if-ge v4, v8, :cond_4

    aget-object v9, v7, v4

    .line 559
    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 564
    :goto_1
    if-eqz v2, :cond_2

    .line 565
    const-string/jumbo v2, "profile"

    invoke-virtual {v5, v2, v10}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 575
    :goto_2
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x1

    :try_start_0
    invoke-virtual {v0, v5, v2, v4, v6}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 582
    :goto_3
    if-nez v0, :cond_0

    .line 584
    const-string/jumbo v0, "CameraBroadcaster"

    const-string/jumbo v2, "Configuring encoder for baseline profile"

    invoke-static {v0, v2}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    const-string/jumbo v0, "video/avc"

    invoke-static {v0}, Landroid/media/MediaCodec;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    .line 586
    const-string/jumbo v2, "video/avc"

    const/16 v4, 0x238

    invoke-static {v2, v11, v4}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v2

    .line 587
    const-string/jumbo v4, "bitrate"

    iget v5, p0, Lcyg;->K:I

    invoke-virtual {v2, v4, v5}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 588
    const-string/jumbo v4, "frame-rate"

    iget v5, p0, Lcyg;->L:I

    invoke-virtual {v2, v4, v5}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 589
    const-string/jumbo v4, "i-frame-interval"

    invoke-virtual {v2, v4, v10}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 590
    const-string/jumbo v4, "color-format"

    const v5, 0x7f000789

    invoke-virtual {v2, v4, v5}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 591
    invoke-virtual {v0, v2, v1, v1, v3}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 593
    :cond_0
    return-object v0

    .line 558
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 566
    :cond_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-lt v2, v4, :cond_3

    .line 568
    const-string/jumbo v2, "profile"

    invoke-virtual {v5, v2, v3}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    goto :goto_2

    .line 571
    :cond_3
    const-string/jumbo v2, "profile"

    const/16 v4, 0x8

    invoke-virtual {v5, v2, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    goto :goto_2

    .line 576
    :catch_0
    move-exception v2

    .line 577
    const-string/jumbo v4, "CameraBroadcaster"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    move-object v0, v1

    .line 579
    goto :goto_3

    :cond_4
    move v2, v3

    goto :goto_1
.end method

.method private r()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 597
    iget-object v0, p0, Lcyg;->g:Ltv/periscope/android/graphics/GLRenderView;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/GLRenderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 598
    invoke-static {v0}, Ltv/periscope/android/util/ad;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 599
    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v1, v0}, Ltv/periscope/android/util/Size;->a(II)Ltv/periscope/android/util/Size;

    move-result-object v0

    .line 600
    iget-object v1, p0, Lcyg;->a:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->stopPreview()V

    .line 601
    iget-object v1, p0, Lcyg;->a:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 602
    invoke-virtual {v1, v4}, Landroid/hardware/Camera$Parameters;->setRecordingHint(Z)V

    .line 603
    const/16 v2, 0x5dc0

    .line 604
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v3

    .line 603
    invoke-static {v2, v3}, Ltv/periscope/android/util/h;->a(ILjava/util/List;)[I

    move-result-object v2

    .line 605
    if-eqz v2, :cond_0

    .line 606
    const/4 v3, 0x0

    aget v3, v2, v3

    aget v2, v2, v4

    invoke-virtual {v1, v3, v2}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    .line 608
    :cond_0
    iget-object v2, p0, Lcyg;->a:Landroid/hardware/Camera;

    invoke-static {v2, v1}, Ltv/periscope/android/util/h;->a(Landroid/hardware/Camera;Landroid/hardware/Camera$Parameters;)Z

    .line 609
    iget-object v2, p0, Lcyg;->b:Landroid/hardware/Camera$CameraInfo;

    iget v2, v2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    .line 611
    invoke-virtual {v0, v2}, Ltv/periscope/android/util/Size;->a(I)Ltv/periscope/android/util/Size;

    move-result-object v0

    .line 612
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v2

    .line 611
    invoke-static {v0, v2}, Ltv/periscope/android/util/h;->a(Ltv/periscope/android/util/Size;Ljava/util/List;)Ltv/periscope/android/util/Size;

    move-result-object v0

    .line 613
    const-string/jumbo v2, "CameraBroadcaster"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Camera Resolution: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->a()I

    move-result v2

    invoke-virtual {v0}, Ltv/periscope/android/util/Size;->b()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 615
    iput-object v0, p0, Lcyg;->w:Ltv/periscope/android/util/Size;

    .line 616
    iget v0, p0, Lcyg;->O:I

    invoke-virtual {v1, v0}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    .line 617
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 618
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 619
    return-void
.end method

.method private s()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 977
    iget-object v0, p0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    if-eqz v0, :cond_0

    .line 978
    iget-object v1, p0, Lcyg;->z:Ljava/lang/Object;

    monitor-enter v1

    .line 979
    :try_start_0
    iget v0, p0, Lcyg;->r:I

    iput v0, p0, Lcyg;->s:I

    .line 980
    iget-object v0, p0, Lcyg;->b:Landroid/hardware/Camera$CameraInfo;

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iput v0, p0, Lcyg;->v:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 982
    :try_start_1
    iget-object v0, p0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/i;->a()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 989
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    .line 991
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcyg;->A:Z

    .line 992
    new-instance v0, Ltv/periscope/android/graphics/i;

    invoke-direct {v0}, Ltv/periscope/android/graphics/i;-><init>()V

    iput-object v0, p0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    .line 993
    iget-object v0, p0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/i;->d()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 994
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    iget-object v2, p0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    invoke-virtual {v2}, Ltv/periscope/android/graphics/i;->d()Landroid/graphics/SurfaceTexture;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 995
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 997
    :cond_0
    return-void

    .line 983
    :catch_0
    move-exception v0

    .line 987
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 989
    const/4 v0, 0x0

    :try_start_4
    iput-object v0, p0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    goto :goto_0

    .line 995
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 989
    :catchall_1
    move-exception v0

    const/4 v2, 0x0

    :try_start_5
    iput-object v2, p0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    .prologue
    .line 234
    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "CameraBroadcaster"

    const-string/jumbo v1, "stopPreview"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 237
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 238
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    .line 240
    :cond_0
    iget-object v0, p0, Lcyg;->E:Ltv/periscope/android/graphics/b;

    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Lcyg;->E:Ltv/periscope/android/graphics/b;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/b;->b()V

    .line 242
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->E:Ltv/periscope/android/graphics/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    :cond_1
    monitor-exit p0

    return-void

    .line 234
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 248
    iput p1, p0, Lcyg;->t:I

    .line 249
    return-void
.end method

.method public a(Lcyf$a;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcyg;->f:Lcyf$a;

    .line 230
    return-void
.end method

.method a(Lcyg$b;)V
    .locals 2

    .prologue
    .line 854
    :try_start_0
    iget-object v0, p0, Lcyg;->o:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ArrayBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 858
    :goto_0
    return-void

    .line 855
    :catch_0
    move-exception v0

    .line 856
    const-string/jumbo v0, "cameraBroadcaster"

    const-string/jumbo v1, "dropping audio samples"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized a(Ltv/periscope/android/graphics/GLRenderView;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 152
    monitor-enter p0

    :try_start_0
    const-string/jumbo v2, "CameraBroadcaster"

    const-string/jumbo v3, "startPreview"

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iput-object p1, p0, Lcyg;->g:Ltv/periscope/android/graphics/GLRenderView;

    .line 154
    invoke-direct {p0}, Lcyg;->n()V

    .line 156
    invoke-direct {p0}, Lcyg;->l()V

    .line 159
    invoke-direct {p0}, Lcyg;->q()Landroid/media/MediaCodec;

    move-result-object v2

    iput-object v2, p0, Lcyg;->d:Landroid/media/MediaCodec;

    .line 160
    iget-object v2, p0, Lcyg;->d:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->createInputSurface()Landroid/view/Surface;

    move-result-object v2

    .line 162
    invoke-direct {p0}, Lcyg;->p()Landroid/media/MediaCodec;

    move-result-object v3

    iput-object v3, p0, Lcyg;->e:Landroid/media/MediaCodec;

    .line 164
    const-string/jumbo v3, "CameraBroadcaster"

    const-string/jumbo v4, "Encoders created"

    invoke-static {v3, v4}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    new-instance v3, Ltv/periscope/android/graphics/b;

    invoke-direct {v3}, Ltv/periscope/android/graphics/b;-><init>()V

    iput-object v3, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;

    .line 168
    iget-object v3, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v2}, Ltv/periscope/android/graphics/b;->a(Ltv/periscope/android/graphics/b;Landroid/view/Surface;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;

    .line 170
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Failed to create video encoder context"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    :catch_0
    move-exception v0

    .line 221
    :try_start_1
    const-string/jumbo v2, "CameraBroadcaster"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Failed to start camera: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-static {v0}, Lf;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 223
    :goto_0
    monitor-exit p0

    return v0

    .line 172
    :cond_0
    :try_start_2
    iget-object v2, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;

    new-instance v3, Lcyg$1;

    invoke-direct {v3, p0, p1}, Lcyg$1;-><init>(Lcyg;Ltv/periscope/android/graphics/GLRenderView;)V

    invoke-virtual {v2, v3}, Ltv/periscope/android/graphics/b;->a(Ltv/periscope/android/graphics/b$c;)Z

    .line 190
    new-instance v2, Ltv/periscope/android/graphics/b;

    invoke-direct {v2}, Ltv/periscope/android/graphics/b;-><init>()V

    iput-object v2, p0, Lcyg;->E:Ltv/periscope/android/graphics/b;

    .line 191
    iget-object v2, p0, Lcyg;->E:Ltv/periscope/android/graphics/b;

    iget-object v3, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Ltv/periscope/android/graphics/b;->a(Ltv/periscope/android/graphics/b;Landroid/view/Surface;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 192
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Failed to create video render context"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 194
    :cond_1
    :try_start_3
    iget-object v2, p0, Lcyg;->g:Ltv/periscope/android/graphics/GLRenderView;

    iget-object v3, p0, Lcyg;->E:Ltv/periscope/android/graphics/b;

    invoke-virtual {v3}, Ltv/periscope/android/graphics/b;->d()Ltv/periscope/android/graphics/GLRenderView$f;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/android/graphics/GLRenderView;->setEGLContextFactory(Ltv/periscope/android/graphics/GLRenderView$f;)V

    .line 195
    iget-object v2, p0, Lcyg;->g:Ltv/periscope/android/graphics/GLRenderView;

    iget-object v3, p0, Lcyg;->E:Ltv/periscope/android/graphics/b;

    invoke-virtual {v3}, Ltv/periscope/android/graphics/b;->e()Ltv/periscope/android/graphics/GLRenderView$e;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/android/graphics/GLRenderView;->setEGLConfigChooser(Ltv/periscope/android/graphics/GLRenderView$e;)V

    .line 196
    iget-object v2, p0, Lcyg;->g:Ltv/periscope/android/graphics/GLRenderView;

    invoke-virtual {v2}, Ltv/periscope/android/graphics/GLRenderView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 197
    iget-object v2, p0, Lcyg;->g:Ltv/periscope/android/graphics/GLRenderView;

    new-instance v3, Lcyg$f;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcyg$f;-><init>(Lcyg;Lcyg$1;)V

    invoke-virtual {v2, v3}, Ltv/periscope/android/graphics/GLRenderView;->setRenderer(Ltv/periscope/android/graphics/GLRenderView$k;)V

    .line 198
    iget-object v2, p0, Lcyg;->g:Ltv/periscope/android/graphics/GLRenderView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ltv/periscope/android/graphics/GLRenderView;->setRenderMode(I)V

    .line 200
    invoke-direct {p0}, Lcyg;->r()V

    .line 202
    iget-object v2, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;

    new-instance v3, Lcyg$2;

    invoke-direct {v3, p0}, Lcyg$2;-><init>(Lcyg;)V

    invoke-virtual {v2, v3}, Ltv/periscope/android/graphics/b;->a(Ltv/periscope/android/graphics/b$c;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized b()V
    .locals 6

    .prologue
    .line 253
    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "CameraBroadcaster"

    const-string/jumbo v1, "startEncoding"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-boolean v0, p0, Lcyg;->J:Z

    if-eqz v0, :cond_0

    .line 255
    const-string/jumbo v0, "CameraBroadcaster"

    const-string/jumbo v1, "Already encoding, no need to start it"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    :goto_0
    monitor-exit p0

    return-void

    .line 258
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcyg;->J:Z

    .line 259
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcyg;->m:J

    .line 260
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v0

    iput-wide v0, p0, Lcyg;->l:J

    .line 261
    const-string/jumbo v0, "RTMP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Base time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, p0, Lcyg;->l:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;

    if-nez v0, :cond_1

    .line 263
    const-string/jumbo v0, "CameraBroadcaster"

    const-string/jumbo v1, "Video encoder null in startEncoding"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 253
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 267
    :cond_1
    :try_start_2
    new-instance v0, Lcyg$a;

    invoke-direct {v0, p0}, Lcyg$a;-><init>(Lcyg;)V

    iput-object v0, p0, Lcyg;->I:Lcyg$a;

    .line 268
    iget-object v0, p0, Lcyg;->I:Lcyg$a;

    invoke-virtual {v0}, Lcyg$a;->b()V

    .line 269
    iget-object v0, p0, Lcyg;->I:Lcyg$a;

    invoke-virtual {v0}, Lcyg$a;->c()V

    .line 271
    new-instance v0, Lcyg$c;

    invoke-direct {v0, p0}, Lcyg$c;-><init>(Lcyg;)V

    iput-object v0, p0, Lcyg;->H:Lcyg$c;

    .line 272
    iget-object v0, p0, Lcyg;->H:Lcyg$c;

    invoke-virtual {v0}, Lcyg$c;->b()V

    .line 274
    new-instance v0, Lcyg$g;

    invoke-direct {v0, p0}, Lcyg$g;-><init>(Lcyg;)V

    iput-object v0, p0, Lcyg;->G:Lcyg$g;

    .line 275
    iget-object v0, p0, Lcyg;->G:Lcyg$g;

    invoke-virtual {v0}, Lcyg$g;->b()V

    .line 276
    iget-object v0, p0, Lcyg;->G:Lcyg$g;

    invoke-virtual {v0}, Lcyg$g;->c()V

    .line 278
    new-instance v0, Lcyg$d;

    invoke-direct {v0, p0}, Lcyg$d;-><init>(Lcyg;)V

    iput-object v0, p0, Lcyg;->F:Lcyg$d;

    .line 279
    iget-object v0, p0, Lcyg;->F:Lcyg$d;

    invoke-virtual {v0}, Lcyg$d;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 362
    iget v0, p0, Lcyg;->K:I

    if-eq v0, p1, :cond_0

    .line 363
    iput p1, p0, Lcyg;->K:I

    .line 364
    iget-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    .line 365
    invoke-virtual {p0}, Lcyg;->g()V

    .line 368
    :cond_0
    return-void
.end method

.method b(Lcyg$b;)V
    .locals 2

    .prologue
    .line 862
    :try_start_0
    iget-object v0, p0, Lcyg;->n:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ArrayBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 866
    :goto_0
    return-void

    .line 863
    :catch_0
    move-exception v0

    .line 864
    const-string/jumbo v0, "cameraBroadcaster"

    const-string/jumbo v1, "dropping audio samples"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized c()V
    .locals 1

    .prologue
    .line 284
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcyg;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    monitor-exit p0

    return-void

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 377
    iget v0, p0, Lcyg;->r:I

    if-eq v0, p1, :cond_0

    .line 378
    iput p1, p0, Lcyg;->r:I

    .line 379
    invoke-direct {p0}, Lcyg;->o()V

    .line 381
    :cond_0
    return-void
.end method

.method public declared-synchronized d()V
    .locals 2

    .prologue
    .line 289
    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "CameraBroadcaster"

    const-string/jumbo v1, "stopBroadcast"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcyg;->J:Z

    .line 291
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcyg;->p:Z

    .line 292
    iget-object v0, p0, Lcyg;->F:Lcyg$d;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcyg;->F:Lcyg$d;

    invoke-virtual {v0}, Lcyg$d;->d()V

    .line 294
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->F:Lcyg$d;

    .line 296
    :cond_0
    iget-object v0, p0, Lcyg;->H:Lcyg$c;

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcyg;->H:Lcyg$c;

    invoke-virtual {v0}, Lcyg$c;->d()V

    .line 298
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->H:Lcyg$c;

    .line 300
    :cond_1
    iget-object v0, p0, Lcyg;->I:Lcyg$a;

    if-eqz v0, :cond_2

    .line 301
    iget-object v0, p0, Lcyg;->I:Lcyg$a;

    invoke-virtual {v0}, Lcyg$a;->d()V

    .line 302
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->I:Lcyg$a;

    .line 304
    :cond_2
    iget-object v0, p0, Lcyg;->G:Lcyg$g;

    if-eqz v0, :cond_3

    .line 305
    iget-object v0, p0, Lcyg;->G:Lcyg$g;

    invoke-virtual {v0}, Lcyg$g;->d()V

    .line 306
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->G:Lcyg$g;

    .line 308
    :cond_3
    iget-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;

    if-eqz v0, :cond_4

    .line 309
    iget-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 310
    iget-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    .line 311
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;

    .line 313
    :cond_4
    iget-object v0, p0, Lcyg;->j:Ltv/periscope/android/graphics/o;

    if-eqz v0, :cond_5

    .line 314
    iget-object v0, p0, Lcyg;->j:Ltv/periscope/android/graphics/o;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/o;->a()V

    .line 315
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->j:Ltv/periscope/android/graphics/o;

    .line 317
    :cond_5
    iget-object v0, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;

    if-eqz v0, :cond_6

    .line 318
    iget-object v0, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/b;->b()V

    .line 319
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;

    .line 321
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->y:Landroid/media/MediaFormat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 322
    monitor-exit p0

    return-void

    .line 289
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d(I)V
    .locals 3

    .prologue
    .line 390
    monitor-enter p0

    .line 391
    :try_start_0
    iget v0, p0, Lcyg;->N:I

    if-ne p1, v0, :cond_1

    .line 392
    monitor-exit p0

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 394
    :cond_1
    iput p1, p0, Lcyg;->N:I

    .line 395
    const-string/jumbo v0, "CameraBroadcaster"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Audio bitrate change to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcyg;->N:I

    div-int/lit16 v2, v2, 0x400

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " kbits/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    iget-object v0, p0, Lcyg;->e:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    .line 398
    invoke-direct {p0}, Lcyg;->m()V

    goto :goto_0

    .line 396
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 372
    iget v0, p0, Lcyg;->K:I

    return v0
.end method

.method public declared-synchronized e(I)V
    .locals 4

    .prologue
    .line 128
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isZoomSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    if-ltz p1, :cond_0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result v1

    if-gt p1, v1, :cond_0

    .line 131
    const-string/jumbo v1, "CameraBroadcaster"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Zoom: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " (max: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iput p1, p0, Lcyg;->O:I

    .line 133
    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    .line 134
    iget-object v1, p0, Lcyg;->a:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :cond_0
    monitor-exit p0

    return-void

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 385
    iget v0, p0, Lcyg;->r:I

    return v0
.end method

.method public declared-synchronized g()V
    .locals 3

    .prologue
    .line 490
    monitor-enter p0

    :try_start_0
    const-string/jumbo v1, "CameraBroadcaster"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "resetVideoEncoder "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lcyg;->J:Z

    if-eqz v0, :cond_4

    const-string/jumbo v0, "Encoding"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    iget-boolean v0, p0, Lcyg;->J:Z

    if-eqz v0, :cond_2

    .line 492
    iget-object v0, p0, Lcyg;->F:Lcyg$d;

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lcyg;->F:Lcyg$d;

    invoke-virtual {v0}, Lcyg$d;->d()V

    .line 494
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->F:Lcyg$d;

    .line 496
    :cond_0
    iget-object v0, p0, Lcyg;->G:Lcyg$g;

    if-eqz v0, :cond_1

    .line 497
    iget-object v0, p0, Lcyg;->G:Lcyg$g;

    invoke-virtual {v0}, Lcyg$g;->d()V

    .line 498
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->G:Lcyg$g;

    .line 500
    :cond_1
    iget-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;

    if-eqz v0, :cond_2

    .line 501
    iget-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 502
    iget-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    .line 503
    const/4 v0, 0x0

    iput-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 508
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcyg;->q()Landroid/media/MediaCodec;

    move-result-object v0

    iput-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;

    .line 509
    iget-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->createInputSurface()Landroid/view/Surface;

    move-result-object v0

    .line 510
    iget-object v1, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;

    invoke-virtual {v1, v0}, Ltv/periscope/android/graphics/b;->a(Landroid/view/Surface;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 515
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcyg;->d:Landroid/media/MediaCodec;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcyg;->J:Z

    if-eqz v0, :cond_3

    .line 516
    new-instance v0, Lcyg$g;

    invoke-direct {v0, p0}, Lcyg$g;-><init>(Lcyg;)V

    iput-object v0, p0, Lcyg;->G:Lcyg$g;

    .line 517
    iget-object v0, p0, Lcyg;->G:Lcyg$g;

    invoke-virtual {v0}, Lcyg$g;->b()V

    .line 518
    iget-object v0, p0, Lcyg;->G:Lcyg$g;

    invoke-virtual {v0}, Lcyg$g;->c()V

    .line 520
    new-instance v0, Lcyg$d;

    invoke-direct {v0, p0}, Lcyg$d;-><init>(Lcyg;)V

    iput-object v0, p0, Lcyg;->F:Lcyg$d;

    .line 521
    iget-object v0, p0, Lcyg;->F:Lcyg$d;

    invoke-virtual {v0}, Lcyg$d;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 523
    :cond_3
    monitor-exit p0

    return-void

    .line 490
    :cond_4
    :try_start_3
    const-string/jumbo v0, "Not encoding"

    goto :goto_0

    .line 511
    :catch_0
    move-exception v0

    .line 512
    invoke-static {v0}, Lf;->a(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 490
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcyg;->q:Z

    .line 405
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 409
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcyg;->q:Z

    .line 411
    invoke-direct {p0}, Lcyg;->m()V

    .line 412
    return-void
.end method

.method public j()I
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcyg;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isZoomSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result v0

    .line 118
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method k()V
    .locals 9

    .prologue
    const v8, 0x2c600

    const/16 v3, 0x238

    const/16 v2, 0x140

    const/4 v0, 0x0

    .line 328
    new-array v7, v8, [I

    .line 329
    invoke-static {v7}, Ljava/nio/IntBuffer;->wrap([I)Ljava/nio/IntBuffer;

    move-result-object v6

    .line 330
    const/16 v4, 0x1908

    const/16 v5, 0x1401

    move v1, v0

    invoke-static/range {v0 .. v6}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 332
    invoke-static {v7}, Lcyk;->a([I)[I

    move-result-object v5

    .line 333
    new-array v6, v8, [I

    move v4, v0

    .line 335
    :goto_0
    if-ge v4, v3, :cond_1

    move v1, v0

    .line 336
    :goto_1
    if-ge v1, v2, :cond_0

    .line 337
    rsub-int v7, v4, 0x238

    add-int/lit8 v7, v7, -0x1

    mul-int/lit16 v7, v7, 0x140

    add-int/2addr v7, v1

    mul-int/lit16 v8, v4, 0x140

    add-int/2addr v8, v1

    aget v8, v5, v8

    aput v8, v6, v7

    .line 336
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 335
    :cond_0
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 341
    :cond_1
    iget-object v0, p0, Lcyg;->M:Landroid/os/Handler;

    new-instance v1, Lcyg$3;

    invoke-direct {v1, p0, v6}, Lcyg$3;-><init>(Lcyg;[I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 347
    return-void
.end method

.method public declared-synchronized onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    .line 623
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 645
    :goto_0
    monitor-exit p0

    return-void

    .line 626
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcyg;->i:Ltv/periscope/android/graphics/b;

    new-instance v1, Lcyg$4;

    invoke-direct {v1, p0, p1}, Lcyg$4;-><init>(Lcyg;Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/graphics/b;->a(Ltv/periscope/android/graphics/b$c;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 623
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
