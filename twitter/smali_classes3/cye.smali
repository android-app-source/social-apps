.class public final Lcye;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final a:Ljava/util/concurrent/Executor;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ltv/periscope/android/library/c;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-interface {p1}, Ltv/periscope/android/library/c;->o()Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Lcye;->a:Ljava/util/concurrent/Executor;

    .line 28
    return-void
.end method

.method static synthetic a(Lcye;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcye;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcye;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcye;->b:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcye;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 38
    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 39
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p1, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 40
    iget-object v1, p0, Lcye;->a:Ljava/util/concurrent/Executor;

    new-instance v2, Lcye$1;

    invoke-direct {v2, p0, v0}, Lcye$1;-><init>(Lcye;Ljava/io/ByteArrayOutputStream;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcye;->b:Ljava/lang/String;

    .line 32
    return-void
.end method
