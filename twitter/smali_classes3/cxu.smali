.class public Lcxu;
.super Lcxg;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcxj;

.field private final c:Ltv/periscope/android/util/i$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ltv/periscope/android/util/i$a",
            "<",
            "Ldcd;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ltv/periscope/android/util/i$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ltv/periscope/android/util/i$a",
            "<",
            "Ldcd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcxu;->a:Ljava/util/Set;

    .line 34
    sget-object v0, Lcxu;->a:Ljava/util/Set;

    const-string/jumbo v1, "tip_viewer_1"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 35
    return-void
.end method

.method public constructor <init>(Lcxm;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcxg;-><init>(Lcxm;)V

    .line 44
    new-instance v0, Lcxu$1;

    invoke-direct {v0, p0}, Lcxu$1;-><init>(Lcxu;)V

    iput-object v0, p0, Lcxu;->c:Ltv/periscope/android/util/i$a;

    .line 50
    new-instance v0, Lcxu$2;

    invoke-direct {v0, p0}, Lcxu$2;-><init>(Lcxu;)V

    iput-object v0, p0, Lcxu;->d:Ltv/periscope/android/util/i$a;

    .line 56
    new-instance v0, Lcxj;

    invoke-direct {v0}, Lcxj;-><init>()V

    iput-object v0, p0, Lcxu;->b:Lcxj;

    .line 57
    return-void
.end method

.method private a(Ltv/periscope/android/util/i$a;)Ldcd;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/android/util/i$a",
            "<",
            "Ldcd;",
            ">;)",
            "Ldcd;"
        }
    .end annotation

    .prologue
    .line 76
    invoke-virtual {p0}, Lcxu;->a()Lcxm;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcxm;->a(I)Ljava/util/List;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcxu;->b:Lcxj;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 79
    invoke-static {v0, p1}, Ltv/periscope/android/util/i;->a(Ljava/util/Collection;Ltv/periscope/android/util/i$a;)Ljava/util/Collection;

    move-result-object v0

    .line 83
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 84
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldcd;

    .line 85
    invoke-virtual {v0}, Ldcd;->d()J

    move-result-wide v4

    sub-long v4, v2, v4

    sget-wide v6, Lcxh;->d:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 89
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcxu;->a:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public a(J)Ldcd;
    .locals 3

    .prologue
    .line 64
    const-wide/16 v0, 0x64

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 65
    const/4 v0, 0x0

    .line 69
    :goto_0
    return-object v0

    .line 66
    :cond_0
    const-wide/16 v0, 0x3

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 67
    iget-object v0, p0, Lcxu;->d:Ltv/periscope/android/util/i$a;

    invoke-direct {p0, v0}, Lcxu;->a(Ltv/periscope/android/util/i$a;)Ldcd;

    move-result-object v0

    goto :goto_0

    .line 69
    :cond_1
    iget-object v0, p0, Lcxu;->c:Ltv/periscope/android/util/i$a;

    invoke-direct {p0, v0}, Lcxu;->a(Ltv/periscope/android/util/i$a;)Ldcd;

    move-result-object v0

    goto :goto_0
.end method
