.class public Lml;
.super Lmq;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmq",
        "<",
        "Lnk;",
        "Lcom/twitter/android/provider/e;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lmu;

.field private final b:Lmn;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lmq;-><init>(Landroid/content/Context;)V

    .line 26
    new-instance v0, Lmu;

    const v1, 0x7f04041a

    invoke-direct {v0, p1, v1}, Lmu;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lml;->a:Lmu;

    .line 27
    new-instance v0, Lmn;

    invoke-direct {v0, p1}, Lmn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lml;->b:Lmn;

    .line 28
    return-void
.end method

.method public static a(ILcom/twitter/android/provider/e;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 109
    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast p1, Lcom/twitter/android/provider/f;

    iget-object v1, p1, Lcom/twitter/android/provider/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 114
    :goto_0
    return-object v0

    .line 111
    :cond_0
    const/4 v0, 0x2

    if-ne p0, v0, :cond_1

    .line 112
    check-cast p1, Lcom/twitter/android/provider/c;

    iget-object v0, p1, Lcom/twitter/android/provider/c;->a:Ljava/lang/String;

    goto :goto_0

    .line 114
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lml;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnk;

    .line 37
    if-eqz v0, :cond_0

    iget v0, v0, Lnk;->b:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/android/provider/e;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lml;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 53
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 45
    :pswitch_0
    iget-object v0, p0, Lml;->a:Lmu;

    check-cast p2, Lcom/twitter/android/provider/f;

    invoke-virtual {v0, p1, p2, p3}, Lmu;->a(Landroid/content/Context;Lcom/twitter/android/provider/f;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 48
    :pswitch_1
    iget-object v0, p0, Lml;->b:Lmn;

    check-cast p2, Lcom/twitter/android/provider/c;

    invoke-virtual {v0, p1, p2, p3}, Lmn;->a(Landroid/content/Context;Lcom/twitter/android/provider/c;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 19
    check-cast p2, Lcom/twitter/android/provider/e;

    invoke-virtual {p0, p1, p2, p3}, Lml;->a(Landroid/content/Context;Lcom/twitter/android/provider/e;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/provider/e;)V
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lml;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 70
    :goto_0
    return-void

    .line 60
    :pswitch_0
    iget-object v0, p0, Lml;->a:Lmu;

    check-cast p3, Lcom/twitter/android/provider/f;

    invoke-virtual {v0, p1, p2, p3}, Lmu;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/provider/f;)V

    goto :goto_0

    .line 64
    :pswitch_1
    iget-object v0, p0, Lml;->b:Lmn;

    check-cast p3, Lcom/twitter/android/provider/c;

    invoke-virtual {v0, p1, p2, p3}, Lmn;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/provider/c;)V

    goto :goto_0

    .line 58
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p3, Lcom/twitter/android/provider/e;

    invoke-virtual {p0, p1, p2, p3}, Lml;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/provider/e;)V

    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lml;->a:Lmu;

    invoke-virtual {v0, p1}, Lmu;->a(Ljava/util/Collection;)V

    .line 32
    return-void
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p0}, Lml;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 84
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    .line 76
    :pswitch_0
    iget-object v0, p0, Lml;->a:Lmu;

    invoke-virtual {v0, p1}, Lmu;->getItemId(I)J

    move-result-wide v0

    goto :goto_0

    .line 79
    :pswitch_1
    iget-object v0, p0, Lml;->b:Lmn;

    invoke-virtual {v0, p1}, Lmn;->getItemId(I)J

    move-result-wide v0

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Lml;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 99
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 91
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 94
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x2

    return v0
.end method
