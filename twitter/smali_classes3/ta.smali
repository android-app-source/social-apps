.class public Lta;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lsy$b;
.implements Lte$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lta$a;
    }
.end annotation


# instance fields
.field protected final a:Ltc;

.field protected final b:Lcom/twitter/library/client/Session;

.field protected final c:Lcom/twitter/util/a;

.field protected d:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<",
            "Lsx;",
            ">;"
        }
    .end annotation
.end field

.field protected e:Lrx/j;

.field protected f:Lsz;

.field protected g:Lta$a;

.field protected h:Lsy;

.field private i:Lte;

.field private j:Z

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ltc;Lcom/twitter/library/client/Session;Lcom/twitter/util/a;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v0

    iput-object v0, p0, Lta;->d:Lcbi;

    .line 59
    iput-object p1, p0, Lta;->a:Ltc;

    .line 60
    iput-object p2, p0, Lta;->b:Lcom/twitter/library/client/Session;

    .line 61
    iput-object p3, p0, Lta;->c:Lcom/twitter/util/a;

    .line 62
    return-void
.end method

.method public static a(Lcom/twitter/util/a;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/a;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    const-string/jumbo v0, "selected_interests"

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/twitter/util/a;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    .line 171
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/y;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 1

    .prologue
    .line 36
    invoke-static {p0}, Lta;->b(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 342
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lta;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lta;->k:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "interest_picker"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    aput-object p2, v1, v2

    .line 343
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 344
    invoke-virtual {v0, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 342
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 345
    return-void
.end method

.method static synthetic a(Lta;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lta;->j()V

    return-void
.end method

.method private a(Ltg;)V
    .locals 3

    .prologue
    .line 321
    iget-object v0, p0, Lta;->a:Ltc;

    invoke-virtual {v0, p1}, Ltc;->a(Lsx;)V

    .line 322
    iget-object v0, p0, Lta;->g:Lta$a;

    const v1, 0x7f0a0427

    invoke-interface {v0, v1}, Lta$a;->setSearchHint(I)V

    .line 323
    const-string/jumbo v0, "search"

    const-string/jumbo v1, "select"

    iget-object v2, p1, Ltg;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lta;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    return-void
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lsx;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lsx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 118
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    .line 119
    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 120
    iget-object v0, v0, Lsx;->d:Ljava/util/List;

    invoke-static {v0}, Lta;->b(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 122
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private h()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lsx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lta;->d:Lcbi;

    invoke-static {v0}, Lrx/c;->a(Ljava/lang/Iterable;)Lrx/c;

    move-result-object v0

    new-instance v1, Lta$2;

    invoke-direct {v1, p0}, Lta$2;-><init>(Lta;)V

    .line 97
    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lta$1;

    invoke-direct {v1, p0}, Lta$1;-><init>(Lta;)V

    .line 103
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 96
    return-object v0
.end method

.method private i()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ltg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0}, Lta;->h()Lrx/c;

    move-result-object v0

    const-class v1, Ltg;

    invoke-virtual {v0, v1}, Lrx/c;->b(Ljava/lang/Class;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lta;->h:Lsy;

    iget-object v1, p0, Lta;->d:Lcbi;

    invoke-virtual {v0, v1}, Lsy;->a(Lcbi;)V

    .line 285
    iget-object v0, p0, Lta;->f:Lsz;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lta;->f:Lsz;

    iget-object v1, p0, Lta;->d:Lcbi;

    invoke-interface {v0, v1}, Lsz;->a(Lcbi;)V

    .line 288
    :cond_0
    iget-object v0, p0, Lta;->g:Lta$a;

    if-eqz v0, :cond_1

    .line 289
    iget-object v1, p0, Lta;->g:Lta$a;

    iget-object v0, p0, Lta;->d:Lcbi;

    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lta$a;->a(Z)V

    .line 291
    :cond_1
    return-void

    .line 289
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    invoke-direct {p0}, Lta;->i()Lrx/c;

    move-result-object v0

    new-instance v1, Lta$5;

    invoke-direct {v1, p0, p1}, Lta$5;-><init>(Lta;I)V

    .line 186
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lta$4;

    invoke-direct {v1, p0, p1}, Lta$4;-><init>(Lta;I)V

    .line 196
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 206
    invoke-virtual {v0}, Lrx/c;->q()Lrx/c;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Lrx/c;->p()Lcwa;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Lcwa;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 185
    return-object v0
.end method

.method public a(Lcom/twitter/model/stratostore/SourceLocation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/stratostore/SourceLocation;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/json/stratostore/JsonInterestSelections$JsonInterestSelection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    invoke-direct {p0}, Lta;->i()Lrx/c;

    move-result-object v0

    new-instance v1, Lta$7;

    invoke-direct {v1, p0}, Lta$7;-><init>(Lta;)V

    .line 218
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v6

    new-instance v0, Lta$6;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lta$6;-><init>(Lta;Lcom/twitter/model/stratostore/SourceLocation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    invoke-virtual {v6, v0}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 240
    invoke-virtual {v0}, Lrx/c;->q()Lrx/c;

    move-result-object v0

    .line 241
    invoke-virtual {v0}, Lrx/c;->p()Lcwa;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Lcwa;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 217
    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 74
    iget-object v1, p0, Lta;->g:Lta$a;

    iget-object v0, p0, Lta;->d:Lcbi;

    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lta$a;->a(Z)V

    .line 75
    iget-boolean v0, p0, Lta;->j:Z

    if-eqz v0, :cond_2

    .line 76
    iget-object v0, p0, Lta;->g:Lta$a;

    const v1, 0x7f0a0425

    invoke-interface {v0, v1}, Lta$a;->setSeamfulSignupHeader(I)V

    .line 80
    :goto_1
    iget-object v0, p0, Lta;->g:Lta$a;

    iget-object v1, p0, Lta;->h:Lsy;

    invoke-interface {v0, v1}, Lta$a;->setAdapterAndAttachHeaders(Lsy;)V

    .line 83
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lta;->a(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lta;->g:Lta$a;

    const v1, 0x7f0a0427

    invoke-interface {v0, v1}, Lta$a;->setSearchHint(I)V

    .line 86
    :cond_0
    return-void

    .line 74
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 78
    :cond_2
    iget-object v0, p0, Lta;->g:Lta$a;

    const v1, 0x7f0a0424

    invoke-interface {v0, v1}, Lta$a;->setSignupHeader(I)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/android/util/CategoryListItem;)V
    .locals 1

    .prologue
    .line 299
    invoke-static {p1}, Ltg;->a(Lcom/twitter/android/util/CategoryListItem;)Ltg;

    move-result-object v0

    .line 300
    invoke-direct {p0, v0}, Lta;->a(Ltg;)V

    .line 301
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 150
    .line 151
    invoke-direct {p0}, Lta;->i()Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->q()Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->p()Lcwa;

    move-result-object v0

    invoke-virtual {v0}, Lcwa;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lsw;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 153
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lta;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 154
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/util/Collection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lta;->k:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "interest_picker"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object p1, v2, v3

    .line 155
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 156
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 153
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 157
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lta;->c:Lcom/twitter/util/a;

    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "selected_interests"

    .line 161
    invoke-static {}, Lcom/twitter/util/y;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 163
    return-void
.end method

.method public a(Lsx;)V
    .locals 4

    .prologue
    .line 329
    instance-of v0, p1, Ltg;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 330
    check-cast v0, Ltg;

    .line 331
    iget v1, v0, Ltg;->g:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-boolean v0, v0, Ltg;->h:Z

    if-nez v0, :cond_0

    .line 332
    const-string/jumbo v0, "search"

    const-string/jumbo v1, "deselect"

    iget-object v2, p1, Lsx;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lta;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :cond_0
    iget-object v0, p0, Lta;->a:Ltc;

    invoke-virtual {v0}, Ltc;->a()V

    .line 339
    :cond_1
    :goto_0
    return-void

    .line 335
    :cond_2
    instance-of v0, p1, Ltf;

    if-eqz v0, :cond_1

    .line 336
    const-string/jumbo v0, "pivot"

    const-string/jumbo v1, "click"

    iget-wide v2, p1, Lsx;->c:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lta;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    iget-object v0, p0, Lta;->g:Lta$a;

    iget-wide v2, p1, Lsx;->c:J

    invoke-interface {v0, v2, v3}, Lta$a;->a(J)V

    goto :goto_0
.end method

.method public a(Lsz;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lta;->f:Lsz;

    .line 70
    invoke-direct {p0}, Lta;->j()V

    .line 71
    return-void
.end method

.method public a(Lta$a;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lta;->g:Lta$a;

    .line 66
    return-void
.end method

.method public a(Lte;)V
    .locals 1

    .prologue
    .line 89
    iput-object p1, p0, Lta;->i:Lte;

    .line 90
    invoke-virtual {p1, p0}, Lte;->a(Lte$a;)V

    .line 91
    iget-object v0, p0, Lta;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lte;->b(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lta;->g:Lta$a;

    invoke-interface {v0, p1}, Lta$a;->setupSearchController(Lte;)V

    .line 93
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 180
    iput-boolean p1, p0, Lta;->j:Z

    .line 181
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 126
    invoke-direct {p0}, Lta;->i()Lrx/c;

    move-result-object v0

    new-instance v1, Lta$3;

    invoke-direct {v1, p0}, Lta$3;-><init>(Lta;)V

    .line 127
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Lrx/c;->h()Lrx/c;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lrx/c;->p()Lcwa;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Lcwa;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 126
    return v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lta;->k:Ljava/lang/String;

    .line 177
    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 304
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    .line 305
    iget-object v2, p0, Lta;->a:Ltc;

    invoke-virtual {v2, v0}, Ltc;->a(Lsx;)V

    goto :goto_0

    .line 307
    :cond_0
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 140
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lta;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lta;->k:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "interest_picker"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "search"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 141
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 140
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 142
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 315
    invoke-static {p1}, Ltg;->a(Ljava/lang/String;)Ltg;

    move-result-object v0

    .line 316
    invoke-direct {p0, v0}, Lta;->a(Ltg;)V

    .line 317
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lta;->g:Lta$a;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lta$a;->a(Z)V

    .line 146
    invoke-virtual {p0}, Lta;->g()V

    .line 147
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 246
    new-instance v0, Lsy;

    invoke-direct {v0, p0}, Lsy;-><init>(Lsy$b;)V

    iput-object v0, p0, Lta;->h:Lsy;

    .line 247
    invoke-virtual {p0}, Lta;->g()V

    .line 248
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 251
    iget-object v0, p0, Lta;->e:Lrx/j;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lta;->e:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 253
    iput-object v1, p0, Lta;->e:Lrx/j;

    .line 256
    :cond_0
    iget-object v0, p0, Lta;->i:Lte;

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lta;->i:Lte;

    invoke-virtual {v0}, Lte;->b()V

    .line 258
    iput-object v1, p0, Lta;->i:Lte;

    .line 260
    :cond_1
    return-void
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lta;->e:Lrx/j;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lta;->e:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 266
    :cond_0
    iget-object v0, p0, Lta;->a:Ltc;

    invoke-virtual {v0}, Ltc;->b()Lrx/c;

    move-result-object v0

    new-instance v1, Lta$8;

    invoke-direct {v1, p0}, Lta$8;-><init>(Lta;)V

    .line 267
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lta;->e:Lrx/j;

    .line 281
    return-void
.end method
