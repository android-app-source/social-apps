.class public Lcxk;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(Landroid/content/Context;Landroid/content/SharedPreferences;Lcyw;)Lcxl;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 19
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 20
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 21
    const-string/jumbo v2, "tv.periscope.android.dev"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "tv.periscope.android.beta"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string/jumbo v0, "pref_profile_is_employee"

    .line 22
    invoke-interface {v1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "pref_force_broadcast_tips"

    .line 23
    invoke-interface {v1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24
    new-instance v0, Lcxs;

    invoke-direct {v0}, Lcxs;-><init>()V

    .line 26
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p2}, Lcyw;->f()Ltv/periscope/model/user/f;

    move-result-object v0

    invoke-static {p1, v0}, Lcxt;->a(Landroid/content/SharedPreferences;Ltv/periscope/model/user/f;)Lcxl;

    move-result-object v0

    goto :goto_0
.end method
