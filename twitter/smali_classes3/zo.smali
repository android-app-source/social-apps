.class public Lzo;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/fullscreen/ak;

.field private final b:Lzn;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/ak;Lzn;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lzo;->a:Lcom/twitter/android/moments/ui/fullscreen/ak;

    .line 17
    iput-object p2, p0, Lzo;->b:Lzn;

    .line 18
    iget-object v0, p0, Lzo;->a:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ak;->c()Lcom/twitter/util/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 19
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lzo;->a:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ak;->c()Lcom/twitter/util/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 32
    return-void
.end method

.method public onEvent(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 23
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lzo;->b:Lzn;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lzn;->a(I)V

    .line 28
    :goto_0
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Lzo;->b:Lzn;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lzn;->a(I)V

    goto :goto_0
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lzo;->onEvent(Ljava/lang/Boolean;)V

    return-void
.end method
