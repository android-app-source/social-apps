.class public Lcpg;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcpg$b;,
        Lcpg$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcpg$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcpg$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field private static d:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lcpg;->a:Ljava/util/List;

    .line 21
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lcpg;->b:Ljava/util/List;

    .line 23
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    const-class v1, Ljava/lang/OutOfMemoryError;

    .line 24
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcpg;->c:Ljava/util/List;

    .line 23
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method private static a(Lcpf;Z)V
    .locals 1

    .prologue
    .line 120
    if-eqz p1, :cond_0

    .line 122
    :try_start_0
    invoke-static {p0}, Lcpd;->d(Lcpb;)V

    .line 130
    :goto_0
    return-void

    .line 124
    :cond_0
    invoke-static {p0}, Lcpd;->c(Lcpb;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    goto :goto_0

    .line 127
    :catch_1
    move-exception v0

    .line 128
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Lcpg$a;)V
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcpg;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    const-class v0, Lcpg;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 52
    return-void
.end method

.method public static a(Lcpg$b;)V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcpg;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    const-class v0, Lcpg;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 47
    return-void
.end method

.method public static a(Ljava/lang/Class;)V
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcpg;->c:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    const-class v0, Lcpg;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 42
    return-void
.end method

.method public static a(Ljava/lang/OutOfMemoryError;)V
    .locals 0

    .prologue
    .line 58
    invoke-static {p0}, Lcpg;->a(Ljava/lang/Throwable;)Z

    .line 59
    return-void
.end method

.method public static declared-synchronized a(Ljava/lang/OutOfMemoryError;ZLjava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/OutOfMemoryError;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    const-class v0, Lcpg;

    monitor-enter v0

    :try_start_0
    invoke-static {p0, p1, p2}, Lcpg;->a(Ljava/lang/Throwable;ZLjava/util/Map;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    monitor-exit v0

    return-void

    .line 73
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static a()Z
    .locals 8

    .prologue
    .line 174
    invoke-static {}, Lcom/twitter/util/i;->b()J

    move-result-wide v2

    .line 176
    const-class v1, Lcpg;

    monitor-enter v1

    .line 177
    :try_start_0
    sget-wide v4, Lcpg;->d:J

    sub-long v4, v2, v4

    const-wide/32 v6, 0xea60

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 178
    :goto_0
    if-eqz v0, :cond_0

    .line 179
    sput-wide v2, Lcpg;->d:J

    .line 180
    const-class v2, Lcpg;

    invoke-static {v2}, Lcru;->a(Ljava/lang/Class;)V

    .line 182
    :cond_0
    monitor-exit v1

    .line 183
    return v0

    .line 177
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcpg;->a(Ljava/lang/Throwable;Z)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/Throwable;Z)Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcpg;->a(Ljava/lang/Throwable;ZLjava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized a(Ljava/lang/Throwable;ZLjava/util/Map;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 98
    const-class v1, Lcpg;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcpg;->b(Ljava/lang/Throwable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 99
    const/4 v0, 0x0

    .line 115
    :goto_0
    monitor-exit v1

    return v0

    .line 102
    :cond_0
    :try_start_1
    sget-object v0, Lcpg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpg$b;

    .line 103
    invoke-interface {v0, p0}, Lcpg$b;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 106
    :cond_1
    :try_start_2
    invoke-static {}, Lcpg;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107
    invoke-static {p0, p1, p2}, Lcpg;->b(Ljava/lang/Throwable;ZLjava/util/Map;)Lcpf;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_3

    .line 109
    invoke-static {v0, p1}, Lcpg;->a(Lcpf;Z)V

    .line 115
    :cond_2
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 112
    :cond_3
    invoke-static {p0}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private static b(Ljava/lang/Throwable;ZLjava/util/Map;)Lcpf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcpf;"
        }
    .end annotation

    .prologue
    .line 154
    const/4 v0, 0x0

    .line 156
    :try_start_0
    new-instance v2, Lcpf;

    invoke-direct {v2, p0}, Lcpf;-><init>(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 157
    :try_start_1
    sget-object v0, Lcpg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpg$a;

    .line 158
    invoke-interface {v0, v2, p1}, Lcpg$a;->a(Lcpf;Z)V

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    move-object v0, v2

    .line 170
    :goto_1
    return-object v0

    .line 160
    :cond_0
    if-eqz p2, :cond_1

    .line 161
    invoke-virtual {v2}, Lcpf;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 162
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcpf;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 167
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    .line 168
    :goto_3
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 169
    goto :goto_1

    .line 167
    :catch_2
    move-exception v1

    goto :goto_3

    .line 165
    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method private static b(Ljava/lang/Class;)Z
    .locals 2

    .prologue
    .line 143
    sget-object v0, Lcpg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 144
    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const/4 v0, 0x1

    .line 148
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 133
    :goto_0
    if-eqz p0, :cond_1

    .line 134
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcpg;->b(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    const/4 v0, 0x1

    .line 139
    :goto_1
    return v0

    .line 137
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0

    .line 139
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
