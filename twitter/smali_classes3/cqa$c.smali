.class Lcqa$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcqa$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcqa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/util/collection/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/f",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 508
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcqa$c;->a:Ljava/util/Map;

    .line 510
    new-instance v0, Lcom/twitter/util/collection/f;

    invoke-direct {v0}, Lcom/twitter/util/collection/f;-><init>()V

    iput-object v0, p0, Lcqa$c;->b:Lcom/twitter/util/collection/f;

    return-void
.end method

.method synthetic constructor <init>(Lcqa$1;)V
    .locals 0

    .prologue
    .line 507
    invoke-direct {p0}, Lcqa$c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcqa$c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 527
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lcqa$c;->a:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    iget-object v0, p0, Lcqa$c;->b:Lcom/twitter/util/collection/f;

    invoke-virtual {v0, p2, p1}, Lcom/twitter/util/collection/f;->a(ILjava/lang/Object;)Ljava/lang/Object;

    .line 516
    return-void
.end method
