.class public Lsa;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lsb;

.field private final c:Landroid/support/v4/app/FragmentManager;

.field private final d:Lcom/twitter/android/composer/o;

.field private final e:Landroid/widget/ImageButton;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/ImageView;

.field private i:Lcom/twitter/library/client/Session;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lsb;Landroid/support/v4/app/FragmentManager;Landroid/widget/ImageButton;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Lcom/twitter/android/composer/o;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lsa;->a:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lsa;->b:Lsb;

    .line 57
    iput-object p3, p0, Lsa;->c:Landroid/support/v4/app/FragmentManager;

    .line 58
    iput-object p8, p0, Lsa;->d:Lcom/twitter/android/composer/o;

    .line 59
    iput-object p4, p0, Lsa;->e:Landroid/widget/ImageButton;

    .line 60
    iput-object p5, p0, Lsa;->f:Landroid/view/View;

    .line 61
    iput-object p6, p0, Lsa;->g:Landroid/widget/TextView;

    .line 62
    iput-object p7, p0, Lsa;->h:Landroid/widget/ImageView;

    .line 64
    iget-object v0, p0, Lsa;->e:Landroid/widget/ImageButton;

    iget-object v1, p0, Lsa;->b:Lsb;

    new-instance v2, Lsa$1;

    invoke-direct {v2, p0}, Lsa$1;-><init>(Lsa;)V

    invoke-interface {v1, v2}, Lsb;->a(Ljava/lang/Runnable;)Lcom/twitter/android/composer/ComposerActivity$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    if-eqz p9, :cond_0

    .line 72
    invoke-direct {p0, p9}, Lsa;->b(Landroid/os/Bundle;)V

    .line 74
    :cond_0
    return-void
.end method

.method static synthetic a(Lsa;)Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lsa;->j:Z

    return v0
.end method

.method static synthetic b(Lsa;)Lcom/twitter/android/composer/o;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lsa;->d:Lcom/twitter/android/composer/o;

    return-object v0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 153
    const-string/jumbo v0, "bundle_lifeline_module"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_0

    .line 155
    const-string/jumbo v1, "lifeline_alert"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lsa;->j:Z

    .line 157
    :cond_0
    return-void
.end method

.method static synthetic c(Lsa;)Lsb;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lsa;->b:Lsb;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lsa;->d:Lcom/twitter/android/composer/o;

    invoke-virtual {v0}, Lcom/twitter/android/composer/o;->h()V

    .line 124
    new-instance v1, Lsa$2;

    invoke-direct {v1, p0}, Lsa$2;-><init>(Lsa;)V

    .line 135
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, p1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v2, 0x7f0a0449

    .line 136
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a0448

    .line 137
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a0447

    .line 138
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a00f6

    .line 139
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const/4 v2, 0x0

    .line 140
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->a(Z)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 141
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 142
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 143
    iget-object v1, p0, Lsa;->c:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 144
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 147
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 148
    const-string/jumbo v1, "lifeline_alert"

    iget-boolean v2, p0, Lsa;->j:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 149
    const-string/jumbo v1, "bundle_lifeline_module"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 150
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-object v0, p0, Lsa;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    :goto_0
    return-void

    .line 84
    :cond_0
    iput-object p1, p0, Lsa;->i:Lcom/twitter/library/client/Session;

    .line 85
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/twitter/model/core/TwitterUser;->o:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "legacy_deciders_lifeline_alerts_enabled"

    .line 87
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    iget-boolean v0, p0, Lsa;->j:Z

    invoke-virtual {p0, v0}, Lsa;->a(Z)V

    .line 89
    iget-object v0, p0, Lsa;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 91
    :cond_1
    invoke-virtual {p0, v1}, Lsa;->a(Z)V

    .line 92
    iget-object v0, p0, Lsa;->e:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/model/core/TwitterUser;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 97
    iget-object v0, p2, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    .line 99
    if-eqz p1, :cond_0

    .line 100
    iget-object v1, p0, Lsa;->a:Landroid/content/Context;

    const v2, 0x7f0a044b

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    aput-object p1, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 104
    :goto_0
    iget-object v1, p0, Lsa;->f:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 105
    iget-object v1, p0, Lsa;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lsa;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lsa;->h:Landroid/widget/ImageView;

    const v1, 0x7f020809

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 108
    iget-object v0, p0, Lsa;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 109
    return-void

    .line 102
    :cond_0
    iget-object v1, p0, Lsa;->a:Landroid/content/Context;

    const v2, 0x7f0a044a

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method a(Z)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 112
    iput-boolean p1, p0, Lsa;->j:Z

    .line 113
    if-eqz p1, :cond_0

    .line 114
    iget-object v0, p0, Lsa;->d:Lcom/twitter/android/composer/o;

    invoke-virtual {v0}, Lcom/twitter/android/composer/o;->g()V

    .line 115
    iget-object v0, p0, Lsa;->e:Landroid/widget/ImageButton;

    const v1, 0x7f02036c

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 119
    :goto_0
    iget-object v0, p0, Lsa;->b:Lsb;

    iget-boolean v1, p0, Lsa;->j:Z

    invoke-interface {v0, v1}, Lsb;->a(Z)V

    .line 120
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lsa;->e:Landroid/widget/ImageButton;

    const v1, 0x7f02036b

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lsa;->j:Z

    return v0
.end method
