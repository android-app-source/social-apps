.class public Lwq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;
.implements Lxm;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lwq$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Ljava/lang/Long;",
        "Lcom/twitter/util/collection/m",
        "<",
        "Lcom/twitter/model/moments/viewmodels/b;",
        "Lcom/twitter/model/moments/b;",
        ">;>;",
        "Lxm;"
    }
.end annotation


# instance fields
.field private final a:Lwq$a;

.field private final b:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lwq$a;Lxv;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lwq;->a:Lwq$a;

    .line 37
    iput-object p2, p0, Lwq;->b:Lauj;

    .line 38
    return-void
.end method

.method static synthetic a(Lwq;)Lauj;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lwq;->b:Lauj;

    return-object v0
.end method

.method private b(J)Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/b;",
            ">;",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 56
    new-instance v0, Lwq$1;

    invoke-direct {v0, p0, p1, p2}, Lwq$1;-><init>(Lwq;J)V

    return-object v0
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lwq;->a(Ljava/lang/Long;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Long;)Lrx/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lwq;->a:Lwq$a;

    invoke-virtual {v0, p1}, Lwq$a;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 44
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lwq;->b(J)Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 43
    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lwq;->a(Ljava/lang/Long;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lwq;->a:Lwq$a;

    invoke-virtual {v0}, Lwq$a;->close()V

    .line 75
    iget-object v0, p0, Lwq;->b:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 76
    return-void
.end method
