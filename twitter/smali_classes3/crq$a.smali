.class Lcrq$a;
.super Landroid/telephony/PhoneStateListener;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcrq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcrq;


# direct methods
.method private constructor <init>(Lcrq;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcrq$a;->a:Lcrq;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcrq;Lcrq$1;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcrq$a;-><init>(Lcrq;)V

    return-void
.end method


# virtual methods
.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcrq$a;->a:Lcrq;

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v1

    invoke-static {v0, v1}, Lcrq;->a(Lcrq;I)I

    .line 100
    :goto_0
    iget-object v0, p0, Lcrq$a;->a:Lcrq;

    iget-object v1, p0, Lcrq$a;->a:Lcrq;

    invoke-static {v1, p1}, Lcrq;->a(Lcrq;Landroid/telephony/SignalStrength;)I

    move-result v1

    invoke-static {v0, v1}, Lcrq;->b(Lcrq;I)I

    .line 101
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcrq$a;->a:Lcrq;

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v1

    invoke-static {v0, v1}, Lcrq;->a(Lcrq;I)I

    goto :goto_0
.end method
