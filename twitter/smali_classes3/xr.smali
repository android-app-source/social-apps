.class public Lxr;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lxm;


# instance fields
.field private final a:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lwo;Lxv;Lwq;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lxr;->a:Lauj;

    .line 41
    iput-object p2, p0, Lxr;->b:Lauj;

    .line 42
    iput-object p3, p0, Lxr;->c:Lauj;

    .line 43
    return-void
.end method

.method static synthetic a(Lxr;Z)Lauj;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lxr;->a(Z)Lauj;

    move-result-object v0

    return-object v0
.end method

.method private a(Z)Lauj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 59
    if-eqz p1, :cond_0

    iget-object v0, p0, Lxr;->b:Lauj;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lxr;->c:Lauj;

    goto :goto_0
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lxr;->a:Lauj;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lxr$1;

    invoke-direct {v1, p0, p1, p2}, Lxr$1;-><init>(Lxr;J)V

    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    .line 55
    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    .line 48
    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lxr;->a:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 65
    iget-object v0, p0, Lxr;->b:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 66
    iget-object v0, p0, Lxr;->c:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 67
    return-void
.end method
