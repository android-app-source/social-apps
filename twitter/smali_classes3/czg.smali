.class public Lczg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/google/android/exoplayer/SampleSource;
.implements Lcom/google/android/exoplayer/SampleSource$SampleSourceReader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lczg$b;,
        Lczg$a;
    }
.end annotation


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:I

.field private c:Lcom/google/android/exoplayer/MediaFormat;

.field private d:D

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lczg$b;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lczg$a;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lczg$a;)V
    .locals 6

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lczg;->d:D

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lczg;->e:Ljava/util/ArrayList;

    .line 56
    iput p1, p0, Lczg;->b:I

    .line 57
    iput-object p2, p0, Lczg;->a:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lczg;->f:Lczg$a;

    .line 60
    iget v0, p0, Lczg;->b:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lczg;->a:Ljava/lang/String;

    const/4 v2, -0x1

    const-wide/16 v4, -0x1

    invoke-static {v0, v1, v2, v4, v5}, Lcom/google/android/exoplayer/MediaFormat;->createFormatForMimeType(Ljava/lang/String;Ljava/lang/String;IJ)Lcom/google/android/exoplayer/MediaFormat;

    move-result-object v0

    iput-object v0, p0, Lczg;->c:Lcom/google/android/exoplayer/MediaFormat;

    .line 61
    return-void
.end method


# virtual methods
.method protected a()Lczg$b;
    .locals 3

    .prologue
    .line 142
    iget-object v1, p0, Lczg;->e:Ljava/util/ArrayList;

    monitor-enter v1

    .line 143
    :try_start_0
    iget-object v0, p0, Lczg;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 144
    iget-object v0, p0, Lczg;->e:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczg$b;

    monitor-exit v1

    .line 146
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a([BD)V
    .locals 6

    .prologue
    .line 64
    new-instance v0, Lczg$b;

    invoke-direct {v0, p0, p1, p2, p3}, Lczg$b;-><init>(Lczg;[BD)V

    .line 65
    iget-object v1, p0, Lczg;->e:Ljava/util/ArrayList;

    monitor-enter v1

    .line 66
    :try_start_0
    iget-object v2, p0, Lczg;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    invoke-virtual {v0}, Lczg$b;->a()D

    move-result-wide v2

    iget-wide v4, p0, Lczg;->d:D

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 69
    invoke-virtual {v0}, Lczg$b;->a()D

    move-result-wide v2

    iput-wide v2, p0, Lczg;->d:D

    .line 71
    :cond_0
    monitor-exit v1

    .line 72
    return-void

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public continueBuffering(IJ)Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lczg;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public disable(I)V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method public enable(IJ)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public getBufferedPositionUs()J
    .locals 6

    .prologue
    const-wide v4, 0x408f400000000000L    # 1000.0

    .line 127
    iget-object v1, p0, Lczg;->e:Ljava/util/ArrayList;

    monitor-enter v1

    .line 128
    :try_start_0
    iget-wide v2, p0, Lczg;->d:D

    mul-double/2addr v2, v4

    mul-double/2addr v2, v4

    double-to-long v2, v2

    monitor-exit v1

    return-wide v2

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getFormat(I)Lcom/google/android/exoplayer/MediaFormat;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lczg;->c:Lcom/google/android/exoplayer/MediaFormat;

    return-object v0
.end method

.method public getTrackCount()I
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x1

    return v0
.end method

.method public maybeThrowError()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    return-void
.end method

.method public prepare(J)Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method

.method public readData(IJLcom/google/android/exoplayer/MediaFormatHolder;Lcom/google/android/exoplayer/SampleHolder;)I
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public readDiscontinuity(I)J
    .locals 2

    .prologue
    .line 112
    const-wide/high16 v0, -0x8000000000000000L

    return-wide v0
.end method

.method public register()Lcom/google/android/exoplayer/SampleSource$SampleSourceReader;
    .locals 0

    .prologue
    .line 152
    return-object p0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lczg;->f:Lczg$a;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lczg;->f:Lczg$a;

    invoke-interface {v0}, Lczg$a;->d()V

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lczg;->f:Lczg$a;

    .line 139
    :cond_0
    return-void
.end method

.method public seekToUs(J)V
    .locals 0

    .prologue
    .line 123
    return-void
.end method
