.class Lcrc$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcrc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/c$a",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v4/content/LocalBroadcastManager;

.field private final b:Landroid/content/IntentFilter;


# direct methods
.method constructor <init>(Landroid/support/v4/content/LocalBroadcastManager;Landroid/content/IntentFilter;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcrc$a;->a:Landroid/support/v4/content/LocalBroadcastManager;

    .line 49
    iput-object p2, p0, Lcrc$a;->b:Landroid/content/IntentFilter;

    .line 50
    return-void
.end method

.method static synthetic a(Lcrc$a;)Landroid/support/v4/content/LocalBroadcastManager;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcrc$a;->a:Landroid/support/v4/content/LocalBroadcastManager;

    return-object v0
.end method


# virtual methods
.method public a(Lrx/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-",
            "Landroid/content/Intent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lcrc$a$1;

    invoke-direct {v0, p0, p1}, Lcrc$a$1;-><init>(Lcrc$a;Lrx/i;)V

    .line 60
    new-instance v1, Lcrc$a$2;

    invoke-direct {v1, p0, v0}, Lcrc$a$2;-><init>(Lcrc$a;Landroid/content/BroadcastReceiver;)V

    invoke-static {v1}, Lcwy;->a(Lrx/functions/a;)Lrx/j;

    move-result-object v1

    invoke-virtual {p1, v1}, Lrx/i;->a(Lrx/j;)V

    .line 66
    iget-object v1, p0, Lcrc$a;->a:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v2, p0, Lcrc$a;->b:Landroid/content/IntentFilter;

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 67
    return-void
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 41
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lcrc$a;->a(Lrx/i;)V

    return-void
.end method
