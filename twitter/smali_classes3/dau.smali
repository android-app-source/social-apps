.class public Ldau;
.super Ldaj;
.source "Twttr"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ldaj;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    .line 12
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 21
    sget v0, Ltv/periscope/android/library/f$f;->ps__ic_file_download:I

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ldau;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/h;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    sget v0, Ltv/periscope/android/library/f$l;->ps__action_sheet_saved_to_gallery:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 34
    :goto_0
    return-object v0

    :cond_0
    sget v0, Ltv/periscope/android/library/f$l;->ps__action_sheet_save_to_gallery:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 26
    sget v0, Ltv/periscope/android/library/f$d;->ps__bg_blue:I

    return v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Ldau;->b:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/h;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Ldau;->b:Ltv/periscope/android/ui/broadcast/h;

    iget-object v1, p0, Ldau;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/h;->e(Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x1

    .line 51
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
