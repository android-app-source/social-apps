.class public Lsy;
.super Lbac;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsy$a;,
        Lsy$d;,
        Lsy$c;,
        Lsy$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbac",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lsy$b;

.field private b:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<",
            "Lsx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lsy$b;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lbac;-><init>()V

    .line 33
    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v0

    iput-object v0, p0, Lsy;->b:Lcbi;

    .line 36
    iput-object p1, p0, Lsy;->a:Lsy$b;

    .line 37
    return-void
.end method

.method private static a(Lsx;)I
    .locals 1

    .prologue
    .line 165
    instance-of v0, p0, Ltf;

    if-eqz v0, :cond_0

    .line 166
    const/4 v0, 0x1

    .line 168
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lsy;)Lsy$b;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lsy;->a:Lsy$b;

    return-object v0
.end method

.method static synthetic a(Ltg;Lcom/twitter/internal/android/widget/PillToggleButton;)V
    .locals 0

    .prologue
    .line 25
    invoke-static {p0, p1}, Lsy;->b(Ltg;Lcom/twitter/internal/android/widget/PillToggleButton;)V

    return-void
.end method

.method private static b(Lcbi;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lsx;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/util/collection/q",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 75
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 76
    invoke-virtual {p0}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    .line 77
    invoke-static {v0}, Lsw;->a(Lsx;)Lcom/twitter/util/collection/q;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 79
    :cond_0
    return-object v1
.end method

.method private static b(Ltg;Lcom/twitter/internal/android/widget/PillToggleButton;)V
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Ltg;->h:Z

    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {p1}, Lcom/twitter/internal/android/widget/PillToggleButton;->a()V

    .line 152
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-boolean v0, p0, Ltg;->j:Z

    if-eqz v0, :cond_1

    .line 148
    invoke-virtual {p1}, Lcom/twitter/internal/android/widget/PillToggleButton;->b()V

    goto :goto_0

    .line 150
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/internal/android/widget/PillToggleButton;->c()V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lsy;->b:Lcbi;

    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v0

    return v0
.end method

.method protected a(I)I
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0, p1}, Lsy;->b(I)Lsx;

    move-result-object v0

    invoke-static {v0}, Lsy;->a(Lsx;)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 86
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 87
    const v1, 0x7f04016e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 88
    new-instance v0, Lsy$d;

    invoke-direct {v0, v1}, Lsy$d;-><init>(Landroid/view/View;)V

    .line 92
    :goto_0
    return-object v0

    .line 90
    :cond_0
    new-instance v1, Lcom/twitter/internal/android/widget/PillToggleButton;

    new-instance v0, Landroid/view/ContextThemeWrapper;

    .line 91
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d02d7

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v1, v0}, Lcom/twitter/internal/android/widget/PillToggleButton;-><init>(Landroid/content/Context;)V

    .line 92
    new-instance v0, Lsy$c;

    invoke-direct {v0, v1}, Lsy$c;-><init>(Lcom/twitter/internal/android/widget/PillToggleButton;)V

    goto :goto_0
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 3

    .prologue
    .line 98
    invoke-virtual {p0, p2}, Lsy;->b(I)Lsx;

    move-result-object v0

    .line 99
    if-nez v0, :cond_0

    .line 142
    :goto_0
    return-void

    .line 103
    :cond_0
    invoke-static {v0}, Lsy;->a(Lsx;)I

    move-result v1

    .line 105
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 107
    :pswitch_0
    check-cast v0, Ltg;

    .line 108
    check-cast p1, Lsy$c;

    iget-object v1, p1, Lsy$c;->a:Lcom/twitter/internal/android/widget/PillToggleButton;

    .line 109
    iget-object v2, v0, Ltg;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/PillToggleButton;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-boolean v2, v0, Ltg;->h:Z

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/PillToggleButton;->setChecked(Z)V

    .line 111
    invoke-static {v0, v1}, Lsy;->b(Ltg;Lcom/twitter/internal/android/widget/PillToggleButton;)V

    .line 112
    new-instance v2, Lsy$1;

    invoke-direct {v2, p0, v1, v0}, Lsy$1;-><init>(Lsy;Lcom/twitter/internal/android/widget/PillToggleButton;Ltg;)V

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/PillToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 126
    :pswitch_1
    check-cast p1, Lsy$d;

    .line 127
    iget-object v1, p1, Lsy$d;->a:Lcom/twitter/ui/widget/TwitterButton;

    new-instance v2, Lsy$2;

    invoke-direct {v2, p0, v0}, Lsy$2;-><init>(Lsy;Lsx;)V

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcbi;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lsx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v2, p0, Lsy;->b:Lcbi;

    .line 48
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 50
    invoke-static {p1}, Lsy;->b(Lcbi;)Ljava/util/Set;

    move-result-object v3

    .line 51
    invoke-virtual {v2}, Lcbi;->be_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 52
    invoke-virtual {v2, v1}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    .line 54
    invoke-static {v0}, Lsw;->a(Lsx;)Lcom/twitter/util/collection/q;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p0, v1}, Lsy;->d(I)V

    .line 51
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 60
    :cond_1
    invoke-static {v2}, Lsy;->b(Lcbi;)Ljava/util/Set;

    move-result-object v2

    .line 61
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p1}, Lcbi;->be_()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 62
    invoke-virtual {p1, v1}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    .line 64
    invoke-static {v0}, Lsw;->a(Lsx;)Lcom/twitter/util/collection/q;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 65
    invoke-virtual {p0, v1}, Lsy;->c(I)V

    .line 61
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 68
    :cond_3
    iput-object p1, p0, Lsy;->b:Lcbi;

    .line 70
    :cond_4
    return-void
.end method

.method public b(I)Lsx;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lsy;->b:Lcbi;

    invoke-virtual {v0, p1}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    return-object v0
.end method
