.class Lpa$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lpa$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:I

.field private final b:Lpg;


# direct methods
.method constructor <init>(I)V
    .locals 1

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput p1, p0, Lpa$a;->a:I

    .line 191
    new-instance v0, Lpg;

    invoke-direct {v0}, Lpg;-><init>()V

    iput-object v0, p0, Lpa$a;->b:Lpg;

    .line 192
    return-void
.end method

.method private b(Lcom/twitter/library/av/m;)Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p1, Lcom/twitter/library/av/m;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lpa$a;->b:Lpg;

    invoke-virtual {v0}, Lpg;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/twitter/library/av/m;)V
    .locals 2

    .prologue
    .line 196
    invoke-direct {p0, p1}, Lpa$a;->b(Lcom/twitter/library/av/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/twitter/library/av/m;->j:I

    iget v1, p0, Lpa$a;->a:I

    if-lt v0, v1, :cond_0

    .line 197
    iget-object v0, p0, Lpa$a;->b:Lpg;

    invoke-virtual {v0}, Lpg;->a()V

    .line 199
    :cond_0
    return-void
.end method
