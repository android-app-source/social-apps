.class public Lcyj;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field a:F

.field private final b:Lcyi;

.field private c:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# direct methods
.method public constructor <init>(Lcyi;)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput v0, p0, Lcyj;->a:F

    .line 13
    iput-object p1, p0, Lcyj;->b:Lcyi;

    .line 14
    return-void
.end method

.method static synthetic a(Lcyj;)Lcyi;
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lcyj;->b:Lcyi;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcyj;->c:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    if-nez v0, :cond_0

    .line 19
    new-instance v0, Lcyj$1;

    invoke-direct {v0, p0}, Lcyj$1;-><init>(Lcyj;)V

    iput-object v0, p0, Lcyj;->c:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    .line 40
    :cond_0
    iget-object v0, p0, Lcyj;->c:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    return-object v0
.end method

.method a(Lcyf;F)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 50
    iget v0, p0, Lcyj;->a:F

    add-float/2addr v0, p2

    iput v0, p0, Lcyj;->a:F

    .line 51
    const/4 v0, 0x0

    iget v1, p0, Lcyj;->a:F

    sub-float/2addr v1, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcyj;->a:F

    .line 52
    iget v0, p0, Lcyj;->a:F

    invoke-interface {p1}, Lcyf;->j()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-interface {p1, v0}, Lcyf;->e(I)V

    .line 53
    return-void
.end method
