.class public Lxt;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laun;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laun",
        "<",
        "Ljava/util/Collection",
        "<",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Long;",
        "Lcep;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lxx;

.field private final b:Lyd;


# direct methods
.method public constructor <init>(Lxx;Lyd;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lxt;->a:Lxx;

    .line 38
    iput-object p2, p0, Lxt;->b:Lyd;

    .line 39
    return-void
.end method

.method static synthetic a(Lxt;Ljava/util/Collection;)Lrx/g;
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lxt;->c(Ljava/util/Collection;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lxt;)Lxx;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lxt;->a:Lxx;

    return-object v0
.end method

.method private b(Ljava/util/Collection;)Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/functions/d",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;",
            "Lrx/g",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, Lxt$1;

    invoke-direct {v0, p0, p1}, Lxt$1;-><init>(Lxt;Ljava/util/Collection;)V

    return-object v0
.end method

.method private c(Ljava/util/Collection;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/g",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lxt;->b:Lyd;

    invoke-virtual {v0, p1}, Lyd;->a(Ljava/util/Collection;)Lrx/g;

    move-result-object v0

    new-instance v1, Lxt$2;

    invoke-direct {v1, p0}, Lxt$2;-><init>(Lxt;)V

    .line 68
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 67
    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Lrx/g;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lxt;->a(Ljava/util/Collection;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Collection;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/g",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lxt;->a:Lxx;

    invoke-virtual {v0, p1}, Lxx;->a(Ljava/util/Collection;)Lrx/g;

    move-result-object v0

    .line 45
    invoke-direct {p0, p1}, Lxt;->b(Ljava/util/Collection;)Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 44
    return-object v0
.end method
