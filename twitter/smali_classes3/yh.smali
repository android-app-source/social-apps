.class public Lyh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lyg;


# instance fields
.field private final a:Laun;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laun",
            "<",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/android/moments/data/t;

.field private final c:Lyf;


# direct methods
.method public constructor <init>(Laun;Lcom/twitter/android/moments/data/t;Lyf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laun",
            "<",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;",
            "Lcom/twitter/android/moments/data/t;",
            "Lyf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lyh;->a:Laun;

    .line 42
    iput-object p2, p0, Lyh;->b:Lcom/twitter/android/moments/data/t;

    .line 43
    iput-object p3, p0, Lyh;->c:Lyf;

    .line 44
    return-void
.end method

.method private a(Lcep;Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/moments/viewmodels/g;
    .locals 8

    .prologue
    .line 73
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v6

    .line 74
    invoke-virtual {p1}, Lcep;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lceo;

    .line 77
    new-instance v0, Lcom/twitter/model/moments/Moment$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/Moment$a;-><init>()V

    const-wide/16 v2, 0xb

    .line 78
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/moments/Moment$a;->a(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    const-string/jumbo v1, "ANDROID-19519 fix it now!!"

    .line 79
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    const-string/jumbo v1, "Please fix me."

    .line 80
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->f(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/twitter/model/moments/Moment$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/moments/Moment;

    .line 82
    invoke-static {v5}, Lcom/twitter/model/moments/r$a;->a(Lceo;)Lcom/twitter/model/moments/r$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/r$a;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/moments/r;

    .line 83
    iget-object v0, p0, Lyh;->b:Lcom/twitter/android/moments/data/t;

    sget-object v4, Lcom/twitter/model/moments/MomentPageDisplayMode;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/moments/data/t;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/MomentPageDisplayMode;Lceo;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 86
    :cond_0
    new-instance v1, Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {v6}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {v1, v0}, Lcom/twitter/model/moments/viewmodels/g;-><init>(Ljava/util/List;)V

    return-object v1
.end method

.method static synthetic a(Lyh;Lcep;Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/moments/viewmodels/g;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lyh;->a(Lcep;Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/moments/viewmodels/g;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/Collection;)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/g",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lyh;->c:Lyf;

    .line 50
    invoke-virtual {v0, p1}, Lyf;->a(Ljava/lang/Iterable;)Lrx/c;

    move-result-object v0

    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v1

    .line 49
    invoke-static {v0, v1}, Lcre;->a(Lrx/c;Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lyh;->a:Laun;

    invoke-interface {v1, p1}, Laun;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v1

    .line 52
    new-instance v2, Lyh$1;

    invoke-direct {v2, p0}, Lyh$1;-><init>(Lyh;)V

    invoke-static {v0, v1, v2}, Lrx/g;->a(Lrx/g;Lrx/g;Lrx/functions/e;)Lrx/g;

    move-result-object v0

    return-object v0
.end method
