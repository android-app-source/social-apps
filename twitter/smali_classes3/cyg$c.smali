.class Lcyg$c;
.super Lcyg$e;
.source "Twttr"

# interfaces
.implements Landroid/media/AudioRecord$OnRecordPositionUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcyg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcyg;

.field private b:I

.field private c:I

.field private d:J

.field private e:I


# direct methods
.method constructor <init>(Lcyg;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 754
    iput-object p1, p0, Lcyg$c;->a:Lcyg;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcyg$e;-><init>(Lcyg$1;)V

    .line 755
    iput v2, p0, Lcyg$c;->b:I

    .line 756
    iput v2, p0, Lcyg$c;->c:I

    .line 757
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcyg$c;->d:J

    .line 758
    iput v2, p0, Lcyg$c;->e:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 12

    .prologue
    .line 762
    iget-object v0, p0, Lcyg$c;->a:Lcyg;

    iget-object v0, v0, Lcyg;->c:Landroid/media/AudioRecord;

    invoke-virtual {v0, p0}, Landroid/media/AudioRecord;->setRecordPositionUpdateListener(Landroid/media/AudioRecord$OnRecordPositionUpdateListener;)V

    .line 763
    iget-object v0, p0, Lcyg$c;->a:Lcyg;

    iget-object v0, v0, Lcyg;->c:Landroid/media/AudioRecord;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/media/AudioRecord;->setPositionNotificationPeriod(I)I

    .line 765
    iget-object v0, p0, Lcyg$c;->a:Lcyg;

    iget-object v0, v0, Lcyg;->c:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    .line 766
    invoke-virtual {p0}, Lcyg$c;->f()V

    .line 767
    :goto_0
    invoke-virtual {p0}, Lcyg$c;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 768
    iget-object v0, p0, Lcyg$c;->a:Lcyg;

    iget-object v0, v0, Lcyg;->n:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ArrayBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyg$b;

    .line 769
    if-nez v0, :cond_0

    .line 770
    new-instance v0, Lcyg$b;

    iget-object v1, p0, Lcyg$c;->a:Lcyg;

    const/16 v2, 0x400

    invoke-direct {v0, v1, v2}, Lcyg$b;-><init>(Lcyg;I)V

    .line 773
    :cond_0
    iget-wide v2, p0, Lcyg$c;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 775
    const/4 v1, 0x0

    .line 776
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    .line 777
    iget-wide v4, p0, Lcyg$c;->d:J

    sub-long/2addr v2, v4

    .line 781
    iget v4, p0, Lcyg$c;->c:I

    iget v5, p0, Lcyg$c;->e:I

    add-int/2addr v4, v5

    int-to-long v4, v4

    .line 782
    const-wide/16 v6, 0x400

    mul-long/2addr v6, v4

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    const-wide/32 v8, 0xac44

    div-long/2addr v6, v8

    .line 784
    const-wide/32 v8, 0xf4240

    mul-long/2addr v6, v8

    .line 786
    iget v8, p0, Lcyg$c;->b:I

    int-to-long v8, v8

    sub-long v4, v8, v4

    iget-object v8, p0, Lcyg$c;->a:Lcyg;

    iget-wide v8, v8, Lcyg;->C:J

    const-wide/16 v10, 0x400

    div-long/2addr v8, v10

    cmp-long v4, v4, v8

    if-lez v4, :cond_4

    .line 788
    const-string/jumbo v1, "CameraBroadcaster"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Audio lost: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcyg$c;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " rec vs read: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcyg$c;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    const/4 v1, 0x1

    .line 796
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 803
    iget-object v1, p0, Lcyg$c;->a:Lcyg;

    iget-object v1, v1, Lcyg;->c:Landroid/media/AudioRecord;

    invoke-virtual {v1}, Landroid/media/AudioRecord;->stop()V

    .line 804
    iget-object v1, p0, Lcyg$c;->a:Lcyg;

    iget-object v1, v1, Lcyg;->c:Landroid/media/AudioRecord;

    invoke-virtual {v1}, Landroid/media/AudioRecord;->startRecording()V

    .line 806
    sub-long/2addr v2, v6

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    .line 807
    const-wide/32 v4, 0xac44

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 808
    const-wide/16 v4, 0x400

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 810
    iget v1, p0, Lcyg$c;->e:I

    int-to-long v4, v1

    const-wide/16 v6, 0x400

    div-long/2addr v2, v6

    add-long/2addr v2, v4

    long-to-int v1, v2

    iput v1, p0, Lcyg$c;->e:I

    .line 811
    const-string/jumbo v1, "CameraBroadcaster"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Fill blocks: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcyg$c;->e:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    :cond_2
    iget v1, p0, Lcyg$c;->e:I

    if-lez v1, :cond_5

    .line 818
    invoke-virtual {v0}, Lcyg$b;->b()V

    .line 819
    iget v1, p0, Lcyg$c;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcyg$c;->e:I

    .line 830
    :cond_3
    :goto_2
    monitor-enter p0

    .line 831
    :try_start_0
    iget v1, p0, Lcyg$c;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcyg$c;->c:I

    .line 832
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 833
    iget-object v1, p0, Lcyg$c;->a:Lcyg;

    invoke-virtual {v1, v0}, Lcyg;->a(Lcyg$b;)V

    goto/16 :goto_0

    .line 790
    :cond_4
    sub-long v4, v2, v6

    iget-object v8, p0, Lcyg$c;->a:Lcyg;

    iget-wide v8, v8, Lcyg;->D:J

    cmp-long v4, v4, v8

    if-lez v4, :cond_1

    .line 792
    const-string/jumbo v1, "CameraBroadcaster"

    const-string/jumbo v4, "Audio lost (duration estimate)"

    invoke-static {v1, v4}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    const/4 v1, 0x1

    goto :goto_1

    .line 823
    :cond_5
    iget-object v1, p0, Lcyg$c;->a:Lcyg;

    iget-object v1, v1, Lcyg;->c:Landroid/media/AudioRecord;

    iget-object v2, p0, Lcyg$c;->a:Lcyg;

    iget-wide v2, v2, Lcyg;->l:J

    invoke-virtual {v0, v1, v2, v3}, Lcyg$b;->a(Landroid/media/AudioRecord;J)I

    .line 824
    iget-wide v2, p0, Lcyg$c;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    .line 826
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    iput-wide v2, p0, Lcyg$c;->d:J

    goto :goto_2

    .line 832
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 835
    :cond_6
    iget-object v0, p0, Lcyg$c;->a:Lcyg;

    iget-object v0, v0, Lcyg;->n:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ArrayBlockingQueue;->clear()V

    .line 836
    iget-object v0, p0, Lcyg$c;->a:Lcyg;

    iget-object v0, v0, Lcyg;->c:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 837
    iget-object v0, p0, Lcyg$c;->a:Lcyg;

    iget-object v0, v0, Lcyg;->c:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 838
    return-void
.end method

.method public onMarkerReached(Landroid/media/AudioRecord;)V
    .locals 0

    .prologue
    .line 842
    return-void
.end method

.method public onPeriodicNotification(Landroid/media/AudioRecord;)V
    .locals 1

    .prologue
    .line 846
    monitor-enter p0

    .line 847
    :try_start_0
    iget v0, p0, Lcyg$c;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcyg$c;->b:I

    .line 848
    monitor-exit p0

    .line 849
    return-void

    .line 848
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
