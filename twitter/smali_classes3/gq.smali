.class public Lgq;
.super Lgk;
.source "Twttr"


# instance fields
.field private l:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Lfq;Lew;Lhp;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lgk;-><init>(Lfq;Lew;Lhp;)V

    .line 51
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lgq;->l:Landroid/graphics/RectF;

    .line 35
    iget-object v0, p0, Lgq;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 36
    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    .line 41
    iget-object v0, p0, Lgq;->a:Lfq;

    invoke-interface {v0}, Lfq;->getBarData()Lcom/github/mikephil/charting/data/a;

    move-result-object v3

    .line 42
    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/a;->d()I

    move-result v0

    new-array v0, v0, [Lez;

    iput-object v0, p0, Lgq;->c:[Ley;

    .line 44
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lgq;->c:[Ley;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 45
    invoke-virtual {v3, v1}, Lcom/github/mikephil/charting/data/a;->a(I)Lgc;

    move-result-object v0

    check-cast v0, Lfy;

    .line 46
    iget-object v4, p0, Lgq;->c:[Ley;

    new-instance v5, Lez;

    invoke-interface {v0}, Lfy;->s()I

    move-result v2

    mul-int/lit8 v6, v2, 0x4

    invoke-interface {v0}, Lfy;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lfy;->r()I

    move-result v2

    :goto_1
    mul-int/2addr v2, v6

    .line 47
    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/a;->d()I

    move-result v6

    invoke-interface {v0}, Lfy;->d()Z

    move-result v0

    invoke-direct {v5, v2, v6, v0}, Lez;-><init>(IIZ)V

    aput-object v5, v4, v1

    .line 44
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 46
    :cond_0
    const/4 v2, 0x1

    goto :goto_1

    .line 49
    :cond_1
    return-void
.end method

.method protected a(FFFFLhm;)V
    .locals 3

    .prologue
    .line 328
    sub-float v0, p1, p4

    .line 329
    add-float v1, p1, p4

    .line 333
    iget-object v2, p0, Lgq;->b:Landroid/graphics/RectF;

    invoke-virtual {v2, p2, v0, p3, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 335
    iget-object v0, p0, Lgq;->b:Landroid/graphics/RectF;

    iget-object v1, p0, Lgq;->g:Lew;

    invoke-virtual {v1}, Lew;->a()F

    move-result v1

    invoke-virtual {p5, v0, v1}, Lhm;->b(Landroid/graphics/RectF;F)V

    .line 336
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;Lfy;I)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    iget-object v0, p0, Lgq;->a:Lfq;

    invoke-interface {p2}, Lfy;->q()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v3

    invoke-interface {v0, v3}, Lfq;->a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lhm;

    move-result-object v4

    .line 58
    iget-object v0, p0, Lgq;->e:Landroid/graphics/Paint;

    invoke-interface {p2}, Lfy;->z()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 59
    iget-object v0, p0, Lgq;->e:Landroid/graphics/Paint;

    invoke-interface {p2}, Lfy;->u()F

    move-result v3

    invoke-static {v3}, Lho;->a(F)F

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 61
    invoke-interface {p2}, Lfy;->u()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v6, v1

    .line 63
    :goto_0
    iget-object v0, p0, Lgq;->g:Lew;

    invoke-virtual {v0}, Lew;->b()F

    move-result v5

    .line 64
    iget-object v0, p0, Lgq;->g:Lew;

    invoke-virtual {v0}, Lew;->a()F

    move-result v7

    .line 67
    iget-object v0, p0, Lgq;->a:Lfq;

    invoke-interface {v0}, Lfq;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    iget-object v0, p0, Lgq;->d:Landroid/graphics/Paint;

    invoke-interface {p2}, Lfy;->t()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 70
    iget-object v0, p0, Lgq;->a:Lfq;

    invoke-interface {v0}, Lfq;->getBarData()Lcom/github/mikephil/charting/data/a;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/a;->a()F

    move-result v0

    .line 73
    const/high16 v3, 0x40000000    # 2.0f

    div-float v8, v0, v3

    .line 76
    invoke-interface {p2}, Lfy;->s()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v5

    float-to-double v10, v0

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v0, v10

    invoke-interface {p2}, Lfy;->s()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v9

    move v3, v2

    .line 77
    :goto_1
    if-ge v3, v9, :cond_2

    .line 80
    invoke-interface {p2, v3}, Lfy;->f(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/BarEntry;

    .line 82
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/BarEntry;->h()F

    move-result v0

    .line 84
    iget-object v10, p0, Lgq;->l:Landroid/graphics/RectF;

    sub-float v11, v0, v8

    iput v11, v10, Landroid/graphics/RectF;->top:F

    .line 85
    iget-object v10, p0, Lgq;->l:Landroid/graphics/RectF;

    add-float/2addr v0, v8

    iput v0, v10, Landroid/graphics/RectF;->bottom:F

    .line 87
    iget-object v0, p0, Lgq;->l:Landroid/graphics/RectF;

    invoke-virtual {v4, v0}, Lhm;->a(Landroid/graphics/RectF;)V

    .line 89
    iget-object v0, p0, Lgq;->o:Lhp;

    iget-object v10, p0, Lgq;->l:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v10}, Lhp;->i(F)Z

    move-result v0

    if-nez v0, :cond_1

    .line 78
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v6, v2

    .line 61
    goto :goto_0

    .line 92
    :cond_1
    iget-object v0, p0, Lgq;->o:Lhp;

    iget-object v10, p0, Lgq;->l:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v10}, Lhp;->j(F)Z

    move-result v0

    if-nez v0, :cond_5

    .line 103
    :cond_2
    iget-object v0, p0, Lgq;->c:[Ley;

    aget-object v9, v0, p3

    .line 104
    invoke-virtual {v9, v5, v7}, Ley;->a(FF)V

    .line 105
    invoke-virtual {v9, p3}, Ley;->a(I)V

    .line 106
    iget-object v0, p0, Lgq;->a:Lfq;

    invoke-interface {p2}, Lfy;->q()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v3

    invoke-interface {v0, v3}, Lfq;->c(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Z

    move-result v0

    invoke-virtual {v9, v0}, Ley;->a(Z)V

    .line 107
    iget-object v0, p0, Lgq;->a:Lfq;

    invoke-interface {v0}, Lfq;->getBarData()Lcom/github/mikephil/charting/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/a;->a()F

    move-result v0

    invoke-virtual {v9, v0}, Ley;->a(F)V

    .line 109
    invoke-virtual {v9, p2}, Ley;->a(Lfy;)V

    .line 111
    iget-object v0, v9, Ley;->b:[F

    invoke-virtual {v4, v0}, Lhm;->a([F)V

    .line 113
    invoke-interface {p2}, Lfy;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v8, v1

    .line 115
    :goto_3
    if-eqz v8, :cond_3

    .line 116
    iget-object v0, p0, Lgq;->h:Landroid/graphics/Paint;

    invoke-interface {p2}, Lfy;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    :cond_3
    move v7, v2

    .line 119
    :goto_4
    invoke-virtual {v9}, Ley;->b()I

    move-result v0

    if-ge v7, v0, :cond_4

    .line 121
    iget-object v0, p0, Lgq;->o:Lhp;

    iget-object v1, v9, Ley;->b:[F

    add-int/lit8 v2, v7, 0x3

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lhp;->i(F)Z

    move-result v0

    if-nez v0, :cond_7

    .line 141
    :cond_4
    return-void

    .line 95
    :cond_5
    iget-object v0, p0, Lgq;->l:Landroid/graphics/RectF;

    iget-object v10, p0, Lgq;->o:Lhp;

    invoke-virtual {v10}, Lhp;->f()F

    move-result v10

    iput v10, v0, Landroid/graphics/RectF;->left:F

    .line 96
    iget-object v0, p0, Lgq;->l:Landroid/graphics/RectF;

    iget-object v10, p0, Lgq;->o:Lhp;

    invoke-virtual {v10}, Lhp;->g()F

    move-result v10

    iput v10, v0, Landroid/graphics/RectF;->right:F

    .line 98
    iget-object v0, p0, Lgq;->l:Landroid/graphics/RectF;

    iget-object v10, p0, Lgq;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v10}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    :cond_6
    move v8, v2

    .line 113
    goto :goto_3

    .line 124
    :cond_7
    iget-object v0, p0, Lgq;->o:Lhp;

    iget-object v1, v9, Ley;->b:[F

    add-int/lit8 v2, v7, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lhp;->j(F)Z

    move-result v0

    if-nez v0, :cond_9

    .line 119
    :cond_8
    :goto_5
    add-int/lit8 v2, v7, 0x4

    move v7, v2

    goto :goto_4

    .line 127
    :cond_9
    if-nez v8, :cond_a

    .line 130
    iget-object v0, p0, Lgq;->h:Landroid/graphics/Paint;

    div-int/lit8 v1, v7, 0x4

    invoke-interface {p2, v1}, Lfy;->b(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 133
    :cond_a
    iget-object v0, v9, Ley;->b:[F

    aget v1, v0, v7

    iget-object v0, v9, Ley;->b:[F

    add-int/lit8 v2, v7, 0x1

    aget v2, v0, v2

    iget-object v0, v9, Ley;->b:[F

    add-int/lit8 v3, v7, 0x2

    aget v3, v0, v3

    iget-object v0, v9, Ley;->b:[F

    add-int/lit8 v4, v7, 0x3

    aget v4, v0, v4

    iget-object v5, p0, Lgq;->h:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 136
    if-eqz v6, :cond_8

    .line 137
    iget-object v0, v9, Ley;->b:[F

    aget v1, v0, v7

    iget-object v0, v9, Ley;->b:[F

    add-int/lit8 v2, v7, 0x1

    aget v2, v0, v2

    iget-object v0, v9, Ley;->b:[F

    add-int/lit8 v3, v7, 0x2

    aget v3, v0, v3

    iget-object v0, v9, Ley;->b:[F

    add-int/lit8 v4, v7, 0x3

    aget v4, v0, v4

    iget-object v5, p0, Lgq;->e:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_5
.end method

.method protected a(Landroid/graphics/Canvas;Ljava/lang/String;FFI)V
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lgq;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, p5}, Landroid/graphics/Paint;->setColor(I)V

    .line 322
    iget-object v0, p0, Lgq;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3, p4, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 323
    return-void
.end method

.method protected a(Lfj;Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 340
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    iget v1, p2, Landroid/graphics/RectF;->right:F

    invoke-virtual {p1, v0, v1}, Lfj;->a(FF)V

    .line 341
    return-void
.end method

.method protected a(Lfu;)Z
    .locals 3

    .prologue
    .line 345
    invoke-interface {p1}, Lfu;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->j()I

    move-result v0

    int-to-float v0, v0

    invoke-interface {p1}, Lfu;->getMaxVisibleCount()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lgq;->o:Lhp;

    .line 346
    invoke-virtual {v2}, Lhp;->r()F

    move-result v2

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    .line 345
    :goto_0
    return v0

    .line 346
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/graphics/Canvas;)V
    .locals 27

    .prologue
    .line 146
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->a:Lfq;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lgq;->a(Lfu;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 148
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->a:Lfq;

    invoke-interface {v2}, Lfq;->getBarData()Lcom/github/mikephil/charting/data/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/github/mikephil/charting/data/a;->i()Ljava/util/List;

    move-result-object v15

    .line 150
    const/high16 v2, 0x40a00000    # 5.0f

    invoke-static {v2}, Lho;->a(F)F

    move-result v14

    .line 153
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->a:Lfq;

    invoke-interface {v2}, Lfq;->c()Z

    move-result v16

    .line 155
    const/4 v2, 0x0

    move v10, v2

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->a:Lfq;

    invoke-interface {v2}, Lfq;->getBarData()Lcom/github/mikephil/charting/data/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/github/mikephil/charting/data/a;->d()I

    move-result v2

    if-ge v10, v2, :cond_19

    .line 157
    invoke-interface {v15, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lfy;

    .line 159
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lgq;->a(Lgc;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 155
    :cond_0
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_0

    .line 162
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->a:Lfq;

    invoke-interface {v8}, Lfy;->q()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v3

    invoke-interface {v2, v3}, Lfq;->c(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Z

    move-result v17

    .line 165
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lgq;->b(Lgc;)V

    .line 166
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->k:Landroid/graphics/Paint;

    const-string/jumbo v3, "10"

    invoke-static {v2, v3}, Lho;->b(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float v18, v2, v3

    .line 168
    invoke-interface {v8}, Lfy;->g()Lff;

    move-result-object v19

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->c:[Ley;

    aget-object v20, v2, v10

    .line 173
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->g:Lew;

    invoke-virtual {v2}, Lew;->a()F

    move-result v21

    .line 176
    invoke-interface {v8}, Lfy;->d()Z

    move-result v2

    if-nez v2, :cond_8

    .line 178
    const/4 v2, 0x0

    move v9, v2

    :goto_1
    int-to-float v2, v9

    move-object/from16 v0, v20

    iget-object v3, v0, Ley;->b:[F

    array-length v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lgq;->g:Lew;

    invoke-virtual {v4}, Lew;->b()F

    move-result v4

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 180
    move-object/from16 v0, v20

    iget-object v2, v0, Ley;->b:[F

    add-int/lit8 v3, v9, 0x1

    aget v2, v2, v3

    move-object/from16 v0, v20

    iget-object v3, v0, Ley;->b:[F

    add-int/lit8 v4, v9, 0x3

    aget v3, v3, v4

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float v6, v2, v3

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->o:Lhp;

    move-object/from16 v0, v20

    iget-object v3, v0, Ley;->b:[F

    add-int/lit8 v4, v9, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Lhp;->i(F)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 185
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->o:Lhp;

    move-object/from16 v0, v20

    iget-object v3, v0, Ley;->b:[F

    aget v3, v3, v9

    invoke-virtual {v2, v3}, Lhp;->e(F)Z

    move-result v2

    if-nez v2, :cond_3

    .line 178
    :cond_2
    :goto_2
    add-int/lit8 v2, v9, 0x4

    move v9, v2

    goto :goto_1

    .line 188
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->o:Lhp;

    move-object/from16 v0, v20

    iget-object v3, v0, Ley;->b:[F

    add-int/lit8 v4, v9, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Lhp;->j(F)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 191
    div-int/lit8 v2, v9, 0x4

    invoke-interface {v8, v2}, Lfy;->f(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v2

    check-cast v2, Lcom/github/mikephil/charting/data/BarEntry;

    .line 192
    invoke-virtual {v2}, Lcom/github/mikephil/charting/data/BarEntry;->b()F

    move-result v5

    .line 193
    move-object/from16 v0, p0

    iget-object v3, v0, Lgq;->o:Lhp;

    move-object/from16 v0, v19

    invoke-interface {v0, v5, v2, v10, v3}, Lff;->a(FLcom/github/mikephil/charting/data/Entry;ILhp;)Ljava/lang/String;

    move-result-object v4

    .line 196
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->k:Landroid/graphics/Paint;

    invoke-static {v2, v4}, Lho;->a(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v2

    int-to-float v7, v2

    .line 197
    if-eqz v16, :cond_5

    move v3, v14

    .line 198
    :goto_3
    if-eqz v16, :cond_6

    add-float v2, v7, v14

    neg-float v2, v2

    .line 200
    :goto_4
    if-eqz v17, :cond_4

    .line 201
    neg-float v3, v3

    sub-float/2addr v3, v7

    .line 202
    neg-float v2, v2

    sub-float/2addr v2, v7

    .line 205
    :cond_4
    move-object/from16 v0, v20

    iget-object v7, v0, Ley;->b:[F

    add-int/lit8 v11, v9, 0x2

    aget v7, v7, v11

    const/4 v11, 0x0

    cmpl-float v5, v5, v11

    if-ltz v5, :cond_7

    :goto_5
    add-float v5, v7, v3

    add-float v6, v6, v18

    div-int/lit8 v2, v9, 0x2

    .line 206
    invoke-interface {v8, v2}, Lfy;->e(I)I

    move-result v7

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 205
    invoke-virtual/range {v2 .. v7}, Lgq;->a(Landroid/graphics/Canvas;Ljava/lang/String;FFI)V

    goto :goto_2

    .line 197
    :cond_5
    add-float v2, v7, v14

    neg-float v3, v2

    goto :goto_3

    :cond_6
    move v2, v14

    .line 198
    goto :goto_4

    :cond_7
    move v3, v2

    .line 205
    goto :goto_5

    .line 212
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->a:Lfq;

    invoke-interface {v8}, Lfy;->q()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v3

    invoke-interface {v2, v3}, Lfq;->a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lhm;

    move-result-object v22

    .line 214
    const/4 v3, 0x0

    .line 215
    const/4 v2, 0x0

    move v11, v2

    move v12, v3

    .line 217
    :cond_9
    :goto_6
    int-to-float v2, v11

    invoke-interface {v8}, Lfy;->s()I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lgq;->g:Lew;

    invoke-virtual {v4}, Lew;->b()F

    move-result v4

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 219
    invoke-interface {v8, v11}, Lfy;->f(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/github/mikephil/charting/data/BarEntry;

    .line 221
    invoke-interface {v8, v11}, Lfy;->e(I)I

    move-result v7

    .line 222
    invoke-virtual {v9}, Lcom/github/mikephil/charting/data/BarEntry;->a()[F

    move-result-object v23

    .line 227
    if-nez v23, :cond_f

    .line 229
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->o:Lhp;

    move-object/from16 v0, v20

    iget-object v3, v0, Ley;->b:[F

    add-int/lit8 v4, v12, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Lhp;->i(F)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 232
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->o:Lhp;

    move-object/from16 v0, v20

    iget-object v3, v0, Ley;->b:[F

    aget v3, v3, v12

    invoke-virtual {v2, v3}, Lhp;->e(F)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 235
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->o:Lhp;

    move-object/from16 v0, v20

    iget-object v3, v0, Ley;->b:[F

    add-int/lit8 v4, v12, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Lhp;->j(F)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 238
    invoke-virtual {v9}, Lcom/github/mikephil/charting/data/BarEntry;->b()F

    move-result v2

    .line 239
    move-object/from16 v0, p0

    iget-object v3, v0, Lgq;->o:Lhp;

    move-object/from16 v0, v19

    invoke-interface {v0, v2, v9, v10, v3}, Lff;->a(FLcom/github/mikephil/charting/data/Entry;ILhp;)Ljava/lang/String;

    move-result-object v4

    .line 242
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->k:Landroid/graphics/Paint;

    invoke-static {v2, v4}, Lho;->a(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v2

    int-to-float v5, v2

    .line 243
    if-eqz v16, :cond_c

    move v3, v14

    .line 244
    :goto_7
    if-eqz v16, :cond_d

    add-float v2, v5, v14

    neg-float v2, v2

    .line 246
    :goto_8
    if-eqz v17, :cond_a

    .line 247
    neg-float v3, v3

    sub-float/2addr v3, v5

    .line 248
    neg-float v2, v2

    sub-float/2addr v2, v5

    .line 251
    :cond_a
    move-object/from16 v0, v20

    iget-object v5, v0, Ley;->b:[F

    add-int/lit8 v6, v12, 0x2

    aget v5, v5, v6

    .line 252
    invoke-virtual {v9}, Lcom/github/mikephil/charting/data/BarEntry;->b()F

    move-result v6

    const/4 v9, 0x0

    cmpl-float v6, v6, v9

    if-ltz v6, :cond_e

    :goto_9
    add-float/2addr v5, v3

    move-object/from16 v0, v20

    iget-object v2, v0, Ley;->b:[F

    add-int/lit8 v3, v12, 0x1

    aget v2, v2, v3

    add-float v6, v2, v18

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 251
    invoke-virtual/range {v2 .. v7}, Lgq;->a(Landroid/graphics/Canvas;Ljava/lang/String;FFI)V

    .line 312
    :cond_b
    if-nez v23, :cond_18

    add-int/lit8 v3, v12, 0x4

    .line 313
    :goto_a
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    move v12, v3

    .line 314
    goto/16 :goto_6

    .line 243
    :cond_c
    add-float v2, v5, v14

    neg-float v3, v2

    goto :goto_7

    :cond_d
    move v2, v14

    .line 244
    goto :goto_8

    :cond_e
    move v3, v2

    .line 252
    goto :goto_9

    .line 257
    :cond_f
    move-object/from16 v0, v23

    array-length v2, v0

    mul-int/lit8 v2, v2, 0x2

    new-array v0, v2, [F

    move-object/from16 v24, v0

    .line 259
    const/4 v5, 0x0

    .line 260
    invoke-virtual {v9}, Lcom/github/mikephil/charting/data/BarEntry;->f()F

    move-result v2

    neg-float v4, v2

    .line 262
    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_b
    move-object/from16 v0, v24

    array-length v6, v0

    if-ge v3, v6, :cond_11

    .line 264
    aget v6, v23, v2

    .line 267
    const/4 v13, 0x0

    cmpl-float v13, v6, v13

    if-ltz v13, :cond_10

    .line 268
    add-float/2addr v5, v6

    move v6, v5

    .line 275
    :goto_c
    mul-float v6, v6, v21

    aput v6, v24, v3

    .line 262
    add-int/lit8 v3, v3, 0x2

    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 272
    :cond_10
    sub-float v6, v4, v6

    move/from16 v26, v4

    move v4, v6

    move/from16 v6, v26

    goto :goto_c

    .line 278
    :cond_11
    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lhm;->a([F)V

    .line 280
    const/4 v2, 0x0

    move v13, v2

    :goto_d
    move-object/from16 v0, v24

    array-length v2, v0

    if-ge v13, v2, :cond_b

    .line 282
    div-int/lit8 v2, v13, 0x2

    aget v5, v23, v2

    .line 283
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->o:Lhp;

    move-object/from16 v0, v19

    invoke-interface {v0, v5, v9, v10, v2}, Lff;->a(FLcom/github/mikephil/charting/data/Entry;ILhp;)Ljava/lang/String;

    move-result-object v4

    .line 286
    move-object/from16 v0, p0

    iget-object v2, v0, Lgq;->k:Landroid/graphics/Paint;

    invoke-static {v2, v4}, Lho;->a(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v2

    int-to-float v6, v2

    .line 287
    if-eqz v16, :cond_14

    move v3, v14

    .line 288
    :goto_e
    if-eqz v16, :cond_15

    add-float v2, v6, v14

    neg-float v2, v2

    .line 290
    :goto_f
    if-eqz v17, :cond_12

    .line 291
    neg-float v3, v3

    sub-float/2addr v3, v6

    .line 292
    neg-float v2, v2

    sub-float/2addr v2, v6

    .line 295
    :cond_12
    aget v6, v24, v13

    const/16 v25, 0x0

    cmpl-float v5, v5, v25

    if-ltz v5, :cond_16

    :goto_10
    add-float v5, v6, v3

    .line 297
    move-object/from16 v0, v20

    iget-object v2, v0, Ley;->b:[F

    add-int/lit8 v3, v12, 0x1

    aget v2, v2, v3

    move-object/from16 v0, v20

    iget-object v3, v0, Ley;->b:[F

    add-int/lit8 v6, v12, 0x3

    aget v3, v3, v6

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    .line 299
    move-object/from16 v0, p0

    iget-object v3, v0, Lgq;->o:Lhp;

    invoke-virtual {v3, v2}, Lhp;->i(F)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 302
    move-object/from16 v0, p0

    iget-object v3, v0, Lgq;->o:Lhp;

    invoke-virtual {v3, v5}, Lhp;->e(F)Z

    move-result v3

    if-nez v3, :cond_17

    .line 280
    :cond_13
    :goto_11
    add-int/lit8 v2, v13, 0x2

    move v13, v2

    goto :goto_d

    .line 287
    :cond_14
    add-float v2, v6, v14

    neg-float v3, v2

    goto :goto_e

    :cond_15
    move v2, v14

    .line 288
    goto :goto_f

    :cond_16
    move v3, v2

    .line 295
    goto :goto_10

    .line 305
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lgq;->o:Lhp;

    invoke-virtual {v3, v2}, Lhp;->j(F)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 308
    add-float v6, v2, v18

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lgq;->a(Landroid/graphics/Canvas;Ljava/lang/String;FFI)V

    goto :goto_11

    .line 312
    :cond_18
    move-object/from16 v0, v23

    array-length v2, v0

    mul-int/lit8 v2, v2, 0x4

    add-int v3, v12, v2

    goto/16 :goto_a

    .line 318
    :cond_19
    return-void
.end method
