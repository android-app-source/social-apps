.class final Ldcg;
.super Ldck;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ldcg$a;
    }
.end annotation


# instance fields
.field private final a:F

.field private final b:J

.field private final c:J

.field private final d:J

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldcl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(FJJJLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FJJJ",
            "Ljava/util/List",
            "<",
            "Ldcl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ldck;-><init>()V

    .line 22
    iput p1, p0, Ldcg;->a:F

    .line 23
    iput-wide p2, p0, Ldcg;->b:J

    .line 24
    iput-wide p4, p0, Ldcg;->c:J

    .line 25
    iput-wide p6, p0, Ldcg;->d:J

    .line 26
    iput-object p8, p0, Ldcg;->e:Ljava/util/List;

    .line 27
    return-void
.end method

.method synthetic constructor <init>(FJJJLjava/util/List;Ldcg$1;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct/range {p0 .. p8}, Ldcg;-><init>(FJJJLjava/util/List;)V

    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Ldcg;->a:F

    return v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Ldcg;->b:J

    return-wide v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Ldcg;->c:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Ldcg;->d:J

    return-wide v0
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldcl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Ldcg;->e:Ljava/util/List;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 67
    if-ne p1, p0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v0

    .line 70
    :cond_1
    instance-of v2, p1, Ldck;

    if-eqz v2, :cond_3

    .line 71
    check-cast p1, Ldck;

    .line 72
    iget v2, p0, Ldcg;->a:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    invoke-virtual {p1}, Ldck;->a()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Ldcg;->b:J

    .line 73
    invoke-virtual {p1}, Ldck;->b()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ldcg;->c:J

    .line 74
    invoke-virtual {p1}, Ldck;->c()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Ldcg;->d:J

    .line 75
    invoke-virtual {p1}, Ldck;->d()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Ldcg;->e:Ljava/util/List;

    .line 76
    invoke-virtual {p1}, Ldck;->e()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 78
    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/16 v7, 0x20

    const v6, 0xf4243

    .line 83
    .line 85
    iget v0, p0, Ldcg;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    xor-int/2addr v0, v6

    .line 86
    mul-int/2addr v0, v6

    .line 87
    int-to-long v0, v0

    iget-wide v2, p0, Ldcg;->b:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Ldcg;->b:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 88
    mul-int/2addr v0, v6

    .line 89
    int-to-long v0, v0

    iget-wide v2, p0, Ldcg;->c:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Ldcg;->c:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 90
    mul-int/2addr v0, v6

    .line 91
    int-to-long v0, v0

    iget-wide v2, p0, Ldcg;->d:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Ldcg;->d:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 92
    mul-int/2addr v0, v6

    .line 93
    iget-object v1, p0, Ldcg;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 94
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "HighlightChunk{score="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ldcg;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", videoDurationMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ldcg;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", audioDurationMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ldcg;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", ntpTimestampMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ldcg;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", id3Tags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldcg;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
