.class public Lzx;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lzu;

.field private final b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;


# direct methods
.method constructor <init>(Lzu;Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lzx;->a:Lzu;

    .line 43
    iput-object p2, p0, Lzx;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 44
    return-void
.end method

.method public static a(J)Lzx;
    .locals 4

    .prologue
    .line 36
    new-instance v0, Lzx;

    invoke-static {}, Lzu;->a()Lzu;

    move-result-object v1

    new-instance v2, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 37
    invoke-virtual {v2, p0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lzx;-><init>(Lzu;Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;)V

    .line 36
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 47
    iget-object v1, p0, Lzx;->a:Lzu;

    iget-object v0, p0, Lzx;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:reorder:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, ""

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "show"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    .line 51
    iget-object v1, p0, Lzx;->a:Lzu;

    iget-object v0, p0, Lzx;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:reorder:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, ""

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "hide"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public b(J)V
    .locals 7

    .prologue
    .line 55
    iget-object v1, p0, Lzx;->a:Lzu;

    iget-object v0, p0, Lzx;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:reorder:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "operations"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "delete"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public c(J)V
    .locals 7

    .prologue
    .line 60
    iget-object v1, p0, Lzx;->a:Lzu;

    iget-object v0, p0, Lzx;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:reorder:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "operations"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "reorder"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 62
    return-void
.end method
