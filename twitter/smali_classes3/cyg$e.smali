.class abstract Lcyg$e;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcyg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcyg$e$a;
    }
.end annotation


# instance fields
.field private volatile a:Z

.field private volatile b:Z

.field private final c:Ljava/lang/Object;

.field private d:Ljava/lang/Thread;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 650
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcyg$e;->c:Ljava/lang/Object;

    return-void
.end method

.method synthetic constructor <init>(Lcyg$1;)V
    .locals 0

    .prologue
    .line 647
    invoke-direct {p0}, Lcyg$e;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public b()V
    .locals 3

    .prologue
    .line 654
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcyg$e$a;

    invoke-direct {v1, p0}, Lcyg$e$a;-><init>(Lcyg$e;)V

    const-string/jumbo v2, "CameraWorker"

    invoke-static {v2}, Ltv/periscope/android/util/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lcyg$e;->d:Ljava/lang/Thread;

    .line 655
    iget-object v0, p0, Lcyg$e;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 656
    return-void
.end method

.method c()V
    .locals 2

    .prologue
    .line 659
    iget-object v1, p0, Lcyg$e;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 660
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcyg$e;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcyg$e;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 662
    :try_start_1
    iget-object v0, p0, Lcyg$e;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 663
    :catch_0
    move-exception v0

    goto :goto_0

    .line 666
    :cond_0
    :try_start_2
    monitor-exit v1

    .line 667
    return-void

    .line 666
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 672
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcyg$e;->a:Z

    .line 674
    :try_start_0
    iget-object v0, p0, Lcyg$e;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 677
    :goto_0
    return-void

    .line 675
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method e()Z
    .locals 1

    .prologue
    .line 680
    iget-boolean v0, p0, Lcyg$e;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method f()V
    .locals 2

    .prologue
    .line 684
    iget-object v1, p0, Lcyg$e;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 685
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcyg$e;->b:Z

    .line 686
    iget-object v0, p0, Lcyg$e;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 687
    monitor-exit v1

    .line 688
    return-void

    .line 687
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
