.class public Lczq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/google/android/gms/common/api/c$b;
.implements Lcom/google/android/gms/common/api/c$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lczq$a;,
        Lczq$b;
    }
.end annotation


# static fields
.field private static final f:[Ljava/lang/String;


# instance fields
.field protected a:Lcom/google/android/gms/location/places/AutocompleteFilter;

.field protected b:Lcom/google/android/gms/maps/model/LatLngBounds;

.field protected c:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/common/api/i",
            "<",
            "Lcom/google/android/gms/location/places/b;",
            ">;>;>;"
        }
    .end annotation
.end field

.field protected d:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/common/api/i",
            "<",
            "Lcom/google/android/gms/location/places/e;",
            ">;>;>;"
        }
    .end annotation
.end field

.field protected e:Z

.field private g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/android/gms/common/api/c;

.field private i:Lcom/google/android/gms/location/LocationRequest;

.field private j:Lcom/google/android/gms/location/LocationSettingsRequest;

.field private k:Lczq$b;

.field private l:Lcom/google/android/gms/location/f;

.field private m:Lczq$a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, Lczq;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lczq$b;Lcom/google/android/gms/location/f;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x2710

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    new-instance v0, Lczq$2;

    invoke-direct {v0, p0}, Lczq$2;-><init>(Lczq;)V

    iput-object v0, p0, Lczq;->m:Lczq$a;

    .line 96
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lczq;->g:Ljava/lang/ref/WeakReference;

    .line 97
    iput-object p2, p0, Lczq;->k:Lczq$b;

    .line 98
    iput-object p3, p0, Lczq;->l:Lcom/google/android/gms/location/f;

    .line 101
    new-instance v0, Lcom/google/android/gms/common/api/c$a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/c$a;-><init>(Landroid/content/Context;)V

    .line 102
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/c$a;->a(Lcom/google/android/gms/common/api/c$b;)Lcom/google/android/gms/common/api/c$a;

    move-result-object v0

    .line 103
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/c$a;->a(Lcom/google/android/gms/common/api/c$c;)Lcom/google/android/gms/common/api/c$a;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/h;->a:Lcom/google/android/gms/common/api/a;

    .line 104
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/c$a;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/c$a;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/places/k;->c:Lcom/google/android/gms/common/api/a;

    .line 105
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/c$a;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/c$a;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/places/k;->d:Lcom/google/android/gms/common/api/a;

    .line 106
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/c$a;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/c$a;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/c$a;->b()Lcom/google/android/gms/common/api/c;

    move-result-object v0

    iput-object v0, p0, Lczq;->h:Lcom/google/android/gms/common/api/c;

    .line 109
    new-instance v0, Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v0}, Lcom/google/android/gms/location/LocationRequest;-><init>()V

    .line 110
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    .line 111
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->b(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const/high16 v1, 0x40400000    # 3.0f

    .line 112
    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(F)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const/16 v1, 0x64

    .line 113
    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iput-object v0, p0, Lczq;->i:Lcom/google/android/gms/location/LocationRequest;

    .line 116
    new-instance v0, Lcom/google/android/gms/location/LocationSettingsRequest$a;

    invoke-direct {v0}, Lcom/google/android/gms/location/LocationSettingsRequest$a;-><init>()V

    iget-object v1, p0, Lczq;->i:Lcom/google/android/gms/location/LocationRequest;

    .line 117
    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationSettingsRequest$a;->a(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/location/LocationSettingsRequest$a;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationSettingsRequest$a;->a()Lcom/google/android/gms/location/LocationSettingsRequest;

    move-result-object v0

    iput-object v0, p0, Lczq;->j:Lcom/google/android/gms/location/LocationSettingsRequest;

    .line 119
    return-void
.end method

.method static synthetic a(Lczq;)Lczq$b;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lczq;->k:Lczq$b;

    return-object v0
.end method

.method static synthetic b(Lczq;)Lcom/google/android/gms/common/api/c;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lczq;->h:Lcom/google/android/gms/common/api/c;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 230
    sget-object v0, Lcom/google/android/gms/location/h;->d:Lcom/google/android/gms/location/i;

    iget-object v1, p0, Lczq;->h:Lcom/google/android/gms/common/api/c;

    iget-object v2, p0, Lczq;->j:Lcom/google/android/gms/location/LocationSettingsRequest;

    .line 231
    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/location/i;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/location/LocationSettingsRequest;)Lcom/google/android/gms/common/api/e;

    move-result-object v0

    .line 232
    new-instance v1, Lczq$1;

    invoke-direct {v1, p0}, Lczq$1;-><init>(Lczq;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/e;->a(Lcom/google/android/gms/common/api/i;)V

    .line 246
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 294
    invoke-virtual {p0}, Lczq;->c()V

    .line 295
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 209
    iget-boolean v0, p0, Lczq;->e:Z

    if-eqz v0, :cond_0

    .line 210
    invoke-virtual {p0}, Lczq;->f()V

    .line 213
    :cond_0
    iget-object v0, p0, Lczq;->c:Landroid/util/Pair;

    if-eqz v0, :cond_2

    .line 214
    iget-object v0, p0, Lczq;->c:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 215
    iget-object v0, p0, Lczq;->c:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lczq;->c:Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/api/i;

    invoke-virtual {p0, v0, v1}, Lczq;->a(Ljava/lang/String;Lcom/google/android/gms/common/api/i;)V

    .line 217
    :cond_1
    iput-object v2, p0, Lczq;->c:Landroid/util/Pair;

    .line 220
    :cond_2
    iget-object v0, p0, Lczq;->d:Landroid/util/Pair;

    if-eqz v0, :cond_4

    .line 221
    iget-object v0, p0, Lczq;->d:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 222
    iget-object v0, p0, Lczq;->d:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lczq;->d:Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/api/i;

    invoke-virtual {p0, v0, v1}, Lczq;->b(Ljava/lang/String;Lcom/google/android/gms/common/api/i;)V

    .line 224
    :cond_3
    iput-object v2, p0, Lczq;->d:Landroid/util/Pair;

    .line 227
    :cond_4
    return-void
.end method

.method public a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3

    .prologue
    .line 300
    const-string/jumbo v0, "LocationManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onConnectionFailed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/android/gms/common/api/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/api/i",
            "<",
            "Lcom/google/android/gms/location/places/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lczq;->h:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/c;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    new-instance v0, Landroid/util/Pair;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, p1, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lczq;->c:Landroid/util/Pair;

    .line 257
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v0, p0, Lczq;->m:Lczq$a;

    invoke-virtual {p0}, Lczq;->h()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v1

    invoke-virtual {p0}, Lczq;->i()Lcom/google/android/gms/location/places/AutocompleteFilter;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lczq$a;->a(Ljava/lang/String;Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lczq;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 133
    if-eqz v0, :cond_0

    .line 134
    sget-object v1, Lczq;->f:[Ljava/lang/String;

    invoke-static {v0, v1}, Ltv/periscope/android/permissions/b;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    .line 136
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Lcom/google/android/gms/common/api/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/api/i",
            "<",
            "Lcom/google/android/gms/location/places/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lczq;->h:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/c;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 262
    new-instance v0, Landroid/util/Pair;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, p1, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lczq;->d:Landroid/util/Pair;

    .line 267
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lczq;->m:Lczq$a;

    invoke-interface {v0, p1, p2}, Lczq$a;->a(Ljava/lang/String;Lcom/google/android/gms/common/api/i;)V

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lczq;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 142
    if-eqz v0, :cond_0

    sget-object v1, Lczq;->f:[Ljava/lang/String;

    .line 145
    invoke-static {v0, v1}, Ltv/periscope/android/permissions/b;->a(Landroid/app/Activity;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lczq;->k:Lczq$b;

    sget-object v1, Lczq;->f:[Ljava/lang/String;

    invoke-interface {v0, v1}, Lczq$b;->a([Ljava/lang/String;)V

    .line 147
    const/4 v0, 0x1

    .line 149
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lczq;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lczq;->h:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/c;->j()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lczq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lczq;->h:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/c;->e()V

    .line 157
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 161
    iput-object v0, p0, Lczq;->c:Landroid/util/Pair;

    .line 162
    iput-object v0, p0, Lczq;->d:Landroid/util/Pair;

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lczq;->e:Z

    .line 164
    invoke-virtual {p0}, Lczq;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lczq;->h:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/c;->g()V

    .line 167
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lczq;->h:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/c;->i()Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 4

    .prologue
    .line 184
    invoke-virtual {p0}, Lczq;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lczq;->e:Z

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    invoke-direct {p0}, Lczq;->j()V

    .line 190
    invoke-virtual {p0}, Lczq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    sget-object v0, Lcom/google/android/gms/location/h;->b:Lcom/google/android/gms/location/c;

    iget-object v1, p0, Lczq;->h:Lcom/google/android/gms/common/api/c;

    iget-object v2, p0, Lczq;->i:Lcom/google/android/gms/location/LocationRequest;

    iget-object v3, p0, Lczq;->l:Lcom/google/android/gms/location/f;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/location/c;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/f;)Lcom/google/android/gms/common/api/e;

    goto :goto_0
.end method

.method public g()V
    .locals 3

    .prologue
    .line 197
    invoke-virtual {p0}, Lczq;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 198
    const/4 v0, 0x0

    iput-boolean v0, p0, Lczq;->e:Z

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    invoke-virtual {p0}, Lczq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    sget-object v0, Lcom/google/android/gms/location/h;->b:Lcom/google/android/gms/location/c;

    iget-object v1, p0, Lczq;->h:Lcom/google/android/gms/common/api/c;

    iget-object v2, p0, Lczq;->l:Lcom/google/android/gms/location/f;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/location/c;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/location/f;)Lcom/google/android/gms/common/api/e;

    goto :goto_0
.end method

.method public h()Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 6

    .prologue
    .line 274
    iget-object v0, p0, Lczq;->b:Lcom/google/android/gms/maps/model/LatLngBounds;

    if-nez v0, :cond_0

    .line 275
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    const-wide v2, -0x3faac00000000000L    # -85.0

    const-wide v4, 0x4066800000000000L    # 180.0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 276
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    const-wide v2, 0x4055400000000000L    # 85.0

    const-wide v4, -0x3f99800000000000L    # -180.0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 277
    new-instance v2, Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    iput-object v2, p0, Lczq;->b:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 279
    :cond_0
    iget-object v0, p0, Lczq;->b:Lcom/google/android/gms/maps/model/LatLngBounds;

    return-object v0
.end method

.method public i()Lcom/google/android/gms/location/places/AutocompleteFilter;
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lczq;->a:Lcom/google/android/gms/location/places/AutocompleteFilter;

    if-nez v0, :cond_0

    .line 285
    new-instance v0, Lcom/google/android/gms/location/places/AutocompleteFilter$a;

    invoke-direct {v0}, Lcom/google/android/gms/location/places/AutocompleteFilter$a;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/AutocompleteFilter$a;->a(I)Lcom/google/android/gms/location/places/AutocompleteFilter$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/AutocompleteFilter$a;->a()Lcom/google/android/gms/location/places/AutocompleteFilter;

    move-result-object v0

    iput-object v0, p0, Lczq;->a:Lcom/google/android/gms/location/places/AutocompleteFilter;

    .line 287
    :cond_0
    iget-object v0, p0, Lczq;->a:Lcom/google/android/gms/location/places/AutocompleteFilter;

    return-object v0
.end method
