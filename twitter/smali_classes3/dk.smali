.class public Ldk;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Landroid/content/ContentResolver;

.field private b:Landroid/content/res/Resources;

.field private c:Landroid/content/res/AssetManager;

.field private final d:Lcom/facebook/imagepipeline/memory/e;

.field private final e:Lcom/facebook/imagepipeline/decoder/a;

.field private final f:Lcom/facebook/imagepipeline/decoder/b;

.field private final g:Z

.field private final h:Z

.field private final i:Ldf;

.field private final j:Lcom/facebook/imagepipeline/memory/w;

.field private final k:Lcm;

.field private final l:Lcm;

.field private final m:Lcx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcx",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcx",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Ldq;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lcn;

.field private final p:Lcom/facebook/imagepipeline/bitmaps/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/imagepipeline/memory/e;Lcom/facebook/imagepipeline/decoder/a;Lcom/facebook/imagepipeline/decoder/b;ZZLdf;Lcom/facebook/imagepipeline/memory/w;Lcx;Lcx;Lcm;Lcm;Lcn;Lcom/facebook/imagepipeline/bitmaps/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/imagepipeline/memory/e;",
            "Lcom/facebook/imagepipeline/decoder/a;",
            "Lcom/facebook/imagepipeline/decoder/b;",
            "ZZ",
            "Ldf;",
            "Lcom/facebook/imagepipeline/memory/w;",
            "Lcx",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Ldq;",
            ">;",
            "Lcx",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;",
            "Lcm;",
            "Lcm;",
            "Lcn;",
            "Lcom/facebook/imagepipeline/bitmaps/e;",
            ")V"
        }
    .end annotation

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Ldk;->a:Landroid/content/ContentResolver;

    .line 101
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Ldk;->b:Landroid/content/res/Resources;

    .line 102
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Ldk;->c:Landroid/content/res/AssetManager;

    .line 104
    iput-object p2, p0, Ldk;->d:Lcom/facebook/imagepipeline/memory/e;

    .line 105
    iput-object p3, p0, Ldk;->e:Lcom/facebook/imagepipeline/decoder/a;

    .line 106
    iput-object p4, p0, Ldk;->f:Lcom/facebook/imagepipeline/decoder/b;

    .line 107
    iput-boolean p5, p0, Ldk;->g:Z

    .line 108
    iput-boolean p6, p0, Ldk;->h:Z

    .line 110
    iput-object p7, p0, Ldk;->i:Ldf;

    .line 111
    iput-object p8, p0, Ldk;->j:Lcom/facebook/imagepipeline/memory/w;

    .line 113
    iput-object p9, p0, Ldk;->n:Lcx;

    .line 114
    iput-object p10, p0, Ldk;->m:Lcx;

    .line 115
    iput-object p11, p0, Ldk;->k:Lcm;

    .line 116
    iput-object p12, p0, Ldk;->l:Lcm;

    .line 117
    iput-object p13, p0, Ldk;->o:Lcn;

    .line 119
    iput-object p14, p0, Ldk;->p:Lcom/facebook/imagepipeline/bitmaps/e;

    .line 120
    return-void
.end method

.method public static a(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lds;",
            ">;)",
            "Lcom/facebook/imagepipeline/producers/a;"
        }
    .end annotation

    .prologue
    .line 124
    new-instance v0, Lcom/facebook/imagepipeline/producers/a;

    invoke-direct {v0, p0}, Lcom/facebook/imagepipeline/producers/a;-><init>(Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/imagepipeline/producers/af;Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lds;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lds;",
            ">;)",
            "Lcom/facebook/imagepipeline/producers/i;"
        }
    .end annotation

    .prologue
    .line 145
    new-instance v0, Lcom/facebook/imagepipeline/producers/i;

    invoke-direct {v0, p0, p1}, Lcom/facebook/imagepipeline/producers/i;-><init>(Lcom/facebook/imagepipeline/producers/af;Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/facebook/imagepipeline/producers/ac;)Lcom/facebook/imagepipeline/producers/ab;
    .locals 3

    .prologue
    .line 220
    new-instance v0, Lcom/facebook/imagepipeline/producers/ab;

    iget-object v1, p0, Ldk;->j:Lcom/facebook/imagepipeline/memory/w;

    iget-object v2, p0, Ldk;->d:Lcom/facebook/imagepipeline/memory/e;

    invoke-direct {v0, v1, v2, p1}, Lcom/facebook/imagepipeline/producers/ab;-><init>(Lcom/facebook/imagepipeline/memory/w;Lcom/facebook/imagepipeline/memory/e;Lcom/facebook/imagepipeline/producers/ac;)V

    return-object v0
.end method

.method public a(ILcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/an;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Lcom/facebook/imagepipeline/producers/af",
            "<TT;>;)",
            "Lcom/facebook/imagepipeline/producers/an",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 262
    new-instance v0, Lcom/facebook/imagepipeline/producers/an;

    iget-object v1, p0, Ldk;->i:Ldf;

    invoke-interface {v1}, Ldf;->e()Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-direct {v0, p1, v1, p2}, Lcom/facebook/imagepipeline/producers/an;-><init>(ILjava/util/concurrent/Executor;Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method

.method public a()Lcom/facebook/imagepipeline/producers/k;
    .locals 2

    .prologue
    .line 149
    new-instance v0, Lcom/facebook/imagepipeline/producers/k;

    iget-object v1, p0, Ldk;->j:Lcom/facebook/imagepipeline/memory/w;

    invoke-direct {v0, v1}, Lcom/facebook/imagepipeline/producers/k;-><init>(Lcom/facebook/imagepipeline/memory/w;)V

    return-object v0
.end method

.method public b(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lcom/facebook/common/references/a",
            "<",
            "Ldq;",
            ">;>;)",
            "Lcom/facebook/imagepipeline/producers/f;"
        }
    .end annotation

    .prologue
    .line 129
    new-instance v0, Lcom/facebook/imagepipeline/producers/f;

    iget-object v1, p0, Ldk;->n:Lcx;

    iget-object v2, p0, Ldk;->o:Lcn;

    invoke-direct {v0, v1, v2, p1}, Lcom/facebook/imagepipeline/producers/f;-><init>(Lcx;Lcn;Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method

.method public b()Lcom/facebook/imagepipeline/producers/t;
    .locals 4

    .prologue
    .line 183
    new-instance v0, Lcom/facebook/imagepipeline/producers/t;

    iget-object v1, p0, Ldk;->i:Ldf;

    invoke-interface {v1}, Ldf;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Ldk;->j:Lcom/facebook/imagepipeline/memory/w;

    iget-object v3, p0, Ldk;->c:Landroid/content/res/AssetManager;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/imagepipeline/producers/t;-><init>(Ljava/util/concurrent/Executor;Lcom/facebook/imagepipeline/memory/w;Landroid/content/res/AssetManager;)V

    return-object v0
.end method

.method public c(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lcom/facebook/common/references/a",
            "<",
            "Ldq;",
            ">;>;)",
            "Lcom/facebook/imagepipeline/producers/g;"
        }
    .end annotation

    .prologue
    .line 134
    new-instance v0, Lcom/facebook/imagepipeline/producers/g;

    iget-object v1, p0, Ldk;->o:Lcn;

    invoke-direct {v0, v1, p1}, Lcom/facebook/imagepipeline/producers/g;-><init>(Lcn;Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method

.method public c()Lcom/facebook/imagepipeline/producers/u;
    .locals 4

    .prologue
    .line 190
    new-instance v0, Lcom/facebook/imagepipeline/producers/u;

    iget-object v1, p0, Ldk;->i:Ldf;

    invoke-interface {v1}, Ldf;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Ldk;->j:Lcom/facebook/imagepipeline/memory/w;

    iget-object v3, p0, Ldk;->a:Landroid/content/ContentResolver;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/imagepipeline/producers/u;-><init>(Ljava/util/concurrent/Executor;Lcom/facebook/imagepipeline/memory/w;Landroid/content/ContentResolver;)V

    return-object v0
.end method

.method public d(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/h;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lcom/facebook/common/references/a",
            "<",
            "Ldq;",
            ">;>;)",
            "Lcom/facebook/imagepipeline/producers/h;"
        }
    .end annotation

    .prologue
    .line 139
    new-instance v0, Lcom/facebook/imagepipeline/producers/h;

    iget-object v1, p0, Ldk;->n:Lcx;

    iget-object v2, p0, Ldk;->o:Lcn;

    invoke-direct {v0, v1, v2, p1}, Lcom/facebook/imagepipeline/producers/h;-><init>(Lcx;Lcn;Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method

.method public d()Lcom/facebook/imagepipeline/producers/v;
    .locals 3

    .prologue
    .line 197
    new-instance v0, Lcom/facebook/imagepipeline/producers/v;

    iget-object v1, p0, Ldk;->i:Ldf;

    invoke-interface {v1}, Ldf;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Ldk;->j:Lcom/facebook/imagepipeline/memory/w;

    invoke-direct {v0, v1, v2}, Lcom/facebook/imagepipeline/producers/v;-><init>(Ljava/util/concurrent/Executor;Lcom/facebook/imagepipeline/memory/w;)V

    return-object v0
.end method

.method public e(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/l;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lds;",
            ">;)",
            "Lcom/facebook/imagepipeline/producers/l;"
        }
    .end annotation

    .prologue
    .line 153
    new-instance v0, Lcom/facebook/imagepipeline/producers/l;

    iget-object v1, p0, Ldk;->d:Lcom/facebook/imagepipeline/memory/e;

    iget-object v2, p0, Ldk;->i:Ldf;

    invoke-interface {v2}, Ldf;->c()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, p0, Ldk;->e:Lcom/facebook/imagepipeline/decoder/a;

    iget-object v4, p0, Ldk;->f:Lcom/facebook/imagepipeline/decoder/b;

    iget-boolean v5, p0, Ldk;->g:Z

    iget-boolean v6, p0, Ldk;->h:Z

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/facebook/imagepipeline/producers/l;-><init>(Lcom/facebook/imagepipeline/memory/e;Ljava/util/concurrent/Executor;Lcom/facebook/imagepipeline/decoder/a;Lcom/facebook/imagepipeline/decoder/b;ZZLcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method

.method public e()Lcom/facebook/imagepipeline/producers/x;
    .locals 3

    .prologue
    .line 203
    new-instance v0, Lcom/facebook/imagepipeline/producers/x;

    iget-object v1, p0, Ldk;->i:Ldf;

    invoke-interface {v1}, Ldf;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Ldk;->j:Lcom/facebook/imagepipeline/memory/w;

    invoke-direct {v0, v1, v2}, Lcom/facebook/imagepipeline/producers/x;-><init>(Ljava/util/concurrent/Executor;Lcom/facebook/imagepipeline/memory/w;)V

    return-object v0
.end method

.method public f(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/n;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lds;",
            ">;)",
            "Lcom/facebook/imagepipeline/producers/n;"
        }
    .end annotation

    .prologue
    .line 165
    new-instance v0, Lcom/facebook/imagepipeline/producers/n;

    iget-object v1, p0, Ldk;->k:Lcm;

    iget-object v2, p0, Ldk;->l:Lcm;

    iget-object v3, p0, Ldk;->o:Lcn;

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/facebook/imagepipeline/producers/n;-><init>(Lcm;Lcm;Lcn;Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method

.method public f()Lcom/facebook/imagepipeline/producers/y;
    .locals 4

    .prologue
    .line 209
    new-instance v0, Lcom/facebook/imagepipeline/producers/y;

    iget-object v1, p0, Ldk;->i:Ldf;

    invoke-interface {v1}, Ldf;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Ldk;->j:Lcom/facebook/imagepipeline/memory/w;

    iget-object v3, p0, Ldk;->b:Landroid/content/res/Resources;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/imagepipeline/producers/y;-><init>(Ljava/util/concurrent/Executor;Lcom/facebook/imagepipeline/memory/w;Landroid/content/res/Resources;)V

    return-object v0
.end method

.method public g(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/p;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lds;",
            ">;)",
            "Lcom/facebook/imagepipeline/producers/p;"
        }
    .end annotation

    .prologue
    .line 174
    new-instance v0, Lcom/facebook/imagepipeline/producers/p;

    iget-object v1, p0, Ldk;->o:Lcn;

    invoke-direct {v0, v1, p1}, Lcom/facebook/imagepipeline/producers/p;-><init>(Lcn;Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method

.method public g()Lcom/facebook/imagepipeline/producers/z;
    .locals 2

    .prologue
    .line 216
    new-instance v0, Lcom/facebook/imagepipeline/producers/z;

    iget-object v1, p0, Ldk;->i:Ldf;

    invoke-interface {v1}, Ldf;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/imagepipeline/producers/z;-><init>(Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public h(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/q;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lds;",
            ">;)",
            "Lcom/facebook/imagepipeline/producers/q;"
        }
    .end annotation

    .prologue
    .line 179
    new-instance v0, Lcom/facebook/imagepipeline/producers/q;

    iget-object v1, p0, Ldk;->m:Lcx;

    iget-object v2, p0, Ldk;->o:Lcn;

    invoke-direct {v0, v1, v2, p1}, Lcom/facebook/imagepipeline/producers/q;-><init>(Lcx;Lcn;Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method

.method public i(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/ad;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lcom/facebook/common/references/a",
            "<",
            "Ldq;",
            ">;>;)",
            "Lcom/facebook/imagepipeline/producers/ad;"
        }
    .end annotation

    .prologue
    .line 232
    new-instance v0, Lcom/facebook/imagepipeline/producers/ad;

    iget-object v1, p0, Ldk;->n:Lcx;

    iget-object v2, p0, Ldk;->o:Lcn;

    invoke-direct {v0, v1, v2, p1}, Lcom/facebook/imagepipeline/producers/ad;-><init>(Lcx;Lcn;Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method

.method public j(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/ae;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lcom/facebook/common/references/a",
            "<",
            "Ldq;",
            ">;>;)",
            "Lcom/facebook/imagepipeline/producers/ae;"
        }
    .end annotation

    .prologue
    .line 238
    new-instance v0, Lcom/facebook/imagepipeline/producers/ae;

    iget-object v1, p0, Ldk;->p:Lcom/facebook/imagepipeline/bitmaps/e;

    iget-object v2, p0, Ldk;->i:Ldf;

    invoke-interface {v2}, Ldf;->d()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/facebook/imagepipeline/producers/ae;-><init>(Lcom/facebook/imagepipeline/producers/af;Lcom/facebook/imagepipeline/bitmaps/e;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public k(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/aj;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lds;",
            ">;)",
            "Lcom/facebook/imagepipeline/producers/aj;"
        }
    .end annotation

    .prologue
    .line 243
    new-instance v0, Lcom/facebook/imagepipeline/producers/aj;

    iget-object v1, p0, Ldk;->i:Ldf;

    invoke-interface {v1}, Ldf;->d()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Ldk;->j:Lcom/facebook/imagepipeline/memory/w;

    invoke-direct {v0, v1, v2, p1}, Lcom/facebook/imagepipeline/producers/aj;-><init>(Ljava/util/concurrent/Executor;Lcom/facebook/imagepipeline/memory/w;Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method

.method public l(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<TT;>;)",
            "Lcom/facebook/imagepipeline/producers/am",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 254
    new-instance v0, Lcom/facebook/imagepipeline/producers/am;

    iget-object v1, p0, Ldk;->i:Ldf;

    invoke-interface {v1}, Ldf;->e()Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/facebook/imagepipeline/producers/am;-><init>(Ljava/util/concurrent/Executor;Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method

.method public m(Lcom/facebook/imagepipeline/producers/af;)Lcom/facebook/imagepipeline/producers/ao;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/af",
            "<",
            "Lds;",
            ">;)",
            "Lcom/facebook/imagepipeline/producers/ao;"
        }
    .end annotation

    .prologue
    .line 270
    new-instance v0, Lcom/facebook/imagepipeline/producers/ao;

    iget-object v1, p0, Ldk;->i:Ldf;

    invoke-interface {v1}, Ldf;->d()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Ldk;->j:Lcom/facebook/imagepipeline/memory/w;

    invoke-direct {v0, v1, v2, p1}, Lcom/facebook/imagepipeline/producers/ao;-><init>(Ljava/util/concurrent/Executor;Lcom/facebook/imagepipeline/memory/w;Lcom/facebook/imagepipeline/producers/af;)V

    return-object v0
.end method
