.class public Lvy;
.super Lant;
.source "Twttr"


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;Lank;J)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lant;-><init>(Landroid/app/Activity;Lank;)V

    .line 29
    iput-wide p3, p0, Lvy;->a:J

    .line 30
    return-void
.end method

.method static a(Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 4

    .prologue
    .line 36
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 37
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "live_video_timeline"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 38
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 39
    return-object v0
.end method


# virtual methods
.method a()Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 6

    .prologue
    .line 46
    new-instance v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 47
    iget-wide v0, p0, Lvy;->a:J

    iput-wide v0, v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 48
    const/16 v0, 0x1c

    iput v0, v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 49
    new-instance v3, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    invoke-direct {v3}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;-><init>()V

    iget-wide v0, p0, Lvy;->a:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lvy;->a:J

    .line 50
    :goto_0
    invoke-virtual {v3, v0, v1}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    iput-object v0, v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ar:Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    .line 53
    return-object v2

    .line 49
    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method b()Laog;
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return-object v0
.end method
