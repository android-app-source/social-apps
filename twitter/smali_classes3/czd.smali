.class public Lczd;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lczi$f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lczd$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

.field private e:Lczd$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/exoplayer/upstream/BandwidthMeter;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lczd;->a:Landroid/content/Context;

    .line 72
    iput-object p2, p0, Lczd;->b:Ljava/lang/String;

    .line 73
    iput-object p3, p0, Lczd;->c:Ljava/lang/String;

    .line 74
    iput-object p4, p0, Lczd;->d:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    .line 75
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lczd;->e:Lczd$a;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lczd;->e:Lczd$a;

    invoke-virtual {v0}, Lczd$a;->b()V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lczd;->e:Lczd$a;

    .line 89
    :cond_0
    return-void
.end method

.method public a(Lczi;)V
    .locals 6

    .prologue
    .line 79
    new-instance v0, Lczd$a;

    iget-object v1, p0, Lczd;->a:Landroid/content/Context;

    iget-object v2, p0, Lczd;->b:Ljava/lang/String;

    iget-object v3, p0, Lczd;->c:Ljava/lang/String;

    iget-object v5, p0, Lczd;->d:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lczd$a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lczi;Lcom/google/android/exoplayer/upstream/BandwidthMeter;)V

    iput-object v0, p0, Lczd;->e:Lczd$a;

    .line 80
    iget-object v0, p0, Lczd;->e:Lczd$a;

    invoke-virtual {v0}, Lczd$a;->a()V

    .line 81
    return-void
.end method
