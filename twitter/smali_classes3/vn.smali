.class public Lvn;
.super Lath;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;
.implements Lvm;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lvn$a;
    }
.end annotation


# instance fields
.field private final b:Lvo;

.field private final c:Landroid/support/v4/app/FragmentManager;

.field private d:Ljava/lang/String;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lvn$a;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lath;-><init>(Lath$a;)V

    .line 55
    iget-object v0, p1, Lvn$a;->a:Lvo;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvo;

    iput-object v0, p0, Lvn;->b:Lvo;

    .line 56
    iget-object v0, p0, Lvn;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lvn;->c:Landroid/support/v4/app/FragmentManager;

    .line 57
    return-void
.end method

.method synthetic constructor <init>(Lvn$a;Lvn$1;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lvn;-><init>(Lvn$a;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lvn;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 86
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x1

    if-ne v0, p2, :cond_0

    .line 130
    iget-object v0, p0, Lvn;->b:Lvo;

    invoke-interface {v0, p3}, Lvo;->b(I)V

    .line 132
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lvn;->d:Ljava/lang/String;

    .line 74
    invoke-super {p0, p1}, Lath;->a(Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method protected aF_()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lvn;->a:Landroid/support/v4/app/FragmentActivity;

    const v1, 0x7f0a06c0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aG_()V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a06bd

    .line 91
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a07b3

    .line 92
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0262

    .line 93
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 94
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 95
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lvn;->c:Landroid/support/v4/app/FragmentManager;

    .line 96
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 97
    return-void
.end method

.method public aH_()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lvn;->b:Lvo;

    invoke-interface {v0}, Lvo;->i()V

    .line 102
    return-void
.end method

.method public aI_()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lvn;->e:Ljava/util/List;

    return-object v0
.end method

.method public aJ_()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lvn;->b:Lvo;

    invoke-interface {v0}, Lvo;->b()V

    .line 125
    return-void
.end method

.method public a_(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    iput-object p1, p0, Lvn;->e:Ljava/util/List;

    .line 111
    return-void
.end method

.method public e()Lcom/twitter/android/media/selection/a;
    .locals 0

    .prologue
    .line 80
    return-object p0
.end method

.method protected n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lvn;->d:Ljava/lang/String;

    return-object v0
.end method
