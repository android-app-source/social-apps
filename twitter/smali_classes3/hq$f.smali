.class public final Lhq$f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation


# static fields
.field public static final AdsAttrs:[I

.field public static final AdsAttrs_adSize:I = 0x0

.field public static final AdsAttrs_adSizes:I = 0x1

.field public static final AdsAttrs_adUnitId:I = 0x2

.field public static final LoadingImageView:[I

.field public static final LoadingImageView_circleCrop:I = 0x2

.field public static final LoadingImageView_imageAspectRatio:I = 0x1

.field public static final LoadingImageView_imageAspectRatioAdjust:I = 0x0

.field public static final MapAttrs:[I

.field public static final MapAttrs_ambientEnabled:I = 0x10

.field public static final MapAttrs_cameraBearing:I = 0x1

.field public static final MapAttrs_cameraMaxZoomPreference:I = 0x12

.field public static final MapAttrs_cameraMinZoomPreference:I = 0x11

.field public static final MapAttrs_cameraTargetLat:I = 0x2

.field public static final MapAttrs_cameraTargetLng:I = 0x3

.field public static final MapAttrs_cameraTilt:I = 0x4

.field public static final MapAttrs_cameraZoom:I = 0x5

.field public static final MapAttrs_latLngBoundsNorthEastLatitude:I = 0x15

.field public static final MapAttrs_latLngBoundsNorthEastLongitude:I = 0x16

.field public static final MapAttrs_latLngBoundsSouthWestLatitude:I = 0x13

.field public static final MapAttrs_latLngBoundsSouthWestLongitude:I = 0x14

.field public static final MapAttrs_liteMode:I = 0x6

.field public static final MapAttrs_mapType:I = 0x0

.field public static final MapAttrs_uiCompass:I = 0x7

.field public static final MapAttrs_uiMapToolbar:I = 0xf

.field public static final MapAttrs_uiRotateGestures:I = 0x8

.field public static final MapAttrs_uiScrollGestures:I = 0x9

.field public static final MapAttrs_uiTiltGestures:I = 0xa

.field public static final MapAttrs_uiZoomControls:I = 0xb

.field public static final MapAttrs_uiZoomGestures:I = 0xc

.field public static final MapAttrs_useViewLifecycle:I = 0xd

.field public static final MapAttrs_zOrderOnTop:I = 0xe

.field public static final SignInButton:[I

.field public static final SignInButton_buttonSize:I = 0x0

.field public static final SignInButton_colorScheme:I = 0x1

.field public static final SignInButton_scopeUris:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 209
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lhq$f;->AdsAttrs:[I

    .line 213
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lhq$f;->LoadingImageView:[I

    .line 217
    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lhq$f;->MapAttrs:[I

    .line 241
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lhq$f;->SignInButton:[I

    return-void

    .line 209
    :array_0
    .array-data 4
        0x7f01010d
        0x7f01010e
        0x7f01010f
    .end array-data

    .line 213
    :array_1
    .array-data 4
        0x7f01029e
        0x7f01029f
        0x7f0102a0
    .end array-data

    .line 217
    :array_2
    .array-data 4
        0x7f0102a1
        0x7f0102a2
        0x7f0102a3
        0x7f0102a4
        0x7f0102a5
        0x7f0102a6
        0x7f0102a7
        0x7f0102a8
        0x7f0102a9
        0x7f0102aa
        0x7f0102ab
        0x7f0102ac
        0x7f0102ad
        0x7f0102ae
        0x7f0102af
        0x7f0102b0
        0x7f0102b1
        0x7f0102b2
        0x7f0102b3
        0x7f0102b4
        0x7f0102b5
        0x7f0102b6
        0x7f0102b7
    .end array-data

    .line 241
    :array_3
    .array-data 4
        0x7f01035e
        0x7f01035f
        0x7f010360
    .end array-data
.end method
