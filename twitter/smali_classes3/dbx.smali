.class public abstract Ldbx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/a;


# instance fields
.field protected final a:Ltv/periscope/android/view/aj;


# direct methods
.method public constructor <init>(Ltv/periscope/android/view/aj;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Ldbx;->a:Ltv/periscope/android/view/aj;

    .line 16
    return-void
.end method


# virtual methods
.method public abstract a(Ltv/periscope/android/api/PsUser;)Z
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "You must call execute with a user!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public f()Ltv/periscope/android/view/c;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Ltv/periscope/android/view/c;->c:Ltv/periscope/android/view/c;

    return-object v0
.end method
