.class public Lzp;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lzp$c;,
        Lzp$b;,
        Lzp$a;
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;

.field private final c:Lzp$c;

.field private final d:Lzp$b;

.field private final e:Lzp$b;

.field private final f:Lzp$a;

.field private final g:Lcom/twitter/android/moments/ui/fullscreen/c;

.field private final h:Lbse;

.field private i:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/twitter/android/moments/ui/guide/r;


# direct methods
.method public constructor <init>(JLjava/lang/String;Lcom/twitter/android/moments/ui/fullscreen/c;)V
    .locals 7

    .prologue
    .line 82
    .line 83
    invoke-static {p1, p2}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 82
    invoke-static {v0}, Lbse;->a(Lcom/twitter/library/provider/t;)Lbse;

    move-result-object v6

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Lzp;-><init>(JLjava/lang/String;Lcom/twitter/android/moments/ui/fullscreen/c;Lbse;)V

    .line 84
    return-void
.end method

.method constructor <init>(JLjava/lang/String;Lcom/twitter/android/moments/ui/fullscreen/c;Lbse;)V
    .locals 3

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-static {}, Lrx/c;->d()Lrx/c;

    move-result-object v0

    iput-object v0, p0, Lzp;->i:Lrx/c;

    .line 77
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lzp;->j:Ljava/util/List;

    .line 89
    iput-wide p1, p0, Lzp;->a:J

    .line 90
    iput-object p3, p0, Lzp;->b:Ljava/lang/String;

    .line 91
    new-instance v0, Lzp$c;

    const-string/jumbo v1, "list"

    invoke-direct {v0, p0, v1}, Lzp$c;-><init>(Lzp;Ljava/lang/String;)V

    iput-object v0, p0, Lzp;->c:Lzp$c;

    .line 92
    new-instance v0, Lzp$b;

    const-string/jumbo v1, "hero"

    invoke-direct {v0, p0, v1}, Lzp$b;-><init>(Lzp;Ljava/lang/String;)V

    iput-object v0, p0, Lzp;->d:Lzp$b;

    .line 93
    new-instance v0, Lzp$b;

    const-string/jumbo v1, "superhero"

    invoke-direct {v0, p0, v1}, Lzp$b;-><init>(Lzp;Ljava/lang/String;)V

    iput-object v0, p0, Lzp;->e:Lzp$b;

    .line 94
    new-instance v0, Lzp$a;

    const-string/jumbo v1, "carousel"

    invoke-direct {v0, p0, v1}, Lzp$a;-><init>(Lzp;Ljava/lang/String;)V

    iput-object v0, p0, Lzp;->f:Lzp$a;

    .line 95
    iput-object p4, p0, Lzp;->g:Lcom/twitter/android/moments/ui/fullscreen/c;

    .line 96
    iput-object p5, p0, Lzp;->h:Lbse;

    .line 97
    return-void
.end method

.method static synthetic a(Lzp;)Lrx/c;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lzp;->i:Lrx/c;

    return-object v0
.end method

.method public static a(J)V
    .locals 4

    .prologue
    .line 123
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "moments:modern_guide:::open"

    aput-object v3, v1, v2

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 124
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 152
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 153
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    .line 154
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lzp;->a:J

    new-array v4, v6, [Ljava/lang/String;

    const-string/jumbo v5, "moments:modern_guide:category:%s:navigate"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p2, v6, v7

    .line 155
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    .line 156
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 154
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 157
    return-void
.end method

.method private b(J)Lcpv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcpv",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    new-instance v0, Lzp$1;

    invoke-direct {v0, p0, p1, p2}, Lzp$1;-><init>(Lzp;J)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/DisplayStyle;)Lzp$a;
    .locals 3

    .prologue
    .line 161
    sget-object v0, Lzp$2;->a:[I

    invoke-virtual {p1}, Lcom/twitter/model/moments/DisplayStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 169
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unrecognized type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :pswitch_0
    invoke-virtual {p0}, Lzp;->c()Lzp$c;

    move-result-object v0

    .line 166
    :goto_0
    return-object v0

    :pswitch_1
    invoke-virtual {p0}, Lzp;->d()Lzp$b;

    move-result-object v0

    goto :goto_0

    .line 161
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()V
    .locals 6

    .prologue
    .line 133
    iget-object v0, p0, Lzp;->g:Lcom/twitter/android/moments/ui/fullscreen/c;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/c;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lzp;->a:J

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "moments:modern_guide:::impression"

    aput-object v5, v1, v4

    invoke-direct {v0, v2, v3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 136
    :cond_0
    return-void
.end method

.method a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 211
    iget-object v0, p0, Lzp;->g:Lcom/twitter/android/moments/ui/fullscreen/c;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/c;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 212
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 213
    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    iget-object v1, p0, Lzp;->b:Ljava/lang/String;

    .line 214
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v1

    .line 215
    if-eqz p5, :cond_0

    .line 216
    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->c(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 218
    :cond_0
    iget-object v0, p0, Lzp;->j:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lzp;->b(J)Lcpv;

    move-result-object v2

    invoke-static {v0, v2}, Lcpt;->c(Ljava/lang/Iterable;Lcpv;)I

    move-result v2

    .line 219
    const/4 v0, -0x1

    if-eq v2, v0, :cond_1

    .line 220
    iget-object v0, p0, Lzp;->j:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule;

    .line 221
    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentModule;->f()Lceg;

    move-result-object v3

    .line 222
    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentModule;->i()Lcom/twitter/model/moments/u;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/model/moments/u;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 223
    if-eqz v3, :cond_4

    iget-object v0, v3, Lceg;->c:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 225
    :cond_1
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lzp;->a:J

    new-array v0, v6, [Ljava/lang/String;

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p4, v6, v7

    invoke-static {p3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v7

    invoke-direct {v3, v4, v5, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    .line 227
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 226
    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 228
    if-ltz v2, :cond_2

    .line 229
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->g(I)Lcom/twitter/analytics/model/ScribeLog;

    .line 231
    :cond_2
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 233
    :cond_3
    return-void

    .line 223
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/moments/ui/guide/r;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lzp;->k:Lcom/twitter/android/moments/ui/guide/r;

    .line 106
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/MomentGuide;)V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lzp;->h:Lbse;

    iget-object v1, p0, Lzp;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbse;->a(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->g()Lrx/c;

    move-result-object v0

    iput-object v0, p0, Lzp;->i:Lrx/c;

    .line 115
    iget-object v0, p1, Lcom/twitter/android/moments/viewmodels/MomentGuide;->c:Ljava/util/List;

    invoke-static {v0}, Lcpt;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lzp;->j:Ljava/util/List;

    .line 116
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 139
    const-string/jumbo v0, "header"

    invoke-direct {p0, p1, v0}, Lzp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    .line 147
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lzp;->a:J

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "moments:modern_guide:category:category_item:impression"

    aput-object v5, v1, v4

    invoke-direct {v0, v2, v3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 148
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 143
    const-string/jumbo v0, "footer"

    invoke-direct {p0, p1, v0}, Lzp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    return-void
.end method

.method public c()Lzp$c;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lzp;->c:Lzp$c;

    return-object v0
.end method

.method public d()Lzp$b;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lzp;->k:Lcom/twitter/android/moments/ui/guide/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzp;->k:Lcom/twitter/android/moments/ui/guide/r;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/r;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lzp;->e:Lzp$b;

    .line 189
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lzp;->d:Lzp$b;

    goto :goto_0
.end method
