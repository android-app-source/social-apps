.class public Lcyx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcyw;


# static fields
.field static final a:Lcom/google/gson/e;


# instance fields
.field b:Ltv/periscope/android/api/PsUser;

.field protected final c:Landroid/content/SharedPreferences;

.field protected final d:Lde/greenrobot/event/c;

.field final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ltv/periscope/model/user/UserType;",
            "Lcyv;",
            ">;"
        }
    .end annotation
.end field

.field final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ltv/periscope/model/user/UserType;",
            "Lcyv;",
            ">;>;"
        }
    .end annotation
.end field

.field final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcyv;",
            ">;"
        }
    .end annotation
.end field

.field final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ltv/periscope/android/api/PsUser;",
            ">;"
        }
    .end annotation
.end field

.field final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ltv/periscope/model/user/UserType;",
            "Ltv/periscope/android/event/CacheEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcyn;

.field private final k:Ldbb;

.field private final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/ac;",
            ">;>;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/ac;",
            ">;>;"
        }
    .end annotation
.end field

.field private final n:Ldbr;

.field private final o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ltv/periscope/model/ab;",
            "Ltv/periscope/model/aa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lcom/google/gson/f;

    invoke-direct {v0}, Lcom/google/gson/f;-><init>()V

    new-instance v1, Ltv/periscope/android/api/SafeListAdapter;

    invoke-direct {v1}, Ltv/periscope/android/api/SafeListAdapter;-><init>()V

    .line 45
    invoke-virtual {v0, v1}, Lcom/google/gson/f;->a(Lcom/google/gson/s;)Lcom/google/gson/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/f;->a()Lcom/google/gson/e;

    move-result-object v0

    sput-object v0, Lcyx;->a:Lcom/google/gson/e;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Lcyn;Lde/greenrobot/event/c;Ldbr;)V
    .locals 6

    .prologue
    .line 84
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcyx;-><init>(Landroid/content/SharedPreferences;Lcyn;Lde/greenrobot/event/c;Ldbr;Ljava/util/Map;)V

    .line 85
    return-void
.end method

.method constructor <init>(Landroid/content/SharedPreferences;Lcyn;Lde/greenrobot/event/c;Ldbr;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcyn;",
            "Lde/greenrobot/event/c;",
            "Ldbr;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ltv/periscope/android/api/PsUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Ltv/periscope/model/user/UserType;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcyx;->e:Ljava/util/Map;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcyx;->f:Ljava/util/Map;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcyx;->g:Ljava/util/Map;

    .line 57
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Ltv/periscope/model/user/UserType;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcyx;->i:Ljava/util/Map;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcyx;->l:Ljava/util/Map;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcyx;->m:Ljava/util/Map;

    .line 67
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcyx;->o:Ljava/util/Set;

    .line 73
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcyx;->p:Ljava/util/Set;

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcyx;->q:Ljava/util/Map;

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcyx;->r:Ljava/util/Map;

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcyx;->s:Ljava/util/Map;

    .line 90
    iput-object p1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    .line 91
    iput-object p2, p0, Lcyx;->j:Lcyn;

    .line 92
    iput-object p3, p0, Lcyx;->d:Lde/greenrobot/event/c;

    .line 93
    iput-object p4, p0, Lcyx;->n:Ldbr;

    .line 94
    iput-object p5, p0, Lcyx;->h:Ljava/util/Map;

    .line 95
    new-instance v0, Ldbc;

    invoke-direct {v0}, Ldbc;-><init>()V

    iput-object v0, p0, Lcyx;->k:Ldbb;

    .line 96
    invoke-direct {p0}, Lcyx;->k()V

    .line 97
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)Ljava/util/Set;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/PsUser;",
            ">;Z)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 605
    if-eqz p3, :cond_2

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 606
    new-instance v9, Ljava/util/TreeSet;

    invoke-direct {v9}, Ljava/util/TreeSet;-><init>()V

    .line 607
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ltv/periscope/android/api/PsUser;

    .line 608
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    iget-object v1, v8, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 609
    iget-object v0, v8, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v8, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    iget-object v1, v8, Ltv/periscope/android/api/PsUser;->twitterId:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcyx;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 612
    iget-object v0, v8, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-interface {v9, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 613
    iget-object v2, v8, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    .line 614
    invoke-virtual {v8}, Ltv/periscope/android/api/PsUser;->getNumHeartsGiven()J

    move-result-wide v4

    invoke-virtual {v8}, Ltv/periscope/android/api/PsUser;->getParticipantIndex()J

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move v3, p4

    .line 613
    invoke-direct/range {v0 .. v7}, Lcyx;->a(Ljava/lang/String;Ljava/lang/String;ZJJ)Ltv/periscope/model/aa;

    move-result-object v0

    .line 615
    iget-object v1, p0, Lcyx;->s:Ljava/util/Map;

    iget-object v2, v8, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-static {p1, v2, p4}, Ltv/periscope/model/ab;->a(Ljava/lang/String;Ljava/lang/String;Z)Ltv/periscope/model/ab;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object v0, v9

    .line 619
    :goto_1
    return-object v0

    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ZJJ)Ltv/periscope/model/aa;
    .locals 8

    .prologue
    .line 576
    invoke-virtual {p0, p1, p2, p3}, Lcyx;->a(Ljava/lang/String;Ljava/lang/String;Z)Ltv/periscope/model/aa;

    move-result-object v1

    .line 577
    if-nez v1, :cond_0

    .line 578
    new-instance v1, Ltv/periscope/model/aa;

    move-wide v2, p4

    move-wide v4, p6

    move v6, p3

    invoke-direct/range {v1 .. v6}, Ltv/periscope/model/aa;-><init>(JJZ)V

    .line 580
    :cond_0
    return-object v1
.end method

.method private k()V
    .locals 5

    .prologue
    .line 101
    iget-object v0, p0, Lcyx;->o:Ljava/util/Set;

    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->j:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 103
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->a:Ltv/periscope/model/user/UserType;

    new-instance v2, Lcyv;

    sget-object v3, Ltv/periscope/model/user/UserType;->a:Ltv/periscope/model/user/UserType;

    iget-object v4, p0, Lcyx;->h:Ljava/util/Map;

    invoke-direct {v2, v3, v4}, Lcyv;-><init>(Ltv/periscope/model/user/UserType;Ljava/util/Map;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->b:Ltv/periscope/model/user/UserType;

    new-instance v2, Lcyv;

    sget-object v3, Ltv/periscope/model/user/UserType;->b:Ltv/periscope/model/user/UserType;

    iget-object v4, p0, Lcyx;->h:Ljava/util/Map;

    invoke-direct {v2, v3, v4}, Lcyv;-><init>(Ltv/periscope/model/user/UserType;Ljava/util/Map;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->c:Ltv/periscope/model/user/UserType;

    new-instance v2, Lcyv;

    sget-object v3, Ltv/periscope/model/user/UserType;->c:Ltv/periscope/model/user/UserType;

    iget-object v4, p0, Lcyx;->h:Ljava/util/Map;

    invoke-direct {v2, v3, v4}, Lcyv;-><init>(Ltv/periscope/model/user/UserType;Ljava/util/Map;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->d:Ltv/periscope/model/user/UserType;

    new-instance v2, Lcyv;

    sget-object v3, Ltv/periscope/model/user/UserType;->d:Ltv/periscope/model/user/UserType;

    iget-object v4, p0, Lcyx;->h:Ljava/util/Map;

    invoke-direct {v2, v3, v4}, Lcyv;-><init>(Ltv/periscope/model/user/UserType;Ljava/util/Map;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    iget-object v0, p0, Lcyx;->i:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->a:Ltv/periscope/model/user/UserType;

    sget-object v2, Ltv/periscope/android/event/CacheEvent;->g:Ltv/periscope/android/event/CacheEvent;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lcyx;->i:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->b:Ltv/periscope/model/user/UserType;

    sget-object v2, Ltv/periscope/android/event/CacheEvent;->h:Ltv/periscope/android/event/CacheEvent;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v0, p0, Lcyx;->i:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->d:Ltv/periscope/model/user/UserType;

    sget-object v2, Ltv/periscope/android/event/CacheEvent;->u:Ltv/periscope/android/event/CacheEvent;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    return-void
.end method

.method private l()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/PsProfileImageUrl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v1, Lcyy;->k:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 220
    if-eqz v0, :cond_0

    .line 222
    :try_start_0
    sget-object v1, Lcyx;->a:Lcom/google/gson/e;

    const-class v2, Ltv/periscope/android/api/PsProfileImageUrls;

    invoke-virtual {v1, v0, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsProfileImageUrls;

    .line 223
    iget-object v0, v0, Ltv/periscope/android/api/PsProfileImageUrls;->profileImageUrls:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :goto_0
    return-object v0

    .line 224
    :catch_0
    move-exception v0

    .line 227
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ltv/periscope/model/user/UserType;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ltv/periscope/model/user/UserType;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 334
    invoke-static {p1}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcyx;->b()Ltv/periscope/android/api/PsUser;

    move-result-object v0

    iget-object v0, v0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 335
    :cond_0
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    .line 336
    invoke-virtual {v0}, Lcyv;->a()Ljava/util/List;

    move-result-object v0

    .line 342
    :goto_0
    return-object v0

    .line 338
    :cond_1
    iget-object v0, p0, Lcyx;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 339
    if-eqz v0, :cond_2

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    .line 340
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 342
    :cond_3
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    invoke-virtual {v0}, Lcyv;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Z)Ltv/periscope/model/aa;
    .locals 2

    .prologue
    .line 393
    iget-object v0, p0, Lcyx;->s:Ljava/util/Map;

    invoke-static {p1, p2, p3}, Ltv/periscope/model/ab;->a(Ljava/lang/String;Ljava/lang/String;Z)Ltv/periscope/model/ab;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/aa;

    return-object v0
.end method

.method public a()V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 785
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    .line 786
    invoke-virtual {v0}, Lcyv;->b()V

    goto :goto_0

    .line 788
    :cond_0
    iget-object v0, p0, Lcyx;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 789
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 791
    iget-object v0, p0, Lcyx;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 792
    iget-object v0, p0, Lcyx;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 793
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 9

    .prologue
    .line 454
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p5

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Lcyx;->a(Ljava/lang/String;Ljava/lang/String;ZJJ)Ltv/periscope/model/aa;

    move-result-object v1

    .line 455
    iget-wide v2, v1, Ltv/periscope/model/aa;->b:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Ltv/periscope/model/aa;->b:J

    .line 456
    iget-object v0, p0, Lcyx;->s:Ljava/util/Map;

    invoke-static {p1, p2, p5}, Ltv/periscope/model/ab;->a(Ljava/lang/String;Ljava/lang/String;Z)Ltv/periscope/model/ab;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    iget-object v6, p0, Lcyx;->d:Lde/greenrobot/event/c;

    new-instance v0, Ltv/periscope/android/event/ParticipantHeartCountEvent;

    iget-wide v4, v1, Ltv/periscope/model/aa;->b:J

    move-object v1, p1

    move-object v2, p2

    move v3, p5

    invoke-direct/range {v0 .. v5}, Ltv/periscope/android/event/ParticipantHeartCountEvent;-><init>(Ljava/lang/String;Ljava/lang/String;ZJ)V

    invoke-virtual {v6, v0}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 458
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/chatman/api/Occupant;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 420
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v9

    .line 422
    new-instance v10, Ljava/util/ArrayList;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 423
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ltv/periscope/chatman/api/Occupant;

    .line 424
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    iget-object v1, v8, Ltv/periscope/chatman/api/Occupant;->userId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 426
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    iget-object v1, v8, Ltv/periscope/chatman/api/Occupant;->userId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    :goto_1
    iget-object v2, v8, Ltv/periscope/chatman/api/Occupant;->userId:Ljava/lang/String;

    const-wide/16 v4, 0x0

    iget-wide v6, v8, Ltv/periscope/chatman/api/Occupant;->participantIndex:J

    move-object v0, p0

    move-object v1, p2

    .line 441
    invoke-direct/range {v0 .. v7}, Lcyx;->a(Ljava/lang/String;Ljava/lang/String;ZJJ)Ltv/periscope/model/aa;

    move-result-object v0

    .line 442
    iget-object v1, p0, Lcyx;->s:Ljava/util/Map;

    iget-object v2, v8, Ltv/periscope/chatman/api/Occupant;->userId:Ljava/lang/String;

    invoke-static {p2, v2, v3}, Ltv/periscope/model/ab;->a(Ljava/lang/String;Ljava/lang/String;Z)Ltv/periscope/model/ab;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 428
    :cond_1
    iget-object v0, v8, Ltv/periscope/chatman/api/Occupant;->displayName:Ljava/lang/String;

    invoke-static {v0}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 432
    new-instance v0, Ltv/periscope/android/api/PsUser;

    invoke-direct {v0}, Ltv/periscope/android/api/PsUser;-><init>()V

    .line 433
    iget-object v1, v8, Ltv/periscope/chatman/api/Occupant;->displayName:Ljava/lang/String;

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    .line 435
    iget-object v1, v8, Ltv/periscope/chatman/api/Occupant;->profileImageUrl:Ljava/lang/String;

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->profileUrlLarge:Ljava/lang/String;

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->profileUrlMedium:Ljava/lang/String;

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->profileUrlSmall:Ljava/lang/String;

    .line 436
    iget-object v1, v8, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    .line 437
    iget-object v1, v8, Ltv/periscope/chatman/api/Occupant;->userId:Ljava/lang/String;

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    .line 438
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 446
    :cond_2
    iget-object v0, p0, Lcyx;->j:Lcyn;

    add-int/lit8 v1, v9, -0x1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-interface {v0, p2, v1}, Lcyn;->a(Ljava/lang/String;I)V

    .line 448
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v10, v0}, Lcyx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    .line 449
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/PsUser;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/PsUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 594
    iget-object v0, p0, Lcyx;->q:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-direct {p0, p2, p1, p3, v1}, Lcyx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 596
    iget-object v0, p0, Lcyx;->r:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-direct {p0, p2, p1, p4, v1}, Lcyx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 600
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->o:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 601
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/PsUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 398
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->c:Ltv/periscope/model/user/UserType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    .line 400
    if-eqz p1, :cond_0

    .line 401
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/api/PsUser;

    .line 402
    const/4 v3, 0x1

    iput-boolean v3, v1, Ltv/periscope/android/api/PsUser;->isFollowing:Z

    goto :goto_0

    .line 405
    :cond_0
    invoke-virtual {v0, p1}, Lcyv;->a(Ljava/util/List;)V

    .line 406
    return-void
.end method

.method public a(Ltv/periscope/android/api/PsUser;)V
    .locals 6

    .prologue
    .line 178
    new-instance v0, Ltv/periscope/android/api/PsProfileImageUrls;

    invoke-direct {v0}, Ltv/periscope/android/api/PsProfileImageUrls;-><init>()V

    .line 179
    iget-object v1, p1, Ltv/periscope/android/api/PsUser;->profileImageUrls:Ljava/util/ArrayList;

    iput-object v1, v0, Ltv/periscope/android/api/PsProfileImageUrls;->profileImageUrls:Ljava/util/List;

    .line 181
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 182
    sget-object v2, Lcyy;->b:Ljava/lang/String;

    iget-object v3, p1, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 183
    sget-object v2, Lcyy;->a:Ljava/lang/String;

    iget-object v3, p1, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 184
    sget-object v2, Lcyy;->e:Ljava/lang/String;

    iget-object v3, p1, Ltv/periscope/android/api/PsUser;->description:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 185
    sget-object v2, Lcyy;->c:Ljava/lang/String;

    iget-object v3, p1, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 186
    sget-object v2, Lcyy;->d:Ljava/lang/String;

    iget-object v3, p1, Ltv/periscope/android/api/PsUser;->initials:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 187
    sget-object v2, Lcyy;->k:Ljava/lang/String;

    sget-object v3, Lcyx;->a:Lcom/google/gson/e;

    invoke-virtual {v3, v0}, Lcom/google/gson/e;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 188
    sget-object v0, Lcyy;->m:Ljava/lang/String;

    iget-object v2, p1, Ltv/periscope/android/api/PsUser;->vipBadge:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 189
    sget-object v0, Lcyy;->f:Ljava/lang/String;

    iget-wide v2, p1, Ltv/periscope/android/api/PsUser;->numFollowers:J

    invoke-interface {v1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 190
    sget-object v0, Lcyy;->g:Ljava/lang/String;

    iget-wide v2, p1, Ltv/periscope/android/api/PsUser;->numFollowing:J

    invoke-interface {v1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 191
    sget-object v0, Lcyy;->i:Ljava/lang/String;

    iget-wide v2, p1, Ltv/periscope/android/api/PsUser;->numHearts:J

    const-wide/16 v4, 0x1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-interface {v1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 192
    sget-object v0, Lcyy;->o:Ljava/lang/String;

    iget-boolean v2, p1, Ltv/periscope/android/api/PsUser;->isEmployee:Z

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 193
    sget-object v0, Lcyy;->p:Ljava/lang/String;

    iget-boolean v2, p1, Ltv/periscope/android/api/PsUser;->hasDigitsId:Z

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 194
    sget-object v0, Lcyy;->l:Ljava/lang/String;

    iget-boolean v2, p1, Ltv/periscope/android/api/PsUser;->isVerified:Z

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 195
    sget-object v0, Lcyy;->q:Ljava/lang/String;

    iget-boolean v2, p1, Ltv/periscope/android/api/PsUser;->isBluebirdUser:Z

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 196
    sget-object v0, Lcyy;->r:Ljava/lang/String;

    iget-object v2, p1, Ltv/periscope/android/api/PsUser;->twitterId:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 197
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcyx;->b:Ltv/periscope/android/api/PsUser;

    .line 200
    invoke-virtual {p0}, Lcyx;->b()Ltv/periscope/android/api/PsUser;

    .line 202
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->f:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 203
    return-void
.end method

.method public a(Ltv/periscope/model/user/f;)V
    .locals 3

    .prologue
    .line 252
    invoke-virtual {p0}, Lcyx;->c()Ljava/lang/String;

    move-result-object v0

    .line 253
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ltv/periscope/model/user/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 257
    :cond_1
    invoke-virtual {p1}, Ltv/periscope/model/user/f;->b()Ljava/lang/Boolean;

    move-result-object v0

    .line 258
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 259
    if-eqz v0, :cond_0

    .line 260
    sget-object v2, Lcyy;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 261
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->c:Ltv/periscope/model/user/UserType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    invoke-virtual {v0, p1}, Lcyv;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcyx;->o:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    invoke-static {p2}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcyx;->n:Ldbr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcyx;->n:Ldbr;

    invoke-interface {v0, p2}, Ldbr;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 280
    :goto_0
    return v0

    .line 281
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ltv/periscope/android/api/PsUser;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 150
    iget-object v0, p0, Lcyx;->b:Ltv/periscope/android/api/PsUser;

    if-nez v0, :cond_0

    .line 151
    new-instance v0, Ltv/periscope/android/api/PsUser;

    invoke-direct {v0}, Ltv/periscope/android/api/PsUser;-><init>()V

    .line 152
    invoke-virtual {p0}, Lcyx;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    .line 153
    invoke-virtual {p0}, Lcyx;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    .line 154
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->e:Ljava/lang/String;

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->description:Ljava/lang/String;

    .line 155
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    .line 156
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->d:Ljava/lang/String;

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->initials:Ljava/lang/String;

    .line 157
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {p0}, Lcyx;->l()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->profileImageUrls:Ljava/util/ArrayList;

    .line 158
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->f:Ljava/lang/String;

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v0, Ltv/periscope/android/api/PsUser;->numFollowers:J

    .line 159
    invoke-virtual {p0}, Lcyx;->i()J

    move-result-wide v2

    iput-wide v2, v0, Ltv/periscope/android/api/PsUser;->numFollowing:J

    .line 160
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->i:Ljava/lang/String;

    const-wide/16 v4, 0x1

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v0, Ltv/periscope/android/api/PsUser;->numHearts:J

    .line 161
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->o:Ljava/lang/String;

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Ltv/periscope/android/api/PsUser;->isEmployee:Z

    .line 162
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->p:Ljava/lang/String;

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Ltv/periscope/android/api/PsUser;->hasDigitsId:Z

    .line 163
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->l:Ljava/lang/String;

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Ltv/periscope/android/api/PsUser;->isVerified:Z

    .line 164
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->m:Ljava/lang/String;

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->vipBadge:Ljava/lang/String;

    .line 165
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->q:Ljava/lang/String;

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Ltv/periscope/android/api/PsUser;->isBluebirdUser:Z

    .line 166
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->r:Ljava/lang/String;

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->twitterId:Ljava/lang/String;

    .line 167
    iput-object v0, p0, Lcyx;->b:Ltv/periscope/android/api/PsUser;

    .line 173
    :goto_0
    iget-object v0, p0, Lcyx;->b:Ltv/periscope/android/api/PsUser;

    return-object v0

    .line 169
    :cond_0
    iget-object v0, p0, Lcyx;->b:Ltv/periscope/android/api/PsUser;

    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    .line 170
    iget-object v0, p0, Lcyx;->b:Ltv/periscope/android/api/PsUser;

    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->e:Ljava/lang/String;

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->description:Ljava/lang/String;

    .line 171
    iget-object v0, p0, Lcyx;->b:Ltv/periscope/android/api/PsUser;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {p0}, Lcyx;->l()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Ltv/periscope/android/api/PsUser;->profileImageUrls:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/PsUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 410
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->d:Ltv/periscope/model/user/UserType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    .line 411
    invoke-virtual {v0, p1}, Lcyv;->a(Ljava/util/List;)V

    .line 412
    invoke-virtual {p0}, Lcyx;->j()V

    .line 414
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->u:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 415
    return-void
.end method

.method public b(Ltv/periscope/android/api/PsUser;)V
    .locals 2

    .prologue
    .line 636
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    iget-object v1, p1, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 637
    iget-boolean v0, p1, Ltv/periscope/android/api/PsUser;->isFollowing:Z

    if-eqz v0, :cond_0

    .line 638
    iget-object v0, p1, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcyx;->m(Ljava/lang/String;)V

    .line 640
    :cond_0
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->i:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 641
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 305
    invoke-virtual {p0}, Lcyx;->c()Ljava/lang/String;

    move-result-object v0

    .line 306
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 291
    iget-object v0, p0, Lcyx;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return v1

    .line 293
    :cond_1
    invoke-static {p2}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcyx;->n:Ldbr;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcyx;->n:Ldbr;

    invoke-interface {v0, p2}, Ldbr;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    :cond_2
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 299
    if-eqz v0, :cond_3

    iget-boolean v0, v0, Ltv/periscope/android/api/PsUser;->isFollowing:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public synthetic c(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcyx;->p(Ljava/lang/String;)Ltv/periscope/android/api/PsUser;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 208
    iget-object v0, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v1, Lcyy;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcyx;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 133
    iget-object v0, p0, Lcyx;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 134
    return-void
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 705
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcyx;->l:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 715
    :goto_0
    return v0

    .line 708
    :cond_1
    iget-object v0, p0, Lcyx;->l:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 709
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/ac;

    .line 710
    invoke-virtual {v0}, Ltv/periscope/model/ac;->a()Ltv/periscope/model/user/UserItem;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    iget-object v0, v0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    .line 711
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 712
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 715
    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 214
    iget-object v0, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v1, Lcyy;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366
    iget-object v0, p0, Lcyx;->q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcyx;->q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public d(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcyx;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 118
    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 233
    invoke-direct {p0}, Lcyx;->l()Ljava/util/List;

    move-result-object v0

    .line 234
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 235
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsProfileImageUrl;

    iget-object v0, v0, Ltv/periscope/android/api/PsProfileImageUrl;->url:Ljava/lang/String;

    .line 237
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 372
    iget-object v0, p0, Lcyx;->r:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcyx;->r:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public e(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lcyx;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 139
    return-void
.end method

.method public f()Ltv/periscope/model/user/f;
    .locals 4

    .prologue
    .line 268
    invoke-virtual {p0}, Lcyx;->c()Ljava/lang/String;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->n:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 270
    :cond_0
    const/4 v0, 0x0

    .line 273
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v2, Lcyy;->n:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/model/user/f;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ltv/periscope/model/user/f;

    move-result-object v0

    goto :goto_0
.end method

.method public f(Ljava/lang/String;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 468
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 469
    if-nez v0, :cond_1

    .line 484
    :cond_0
    :goto_0
    return-void

    .line 472
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, v0, Ltv/periscope/android/api/PsUser;->isFollowing:Z

    .line 473
    iget-wide v2, v0, Ltv/periscope/android/api/PsUser;->numFollowers:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Ltv/periscope/android/api/PsUser;->numFollowers:J

    .line 476
    invoke-virtual {p0, p1}, Lcyx;->m(Ljava/lang/String;)V

    .line 477
    iget-object v0, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcyy;->g:Ljava/lang/String;

    invoke-virtual {p0}, Lcyx;->i()J

    move-result-wide v2

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 478
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->b:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 479
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->f:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 481
    invoke-virtual {p0, p1}, Lcyx;->o(Ljava/lang/String;)Ltv/periscope/model/user/UserType;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/user/UserType;->i:Ltv/periscope/model/user/UserType;

    if-ne v0, v1, :cond_0

    .line 482
    sget-object v0, Ltv/periscope/android/analytics/Event;->aZ:Ltv/periscope/android/analytics/Event;

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/Event;)V

    goto :goto_0
.end method

.method public g()Ldbb;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcyx;->k:Ldbb;

    return-object v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 488
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 489
    if-nez v0, :cond_0

    .line 507
    :goto_0
    return-void

    .line 492
    :cond_0
    iput-boolean v1, v0, Ltv/periscope/android/api/PsUser;->isFollowing:Z

    .line 493
    iput-boolean v1, v0, Ltv/periscope/android/api/PsUser;->isMuted:Z

    .line 494
    invoke-virtual {p0}, Lcyx;->i()J

    move-result-wide v2

    sub-long/2addr v2, v8

    .line 495
    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    move-wide v2, v4

    .line 500
    :cond_1
    iget-object v1, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v6, Ltv/periscope/model/user/UserType;->c:Ltv/periscope/model/user/UserType;

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcyv;

    invoke-virtual {v1, p1}, Lcyv;->b(Ljava/lang/String;)Z

    .line 501
    invoke-virtual {p0, p1}, Lcyx;->n(Ljava/lang/String;)V

    .line 503
    iget-wide v6, v0, Ltv/periscope/android/api/PsUser;->numFollowers:J

    sub-long/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, v0, Ltv/periscope/android/api/PsUser;->numFollowers:J

    .line 504
    iget-object v0, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcyy;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 505
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->c:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 506
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->f:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 778
    const/4 v0, 0x0

    iput-object v0, p0, Lcyx;->b:Ltv/periscope/android/api/PsUser;

    .line 779
    invoke-virtual {p0}, Lcyx;->a()V

    .line 780
    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 511
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 512
    const/4 v1, 0x1

    iput-boolean v1, v0, Ltv/periscope/android/api/PsUser;->isMuted:Z

    .line 513
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->d:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 514
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->f:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 515
    return-void
.end method

.method public i()J
    .locals 4

    .prologue
    .line 243
    iget-object v0, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    sget-object v1, Lcyy;->g:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public i(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 519
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 520
    const/4 v1, 0x0

    iput-boolean v1, v0, Ltv/periscope/android/api/PsUser;->isMuted:Z

    .line 521
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->e:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 522
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->f:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 523
    return-void
.end method

.method j()V
    .locals 4

    .prologue
    .line 624
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->d:Ltv/periscope/model/user/UserType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    .line 626
    iget-object v1, p0, Lcyx;->o:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 627
    iget-object v1, p0, Lcyx;->o:Ljava/util/Set;

    invoke-virtual {v0}, Lcyv;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 628
    iget-object v0, p0, Lcyx;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 629
    sget-object v1, Lcyy;->h:Ljava/lang/String;

    iget-object v2, p0, Lcyx;->o:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 630
    sget-object v1, Lcyy;->j:Ljava/lang/String;

    iget-object v2, p0, Lcyx;->o:Ljava/util/Set;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 631
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 632
    return-void
.end method

.method public j(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 527
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 528
    if-eqz v0, :cond_1

    .line 529
    iget-boolean v1, v0, Ltv/periscope/android/api/PsUser;->isFollowing:Z

    if-eqz v1, :cond_0

    .line 533
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v0, Ltv/periscope/android/api/PsUser;->isBlocked:Z

    .line 534
    iput-boolean v2, v0, Ltv/periscope/android/api/PsUser;->isFollowing:Z

    .line 535
    iput-boolean v2, v0, Ltv/periscope/android/api/PsUser;->isMuted:Z

    .line 537
    :cond_1
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->d:Ltv/periscope/model/user/UserType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    .line 538
    invoke-virtual {v0, p1}, Lcyv;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 539
    invoke-virtual {v0, p1}, Lcyv;->a(Ljava/lang/String;)V

    .line 541
    :cond_2
    invoke-virtual {p0}, Lcyx;->j()V

    .line 542
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->c:Ltv/periscope/model/user/UserType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    invoke-virtual {v0, p1}, Lcyv;->b(Ljava/lang/String;)Z

    .line 544
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->s:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 545
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->f:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 546
    return-void
.end method

.method public k(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 550
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    .line 551
    iput-boolean v1, v0, Ltv/periscope/android/api/PsUser;->isBlocked:Z

    .line 552
    iput-boolean v1, v0, Ltv/periscope/android/api/PsUser;->isFollowing:Z

    .line 554
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->d:Ltv/periscope/model/user/UserType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    .line 555
    invoke-virtual {v0, p1}, Lcyv;->b(Ljava/lang/String;)Z

    .line 556
    invoke-virtual {p0}, Lcyx;->j()V

    .line 558
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->t:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 559
    iget-object v0, p0, Lcyx;->d:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->f:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 560
    return-void
.end method

.method public l(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcyx;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcyx;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcyx;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    :goto_0
    return-void

    .line 125
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 126
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    invoke-virtual {p0, v0}, Lcyx;->d(Ljava/util/List;)V

    goto :goto_0
.end method

.method public n(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 142
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 143
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    invoke-virtual {p0, v0}, Lcyx;->e(Ljava/util/List;)V

    .line 145
    return-void
.end method

.method public o(Ljava/lang/String;)Ltv/periscope/model/user/UserType;
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->f:Ltv/periscope/model/user/UserType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    invoke-virtual {v0, p1}, Lcyv;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    sget-object v0, Ltv/periscope/model/user/UserType;->f:Ltv/periscope/model/user/UserType;

    .line 328
    :goto_0
    return-object v0

    .line 316
    :cond_0
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->e:Ltv/periscope/model/user/UserType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    invoke-virtual {v0, p1}, Lcyv;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317
    sget-object v0, Ltv/periscope/model/user/UserType;->e:Ltv/periscope/model/user/UserType;

    goto :goto_0

    .line 319
    :cond_1
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->h:Ltv/periscope/model/user/UserType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    invoke-virtual {v0, p1}, Lcyv;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 320
    sget-object v0, Ltv/periscope/model/user/UserType;->h:Ltv/periscope/model/user/UserType;

    goto :goto_0

    .line 322
    :cond_2
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->g:Ltv/periscope/model/user/UserType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    invoke-virtual {v0, p1}, Lcyv;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 323
    sget-object v0, Ltv/periscope/model/user/UserType;->g:Ltv/periscope/model/user/UserType;

    goto :goto_0

    .line 325
    :cond_3
    iget-object v0, p0, Lcyx;->e:Ljava/util/Map;

    sget-object v1, Ltv/periscope/model/user/UserType;->i:Ltv/periscope/model/user/UserType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyv;

    invoke-virtual {v0, p1}, Lcyv;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 326
    sget-object v0, Ltv/periscope/model/user/UserType;->i:Ltv/periscope/model/user/UserType;

    goto :goto_0

    .line 328
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p(Ljava/lang/String;)Ltv/periscope/android/api/PsUser;
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lcyx;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsUser;

    return-object v0
.end method
