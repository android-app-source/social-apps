.class public Lyu;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Lcom/twitter/database/lru/LruPolicy;

.field private static final b:Lcom/twitter/database/lru/k;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/database/lru/LruPolicy;

    sget-object v1, Lcom/twitter/database/lru/LruPolicy$Type;->a:Lcom/twitter/database/lru/LruPolicy$Type;

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/twitter/database/lru/LruPolicy;-><init>(Lcom/twitter/database/lru/LruPolicy$Type;I)V

    sput-object v0, Lyu;->a:Lcom/twitter/database/lru/LruPolicy;

    .line 22
    new-instance v0, Lcom/twitter/database/lru/k;

    sget-object v1, Lyu;->a:Lcom/twitter/database/lru/LruPolicy;

    const-wide v2, 0x7fffffffffffffffL

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/database/lru/k;-><init>(Lcom/twitter/database/lru/LruPolicy;J)V

    sput-object v0, Lyu;->b:Lcom/twitter/database/lru/k;

    return-void
.end method

.method static a(Lawb;)Lcom/twitter/database/lru/l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lawb;",
            ")",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    invoke-static {}, Lcom/twitter/database/lru/e$a;->b()Lcom/twitter/database/lru/e$a;

    move-result-object v0

    const-string/jumbo v1, "moment_maker_local_operations"

    .line 31
    invoke-virtual {v0, v1}, Lcom/twitter/database/lru/e$a;->a(Ljava/lang/String;)Lcom/twitter/database/lru/e$a;

    move-result-object v0

    sget-object v1, Lcfe;->a:Lcom/twitter/util/serialization/l;

    .line 32
    invoke-virtual {v0, v1}, Lcom/twitter/database/lru/e$a;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/database/lru/e$a;

    move-result-object v0

    sget-object v1, Lyu;->b:Lcom/twitter/database/lru/k;

    .line 33
    invoke-virtual {v0, v1}, Lcom/twitter/database/lru/e$a;->a(Lcom/twitter/database/lru/k;)Lcom/twitter/database/lru/e$a;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcom/twitter/database/lru/e$a;->c()Lcom/twitter/database/lru/e;

    move-result-object v0

    .line 30
    invoke-virtual {p0, v0}, Lawb;->a(Lcom/twitter/database/lru/e;)Lcom/twitter/database/lru/l;

    move-result-object v0

    return-object v0
.end method
