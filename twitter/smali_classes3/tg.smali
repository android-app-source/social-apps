.class public Ltg;
.super Lsx;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltg$b;,
        Ltg$a;
    }
.end annotation


# static fields
.field public static final f:Ltg$b;


# instance fields
.field public final g:I

.field public h:Z

.field public i:Z

.field public final j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Ltg$b;

    invoke-direct {v0}, Ltg$b;-><init>()V

    sput-object v0, Ltg;->f:Ltg$b;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JIZZJLjava/lang/String;Ljava/util/List;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JIZZJ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lsx;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 42
    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p7

    move-object/from16 v8, p10

    move-object/from16 v9, p9

    invoke-direct/range {v2 .. v9}, Lsx;-><init>(Ljava/lang/String;JJLjava/util/List;Ljava/lang/String;)V

    .line 43
    iput p4, p0, Ltg;->g:I

    .line 44
    iput-boolean p5, p0, Ltg;->h:Z

    .line 45
    move/from16 v0, p6

    iput-boolean v0, p0, Ltg;->i:Z

    .line 46
    move/from16 v0, p11

    iput-boolean v0, p0, Ltg;->j:Z

    .line 47
    return-void
.end method

.method private constructor <init>(Ltg$a;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lsx;-><init>(Lsx$a;)V

    .line 51
    iget v0, p1, Ltg$a;->f:I

    iput v0, p0, Ltg;->g:I

    .line 52
    iget-boolean v0, p1, Ltg$a;->g:Z

    iput-boolean v0, p0, Ltg;->h:Z

    .line 53
    iget-boolean v0, p1, Ltg$a;->h:Z

    iput-boolean v0, p0, Ltg;->i:Z

    .line 54
    iget-boolean v0, p1, Ltg$a;->i:Z

    iput-boolean v0, p0, Ltg;->j:Z

    .line 55
    return-void
.end method

.method synthetic constructor <init>(Ltg$a;Ltg$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Ltg;-><init>(Ltg$a;)V

    return-void
.end method

.method public static a(Lcom/twitter/android/util/CategoryListItem;)Ltg;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Ltg$a;

    invoke-direct {v0}, Ltg$a;-><init>()V

    .line 74
    invoke-virtual {p0}, Lcom/twitter/android/util/CategoryListItem;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltg$a;->a(Ljava/lang/String;)Lsx$a;

    move-result-object v0

    check-cast v0, Ltg$a;

    const/4 v1, 0x2

    .line 75
    invoke-virtual {v0, v1}, Ltg$a;->a(I)Ltg$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 76
    invoke-virtual {v0, v1}, Ltg$a;->a(Z)Ltg$a;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Lcom/twitter/android/util/CategoryListItem;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltg$a;->b(Ljava/lang/String;)Lsx$a;

    move-result-object v0

    check-cast v0, Ltg$a;

    .line 78
    invoke-virtual {v0}, Ltg$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltg;

    .line 73
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ltg;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ltg$a;

    invoke-direct {v0}, Ltg$a;-><init>()V

    .line 65
    invoke-virtual {v0, p0}, Ltg$a;->a(Ljava/lang/String;)Lsx$a;

    move-result-object v0

    check-cast v0, Ltg$a;

    const/4 v1, 0x2

    .line 66
    invoke-virtual {v0, v1}, Ltg$a;->a(I)Ltg$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 67
    invoke-virtual {v0, v1}, Ltg$a;->a(Z)Ltg$a;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Ltg$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltg;

    .line 64
    return-object v0
.end method


# virtual methods
.method public b()V
    .locals 2

    .prologue
    .line 58
    iget-boolean v0, p0, Ltg;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Ltg;->h:Z

    .line 59
    iget-boolean v0, p0, Ltg;->i:Z

    iget-boolean v1, p0, Ltg;->h:Z

    or-int/2addr v0, v1

    iput-boolean v0, p0, Ltg;->i:Z

    .line 60
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
