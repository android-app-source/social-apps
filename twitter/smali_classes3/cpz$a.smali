.class public final Lcpz$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcpz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcpz$a$a;
    }
.end annotation


# instance fields
.field final a:Lcpz$b;

.field final synthetic b:Lcpz;

.field private c:Z


# direct methods
.method constructor <init>(Lcpz;Lcpz$b;)V
    .locals 0

    .prologue
    .line 716
    iput-object p1, p0, Lcpz$a;->b:Lcpz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 717
    iput-object p2, p0, Lcpz$a;->a:Lcpz$b;

    .line 718
    return-void
.end method

.method static synthetic a(Lcpz$a;Z)Z
    .locals 0

    .prologue
    .line 711
    iput-boolean p1, p0, Lcpz$a;->c:Z

    return p1
.end method


# virtual methods
.method public a(I)Ljava/io/OutputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 727
    iget-object v1, p0, Lcpz$a;->b:Lcpz;

    monitor-enter v1

    .line 728
    :try_start_0
    iget-object v0, p0, Lcpz$a;->a:Lcpz$b;

    invoke-virtual {v0}, Lcpz$b;->b()Lcpz$a;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 729
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 737
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 732
    :cond_0
    :try_start_1
    new-instance v0, Lcpz$a$a;

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcpz$a;->a:Lcpz$b;

    invoke-virtual {v3, p1}, Lcpz$b;->b(I)Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v3, 0x0

    invoke-direct {v0, p0, v2, v3}, Lcpz$a$a;-><init>(Lcpz$a;Ljava/io/OutputStream;Lcpz$1;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1

    return-object v0

    .line 733
    :catch_0
    move-exception v0

    .line 734
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcpz$a;->c:Z

    .line 735
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 741
    iget-boolean v0, p0, Lcpz$a;->c:Z

    return v0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 749
    iget-boolean v0, p0, Lcpz$a;->c:Z

    if-eqz v0, :cond_0

    .line 750
    iget-object v0, p0, Lcpz$a;->b:Lcpz;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcpz;->a(Lcpz;Lcpz$a;Z)V

    .line 751
    iget-object v0, p0, Lcpz$a;->b:Lcpz;

    iget-object v1, p0, Lcpz$a;->a:Lcpz$b;

    iget-object v1, v1, Lcpz$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcpz;->c(Ljava/lang/String;)Z

    .line 755
    :goto_0
    return-void

    .line 753
    :cond_0
    iget-object v0, p0, Lcpz$a;->b:Lcpz;

    const/4 v1, 0x1

    invoke-static {v0, p0, v1}, Lcpz;->a(Lcpz;Lcpz$a;Z)V

    goto :goto_0
.end method

.method public c()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 762
    iget-object v0, p0, Lcpz$a;->b:Lcpz;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcpz;->a(Lcpz;Lcpz$a;Z)V

    .line 763
    return-void
.end method
