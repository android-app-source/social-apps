.class public Lxh;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/database/lru/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lxe;

.field private final c:Lcif;


# direct methods
.method public constructor <init>(Lcom/twitter/database/lru/l;Lxe;Lcif;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;",
            "Lxe;",
            "Lcif;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lxh;->a:Lcom/twitter/database/lru/l;

    .line 36
    iput-object p2, p0, Lxh;->b:Lxe;

    .line 37
    iput-object p3, p0, Lxh;->c:Lcif;

    .line 38
    return-void
.end method

.method static synthetic a(Lxh;)Lcif;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lxh;->c:Lcif;

    return-object v0
.end method

.method static synthetic b(Lxh;)Lxe;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lxh;->b:Lxe;

    return-object v0
.end method


# virtual methods
.method public a(JLcfd;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcfd;",
            ")",
            "Lrx/g",
            "<",
            "Lxh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    invoke-static {p3}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lxh;->a(JLjava/lang/Iterable;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(JLjava/lang/Iterable;)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Iterable",
            "<",
            "Lcfd;",
            ">;)",
            "Lrx/g",
            "<",
            "Lxh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    invoke-static {p0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lxh;->a:Lcom/twitter/database/lru/l;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lxh$2;

    invoke-direct {v2, p0, p3}, Lxh$2;-><init>(Lxh;Ljava/lang/Iterable;)V

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/lru/l;->a(Ljava/lang/Object;Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 69
    invoke-static {p0}, Lcre;->a(Ljava/lang/Object;)Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    new-instance v1, Lxh$1;

    invoke-direct {v1, p0, p1, p2}, Lxh$1;-><init>(Lxh;J)V

    .line 70
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method
