.class Lcpt$2$1;
.super Lcpn;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcpt$2;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcpn",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field final synthetic b:Lcpt$2;

.field private c:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 433
    const-class v0, Lcpt;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcpt$2$1;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcpt$2;)V
    .locals 2

    .prologue
    .line 433
    iput-object p1, p0, Lcpt$2$1;->b:Lcpt$2;

    invoke-direct {p0}, Lcpn;-><init>()V

    .line 434
    iget-object v0, p0, Lcpt$2$1;->b:Lcpt$2;

    iget-object v0, v0, Lcpt$2;->a:[Ljava/lang/Iterable;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcpt$2$1;->b:Lcpt$2;

    iget-object v0, v0, Lcpt$2;->a:[Ljava/lang/Iterable;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 436
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcpt$2$1;->c:Ljava/util/Iterator;

    .line 437
    const/4 v0, 0x1

    iput v0, p0, Lcpt$2$1;->d:I

    return-void

    .line 436
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 452
    sget-boolean v0, Lcpt$2$1;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcpt$2$1;->c:Ljava/util/Iterator;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 453
    :cond_0
    iget-object v0, p0, Lcpt$2$1;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 441
    :goto_0
    iget-object v0, p0, Lcpt$2$1;->c:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcpt$2$1;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcpt$2$1;->d:I

    iget-object v1, p0, Lcpt$2$1;->b:Lcpt$2;

    iget-object v1, v1, Lcpt$2;->a:[Ljava/lang/Iterable;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 443
    iget-object v0, p0, Lcpt$2$1;->b:Lcpt$2;

    iget-object v0, v0, Lcpt$2;->a:[Ljava/lang/Iterable;

    iget v1, p0, Lcpt$2$1;->d:I

    aget-object v0, v0, v1

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcpt$2$1;->c:Ljava/util/Iterator;

    .line 444
    iget v0, p0, Lcpt$2$1;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcpt$2$1;->d:I

    goto :goto_0

    .line 446
    :cond_0
    iget-object v0, p0, Lcpt$2$1;->c:Ljava/util/Iterator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcpt$2$1;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
