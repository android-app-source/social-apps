.class public Lyb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lya;


# instance fields
.field private final a:Lyh;

.field private final b:Lxx;

.field private final c:Lcom/twitter/library/api/moments/maker/r;

.field private final d:Lbsb;

.field private final e:J


# direct methods
.method public constructor <init>(Lzj;Lxx;Lyh;Lcom/twitter/library/api/moments/maker/r;Lbsb;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iget-wide v0, p1, Lzj;->a:J

    iput-wide v0, p0, Lyb;->e:J

    .line 51
    iput-object p3, p0, Lyb;->a:Lyh;

    .line 52
    iput-object p2, p0, Lyb;->b:Lxx;

    .line 53
    iput-object p4, p0, Lyb;->c:Lcom/twitter/library/api/moments/maker/r;

    .line 54
    iput-object p5, p0, Lyb;->d:Lbsb;

    .line 55
    return-void
.end method

.method private a()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<-",
            "Lcfh;",
            "+",
            "Lrx/g",
            "<",
            "Lcfh;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Lyb$1;

    invoke-direct {v0, p0}, Lyb$1;-><init>(Lyb;)V

    return-object v0
.end method

.method private a(Lcfh;)Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcfh;",
            ")",
            "Lrx/functions/d",
            "<-",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;+",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 120
    new-instance v0, Lyb$4;

    invoke-direct {v0, p0, p1}, Lyb$4;-><init>(Lyb;Lcfh;)V

    return-object v0
.end method

.method static synthetic a(Lyb;Lcfh;)Lrx/functions/d;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lyb;->a(Lcfh;)Lrx/functions/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lyb;)Lxx;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lyb;->b:Lxx;

    return-object v0
.end method

.method static synthetic b(Lyb;)J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lyb;->e:J

    return-wide v0
.end method

.method private b()Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/b",
            "<-",
            "Lcfh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    new-instance v0, Lyb$2;

    invoke-direct {v0, p0}, Lyb$2;-><init>(Lyb;)V

    return-object v0
.end method

.method static synthetic c(Lyb;)Lbsb;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lyb;->d:Lbsb;

    return-object v0
.end method

.method private c()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<-",
            "Lcfh;",
            "Lrx/g",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 99
    new-instance v0, Lyb$3;

    invoke-direct {v0, p0}, Lyb$3;-><init>(Lyb;)V

    return-object v0
.end method

.method static synthetic d(Lyb;)Lyh;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lyb;->a:Lyh;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/moments/maker/q;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/api/moments/maker/q;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lyb;->c:Lcom/twitter/library/api/moments/maker/r;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/moments/maker/r;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    sget-object v1, Lcfh;->a:Lcfh;

    invoke-static {v0, v1}, Lcre;->a(Lrx/c;Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    .line 65
    invoke-direct {p0}, Lyb;->b()Lrx/functions/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    .line 66
    invoke-direct {p0}, Lyb;->a()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 67
    invoke-direct {p0}, Lyb;->c()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 63
    return-object v0
.end method
