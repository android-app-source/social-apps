.class public Ldbg;
.super Ldbh;
.source "Twttr"


# instance fields
.field private final e:Ltv/periscope/android/ui/broadcast/moderator/i;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;Ltv/periscope/android/ui/broadcast/moderator/i;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3, p4}, Ldbh;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;)V

    .line 21
    iput-object p5, p0, Ldbg;->e:Ltv/periscope/android/ui/broadcast/moderator/i;

    .line 22
    return-void
.end method

.method private b(Ltv/periscope/model/chat/MessageType$VoteType;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Ldbg;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/d;->b()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 51
    if-nez v0, :cond_1

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_0

    .line 59
    iget-object v1, p0, Ldbg;->c:Ltv/periscope/android/ui/broadcast/moderator/g;

    invoke-interface {v1, v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/g;->a(Ljava/lang/String;Ltv/periscope/model/chat/MessageType$VoteType;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    const-string/jumbo v0, "ModerationSelection"

    return-object v0
.end method

.method public a(Ltv/periscope/model/chat/MessageType$VoteType;)V
    .locals 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Ldbh;->a(Ltv/periscope/model/chat/MessageType$VoteType;)V

    .line 44
    invoke-direct {p0, p1}, Ldbg;->b(Ltv/periscope/model/chat/MessageType$VoteType;)V

    .line 45
    iget-object v0, p0, Ldbg;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/d;->b()Ltv/periscope/model/chat/Message;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/chat/Message;->G()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit16 v0, v0, 0x2710

    .line 46
    iget-object v1, p0, Ldbg;->e:Ltv/periscope/android/ui/broadcast/moderator/i;

    const-string/jumbo v2, "CollectModerationConsensus"

    invoke-virtual {v1, v2, v0}, Ltv/periscope/android/ui/broadcast/moderator/i;->a(Ljava/lang/String;I)V

    .line 47
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;)Z
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->C:Ltv/periscope/model/chat/MessageType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ldbg;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/d;->b()Ltv/periscope/model/chat/Message;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ldbg;->a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ldbg;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/d;->a()V

    .line 38
    iget-object v0, p0, Ldbg;->e:Ltv/periscope/android/ui/broadcast/moderator/i;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/i;->a(Ltv/periscope/model/chat/Message;)V

    .line 39
    return-void
.end method
