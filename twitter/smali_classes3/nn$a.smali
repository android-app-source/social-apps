.class final Lnn$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/v;

.field private final c:Lcom/twitter/model/av/AVMediaPlaylist;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/String;

.field private final f:Lcom/twitter/library/av/d;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/model/av/AVMediaPlaylist;Ljava/util/List;Ljava/lang/String;Lcom/twitter/library/av/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/v;",
            "Lcom/twitter/model/av/AVMediaPlaylist;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/twitter/library/av/d;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lnn$a;->a:Landroid/content/Context;

    .line 62
    iput-object p2, p0, Lnn$a;->b:Lcom/twitter/library/client/v;

    .line 63
    iput-object p3, p0, Lnn$a;->c:Lcom/twitter/model/av/AVMediaPlaylist;

    .line 64
    iput-object p4, p0, Lnn$a;->d:Ljava/util/List;

    .line 65
    iput-object p5, p0, Lnn$a;->e:Ljava/lang/String;

    .line 66
    iput-object p6, p0, Lnn$a;->f:Lcom/twitter/library/av/d;

    .line 67
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lnn$a;->b:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 72
    iget-object v0, p0, Lnn$a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 73
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 74
    new-instance v3, Lcom/twitter/library/network/k;

    iget-object v4, p0, Lnn$a;->a:Landroid/content/Context;

    iget-object v5, p0, Lnn$a;->f:Lcom/twitter/library/av/d;

    iget-object v6, p0, Lnn$a;->e:Ljava/lang/String;

    iget-object v7, p0, Lnn$a;->c:Lcom/twitter/model/av/AVMediaPlaylist;

    .line 75
    invoke-virtual {v5, v1, v0, v6, v7}, Lcom/twitter/library/av/d;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMediaPlaylist;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/network/k;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/twitter/network/HttpOperation$RequestMethod;->a:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 76
    invoke-virtual {v3, v0}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    goto :goto_0

    .line 82
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lnn$a;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
