.class Ldaa$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ldaa;->a(Ldab;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ldab;

.field final synthetic b:Ldaa;


# direct methods
.method constructor <init>(Ldaa;Ldab;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Ldaa$3;->b:Ldaa;

    iput-object p2, p0, Ldaa$3;->a:Ldab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 117
    iget-object v0, p0, Ldaa$3;->b:Ldaa;

    invoke-static {v0}, Ldaa;->a(Ldaa;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Ldaa$3;->a:Ldab;

    new-instance v1, Ljava/lang/Exception;

    const-string/jumbo v2, "Logger is closed!!!"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ldab;->onError(Ljava/lang/Exception;)V

    .line 134
    :cond_0
    :goto_0
    return-object v3

    .line 124
    :cond_1
    :try_start_0
    iget-object v0, p0, Ldaa$3;->b:Ldaa;

    invoke-static {v0}, Ldaa;->c(Ldaa;)[Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 131
    iget-object v1, p0, Ldaa$3;->a:Ldab;

    invoke-interface {v1, v0}, Ldab;->onReceive([Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    iget-object v1, p0, Ldaa$3;->b:Ldaa;

    invoke-virtual {v1, v0}, Ldaa;->a([Ljava/io/File;)V

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    .line 127
    iget-object v1, p0, Ldaa$3;->a:Ldab;

    invoke-interface {v1, v0}, Ldab;->onError(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 114
    invoke-virtual {p0}, Ldaa$3;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
