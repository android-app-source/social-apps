.class Lpb;
.super Lpf;
.source "Twttr"


# instance fields
.field a:J


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;)V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lpf;-><init>(Lcom/twitter/library/av/playback/AVPlayer;)V

    .line 16
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lpb;->a:J

    .line 20
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method protected a(Lcom/twitter/library/av/playback/aa;Lcom/twitter/library/av/m;)V
    .locals 4

    .prologue
    .line 24
    iget-wide v0, p0, Lpb;->a:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 25
    iget-wide v0, p1, Lcom/twitter/library/av/playback/aa;->c:J

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    iput-wide v0, p0, Lpb;->a:J

    .line 27
    :cond_0
    invoke-virtual {p2}, Lcom/twitter/library/av/m;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p2, Lcom/twitter/library/av/m;->i:Z

    if-nez v0, :cond_1

    .line 28
    iget-wide v0, p0, Lpb;->c:J

    const-wide/16 v2, 0xa

    add-long/2addr v0, v2

    iput-wide v0, p0, Lpb;->c:J

    .line 30
    :cond_1
    return-void
.end method

.method protected a()Z
    .locals 4

    .prologue
    .line 37
    iget-wide v0, p0, Lpb;->a:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lpb;->c:J

    iget-wide v2, p0, Lpb;->a:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-string/jumbo v0, "video_groupm_view"

    return-object v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 47
    invoke-super {p0}, Lpf;->c()V

    .line 48
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lpb;->a:J

    .line 49
    return-void
.end method
