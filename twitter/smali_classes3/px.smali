.class public Lpx;
.super Laum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laum",
        "<",
        "Lcom/twitter/model/av/MonetizationCategory;",
        "Lcbi",
        "<",
        "Lcom/twitter/model/av/MonetizationCategory;",
        ">;",
        "Lbbr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Laum;-><init>()V

    .line 28
    iput-object p1, p0, Lpx;->a:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lpx;->b:Lcom/twitter/library/client/Session;

    .line 30
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/model/av/MonetizationCategory;)Lbbr;
    .locals 3

    .prologue
    .line 41
    new-instance v0, Lbbr;

    iget-object v1, p0, Lpx;->a:Landroid/content/Context;

    iget-object v2, p0, Lpx;->b:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v1, v2}, Lbbr;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    return-object v0
.end method

.method protected a(Lbbr;)Lcbi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbbr;",
            ")",
            "Lcbi",
            "<",
            "Lcom/twitter/model/av/MonetizationCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Lcbl;

    invoke-virtual {p1}, Lbbr;->e()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/library/service/s;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lcom/twitter/model/av/MonetizationCategory;

    invoke-virtual {p0, p1}, Lpx;->a(Lcom/twitter/model/av/MonetizationCategory;)Lbbr;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/library/service/s;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lbbr;

    invoke-virtual {p0, p1}, Lpx;->a(Lbbr;)Lcbi;

    move-result-object v0

    return-object v0
.end method
