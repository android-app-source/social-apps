.class public Lvs;
.super Lcom/twitter/android/AbsPagesAdapter;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;",
            "Landroid/support/v4/view/ViewPager;",
            "Lcom/twitter/internal/android/widget/HorizontalListView;",
            "Lcom/twitter/android/at;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/AbsPagesAdapter;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V

    .line 24
    invoke-virtual {p3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iput v0, p0, Lvs;->f:I

    .line 25
    return-void
.end method


# virtual methods
.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-super {p0, p1, p2}, Lcom/twitter/android/AbsPagesAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/base/BaseFragment;

    .line 30
    invoke-virtual {p0, v0, p2}, Lvs;->a(Lcom/twitter/app/common/base/BaseFragment;I)V

    .line 31
    return-object v0
.end method

.method public onPageSelected(I)V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/twitter/android/AbsPagesAdapter;->onPageSelected(I)V

    .line 37
    iget v0, p0, Lvs;->f:I

    invoke-virtual {p0, v0}, Lvs;->c(I)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lvs;->a(Lcom/twitter/library/client/m;)Z

    .line 38
    invoke-virtual {p0, p1}, Lvs;->a(I)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lvs;->b(Lcom/twitter/library/client/m;)Z

    .line 39
    iput p1, p0, Lvs;->f:I

    .line 40
    return-void
.end method
