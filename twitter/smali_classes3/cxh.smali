.class final Lcxh;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final d:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 42
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcxh;->a:Ljava/util/Map;

    .line 44
    sget-object v0, Lcxh;->a:Ljava/util/Map;

    const-string/jumbo v1, "tip_pre_broadcast_1"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_pre_broadcast_1:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcxh;->a:Ljava/util/Map;

    const-string/jumbo v1, "tip_pre_broadcast_2"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_pre_broadcast_2:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcxh;->a:Ljava/util/Map;

    const-string/jumbo v1, "tip_pre_broadcast_3"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_pre_broadcast_3:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcxh;->a:Ljava/util/Map;

    const-string/jumbo v1, "tip_pre_broadcast_4"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_pre_broadcast_4:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcxh;->a:Ljava/util/Map;

    const-string/jumbo v1, "tip_pre_broadcast_5"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_pre_broadcast_5:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcxh;->a:Ljava/util/Map;

    const-string/jumbo v1, "tip_pre_broadcast_6"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_pre_broadcast_6:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcxh;->a:Ljava/util/Map;

    const-string/jumbo v1, "tip_pre_broadcast_8"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_pre_broadcast_8:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcxh;->a:Ljava/util/Map;

    const-string/jumbo v1, "tip_pre_broadcast_9"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_pre_broadcast_9:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcxh;->b:Ljava/util/Map;

    .line 70
    sget-object v0, Lcxh;->b:Ljava/util/Map;

    const-string/jumbo v1, "tip_broadcaster_1"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_broadcaster_1:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcxh;->b:Ljava/util/Map;

    const-string/jumbo v1, "tip_broadcaster_2"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_broadcaster_2:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcxh;->b:Ljava/util/Map;

    const-string/jumbo v1, "tip_broadcaster_3"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_broadcaster_3:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcxh;->b:Ljava/util/Map;

    const-string/jumbo v1, "tip_broadcaster_4"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_broadcaster_4:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcxh;->b:Ljava/util/Map;

    const-string/jumbo v1, "tip_broadcaster_5"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_broadcaster_5:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcxh;->b:Ljava/util/Map;

    const-string/jumbo v1, "tip_broadcaster_6"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_broadcaster_6:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcxh;->b:Ljava/util/Map;

    const-string/jumbo v1, "tip_broadcaster_7"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_broadcaster_7:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcxh;->b:Ljava/util/Map;

    const-string/jumbo v1, "tip_broadcaster_8"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_broadcaster_8:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcxh;->b:Ljava/util/Map;

    const-string/jumbo v1, "tip_broadcaster_9"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_broadcaster_9:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcxh;->b:Ljava/util/Map;

    const-string/jumbo v1, "tip_broadcaster_11"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_broadcaster_11:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcxh;->c:Ljava/util/Map;

    .line 93
    sget-object v0, Lcxh;->c:Ljava/util/Map;

    const-string/jumbo v1, "tip_viewer_1"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_viewer_1:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcxh;->c:Ljava/util/Map;

    const-string/jumbo v1, "tip_viewer_2"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_viewer_2:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcxh;->c:Ljava/util/Map;

    const-string/jumbo v1, "tip_viewer_3"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_viewer_3:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcxh;->c:Ljava/util/Map;

    const-string/jumbo v1, "tip_viewer_4"

    sget v2, Ltv/periscope/android/library/f$l;->ps__tip_viewer_4:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcxh;->d:J

    return-void
.end method
