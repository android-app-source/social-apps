.class public Lxk;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lyg;


# direct methods
.method public constructor <init>(Lyg;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lxk;->a:Lyg;

    .line 45
    return-void
.end method

.method private a(Lcom/twitter/android/moments/viewmodels/b;Ljava/util/List;)Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/viewmodels/b;",
            "Ljava/util/List",
            "<",
            "Lcfd;",
            ">;)",
            "Lrx/functions/d",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    new-instance v0, Lxk$3;

    invoke-direct {v0, p0, p2, p1}, Lxk$3;-><init>(Lxk;Ljava/util/List;Lcom/twitter/android/moments/viewmodels/b;)V

    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/a;Ljava/util/List;)Lrx/g;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Ljava/util/List",
            "<",
            "Lcfd;",
            ">;)",
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    new-instance v2, Lcom/twitter/android/moments/viewmodels/b;

    invoke-direct {v2, p1}, Lcom/twitter/android/moments/viewmodels/b;-><init>(Lcom/twitter/model/moments/viewmodels/a;)V

    .line 97
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v3

    .line 98
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfd;

    .line 99
    instance-of v1, v0, Lces;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 100
    check-cast v1, Lces;

    iget-wide v6, v1, Lces;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 102
    :cond_1
    instance-of v1, v0, Lcex;

    if-eqz v1, :cond_0

    .line 103
    check-cast v0, Lcex;

    .line 104
    iget-object v1, v0, Lcex;->d:Lcez;

    if-eqz v1, :cond_0

    .line 105
    iget-object v0, v0, Lcex;->d:Lcez;

    iget-object v0, v0, Lcez;->b:Lcom/twitter/model/moments/r;

    iget-object v0, v0, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    invoke-interface {v3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    :cond_2
    iget-object v0, p0, Lxk;->a:Lyg;

    invoke-interface {v0, v3}, Lyg;->a(Ljava/util/Collection;)Lrx/g;

    move-result-object v0

    .line 111
    invoke-direct {p0, v2, p2}, Lxk;->a(Lcom/twitter/android/moments/viewmodels/b;Ljava/util/List;)Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 110
    return-object v0
.end method


# virtual methods
.method public a()Lrx/functions/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/e",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;",
            "Lcfe;",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Lxk$2;

    invoke-direct {v0, p0}, Lxk$2;-><init>(Lxk;)V

    return-object v0
.end method

.method public a(Lcom/twitter/util/collection/m;Lcfe;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;",
            "Lcfe;",
            ")",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/twitter/util/collection/m;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    invoke-static {p1}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    .line 55
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/util/collection/m;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/b;

    .line 56
    if-eqz p2, :cond_1

    iget-object v1, p2, Lcfe;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 57
    :cond_1
    invoke-static {v0}, Lcom/twitter/util/collection/m;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/m;

    move-result-object v0

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    goto :goto_0

    .line 60
    :cond_2
    iget-object v0, v0, Lcom/twitter/model/moments/viewmodels/b;->a:Lcom/twitter/model/moments/viewmodels/a;

    .line 61
    iget-object v1, p2, Lcfe;->c:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lxk;->a(Lcom/twitter/model/moments/viewmodels/a;Ljava/util/List;)Lrx/g;

    move-result-object v0

    new-instance v1, Lxk$1;

    invoke-direct {v1, p0}, Lxk$1;-><init>(Lxk;)V

    .line 62
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method
