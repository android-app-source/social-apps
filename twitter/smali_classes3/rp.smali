.class public Lrp;
.super Lcom/twitter/async/service/AsyncOperation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lrp;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;)V

    .line 26
    iput-wide p1, p0, Lrp;->a:J

    .line 27
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->c:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {p0, v0}, Lrp;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Integer;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 43
    iget-wide v2, p0, Lrp;->a:J

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 44
    invoke-virtual {v1}, Lcom/twitter/library/provider/t;->h()I

    move-result v2

    int-to-long v2, v2

    .line 47
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    .line 48
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 72
    :goto_0
    return-object v0

    .line 52
    :cond_0
    invoke-static {}, Lcom/twitter/library/resilient/e;->c()Lcom/twitter/library/resilient/e;

    move-result-object v4

    .line 53
    iget-wide v6, p0, Lrp;->a:J

    const-string/jumbo v5, "tweet"

    invoke-virtual {v4, v6, v7, v5}, Lcom/twitter/library/resilient/e;->a(JLjava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 55
    iget-wide v6, p0, Lrp;->a:J

    const-string/jumbo v8, "tweet_upload"

    invoke-virtual {v4, v6, v7, v8}, Lcom/twitter/library/resilient/e;->a(JLjava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 60
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 62
    invoke-virtual {v1}, Lcom/twitter/library/provider/t;->i()I

    move-result v0

    .line 65
    new-instance v1, Lcpb;

    invoke-direct {v1}, Lcpb;-><init>()V

    const-string/jumbo v4, "message"

    const-string/jumbo v5, "Orphaned pending tweets"

    .line 66
    invoke-virtual {v1, v4, v5}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v1

    const-string/jumbo v4, "previewCount"

    .line 67
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v1

    const-string/jumbo v2, "deletedCount"

    .line 68
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "No persistent job for preview tweet"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 69
    invoke-virtual {v1, v2}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v1

    .line 65
    invoke-static {v1}, Lcpd;->c(Lcpb;)V

    .line 72
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method protected b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lrp;->b()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0}, Lrp;->a()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
