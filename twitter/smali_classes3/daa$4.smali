.class Ldaa$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ldaa;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ldaa;


# direct methods
.method constructor <init>(Ldaa;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Ldaa$4;->a:Ldaa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 238
    iget-object v0, p0, Ldaa$4;->a:Ldaa;

    invoke-static {v0}, Ldaa;->a(Ldaa;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    :goto_0
    return-void

    .line 242
    :cond_0
    :try_start_0
    iget-object v0, p0, Ldaa$4;->a:Ldaa;

    invoke-static {v0}, Ldaa;->b(Ldaa;)Ljava/io/Writer;

    move-result-object v0

    invoke-static {v0}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 243
    iget-object v0, p0, Ldaa$4;->a:Ldaa;

    iget-object v1, p0, Ldaa$4;->a:Ldaa;

    iget-object v1, v1, Ldaa;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Ldaa;->a(Ldaa;Ljava/lang/String;)V

    .line 244
    iget-object v0, p0, Ldaa$4;->a:Ldaa;

    invoke-virtual {v0}, Ldaa;->f()Ljava/io/File;

    move-result-object v1

    .line 245
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 247
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 250
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 251
    iget-object v0, p0, Ldaa$4;->a:Ldaa;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ldaa;->a(Ldaa;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 252
    :catch_0
    move-exception v0

    .line 253
    iget-object v0, p0, Ldaa$4;->a:Ldaa;

    const-string/jumbo v1, "An error occurred attempting to close the logger."

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Ldaa;->a(Ldaa;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
