.class Lcxt;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcxl;


# instance fields
.field private final a:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcxt;->a:Ljava/lang/Boolean;

    .line 19
    return-void
.end method

.method public static a(Landroid/content/SharedPreferences;Ltv/periscope/model/user/f;)Lcxl;
    .locals 4

    .prologue
    .line 22
    const-string/jumbo v0, "broadcast_tips.initial_has_low_broadcast_count"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    new-instance v0, Lcxt;

    const-string/jumbo v1, "broadcast_tips.initial_has_low_broadcast_count"

    const/4 v2, 0x0

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcxt;-><init>(Ljava/lang/Boolean;)V

    .line 39
    :goto_0
    return-object v0

    .line 28
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ltv/periscope/model/user/f;->b()Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_2

    .line 30
    :cond_1
    new-instance v0, Lcxt;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcxt;-><init>(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 35
    :cond_2
    invoke-virtual {p1}, Ltv/periscope/model/user/f;->b()Ljava/lang/Boolean;

    move-result-object v1

    .line 36
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 37
    const-string/jumbo v2, "broadcast_tips.initial_has_low_broadcast_count"

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 38
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 39
    new-instance v0, Lcxt;

    invoke-direct {v0, v1}, Lcxt;-><init>(Ljava/lang/Boolean;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcxt;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcxt;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcxt;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcxt;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    return v0
.end method
