.class Lxt$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lxt;->b(Ljava/util/Collection;)Lrx/functions/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Long;",
        "Lcep;",
        ">;",
        "Lrx/g",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Long;",
        "Lcep;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Collection;

.field final synthetic b:Lxt;


# direct methods
.method constructor <init>(Lxt;Ljava/util/Collection;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lxt$1;->b:Lxt;

    iput-object p2, p0, Lxt$1;->a:Ljava/util/Collection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lxt$1;->a(Ljava/util/Map;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Map;)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;)",
            "Lrx/g",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lxt$1;->a:Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 56
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    invoke-static {p1}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    .line 59
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lxt$1;->b:Lxt;

    invoke-static {v1, v0}, Lxt;->a(Lxt;Ljava/util/Collection;)Lrx/g;

    move-result-object v0

    .line 60
    invoke-static {p1}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v1

    invoke-static {}, Lcrb;->a()Lrx/functions/e;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lrx/g;->a(Lrx/g;Lrx/functions/e;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method
