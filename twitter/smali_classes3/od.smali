.class public Lod;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbkn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()J
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 63
    const-string/jumbo v0, "live_video_scribe_heartbeat_interval_android"

    invoke-static {v0}, Lod;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 74
    const-wide/16 v0, 0x1e

    invoke-static {p0, v0, v1}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 75
    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method private static b()J
    .locals 2

    .prologue
    .line 70
    const-string/jumbo v0, "video_on_demand_heartbeat_interval_android"

    invoke-static {v0}, Lod;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private b(Lcom/twitter/library/av/playback/AVPlayer;)Z
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayer;->e()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/playback/AVPlayer;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/av/playback/AVPlayer;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbiy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/av/playback/AVPlayer;",
            "Lcom/twitter/model/av/AVMedia;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbiy;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 39
    invoke-direct {p0, p1}, Lod;->b(Lcom/twitter/library/av/playback/AVPlayer;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/twitter/model/av/b;->a(Lcom/twitter/model/av/AVMedia;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Loe;

    .line 42
    invoke-static {}, Lod;->a()J

    move-result-wide v2

    invoke-direct {v0, p1, p2, v2, v3}, Loe;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;J)V

    new-array v1, v1, [Lbiy;

    new-instance v2, Low;

    invoke-direct {v2, p1, p2}, Low;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V

    aput-object v2, v1, v4

    .line 41
    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 51
    :goto_0
    return-object v0

    .line 44
    :cond_0
    const-string/jumbo v0, "video_on_demand_heartbeat_android_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    new-instance v0, Loe;

    .line 47
    invoke-static {}, Lod;->b()J

    move-result-wide v2

    invoke-direct {v0, p1, p2, v2, v3}, Loe;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;J)V

    new-array v1, v1, [Lbiy;

    new-instance v2, Lok;

    invoke-direct {v2, p1, p2}, Lok;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V

    aput-object v2, v1, v4

    .line 46
    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
