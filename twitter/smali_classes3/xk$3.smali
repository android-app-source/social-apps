.class Lxk$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lxk;->a(Lcom/twitter/android/moments/viewmodels/b;Ljava/util/List;)Lrx/functions/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Long;",
        "Lcom/twitter/model/moments/viewmodels/g;",
        ">;",
        "Lcom/twitter/model/moments/viewmodels/a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/twitter/android/moments/viewmodels/b;

.field final synthetic c:Lxk;


# direct methods
.method constructor <init>(Lxk;Ljava/util/List;Lcom/twitter/android/moments/viewmodels/b;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lxk$3;->c:Lxk;

    iput-object p2, p0, Lxk$3;->a:Ljava/util/List;

    iput-object p3, p0, Lxk$3;->b:Lcom/twitter/android/moments/viewmodels/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/Map;)Lcom/twitter/model/moments/viewmodels/a;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;)",
            "Lcom/twitter/model/moments/viewmodels/a;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lxk$3;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfd;

    .line 122
    const/4 v1, 0x0

    .line 123
    instance-of v3, v0, Lcfl;

    if-eqz v3, :cond_0

    move-object v1, v0

    .line 124
    check-cast v1, Lcfl;

    iget-wide v4, v1, Lcfl;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/moments/viewmodels/g;

    .line 126
    :cond_0
    iget-object v3, p0, Lxk$3;->b:Lcom/twitter/android/moments/viewmodels/b;

    invoke-virtual {v3, v0, v1}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcfd;Lcom/twitter/model/moments/viewmodels/g;)Lcom/twitter/android/moments/viewmodels/b;

    goto :goto_0

    .line 128
    :cond_1
    iget-object v0, p0, Lxk$3;->b:Lcom/twitter/android/moments/viewmodels/b;

    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/b;->a()Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lxk$3;->a(Ljava/util/Map;)Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v0

    return-object v0
.end method
