.class public Ldi;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static a:Ldi;


# instance fields
.field private final b:Ldh;

.field private c:Lch;

.field private d:Lcf;

.field private e:Lcg;

.field private f:Lcp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcp",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Ldq;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcx",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Ldq;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcp",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcx",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcm;

.field private k:Lcom/facebook/cache/disk/e;

.field private l:Lcom/facebook/imagepipeline/decoder/a;

.field private m:Ldg;

.field private n:Lcom/facebook/imagepipeline/bitmaps/e;

.field private o:Ldk;

.field private p:Ldl;

.field private q:Lcm;

.field private r:Lcom/facebook/cache/disk/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    sput-object v0, Ldi;->a:Ldi;

    return-void
.end method

.method public constructor <init>(Ldh;)V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    invoke-static {p1}, Lax;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldh;

    iput-object v0, p0, Ldi;->b:Ldh;

    .line 116
    return-void
.end method

.method public static a(Lch;Lcom/facebook/imagepipeline/bitmaps/e;)Lcg;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Ldi$1;

    invoke-direct {v0, p0}, Ldi$1;-><init>(Lch;)V

    .line 137
    new-instance v1, Lcg;

    invoke-direct {v1, v0, p1}, Lcg;-><init>(Lcom/facebook/imagepipeline/animated/impl/b;Lcom/facebook/imagepipeline/bitmaps/e;)V

    return-object v1
.end method

.method public static a(Lcom/facebook/imagepipeline/memory/s;)Lcom/facebook/imagepipeline/bitmaps/e;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 245
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v0, v2, :cond_1

    new-instance v0, Lcom/facebook/imagepipeline/bitmaps/d;

    invoke-direct {v0}, Lcom/facebook/imagepipeline/bitmaps/d;-><init>()V

    .line 248
    :goto_0
    new-instance v2, Lcom/facebook/imagepipeline/bitmaps/b;

    new-instance v3, Lcom/facebook/imagepipeline/bitmaps/c;

    invoke-virtual {p0}, Lcom/facebook/imagepipeline/memory/s;->e()Lcom/facebook/imagepipeline/memory/w;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/imagepipeline/bitmaps/c;-><init>(Lcom/facebook/imagepipeline/memory/w;)V

    invoke-virtual {p0}, Lcom/facebook/imagepipeline/memory/s;->b()Lcom/facebook/imagepipeline/memory/j;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/facebook/imagepipeline/bitmaps/b;-><init>(Lcom/facebook/imagepipeline/bitmaps/c;Lcom/facebook/imagepipeline/memory/j;)V

    .line 251
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_0

    new-instance v1, Lcom/facebook/imagepipeline/bitmaps/a;

    invoke-virtual {p0}, Lcom/facebook/imagepipeline/memory/s;->a()Lcom/facebook/imagepipeline/memory/c;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/imagepipeline/memory/s;->c()I

    move-result v4

    invoke-direct {v1, v3, v4}, Lcom/facebook/imagepipeline/bitmaps/a;-><init>(Lcom/facebook/imagepipeline/memory/c;I)V

    .line 256
    :cond_0
    new-instance v3, Lcom/facebook/imagepipeline/bitmaps/e;

    invoke-direct {v3, v0, v2, v1}, Lcom/facebook/imagepipeline/bitmaps/e;-><init>(Lcom/facebook/imagepipeline/bitmaps/d;Lcom/facebook/imagepipeline/bitmaps/b;Lcom/facebook/imagepipeline/bitmaps/a;)V

    return-object v3

    :cond_1
    move-object v0, v1

    .line 245
    goto :goto_0
.end method

.method public static a()Ldi;
    .locals 2

    .prologue
    .line 72
    sget-object v0, Ldi;->a:Ldi;

    const-string/jumbo v1, "ImagePipelineFactory was not initialized!"

    invoke-static {v0, v1}, Lax;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldi;

    return-object v0
.end method

.method public static a(Ldh;)V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Ldi;

    invoke-direct {v0, p0}, Ldi;-><init>(Ldh;)V

    sput-object v0, Ldi;->a:Ldi;

    .line 83
    return-void
.end method

.method private j()Lch;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Ldi;->c:Lch;

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lch;

    invoke-direct {v0}, Lch;-><init>()V

    iput-object v0, p0, Ldi;->c:Lch;

    .line 124
    :cond_0
    iget-object v0, p0, Ldi;->c:Lch;

    return-object v0
.end method

.method private k()Lcg;
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Ldi;->e:Lcg;

    if-nez v0, :cond_0

    .line 142
    iget-object v0, p0, Ldi;->b:Ldh;

    invoke-virtual {v0}, Ldh;->a()Lcg;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Ldi;->b:Ldh;

    invoke-virtual {v0}, Ldh;->a()Lcg;

    move-result-object v0

    iput-object v0, p0, Ldi;->e:Lcg;

    .line 150
    :cond_0
    :goto_0
    iget-object v0, p0, Ldi;->e:Lcg;

    return-object v0

    .line 145
    :cond_1
    invoke-direct {p0}, Ldi;->j()Lch;

    move-result-object v0

    invoke-direct {p0}, Ldi;->n()Lcom/facebook/imagepipeline/bitmaps/e;

    move-result-object v1

    invoke-static {v0, v1}, Ldi;->a(Lch;Lcom/facebook/imagepipeline/bitmaps/e;)Lcg;

    move-result-object v0

    iput-object v0, p0, Ldi;->e:Lcg;

    goto :goto_0
.end method

.method private l()Lcom/facebook/imagepipeline/decoder/a;
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Ldi;->l:Lcom/facebook/imagepipeline/decoder/a;

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Ldi;->b:Ldh;

    invoke-virtual {v0}, Ldh;->h()Lcom/facebook/imagepipeline/decoder/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Ldi;->b:Ldh;

    invoke-virtual {v0}, Ldh;->h()Lcom/facebook/imagepipeline/decoder/a;

    move-result-object v0

    iput-object v0, p0, Ldi;->l:Lcom/facebook/imagepipeline/decoder/a;

    .line 202
    :cond_0
    :goto_0
    iget-object v0, p0, Ldi;->l:Lcom/facebook/imagepipeline/decoder/a;

    return-object v0

    .line 199
    :cond_1
    new-instance v0, Lcom/facebook/imagepipeline/decoder/a;

    invoke-direct {p0}, Ldi;->k()Lcg;

    move-result-object v1

    invoke-direct {p0}, Ldi;->n()Lcom/facebook/imagepipeline/bitmaps/e;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/imagepipeline/decoder/a;-><init>(Lcg;Lcom/facebook/imagepipeline/bitmaps/e;)V

    iput-object v0, p0, Ldi;->l:Lcom/facebook/imagepipeline/decoder/a;

    goto :goto_0
.end method

.method private m()Lcm;
    .locals 7

    .prologue
    .line 206
    iget-object v0, p0, Ldi;->j:Lcm;

    if-nez v0, :cond_0

    .line 207
    new-instance v0, Lcm;

    invoke-virtual {p0}, Ldi;->f()Lcom/facebook/cache/disk/e;

    move-result-object v1

    iget-object v2, p0, Ldi;->b:Ldh;

    invoke-virtual {v2}, Ldh;->n()Lcom/facebook/imagepipeline/memory/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/imagepipeline/memory/s;->e()Lcom/facebook/imagepipeline/memory/w;

    move-result-object v2

    iget-object v3, p0, Ldi;->b:Ldh;

    invoke-virtual {v3}, Ldh;->n()Lcom/facebook/imagepipeline/memory/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/imagepipeline/memory/s;->f()Lcom/facebook/imagepipeline/memory/z;

    move-result-object v3

    iget-object v4, p0, Ldi;->b:Ldh;

    invoke-virtual {v4}, Ldh;->f()Ldf;

    move-result-object v4

    invoke-interface {v4}, Ldf;->a()Ljava/util/concurrent/Executor;

    move-result-object v4

    iget-object v5, p0, Ldi;->b:Ldh;

    invoke-virtual {v5}, Ldh;->f()Ldf;

    move-result-object v5

    invoke-interface {v5}, Ldf;->b()Ljava/util/concurrent/Executor;

    move-result-object v5

    iget-object v6, p0, Ldi;->b:Ldh;

    invoke-virtual {v6}, Ldh;->g()Lcv;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcm;-><init>(Lcom/facebook/cache/disk/g;Lcom/facebook/imagepipeline/memory/w;Lcom/facebook/imagepipeline/memory/z;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcv;)V

    iput-object v0, p0, Ldi;->j:Lcm;

    .line 216
    :cond_0
    iget-object v0, p0, Ldi;->j:Lcm;

    return-object v0
.end method

.method private n()Lcom/facebook/imagepipeline/bitmaps/e;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Ldi;->n:Lcom/facebook/imagepipeline/bitmaps/e;

    if-nez v0, :cond_0

    .line 261
    iget-object v0, p0, Ldi;->b:Ldh;

    invoke-virtual {v0}, Ldh;->n()Lcom/facebook/imagepipeline/memory/s;

    move-result-object v0

    invoke-static {v0}, Ldi;->a(Lcom/facebook/imagepipeline/memory/s;)Lcom/facebook/imagepipeline/bitmaps/e;

    move-result-object v0

    iput-object v0, p0, Ldi;->n:Lcom/facebook/imagepipeline/bitmaps/e;

    .line 264
    :cond_0
    iget-object v0, p0, Ldi;->n:Lcom/facebook/imagepipeline/bitmaps/e;

    return-object v0
.end method

.method private o()Ldk;
    .locals 15

    .prologue
    .line 268
    iget-object v0, p0, Ldi;->o:Ldk;

    if-nez v0, :cond_0

    .line 269
    new-instance v0, Ldk;

    iget-object v1, p0, Ldi;->b:Ldh;

    invoke-virtual {v1}, Ldh;->d()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ldi;->b:Ldh;

    invoke-virtual {v2}, Ldh;->n()Lcom/facebook/imagepipeline/memory/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/imagepipeline/memory/s;->g()Lcom/facebook/imagepipeline/memory/e;

    move-result-object v2

    invoke-direct {p0}, Ldi;->l()Lcom/facebook/imagepipeline/decoder/a;

    move-result-object v3

    iget-object v4, p0, Ldi;->b:Ldh;

    invoke-virtual {v4}, Ldh;->o()Lcom/facebook/imagepipeline/decoder/b;

    move-result-object v4

    iget-object v5, p0, Ldi;->b:Ldh;

    invoke-virtual {v5}, Ldh;->m()Z

    move-result v5

    iget-object v6, p0, Ldi;->b:Ldh;

    invoke-virtual {v6}, Ldh;->q()Z

    move-result v6

    iget-object v7, p0, Ldi;->b:Ldh;

    invoke-virtual {v7}, Ldh;->f()Ldf;

    move-result-object v7

    iget-object v8, p0, Ldi;->b:Ldh;

    invoke-virtual {v8}, Ldh;->n()Lcom/facebook/imagepipeline/memory/s;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/imagepipeline/memory/s;->e()Lcom/facebook/imagepipeline/memory/w;

    move-result-object v8

    invoke-virtual {p0}, Ldi;->c()Lcx;

    move-result-object v9

    invoke-virtual {p0}, Ldi;->e()Lcx;

    move-result-object v10

    invoke-direct {p0}, Ldi;->m()Lcm;

    move-result-object v11

    invoke-direct {p0}, Ldi;->q()Lcm;

    move-result-object v12

    iget-object v13, p0, Ldi;->b:Ldh;

    invoke-virtual {v13}, Ldh;->c()Lcn;

    move-result-object v13

    invoke-direct {p0}, Ldi;->n()Lcom/facebook/imagepipeline/bitmaps/e;

    move-result-object v14

    invoke-direct/range {v0 .. v14}, Ldk;-><init>(Landroid/content/Context;Lcom/facebook/imagepipeline/memory/e;Lcom/facebook/imagepipeline/decoder/a;Lcom/facebook/imagepipeline/decoder/b;ZZLdf;Lcom/facebook/imagepipeline/memory/w;Lcx;Lcx;Lcm;Lcm;Lcn;Lcom/facebook/imagepipeline/bitmaps/e;)V

    iput-object v0, p0, Ldi;->o:Ldk;

    .line 286
    :cond_0
    iget-object v0, p0, Ldi;->o:Ldk;

    return-object v0
.end method

.method private p()Ldl;
    .locals 5

    .prologue
    .line 290
    iget-object v0, p0, Ldi;->p:Ldl;

    if-nez v0, :cond_0

    .line 291
    new-instance v0, Ldl;

    invoke-direct {p0}, Ldi;->o()Ldk;

    move-result-object v1

    iget-object v2, p0, Ldi;->b:Ldh;

    invoke-virtual {v2}, Ldh;->l()Lcom/facebook/imagepipeline/producers/ac;

    move-result-object v2

    iget-object v3, p0, Ldi;->b:Ldh;

    invoke-virtual {v3}, Ldh;->q()Z

    move-result v3

    iget-object v4, p0, Ldi;->b:Ldh;

    invoke-virtual {v4}, Ldh;->m()Z

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Ldl;-><init>(Ldk;Lcom/facebook/imagepipeline/producers/ac;ZZ)V

    iput-object v0, p0, Ldi;->p:Ldl;

    .line 298
    :cond_0
    iget-object v0, p0, Ldi;->p:Ldl;

    return-object v0
.end method

.method private q()Lcm;
    .locals 7

    .prologue
    .line 310
    iget-object v0, p0, Ldi;->q:Lcm;

    if-nez v0, :cond_0

    .line 311
    new-instance v0, Lcm;

    invoke-virtual {p0}, Ldi;->h()Lcom/facebook/cache/disk/e;

    move-result-object v1

    iget-object v2, p0, Ldi;->b:Ldh;

    invoke-virtual {v2}, Ldh;->n()Lcom/facebook/imagepipeline/memory/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/imagepipeline/memory/s;->e()Lcom/facebook/imagepipeline/memory/w;

    move-result-object v2

    iget-object v3, p0, Ldi;->b:Ldh;

    invoke-virtual {v3}, Ldh;->n()Lcom/facebook/imagepipeline/memory/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/imagepipeline/memory/s;->f()Lcom/facebook/imagepipeline/memory/z;

    move-result-object v3

    iget-object v4, p0, Ldi;->b:Ldh;

    invoke-virtual {v4}, Ldh;->f()Ldf;

    move-result-object v4

    invoke-interface {v4}, Ldf;->a()Ljava/util/concurrent/Executor;

    move-result-object v4

    iget-object v5, p0, Ldi;->b:Ldh;

    invoke-virtual {v5}, Ldh;->f()Ldf;

    move-result-object v5

    invoke-interface {v5}, Ldf;->b()Ljava/util/concurrent/Executor;

    move-result-object v5

    iget-object v6, p0, Ldi;->b:Ldh;

    invoke-virtual {v6}, Ldh;->g()Lcv;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcm;-><init>(Lcom/facebook/cache/disk/g;Lcom/facebook/imagepipeline/memory/w;Lcom/facebook/imagepipeline/memory/z;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcv;)V

    iput-object v0, p0, Ldi;->q:Lcm;

    .line 320
    :cond_0
    iget-object v0, p0, Ldi;->q:Lcm;

    return-object v0
.end method


# virtual methods
.method public b()Lcp;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcp",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Ldq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Ldi;->f:Lcp;

    if-nez v0, :cond_0

    .line 156
    iget-object v0, p0, Ldi;->b:Ldh;

    invoke-virtual {v0}, Ldh;->b()Laz;

    move-result-object v0

    iget-object v1, p0, Ldi;->b:Ldh;

    invoke-virtual {v1}, Ldh;->k()Lcom/facebook/common/memory/b;

    move-result-object v1

    invoke-static {v0, v1}, Lci;->a(Laz;Lcom/facebook/common/memory/b;)Lcp;

    move-result-object v0

    iput-object v0, p0, Ldi;->f:Lcp;

    .line 161
    :cond_0
    iget-object v0, p0, Ldi;->f:Lcp;

    return-object v0
.end method

.method public c()Lcx;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcx",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Ldq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Ldi;->g:Lcx;

    if-nez v0, :cond_0

    .line 166
    invoke-virtual {p0}, Ldi;->b()Lcp;

    move-result-object v0

    iget-object v1, p0, Ldi;->b:Ldh;

    invoke-virtual {v1}, Ldh;->g()Lcv;

    move-result-object v1

    invoke-static {v0, v1}, Lcj;->a(Lcp;Lcv;)Lcx;

    move-result-object v0

    iput-object v0, p0, Ldi;->g:Lcx;

    .line 171
    :cond_0
    iget-object v0, p0, Ldi;->g:Lcx;

    return-object v0
.end method

.method public d()Lcp;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcp",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Ldi;->h:Lcp;

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Ldi;->b:Ldh;

    invoke-virtual {v0}, Ldh;->e()Laz;

    move-result-object v0

    iget-object v1, p0, Ldi;->b:Ldh;

    invoke-virtual {v1}, Ldh;->k()Lcom/facebook/common/memory/b;

    move-result-object v1

    invoke-static {v0, v1}, Lct;->a(Laz;Lcom/facebook/common/memory/b;)Lcp;

    move-result-object v0

    iput-object v0, p0, Ldi;->h:Lcp;

    .line 181
    :cond_0
    iget-object v0, p0, Ldi;->h:Lcp;

    return-object v0
.end method

.method public e()Lcx;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcx",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Ldi;->i:Lcx;

    if-nez v0, :cond_0

    .line 186
    invoke-virtual {p0}, Ldi;->d()Lcp;

    move-result-object v0

    iget-object v1, p0, Ldi;->b:Ldh;

    invoke-virtual {v1}, Ldh;->g()Lcv;

    move-result-object v1

    invoke-static {v0, v1}, Lcu;->a(Lcp;Lcv;)Lcx;

    move-result-object v0

    iput-object v0, p0, Ldi;->i:Lcx;

    .line 191
    :cond_0
    iget-object v0, p0, Ldi;->i:Lcx;

    return-object v0
.end method

.method public f()Lcom/facebook/cache/disk/e;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Ldi;->k:Lcom/facebook/cache/disk/e;

    if-nez v0, :cond_0

    .line 221
    iget-object v0, p0, Ldi;->b:Ldh;

    invoke-virtual {v0}, Ldh;->j()Lcom/facebook/cache/disk/b;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/cache/disk/c;->a(Lcom/facebook/cache/disk/b;)Lcom/facebook/cache/disk/e;

    move-result-object v0

    iput-object v0, p0, Ldi;->k:Lcom/facebook/cache/disk/e;

    .line 224
    :cond_0
    iget-object v0, p0, Ldi;->k:Lcom/facebook/cache/disk/e;

    return-object v0
.end method

.method public g()Ldg;
    .locals 9

    .prologue
    .line 228
    iget-object v0, p0, Ldi;->m:Ldg;

    if-nez v0, :cond_0

    .line 229
    new-instance v0, Ldg;

    invoke-direct {p0}, Ldi;->p()Ldl;

    move-result-object v1

    iget-object v2, p0, Ldi;->b:Ldh;

    invoke-virtual {v2}, Ldh;->p()Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, Ldi;->b:Ldh;

    invoke-virtual {v3}, Ldh;->i()Laz;

    move-result-object v3

    invoke-virtual {p0}, Ldi;->c()Lcx;

    move-result-object v4

    invoke-virtual {p0}, Ldi;->e()Lcx;

    move-result-object v5

    invoke-direct {p0}, Ldi;->m()Lcm;

    move-result-object v6

    invoke-direct {p0}, Ldi;->q()Lcm;

    move-result-object v7

    iget-object v8, p0, Ldi;->b:Ldh;

    invoke-virtual {v8}, Ldh;->c()Lcn;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Ldg;-><init>(Ldl;Ljava/util/Set;Laz;Lcx;Lcx;Lcm;Lcm;Lcn;)V

    iput-object v0, p0, Ldi;->m:Ldg;

    .line 240
    :cond_0
    iget-object v0, p0, Ldi;->m:Ldg;

    return-object v0
.end method

.method public h()Lcom/facebook/cache/disk/e;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Ldi;->r:Lcom/facebook/cache/disk/e;

    if-nez v0, :cond_0

    .line 303
    iget-object v0, p0, Ldi;->b:Ldh;

    invoke-virtual {v0}, Ldh;->r()Lcom/facebook/cache/disk/b;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/cache/disk/c;->a(Lcom/facebook/cache/disk/b;)Lcom/facebook/cache/disk/e;

    move-result-object v0

    iput-object v0, p0, Ldi;->r:Lcom/facebook/cache/disk/e;

    .line 306
    :cond_0
    iget-object v0, p0, Ldi;->r:Lcom/facebook/cache/disk/e;

    return-object v0
.end method

.method public i()Lcf;
    .locals 7

    .prologue
    .line 324
    iget-object v0, p0, Ldi;->d:Lcf;

    if-nez v0, :cond_0

    .line 325
    invoke-direct {p0}, Ldi;->j()Lch;

    move-result-object v4

    .line 326
    invoke-static {}, Lbk;->a()Lbk;

    move-result-object v5

    .line 327
    new-instance v2, Laj;

    iget-object v0, p0, Ldi;->b:Ldh;

    invoke-virtual {v0}, Ldh;->f()Ldf;

    move-result-object v0

    invoke-interface {v0}, Ldf;->c()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-direct {v2, v0}, Laj;-><init>(Ljava/util/concurrent/Executor;)V

    .line 329
    iget-object v0, p0, Ldi;->b:Ldh;

    invoke-virtual {v0}, Ldh;->d()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager;

    .line 332
    new-instance v0, Ldi$2;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldi$2;-><init>(Ldi;Lan;Landroid/app/ActivityManager;Lch;Lbj;)V

    .line 348
    new-instance v2, Ldi$3;

    invoke-direct {v2, p0, v4}, Ldi$3;-><init>(Ldi;Lch;)V

    .line 355
    new-instance v1, Lcf;

    invoke-static {}, Lap;->b()Lap;

    move-result-object v5

    iget-object v3, p0, Ldi;->b:Ldh;

    invoke-virtual {v3}, Ldh;->d()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    move-object v3, v0

    invoke-direct/range {v1 .. v6}, Lcf;-><init>(Lcom/facebook/imagepipeline/animated/impl/b;Lcom/facebook/imagepipeline/animated/impl/d;Lch;Ljava/util/concurrent/ScheduledExecutorService;Landroid/content/res/Resources;)V

    iput-object v1, p0, Ldi;->d:Lcf;

    .line 362
    :cond_0
    iget-object v0, p0, Ldi;->d:Lcf;

    return-object v0
.end method
