.class public Lxa;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/api/moments/maker/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/c;",
            "Lcom/twitter/model/json/moments/maker/JsonDeleteMomentResponse;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lbsb;

.field private final c:Lcif;

.field private final d:Lcom/twitter/library/api/moments/maker/j;

.field private final e:Lxj;

.field private final f:Lcom/twitter/util/android/g;

.field private final g:J


# direct methods
.method public constructor <init>(Lcom/twitter/library/api/moments/maker/f;Lxj;Lcom/twitter/util/android/g;JLbsb;Lcif;Lcom/twitter/library/api/moments/maker/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/c;",
            "Lcom/twitter/model/json/moments/maker/JsonDeleteMomentResponse;",
            "Lcom/twitter/model/core/z;",
            ">;",
            "Lxj;",
            "Lcom/twitter/util/android/g;",
            "J",
            "Lbsb;",
            "Lcif;",
            "Lcom/twitter/library/api/moments/maker/j;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lxa;->a:Lcom/twitter/library/api/moments/maker/f;

    .line 61
    iput-object p2, p0, Lxa;->e:Lxj;

    .line 62
    iput-wide p4, p0, Lxa;->g:J

    .line 63
    iput-object p3, p0, Lxa;->f:Lcom/twitter/util/android/g;

    .line 64
    iput-object p6, p0, Lxa;->b:Lbsb;

    .line 65
    iput-object p7, p0, Lxa;->c:Lcif;

    .line 66
    iput-object p8, p0, Lxa;->d:Lcom/twitter/library/api/moments/maker/j;

    .line 67
    return-void
.end method

.method static synthetic a(Lxa;)Lcom/twitter/library/api/moments/maker/j;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lxa;->d:Lcom/twitter/library/api/moments/maker/j;

    return-object v0
.end method

.method public static a(Landroid/content/Context;JLcom/twitter/library/api/moments/maker/j;)Lxa;
    .locals 9

    .prologue
    .line 42
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/twitter/library/api/moments/maker/f;

    invoke-direct {v1, p0, v0}, Lcom/twitter/library/api/moments/maker/f;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 45
    invoke-static {p0}, Lbsb;->a(Landroid/content/Context;)Lbsb;

    move-result-object v6

    .line 46
    new-instance v7, Lcif;

    new-instance v0, Lcrc;

    .line 47
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    invoke-direct {v0, v2}, Lcrc;-><init>(Landroid/support/v4/content/LocalBroadcastManager;)V

    invoke-direct {v7, v0}, Lcif;-><init>(Lcrc;)V

    .line 48
    new-instance v0, Lxa;

    invoke-static {}, Lxj;->a()Lxj;

    move-result-object v2

    new-instance v3, Lcom/twitter/util/android/g;

    .line 49
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/twitter/util/android/g;-><init>(Landroid/content/Context;)V

    move-wide v4, p1

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lxa;-><init>(Lcom/twitter/library/api/moments/maker/f;Lxj;Lcom/twitter/util/android/g;JLbsb;Lcif;Lcom/twitter/library/api/moments/maker/j;)V

    .line 48
    return-object v0
.end method

.method static synthetic b(Lxa;)Lcif;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lxa;->c:Lcif;

    return-object v0
.end method

.method private b()Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/b",
            "<-",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Lxa$1;

    invoke-direct {v0, p0}, Lxa$1;-><init>(Lxa;)V

    return-object v0
.end method

.method static synthetic c(Lxa;)Lcom/twitter/library/api/moments/maker/f;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lxa;->a:Lcom/twitter/library/api/moments/maker/f;

    return-object v0
.end method

.method private c()Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/b",
            "<-",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    new-instance v0, Lxa$2;

    invoke-direct {v0, p0}, Lxa$2;-><init>(Lxa;)V

    return-object v0
.end method

.method static synthetic d(Lxa;)J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lxa;->g:J

    return-wide v0
.end method

.method private d()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<-",
            "Ljava/lang/Long;",
            "+",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/json/moments/maker/JsonDeleteMomentResponse;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 105
    new-instance v0, Lxa$3;

    invoke-direct {v0, p0}, Lxa$3;-><init>(Lxa;)V

    return-object v0
.end method

.method static synthetic e(Lxa;)Lbsb;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lxa;->b:Lbsb;

    return-object v0
.end method

.method private e()Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/b",
            "<-",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    new-instance v0, Lxa$4;

    invoke-direct {v0, p0}, Lxa$4;-><init>(Lxa;)V

    return-object v0
.end method

.method static synthetic f(Lxa;)Lcom/twitter/util/android/g;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lxa;->f:Lcom/twitter/util/android/g;

    return-object v0
.end method

.method private static f()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<-",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/json/moments/maker/JsonDeleteMomentResponse;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    new-instance v0, Lxa$5;

    invoke-direct {v0}, Lxa$5;-><init>()V

    return-object v0
.end method

.method private g()Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/b",
            "<-",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    new-instance v0, Lxa$6;

    invoke-direct {v0, p0}, Lxa$6;-><init>(Lxa;)V

    return-object v0
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-wide v0, p0, Lxa;->g:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 72
    invoke-direct {p0}, Lxa;->b()Lrx/functions/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    .line 73
    invoke-direct {p0}, Lxa;->e()Lrx/functions/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    .line 74
    invoke-direct {p0}, Lxa;->c()Lrx/functions/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    .line 75
    invoke-direct {p0}, Lxa;->d()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 76
    invoke-static {}, Lxa;->f()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lxa;->e:Lxj;

    iget-object v1, v1, Lxj;->a:Lrx/f;

    .line 77
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lxa;->e:Lxj;

    iget-object v1, v1, Lxj;->b:Lrx/f;

    .line 78
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 79
    invoke-direct {p0}, Lxa;->g()Lrx/functions/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    .line 71
    return-object v0
.end method
