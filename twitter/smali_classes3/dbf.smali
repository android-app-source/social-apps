.class public Ldbf;
.super Ldbh;
.source "Twttr"


# instance fields
.field private final e:Ltv/periscope/android/ui/broadcast/moderator/i;

.field private f:Ltv/periscope/model/chat/Message;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;Ltv/periscope/android/ui/broadcast/moderator/i;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3, p4}, Ldbh;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;)V

    .line 23
    iput-object p5, p0, Ldbf;->e:Ltv/periscope/android/ui/broadcast/moderator/i;

    .line 24
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    const-string/jumbo v0, "CollectModerationConsensus"

    return-object v0
.end method

.method public a(Ltv/periscope/model/chat/Message;)Z
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->C:Ltv/periscope/model/chat/MessageType;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->B:Ltv/periscope/model/chat/MessageType;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Ldbf;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    .line 35
    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/d;->b()Ltv/periscope/model/chat/Message;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ldbf;->a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/Message;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 34
    :goto_0
    return v0

    .line 35
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Ldbh;->b()V

    .line 47
    iget-object v0, p0, Ldbf;->f:Ltv/periscope/model/chat/Message;

    if-nez v0, :cond_0

    .line 51
    :goto_0
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Ldbf;->e:Ltv/periscope/android/ui/broadcast/moderator/i;

    iget-object v1, p0, Ldbf;->f:Ltv/periscope/model/chat/Message;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/moderator/i;->a(Ltv/periscope/model/chat/Message;)V

    goto :goto_0
.end method

.method public b(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 40
    iput-object p1, p0, Ldbf;->f:Ltv/periscope/model/chat/Message;

    .line 41
    iget-object v0, p0, Ldbf;->e:Ltv/periscope/android/ui/broadcast/moderator/i;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/i;->a(Ltv/periscope/model/chat/Message;)V

    .line 42
    return-void
.end method
