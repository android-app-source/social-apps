.class Lcxd$d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcxd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "d"
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field final synthetic b:Lcxd;

.field private c:Ljava/lang/String;

.field private d:Z


# direct methods
.method constructor <init>(Lcxd;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Lcxd$d;->b:Lcxd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342
    iput-object p2, p0, Lcxd$d;->c:Ljava/lang/String;

    .line 343
    return-void
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 347
    sget-object v0, Lcxd$1;->b:[I

    iget-object v1, p0, Lcxd$d;->b:Lcxd;

    iget-object v1, v1, Lcxd;->d:Ltv/periscope/android/player/PlayMode;

    invoke-virtual {v1}, Ltv/periscope/android/player/PlayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 361
    :goto_0
    return-object v3

    .line 352
    :pswitch_0
    iget-object v0, p0, Lcxd$d;->b:Lcxd;

    iget-object v0, v0, Lcxd;->b:Ltv/periscope/android/ui/broadcast/ac;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcxd$d;->b:Lcxd;

    iget-object v0, v0, Lcxd;->b:Ltv/periscope/android/ui/broadcast/ac;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ac;->s()Ljava/lang/String;

    move-result-object v3

    .line 357
    :cond_0
    iget-object v0, p0, Lcxd$d;->b:Lcxd;

    iget-object v1, v0, Lcxd;->a:Ltv/periscope/android/api/ApiManager;

    iget-object v2, p0, Lcxd$d;->a:Ljava/lang/String;

    iget-object v0, p0, Lcxd$d;->b:Lcxd;

    iget-object v0, v0, Lcxd;->e:Ltv/periscope/android/ui/chat/p;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/p;->h()I

    move-result v0

    int-to-long v4, v0

    iget-object v0, p0, Lcxd$d;->b:Lcxd;

    iget-object v0, v0, Lcxd;->e:Ltv/periscope/android/ui/chat/p;

    .line 358
    invoke-interface {v0}, Ltv/periscope/android/ui/chat/p;->i()I

    move-result v0

    int-to-long v6, v0

    .line 357
    invoke-interface/range {v1 .. v7}, Ltv/periscope/android/api/ApiManager;->endWatching(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 347
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method a(Z)V
    .locals 0

    .prologue
    .line 369
    iput-boolean p1, p0, Lcxd$d;->d:Z

    .line 370
    return-void
.end method

.method b()Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcxd$d;->d:Z

    return v0
.end method

.method c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 374
    sget-object v0, Lcxd$1;->b:[I

    iget-object v1, p0, Lcxd$d;->b:Lcxd;

    iget-object v1, v1, Lcxd;->d:Ltv/periscope/android/player/PlayMode;

    invoke-virtual {v1}, Ltv/periscope/android/player/PlayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 381
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 378
    :pswitch_0
    iget-object v0, p0, Lcxd$d;->b:Lcxd;

    iget-object v0, v0, Lcxd;->a:Ltv/periscope/android/api/ApiManager;

    iget-object v1, p0, Lcxd$d;->c:Ljava/lang/String;

    iget-object v2, p0, Lcxd$d;->b:Lcxd;

    invoke-static {v2}, Lcxd;->a(Lcxd;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/api/ApiManager;->startWatching(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 374
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method d()V
    .locals 8

    .prologue
    .line 385
    invoke-virtual {p0}, Lcxd$d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 386
    sget-object v0, Lcxd$1;->b:[I

    iget-object v1, p0, Lcxd$d;->b:Lcxd;

    iget-object v1, v1, Lcxd;->d:Ltv/periscope/android/player/PlayMode;

    invoke-virtual {v1}, Ltv/periscope/android/player/PlayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 391
    :pswitch_0
    iget-object v0, p0, Lcxd$d;->b:Lcxd;

    iget-object v0, v0, Lcxd;->b:Ltv/periscope/android/ui/broadcast/ac;

    if-eqz v0, :cond_1

    .line 392
    iget-object v0, p0, Lcxd$d;->b:Lcxd;

    iget-object v0, v0, Lcxd;->b:Ltv/periscope/android/ui/broadcast/ac;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/ac;->s()Ljava/lang/String;

    move-result-object v3

    .line 396
    :goto_1
    iget-object v0, p0, Lcxd$d;->b:Lcxd;

    iget-object v1, v0, Lcxd;->a:Ltv/periscope/android/api/ApiManager;

    iget-object v2, p0, Lcxd$d;->a:Ljava/lang/String;

    iget-object v0, p0, Lcxd$d;->b:Lcxd;

    iget-object v0, v0, Lcxd;->e:Ltv/periscope/android/ui/chat/p;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/p;->h()I

    move-result v0

    int-to-long v4, v0

    iget-object v0, p0, Lcxd$d;->b:Lcxd;

    iget-object v0, v0, Lcxd;->e:Ltv/periscope/android/ui/chat/p;

    invoke-interface {v0}, Ltv/periscope/android/ui/chat/p;->i()I

    move-result v0

    int-to-long v6, v0

    invoke-interface/range {v1 .. v7}, Ltv/periscope/android/api/ApiManager;->pingWatching(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;

    goto :goto_0

    .line 394
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 386
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 404
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " mLifecycleToken: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcxd$d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n sessionId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcxd$d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
