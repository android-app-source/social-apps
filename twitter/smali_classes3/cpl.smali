.class public Lcpl;
.super Lcpi;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcpj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcpi;-><init>()V

    .line 20
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcpl;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a(Lcnz;Lcpk;)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpj;

    .line 36
    invoke-interface {v0, p2}, Lcpj;->a(Lcpk;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    invoke-interface {v0, p1, p2}, Lcpj;->a(Lcnz;Lcpk;)V

    goto :goto_0

    .line 40
    :cond_1
    return-void
.end method

.method public a(Lcpj;)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcpl;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    return-void
.end method
