.class final Lcya$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcya$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcya;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcya$a;Ltv/periscope/android/branch/api/BranchApiClient;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcya$a;

.field final synthetic c:Landroid/content/Context;


# direct methods
.method constructor <init>(ZLcya$a;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 58
    iput-boolean p1, p0, Lcya$1;->a:Z

    iput-object p2, p0, Lcya$1;->b:Lcya$a;

    iput-object p3, p0, Lcya$1;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 61
    iget-boolean v0, p0, Lcya$1;->a:Z

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcya$1;->b:Lcya$a;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-interface {v0, v1}, Lcya$a;->a(Landroid/content/Intent;)V

    .line 68
    :goto_0
    return-void

    .line 64
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcya$1;->c:Landroid/content/Context;

    const-class v2, Ltv/periscope/android/library/PeriscopeInterstitialActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    const-string/jumbo v1, "create_broadcast"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 66
    iget-object v1, p0, Lcya$1;->b:Lcya$a;

    invoke-interface {v1, v0}, Lcya$a;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
