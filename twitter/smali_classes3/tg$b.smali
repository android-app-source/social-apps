.class public Ltg$b;
.super Lsx$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsx$b",
        "<",
        "Ltg;",
        "Ltg$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lsx$b;-><init>(I)V

    .line 124
    return-void
.end method


# virtual methods
.method protected a()Ltg$a;
    .locals 1

    .prologue
    .line 163
    new-instance v0, Ltg$a;

    invoke-direct {v0}, Ltg$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 118
    check-cast p2, Ltg$a;

    invoke-virtual {p0, p1, p2, p3}, Ltg$b;->a(Lcom/twitter/util/serialization/n;Ltg$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lsx$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 118
    check-cast p2, Ltg$a;

    invoke-virtual {p0, p1, p2, p3}, Ltg$b;->a(Lcom/twitter/util/serialization/n;Ltg$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Ltg$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-super {p0, p1, p2, p3}, Lsx$b;->a(Lcom/twitter/util/serialization/n;Lsx$a;I)V

    .line 145
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    .line 146
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    .line 147
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v2

    .line 148
    const/4 v3, 0x1

    if-ge p3, v3, :cond_0

    .line 150
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    .line 152
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v3

    .line 154
    invoke-virtual {p2, v0}, Ltg$a;->a(I)Ltg$a;

    move-result-object v0

    .line 155
    invoke-virtual {v0, v1}, Ltg$a;->a(Z)Ltg$a;

    move-result-object v0

    .line 156
    invoke-virtual {v0, v2}, Ltg$a;->b(Z)Ltg$a;

    move-result-object v0

    .line 157
    invoke-virtual {v0, v3}, Ltg$a;->c(Z)Ltg$a;

    .line 158
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lsx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    check-cast p2, Ltg;

    invoke-virtual {p0, p1, p2}, Ltg$b;->a(Lcom/twitter/util/serialization/o;Ltg;)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Ltg;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    invoke-super {p0, p1, p2}, Lsx$b;->a(Lcom/twitter/util/serialization/o;Lsx;)V

    .line 132
    iget v0, p2, Ltg;->g:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Ltg;->h:Z

    .line 133
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Ltg;->i:Z

    .line 134
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Ltg;->j:Z

    .line 135
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 136
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    check-cast p2, Ltg;

    invoke-virtual {p0, p1, p2}, Ltg$b;->a(Lcom/twitter/util/serialization/o;Ltg;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Ltg$b;->a()Ltg$a;

    move-result-object v0

    return-object v0
.end method
