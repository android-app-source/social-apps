.class public Lcyc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;
.implements Landroid/content/ComponentCallbacks;
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcyi;
.implements Ltv/periscope/android/ui/broadcast/r;
.implements Ltv/periscope/android/ui/broadcaster/a;
.implements Ltv/periscope/android/ui/broadcaster/prebroadcast/a$a;
.implements Ltv/periscope/android/ui/broadcaster/prebroadcast/a$b;
.implements Ltv/periscope/android/ui/broadcaster/prebroadcast/a$c;
.implements Ltv/periscope/android/util/ae$a;
.implements Ltv/periscope/android/video/RTMPPublisher$a;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcyc$d;,
        Lcyc$a;,
        Lcyc$b;,
        Lcyc$c;
    }
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:J


# instance fields
.field private final A:Ltv/periscope/android/network/ConnectivityChangeReceiver;

.field private B:Lczz;

.field private final C:Ldbt;

.field private D:Landroid/support/v7/app/AlertDialog;

.field private final E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Landroid/location/Location;

.field private H:Landroid/os/Bundle;

.field private I:Z

.field private J:Z

.field private K:Ltv/periscope/model/w;

.field private L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

.field private M:Ltv/periscope/android/ui/broadcast/p;

.field private N:Ltv/periscope/model/p;

.field private O:Ljava/lang/String;

.field private P:Z

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:I

.field private U:I

.field private final V:Ljava/lang/Runnable;

.field private final W:Ljava/lang/Runnable;

.field private final X:Ljava/lang/Runnable;

.field private final c:Landroid/app/Activity;

.field private final d:Landroid/os/Handler;

.field private final e:Ltv/periscope/android/library/c;

.field private final f:Ltv/periscope/android/analytics/summary/a;

.field private final g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

.field private final h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

.field private final i:Lcyb;

.field private final j:Lcxy;

.field private final k:Ltv/periscope/android/ui/broadcast/au;

.field private final l:Ltv/periscope/android/ui/broadcast/f;

.field private final m:Ltv/periscope/android/ui/broadcast/b;

.field private final n:Landroid/content/res/Resources;

.field private final o:Landroid/content/SharedPreferences;

.field private final p:Ltv/periscope/android/util/ae;

.field private final q:Lczq;

.field private final r:Lcye;

.field private final s:Ltv/periscope/android/ui/broadcast/g;

.field private final t:Ltv/periscope/android/ui/broadcast/j;

.field private final u:Ltv/periscope/android/ui/broadcast/h;

.field private final v:Ltv/periscope/android/view/t;

.field private final w:Lcxw;

.field private final x:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

.field private final y:Ltv/periscope/android/ui/user/a;

.field private final z:Ltv/periscope/android/util/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 124
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "android.permission.RECORD_AUDIO"

    aput-object v2, v0, v1

    sput-object v0, Lcyc;->a:[Ljava/lang/String;

    .line 130
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcyc;->b:J

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Ltv/periscope/android/library/c;Ltv/periscope/android/view/t;Lcxw;Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;Ltv/periscope/android/ui/user/a;Ljava/lang/String;Z)V
    .locals 12

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcyc;->d:Landroid/os/Handler;

    .line 172
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcyc;->P:Z

    .line 176
    const/4 v1, 0x0

    iput v1, p0, Lcyc;->T:I

    .line 179
    new-instance v1, Lcyc$c;

    invoke-direct {v1, p0}, Lcyc$c;-><init>(Lcyc;)V

    iput-object v1, p0, Lcyc;->V:Ljava/lang/Runnable;

    .line 180
    new-instance v1, Lcyc$1;

    invoke-direct {v1, p0}, Lcyc$1;-><init>(Lcyc;)V

    iput-object v1, p0, Lcyc;->W:Ljava/lang/Runnable;

    .line 187
    new-instance v1, Lcyc$2;

    invoke-direct {v1, p0}, Lcyc$2;-><init>(Lcyc;)V

    iput-object v1, p0, Lcyc;->X:Ljava/lang/Runnable;

    .line 232
    iput-object p1, p0, Lcyc;->c:Landroid/app/Activity;

    .line 233
    iget-object v1, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcyc;->n:Landroid/content/res/Resources;

    .line 234
    iput-object p2, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 235
    move-object/from16 v0, p7

    iput-object v0, p0, Lcyc;->E:Ljava/lang/String;

    .line 236
    invoke-static {}, Ltv/periscope/android/analytics/d;->a()Lcrz;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/analytics/summary/c;

    iput-object v1, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    .line 237
    invoke-interface {p2}, Ltv/periscope/android/library/c;->p()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcyc;->o:Landroid/content/SharedPreferences;

    .line 238
    move-object/from16 v0, p4

    iput-object v0, p0, Lcyc;->w:Lcxw;

    .line 239
    move-object/from16 v0, p5

    iput-object v0, p0, Lcyc;->x:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    .line 240
    move-object/from16 v0, p6

    iput-object v0, p0, Lcyc;->y:Ltv/periscope/android/ui/user/a;

    .line 242
    invoke-direct {p0}, Lcyc;->s()V

    .line 244
    sget v1, Ltv/periscope/android/library/f$g;->broadcaster_view:I

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    iput-object v1, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    .line 245
    iget-object v1, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v1, p0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setBroadcasterDelegate(Ltv/periscope/android/ui/broadcaster/a;)V

    .line 246
    iget-object v1, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->getChatRoomView()Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-result-object v1

    iput-object v1, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    .line 248
    invoke-direct {p0}, Lcyc;->v()V

    .line 250
    iget-object v1, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 251
    iget-object v1, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/Application;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 253
    new-instance v1, Ltv/periscope/android/util/ae;

    iget-object v2, p0, Lcyc;->d:Landroid/os/Handler;

    invoke-direct {v1, p1, v2}, Ltv/periscope/android/util/ae;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v1, p0, Lcyc;->p:Ltv/periscope/android/util/ae;

    .line 254
    iget-object v1, p0, Lcyc;->p:Ltv/periscope/android/util/ae;

    invoke-virtual {v1, p0}, Ltv/periscope/android/util/ae;->a(Ltv/periscope/android/util/ae$a;)V

    .line 256
    new-instance v11, Lcyj;

    invoke-direct {v11, p0}, Lcyj;-><init>(Lcyi;)V

    .line 257
    iget-object v1, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->getCameraPreview()Ltv/periscope/android/ui/broadcaster/CameraPreviewLayout;

    move-result-object v1

    invoke-virtual {v11}, Lcyj;->a()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcaster/CameraPreviewLayout;->setScaleGestureListener(Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    .line 259
    if-eqz p3, :cond_1

    :goto_0
    iput-object p3, p0, Lcyc;->v:Ltv/periscope/android/view/t;

    .line 261
    new-instance v1, Lcxy;

    iget-object v2, p0, Lcyc;->c:Landroid/app/Activity;

    iget-object v3, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    iget-object v4, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v6, p0, Lcyc;->v:Ltv/periscope/android/view/t;

    iget-object v7, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    move-object v5, p0

    invoke-direct/range {v1 .. v7}, Lcxy;-><init>(Landroid/app/Activity;Ltv/periscope/android/library/c;Ltv/periscope/android/ui/broadcast/ChatRoomView;Ltv/periscope/android/ui/broadcast/u;Ltv/periscope/android/view/t;Lcrz;)V

    iput-object v1, p0, Lcyc;->j:Lcxy;

    .line 264
    new-instance v1, Ltv/periscope/android/ui/broadcast/f;

    iget-object v2, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    new-instance v3, Ltv/periscope/android/view/f;

    invoke-direct {v3}, Ltv/periscope/android/view/f;-><init>()V

    iget-object v4, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    sget v5, Ltv/periscope/android/library/f$g;->broadcast__action_sheet:I

    .line 265
    invoke-virtual {v4, v5}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Ltv/periscope/android/view/ActionSheet;

    iget-object v5, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, Ltv/periscope/android/ui/broadcast/f;-><init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/f;Ltv/periscope/android/view/ActionSheet;Ltv/periscope/android/ui/broadcast/ChatRoomView;Z)V

    iput-object v1, p0, Lcyc;->l:Ltv/periscope/android/ui/broadcast/f;

    .line 267
    new-instance v5, Lcyc$3;

    iget-object v1, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v1

    iget-object v2, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    iget-object v3, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 268
    invoke-interface {v3}, Ltv/periscope/android/library/c;->r()Ldbr;

    move-result-object v3

    invoke-direct {v5, p0, v1, v2, v3}, Lcyc$3;-><init>(Lcyc;Ltv/periscope/android/api/ApiManager;Lcsa;Ldbr;)V

    .line 277
    iget-object v1, p0, Lcyc;->v:Ltv/periscope/android/view/t;

    invoke-interface {v1, v5}, Ltv/periscope/android/view/t;->a(Ltv/periscope/android/ui/user/g;)V

    .line 278
    new-instance v1, Ltv/periscope/android/ui/broadcast/au;

    iget-object v2, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    iget-object v3, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    sget v4, Ltv/periscope/android/library/f$g;->viewer_action_sheet:I

    .line 279
    invoke-virtual {v3, v4}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Ltv/periscope/android/view/ActionSheet;

    iget-object v4, p0, Lcyc;->v:Ltv/periscope/android/view/t;

    iget-object v6, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    iget-object v7, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 280
    invoke-interface {v7}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v7

    iget-object v8, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v8}, Ltv/periscope/android/library/c;->h()Lcyn;

    move-result-object v8

    iget-object v9, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 281
    invoke-interface {v9}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v9

    const/4 v10, 0x1

    invoke-direct/range {v1 .. v10}, Ltv/periscope/android/ui/broadcast/au;-><init>(Ltv/periscope/android/view/RootDragLayout;Ltv/periscope/android/view/ActionSheet;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;Lcrz;Lcyw;Lcyn;Ldae;Z)V

    iput-object v1, p0, Lcyc;->k:Ltv/periscope/android/ui/broadcast/au;

    .line 283
    new-instance v1, Ltv/periscope/android/ui/broadcast/s;

    iget-object v2, p0, Lcyc;->c:Landroid/app/Activity;

    iget-object v3, p0, Lcyc;->l:Ltv/periscope/android/ui/broadcast/f;

    iget-object v4, p0, Lcyc;->v:Ltv/periscope/android/view/t;

    move-object v5, p0

    move/from16 v6, p8

    invoke-direct/range {v1 .. v6}, Ltv/periscope/android/ui/broadcast/s;-><init>(Landroid/content/Context;Ltv/periscope/android/view/b;Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/broadcast/r;Z)V

    iput-object v1, p0, Lcyc;->u:Ltv/periscope/android/ui/broadcast/h;

    .line 286
    new-instance v1, Lcxx;

    iget-object v2, p0, Lcyc;->l:Ltv/periscope/android/ui/broadcast/f;

    iget-object v3, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 287
    invoke-interface {v3}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v3

    iget-object v4, p0, Lcyc;->u:Ltv/periscope/android/ui/broadcast/h;

    iget-object v5, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v5}, Ltv/periscope/android/library/c;->h()Lcyn;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcxx;-><init>(Ltv/periscope/android/view/b;Ldae;Ltv/periscope/android/ui/broadcast/h;Lcyn;)V

    iput-object v1, p0, Lcyc;->m:Ltv/periscope/android/ui/broadcast/b;

    .line 289
    iget-object v1, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v7

    .line 290
    new-instance v1, Lcyb;

    iget-object v2, p0, Lcyc;->c:Landroid/app/Activity;

    iget-object v3, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v3}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->getRenderView()Ltv/periscope/android/graphics/GLRenderView;

    move-result-object v3

    iget-object v4, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    iget-object v5, p0, Lcyc;->u:Ltv/periscope/android/ui/broadcast/h;

    iget-object v6, p0, Lcyc;->B:Lczz;

    invoke-direct/range {v1 .. v7}, Lcyb;-><init>(Landroid/app/Activity;Ltv/periscope/android/graphics/GLRenderView;Ltv/periscope/android/library/c;Ltv/periscope/android/ui/broadcast/h;Lczz;I)V

    iput-object v1, p0, Lcyc;->i:Lcyb;

    .line 293
    iget-object v1, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    new-instance v2, Ltv/periscope/android/ui/broadcaster/b;

    iget-object v3, p0, Lcyc;->c:Landroid/app/Activity;

    .line 294
    invoke-virtual {v11}, Lcyj;->a()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ltv/periscope/android/ui/broadcaster/b;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    .line 293
    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setOnInterceptTouchEventListener(Landroid/view/View$OnTouchListener;)V

    .line 296
    iget-object v1, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v2, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setAvatarImageLoader(Ldae;)V

    .line 297
    iget-object v1, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v2, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->v()Ldae;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setImageLoader(Ldae;)V

    .line 300
    const-string/jumbo v1, "api.periscope.tv"

    iget-object v2, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "canary-api.periscope.tv"

    iget-object v2, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 301
    iget-object v1, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    sget v2, Ltv/periscope/android/library/f$g;->dev:I

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 304
    :cond_0
    new-instance v1, Ltv/periscope/android/ui/broadcast/k;

    iget-object v2, p0, Lcyc;->c:Landroid/app/Activity;

    iget-object v3, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v3}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v3

    iget-object v4, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v4}, Ltv/periscope/android/library/c;->h()Lcyn;

    move-result-object v4

    iget-object v5, p0, Lcyc;->u:Ltv/periscope/android/ui/broadcast/h;

    iget-object v6, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 305
    invoke-interface {v6}, Ltv/periscope/android/library/c;->i()Ltv/periscope/android/ui/broadcast/y;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Ltv/periscope/android/ui/broadcast/k;-><init>(Landroid/content/Context;Lcyw;Lcyn;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/android/ui/broadcast/y;Z)V

    iput-object v1, p0, Lcyc;->t:Ltv/periscope/android/ui/broadcast/j;

    .line 306
    new-instance v1, Ltv/periscope/android/ui/broadcast/g;

    iget-object v2, p0, Lcyc;->c:Landroid/app/Activity;

    iget-object v3, p0, Lcyc;->t:Ltv/periscope/android/ui/broadcast/j;

    iget-object v4, p0, Lcyc;->u:Ltv/periscope/android/ui/broadcast/h;

    iget-object v5, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 307
    invoke-interface {v5}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v5

    iget-object v6, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v6}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Ltv/periscope/android/ui/broadcast/g;-><init>(Landroid/content/Context;Ltv/periscope/android/ui/broadcast/j;Ltv/periscope/android/ui/broadcast/h;Lcyw;Ldae;)V

    iput-object v1, p0, Lcyc;->s:Ltv/periscope/android/ui/broadcast/g;

    .line 308
    iget-object v1, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    iget-object v2, p0, Lcyc;->s:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setBroadcastInfoAdapter(Ltv/periscope/android/ui/broadcast/g;)V

    .line 310
    iget-object v1, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    iget-object v2, p0, Lcyc;->s:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setBroadcastInfoAdapter(Ltv/periscope/android/ui/broadcast/g;)V

    .line 311
    iget-object v1, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1, p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setParticipantClickListener(Landroid/view/View$OnClickListener;)V

    .line 312
    iget-object v1, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v1, p0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setOverflowClickListener(Landroid/view/View$OnClickListener;)V

    .line 314
    new-instance v1, Lcyc$d;

    invoke-direct {v1, p0}, Lcyc$d;-><init>(Lcyc;)V

    .line 315
    new-instance v2, Lczq;

    iget-object v3, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-direct {v2, v3, v1, v1}, Lczq;-><init>(Landroid/app/Activity;Lczq$b;Lcom/google/android/gms/location/f;)V

    iput-object v2, p0, Lcyc;->q:Lczq;

    .line 316
    iget-object v1, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    sget v2, Ltv/periscope/android/library/f$g;->pre_broadcast_details:I

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastView;

    .line 317
    iget-object v2, p0, Lcyc;->q:Lczq;

    iget-object v3, p0, Lcyc;->y:Ltv/periscope/android/ui/user/a;

    invoke-direct {p0, v1, v2, v3}, Lcyc;->a(Ltv/periscope/android/ui/broadcaster/prebroadcast/d;Lczq;Ltv/periscope/android/ui/user/a;)V

    .line 319
    new-instance v1, Ldbt;

    iget-object v2, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-direct {v1, v2}, Ldbt;-><init>(Ltv/periscope/android/library/c;)V

    iput-object v1, p0, Lcyc;->C:Ldbt;

    .line 321
    iget-object v1, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v1

    invoke-virtual {v1, p0}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 322
    iget-object v1, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v1

    iget-object v2, p0, Lcyc;->u:Ltv/periscope/android/ui/broadcast/h;

    invoke-virtual {v1, v2}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 323
    iget-object v1, p0, Lcyc;->v:Ltv/periscope/android/view/t;

    invoke-interface {v1}, Ltv/periscope/android/view/t;->f()V

    .line 324
    iget-object v1, p0, Lcyc;->y:Ltv/periscope/android/ui/user/a;

    invoke-interface {v1}, Ltv/periscope/android/ui/user/a;->e()V

    .line 326
    new-instance v1, Lcye;

    iget-object v2, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-direct {v1, v2}, Lcye;-><init>(Ltv/periscope/android/library/c;)V

    iput-object v1, p0, Lcyc;->r:Lcye;

    .line 327
    new-instance v1, Ltv/periscope/android/util/a;

    invoke-direct {v1}, Ltv/periscope/android/util/a;-><init>()V

    iput-object v1, p0, Lcyc;->z:Ltv/periscope/android/util/a;

    .line 329
    new-instance v1, Ltv/periscope/android/network/ConnectivityChangeReceiver;

    iget-object v2, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Ltv/periscope/android/network/ConnectivityChangeReceiver;-><init>(Landroid/app/Activity;Lde/greenrobot/event/c;)V

    iput-object v1, p0, Lcyc;->A:Ltv/periscope/android/network/ConnectivityChangeReceiver;

    .line 330
    iget-object v1, p0, Lcyc;->A:Ltv/periscope/android/network/ConnectivityChangeReceiver;

    invoke-virtual {v1}, Ltv/periscope/android/network/ConnectivityChangeReceiver;->a()V

    .line 332
    iget-object v1, p0, Lcyc;->C:Ldbt;

    invoke-virtual {v1}, Ldbt;->b()V

    .line 333
    return-void

    .line 259
    :cond_1
    iget-object v1, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-direct {p0, v1}, Lcyc;->a(Landroid/view/ViewGroup;)Ltv/periscope/android/view/t;

    move-result-object p3

    goto/16 :goto_0
.end method

.method private A()V
    .locals 15

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 743
    invoke-direct {p0}, Lcyc;->I()V

    .line 747
    invoke-direct {p0}, Lcyc;->H()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcyc;->I:Z

    if-eqz v0, :cond_1

    :cond_0
    move v12, v3

    .line 748
    :goto_0
    if-eqz v12, :cond_2

    .line 749
    iget-object v0, p0, Lcyc;->G:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    double-to-float v0, v0

    .line 750
    iget-object v1, p0, Lcyc;->G:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    double-to-float v7, v4

    move v11, v0

    .line 756
    :goto_1
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcyc;->O:Ljava/lang/String;

    .line 757
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->e()Ljava/util/ArrayList;

    move-result-object v13

    .line 758
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->f()Ljava/util/ArrayList;

    move-result-object v14

    .line 759
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcyc;->Q:Z

    .line 761
    iget-object v0, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    check-cast v0, Ltv/periscope/android/analytics/summary/c;

    iget-object v1, p0, Lcyc;->O:Ljava/lang/String;

    .line 762
    invoke-static {v1}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    iget-object v4, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 763
    invoke-interface {v4}, Ltv/periscope/android/library/c;->j()Ltv/periscope/android/session/a;

    move-result-object v4

    invoke-interface {v4}, Ltv/periscope/android/session/a;->a()Ltv/periscope/android/session/Session;

    move-result-object v4

    iget-object v5, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v5}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v5

    invoke-interface {v5}, Lcyw;->b()Ltv/periscope/android/api/PsUser;

    move-result-object v5

    iget-boolean v5, v5, Ltv/periscope/android/api/PsUser;->isVerified:Z

    iget-object v6, p0, Lcyc;->E:Ljava/lang/String;

    .line 761
    invoke-static/range {v0 .. v6}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/summary/c;Ljava/lang/String;Lcyd;ZLtv/periscope/android/session/Session;ZLjava/lang/String;)V

    .line 765
    iget-object v0, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    iget-object v2, p0, Lcyc;->O:Ljava/lang/String;

    iget-object v3, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    .line 766
    invoke-interface {v3}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->g()Z

    move-result v8

    iget-object v3, p0, Lcyc;->i:Lcyb;

    .line 767
    invoke-virtual {v3}, Lcyb;->c()I

    move-result v9

    iget-object v3, p0, Lcyc;->i:Lcyb;

    invoke-virtual {v3}, Lcyb;->f()I

    move-result v10

    move-object v3, v13

    move-object v4, v14

    move v5, v12

    move v6, v11

    .line 765
    invoke-interface/range {v0 .. v10}, Ltv/periscope/android/api/ApiManager;->publishBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;ZFFZII)Ljava/lang/String;

    .line 768
    return-void

    .line 747
    :cond_1
    const/4 v0, 0x0

    move v12, v0

    goto :goto_0

    :cond_2
    move v11, v7

    .line 753
    goto :goto_1
.end method

.method private B()V
    .locals 5

    .prologue
    .line 829
    invoke-direct {p0}, Lcyc;->E()V

    .line 831
    iget-object v0, p0, Lcyc;->i:Lcyb;

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcyb;->b(Ljava/lang/String;)V

    .line 832
    iget-object v0, p0, Lcyc;->z:Ltv/periscope/android/util/a;

    iget-object v1, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v0, v1, p0}, Ltv/periscope/android/util/a;->b(Landroid/content/Context;Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    .line 834
    iget-object v0, p0, Lcyc;->i:Lcyb;

    invoke-virtual {v0}, Lcyb;->g()Ljava/util/HashMap;

    move-result-object v0

    .line 835
    const-string/jumbo v1, "PeriscopeBroadcaster"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Broadcast stats: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 838
    const-string/jumbo v2, "n_screenshots"

    iget v3, p0, Lcyc;->U:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 840
    iget-object v2, p0, Lcyc;->j:Lcxy;

    invoke-virtual {v2}, Lcxy;->h()Ltv/periscope/android/api/ChatStats;

    move-result-object v2

    .line 841
    iget-object v3, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v3}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v3

    iget-object v4, p0, Lcyc;->F:Ljava/lang/String;

    invoke-interface {v3, v4, v0, v1, v2}, Ltv/periscope/android/api/ApiManager;->broadcastMeta(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;Ltv/periscope/android/api/ChatStats;)Ljava/lang/String;

    .line 842
    return-void
.end method

.method private C()V
    .locals 2

    .prologue
    .line 845
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 846
    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 847
    iget-object v1, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v1

    invoke-interface {v1, v0}, Ltv/periscope/android/api/ApiManager;->getBroadcasts(Ljava/util/ArrayList;)Ljava/lang/String;

    .line 848
    return-void
.end method

.method private D()V
    .locals 2

    .prologue
    .line 851
    iget-object v0, p0, Lcyc;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcyc;->V:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 852
    iget-object v0, p0, Lcyc;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcyc;->V:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 853
    return-void
.end method

.method private E()V
    .locals 2

    .prologue
    .line 856
    iget-object v0, p0, Lcyc;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcyc;->V:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 857
    return-void
.end method

.method private F()V
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->b()Z

    move-result v0

    .line 873
    if-nez v0, :cond_0

    .line 875
    invoke-virtual {p0}, Lcyc;->j()V

    .line 877
    :cond_0
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->c()V

    .line 878
    return-void
.end method

.method private G()V
    .locals 1

    .prologue
    .line 884
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 885
    invoke-virtual {p0}, Lcyc;->j()V

    .line 887
    :cond_0
    return-void
.end method

.method private H()Z
    .locals 1

    .prologue
    .line 890
    iget-object v0, p0, Lcyc;->M:Ltv/periscope/android/ui/broadcast/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcyc;->M:Ltv/periscope/android/ui/broadcast/p;

    iget-boolean v0, v0, Ltv/periscope/android/ui/broadcast/p;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcyc;->G:Landroid/location/Location;

    .line 891
    invoke-static {v0}, Lczr;->a(Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 890
    :goto_0
    return v0

    .line 891
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private I()V
    .locals 4

    .prologue
    .line 895
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    .line 896
    if-nez v0, :cond_0

    .line 908
    :goto_0
    return-void

    .line 900
    :cond_0
    new-instance v1, Ltv/periscope/android/ui/broadcast/p;

    invoke-interface {v0}, Lcyd;->a()Z

    move-result v2

    .line 901
    invoke-interface {v0}, Lcyd;->g()Z

    move-result v3

    invoke-interface {v0}, Lcyd;->h()Z

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Ltv/periscope/android/ui/broadcast/p;-><init>(ZZZ)V

    .line 902
    iput-object v1, p0, Lcyc;->M:Ltv/periscope/android/ui/broadcast/p;

    .line 903
    iget-object v0, p0, Lcyc;->o:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 904
    sget-object v2, Lcxz;->e:Ljava/lang/String;

    iget-boolean v3, v1, Ltv/periscope/android/ui/broadcast/p;->c:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 905
    sget-object v2, Lcxz;->c:Ljava/lang/String;

    iget-boolean v3, v1, Ltv/periscope/android/ui/broadcast/p;->a:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 906
    sget-object v2, Lcxz;->d:Ljava/lang/String;

    iget-boolean v1, v1, Ltv/periscope/android/ui/broadcast/p;->b:Z

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 907
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method static synthetic a(Lcyc;Landroid/location/Location;)Landroid/location/Location;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcyc;->G:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic a(Lcyc;)Lcxy;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcyc;->j:Lcxy;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Ltv/periscope/android/library/c;Ltv/periscope/android/view/t;Lcxw;Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;Ljava/lang/String;Z)Lcyc;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltv/periscope/android/permissions/PermissionsException;
        }
    .end annotation

    .prologue
    .line 211
    new-instance v5, Ltv/periscope/android/ui/user/f;

    invoke-direct {v5}, Ltv/periscope/android/ui/user/f;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lcyc;->a(Landroid/app/Activity;Ltv/periscope/android/library/c;Ltv/periscope/android/view/t;Lcxw;Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;Ltv/periscope/android/ui/user/a;Ljava/lang/String;Z)Lcyc;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Ltv/periscope/android/library/c;Ltv/periscope/android/view/t;Lcxw;Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;Ltv/periscope/android/ui/user/a;Ljava/lang/String;Z)Lcyc;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltv/periscope/android/permissions/PermissionsException;
        }
    .end annotation

    .prologue
    .line 220
    sget-object v0, Lcyc;->a:[Ljava/lang/String;

    invoke-static {p0, v0}, Ltv/periscope/android/permissions/b;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    new-instance v0, Ltv/periscope/android/permissions/PermissionsException;

    const-string/jumbo v1, "Required permissions not granted"

    invoke-direct {v0, v1}, Ltv/periscope/android/permissions/PermissionsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 223
    :cond_0
    new-instance v0, Lcyc;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcyc;-><init>(Landroid/app/Activity;Ltv/periscope/android/library/c;Ltv/periscope/android/view/t;Lcxw;Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;Ltv/periscope/android/ui/user/a;Ljava/lang/String;Z)V

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;)Ltv/periscope/android/view/t;
    .locals 9

    .prologue
    .line 350
    new-instance v0, Ltv/periscope/android/ui/c;

    iget-object v1, p0, Lcyc;->c:Landroid/app/Activity;

    iget-object v2, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v2

    iget-object v3, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v3}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    iget-object v6, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 351
    invoke-interface {v6}, Ltv/periscope/android/library/c;->u()Ldae;

    move-result-object v6

    iget-object v7, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v7}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v8

    move-object v7, p1

    invoke-direct/range {v0 .. v8}, Ltv/periscope/android/ui/c;-><init>(Landroid/content/Context;Ltv/periscope/android/api/ApiManager;Lcyw;Ltv/periscope/android/view/ab$a;Lcsa;Ldae;Landroid/view/ViewGroup;Lde/greenrobot/event/c;)V

    .line 350
    return-object v0
.end method

.method private a(Ltv/periscope/android/ui/broadcaster/prebroadcast/d;Lczq;Ltv/periscope/android/ui/user/a;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 403
    new-instance v3, Ltv/periscope/android/ui/broadcast/p;

    iget-object v0, p0, Lcyc;->o:Landroid/content/SharedPreferences;

    sget-object v1, Lcxz;->c:Ljava/lang/String;

    .line 404
    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lcyc;->o:Landroid/content/SharedPreferences;

    sget-object v2, Lcxz;->d:Ljava/lang/String;

    .line 405
    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v2, p0, Lcyc;->o:Landroid/content/SharedPreferences;

    sget-object v4, Lcxz;->e:Ljava/lang/String;

    .line 406
    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-direct {v3, v0, v1, v2}, Ltv/periscope/android/ui/broadcast/p;-><init>(ZZZ)V

    .line 408
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    iget-object v4, p0, Lcyc;->x:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    iget-object v1, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 409
    invoke-interface {v1}, Ltv/periscope/android/library/c;->v()Ldae;

    move-result-object v5

    move-object v1, p2

    move-object v2, p3

    .line 408
    invoke-static/range {v0 .. v5}, Ltv/periscope/android/ui/broadcaster/prebroadcast/b;->a(Landroid/app/Activity;Lczq;Ltv/periscope/android/ui/user/a;Ltv/periscope/android/ui/broadcast/p;Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;Ldae;)Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    move-result-object v0

    iput-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    .line 410
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0, p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->a(Ltv/periscope/android/ui/broadcaster/prebroadcast/a$c;)V

    .line 411
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0, p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->a(Ltv/periscope/android/ui/broadcaster/prebroadcast/a$a;)V

    .line 412
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0, p0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->a(Ltv/periscope/android/ui/broadcaster/prebroadcast/a$b;)V

    .line 413
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->a(Ljava/lang/Object;)V

    .line 414
    iget-object v0, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v0

    invoke-interface {v0}, Lcyw;->f()Ltv/periscope/model/user/f;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 415
    invoke-direct {p0}, Lcyc;->u()V

    .line 417
    :cond_0
    return-void
.end method

.method private a(ZI)V
    .locals 3
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 678
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v1, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcyc;->n:Landroid/content/res/Resources;

    .line 679
    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcyc;->n:Landroid/content/res/Resources;

    sget v2, Ltv/periscope/android/library/f$l;->ps__dialog_btn_confirm_end_broadcast:I

    .line 680
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcyc$b;

    invoke-direct {v2, p0, p1}, Lcyc$b;-><init>(Lcyc;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcyc;->n:Landroid/content/res/Resources;

    sget v2, Ltv/periscope/android/library/f$l;->ps__dialog_btn_cancel_end_broadcast:I

    .line 682
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 683
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 684
    return-void
.end method

.method static synthetic b(Lcyc;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcyc;->F:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcyc;)Ltv/periscope/android/ui/broadcast/au;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcyc;->k:Ltv/periscope/android/ui/broadcast/au;

    return-object v0
.end method

.method static synthetic d(Lcyc;)Ltv/periscope/android/ui/broadcaster/BroadcasterView;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    return-object v0
.end method

.method static synthetic e(Lcyc;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcyc;->n:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic f(Lcyc;)Ltv/periscope/android/ui/broadcast/ChatRoomView;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    return-object v0
.end method

.method static synthetic g(Lcyc;)Ltv/periscope/android/analytics/summary/a;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    return-object v0
.end method

.method static synthetic h(Lcyc;)Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcyc;->J:Z

    return v0
.end method

.method static synthetic i(Lcyc;)Lcyb;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcyc;->i:Lcyb;

    return-object v0
.end method

.method static synthetic j(Lcyc;)Ltv/periscope/android/library/c;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    return-object v0
.end method

.method static synthetic k(Lcyc;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcyc;->d:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic l(Lcyc;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    return-object v0
.end method

.method private s()V
    .locals 3

    .prologue
    .line 355
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 356
    sget v1, Ltv/periscope/android/library/f$d;->ps__black:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 357
    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 359
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 360
    const/4 v2, 0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->rotationAnimation:I

    .line 361
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 363
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 364
    return-void
.end method

.method private t()V
    .locals 3

    .prologue
    .line 390
    iget-object v0, p0, Lcyc;->k:Ltv/periscope/android/ui/broadcast/au;

    iget-object v1, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v1

    iget-object v2, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->v()Ldae;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/au;->a(Lcyw;Ldae;)V

    .line 391
    iget-object v0, p0, Lcyc;->k:Ltv/periscope/android/ui/broadcast/au;

    iget-object v1, p0, Lcyc;->j:Lcxy;

    invoke-virtual {v1}, Lcxy;->i()Ltv/periscope/android/view/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/au;->a(Ltv/periscope/android/view/i;)V

    .line 392
    iget-object v0, p0, Lcyc;->k:Ltv/periscope/android/ui/broadcast/au;

    new-instance v1, Lcyc$4;

    invoke-direct {v1, p0}, Lcyc$4;-><init>(Lcyc;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/au;->a(Ltv/periscope/android/view/CarouselView$a;)V

    .line 399
    return-void
.end method

.method private u()V
    .locals 4

    .prologue
    .line 420
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    iget-object v1, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 421
    invoke-interface {v1}, Ltv/periscope/android/library/c;->p()Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v2

    iget-object v3, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v3}, Ltv/periscope/android/library/c;->s()Lcxi;

    move-result-object v3

    .line 420
    invoke-static {v0, v1, v2, v3}, Lcxn;->a(Landroid/content/Context;Landroid/content/SharedPreferences;Lcyw;Lcxi;)Lcxm;

    move-result-object v0

    .line 422
    iget-object v1, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    new-instance v2, Lcxr;

    invoke-direct {v2, v0}, Lcxr;-><init>(Lcxm;)V

    invoke-interface {v1, v2}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->a(Lcxr;)V

    .line 423
    return-void
.end method

.method private v()V
    .locals 6

    .prologue
    .line 610
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "BroadcasterActivity"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 611
    iget-object v1, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFilesDir()Ljava/io/File;

    move-result-object v1

    .line 612
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "broadcaster_logs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 613
    new-instance v2, Lczz;

    new-instance v3, Ldaa$a;

    const-string/jumbo v4, "broadcaster_log"

    const-string/jumbo v5, ".txt"

    invoke-direct {v3, v0, v4, v5, v1}, Ldaa$a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lczz;-><init>(Ldaa$a;)V

    iput-object v2, p0, Lcyc;->B:Lczz;

    .line 615
    invoke-static {}, Ldac;->a()Ldac;

    move-result-object v0

    iget-object v1, p0, Lcyc;->B:Lczz;

    invoke-virtual {v0, v1}, Ldac;->a(Ldad;)Ldad;

    .line 616
    iget-object v0, p0, Lcyc;->B:Lczz;

    invoke-static {v0}, Ltv/periscope/android/util/t;->a(Ldad;)V

    .line 617
    iget-object v0, p0, Lcyc;->B:Lczz;

    const-string/jumbo v1, "=================================================="

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 618
    iget-object v0, p0, Lcyc;->B:Lczz;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Android OS Version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ltv/periscope/android/util/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 619
    iget-object v0, p0, Lcyc;->B:Lczz;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Model Info: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ltv/periscope/android/util/j;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 620
    iget-object v0, p0, Lcyc;->B:Lczz;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "App Version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-static {v2}, Ltv/periscope/android/util/ak;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 621
    return-void
.end method

.method private w()V
    .locals 2

    .prologue
    .line 627
    invoke-static {}, Ldac;->a()Ldac;

    move-result-object v0

    iget-object v1, p0, Lcyc;->B:Lczz;

    invoke-virtual {v0, v1}, Ldac;->b(Ldad;)Z

    .line 628
    iget-object v0, p0, Lcyc;->B:Lczz;

    invoke-static {v0}, Ltv/periscope/android/util/t;->b(Ldad;)Z

    .line 629
    iget-object v0, p0, Lcyc;->B:Lczz;

    invoke-virtual {v0}, Lczz;->a()V

    .line 630
    return-void
.end method

.method private x()V
    .locals 3

    .prologue
    .line 687
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 698
    :goto_0
    return-void

    .line 690
    :cond_0
    iget-object v0, p0, Lcyc;->D:Landroid/support/v7/app/AlertDialog;

    if-nez v0, :cond_1

    .line 691
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v1, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcyc;->n:Landroid/content/res/Resources;

    sget v2, Ltv/periscope/android/library/f$l;->ps__broadcaster_audio_muted:I

    .line 692
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcyc;->n:Landroid/content/res/Resources;

    sget v2, Ltv/periscope/android/library/f$l;->ps__broadcaster_unmute:I

    .line 693
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcyc$a;

    invoke-direct {v2, p0}, Lcyc$a;-><init>(Lcyc;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcyc;->n:Landroid/content/res/Resources;

    sget v2, Ltv/periscope/android/library/f$l;->ps__dialog_btn_cancel_end_broadcast:I

    .line 695
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcyc;->D:Landroid/support/v7/app/AlertDialog;

    .line 697
    :cond_1
    iget-object v0, p0, Lcyc;->D:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private y()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 701
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-static {v0}, Ltv/periscope/android/util/n;->a(Landroid/view/View;)V

    .line 702
    iget-object v0, p0, Lcyc;->i:Lcyb;

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcyb;->a(Ljava/lang/String;)V

    .line 703
    iget-object v0, p0, Lcyc;->z:Ltv/periscope/android/util/a;

    iget-object v1, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v0, v1, p0}, Ltv/periscope/android/util/a;->a(Landroid/content/Context;Landroid/media/AudioManager$OnAudioFocusChangeListener;)Z

    .line 704
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->e()V

    .line 705
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v0, v2}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setDraggable(Z)V

    .line 706
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->d()V

    .line 707
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->a()V

    .line 708
    iput-boolean v2, p0, Lcyc;->R:Z

    .line 709
    iget-object v0, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    sget-object v1, Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;->c:Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->setBottomTrayState(Ltv/periscope/android/ui/broadcast/ChatRoomView$BottomTrayState;)V

    .line 710
    iget-object v0, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-static {}, Ltv/periscope/model/chat/Message;->Q()Ltv/periscope/model/chat/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ltv/periscope/model/chat/Message;)V

    .line 711
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->l()V

    .line 712
    iget-object v0, p0, Lcyc;->w:Lcxw;

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcxw;->a(Ljava/lang/String;)V

    .line 714
    invoke-direct {p0}, Lcyc;->z()V

    .line 715
    return-void
.end method

.method private z()V
    .locals 5

    .prologue
    .line 718
    new-instance v0, Lcxp;

    iget-object v1, p0, Lcyc;->c:Landroid/app/Activity;

    iget-object v2, p0, Lcyc;->o:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 719
    invoke-interface {v3}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v3

    iget-object v4, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v4}, Ltv/periscope/android/library/c;->s()Lcxi;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcxn;->a(Landroid/content/Context;Landroid/content/SharedPreferences;Lcyw;Lcxi;)Lcxm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcxp;-><init>(Lcxm;)V

    .line 720
    invoke-virtual {v0}, Lcxp;->b()Ldcd;

    move-result-object v1

    .line 721
    if-eqz v1, :cond_0

    .line 722
    iget-object v2, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    new-instance v3, Lcyc$6;

    invoke-direct {v3, p0, v1, v0}, Lcyc$6;-><init>(Lcyc;Ldcd;Lcxp;)V

    sget-wide v0, Lcyc;->b:J

    invoke-virtual {v2, v3, v0, v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 740
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->getSettings()Ljava/lang/String;

    .line 342
    iget-object v0, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->performUploadTest()V

    .line 343
    iget-object v0, p0, Lcyc;->x:Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/PreBroadcastOptionsDelegate;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    iget-object v1, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v1

    invoke-interface {v1}, Lcyw;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->b(Ljava/lang/String;)V

    .line 346
    :cond_0
    return-void
.end method

.method public a(Landroid/location/Location;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 383
    iput-object p1, p0, Lcyc;->G:Landroid/location/Location;

    .line 384
    iput-object p3, p0, Lcyc;->H:Landroid/os/Bundle;

    .line 385
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcyc;->I:Z

    .line 386
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0, p2}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->a(Ljava/lang/String;)V

    .line 387
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 367
    const-string/jumbo v0, ""

    .line 368
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 369
    if-eqz p2, :cond_0

    .line 370
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 375
    :cond_0
    :goto_0
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->c(Ljava/lang/String;)V

    .line 376
    return-void

    :cond_1
    move-object p1, v0

    goto :goto_0
.end method

.method public a(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1046
    iget-boolean v1, p0, Lcyc;->P:Z

    .line 1047
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcyc;->P:Z

    .line 1050
    iget-object v0, p0, Lcyc;->K:Ltv/periscope/model/w;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcyc;->P:Z

    if-eq v1, v0, :cond_0

    .line 1051
    const-string/jumbo v1, "PeriscopeBroadcaster"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Switching "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lcyc;->P:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "to"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " private"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__initializing:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setButtonDisabledMessage(I)V

    .line 1053
    iget-object v0, p0, Lcyc;->i:Lcyb;

    iget-object v1, p0, Lcyc;->K:Ltv/periscope/model/w;

    iget-boolean v2, p0, Lcyc;->P:Z

    invoke-virtual {v0, v1, p0, v2}, Lcyb;->a(Ltv/periscope/model/w;Ltv/periscope/android/video/RTMPPublisher$a;Z)V

    .line 1055
    :cond_0
    return-void

    .line 1047
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1051
    :cond_2
    const-string/jumbo v0, "from"

    goto :goto_1
.end method

.method public a(Ltv/periscope/android/video/RTMPPublisher$PublishState;)V
    .locals 2

    .prologue
    .line 1060
    sget-object v0, Lcyc$8;->a:[I

    invoke-virtual {p1}, Ltv/periscope/android/video/RTMPPublisher$PublishState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1076
    :goto_0
    return-void

    .line 1063
    :pswitch_0
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    new-instance v1, Lcyc$7;

    invoke-direct {v1, p0}, Lcyc$7;-><init>(Lcyc;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1060
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ltv/periscope/model/chat/Message;I)V
    .locals 3

    .prologue
    .line 1040
    iget-object v0, p0, Lcyc;->k:Ltv/periscope/android/ui/broadcast/au;

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, p2, v2}, Ltv/periscope/android/ui/broadcast/au;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;IZ)V

    .line 1041
    return-void
.end method

.method a(Z)V
    .locals 4

    .prologue
    .line 776
    monitor-enter p0

    .line 777
    :try_start_0
    iget v0, p0, Lcyc;->T:I

    if-eqz v0, :cond_0

    .line 778
    const-string/jumbo v0, "PeriscopeBroadcaster"

    const-string/jumbo v1, "End has already been initiated or completed, nothing else to do"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    monitor-exit p0

    .line 805
    :goto_0
    return-void

    .line 781
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcyc;->T:I

    .line 782
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 783
    const-string/jumbo v0, "PeriscopeBroadcaster"

    const-string/jumbo v1, "Initiating end broadcast"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    iget-boolean v0, p0, Lcyc;->R:Z

    if-eqz v0, :cond_1

    .line 789
    iget-object v0, p0, Lcyc;->j:Lcxy;

    invoke-virtual {v0}, Lcxy;->d()V

    .line 790
    iget-object v0, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    iget-object v2, p0, Lcyc;->B:Lczz;

    invoke-virtual {v2}, Lczz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/api/ApiManager;->endBroadcast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 794
    :cond_1
    if-eqz p1, :cond_2

    .line 795
    iget-object v0, p0, Lcyc;->i:Lcyb;

    iget-object v1, p0, Lcyc;->X:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcyb;->a(Ljava/lang/Runnable;)V

    .line 797
    const-string/jumbo v0, "PeriscopeBroadcaster"

    const-string/jumbo v1, "Setting timer of 1000 for how long we\'ll wait before completing shutdown."

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    iget-object v0, p0, Lcyc;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcyc;->X:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 801
    iget-object v0, p0, Lcyc;->t:Ltv/periscope/android/ui/broadcast/j;

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/j;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 782
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 803
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcyc;->b(Z)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 584
    iget-object v0, p0, Lcyc;->p:Ltv/periscope/android/util/ae;

    invoke-virtual {v0}, Ltv/periscope/android/util/ae;->b()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 585
    iget v0, p0, Lcyc;->U:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcyc;->U:I

    .line 586
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    new-instance v1, Lcyc$5;

    invoke-direct {v1, p0}, Lcyc$5;-><init>(Lcyc;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 593
    :cond_0
    return-void
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 1021
    iget-object v0, p0, Lcyc;->t:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/j;->b(J)V

    .line 1022
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1023
    iget-object v0, p0, Lcyc;->t:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->g()V

    .line 1025
    :cond_0
    return-void
.end method

.method b(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 809
    iget v0, p0, Lcyc;->T:I

    if-ne v0, v4, :cond_0

    .line 825
    :goto_0
    return-void

    .line 812
    :cond_0
    invoke-direct {p0}, Lcyc;->B()V

    .line 814
    iget-boolean v0, p0, Lcyc;->R:Z

    if-eqz v0, :cond_1

    .line 815
    if-eqz p1, :cond_2

    .line 816
    iget-object v0, p0, Lcyc;->w:Lcxw;

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    iget-object v2, p0, Lcyc;->u:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v2}, Ltv/periscope/android/ui/broadcast/h;->d()Z

    move-result v2

    iget-object v3, p0, Lcyc;->i:Lcyb;

    .line 817
    invoke-virtual {v3}, Lcyb;->e()Ljava/lang/String;

    move-result-object v3

    .line 816
    invoke-interface {v0, v1, v2, v3}, Lcxw;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 818
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcyc;->S:Z

    .line 822
    :goto_1
    iget-object v0, p0, Lcyc;->j:Lcxy;

    invoke-virtual {v0}, Lcxy;->b()V

    .line 824
    :cond_1
    iput v4, p0, Lcyc;->T:I

    goto :goto_0

    .line 820
    :cond_2
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1
.end method

.method public b_(J)V
    .locals 1

    .prologue
    .line 1030
    iget-object v0, p0, Lcyc;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1031
    iget-object v0, p0, Lcyc;->t:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0, p1, p2}, Ltv/periscope/android/ui/broadcast/j;->a(J)V

    .line 1032
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1033
    iget-object v0, p0, Lcyc;->t:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->e()V

    .line 1035
    :cond_0
    return-void
.end method

.method public c()Lcyf;
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lcyc;->i:Lcyb;

    invoke-virtual {v0}, Lcyb;->d()Lcyf;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 642
    iget-object v1, p0, Lcyc;->y:Ltv/periscope/android/ui/user/a;

    invoke-interface {v1}, Ltv/periscope/android/ui/user/a;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    move v3, v0

    .line 674
    :goto_0
    return v3

    .line 644
    :cond_0
    iget-object v1, p0, Lcyc;->y:Ltv/periscope/android/ui/user/a;

    invoke-interface {v1}, Ltv/periscope/android/ui/user/a;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 645
    iget-object v1, p0, Lcyc;->y:Ltv/periscope/android/ui/user/a;

    invoke-interface {v1}, Ltv/periscope/android/ui/user/a;->b()V

    :goto_1
    move v3, v0

    .line 674
    goto :goto_0

    .line 646
    :cond_1
    iget-object v1, p0, Lcyc;->v:Ltv/periscope/android/view/t;

    invoke-interface {v1}, Ltv/periscope/android/view/t;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 647
    iget-object v1, p0, Lcyc;->v:Ltv/periscope/android/view/t;

    invoke-interface {v1}, Ltv/periscope/android/view/t;->i()V

    goto :goto_1

    .line 648
    :cond_2
    iget-object v1, p0, Lcyc;->l:Ltv/periscope/android/ui/broadcast/f;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/f;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 649
    iget-object v1, p0, Lcyc;->l:Ltv/periscope/android/ui/broadcast/f;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/f;->e()V

    goto :goto_1

    .line 650
    :cond_3
    iget-object v1, p0, Lcyc;->k:Ltv/periscope/android/ui/broadcast/au;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/au;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 651
    iget-object v1, p0, Lcyc;->k:Ltv/periscope/android/ui/broadcast/au;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcast/au;->e()V

    goto :goto_1

    .line 652
    :cond_4
    iget-object v1, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 653
    invoke-direct {p0}, Lcyc;->F()V

    goto :goto_1

    .line 654
    :cond_5
    iget-boolean v1, p0, Lcyc;->R:Z

    if-eqz v1, :cond_6

    .line 655
    sget v1, Ltv/periscope/android/library/f$l;->ps__dialog_message_end_broadcast:I

    invoke-direct {p0, v3, v1}, Lcyc;->a(ZI)V

    goto :goto_1

    .line 656
    :cond_6
    iget-object v1, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 657
    sget v1, Ltv/periscope/android/library/f$l;->ps__dialog_message_cancel_broadcast:I

    invoke-direct {p0, v0, v1}, Lcyc;->a(ZI)V

    goto :goto_1

    .line 660
    :cond_7
    iget-boolean v0, p0, Lcyc;->R:Z

    if-nez v0, :cond_8

    .line 661
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->k()Ljava/lang/String;

    move-result-object v1

    .line 662
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcyc;->Q:Z

    .line 664
    iget-object v0, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    check-cast v0, Ltv/periscope/android/analytics/summary/c;

    .line 665
    invoke-static {v1}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    iget-object v4, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 666
    invoke-interface {v4}, Ltv/periscope/android/library/c;->j()Ltv/periscope/android/session/a;

    move-result-object v4

    invoke-interface {v4}, Ltv/periscope/android/session/a;->a()Ltv/periscope/android/session/Session;

    move-result-object v4

    iget-object v5, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v5}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v5

    invoke-interface {v5}, Lcyw;->b()Ltv/periscope/android/api/PsUser;

    move-result-object v5

    iget-boolean v5, v5, Ltv/periscope/android/api/PsUser;->isVerified:Z

    iget-object v6, p0, Lcyc;->E:Ljava/lang/String;

    .line 664
    invoke-static/range {v0 .. v6}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/summary/c;Ljava/lang/String;Lcyd;ZLtv/periscope/android/session/Session;ZLjava/lang/String;)V

    .line 667
    iget-object v0, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    check-cast v0, Ltv/periscope/android/analytics/summary/c;

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/summary/c;)V

    .line 668
    iget-object v0, p0, Lcyc;->w:Lcxw;

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcxw;->b(Ljava/lang/String;)V

    .line 671
    :cond_8
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    sget v1, Ltv/periscope/android/library/f$a;->ps__grow_fade_in:I

    sget v2, Ltv/periscope/android/library/f$a;->ps__slide_to_bottom:I

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto/16 :goto_0
.end method

.method e()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 911
    iget v1, p0, Lcyc;->T:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcyc;->T:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 3

    .prologue
    .line 942
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__starting_broadcast:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setButtonDisabledMessage(I)V

    .line 943
    invoke-direct {p0}, Lcyc;->A()V

    .line 944
    iget-object v0, p0, Lcyc;->o:Landroid/content/SharedPreferences;

    sget-object v1, Lcxz;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 945
    iget-object v0, p0, Lcyc;->o:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcxz;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 947
    :cond_0
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 952
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    iget-object v1, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->getCameraPreview()Ltv/periscope/android/ui/broadcaster/CameraPreviewLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 953
    iget-object v0, p0, Lcyc;->B:Lczz;

    const-string/jumbo v1, "Stop Broadcast pressed"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 954
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcyc;->a(Z)V

    .line 956
    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 961
    iget-object v0, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    check-cast v0, Ltv/periscope/android/analytics/summary/c;

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->b(Ltv/periscope/android/analytics/summary/c;)V

    .line 962
    iget-object v0, p0, Lcyc;->i:Lcyb;

    invoke-virtual {v0}, Lcyb;->i()V

    .line 963
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 968
    iget-object v0, p0, Lcyc;->j:Lcxy;

    invoke-virtual {v0}, Lcxy;->c()V

    .line 970
    invoke-direct {p0}, Lcyc;->C()V

    .line 971
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 980
    iget-object v0, p0, Lcyc;->t:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/j;->n()V

    .line 981
    return-void
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 986
    iget-object v0, p0, Lcyc;->l:Ltv/periscope/android/ui/broadcast/f;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/f;->f()Z

    move-result v0

    return v0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 992
    iget-object v0, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    invoke-static {v0}, Ltv/periscope/android/analytics/d;->h(Lcrz;)V

    .line 993
    iget-object v0, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->e()V

    .line 994
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 999
    iget-object v0, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->d()V

    .line 1000
    return-void
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 1005
    iget-object v0, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->g()Z

    move-result v0

    return v0
.end method

.method public o()V
    .locals 1

    .prologue
    .line 1010
    iget-object v0, p0, Lcyc;->j:Lcxy;

    invoke-virtual {v0}, Lcxy;->f()V

    .line 1011
    return-void
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    if-ne p1, v0, :cond_0

    .line 430
    :cond_0
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 512
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    if-ne p1, v0, :cond_0

    .line 513
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 514
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/Application;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 515
    iget-object v0, p0, Lcyc;->i:Lcyb;

    invoke-virtual {v0}, Lcyb;->h()V

    .line 516
    iget-object v0, p0, Lcyc;->j:Lcxy;

    invoke-virtual {v0}, Lcxy;->b()V

    .line 517
    iget-object v0, p0, Lcyc;->z:Ltv/periscope/android/util/a;

    iget-object v1, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v0, v1, p0}, Ltv/periscope/android/util/a;->b(Landroid/content/Context;Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    .line 518
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->j()V

    .line 520
    iget-object v0, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 521
    iget-object v0, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v0

    iget-object v1, p0, Lcyc;->u:Ltv/periscope/android/ui/broadcast/h;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 522
    iget-object v0, p0, Lcyc;->v:Ltv/periscope/android/view/t;

    invoke-interface {v0}, Ltv/periscope/android/view/t;->g()V

    .line 523
    iget-object v0, p0, Lcyc;->y:Ltv/periscope/android/ui/user/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/user/a;->f()V

    .line 525
    iget-object v0, p0, Lcyc;->s:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->g()V

    .line 527
    iget-boolean v0, p0, Lcyc;->S:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcyc;->u:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/h;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 528
    iget-object v0, p0, Lcyc;->i:Lcyb;

    invoke-virtual {v0}, Lcyb;->b()V

    .line 529
    iget-object v0, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    check-cast v0, Ltv/periscope/android/analytics/summary/c;

    invoke-static {v0, v2, v2}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/summary/c;ZZ)V

    .line 534
    :goto_0
    invoke-direct {p0}, Lcyc;->w()V

    .line 536
    :cond_0
    return-void

    .line 531
    :cond_1
    iget-object v0, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    check-cast v0, Ltv/periscope/android/analytics/summary/c;

    const/4 v1, 0x1

    iget-object v2, p0, Lcyc;->u:Ltv/periscope/android/ui/broadcast/h;

    invoke-interface {v2}, Ltv/periscope/android/ui/broadcast/h;->e()Z

    move-result v2

    invoke-static {v0, v1, v2}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/summary/c;ZZ)V

    goto :goto_0
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 459
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    if-ne p1, v0, :cond_0

    .line 460
    iget-object v0, p0, Lcyc;->s:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->e()V

    .line 461
    iget-object v0, p0, Lcyc;->B:Lczz;

    const-string/jumbo v1, "onPause"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 463
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    .line 464
    iget-object v0, p0, Lcyc;->q:Lczq;

    invoke-virtual {v0}, Lczq;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcyc;->q:Lczq;

    invoke-virtual {v0}, Lczq;->g()V

    .line 469
    :cond_0
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    if-ne p1, v0, :cond_0

    .line 444
    iget-object v0, p0, Lcyc;->A:Ltv/periscope/android/network/ConnectivityChangeReceiver;

    invoke-virtual {v0}, Ltv/periscope/android/network/ConnectivityChangeReceiver;->a()V

    .line 445
    iget-object v0, p0, Lcyc;->s:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->f()V

    .line 446
    iget-object v0, p0, Lcyc;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcyc;->W:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 450
    iget-object v0, p0, Lcyc;->B:Lczz;

    const-string/jumbo v1, "onResume"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 451
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcyc;->q:Lczq;

    invoke-virtual {v0}, Lczq;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcyc;->q:Lczq;

    invoke-virtual {v0}, Lczq;->f()V

    .line 455
    :cond_0
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    if-ne p1, v0, :cond_0

    .line 508
    :cond_0
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    if-ne p1, v0, :cond_0

    .line 435
    iget-object v0, p0, Lcyc;->B:Lczz;

    const-string/jumbo v1, "onStart"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 436
    iget-object v0, p0, Lcyc;->p:Ltv/periscope/android/util/ae;

    invoke-virtual {v0, p0}, Ltv/periscope/android/util/ae;->a(Ltv/periscope/android/util/ae$a;)V

    .line 437
    iget-object v0, p0, Lcyc;->q:Lczq;

    invoke-virtual {v0}, Lczq;->c()V

    .line 439
    :cond_0
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 473
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    if-ne p1, v0, :cond_2

    .line 474
    iget-object v0, p0, Lcyc;->B:Lczz;

    const-string/jumbo v1, "onStop"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 475
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 476
    iget-object v0, p0, Lcyc;->q:Lczq;

    invoke-virtual {v0}, Lczq;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcyc;->q:Lczq;

    invoke-virtual {v0}, Lczq;->g()V

    .line 480
    :cond_0
    iget-object v0, p0, Lcyc;->p:Ltv/periscope/android/util/ae;

    invoke-virtual {v0}, Ltv/periscope/android/util/ae;->a()V

    .line 481
    iget-object v0, p0, Lcyc;->q:Lczq;

    invoke-virtual {v0}, Lczq;->d()V

    .line 482
    iget-object v0, p0, Lcyc;->A:Ltv/periscope/android/network/ConnectivityChangeReceiver;

    invoke-virtual {v0}, Ltv/periscope/android/network/ConnectivityChangeReceiver;->b()V

    .line 488
    iget-object v0, p0, Lcyc;->M:Ltv/periscope/android/ui/broadcast/p;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcyc;->o:Landroid/content/SharedPreferences;

    sget-object v1, Lcxz;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 489
    invoke-direct {p0}, Lcyc;->I()V

    .line 492
    :cond_1
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 493
    const-string/jumbo v0, "PeriscopeBroadcaster"

    const-string/jumbo v1, "onStop and Finishing, ending broadcast"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcyc;->a(Z)V

    .line 499
    :goto_0
    invoke-static {}, Ltv/periscope/android/util/aa;->a()V

    .line 501
    :cond_2
    return-void

    .line 496
    :cond_3
    const-string/jumbo v0, "PeriscopeBroadcaster"

    const-string/jumbo v1, "Unexpected onStop, setting 5000 second timer to end broadcast."

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    iget-object v0, p0, Lcyc;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcyc;->W:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onAudioFocusChange(I)V
    .locals 1

    .prologue
    .line 916
    invoke-virtual {p0}, Lcyc;->c()Lcyf;

    move-result-object v0

    if-nez v0, :cond_1

    .line 937
    :cond_0
    :goto_0
    return-void

    .line 919
    :cond_1
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 923
    :pswitch_1
    invoke-virtual {p0}, Lcyc;->c()Lcyf;

    move-result-object v0

    invoke-interface {v0}, Lcyf;->h()V

    .line 924
    invoke-direct {p0}, Lcyc;->x()V

    goto :goto_0

    .line 928
    :pswitch_2
    invoke-virtual {p0}, Lcyc;->c()Lcyf;

    move-result-object v0

    invoke-interface {v0}, Lcyf;->i()V

    .line 929
    iget-object v0, p0, Lcyc;->D:Landroid/support/v7/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 930
    iget-object v0, p0, Lcyc;->D:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->dismiss()V

    goto :goto_0

    .line 919
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 861
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 862
    sget v1, Ltv/periscope/android/library/f$g;->participants:I

    if-ne v0, v1, :cond_1

    .line 863
    invoke-direct {p0}, Lcyc;->F()V

    .line 867
    :cond_0
    :goto_0
    return-void

    .line 864
    :cond_1
    sget v1, Ltv/periscope/android/library/f$g;->overflow_button:I

    if-ne v0, v1, :cond_0

    .line 865
    iget-object v0, p0, Lcyc;->l:Ltv/periscope/android/ui/broadcast/f;

    iget-object v1, p0, Lcyc;->m:Ltv/periscope/android/ui/broadcast/b;

    iget-object v2, p0, Lcyc;->F:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v4, v3}, Ltv/periscope/android/ui/broadcast/b;->a(Ljava/lang/String;Ltv/periscope/model/chat/Message;Z)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ltv/periscope/android/ui/broadcast/f;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 597
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 598
    iget-object v1, p0, Lcyc;->i:Lcyb;

    invoke-virtual {v1, v0}, Lcyb;->a(I)V

    .line 599
    return-void
.end method

.method public onEventBackgroundThread([I)V
    .locals 3

    .prologue
    .line 1141
    const/16 v0, 0x140

    const/16 v1, 0x238

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1143
    iget-object v1, p0, Lcyc;->r:Lcye;

    invoke-virtual {v1, v0}, Lcye;->a(Landroid/graphics/Bitmap;)V

    .line 1144
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/chat/ChatRoomEvent;)V
    .locals 2

    .prologue
    .line 1089
    sget-object v0, Lcyc$8;->b:[I

    invoke-virtual {p1}, Ltv/periscope/android/chat/ChatRoomEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1099
    :cond_0
    :goto_0
    return-void

    .line 1091
    :pswitch_0
    invoke-direct {p0}, Lcyc;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1092
    iget-object v0, p0, Lcyc;->t:Ltv/periscope/android/ui/broadcast/j;

    iget-object v1, p0, Lcyc;->G:Landroid/location/Location;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/j;->a(Landroid/location/Location;)V

    .line 1093
    invoke-direct {p0}, Lcyc;->G()V

    .line 1094
    iget-object v0, p0, Lcyc;->j:Lcxy;

    iget-object v1, p0, Lcyc;->G:Landroid/location/Location;

    invoke-virtual {v0, v1}, Lcxy;->a(Landroid/location/Location;)V

    goto :goto_0

    .line 1089
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1148
    sget-object v0, Lcyc$8;->d:[I

    iget-object v2, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v2}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1243
    :cond_0
    :goto_0
    return-void

    .line 1150
    :pswitch_0
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/model/w;

    .line 1152
    invoke-virtual {v0}, Ltv/periscope/model/w;->c()Ltv/periscope/model/p;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcyc;->F:Ljava/lang/String;

    .line 1153
    iget-object v1, p0, Lcyc;->t:Ltv/periscope/android/ui/broadcast/j;

    iget-object v2, p0, Lcyc;->F:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ltv/periscope/android/ui/broadcast/j;->d(Ljava/lang/String;)V

    .line 1154
    iput-object v0, p0, Lcyc;->K:Ltv/periscope/model/w;

    .line 1155
    iget-object v1, p0, Lcyc;->i:Lcyb;

    iget-boolean v2, p0, Lcyc;->P:Z

    invoke-virtual {v1, v0, p0, v2}, Lcyb;->a(Ltv/periscope/model/w;Ltv/periscope/android/video/RTMPPublisher$a;Z)V

    .line 1157
    iget-object v1, p0, Lcyc;->i:Lcyb;

    invoke-virtual {v1}, Lcyb;->a()V

    .line 1160
    iget-object v1, p0, Lcyc;->r:Lcye;

    invoke-virtual {v0}, Ltv/periscope/model/w;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcye;->a(Ljava/lang/String;)V

    .line 1163
    invoke-virtual {v0}, Ltv/periscope/model/w;->a()Ltv/periscope/model/u;

    move-result-object v2

    .line 1164
    iget-object v1, p0, Lcyc;->B:Lczz;

    invoke-virtual {v2}, Ltv/periscope/model/u;->d()Z

    move-result v3

    invoke-virtual {v1, v3}, Lczz;->a(Z)V

    .line 1165
    invoke-virtual {v0}, Ltv/periscope/model/w;->c()Ltv/periscope/model/p;

    move-result-object v1

    iput-object v1, p0, Lcyc;->N:Ltv/periscope/model/p;

    .line 1167
    iget-object v1, p0, Lcyc;->f:Ltv/periscope/android/analytics/summary/a;

    check-cast v1, Ltv/periscope/android/analytics/summary/c;

    iget-object v3, p0, Lcyc;->F:Ljava/lang/String;

    iget-object v4, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    .line 1168
    invoke-interface {v4}, Ltv/periscope/android/library/c;->f()Lcyw;

    move-result-object v4

    invoke-interface {v4}, Lcyw;->c()Ljava/lang/String;

    move-result-object v4

    .line 1167
    invoke-static {v1, v3, v4}, Ltv/periscope/android/analytics/d;->a(Ltv/periscope/android/analytics/summary/c;Ljava/lang/String;Ljava/lang/String;)V

    .line 1171
    :try_start_0
    iget-object v1, p0, Lcyc;->j:Lcxy;

    invoke-virtual {v0}, Ltv/periscope/model/w;->c()Ltv/periscope/model/p;

    move-result-object v3

    invoke-virtual {v3}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ltv/periscope/model/w;->c()Ltv/periscope/model/p;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/p;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcxy;->a(Ltv/periscope/model/u;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    invoke-direct {p0}, Lcyc;->t()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1173
    :catch_0
    move-exception v0

    .line 1175
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1183
    :pswitch_1
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1184
    invoke-direct {p0}, Lcyc;->y()V

    .line 1186
    invoke-direct {p0}, Lcyc;->C()V

    .line 1187
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/api/PublishBroadcastResponse;

    .line 1188
    iget-object v1, p0, Lcyc;->h:Ltv/periscope/android/ui/broadcast/ChatRoomView;

    iget-object v2, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v2}, Ltv/periscope/android/library/c;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Ltv/periscope/android/api/PublishBroadcastResponse;->heartThemes:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1189
    iget-object v0, p0, Lcyc;->K:Ltv/periscope/model/w;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcyc;->P:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcyc;->Q:Z

    if-eqz v0, :cond_1

    .line 1190
    iget-object v0, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->n()Ltv/periscope/android/ui/broadcast/aq;

    move-result-object v0

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    iget-object v2, p0, Lcyc;->O:Ljava/lang/String;

    iget-object v3, p0, Lcyc;->K:Ltv/periscope/model/w;

    invoke-virtual {v3}, Ltv/periscope/model/w;->q()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcyc;->H:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, v3, v4}, Ltv/periscope/android/ui/broadcast/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1193
    :cond_1
    invoke-direct {p0}, Lcyc;->D()V

    goto/16 :goto_0

    .line 1195
    :cond_2
    iget-object v0, p0, Lcyc;->j:Lcxy;

    invoke-virtual {v0}, Lcxy;->b()V

    .line 1199
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__start_broadcast_error:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setButtonDisabledMessage(I)V

    .line 1200
    iget-object v0, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/api/ApiManager;->performUploadTest()V

    goto/16 :goto_0

    .line 1205
    :pswitch_2
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1206
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/api/UploadTestResponse;

    .line 1207
    iget-object v2, p0, Lcyc;->i:Lcyb;

    iget v3, v0, Ltv/periscope/android/api/UploadTestResponse;->byteRateSeconds:F

    invoke-virtual {v2, v3}, Lcyb;->a(F)Z

    move-result v2

    .line 1208
    if-eqz v2, :cond_3

    .line 1209
    iput-boolean v1, p0, Lcyc;->J:Z

    .line 1210
    iget-object v1, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v1}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v1

    iget-object v0, v0, Ltv/periscope/android/api/UploadTestResponse;->region:Ljava/lang/String;

    invoke-interface {v1, v0}, Ltv/periscope/android/api/ApiManager;->createBroadcast(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 1212
    :cond_3
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__bitrate_too_low:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setButtonDisabledMessage(I)V

    goto/16 :goto_0

    .line 1215
    :cond_4
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    sget v1, Ltv/periscope/android/library/f$l;->ps__bitrate_undefined:I

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->setButtonDisabledMessage(I)V

    .line 1216
    const-string/jumbo v0, "PeriscopeBroadcaster"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "An error occurred testing video byterates: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Ltv/periscope/android/event/ApiEvent;->e:Lretrofit/RetrofitError;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1221
    :pswitch_3
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1223
    iget-object v0, p0, Lcyc;->B:Lczz;

    invoke-virtual {v0}, Lczz;->b()Ljava/lang/String;

    move-result-object v0

    .line 1224
    iget-object v2, p0, Lcyc;->F:Ljava/lang/String;

    invoke-static {v2}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 1225
    :goto_1
    if-eqz v0, :cond_0

    .line 1226
    iget-object v0, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v0

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    iget-object v2, p0, Lcyc;->B:Lczz;

    invoke-virtual {v2}, Lczz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/api/ApiManager;->uploadBroadcasterLogs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 1224
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 1232
    :pswitch_4
    invoke-direct {p0}, Lcyc;->w()V

    goto/16 :goto_0

    .line 1236
    :pswitch_5
    invoke-direct {p0}, Lcyc;->u()V

    goto/16 :goto_0

    .line 1148
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onEventMainThread(Ltv/periscope/android/event/CacheEvent;)V
    .locals 3

    .prologue
    .line 1103
    iget-object v0, p0, Lcyc;->F:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1125
    :cond_0
    :goto_0
    return-void

    .line 1106
    :cond_1
    sget-object v0, Lcyc$8;->c:[I

    invoke-virtual {p1}, Ltv/periscope/android/event/CacheEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1108
    :pswitch_0
    invoke-direct {p0}, Lcyc;->H()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1109
    iget-object v0, p0, Lcyc;->t:Ltv/periscope/android/ui/broadcast/j;

    iget-object v1, p0, Lcyc;->G:Landroid/location/Location;

    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/j;->a(Landroid/location/Location;)V

    .line 1111
    :cond_2
    invoke-direct {p0}, Lcyc;->G()V

    .line 1112
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    iget-object v1, p0, Lcyc;->O:Ljava/lang/String;

    iget-object v2, p0, Lcyc;->N:Ltv/periscope/model/p;

    invoke-virtual {v2}, Ltv/periscope/model/p;->e()Ltv/periscope/model/z;

    move-result-object v2

    invoke-virtual {v2}, Ltv/periscope/model/z;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1116
    :pswitch_1
    iget-object v0, p0, Lcyc;->e:Ltv/periscope/android/library/c;

    invoke-interface {v0}, Ltv/periscope/android/library/c;->h()Lcyn;

    move-result-object v0

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcyn;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    .line 1117
    if-eqz v0, :cond_0

    .line 1118
    iput-object v0, p0, Lcyc;->N:Ltv/periscope/model/p;

    .line 1119
    invoke-direct {p0}, Lcyc;->G()V

    .line 1120
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    iget-object v1, p0, Lcyc;->O:Ljava/lang/String;

    iget-object v2, p0, Lcyc;->N:Ltv/periscope/model/p;

    invoke-virtual {v2}, Ltv/periscope/model/p;->e()Ltv/periscope/model/z;

    move-result-object v2

    invoke-virtual {v2}, Ltv/periscope/model/z;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ParticipantHeartCountEvent;)V
    .locals 1

    .prologue
    .line 1129
    iget-object v0, p0, Lcyc;->g:Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130
    iget-object v0, p0, Lcyc;->t:Ltv/periscope/android/ui/broadcast/j;

    invoke-virtual {v0, p1}, Ltv/periscope/android/ui/broadcast/j;->a(Ltv/periscope/android/event/ParticipantHeartCountEvent;)V

    .line 1132
    :cond_0
    return-void
.end method

.method public onEventMainThread(Ltv/periscope/android/network/NetworkMonitorInfo;)V
    .locals 2

    .prologue
    .line 1080
    if-eqz p1, :cond_0

    .line 1081
    const-string/jumbo v0, "PeriscopeBroadcaster"

    iget-object v1, p1, Ltv/periscope/android/network/NetworkMonitorInfo;->a:Landroid/os/Bundle;

    invoke-static {v1}, Ltv/periscope/android/network/a;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    :goto_0
    return-void

    .line 1083
    :cond_0
    const-string/jumbo v0, "PeriscopeBroadcaster"

    const-string/jumbo v1, "Received network activity but info was empty"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 603
    iget-object v0, p0, Lcyc;->B:Lczz;

    const-string/jumbo v1, "onLowMemory"

    invoke-virtual {v0, v1}, Lczz;->a(Ljava/lang/String;)V

    .line 604
    iget-object v0, p0, Lcyc;->s:Ltv/periscope/android/ui/broadcast/g;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/g;->h()V

    .line 605
    return-void
.end method

.method public p()V
    .locals 1

    .prologue
    .line 1015
    iget-object v0, p0, Lcyc;->j:Lcxy;

    invoke-virtual {v0}, Lcxy;->g()V

    .line 1016
    return-void
.end method

.method public q()V
    .locals 2

    .prologue
    .line 1247
    iget-object v0, p0, Lcyc;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1248
    iget-object v0, p0, Lcyc;->w:Lcxw;

    iget-object v1, p0, Lcyc;->F:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcxw;->b(Ljava/lang/String;)V

    .line 1249
    return-void
.end method

.method public r()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1253
    iput-object v1, p0, Lcyc;->G:Landroid/location/Location;

    .line 1254
    iput-object v1, p0, Lcyc;->H:Landroid/os/Bundle;

    .line 1255
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcyc;->I:Z

    .line 1256
    iget-object v0, p0, Lcyc;->L:Ltv/periscope/android/ui/broadcaster/prebroadcast/a;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcaster/prebroadcast/a;->a(Ljava/lang/String;)V

    .line 1257
    return-void
.end method
