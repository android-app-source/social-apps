.class public abstract Lwi;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/media/foundmedia/e;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/twitter/model/media/foundmedia/e;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/Session;I)V
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/twitter/android/util/j;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;I)V

    .line 39
    iput-object p3, p0, Lwi;->a:Ljava/lang/String;

    .line 40
    return-void

    .line 38
    :cond_0
    const/4 p5, 0x0

    goto :goto_0
.end method

.method private static s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    invoke-static {}, Lcom/twitter/android/util/j;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/twitter/library/service/d;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lwi;->e()Lcom/twitter/library/service/d$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/media/foundmedia/e;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/foundmedia/e;

    iput-object v0, p0, Lwi;->b:Lcom/twitter/model/media/foundmedia/e;

    .line 89
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 21
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lwi;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method protected e()Lcom/twitter/library/service/d$a;
    .locals 4

    .prologue
    .line 60
    invoke-virtual {p0}, Lwi;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->a:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 61
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "foundmedia"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 62
    invoke-virtual {p0}, Lwi;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 63
    invoke-static {}, Lwi;->s()Ljava/lang/String;

    move-result-object v1

    .line 64
    iget-object v2, p0, Lwi;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 65
    const-string/jumbo v2, "cursor"

    iget-object v3, p0, Lwi;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 67
    :cond_0
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 68
    const-string/jumbo v2, "provider"

    invoke-virtual {v0, v2, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 71
    :cond_1
    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lwi;->g()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method

.method protected g()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/media/foundmedia/e;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    const-class v0, Lcom/twitter/model/media/foundmedia/e;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/twitter/model/media/foundmedia/e;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lwi;->b:Lcom/twitter/model/media/foundmedia/e;

    return-object v0
.end method
