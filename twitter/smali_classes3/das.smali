.class public Ldas;
.super Ldaj;
.source "Twttr"


# instance fields
.field private final c:Ltv/periscope/model/AbuseType;

.field private final d:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final e:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/model/AbuseType;I)V
    .locals 6
    .param p4    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 22
    sget v5, Ltv/periscope/android/library/f$d;->ps__red:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Ldas;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/model/AbuseType;II)V

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;Ltv/periscope/model/AbuseType;II)V
    .locals 0
    .param p4    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ldaj;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/h;)V

    .line 28
    iput-object p3, p0, Ldas;->c:Ltv/periscope/model/AbuseType;

    .line 29
    iput p4, p0, Ldas;->d:I

    .line 30
    iput p5, p0, Ldas;->e:I

    .line 31
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Ldas;->d:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Ldas;->e:I

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    return v0
.end method

.method public e()Z
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Ldas;->b:Ltv/periscope/android/ui/broadcast/h;

    iget-object v1, p0, Ldas;->a:Ljava/lang/String;

    iget-object v2, p0, Ldas;->c:Ltv/periscope/model/AbuseType;

    invoke-interface {v0, v1, v2}, Ltv/periscope/android/ui/broadcast/h;->a(Ljava/lang/String;Ltv/periscope/model/AbuseType;)V

    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public g()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation

    .prologue
    .line 67
    iget v0, p0, Ldas;->e:I

    return v0
.end method
