.class synthetic Lcyc$8;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcyc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic a:[I

.field static final synthetic b:[I

.field static final synthetic c:[I

.field static final synthetic d:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1148
    invoke-static {}, Ltv/periscope/android/event/ApiEvent$Type;->values()[Ltv/periscope/android/event/ApiEvent$Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcyc$8;->d:[I

    :try_start_0
    sget-object v0, Lcyc$8;->d:[I

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->J:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_0
    :try_start_1
    sget-object v0, Lcyc$8;->d:[I

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->K:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_1
    :try_start_2
    sget-object v0, Lcyc$8;->d:[I

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->ab:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_2
    :try_start_3
    sget-object v0, Lcyc$8;->d:[I

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->V:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_3
    :try_start_4
    sget-object v0, Lcyc$8;->d:[I

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->U:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_4
    :try_start_5
    sget-object v0, Lcyc$8;->d:[I

    sget-object v1, Ltv/periscope/android/event/ApiEvent$Type;->i:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    .line 1106
    :goto_5
    invoke-static {}, Ltv/periscope/android/event/CacheEvent;->values()[Ltv/periscope/android/event/CacheEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcyc$8;->c:[I

    :try_start_6
    sget-object v0, Lcyc$8;->c:[I

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->o:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v1}, Ltv/periscope/android/event/CacheEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_6
    :try_start_7
    sget-object v0, Lcyc$8;->c:[I

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->n:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v1}, Ltv/periscope/android/event/CacheEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    .line 1089
    :goto_7
    invoke-static {}, Ltv/periscope/android/chat/ChatRoomEvent;->values()[Ltv/periscope/android/chat/ChatRoomEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcyc$8;->b:[I

    :try_start_8
    sget-object v0, Lcyc$8;->b:[I

    sget-object v1, Ltv/periscope/android/chat/ChatRoomEvent;->a:Ltv/periscope/android/chat/ChatRoomEvent;

    invoke-virtual {v1}, Ltv/periscope/android/chat/ChatRoomEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    .line 1060
    :goto_8
    invoke-static {}, Ltv/periscope/android/video/RTMPPublisher$PublishState;->values()[Ltv/periscope/android/video/RTMPPublisher$PublishState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcyc$8;->a:[I

    :try_start_9
    sget-object v0, Lcyc$8;->a:[I

    sget-object v1, Ltv/periscope/android/video/RTMPPublisher$PublishState;->b:Ltv/periscope/android/video/RTMPPublisher$PublishState;

    invoke-virtual {v1}, Ltv/periscope/android/video/RTMPPublisher$PublishState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_9
    return-void

    :catch_0
    move-exception v0

    goto :goto_9

    .line 1089
    :catch_1
    move-exception v0

    goto :goto_8

    .line 1106
    :catch_2
    move-exception v0

    goto :goto_7

    :catch_3
    move-exception v0

    goto :goto_6

    .line 1148
    :catch_4
    move-exception v0

    goto :goto_5

    :catch_5
    move-exception v0

    goto :goto_4

    :catch_6
    move-exception v0

    goto :goto_3

    :catch_7
    move-exception v0

    goto :goto_2

    :catch_8
    move-exception v0

    goto/16 :goto_1

    :catch_9
    move-exception v0

    goto/16 :goto_0
.end method
