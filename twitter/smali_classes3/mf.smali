.class public Lmf;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmf$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Lmf$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmf$a",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/j;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lmf$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmf$a",
            "<TK;TT;>;"
        }
    .end annotation
.end field

.field private final c:Lme;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lme",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lmf$1;

    invoke-direct {v0}, Lmf$1;-><init>()V

    sput-object v0, Lmf;->a:Lmf$a;

    return-void
.end method

.method public constructor <init>(Lmf$a;Lme;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmf$a",
            "<TK;TT;>;",
            "Lme",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmf;->d:Ljava/util/Set;

    .line 41
    iput-object p1, p0, Lmf;->b:Lmf$a;

    .line 42
    iput-object p2, p0, Lmf;->c:Lme;

    .line 43
    return-void
.end method

.method public static a(Lme;)Lmf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/twitter/model/core/j;",
            ">(",
            "Lme",
            "<TT;>;)",
            "Lmf",
            "<",
            "Ljava/lang/Long;",
            "TT;>;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v1, Lmf;

    sget-object v0, Lmf;->a:Lmf$a;

    .line 35
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmf$a;

    invoke-direct {v1, v0, p0}, Lmf;-><init>(Lmf$a;Lme;)V

    .line 34
    return-object v1
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lmf;->b:Lmf$a;

    invoke-interface {v0, p1}, Lmf$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lmf;->d:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 51
    iget-object v1, p0, Lmf;->d:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 52
    iget-object v0, p0, Lmf;->c:Lme;

    invoke-interface {v0, p1}, Lme;->a(Ljava/lang/Object;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 54
    :cond_0
    return-void
.end method
