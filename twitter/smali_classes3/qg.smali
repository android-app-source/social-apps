.class public Lqg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Lcom/twitter/library/av/playback/AVDataSource;",
        "Lqm;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/av/playback/AVDataSource;

.field private final b:Lqe;

.field private final c:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/av/i;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Lauj;Lqe;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            "Lauj",
            "<",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/av/i;",
            ">;>;",
            "Lqe;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lqg;->a:Lcom/twitter/library/av/playback/AVDataSource;

    .line 49
    iput-object p2, p0, Lqg;->c:Lauj;

    .line 50
    iput-object p3, p0, Lqg;->b:Lqe;

    .line 51
    return-void
.end method

.method static synthetic a(Lqg;)Lqe;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lqg;->b:Lqe;

    return-object v0
.end method

.method static synthetic b(Lqg;)Lcom/twitter/library/av/playback/AVDataSource;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lqg;->a:Lcom/twitter/library/av/playback/AVDataSource;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/playback/AVDataSource;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            ")",
            "Lrx/c",
            "<",
            "Lqm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lqg;->c:Lauj;

    invoke-interface {v0, p1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 66
    invoke-static {}, Lcre;->b()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 67
    invoke-virtual {p0}, Lqg;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lqg$1;

    invoke-direct {v1, p0}, Lqg$1;-><init>(Lqg;)V

    .line 68
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lqg;->a:Lcom/twitter/library/av/playback/AVDataSource;

    .line 74
    invoke-static {v1}, Lqm;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lqm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 75
    invoke-virtual {p0}, Lqg;->b()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 65
    return-object v0
.end method

.method protected a()Lrx/f;
    .locals 1

    .prologue
    .line 83
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 30
    check-cast p1, Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {p0, p1}, Lqg;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method protected b()Lrx/f;
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lqg;->c:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 97
    return-void
.end method
