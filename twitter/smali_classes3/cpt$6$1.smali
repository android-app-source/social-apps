.class Lcpt$6$1;
.super Lcpn;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcpt$6;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcpn",
        "<TOUT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcpt$6;

.field private final b:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TIN;>;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TOUT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcpt$6;)V
    .locals 1

    .prologue
    .line 119
    iput-object p1, p0, Lcpt$6$1;->a:Lcpt$6;

    invoke-direct {p0}, Lcpn;-><init>()V

    .line 120
    iget-object v0, p0, Lcpt$6$1;->a:Lcpt$6;

    iget-object v0, v0, Lcpt$6;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcpt$6$1;->b:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TOUT;"
        }
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lcpt$6$1;->c:Ljava/lang/Object;

    .line 135
    const/4 v1, 0x0

    iput-object v1, p0, Lcpt$6$1;->c:Ljava/lang/Object;

    .line 136
    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 125
    :goto_0
    iget-object v0, p0, Lcpt$6$1;->c:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcpt$6$1;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcpt$6$1;->a:Lcpt$6;

    iget-object v0, v0, Lcpt$6;->b:Lcpp;

    iget-object v1, p0, Lcpt$6$1;->b:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcpp;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcpt$6$1;->c:Ljava/lang/Object;

    goto :goto_0

    .line 128
    :cond_0
    iget-object v0, p0, Lcpt$6$1;->c:Ljava/lang/Object;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
