.class Ldbs$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ldbs;->a(Ljava/lang/String;)Lrx/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ltv/periscope/android/api/GetFollowingRequest;",
        "Lrx/c",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ldbs;


# direct methods
.method constructor <init>(Ldbs;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Ldbs$1;->a:Ldbs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 105
    check-cast p1, Ltv/periscope/android/api/GetFollowingRequest;

    invoke-virtual {p0, p1}, Ldbs$1;->a(Ltv/periscope/android/api/GetFollowingRequest;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Ltv/periscope/android/api/GetFollowingRequest;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/android/api/GetFollowingRequest;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 110
    :try_start_0
    iget-object v0, p0, Ldbs$1;->a:Ldbs;

    invoke-static {v0}, Ldbs;->a(Ldbs;)Ltv/periscope/android/api/ApiService;

    move-result-object v0

    invoke-interface {v0, p1}, Ltv/periscope/android/api/ApiService;->getFollowingIdsOnly(Ltv/periscope/android/api/GetFollowingRequest;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 112
    :goto_0
    return-object v0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Throwable;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method
