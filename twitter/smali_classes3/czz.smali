.class public Lczz;
.super Ldaa;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lczz$a;,
        Lczz$b;
    }
.end annotation


# instance fields
.field private final c:Lczz$a;

.field private volatile d:Z


# direct methods
.method public constructor <init>(Ldaa$a;)V
    .locals 2

    .prologue
    .line 37
    invoke-static {}, Lczz;->g()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lczz$1;

    invoke-direct {v1}, Lczz$1;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lczz;-><init>(Ldaa$a;Ljava/util/concurrent/ExecutorService;Lczz$a;)V

    .line 43
    return-void
.end method

.method constructor <init>(Ldaa$a;Ljava/util/concurrent/ExecutorService;Lczz$a;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ldaa;-><init>(Ldaa$a;Ljava/util/concurrent/ExecutorService;)V

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lczz;->d:Z

    .line 47
    iput-object p3, p0, Lczz;->c:Lczz$a;

    .line 48
    return-void
.end method

.method private static g()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Lczz$b;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lczz;->d:Z

    .line 91
    invoke-super {p0}, Ldaa;->a()V

    .line 92
    return-void
.end method

.method public bridge synthetic a(Ldab;)V
    .locals 0

    .prologue
    .line 14
    invoke-super {p0, p1}, Ldaa;->a(Ldab;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 57
    iget-boolean v0, p0, Lczz;->d:Z

    if-nez v0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    invoke-static {p1}, Ldcq;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v1, "%s: %s\n"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lczz;->c:Lczz$a;

    invoke-interface {v4}, Lczz$a;->a()Ljava/util/Date;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 65
    invoke-virtual {p0, v0}, Lczz;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 70
    iget-boolean v0, p0, Lczz;->d:Z

    if-nez v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 73
    :cond_0
    if-nez p2, :cond_1

    .line 74
    invoke-virtual {p0, p1}, Lczz;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 78
    :cond_1
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 79
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 80
    invoke-virtual {v1, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 81
    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p2, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 83
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lczz;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lczz;->d:Z

    .line 53
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lczz;->d:Z

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lczz;->c()Ljava/lang/String;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    .line 100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    invoke-super {p0}, Ldaa;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
