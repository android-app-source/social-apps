.class public Lri;
.super Lcom/twitter/android/card/f;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lri$a;
    }
.end annotation


# instance fields
.field private a:Lri$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method private b(Lcom/twitter/analytics/feature/model/ClientEventLog;)V
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e()Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 41
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ar:Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    if-nez v1, :cond_0

    iget-object v1, p0, Lri;->a:Lri$a;

    if-nez v1, :cond_1

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    iget-object v1, p0, Lri;->a:Lri$a;

    invoke-interface {v1}, Lri$a;->j()Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ar:Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lri;->b(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 34
    invoke-super {p0, p1}, Lcom/twitter/android/card/f;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 35
    return-void
.end method

.method public a(Lri$a;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lri;->a:Lri$a;

    .line 29
    return-void
.end method
