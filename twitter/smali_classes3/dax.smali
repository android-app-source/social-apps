.class public Ldax;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/a;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ltv/periscope/android/ui/broadcast/ao;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/ao;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Ldax;->a:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Ldax;->b:Ltv/periscope/android/ui/broadcast/ao;

    .line 19
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 23
    sget v0, Ltv/periscope/android/library/f$f;->ps__ic_share:I

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget v0, Ltv/periscope/android/library/f$l;->ps__share_broadcast_other_app:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 28
    sget v0, Ltv/periscope/android/library/f$d;->ps__light_grey:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 38
    sget v0, Ltv/periscope/android/library/f$d;->ps__primary_text:I

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Ldax;->b:Ltv/periscope/android/ui/broadcast/ao;

    iget-object v1, p0, Ldax;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/ao;->l(Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public f()Ltv/periscope/android/view/c;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Ltv/periscope/android/view/c;->c:Ltv/periscope/android/view/c;

    return-object v0
.end method
