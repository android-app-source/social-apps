.class public Lty;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltz;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/livevideo/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Landroid/support/v4/util/ArrayMap;

    invoke-direct {v0}, Landroid/support/v4/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lty;->a:Ljava/util/Map;

    .line 23
    return-void
.end method


# virtual methods
.method public a(J)Lcom/twitter/model/livevideo/d;
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lty;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/d;

    return-object v0
.end method

.method public a(Lcom/twitter/model/livevideo/b;Lcom/twitter/model/livevideo/d;)Lcom/twitter/model/livevideo/d;
    .locals 4

    .prologue
    .line 28
    iget-object v0, p0, Lty;->a:Ljava/util/Map;

    iget-wide v2, p1, Lcom/twitter/model/livevideo/b;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/d;

    return-object v0
.end method
