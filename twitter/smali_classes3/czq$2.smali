.class Lczq$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lczq$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lczq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lczq;


# direct methods
.method constructor <init>(Lczq;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lczq$2;->a:Lczq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/google/android/gms/common/api/i;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/api/i",
            "<",
            "Lcom/google/android/gms/location/places/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 333
    sget-object v0, Lcom/google/android/gms/location/places/k;->e:Lcom/google/android/gms/location/places/c;

    iget-object v1, p0, Lczq$2;->a:Lczq;

    invoke-static {v1}, Lczq;->b(Lczq;)Lcom/google/android/gms/common/api/c;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/location/places/c;->a(Lcom/google/android/gms/common/api/c;[Ljava/lang/String;)Lcom/google/android/gms/common/api/e;

    move-result-object v0

    .line 334
    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/api/e;->a(Lcom/google/android/gms/common/api/i;)V

    .line 335
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/api/i",
            "<",
            "Lcom/google/android/gms/location/places/b;",
            ">;",
            "Lcom/google/android/gms/maps/model/LatLngBounds;",
            "Lcom/google/android/gms/location/places/AutocompleteFilter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 321
    sget-object v0, Lcom/google/android/gms/location/places/k;->e:Lcom/google/android/gms/location/places/c;

    iget-object v1, p0, Lczq$2;->a:Lczq;

    invoke-static {v1}, Lczq;->b(Lczq;)Lcom/google/android/gms/common/api/c;

    move-result-object v1

    iget-object v2, p0, Lczq$2;->a:Lczq;

    .line 322
    invoke-virtual {v2}, Lczq;->h()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v2

    iget-object v3, p0, Lczq$2;->a:Lczq;

    invoke-virtual {v3}, Lczq;->i()Lcom/google/android/gms/location/places/AutocompleteFilter;

    move-result-object v3

    .line 321
    invoke-interface {v0, v1, p1, v2, v3}, Lcom/google/android/gms/location/places/c;->a(Lcom/google/android/gms/common/api/c;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)Lcom/google/android/gms/common/api/e;

    move-result-object v0

    .line 323
    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/api/e;->a(Lcom/google/android/gms/common/api/i;)V

    .line 324
    return-void
.end method
