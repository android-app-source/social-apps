.class abstract Ldaa;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldad;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ldaa$a;
    }
.end annotation


# static fields
.field private static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:Ldaa$a;

.field final b:Ljava/lang/String;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private e:Ljava/io/Writer;

.field private f:Z

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Ldaa$1;

    invoke-direct {v0}, Ldaa$1;-><init>()V

    sput-object v0, Ldaa;->c:Ljava/util/Comparator;

    return-void
.end method

.method protected constructor <init>(Ldaa$a;Ljava/util/concurrent/ExecutorService;)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Ldaa;->a:Ldaa$a;

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Ldaa$a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Ldaa$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_working."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Ldaa$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldaa;->b:Ljava/lang/String;

    .line 63
    iput-object p2, p0, Ldaa;->d:Ljava/util/concurrent/ExecutorService;

    .line 64
    return-void
.end method

.method static synthetic a(Ldaa;Ljava/io/Writer;)Ljava/io/Writer;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Ldaa;->e:Ljava/io/Writer;

    return-object p1
.end method

.method static synthetic a(Ldaa;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Ldaa;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Ldaa;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ldaa;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 264
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Failed to delete log "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ldaa;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 269
    :cond_0
    return-void
.end method

.method static synthetic a(Ldaa;)Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Ldaa;->g:Z

    return v0
.end method

.method static synthetic a(Ldaa;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Ldaa;->g:Z

    return p1
.end method

.method static synthetic b(Ldaa;)Ljava/io/Writer;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Ldaa;->e:Ljava/io/Writer;

    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 94
    const-string/jumbo v0, "FileBasedLogger"

    invoke-static {v0, p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 95
    return-void
.end method

.method private b()[Ljava/io/File;
    .locals 2

    .prologue
    .line 157
    invoke-direct {p0}, Ldaa;->g()V

    .line 159
    invoke-virtual {p0}, Ldaa;->f()Ljava/io/File;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 162
    sget-object v1, Ldaa;->c:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 166
    :goto_0
    return-object v0

    .line 164
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 260
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Ldaa;->a(Ljava/io/File;)V

    .line 261
    return-void
.end method

.method static synthetic c(Ldaa;)[Ljava/io/File;
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ldaa;->b()[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 174
    iget-object v0, p0, Ldaa;->e:Ljava/io/Writer;

    if-nez v0, :cond_0

    .line 199
    :goto_0
    return-void

    .line 179
    :cond_0
    :try_start_0
    invoke-direct {p0}, Ldaa;->h()Z

    .line 181
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Ldaa;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0}, Ldaa;->e()Ljava/io/File;

    move-result-object v1

    .line 183
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldaa;->f:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 193
    :catch_0
    move-exception v0

    .line 194
    const-string/jumbo v1, "Failed to prepare log"

    invoke-direct {p0, v1, v0}, Ldaa;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 197
    iput-boolean v2, p0, Ldaa;->f:Z

    goto :goto_0

    .line 187
    :cond_1
    :try_start_1
    const-string/jumbo v0, "Failed to rename file"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ldaa;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldaa;->f:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private h()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 206
    .line 208
    :try_start_0
    iget-object v0, p0, Ldaa;->e:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    const/4 v0, 0x1

    .line 211
    iget-object v1, p0, Ldaa;->e:Ljava/io/Writer;

    invoke-static {v1}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 212
    iput-object v2, p0, Ldaa;->e:Ljava/io/Writer;

    .line 214
    return v0

    .line 211
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ldaa;->e:Ljava/io/Writer;

    invoke-static {v1}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 212
    iput-object v2, p0, Ldaa;->e:Ljava/io/Writer;

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Ldaa;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Ldaa$4;

    invoke-direct {v1, p0}, Ldaa$4;-><init>(Ldaa;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 257
    return-void
.end method

.method public a(Ldab;)V
    .locals 2

    .prologue
    .line 114
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Ldaa$3;

    invoke-direct {v1, p0, p1}, Ldaa$3;-><init>(Ldaa;Ldab;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 138
    iget-object v1, p0, Ldaa;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 140
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 146
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v0

    .line 142
    const-string/jumbo v1, "Failed to access logs"

    invoke-direct {p0, v1, v0}, Ldaa;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 143
    :catch_1
    move-exception v0

    .line 144
    const-string/jumbo v1, "Failed to access logs"

    invoke-direct {p0, v1, v0}, Ldaa;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method a([Ljava/io/File;)V
    .locals 5

    .prologue
    .line 223
    if-nez p1, :cond_1

    .line 231
    :cond_0
    return-void

    .line 226
    :cond_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 227
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Ldaa;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 228
    invoke-direct {p0, v2}, Ldaa;->a(Ljava/io/File;)V

    .line 226
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Ldaa;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Ldaa$2;

    invoke-direct {v1, p0, p1}, Ldaa$2;-><init>(Ldaa;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 89
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Ldaa;->a:Ldaa$a;

    iget-object v0, v0, Ldaa$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected d()Ljava/io/Writer;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Ldaa;->a:Ldaa$a;

    iget-object v1, v1, Ldaa$a;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 106
    :cond_0
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/FileWriter;

    iget-object v2, p0, Ldaa;->b:Ljava/lang/String;

    iget-boolean v3, p0, Ldaa;->f:Z

    invoke-direct {v1, v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    return-object v0
.end method

.method protected e()Ljava/io/File;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 272
    invoke-virtual {p0}, Ldaa;->f()Ljava/io/File;

    move-result-object v0

    .line 273
    iget-object v1, p0, Ldaa;->a:Ldaa$a;

    iget-object v1, v1, Ldaa$a;->b:Ljava/lang/String;

    iget-object v2, p0, Ldaa;->a:Ldaa$a;

    iget-object v2, v2, Ldaa$a;->c:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected f()Ljava/io/File;
    .locals 3

    .prologue
    .line 277
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Ldaa;->a:Ldaa$a;

    iget-object v1, v1, Ldaa$a;->d:Ljava/lang/String;

    const-string/jumbo v2, "access"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 279
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 281
    :cond_0
    return-object v0
.end method
