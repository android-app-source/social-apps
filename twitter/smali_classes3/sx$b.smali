.class public abstract Lsx$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lsx;",
        "B:",
        "Lsx$a",
        "<TT;TB;>;>",
        "Lcom/twitter/util/serialization/b",
        "<TT;TB;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>(I)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/twitter/util/serialization/b;-><init>(I)V

    .line 110
    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 105
    check-cast p2, Lsx$a;

    invoke-virtual {p0, p1, p2, p3}, Lsx$b;->a(Lcom/twitter/util/serialization/n;Lsx$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lsx$a;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TB;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 127
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    .line 128
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v4

    .line 130
    invoke-static {}, Lsx;->a()Lcom/twitter/util/serialization/l;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v1

    .line 131
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v6

    .line 133
    invoke-virtual {p2, v0}, Lsx$a;->a(Ljava/lang/String;)Lsx$a;

    move-result-object v0

    .line 134
    invoke-virtual {v0, v2, v3}, Lsx$a;->a(J)Lsx$a;

    move-result-object v0

    .line 135
    invoke-virtual {v0, v4, v5}, Lsx$a;->b(J)Lsx$a;

    move-result-object v0

    .line 136
    invoke-virtual {v0, v1}, Lsx$a;->a(Ljava/util/List;)Lsx$a;

    move-result-object v0

    .line 137
    invoke-virtual {v0, v6}, Lsx$a;->b(Ljava/lang/String;)Lsx$a;

    .line 138
    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lsx;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p2, Lsx;->a:Ljava/lang/String;

    .line 115
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lsx;->b:J

    .line 116
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lsx;->c:J

    .line 117
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 119
    iget-object v0, p2, Lsx;->d:Ljava/util/List;

    invoke-static {}, Lsx;->a()Lcom/twitter/util/serialization/l;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 120
    iget-object v0, p2, Lsx;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 121
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    check-cast p2, Lsx;

    invoke-virtual {p0, p1, p2}, Lsx$b;->a(Lcom/twitter/util/serialization/o;Lsx;)V

    return-void
.end method
