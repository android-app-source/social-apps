.class Lcyb$c;
.super Landroid/view/OrientationEventListener;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcyb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcyb;


# direct methods
.method constructor <init>(Lcyb;Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 572
    iput-object p1, p0, Lcyb$c;->a:Lcyb;

    .line 573
    invoke-direct {p0, p2, p3}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;I)V

    .line 574
    return-void
.end method

.method private a(D)I
    .locals 3

    .prologue
    .line 577
    const-wide v0, 0x4046800000000000L    # 45.0

    add-double/2addr v0, p1

    double-to-int v0, v0

    div-int/lit8 v0, v0, 0x5a

    rem-int/lit8 v0, v0, 0x4

    return v0
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 582
    const/4 v0, -0x1

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcyb$c;->a:Lcyb;

    invoke-static {v0}, Lcyb;->b(Lcyb;)Lcyf;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 583
    iget-object v0, p0, Lcyb$c;->a:Lcyb;

    .line 584
    invoke-static {v0}, Lcyb;->b(Lcyb;)Lcyf;

    move-result-object v0

    invoke-interface {v0}, Lcyf;->f()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    .line 586
    :goto_0
    int-to-double v4, p1

    invoke-direct {p0, v4, v5}, Lcyb$c;->a(D)I

    move-result v3

    .line 588
    if-eqz v0, :cond_5

    .line 589
    iget-object v4, p0, Lcyb$c;->a:Lcyb;

    rsub-int v5, p1, 0x168

    int-to-double v6, v5

    invoke-static {v4, v6, v7}, Lcyb;->a(Lcyb;D)D

    .line 596
    :goto_1
    iget-object v4, p0, Lcyb$c;->a:Lcyb;

    invoke-static {v4}, Lcyb;->c(Lcyb;)Ltv/periscope/android/video/RTMPPublisher;

    move-result-object v4

    if-nez v4, :cond_6

    const-wide/16 v4, 0x0

    .line 597
    :goto_2
    iget-object v6, p0, Lcyb$c;->a:Lcyb;

    invoke-static {v6}, Lcyb;->j(Lcyb;)D

    move-result-wide v6

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    .line 598
    const-wide v6, 0x4066800000000000L    # 180.0

    cmpl-double v6, v4, v6

    if-lez v6, :cond_0

    .line 600
    const-wide v6, 0x4076800000000000L    # 360.0

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    .line 602
    :cond_0
    const-wide/high16 v6, 0x402e000000000000L    # 15.0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_1

    move v2, v1

    .line 604
    :cond_1
    if-eqz v2, :cond_2

    .line 605
    const-string/jumbo v4, "BroadcasterVideoController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Rotation: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcyb$c;->a:Lcyb;

    invoke-static {v6}, Lcyb;->j(Lcyb;)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " front facing "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    iget-object v0, p0, Lcyb$c;->a:Lcyb;

    invoke-static {v0}, Lcyb;->k(Lcyb;)I

    move-result v0

    .line 611
    sget-object v4, Lcxz;->a:[I

    aget v3, v4, v3

    .line 615
    if-ne v0, v1, :cond_7

    if-ne v3, v8, :cond_7

    .line 616
    iget-object v0, p0, Lcyb$c;->a:Lcyb;

    invoke-virtual {v0, v3}, Lcyb;->a(I)V

    .line 627
    :cond_2
    :goto_3
    iget-object v0, p0, Lcyb$c;->a:Lcyb;

    invoke-static {v0, v2}, Lcyb;->a(Lcyb;Z)V

    .line 629
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 584
    goto/16 :goto_0

    .line 591
    :cond_5
    iget-object v4, p0, Lcyb$c;->a:Lcyb;

    int-to-double v6, p1

    invoke-static {v4, v6, v7}, Lcyb;->a(Lcyb;D)D

    goto :goto_1

    .line 596
    :cond_6
    iget-object v4, p0, Lcyb$c;->a:Lcyb;

    invoke-static {v4}, Lcyb;->c(Lcyb;)Ltv/periscope/android/video/RTMPPublisher;

    move-result-object v4

    invoke-virtual {v4}, Ltv/periscope/android/video/RTMPPublisher;->a()D

    move-result-wide v4

    goto :goto_2

    .line 617
    :cond_7
    if-ne v0, v8, :cond_2

    if-ne v3, v1, :cond_2

    .line 618
    iget-object v0, p0, Lcyb$c;->a:Lcyb;

    invoke-virtual {v0, v3}, Lcyb;->a(I)V

    goto :goto_3
.end method
