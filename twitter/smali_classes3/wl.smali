.class public Lwl;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/support/v4/app/FragmentManager;

.field private final c:Lcom/twitter/android/util/h;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/twitter/android/util/h;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lwl;->a:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lwl;->b:Landroid/support/v4/app/FragmentManager;

    .line 35
    iput-object p3, p0, Lwl;->c:Lcom/twitter/android/util/h;

    .line 36
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;J)Lwl;
    .locals 8

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/android/util/h;

    const-string/jumbo v2, "guide_fatigue"

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    move-object v1, p0

    move-wide v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/util/h;-><init>(Landroid/content/Context;Ljava/lang/String;IJJ)V

    .line 28
    new-instance v1, Lwl;

    invoke-direct {v1, p0, p1, v0}, Lwl;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/twitter/android/util/h;)V

    return-object v1
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Lwk;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lwl;->b:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lwl;->c:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lwl;->c:Lcom/twitter/android/util/h;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 44
    iget-object v0, p0, Lwl;->a:Landroid/content/Context;

    const v1, 0x7f1307a8

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0d023a

    .line 45
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->b(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0a03dc

    .line 46
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 47
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->c(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    iget-object v1, p0, Lwl;->b:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "guide_tooltip"

    .line 48
    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    .line 49
    return-void
.end method
