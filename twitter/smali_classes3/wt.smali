.class public Lwt;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lauj",
        "<TA;",
        "Lcom/twitter/util/collection/k",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Iterable",
            "<TA;>;",
            "Ljava/util/Map",
            "<TA;TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lauj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lauj",
            "<",
            "Ljava/lang/Iterable",
            "<TA;>;",
            "Ljava/util/Map",
            "<TA;TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lwt;->a:Lauj;

    .line 24
    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/Object;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/k",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lwt;->a:Lauj;

    invoke-static {p1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lwt$1;

    invoke-direct {v1, p0, p1}, Lwt$1;-><init>(Lwt;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lwt;->a:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 41
    return-void
.end method
