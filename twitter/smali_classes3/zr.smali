.class public Lzr;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lzu;

.field private final b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;


# direct methods
.method private constructor <init>(Lzu;Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lzr;->a:Lzu;

    .line 44
    iput-object p2, p0, Lzr;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 45
    return-void
.end method

.method public static a(J)Lzr;
    .locals 4

    .prologue
    .line 37
    new-instance v0, Lzr;

    invoke-static {}, Lzu;->a()Lzu;

    move-result-object v1

    new-instance v2, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 38
    invoke-virtual {v2, p0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lzr;-><init>(Lzu;Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;)V

    .line 37
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 64
    iget-object v1, p0, Lzr;->a:Lzu;

    iget-object v0, p0, Lzr;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:capsule:%s::%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "cover"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "like"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public a(I)V
    .locals 6

    .prologue
    .line 49
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;-><init>()V

    .line 50
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->b(I)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    .line 51
    iget-object v1, p0, Lzr;->a:Lzu;

    iget-object v2, p0, Lzr;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:capsule:%s::%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "capsule_page"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "like"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    .line 68
    iget-object v1, p0, Lzr;->a:Lzu;

    iget-object v0, p0, Lzr;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:capsule:%s::%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "cover"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "unlike"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method public b(I)V
    .locals 6

    .prologue
    .line 57
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;-><init>()V

    .line 58
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->b(I)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    .line 59
    iget-object v1, p0, Lzr;->a:Lzu;

    iget-object v2, p0, Lzr;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:capsule:%s::%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "capsule_page"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "unlike"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public c()V
    .locals 6

    .prologue
    .line 72
    iget-object v1, p0, Lzr;->a:Lzu;

    iget-object v0, p0, Lzr;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:capsule:%s::%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "end"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "like"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public d()V
    .locals 6

    .prologue
    .line 76
    iget-object v1, p0, Lzr;->a:Lzu;

    iget-object v0, p0, Lzr;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:capsule:%s::%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "end"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "unlike"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public e()V
    .locals 6

    .prologue
    .line 80
    iget-object v1, p0, Lzr;->a:Lzu;

    iget-object v0, p0, Lzr;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:capsule:%s:like_count:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "cover"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "click"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public f()V
    .locals 6

    .prologue
    .line 84
    iget-object v1, p0, Lzr;->a:Lzu;

    iget-object v0, p0, Lzr;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:capsule:%s:like_count:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "end"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "click"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 85
    return-void
.end method
