.class public abstract Lyk;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Lcom/twitter/database/lru/LruPolicy;

.field private static final b:Lcom/twitter/database/lru/k;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 56
    new-instance v0, Lcom/twitter/database/lru/LruPolicy;

    sget-object v1, Lcom/twitter/database/lru/LruPolicy$Type;->a:Lcom/twitter/database/lru/LruPolicy$Type;

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/twitter/database/lru/LruPolicy;-><init>(Lcom/twitter/database/lru/LruPolicy$Type;I)V

    sput-object v0, Lyk;->a:Lcom/twitter/database/lru/LruPolicy;

    .line 57
    new-instance v0, Lcom/twitter/database/lru/k;

    sget-object v1, Lyk;->a:Lcom/twitter/database/lru/LruPolicy;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    .line 58
    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/database/lru/k;-><init>(Lcom/twitter/database/lru/LruPolicy;J)V

    sput-object v0, Lyk;->b:Lcom/twitter/database/lru/k;

    .line 57
    return-void
.end method

.method static a(Landroid/content/Context;)Lcom/twitter/android/moments/data/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/twitter/android/moments/data/c",
            "<",
            "Lapb;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Lyk$1;

    invoke-direct {v0, p0}, Lyk$1;-><init>(Landroid/content/Context;)V

    .line 95
    new-instance v1, Lcom/twitter/android/moments/data/c;

    invoke-direct {v1, v0}, Lcom/twitter/android/moments/data/c;-><init>(Lcom/twitter/util/object/j;)V

    return-object v1
.end method

.method static a(Lawb;)Lcom/twitter/database/lru/l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lawb;",
            ")",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    invoke-static {}, Lcom/twitter/database/lru/e$a;->b()Lcom/twitter/database/lru/e$a;

    move-result-object v0

    const-string/jumbo v1, "moment_maker_unassociated_page_data_groups"

    .line 66
    invoke-virtual {v0, v1}, Lcom/twitter/database/lru/e$a;->a(Ljava/lang/String;)Lcom/twitter/database/lru/e$a;

    move-result-object v0

    sget-object v1, Lcep;->a:Lcom/twitter/util/serialization/l;

    .line 67
    invoke-virtual {v0, v1}, Lcom/twitter/database/lru/e$a;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/database/lru/e$a;

    move-result-object v0

    sget-object v1, Lyk;->b:Lcom/twitter/database/lru/k;

    .line 68
    invoke-virtual {v0, v1}, Lcom/twitter/database/lru/e$a;->a(Lcom/twitter/database/lru/k;)Lcom/twitter/database/lru/e$a;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/twitter/database/lru/e$a;->c()Lcom/twitter/database/lru/e;

    move-result-object v0

    .line 65
    invoke-virtual {p0, v0}, Lawb;->a(Lcom/twitter/database/lru/e;)Lcom/twitter/database/lru/l;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lbsb;)Lcom/twitter/util/object/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "Lbsb;",
            ")",
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/Long;",
            "Lbdk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    new-instance v0, Lyk$2;

    invoke-direct {v0, p0, p1, p2}, Lyk$2;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lbsb;)V

    return-object v0
.end method

.method static a(Lcom/twitter/database/schema/TwitterSchema;)Lwo;
    .locals 1

    .prologue
    .line 116
    invoke-static {p0}, Lwo;->a(Lcom/twitter/database/model/i;)Lwo;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lyf;
    .locals 1

    .prologue
    .line 76
    invoke-static {p0, p1}, Lyf;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lyf;

    move-result-object v0

    return-object v0
.end method
