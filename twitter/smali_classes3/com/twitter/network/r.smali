.class public Lcom/twitter/network/r;
.super Lcom/twitter/network/m;
.source "Twttr"


# instance fields
.field protected final b:Lcom/squareup/okhttp/v_1_5_1/h;

.field private final c:Lcom/squareup/okhttp/v_1_5_1/f;


# direct methods
.method public constructor <init>(Lcom/twitter/network/f;)V
    .locals 4

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/twitter/network/m;-><init>(Lcom/twitter/network/f;)V

    .line 28
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-direct {v0}, Lcom/squareup/okhttp/v_1_5_1/h;-><init>()V

    iput-object v0, p0, Lcom/twitter/network/r;->b:Lcom/squareup/okhttp/v_1_5_1/h;

    .line 29
    iget-object v0, p0, Lcom/twitter/network/r;->b:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {p0}, Lcom/twitter/network/r;->c()Ljava/net/Proxy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Ljava/net/Proxy;)Lcom/squareup/okhttp/v_1_5_1/h;

    .line 32
    :try_start_0
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/f;

    invoke-virtual {p1}, Lcom/twitter/network/f;->e()Ljava/io/File;

    move-result-object v1

    .line 33
    invoke-virtual {p1}, Lcom/twitter/network/f;->d()I

    move-result v2

    int-to-long v2, v2

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/okhttp/v_1_5_1/f;-><init>(Ljava/io/File;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :goto_0
    iget-object v1, p0, Lcom/twitter/network/r;->b:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Lcom/squareup/okhttp/v_1_5_1/i;)Lcom/squareup/okhttp/v_1_5_1/h;

    .line 39
    iput-object v0, p0, Lcom/twitter/network/r;->c:Lcom/squareup/okhttp/v_1_5_1/f;

    .line 40
    return-void

    .line 34
    :catch_0
    move-exception v0

    .line 35
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 36
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)Lcom/twitter/network/HttpOperation;
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/twitter/network/q;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/twitter/network/q;-><init>(Lcom/twitter/network/m;Ljava/net/URI;Lcom/twitter/network/HttpOperation$RequestMethod;Lcom/twitter/network/j;)V

    return-object v0
.end method

.method public a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/network/r;->b:Lcom/squareup/okhttp/v_1_5_1/h;

    iget-object v1, p0, Lcom/twitter/network/r;->a:Lcom/twitter/network/f;

    invoke-virtual {v1}, Lcom/twitter/network/f;->c()I

    move-result v1

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcom/squareup/okhttp/v_1_5_1/h;->a(JLjava/util/concurrent/TimeUnit;)V

    .line 46
    iget-object v0, p0, Lcom/twitter/network/r;->b:Lcom/squareup/okhttp/v_1_5_1/h;

    iget-object v1, p0, Lcom/twitter/network/r;->a:Lcom/twitter/network/f;

    invoke-virtual {v1}, Lcom/twitter/network/f;->b()I

    move-result v1

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcom/squareup/okhttp/v_1_5_1/h;->b(JLjava/util/concurrent/TimeUnit;)V

    .line 47
    iget-object v0, p0, Lcom/twitter/network/r;->b:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Lcom/twitter/network/m;->a()V

    .line 60
    iget-object v0, p0, Lcom/twitter/network/r;->c:Lcom/squareup/okhttp/v_1_5_1/f;

    if-eqz v0, :cond_0

    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/twitter/network/r;->b:Lcom/squareup/okhttp/v_1_5_1/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Ljava/net/ResponseCache;)Lcom/squareup/okhttp/v_1_5_1/h;

    .line 63
    iget-object v0, p0, Lcom/twitter/network/r;->c:Lcom/squareup/okhttp/v_1_5_1/f;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/f;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 65
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
