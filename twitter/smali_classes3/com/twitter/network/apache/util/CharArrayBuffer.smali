.class public final Lcom/twitter/network/apache/util/CharArrayBuffer;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/CharSequence;


# static fields
.field private static final serialVersionUID:J = -0x562aa19b667920bfL


# instance fields
.field private buffer:[C

.field private len:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-string/jumbo v0, "Buffer capacity"

    invoke-static {p1, v0}, Lcom/twitter/network/apache/util/a;->a(ILjava/lang/String;)I

    .line 56
    new-array v0, p1, [C

    iput-object v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    .line 57
    return-void
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 60
    iget-object v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v0, v0, [C

    .line 61
    iget-object v1, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    iget v2, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 62
    iput-object v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    .line 63
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    .line 235
    return-void
.end method

.method public a(C)V
    .locals 3

    .prologue
    .line 154
    iget v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    add-int/lit8 v0, v0, 0x1

    .line 155
    iget-object v1, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 156
    invoke-direct {p0, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->b(I)V

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    iget v2, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    aput-char p1, v1, v2

    .line 159
    iput v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    .line 160
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 302
    if-gtz p1, :cond_1

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    array-length v0, v0

    iget v1, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    sub-int/2addr v0, v1

    .line 306
    if-le p1, v0, :cond_0

    .line 307
    iget v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    add-int/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->b(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 103
    if-eqz p1, :cond_1

    .line 104
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 105
    iget v1, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    add-int/2addr v1, v0

    .line 106
    iget-object v2, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    array-length v2, v2

    if-le v1, v2, :cond_0

    .line 107
    invoke-direct {p0, v1}, Lcom/twitter/network/apache/util/CharArrayBuffer;->b(I)V

    .line 109
    :cond_0
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    iget v4, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    invoke-virtual {p1, v2, v0, v3, v4}, Ljava/lang/String;->getChars(II[CI)V

    .line 110
    iput v1, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    .line 111
    return-void

    .line 103
    :cond_1
    const-string/jumbo p1, "null"

    goto :goto_0
.end method

.method public a([CII)V
    .locals 3

    .prologue
    .line 78
    if-nez p1, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    if-ltz p2, :cond_2

    array-length v0, p1

    if-gt p2, v0, :cond_2

    if-ltz p3, :cond_2

    add-int v0, p2, p3

    if-ltz v0, :cond_2

    add-int v0, p2, p3

    array-length v1, p1

    if-le v0, v1, :cond_3

    .line 83
    :cond_2
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "off: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " len: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " b.length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_3
    if-eqz p3, :cond_0

    .line 88
    iget v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    add-int/2addr v0, p3

    .line 89
    iget-object v1, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    array-length v1, v1

    if-le v0, v1, :cond_4

    .line 90
    invoke-direct {p0, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->b(I)V

    .line 92
    :cond_4
    iget-object v1, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    iget v2, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    iput v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    goto :goto_0
.end method

.method public charAt(I)C
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    aget-char v0, v0, p1

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 290
    iget v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 473
    if-gez p1, :cond_0

    .line 474
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Negative beginIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 476
    :cond_0
    iget v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    if-le p2, v0, :cond_1

    .line 477
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "endIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " > length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 479
    :cond_1
    if-le p1, p2, :cond_2

    .line 480
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "beginIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " > endIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 482
    :cond_2
    iget-object v0, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    invoke-static {v0, p1, p2}, Ljava/nio/CharBuffer;->wrap([CII)Ljava/nio/CharBuffer;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 487
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->buffer:[C

    const/4 v2, 0x0

    iget v3, p0, Lcom/twitter/network/apache/util/CharArrayBuffer;->len:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
