.class public final Lcom/twitter/network/apache/util/b;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/network/apache/e;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/twitter/network/apache/ParseException;
        }
    .end annotation

    .prologue
    .line 291
    const/4 v0, 0x0

    check-cast v0, Ljava/nio/charset/Charset;

    invoke-static {p0, v0}, Lcom/twitter/network/apache/util/b;->a(Lcom/twitter/network/apache/e;Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/twitter/network/apache/e;Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/twitter/network/apache/ParseException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 214
    const-string/jumbo v2, "Entity"

    invoke-static {p0, v2}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 215
    invoke-interface {p0}, Lcom/twitter/network/apache/e;->b()Ljava/io/InputStream;

    move-result-object v3

    .line 216
    if-nez v3, :cond_0

    .line 250
    :goto_0
    return-object v1

    .line 220
    :cond_0
    :try_start_0
    invoke-interface {p0}, Lcom/twitter/network/apache/e;->a()J

    move-result-wide v4

    const-wide/32 v6, 0x7fffffff

    cmp-long v2, v4, v6

    if-gtz v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    const-string/jumbo v2, "HTTP entity too large to be buffered in memory"

    invoke-static {v0, v2}, Lcom/twitter/network/apache/util/a;->a(ZLjava/lang/String;)V

    .line 222
    invoke-interface {p0}, Lcom/twitter/network/apache/e;->a()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    long-to-int v0, v4

    .line 223
    if-gez v0, :cond_2

    .line 224
    const/16 v0, 0x1000

    .line 228
    :cond_2
    :try_start_1
    invoke-static {p0}, Lcom/twitter/network/apache/entity/ContentType;->a(Lcom/twitter/network/apache/e;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v2

    .line 229
    if-eqz v2, :cond_3

    .line 230
    invoke-virtual {v2}, Lcom/twitter/network/apache/entity/ContentType;->b()Ljava/nio/charset/Charset;
    :try_end_1
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 237
    :cond_3
    if-nez v1, :cond_4

    move-object v1, p1

    .line 240
    :cond_4
    if-nez v1, :cond_5

    .line 241
    :try_start_2
    sget-object v1, Lciz;->a:Ljava/nio/charset/Charset;

    .line 243
    :cond_5
    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    .line 244
    new-instance v1, Lcom/twitter/network/apache/util/CharArrayBuffer;

    invoke-direct {v1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;-><init>(I)V

    .line 245
    const/16 v0, 0x400

    new-array v0, v0, [C

    .line 247
    :goto_1
    invoke-virtual {v2, v0}, Ljava/io/Reader;->read([C)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_6

    .line 248
    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5, v4}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a([CII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 252
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v0

    .line 232
    :catch_0
    move-exception v2

    .line 233
    if-nez p1, :cond_3

    .line 234
    :try_start_3
    new-instance v0, Ljava/io/UnsupportedEncodingException;

    invoke-virtual {v2}, Ljava/nio/charset/UnsupportedCharsetException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :cond_6
    invoke-virtual {v1}, Lcom/twitter/network/apache/util/CharArrayBuffer;->toString()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 252
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_0
.end method
