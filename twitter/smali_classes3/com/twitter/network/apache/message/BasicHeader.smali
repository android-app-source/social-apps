.class public Lcom/twitter/network/apache/message/BasicHeader;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/network/apache/c;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final serialVersionUID:J = -0x4b516aaf286317beL


# instance fields
.field private final name:Ljava/lang/String;

.field private final value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const-string/jumbo v0, "Name"

    invoke-static {p1, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/network/apache/message/BasicHeader;->name:Ljava/lang/String;

    .line 58
    iput-object p2, p0, Lcom/twitter/network/apache/message/BasicHeader;->value:Ljava/lang/String;

    .line 59
    return-void
.end method


# virtual methods
.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/network/apache/message/BasicHeader;->name:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/network/apache/message/BasicHeader;->value:Ljava/lang/String;

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 89
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d()[Lcom/twitter/network/apache/d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/twitter/network/apache/ParseException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/network/apache/message/BasicHeader;->value:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/twitter/network/apache/message/BasicHeader;->value:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/network/apache/message/c;->a(Ljava/lang/String;Lcom/twitter/network/apache/message/e;)[Lcom/twitter/network/apache/d;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/twitter/network/apache/d;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    sget-object v0, Lcom/twitter/network/apache/message/d;->b:Lcom/twitter/network/apache/message/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Lcom/twitter/network/apache/message/d;->a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/c;)Lcom/twitter/network/apache/util/CharArrayBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
