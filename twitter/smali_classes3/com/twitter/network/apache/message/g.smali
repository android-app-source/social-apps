.class public Lcom/twitter/network/apache/message/g;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/twitter/network/apache/message/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/twitter/network/apache/message/g;

    invoke-direct {v0}, Lcom/twitter/network/apache/message/g;-><init>()V

    sput-object v0, Lcom/twitter/network/apache/message/g;->a:Lcom/twitter/network/apache/message/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs a([I)Ljava/util/BitSet;
    .locals 4

    .prologue
    .line 45
    new-instance v1, Ljava/util/BitSet;

    invoke-direct {v1}, Ljava/util/BitSet;-><init>()V

    .line 46
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p0, v0

    .line 47
    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    return-object v1
.end method

.method public static a(C)Z
    .locals 1

    .prologue
    .line 71
    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;Ljava/util/BitSet;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 86
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 88
    :goto_0
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 89
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/twitter/network/apache/util/CharArrayBuffer;->charAt(I)C

    move-result v3

    .line 90
    if-eqz p3, :cond_1

    invoke-virtual {p3, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 103
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 92
    :cond_1
    invoke-static {v3}, Lcom/twitter/network/apache/message/g;->a(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 93
    invoke-virtual {p0, p1, p2}, Lcom/twitter/network/apache/message/g;->a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)V

    .line 94
    const/4 v0, 0x1

    goto :goto_0

    .line 96
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 97
    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 99
    :cond_3
    invoke-virtual {p0, p1, p2, p3, v2}, Lcom/twitter/network/apache/message/g;->a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;Ljava/util/BitSet;Ljava/lang/StringBuilder;)V

    move v0, v1

    .line 100
    goto :goto_0
.end method

.method public a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)V
    .locals 4

    .prologue
    .line 151
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v1

    .line 152
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v0

    .line 153
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->a()I

    move-result v2

    .line 154
    :goto_0
    if-ge v0, v2, :cond_0

    .line 155
    invoke-virtual {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->charAt(I)C

    move-result v3

    .line 156
    invoke-static {v3}, Lcom/twitter/network/apache/message/g;->a(C)Z

    move-result v3

    if-nez v3, :cond_1

    .line 162
    :cond_0
    invoke-virtual {p2, v1}, Lcom/twitter/network/apache/message/f;->a(I)V

    .line 163
    return-void

    .line 159
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;Ljava/lang/StringBuilder;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x5c

    const/16 v7, 0x22

    .line 229
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v0

    .line 233
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v3

    .line 234
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->a()I

    move-result v4

    .line 235
    invoke-virtual {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->charAt(I)C

    move-result v2

    .line 236
    if-ne v2, v7, :cond_0

    .line 239
    add-int/lit8 v2, v0, 0x1

    .line 240
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v1

    .line 242
    :goto_1
    if-ge v3, v4, :cond_7

    .line 243
    invoke-virtual {p1, v3}, Lcom/twitter/network/apache/util/CharArrayBuffer;->charAt(I)C

    move-result v5

    .line 244
    if-eqz v0, :cond_4

    .line 245
    if-eq v5, v7, :cond_2

    if-eq v5, v8, :cond_2

    .line 246
    invoke-virtual {p3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 248
    :cond_2
    invoke-virtual {p3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    .line 242
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 251
    :cond_4
    if-ne v5, v7, :cond_5

    .line 252
    add-int/lit8 v0, v2, 0x1

    .line 262
    :goto_3
    invoke-virtual {p2, v0}, Lcom/twitter/network/apache/message/f;->a(I)V

    goto :goto_0

    .line 255
    :cond_5
    if-ne v5, v8, :cond_6

    .line 256
    const/4 v0, 0x1

    goto :goto_2

    .line 257
    :cond_6
    const/16 v6, 0xd

    if-eq v5, v6, :cond_3

    const/16 v6, 0xa

    if-eq v5, v6, :cond_3

    .line 258
    invoke-virtual {p3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method public a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;Ljava/util/BitSet;Ljava/lang/StringBuilder;)V
    .locals 5

    .prologue
    .line 177
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v1

    .line 178
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v0

    .line 179
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->a()I

    move-result v2

    .line 180
    :goto_0
    if-ge v0, v2, :cond_1

    .line 181
    invoke-virtual {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->charAt(I)C

    move-result v3

    .line 182
    if-eqz p3, :cond_0

    invoke-virtual {p3, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    invoke-static {v3}, Lcom/twitter/network/apache/message/g;->a(C)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 189
    :cond_1
    invoke-virtual {p2, v1}, Lcom/twitter/network/apache/message/f;->a(I)V

    .line 190
    return-void

    .line 185
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 186
    invoke-virtual {p4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public b(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;Ljava/util/BitSet;)Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0x20

    const/4 v1, 0x0

    .line 117
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 119
    :goto_0
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 120
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/twitter/network/apache/util/CharArrayBuffer;->charAt(I)C

    move-result v3

    .line 121
    if-eqz p3, :cond_1

    invoke-virtual {p3, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 140
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 123
    :cond_1
    invoke-static {v3}, Lcom/twitter/network/apache/message/g;->a(C)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 124
    invoke-virtual {p0, p1, p2}, Lcom/twitter/network/apache/message/g;->a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)V

    .line 125
    const/4 v0, 0x1

    goto :goto_0

    .line 126
    :cond_2
    const/16 v4, 0x22

    if-ne v3, v4, :cond_4

    .line 127
    if-eqz v0, :cond_3

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 128
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130
    :cond_3
    invoke-virtual {p0, p1, p2, v2}, Lcom/twitter/network/apache/message/g;->a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;Ljava/lang/StringBuilder;)V

    move v0, v1

    .line 131
    goto :goto_0

    .line 133
    :cond_4
    if-eqz v0, :cond_5

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 134
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 136
    :cond_5
    invoke-virtual {p0, p1, p2, p3, v2}, Lcom/twitter/network/apache/message/g;->b(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;Ljava/util/BitSet;Ljava/lang/StringBuilder;)V

    move v0, v1

    .line 137
    goto :goto_0
.end method

.method public b(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;Ljava/util/BitSet;Ljava/lang/StringBuilder;)V
    .locals 5

    .prologue
    .line 204
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v1

    .line 205
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v0

    .line 206
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->a()I

    move-result v2

    .line 207
    :goto_0
    if-ge v0, v2, :cond_1

    .line 208
    invoke-virtual {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->charAt(I)C

    move-result v3

    .line 209
    if-eqz p3, :cond_0

    invoke-virtual {p3, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 210
    :cond_0
    invoke-static {v3}, Lcom/twitter/network/apache/message/g;->a(C)Z

    move-result v4

    if-nez v4, :cond_1

    const/16 v4, 0x22

    if-ne v3, v4, :cond_2

    .line 217
    :cond_1
    invoke-virtual {p2, v1}, Lcom/twitter/network/apache/message/f;->a(I)V

    .line 218
    return-void

    .line 213
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 214
    invoke-virtual {p4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 207
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
