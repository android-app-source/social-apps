.class public Lcom/twitter/network/apache/message/BasicNameValuePair;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/network/apache/f;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final serialVersionUID:J = -0x5957a9ac336aca08L


# instance fields
.field private final name:Ljava/lang/String;

.field private final value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const-string/jumbo v0, "Name"

    invoke-static {p1, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->name:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->value:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->name:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->value:Ljava/lang/String;

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 87
    if-ne p0, p1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 90
    :cond_1
    instance-of v2, p1, Lcom/twitter/network/apache/f;

    if-eqz v2, :cond_3

    .line 91
    check-cast p1, Lcom/twitter/network/apache/message/BasicNameValuePair;

    .line 92
    iget-object v2, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/network/apache/message/BasicNameValuePair;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->value:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/network/apache/message/BasicNameValuePair;->value:Ljava/lang/String;

    .line 93
    invoke-static {v2, v3}, Lcom/twitter/network/apache/util/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 95
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 100
    const/16 v0, 0x11

    .line 101
    iget-object v1, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/network/apache/util/c;->a(ILjava/lang/Object;)I

    move-result v0

    .line 102
    iget-object v1, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->value:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/network/apache/util/c;->a(ILjava/lang/Object;)I

    move-result v0

    .line 103
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->value:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->name:Ljava/lang/String;

    .line 82
    :goto_0
    return-object v0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 79
    iget-object v0, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    const-string/jumbo v0, "="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    iget-object v0, p0, Lcom/twitter/network/apache/message/BasicNameValuePair;->value:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
