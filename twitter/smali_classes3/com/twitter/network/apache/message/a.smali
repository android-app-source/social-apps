.class public Lcom/twitter/network/apache/message/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/network/apache/d;
.implements Ljava/lang/Cloneable;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:[Lcom/twitter/network/apache/f;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Lcom/twitter/network/apache/f;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-string/jumbo v0, "Name"

    invoke-static {p1, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/network/apache/message/a;->a:Ljava/lang/String;

    .line 60
    iput-object p2, p0, Lcom/twitter/network/apache/message/a;->b:Ljava/lang/String;

    .line 61
    if-eqz p3, :cond_0

    .line 62
    iput-object p3, p0, Lcom/twitter/network/apache/message/a;->c:[Lcom/twitter/network/apache/f;

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/twitter/network/apache/f;

    iput-object v0, p0, Lcom/twitter/network/apache/message/a;->c:[Lcom/twitter/network/apache/f;

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/network/apache/message/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/network/apache/message/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()[Lcom/twitter/network/apache/f;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/network/apache/message/a;->c:[Lcom/twitter/network/apache/f;

    invoke-virtual {v0}, [Lcom/twitter/network/apache/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/network/apache/f;

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 162
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 119
    if-ne p0, p1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v0

    .line 122
    :cond_1
    instance-of v2, p1, Lcom/twitter/network/apache/d;

    if-eqz v2, :cond_3

    .line 123
    check-cast p1, Lcom/twitter/network/apache/message/a;

    .line 124
    iget-object v2, p0, Lcom/twitter/network/apache/message/a;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/network/apache/message/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/network/apache/message/a;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/network/apache/message/a;->b:Ljava/lang/String;

    .line 125
    invoke-static {v2, v3}, Lcom/twitter/network/apache/util/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/network/apache/message/a;->c:[Lcom/twitter/network/apache/f;

    iget-object v3, p1, Lcom/twitter/network/apache/message/a;->c:[Lcom/twitter/network/apache/f;

    .line 126
    invoke-static {v2, v3}, Lcom/twitter/network/apache/util/c;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 128
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 134
    const/16 v0, 0x11

    .line 135
    iget-object v1, p0, Lcom/twitter/network/apache/message/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/network/apache/util/c;->a(ILjava/lang/Object;)I

    move-result v0

    .line 136
    iget-object v1, p0, Lcom/twitter/network/apache/message/a;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/network/apache/util/c;->a(ILjava/lang/Object;)I

    move-result v1

    .line 137
    iget-object v2, p0, Lcom/twitter/network/apache/message/a;->c:[Lcom/twitter/network/apache/f;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 138
    invoke-static {v1, v4}, Lcom/twitter/network/apache/util/c;->a(ILjava/lang/Object;)I

    move-result v1

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_0
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 145
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    iget-object v0, p0, Lcom/twitter/network/apache/message/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    iget-object v0, p0, Lcom/twitter/network/apache/message/a;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 148
    const-string/jumbo v0, "="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    iget-object v0, p0, Lcom/twitter/network/apache/message/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    :cond_0
    iget-object v2, p0, Lcom/twitter/network/apache/message/a;->c:[Lcom/twitter/network/apache/f;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 152
    const-string/jumbo v5, "; "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
