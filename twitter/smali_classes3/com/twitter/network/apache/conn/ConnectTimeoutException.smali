.class public Lcom/twitter/network/apache/conn/ConnectTimeoutException;
.super Ljava/io/InterruptedIOException;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = -0x42d84b949dece2f5L


# instance fields
.field private final host:Lcom/twitter/network/apache/HttpHost;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/io/InterruptedIOException;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/network/apache/conn/ConnectTimeoutException;->host:Lcom/twitter/network/apache/HttpHost;

    .line 56
    return-void
.end method
