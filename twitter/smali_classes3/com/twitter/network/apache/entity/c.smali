.class public Lcom/twitter/network/apache/entity/c;
.super Lcom/twitter/network/apache/entity/a;
.source "Twttr"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field protected final d:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/twitter/network/apache/entity/ContentType;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/charset/UnsupportedCharsetException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/twitter/network/apache/entity/a;-><init>()V

    .line 65
    const-string/jumbo v0, "Source string"

    invoke-static {p1, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 66
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/twitter/network/apache/entity/ContentType;->b()Ljava/nio/charset/Charset;

    move-result-object v0

    .line 67
    :goto_0
    if-nez v0, :cond_0

    .line 68
    sget-object v0, Lciz;->a:Ljava/nio/charset/Charset;

    .line 70
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/network/apache/entity/c;->d:[B

    .line 71
    if-eqz p2, :cond_1

    .line 72
    invoke-virtual {p2}, Lcom/twitter/network/apache/entity/ContentType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/network/apache/entity/c;->a(Ljava/lang/String;)V

    .line 74
    :cond_1
    return-void

    .line 66
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/charset/UnsupportedCharsetException;
        }
    .end annotation

    .prologue
    .line 116
    sget-object v0, Lcom/twitter/network/apache/entity/ContentType;->j:Lcom/twitter/network/apache/entity/ContentType;

    invoke-virtual {v0}, Lcom/twitter/network/apache/entity/ContentType;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/network/apache/entity/c;-><init>(Ljava/lang/String;Lcom/twitter/network/apache/entity/ContentType;)V

    .line 117
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/twitter/network/apache/entity/ContentType;->j:Lcom/twitter/network/apache/entity/ContentType;

    invoke-virtual {v0}, Lcom/twitter/network/apache/entity/ContentType;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/network/apache/entity/c;-><init>(Ljava/lang/String;Lcom/twitter/network/apache/entity/ContentType;)V

    .line 133
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/network/apache/entity/c;->d:[B

    array-length v0, v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public a(Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    const-string/jumbo v0, "Output stream"

    invoke-static {p1, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 167
    iget-object v0, p0, Lcom/twitter/network/apache/entity/c;->d:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 168
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 169
    return-void
.end method

.method public b()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/twitter/network/apache/entity/c;->d:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 183
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
