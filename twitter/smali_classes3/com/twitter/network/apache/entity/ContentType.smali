.class public final Lcom/twitter/network/apache/entity/ContentType;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lcom/twitter/network/apache/entity/ContentType;

.field public static final b:Lcom/twitter/network/apache/entity/ContentType;

.field public static final c:Lcom/twitter/network/apache/entity/ContentType;

.field public static final d:Lcom/twitter/network/apache/entity/ContentType;

.field public static final e:Lcom/twitter/network/apache/entity/ContentType;

.field public static final f:Lcom/twitter/network/apache/entity/ContentType;

.field public static final g:Lcom/twitter/network/apache/entity/ContentType;

.field public static final h:Lcom/twitter/network/apache/entity/ContentType;

.field public static final i:Lcom/twitter/network/apache/entity/ContentType;

.field public static final j:Lcom/twitter/network/apache/entity/ContentType;

.field public static final k:Lcom/twitter/network/apache/entity/ContentType;

.field public static final l:Lcom/twitter/network/apache/entity/ContentType;

.field public static final m:Lcom/twitter/network/apache/entity/ContentType;

.field public static final n:Lcom/twitter/network/apache/entity/ContentType;

.field private static final serialVersionUID:J = -0x6bcff2af98b1a2b8L


# instance fields
.field private final charset:Ljava/nio/charset/Charset;

.field private final mimeType:Ljava/lang/String;

.field private final params:[Lcom/twitter/network/apache/f;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 67
    const-string/jumbo v0, "application/atom+xml"

    sget-object v2, Lcom/twitter/network/apache/a;->c:Ljava/nio/charset/Charset;

    invoke-static {v0, v2}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->a:Lcom/twitter/network/apache/entity/ContentType;

    .line 69
    const-string/jumbo v0, "application/x-www-form-urlencoded"

    sget-object v2, Lcom/twitter/network/apache/a;->c:Ljava/nio/charset/Charset;

    invoke-static {v0, v2}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->b:Lcom/twitter/network/apache/entity/ContentType;

    .line 71
    const-string/jumbo v0, "application/json"

    sget-object v2, Lcom/twitter/network/apache/a;->a:Ljava/nio/charset/Charset;

    invoke-static {v0, v2}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->c:Lcom/twitter/network/apache/entity/ContentType;

    .line 73
    const-string/jumbo v2, "application/octet-stream"

    move-object v0, v1

    check-cast v0, Ljava/nio/charset/Charset;

    invoke-static {v2, v0}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->d:Lcom/twitter/network/apache/entity/ContentType;

    .line 75
    const-string/jumbo v0, "application/svg+xml"

    sget-object v2, Lcom/twitter/network/apache/a;->c:Ljava/nio/charset/Charset;

    invoke-static {v0, v2}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->e:Lcom/twitter/network/apache/entity/ContentType;

    .line 77
    const-string/jumbo v0, "application/xhtml+xml"

    sget-object v2, Lcom/twitter/network/apache/a;->c:Ljava/nio/charset/Charset;

    invoke-static {v0, v2}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->f:Lcom/twitter/network/apache/entity/ContentType;

    .line 79
    const-string/jumbo v0, "application/xml"

    sget-object v2, Lcom/twitter/network/apache/a;->c:Ljava/nio/charset/Charset;

    invoke-static {v0, v2}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->g:Lcom/twitter/network/apache/entity/ContentType;

    .line 81
    const-string/jumbo v0, "multipart/form-data"

    sget-object v2, Lcom/twitter/network/apache/a;->c:Ljava/nio/charset/Charset;

    invoke-static {v0, v2}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->h:Lcom/twitter/network/apache/entity/ContentType;

    .line 83
    const-string/jumbo v0, "text/html"

    sget-object v2, Lcom/twitter/network/apache/a;->c:Ljava/nio/charset/Charset;

    invoke-static {v0, v2}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->i:Lcom/twitter/network/apache/entity/ContentType;

    .line 85
    const-string/jumbo v0, "text/plain"

    sget-object v2, Lcom/twitter/network/apache/a;->c:Ljava/nio/charset/Charset;

    invoke-static {v0, v2}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->j:Lcom/twitter/network/apache/entity/ContentType;

    .line 87
    const-string/jumbo v0, "text/xml"

    sget-object v2, Lcom/twitter/network/apache/a;->c:Ljava/nio/charset/Charset;

    invoke-static {v0, v2}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->k:Lcom/twitter/network/apache/entity/ContentType;

    .line 89
    const-string/jumbo v0, "*/*"

    check-cast v1, Ljava/nio/charset/Charset;

    invoke-static {v0, v1}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->l:Lcom/twitter/network/apache/entity/ContentType;

    .line 93
    sget-object v0, Lcom/twitter/network/apache/entity/ContentType;->j:Lcom/twitter/network/apache/entity/ContentType;

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->m:Lcom/twitter/network/apache/entity/ContentType;

    .line 94
    sget-object v0, Lcom/twitter/network/apache/entity/ContentType;->d:Lcom/twitter/network/apache/entity/ContentType;

    sput-object v0, Lcom/twitter/network/apache/entity/ContentType;->n:Lcom/twitter/network/apache/entity/ContentType;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/twitter/network/apache/entity/ContentType;->mimeType:Ljava/lang/String;

    .line 104
    iput-object p2, p0, Lcom/twitter/network/apache/entity/ContentType;->charset:Ljava/nio/charset/Charset;

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/network/apache/entity/ContentType;->params:[Lcom/twitter/network/apache/f;

    .line 106
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/nio/charset/Charset;[Lcom/twitter/network/apache/f;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-object p1, p0, Lcom/twitter/network/apache/entity/ContentType;->mimeType:Ljava/lang/String;

    .line 113
    iput-object p2, p0, Lcom/twitter/network/apache/entity/ContentType;->charset:Ljava/nio/charset/Charset;

    .line 114
    iput-object p3, p0, Lcom/twitter/network/apache/entity/ContentType;->params:[Lcom/twitter/network/apache/f;

    .line 115
    return-void
.end method

.method private static a(Lcom/twitter/network/apache/d;Z)Lcom/twitter/network/apache/entity/ContentType;
    .locals 2

    .prologue
    .line 211
    invoke-interface {p0}, Lcom/twitter/network/apache/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/twitter/network/apache/d;->c()[Lcom/twitter/network/apache/f;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;[Lcom/twitter/network/apache/f;Z)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/twitter/network/apache/e;)Lcom/twitter/network/apache/entity/ContentType;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/twitter/network/apache/ParseException;,
            Ljava/nio/charset/UnsupportedCharsetException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 289
    if-nez p0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-object v0

    .line 292
    :cond_1
    invoke-interface {p0}, Lcom/twitter/network/apache/e;->f()Lcom/twitter/network/apache/c;

    move-result-object v1

    .line 293
    if-eqz v1, :cond_0

    .line 294
    invoke-interface {v1}, Lcom/twitter/network/apache/c;->d()[Lcom/twitter/network/apache/d;

    move-result-object v1

    .line 295
    array-length v2, v1

    if-lez v2, :cond_0

    .line 296
    const/4 v0, 0x0

    aget-object v0, v1, v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/network/apache/entity/ContentType;->a(Lcom/twitter/network/apache/d;Z)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/apache/entity/ContentType;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/charset/UnsupportedCharsetException;
        }
    .end annotation

    .prologue
    .line 207
    invoke-static {p1}, Lcom/twitter/network/apache/util/d;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    :goto_0
    invoke-static {p0, v0}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/nio/charset/Charset;)Lcom/twitter/network/apache/entity/ContentType;
    .locals 3

    .prologue
    .line 178
    const-string/jumbo v0, "MIME type"

    invoke-static {p0, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 179
    invoke-static {v0}, Lcom/twitter/network/apache/entity/ContentType;->a(Ljava/lang/String;)Z

    move-result v1

    const-string/jumbo v2, "MIME type may not contain reserved characters"

    invoke-static {v1, v2}, Lcom/twitter/network/apache/util/a;->a(ZLjava/lang/String;)V

    .line 180
    new-instance v1, Lcom/twitter/network/apache/entity/ContentType;

    invoke-direct {v1, v0, p1}, Lcom/twitter/network/apache/entity/ContentType;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    return-object v1
.end method

.method private static a(Ljava/lang/String;[Lcom/twitter/network/apache/f;Z)Lcom/twitter/network/apache/entity/ContentType;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 215
    .line 216
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, p1, v0

    .line 217
    invoke-interface {v3}, Lcom/twitter/network/apache/f;->a()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "charset"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 218
    invoke-interface {v3}, Lcom/twitter/network/apache/f;->b()Ljava/lang/String;

    move-result-object v0

    .line 219
    invoke-static {v0}, Lcom/twitter/network/apache/util/d;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 221
    :try_start_0
    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;
    :try_end_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 231
    :goto_1
    new-instance v2, Lcom/twitter/network/apache/entity/ContentType;

    if-eqz p1, :cond_2

    array-length v3, p1

    if-lez v3, :cond_2

    :goto_2
    invoke-direct {v2, p0, v0, p1}, Lcom/twitter/network/apache/entity/ContentType;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;[Lcom/twitter/network/apache/f;)V

    return-object v2

    .line 222
    :catch_0
    move-exception v0

    .line 223
    if-eqz p2, :cond_0

    .line 224
    throw v0

    :cond_0
    move-object v0, v1

    .line 226
    goto :goto_1

    .line 216
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move-object p1, v1

    .line 231
    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 160
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 161
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 162
    const/16 v3, 0x22

    if-eq v2, v3, :cond_0

    const/16 v3, 0x2c

    if-eq v2, v3, :cond_0

    const/16 v3, 0x3b

    if-ne v2, v3, :cond_1

    .line 166
    :cond_0
    :goto_1
    return v1

    .line 160
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 166
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/network/apache/entity/ContentType;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/nio/charset/Charset;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/network/apache/entity/ContentType;->charset:Ljava/nio/charset/Charset;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 147
    new-instance v0, Lcom/twitter/network/apache/util/CharArrayBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lcom/twitter/network/apache/util/CharArrayBuffer;-><init>(I)V

    .line 148
    iget-object v1, p0, Lcom/twitter/network/apache/entity/ContentType;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lcom/twitter/network/apache/entity/ContentType;->params:[Lcom/twitter/network/apache/f;

    if-eqz v1, :cond_1

    .line 150
    const-string/jumbo v1, "; "

    invoke-virtual {v0, v1}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(Ljava/lang/String;)V

    .line 151
    sget-object v1, Lcom/twitter/network/apache/message/b;->b:Lcom/twitter/network/apache/message/b;

    iget-object v2, p0, Lcom/twitter/network/apache/entity/ContentType;->params:[Lcom/twitter/network/apache/f;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/network/apache/message/b;->a(Lcom/twitter/network/apache/util/CharArrayBuffer;[Lcom/twitter/network/apache/f;Z)Lcom/twitter/network/apache/util/CharArrayBuffer;

    .line 156
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 152
    :cond_1
    iget-object v1, p0, Lcom/twitter/network/apache/entity/ContentType;->charset:Ljava/nio/charset/Charset;

    if-eqz v1, :cond_0

    .line 153
    const-string/jumbo v1, "; charset="

    invoke-virtual {v0, v1}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(Ljava/lang/String;)V

    .line 154
    iget-object v1, p0, Lcom/twitter/network/apache/entity/ContentType;->charset:Ljava/nio/charset/Charset;

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
