.class public abstract Lcom/twitter/network/apache/entity/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/network/apache/e;


# instance fields
.field protected a:Lcom/twitter/network/apache/c;

.field protected b:Lcom/twitter/network/apache/c;

.field protected c:Z


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/network/apache/c;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/twitter/network/apache/entity/a;->a:Lcom/twitter/network/apache/c;

    .line 115
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 126
    const/4 v0, 0x0

    .line 127
    if-eqz p1, :cond_0

    .line 128
    new-instance v0, Lcom/twitter/network/apache/message/BasicHeader;

    const-string/jumbo v1, "Content-Type"

    invoke-direct {v0, v1, p1}, Lcom/twitter/network/apache/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/network/apache/entity/a;->a(Lcom/twitter/network/apache/c;)V

    .line 131
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/twitter/network/apache/entity/a;->c:Z

    return v0
.end method

.method public f()Lcom/twitter/network/apache/c;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/network/apache/entity/a;->a:Lcom/twitter/network/apache/c;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x2c

    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 196
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 197
    iget-object v1, p0, Lcom/twitter/network/apache/entity/a;->a:Lcom/twitter/network/apache/c;

    if-eqz v1, :cond_0

    .line 198
    const-string/jumbo v1, "Content-Type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    iget-object v1, p0, Lcom/twitter/network/apache/entity/a;->a:Lcom/twitter/network/apache/c;

    invoke-interface {v1}, Lcom/twitter/network/apache/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 202
    :cond_0
    iget-object v1, p0, Lcom/twitter/network/apache/entity/a;->b:Lcom/twitter/network/apache/c;

    if-eqz v1, :cond_1

    .line 203
    const-string/jumbo v1, "Content-Encoding: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    iget-object v1, p0, Lcom/twitter/network/apache/entity/a;->b:Lcom/twitter/network/apache/c;

    invoke-interface {v1}, Lcom/twitter/network/apache/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 207
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/network/apache/entity/a;->a()J

    move-result-wide v2

    .line 208
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    .line 209
    const-string/jumbo v1, "Content-Length: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 211
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 213
    :cond_2
    const-string/jumbo v1, "Chunked: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    iget-boolean v1, p0, Lcom/twitter/network/apache/entity/a;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 215
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 216
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
