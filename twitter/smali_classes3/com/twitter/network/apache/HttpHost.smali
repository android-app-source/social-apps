.class public final Lcom/twitter/network/apache/HttpHost;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final serialVersionUID:J = -0x687dd718ea3e061aL


# instance fields
.field protected final address:Ljava/net/InetAddress;

.field protected final hostname:Ljava/lang/String;

.field protected final lcHostname:Ljava/lang/String;

.field protected final port:I

.field protected final schemeName:Ljava/lang/String;


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 264
    iget-object v1, p0, Lcom/twitter/network/apache/HttpHost;->schemeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    const-string/jumbo v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    iget-object v1, p0, Lcom/twitter/network/apache/HttpHost;->hostname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    iget v1, p0, Lcom/twitter/network/apache/HttpHost;->port:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 268
    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 269
    iget v1, p0, Lcom/twitter/network/apache/HttpHost;->port:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 333
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 302
    if-ne p0, p1, :cond_1

    .line 312
    :cond_0
    :goto_0
    return v0

    .line 305
    :cond_1
    instance-of v2, p1, Lcom/twitter/network/apache/HttpHost;

    if-eqz v2, :cond_4

    .line 306
    check-cast p1, Lcom/twitter/network/apache/HttpHost;

    .line 307
    iget-object v2, p0, Lcom/twitter/network/apache/HttpHost;->lcHostname:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/network/apache/HttpHost;->lcHostname:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/twitter/network/apache/HttpHost;->port:I

    iget v3, p1, Lcom/twitter/network/apache/HttpHost;->port:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/twitter/network/apache/HttpHost;->schemeName:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/network/apache/HttpHost;->schemeName:Ljava/lang/String;

    .line 309
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/network/apache/HttpHost;->address:Ljava/net/InetAddress;

    if-nez v2, :cond_3

    iget-object v2, p1, Lcom/twitter/network/apache/HttpHost;->address:Ljava/net/InetAddress;

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    .line 310
    goto :goto_0

    .line 309
    :cond_3
    iget-object v2, p0, Lcom/twitter/network/apache/HttpHost;->address:Ljava/net/InetAddress;

    iget-object v3, p1, Lcom/twitter/network/apache/HttpHost;->address:Ljava/net/InetAddress;

    .line 310
    invoke-virtual {v2, v3}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_4
    move v0, v1

    .line 312
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 321
    const/16 v0, 0x11

    .line 322
    iget-object v1, p0, Lcom/twitter/network/apache/HttpHost;->lcHostname:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/network/apache/util/c;->a(ILjava/lang/Object;)I

    move-result v0

    .line 323
    iget v1, p0, Lcom/twitter/network/apache/HttpHost;->port:I

    invoke-static {v0, v1}, Lcom/twitter/network/apache/util/c;->a(II)I

    move-result v0

    .line 324
    iget-object v1, p0, Lcom/twitter/network/apache/HttpHost;->schemeName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/network/apache/util/c;->a(ILjava/lang/Object;)I

    move-result v0

    .line 325
    iget-object v1, p0, Lcom/twitter/network/apache/HttpHost;->address:Ljava/net/InetAddress;

    if-eqz v1, :cond_0

    .line 326
    iget-object v1, p0, Lcom/twitter/network/apache/HttpHost;->address:Ljava/net/InetAddress;

    invoke-static {v0, v1}, Lcom/twitter/network/apache/util/c;->a(ILjava/lang/Object;)I

    move-result v0

    .line 328
    :cond_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/twitter/network/apache/HttpHost;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
