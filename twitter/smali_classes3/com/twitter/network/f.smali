.class public Lcom/twitter/network/f;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:I

.field private static b:I

.field private static c:I

.field private static d:I

.field private static e:I

.field private static f:I


# instance fields
.field private final g:Lcom/twitter/network/k;

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x4e20

    sput v0, Lcom/twitter/network/f;->a:I

    .line 29
    const v0, 0x15f90

    sput v0, Lcom/twitter/network/f;->b:I

    .line 30
    const v0, 0xea60

    sput v0, Lcom/twitter/network/f;->c:I

    .line 31
    const/4 v0, 0x2

    sput v0, Lcom/twitter/network/f;->d:I

    .line 32
    const/16 v0, 0x64

    sput v0, Lcom/twitter/network/f;->e:I

    .line 33
    const/16 v0, 0x800

    sput v0, Lcom/twitter/network/f;->f:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 94
    invoke-direct {p0, v0, v0}, Lcom/twitter/network/f;-><init>(Lcom/twitter/network/k;Ljava/io/File;)V

    .line 95
    return-void
.end method

.method public constructor <init>(Lcom/twitter/network/k;IIIIIIILjava/io/File;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/twitter/network/f;->g:Lcom/twitter/network/k;

    .line 58
    iput p3, p0, Lcom/twitter/network/f;->h:I

    .line 59
    iput p2, p0, Lcom/twitter/network/f;->i:I

    .line 60
    iput p4, p0, Lcom/twitter/network/f;->j:I

    .line 61
    iput p5, p0, Lcom/twitter/network/f;->k:I

    .line 62
    iput p6, p0, Lcom/twitter/network/f;->l:I

    .line 63
    iput p7, p0, Lcom/twitter/network/f;->m:I

    .line 64
    iput p8, p0, Lcom/twitter/network/f;->n:I

    .line 65
    iput-object p9, p0, Lcom/twitter/network/f;->o:Ljava/io/File;

    .line 66
    return-void
.end method

.method public constructor <init>(Lcom/twitter/network/k;IILjava/io/File;)V
    .locals 10

    .prologue
    .line 72
    .line 75
    invoke-static {}, Lcom/twitter/network/f;->i()I

    move-result v4

    .line 76
    invoke-static {}, Lcom/twitter/network/f;->h()I

    move-result v5

    .line 77
    invoke-static {}, Lcom/twitter/network/f;->j()I

    move-result v6

    .line 78
    invoke-static {}, Lcom/twitter/network/f;->k()I

    move-result v7

    .line 79
    invoke-static {}, Lcom/twitter/network/f;->l()I

    move-result v8

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v9, p4

    .line 72
    invoke-direct/range {v0 .. v9}, Lcom/twitter/network/f;-><init>(Lcom/twitter/network/k;IIIIIIILjava/io/File;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Lcom/twitter/network/k;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 87
    invoke-static {}, Lcom/twitter/network/f;->f()I

    move-result v0

    invoke-static {}, Lcom/twitter/network/f;->g()I

    move-result v1

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/twitter/network/f;-><init>(Lcom/twitter/network/k;IILjava/io/File;)V

    .line 88
    return-void
.end method

.method public static a(II)V
    .locals 1

    .prologue
    .line 150
    sput p0, Lcom/twitter/network/f;->b:I

    .line 151
    sput p1, Lcom/twitter/network/f;->a:I

    .line 152
    const-class v0, Lcom/twitter/network/f;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 153
    return-void
.end method

.method public static f()I
    .locals 1

    .prologue
    .line 193
    sget v0, Lcom/twitter/network/f;->b:I

    return v0
.end method

.method public static g()I
    .locals 1

    .prologue
    .line 204
    sget v0, Lcom/twitter/network/f;->a:I

    return v0
.end method

.method public static h()I
    .locals 1

    .prologue
    .line 213
    sget v0, Lcom/twitter/network/f;->d:I

    return v0
.end method

.method public static i()I
    .locals 1

    .prologue
    .line 222
    sget v0, Lcom/twitter/network/f;->c:I

    return v0
.end method

.method public static j()I
    .locals 1

    .prologue
    .line 229
    sget v0, Lcom/twitter/network/f;->e:I

    return v0
.end method

.method public static k()I
    .locals 1

    .prologue
    .line 238
    sget v0, Lcom/twitter/network/f;->f:I

    return v0
.end method

.method public static l()I
    .locals 1

    .prologue
    .line 245
    const/high16 v0, 0x200000

    return v0
.end method


# virtual methods
.method public a()Lcom/twitter/network/k;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/network/f;->g:Lcom/twitter/network/k;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/twitter/network/f;->h:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/twitter/network/f;->i:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/twitter/network/f;->n:I

    return v0
.end method

.method public e()Ljava/io/File;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/network/f;->o:Ljava/io/File;

    return-object v0
.end method
