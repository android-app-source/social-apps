.class public Lcom/twitter/network/p;
.super Lcom/twitter/network/e;
.source "Twttr"


# instance fields
.field private b:Lokhttp3/OkHttpClient;


# direct methods
.method public constructor <init>(Lcom/twitter/network/f;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/network/e;-><init>(Lcom/twitter/network/f;)V

    .line 21
    return-void
.end method

.method private declared-synchronized c()Lokhttp3/OkHttpClient;
    .locals 1

    .prologue
    .line 25
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/network/p;->b:Lokhttp3/OkHttpClient;

    if-nez v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/twitter/network/p;->a:Lcom/twitter/network/f;

    invoke-virtual {p0, v0}, Lcom/twitter/network/p;->a(Lcom/twitter/network/f;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/network/p;->b:Lokhttp3/OkHttpClient;

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/twitter/network/p;->b:Lokhttp3/OkHttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)Lcom/twitter/network/HttpOperation;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/network/o;

    invoke-direct {p0}, Lcom/twitter/network/p;->c()Lokhttp3/OkHttpClient;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/twitter/network/o;-><init>(Lokhttp3/OkHttpClient;Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)V

    return-object v0
.end method

.method protected a(Lcom/twitter/network/f;)Lokhttp3/OkHttpClient$Builder;
    .locals 6

    .prologue
    .line 58
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 59
    invoke-virtual {p1}, Lcom/twitter/network/f;->c()I

    move-result v1

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 60
    invoke-virtual {p1}, Lcom/twitter/network/f;->b()I

    move-result v1

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 61
    invoke-virtual {p1}, Lcom/twitter/network/f;->b()I

    move-result v1

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 63
    invoke-virtual {p1}, Lcom/twitter/network/f;->a()Lcom/twitter/network/k;

    move-result-object v1

    .line 65
    iget-object v2, p0, Lcom/twitter/network/p;->a:Lcom/twitter/network/f;

    invoke-virtual {v2}, Lcom/twitter/network/f;->e()Ljava/io/File;

    move-result-object v2

    .line 66
    if-eqz v2, :cond_0

    .line 67
    new-instance v3, Lokhttp3/Cache;

    iget-object v4, p0, Lcom/twitter/network/p;->a:Lcom/twitter/network/f;

    invoke-virtual {v4}, Lcom/twitter/network/f;->d()I

    move-result v4

    int-to-long v4, v4

    invoke-direct {v3, v2, v4, v5}, Lokhttp3/Cache;-><init>(Ljava/io/File;J)V

    invoke-virtual {v0, v3}, Lokhttp3/OkHttpClient$Builder;->cache(Lokhttp3/Cache;)Lokhttp3/OkHttpClient$Builder;

    .line 70
    :cond_0
    if-eqz v1, :cond_1

    iget-boolean v2, v1, Lcom/twitter/network/k;->a:Z

    if-eqz v2, :cond_1

    .line 71
    invoke-virtual {v1}, Lcom/twitter/network/k;->a()Ljava/net/Proxy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->proxy(Ljava/net/Proxy;)Lokhttp3/OkHttpClient$Builder;

    .line 73
    :cond_1
    return-object v0
.end method

.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/network/p;->b:Lokhttp3/OkHttpClient;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/twitter/network/p;->b:Lokhttp3/OkHttpClient;

    invoke-virtual {v0}, Lokhttp3/OkHttpClient;->dispatcher()Lokhttp3/Dispatcher;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Dispatcher;->executorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 42
    iget-object v0, p0, Lcom/twitter/network/p;->b:Lokhttp3/OkHttpClient;

    invoke-virtual {v0}, Lokhttp3/OkHttpClient;->connectionPool()Lokhttp3/ConnectionPool;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ConnectionPool;->evictAll()V

    .line 43
    iget-object v0, p0, Lcom/twitter/network/p;->b:Lokhttp3/OkHttpClient;

    invoke-virtual {v0}, Lokhttp3/OkHttpClient;->cache()Lokhttp3/Cache;

    move-result-object v0

    .line 44
    invoke-static {v0}, Lcpw;->a(Ljava/io/Closeable;)V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/network/p;->b:Lokhttp3/OkHttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    :cond_0
    monitor-exit p0

    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/network/p;->b:Lokhttp3/OkHttpClient;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/twitter/network/p;->b:Lokhttp3/OkHttpClient;

    invoke-virtual {v0}, Lokhttp3/OkHttpClient;->connectionPool()Lokhttp3/ConnectionPool;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ConnectionPool;->markConnectionsStale()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :cond_0
    monitor-exit p0

    return-void

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
