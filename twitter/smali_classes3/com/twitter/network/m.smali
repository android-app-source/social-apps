.class public Lcom/twitter/network/m;
.super Lcom/twitter/network/e;
.source "Twttr"


# direct methods
.method public constructor <init>(Lcom/twitter/network/f;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/twitter/network/e;-><init>(Lcom/twitter/network/f;)V

    .line 22
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)Lcom/twitter/network/HttpOperation;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/network/n;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/twitter/network/n;-><init>(Lcom/twitter/network/m;Ljava/net/URI;Lcom/twitter/network/HttpOperation$RequestMethod;Lcom/twitter/network/j;)V

    return-object v0
.end method

.method public a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/twitter/network/m;->c()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 40
    iget-object v1, p0, Lcom/twitter/network/m;->a:Lcom/twitter/network/f;

    invoke-virtual {v1}, Lcom/twitter/network/f;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 41
    iget-object v1, p0, Lcom/twitter/network/m;->a:Lcom/twitter/network/f;

    invoke-virtual {v1}, Lcom/twitter/network/f;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 45
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 47
    return-object v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method protected c()Ljava/net/Proxy;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/network/m;->a:Lcom/twitter/network/f;

    invoke-virtual {v0}, Lcom/twitter/network/f;->a()Lcom/twitter/network/k;

    move-result-object v0

    if-nez v0, :cond_0

    .line 53
    sget-object v0, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    .line 55
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/network/m;->a:Lcom/twitter/network/f;

    invoke-virtual {v0}, Lcom/twitter/network/f;->a()Lcom/twitter/network/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/k;->a()Ljava/net/Proxy;

    move-result-object v0

    goto :goto_0
.end method
