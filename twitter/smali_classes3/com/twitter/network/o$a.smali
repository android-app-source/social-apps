.class Lcom/twitter/network/o$a;
.super Lokhttp3/RequestBody;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/network/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# static fields
.field private static final a:Lokhttp3/MediaType;


# instance fields
.field private final b:Lcom/twitter/network/apache/e;

.field private final c:Lokhttp3/MediaType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 184
    const-string/jumbo v0, "application/octet-stream"

    invoke-static {v0}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/o$a;->a:Lokhttp3/MediaType;

    return-void
.end method

.method constructor <init>(Lcom/twitter/network/apache/e;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 191
    invoke-direct {p0}, Lokhttp3/RequestBody;-><init>()V

    .line 192
    iput-object p1, p0, Lcom/twitter/network/o$a;->b:Lcom/twitter/network/apache/e;

    .line 194
    if-eqz p2, :cond_0

    .line 195
    invoke-static {p2}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/network/o$a;->c:Lokhttp3/MediaType;

    .line 203
    :goto_0
    return-void

    .line 196
    :cond_0
    invoke-interface {p1}, Lcom/twitter/network/apache/e;->f()Lcom/twitter/network/apache/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 197
    invoke-interface {p1}, Lcom/twitter/network/apache/e;->f()Lcom/twitter/network/apache/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/network/apache/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/network/o$a;->c:Lokhttp3/MediaType;

    goto :goto_0

    .line 201
    :cond_1
    sget-object v0, Lcom/twitter/network/o$a;->a:Lokhttp3/MediaType;

    iput-object v0, p0, Lcom/twitter/network/o$a;->c:Lokhttp3/MediaType;

    goto :goto_0
.end method


# virtual methods
.method public contentLength()J
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/twitter/network/o$a;->b:Lcom/twitter/network/apache/e;

    invoke-interface {v0}, Lcom/twitter/network/apache/e;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public contentType()Lokhttp3/MediaType;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/twitter/network/o$a;->c:Lokhttp3/MediaType;

    return-object v0
.end method

.method public writeTo(Lokio/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/twitter/network/o$a;->b:Lcom/twitter/network/apache/e;

    invoke-interface {p1}, Lokio/d;->c()Ljava/io/OutputStream;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/network/apache/e;->a(Ljava/io/OutputStream;)V

    .line 219
    return-void
.end method
