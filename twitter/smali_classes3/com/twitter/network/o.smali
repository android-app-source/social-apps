.class public Lcom/twitter/network/o;
.super Lcom/twitter/network/HttpOperation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/network/o$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/network/HttpOperation",
        "<",
        "Lokhttp3/Request$Builder;",
        "Lokhttp3/Response;",
        ">;"
    }
.end annotation


# instance fields
.field private g:Lokhttp3/OkHttpClient;

.field private h:Lokhttp3/Call;


# direct methods
.method protected constructor <init>(Lokhttp3/OkHttpClient;Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p2, p3, p4}, Lcom/twitter/network/HttpOperation;-><init>(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)V

    .line 36
    iput-object p1, p0, Lcom/twitter/network/o;->g:Lokhttp3/OkHttpClient;

    .line 37
    return-void
.end method


# virtual methods
.method protected synthetic a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/twitter/network/o;->v()Lokhttp3/Request$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Response;

    invoke-virtual {p0, p1}, Lcom/twitter/network/o;->a(Lokhttp3/Response;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Response;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/network/o;->a(Lokhttp3/Response;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lokhttp3/Response;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-string/jumbo v0, "Content-Encoding"

    invoke-virtual {p1, v0}, Lokhttp3/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lokhttp3/Response;Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    invoke-virtual {p1, p2}, Lokhttp3/Response;->headers(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 118
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p3, :cond_0

    .line 119
    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 121
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lokhttp3/Request$Builder;)Lokhttp3/Response;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/network/o;->g:Lokhttp3/OkHttpClient;

    invoke-virtual {p1}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/network/o;->h:Lokhttp3/Call;

    .line 87
    iget-object v0, p0, Lcom/twitter/network/o;->h:Lokhttp3/Call;

    invoke-interface {v0}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Request$Builder;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/network/o;->a(Lokhttp3/Request$Builder;I)V

    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Lcom/twitter/network/apache/e;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Request$Builder;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/network/o;->a(Lokhttp3/Request$Builder;Lcom/twitter/network/apache/e;)V

    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Request$Builder;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/network/o;->a(Lokhttp3/Request$Builder;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected a(Lokhttp3/Request$Builder;I)V
    .locals 4

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/network/o;->g:Lokhttp3/OkHttpClient;

    invoke-virtual {v0}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    int-to-long v2, p2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/network/o;->g:Lokhttp3/OkHttpClient;

    .line 141
    return-void
.end method

.method protected a(Lokhttp3/Request$Builder;Lcom/twitter/network/apache/e;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 127
    invoke-virtual {p0}, Lcom/twitter/network/o;->h()Lcom/twitter/network/HttpOperation$RequestMethod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation$RequestMethod;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 128
    if-eqz p2, :cond_0

    .line 129
    new-instance v0, Lcom/twitter/network/o$a;

    invoke-direct {v0, p2, v1}, Lcom/twitter/network/o$a;-><init>(Lcom/twitter/network/apache/e;Ljava/lang/String;)V

    move-object v1, v0

    .line 134
    :cond_0
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Lokhttp3/Request$Builder;->requestBodyPresent(Z)Lokhttp3/Request$Builder;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->method(Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    .line 135
    return-void

    .line 134
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lokhttp3/Request$Builder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80
    invoke-virtual {p1, p2, p3}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 81
    return-void
.end method

.method protected a(Lcom/twitter/network/HttpOperation$Protocol;)Z
    .locals 1

    .prologue
    .line 158
    :try_start_0
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation$Protocol;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lokhttp3/Protocol;->get(Ljava/lang/String;)Lokhttp3/Protocol;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    const/4 v0, 0x1

    .line 161
    :goto_0
    return v0

    .line 160
    :catch_0
    move-exception v0

    .line 161
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Response;

    invoke-virtual {p0, p1}, Lcom/twitter/network/o;->b(Lokhttp3/Response;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lokhttp3/Response;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const-string/jumbo v0, "Content-Type"

    invoke-virtual {p1, v0}, Lokhttp3/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lokhttp3/Request$Builder;)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/network/o;->h:Lokhttp3/Call;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/twitter/network/o;->h:Lokhttp3/Call;

    invoke-interface {v0}, Lokhttp3/Call;->cancel()V

    .line 95
    :cond_0
    return-void
.end method

.method protected b([Lcom/twitter/network/HttpOperation$Protocol;)V
    .locals 5

    .prologue
    .line 167
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 168
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 170
    :try_start_0
    invoke-virtual {v3}, Lcom/twitter/network/HttpOperation$Protocol;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lokhttp3/Protocol;->get(Ljava/lang/String;)Lokhttp3/Protocol;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown Protocol "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/twitter/network/o;->g:Lokhttp3/OkHttpClient;

    invoke-virtual {v0}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v2, v0}, Lokhttp3/OkHttpClient$Builder;->protocols(Ljava/util/List;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/network/o;->g:Lokhttp3/OkHttpClient;

    .line 176
    return-void
.end method

.method protected bridge synthetic c(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Response;

    invoke-virtual {p0, p1}, Lcom/twitter/network/o;->c(Lokhttp3/Response;)I

    move-result v0

    return v0
.end method

.method protected c(Lokhttp3/Response;)I
    .locals 4

    .prologue
    .line 53
    invoke-virtual {p1}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->contentLength()J

    move-result-wide v0

    .line 54
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 55
    const-string/jumbo v0, "TwitterNetwork"

    const-string/jumbo v1, "OkHttp3 response body exceeded Integer.MAX_VALUE. Returning Integer.MAX_VALUE"

    invoke-static {v0, v1}, Lcqj;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const v0, 0x7fffffff

    .line 59
    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method

.method protected bridge synthetic d(Ljava/lang/Object;)Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Response;

    invoke-virtual {p0, p1}, Lcom/twitter/network/o;->d(Lokhttp3/Response;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lokhttp3/Response;)Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p1}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->byteStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    const-string/jumbo v0, "OkHttp3"

    return-object v0
.end method

.method protected e(Lokhttp3/Response;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/Response;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p1}, Lokhttp3/Response;->headers()Lokhttp3/Headers;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Headers;->toMultimap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic e(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Request$Builder;

    invoke-virtual {p0, p1}, Lcom/twitter/network/o;->b(Lokhttp3/Request$Builder;)V

    return-void
.end method

.method protected f(Lokhttp3/Response;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p1}, Lokhttp3/Response;->code()I

    move-result v0

    return v0
.end method

.method protected synthetic f(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Request$Builder;

    invoke-virtual {p0, p1}, Lcom/twitter/network/o;->a(Lokhttp3/Request$Builder;)Lokhttp3/Response;

    move-result-object v0

    return-object v0
.end method

.method protected g(Lokhttp3/Response;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p1}, Lokhttp3/Response;->message()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Ljava/lang/Object;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Response;

    invoke-virtual {p0, p1}, Lcom/twitter/network/o;->e(Lokhttp3/Response;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic h(Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Response;

    invoke-virtual {p0, p1}, Lcom/twitter/network/o;->f(Lokhttp3/Response;)I

    move-result v0

    return v0
.end method

.method protected h(Lokhttp3/Response;)Lcom/twitter/network/HttpOperation$Protocol;
    .locals 1

    .prologue
    .line 152
    invoke-virtual {p1}, Lokhttp3/Response;->protocol()Lokhttp3/Protocol;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Protocol;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/network/HttpOperation$Protocol;->a(Ljava/lang/String;)Lcom/twitter/network/HttpOperation$Protocol;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic i(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Response;

    invoke-virtual {p0, p1}, Lcom/twitter/network/o;->g(Lokhttp3/Response;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic k(Ljava/lang/Object;)Lcom/twitter/network/HttpOperation$Protocol;
    .locals 1

    .prologue
    .line 28
    check-cast p1, Lokhttp3/Response;

    invoke-virtual {p0, p1}, Lcom/twitter/network/o;->h(Lokhttp3/Response;)Lcom/twitter/network/HttpOperation$Protocol;

    move-result-object v0

    return-object v0
.end method

.method protected v()Lokhttp3/Request$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    sget-object v0, Lokhttp3/internal/Internal;->instance:Lokhttp3/internal/Internal;

    invoke-virtual {p0}, Lcom/twitter/network/o;->i()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/internal/Internal;->getHttpUrlChecked(Ljava/lang/String;)Lokhttp3/HttpUrl;

    move-result-object v0

    .line 74
    new-instance v1, Lokhttp3/Request$Builder;

    invoke-direct {v1}, Lokhttp3/Request$Builder;-><init>()V

    .line 75
    invoke-virtual {v1, v0}, Lokhttp3/Request$Builder;->url(Lokhttp3/HttpUrl;)Lokhttp3/Request$Builder;

    move-result-object v0

    .line 74
    return-object v0
.end method
