.class public final Lcom/twitter/network/k;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 23
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 25
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/network/k;->c:Ljava/lang/String;

    move p1, v0

    .line 30
    :goto_0
    const/4 v1, -0x1

    .line 32
    :try_start_0
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 36
    :goto_1
    iput v0, p0, Lcom/twitter/network/k;->b:I

    .line 37
    iput-boolean p1, p0, Lcom/twitter/network/k;->a:Z

    .line 38
    return-void

    .line 27
    :cond_0
    iput-object v1, p0, Lcom/twitter/network/k;->c:Ljava/lang/String;

    goto :goto_0

    .line 33
    :catch_0
    move-exception v2

    move p1, v0

    move v0, v1

    .line 34
    goto :goto_1
.end method


# virtual methods
.method public a()Ljava/net/Proxy;
    .locals 3

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/twitter/network/k;->a:Z

    if-eqz v0, :cond_0

    .line 47
    new-instance v1, Ljava/net/InetSocketAddress;

    iget-object v0, p0, Lcom/twitter/network/k;->c:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/network/k;->b:I

    invoke-direct {v1, v0, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 48
    new-instance v0, Ljava/net/Proxy;

    sget-object v2, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    invoke-direct {v0, v2, v1}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    .line 50
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    goto :goto_0
.end method
