.class public abstract Lcom/twitter/network/HttpOperation;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/network/HttpOperation$RequestMethod;,
        Lcom/twitter/network/HttpOperation$Protocol;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field protected static final a:Lcom/twitter/network/j;

.field private static final g:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field protected final b:Lcqt;

.field protected final c:Lcom/twitter/network/HttpOperation$RequestMethod;

.field protected final d:Ljava/net/URI;

.field protected final e:Lcom/twitter/network/j;

.field protected f:J

.field private h:Lcom/twitter/network/apache/e;

.field private final i:Lcom/twitter/network/d;

.field private final j:Lcom/twitter/network/d;

.field private final k:Lcom/twitter/util/connectivity/b;

.field private l:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private m:Z

.field private final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final o:Lcom/twitter/network/l;

.field private volatile p:Z

.field private volatile q:Z

.field private r:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private s:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field

.field private t:I

.field private u:Ljava/util/zip/Inflater;

.field private v:[Lcom/twitter/network/HttpOperation$Protocol;

.field private w:J

.field private x:Z

.field private y:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 132
    new-instance v0, Lcom/twitter/network/b;

    invoke-direct {v0}, Lcom/twitter/network/b;-><init>()V

    sput-object v0, Lcom/twitter/network/HttpOperation;->a:Lcom/twitter/network/j;

    .line 136
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/twitter/network/HttpOperation;->g:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method protected constructor <init>(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)V
    .locals 7

    .prologue
    .line 277
    .line 280
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v4

    .line 281
    invoke-static {}, Lcom/twitter/network/d;->a()Lcom/twitter/network/d;

    move-result-object v5

    .line 282
    invoke-static {}, Lcom/twitter/util/connectivity/b;->a()Lcom/twitter/util/connectivity/b;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 277
    invoke-direct/range {v0 .. v6}, Lcom/twitter/network/HttpOperation;-><init>(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;Lcqt;Lcom/twitter/network/d;Lcom/twitter/util/connectivity/b;)V

    .line 283
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;Lcqt;Lcom/twitter/network/d;Lcom/twitter/util/connectivity/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    .line 258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-wide v4, p0, Lcom/twitter/network/HttpOperation;->f:J

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/network/HttpOperation;->h:Lcom/twitter/network/apache/e;

    .line 159
    new-instance v0, Lcom/twitter/network/d;

    invoke-direct {v0}, Lcom/twitter/network/d;-><init>()V

    iput-object v0, p0, Lcom/twitter/network/HttpOperation;->i:Lcom/twitter/network/d;

    .line 171
    iput-boolean v2, p0, Lcom/twitter/network/HttpOperation;->m:Z

    .line 174
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/network/HttpOperation;->n:Ljava/util/Map;

    .line 175
    new-instance v0, Lcom/twitter/network/l;

    invoke-direct {v0}, Lcom/twitter/network/l;-><init>()V

    iput-object v0, p0, Lcom/twitter/network/HttpOperation;->o:Lcom/twitter/network/l;

    .line 181
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/network/HttpOperation;->t:I

    .line 183
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/twitter/network/HttpOperation$Protocol;

    sget-object v1, Lcom/twitter/network/HttpOperation$Protocol;->c:Lcom/twitter/network/HttpOperation$Protocol;

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/twitter/network/HttpOperation;->v:[Lcom/twitter/network/HttpOperation$Protocol;

    .line 185
    iput-boolean v2, p0, Lcom/twitter/network/HttpOperation;->x:Z

    .line 186
    iput-wide v4, p0, Lcom/twitter/network/HttpOperation;->y:J

    .line 259
    iput-object p4, p0, Lcom/twitter/network/HttpOperation;->b:Lcqt;

    .line 260
    iput-object p5, p0, Lcom/twitter/network/HttpOperation;->j:Lcom/twitter/network/d;

    .line 261
    iput-object p6, p0, Lcom/twitter/network/HttpOperation;->k:Lcom/twitter/util/connectivity/b;

    .line 263
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->b:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/network/HttpOperation;->w:J

    .line 264
    iput-object p1, p0, Lcom/twitter/network/HttpOperation;->c:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 265
    iput-object p2, p0, Lcom/twitter/network/HttpOperation;->d:Ljava/net/URI;

    .line 266
    sget-object v0, Lcom/twitter/network/HttpOperation;->a:Lcom/twitter/network/j;

    invoke-static {p3, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/network/j;

    iput-object v0, p0, Lcom/twitter/network/HttpOperation;->e:Lcom/twitter/network/j;

    .line 267
    return-void
.end method

.method private static a(Lcqd;)J
    .locals 2

    .prologue
    .line 682
    if-eqz p0, :cond_0

    .line 683
    invoke-virtual {p0}, Lcqd;->a()J

    move-result-wide v0

    .line 685
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 626
    if-eqz p1, :cond_0

    .line 627
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/Object;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 631
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(IJ)V
    .locals 2

    .prologue
    .line 883
    iget-wide v0, p0, Lcom/twitter/network/HttpOperation;->w:J

    cmp-long v0, p2, v0

    if-gez v0, :cond_0

    .line 889
    :goto_0
    return-void

    .line 886
    :cond_0
    iget-wide v0, p0, Lcom/twitter/network/HttpOperation;->w:J

    sub-long v0, p2, v0

    long-to-int v0, v0

    .line 887
    iget-object v1, p0, Lcom/twitter/network/HttpOperation;->o:Lcom/twitter/network/l;

    iget-object v1, v1, Lcom/twitter/network/l;->t:[I

    aput v0, v1, p1

    .line 888
    iput-wide p2, p0, Lcom/twitter/network/HttpOperation;->w:J

    goto :goto_0
.end method

.method private a(Lcom/twitter/network/l;Ljava/lang/Object;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/l;",
            "TS;)V"
        }
    .end annotation

    .prologue
    .line 707
    iget-object v5, p1, Lcom/twitter/network/l;->m:Ljava/lang/String;

    .line 708
    iget-object v4, p1, Lcom/twitter/network/l;->l:Ljava/lang/String;

    .line 709
    iget v3, p1, Lcom/twitter/network/l;->k:I

    .line 710
    const/4 v2, 0x0

    .line 711
    const/4 v6, 0x0

    .line 712
    const/4 v0, 0x0

    .line 713
    const/4 v8, 0x0

    .line 716
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/twitter/network/HttpOperation;->d(Ljava/lang/Object;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 717
    if-eqz v1, :cond_b

    .line 718
    :try_start_1
    new-instance v7, Lcqd;

    invoke-direct {v7, v1}, Lcqd;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 720
    :try_start_2
    iget-object v2, p0, Lcom/twitter/network/HttpOperation;->l:Lcom/twitter/util/q;

    if-eqz v2, :cond_2

    new-instance v6, Lcqf;

    iget-object v2, p0, Lcom/twitter/network/HttpOperation;->l:Lcom/twitter/util/q;

    invoke-direct {v6, v7, v3, v2}, Lcqf;-><init>(Ljava/io/InputStream;ILcom/twitter/util/q;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 724
    :goto_0
    if-eqz v4, :cond_3

    :try_start_3
    const-string/jumbo v1, "application/octet-stream"

    .line 725
    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "video/mp4"

    .line 726
    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "binary/octet-stream"

    .line 727
    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "application/zip"

    .line 728
    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "text/event-stream"

    .line 729
    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 730
    invoke-static {v4}, Lcqe;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "image/"

    .line 731
    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 732
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unsupported content type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 801
    :catch_0
    move-exception v1

    move v2, v8

    move-object v3, v0

    move-object v0, v1

    move-object v1, v7

    .line 805
    :goto_1
    if-nez v2, :cond_0

    .line 806
    if-eqz v1, :cond_7

    :try_start_4
    invoke-virtual {v1}, Lcqd;->d()Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, p1, Lcom/twitter/network/l;->d:Z

    .line 807
    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->b(Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_6

    .line 810
    :cond_0
    invoke-static {v3}, Lcpw;->a(Ljava/io/Closeable;)V

    .line 811
    invoke-static {v6}, Lcpw;->a(Ljava/io/Closeable;)V

    .line 813
    :goto_3
    invoke-static {v1}, Lcom/twitter/network/HttpOperation;->a(Lcqd;)J

    move-result-wide v2

    iput-wide v2, p1, Lcom/twitter/network/l;->f:J

    .line 814
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcqd;->c()J

    move-result-wide v2

    :goto_4
    iput-wide v2, p1, Lcom/twitter/network/l;->i:J

    .line 815
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcqd;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 816
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->o:Lcom/twitter/network/l;

    invoke-virtual {v1}, Lcqd;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/twitter/network/l;->r:J

    .line 818
    :cond_1
    return-void

    .line 720
    :cond_2
    :try_start_5
    new-instance v6, Ljava/io/BufferedInputStream;

    const/16 v2, 0x1000

    invoke-direct {v6, v7, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto/16 :goto_0

    .line 801
    :catch_1
    move-exception v2

    move-object v3, v0

    move-object v6, v1

    move-object v0, v2

    move-object v1, v7

    move v2, v8

    goto :goto_1

    .line 735
    :cond_3
    if-eqz v5, :cond_a

    .line 736
    :try_start_6
    const-string/jumbo v1, "gzip"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 743
    const/16 v1, 0xe

    new-array v1, v1, [B

    .line 744
    invoke-virtual {v6, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 746
    new-instance v2, Ljava/io/SequenceInputStream;

    new-instance v9, Ljava/io/ByteArrayInputStream;

    const/4 v10, 0x0

    invoke-direct {v9, v1, v10, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-direct {v2, v9, v6}, Ljava/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 750
    :try_start_7
    invoke-virtual {p0, v2}, Lcom/twitter/network/HttpOperation;->a(Ljava/io/InputStream;)Ljava/util/zip/GZIPInputStream;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result-object v6

    .line 751
    const/4 v3, -0x1

    move-object v2, v6

    .line 758
    :goto_5
    :try_start_8
    iget v1, p1, Lcom/twitter/network/l;->a:I

    const/16 v6, 0x190

    if-lt v1, v6, :cond_9

    iget v1, p1, Lcom/twitter/network/l;->a:I

    const/16 v6, 0x258

    if-ge v1, v6, :cond_9

    iget-boolean v1, p0, Lcom/twitter/network/HttpOperation;->m:Z

    if-eqz v1, :cond_9

    .line 761
    invoke-static {v4}, Lcqe;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 762
    new-instance v6, Ljava/io/BufferedInputStream;

    invoke-direct {v6, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 763
    const/16 v1, 0x1000

    :try_start_9
    invoke-virtual {v6, v1}, Ljava/io/InputStream;->mark(I)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 765
    const/16 v1, 0x1000

    :try_start_a
    new-array v1, v1, [B

    .line 766
    const/4 v2, 0x0

    const/16 v9, 0x1000

    invoke-virtual {v6, v1, v2, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 767
    new-instance v9, Ljava/lang/String;

    const/4 v10, 0x0

    const-string/jumbo v11, "UTF-8"

    .line 771
    invoke-static {v11}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v11

    invoke-direct {v9, v1, v10, v2, v11}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v9, p1, Lcom/twitter/network/l;->s:Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 775
    :try_start_b
    invoke-virtual {v6}, Ljava/io/InputStream;->reset()V

    .line 780
    :goto_6
    new-instance v2, Lcpx;

    invoke-direct {v2, v6}, Lcpx;-><init>(Ljava/io/InputStream;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 781
    :try_start_c
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->e:Lcom/twitter/network/j;

    iget v1, p1, Lcom/twitter/network/l;->a:I

    invoke-interface/range {v0 .. v5}, Lcom/twitter/network/j;->a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V

    .line 784
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->b:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    .line 785
    invoke-static {v2}, Lcom/twitter/network/HttpOperation;->a(Lcpx;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 786
    invoke-static {v2}, Lcpw;->a(Ljava/io/InputStream;)I

    .line 788
    :cond_4
    iget-object v3, p0, Lcom/twitter/network/HttpOperation;->b:Lcqt;

    invoke-interface {v3}, Lcqt;->b()J

    move-result-wide v4

    sub-long v0, v4, v0

    iput-wide v0, p1, Lcom/twitter/network/l;->h:J

    .line 795
    invoke-virtual {v7}, Lcqd;->e()Ljava/io/IOException;

    move-result-object v0

    .line 796
    if-eqz v0, :cond_6

    .line 797
    iget-boolean v1, p0, Lcom/twitter/network/HttpOperation;->x:Z
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    .line 798
    :try_start_d
    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    .line 801
    :catch_2
    move-exception v0

    move-object v3, v2

    move v2, v1

    move-object v1, v7

    goto/16 :goto_1

    .line 752
    :cond_5
    :try_start_e
    const-string/jumbo v1, "deflate"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 753
    new-instance v2, Ljava/util/zip/InflaterInputStream;

    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->t()Ljava/util/zip/Inflater;

    move-result-object v1

    invoke-direct {v2, v6, v1}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V

    .line 754
    const/4 v3, -0x1

    goto/16 :goto_5

    .line 772
    :catch_3
    move-exception v1

    .line 775
    invoke-virtual {v6}, Ljava/io/InputStream;->reset()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_6

    .line 810
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_7
    invoke-static {v2}, Lcpw;->a(Ljava/io/Closeable;)V

    .line 811
    invoke-static {v6}, Lcpw;->a(Ljava/io/Closeable;)V

    throw v0

    .line 775
    :catchall_1
    move-exception v1

    :try_start_f
    invoke-virtual {v6}, Ljava/io/InputStream;->reset()V

    throw v1
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_0
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :cond_6
    move-object v0, v6

    move-object v1, v7

    .line 810
    :goto_8
    invoke-static {v2}, Lcpw;->a(Ljava/io/Closeable;)V

    .line 811
    invoke-static {v0}, Lcpw;->a(Ljava/io/Closeable;)V

    goto/16 :goto_3

    .line 806
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 814
    :cond_8
    const-wide/16 v2, 0x0

    goto/16 :goto_4

    .line 810
    :catchall_2
    move-exception v2

    move-object v6, v1

    move-object v12, v0

    move-object v0, v2

    move-object v2, v12

    goto :goto_7

    :catchall_3
    move-exception v1

    move-object v6, v2

    move-object v2, v0

    move-object v0, v1

    goto :goto_7

    :catchall_4
    move-exception v1

    move-object v6, v2

    move-object v2, v0

    move-object v0, v1

    goto :goto_7

    :catchall_5
    move-exception v0

    goto :goto_7

    :catchall_6
    move-exception v0

    move-object v2, v3

    goto :goto_7

    .line 801
    :catch_4
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    move-object v1, v2

    move v2, v8

    goto/16 :goto_1

    :catch_5
    move-exception v3

    move-object v6, v1

    move-object v1, v2

    move v2, v8

    move-object v12, v3

    move-object v3, v0

    move-object v0, v12

    goto/16 :goto_1

    :catch_6
    move-exception v1

    move-object v3, v0

    move-object v6, v2

    move-object v0, v1

    move v2, v8

    move-object v1, v7

    goto/16 :goto_1

    :catch_7
    move-exception v1

    move-object v3, v0

    move-object v6, v2

    move-object v0, v1

    move v2, v8

    move-object v1, v7

    goto/16 :goto_1

    :catch_8
    move-exception v0

    move-object v3, v2

    move-object v1, v7

    move v2, v8

    goto/16 :goto_1

    :cond_9
    move-object v6, v2

    goto/16 :goto_6

    :cond_a
    move-object v2, v6

    goto/16 :goto_5

    :cond_b
    move-object v12, v0

    move-object v0, v1

    move-object v1, v2

    move-object v2, v12

    goto :goto_8
.end method

.method private static a(Lcpx;)Z
    .locals 1

    .prologue
    .line 590
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcpx;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 879
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->b:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/network/HttpOperation;->a(IJ)V

    .line 880
    return-void
.end method

.method private c(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 874
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->i:Lcom/twitter/network/d;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/network/d;->a(Lcom/twitter/network/HttpOperation;Ljava/lang/Exception;)V

    .line 875
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->j:Lcom/twitter/network/d;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/network/d;->a(Lcom/twitter/network/HttpOperation;Ljava/lang/Exception;)V

    .line 876
    return-void
.end method

.method private l(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 574
    iget v0, p0, Lcom/twitter/network/HttpOperation;->t:I

    if-ltz v0, :cond_0

    .line 575
    iget v0, p0, Lcom/twitter/network/HttpOperation;->t:I

    invoke-virtual {p0, p1, v0}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/Object;I)V

    .line 579
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->e()Lcom/twitter/network/apache/e;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/Object;Lcom/twitter/network/apache/e;)V

    .line 582
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 583
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 584
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, p1, v2, v1}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 587
    :cond_2
    return-void
.end method

.method private m(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .prologue
    .line 833
    if-nez p1, :cond_1

    .line 842
    :cond_0
    :goto_0
    return-void

    .line 836
    :cond_1
    invoke-direct {p0, p1}, Lcom/twitter/network/HttpOperation;->n(Ljava/lang/Object;)Ljava/util/Date;

    move-result-object v0

    .line 837
    if-eqz v0, :cond_0

    .line 838
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 839
    iget-object v2, p0, Lcom/twitter/network/HttpOperation;->b:Lcqt;

    invoke-interface {v2}, Lcqt;->a()J

    move-result-wide v2

    .line 840
    sget-object v4, Lcom/twitter/network/HttpOperation;->g:Ljava/util/concurrent/atomic/AtomicLong;

    sub-long/2addr v0, v2

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    goto :goto_0
.end method

.method private n(Ljava/lang/Object;)Ljava/util/Date;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)",
            "Ljava/util/Date;"
        }
    .end annotation

    .prologue
    .line 846
    const-string/jumbo v0, "Date"

    invoke-direct {p0, p1, v0}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 847
    if-eqz v0, :cond_0

    .line 849
    :try_start_0
    sget-object v1, Lcom/twitter/util/i;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 853
    :goto_0
    return-object v0

    .line 850
    :catch_0
    move-exception v0

    .line 853
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()V
    .locals 2

    .prologue
    .line 858
    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 859
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Request not yet complete for this HttpOperation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 861
    :cond_0
    return-void
.end method

.method private w()V
    .locals 1

    .prologue
    .line 864
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->i:Lcom/twitter/network/d;

    invoke-virtual {v0, p0}, Lcom/twitter/network/d;->a(Lcom/twitter/network/HttpOperation;)V

    .line 865
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->j:Lcom/twitter/network/d;

    invoke-virtual {v0, p0}, Lcom/twitter/network/d;->a(Lcom/twitter/network/HttpOperation;)V

    .line 866
    return-void
.end method

.method private x()V
    .locals 1

    .prologue
    .line 869
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->i:Lcom/twitter/network/d;

    invoke-virtual {v0, p0}, Lcom/twitter/network/d;->b(Lcom/twitter/network/HttpOperation;)V

    .line 870
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->j:Lcom/twitter/network/d;

    invoke-virtual {v0, p0}, Lcom/twitter/network/d;->b(Lcom/twitter/network/HttpOperation;)V

    .line 871
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/network/apache/e;)Lcom/twitter/network/HttpOperation;
    .locals 0

    .prologue
    .line 442
    iput-object p1, p0, Lcom/twitter/network/HttpOperation;->h:Lcom/twitter/network/apache/e;

    .line 443
    return-object p0
.end method

.method public a(Lcom/twitter/network/c;)Lcom/twitter/network/HttpOperation;
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->i:Lcom/twitter/network/d;

    invoke-virtual {v0, p1}, Lcom/twitter/network/d;->a(Lcom/twitter/network/c;)V

    .line 484
    return-object p0
.end method

.method public a(Ljava/lang/Exception;)Lcom/twitter/network/HttpOperation;
    .locals 2

    .prologue
    .line 559
    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->b()V

    .line 560
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->o:Lcom/twitter/network/l;

    .line 561
    const/4 v1, 0x0

    iput v1, v0, Lcom/twitter/network/l;->a:I

    .line 562
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/network/l;->b:Ljava/lang/String;

    .line 563
    iput-object p1, v0, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    .line 564
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 316
    :goto_0
    return-object p0

    .line 312
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 313
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    iget-object v1, p0, Lcom/twitter/network/HttpOperation;->n:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract a(Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method protected abstract a(Ljava/lang/Object;Ljava/lang/String;I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method protected a(Ljava/io/InputStream;)Ljava/util/zip/GZIPInputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 825
    iget-boolean v0, p0, Lcom/twitter/network/HttpOperation;->x:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcqg;

    invoke-direct {v0, p1}, Lcqg;-><init>(Ljava/io/InputStream;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, p1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 526
    iput p1, p0, Lcom/twitter/network/HttpOperation;->t:I

    .line 527
    return-void
.end method

.method public a(Lcom/twitter/util/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 699
    iput-object p1, p0, Lcom/twitter/network/HttpOperation;->l:Lcom/twitter/util/q;

    .line 700
    return-void
.end method

.method protected abstract a(Ljava/lang/Object;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;I)V"
        }
    .end annotation
.end method

.method protected abstract a(Ljava/lang/Object;Lcom/twitter/network/apache/e;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/twitter/network/apache/e;",
            ")V"
        }
    .end annotation
.end method

.method protected abstract a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 291
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->n:Ljava/util/Map;

    const-string/jumbo v1, "Host"

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    :goto_0
    return-void

    .line 294
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 295
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    iget-object v1, p0, Lcom/twitter/network/HttpOperation;->n:Ljava/util/Map;

    const-string/jumbo v2, "Host"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 648
    iput-boolean p1, p0, Lcom/twitter/network/HttpOperation;->m:Z

    .line 649
    return-void
.end method

.method public final a([Lcom/twitter/network/HttpOperation$Protocol;)V
    .locals 4

    .prologue
    .line 656
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 657
    invoke-virtual {p0, v2}, Lcom/twitter/network/HttpOperation;->a(Lcom/twitter/network/HttpOperation$Protocol;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 658
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is not supported by this HttpOperation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 656
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 661
    :cond_1
    iput-object p1, p0, Lcom/twitter/network/HttpOperation;->v:[Lcom/twitter/network/HttpOperation$Protocol;

    .line 662
    invoke-virtual {p0, p1}, Lcom/twitter/network/HttpOperation;->b([Lcom/twitter/network/HttpOperation$Protocol;)V

    .line 663
    return-void
.end method

.method protected a(Lcom/twitter/network/HttpOperation$Protocol;)Z
    .locals 1

    .prologue
    .line 652
    sget-object v0, Lcom/twitter/network/HttpOperation$Protocol;->c:Lcom/twitter/network/HttpOperation$Protocol;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b(Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 606
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 609
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->r:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->r:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->e(Ljava/lang/Object;)V

    .line 240
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/network/HttpOperation;->p:Z

    .line 241
    return-void
.end method

.method protected b(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 636
    invoke-direct {p0, p1}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/Exception;)V

    .line 637
    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->b()V

    .line 638
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->o:Lcom/twitter/network/l;

    .line 639
    const/4 v1, 0x0

    iput v1, v0, Lcom/twitter/network/l;->a:I

    .line 640
    iput-object p1, v0, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    .line 641
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 922
    iput-boolean p1, p0, Lcom/twitter/network/HttpOperation;->x:Z

    .line 923
    return-void
.end method

.method protected b([Lcom/twitter/network/HttpOperation$Protocol;)V
    .locals 0

    .prologue
    .line 670
    return-void
.end method

.method protected abstract c(Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)I"
        }
    .end annotation
.end method

.method public final c()Lcom/twitter/network/HttpOperation;
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v10, 0x0

    .line 324
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->k:Lcom/twitter/util/connectivity/b;

    invoke-virtual {v0}, Lcom/twitter/util/connectivity/b;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 325
    new-instance v0, Ljava/net/NoRouteToHostException;

    const-string/jumbo v1, "Wifi only mode is enabled."

    invoke-direct {v0, v1}, Ljava/net/NoRouteToHostException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->b(Ljava/lang/Exception;)V

    .line 412
    :cond_0
    :goto_0
    return-object p0

    .line 330
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/network/HttpOperation;->q:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 333
    invoke-direct {p0, v10}, Lcom/twitter/network/HttpOperation;->b(I)V

    .line 334
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->b:Lcqt;

    invoke-interface {v0}, Lcqt;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/network/HttpOperation;->y:J

    .line 335
    iput-boolean v3, p0, Lcom/twitter/network/HttpOperation;->q:Z

    .line 336
    invoke-direct {p0}, Lcom/twitter/network/HttpOperation;->x()V

    .line 339
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->b:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v4

    .line 341
    iget-object v6, p0, Lcom/twitter/network/HttpOperation;->o:Lcom/twitter/network/l;

    .line 342
    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/twitter/network/l;->p:Ljava/lang/String;

    .line 343
    const/4 v1, 0x0

    .line 344
    const/4 v0, 0x1

    .line 346
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->a()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/network/HttpOperation;->r:Ljava/lang/Object;

    .line 349
    iget-object v2, p0, Lcom/twitter/network/HttpOperation;->r:Ljava/lang/Object;

    invoke-direct {p0, v2}, Lcom/twitter/network/HttpOperation;->l(Ljava/lang/Object;)V

    .line 350
    invoke-direct {p0, v0}, Lcom/twitter/network/HttpOperation;->b(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    const/4 v2, 0x2

    .line 353
    :try_start_1
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->r:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->f(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 354
    :try_start_2
    invoke-direct {p0, v2}, Lcom/twitter/network/HttpOperation;->b(I)V

    .line 355
    const/4 v2, 0x3

    .line 362
    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->h(Ljava/lang/Object;)I

    move-result v1

    iput v1, v6, Lcom/twitter/network/l;->a:I

    .line 363
    invoke-direct {p0, v2}, Lcom/twitter/network/HttpOperation;->b(I)V

    .line 364
    iget-object v1, p0, Lcom/twitter/network/HttpOperation;->b:Lcqt;

    invoke-interface {v1}, Lcqt;->b()J

    move-result-wide v8

    iput-wide v8, v6, Lcom/twitter/network/l;->r:J

    .line 366
    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->i(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/twitter/network/l;->b:Ljava/lang/String;

    .line 367
    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/Object;)I

    move-result v1

    iput v1, v6, Lcom/twitter/network/l;->k:I

    .line 368
    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/twitter/network/l;->l:Ljava/lang/String;

    .line 369
    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/twitter/network/l;->m:Ljava/lang/String;

    .line 370
    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->k(Ljava/lang/Object;)Lcom/twitter/network/HttpOperation$Protocol;

    move-result-object v1

    iput-object v1, v6, Lcom/twitter/network/l;->o:Lcom/twitter/network/HttpOperation$Protocol;

    .line 371
    const-string/jumbo v1, "x-served-by"

    const/4 v7, 0x0

    invoke-virtual {p0, v0, v1, v7}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/Object;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/twitter/network/l;->q:Ljava/lang/String;

    .line 372
    iget-object v1, v6, Lcom/twitter/network/l;->q:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 373
    const-string/jumbo v1, "server"

    const/4 v7, 0x0

    invoke-virtual {p0, v0, v1, v7}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/Object;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/twitter/network/l;->q:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 381
    :cond_2
    :goto_1
    invoke-direct {p0, v0}, Lcom/twitter/network/HttpOperation;->m(Ljava/lang/Object;)V

    .line 383
    iget-object v1, p0, Lcom/twitter/network/HttpOperation;->b:Lcqt;

    invoke-interface {v1}, Lcqt;->b()J

    move-result-wide v8

    sub-long v4, v8, v4

    iput-wide v4, v6, Lcom/twitter/network/l;->g:J

    .line 384
    if-eqz v0, :cond_3

    iget-object v1, v6, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    if-nez v1, :cond_3

    .line 385
    invoke-direct {p0, v6, v0}, Lcom/twitter/network/HttpOperation;->a(Lcom/twitter/network/l;Ljava/lang/Object;)V

    .line 390
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/twitter/network/HttpOperation;->o:Lcom/twitter/network/l;

    iget-wide v4, v2, Lcom/twitter/network/l;->r:J

    invoke-direct {p0, v1, v4, v5}, Lcom/twitter/network/HttpOperation;->a(IJ)V

    .line 391
    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/twitter/network/HttpOperation;->b(I)V

    .line 393
    :cond_3
    iget-wide v4, v6, Lcom/twitter/network/l;->g:J

    iget-wide v8, v6, Lcom/twitter/network/l;->f:J

    add-long/2addr v4, v8

    iget-wide v8, v6, Lcom/twitter/network/l;->h:J

    add-long/2addr v4, v8

    iput-wide v4, v6, Lcom/twitter/network/l;->e:J

    .line 396
    iput-boolean v3, p0, Lcom/twitter/network/HttpOperation;->p:Z

    .line 397
    iput-boolean v10, p0, Lcom/twitter/network/HttpOperation;->q:Z

    .line 398
    iput-object v0, p0, Lcom/twitter/network/HttpOperation;->s:Ljava/lang/Object;

    .line 400
    invoke-virtual {v6}, Lcom/twitter/network/l;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 401
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->e:Lcom/twitter/network/j;

    invoke-interface {v0, v6}, Lcom/twitter/network/j;->a(Lcom/twitter/network/l;)V

    .line 405
    :cond_4
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->r:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 406
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->r:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->j(Ljava/lang/Object;)V

    .line 409
    :cond_5
    invoke-direct {p0}, Lcom/twitter/network/HttpOperation;->w()V

    .line 410
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/twitter/network/HttpOperation;->b(I)V

    goto/16 :goto_0

    .line 375
    :catch_0
    move-exception v0

    move v2, v3

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    .line 376
    :goto_2
    invoke-direct {p0, v2}, Lcom/twitter/network/HttpOperation;->b(I)V

    .line 377
    instance-of v2, v1, Ljava/io/IOException;

    iput-boolean v2, v6, Lcom/twitter/network/l;->d:Z

    .line 378
    invoke-virtual {p0, v1}, Lcom/twitter/network/HttpOperation;->b(Ljava/lang/Exception;)V

    goto :goto_1

    .line 375
    :catch_1
    move-exception v0

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_2
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 620
    invoke-direct {p0}, Lcom/twitter/network/HttpOperation;->v()V

    .line 621
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->s:Ljava/lang/Object;

    invoke-direct {p0, v0, p1}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract d(Ljava/lang/Object;)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)",
            "Ljava/io/InputStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract d()Ljava/lang/String;
.end method

.method public final e()Lcom/twitter/network/apache/e;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->h:Lcom/twitter/network/apache/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->c:Lcom/twitter/network/HttpOperation$RequestMethod;

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation$RequestMethod;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->h:Lcom/twitter/network/apache/e;

    .line 451
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract e(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation
.end method

.method public f()J
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->h:Lcom/twitter/network/apache/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->h:Lcom/twitter/network/apache/e;

    invoke-interface {v0}, Lcom/twitter/network/apache/e;->a()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method protected abstract f(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)TS;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public g()J
    .locals 2

    .prologue
    .line 472
    iget-wide v0, p0, Lcom/twitter/network/HttpOperation;->f:J

    return-wide v0
.end method

.method protected abstract g(Ljava/lang/Object;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end method

.method protected abstract h(Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public h()Lcom/twitter/network/HttpOperation$RequestMethod;
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->c:Lcom/twitter/network/HttpOperation$RequestMethod;

    return-object v0
.end method

.method protected abstract i(Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public i()Ljava/net/URI;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->d:Ljava/net/URI;

    return-object v0
.end method

.method public j()Ljava/net/URI;
    .locals 2

    .prologue
    .line 511
    const-string/jumbo v0, "Host"

    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 512
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 514
    :try_start_0
    iget-object v1, p0, Lcom/twitter/network/HttpOperation;->d:Ljava/net/URI;

    invoke-static {v1, v0}, Lcom/twitter/util/j;->a(Ljava/net/URI;Ljava/lang/String;)Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 518
    :goto_0
    return-object v0

    .line 515
    :catch_0
    move-exception v0

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->d:Ljava/net/URI;

    goto :goto_0
.end method

.method protected j(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 424
    return-void
.end method

.method protected abstract k(Ljava/lang/Object;)Lcom/twitter/network/HttpOperation$Protocol;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)",
            "Lcom/twitter/network/HttpOperation$Protocol;"
        }
    .end annotation
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 540
    invoke-direct {p0}, Lcom/twitter/network/HttpOperation;->v()V

    .line 541
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->o:Lcom/twitter/network/l;

    iget v0, v0, Lcom/twitter/network/l;->a:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 548
    invoke-direct {p0}, Lcom/twitter/network/HttpOperation;->v()V

    .line 549
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->o:Lcom/twitter/network/l;

    invoke-virtual {v0}, Lcom/twitter/network/l;->a()Z

    move-result v0

    return v0
.end method

.method public m()Lcom/twitter/network/l;
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->o:Lcom/twitter/network/l;

    return-object v0
.end method

.method public n()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 595
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 597
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 598
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 601
    :cond_0
    return-object v1
.end method

.method public o()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 614
    invoke-direct {p0}, Lcom/twitter/network/HttpOperation;->v()V

    .line 615
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->s:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->s:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->g(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 644
    iget-boolean v0, p0, Lcom/twitter/network/HttpOperation;->p:Z

    return v0
.end method

.method public q()[Lcom/twitter/network/HttpOperation$Protocol;
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->v:[Lcom/twitter/network/HttpOperation$Protocol;

    return-object v0
.end method

.method public r()Lcom/twitter/network/j;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->e:Lcom/twitter/network/j;

    return-object v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 895
    sget-object v0, Lcom/twitter/network/HttpOperation;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized t()Ljava/util/zip/Inflater;
    .locals 2

    .prologue
    .line 911
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->u:Ljava/util/zip/Inflater;

    if-nez v0, :cond_0

    .line 912
    new-instance v0, Ljava/util/zip/Inflater;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/zip/Inflater;-><init>(Z)V

    iput-object v0, p0, Lcom/twitter/network/HttpOperation;->u:Ljava/util/zip/Inflater;

    .line 914
    :cond_0
    iget-object v0, p0, Lcom/twitter/network/HttpOperation;->u:Ljava/util/zip/Inflater;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 911
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 942
    iget-wide v0, p0, Lcom/twitter/network/HttpOperation;->y:J

    return-wide v0
.end method
