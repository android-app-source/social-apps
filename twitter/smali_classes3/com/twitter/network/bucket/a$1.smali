.class Lcom/twitter/network/bucket/a$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/network/bucket/a;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/twitter/network/bucket/a;


# direct methods
.method constructor <init>(Lcom/twitter/network/bucket/a;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/twitter/network/bucket/a$1;->b:Lcom/twitter/network/bucket/a;

    iput-object p2, p0, Lcom/twitter/network/bucket/a$1;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 70
    const-string/jumbo v0, "data_usage_meter"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    const-string/jumbo v0, "data_usage_meter"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/twitter/network/bucket/a$1;->b:Lcom/twitter/network/bucket/a;

    invoke-virtual {v0}, Lcom/twitter/network/bucket/a;->a()V

    .line 73
    iget-object v0, p0, Lcom/twitter/network/bucket/a$1;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/network/bucket/OverlayService;->a(Landroid/content/Context;)V

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/twitter/network/bucket/a$1;->b:Lcom/twitter/network/bucket/a;

    invoke-static {v0}, Lcom/twitter/network/bucket/a;->a(Lcom/twitter/network/bucket/a;)V

    .line 76
    iget-object v0, p0, Lcom/twitter/network/bucket/a$1;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/network/bucket/OverlayService;->b(Landroid/content/Context;)V

    goto :goto_0
.end method
