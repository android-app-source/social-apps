.class Lcom/twitter/network/bucket/OverlayService$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/network/bucket/OverlayService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/WindowManager$LayoutParams;

.field final synthetic b:Landroid/view/WindowManager;

.field final synthetic c:Lcom/twitter/network/bucket/OverlayService;

.field private d:I

.field private e:I

.field private f:F

.field private g:F


# direct methods
.method constructor <init>(Lcom/twitter/network/bucket/OverlayService;Landroid/view/WindowManager$LayoutParams;Landroid/view/WindowManager;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/twitter/network/bucket/OverlayService$2;->c:Lcom/twitter/network/bucket/OverlayService;

    iput-object p2, p0, Lcom/twitter/network/bucket/OverlayService$2;->a:Landroid/view/WindowManager$LayoutParams;

    iput-object p3, p0, Lcom/twitter/network/bucket/OverlayService$2;->b:Landroid/view/WindowManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 95
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 116
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 97
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService$2;->a:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v0, p0, Lcom/twitter/network/bucket/OverlayService$2;->d:I

    .line 98
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService$2;->a:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v0, p0, Lcom/twitter/network/bucket/OverlayService$2;->e:I

    .line 99
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lcom/twitter/network/bucket/OverlayService$2;->f:F

    .line 100
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/twitter/network/bucket/OverlayService$2;->g:F

    goto :goto_0

    .line 107
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService$2;->a:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/twitter/network/bucket/OverlayService$2;->d:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iget v3, p0, Lcom/twitter/network/bucket/OverlayService$2;->f:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 108
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService$2;->a:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/twitter/network/bucket/OverlayService$2;->e:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget v3, p0, Lcom/twitter/network/bucket/OverlayService$2;->g:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 109
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService$2;->b:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/twitter/network/bucket/OverlayService$2;->c:Lcom/twitter/network/bucket/OverlayService;

    invoke-static {v1}, Lcom/twitter/network/bucket/OverlayService;->a(Lcom/twitter/network/bucket/OverlayService;)Landroid/widget/FrameLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/network/bucket/OverlayService$2;->a:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
