.class public Lcom/twitter/network/bucket/OverlayService;
.super Landroid/app/Service;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/network/bucket/OverlayService$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/os/IBinder;

.field private b:Landroid/widget/FrameLayout;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Lrx/j;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 36
    new-instance v0, Lcom/twitter/network/bucket/OverlayService$a;

    invoke-direct {v0, p0}, Lcom/twitter/network/bucket/OverlayService$a;-><init>(Lcom/twitter/network/bucket/OverlayService;)V

    iput-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->a:Landroid/os/IBinder;

    return-void
.end method

.method static synthetic a(Lcom/twitter/network/bucket/OverlayService;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    invoke-static {p0}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    :goto_0
    return-void

    .line 51
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/network/bucket/OverlayService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/network/bucket/OverlayService;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/network/bucket/OverlayService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 56
    return-void
.end method

.method static synthetic c(Lcom/twitter/network/bucket/OverlayService;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/network/bucket/OverlayService;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/network/bucket/OverlayService;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/network/bucket/OverlayService;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->g:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->a:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const/4 v1, -0x2

    .line 60
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 62
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d2

    const/16 v4, 0x8

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 68
    const v1, 0x800033

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 69
    const-string/jumbo v1, "window"

    invoke-virtual {p0, v1}, Lcom/twitter/network/bucket/OverlayService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 71
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    .line 72
    iget-object v2, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04009a

    iget-object v4, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 75
    iget-object v2, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    new-instance v3, Lcom/twitter/network/bucket/OverlayService$1;

    invoke-direct {v3, p0}, Lcom/twitter/network/bucket/OverlayService$1;-><init>(Lcom/twitter/network/bucket/OverlayService;)V

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 87
    iget-object v2, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    new-instance v3, Lcom/twitter/network/bucket/OverlayService$2;

    invoke-direct {v3, p0, v0, v1}, Lcom/twitter/network/bucket/OverlayService$2;-><init>(Lcom/twitter/network/bucket/OverlayService;Landroid/view/WindowManager$LayoutParams;Landroid/view/WindowManager;)V

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 120
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    const v1, 0x7f1302ce

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->c:Landroid/widget/TextView;

    .line 121
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    const v1, 0x7f1302cf

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->d:Landroid/widget/TextView;

    .line 122
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    const v1, 0x7f1302d0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->g:Landroid/widget/TextView;

    .line 123
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    const v1, 0x7f1302d1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->e:Landroid/widget/TextView;

    .line 124
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    const v1, 0x7f1302d2

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->f:Landroid/widget/TextView;

    .line 126
    invoke-static {p0}, Lcom/twitter/network/bucket/a;->a(Landroid/content/Context;)Lcom/twitter/network/bucket/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/bucket/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    invoke-static {p0}, Lcom/twitter/network/bucket/a;->a(Landroid/content/Context;)Lcom/twitter/network/bucket/a;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lcom/twitter/network/bucket/a;->c()Lrx/c;

    move-result-object v0

    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/network/bucket/OverlayService$3;

    invoke-direct {v1, p0}, Lcom/twitter/network/bucket/OverlayService$3;-><init>(Lcom/twitter/network/bucket/OverlayService;)V

    .line 129
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/b;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->h:Lrx/j;

    .line 148
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 152
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 153
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 155
    const-string/jumbo v0, "window"

    invoke-virtual {p0, v0}, Lcom/twitter/network/bucket/OverlayService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 156
    iget-object v1, p0, Lcom/twitter/network/bucket/OverlayService;->b:Landroid/widget/FrameLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->h:Lrx/j;

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/twitter/network/bucket/OverlayService;->h:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 161
    iput-object v2, p0, Lcom/twitter/network/bucket/OverlayService;->h:Lrx/j;

    .line 163
    :cond_1
    return-void
.end method
