.class public Lcom/twitter/network/bucket/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/network/bucket/a$a;,
        Lcom/twitter/network/bucket/a$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/library/network/DataUsageEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/network/bucket/a;


# instance fields
.field private final b:Lcom/twitter/network/bucket/a$b;

.field private final c:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Lcom/twitter/network/bucket/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcqt;

.field private e:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private f:Ljava/util/Timer;

.field private final g:I

.field private h:Z

.field private i:Z

.field private j:J

.field private k:J

.field private l:J

.field private m:J

.field private n:J

.field private o:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lcom/twitter/network/bucket/a$b;

    invoke-direct {v0, p0}, Lcom/twitter/network/bucket/a$b;-><init>(Lcom/twitter/network/bucket/a;)V

    iput-object v0, p0, Lcom/twitter/network/bucket/a;->b:Lcom/twitter/network/bucket/a$b;

    .line 56
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/network/bucket/a;->d:Lcqt;

    .line 57
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    iput v0, p0, Lcom/twitter/network/bucket/a;->g:I

    .line 58
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/network/bucket/a;->c:Lrx/subjects/PublishSubject;

    .line 59
    iget-object v0, p0, Lcom/twitter/network/bucket/a;->c:Lrx/subjects/PublishSubject;

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->n()Lrx/c;

    .line 61
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_0

    .line 63
    const-string/jumbo v1, "data_usage_meter"

    const/4 v2, 0x0

    .line 64
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/network/bucket/a;->i:Z

    .line 66
    new-instance v1, Lcom/twitter/network/bucket/a$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/network/bucket/a$1;-><init>(Lcom/twitter/network/bucket/a;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/network/bucket/a;->e:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 82
    iget-object v1, p0, Lcom/twitter/network/bucket/a;->e:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 84
    :cond_0
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/network/bucket/a;
    .locals 2

    .prologue
    .line 88
    const-class v1, Lcom/twitter/network/bucket/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/network/bucket/a;->a:Lcom/twitter/network/bucket/a;

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Lcom/twitter/network/bucket/a;

    invoke-direct {v0, p0}, Lcom/twitter/network/bucket/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/network/bucket/a;->a:Lcom/twitter/network/bucket/a;

    .line 90
    const-class v0, Lcom/twitter/network/bucket/a;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 92
    :cond_0
    sget-object v0, Lcom/twitter/network/bucket/a;->a:Lcom/twitter/network/bucket/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(J)Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/high16 v10, 0x4090000000000000L    # 1024.0

    .line 101
    .line 102
    const-wide/16 v0, 0x400

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " B"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 107
    :goto_0
    return-object v0

    .line 105
    :cond_0
    long-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    invoke-static {v10, v11}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    .line 106
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "KMGTPE"

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "i"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 107
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v3, "%.1f %sB"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    long-to-double v6, p0

    int-to-double v8, v0

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/network/bucket/a;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/network/bucket/a;->e()V

    return-void
.end method

.method public static b(J)Ljava/lang/String;
    .locals 14

    .prologue
    const-wide/16 v12, 0x1

    const-wide/16 v10, 0x0

    .line 112
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, p1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    .line 113
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, p0, p1}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v2

    .line 114
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, p0, p1}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v4

    .line 115
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    cmp-long v7, v4, v10

    if-lez v7, :cond_0

    .line 118
    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "d "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_0
    cmp-long v7, v2, v10

    if-lez v7, :cond_1

    .line 122
    sget-object v7, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v12, v13}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v8

    cmp-long v7, v2, v8

    if-ltz v7, :cond_2

    .line 123
    sget-object v7, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v4, v5}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v4

    sub-long v4, v2, v4

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 127
    :goto_0
    const-string/jumbo v4, "h "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    :cond_1
    sget-object v4, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v12, v13}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v4

    cmp-long v4, v0, v4

    if-ltz v4, :cond_3

    .line 131
    sget-object v4, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 135
    :goto_1
    const-string/jumbo v0, "m"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 125
    :cond_2
    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 133
    :cond_3
    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method static synthetic b(Lcom/twitter/network/bucket/a;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/network/bucket/a;->f()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/network/bucket/a;)Lcqt;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/network/bucket/a;->d:Lcqt;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/network/bucket/a;)J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/twitter/network/bucket/a;->j:J

    return-wide v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 190
    monitor-enter p0

    .line 191
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/twitter/network/bucket/a;->h:Z

    .line 192
    invoke-static {}, Lcom/twitter/library/network/b;->a()Lcom/twitter/library/network/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/network/b;->b(Lcom/twitter/util/q;)Z

    .line 193
    iget-object v0, p0, Lcom/twitter/network/bucket/a;->f:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/twitter/network/bucket/a;->f:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 196
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/network/bucket/a;->i:Z

    .line 197
    monitor-exit p0

    .line 198
    return-void

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 201
    monitor-enter p0

    .line 202
    :try_start_0
    iget v0, p0, Lcom/twitter/network/bucket/a;->g:I

    invoke-static {v0}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v0

    iget v2, p0, Lcom/twitter/network/bucket/a;->g:I

    .line 203
    invoke-static {v2}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/twitter/network/bucket/a;->k:J

    sub-long/2addr v0, v2

    .line 204
    iget-wide v2, p0, Lcom/twitter/network/bucket/a;->l:J

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    iget-boolean v2, p0, Lcom/twitter/network/bucket/a;->o:Z

    if-eqz v2, :cond_1

    .line 205
    :cond_0
    iput-wide v0, p0, Lcom/twitter/network/bucket/a;->l:J

    .line 206
    invoke-direct {p0}, Lcom/twitter/network/bucket/a;->g()V

    .line 208
    :cond_1
    monitor-exit p0

    .line 209
    return-void

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private g()V
    .locals 10

    .prologue
    .line 212
    iget-object v9, p0, Lcom/twitter/network/bucket/a;->c:Lrx/subjects/PublishSubject;

    new-instance v0, Lcom/twitter/network/bucket/a$a;

    iget-wide v2, p0, Lcom/twitter/network/bucket/a;->l:J

    iget-wide v4, p0, Lcom/twitter/network/bucket/a;->m:J

    iget-wide v6, p0, Lcom/twitter/network/bucket/a;->n:J

    const/4 v8, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/twitter/network/bucket/a$a;-><init>(Lcom/twitter/network/bucket/a;JJJLcom/twitter/network/bucket/a$1;)V

    invoke-virtual {v9, v0}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/network/bucket/a;->o:Z

    .line 214
    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    .line 140
    monitor-enter p0

    .line 141
    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/network/bucket/a;->h:Z

    if-nez v0, :cond_0

    .line 142
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/network/bucket/a;->b:Lcom/twitter/network/bucket/a$b;

    invoke-virtual {v0, v1}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 143
    invoke-virtual {p0}, Lcom/twitter/network/bucket/a;->d()V

    .line 144
    invoke-static {}, Lcom/twitter/library/network/b;->a()Lcom/twitter/library/network/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/network/b;->a(Lcom/twitter/util/q;)Z

    .line 145
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/twitter/network/bucket/a;->f:Ljava/util/Timer;

    .line 146
    iget-object v0, p0, Lcom/twitter/network/bucket/a;->f:Ljava/util/Timer;

    new-instance v1, Lcom/twitter/network/bucket/a$2;

    invoke-direct {v1, p0}, Lcom/twitter/network/bucket/a$2;-><init>(Lcom/twitter/network/bucket/a;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    .line 151
    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 146
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/network/bucket/a;->h:Z

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/network/bucket/a;->i:Z

    .line 155
    :cond_0
    monitor-exit p0

    .line 156
    return-void

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 159
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/network/bucket/a;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/network/bucket/a$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lcom/twitter/network/bucket/a;->c:Lrx/subjects/PublishSubject;

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->f()Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 179
    monitor-enter p0

    .line 180
    :try_start_0
    iget-object v0, p0, Lcom/twitter/network/bucket/a;->d:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/network/bucket/a;->j:J

    .line 181
    iget v0, p0, Lcom/twitter/network/bucket/a;->g:I

    invoke-static {v0}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v0

    iget v2, p0, Lcom/twitter/network/bucket/a;->g:I

    invoke-static {v2}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/network/bucket/a;->k:J

    .line 182
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/network/bucket/a;->l:J

    .line 183
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/network/bucket/a;->m:J

    .line 184
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/network/bucket/a;->n:J

    .line 185
    invoke-direct {p0}, Lcom/twitter/network/bucket/a;->g()V

    .line 186
    monitor-exit p0

    .line 187
    return-void

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onEvent(Lcom/twitter/library/network/DataUsageEvent;)V
    .locals 6

    .prologue
    .line 169
    if-eqz p1, :cond_0

    .line 170
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/network/bucket/a;->o:Z

    .line 171
    iget-wide v0, p0, Lcom/twitter/network/bucket/a;->m:J

    iget-wide v2, p1, Lcom/twitter/library/network/DataUsageEvent;->f:J

    iget-wide v4, p1, Lcom/twitter/library/network/DataUsageEvent;->g:J

    add-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/network/bucket/a;->m:J

    .line 172
    iget-object v0, p1, Lcom/twitter/library/network/DataUsageEvent;->a:Lcom/twitter/library/network/DataUsageEvent$Type;

    sget-object v1, Lcom/twitter/library/network/DataUsageEvent$Type;->d:Lcom/twitter/library/network/DataUsageEvent$Type;

    if-ne v0, v1, :cond_0

    .line 173
    iget-wide v0, p0, Lcom/twitter/network/bucket/a;->n:J

    iget-wide v2, p1, Lcom/twitter/library/network/DataUsageEvent;->f:J

    iget-wide v4, p1, Lcom/twitter/library/network/DataUsageEvent;->g:J

    add-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/network/bucket/a;->n:J

    .line 176
    :cond_0
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    check-cast p1, Lcom/twitter/library/network/DataUsageEvent;

    invoke-virtual {p0, p1}, Lcom/twitter/network/bucket/a;->onEvent(Lcom/twitter/library/network/DataUsageEvent;)V

    return-void
.end method
