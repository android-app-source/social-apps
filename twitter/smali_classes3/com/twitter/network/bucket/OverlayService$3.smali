.class Lcom/twitter/network/bucket/OverlayService$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/network/bucket/OverlayService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/b",
        "<",
        "Lcom/twitter/network/bucket/a$a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/network/bucket/OverlayService;


# direct methods
.method constructor <init>(Lcom/twitter/network/bucket/OverlayService;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/twitter/network/bucket/OverlayService$3;->a:Lcom/twitter/network/bucket/OverlayService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/network/bucket/a$a;)V
    .locals 10

    .prologue
    .line 132
    iget-wide v0, p1, Lcom/twitter/network/bucket/a$a;->a:J

    .line 133
    iget-object v2, p0, Lcom/twitter/network/bucket/OverlayService$3;->a:Lcom/twitter/network/bucket/OverlayService;

    invoke-static {v2}, Lcom/twitter/network/bucket/OverlayService;->b(Lcom/twitter/network/bucket/OverlayService;)Landroid/widget/TextView;

    move-result-object v2

    invoke-static {v0, v1}, Lcom/twitter/network/bucket/a;->b(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v2, p0, Lcom/twitter/network/bucket/OverlayService$3;->a:Lcom/twitter/network/bucket/OverlayService;

    invoke-static {v2}, Lcom/twitter/network/bucket/OverlayService;->c(Lcom/twitter/network/bucket/OverlayService;)Landroid/widget/TextView;

    move-result-object v2

    iget-wide v4, p1, Lcom/twitter/network/bucket/a$a;->c:J

    invoke-static {v4, v5}, Lcom/twitter/network/bucket/a;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v2, p0, Lcom/twitter/network/bucket/OverlayService$3;->a:Lcom/twitter/network/bucket/OverlayService;

    invoke-static {v2}, Lcom/twitter/network/bucket/OverlayService;->d(Lcom/twitter/network/bucket/OverlayService;)Landroid/widget/TextView;

    move-result-object v2

    iget-wide v4, p1, Lcom/twitter/network/bucket/a$a;->d:J

    .line 136
    invoke-static {v4, v5}, Lcom/twitter/network/bucket/a;->a(J)Ljava/lang/String;

    move-result-object v3

    .line 135
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v2, p0, Lcom/twitter/network/bucket/OverlayService$3;->a:Lcom/twitter/network/bucket/OverlayService;

    invoke-static {v2}, Lcom/twitter/network/bucket/OverlayService;->e(Lcom/twitter/network/bucket/OverlayService;)Landroid/widget/TextView;

    move-result-object v2

    iget-wide v4, p1, Lcom/twitter/network/bucket/a$a;->b:J

    invoke-static {v4, v5}, Lcom/twitter/network/bucket/a;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v3, "%s /m"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p1, Lcom/twitter/network/bucket/a$a;->c:J

    div-long v0, v6, v0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1

    .line 142
    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    mul-long/2addr v0, v6

    .line 140
    invoke-static {v0, v1}, Lcom/twitter/network/bucket/a;->a(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 139
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 144
    :goto_0
    iget-object v1, p0, Lcom/twitter/network/bucket/OverlayService$3;->a:Lcom/twitter/network/bucket/OverlayService;

    invoke-static {v1}, Lcom/twitter/network/bucket/OverlayService;->f(Lcom/twitter/network/bucket/OverlayService;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    return-void

    .line 139
    :cond_0
    const-string/jumbo v0, "n/a"

    goto :goto_0
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 129
    check-cast p1, Lcom/twitter/network/bucket/a$a;

    invoke-virtual {p0, p1}, Lcom/twitter/network/bucket/OverlayService$3;->a(Lcom/twitter/network/bucket/a$a;)V

    return-void
.end method
