.class public final enum Lcom/twitter/network/HttpOperation$Protocol;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/network/HttpOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Protocol"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/network/HttpOperation$Protocol;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/network/HttpOperation$Protocol;

.field public static final enum b:Lcom/twitter/network/HttpOperation$Protocol;

.field public static final enum c:Lcom/twitter/network/HttpOperation$Protocol;

.field public static final enum d:Lcom/twitter/network/HttpOperation$Protocol;

.field public static final enum e:Lcom/twitter/network/HttpOperation$Protocol;

.field public static final enum f:Lcom/twitter/network/HttpOperation$Protocol;

.field public static final enum g:Lcom/twitter/network/HttpOperation$Protocol;

.field private static final synthetic h:[Lcom/twitter/network/HttpOperation$Protocol;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 70
    new-instance v0, Lcom/twitter/network/HttpOperation$Protocol;

    const-string/jumbo v1, "UNDEFINED"

    const-string/jumbo v2, ""

    invoke-direct {v0, v1, v4, v2}, Lcom/twitter/network/HttpOperation$Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/network/HttpOperation$Protocol;->a:Lcom/twitter/network/HttpOperation$Protocol;

    .line 71
    new-instance v0, Lcom/twitter/network/HttpOperation$Protocol;

    const-string/jumbo v1, "HTTP_1_0"

    const-string/jumbo v2, "http/1.0"

    invoke-direct {v0, v1, v5, v2}, Lcom/twitter/network/HttpOperation$Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/network/HttpOperation$Protocol;->b:Lcom/twitter/network/HttpOperation$Protocol;

    .line 72
    new-instance v0, Lcom/twitter/network/HttpOperation$Protocol;

    const-string/jumbo v1, "HTTP_1_1"

    const-string/jumbo v2, "http/1.1"

    invoke-direct {v0, v1, v6, v2}, Lcom/twitter/network/HttpOperation$Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/network/HttpOperation$Protocol;->c:Lcom/twitter/network/HttpOperation$Protocol;

    .line 73
    new-instance v0, Lcom/twitter/network/HttpOperation$Protocol;

    const-string/jumbo v1, "H2"

    const-string/jumbo v2, "h2"

    invoke-direct {v0, v1, v7, v2}, Lcom/twitter/network/HttpOperation$Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/network/HttpOperation$Protocol;->d:Lcom/twitter/network/HttpOperation$Protocol;

    .line 74
    new-instance v0, Lcom/twitter/network/HttpOperation$Protocol;

    const-string/jumbo v1, "SPDY_2"

    const-string/jumbo v2, "spdy/2"

    invoke-direct {v0, v1, v8, v2}, Lcom/twitter/network/HttpOperation$Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/network/HttpOperation$Protocol;->e:Lcom/twitter/network/HttpOperation$Protocol;

    .line 75
    new-instance v0, Lcom/twitter/network/HttpOperation$Protocol;

    const-string/jumbo v1, "SPDY_3"

    const/4 v2, 0x5

    const-string/jumbo v3, "spdy/3"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/network/HttpOperation$Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/network/HttpOperation$Protocol;->f:Lcom/twitter/network/HttpOperation$Protocol;

    .line 76
    new-instance v0, Lcom/twitter/network/HttpOperation$Protocol;

    const-string/jumbo v1, "SPDY_3_1"

    const/4 v2, 0x6

    const-string/jumbo v3, "spdy/3.1"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/network/HttpOperation$Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/network/HttpOperation$Protocol;->g:Lcom/twitter/network/HttpOperation$Protocol;

    .line 69
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/twitter/network/HttpOperation$Protocol;

    sget-object v1, Lcom/twitter/network/HttpOperation$Protocol;->a:Lcom/twitter/network/HttpOperation$Protocol;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/network/HttpOperation$Protocol;->b:Lcom/twitter/network/HttpOperation$Protocol;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/network/HttpOperation$Protocol;->c:Lcom/twitter/network/HttpOperation$Protocol;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/network/HttpOperation$Protocol;->d:Lcom/twitter/network/HttpOperation$Protocol;

    aput-object v1, v0, v7

    sget-object v1, Lcom/twitter/network/HttpOperation$Protocol;->e:Lcom/twitter/network/HttpOperation$Protocol;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/twitter/network/HttpOperation$Protocol;->f:Lcom/twitter/network/HttpOperation$Protocol;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/twitter/network/HttpOperation$Protocol;->g:Lcom/twitter/network/HttpOperation$Protocol;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/network/HttpOperation$Protocol;->h:[Lcom/twitter/network/HttpOperation$Protocol;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 82
    iput-object p3, p0, Lcom/twitter/network/HttpOperation$Protocol;->mName:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/twitter/network/HttpOperation$Protocol;
    .locals 3

    .prologue
    .line 95
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "[-/.]"

    const-string/jumbo v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/network/HttpOperation$Protocol;->valueOf(Ljava/lang/String;)Lcom/twitter/network/HttpOperation$Protocol;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    sget-object v0, Lcom/twitter/network/HttpOperation$Protocol;->a:Lcom/twitter/network/HttpOperation$Protocol;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/network/HttpOperation$Protocol;
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/twitter/network/HttpOperation$Protocol;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/network/HttpOperation$Protocol;

    return-object v0
.end method

.method public static values()[Lcom/twitter/network/HttpOperation$Protocol;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/twitter/network/HttpOperation$Protocol;->h:[Lcom/twitter/network/HttpOperation$Protocol;

    invoke-virtual {v0}, [Lcom/twitter/network/HttpOperation$Protocol;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/network/HttpOperation$Protocol;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/network/HttpOperation$Protocol;->mName:Ljava/lang/String;

    return-object v0
.end method
