.class public Lcom/twitter/network/a;
.super Ljava/io/FilterOutputStream;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/network/s;

.field private final b:J

.field private final c:J

.field private d:J

.field private e:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;JLcom/twitter/network/s;)V
    .locals 4

    .prologue
    .line 30
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 31
    iput-object p4, p0, Lcom/twitter/network/a;->a:Lcom/twitter/network/s;

    .line 32
    const-wide/16 v0, 0x2

    mul-long/2addr v0, p2

    iput-wide v0, p0, Lcom/twitter/network/a;->b:J

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/network/a;->d:J

    .line 34
    iget-wide v0, p0, Lcom/twitter/network/a;->b:J

    const-wide/16 v2, 0x5

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/network/a;->c:J

    .line 35
    iget-wide v0, p0, Lcom/twitter/network/a;->c:J

    iput-wide v0, p0, Lcom/twitter/network/a;->e:J

    .line 36
    return-void
.end method


# virtual methods
.method public write(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-super {p0, p1}, Ljava/io/FilterOutputStream;->write(I)V

    .line 54
    iget-wide v0, p0, Lcom/twitter/network/a;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/network/a;->d:J

    .line 55
    iget-wide v0, p0, Lcom/twitter/network/a;->d:J

    iget-wide v2, p0, Lcom/twitter/network/a;->e:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 56
    invoke-super {p0}, Ljava/io/FilterOutputStream;->flush()V

    .line 57
    iget-object v0, p0, Lcom/twitter/network/a;->a:Lcom/twitter/network/s;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/twitter/network/a;->a:Lcom/twitter/network/s;

    iget-wide v2, p0, Lcom/twitter/network/a;->d:J

    iget-wide v4, p0, Lcom/twitter/network/a;->b:J

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/twitter/network/s;->a(JJ)V

    .line 60
    :cond_0
    iget-wide v0, p0, Lcom/twitter/network/a;->e:J

    iget-wide v2, p0, Lcom/twitter/network/a;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/network/a;->e:J

    .line 62
    :cond_1
    return-void
.end method

.method public write([BII)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterOutputStream;->write([BII)V

    .line 41
    iget-wide v0, p0, Lcom/twitter/network/a;->d:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/network/a;->d:J

    .line 42
    iget-wide v0, p0, Lcom/twitter/network/a;->d:J

    iget-wide v2, p0, Lcom/twitter/network/a;->e:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 43
    invoke-super {p0}, Ljava/io/FilterOutputStream;->flush()V

    .line 44
    iget-object v0, p0, Lcom/twitter/network/a;->a:Lcom/twitter/network/s;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/twitter/network/a;->a:Lcom/twitter/network/s;

    iget-wide v2, p0, Lcom/twitter/network/a;->d:J

    iget-wide v4, p0, Lcom/twitter/network/a;->b:J

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/twitter/network/s;->a(JJ)V

    .line 47
    :cond_0
    iget-wide v0, p0, Lcom/twitter/network/a;->e:J

    iget-wide v2, p0, Lcom/twitter/network/a;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/network/a;->e:J

    .line 49
    :cond_1
    return-void
.end method
