.class public abstract Lcom/twitter/library/service/s;
.super Lcom/twitter/async/service/AsyncOperation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Landroid/os/Bundle;",
        "Lcom/twitter/library/service/u;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/client/p;

.field private b:I

.field private c:Z

.field private g:Ljava/lang/String;

.field private h:Lcom/twitter/library/service/v;

.field public final o:Landroid/os/Bundle;

.field protected final p:Landroid/content/Context;

.field protected final q:Lcom/twitter/library/network/ab;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 82
    invoke-direct {p0, p2}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;)V

    .line 70
    const-string/jumbo v0, "Proxying app visibility from request."

    iput-object v0, p0, Lcom/twitter/library/service/s;->g:Ljava/lang/String;

    .line 83
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/service/s;->o:Landroid/os/Bundle;

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/s;->p:Landroid/content/Context;

    .line 85
    invoke-static {p1}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/s;->q:Lcom/twitter/library/network/ab;

    .line 86
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/s;->a:Lcom/twitter/library/client/p;

    .line 87
    new-instance v0, Lcom/twitter/library/service/f;

    invoke-direct {v0}, Lcom/twitter/library/service/f;-><init>()V

    new-instance v1, Lcom/twitter/library/service/l;

    invoke-direct {v1, v2}, Lcom/twitter/library/service/l;-><init>(I)V

    .line 88
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/service/g;

    invoke-direct {v1, p1}, Lcom/twitter/library/service/g;-><init>(Landroid/content/Context;)V

    .line 89
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v0

    .line 87
    invoke-virtual {p0, v0}, Lcom/twitter/library/service/s;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 90
    invoke-virtual {p0, v2}, Lcom/twitter/library/service/s;->b(I)Lcom/twitter/async/service/AsyncOperation;

    .line 92
    instance-of v0, p0, Lcom/twitter/library/resilient/b;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/twitter/library/service/s;->p:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/resilient/a;->a(Landroid/content/Context;)Lcom/twitter/library/resilient/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/service/s;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    .line 95
    :cond_0
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 110
    if-eqz p3, :cond_0

    .line 111
    new-instance v0, Lcom/twitter/library/service/v;

    invoke-direct {v0, p3}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    iput-object v0, p0, Lcom/twitter/library/service/s;->h:Lcom/twitter/library/service/v;

    .line 113
    :cond_0
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 131
    iput-object p3, p0, Lcom/twitter/library/service/s;->h:Lcom/twitter/library/service/v;

    .line 132
    return-void
.end method

.method static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 408
    invoke-static {p0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 411
    :cond_0
    return-void
.end method


# virtual methods
.method public final L()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/twitter/library/service/s;->b:I

    return v0
.end method

.method public final M()Lcom/twitter/library/service/v;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/twitter/library/service/s;->h:Lcom/twitter/library/service/v;

    return-object v0
.end method

.method public final N()Z
    .locals 1

    .prologue
    .line 327
    iget-boolean v0, p0, Lcom/twitter/library/service/s;->c:Z

    return v0
.end method

.method public O()Lcom/twitter/library/service/u;
    .locals 2

    .prologue
    .line 435
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0}, Lcpd;->b()Lcpa;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/service/s$1;

    invoke-direct {v1, p0}, Lcom/twitter/library/service/s$1;-><init>(Lcom/twitter/library/service/s;)V

    invoke-virtual {v0, v1}, Lcpa;->a(Lcom/twitter/util/concurrent/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 451
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    return-object v0
.end method

.method protected P()Lcom/twitter/library/service/u;
    .locals 2

    .prologue
    .line 468
    new-instance v0, Lcom/twitter/library/service/u;

    invoke-direct {v0}, Lcom/twitter/library/service/u;-><init>()V

    .line 469
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/u;->a(Z)V

    .line 470
    return-object v0
.end method

.method protected final Q()Lcom/twitter/library/service/u;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 476
    invoke-virtual {p0}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 477
    if-eqz v0, :cond_0

    .line 478
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v1

    invoke-virtual {v1}, Lcpd;->b()Lcpa;

    move-result-object v1

    const-string/jumbo v2, "request_user_id"

    iget-wide v4, v0, Lcom/twitter/library/service/v;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    :cond_0
    new-instance v0, Lcom/twitter/library/service/u;

    invoke-direct {v0}, Lcom/twitter/library/service/u;-><init>()V

    .line 482
    invoke-virtual {p0, v0}, Lcom/twitter/library/service/s;->b(Lcom/twitter/library/service/u;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 483
    invoke-virtual {p0, v0}, Lcom/twitter/library/service/s;->a_(Lcom/twitter/library/service/u;)V

    .line 485
    :cond_1
    return-object v0
.end method

.method public final R()Lcom/twitter/library/provider/t;
    .locals 2

    .prologue
    .line 494
    iget-object v0, p0, Lcom/twitter/library/service/s;->h:Lcom/twitter/library/service/v;

    if-nez v0, :cond_0

    .line 495
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Session is null when accessing DB. Did you forget to pass in a Session to the constructor?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 498
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/service/s;->h:Lcom/twitter/library/service/v;

    iget-wide v0, v0, Lcom/twitter/library/service/v;->c:J

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    return-object v0
.end method

.method protected final S()Laut;
    .locals 2

    .prologue
    .line 505
    new-instance v0, Laut;

    iget-object v1, p0, Lcom/twitter/library/service/s;->p:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Laut;-><init>(Landroid/content/ContentResolver;)V

    return-object v0
.end method

.method public T()Z
    .locals 2

    .prologue
    .line 531
    invoke-virtual {p0}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    .line 532
    if-eqz v0, :cond_0

    .line 533
    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 532
    :goto_0
    return v0

    .line 533
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/twitter/library/network/k;
    .locals 2

    .prologue
    .line 340
    new-instance v0, Lcom/twitter/library/network/k;

    invoke-direct {v0, p1, p2}, Lcom/twitter/library/network/k;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 341
    invoke-virtual {p0}, Lcom/twitter/library/service/s;->N()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342
    iget-object v1, p0, Lcom/twitter/library/service/s;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Ljava/lang/String;)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 344
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/service/s;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->b(Ljava/lang/String;)Lcom/twitter/library/network/k;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Lcom/twitter/library/service/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/library/service/s;",
            ">(",
            "Landroid/os/Bundle;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 312
    iget-object v0, p0, Lcom/twitter/library/service/s;->o:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 313
    return-object p0
.end method

.method public final a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/library/service/s;",
            ">(",
            "Lcom/twitter/library/service/s;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 422
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p1, Lcom/twitter/library/service/s;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/library/service/s;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    .line 427
    :goto_0
    return-object p0

    .line 425
    :cond_0
    iget-object v0, p1, Lcom/twitter/library/service/s;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/library/service/s;->m(Ljava/lang/String;)Lcom/twitter/library/service/s;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)Lcom/twitter/library/service/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/library/service/s;",
            ">(",
            "Ljava/lang/String;",
            "I)TT;"
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/twitter/library/service/s;->o:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 153
    return-object p0
.end method

.method public final a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/library/service/s;",
            ">(",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lcom/twitter/library/service/s;->o:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 201
    return-object p0
.end method

.method public final a(Ljava/lang/String;Z)Lcom/twitter/library/service/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/library/service/s;",
            ">(",
            "Ljava/lang/String;",
            "Z)TT;"
        }
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lcom/twitter/library/service/s;->o:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 177
    return-object p0
.end method

.method public final a(Ljava/lang/String;[I)Lcom/twitter/library/service/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/library/service/s;",
            ">(",
            "Ljava/lang/String;",
            "[I)TT;"
        }
    .end annotation

    .prologue
    .line 236
    iget-object v0, p0, Lcom/twitter/library/service/s;->o:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 237
    return-object p0
.end method

.method protected abstract a_(Lcom/twitter/library/service/u;)V
.end method

.method public final b(Ljava/lang/String;J)Lcom/twitter/library/service/s;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/library/service/s;",
            ">(",
            "Ljava/lang/String;",
            "J)TT;"
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lcom/twitter/library/service/s;->o:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 171
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/library/service/s;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/twitter/library/service/s;->o:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    return-object p0
.end method

.method protected final b(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncOperation",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 460
    iget-object v0, p0, Lcom/twitter/library/service/s;->a:Lcom/twitter/library/client/p;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 461
    return-void
.end method

.method protected b(Lcom/twitter/library/service/u;)Z
    .locals 1

    .prologue
    .line 519
    const/4 v0, 0x1

    return v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/twitter/library/service/s;->P()Lcom/twitter/library/service/u;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/twitter/library/service/s;->Q()Lcom/twitter/library/service/u;

    move-result-object v0

    return-object v0
.end method

.method public final g(I)Lcom/twitter/library/service/s;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/library/service/s;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 140
    iput p1, p0, Lcom/twitter/library/service/s;->b:I

    .line 141
    return-object p0
.end method

.method public final l(Ljava/lang/String;)Lcom/twitter/library/service/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/library/service/s;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 368
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    const-string/jumbo v0, "Cannot force polling without a reason"

    invoke-static {p1, v0}, Lcom/twitter/library/service/s;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    :cond_0
    iput-object p1, p0, Lcom/twitter/library/service/s;->g:Ljava/lang/String;

    .line 372
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/service/s;->c:Z

    .line 373
    return-object p0
.end method

.method public final m(Ljava/lang/String;)Lcom/twitter/library/service/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/library/service/s;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 396
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    const-string/jumbo v0, "Cannot force non-polling without a reason"

    invoke-static {p1, v0}, Lcom/twitter/library/service/s;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    :cond_0
    iput-object p1, p0, Lcom/twitter/library/service/s;->g:Ljava/lang/String;

    .line 400
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/service/s;->c:Z

    .line 401
    return-object p0
.end method
