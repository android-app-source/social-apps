.class public Lcom/twitter/library/service/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/async/service/e;


# instance fields
.field public final a:Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;


# direct methods
.method public constructor <init>(Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/twitter/library/service/m;->a:Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;

    .line 16
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Network condition changed from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/service/m;->a:Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;

    iget-object v1, v1, Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;->a:Lcom/twitter/library/network/forecaster/NetworkQuality;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/service/m;->a:Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;

    iget-object v1, v1, Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;->b:Lcom/twitter/library/network/forecaster/NetworkQuality;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
