.class public final Lcom/twitter/library/service/f;
.super Lcom/twitter/async/service/k;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/async/service/k",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/async/service/k",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private b:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/twitter/async/service/k;-><init>()V

    .line 20
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/service/f;->a:Ljava/util/List;

    .line 21
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/k",
            "<TT;>;)",
            "Lcom/twitter/library/service/f",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/library/service/f;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    return-object p0
.end method

.method public a(Lcom/twitter/async/service/e;Lcom/twitter/async/service/j;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/e;",
            "Lcom/twitter/async/service/j",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/library/service/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/k;

    .line 42
    invoke-virtual {v0, p1, p2}, Lcom/twitter/async/service/k;->a(Lcom/twitter/async/service/e;Lcom/twitter/async/service/j;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 43
    invoke-virtual {v0, p2}, Lcom/twitter/async/service/k;->b(Lcom/twitter/async/service/j;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/service/f;->b:J

    .line 44
    const/4 v0, 0x1

    .line 47
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/async/service/j;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/library/service/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/k;

    .line 31
    invoke-virtual {v0, p1}, Lcom/twitter/async/service/k;->a(Lcom/twitter/async/service/j;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 32
    invoke-virtual {v0, p1}, Lcom/twitter/async/service/k;->b(Lcom/twitter/async/service/j;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/service/f;->b:J

    .line 33
    const/4 v0, 0x1

    .line 36
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/twitter/async/service/j;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<TT;>;)J"
        }
    .end annotation

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/twitter/library/service/f;->b:J

    return-wide v0
.end method
