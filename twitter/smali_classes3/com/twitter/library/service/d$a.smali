.class public final Lcom/twitter/library/service/d$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/service/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/network/apache/f;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private d:Lcom/twitter/network/apache/e;

.field private e:Ljava/lang/String;

.field private final f:Ljava/lang/StringBuilder;

.field private g:Lcom/twitter/network/HttpOperation$RequestMethod;

.field private h:Ljava/lang/String;

.field private i:Lcom/twitter/library/network/a;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/service/d$a;->a:Ljava/util/ArrayList;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/service/d$a;->b:Ljava/util/ArrayList;

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/service/d$a;->d:Lcom/twitter/network/apache/e;

    .line 112
    const-string/jumbo v0, "/"

    iput-object v0, p0, Lcom/twitter/library/service/d$a;->k:Ljava/lang/String;

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/service/d$a;->l:Z

    .line 117
    const-string/jumbo v0, "https"

    iput-object v0, p0, Lcom/twitter/library/service/d$a;->j:Ljava/lang/String;

    .line 118
    const-string/jumbo v0, "api.twitter.com"

    iput-object v0, p0, Lcom/twitter/library/service/d$a;->h:Ljava/lang/String;

    .line 119
    const-string/jumbo v0, "1.1"

    iput-object v0, p0, Lcom/twitter/library/service/d$a;->e:Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/service/d$a;->f:Ljava/lang/StringBuilder;

    .line 121
    sget-object v0, Lcom/twitter/network/HttpOperation$RequestMethod;->a:Lcom/twitter/network/HttpOperation$RequestMethod;

    iput-object v0, p0, Lcom/twitter/library/service/d$a;->g:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 122
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/d$a;->c:Landroid/content/Context;

    .line 123
    return-void
.end method

.method private a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/network/apache/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 379
    const/16 v0, 0x3f

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 380
    iget-object v0, p0, Lcom/twitter/library/service/d$a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 381
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 382
    iget-object v0, p0, Lcom/twitter/library/service/d$a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/network/apache/f;

    .line 383
    invoke-interface {v0}, Lcom/twitter/network/apache/f;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/network/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x3d

    .line 384
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 385
    invoke-interface {v0}, Lcom/twitter/network/apache/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/network/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    add-int/lit8 v0, v2, -0x1

    if-ge v1, v0, :cond_0

    .line 387
    const/16 v0, 0x26

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 381
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 390
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/network/a;)Lcom/twitter/library/service/d$a;
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/twitter/library/service/d$a;->i:Lcom/twitter/library/network/a;

    .line 183
    return-object p0
.end method

.method public a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;
    .locals 0

    .prologue
    .line 168
    if-eqz p1, :cond_0

    .line 169
    iput-object p1, p0, Lcom/twitter/library/service/d$a;->g:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 171
    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/network/apache/e;)Lcom/twitter/library/service/d$a;
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/twitter/library/service/d$a;->d:Lcom/twitter/network/apache/e;

    .line 274
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/twitter/library/service/d$a;->e:Ljava/lang/String;

    .line 148
    return-object p0
.end method

.method public a(Ljava/lang/String;D)Lcom/twitter/library/service/d$a;
    .locals 2

    .prologue
    .line 236
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 237
    return-object p0
.end method

.method public a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;
    .locals 2

    .prologue
    .line 200
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 201
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/twitter/library/service/d$a;->a:Ljava/util/ArrayList;

    new-instance v1, Lcom/twitter/network/apache/message/BasicNameValuePair;

    invoke-direct {v1, p1, p2}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/util/Collection;)Lcom/twitter/library/service/d$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<*>;)",
            "Lcom/twitter/library/service/d$a;"
        }
    .end annotation

    .prologue
    .line 223
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    .line 224
    new-array v1, v0, [Ljava/lang/String;

    .line 225
    const/4 v0, 0x0

    .line 226
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 227
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 228
    add-int/lit8 v0, v0, 0x1

    .line 229
    goto :goto_0

    .line 230
    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 231
    return-object p0
.end method

.method public a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;
    .locals 1

    .prologue
    .line 194
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 195
    return-object p0
.end method

.method public a(Ljava/lang/String;[J)Lcom/twitter/library/service/d$a;
    .locals 6

    .prologue
    .line 212
    array-length v1, p2

    .line 213
    new-array v2, v1, [Ljava/lang/String;

    .line 214
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 215
    aget-wide v4, p2, v0

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 214
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 217
    :cond_0
    invoke-virtual {p0, p1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 218
    return-object p0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/library/service/d$a;
    .locals 1

    .prologue
    .line 206
    const-string/jumbo v0, ","

    invoke-static {v0, p2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 207
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcom/twitter/library/service/d$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/twitter/library/service/d$a;"
        }
    .end annotation

    .prologue
    .line 242
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 243
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_0

    .line 245
    :cond_0
    return-object p0
.end method

.method public varargs a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;
    .locals 2

    .prologue
    .line 127
    invoke-virtual {p0, p1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    .line 128
    iget-object v0, p0, Lcom/twitter/library/service/d$a;->f:Ljava/lang/StringBuilder;

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    return-object p0
.end method

.method public a()Lcom/twitter/library/service/d;
    .locals 7

    .prologue
    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/service/d$a;->j:Ljava/lang/String;

    .line 286
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/service/d$a;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 287
    iget-object v0, p0, Lcom/twitter/library/service/d$a;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    const/16 v0, 0x2f

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/service/d$a;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/service/d$a;->f:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 291
    const/4 v0, 0x0

    .line 292
    iget-object v2, p0, Lcom/twitter/library/service/d$a;->g:Lcom/twitter/network/HttpOperation$RequestMethod;

    invoke-virtual {v2}, Lcom/twitter/network/HttpOperation$RequestMethod;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 293
    iget-object v2, p0, Lcom/twitter/library/service/d$a;->d:Lcom/twitter/network/apache/e;

    if-eqz v2, :cond_1

    .line 294
    iget-object v0, p0, Lcom/twitter/library/service/d$a;->d:Lcom/twitter/network/apache/e;

    .line 295
    iget-object v2, p0, Lcom/twitter/library/service/d$a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 296
    iget-object v2, p0, Lcom/twitter/library/service/d$a;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V

    move-object v3, v0

    .line 316
    :goto_0
    new-instance v0, Lcom/twitter/library/service/d;

    iget-object v2, p0, Lcom/twitter/library/service/d$a;->g:Lcom/twitter/network/HttpOperation$RequestMethod;

    iget-object v4, p0, Lcom/twitter/library/service/d$a;->b:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/twitter/library/service/d$a;->i:Lcom/twitter/library/network/a;

    iget-boolean v6, p0, Lcom/twitter/library/service/d$a;->l:Z

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/service/d;-><init>(Ljava/lang/CharSequence;Lcom/twitter/network/HttpOperation$RequestMethod;Lcom/twitter/network/apache/e;Ljava/util/List;Lcom/twitter/library/network/a;Z)V

    return-object v0

    .line 299
    :cond_1
    iget-object v2, p0, Lcom/twitter/library/service/d$a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 300
    iget-object v2, p0, Lcom/twitter/library/service/d$a;->a:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/twitter/library/util/af;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 301
    if-eqz v2, :cond_2

    .line 302
    new-instance v0, Lcom/twitter/network/apache/entity/c;

    sget-object v3, Lcom/twitter/network/apache/a;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v2, v3}, Lcom/twitter/network/apache/entity/c;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 303
    const-string/jumbo v2, "application/x-www-form-urlencoded"

    invoke-virtual {v0, v2}, Lcom/twitter/network/apache/entity/c;->a(Ljava/lang/String;)V

    :cond_2
    move-object v3, v0

    .line 306
    goto :goto_0

    .line 309
    :cond_3
    iget-object v2, p0, Lcom/twitter/library/service/d$a;->d:Lcom/twitter/network/apache/e;

    if-eqz v2, :cond_4

    .line 310
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "HttpEntity not allowed in GET"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 312
    :cond_4
    iget-object v2, p0, Lcom/twitter/library/service/d$a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 313
    iget-object v2, p0, Lcom/twitter/library/service/d$a;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V

    :cond_5
    move-object v3, v0

    goto :goto_0

    :cond_6
    move-object v3, v0

    goto :goto_0
.end method

.method public b()Lcom/twitter/library/service/d$a;
    .locals 2

    .prologue
    .line 336
    const-string/jumbo v0, "include_cards"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 337
    const-string/jumbo v0, "cards_platform"

    const-string/jumbo v1, "Android-12"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 338
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/service/d$a;
    .locals 3

    .prologue
    .line 261
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "host cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_0
    const/16 v0, 0x2f

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 265
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid host: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 267
    :cond_1
    iput-object p1, p0, Lcom/twitter/library/service/d$a;->h:Ljava/lang/String;

    .line 268
    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/twitter/library/service/d$a;->b:Ljava/util/ArrayList;

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    return-object p0
.end method

.method public varargs b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;
    .locals 7

    .prologue
    const/16 v6, 0x2f

    const/4 v1, 0x0

    .line 134
    if-eqz p1, :cond_1

    .line 135
    array-length v2, p1

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 136
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v6, :cond_0

    .line 137
    iget-object v4, p0, Lcom/twitter/library/service/d$a;->f:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 139
    :cond_0
    iget-object v4, p0, Lcom/twitter/library/service/d$a;->f:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/twitter/library/service/d$a;->k:Ljava/lang/String;

    invoke-static {v3, v5}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    :cond_1
    return-object p0
.end method

.method public c()Lcom/twitter/library/service/d$a;
    .locals 2

    .prologue
    .line 343
    const-string/jumbo v0, "tweet_mode"

    const-string/jumbo v1, "extended"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 344
    const-string/jumbo v0, "include_reply_count"

    const-string/jumbo v1, "true"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 345
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/service/d$a;
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 324
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "scheme cannot be empty or null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 327
    :cond_0
    const/16 v0, 0x2f

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    const/16 v0, 0x3a

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-eq v0, v1, :cond_2

    .line 328
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid scheme: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330
    :cond_2
    iput-object p1, p0, Lcom/twitter/library/service/d$a;->j:Ljava/lang/String;

    .line 331
    return-object p0
.end method

.method public d()Lcom/twitter/library/service/d$a;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 356
    const-string/jumbo v0, "include_blocked_by_and_blocking_in_requests_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    const-string/jumbo v0, "include_blocking"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 359
    const-string/jumbo v0, "include_blocked_by"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 361
    :cond_0
    return-object p0
.end method

.method public e()Lcom/twitter/library/service/d$a;
    .locals 2

    .prologue
    .line 366
    const-string/jumbo v0, "ext"

    sget-object v1, Lcom/twitter/library/service/d;->a:[Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/collection/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/twitter/library/service/d$a;

    .line 367
    return-object p0
.end method

.method public f()Lcom/twitter/library/service/d$a;
    .locals 2

    .prologue
    .line 372
    const-string/jumbo v0, "include_carousels"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 373
    return-object p0
.end method
