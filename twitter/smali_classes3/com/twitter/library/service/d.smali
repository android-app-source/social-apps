.class public Lcom/twitter/library/service/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/service/d$a;
    }
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/CharSequence;

.field public final c:Lcom/twitter/network/HttpOperation$RequestMethod;

.field public final d:Lcom/twitter/network/apache/e;

.field final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field final f:Z

.field final g:Lcom/twitter/library/network/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "stickerInfo"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "mediaRestrictions"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "altText"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "mediaStats"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "mediaColor"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/service/d;->a:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/CharSequence;Lcom/twitter/network/HttpOperation$RequestMethod;Lcom/twitter/network/apache/e;Ljava/util/List;Lcom/twitter/library/network/a;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Lcom/twitter/network/HttpOperation$RequestMethod;",
            "Lcom/twitter/network/apache/e;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/twitter/library/network/a;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/twitter/library/service/d;->b:Ljava/lang/CharSequence;

    .line 55
    iput-object p2, p0, Lcom/twitter/library/service/d;->c:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 56
    iput-object p3, p0, Lcom/twitter/library/service/d;->d:Lcom/twitter/network/apache/e;

    .line 57
    iput-object p4, p0, Lcom/twitter/library/service/d;->e:Ljava/util/List;

    .line 58
    iput-object p5, p0, Lcom/twitter/library/service/d;->g:Lcom/twitter/library/network/a;

    .line 59
    iput-boolean p6, p0, Lcom/twitter/library/service/d;->f:Z

    .line 60
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/library/service/d$a;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 69
    new-instance v0, Lcom/twitter/library/service/d$a;

    invoke-direct {v0, p0}, Lcom/twitter/library/service/d$a;-><init>(Landroid/content/Context;)V

    .line 70
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    const-string/jumbo v1, "debug_prefs"

    .line 72
    invoke-virtual {p0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 73
    const-string/jumbo v2, "staging_enabled"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    const-string/jumbo v2, "staging_url"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75
    if-eqz v1, :cond_0

    .line 76
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 77
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/service/d$a;->c(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    .line 78
    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/library/service/d$a;->b(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 82
    :cond_0
    return-object v0
.end method
