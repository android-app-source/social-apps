.class public abstract Lcom/twitter/library/service/b;
.super Lcom/twitter/library/service/s;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/library/service/c;",
        ">",
        "Lcom/twitter/library/service/s;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/twitter/network/HttpOperation;

.field private c:Z

.field private g:Z

.field private h:Z

.field protected final n:Lcom/twitter/async/service/b;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 164
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 130
    iput v1, p0, Lcom/twitter/library/service/b;->a:I

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/service/b;->b:Lcom/twitter/network/HttpOperation;

    .line 134
    iput-boolean v2, p0, Lcom/twitter/library/service/b;->c:Z

    .line 136
    iput-boolean v2, p0, Lcom/twitter/library/service/b;->g:Z

    .line 138
    iput-boolean v1, p0, Lcom/twitter/library/service/b;->h:Z

    .line 165
    if-nez p3, :cond_0

    .line 166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Session cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    new-instance v0, Lcom/twitter/async/service/b;

    invoke-direct {v0}, Lcom/twitter/async/service/b;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/service/b;->n:Lcom/twitter/async/service/b;

    .line 170
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;I)V
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 186
    if-nez p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/service/b;->g:Z

    .line 187
    return-void

    .line 186
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 202
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 130
    iput v2, p0, Lcom/twitter/library/service/b;->a:I

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/service/b;->b:Lcom/twitter/network/HttpOperation;

    .line 134
    iput-boolean v1, p0, Lcom/twitter/library/service/b;->c:Z

    .line 136
    iput-boolean v1, p0, Lcom/twitter/library/service/b;->g:Z

    .line 138
    iput-boolean v2, p0, Lcom/twitter/library/service/b;->h:Z

    .line 203
    if-nez p3, :cond_0

    .line 204
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "SessionStamp cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :cond_0
    new-instance v0, Lcom/twitter/async/service/b;

    invoke-direct {v0}, Lcom/twitter/async/service/b;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/service/b;->n:Lcom/twitter/async/service/b;

    .line 208
    iput-boolean v1, p0, Lcom/twitter/library/service/b;->g:Z

    .line 209
    return-void
.end method


# virtual methods
.method protected final J()Lcom/twitter/library/service/d$a;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/twitter/library/service/b;->p:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/service/d;->a(Landroid/content/Context;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    return-object v0
.end method

.method public K()V
    .locals 1

    .prologue
    .line 362
    monitor-enter p0

    .line 363
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/twitter/library/service/b;->cancel(Z)Z

    .line 364
    iget-object v0, p0, Lcom/twitter/library/service/b;->b:Lcom/twitter/network/HttpOperation;

    .line 365
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 366
    if-eqz v0, :cond_0

    .line 367
    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->b()V

    .line 369
    :cond_0
    return-void

    .line 365
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected abstract a()Lcom/twitter/library/service/d;
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 385
    return-void
.end method

.method protected final a_(Lcom/twitter/library/service/u;)V
    .locals 8

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/twitter/library/service/b;->a()Lcom/twitter/library/service/d;

    move-result-object v1

    .line 298
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0}, Lcpd;->b()Lcpa;

    move-result-object v0

    const-string/jumbo v2, "api_request_uri"

    iget-object v3, v1, Lcom/twitter/library/service/d;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v3}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    invoke-virtual {p0}, Lcom/twitter/library/service/b;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/v;

    .line 301
    invoke-virtual {p0}, Lcom/twitter/library/service/b;->f()Lcom/twitter/library/service/c;

    move-result-object v3

    .line 302
    iget-object v2, p0, Lcom/twitter/library/service/b;->p:Landroid/content/Context;

    iget-object v4, v1, Lcom/twitter/library/service/d;->b:Ljava/lang/CharSequence;

    .line 303
    invoke-virtual {p0, v2, v4}, Lcom/twitter/library/service/b;->a(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/twitter/library/network/k;

    move-result-object v2

    iget-wide v4, v0, Lcom/twitter/library/service/v;->c:J

    .line 304
    invoke-virtual {v2, v4, v5}, Lcom/twitter/library/network/k;->a(J)Lcom/twitter/library/network/k;

    move-result-object v2

    iget-object v4, v1, Lcom/twitter/library/service/d;->c:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 305
    invoke-virtual {v2, v4}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/k;

    move-result-object v2

    iget-object v4, v1, Lcom/twitter/library/service/d;->d:Lcom/twitter/network/apache/e;

    .line 306
    invoke-virtual {v2, v4}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/apache/e;)Lcom/twitter/library/network/k;

    move-result-object v2

    iget-boolean v4, p0, Lcom/twitter/library/service/b;->g:Z

    .line 307
    invoke-virtual {v2, v4}, Lcom/twitter/library/network/k;->d(Z)Lcom/twitter/library/network/k;

    move-result-object v2

    .line 308
    invoke-virtual {v2, v3}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;

    move-result-object v2

    iget-object v4, p0, Lcom/twitter/library/service/b;->n:Lcom/twitter/async/service/b;

    .line 309
    invoke-virtual {v2, v4}, Lcom/twitter/library/network/k;->a(Lcom/twitter/async/service/b;)Lcom/twitter/library/network/k;

    move-result-object v2

    iget-boolean v4, p0, Lcom/twitter/library/service/b;->c:Z

    .line 310
    invoke-virtual {v2, v4}, Lcom/twitter/library/network/k;->a(Z)Lcom/twitter/library/network/k;

    move-result-object v2

    iget-boolean v4, p0, Lcom/twitter/library/service/b;->h:Z

    .line 311
    invoke-virtual {v2, v4}, Lcom/twitter/library/network/k;->b(Z)Lcom/twitter/library/network/k;

    move-result-object v2

    .line 312
    iget-object v4, v1, Lcom/twitter/library/service/d;->g:Lcom/twitter/library/network/a;

    if-eqz v4, :cond_1

    .line 313
    iget-object v4, v1, Lcom/twitter/library/service/d;->g:Lcom/twitter/library/network/a;

    invoke-virtual {v2, v4}, Lcom/twitter/library/network/k;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/k;

    .line 318
    :goto_0
    iget v4, p0, Lcom/twitter/library/service/b;->a:I

    if-lez v4, :cond_0

    .line 319
    iget v4, p0, Lcom/twitter/library/service/b;->a:I

    invoke-virtual {v2, v4}, Lcom/twitter/library/network/k;->a(I)Lcom/twitter/library/network/k;

    .line 321
    :cond_0
    invoke-virtual {v2}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v4

    .line 323
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 324
    :try_start_1
    iput-object v4, p0, Lcom/twitter/library/service/b;->b:Lcom/twitter/network/HttpOperation;

    .line 325
    invoke-virtual {p0}, Lcom/twitter/library/service/b;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 326
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 338
    monitor-enter p0

    .line 339
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/twitter/library/service/b;->b:Lcom/twitter/network/HttpOperation;

    .line 340
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 354
    :goto_1
    return-void

    .line 315
    :cond_1
    new-instance v4, Lcom/twitter/library/network/t;

    iget-object v5, v0, Lcom/twitter/library/service/v;->d:Lcom/twitter/model/account/OAuthToken;

    invoke-direct {v4, v5}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    invoke-virtual {v2, v4}, Lcom/twitter/library/network/k;->b(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/k;

    goto :goto_0

    .line 340
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 328
    :cond_2
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 330
    :try_start_5
    iget-object v1, v1, Lcom/twitter/library/service/d;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 331
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v2, v1}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    .line 338
    :catchall_1
    move-exception v0

    monitor-enter p0

    .line 339
    const/4 v1, 0x0

    :try_start_6
    iput-object v1, p0, Lcom/twitter/library/service/b;->b:Lcom/twitter/network/HttpOperation;

    .line 340
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    throw v0

    .line 328
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw v0

    .line 333
    :cond_3
    invoke-virtual {v4}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 334
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-nez v1, :cond_4

    if-eqz v3, :cond_4

    .line 335
    invoke-virtual {v3}, Lcom/twitter/library/service/c;->a()Lcom/twitter/library/service/a;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/library/service/r;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 338
    :cond_4
    monitor-enter p0

    .line 339
    const/4 v1, 0x0

    :try_start_9
    iput-object v1, p0, Lcom/twitter/library/service/b;->b:Lcom/twitter/network/HttpOperation;

    .line 340
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 344
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    iget-wide v6, v0, Lcom/twitter/library/service/v;->c:J

    invoke-virtual {v1, v6, v7}, Lcom/twitter/library/client/v;->c(J)Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 345
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    .line 346
    :goto_3
    iget-boolean v0, v0, Lcom/twitter/library/service/v;->b:Z

    if-eqz v0, :cond_5

    if-eqz v1, :cond_6

    .line 347
    :cond_5
    invoke-virtual {p0, v4, p1, v3}, Lcom/twitter/library/service/b;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    .line 349
    :cond_6
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->c()Ljava/lang/Exception;

    move-result-object v0

    .line 350
    if-eqz v0, :cond_7

    .line 351
    iget-object v1, p0, Lcom/twitter/library/service/b;->p:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Ljava/lang/Throwable;)V

    .line 353
    :cond_7
    const-string/jumbo v0, "APIRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Action complete: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/library/service/b;->L()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", success: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/service/u;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 340
    :catchall_3
    move-exception v0

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v0

    :catchall_4
    move-exception v0

    :try_start_b
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    throw v0

    .line 345
    :cond_8
    const/4 v1, 0x0

    goto :goto_3
.end method

.method protected abstract f()Lcom/twitter/library/service/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public f(I)V
    .locals 0

    .prologue
    .line 244
    iput p1, p0, Lcom/twitter/library/service/b;->a:I

    .line 245
    return-void
.end method

.method public f(Z)V
    .locals 0

    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/twitter/library/service/b;->c:Z

    .line 234
    return-void
.end method

.method public g(Z)V
    .locals 0

    .prologue
    .line 252
    iput-boolean p1, p0, Lcom/twitter/library/service/b;->h:Z

    .line 253
    return-void
.end method

.method public m()Lcom/twitter/async/service/b;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/twitter/library/service/b;->n:Lcom/twitter/async/service/b;

    return-object v0
.end method
