.class public Lcom/twitter/library/service/p;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/library/service/p;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {}, Lcom/twitter/library/network/forecaster/c;->a()Lcom/twitter/library/network/forecaster/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/network/forecaster/c;->a(Lcom/twitter/util/q;)Z

    .line 26
    return-void
.end method

.method public static declared-synchronized a()V
    .locals 2

    .prologue
    .line 29
    const-class v1, Lcom/twitter/library/service/p;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/service/p;->a:Lcom/twitter/library/service/p;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/twitter/library/service/p;

    invoke-direct {v0}, Lcom/twitter/library/service/p;-><init>()V

    sput-object v0, Lcom/twitter/library/service/p;->a:Lcom/twitter/library/service/p;

    .line 31
    const-class v0, Lcom/twitter/library/service/p;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :cond_0
    monitor-exit v1

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onEvent(Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;)V
    .locals 4

    .prologue
    .line 42
    new-instance v0, Lcom/twitter/library/service/m;

    invoke-direct {v0, p1}, Lcom/twitter/library/service/m;-><init>(Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;)V

    .line 43
    const-string/jumbo v1, "NetworkRequestRetryObs"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- will inform RequestController"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "ANDROID-10803"

    invoke-static {v1, v2, v3}, Lcqi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/e;)V

    .line 45
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;

    invoke-virtual {p0, p1}, Lcom/twitter/library/service/p;->onEvent(Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;)V

    return-void
.end method
