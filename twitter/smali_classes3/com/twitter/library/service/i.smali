.class public Lcom/twitter/library/service/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/network/j;


# instance fields
.field private final a:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/twitter/library/service/i;->a:Ljava/io/File;

    .line 20
    return-void
.end method


# virtual methods
.method public a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lcom/twitter/library/service/i;->a:Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 27
    const/16 v0, 0x1000

    :try_start_0
    invoke-static {p2, v1, v0}, Lcqc;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)I

    .line 28
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 33
    :goto_0
    return-void

    .line 29
    :catch_0
    move-exception v0

    .line 31
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public a(Lcom/twitter/network/l;)V
    .locals 0

    .prologue
    .line 37
    return-void
.end method
