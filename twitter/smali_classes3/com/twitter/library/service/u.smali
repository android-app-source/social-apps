.class public Lcom/twitter/library/service/u;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:I

.field private b:Z

.field public final c:Landroid/os/Bundle;

.field private d:Ljava/lang/Exception;

.field private e:Ljava/lang/String;

.field private f:Lcom/twitter/library/api/RateLimit;

.field private g:Lcom/twitter/network/HttpOperation;

.field private h:Lcom/twitter/library/service/r;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/service/u;->i:Z

    .line 31
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    .line 32
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-virtual {p0, p1, v0, v0}, Lcom/twitter/library/service/u;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    .line 44
    return-void
.end method

.method public a(ILjava/lang/Exception;)V
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/library/service/u;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    .line 52
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/service/u;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    .line 48
    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/service/u;->b:Z

    .line 56
    iput p1, p0, Lcom/twitter/library/service/u;->a:I

    .line 57
    iput-object p2, p0, Lcom/twitter/library/service/u;->e:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lcom/twitter/library/service/u;->d:Ljava/lang/Exception;

    .line 59
    return-void
.end method

.method public a(Lcom/twitter/library/service/r;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/twitter/library/service/u;->h:Lcom/twitter/library/service/r;

    .line 112
    return-void
.end method

.method public a(Lcom/twitter/library/service/u;)V
    .locals 2

    .prologue
    .line 125
    iget-boolean v0, p1, Lcom/twitter/library/service/u;->b:Z

    iput-boolean v0, p0, Lcom/twitter/library/service/u;->b:Z

    .line 126
    iget v0, p1, Lcom/twitter/library/service/u;->a:I

    iput v0, p0, Lcom/twitter/library/service/u;->a:I

    .line 127
    iget-object v0, p1, Lcom/twitter/library/service/u;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/service/u;->e:Ljava/lang/String;

    .line 128
    iget-object v0, p1, Lcom/twitter/library/service/u;->d:Ljava/lang/Exception;

    iput-object v0, p0, Lcom/twitter/library/service/u;->d:Ljava/lang/Exception;

    .line 129
    iget-object v0, p1, Lcom/twitter/library/service/u;->f:Lcom/twitter/library/api/RateLimit;

    iput-object v0, p0, Lcom/twitter/library/service/u;->f:Lcom/twitter/library/api/RateLimit;

    .line 130
    iget-object v0, p1, Lcom/twitter/library/service/u;->g:Lcom/twitter/network/HttpOperation;

    iput-object v0, p0, Lcom/twitter/library/service/u;->g:Lcom/twitter/network/HttpOperation;

    .line 131
    iget-object v0, p0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    iget-object v1, p1, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 132
    iget-object v0, p1, Lcom/twitter/library/service/u;->h:Lcom/twitter/library/service/r;

    iput-object v0, p0, Lcom/twitter/library/service/u;->h:Lcom/twitter/library/service/r;

    .line 133
    return-void
.end method

.method public a(Lcom/twitter/network/HttpOperation;)V
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/service/u;->b:Z

    .line 63
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v0

    .line 64
    iget v1, v0, Lcom/twitter/network/l;->a:I

    iput v1, p0, Lcom/twitter/library/service/u;->a:I

    .line 65
    iget-object v1, v0, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    iput-object v1, p0, Lcom/twitter/library/service/u;->d:Ljava/lang/Exception;

    .line 66
    iget-object v0, v0, Lcom/twitter/network/l;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/service/u;->e:Ljava/lang/String;

    .line 67
    invoke-static {p1}, Lcom/twitter/library/network/ab;->a(Lcom/twitter/network/HttpOperation;)Lcom/twitter/library/api/RateLimit;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/u;->f:Lcom/twitter/library/api/RateLimit;

    .line 68
    iput-object p1, p0, Lcom/twitter/library/service/u;->g:Lcom/twitter/network/HttpOperation;

    .line 69
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/twitter/library/service/u;->b:Z

    .line 73
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/twitter/library/service/u;->b:Z

    return v0
.end method

.method public c()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/library/service/u;->d:Ljava/lang/Exception;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/twitter/library/service/u;->a:I

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/library/service/u;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Lcom/twitter/network/HttpOperation;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/library/service/u;->g:Lcom/twitter/network/HttpOperation;

    return-object v0
.end method

.method public g()Lcom/twitter/network/l;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/library/service/u;->g:Lcom/twitter/network/HttpOperation;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/twitter/library/service/u;->g:Lcom/twitter/network/HttpOperation;

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v0

    .line 101
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Lcom/twitter/library/api/RateLimit;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/library/service/u;->f:Lcom/twitter/library/api/RateLimit;

    return-object v0
.end method

.method public i()Lcom/twitter/library/service/r;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/library/service/u;->h:Lcom/twitter/library/service/r;

    return-object v0
.end method
