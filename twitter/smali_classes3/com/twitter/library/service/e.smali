.class public abstract Lcom/twitter/library/service/e;
.super Lcom/twitter/async/service/k;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/async/service/k",
        "<",
        "Lcom/twitter/library/service/u;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:I

.field private b:J

.field private c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:Z

.field private final i:Lcqt;


# direct methods
.method public constructor <init>(III)V
    .locals 1

    .prologue
    .line 50
    const v0, 0x7fffffff

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/library/service/e;-><init>(IIII)V

    .line 51
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/twitter/async/service/k;-><init>()V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/service/e;->a:I

    .line 38
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/library/service/e;->b:J

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/service/e;->c:I

    .line 57
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    if-lez p3, :cond_0

    if-gtz p4, :cond_1

    .line 61
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 64
    :cond_1
    iput p1, p0, Lcom/twitter/library/service/e;->d:I

    .line 65
    iput p2, p0, Lcom/twitter/library/service/e;->e:I

    .line 66
    iput p3, p0, Lcom/twitter/library/service/e;->f:I

    .line 67
    iput p4, p0, Lcom/twitter/library/service/e;->g:I

    .line 68
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/e;->i:Lcqt;

    .line 70
    const-string/jumbo v0, "android_disable_offline_retries"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/service/e;->h:Z

    .line 71
    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method public a(Lcom/twitter/async/service/e;Lcom/twitter/async/service/j;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/e;",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 103
    const/4 v0, 0x0

    .line 104
    instance-of v1, p1, Lcom/twitter/library/service/m;

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 105
    check-cast p1, Lcom/twitter/library/service/m;

    iget-object v1, p1, Lcom/twitter/library/service/m;->a:Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;

    .line 106
    iget-object v2, v1, Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;->a:Lcom/twitter/library/network/forecaster/NetworkQuality;

    sget-object v3, Lcom/twitter/library/network/forecaster/NetworkQuality;->a:Lcom/twitter/library/network/forecaster/NetworkQuality;

    if-ne v2, v3, :cond_0

    iget-object v1, v1, Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;->b:Lcom/twitter/library/network/forecaster/NetworkQuality;

    sget-object v2, Lcom/twitter/library/network/forecaster/NetworkQuality;->a:Lcom/twitter/library/network/forecaster/NetworkQuality;

    if-eq v1, v2, :cond_0

    .line 107
    invoke-virtual {p2}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 108
    invoke-virtual {p0, v0}, Lcom/twitter/library/service/e;->a(Lcom/twitter/library/service/u;)Z

    move-result v0

    .line 111
    :cond_0
    return v0
.end method

.method public final a(Lcom/twitter/async/service/j;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 76
    invoke-virtual {p0, v0}, Lcom/twitter/library/service/e;->a(Lcom/twitter/library/service/u;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 77
    iget v0, p0, Lcom/twitter/library/service/e;->a:I

    if-gez v0, :cond_1

    .line 78
    iget v0, p0, Lcom/twitter/library/service/e;->d:I

    iput v0, p0, Lcom/twitter/library/service/e;->a:I

    .line 79
    iget-object v0, p0, Lcom/twitter/library/service/e;->i:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/library/service/e;->b:J

    .line 85
    :goto_0
    iget v0, p0, Lcom/twitter/library/service/e;->c:I

    iget v2, p0, Lcom/twitter/library/service/e;->g:I

    if-ge v0, v2, :cond_2

    invoke-virtual {p0}, Lcom/twitter/library/service/e;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 86
    :goto_1
    if-eqz v0, :cond_0

    .line 87
    iget v1, p0, Lcom/twitter/library/service/e;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/twitter/library/service/e;->c:I

    .line 92
    :cond_0
    :goto_2
    return v0

    .line 81
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/service/e;->a()I

    move-result v0

    .line 82
    iget v2, p0, Lcom/twitter/library/service/e;->e:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/service/e;->a:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 85
    goto :goto_1

    :cond_3
    move v0, v1

    .line 92
    goto :goto_2
.end method

.method protected a(Lcom/twitter/library/service/u;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 120
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v0

    .line 123
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v1

    .line 124
    if-eqz v1, :cond_0

    .line 127
    invoke-virtual {v1}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v1

    .line 128
    if-eqz v1, :cond_0

    iget-boolean v1, v1, Lcom/twitter/network/l;->d:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b()J
    .locals 6

    .prologue
    .line 148
    iget-wide v0, p0, Lcom/twitter/library/service/e;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 149
    iget v0, p0, Lcom/twitter/library/service/e;->f:I

    int-to-long v0, v0

    iget-object v2, p0, Lcom/twitter/library/service/e;->i:Lcqt;

    invoke-interface {v2}, Lcqt;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/library/service/e;->b:J

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    .line 151
    :goto_0
    return-wide v0

    :cond_0
    iget v0, p0, Lcom/twitter/library/service/e;->f:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final b(Lcom/twitter/async/service/j;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/twitter/library/service/e;->h:Z

    if-eqz v0, :cond_0

    .line 139
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 140
    :goto_0
    if-eqz v0, :cond_1

    .line 141
    invoke-virtual {p0}, Lcom/twitter/library/service/e;->b()J

    move-result-wide v0

    long-to-int v0, v0

    int-to-long v0, v0

    .line 143
    :goto_1
    return-wide v0

    .line 139
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 143
    :cond_1
    iget v0, p0, Lcom/twitter/library/service/e;->a:I

    int-to-long v0, v0

    goto :goto_1
.end method
