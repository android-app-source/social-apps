.class Lcom/twitter/library/widget/InlineActionBar$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/anim/e$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/widget/InlineActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/InlineActionBar;

.field private final b:Lcom/twitter/library/widget/InlineActionView;

.field private final c:Lbwu;

.field private final d:Z

.field private final e:J

.field private f:I

.field private g:I


# direct methods
.method private constructor <init>(Lcom/twitter/library/widget/InlineActionBar;Lbwu;Z)V
    .locals 2

    .prologue
    .line 550
    iput-object p1, p0, Lcom/twitter/library/widget/InlineActionBar$a;->a:Lcom/twitter/library/widget/InlineActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 551
    iput-object p2, p0, Lcom/twitter/library/widget/InlineActionBar$a;->c:Lbwu;

    .line 552
    invoke-static {p2}, Lcom/twitter/library/widget/InlineActionBar;->a(Lbwu;)Lcom/twitter/library/widget/InlineActionView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->b:Lcom/twitter/library/widget/InlineActionView;

    .line 553
    iput-boolean p3, p0, Lcom/twitter/library/widget/InlineActionBar$a;->d:Z

    .line 554
    invoke-static {p1}, Lcom/twitter/library/widget/InlineActionBar;->b(Lcom/twitter/library/widget/InlineActionBar;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    iput-wide v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->e:J

    .line 555
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/widget/InlineActionBar;Lbwu;ZLcom/twitter/library/widget/InlineActionBar$1;)V
    .locals 0

    .prologue
    .line 536
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/widget/InlineActionBar$a;-><init>(Lcom/twitter/library/widget/InlineActionBar;Lbwu;Z)V

    return-void
.end method

.method private b(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->a:Lcom/twitter/library/widget/InlineActionBar;

    invoke-static {v0, p1}, Lcom/twitter/library/widget/InlineActionBar;->a(Lcom/twitter/library/widget/InlineActionBar;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 604
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->a:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 605
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 588
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/InlineActionBar$a;->b(Landroid/graphics/Bitmap;)V

    .line 589
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->b:Lcom/twitter/library/widget/InlineActionView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionView;->a()V

    .line 590
    iget v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->f:I

    if-nez v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->b:Lcom/twitter/library/widget/InlineActionView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionView;->getTextView()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/widget/InlineActionBar$a;->f:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 593
    :cond_0
    iget v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->g:I

    if-nez v0, :cond_1

    .line 594
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->b:Lcom/twitter/library/widget/InlineActionView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionView;->getIconView()Landroid/widget/ImageView;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/widget/InlineActionBar$a;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 596
    :cond_1
    return-void
.end method

.method private e()Z
    .locals 4

    .prologue
    .line 608
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->a:Lcom/twitter/library/widget/InlineActionBar;

    invoke-static {v0}, Lcom/twitter/library/widget/InlineActionBar;->b(Lcom/twitter/library/widget/InlineActionBar;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->a:Lcom/twitter/library/widget/InlineActionBar;

    invoke-static {v0}, Lcom/twitter/library/widget/InlineActionBar;->b(Lcom/twitter/library/widget/InlineActionBar;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    iget-wide v2, p0, Lcom/twitter/library/widget/InlineActionBar$a;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 564
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->b:Lcom/twitter/library/widget/InlineActionView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionView;->getTextView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->f:I

    .line 565
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->b:Lcom/twitter/library/widget/InlineActionView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionView;->getIconView()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->g:I

    .line 566
    iget v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->f:I

    if-nez v0, :cond_0

    .line 567
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->b:Lcom/twitter/library/widget/InlineActionView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionView;->getTextView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 569
    :cond_0
    iget v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->g:I

    if-nez v0, :cond_1

    .line 570
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->b:Lcom/twitter/library/widget/InlineActionView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionView;->getIconView()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 572
    :cond_1
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/InlineActionBar$a;->b(Landroid/graphics/Bitmap;)V

    .line 560
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 576
    invoke-direct {p0}, Lcom/twitter/library/widget/InlineActionBar$a;->d()V

    .line 577
    iget-boolean v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->a:Lcom/twitter/library/widget/InlineActionBar;

    invoke-static {v0}, Lcom/twitter/library/widget/InlineActionBar;->a(Lcom/twitter/library/widget/InlineActionBar;)Lcom/twitter/library/widget/InlineActionBar$b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/widget/InlineActionBar$a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar$a;->a:Lcom/twitter/library/widget/InlineActionBar;

    invoke-static {v0}, Lcom/twitter/library/widget/InlineActionBar;->a(Lcom/twitter/library/widget/InlineActionBar;)Lcom/twitter/library/widget/InlineActionBar$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/InlineActionBar$a;->c:Lbwu;

    invoke-virtual {v1}, Lbwu;->a()Lcom/twitter/model/core/TweetActionType;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/library/widget/InlineActionBar$b;->a(Lcom/twitter/model/core/TweetActionType;)V

    .line 580
    :cond_0
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 584
    invoke-direct {p0}, Lcom/twitter/library/widget/InlineActionBar$a;->d()V

    .line 585
    return-void
.end method
