.class public abstract Lcom/twitter/library/widget/renderablecontent/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/renderablecontent/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ContentSource:",
        "Ljava/lang/Object;",
        "ContentContainer::",
        "Lcom/twitter/library/widget/renderablecontent/c;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/twitter/library/widget/renderablecontent/d;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/common/util/b$a;

.field public final h:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TContentSource;"
        }
    .end annotation
.end field

.field public final i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field public final j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field public final k:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/twitter/library/widget/renderablecontent/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TContentContainer;"
        }
    .end annotation
.end field

.field protected m:Z

.field protected final n:Lcom/twitter/library/widget/renderablecontent/DisplayMode;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;Ljava/lang/Object;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "TContentSource;",
            "Lcom/twitter/library/widget/renderablecontent/DisplayMode;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ")V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/twitter/library/widget/renderablecontent/a$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/renderablecontent/a$1;-><init>(Lcom/twitter/library/widget/renderablecontent/a;)V

    iput-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->a:Lcom/twitter/app/common/util/b$a;

    .line 81
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->k:Ljava/lang/ref/WeakReference;

    .line 82
    iput-object p2, p0, Lcom/twitter/library/widget/renderablecontent/a;->h:Ljava/lang/Object;

    .line 83
    iput-object p3, p0, Lcom/twitter/library/widget/renderablecontent/a;->n:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 84
    iput-object p4, p0, Lcom/twitter/library/widget/renderablecontent/a;->i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 85
    iput-object p5, p0, Lcom/twitter/library/widget/renderablecontent/a;->j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 86
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/app/Activity;)Lcom/twitter/library/widget/renderablecontent/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            ")TContentContainer;"
        }
    .end annotation
.end method

.method protected abstract a()Ljava/lang/Object;
.end method

.method public bg_()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 91
    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/renderablecontent/a;->a(Landroid/app/Activity;)Lcom/twitter/library/widget/renderablecontent/c;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    .line 93
    iget-object v1, p0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    if-eqz v1, :cond_0

    .line 94
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    iget-object v1, p0, Lcom/twitter/library/widget/renderablecontent/a;->a:Lcom/twitter/app/common/util/b$a;

    .line 95
    invoke-interface {v0, v1}, Lcom/twitter/app/common/util/j;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 97
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    invoke-virtual {p0}, Lcom/twitter/library/widget/renderablecontent/a;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/library/widget/renderablecontent/c;->a(Ljava/lang/Object;)V

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->m:Z

    .line 101
    :cond_0
    return-void
.end method

.method public bh_()V
    .locals 2

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->m:Z

    .line 120
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/c;->a()V

    .line 122
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 123
    if-eqz v0, :cond_0

    .line 124
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    iget-object v1, p0, Lcom/twitter/library/widget/renderablecontent/a;->a:Lcom/twitter/app/common/util/b$a;

    .line 125
    invoke-interface {v0, v1}, Lcom/twitter/app/common/util/j;->b(Lcom/twitter/app/common/util/b$a;)V

    .line 128
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/c;->b()V

    .line 114
    :cond_0
    return-void
.end method

.method public d()Landroid/view/View;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/c;->e()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Lcom/twitter/library/widget/renderablecontent/c;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    return-object v0
.end method
