.class Lcom/twitter/library/widget/renderablecontent/a$1;
.super Lcom/twitter/app/common/util/b$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/widget/renderablecontent/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/renderablecontent/a;


# direct methods
.method constructor <init>(Lcom/twitter/library/widget/renderablecontent/a;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    invoke-direct {p0}, Lcom/twitter/app/common/util/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    iget-object v0, v0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    iget-object v0, v0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    invoke-interface {v0, p2}, Lcom/twitter/library/widget/renderablecontent/c;->a(Landroid/content/res/Configuration;)V

    .line 75
    :cond_0
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    invoke-virtual {v0}, Lcom/twitter/library/widget/renderablecontent/a;->bh_()V

    .line 65
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    iget-object v0, v0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    iget-object v1, v0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    invoke-interface {v0}, Lcom/twitter/app/common/util/j;->isChangingConfigurations()Z

    move-result v0

    invoke-interface {v1, v0}, Lcom/twitter/library/widget/renderablecontent/c;->b(Z)V

    .line 68
    :cond_0
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    iget-object v0, v0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    iget-object v0, v0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/c;->aj_()V

    .line 53
    :cond_0
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    iget-object v0, v0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    iget-object v0, v0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/c;->d()V

    .line 46
    :cond_0
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    iget-object v0, v0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    iget-object v0, v0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/c;->ai_()V

    .line 39
    :cond_0
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    iget-object v0, v0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/twitter/library/widget/renderablecontent/a$1;->a:Lcom/twitter/library/widget/renderablecontent/a;

    iget-object v1, v0, Lcom/twitter/library/widget/renderablecontent/a;->l:Lcom/twitter/library/widget/renderablecontent/c;

    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    invoke-interface {v0}, Lcom/twitter/app/common/util/j;->isChangingConfigurations()Z

    move-result v0

    invoke-interface {v1, v0}, Lcom/twitter/library/widget/renderablecontent/c;->a(Z)V

    .line 60
    :cond_0
    return-void
.end method
