.class Lcom/twitter/library/widget/InlineDismissView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/widget/InlineDismissView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/widget/InlineDismissView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:Lcom/twitter/model/timeline/k;

.field final b:Lcom/twitter/model/timeline/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 266
    new-instance v0, Lcom/twitter/library/widget/InlineDismissView$SavedState$1;

    invoke-direct {v0}, Lcom/twitter/library/widget/InlineDismissView$SavedState$1;-><init>()V

    sput-object v0, Lcom/twitter/library/widget/InlineDismissView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 291
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 292
    sget-object v0, Lcom/twitter/model/timeline/k;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/k;

    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView$SavedState;->a:Lcom/twitter/model/timeline/k;

    .line 293
    sget-object v0, Lcom/twitter/model/timeline/g;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/g;

    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView$SavedState;->b:Lcom/twitter/model/timeline/g;

    .line 294
    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;Lcom/twitter/model/timeline/k;Lcom/twitter/model/timeline/g;)V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 286
    iput-object p2, p0, Lcom/twitter/library/widget/InlineDismissView$SavedState;->a:Lcom/twitter/model/timeline/k;

    .line 287
    iput-object p3, p0, Lcom/twitter/library/widget/InlineDismissView$SavedState;->b:Lcom/twitter/model/timeline/g;

    .line 288
    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 298
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 299
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView$SavedState;->a:Lcom/twitter/model/timeline/k;

    sget-object v1, Lcom/twitter/model/timeline/k;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 300
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView$SavedState;->b:Lcom/twitter/model/timeline/g;

    sget-object v1, Lcom/twitter/model/timeline/g;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 301
    return-void
.end method
