.class public Lcom/twitter/library/widget/g;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/widget/g$b;,
        Lcom/twitter/library/widget/g$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/View;

.field private final c:Landroid/graphics/Paint$FontMetrics;

.field private d:Lcne;

.field private e:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private l:Ljava/lang/CharSequence;

.field private m:Ljava/lang/String;

.field private n:Lcom/twitter/library/widget/g$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/graphics/Paint$FontMetrics;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/twitter/library/widget/g;->a:Landroid/content/Context;

    .line 53
    iput-object p2, p0, Lcom/twitter/library/widget/g;->b:Landroid/view/View;

    .line 54
    iput-object p3, p0, Lcom/twitter/library/widget/g;->c:Landroid/graphics/Paint$FontMetrics;

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/g;->i:Z

    .line 56
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 8

    .prologue
    .line 191
    iget-object v0, p0, Lcom/twitter/library/widget/g;->l:Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    .line 193
    iget-object v0, p0, Lcom/twitter/library/widget/g;->m:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 194
    iget-object v0, p0, Lcom/twitter/library/widget/g;->l:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 195
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 196
    new-instance v0, Lcom/twitter/library/widget/g$1;

    iget v2, p0, Lcom/twitter/library/widget/g;->e:I

    iget v1, p0, Lcom/twitter/library/widget/g;->k:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/widget/g$1;-><init>(Lcom/twitter/library/widget/g;ILjava/lang/Integer;ZZ)V

    .line 202
    const/16 v1, 0x21

    invoke-virtual {p2, v0, v6, v7, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 204
    :cond_0
    return-object p2
.end method

.method private a(Lcom/twitter/model/core/v;Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 2

    .prologue
    .line 178
    invoke-static {p2}, Lcnf;->a(Ljava/lang/CharSequence;)Lcnf;

    move-result-object v0

    .line 179
    invoke-virtual {v0, p1}, Lcnf;->a(Lcom/twitter/model/core/v;)Lcnf;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/widget/g;->e:I

    .line 180
    invoke-virtual {v0, v1}, Lcnf;->b(I)Lcnf;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/g;->d:Lcne;

    .line 181
    invoke-virtual {v0, v1}, Lcnf;->a(Lcne;)Lcnf;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/library/widget/g;->f:Z

    .line 182
    invoke-virtual {v0, v1}, Lcnf;->e(Z)Lcnf;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/library/widget/g;->g:Z

    .line 183
    invoke-virtual {v0, v1}, Lcnf;->f(Z)Lcnf;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/library/widget/g;->h:Z

    .line 184
    invoke-virtual {v0, v1}, Lcnf;->d(Z)Lcnf;

    move-result-object v0

    .line 185
    invoke-virtual {v0}, Lcnf;->a()Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 186
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/widget/g;)Lcom/twitter/library/widget/g$b;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/library/widget/g;->n:Lcom/twitter/library/widget/g$b;

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;Landroid/text/SpannableStringBuilder;Landroid/content/Context;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/twitter/model/core/d;",
            ">;",
            "Landroid/text/SpannableStringBuilder;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 163
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 164
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d;

    .line 165
    iget v3, v0, Lcom/twitter/model/core/d;->g:I

    if-ltz v3, :cond_0

    iget v3, v0, Lcom/twitter/model/core/d;->h:I

    if-gt v3, v1, :cond_0

    .line 166
    new-instance v3, Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v4, 0x1

    invoke-direct {v3, p2, v4}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    iget v4, v0, Lcom/twitter/model/core/d;->g:I

    iget v0, v0, Lcom/twitter/model/core/d;->h:I

    const/16 v5, 0x21

    invoke-virtual {p1, v3, v4, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 170
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;Lcom/twitter/model/core/v;Ljava/lang/Iterable;)Landroid/text/SpannableStringBuilder;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Lcom/twitter/model/core/v;",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/twitter/model/core/d;",
            ">;)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 116
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/twitter/library/widget/g;->a:Landroid/content/Context;

    invoke-static {p3, v1, v0}, Lcom/twitter/library/widget/g;->a(Ljava/lang/Iterable;Landroid/text/SpannableStringBuilder;Landroid/content/Context;)V

    .line 120
    invoke-static {}, Lcom/twitter/library/view/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v2, v0

    .line 122
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/library/widget/g;->i:Z

    if-eqz v0, :cond_6

    .line 123
    invoke-direct {p0, p2, v1}, Lcom/twitter/library/widget/g;->a(Lcom/twitter/model/core/v;Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    .line 125
    if-eqz v2, :cond_5

    .line 127
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 128
    invoke-virtual {p2}, Lcom/twitter/model/core/v;->b()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 129
    new-instance v6, Lcom/twitter/library/widget/g$a;

    invoke-direct {v6, v0}, Lcom/twitter/library/widget/g$a;-><init>(Lcom/twitter/model/core/ad;)V

    invoke-virtual {v1, v6}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    :cond_0
    move v2, v3

    .line 120
    goto :goto_0

    .line 131
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 134
    new-instance v1, Lcom/twitter/model/core/v$a;

    invoke-direct {v1, p2}, Lcom/twitter/model/core/v$a;-><init>(Lcom/twitter/model/core/v;)V

    .line 135
    invoke-virtual {v1}, Lcom/twitter/model/core/v$a;->f()Lcom/twitter/model/core/v$a;

    move-result-object v1

    .line 136
    invoke-virtual {v1}, Lcom/twitter/model/core/v$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/v;

    .line 138
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/g$a;

    .line 139
    iget v6, v0, Lcom/twitter/library/widget/g$a;->a:I

    iget v0, v0, Lcom/twitter/library/widget/g$a;->b:I

    invoke-virtual {v1, v6, v0}, Lcom/twitter/model/core/v;->b(II)V

    goto :goto_2

    :cond_2
    move-object v0, v4

    move-object p2, v1

    .line 146
    :goto_3
    if-eqz v2, :cond_3

    .line 147
    iget-object v1, p0, Lcom/twitter/library/widget/g;->a:Landroid/content/Context;

    iget-object v2, p2, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    iget-object v4, p0, Lcom/twitter/library/widget/g;->b:Landroid/view/View;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/twitter/library/view/b;->a(Landroid/content/Context;Ljava/lang/Iterable;Landroid/text/SpannableStringBuilder;ZLandroid/view/View;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 151
    :cond_3
    iget-boolean v1, p0, Lcom/twitter/library/widget/g;->j:Z

    if-eqz v1, :cond_4

    .line 152
    iget-object v1, p0, Lcom/twitter/library/widget/g;->a:Landroid/content/Context;

    invoke-direct {p0, v1, v0}, Lcom/twitter/library/widget/g;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 155
    :cond_4
    return-object v0

    :cond_5
    move-object v0, v4

    goto :goto_3

    :cond_6
    move-object v0, v1

    goto :goto_3
.end method

.method public a(I)Lcom/twitter/library/widget/g;
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 64
    iput p1, p0, Lcom/twitter/library/widget/g;->e:I

    .line 65
    return-object p0
.end method

.method public a(Lcne;)Lcom/twitter/library/widget/g;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/twitter/library/widget/g;->d:Lcne;

    .line 60
    return-object p0
.end method

.method public a(Lcom/twitter/library/widget/g$b;)Lcom/twitter/library/widget/g;
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/twitter/library/widget/g;->n:Lcom/twitter/library/widget/g$b;

    .line 105
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/twitter/library/widget/g;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/twitter/library/widget/g;->l:Ljava/lang/CharSequence;

    .line 100
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/widget/g;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/twitter/library/widget/g;->m:Ljava/lang/String;

    .line 110
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/widget/g;
    .locals 0

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/twitter/library/widget/g;->f:Z

    .line 75
    return-object p0
.end method

.method public b(I)Lcom/twitter/library/widget/g;
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 69
    iput p1, p0, Lcom/twitter/library/widget/g;->k:I

    .line 70
    return-object p0
.end method

.method public b(Z)Lcom/twitter/library/widget/g;
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/twitter/library/widget/g;->g:Z

    .line 80
    return-object p0
.end method

.method public c(Z)Lcom/twitter/library/widget/g;
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/twitter/library/widget/g;->h:Z

    .line 85
    return-object p0
.end method

.method public d(Z)Lcom/twitter/library/widget/g;
    .locals 0

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/twitter/library/widget/g;->i:Z

    .line 90
    return-object p0
.end method

.method public e(Z)Lcom/twitter/library/widget/g;
    .locals 0

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/twitter/library/widget/g;->j:Z

    .line 95
    return-object p0
.end method
