.class Lcom/twitter/library/widget/TweetView$11;
.super Lcnc;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/widget/TweetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/TweetView;


# direct methods
.method constructor <init>(Lcom/twitter/library/widget/TweetView;)V
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-direct {p0}, Lcnc;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/ad;)V
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/view/d;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 455
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/view/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v1}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/twitter/library/view/d;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;)V

    .line 457
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/core/b;)V
    .locals 2

    .prologue
    .line 479
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/view/d;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/view/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v1}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/twitter/library/view/d;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/b;)V

    .line 482
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/core/h;)V
    .locals 2

    .prologue
    .line 472
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/view/d;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/view/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v1}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/twitter/library/view/d;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/h;)V

    .line 475
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/core/q;)V
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/view/d;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/view/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v1}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/twitter/library/view/d;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/q;)V

    .line 489
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/model/core/ad;)Z
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/ad;Lcax;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView$11;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v1}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
