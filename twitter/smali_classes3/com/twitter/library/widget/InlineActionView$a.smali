.class Lcom/twitter/library/widget/InlineActionView$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/widget/InlineActionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field public final a:I

.field public final b:F

.field public final c:I

.field public final d:I

.field public final e:Landroid/content/res/ColorStateList;


# direct methods
.method constructor <init>(IFIILandroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268
    iput p1, p0, Lcom/twitter/library/widget/InlineActionView$a;->a:I

    .line 269
    iput p2, p0, Lcom/twitter/library/widget/InlineActionView$a;->b:F

    .line 270
    iput p3, p0, Lcom/twitter/library/widget/InlineActionView$a;->c:I

    .line 271
    iput p4, p0, Lcom/twitter/library/widget/InlineActionView$a;->d:I

    .line 272
    iput-object p5, p0, Lcom/twitter/library/widget/InlineActionView$a;->e:Landroid/content/res/ColorStateList;

    .line 273
    return-void
.end method

.method public static a(Landroid/content/Context;I)Lcom/twitter/library/widget/InlineActionView$a;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 276
    sget-object v0, Lazw$l;->InlineActionTextStyle:[I

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 277
    sget v1, Lazw$l;->InlineActionTextStyle_textBackground:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 278
    sget v2, Lazw$l;->InlineActionTextStyle_textFontSize:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    .line 279
    sget v3, Lazw$l;->InlineActionTextStyle_textHorizontalPadding:I

    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 280
    sget v4, Lazw$l;->InlineActionTextStyle_textVerticalPadding:I

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 281
    sget v5, Lazw$l;->InlineActionTextStyle_textColor:I

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    .line 282
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 283
    new-instance v0, Lcom/twitter/library/widget/InlineActionView$a;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/widget/InlineActionView$a;-><init>(IFIILandroid/content/res/ColorStateList;)V

    return-object v0
.end method
