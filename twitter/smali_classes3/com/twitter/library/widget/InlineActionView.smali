.class public Lcom/twitter/library/widget/InlineActionView;
.super Landroid/view/ViewGroup;
.source "Twttr"

# interfaces
.implements Lbwu$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/widget/InlineActionView$a;
    }
.end annotation


# static fields
.field private static final a:[I


# instance fields
.field private final b:Landroid/widget/ImageView;

.field private final c:Lcom/twitter/ui/widget/TextLayoutView;

.field private final d:Landroid/graphics/drawable/Drawable;

.field private final e:I

.field private final f:Z

.field private final g:Lcom/twitter/library/widget/InlineActionView$a;

.field private final h:I

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:F

.field private l:I

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/library/widget/InlineActionView;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/InlineActionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/InlineActionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 57
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    iput-boolean v3, p0, Lcom/twitter/library/widget/InlineActionView;->i:Z

    .line 43
    iput-object v5, p0, Lcom/twitter/library/widget/InlineActionView;->j:Ljava/lang/String;

    .line 44
    sget v0, Lcni;->a:F

    invoke-static {v0}, Lcni;->a(F)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/InlineActionView;->k:F

    .line 58
    sget-object v0, Lazw$l;->InlineActionView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 59
    sget v0, Lazw$l;->InlineActionView_inlineActionDrawable:I

    .line 60
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 59
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Landroid/support/v4/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->d:Landroid/graphics/drawable/Drawable;

    .line 61
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->d:Landroid/graphics/drawable/Drawable;

    sget v2, Lazw$l;->InlineActionView_inlineActionTint:I

    .line 62
    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v4/graphics/drawable/DrawableCompat;->setTintList(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 63
    sget v0, Lazw$l;->InlineActionView_iconPaddingNormal:I

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/InlineActionView;->h:I

    .line 64
    sget v0, Lazw$l;->InlineActionView_inlineActionLabelMargin:I

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/InlineActionView;->e:I

    .line 65
    sget v0, Lazw$l;->InlineActionView_showLabel:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/widget/InlineActionView;->f:Z

    .line 66
    iget-boolean v0, p0, Lcom/twitter/library/widget/InlineActionView;->f:Z

    if-eqz v0, :cond_0

    .line 67
    sget v0, Lazw$l;->InlineActionView_labelTextStyle:I

    .line 68
    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 67
    invoke-static {p1, v0}, Lcom/twitter/library/widget/InlineActionView$a;->a(Landroid/content/Context;I)Lcom/twitter/library/widget/InlineActionView$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->g:Lcom/twitter/library/widget/InlineActionView$a;

    .line 72
    :goto_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 74
    new-instance v0, Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/ui/widget/TextLayoutView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    .line 75
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/TextLayoutView;->setDuplicateParentStateEnabled(Z)V

    .line 76
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/TextLayoutView;->a(Z)Lcom/twitter/ui/widget/TextLayoutView;

    .line 77
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-static {p1}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TextLayoutView;->a(Landroid/graphics/Typeface;)Lcom/twitter/ui/widget/TextLayoutView;

    .line 78
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineActionView;->addView(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->g:Lcom/twitter/library/widget/InlineActionView$a;

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/InlineActionView;->a(Lcom/twitter/library/widget/InlineActionView$a;)V

    .line 80
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineActionView;->setLabel(Ljava/lang/String;)V

    .line 82
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    .line 83
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setDuplicateParentStateEnabled(Z)V

    .line 84
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 85
    iget v0, p0, Lcom/twitter/library/widget/InlineActionView;->h:I

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/InlineActionView;->a(I)V

    .line 86
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/library/widget/InlineActionView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 87
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineActionView;->addView(Landroid/view/View;)V

    .line 90
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/widget/InlineActionView;->m:Z

    .line 91
    return-void

    .line 70
    :cond_0
    iput-object v5, p0, Lcom/twitter/library/widget/InlineActionView;->g:Lcom/twitter/library/widget/InlineActionView$a;

    goto :goto_0
.end method

.method private a(I)V
    .locals 5

    .prologue
    .line 238
    iget v0, p0, Lcom/twitter/library/widget/InlineActionView;->e:I

    sub-int v0, p1, v0

    iput v0, p0, Lcom/twitter/library/widget/InlineActionView;->l:I

    .line 239
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    mul-int/lit8 v2, p1, 0x2

    iget-object v3, p0, Lcom/twitter/library/widget/InlineActionView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/2addr v2, v3

    mul-int/lit8 v3, p1, 0x2

    iget-object v4, p0, Lcom/twitter/library/widget/InlineActionView;->d:Landroid/graphics/drawable/Drawable;

    .line 240
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 239
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 241
    return-void
.end method

.method private static a(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 256
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 257
    return-void
.end method

.method private a(Lcom/twitter/library/widget/InlineActionView$a;)V
    .locals 5

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/twitter/library/widget/InlineActionView;->f:Z

    if-eqz v0, :cond_0

    .line 229
    iget v0, p1, Lcom/twitter/library/widget/InlineActionView$a;->a:I

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/InlineActionView;->setupTextBackground(I)V

    .line 230
    iget v0, p1, Lcom/twitter/library/widget/InlineActionView$a;->b:F

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/InlineActionView;->setupTextSize(F)V

    .line 231
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    iget v1, p1, Lcom/twitter/library/widget/InlineActionView$a;->c:I

    iget v2, p1, Lcom/twitter/library/widget/InlineActionView$a;->d:I

    iget v3, p1, Lcom/twitter/library/widget/InlineActionView$a;->c:I

    iget v4, p1, Lcom/twitter/library/widget/InlineActionView$a;->d:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/ui/widget/TextLayoutView;->setPadding(IIII)V

    .line 233
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    iget-object v1, p1, Lcom/twitter/library/widget/InlineActionView$a;->e:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TextLayoutView;->a(Landroid/content/res/ColorStateList;)Lcom/twitter/ui/widget/TextLayoutView;

    .line 235
    :cond_0
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/twitter/library/widget/InlineActionView;->m:Z

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TextLayoutView;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/library/widget/InlineActionView;->l:I

    add-int/2addr v0, v1

    .line 224
    :goto_0
    iget-object v1, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionView;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v3}, Lcom/twitter/ui/widget/TextLayoutView;->getMeasuredHeight()I

    move-result v3

    invoke-static {v2, v3}, Lcom/twitter/util/ui/k;->b(II)I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/twitter/library/widget/InlineActionView;->a(Landroid/view/View;II)V

    .line 225
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getRight()I

    move-result v0

    iget v1, p0, Lcom/twitter/library/widget/InlineActionView;->l:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/twitter/library/widget/InlineActionView;->f:Z

    return v0
.end method

.method private setToggleOn(Z)V
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lcom/twitter/library/widget/InlineActionView;->i:Z

    if-eq v0, p1, :cond_0

    .line 212
    iput-boolean p1, p0, Lcom/twitter/library/widget/InlineActionView;->i:Z

    .line 213
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionView;->refreshDrawableState()V

    .line 215
    :cond_0
    return-void
.end method

.method private setupTextBackground(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 248
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TextLayoutView;->setBackgroundResource(I)V

    .line 249
    return-void
.end method

.method private setupTextSize(F)V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    iget v1, p0, Lcom/twitter/library/widget/InlineActionView;->k:F

    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TextLayoutView;->a(F)Lcom/twitter/ui/widget/TextLayoutView;

    .line 245
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/library/widget/InlineActionView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 198
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 1

    .prologue
    .line 184
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 185
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 186
    return-void
.end method

.method public getIconView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getTextView()Landroid/view/View;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    return-object v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 175
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 176
    iget-boolean v1, p0, Lcom/twitter/library/widget/InlineActionView;->i:Z

    if-eqz v1, :cond_0

    .line 177
    sget-object v1, Lcom/twitter/library/widget/InlineActionView;->a:[I

    invoke-static {v0, v1}, Lcom/twitter/library/widget/InlineActionView;->mergeDrawableStates([I[I)[I

    .line 179
    :cond_0
    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionView;->getWidth()I

    move-result v0

    .line 158
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionView;->getHeight()I

    move-result v1

    .line 161
    iget-boolean v2, p0, Lcom/twitter/library/widget/InlineActionView;->m:Z

    if-eqz v2, :cond_1

    .line 162
    iget-object v2, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    .line 166
    :goto_0
    iget-object v2, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    invoke-static {v1, v3}, Lcom/twitter/util/ui/k;->b(II)I

    move-result v1

    invoke-static {v2, v0, v1}, Lcom/twitter/library/widget/InlineActionView;->a(Landroid/view/View;II)V

    .line 168
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TextLayoutView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 169
    invoke-direct {p0}, Lcom/twitter/library/widget/InlineActionView;->b()V

    .line 171
    :cond_0
    return-void

    .line 164
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 142
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/widget/InlineActionView;->measureChildren(II)V

    .line 144
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TextLayoutView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 145
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    .line 146
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v0

    .line 152
    :goto_0
    invoke-static {v1, p1}, Lcom/twitter/library/widget/InlineActionView;->resolveSize(II)I

    move-result v1

    invoke-static {v0, p2}, Lcom/twitter/library/widget/InlineActionView;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/widget/InlineActionView;->setMeasuredDimension(II)V

    .line 153
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    .line 149
    iget-object v1, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TextLayoutView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/library/widget/InlineActionView;->e:I

    add-int/2addr v1, v0

    .line 150
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v0

    iget-object v2, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v2}, Lcom/twitter/ui/widget/TextLayoutView;->getMeasuredHeight()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public setBylineSize(F)V
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/twitter/library/widget/InlineActionView;->f:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/widget/InlineActionView;->k:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 190
    iput p1, p0, Lcom/twitter/library/widget/InlineActionView;->k:F

    .line 191
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->g:Lcom/twitter/library/widget/InlineActionView$a;

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/InlineActionView;->a(Lcom/twitter/library/widget/InlineActionView$a;)V

    .line 192
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionView;->requestLayout()V

    .line 194
    :cond_0
    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/twitter/library/widget/InlineActionView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TextLayoutView;->setTextWithVisibility(Ljava/lang/CharSequence;)V

    .line 130
    :goto_0
    iput-object p1, p0, Lcom/twitter/library/widget/InlineActionView;->j:Ljava/lang/String;

    .line 131
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->c:Lcom/twitter/ui/widget/TextLayoutView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TextLayoutView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setLabelOnLeft(Z)V
    .locals 0

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/twitter/library/widget/InlineActionView;->m:Z

    .line 137
    return-void
.end method

.method public setState(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 95
    .line 98
    packed-switch p1, :pswitch_data_0

    move v2, v0

    move v3, v1

    .line 116
    :goto_0
    if-eqz v3, :cond_0

    :goto_1
    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineActionView;->setVisibility(I)V

    .line 117
    invoke-direct {p0, v2}, Lcom/twitter/library/widget/InlineActionView;->setToggleOn(Z)V

    .line 118
    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/InlineActionView;->setEnabled(Z)V

    .line 120
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionView;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineActionView;->setLabel(Ljava/lang/String;)V

    .line 121
    return-void

    :pswitch_0
    move v2, v0

    move v3, v0

    .line 101
    goto :goto_0

    :pswitch_1
    move v2, v1

    move v3, v1

    .line 105
    goto :goto_0

    :pswitch_2
    move v2, v0

    move v3, v1

    move v1, v0

    .line 109
    goto :goto_0

    .line 116
    :cond_0
    const/16 v0, 0x8

    goto :goto_1

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
