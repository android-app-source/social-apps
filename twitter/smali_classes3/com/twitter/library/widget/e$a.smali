.class final Lcom/twitter/library/widget/e$a;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/widget/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field final a:Landroid/graphics/drawable/BitmapDrawable;

.field final b:Lcom/twitter/library/util/ab;

.field final c:Lcom/twitter/library/util/ad;

.field final d:Landroid/graphics/Paint;

.field e:J


# direct methods
.method constructor <init>(Landroid/graphics/drawable/BitmapDrawable;Lcom/twitter/library/util/ab;Lcom/twitter/library/util/ad;Landroid/graphics/Paint;J)V
    .locals 1

    .prologue
    .line 396
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    .line 397
    iput-object p1, p0, Lcom/twitter/library/widget/e$a;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 398
    iput-object p2, p0, Lcom/twitter/library/widget/e$a;->b:Lcom/twitter/library/util/ab;

    .line 399
    iput-object p3, p0, Lcom/twitter/library/widget/e$a;->c:Lcom/twitter/library/util/ad;

    .line 400
    iput-object p4, p0, Lcom/twitter/library/widget/e$a;->d:Landroid/graphics/Paint;

    .line 401
    iput-wide p5, p0, Lcom/twitter/library/widget/e$a;->e:J

    .line 402
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/widget/e$a;)V
    .locals 2

    .prologue
    .line 407
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    .line 409
    iget-object v0, p1, Lcom/twitter/library/widget/e$a;->a:Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/twitter/library/widget/e$a;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 412
    iget-object v0, p1, Lcom/twitter/library/widget/e$a;->b:Lcom/twitter/library/util/ab;

    iput-object v0, p0, Lcom/twitter/library/widget/e$a;->b:Lcom/twitter/library/util/ab;

    .line 417
    iget-object v0, p1, Lcom/twitter/library/widget/e$a;->c:Lcom/twitter/library/util/ad;

    iput-object v0, p0, Lcom/twitter/library/widget/e$a;->c:Lcom/twitter/library/util/ad;

    .line 418
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p1, Lcom/twitter/library/widget/e$a;->d:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/twitter/library/widget/e$a;->d:Landroid/graphics/Paint;

    .line 419
    iget-wide v0, p1, Lcom/twitter/library/widget/e$a;->e:J

    iput-wide v0, p0, Lcom/twitter/library/widget/e$a;->e:J

    .line 420
    return-void
.end method


# virtual methods
.method public canApplyTheme()Z
    .locals 1

    .prologue
    .line 427
    const/4 v0, 0x0

    return v0
.end method

.method public getChangingConfigurations()I
    .locals 1

    .prologue
    .line 448
    const/4 v0, 0x0

    return v0
.end method

.method public newDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 432
    new-instance v0, Lcom/twitter/library/widget/e;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/e;-><init>(Lcom/twitter/library/widget/e$a;)V

    return-object v0
.end method

.method public newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 437
    new-instance v0, Lcom/twitter/library/widget/e;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/e;-><init>(Lcom/twitter/library/widget/e$a;)V

    return-object v0
.end method

.method public newDrawable(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 443
    new-instance v0, Lcom/twitter/library/widget/e;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/e;-><init>(Lcom/twitter/library/widget/e$a;)V

    return-object v0
.end method
