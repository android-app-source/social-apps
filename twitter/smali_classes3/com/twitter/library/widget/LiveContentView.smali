.class public Lcom/twitter/library/widget/LiveContentView;
.super Landroid/widget/FrameLayout;
.source "Twttr"


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/ImageView;

.field private d:Lcom/twitter/model/timeline/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    sget v0, Lazw$h;->live_content_view:I

    invoke-static {p1, v0, p0}, Lcom/twitter/library/widget/LiveContentView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 37
    sget v0, Lazw$g;->header_text:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/LiveContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/library/widget/LiveContentView;->a:Landroid/widget/TextView;

    .line 38
    sget v0, Lazw$g;->detail_text:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/LiveContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/library/widget/LiveContentView;->b:Landroid/widget/TextView;

    .line 39
    sget v0, Lazw$g;->caret:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/LiveContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/library/widget/LiveContentView;->c:Landroid/widget/ImageView;

    .line 40
    invoke-virtual {p0}, Lcom/twitter/library/widget/LiveContentView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lazw$f;->ic_chevron_down:I

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 41
    invoke-virtual {p0}, Lcom/twitter/library/widget/LiveContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lazw$d;->caret:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v0, v1}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 42
    iget-object v1, p0, Lcom/twitter/library/widget/LiveContentView;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 43
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/LiveContentView;->a(Z)V

    .line 44
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    .prologue
    .line 77
    iget-object v1, p0, Lcom/twitter/library/widget/LiveContentView;->c:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 78
    return-void

    .line 77
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public getBannerPrompt()Lcom/twitter/model/timeline/b;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/library/widget/LiveContentView;->d:Lcom/twitter/model/timeline/b;

    return-object v0
.end method

.method public getCaretView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/widget/LiveContentView;->c:Landroid/widget/ImageView;

    return-object v0
.end method

.method public setBannerPrompt(Lcom/twitter/model/timeline/b;)V
    .locals 3

    .prologue
    .line 58
    iput-object p1, p0, Lcom/twitter/library/widget/LiveContentView;->d:Lcom/twitter/model/timeline/b;

    .line 59
    iget-object v0, p0, Lcom/twitter/library/widget/LiveContentView;->d:Lcom/twitter/model/timeline/b;

    if-nez v0, :cond_0

    .line 60
    const-string/jumbo v1, ""

    .line 61
    const-string/jumbo v0, ""

    .line 66
    :goto_0
    iget-object v2, p0, Lcom/twitter/library/widget/LiveContentView;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v1, p0, Lcom/twitter/library/widget/LiveContentView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/LiveContentView;->d:Lcom/twitter/model/timeline/b;

    iget-object v1, v0, Lcom/twitter/model/timeline/b;->b:Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lcom/twitter/library/widget/LiveContentView;->d:Lcom/twitter/model/timeline/b;

    iget-object v0, v0, Lcom/twitter/model/timeline/b;->c:Ljava/lang/String;

    goto :goto_0
.end method
