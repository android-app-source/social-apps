.class public Lcom/twitter/library/widget/InlineActionBar;
.super Landroid/view/ViewGroup;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/widget/InlineActionBar$a;,
        Lcom/twitter/library/widget/InlineActionBar$b;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TweetActionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TweetActionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TweetActionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TweetActionType;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Z

.field private static final f:Landroid/graphics/Paint;


# instance fields
.field private g:Landroid/graphics/Bitmap;

.field private final h:Z

.field private final i:I

.field private final j:F

.field private final k:Z

.field private final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/core/TweetActionType;",
            "Lbwu;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbwu;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TweetActionType;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcom/twitter/model/core/Tweet;

.field private p:Z

.field private q:F

.field private r:Lcom/twitter/model/util/FriendshipCache;

.field private s:Lbwt;

.field private t:Lcom/twitter/ui/anim/e;

.field private u:Lcom/twitter/library/widget/InlineActionBar$b;

.field private v:J


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 57
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->d:Lcom/twitter/model/core/TweetActionType;

    new-array v1, v6, [Lcom/twitter/model/core/TweetActionType;

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->c:Lcom/twitter/model/core/TweetActionType;

    aput-object v2, v1, v4

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->b:Lcom/twitter/model/core/TweetActionType;

    aput-object v2, v1, v3

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->o:Lcom/twitter/model/core/TweetActionType;

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/widget/InlineActionBar;->a:Ljava/util/List;

    .line 59
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->d:Lcom/twitter/model/core/TweetActionType;

    new-array v1, v6, [Lcom/twitter/model/core/TweetActionType;

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->c:Lcom/twitter/model/core/TweetActionType;

    aput-object v2, v1, v4

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->b:Lcom/twitter/model/core/TweetActionType;

    aput-object v2, v1, v3

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->n:Lcom/twitter/model/core/TweetActionType;

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/widget/InlineActionBar;->b:Ljava/util/List;

    .line 61
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->b:Lcom/twitter/model/core/TweetActionType;

    new-array v1, v5, [Lcom/twitter/model/core/TweetActionType;

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->c:Lcom/twitter/model/core/TweetActionType;

    aput-object v2, v1, v4

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->d:Lcom/twitter/model/core/TweetActionType;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/widget/InlineActionBar;->c:Ljava/util/List;

    .line 63
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->b:Lcom/twitter/model/core/TweetActionType;

    new-array v1, v5, [Lcom/twitter/model/core/TweetActionType;

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->d:Lcom/twitter/model/core/TweetActionType;

    aput-object v2, v1, v4

    sget-object v2, Lcom/twitter/model/core/TweetActionType;->c:Lcom/twitter/model/core/TweetActionType;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/widget/InlineActionBar;->d:Ljava/util/List;

    .line 66
    invoke-static {}, Lcrt;->a()Z

    move-result v0

    sput-boolean v0, Lcom/twitter/library/widget/InlineActionBar;->e:Z

    .line 67
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/twitter/library/widget/InlineActionBar;->f:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/InlineActionBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 103
    sget v0, Lazw$c;->inlineActionBarStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/InlineActionBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 107
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 81
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/widget/InlineActionBar;->k:Z

    .line 83
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/twitter/model/core/TweetActionType;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->l:Ljava/util/Map;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->m:Ljava/util/List;

    .line 96
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/library/widget/InlineActionBar;->v:J

    .line 108
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 110
    sget-object v1, Lazw$l;->InlineActionBar:[I

    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 111
    sget v2, Lazw$l;->InlineActionBar_displayBorder:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/library/widget/InlineActionBar;->h:Z

    .line 112
    sget v2, Lazw$d;->light_gray:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/InlineActionBar;->i:I

    .line 113
    sget v0, Lazw$l;->InlineActionBar_inlineActionBorderWidth:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/twitter/library/widget/InlineActionBar;->j:F

    .line 114
    sget v0, Lcni;->a:F

    invoke-static {v0}, Lcni;->a(F)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/InlineActionBar;->q:F

    .line 115
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 116
    return-void
.end method

.method private a(I)I
    .locals 2

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x4

    .line 277
    add-int/2addr v0, v1

    .line 278
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 279
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/library/widget/InlineActionBar;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/twitter/library/widget/InlineActionBar;->g:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/library/widget/InlineActionBar;)Lcom/twitter/library/widget/InlineActionBar$b;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->u:Lcom/twitter/library/widget/InlineActionBar$b;

    return-object v0
.end method

.method static synthetic a(Lbwu;)Lcom/twitter/library/widget/InlineActionView;
    .locals 1

    .prologue
    .line 55
    invoke-static {p0}, Lcom/twitter/library/widget/InlineActionBar;->c(Lbwu;)Lcom/twitter/library/widget/InlineActionView;

    move-result-object v0

    return-object v0
.end method

.method private static a(IILcom/twitter/library/widget/InlineActionView;)V
    .locals 2

    .prologue
    .line 256
    invoke-virtual {p2}, Lcom/twitter/library/widget/InlineActionView;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, p0

    invoke-virtual {p2}, Lcom/twitter/library/widget/InlineActionView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p2, p0, p1, v0, v1}, Lcom/twitter/library/widget/InlineActionView;->layout(IIII)V

    .line 257
    return-void
.end method

.method private a(ILcom/twitter/library/widget/InlineActionView;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 232
    invoke-virtual {p2}, Lcom/twitter/library/widget/InlineActionView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 253
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/widget/InlineActionBar;->p:Z

    if-nez v0, :cond_1

    invoke-static {}, Lcmj;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x3

    .line 238
    :goto_1
    if-lt p1, v0, :cond_4

    .line 240
    iget-boolean v0, p0, Lcom/twitter/library/widget/InlineActionBar;->k:Z

    if-eqz v0, :cond_3

    .line 241
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getPaddingLeft()I

    move-result v0

    invoke-static {v0, v2, p2}, Lcom/twitter/library/widget/InlineActionBar;->a(IILcom/twitter/library/widget/InlineActionView;)V

    goto :goto_0

    .line 236
    :cond_2
    const/4 v0, 0x4

    goto :goto_1

    .line 243
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p2}, Lcom/twitter/library/widget/InlineActionView;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0, v2, p2}, Lcom/twitter/library/widget/InlineActionBar;->a(IILcom/twitter/library/widget/InlineActionView;)V

    goto :goto_0

    .line 246
    :cond_4
    iget-boolean v0, p0, Lcom/twitter/library/widget/InlineActionBar;->k:Z

    if-eqz v0, :cond_5

    .line 247
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    mul-int v1, p3, p1

    sub-int/2addr v0, v1

    invoke-virtual {p2}, Lcom/twitter/library/widget/InlineActionView;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0, v2, p2}, Lcom/twitter/library/widget/InlineActionBar;->a(IILcom/twitter/library/widget/InlineActionView;)V

    goto :goto_0

    .line 250
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getPaddingLeft()I

    move-result v0

    mul-int v1, p3, p1

    add-int/2addr v0, v1

    invoke-static {v0, v2, p2}, Lcom/twitter/library/widget/InlineActionBar;->a(IILcom/twitter/library/widget/InlineActionView;)V

    goto :goto_0
.end method

.method private a(Lbwu;Z)V
    .locals 3

    .prologue
    .line 185
    sget-boolean v0, Lcom/twitter/library/widget/InlineActionBar;->e:Z

    if-eqz v0, :cond_1

    .line 186
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->u:Lcom/twitter/library/widget/InlineActionBar$b;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->u:Lcom/twitter/library/widget/InlineActionBar$b;

    invoke-virtual {p1}, Lbwu;->a()Lcom/twitter/model/core/TweetActionType;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/library/widget/InlineActionBar$b;->a(Lcom/twitter/model/core/TweetActionType;)V

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    invoke-static {p1}, Lcom/twitter/library/widget/InlineActionBar;->c(Lbwu;)Lcom/twitter/library/widget/InlineActionView;

    move-result-object v0

    .line 193
    invoke-virtual {p1}, Lbwu;->a()Lcom/twitter/model/core/TweetActionType;

    move-result-object v1

    .line 194
    sget-object v2, Lcom/twitter/model/core/TweetActionType;->b:Lcom/twitter/model/core/TweetActionType;

    if-ne v1, v2, :cond_3

    invoke-static {}, Lcom/twitter/library/widget/InlineActionBar;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 195
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->t:Lcom/twitter/ui/anim/e;

    if-nez v0, :cond_2

    .line 196
    new-instance v0, Lcom/twitter/ui/anim/e;

    invoke-direct {v0, p0}, Lcom/twitter/ui/anim/e;-><init>(Landroid/view/View;)V

    .line 197
    invoke-static {}, Lbpm;->a()Lbpm;

    move-result-object v1

    invoke-virtual {v1}, Lbpm;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/anim/e;->a(Ljava/util/List;)Lcom/twitter/ui/anim/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->t:Lcom/twitter/ui/anim/e;

    .line 199
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->t:Lcom/twitter/ui/anim/e;

    new-instance v1, Lcom/twitter/library/widget/InlineActionBar$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, p2, v2}, Lcom/twitter/library/widget/InlineActionBar$a;-><init>(Lcom/twitter/library/widget/InlineActionBar;Lbwu;ZLcom/twitter/library/widget/InlineActionBar$1;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/anim/e;->a(Lcom/twitter/ui/anim/e$c;)Lcom/twitter/ui/anim/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/ui/anim/e;->a()V

    goto :goto_0

    .line 201
    :cond_3
    invoke-static {}, Lcom/twitter/library/widget/InlineActionBar;->c()Landroid/view/animation/Animation;

    move-result-object v1

    .line 202
    new-instance v2, Lcom/twitter/library/widget/InlineActionBar$1;

    invoke-direct {v2, p0, p2, p1}, Lcom/twitter/library/widget/InlineActionBar$1;-><init>(Lcom/twitter/library/widget/InlineActionBar;ZLbwu;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 210
    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionView;->getIconView()Landroid/widget/ImageView;

    move-result-object v0

    .line 211
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 212
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/library/widget/InlineActionBar;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->o:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method private static b(I)Lcom/twitter/model/core/TweetActionType;
    .locals 3
    .param p0    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 475
    sget v0, Lazw$g;->inline_reply:I

    if-ne p0, v0, :cond_0

    .line 476
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->d:Lcom/twitter/model/core/TweetActionType;

    .line 484
    :goto_0
    return-object v0

    .line 477
    :cond_0
    sget v0, Lazw$g;->inline_retweet:I

    if-ne p0, v0, :cond_1

    .line 478
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->c:Lcom/twitter/model/core/TweetActionType;

    goto :goto_0

    .line 479
    :cond_1
    sget v0, Lazw$g;->inline_like:I

    if-ne p0, v0, :cond_2

    .line 480
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->b:Lcom/twitter/model/core/TweetActionType;

    goto :goto_0

    .line 481
    :cond_2
    sget v0, Lazw$g;->inline_analytics:I

    if-ne p0, v0, :cond_3

    .line 482
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->o:Lcom/twitter/model/core/TweetActionType;

    goto :goto_0

    .line 483
    :cond_3
    sget v0, Lazw$g;->inline_dm:I

    if-ne p0, v0, :cond_4

    .line 484
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->n:Lcom/twitter/model/core/TweetActionType;

    goto :goto_0

    .line 486
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unexpected id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(Lbwu;)V
    .locals 2

    .prologue
    .line 470
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->l:Ljava/util/Map;

    invoke-virtual {p1}, Lbwu;->a()Lcom/twitter/model/core/TweetActionType;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    return-void
.end method

.method private b(Lcom/twitter/model/core/TweetActionType;)V
    .locals 2

    .prologue
    .line 171
    sget-boolean v0, Lcni;->b:Z

    if-eqz v0, :cond_0

    .line 172
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->d:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    .line 174
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/r;->a(Landroid/content/Context;)Lcom/twitter/library/util/r;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/util/r;->a(I)I

    .line 176
    :cond_0
    return-void

    .line 172
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static c()Landroid/view/animation/Animation;
    .locals 16

    .prologue
    const v2, 0x3fb33333    # 1.4f

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    .line 365
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 368
    const-wide/16 v8, 0x55

    invoke-virtual {v0, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 369
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 371
    new-instance v7, Landroid/view/animation/ScaleAnimation;

    move v8, v2

    move v9, v1

    move v10, v2

    move v11, v1

    move v12, v5

    move v13, v6

    move v14, v5

    move v15, v6

    invoke-direct/range {v7 .. v15}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 374
    const-wide/16 v2, 0xa5

    invoke-virtual {v7, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 376
    new-instance v1, Landroid/view/animation/AnimationSet;

    invoke-direct {v1, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 377
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 378
    invoke-virtual {v1, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 380
    return-object v1
.end method

.method private static c(Lbwu;)Lcom/twitter/library/widget/InlineActionView;
    .locals 1

    .prologue
    .line 514
    invoke-virtual {p0}, Lbwu;->d()Lbwu$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/InlineActionView;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v2, v0, 0x4

    .line 224
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 225
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 226
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwu;

    invoke-static {v0}, Lcom/twitter/library/widget/InlineActionBar;->c(Lbwu;)Lcom/twitter/library/widget/InlineActionView;

    move-result-object v0

    .line 227
    invoke-direct {p0, v1, v0, v2}, Lcom/twitter/library/widget/InlineActionBar;->a(ILcom/twitter/library/widget/InlineActionView;I)V

    .line 225
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 229
    :cond_0
    return-void
.end method

.method private e()Z
    .locals 6

    .prologue
    .line 503
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 504
    iget-wide v2, p0, Lcom/twitter/library/widget/InlineActionBar;->v:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/twitter/library/widget/InlineActionBar;->v:J

    sub-long v2, v0, v2

    invoke-static {}, Landroid/view/ViewConfiguration;->getJumpTapTimeout()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 505
    :cond_0
    iput-wide v0, p0, Lcom/twitter/library/widget/InlineActionBar;->v:J

    .line 506
    const/4 v0, 0x1

    .line 508
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f()Z
    .locals 1

    .prologue
    .line 518
    invoke-static {}, Lbpm;->a()Lbpm;

    move-result-object v0

    invoke-virtual {v0}, Lbpm;->b()Z

    move-result v0

    return v0
.end method

.method private getDesiredHeight()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 491
    .line 492
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getChildCount()I

    move-result v2

    move v1, v0

    .line 493
    :goto_0
    if-ge v1, v2, :cond_1

    .line 494
    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/InlineActionBar;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 495
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 496
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 493
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 499
    :cond_1
    return v0
.end method

.method private getInlineActionConfig()Lbwt;
    .locals 3

    .prologue
    .line 424
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->s:Lbwt;

    if-nez v0, :cond_0

    .line 425
    new-instance v0, Lbwt;

    iget-object v1, p0, Lcom/twitter/library/widget/InlineActionBar;->r:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbwt;-><init>(Lcom/twitter/model/util/FriendshipCache;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->s:Lbwt;

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->s:Lbwt;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    iput-object v1, v0, Lbwt;->c:Lcom/twitter/model/core/TwitterUser;

    .line 428
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->s:Lbwt;

    return-object v0
.end method

.method private setupChildView(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 432
    instance-of v0, p1, Lcom/twitter/library/widget/InlineActionView;

    if-nez v0, :cond_0

    .line 467
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 436
    check-cast v0, Lcom/twitter/library/widget/InlineActionView;

    .line 437
    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/InlineActionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 438
    iget v1, p0, Lcom/twitter/library/widget/InlineActionBar;->q:F

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/InlineActionView;->setBylineSize(F)V

    .line 440
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/InlineActionView;->setSoundEffectsEnabled(Z)V

    .line 441
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Lcom/twitter/library/widget/InlineActionBar;->b(I)Lcom/twitter/model/core/TweetActionType;

    move-result-object v1

    .line 442
    sget-object v2, Lcom/twitter/library/widget/InlineActionBar$2;->a:[I

    invoke-virtual {v1}, Lcom/twitter/model/core/TweetActionType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 444
    :pswitch_0
    new-instance v1, Lbwv;

    invoke-direct {v1, v0}, Lbwv;-><init>(Lbwu$a;)V

    invoke-direct {p0, v1}, Lcom/twitter/library/widget/InlineActionBar;->b(Lbwu;)V

    goto :goto_0

    .line 448
    :pswitch_1
    new-instance v1, Lbww;

    invoke-direct {v1, v0}, Lbww;-><init>(Lbwu$a;)V

    invoke-direct {p0, v1}, Lcom/twitter/library/widget/InlineActionBar;->b(Lbwu;)V

    goto :goto_0

    .line 452
    :pswitch_2
    new-instance v1, Lbws;

    invoke-direct {v1, v0}, Lbws;-><init>(Lbwu$a;)V

    invoke-direct {p0, v1}, Lcom/twitter/library/widget/InlineActionBar;->b(Lbwu;)V

    goto :goto_0

    .line 456
    :pswitch_3
    new-instance v1, Lbwq;

    invoke-direct {v1, v0}, Lbwq;-><init>(Lbwu$a;)V

    invoke-direct {p0, v1}, Lcom/twitter/library/widget/InlineActionBar;->b(Lbwu;)V

    goto :goto_0

    .line 460
    :pswitch_4
    new-instance v1, Lbwx;

    invoke-direct {v1, v0}, Lbwx;-><init>(Lbwu$a;)V

    invoke-direct {p0, v1}, Lcom/twitter/library/widget/InlineActionBar;->b(Lbwu;)V

    goto :goto_0

    .line 442
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/TweetActionType;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwu;

    .line 407
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbwu;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 304
    invoke-static {}, Lcmj;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    sget-object v0, Lcom/twitter/library/widget/InlineActionBar;->c:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineActionBar;->setInlineActionTypes(Ljava/util/List;)V

    .line 313
    :goto_0
    return-void

    .line 306
    :cond_0
    invoke-static {}, Lcmj;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    sget-object v0, Lcom/twitter/library/widget/InlineActionBar;->d:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineActionBar;->setInlineActionTypes(Ljava/util/List;)V

    goto :goto_0

    .line 308
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/library/widget/InlineActionBar;->p:Z

    if-nez v0, :cond_2

    invoke-static {}, Lcmj;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 309
    :cond_2
    sget-object v0, Lcom/twitter/library/widget/InlineActionBar;->a:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineActionBar;->setInlineActionTypes(Ljava/util/List;)V

    goto :goto_0

    .line 311
    :cond_3
    sget-object v0, Lcom/twitter/library/widget/InlineActionBar;->b:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineActionBar;->setInlineActionTypes(Ljava/util/List;)V

    goto :goto_0
.end method

.method public a(Landroid/graphics/Canvas;Landroid/view/ViewGroup;)V
    .locals 5

    .prologue
    .line 522
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->l:Ljava/util/Map;

    sget-object v1, Lcom/twitter/model/core/TweetActionType;->b:Lcom/twitter/model/core/TweetActionType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwu;

    .line 524
    invoke-static {v0}, Lcom/twitter/library/widget/InlineActionBar;->c(Lbwu;)Lcom/twitter/library/widget/InlineActionView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/widget/InlineActionView;->getIconView()Landroid/widget/ImageView;

    move-result-object v0

    .line 525
    invoke-static {v0, p2}, Lcom/twitter/util/ui/k;->a(Landroid/view/View;Landroid/view/View;)I

    move-result v1

    .line 526
    invoke-static {v0, p2}, Lcom/twitter/util/ui/k;->b(Landroid/view/View;Landroid/view/View;)I

    move-result v2

    .line 527
    iget-object v3, p0, Lcom/twitter/library/widget/InlineActionBar;->t:Lcom/twitter/ui/anim/e;

    invoke-virtual {v3}, Lcom/twitter/ui/anim/e;->b()Lcom/twitter/util/math/Size;

    move-result-object v3

    .line 528
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    .line 529
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    .line 530
    invoke-virtual {v3}, Lcom/twitter/util/math/Size;->a()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 531
    invoke-virtual {v3}, Lcom/twitter/util/math/Size;->b()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    .line 532
    iget-object v2, p0, Lcom/twitter/library/widget/InlineActionBar;->g:Landroid/graphics/Bitmap;

    int-to-float v1, v1

    int-to-float v0, v0

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v1, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 534
    :cond_0
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    .line 347
    iget-object v2, p0, Lcom/twitter/library/widget/InlineActionBar;->o:Lcom/twitter/model/core/Tweet;

    .line 348
    if-nez v2, :cond_1

    .line 357
    :cond_0
    return-void

    .line 352
    :cond_1
    invoke-direct {p0}, Lcom/twitter/library/widget/InlineActionBar;->getInlineActionConfig()Lbwt;

    move-result-object v3

    .line 353
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .line 354
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 355
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwu;

    invoke-virtual {v0, v2, v3}, Lbwu;->c(Lcom/twitter/model/core/Tweet;Lbwt;)Z

    .line 354
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 284
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 285
    iget-boolean v0, p0, Lcom/twitter/library/widget/InlineActionBar;->h:Z

    if-eqz v0, :cond_0

    .line 286
    sget-object v5, Lcom/twitter/library/widget/InlineActionBar;->f:Landroid/graphics/Paint;

    .line 287
    iget v0, p0, Lcom/twitter/library/widget/InlineActionBar;->i:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 288
    iget v0, p0, Lcom/twitter/library/widget/InlineActionBar;->j:F

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 289
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget v4, p0, Lcom/twitter/library/widget/InlineActionBar;->j:F

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 291
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->u:Lcom/twitter/library/widget/InlineActionBar$b;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/widget/InlineActionBar;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/library/widget/InlineActionBar;->b(I)Lcom/twitter/model/core/TweetActionType;

    move-result-object v1

    .line 134
    invoke-direct {p0, v1}, Lcom/twitter/library/widget/InlineActionBar;->b(Lcom/twitter/model/core/TweetActionType;)V

    .line 135
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->l:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwu;

    .line 136
    sget-object v2, Lcom/twitter/library/widget/InlineActionBar$2;->a:[I

    invoke-virtual {v1}, Lcom/twitter/model/core/TweetActionType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 138
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->u:Lcom/twitter/library/widget/InlineActionBar$b;

    invoke-interface {v0, v1}, Lcom/twitter/library/widget/InlineActionBar$b;->a(Lcom/twitter/model/core/TweetActionType;)V

    goto :goto_0

    .line 142
    :pswitch_1
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/widget/InlineActionBar;->a(Lbwu;Z)V

    .line 143
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->u:Lcom/twitter/library/widget/InlineActionBar$b;

    invoke-interface {v0, v1}, Lcom/twitter/library/widget/InlineActionBar$b;->a(Lcom/twitter/model/core/TweetActionType;)V

    goto :goto_0

    .line 147
    :pswitch_2
    iget-object v2, p0, Lcom/twitter/library/widget/InlineActionBar;->o:Lcom/twitter/model/core/Tweet;

    if-eqz v2, :cond_0

    .line 148
    iget-object v2, p0, Lcom/twitter/library/widget/InlineActionBar;->o:Lcom/twitter/model/core/Tweet;

    iget-boolean v2, v2, Lcom/twitter/model/core/Tweet;->a:Z

    if-eqz v2, :cond_2

    .line 149
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->u:Lcom/twitter/library/widget/InlineActionBar$b;

    invoke-interface {v0, v1}, Lcom/twitter/library/widget/InlineActionBar$b;->a(Lcom/twitter/model/core/TweetActionType;)V

    goto :goto_0

    .line 151
    :cond_2
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/widget/InlineActionBar;->a(Lbwu;Z)V

    goto :goto_0

    .line 157
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->u:Lcom/twitter/library/widget/InlineActionBar$b;

    invoke-interface {v0, v1}, Lcom/twitter/library/widget/InlineActionBar$b;->a(Lcom/twitter/model/core/TweetActionType;)V

    goto :goto_0

    .line 161
    :pswitch_4
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->u:Lcom/twitter/library/widget/InlineActionBar$b;

    invoke-interface {v0, v1}, Lcom/twitter/library/widget/InlineActionBar$b;->a(Lcom/twitter/model/core/TweetActionType;)V

    goto :goto_0

    .line 136
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 261
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 262
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->t:Lcom/twitter/ui/anim/e;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->t:Lcom/twitter/ui/anim/e;

    invoke-virtual {v0}, Lcom/twitter/ui/anim/e;->f()V

    .line 265
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 120
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 121
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->getChildCount()I

    move-result v1

    .line 122
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 123
    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineActionBar;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/twitter/library/widget/InlineActionBar;->setupChildView(Landroid/view/View;)V

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->a()V

    .line 126
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/twitter/library/widget/InlineActionBar;->d()V

    .line 219
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .prologue
    .line 269
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/InlineActionBar;->a(I)I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/twitter/library/widget/InlineActionBar;->measureChildren(II)V

    .line 270
    invoke-direct {p0}, Lcom/twitter/library/widget/InlineActionBar;->getDesiredHeight()I

    move-result v0

    invoke-static {v0, p2}, Lcom/twitter/library/widget/InlineActionBar;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/widget/InlineActionBar;->setMeasuredDimension(II)V

    .line 271
    return-void
.end method

.method public setBylineSize(F)V
    .locals 2

    .prologue
    .line 384
    iget v0, p0, Lcom/twitter/library/widget/InlineActionBar;->q:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_1

    .line 385
    iput p1, p0, Lcom/twitter/library/widget/InlineActionBar;->q:F

    .line 386
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwu;

    .line 387
    invoke-static {v0}, Lcom/twitter/library/widget/InlineActionBar;->c(Lbwu;)Lcom/twitter/library/widget/InlineActionView;

    move-result-object v0

    .line 388
    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/InlineActionView;->setBylineSize(F)V

    goto :goto_0

    .line 390
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->requestLayout()V

    .line 392
    :cond_1
    return-void
.end method

.method public setForceHideDMInlineAction(Z)V
    .locals 0

    .prologue
    .line 299
    iput-boolean p1, p0, Lcom/twitter/library/widget/InlineActionBar;->p:Z

    .line 300
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->a()V

    .line 301
    return-void
.end method

.method public setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V
    .locals 1

    .prologue
    .line 294
    iput-object p1, p0, Lcom/twitter/library/widget/InlineActionBar;->r:Lcom/twitter/model/util/FriendshipCache;

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->s:Lbwt;

    .line 296
    return-void
.end method

.method public setInlineActionTypes(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TweetActionType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 316
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->n:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 317
    iput-object p1, p0, Lcom/twitter/library/widget/InlineActionBar;->n:Ljava/util/List;

    .line 318
    iget-object v0, p0, Lcom/twitter/library/widget/InlineActionBar;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 319
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TweetActionType;

    .line 320
    iget-object v2, p0, Lcom/twitter/library/widget/InlineActionBar;->l:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwu;

    .line 321
    if-eqz v0, :cond_0

    .line 322
    iget-object v2, p0, Lcom/twitter/library/widget/InlineActionBar;->m:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 323
    invoke-virtual {v0}, Lbwu;->b()V

    goto :goto_0

    .line 327
    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/InlineActionBar;->l:Ljava/util/Map;

    .line 328
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 329
    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 330
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 331
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TweetActionType;

    .line 332
    iget-object v2, p0, Lcom/twitter/library/widget/InlineActionBar;->l:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwu;

    invoke-static {v0}, Lcom/twitter/library/widget/InlineActionBar;->c(Lbwu;)Lcom/twitter/library/widget/InlineActionView;

    move-result-object v0

    .line 333
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 336
    :cond_2
    return-void
.end method

.method public setOnInlineActionClickListener(Lcom/twitter/library/widget/InlineActionBar$b;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/twitter/library/widget/InlineActionBar;->u:Lcom/twitter/library/widget/InlineActionBar$b;

    .line 361
    return-void
.end method

.method public setTweet(Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 339
    if-nez p1, :cond_0

    .line 344
    :goto_0
    return-void

    .line 342
    :cond_0
    iput-object p1, p0, Lcom/twitter/library/widget/InlineActionBar;->o:Lcom/twitter/model/core/Tweet;

    .line 343
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineActionBar;->b()V

    goto :goto_0
.end method
