.class public Lcom/twitter/library/widget/InlineDismissView;
.super Landroid/widget/ViewSwitcher;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/widget/InlineDismissView$a;,
        Lcom/twitter/library/widget/InlineDismissView$SavedState;,
        Lcom/twitter/library/widget/InlineDismissView$c;,
        Lcom/twitter/library/widget/InlineDismissView$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/widget/InlineDismissView$b;

.field private b:Landroid/view/ViewGroup;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/twitter/model/timeline/k;

.field private f:Lcom/twitter/model/timeline/g;

.field private g:Lcom/twitter/library/widget/InlineDismissView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0, p1, v1}, Landroid/widget/ViewSwitcher;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Lcom/twitter/library/widget/InlineDismissView$b;

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/widget/InlineDismissView$b;-><init>(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/library/widget/InlineDismissView$1;)V

    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->a:Lcom/twitter/library/widget/InlineDismissView$b;

    .line 48
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/InlineDismissView;->a(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/ViewSwitcher;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Lcom/twitter/library/widget/InlineDismissView$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/widget/InlineDismissView$b;-><init>(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/library/widget/InlineDismissView$1;)V

    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->a:Lcom/twitter/library/widget/InlineDismissView$b;

    .line 53
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/InlineDismissView;->a(Landroid/content/Context;)V

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/widget/InlineDismissView;)Lcom/twitter/model/timeline/g;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->f:Lcom/twitter/model/timeline/g;

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 137
    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->f:Lcom/twitter/model/timeline/g;

    .line 138
    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    .line 139
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineDismissView;->setDisplayedChild(I)V

    .line 140
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->b:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 192
    :cond_0
    return-void

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 190
    iget-object v1, p0, Lcom/twitter/library/widget/InlineDismissView;->b:Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/twitter/library/widget/InlineDismissView;->b()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    sget v0, Lazw$h;->inline_dismiss_view_content:I

    invoke-static {p1, v0, p0}, Lcom/twitter/library/widget/InlineDismissView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/InlineDismissView;->b(Lcom/twitter/model/timeline/g;)V

    return-void
.end method

.method private a(Lcom/twitter/model/timeline/g;)V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineDismissView;->setDisplayedChild(I)V

    .line 149
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/InlineDismissView;->setConfirmationText(Lcom/twitter/model/timeline/g;)V

    .line 150
    return-void
.end method

.method private a(Lcom/twitter/model/timeline/k;)V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p1, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/InlineDismissView;->a(Lcom/twitter/model/timeline/g;)V

    .line 144
    iget-object v0, p1, Lcom/twitter/model/timeline/k;->c:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/InlineDismissView;->a(Ljava/util/List;)V

    .line 145
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 161
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->b:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 183
    :goto_0
    return-void

    .line 165
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 166
    invoke-direct {p0, v4}, Lcom/twitter/library/widget/InlineDismissView;->a(I)V

    move v2, v3

    .line 167
    :goto_1
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 168
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 169
    if-ge v2, v4, :cond_1

    .line 170
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/g;

    .line 171
    sget v1, Lazw$g;->feedback_text:I

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 172
    iget-object v6, v0, Lcom/twitter/model/timeline/g;->c:Ljava/lang/String;

    .line 173
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 175
    invoke-virtual {v5, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 176
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 167
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 178
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 179
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_2

    .line 182
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method private b()Landroid/view/View;
    .locals 3

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineDismissView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lazw$h;->inline_dismiss_item:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/twitter/library/widget/InlineDismissView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 196
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 197
    iget-object v1, p0, Lcom/twitter/library/widget/InlineDismissView;->a:Lcom/twitter/library/widget/InlineDismissView$b;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    return-object v0
.end method

.method static synthetic b(Lcom/twitter/library/widget/InlineDismissView;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/twitter/library/widget/InlineDismissView;->c()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/InlineDismissView;->d(Lcom/twitter/model/timeline/g;)V

    return-void
.end method

.method private b(Lcom/twitter/model/timeline/g;)V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/InlineDismissView;->e(Lcom/twitter/model/timeline/g;)V

    .line 203
    iput-object p1, p0, Lcom/twitter/library/widget/InlineDismissView;->f:Lcom/twitter/model/timeline/g;

    .line 204
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/InlineDismissView;->c(Lcom/twitter/model/timeline/g;)V

    .line 205
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->g:Lcom/twitter/library/widget/InlineDismissView$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->g:Lcom/twitter/library/widget/InlineDismissView$a;

    iget-object v1, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    iget-object v1, v1, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    invoke-interface {v0, p0, v1}, Lcom/twitter/library/widget/InlineDismissView$a;->c(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V

    .line 238
    :cond_0
    return-void
.end method

.method private c(Lcom/twitter/model/timeline/g;)V
    .locals 2

    .prologue
    .line 208
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/InlineDismissView;->a(Lcom/twitter/model/timeline/g;)V

    .line 209
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->b:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 212
    :cond_0
    return-void
.end method

.method private d(Lcom/twitter/model/timeline/g;)V
    .locals 1

    .prologue
    .line 215
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/InlineDismissView;->f(Lcom/twitter/model/timeline/g;)V

    .line 216
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->f:Lcom/twitter/model/timeline/g;

    .line 217
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/InlineDismissView;->a(Lcom/twitter/model/timeline/k;)V

    .line 220
    :cond_0
    return-void
.end method

.method private e(Lcom/twitter/model/timeline/g;)V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->g:Lcom/twitter/library/widget/InlineDismissView$a;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->g:Lcom/twitter/library/widget/InlineDismissView$a;

    invoke-interface {v0, p0, p1}, Lcom/twitter/library/widget/InlineDismissView$a;->a(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V

    .line 226
    :cond_0
    return-void
.end method

.method private f(Lcom/twitter/model/timeline/g;)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->g:Lcom/twitter/library/widget/InlineDismissView$a;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->g:Lcom/twitter/library/widget/InlineDismissView$a;

    invoke-interface {v0, p0, p1}, Lcom/twitter/library/widget/InlineDismissView$a;->b(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V

    .line 232
    :cond_0
    return-void
.end method

.method private setConfirmationText(Lcom/twitter/model/timeline/g;)V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p1, Lcom/twitter/model/timeline/g;->d:Ljava/lang/String;

    .line 155
    iget-object v1, p0, Lcom/twitter/library/widget/InlineDismissView;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v1, p0, Lcom/twitter/library/widget/InlineDismissView;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 158
    :cond_0
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0}, Landroid/widget/ViewSwitcher;->onFinishInflate()V

    .line 63
    sget v0, Lazw$g;->feedback_items:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineDismissView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->b:Landroid/view/ViewGroup;

    .line 64
    sget v0, Lazw$g;->confirmation_text:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineDismissView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->c:Landroid/widget/TextView;

    .line 65
    sget v0, Lazw$g;->undo_feeback:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineDismissView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->d:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->d:Landroid/widget/TextView;

    new-instance v1, Lcom/twitter/library/widget/InlineDismissView$c;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/widget/InlineDismissView$c;-><init>(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/library/widget/InlineDismissView$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/widget/InlineDismissView;->a()V

    .line 70
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 80
    check-cast p1, Lcom/twitter/library/widget/InlineDismissView$SavedState;

    .line 81
    invoke-virtual {p1}, Lcom/twitter/library/widget/InlineDismissView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/ViewSwitcher;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 83
    iget-object v0, p1, Lcom/twitter/library/widget/InlineDismissView$SavedState;->a:Lcom/twitter/model/timeline/k;

    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    .line 84
    iget-object v0, p1, Lcom/twitter/library/widget/InlineDismissView$SavedState;->b:Lcom/twitter/model/timeline/g;

    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->f:Lcom/twitter/model/timeline/g;

    .line 86
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    if-nez v0, :cond_0

    .line 87
    invoke-direct {p0}, Lcom/twitter/library/widget/InlineDismissView;->a()V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->f:Lcom/twitter/model/timeline/g;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineDismissView;->setCurrentFeedbackAction(Lcom/twitter/model/timeline/g;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 74
    invoke-super {p0}, Landroid/widget/ViewSwitcher;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 75
    new-instance v1, Lcom/twitter/library/widget/InlineDismissView$SavedState;

    iget-object v2, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    iget-object v3, p0, Lcom/twitter/library/widget/InlineDismissView;->f:Lcom/twitter/model/timeline/g;

    invoke-direct {v1, v0, v2, v3}, Lcom/twitter/library/widget/InlineDismissView$SavedState;-><init>(Landroid/os/Parcelable;Lcom/twitter/model/timeline/k;Lcom/twitter/model/timeline/g;)V

    return-object v1
.end method

.method public setCurrentFeedbackAction(Lcom/twitter/model/timeline/g;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    if-eqz v0, :cond_1

    .line 126
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    iget-object v0, v0, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    invoke-virtual {v0, p1}, Lcom/twitter/model/timeline/g;->a(Lcom/twitter/model/timeline/g;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 127
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->f:Lcom/twitter/model/timeline/g;

    .line 128
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/InlineDismissView;->a(Lcom/twitter/model/timeline/k;)V

    .line 134
    :cond_1
    :goto_0
    return-void

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    iget-object v0, v0, Lcom/twitter/model/timeline/k;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    iput-object p1, p0, Lcom/twitter/library/widget/InlineDismissView;->f:Lcom/twitter/model/timeline/g;

    .line 131
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/InlineDismissView;->c(Lcom/twitter/model/timeline/g;)V

    goto :goto_0
.end method

.method public setDismissInfo(Lcom/twitter/model/timeline/k;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 98
    if-nez p1, :cond_1

    .line 99
    invoke-direct {p0}, Lcom/twitter/library/widget/InlineDismissView;->a()V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    invoke-virtual {p1, v0}, Lcom/twitter/model/timeline/k;->a(Lcom/twitter/model/timeline/k;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    iput-object p1, p0, Lcom/twitter/library/widget/InlineDismissView;->e:Lcom/twitter/model/timeline/k;

    .line 102
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 103
    iget v0, p1, Lcom/twitter/model/timeline/k;->e:I

    packed-switch v0, :pswitch_data_0

    .line 112
    invoke-virtual {p0}, Lcom/twitter/library/widget/InlineDismissView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lazw$k;->inline_dismiss_undo:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lcom/twitter/library/widget/InlineDismissView;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v1, p0, Lcom/twitter/library/widget/InlineDismissView;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    :cond_2
    :goto_1
    iget-object v0, p1, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InlineDismissView;->setCurrentFeedbackAction(Lcom/twitter/model/timeline/g;)V

    goto :goto_0

    .line 105
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->d:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/model/timeline/k;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->d:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/model/timeline/k;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v0, p0, Lcom/twitter/library/widget/InlineDismissView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setDismissListener(Lcom/twitter/library/widget/InlineDismissView$a;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/twitter/library/widget/InlineDismissView;->g:Lcom/twitter/library/widget/InlineDismissView$a;

    .line 95
    return-void
.end method
