.class public Lcom/twitter/library/widget/TweetView;
.super Lcom/twitter/ui/widget/CellLayout;
.source "Twttr"

# interfaces
.implements Lbxf;
.implements Lcom/twitter/internal/android/widget/b;
.implements Lcom/twitter/library/revenue/a$a;
.implements Lcom/twitter/library/widget/b;
.implements Lcom/twitter/library/widget/h;
.implements Lcom/twitter/media/ui/image/a;


# static fields
.field public static final b:Lcom/twitter/util/math/Size;

.field public static final c:Lcom/twitter/ui/view/h;

.field static d:Landroid/animation/Animator$AnimatorListener;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field private final A:Lcom/twitter/ui/widget/TextLayoutView;

.field private final B:Lcom/twitter/ui/widget/TextLayoutView;

.field private final C:Landroid/view/View$OnClickListener;

.field private final D:Landroid/view/View;

.field private final E:Lcom/twitter/library/widget/UserForwardView;

.field private final F:Lcom/twitter/library/widget/InlineActionBar;

.field private final G:Landroid/graphics/Rect;

.field private final H:Landroid/content/res/Resources;

.field private final I:Lcne;

.field private final J:Lcom/twitter/library/widget/g$b;

.field private final K:Lcom/twitter/library/widget/TextContentView;

.field private final L:Landroid/graphics/drawable/Drawable;

.field private final M:I

.field private N:Lcom/twitter/model/core/Tweet;

.field private O:Lcom/twitter/library/view/d;

.field private P:Lcom/twitter/model/util/FriendshipCache;

.field private Q:Lcom/twitter/library/widget/g;

.field private R:F

.field private S:Ljava/lang/CharSequence;

.field private T:F

.field private U:J

.field private V:Z

.field private W:Z

.field private final aA:Z

.field private aB:Z

.field private aC:Z

.field private aD:Lcom/twitter/ui/view/h;

.field private final aE:Lbxz;

.field private aa:Z

.field private ab:Z

.field private ac:Ljava/lang/String;

.field private ad:Z

.field private ae:Z

.field private af:Z

.field private ag:Z

.field private ah:Z

.field private ai:I

.field private aj:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

.field private ak:I

.field private al:Z

.field private am:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

.field private an:Z

.field private final ao:Z

.field private ap:Lcom/twitter/library/widget/renderablecontent/d;

.field private aq:Z

.field private ar:Z

.field private as:Z

.field private final at:I

.field private au:Z

.field private av:Z

.field private aw:Ljava/lang/String;

.field private ax:I

.field private ay:Lcom/twitter/util/math/Size;

.field private final az:Lcom/twitter/library/revenue/a;

.field final e:Landroid/graphics/Rect;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final f:Landroid/view/View$OnClickListener;

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:Landroid/graphics/drawable/Drawable;

.field private final q:Landroid/graphics/drawable/Drawable;

.field private final r:Z

.field private final s:Lcom/twitter/library/view/QuoteView;

.field private final t:Lcom/twitter/library/view/SocialProofView;

.field private final u:Lbwz;

.field private final v:Lcom/twitter/media/ui/image/UserImageView;

.field private final w:Lcom/twitter/ui/widget/TweetHeaderView;

.field private final x:Lbxr;

.field private final y:Lcom/twitter/ui/widget/TextLayoutView;

.field private final z:Lcom/twitter/ui/widget/BadgeView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/16 v0, 0x64

    .line 106
    invoke-static {v0, v0}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/util/math/Size;

    .line 108
    new-instance v0, Lcom/twitter/ui/view/h$a;

    invoke-direct {v0}, Lcom/twitter/ui/view/h$a;-><init>()V

    .line 109
    invoke-virtual {v0}, Lcom/twitter/ui/view/h$a;->a()Lcom/twitter/ui/view/h;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/widget/TweetView;->c:Lcom/twitter/ui/view/h;

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/TweetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 289
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 292
    sget v0, Lazw$c;->tweetViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/TweetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 293
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 296
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/ui/widget/CellLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 141
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->e:Landroid/graphics/Rect;

    .line 171
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    .line 203
    iput-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->af:Z

    .line 238
    sget-object v0, Lcom/twitter/library/widget/TweetView;->c:Lcom/twitter/ui/view/h;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    .line 240
    new-instance v0, Lcom/twitter/library/widget/TweetView$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/TweetView$1;-><init>(Lcom/twitter/library/widget/TweetView;)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->aE:Lbxz;

    .line 297
    invoke-virtual {p0, v2}, Lcom/twitter/library/widget/TweetView;->setWillNotDraw(Z)V

    .line 298
    invoke-virtual {p0, v2}, Lcom/twitter/library/widget/TweetView;->setClipToPadding(Z)V

    .line 299
    invoke-virtual {p0, v2}, Lcom/twitter/library/widget/TweetView;->setClipChildren(Z)V

    .line 300
    const-string/jumbo v0, "legacy_deciders_amplify_player_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ao:Z

    .line 302
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->H:Landroid/content/res/Resources;

    .line 304
    sget-object v0, Lazw$l;->TweetView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 305
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v4, Lazw$l;->TweetView_tweetViewLayoutId:I

    invoke-virtual {v3, v4, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    invoke-virtual {v0, v4, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 306
    sget v0, Lazw$g;->tweet_social_proof:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/view/SocialProofView;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->t:Lcom/twitter/library/view/SocialProofView;

    .line 307
    new-instance v0, Lbwz;

    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->t:Lcom/twitter/library/view/SocialProofView;

    iget-object v5, p0, Lcom/twitter/library/widget/TweetView;->H:Landroid/content/res/Resources;

    invoke-direct {v0, v4, v5}, Lbwz;-><init>(Lbwz$a;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->u:Lbwz;

    .line 308
    sget v0, Lazw$g;->tweet_header:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TweetHeaderView;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->w:Lcom/twitter/ui/widget/TweetHeaderView;

    .line 309
    sget v0, Lazw$g;->tweet_reply_context:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TextLayoutView;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->y:Lcom/twitter/ui/widget/TextLayoutView;

    .line 310
    new-instance v0, Lbxr;

    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->y:Lcom/twitter/ui/widget/TextLayoutView;

    iget-object v5, p0, Lcom/twitter/library/widget/TweetView;->H:Landroid/content/res/Resources;

    invoke-direct {v0, v4, v5}, Lbxr;-><init>(Lcom/twitter/ui/widget/TextLayoutView;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->x:Lbxr;

    .line 312
    sget v0, Lazw$g;->tweet_quote:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/view/QuoteView;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    .line 313
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    new-instance v4, Lcom/twitter/library/widget/TweetView$4;

    invoke-direct {v4, p0}, Lcom/twitter/library/widget/TweetView$4;-><init>(Lcom/twitter/library/widget/TweetView;)V

    invoke-virtual {v0, v4}, Lcom/twitter/library/view/QuoteView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 319
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    new-instance v4, Lcom/twitter/library/widget/TweetView$5;

    invoke-direct {v4, p0}, Lcom/twitter/library/widget/TweetView$5;-><init>(Lcom/twitter/library/widget/TweetView;)V

    invoke-virtual {v0, v4}, Lcom/twitter/library/view/QuoteView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 325
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    iget-boolean v4, p0, Lcom/twitter/library/widget/TweetView;->g_:Z

    invoke-virtual {v0, v4}, Lcom/twitter/library/view/QuoteView;->setRenderRtl(Z)V

    .line 327
    sget v0, Lazw$g;->tweet_promoted_badge:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/BadgeView;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->z:Lcom/twitter/ui/widget/BadgeView;

    .line 328
    sget v0, Lazw$l;->TweetView_iconSpacing:I

    const/4 v4, 0x4

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->g:I

    .line 330
    sget v0, Lazw$g;->tweet_content_text:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TextContentView;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->K:Lcom/twitter/library/widget/TextContentView;

    .line 332
    sget v0, Lazw$l;->TweetView_bylineSize:I

    invoke-static {}, Lcni;->b()F

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->R:F

    .line 333
    sget v0, Lazw$g;->tweet_attribution:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TextLayoutView;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->A:Lcom/twitter/ui/widget/TextLayoutView;

    .line 334
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->A:Lcom/twitter/ui/widget/TextLayoutView;

    new-instance v4, Lcom/twitter/library/widget/TweetView$6;

    invoke-direct {v4, p0}, Lcom/twitter/library/widget/TweetView$6;-><init>(Lcom/twitter/library/widget/TweetView;)V

    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/TextLayoutView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    sget v0, Lazw$g;->tweet_media_tags:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TextLayoutView;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->B:Lcom/twitter/ui/widget/TextLayoutView;

    .line 342
    new-instance v0, Lcom/twitter/library/widget/TweetView$7;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/TweetView$7;-><init>(Lcom/twitter/library/widget/TweetView;)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->C:Landroid/view/View$OnClickListener;

    .line 349
    sget v0, Lazw$l;->TweetView_contentSize:I

    invoke-static {}, Lcni;->a()F

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->setContentSize(F)V

    .line 351
    sget v0, Lazw$g;->tweet_curation_action:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->D:Landroid/view/View;

    .line 353
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->D:Landroid/view/View;

    new-instance v4, Lcom/twitter/library/widget/TweetView$8;

    invoke-direct {v4, p0}, Lcom/twitter/library/widget/TweetView$8;-><init>(Lcom/twitter/library/widget/TweetView;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 360
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->D:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->a(Landroid/view/View;)Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;

    move-result-object v0

    .line 362
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lazw$f;->tweet_curation:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 363
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lazw$d;->caret:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-static {v4, v5}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 364
    iget-object v5, p0, Lcom/twitter/library/widget/TweetView;->D:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 365
    iget v4, v0, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->topMargin:I

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lazw$e;->tweet_caret_vertical_alignment:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v0, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->topMargin:I

    .line 367
    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->D:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 369
    iget-boolean v5, p0, Lcom/twitter/library/widget/TweetView;->g_:Z

    if-eqz v5, :cond_0

    .line 370
    iget v5, v0, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->rightMargin:I

    iget v6, p0, Lcom/twitter/library/widget/TweetView;->g:I

    add-int/2addr v5, v6

    iput v5, v0, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->rightMargin:I

    .line 376
    :goto_0
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x16

    if-ge v5, v6, :cond_1

    .line 377
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->D:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getPaddingLeft()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->D:Landroid/view/View;

    .line 378
    invoke-virtual {v6}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v0, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->width:I

    .line 379
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    iget-object v5, p0, Lcom/twitter/library/widget/TweetView;->D:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingTop()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/twitter/library/widget/TweetView;->D:Landroid/view/View;

    .line 380
    invoke-virtual {v5}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v0, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->height:I

    .line 386
    :goto_1
    sget v0, Lazw$g;->tweet_user_forward:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/UserForwardView;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    .line 387
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->P:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/UserForwardView;->setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V

    .line 388
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    iget v4, p0, Lcom/twitter/library/widget/TweetView;->T:F

    iget v5, p0, Lcom/twitter/library/widget/TweetView;->R:F

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/widget/UserForwardView;->a(FF)V

    .line 389
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    new-instance v4, Lcom/twitter/library/widget/TweetView$9;

    invoke-direct {v4, p0}, Lcom/twitter/library/widget/TweetView$9;-><init>(Lcom/twitter/library/widget/TweetView;)V

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/UserForwardView;->setFollowButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 396
    sget v0, Lazw$l;->TweetView_inlineActionBarPaddingNormal:I

    .line 397
    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 398
    sget v0, Lazw$g;->tweet_inline_actions:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/InlineActionBar;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    .line 399
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/InlineActionBar;)V

    .line 400
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    invoke-static {v0}, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->a(Landroid/view/View;)Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;

    move-result-object v0

    .line 401
    neg-int v5, v4

    iput v5, v0, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->leftMargin:I

    .line 402
    neg-int v4, v4

    iput v4, v0, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->rightMargin:I

    .line 404
    sget v0, Lazw$l;->TweetView_verticalConnectorWidth:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->h:I

    .line 405
    sget v0, Lazw$l;->TweetView_verticalConnectorMargin:I

    invoke-virtual {v3, v0, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->i:I

    .line 406
    sget v0, Lazw$l;->TweetView_verticalConnector:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->p:Landroid/graphics/drawable/Drawable;

    .line 407
    sget v0, Lazw$l;->TweetView_verticalConnector:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->q:Landroid/graphics/drawable/Drawable;

    .line 410
    sget v0, Lazw$l;->TweetView_badgeSpacing:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->j:I

    .line 412
    sget v0, Lazw$l;->TweetView_previewFlags:I

    const/4 v4, 0x3

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->o:I

    .line 414
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->o:I

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->r:Z

    .line 415
    sget v0, Lazw$l;->TweetView_mediaTopMargin:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->k:I

    .line 416
    sget v0, Lazw$l;->TweetView_mediaBottomMargin:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->l:I

    .line 418
    sget v0, Lazw$l;->TweetView_mediaTagIcon:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->m:I

    .line 419
    sget v0, Lazw$l;->TweetView_mediaPlaceholderDrawable:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->n:I

    .line 421
    sget v0, Lazw$l;->TweetView_autoLink:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->au:Z

    .line 423
    sget v0, Lazw$g;->tweet_profile_image:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    .line 424
    const-string/jumbo v4, "profile"

    invoke-virtual {v0, v4}, Lcom/twitter/media/ui/image/UserImageView;->setImageType(Ljava/lang/String;)V

    .line 425
    new-instance v4, Lcom/twitter/library/widget/TweetView$10;

    invoke-direct {v4, p0}, Lcom/twitter/library/widget/TweetView$10;-><init>(Lcom/twitter/library/widget/TweetView;)V

    iput-object v4, p0, Lcom/twitter/library/widget/TweetView;->f:Landroid/view/View$OnClickListener;

    .line 431
    invoke-static {v0}, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->a(Landroid/view/View;)Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->a(Z)V

    .line 432
    invoke-static {v0, v7}, Lcom/twitter/util/ui/a;->a(Landroid/view/View;I)V

    .line 433
    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    .line 435
    sget v0, Lazw$l;->TweetView_mediaDivider:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->at:I

    .line 437
    sget v0, Lazw$l;->TweetView_promotedDrawable:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 438
    sget v4, Lazw$l;->TweetView_politicalDrawable:I

    invoke-virtual {v3, v4, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 439
    sget v5, Lazw$l;->TweetView_alertDrawable:I

    invoke-virtual {v3, v5, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    .line 440
    new-instance v6, Lcom/twitter/library/revenue/a;

    invoke-direct {v6, p0, v4, v0, v5}, Lcom/twitter/library/revenue/a;-><init>(Lcom/twitter/library/revenue/a$a;III)V

    iput-object v6, p0, Lcom/twitter/library/widget/TweetView;->az:Lcom/twitter/library/revenue/a;

    .line 442
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->L:Landroid/graphics/drawable/Drawable;

    .line 443
    sget v0, Lazw$l;->TweetView_noPressStateBackgroundDrawable:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->M:I

    .line 444
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 446
    const-string/jumbo v0, "android_media_playback_unload_on_temporary_detach"

    .line 447
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 448
    invoke-static {}, Lblb;->a()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->aA:Z

    .line 451
    new-instance v0, Lcom/twitter/library/widget/TweetView$11;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/TweetView$11;-><init>(Lcom/twitter/library/widget/TweetView;)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->I:Lcne;

    .line 492
    new-instance v0, Lcom/twitter/library/widget/TweetView$2;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/TweetView$2;-><init>(Lcom/twitter/library/widget/TweetView;)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->J:Lcom/twitter/library/widget/g$b;

    .line 498
    new-instance v0, Lcom/twitter/library/widget/g;

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->K:Lcom/twitter/library/widget/TextContentView;

    iget-object v5, p0, Lcom/twitter/library/widget/TweetView;->K:Lcom/twitter/library/widget/TextContentView;

    .line 499
    invoke-virtual {v5}, Lcom/twitter/library/widget/TextContentView;->getContentFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v5

    invoke-direct {v0, v3, v4, v5}, Lcom/twitter/library/widget/g;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/graphics/Paint$FontMetrics;)V

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->I:Lcne;

    .line 500
    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/g;->a(Lcne;)Lcom/twitter/library/widget/g;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->H:Landroid/content/res/Resources;

    sget v4, Lazw$d;->link_selected:I

    .line 501
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/g;->a(I)Lcom/twitter/library/widget/g;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    iget-boolean v0, v0, Lcom/twitter/ui/view/h;->j:Z

    if-nez v0, :cond_4

    move v0, v1

    .line 502
    :goto_4
    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/g;->a(Z)Lcom/twitter/library/widget/g;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    iget-boolean v0, v0, Lcom/twitter/ui/view/h;->k:Z

    if-nez v0, :cond_5

    move v0, v1

    .line 503
    :goto_5
    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/g;->b(Z)Lcom/twitter/library/widget/g;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    iget-boolean v0, v0, Lcom/twitter/ui/view/h;->l:Z

    if-nez v0, :cond_6

    move v0, v1

    .line 504
    :goto_6
    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/g;->c(Z)Lcom/twitter/library/widget/g;

    move-result-object v0

    iget-boolean v3, p0, Lcom/twitter/library/widget/TweetView;->au:Z

    .line 505
    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/g;->d(Z)Lcom/twitter/library/widget/g;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    iget-boolean v3, v3, Lcom/twitter/ui/view/h;->i:Z

    if-nez v3, :cond_7

    .line 506
    :goto_7
    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/g;->e(Z)Lcom/twitter/library/widget/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->H:Landroid/content/res/Resources;

    sget v2, Lazw$d;->subtext:I

    .line 507
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/g;->b(I)Lcom/twitter/library/widget/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->J:Lcom/twitter/library/widget/g$b;

    .line 508
    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/g;->a(Lcom/twitter/library/widget/g$b;)Lcom/twitter/library/widget/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->H:Landroid/content/res/Resources;

    sget v2, Lazw$k;->tagline_separator:I

    .line 509
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/g;->a(Ljava/lang/String;)Lcom/twitter/library/widget/g;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->Q:Lcom/twitter/library/widget/g;

    .line 510
    return-void

    .line 372
    :cond_0
    iget v5, v0, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->leftMargin:I

    iget v6, p0, Lcom/twitter/library/widget/TweetView;->g:I

    add-int/2addr v5, v6

    iput v5, v0, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->leftMargin:I

    goto/16 :goto_0

    .line 382
    :cond_1
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    iput v5, v0, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->width:I

    .line 383
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    iput v4, v0, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->height:I

    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 414
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 448
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 501
    goto :goto_4

    :cond_5
    move v0, v2

    .line 502
    goto :goto_5

    :cond_6
    move v0, v2

    .line 503
    goto :goto_6

    :cond_7
    move v1, v2

    .line 505
    goto :goto_7
.end method

.method private a(III)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1248
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 1249
    if-lez p1, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    .line 1251
    invoke-direct {p0, v0}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1254
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v1

    move v3, v2

    move v4, p1

    move v5, p3

    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/widget/renderablecontent/d;->a(Landroid/content/Context;IIII)Landroid/graphics/Rect;

    move-result-object v0

    .line 1255
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 1256
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v6

    .line 1257
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->k:I

    add-int v7, p3, v0

    .line 1259
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ac:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1260
    add-int v0, v7, v6

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->l:I

    add-int/2addr v0, v3

    .line 1261
    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->A:Lcom/twitter/ui/widget/TextLayoutView;

    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->ac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/twitter/ui/widget/TextLayoutView;->setTextWithVisibility(Ljava/lang/CharSequence;)V

    .line 1262
    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->A:Lcom/twitter/ui/widget/TextLayoutView;

    const/high16 v4, -0x80000000

    invoke-static {p1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v3, v4, v2}, Lcom/twitter/ui/widget/TextLayoutView;->measure(II)V

    .line 1264
    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->A:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v3}, Lcom/twitter/ui/widget/TextLayoutView;->getMeasuredWidth()I

    move-result v4

    .line 1265
    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->A:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v3}, Lcom/twitter/ui/widget/TextLayoutView;->getMeasuredHeight()I

    move-result v3

    move v5, v4

    move v4, v3

    move v3, v0

    .line 1268
    :goto_0
    if-gtz v6, :cond_1

    if-lez v4, :cond_3

    .line 1270
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->g_:Z

    if-eqz v0, :cond_5

    .line 1271
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->as:Z

    if-eqz v0, :cond_4

    move v0, v1

    .line 1273
    :goto_1
    sub-int v1, v0, v1

    .line 1274
    sub-int p2, v0, v5

    move v2, v1

    move v1, v0

    .line 1282
    :goto_2
    iget-object v5, p0, Lcom/twitter/library/widget/TweetView;->e:Landroid/graphics/Rect;

    add-int/2addr v6, v7

    invoke-virtual {v5, v2, v7, v1, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 1283
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->getForwardMediaView()Landroid/view/View;

    move-result-object v1

    .line 1284
    if-eqz v1, :cond_2

    .line 1285
    invoke-static {v1}, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->b(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1286
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->e:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1288
    :cond_2
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->A:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-static {v1}, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->b(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1289
    add-int v2, v3, v4

    invoke-virtual {v1, p2, v3, v0, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1291
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sub-int v2, v0, p3

    .line 1294
    :cond_3
    return v2

    .line 1271
    :cond_4
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, p1

    goto :goto_1

    .line 1278
    :cond_5
    add-int/2addr v1, p2

    .line 1279
    add-int v0, p2, v5

    move v2, p2

    goto :goto_2

    :cond_6
    move v3, p3

    move v4, v2

    move v5, v2

    goto :goto_0
.end method

.method private a(Landroid/graphics/Rect;)I
    .locals 1

    .prologue
    .line 1817
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->g_:Z

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->right:I

    :goto_0
    return v0

    :cond_0
    iget v0, p1, Landroid/graphics/Rect;->left:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/view/d;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    return-object v0
.end method

.method private a(Lcom/twitter/model/core/Tweet;Z)Lcom/twitter/model/core/e;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1091
    invoke-static {p1}, Lcom/twitter/model/util/a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/util/a;

    move-result-object v2

    .line 1093
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v3

    .line 1094
    invoke-static {p1}, Lbwr;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/twitter/model/util/a;->f(Z)Lcom/twitter/model/util/a;

    move-result-object v4

    .line 1095
    invoke-virtual {v4, v3}, Lcom/twitter/model/util/a;->c(Z)Lcom/twitter/model/util/a;

    .line 1097
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1098
    invoke-virtual {v2, v1}, Lcom/twitter/model/util/a;->a(Z)Lcom/twitter/model/util/a;

    move-result-object v2

    .line 1099
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ao()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    invoke-virtual {v2, v0}, Lcom/twitter/model/util/a;->b(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/util/a;->a()Lcom/twitter/model/core/e;

    move-result-object v0

    .line 1118
    :goto_0
    return-object v0

    .line 1100
    :cond_2
    iget v4, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    .line 1101
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->aj()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1102
    invoke-virtual {v2, v1}, Lcom/twitter/model/util/a;->a(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/util/a;->a()Lcom/twitter/model/core/e;

    move-result-object v0

    goto :goto_0

    .line 1104
    :cond_3
    invoke-virtual {v2}, Lcom/twitter/model/util/a;->a()Lcom/twitter/model/core/e;

    move-result-object v0

    goto :goto_0

    .line 1106
    :cond_4
    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v4}, Lcom/twitter/library/view/QuoteView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_5

    .line 1107
    invoke-virtual {v2, v1}, Lcom/twitter/model/util/a;->d(Z)Lcom/twitter/model/util/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/model/util/a;->c(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/util/a;->a()Lcom/twitter/model/core/e;

    move-result-object v0

    goto :goto_0

    .line 1108
    :cond_5
    iget-boolean v4, p0, Lcom/twitter/library/widget/TweetView;->av:Z

    if-eqz v4, :cond_8

    .line 1110
    invoke-virtual {v2, v1}, Lcom/twitter/model/util/a;->a(Z)Lcom/twitter/model/util/a;

    move-result-object v2

    .line 1111
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ao()Z

    move-result v3

    if-nez v3, :cond_6

    if-eqz p2, :cond_7

    :cond_6
    move v0, v1

    :cond_7
    invoke-virtual {v2, v0}, Lcom/twitter/model/util/a;->b(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    .line 1112
    invoke-virtual {v0}, Lcom/twitter/model/util/a;->a()Lcom/twitter/model/core/e;

    move-result-object v0

    goto :goto_0

    .line 1113
    :cond_8
    if-eqz v3, :cond_9

    .line 1114
    invoke-virtual {v2}, Lcom/twitter/model/util/a;->a()Lcom/twitter/model/core/e;

    move-result-object v0

    goto :goto_0

    .line 1116
    :cond_9
    new-instance v0, Lcom/twitter/model/core/e;

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/core/e;-><init>(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Rect;II)V
    .locals 2

    .prologue
    .line 1821
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v0

    .line 1822
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingRight()I

    move-result v1

    sub-int v1, p3, v1

    .line 1823
    invoke-virtual {p1, v0, p2, v1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1824
    return-void
.end method

.method private a(Landroid/graphics/Rect;IIZLcom/twitter/ui/widget/CellLayout$CellLayoutParams;)V
    .locals 0

    .prologue
    .line 1828
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;II)V

    .line 1829
    if-eqz p4, :cond_0

    .line 1830
    invoke-virtual {p0, p1, p5}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;)V

    .line 1832
    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/library/widget/InlineActionBar;)V
    .locals 1

    .prologue
    .line 1783
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->P:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/InlineActionBar;->setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V

    .line 1784
    new-instance v0, Lcom/twitter/library/widget/TweetView$3;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/TweetView$3;-><init>(Lcom/twitter/library/widget/TweetView;)V

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/InlineActionBar;->setOnInlineActionClickListener(Lcom/twitter/library/widget/InlineActionBar$b;)V

    .line 1790
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/twitter/util/ui/a;->a(Landroid/view/View;I)V

    .line 1792
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;ZZZZ)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v0, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 992
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v4

    .line 994
    if-eqz p2, :cond_2

    .line 995
    iget-object v2, p1, Lcom/twitter/model/core/Tweet;->V:Ljava/util/List;

    .line 996
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 997
    invoke-static {v2}, Lcom/twitter/model/util/c;->l(Ljava/lang/Iterable;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v2

    if-eqz v2, :cond_1

    :goto_0
    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    .line 1052
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 997
    goto :goto_0

    .line 1000
    :cond_2
    if-eqz p5, :cond_6

    invoke-static {p1}, Lbxd;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 1001
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1002
    invoke-static {}, Lcmj;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1003
    const/4 v0, 0x4

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    .line 1004
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aj:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    if-nez v0, :cond_3

    .line 1005
    sget v0, Lazw$g;->possibly_sensitive_warning_stub:I

    .line 1006
    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->aj:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    .line 1008
    :cond_3
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aj:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/PossiblySensitiveWarningView;->setVisibility(I)V

    goto :goto_1

    .line 1010
    :cond_4
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aj:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    if-eqz v0, :cond_0

    .line 1011
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aj:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    invoke-virtual {v0, v5}, Lcom/twitter/library/widget/PossiblySensitiveWarningView;->setVisibility(I)V

    goto :goto_1

    .line 1015
    :cond_5
    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    .line 1016
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aj:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    if-eqz v0, :cond_0

    .line 1017
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aj:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    invoke-virtual {v0, v5}, Lcom/twitter/library/widget/PossiblySensitiveWarningView;->setVisibility(I)V

    goto :goto_1

    .line 1020
    :cond_6
    if-nez p4, :cond_7

    if-eqz p3, :cond_8

    :cond_7
    if-nez v4, :cond_8

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->I()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1021
    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    goto :goto_1

    .line 1022
    :cond_8
    if-eqz p4, :cond_f

    if-eqz v4, :cond_f

    .line 1023
    iget-boolean v3, p0, Lcom/twitter/library/widget/TweetView;->ag:Z

    if-eqz v3, :cond_b

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v3

    if-nez v3, :cond_9

    .line 1024
    invoke-static {p1}, Lcom/twitter/library/av/playback/ab;->c(Lcom/twitter/model/core/Tweet;)Z

    move-result v3

    if-eqz v3, :cond_b

    iget-boolean v3, p0, Lcom/twitter/library/widget/TweetView;->ao:Z

    if-eqz v3, :cond_b

    :cond_9
    move v3, v1

    .line 1026
    :goto_2
    if-eqz p3, :cond_c

    if-eqz v3, :cond_c

    .line 1037
    :cond_a
    :goto_3
    if-eqz v1, :cond_0

    .line 1038
    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    .line 1039
    invoke-virtual {v4}, Lcax;->h()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 1040
    if-eqz p3, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lcax;->t()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1041
    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->ac:Ljava/lang/String;

    goto/16 :goto_1

    :cond_b
    move v3, v2

    .line 1024
    goto :goto_2

    .line 1028
    :cond_c
    invoke-virtual {v4}, Lcax;->r()Z

    move-result v5

    if-nez v5, :cond_d

    invoke-virtual {v4}, Lcax;->t()Z

    move-result v5

    if-eqz v5, :cond_e

    :cond_d
    iget v5, p0, Lcom/twitter/library/widget/TweetView;->o:I

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_e

    if-nez p3, :cond_a

    if-nez v3, :cond_a

    .line 1032
    :cond_e
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->I()Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    goto :goto_3

    .line 1044
    :cond_f
    if-eqz p4, :cond_0

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->o:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1047
    if-nez p3, :cond_10

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ag:Z

    if-eqz v0, :cond_0

    :cond_10
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ay:Lcom/twitter/util/math/Size;

    .line 1048
    invoke-static {p1, v0}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1049
    iput v1, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    goto/16 :goto_1
.end method

.method private a(Landroid/view/View;Landroid/graphics/Rect;III)Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1836
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1865
    :goto_0
    return v7

    .line 1839
    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    sub-int v3, p3, v0

    iget v5, p2, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/widget/TweetView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 1840
    invoke-static {p1}, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->a(Landroid/view/View;)Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;

    move-result-object v2

    .line 1841
    invoke-virtual {v2}, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->b()Landroid/graphics/Rect;

    move-result-object v3

    .line 1844
    invoke-virtual {v2}, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->g_:Z

    .line 1845
    :goto_1
    iget v1, p2, Landroid/graphics/Rect;->top:I

    iget v4, v2, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->topMargin:I

    add-int/2addr v4, v1

    .line 1847
    if-eqz v0, :cond_4

    .line 1848
    iget v1, p2, Landroid/graphics/Rect;->left:I

    iget v5, v2, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->leftMargin:I

    add-int/2addr v1, v5

    .line 1852
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v4

    invoke-virtual {v3, v1, v4, v5, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 1855
    if-eqz v0, :cond_5

    .line 1856
    iget v0, v3, Landroid/graphics/Rect;->right:I

    iget v1, v2, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->left:I

    .line 1861
    :goto_3
    invoke-virtual {v2}, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1862
    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    iget v1, v2, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    .line 1863
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    .line 1865
    :cond_1
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-ltz v0, :cond_6

    :goto_4
    move v7, v6

    goto :goto_0

    .line 1844
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->g_:Z

    if-nez v0, :cond_3

    move v0, v6

    goto :goto_1

    :cond_3
    move v0, v7

    goto :goto_1

    .line 1850
    :cond_4
    iget v1, p2, Landroid/graphics/Rect;->right:I

    iget v5, v2, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->rightMargin:I

    sub-int/2addr v1, v5

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v1, v5

    goto :goto_2

    .line 1858
    :cond_5
    iget v0, v3, Landroid/graphics/Rect;->left:I

    iget v1, v2, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->leftMargin:I

    sub-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->right:I

    goto :goto_3

    :cond_6
    move v6, v7

    .line 1865
    goto :goto_4
.end method

.method static synthetic a(Lcom/twitter/library/widget/TweetView;Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/twitter/model/core/Tweet;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 932
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->getOwnerId()J

    move-result-wide v4

    iget-wide v6, p1, Lcom/twitter/model/core/Tweet;->s:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    move v0, v1

    .line 933
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->x()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/twitter/library/widget/TweetView;->al:Z

    if-eqz v3, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 932
    goto :goto_0

    :cond_2
    move v1, v2

    .line 933
    goto :goto_1
.end method

.method private a(Lcom/twitter/model/core/Tweet;ZZ)Z
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v0, 0x1

    .line 1056
    iget v1, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-static {p1, v1}, Lbxd;->b(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1065
    :cond_0
    :goto_0
    return v0

    .line 1060
    :cond_1
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    if-nez p3, :cond_0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    .line 1061
    invoke-static {p1, v1}, Lbxd;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1065
    :cond_2
    iget v1, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    if-eq v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/model/core/ad;Lcax;)Z
    .locals 1

    .prologue
    .line 104
    invoke-static {p0, p1}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/model/core/ad;Lcax;)Z

    move-result v0

    return v0
.end method

.method private a(ZLcom/twitter/model/core/Tweet;ZZ)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 955
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 956
    :goto_0
    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-static {p2, v3}, Lbxd;->b(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/Tweet;)Z

    move-result v5

    .line 959
    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->l()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_0
    if-eqz p3, :cond_4

    move v3, v1

    .line 960
    :goto_1
    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-static {p2, v4}, Lbxd;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/Tweet;)Z

    move-result v6

    .line 962
    if-eqz v3, :cond_5

    if-eqz p4, :cond_5

    move v4, v1

    .line 963
    :goto_2
    invoke-direct {p0, v0, v5}, Lcom/twitter/library/widget/TweetView;->b(ZZ)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v3, v6}, Lcom/twitter/library/widget/TweetView;->b(ZZ)Z

    move-result v0

    if-nez v0, :cond_1

    if-nez v4, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    .line 964
    invoke-direct {p0, v0}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v2, v1

    .line 963
    :cond_2
    return v2

    :cond_3
    move v0, v2

    .line 955
    goto :goto_0

    :cond_4
    move v3, v2

    .line 959
    goto :goto_1

    :cond_5
    move v4, v2

    .line 962
    goto :goto_2
.end method

.method static synthetic b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method private b(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    iget-boolean v0, v0, Lcom/twitter/ui/view/h;->o:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1070
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lbxd;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1069
    :goto_0
    return v0

    .line 1070
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/twitter/model/core/ad;Lcax;)Z
    .locals 2

    .prologue
    .line 937
    instance-of v0, p0, Lcom/twitter/model/core/MediaEntity;

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    .line 938
    invoke-virtual {p1}, Lcax;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    .line 939
    invoke-virtual {p1}, Lcax;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 937
    :goto_0
    return v0

    .line 939
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(ZZ)Z
    .locals 1

    .prologue
    .line 979
    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->aC:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 1795
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    if-eqz v0, :cond_0

    .line 1796
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/TweetView;->d(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1797
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/InlineActionBar;->setVisibility(I)V

    .line 1803
    :cond_0
    :goto_0
    return-void

    .line 1799
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/InlineActionBar;->setVisibility(I)V

    .line 1800
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/InlineActionBar;->setTweet(Lcom/twitter/model/core/Tweet;)V

    goto :goto_0
.end method

.method private d(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 1806
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->W:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->o()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 1807
    invoke-static {p1}, Lbxd;->j(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1806
    :goto_0
    return v0

    .line 1807
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(Z)V
    .locals 1

    .prologue
    .line 1766
    invoke-static {}, Lbpi;->a()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ax:I

    .line 1767
    invoke-static {}, Lbpi;->d()Lcom/twitter/util/math/Size;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->ay:Lcom/twitter/util/math/Size;

    .line 1768
    invoke-static {}, Lbpi;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->aw:Ljava/lang/String;

    .line 1769
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/TweetView;->setupInlineActionBar(Z)V

    .line 1770
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->t()V

    .line 1771
    return-void
.end method

.method private getForwardMediaView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1700
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->d()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getOwner()Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 1075
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    return-object v0
.end method

.method private getOwnerId()J
    .locals 2

    .prologue
    .line 1079
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->getOwner()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 1080
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 1431
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()Z
    .locals 1

    .prologue
    .line 1657
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ah:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lbxd;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    .line 1658
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1657
    :goto_0
    return v0

    .line 1658
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1689
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->getForwardMediaView()Landroid/view/View;

    move-result-object v0

    .line 1690
    if-eqz v0, :cond_0

    .line 1691
    const/4 v1, 0x4

    .line 1692
    invoke-static {v0, v1}, Lcom/twitter/util/ui/a;->a(Landroid/view/View;I)V

    .line 1693
    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->addView(Landroid/view/View;)V

    .line 1694
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->aq:Z

    .line 1696
    :cond_0
    return-void
.end method

.method private o()Z
    .locals 1

    .prologue
    .line 1722
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ab:Z

    return v0
.end method

.method private p()Z
    .locals 1

    .prologue
    .line 1726
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    iget-boolean v0, v0, Lcom/twitter/ui/view/h;->m:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()Z
    .locals 1

    .prologue
    .line 1730
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    iget-boolean v0, v0, Lcom/twitter/ui/view/h;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()V
    .locals 1

    .prologue
    .line 1734
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_1

    .line 1735
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ar:Z

    if-eqz v0, :cond_0

    .line 1736
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->bg_()V

    .line 1737
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->n()V

    .line 1738
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ar:Z

    .line 1740
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->an:Z

    if-nez v0, :cond_1

    .line 1741
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->c()V

    .line 1744
    :cond_1
    return-void
.end method

.method private s()V
    .locals 2

    .prologue
    .line 1747
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_0

    .line 1748
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    .line 1749
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    .line 1750
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->aq:Z

    .line 1751
    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->bh_()V

    .line 1752
    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->d()Landroid/view/View;

    move-result-object v0

    .line 1753
    if-eqz v0, :cond_0

    .line 1754
    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->removeView(Landroid/view/View;)V

    .line 1757
    :cond_0
    return-void
.end method

.method public static setAnimationTestHooks(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1

    .prologue
    .line 277
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 278
    sput-object p0, Lcom/twitter/library/widget/TweetView;->d:Landroid/animation/Animator$AnimatorListener;

    .line 279
    const-class v0, Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 280
    return-void
.end method

.method private setupInlineActionBar(Z)V
    .locals 1

    .prologue
    .line 1811
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    if-eqz v0, :cond_0

    .line 1812
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/InlineActionBar;->setForceHideDMInlineAction(Z)V

    .line 1814
    :cond_0
    return-void
.end method

.method private t()V
    .locals 2

    .prologue
    .line 1778
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->ax:I

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setSize(I)V

    .line 1779
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->t:Lcom/twitter/library/view/SocialProofView;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1}, Lcom/twitter/media/ui/image/UserImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/SocialProofView;->setTextOffset(I)V

    .line 1780
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 1529
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1530
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/widget/UserForwardView;->a(ILjava/lang/String;)V

    .line 1534
    :goto_0
    return-void

    .line 1532
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->z:Lcom/twitter/ui/widget/BadgeView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/ui/widget/BadgeView;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method a(Lcom/twitter/model/core/MediaEntity;)V
    .locals 2

    .prologue
    .line 1442
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    if-eqz v0, :cond_0

    .line 1443
    invoke-static {p1}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1444
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-interface {v0, v1, p0}, Lcom/twitter/library/view/d;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/TweetView;)V

    .line 1449
    :cond_0
    :goto_0
    return-void

    .line 1446
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-interface {v0, v1, p1, p0}, Lcom/twitter/library/view/d;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/library/widget/TweetView;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;)V
    .locals 6

    .prologue
    .line 689
    const/4 v3, 0x0

    new-instance v4, Lbxy;

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v4, v0, p1}, Lbxy;-><init>(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;)V

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;ZLbxy;Z)V

    .line 690
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;ZLbxy;)V
    .locals 6

    .prologue
    .line 713
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;ZLbxy;Z)V

    .line 714
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;ZLbxy;Z)V
    .locals 19

    .prologue
    .line 729
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v14

    .line 730
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->at:I

    .line 731
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 730
    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Lbxy;->a(ILjava/lang/Object;)V

    .line 732
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->n:I

    .line 734
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 732
    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Lbxy;->a(ILjava/lang/Object;)V

    .line 735
    const/4 v2, 0x2

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    invoke-virtual {v0, v2, v1}, Lbxy;->a(ILjava/lang/Object;)V

    .line 736
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->aE:Lbxz;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Lbxy;->a(ILjava/lang/Object;)V

    .line 739
    invoke-static/range {p1 .. p1}, Lcom/twitter/android/av/h;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/library/widget/TweetView;->aB:Z

    .line 740
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    if-eqz v2, :cond_d

    const/4 v4, 0x1

    .line 742
    :goto_0
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/twitter/library/widget/TweetView;->an:Z

    .line 743
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    .line 745
    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/twitter/ui/view/h;->e:Z

    if-nez v2, :cond_0

    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_0
    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/library/widget/TweetView;->as:Z

    .line 748
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->aw:Ljava/lang/String;

    .line 749
    invoke-static {}, Lbpi;->e()Ljava/lang/String;

    move-result-object v3

    .line 748
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    const/4 v2, 0x1

    move v13, v2

    .line 751
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/library/widget/TweetView;->aC:Z

    if-nez v2, :cond_1

    if-nez v13, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/twitter/model/core/Tweet;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v2

    if-nez v2, :cond_1e

    .line 752
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->aj:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    if-eqz v2, :cond_2

    .line 753
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->aj:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/PossiblySensitiveWarningView;->setVisibility(I)V

    .line 756
    :cond_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/twitter/library/widget/TweetView;->U:J

    move-wide/from16 v16, v0

    .line 757
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    .line 758
    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/twitter/ui/view/h;->f:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/twitter/library/widget/TweetView;->g(Z)V

    .line 759
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/library/widget/TweetView;->S:Ljava/lang/CharSequence;

    .line 760
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    .line 761
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/twitter/library/widget/TweetView;->U:J

    .line 762
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/library/widget/TweetView;->ak:I

    .line 763
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/library/widget/TweetView;->ac:Ljava/lang/String;

    .line 764
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->e:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    .line 765
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/library/widget/TweetView;->ad:Z

    .line 766
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/library/widget/TweetView;->ae:Z

    .line 767
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->A:Lcom/twitter/ui/widget/TextLayoutView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/ui/widget/TextLayoutView;->setTextWithVisibility(Ljava/lang/CharSequence;)V

    .line 768
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->B:Lcom/twitter/ui/widget/TextLayoutView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/ui/widget/TextLayoutView;->setTextWithVisibility(Ljava/lang/CharSequence;)V

    .line 770
    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getOwnerId()J

    move-result-wide v10

    .line 772
    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->m()Z

    move-result v5

    .line 774
    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v2

    .line 776
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/library/widget/TweetView;->r:Z

    if-eqz v3, :cond_10

    if-nez v5, :cond_3

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/library/widget/TweetView;->ag:Z

    if-nez v3, :cond_3

    .line 777
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/core/Tweet;->r()Z

    move-result v3

    if-eqz v3, :cond_10

    :cond_3
    if-nez v2, :cond_10

    .line 780
    invoke-static/range {p1 .. p1}, Lbxd;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v2

    if-nez v2, :cond_10

    const/4 v6, 0x1

    .line 782
    :goto_3
    invoke-virtual/range {p4 .. p4}, Lbxy;->a()Lcom/twitter/library/widget/renderablecontent/d;

    move-result-object v18

    .line 783
    if-eqz v18, :cond_4

    .line 784
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v2

    if-eqz v2, :cond_11

    .line 785
    invoke-virtual/range {p4 .. p4}, Lbxy;->b()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 786
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v2

    if-nez v2, :cond_11

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/core/Tweet;->j()Z

    move-result v2

    if-nez v2, :cond_11

    .line 787
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/core/Tweet;->I()Z

    move-result v2

    if-nez v2, :cond_11

    const/4 v7, 0x1

    :goto_4
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 789
    invoke-direct/range {v2 .. v7}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;ZZZZ)V

    .line 793
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/media/ui/image/UserImageView;->setFromMemoryOnly(Z)V

    .line 794
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/twitter/model/core/Tweet;->s:J

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v8, v9, v7}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;JZ)Z

    .line 796
    if-eqz p5, :cond_5

    .line 797
    if-eqz v4, :cond_12

    const v2, 0x3ecccccd    # 0.4f

    :goto_5
    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/twitter/util/ui/k;->a(Landroid/view/View;F)V

    .line 800
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/core/Tweet;->R()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 801
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/library/widget/TweetView;->al:Z

    invoke-virtual {v2, v3}, Lcom/twitter/library/view/QuoteView;->setDisplaySensitiveMedia(Z)V

    .line 802
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/library/widget/TweetView;->ag:Z

    invoke-virtual {v2, v3}, Lcom/twitter/library/view/QuoteView;->setAlwaysExpandMedia(Z)V

    .line 803
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    move/from16 v0, p3

    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/view/QuoteView;->a(Lcom/twitter/model/core/r;Z)V

    .line 804
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/twitter/util/ui/a;->a(Landroid/view/View;I)V

    .line 806
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/library/view/QuoteView;->setVisibility(I)V

    .line 812
    :goto_6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;Z)Lcom/twitter/model/core/e;

    move-result-object v2

    .line 814
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/twitter/model/core/Tweet;->U:[Lcom/twitter/model/core/d;

    invoke-static {v3}, Lcom/twitter/util/collection/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 816
    invoke-static/range {p1 .. p1}, Lbqj;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 817
    invoke-static {v5}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_14

    .line 818
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v7

    const/16 v8, 0x46

    if-gt v7, v8, :cond_14

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    iget-boolean v7, v7, Lcom/twitter/ui/view/h;->i:Z

    if-nez v7, :cond_14

    .line 820
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->H:Landroid/content/res/Resources;

    sget v8, Lazw$k;->tagline_location_poi:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v5, v9, v12

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 821
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->Q:Lcom/twitter/library/widget/g;

    invoke-virtual {v7, v5}, Lcom/twitter/library/widget/g;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/widget/g;

    .line 826
    :goto_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->Q:Lcom/twitter/library/widget/g;

    iget-object v7, v2, Lcom/twitter/model/core/e;->a:Ljava/lang/String;

    iget-object v2, v2, Lcom/twitter/model/core/e;->b:Lcom/twitter/model/core/v;

    invoke-virtual {v5, v7, v2, v3}, Lcom/twitter/library/widget/g;->a(Ljava/lang/CharSequence;Lcom/twitter/model/core/v;Ljava/lang/Iterable;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/library/widget/TweetView;->S:Ljava/lang/CharSequence;

    .line 828
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    if-eqz v2, :cond_6

    .line 829
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    iget-boolean v2, v2, Lcom/twitter/ui/view/h;->m:Z

    if-eqz v2, :cond_15

    .line 830
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/UserForwardView;->setVisibility(I)V

    .line 836
    :cond_6
    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->az:Lcom/twitter/library/revenue/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->H:Landroid/content/res/Resources;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lcom/twitter/library/revenue/a;->a(Lcom/twitter/model/core/Tweet;Landroid/content/res/Resources;)V

    .line 837
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->x:Lbxr;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v1, v10, v11}, Lbxr;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;J)V

    .line 838
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->u:Lbwz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->y:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v2}, Lcom/twitter/ui/widget/TextLayoutView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_16

    const/4 v12, 0x1

    :goto_9
    move-object/from16 v8, p1

    move-object/from16 v9, p2

    invoke-virtual/range {v7 .. v12}, Lbwz;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;JZ)V

    .line 841
    invoke-static {}, Lcmj;->a()Z

    move-result v3

    .line 843
    invoke-static {}, Lcmj;->b()Z

    move-result v5

    .line 845
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->H:Landroid/content/res/Resources;

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/twitter/model/core/Tweet;->q:J

    invoke-static {v2, v8, v9}, Lcom/twitter/util/aa;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v10

    .line 846
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->w:Lcom/twitter/ui/widget/TweetHeaderView;

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/twitter/ui/view/h;->g:Z

    if-nez v2, :cond_17

    const/4 v2, 0x1

    :goto_a
    invoke-virtual {v7, v2}, Lcom/twitter/ui/widget/TweetHeaderView;->setShowTimestamp(Z)V

    .line 847
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->w:Lcom/twitter/ui/widget/TweetHeaderView;

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v8

    invoke-static/range {p1 .. p1}, Lcom/twitter/library/view/e;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/twitter/model/core/Tweet;->L:Z

    if-eqz v2, :cond_18

    if-eqz v3, :cond_18

    const/4 v11, 0x1

    :goto_b
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/twitter/model/core/Tweet;->F:Z

    if-eqz v2, :cond_19

    if-eqz v5, :cond_19

    const/4 v12, 0x1

    :goto_c
    invoke-virtual/range {v7 .. v12}, Lcom/twitter/ui/widget/TweetHeaderView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 850
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->w:Lcom/twitter/ui/widget/TweetHeaderView;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-static {v0, v1, v2}, Lcom/twitter/library/view/e;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/TweetView;Lcom/twitter/ui/widget/TweetHeaderView;)V

    .line 852
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->w:Lcom/twitter/ui/widget/TweetHeaderView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/ui/widget/TweetHeaderView;->setOnAuthorClick(Landroid/view/View$OnClickListener;)V

    .line 854
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/library/widget/TweetView;->V:Z

    if-eqz v2, :cond_1a

    .line 855
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/twitter/library/widget/TweetView;->U:J

    const-wide/16 v8, 0x4

    or-long/2addr v2, v8

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/twitter/library/widget/TweetView;->U:J

    .line 860
    :goto_d
    const-wide/16 v2, 0x0

    cmp-long v2, v16, v2

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/twitter/library/widget/TweetView;->U:J

    const-wide/16 v8, 0x0

    cmp-long v2, v2, v8

    if-eqz v2, :cond_8

    .line 861
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->refreshDrawableState()V

    .line 866
    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v6, v13}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;ZZ)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 867
    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->s()V

    .line 871
    :cond_9
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v15, v6, v13}, Lcom/twitter/library/widget/TweetView;->a(ZLcom/twitter/model/core/Tweet;ZZ)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 872
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/library/widget/TweetView;->ar:Z

    .line 873
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    .line 874
    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->r()V

    .line 878
    :cond_a
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/library/widget/TweetView;->aC:Z

    .line 880
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->ay:Lcom/twitter/util/math/Size;

    .line 881
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v5

    .line 883
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/widget/TweetView;->m:I

    invoke-static {v14, v5, v2}, Lbrw;->a(Landroid/content/Context;Ljava/util/List;I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 885
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/library/widget/TweetView;->aa:Z

    if-nez v3, :cond_b

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 886
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->B:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/core/Tweet;->o()Z

    move-result v4

    invoke-static {v4}, Lcom/twitter/util/b;->a(Z)Landroid/text/Layout$Alignment;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/ui/widget/TextLayoutView;->a(Landroid/text/Layout$Alignment;)Lcom/twitter/ui/widget/TextLayoutView;

    .line 887
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->B:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v3, v2}, Lcom/twitter/ui/widget/TextLayoutView;->setTextWithVisibility(Ljava/lang/CharSequence;)V

    .line 890
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->K:Lcom/twitter/library/widget/TextContentView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->S:Ljava/lang/CharSequence;

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/core/Tweet;->o()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/widget/TextContentView;->a(Ljava/lang/CharSequence;Z)V

    .line 893
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->t:Lcom/twitter/library/view/SocialProofView;

    invoke-virtual {v2}, Lcom/twitter/library/view/SocialProofView;->getSocialProofAccessibilityString()Ljava/lang/String;

    move-result-object v9

    .line 894
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->S:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 895
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1b

    .line 896
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v4

    :goto_e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1c

    :goto_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    iget-object v6, v2, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    iget-object v7, v2, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/twitter/model/core/Tweet;->q:J

    move-object/from16 v3, p0

    .line 895
    invoke-static/range {v3 .. v11}, Lbxs;->a(Landroid/view/View;Lcax;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 904
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 905
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/widget/TweetView;->M:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/TweetView;->setBackgroundResource(I)V

    .line 910
    :goto_10
    if-eqz p5, :cond_c

    .line 911
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    .line 912
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    .line 919
    :cond_c
    :goto_11
    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->c(Lcom/twitter/model/core/Tweet;)V

    .line 921
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v2

    if-nez v2, :cond_1f

    const/4 v2, 0x1

    :goto_12
    invoke-virtual {v3, v2}, Lcom/twitter/media/ui/image/UserImageView;->a(Z)V

    .line 922
    return-void

    .line 740
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 745
    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 748
    :cond_f
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_2

    .line 780
    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_3

    .line 787
    :cond_11
    const/4 v7, 0x0

    goto/16 :goto_4

    .line 797
    :cond_12
    const/high16 v2, 0x3f800000    # 1.0f

    goto/16 :goto_5

    .line 808
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/twitter/library/view/QuoteView;->a(Z)V

    .line 809
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/twitter/library/view/QuoteView;->setVisibility(I)V

    goto/16 :goto_6

    .line 823
    :cond_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->Q:Lcom/twitter/library/widget/g;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/twitter/library/widget/g;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/widget/g;

    goto/16 :goto_7

    .line 832
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/library/widget/UserForwardView;->a(Lcom/twitter/model/core/Tweet;)V

    goto/16 :goto_8

    .line 838
    :cond_16
    const/4 v12, 0x0

    goto/16 :goto_9

    .line 846
    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_a

    .line 847
    :cond_18
    const/4 v11, 0x0

    goto/16 :goto_b

    :cond_19
    const/4 v12, 0x0

    goto/16 :goto_c

    .line 857
    :cond_1a
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/twitter/library/widget/TweetView;->U:J

    const-wide/16 v8, -0x5

    and-long/2addr v2, v8

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/twitter/library/widget/TweetView;->U:J

    goto/16 :goto_d

    .line 896
    :cond_1b
    const/4 v4, 0x0

    goto/16 :goto_e

    :cond_1c
    const/4 v5, 0x0

    goto/16 :goto_f

    .line 907
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->L:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/TweetView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_10

    .line 915
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->s:J

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;JZ)Z

    .line 917
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->e()V

    goto/16 :goto_11

    .line 921
    :cond_1f
    const/4 v2, 0x0

    goto :goto_12
.end method

.method public a(Lcom/twitter/model/core/Tweet;ZLbxy;)V
    .locals 1

    .prologue
    .line 707
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;ZLbxy;Z)V

    .line 708
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;ZLbxy;Z)V
    .locals 6

    .prologue
    .line 720
    sget-object v2, Lcom/twitter/library/widget/TweetView;->c:Lcom/twitter/ui/view/h;

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;ZLbxy;Z)V

    .line 722
    return-void
.end method

.method a(Lcom/twitter/model/core/TweetActionType;)V
    .locals 1

    .prologue
    .line 1505
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    if-eqz v0, :cond_0

    .line 1506
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    invoke-interface {v0, p1, p0}, Lcom/twitter/library/view/d;->a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/library/widget/TweetView;)V

    .line 1508
    :cond_0
    return-void
.end method

.method a(Lcom/twitter/model/media/EditableMedia;)V
    .locals 2

    .prologue
    .line 1461
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    if-eqz v0, :cond_0

    .line 1462
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-interface {v0, v1, p1, p0}, Lcom/twitter/library/view/d;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/media/EditableMedia;Lcom/twitter/library/widget/TweetView;)V

    .line 1464
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1549
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->af:Z

    if-nez v0, :cond_0

    .line 1561
    :goto_0
    return-void

    .line 1553
    :cond_0
    if-eqz p1, :cond_1

    .line 1554
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/model/core/Tweet;->a:Z

    .line 1555
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    iget v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    .line 1560
    :goto_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/TweetView;->c(Lcom/twitter/model/core/Tweet;)V

    goto :goto_0

    .line 1557
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    iput-boolean v2, v0, Lcom/twitter/model/core/Tweet;->a:Z

    .line 1558
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    iget v1, v1, Lcom/twitter/model/core/Tweet;->n:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    goto :goto_1
.end method

.method public a(ZZ)V
    .locals 1

    .prologue
    .line 1680
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ad:Z

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ae:Z

    if-eq v0, p2, :cond_1

    .line 1681
    :cond_0
    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->ad:Z

    .line 1682
    iput-boolean p2, p0, Lcom/twitter/library/widget/TweetView;->ae:Z

    .line 1683
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    .line 1684
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    .line 1686
    :cond_1
    return-void
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1424
    iget v1, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 3

    .prologue
    .line 1452
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    if-eqz v0, :cond_0

    .line 1453
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 1454
    if-eqz v0, :cond_0

    .line 1455
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-interface {v1, v2, v0, p0}, Lcom/twitter/library/view/d;->a(Lcom/twitter/model/core/Tweet;Lcax;Lcom/twitter/library/widget/TweetView;)V

    .line 1458
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1588
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->af:Z

    if-nez v0, :cond_1

    .line 1599
    :cond_0
    :goto_0
    return-void

    .line 1592
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-nez p1, :cond_2

    move v0, v6

    :goto_1
    iput-boolean v0, v1, Lcom/twitter/model/core/Tweet;->c:Z

    .line 1593
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    iget v2, v0, Lcom/twitter/model/core/Tweet;->k:I

    if-eqz p1, :cond_3

    const/4 v0, -0x1

    :goto_2
    add-int/2addr v0, v2

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v1, Lcom/twitter/model/core/Tweet;->k:I

    .line 1594
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/TweetView;->c(Lcom/twitter/model/core/Tweet;)V

    .line 1595
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    iget-boolean v0, v0, Lcom/twitter/ui/view/h;->h:Z

    if-eqz v0, :cond_0

    .line 1596
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->u:Lbwz;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->aD:Lcom/twitter/ui/view/h;

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->getOwnerId()J

    move-result-wide v4

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->y:Lcom/twitter/ui/widget/TextLayoutView;

    .line 1597
    invoke-virtual {v0}, Lcom/twitter/ui/widget/TextLayoutView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 1596
    :goto_3
    invoke-virtual/range {v1 .. v6}, Lbwz;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;JZ)V

    goto :goto_0

    :cond_2
    move v0, v7

    .line 1592
    goto :goto_1

    :cond_3
    move v0, v6

    .line 1593
    goto :goto_2

    :cond_4
    move v6, v7

    .line 1597
    goto :goto_3
.end method

.method c()V
    .locals 6

    .prologue
    .line 1467
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    if-eqz v0, :cond_0

    .line 1468
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    .line 1469
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->N()Lcom/twitter/model/core/MediaEntity;

    move-result-object v1

    .line 1470
    if-eqz v1, :cond_0

    .line 1471
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    iget-wide v4, v1, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-interface {v2, v0, v4, v5, p0}, Lcom/twitter/library/view/d;->a(Lcom/twitter/model/core/Tweet;JLcom/twitter/library/widget/TweetView;)V

    .line 1474
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 1565
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->af:Z

    if-nez v0, :cond_0

    .line 1574
    :goto_0
    return-void

    .line 1569
    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1570
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/UserForwardView;->setFollowButtonChecked(Z)V

    goto :goto_0

    .line 1572
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/TweetView;->c(Lcom/twitter/model/core/Tweet;)V

    goto :goto_0
.end method

.method d()V
    .locals 3

    .prologue
    .line 1477
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    if-eqz v0, :cond_0

    .line 1478
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/v;->b()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 1479
    if-eqz v0, :cond_0

    .line 1480
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-interface {v1, v2, v0}, Lcom/twitter/library/view/d;->b(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;)V

    .line 1483
    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 1578
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1337
    invoke-super {p0, p1}, Lcom/twitter/ui/widget/CellLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1339
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v0, p1, p0}, Lcom/twitter/library/widget/InlineActionBar;->a(Landroid/graphics/Canvas;Landroid/view/ViewGroup;)V

    .line 1340
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/UserImageView;->e()V

    .line 593
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v0}, Lcom/twitter/library/view/QuoteView;->e()V

    .line 594
    return-void
.end method

.method public e(Z)V
    .locals 2

    .prologue
    .line 1524
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->z:Lcom/twitter/ui/widget/BadgeView;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->p()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/BadgeView;->setVisibility(I)V

    .line 1525
    return-void

    .line 1524
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/UserImageView;->f()V

    .line 599
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v0}, Lcom/twitter/library/view/QuoteView;->f()V

    .line 600
    return-void
.end method

.method f(Z)V
    .locals 2

    .prologue
    .line 1436
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    if-eqz v0, :cond_0

    .line 1437
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-static {p0, v1, p1}, Lbxg;->a(Lbxf;Lcom/twitter/model/core/Tweet;Z)Lbxg;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/library/view/d;->a(Lbxg;)V

    .line 1439
    :cond_0
    return-void
.end method

.method g()V
    .locals 2

    .prologue
    .line 1486
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ai:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1487
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-interface {v0, v1, p0}, Lcom/twitter/library/view/d;->c(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/TweetView;)V

    .line 1489
    :cond_0
    return-void
.end method

.method public getAutoPlayableItem()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContentContainer()Lcom/twitter/library/widget/renderablecontent/c;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/widget/c;->a(Lcom/twitter/library/widget/renderablecontent/c;)Lcom/twitter/library/widget/a;

    move-result-object v0

    return-object v0
.end method

.method public getContent()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1085
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->S:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getContentContainer()Lcom/twitter/library/widget/renderablecontent/c;
    .locals 1

    .prologue
    .line 1538
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->e()Lcom/twitter/library/widget/renderablecontent/c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1539
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->e()Lcom/twitter/library/widget/renderablecontent/c;

    move-result-object v0

    .line 1541
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/c;->B:Lcom/twitter/library/widget/renderablecontent/c;

    goto :goto_0
.end method

.method getFavoriteLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1713
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    sget-object v1, Lcom/twitter/model/core/TweetActionType;->b:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/InlineActionBar;->a(Lcom/twitter/model/core/TweetActionType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFriendshipCache()Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 1353
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->P:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method public getPreviewEnabled()Z
    .locals 1

    .prologue
    .line 556
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->r:Z

    return v0
.end method

.method public getReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 544
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->u:Lbwz;

    invoke-virtual {v0}, Lbwz;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReasonIconResId()I
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->u:Lbwz;

    invoke-virtual {v0}, Lbwz;->b()I

    move-result v0

    return v0
.end method

.method getRetweetLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1718
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    sget-object v1, Lcom/twitter/model/core/TweetActionType;->c:Lcom/twitter/model/core/TweetActionType;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/InlineActionBar;->a(Lcom/twitter/model/core/TweetActionType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScribeItem()Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->am:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    return-object v0
.end method

.method public getTweet()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method public getTweetContentHost()Lcom/twitter/library/widget/renderablecontent/d;
    .locals 1

    .prologue
    .line 661
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 662
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ap:Lcom/twitter/library/widget/renderablecontent/d;

    return-object v0
.end method

.method h()V
    .locals 2

    .prologue
    .line 1492
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    if-eqz v0, :cond_0

    .line 1493
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-interface {v0, v1, p0}, Lcom/twitter/library/view/d;->b(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/TweetView;)V

    .line 1495
    :cond_0
    return-void
.end method

.method i()Z
    .locals 2

    .prologue
    .line 1498
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    if-eqz v0, :cond_0

    .line 1499
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    invoke-interface {v0, v1}, Lcom/twitter/library/view/d;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    .line 1501
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method j()V
    .locals 3

    .prologue
    .line 1517
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    iget-object v0, v0, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    if-eqz v0, :cond_0

    .line 1518
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    iget-object v2, v2, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    invoke-interface {v0, v1, v2, p0}, Lcom/twitter/library/view/d;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/geo/TwitterPlace;Lcom/twitter/library/widget/TweetView;)V

    .line 1520
    :cond_0
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1608
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->an:Z

    if-eqz v0, :cond_1

    .line 1609
    iput-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->an:Z

    .line 1611
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setFromMemoryOnly(Z)V

    .line 1612
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v0}, Lcom/twitter/library/view/QuoteView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1613
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/QuoteView;->setMediaFromMemoryOnly(Z)V

    .line 1615
    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->r()V

    .line 1617
    :cond_1
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 647
    sget-object v0, Lcom/twitter/library/widget/TweetView;->a:[I

    array-length v0, v0

    add-int/2addr v0, p1

    invoke-super {p0, v0}, Lcom/twitter/ui/widget/CellLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 648
    iget-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->V:Z

    if-eqz v1, :cond_0

    .line 649
    sget-object v1, Lcom/twitter/library/widget/TweetView;->a:[I

    invoke-static {v0, v1}, Lcom/twitter/library/widget/TweetView;->mergeDrawableStates([I[I)[I

    .line 651
    :cond_0
    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 614
    invoke-super {p0}, Lcom/twitter/ui/widget/CellLayout;->onDetachedFromWindow()V

    .line 615
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->s()V

    .line 616
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v0}, Lcom/twitter/library/view/QuoteView;->b()V

    .line 617
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->f()V

    .line 620
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->aC:Z

    .line 621
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1322
    invoke-super {p0, p1}, Lcom/twitter/ui/widget/CellLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1323
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_1

    .line 1333
    :cond_0
    :goto_0
    return-void

    .line 1327
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ad:Z

    if-eqz v0, :cond_2

    .line 1328
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1330
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ae:Z

    if-eqz v0, :cond_0

    .line 1331
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 606
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/ui/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607
    const/4 v0, 0x1

    .line 609
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/ui/widget/CellLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 1299
    invoke-super/range {p0 .. p5}, Lcom/twitter/ui/widget/CellLayout;->onLayout(ZIIII)V

    .line 1300
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->g_:Z

    if-eqz v0, :cond_3

    .line 1301
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingRight()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v2}, Lcom/twitter/media/ui/image/UserImageView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    .line 1302
    invoke-virtual {v1}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 1303
    :goto_0
    iget-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->ad:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->ae:Z

    if-eqz v1, :cond_2

    .line 1304
    :cond_0
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1}, Lcom/twitter/media/ui/image/UserImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    .line 1305
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1306
    iget v1, p0, Lcom/twitter/library/widget/TweetView;->h:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 1307
    iget-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->ad:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 1308
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->p:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->h:I

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    .line 1309
    invoke-virtual {v4}, Lcom/twitter/media/ui/image/UserImageView;->getTop()I

    move-result v4

    iget v5, p0, Lcom/twitter/library/widget/TweetView;->i:I

    sub-int/2addr v4, v5

    .line 1308
    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1312
    :cond_1
    iget-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->ae:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 1313
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->q:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    .line 1314
    invoke-virtual {v2}, Lcom/twitter/media/ui/image/UserImageView;->getBottom()I

    move-result v2

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->i:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->h:I

    add-int/2addr v3, v0

    .line 1315
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getHeight()I

    move-result v4

    .line 1313
    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1318
    :cond_2
    return-void

    .line 1302
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 15

    .prologue
    .line 1141
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->N:Lcom/twitter/model/core/Tweet;

    .line 1142
    if-nez v1, :cond_0

    .line 1143
    invoke-super/range {p0 .. p2}, Lcom/twitter/ui/widget/CellLayout;->onMeasure(II)V

    .line 1245
    :goto_0
    return-void

    .line 1147
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v1

    move/from16 v0, p1

    invoke-static {v1, v0}, Lcom/twitter/util/ui/i;->a(Landroid/content/Context;I)I

    move-result v5

    .line 1148
    invoke-static {v5}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    .line 1149
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v1

    sub-int v1, v9, v1

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingRight()I

    move-result v2

    sub-int v4, v1, v2

    .line 1151
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    invoke-static {v1}, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->a(Landroid/view/View;)Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;

    move-result-object v11

    .line 1152
    iget v1, v11, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->width:I

    if-gt v4, v1, :cond_1

    .line 1153
    const/high16 v1, 0x1000000

    const/high16 v2, 0x1000000

    invoke-virtual {p0, v1, v2}, Lcom/twitter/library/widget/TweetView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 1157
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingTop()I

    move-result v2

    invoke-direct {p0, v1, v2, v9}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;II)V

    .line 1159
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->t:Lcom/twitter/library/view/SocialProofView;

    invoke-virtual {v1}, Lcom/twitter/library/view/SocialProofView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_2

    .line 1160
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->t:Lcom/twitter/library/view/SocialProofView;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    move-object v1, p0

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/view/View;Landroid/graphics/Rect;III)Z

    .line 1161
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v1, v2, v9}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;II)V

    .line 1165
    :cond_2
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    move-object v1, p0

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/view/View;Landroid/graphics/Rect;III)Z

    .line 1166
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->D:Landroid/view/View;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    move-object v1, p0

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/view/View;Landroid/graphics/Rect;III)Z

    .line 1167
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1}, Lcom/twitter/media/ui/image/UserImageView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_b

    const/4 v10, 0x1

    .line 1168
    :goto_1
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->w:Lcom/twitter/ui/widget/TweetHeaderView;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    move-object v1, p0

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/view/View;Landroid/graphics/Rect;III)Z

    .line 1169
    iget-object v7, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;IIZLcom/twitter/ui/widget/CellLayout$CellLayoutParams;)V

    .line 1170
    invoke-virtual {v11}, Lcom/twitter/ui/widget/CellLayout$CellLayoutParams;->b()Landroid/graphics/Rect;

    move-result-object v1

    iget v14, v1, Landroid/graphics/Rect;->bottom:I

    .line 1173
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->y:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TextLayoutView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_3

    .line 1174
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->y:Lcom/twitter/ui/widget/TextLayoutView;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    move-object v1, p0

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/view/View;Landroid/graphics/Rect;III)Z

    .line 1175
    iget-object v7, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;IIZLcom/twitter/ui/widget/CellLayout$CellLayoutParams;)V

    .line 1179
    :cond_3
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->K:Lcom/twitter/library/widget/TextContentView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TextContentView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_4

    .line 1180
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->K:Lcom/twitter/library/widget/TextContentView;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    move-object v1, p0

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/view/View;Landroid/graphics/Rect;III)Z

    .line 1181
    iget-object v7, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;IIZLcom/twitter/ui/widget/CellLayout$CellLayoutParams;)V

    .line 1185
    :cond_4
    iget v1, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_c

    .line 1186
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->aj:Lcom/twitter/library/widget/PossiblySensitiveWarningView;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    move-object v1, p0

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/view/View;Landroid/graphics/Rect;III)Z

    .line 1187
    iget-object v7, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;IIZLcom/twitter/ui/widget/CellLayout$CellLayoutParams;)V

    .line 1197
    :goto_2
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v1}, Lcom/twitter/library/view/QuoteView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_5

    .line 1198
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    move-object v1, p0

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/view/View;Landroid/graphics/Rect;III)Z

    .line 1199
    iget-object v7, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;IIZLcom/twitter/ui/widget/CellLayout$CellLayoutParams;)V

    .line 1203
    :cond_5
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->B:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TextLayoutView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_6

    .line 1204
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->B:Lcom/twitter/ui/widget/TextLayoutView;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    move-object v1, p0

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/view/View;Landroid/graphics/Rect;III)Z

    .line 1205
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->getForwardMediaView()Landroid/view/View;

    move-result-object v1

    .line 1206
    if-eqz v1, :cond_e

    invoke-virtual {v1}, Landroid/view/View;->isClickable()Z

    move-result v1

    if-eqz v1, :cond_e

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_e

    .line 1207
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->B:Lcom/twitter/ui/widget/TextLayoutView;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/TextLayoutView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1211
    :goto_3
    iget-object v7, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;IIZLcom/twitter/ui/widget/CellLayout$CellLayoutParams;)V

    .line 1214
    :cond_6
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->z:Lcom/twitter/ui/widget/BadgeView;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/BadgeView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_f

    const/4 v1, 0x1

    move v12, v1

    .line 1216
    :goto_4
    const/4 v13, 0x1

    .line 1217
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/UserForwardView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_7

    .line 1218
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    move-object v1, p0

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/view/View;Landroid/graphics/Rect;III)Z

    .line 1219
    iget-object v7, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;IIZLcom/twitter/ui/widget/CellLayout$CellLayoutParams;)V

    .line 1223
    :cond_7
    if-eqz v10, :cond_8

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    if-le v14, v1, :cond_8

    .line 1224
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int v3, v14, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 1227
    :cond_8
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v1}, Lcom/twitter/library/widget/InlineActionBar;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_11

    .line 1228
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    move-object v1, p0

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/view/View;Landroid/graphics/Rect;III)Z

    .line 1229
    iget-object v7, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;IIZLcom/twitter/ui/widget/CellLayout$CellLayoutParams;)V

    .line 1230
    const/4 v1, 0x0

    .line 1234
    :goto_5
    if-eqz v12, :cond_a

    .line 1235
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v1}, Lcom/twitter/library/widget/InlineActionBar;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_9

    .line 1236
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    const/4 v2, 0x0

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->j:I

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 1238
    :cond_9
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->z:Lcom/twitter/ui/widget/BadgeView;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    move-object v1, p0

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/view/View;Landroid/graphics/Rect;III)Z

    .line 1239
    iget-object v7, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;IIZLcom/twitter/ui/widget/CellLayout$CellLayoutParams;)V

    .line 1240
    const/4 v1, 0x1

    .line 1243
    :cond_a
    move/from16 v0, p1

    invoke-static {v9, v0}, Lcom/twitter/library/widget/TweetView;->resolveSize(II)I

    move-result v2

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    .line 1244
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v1, v3

    :goto_6
    move/from16 v0, p2

    invoke-static {v1, v0}, Lcom/twitter/library/widget/TweetView;->resolveSize(II)I

    move-result v1

    .line 1243
    invoke-virtual {p0, v2, v1}, Lcom/twitter/library/widget/TweetView;->setMeasuredDimension(II)V

    goto/16 :goto_0

    .line 1167
    :cond_b
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 1189
    :cond_c
    iget-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->as:Z

    if-eqz v1, :cond_d

    .line 1190
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2, v3, v9, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 1192
    :cond_d
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    invoke-direct {p0, v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;)I

    move-result v6

    iget-object v7, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v3, v6, v7}, Lcom/twitter/library/widget/TweetView;->a(III)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 1193
    iget-object v7, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Rect;IIZLcom/twitter/ui/widget/CellLayout$CellLayoutParams;)V

    goto/16 :goto_2

    .line 1209
    :cond_e
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->B:Lcom/twitter/ui/widget/TextLayoutView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/TextLayoutView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    .line 1214
    :cond_f
    const/4 v1, 0x0

    move v12, v1

    goto/16 :goto_4

    .line 1244
    :cond_10
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->G:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_6

    :cond_11
    move v1, v13

    goto/16 :goto_5
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 625
    invoke-super {p0}, Lcom/twitter/ui/widget/CellLayout;->onStartTemporaryDetach()V

    .line 626
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/UserImageView;->f()V

    .line 628
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->aB:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->aA:Z

    if-eqz v0, :cond_0

    .line 639
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->aC:Z

    .line 640
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->s()V

    .line 642
    :cond_0
    return-void
.end method

.method public setAlwaysExpandMedia(Z)V
    .locals 1

    .prologue
    .line 1639
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ag:Z

    if-eq v0, p1, :cond_0

    .line 1640
    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->ag:Z

    .line 1641
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/view/QuoteView;->setAlwaysExpandMedia(Z)V

    .line 1642
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    .line 1644
    :cond_0
    return-void
.end method

.method public setAlwaysStripMediaUrls(Z)V
    .locals 1

    .prologue
    .line 1666
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->av:Z

    if-eq v0, p1, :cond_0

    .line 1667
    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->av:Z

    .line 1668
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    .line 1670
    :cond_0
    return-void
.end method

.method public setAutoLink(Z)V
    .locals 0

    .prologue
    .line 681
    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->au:Z

    .line 682
    return-void
.end method

.method public setContentSize(F)V
    .locals 4

    .prologue
    .line 1370
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->T:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_2

    .line 1371
    iput p1, p0, Lcom/twitter/library/widget/TweetView;->T:F

    .line 1372
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->T:F

    invoke-static {v0}, Lcni;->a(F)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->R:F

    .line 1373
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->T:F

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->R:F

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/view/QuoteView;->a(FF)V

    .line 1374
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->t:Lcom/twitter/library/view/SocialProofView;

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->R:F

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/SocialProofView;->setContentSize(F)V

    .line 1375
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->w:Lcom/twitter/ui/widget/TweetHeaderView;

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->T:F

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->R:F

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->R:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/ui/widget/TweetHeaderView;->a(FFF)V

    .line 1376
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->y:Lcom/twitter/ui/widget/TextLayoutView;

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->R:F

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TextLayoutView;->a(F)Lcom/twitter/ui/widget/TextLayoutView;

    .line 1377
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->z:Lcom/twitter/ui/widget/BadgeView;

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->R:F

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/BadgeView;->setContentSize(F)V

    .line 1378
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->K:Lcom/twitter/library/widget/TextContentView;

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->T:F

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TextContentView;->setContentSize(F)V

    .line 1379
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    if-eqz v0, :cond_0

    .line 1380
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->T:F

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->R:F

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/widget/UserForwardView;->a(FF)V

    .line 1382
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    if-eqz v0, :cond_1

    .line 1383
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->R:F

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/InlineActionBar;->setBylineSize(F)V

    .line 1385
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    .line 1386
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    .line 1388
    :cond_2
    return-void
.end method

.method public setCurationAction(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1129
    iput p1, p0, Lcom/twitter/library/widget/TweetView;->ai:I

    .line 1130
    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->D:Landroid/view/View;

    if-nez p1, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1131
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->w:Lcom/twitter/ui/widget/TweetHeaderView;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/TweetHeaderView;->setShowTimestamp(Z)V

    .line 1132
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->w:Lcom/twitter/ui/widget/TweetHeaderView;

    if-ne p1, v2, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TweetHeaderView;->a(Z)V

    .line 1133
    return-void

    :cond_1
    move v0, v1

    .line 1130
    goto :goto_0
.end method

.method public setDisplaySensitiveMedia(Z)V
    .locals 0

    .prologue
    .line 552
    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->al:Z

    .line 553
    return-void
.end method

.method public setExpandCardMedia(Z)V
    .locals 1

    .prologue
    .line 1650
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ah:Z

    if-eq v0, p1, :cond_0

    .line 1651
    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->ah:Z

    .line 1652
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    .line 1654
    :cond_0
    return-void
.end method

.method public setForceFollowButtonOnly(Z)V
    .locals 1

    .prologue
    .line 1404
    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->ab:Z

    .line 1405
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/UserForwardView;->setForceUserForwardView(Z)V

    .line 1406
    return-void
.end method

.method public setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V
    .locals 1

    .prologue
    .line 1343
    iput-object p1, p0, Lcom/twitter/library/widget/TweetView;->P:Lcom/twitter/model/util/FriendshipCache;

    .line 1344
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    if-eqz v0, :cond_0

    .line 1345
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->E:Lcom/twitter/library/widget/UserForwardView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/UserForwardView;->setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V

    .line 1347
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    if-eqz v0, :cond_1

    .line 1348
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->F:Lcom/twitter/library/widget/InlineActionBar;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/InlineActionBar;->setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V

    .line 1350
    :cond_1
    return-void
.end method

.method public setHideInlineActions(Z)V
    .locals 0

    .prologue
    .line 1400
    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->W:Z

    .line 1401
    return-void
.end method

.method public setHideMediaTagSummary(Z)V
    .locals 0

    .prologue
    .line 1409
    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->aa:Z

    .line 1410
    return-void
.end method

.method public setHideProfileImage(Z)V
    .locals 2

    .prologue
    .line 1418
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz v0, :cond_0

    .line 1419
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz p1, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setVisibility(I)V

    .line 1421
    :cond_0
    return-void

    .line 1419
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setHighlighted(Z)V
    .locals 1

    .prologue
    .line 1363
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->V:Z

    if-eq v0, p1, :cond_0

    .line 1364
    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->V:Z

    .line 1365
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->refreshDrawableState()V

    .line 1367
    :cond_0
    return-void
.end method

.method public setMaxLines(I)V
    .locals 1

    .prologue
    .line 567
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->K:Lcom/twitter/library/widget/TextContentView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/TextContentView;->setMaxLines(I)V

    .line 568
    return-void
.end method

.method public setMinLines(I)V
    .locals 1

    .prologue
    .line 578
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->K:Lcom/twitter/library/widget/TextContentView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/TextContentView;->setMinLines(I)V

    .line 579
    return-void
.end method

.method public setOnTweetViewClickListener(Lcom/twitter/library/view/d;)V
    .locals 2

    .prologue
    .line 1391
    iput-object p1, p0, Lcom/twitter/library/widget/TweetView;->O:Lcom/twitter/library/view/d;

    .line 1392
    if-eqz p1, :cond_0

    .line 1393
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1397
    :goto_0
    return-void

    .line 1395
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->v:Lcom/twitter/media/ui/image/UserImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setPromotedBadgeEnabled(Z)V
    .locals 1

    .prologue
    .line 1676
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->az:Lcom/twitter/library/revenue/a;

    invoke-virtual {v0, p1}, Lcom/twitter/library/revenue/a;->a(Z)V

    .line 1677
    return-void
.end method

.method public setQuoteDisplayMode(I)V
    .locals 1

    .prologue
    .line 1136
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/view/QuoteView;->setDisplayMode(I)V

    .line 1137
    return-void
.end method

.method public setReason(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->u:Lbwz;

    invoke-virtual {v0, p1}, Lbwz;->a(Ljava/lang/String;)V

    .line 537
    return-void
.end method

.method public setReasonIconResId(I)V
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->u:Lbwz;

    invoke-virtual {v0, p1}, Lbwz;->a(I)V

    .line 541
    return-void
.end method

.method public setScribeItem(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/twitter/library/widget/TweetView;->am:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 524
    return-void
.end method

.method public setShouldSimulateInlineActions(Z)V
    .locals 1

    .prologue
    .line 1627
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->af:Z

    if-eq v0, p1, :cond_0

    .line 1628
    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->af:Z

    .line 1629
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    .line 1631
    :cond_0
    return-void
.end method

.method public setShowSocialBadge(Z)V
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->u:Lbwz;

    invoke-virtual {v0, p1}, Lbwz;->a(Z)V

    .line 520
    return-void
.end method

.method public setSocialContextName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->u:Lbwz;

    invoke-virtual {v0, p1}, Lbwz;->b(Ljava/lang/String;)V

    .line 533
    return-void
.end method

.method public setTruncateText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->K:Lcom/twitter/library/widget/TextContentView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/TextContentView;->setTruncateText(Ljava/lang/CharSequence;)V

    .line 588
    return-void
.end method

.method public setTweet(Lcom/twitter/model/core/Tweet;)V
    .locals 3

    .prologue
    .line 685
    const/4 v1, 0x0

    new-instance v2, Lbxy;

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v2, v0, p1}, Lbxy;-><init>(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;)V

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v1, v2, v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;ZLbxy;Z)V

    .line 686
    return-void
.end method

.method public setTweetNoLayout(Lcom/twitter/model/core/Tweet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 693
    new-instance v1, Lbxy;

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v1, v0, p1}, Lbxy;-><init>(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {p0, p1, v2, v1, v2}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;ZLbxy;Z)V

    .line 695
    return-void
.end method
