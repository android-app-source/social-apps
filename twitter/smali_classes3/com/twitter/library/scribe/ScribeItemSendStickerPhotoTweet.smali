.class public Lcom/twitter/library/scribe/ScribeItemSendStickerPhotoTweet;
.super Lcom/twitter/analytics/model/MapScribeItem;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/scribe/ScribeItemSendStickerPhotoTweet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Lcom/twitter/library/scribe/ScribeItemSendStickerPhotoTweet$1;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItemSendStickerPhotoTweet$1;-><init>()V

    sput-object v0, Lcom/twitter/library/scribe/ScribeItemSendStickerPhotoTweet;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/analytics/model/MapScribeItem;-><init>(I)V

    .line 30
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/twitter/analytics/model/MapScribeItem;-><init>(Landroid/os/Parcel;)V

    .line 34
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/scribe/ScribeItemMediaDetails;)Lcom/twitter/library/scribe/ScribeItemSendStickerPhotoTweet;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/library/scribe/ScribeItemSendStickerPhotoTweet;->a(ILjava/lang/Object;)V

    .line 45
    return-object p0
.end method

.method protected a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string/jumbo v0, "media_details"

    return-object v0
.end method
