.class public Lcom/twitter/library/scribe/c;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/network/l;)I
    .locals 2

    .prologue
    .line 76
    if-eqz p0, :cond_0

    iget v0, p0, Lcom/twitter/network/l;->a:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_1

    .line 77
    :cond_0
    const/4 v0, -0x1

    .line 90
    :goto_0
    return v0

    .line 79
    :cond_1
    iget v0, p0, Lcom/twitter/network/l;->a:I

    if-eqz v0, :cond_2

    .line 80
    const/4 v0, 0x1

    goto :goto_0

    .line 82
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/network/l;->d:Z

    if-eqz v0, :cond_5

    .line 83
    iget-object v0, p0, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    instance-of v0, v0, Ljava/net/SocketTimeoutException;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    instance-of v0, v0, Lcom/twitter/network/apache/conn/ConnectTimeoutException;

    if-eqz v0, :cond_4

    .line 85
    :cond_3
    const/4 v0, 0x2

    goto :goto_0

    .line 87
    :cond_4
    const/4 v0, 0x4

    goto :goto_0

    .line 90
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lbis;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/model/av/AVMedia;)V
    .locals 2

    .prologue
    .line 118
    invoke-interface {p0}, Lbis;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->L:Ljava/lang/String;

    .line 119
    invoke-interface {p0, p2}, Lbis;->a(Lcom/twitter/model/av/AVMedia;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->M:Ljava/lang/String;

    .line 121
    invoke-interface {p0}, Lbis;->b()J

    move-result-wide v0

    .line 122
    iput-wide v0, p1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ab:J

    .line 123
    iput-wide v0, p1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->G:J

    .line 124
    return-void
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 171
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->a()Ljava/lang/String;

    move-result-object v4

    .line 173
    const/4 v0, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 185
    invoke-virtual {p0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e(I)Lcom/twitter/analytics/model/ScribeLog;

    .line 189
    :goto_1
    return-void

    .line 173
    :sswitch_0
    const-string/jumbo v5, "2g"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string/jumbo v5, "cellular"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string/jumbo v5, "wifi"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v3

    goto :goto_0

    .line 176
    :pswitch_0
    invoke-static {}, Lcrq;->a()Lcrq;

    move-result-object v0

    invoke-virtual {v0}, Lcrq;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(I)Lcom/twitter/analytics/model/ScribeLog;

    .line 177
    invoke-virtual {p0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e(I)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_1

    .line 181
    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e(I)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_1

    .line 173
    :sswitch_data_0
    .sparse-switch
        -0x36a22696 -> :sswitch_1
        0x675 -> :sswitch_0
        0x37af15 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    invoke-static {p1, p2, p3, p4, v0}, Lcom/twitter/library/scribe/b;->a(JLcgi;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 129
    return-void
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 133
    move-wide v0, p1

    move-object v2, p3

    move-object v3, p4

    move v5, p5

    move-object v6, v4

    invoke-static/range {v0 .. v6}, Lcom/twitter/library/scribe/b;->a(JLcgi;Ljava/lang/String;Ljava/lang/String;ILcom/twitter/model/timeline/r;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 135
    return-void
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;Lcom/twitter/model/timeline/r;)V
    .locals 7

    .prologue
    .line 151
    const/4 v4, 0x0

    move-wide v0, p1

    move-object v2, p3

    move-object v3, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/b;->a(JLcgi;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/timeline/r;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 153
    return-void
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 166
    invoke-static {p1, p2, p3}, Lcom/twitter/library/scribe/b;->b(JLjava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 167
    return-void
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcax;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 193
    .line 194
    invoke-static {p1, p2, p3}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcax;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 193
    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 195
    return-void
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/library/scribe/ScribeItemsProvider;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 227
    if-eqz p2, :cond_0

    .line 229
    invoke-interface {p2, p1, p3, p4}, Lcom/twitter/library/scribe/ScribeItemsProvider;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 228
    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/util/List;)V

    .line 231
    :cond_0
    return-void
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/scribe/n;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 218
    if-eqz p2, :cond_0

    .line 219
    invoke-static {p1, p3, p2, p4, p5}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcom/twitter/library/scribe/n;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/util/List;)V

    .line 222
    :cond_0
    return-void
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 206
    if-eqz p2, :cond_0

    .line 207
    invoke-static {p1, p2, p3}, Lcom/twitter/library/scribe/b;->b(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/util/List;)V

    .line 210
    :cond_0
    return-void
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Ljava/lang/String;Lcar;J)V
    .locals 2

    .prologue
    .line 157
    if-eqz p3, :cond_0

    .line 159
    invoke-static {p1, p2, p3, p4, p5}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Ljava/lang/String;Lcar;J)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 162
    :cond_0
    return-void
.end method

.method public static a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/network/l;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p1, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p1, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 103
    :cond_0
    iget v0, p1, Lcom/twitter/network/l;->a:I

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(I)Lcom/twitter/analytics/model/ScribeLog;

    .line 104
    iget-object v0, p1, Lcom/twitter/network/l;->o:Lcom/twitter/network/HttpOperation$Protocol;

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation$Protocol;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/network/l;->p:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 105
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "connected"

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 107
    invoke-static {p0}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 108
    return-void

    .line 105
    :cond_1
    const-string/jumbo v0, "disconnected"

    goto :goto_0
.end method

.method public static a(Lcom/twitter/analytics/model/ScribeLog;Lcom/twitter/network/HttpOperation;Z)V
    .locals 2

    .prologue
    .line 55
    const-string/jumbo v0, "cdn::::request"

    invoke-virtual {p0}, Lcom/twitter/analytics/model/ScribeLog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const-string/jumbo v0, "x-cache"

    invoke-virtual {p1, v0}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/model/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 66
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/model/ScribeLog;Ljava/lang/String;Lcom/twitter/network/l;)V

    .line 67
    return-void

    .line 58
    :cond_0
    invoke-static {p1}, Lcom/twitter/library/network/ab;->d(Lcom/twitter/network/HttpOperation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    const-string/jumbo v0, "polling"

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/model/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0

    .line 60
    :cond_1
    if-eqz p2, :cond_2

    .line 61
    const-string/jumbo v0, "non-polling-foreground"

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/model/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0

    .line 63
    :cond_2
    const-string/jumbo v0, "non-polling-background"

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/model/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0
.end method

.method public static a(Lcom/twitter/analytics/model/ScribeLog;Ljava/lang/String;Lcom/twitter/network/l;)V
    .locals 4

    .prologue
    .line 38
    .line 39
    invoke-virtual {p0, p1}, Lcom/twitter/analytics/model/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/network/l;->q:Ljava/lang/String;

    .line 40
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/model/ScribeLog;->p(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    iget v1, p2, Lcom/twitter/network/l;->a:I

    .line 41
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/model/ScribeLog;->b(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/network/l;->e:J

    .line 42
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/model/ScribeLog;->d(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    iget v1, p2, Lcom/twitter/network/l;->n:I

    .line 43
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/model/ScribeLog;->i(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    iget v1, p2, Lcom/twitter/network/l;->k:I

    .line 44
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/model/ScribeLog;->j(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/network/l;->o:Lcom/twitter/network/HttpOperation$Protocol;

    .line 45
    invoke-virtual {v1}, Lcom/twitter/network/HttpOperation$Protocol;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Lcom/twitter/network/l;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/analytics/model/ScribeLog;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    const/4 v1, 0x2

    .line 46
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/model/ScribeLog;->d(I)Lcom/twitter/analytics/model/ScribeLog;

    .line 48
    invoke-virtual {p0}, Lcom/twitter/analytics/model/ScribeLog;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget v0, p2, Lcom/twitter/network/l;->j:I

    if-eqz v0, :cond_0

    .line 49
    iget v0, p2, Lcom/twitter/network/l;->j:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/model/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 51
    :cond_0
    return-void
.end method

.method public static b(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-static {p1, p2, p3, p4, v0}, Lcom/twitter/library/scribe/b;->a(JLcgi;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 147
    return-void
.end method

.method public static b(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-static {p1, p2, p3, p4, v0}, Lcom/twitter/library/scribe/b;->a(JLcgi;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 140
    iput p5, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ax:I

    .line 141
    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 142
    return-void
.end method

.method public static b(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcax;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 199
    invoke-static {p1, p2, p3}, Lcom/twitter/library/scribe/b;->b(Landroid/content/Context;Lcax;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 201
    return-void
.end method
