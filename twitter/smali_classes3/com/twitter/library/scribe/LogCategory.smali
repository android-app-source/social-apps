.class public final enum Lcom/twitter/library/scribe/LogCategory;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/library/scribe/LogCategory;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/library/scribe/LogCategory;

.field public static final enum b:Lcom/twitter/library/scribe/LogCategory;

.field public static final enum c:Lcom/twitter/library/scribe/LogCategory;

.field private static final d:Lcok$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcok$a",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final synthetic e:[Lcom/twitter/library/scribe/LogCategory;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15
    new-instance v0, Lcom/twitter/library/scribe/LogCategory;

    const-string/jumbo v1, "CLIENT_APPLOG_UPLOAD_EVENT"

    const-string/jumbo v2, "client_applog_upload_event"

    invoke-direct {v0, v1, v3, v2}, Lcom/twitter/library/scribe/LogCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/scribe/LogCategory;->a:Lcom/twitter/library/scribe/LogCategory;

    .line 16
    new-instance v0, Lcom/twitter/library/scribe/LogCategory;

    const-string/jumbo v1, "CLIENT_NETWORK_REQUEST_EVENT"

    const-string/jumbo v2, "client_network_request_event"

    invoke-direct {v0, v1, v4, v2}, Lcom/twitter/library/scribe/LogCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/scribe/LogCategory;->b:Lcom/twitter/library/scribe/LogCategory;

    .line 17
    new-instance v0, Lcom/twitter/library/scribe/LogCategory;

    const-string/jumbo v1, "DDG_IMPRESSION"

    const-string/jumbo v2, "ddg_impression"

    invoke-direct {v0, v1, v5, v2}, Lcom/twitter/library/scribe/LogCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/scribe/LogCategory;->c:Lcom/twitter/library/scribe/LogCategory;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/twitter/library/scribe/LogCategory;

    sget-object v1, Lcom/twitter/library/scribe/LogCategory;->a:Lcom/twitter/library/scribe/LogCategory;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/library/scribe/LogCategory;->b:Lcom/twitter/library/scribe/LogCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/library/scribe/LogCategory;->c:Lcom/twitter/library/scribe/LogCategory;

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/library/scribe/LogCategory;->e:[Lcom/twitter/library/scribe/LogCategory;

    .line 20
    const-string/jumbo v0, "thrift_logging_category_blacklist"

    .line 22
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/LogCategory$1;

    invoke-direct {v2}, Lcom/twitter/library/scribe/LogCategory$1;-><init>()V

    .line 21
    invoke-static {v0, v1, v2}, Lcok;->a(Ljava/lang/String;Ljava/lang/Object;Lcpp;)Lcok$a;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/scribe/LogCategory;->d:Lcok$a;

    .line 20
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput-object p3, p0, Lcom/twitter/library/scribe/LogCategory;->mName:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/scribe/LogCategory;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/twitter/library/scribe/LogCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/LogCategory;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/scribe/LogCategory;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/twitter/library/scribe/LogCategory;->e:[Lcom/twitter/library/scribe/LogCategory;

    invoke-virtual {v0}, [Lcom/twitter/library/scribe/LogCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/scribe/LogCategory;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lcom/twitter/library/scribe/LogCategory;->d:Lcok$a;

    invoke-virtual {v0}, Lcok$a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom;

    invoke-virtual {v0}, Lcom;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iget-object v1, p0, Lcom/twitter/library/scribe/LogCategory;->mName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/scribe/LogCategory;->mName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "test_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/scribe/LogCategory;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
