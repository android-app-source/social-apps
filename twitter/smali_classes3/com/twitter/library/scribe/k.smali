.class public abstract Lcom/twitter/library/scribe/k;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T",
        "LogCollection:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field protected static final a:Ljava/nio/charset/Charset;


# instance fields
.field protected final b:Landroid/content/Context;

.field protected final c:J

.field protected final d:Lcom/twitter/model/account/OAuthToken;

.field protected final e:Lcom/twitter/metrics/c;

.field protected final f:Ljava/lang/String;

.field protected final g:Lcom/twitter/library/scribe/g;

.field protected final h:Lcom/twitter/library/scribe/ScribeService$c;

.field protected final i:Z

.field protected final j:Lcom/twitter/library/scribe/ScribeDatabaseHelper$LogType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-string/jumbo v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/scribe/k;->a:Ljava/nio/charset/Charset;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;JLcom/twitter/model/account/OAuthToken;Lcom/twitter/metrics/c;Ljava/lang/String;Lcom/twitter/library/scribe/g;Lcom/twitter/library/scribe/ScribeService$c;ZLcom/twitter/library/scribe/ScribeDatabaseHelper$LogType;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/twitter/library/scribe/k;->b:Landroid/content/Context;

    .line 71
    iput-wide p2, p0, Lcom/twitter/library/scribe/k;->c:J

    .line 72
    iput-object p4, p0, Lcom/twitter/library/scribe/k;->d:Lcom/twitter/model/account/OAuthToken;

    .line 73
    iput-object p6, p0, Lcom/twitter/library/scribe/k;->f:Ljava/lang/String;

    .line 74
    iput-object p5, p0, Lcom/twitter/library/scribe/k;->e:Lcom/twitter/metrics/c;

    .line 75
    iput-object p7, p0, Lcom/twitter/library/scribe/k;->g:Lcom/twitter/library/scribe/g;

    .line 76
    iput-object p8, p0, Lcom/twitter/library/scribe/k;->h:Lcom/twitter/library/scribe/ScribeService$c;

    .line 77
    iput-boolean p9, p0, Lcom/twitter/library/scribe/k;->i:Z

    .line 78
    iput-object p10, p0, Lcom/twitter/library/scribe/k;->j:Lcom/twitter/library/scribe/ScribeDatabaseHelper$LogType;

    .line 79
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT",
            "LogCollection;",
            ")V"
        }
    .end annotation

    .prologue
    .line 198
    return-void
.end method

.method public final a()Z
    .locals 10

    .prologue
    const/4 v5, 0x5

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 121
    .line 122
    const/16 v0, 0x1e

    move v1, v2

    .line 124
    :cond_0
    const/4 v3, 0x6

    invoke-static {v3}, Lcom/twitter/util/y;->a(I)Ljava/lang/String;

    move-result-object v6

    .line 126
    :try_start_0
    invoke-virtual {p0, v6, v0}, Lcom/twitter/library/scribe/k;->b(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v3

    .line 127
    if-eqz v3, :cond_1

    invoke-virtual {p0, v3}, Lcom/twitter/library/scribe/k;->c(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    :cond_1
    move v3, v4

    .line 149
    :goto_0
    if-nez v3, :cond_0

    .line 150
    return v1

    .line 130
    :cond_2
    invoke-virtual {p0, v3, v6}, Lcom/twitter/library/scribe/k;->a(Ljava/lang/Object;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    move v3, v1

    goto :goto_0

    .line 132
    :catch_0
    move-exception v3

    .line 133
    invoke-static {v3}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 134
    if-eq v0, v5, :cond_3

    .line 137
    iget-boolean v0, p0, Lcom/twitter/library/scribe/k;->i:Z

    if-eqz v0, :cond_5

    .line 138
    const-string/jumbo v0, "ScribeService"

    const-string/jumbo v3, "OOM while flush user logs, tune down the log size"

    invoke-static {v0, v3}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v5

    move v3, v2

    .line 147
    :goto_1
    iget-object v7, p0, Lcom/twitter/library/scribe/k;->g:Lcom/twitter/library/scribe/g;

    const-string/jumbo v8, "0"

    iget-object v9, p0, Lcom/twitter/library/scribe/k;->j:Lcom/twitter/library/scribe/ScribeDatabaseHelper$LogType;

    invoke-virtual {v9}, Lcom/twitter/library/scribe/ScribeDatabaseHelper$LogType;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v6, v8, v9}, Lcom/twitter/library/scribe/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 143
    :cond_3
    iget-boolean v1, p0, Lcom/twitter/library/scribe/k;->i:Z

    if-eqz v1, :cond_4

    .line 144
    const-string/jumbo v1, "ScribeService"

    const-string/jumbo v3, "OOM while flush user logs, abort"

    invoke-static {v1, v3}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move v1, v4

    move v3, v4

    goto :goto_1

    :cond_5
    move v0, v5

    move v3, v2

    goto :goto_1
.end method

.method protected final a(Ljava/lang/Object;Ljava/lang/String;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT",
            "LogCollection;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 155
    .line 156
    if-nez p1, :cond_0

    .line 194
    :goto_0
    return v0

    .line 160
    :cond_0
    iget-boolean v1, p0, Lcom/twitter/library/scribe/k;->i:Z

    if-eqz v1, :cond_1

    .line 161
    const-string/jumbo v1, "ScribeService"

    const-string/jumbo v2, "Starting request"

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_1
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/twitter/library/scribe/k;->b(Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 168
    :try_start_1
    iget-object v1, p0, Lcom/twitter/library/scribe/k;->h:Lcom/twitter/library/scribe/ScribeService$c;

    invoke-interface {v1}, Lcom/twitter/library/scribe/ScribeService$c;->a()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    .line 169
    :try_start_2
    iget-object v2, p0, Lcom/twitter/library/scribe/k;->h:Lcom/twitter/library/scribe/ScribeService$c;

    invoke-interface {v2}, Lcom/twitter/library/scribe/ScribeService$c;->b()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v2

    .line 171
    if-eqz v1, :cond_4

    .line 172
    iget-boolean v0, p0, Lcom/twitter/library/scribe/k;->i:Z

    if-eqz v0, :cond_2

    .line 173
    const-string/jumbo v0, "ScribeService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "request success reqId="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/scribe/k;->g:Lcom/twitter/library/scribe/g;

    invoke-interface {v0, p2}, Lcom/twitter/library/scribe/g;->a(Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/twitter/library/scribe/k;->e:Lcom/twitter/metrics/c;

    if-eqz v0, :cond_3

    .line 177
    iget-object v0, p0, Lcom/twitter/library/scribe/k;->e:Lcom/twitter/metrics/c;

    int-to-long v2, v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/metrics/c;->a(J)V

    :cond_3
    :goto_1
    move v0, v1

    .line 194
    goto :goto_0

    .line 180
    :cond_4
    iget-boolean v3, p0, Lcom/twitter/library/scribe/k;->i:Z

    if-eqz v3, :cond_5

    .line 181
    const-string/jumbo v3, "ScribeService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "request failed reqId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " statusCode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_5
    if-eqz v2, :cond_6

    .line 185
    iget-object v3, p0, Lcom/twitter/library/scribe/k;->g:Lcom/twitter/library/scribe/g;

    invoke-interface {v3, p2}, Lcom/twitter/library/scribe/g;->b(Ljava/lang/String;)V

    .line 186
    const/16 v3, 0x190

    if-ne v2, v3, :cond_6

    .line 187
    invoke-virtual {p0, p1}, Lcom/twitter/library/scribe/k;->a(Ljava/lang/Object;)V

    .line 190
    :cond_6
    iget-object v2, p0, Lcom/twitter/library/scribe/k;->g:Lcom/twitter/library/scribe/g;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/library/scribe/k;->j:Lcom/twitter/library/scribe/ScribeDatabaseHelper$LogType;

    invoke-virtual {v3}, Lcom/twitter/library/scribe/ScribeDatabaseHelper$LogType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p2, v0, v3}, Lcom/twitter/library/scribe/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lcom/twitter/library/scribe/k;->g:Lcom/twitter/library/scribe/g;

    invoke-interface {v0}, Lcom/twitter/library/scribe/g;->a()V

    goto :goto_1

    .line 171
    :catchall_0
    move-exception v1

    move v2, v0

    move v3, v0

    :goto_2
    if-eqz v3, :cond_9

    .line 172
    iget-boolean v0, p0, Lcom/twitter/library/scribe/k;->i:Z

    if-eqz v0, :cond_7

    .line 173
    const-string/jumbo v0, "ScribeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "request success reqId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_7
    iget-object v0, p0, Lcom/twitter/library/scribe/k;->g:Lcom/twitter/library/scribe/g;

    invoke-interface {v0, p2}, Lcom/twitter/library/scribe/g;->a(Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/twitter/library/scribe/k;->e:Lcom/twitter/metrics/c;

    if-eqz v0, :cond_8

    .line 177
    iget-object v0, p0, Lcom/twitter/library/scribe/k;->e:Lcom/twitter/metrics/c;

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/metrics/c;->a(J)V

    .line 191
    :cond_8
    :goto_3
    throw v1

    .line 180
    :cond_9
    iget-boolean v2, p0, Lcom/twitter/library/scribe/k;->i:Z

    if-eqz v2, :cond_a

    .line 181
    const-string/jumbo v2, "ScribeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "request failed reqId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " statusCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_a
    iget-object v2, p0, Lcom/twitter/library/scribe/k;->g:Lcom/twitter/library/scribe/g;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/library/scribe/k;->j:Lcom/twitter/library/scribe/ScribeDatabaseHelper$LogType;

    invoke-virtual {v3}, Lcom/twitter/library/scribe/ScribeDatabaseHelper$LogType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p2, v0, v3}, Lcom/twitter/library/scribe/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lcom/twitter/library/scribe/k;->g:Lcom/twitter/library/scribe/g;

    invoke-interface {v0}, Lcom/twitter/library/scribe/g;->a()V

    goto :goto_3

    .line 171
    :catchall_1
    move-exception v1

    move v2, v3

    move v3, v0

    goto :goto_2

    :catchall_2
    move-exception v2

    move-object v6, v2

    move v2, v3

    move v3, v1

    move-object v1, v6

    goto :goto_2
.end method

.method protected abstract b(Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT",
            "LogCollection;",
            ")I"
        }
    .end annotation
.end method

.method protected abstract b(Ljava/lang/String;I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)TT",
            "LogCollection;"
        }
    .end annotation
.end method

.method protected abstract c(Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT",
            "LogCollection;",
            ")Z"
        }
    .end annotation
.end method
