.class public Lcom/twitter/library/scribe/b;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(JLcax;)Lcom/twitter/analytics/feature/model/MomentScribeDetails;
    .locals 4

    .prologue
    .line 678
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 679
    invoke-virtual {v0, p0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 680
    const-string/jumbo v1, "id"

    invoke-virtual {p2, v1}, Lcax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 681
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 682
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 683
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 685
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    return-object v0
.end method

.method public static a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;
    .locals 12

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 580
    if-nez p2, :cond_0

    .line 613
    :goto_0
    return-object v5

    .line 583
    :cond_0
    new-array v0, v8, [I

    .line 584
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 585
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    aget v2, v0, v9

    int-to-float v2, v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 586
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    aget v0, v0, v7

    int-to-float v0, v0

    sub-float v0, v2, v0

    float-to-int v2, v0

    .line 587
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 588
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 589
    const/4 v10, -0x1

    .line 592
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 593
    instance-of v6, v0, Ljava/lang/String;

    if-eqz v6, :cond_7

    .line 594
    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    .line 597
    :goto_1
    instance-of v0, p1, Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_2

    .line 599
    check-cast p1, Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {p1}, Lcom/twitter/media/ui/image/MediaImageView;->getImageRequest()Lcom/twitter/media/request/a;

    move-result-object v0

    .line 600
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/media/request/a;->a()Ljava/lang/String;

    move-result-object v5

    :cond_1
    move-object v11, v5

    move v5, v7

    move-object v7, v11

    .line 613
    :goto_2
    new-instance v0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/twitter/analytics/feature/model/NativeCardUserAction;-><init>(IIIIILjava/lang/String;Ljava/lang/String;I)V

    move-object v5, v0

    goto :goto_0

    .line 601
    :cond_2
    instance-of v0, p1, Landroid/widget/Button;

    if-eqz v0, :cond_3

    .line 602
    const/4 v0, 0x4

    .line 603
    check-cast p1, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v7, v5

    move v5, v0

    goto :goto_2

    .line 604
    :cond_3
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 606
    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v7, v5

    move v5, v8

    goto :goto_2

    .line 607
    :cond_4
    instance-of v0, p1, Lcom/twitter/library/av/VideoPlayerView;

    if-eqz v0, :cond_5

    .line 608
    const/4 v0, 0x5

    .line 609
    check-cast p1, Lcom/twitter/library/av/VideoPlayerView;

    invoke-virtual {p1}, Lcom/twitter/library/av/VideoPlayerView;->getCurrentMediaSource()Ljava/lang/String;

    move-result-object v5

    move-object v7, v5

    move v5, v0

    goto :goto_2

    .line 610
    :cond_5
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_6

    move-object v7, v5

    move v5, v9

    .line 611
    goto :goto_2

    :cond_6
    move-object v7, v5

    move v5, v10

    goto :goto_2

    :cond_7
    move-object v6, v5

    goto :goto_1
.end method

.method public static a()Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 337
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 338
    const/16 v1, 0x10

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 339
    const/4 v1, 0x0

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->h:I

    .line 340
    const-string/jumbo v1, "single"

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 341
    return-object v0
.end method

.method public static a(J)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 403
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 404
    const/16 v1, 0x1e

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 405
    iput-wide p0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 406
    return-object v0
.end method

.method public static a(JLcgi;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 8

    .prologue
    .line 267
    const/4 v5, -0x1

    const/4 v6, 0x0

    move-wide v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v6}, Lcom/twitter/library/scribe/b;->a(JLcgi;Ljava/lang/String;Ljava/lang/String;ILcom/twitter/model/timeline/r;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLcgi;Ljava/lang/String;Ljava/lang/String;ILcom/twitter/model/timeline/r;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 279
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 280
    iput-wide p0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 281
    const/4 v1, 0x3

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 282
    iput-object p4, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 283
    iput p5, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 284
    if-eqz p2, :cond_0

    .line 285
    iget-object v1, p2, Lcgi;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->e:Ljava/lang/String;

    .line 286
    iget-object v1, p2, Lcgi;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->f:Ljava/lang/String;

    .line 288
    :cond_0
    iput-object p3, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->k:Ljava/lang/String;

    .line 289
    iput-object p6, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 291
    return-object v0
.end method

.method public static a(JLcgi;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/timeline/r;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 8

    .prologue
    .line 273
    const/4 v5, -0x1

    move-wide v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/twitter/library/scribe/b;->a(JLcgi;Ljava/lang/String;Ljava/lang/String;ILcom/twitter/model/timeline/r;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLjava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 247
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 248
    iput-wide p0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 253
    iput-object p2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 254
    iput-object p2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->w:Ljava/lang/String;

    .line 255
    iput p3, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 256
    iput p4, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 257
    return-object v0
.end method

.method public static a(Landroid/content/Context;JLcax;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 3

    .prologue
    .line 143
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 144
    iput-wide p1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 145
    const/4 v1, 0x6

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 146
    iput-object p4, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 147
    if-eqz p3, :cond_0

    .line 148
    invoke-virtual {p3}, Lcax;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->l:Ljava/lang/String;

    .line 150
    :cond_0
    if-eqz p3, :cond_1

    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/twitter/library/card/ae;->e(Lcax;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 151
    invoke-static {v0, p3, p0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcax;Landroid/content/Context;)V

    .line 153
    :cond_1
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcax;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 495
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 496
    const/16 v1, 0x19

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 497
    if-eqz p1, :cond_0

    .line 498
    invoke-static {v0, p1, p0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcax;Landroid/content/Context;)V

    .line 500
    :cond_0
    iput-object p2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->u:Ljava/lang/String;

    .line 501
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 1

    .prologue
    .line 547
    invoke-static {p0, p2, p3}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 548
    iput-object p1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ar:Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    .line 549
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 9

    .prologue
    .line 76
    new-instance v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v8}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 77
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->t:J

    iput-wide v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 78
    const/4 v0, 0x0

    iput v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 79
    iput-object p2, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 80
    new-instance v0, Lcom/twitter/analytics/feature/model/e;

    .line 81
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v1

    sget-object v2, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    invoke-static {v1, v2}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 82
    invoke-static {p1}, Lcom/twitter/library/av/playback/ab;->p(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/twitter/library/av/playback/ab;->r(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v3

    .line 83
    invoke-static {p1}, Lcom/twitter/library/av/playback/ab;->s(Lcom/twitter/model/core/Tweet;)J

    move-result-wide v4

    invoke-static {p1}, Lcom/twitter/library/av/playback/ab;->t(Lcom/twitter/model/core/Tweet;)I

    move-result v6

    .line 84
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ah()Z

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/twitter/analytics/feature/model/e;-><init>(ILjava/lang/String;Ljava/lang/String;JIZ)V

    iput-object v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->am:Lcom/twitter/analytics/feature/model/e;

    .line 85
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/ae;->e(Lcax;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-static {v8, v0, p0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcax;Landroid/content/Context;)V

    .line 94
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->r()Z

    move-result v0

    iput-boolean v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->j:Z

    .line 95
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->u:J

    iput-wide v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->d:J

    .line 98
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    .line 100
    iget-object v1, v0, Lcgi;->c:Ljava/lang/String;

    iput-object v1, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->e:Ljava/lang/String;

    .line 101
    iget-object v0, v0, Lcgi;->d:Ljava/lang/String;

    iput-object v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->f:Ljava/lang/String;

    .line 104
    :cond_1
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    iput-object v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 105
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->h()J

    move-result-wide v0

    iput-wide v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aB:J

    .line 107
    return-object v8

    .line 87
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 88
    const/4 v0, 0x2

    iput v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    goto :goto_0

    .line 89
    :cond_3
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->j()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p1}, Lcom/twitter/library/av/playback/ab;->d(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 90
    :cond_4
    const/4 v0, 0x3

    iput v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    goto :goto_0

    .line 92
    :cond_5
    const/4 v0, -0x1

    iput v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/core/r;Lcom/twitter/model/core/Tweet;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 9

    .prologue
    .line 204
    new-instance v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v8}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 205
    iget-wide v0, p1, Lcom/twitter/model/core/r;->e:J

    iput-wide v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 206
    const/16 v0, 0x17

    iput v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 207
    const-string/jumbo v0, "quoted_tweet"

    iput-object v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 208
    new-instance v0, Lcom/twitter/analytics/feature/model/e;

    iget-object v1, p1, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-object v1, v1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    sget-object v2, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    .line 209
    invoke-static {v1, v2}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 210
    invoke-static {p2}, Lcom/twitter/library/av/playback/ab;->p(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Lcom/twitter/library/av/playback/ab;->r(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v3

    .line 211
    invoke-static {p2}, Lcom/twitter/library/av/playback/ab;->s(Lcom/twitter/model/core/Tweet;)J

    move-result-wide v4

    invoke-static {p2}, Lcom/twitter/library/av/playback/ab;->t(Lcom/twitter/model/core/Tweet;)I

    move-result v6

    .line 212
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ah()Z

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/twitter/analytics/feature/model/e;-><init>(ILjava/lang/String;Ljava/lang/String;JIZ)V

    iput-object v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->am:Lcom/twitter/analytics/feature/model/e;

    .line 213
    iget-object v0, p1, Lcom/twitter/model/core/r;->k:Lcax;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/core/r;->k:Lcax;

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/ae;->e(Lcax;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    iget-object v0, p1, Lcom/twitter/model/core/r;->k:Lcax;

    invoke-static {v8, v0, p0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcax;Landroid/content/Context;)V

    .line 222
    :goto_0
    iget-object v0, p1, Lcom/twitter/model/core/r;->m:Lcgi;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p1, Lcom/twitter/model/core/r;->m:Lcgi;

    .line 224
    iget-object v1, v0, Lcgi;->c:Ljava/lang/String;

    iput-object v1, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->e:Ljava/lang/String;

    .line 225
    iget-object v0, v0, Lcgi;->d:Ljava/lang/String;

    iput-object v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->f:Ljava/lang/String;

    .line 227
    :cond_0
    return-object v8

    .line 215
    :cond_1
    iget-object v0, p1, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    sget-object v1, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    invoke-static {v0, v1}, Lcom/twitter/model/util/c;->c(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 216
    const/4 v0, 0x2

    iput v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    goto :goto_0

    .line 217
    :cond_2
    iget-object v0, p1, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0}, Lcom/twitter/model/util/c;->f(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 218
    const/4 v0, 0x3

    iput v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    goto :goto_0

    .line 220
    :cond_3
    const/4 v0, -0x1

    iput v0, v8, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcar;J)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 5

    .prologue
    .line 297
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 299
    const/4 v1, 0x3

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 300
    iput-object p1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->l:Ljava/lang/String;

    .line 301
    const-string/jumbo v1, "app_id"

    invoke-static {v1, p2}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    .line 302
    iget-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p0, :cond_0

    .line 303
    iget-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/twitter/util/d;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 304
    const/4 v1, 0x2

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    .line 309
    :cond_0
    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v1, p3, v2

    if-lez v1, :cond_1

    .line 310
    iput-wide p3, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 312
    :cond_1
    return-object v0

    .line 306
    :cond_2
    const/4 v1, 0x1

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    goto :goto_0
.end method

.method public static a(Lcec;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 8

    .prologue
    .line 427
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 428
    iget-wide v2, p0, Lcec;->a:J

    iput-wide v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 429
    const/16 v0, 0x20

    iput v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 431
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v2

    .line 432
    iget-object v0, p0, Lcec;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcee;

    .line 433
    new-instance v4, Lcom/twitter/analytics/feature/model/g$a;

    invoke-direct {v4}, Lcom/twitter/analytics/feature/model/g$a;-><init>()V

    invoke-virtual {v0}, Lcee;->a()Lcdu;

    move-result-object v0

    iget-wide v6, v0, Lcdu;->h:J

    invoke-virtual {v4, v6, v7}, Lcom/twitter/analytics/feature/model/g$a;->a(J)Lcom/twitter/analytics/feature/model/g$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/g$a;->q()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 435
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/f$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/f$a;-><init>()V

    .line 436
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/f$a;->a(Ljava/util/List;)Lcom/twitter/analytics/feature/model/f$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/f$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/f;

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->as:Lcom/twitter/analytics/feature/model/f;

    .line 438
    return-object v1
.end method

.method public static a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 6

    .prologue
    .line 417
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 418
    iput-object p0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ao:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 419
    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 420
    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->b:J

    iput-wide v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 421
    const/4 v1, 0x0

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 423
    :cond_0
    return-object v0
.end method

.method public static a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 237
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 238
    invoke-virtual {p0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 239
    const/4 v1, 0x3

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 240
    iget-object v1, p0, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 242
    return-object v0
.end method

.method public static a(Lcom/twitter/model/dms/a;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 121
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 122
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->q()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 123
    const/4 v1, 0x6

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 124
    iput-object p1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 125
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->f()Lcax;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->f()Lcax;

    move-result-object v1

    invoke-virtual {v1}, Lcax;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->l:Ljava/lang/String;

    .line 129
    :cond_0
    return-object v0
.end method

.method public static a(Lcom/twitter/model/people/b;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 469
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 470
    invoke-interface {p0}, Lcom/twitter/model/people/b;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ay:Ljava/lang/String;

    .line 471
    invoke-interface {p0}, Lcom/twitter/model/people/b;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->az:Ljava/lang/String;

    .line 472
    return-object v0
.end method

.method public static a(Lcom/twitter/model/people/b;Z)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 477
    invoke-static {p0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/people/b;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 478
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aA:Ljava/lang/Boolean;

    .line 479
    return-object v0
.end method

.method public static a(Lcom/twitter/model/people/l;Lcom/twitter/model/people/b;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 3

    .prologue
    .line 485
    iget-object v0, p0, Lcom/twitter/model/people/l;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 486
    iput p2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ax:I

    .line 487
    invoke-interface {p1}, Lcom/twitter/model/people/b;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ay:Ljava/lang/String;

    .line 488
    invoke-interface {p1}, Lcom/twitter/model/people/b;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->az:Ljava/lang/String;

    .line 489
    new-instance v0, Lcom/twitter/model/timeline/r$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/r$a;-><init>()V

    iget-object v2, p0, Lcom/twitter/model/people/l;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/model/timeline/r$a;->e(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/timeline/r$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 490
    return-object v1
.end method

.method public static a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 316
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 317
    iput-object p0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 318
    const/4 v1, 0x3

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 319
    return-object v0
.end method

.method public static a(Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 345
    invoke-static {}, Lcom/twitter/library/scribe/b;->a()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 346
    iput-object p0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 347
    invoke-static {p1}, Lcom/twitter/model/topic/TwitterTopic;->e(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->x:Ljava/lang/String;

    .line 348
    return-object v0
.end method

.method public static a(Ljava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 262
    const-wide/16 v0, -0x1

    invoke-static {v0, v1, p0, p1, p2}, Lcom/twitter/library/scribe/b;->a(JLjava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 323
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 324
    iput-object p0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 325
    const/16 v1, 0xc

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 326
    iput-object p1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 327
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 554
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 555
    if-eqz p1, :cond_0

    .line 556
    iput-object p1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->x:Ljava/lang/String;

    .line 558
    :cond_0
    if-eqz p0, :cond_1

    .line 559
    iput-object p0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 560
    const/16 v1, 0x10

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 562
    :cond_1
    const/4 v1, -0x1

    if-eq p2, v1, :cond_2

    .line 563
    invoke-static {p2}, Lcom/twitter/model/topic/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 565
    :cond_2
    return-object v0
.end method

.method public static a(JLjava/lang/String;)Lcom/twitter/analytics/model/ScribeItem;
    .locals 2

    .prologue
    .line 391
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 392
    const/16 v0, 0x1c

    iput v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 393
    iput-wide p0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 394
    new-instance v0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;-><init>()V

    .line 396
    invoke-virtual {v0, p0, p1}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    move-result-object v0

    .line 397
    invoke-virtual {v0, p2}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    move-result-object v0

    .line 398
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ar:Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    .line 399
    return-object v1
.end method

.method public static a(Lcom/twitter/model/revenue/a;)Lcom/twitter/analytics/model/ScribeItem;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 526
    const-string/jumbo v0, "tweet"

    iget-object v1, p0, Lcom/twitter/model/revenue/a;->b:Lcom/twitter/model/revenue/a$b;

    iget-object v1, v1, Lcom/twitter/model/revenue/a$b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 527
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 528
    iput v4, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 529
    iget-object v0, p0, Lcom/twitter/model/revenue/a;->b:Lcom/twitter/model/revenue/a$b;

    iget-wide v2, v0, Lcom/twitter/model/revenue/a$b;->a:J

    iput-wide v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 530
    iget-object v0, p0, Lcom/twitter/model/revenue/a;->a:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->w:Lcgi;

    iget-object v0, v0, Lcgi;->c:Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->e:Ljava/lang/String;

    .line 531
    iget-object v0, p0, Lcom/twitter/model/revenue/a;->a:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->w:Lcgi;

    iget-object v0, v0, Lcgi;->k:Ljava/util/Set;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aC:Ljava/util/List;

    .line 532
    new-instance v0, Lcom/twitter/analytics/feature/model/a$b;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/a$b;-><init>()V

    iget-object v2, p0, Lcom/twitter/model/revenue/a;->b:Lcom/twitter/model/revenue/a$b;

    iget-wide v2, v2, Lcom/twitter/model/revenue/a$b;->f:J

    .line 533
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/a$b;->b(J)Lcom/twitter/analytics/feature/model/a$b;

    move-result-object v0

    .line 534
    iget-object v2, p0, Lcom/twitter/model/revenue/a;->b:Lcom/twitter/model/revenue/a$b;

    iget-object v2, v2, Lcom/twitter/model/revenue/a$b;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 535
    iget-object v2, p0, Lcom/twitter/model/revenue/a;->b:Lcom/twitter/model/revenue/a$b;

    iget-object v2, v2, Lcom/twitter/model/revenue/a$b;->d:Ljava/lang/String;

    const-wide/16 v4, 0x0

    invoke-static {v2, v4, v5}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/a$b;->a(J)Lcom/twitter/analytics/feature/model/a$b;

    .line 537
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/a$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/a;

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aq:Lcom/twitter/analytics/feature/model/a;

    move-object v0, v1

    .line 540
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/timeline/t;)Lcom/twitter/analytics/model/ScribeItem;
    .locals 4

    .prologue
    .line 518
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 519
    const/16 v1, 0x1e

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 520
    iget-object v1, p0, Lcom/twitter/model/timeline/t;->a:Lcom/twitter/model/revenue/c;

    iget-object v1, v1, Lcom/twitter/model/revenue/c;->a:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-static {v1, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 521
    return-object v0
.end method

.method public static a(Ljava/util/List;)Ljava/util/Collection;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcdy;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v2

    .line 443
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v3

    .line 445
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdy;

    .line 446
    iget-object v1, v0, Lcdy;->b:Lcdu;

    iget-wide v6, v1, Lcdu;->g:J

    .line 447
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 448
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 449
    iput-wide v6, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 450
    const/16 v5, 0x20

    iput v5, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 451
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v8

    invoke-interface {v3, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v2, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    :cond_0
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    new-instance v5, Lcom/twitter/analytics/feature/model/g$a;

    invoke-direct {v5}, Lcom/twitter/analytics/feature/model/g$a;-><init>()V

    iget-object v0, v0, Lcdy;->b:Lcdu;

    iget-wide v6, v0, Lcdu;->h:J

    .line 455
    invoke-virtual {v5, v6, v7}, Lcom/twitter/analytics/feature/model/g$a;->a(J)Lcom/twitter/analytics/feature/model/g$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/g$a;->q()Ljava/lang/Object;

    move-result-object v0

    .line 454
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 458
    :cond_1
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 459
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 460
    new-instance v5, Lcom/twitter/analytics/feature/model/f$a;

    invoke-direct {v5}, Lcom/twitter/analytics/feature/model/f$a;-><init>()V

    .line 461
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 460
    invoke-virtual {v5, v1}, Lcom/twitter/analytics/feature/model/f$a;->a(Ljava/util/List;)Lcom/twitter/analytics/feature/model/f$a;

    move-result-object v1

    .line 461
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/f$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/f;

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->as:Lcom/twitter/analytics/feature/model/f;

    goto :goto_1

    .line 464
    :cond_2
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/scribe/n;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/scribe/n;",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 189
    invoke-interface {p1, p0, p2, p3, p4}, Lcom/twitter/library/scribe/n;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 190
    invoke-static {v0, p0, p2}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/util/collection/h;Landroid/content/Context;Lcom/twitter/model/core/Tweet;)V

    .line 191
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private static a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcax;Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 619
    const/4 v0, 0x6

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    .line 620
    const-string/jumbo v0, "Android-12"

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->o:Ljava/lang/String;

    .line 621
    invoke-virtual {p1}, Lcax;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->l:Ljava/lang/String;

    .line 622
    invoke-virtual {p1}, Lcax;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->p:Ljava/lang/String;

    .line 623
    invoke-virtual {p1}, Lcax;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->q:Ljava/lang/String;

    .line 624
    invoke-virtual {p1}, Lcax;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->s:Ljava/lang/String;

    .line 625
    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/ae;->a(Lcax;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->r:Z

    .line 626
    invoke-virtual {p1}, Lcax;->i()Ljava/util/Map;

    move-result-object v2

    .line 627
    if-eqz v2, :cond_6

    .line 628
    const-string/jumbo v0, "app_id"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcaw;

    invoke-static {v0}, Lcaw;->a(Lcaw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 629
    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    .line 630
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 631
    invoke-static {p2, v0}, Lcom/twitter/util/d;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 632
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    .line 637
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 638
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcaw;

    .line 639
    iget-object v1, v0, Lcaw;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 640
    iget-object v1, v0, Lcaw;->c:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 641
    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->C:Ljava/util/Map;

    iget-object v4, v0, Lcaw;->d:Ljava/lang/String;

    iget-object v0, v0, Lcaw;->c:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 634
    :cond_2
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    goto :goto_0

    .line 642
    :cond_3
    iget-object v1, v0, Lcaw;->c:Ljava/lang/Object;

    instance-of v1, v1, Lcay;

    if-eqz v1, :cond_4

    .line 643
    iget-object v1, v0, Lcaw;->c:Ljava/lang/Object;

    check-cast v1, Lcay;

    .line 644
    iget-object v1, v1, Lcay;->a:Ljava/lang/String;

    .line 645
    if-eqz v1, :cond_1

    .line 646
    iget-object v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->C:Ljava/util/Map;

    iget-object v0, v0, Lcaw;->d:Ljava/lang/String;

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 648
    :cond_4
    iget-object v1, v0, Lcaw;->c:Ljava/lang/Object;

    instance-of v1, v1, Lcom/twitter/model/card/property/ImageSpec;

    if-eqz v1, :cond_1

    .line 649
    iget-object v1, v0, Lcaw;->c:Ljava/lang/Object;

    check-cast v1, Lcom/twitter/model/card/property/ImageSpec;

    .line 650
    iget-object v1, v1, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    .line 651
    if-eqz v1, :cond_1

    .line 652
    iget-object v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->C:Ljava/util/Map;

    iget-object v0, v0, Lcaw;->d:Ljava/lang/String;

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 657
    :cond_5
    const-string/jumbo v0, "card_url"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcaw;

    invoke-static {v0}, Lcaw;->a(Lcaw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 658
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 659
    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->s:Ljava/lang/String;

    .line 662
    :cond_6
    return-void
.end method

.method private static a(Lcom/twitter/util/collection/h;Landroid/content/Context;Lcom/twitter/model/core/Tweet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/h",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;",
            "Landroid/content/Context;",
            "Lcom/twitter/model/core/Tweet;",
            ")V"
        }
    .end annotation

    .prologue
    .line 666
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p2, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    invoke-static {p1, v0, p2}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcom/twitter/model/core/r;Lcom/twitter/model/core/Tweet;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 669
    :cond_0
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->am()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 670
    iget-wide v0, p2, Lcom/twitter/model/core/Tweet;->G:J

    .line 671
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/b;->a(JLcax;)Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    move-result-object v0

    .line 670
    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 673
    :cond_1
    return-void
.end method

.method public static b()Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 410
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 411
    const/16 v1, 0x13

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 412
    new-instance v1, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;-><init>()V

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    .line 413
    return-object v0
.end method

.method public static b(JLjava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 570
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 571
    const/16 v1, 0x1b

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 572
    iput-wide p0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 573
    iput-object p2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->k:Ljava/lang/String;

    .line 574
    return-object v0
.end method

.method public static b(Landroid/content/Context;Lcax;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 506
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 507
    const/4 v1, 0x6

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 508
    if-eqz p1, :cond_0

    .line 509
    invoke-virtual {p1}, Lcax;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->l:Ljava/lang/String;

    .line 512
    :cond_0
    iput-object p2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->u:Ljava/lang/String;

    .line 513
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 1

    .prologue
    .line 331
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 332
    iput-object p0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 333
    return-object v0
.end method

.method public static b(Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 364
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 365
    const/16 v1, 0x11

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 366
    iput-object p0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 367
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 368
    add-int/lit8 v1, p1, 0x1

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 370
    :cond_0
    return-object v0
.end method

.method public static b(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/model/core/Tweet;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 169
    invoke-static {p0, p1, p2}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 170
    invoke-static {v0, p0, p1}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/util/collection/h;Landroid/content/Context;Lcom/twitter/model/core/Tweet;)V

    .line 171
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 1

    .prologue
    .line 358
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 359
    iput-object p0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->t:Ljava/lang/String;

    .line 360
    return-object v0
.end method
