.class public Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;
.super Lcom/twitter/analytics/model/ScribeSection;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    new-instance v0, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo$1;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo$1;-><init>()V

    sput-object v0, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 91
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "event_namespace"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "media_count"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "media_position"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "original_media_position"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "orientation"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "source"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "torch_active"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "duration_ms"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 103
    const/4 v0, 0x0

    sget-object v1, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a:[Ljava/lang/String;

    array-length v1, v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/analytics/model/ScribeSection;-><init>(Ljava/lang/String;I)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/twitter/analytics/model/ScribeSection;-><init>(Landroid/os/Parcel;)V

    .line 108
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/media/EditableSegmentedVideo;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 111
    invoke-direct {p0}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;-><init>()V

    .line 112
    iget-object v0, p1, Lcom/twitter/model/media/EditableSegmentedVideo;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/SegmentedVideoFile;

    .line 113
    iget-object v1, v0, Lcom/twitter/media/model/SegmentedVideoFile;->f:Lcom/twitter/util/math/Size;

    .line 114
    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->a()I

    move-result v4

    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->b()I

    move-result v1

    if-le v4, v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {p0, v1}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->c(I)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    .line 117
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 118
    iget v4, v0, Lcom/twitter/media/model/SegmentedVideoFile;->h:I

    invoke-static {v4, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 119
    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v1, v2, :cond_1

    :goto_1
    invoke-virtual {p0, v2}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->d(I)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    .line 122
    iget-object v1, v0, Lcom/twitter/media/model/SegmentedVideoFile;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0}, Lcom/twitter/media/model/SegmentedVideoFile;->d()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {p0, v1, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(IIJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;

    .line 123
    return-void

    :cond_0
    move v1, v3

    .line 114
    goto :goto_0

    :cond_1
    move v2, v3

    .line 119
    goto :goto_1
.end method


# virtual methods
.method public a(IIJ)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;
    .locals 3

    .prologue
    .line 159
    const/4 v0, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(ILjava/lang/Object;)V

    .line 160
    const/4 v0, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(ILjava/lang/Object;)V

    .line 161
    const/4 v0, 0x7

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(ILjava/lang/Object;)V

    .line 162
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;
    .locals 2

    .prologue
    .line 167
    const/4 v0, 0x0

    new-instance v1, Lcom/twitter/library/scribe/ScribeSectionNamespace;

    invoke-direct {v1, p1}, Lcom/twitter/library/scribe/ScribeSectionNamespace;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(ILjava/lang/Object;)V

    .line 168
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;
    .locals 2

    .prologue
    .line 146
    const/4 v0, 0x6

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(ILjava/lang/Object;)V

    .line 147
    return-object p0
.end method

.method protected a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b(I)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;
    .locals 2

    .prologue
    .line 133
    const/4 v0, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(ILjava/lang/Object;)V

    .line 134
    return-object p0
.end method

.method public c(I)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;
    .locals 2

    .prologue
    .line 140
    const/4 v0, 0x4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(ILjava/lang/Object;)V

    .line 141
    return-object p0
.end method

.method public d(I)Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;
    .locals 2

    .prologue
    .line 152
    const/4 v0, 0x5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/scribe/ScribeSectionSegmentedVideo;->a(ILjava/lang/Object;)V

    .line 153
    return-object p0
.end method
