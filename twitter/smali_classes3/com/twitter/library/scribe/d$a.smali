.class public Lcom/twitter/library/scribe/d$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/scribe/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 153
    new-instance v0, Lcom/evernote/android/job/JobRequest$a;

    const-string/jumbo v1, "ScribeFlushJob"

    invoke-direct {v0, v1}, Lcom/evernote/android/job/JobRequest$a;-><init>(Ljava/lang/String;)V

    const-wide/32 v2, 0x493e0

    add-long/2addr v2, p1

    .line 154
    invoke-virtual {v0, p1, p2, v2, v3}, Lcom/evernote/android/job/JobRequest$a;->a(JJ)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    sget-object v1, Lcom/evernote/android/job/JobRequest$NetworkType;->b:Lcom/evernote/android/job/JobRequest$NetworkType;

    .line 155
    invoke-virtual {v0, v1}, Lcom/evernote/android/job/JobRequest$a;->a(Lcom/evernote/android/job/JobRequest$NetworkType;)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 156
    invoke-virtual {v0, v4}, Lcom/evernote/android/job/JobRequest$a;->a(Z)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    sget-object v1, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->b:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    .line 157
    invoke-virtual {v0, p1, p2, v1}, Lcom/evernote/android/job/JobRequest$a;->a(JLcom/evernote/android/job/JobRequest$BackoffPolicy;)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 158
    invoke-virtual {v0, v4}, Lcom/evernote/android/job/JobRequest$a;->d(Z)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 159
    invoke-virtual {v0, v4}, Lcom/evernote/android/job/JobRequest$a;->e(Z)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest$a;->a()Lcom/evernote/android/job/JobRequest;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->t()I

    .line 162
    return-void
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 165
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/g;->b(Z)Z

    .line 166
    new-instance v0, Lcom/evernote/android/job/JobRequest$a;

    const-string/jumbo v1, "ScribeFlushJob"

    invoke-direct {v0, v1}, Lcom/evernote/android/job/JobRequest$a;-><init>(Ljava/lang/String;)V

    .line 167
    invoke-virtual {v0, p1, p2}, Lcom/evernote/android/job/JobRequest$a;->a(J)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 168
    invoke-virtual {v0, v1}, Lcom/evernote/android/job/JobRequest$a;->e(Z)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest$a;->a()Lcom/evernote/android/job/JobRequest;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->t()I

    .line 171
    return-void
.end method
