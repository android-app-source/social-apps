.class public Lcom/twitter/library/scribe/ScribeService$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/scribe/ScribeService$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/scribe/ScribeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/scribe/ScribeService$a$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/model/account/OAuthToken;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/network/apache/message/BasicNameValuePair;",
            ">;"
        }
    .end annotation
.end field

.field private c:[B

.field private d:Ljava/lang/String;

.field private e:Lcom/twitter/library/scribe/ScribeService$a$a;

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lcom/twitter/library/scribe/ScribeService$c;
    .locals 3

    .prologue
    .line 375
    new-instance v0, Lcom/twitter/library/network/k;

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeService$a;->d:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/twitter/library/network/k;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 376
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/k;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/t;

    iget-object v2, p0, Lcom/twitter/library/scribe/ScribeService$a;->a:Lcom/twitter/model/account/OAuthToken;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    .line 377
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/k;

    move-result-object v0

    const-string/jumbo v1, "Scribing is never triggered by a user action."

    .line 378
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Ljava/lang/String;)Lcom/twitter/library/network/k;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeService$a;->e:Lcom/twitter/library/scribe/ScribeService$a$a;

    .line 379
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeService$a;->b:Ljava/util/List;

    .line 380
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Ljava/util/List;)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 381
    invoke-virtual {v0}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 382
    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/library/scribe/ScribeService$a;->f:Z

    .line 383
    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v0

    iget v0, v0, Lcom/twitter/network/l;->a:I

    iput v0, p0, Lcom/twitter/library/scribe/ScribeService$a;->g:I

    .line 384
    return-object p0
.end method

.method public a(Lcom/twitter/model/account/OAuthToken;)Lcom/twitter/library/scribe/ScribeService$c;
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeService$a;->a:Lcom/twitter/model/account/OAuthToken;

    .line 346
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeService$c;
    .locals 0

    .prologue
    .line 363
    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeService$a;->d:Ljava/lang/String;

    .line 364
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/library/scribe/ScribeService$c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/network/apache/message/BasicNameValuePair;",
            ">;)",
            "Lcom/twitter/library/scribe/ScribeService$c;"
        }
    .end annotation

    .prologue
    .line 357
    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeService$a;->b:Ljava/util/List;

    .line 358
    return-object p0
.end method

.method public a([B)Lcom/twitter/library/scribe/ScribeService$c;
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeService$a;->c:[B

    .line 352
    return-object p0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/twitter/library/scribe/ScribeService$a;->f:Z

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 411
    iget v0, p0, Lcom/twitter/library/scribe/ScribeService$a;->g:I

    return v0
.end method

.method public b(Landroid/content/Context;)Lcom/twitter/library/scribe/ScribeService$c;
    .locals 3

    .prologue
    .line 389
    new-instance v0, Lcom/twitter/library/network/k;

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeService$a;->d:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/twitter/library/network/k;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 390
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/k;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/t;

    iget-object v2, p0, Lcom/twitter/library/scribe/ScribeService$a;->a:Lcom/twitter/model/account/OAuthToken;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    .line 391
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/k;

    move-result-object v0

    const-string/jumbo v1, "Scribing is never triggered by a user action."

    .line 392
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Ljava/lang/String;)Lcom/twitter/library/network/k;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeService$a;->e:Lcom/twitter/library/scribe/ScribeService$a$a;

    .line 393
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 394
    new-instance v1, Lcom/twitter/network/apache/entity/b;

    iget-object v2, p0, Lcom/twitter/library/scribe/ScribeService$a;->c:[B

    invoke-direct {v1, v2}, Lcom/twitter/network/apache/entity/b;-><init>([B)V

    .line 395
    const-string/jumbo v2, "application/octet-stream"

    invoke-virtual {v1, v2}, Lcom/twitter/network/apache/entity/b;->a(Ljava/lang/String;)V

    .line 396
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/apache/e;)Lcom/twitter/library/network/k;

    .line 398
    invoke-virtual {v0}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 399
    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/library/scribe/ScribeService$a;->f:Z

    .line 400
    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v0

    iget v0, v0, Lcom/twitter/network/l;->a:I

    iput v0, p0, Lcom/twitter/library/scribe/ScribeService$a;->g:I

    .line 401
    return-object p0
.end method
