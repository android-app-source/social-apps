.class public Lcom/twitter/library/scribe/EmptyScribeItemsProvider;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/scribe/ScribeItemsProvider;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/scribe/EmptyScribeItemsProvider;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/twitter/library/scribe/EmptyScribeItemsProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/twitter/library/scribe/EmptyScribeItemsProvider;

    invoke-direct {v0}, Lcom/twitter/library/scribe/EmptyScribeItemsProvider;-><init>()V

    sput-object v0, Lcom/twitter/library/scribe/EmptyScribeItemsProvider;->a:Lcom/twitter/library/scribe/EmptyScribeItemsProvider;

    .line 22
    new-instance v0, Lcom/twitter/library/scribe/EmptyScribeItemsProvider$1;

    invoke-direct {v0}, Lcom/twitter/library/scribe/EmptyScribeItemsProvider$1;-><init>()V

    sput-object v0, Lcom/twitter/library/scribe/EmptyScribeItemsProvider;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/library/scribe/EmptyScribeItemsProvider$1;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/twitter/library/scribe/EmptyScribeItemsProvider;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method
