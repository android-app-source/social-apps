.class public Lcom/twitter/library/scribe/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpj;


# static fields
.field private static final a:Lcom/twitter/util/collection/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/b",
            "<",
            "Lcom/twitter/analytics/model/ScribeLog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    new-instance v0, Lcom/twitter/util/collection/b;

    const/16 v1, 0x1f4

    invoke-direct {v0, v1}, Lcom/twitter/util/collection/b;-><init>(I)V

    sput-object v0, Lcom/twitter/library/scribe/h;->a:Lcom/twitter/util/collection/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/twitter/util/collection/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/b",
            "<",
            "Lcom/twitter/analytics/model/ScribeLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    sget-object v0, Lcom/twitter/library/scribe/h;->a:Lcom/twitter/util/collection/b;

    return-object v0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 29
    const-class v1, Lcom/twitter/library/scribe/h;

    monitor-enter v1

    .line 30
    :try_start_0
    sget-object v0, Lcom/twitter/library/scribe/h;->a:Lcom/twitter/util/collection/b;

    invoke-virtual {v0}, Lcom/twitter/util/collection/b;->clear()V

    .line 31
    monitor-exit v1

    .line 32
    return-void

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lcnz;Lcpk;)V
    .locals 2

    .prologue
    .line 17
    check-cast p2, Lcom/twitter/analytics/model/ScribeLog;

    .line 18
    const-class v1, Lcom/twitter/library/scribe/h;

    monitor-enter v1

    .line 19
    :try_start_0
    sget-object v0, Lcom/twitter/library/scribe/h;->a:Lcom/twitter/util/collection/b;

    invoke-virtual {v0, p2}, Lcom/twitter/util/collection/b;->add(Ljava/lang/Object;)Z

    .line 20
    monitor-exit v1

    .line 21
    return-void

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcpk;)Z
    .locals 1

    .prologue
    .line 36
    instance-of v0, p1, Lcom/twitter/analytics/model/ScribeLog;

    return v0
.end method
