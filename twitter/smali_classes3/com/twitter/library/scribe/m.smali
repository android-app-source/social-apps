.class public Lcom/twitter/library/scribe/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/scribe/m$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/scribe/m$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/scribe/m$a;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/m;->a:Landroid/content/Context;

    .line 19
    iput-object p2, p0, Lcom/twitter/library/scribe/m;->b:Lcom/twitter/library/scribe/m$a;

    .line 20
    return-void
.end method


# virtual methods
.method public a(Lcnz;Lcpk;)V
    .locals 4

    .prologue
    .line 24
    instance-of v0, p2, Lcom/twitter/analytics/model/ScribeLog;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Event must be a ScribeLog, is a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 26
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_0
    check-cast p2, Lcom/twitter/analytics/model/ScribeLog;

    .line 29
    invoke-virtual {p2}, Lcom/twitter/analytics/model/ScribeLog;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 30
    invoke-virtual {p2, p1}, Lcom/twitter/analytics/model/ScribeLog;->a(Lcnz;)Lcom/twitter/analytics/model/ScribeLog;

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/scribe/m;->b:Lcom/twitter/library/scribe/m$a;

    invoke-virtual {v0, p2}, Lcom/twitter/library/scribe/m$a;->a(Lcom/twitter/analytics/model/ScribeLog;)V

    .line 33
    invoke-virtual {p2}, Lcom/twitter/analytics/model/ScribeLog;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 34
    invoke-static {}, Lcom/twitter/library/scribe/j;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 35
    iget-object v0, p0, Lcom/twitter/library/scribe/m;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeService;->b(Landroid/content/Context;)V

    .line 40
    :cond_2
    :goto_0
    return-void

    .line 37
    :cond_3
    iget-object v0, p0, Lcom/twitter/library/scribe/m;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/scribe/d;->a(Landroid/content/Context;)Lcom/twitter/library/scribe/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/scribe/m;->a:Landroid/content/Context;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/scribe/d;->a(Landroid/content/Context;ZLjava/lang/String;)Z

    goto :goto_0
.end method

.method public a(Lcpk;)Z
    .locals 1

    .prologue
    .line 44
    instance-of v0, p1, Lcom/twitter/analytics/model/ScribeLog;

    return v0
.end method
