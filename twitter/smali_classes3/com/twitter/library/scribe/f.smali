.class public Lcom/twitter/library/scribe/f;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/library/scribe/f;


# instance fields
.field private b:Z

.field private c:J

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/content/Context;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-direct {p0}, Lcom/twitter/library/scribe/f;->f()V

    .line 41
    new-instance v0, Lcom/twitter/library/scribe/f$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/scribe/f$1;-><init>(Lcom/twitter/library/scribe/f;)V

    invoke-static {v0}, Lcoj;->a(Lcoj$a;)V

    .line 56
    return-void
.end method

.method public static declared-synchronized a()Lcom/twitter/library/scribe/f;
    .locals 2

    .prologue
    .line 27
    const-class v1, Lcom/twitter/library/scribe/f;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/scribe/f;->a:Lcom/twitter/library/scribe/f;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lcom/twitter/library/scribe/f;

    invoke-direct {v0}, Lcom/twitter/library/scribe/f;-><init>()V

    sput-object v0, Lcom/twitter/library/scribe/f;->a:Lcom/twitter/library/scribe/f;

    .line 29
    const-class v0, Lcom/twitter/library/scribe/f;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 31
    :cond_0
    sget-object v0, Lcom/twitter/library/scribe/f;->a:Lcom/twitter/library/scribe/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/twitter/library/scribe/f;)Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/twitter/library/scribe/f;->b:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/library/scribe/f;)J
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lcom/twitter/library/scribe/f;->c:J

    return-wide v0
.end method

.method static synthetic c(Lcom/twitter/library/scribe/f;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/library/scribe/f;->f()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/library/scribe/f;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/twitter/library/scribe/f;->e:Landroid/content/Context;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 63
    const-string/jumbo v0, "scribe_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/scribe/f;->b:Z

    .line 64
    const-wide/16 v0, 0x3e8

    const-string/jumbo v2, "scribe_interval_seconds"

    const/16 v3, 0x3c

    .line 65
    invoke-static {v2, v3}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/library/scribe/f;->c:J

    .line 66
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 67
    const-string/jumbo v0, "scribe_cdn_host_list"

    invoke-static {v0}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 68
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 69
    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 70
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    :cond_1
    iput-object v1, p0, Lcom/twitter/library/scribe/f;->d:Ljava/util/Set;

    .line 74
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/f;->e:Landroid/content/Context;

    .line 60
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/twitter/library/scribe/f;->b:Z

    return v0
.end method

.method public c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/library/scribe/f;->d:Ljava/util/Set;

    return-object v0
.end method

.method d()J
    .locals 6

    .prologue
    .line 85
    invoke-static {}, Lcom/twitter/library/scribe/j;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    iget-wide v0, p0, Lcom/twitter/library/scribe/f;->c:J

    .line 98
    :cond_0
    :goto_0
    return-wide v0

    .line 89
    :cond_1
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->b()Lcqs;

    move-result-object v0

    .line 90
    const-string/jumbo v1, "log_failure_cnt"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcqs;->a(Ljava/lang/String;I)I

    move-result v2

    .line 93
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/library/scribe/f;->e()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-gtz v3, :cond_0

    .line 96
    :cond_2
    iget-wide v0, p0, Lcom/twitter/library/scribe/f;->c:J

    shl-long/2addr v0, v2

    goto :goto_0
.end method

.method e()J
    .locals 4

    .prologue
    .line 104
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->b()Lcqs;

    move-result-object v0

    .line 105
    const-string/jumbo v1, "default"

    .line 106
    const-string/jumbo v1, "debug_scribe_flushing_frequency"

    const-string/jumbo v2, "default"

    invoke-interface {v0, v1, v2}, Lcqs;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 107
    const-string/jumbo v1, "default"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    const-wide/16 v0, 0x0

    .line 111
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    goto :goto_0
.end method
