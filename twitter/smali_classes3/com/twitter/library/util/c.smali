.class public Lcom/twitter/library/util/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/k;


# instance fields
.field private final a:Landroid/content/SyncResult;


# direct methods
.method public constructor <init>(Landroid/content/SyncResult;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/twitter/library/util/c;->a:Landroid/content/SyncResult;

    .line 19
    return-void
.end method

.method private a(Lcom/twitter/library/service/u;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 32
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    iget v0, v0, Lcom/twitter/network/l;->a:I

    .line 34
    const/16 v1, 0x191

    if-ne v0, v1, :cond_1

    .line 35
    iget-object v0, p0, Lcom/twitter/library/util/c;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 40
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    if-nez v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/twitter/library/util/c;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    goto :goto_0
.end method


# virtual methods
.method public a(Lbbw;Lcom/twitter/library/service/u;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p2}, Lcom/twitter/library/util/c;->a(Lcom/twitter/library/service/u;)V

    .line 24
    return-void
.end method

.method public a(Lbbz;Lcom/twitter/library/service/u;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p2}, Lcom/twitter/library/util/c;->a(Lcom/twitter/library/service/u;)V

    .line 29
    return-void
.end method
