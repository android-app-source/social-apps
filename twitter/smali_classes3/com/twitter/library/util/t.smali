.class public abstract Lcom/twitter/library/util/t;
.super Lcom/twitter/library/util/s;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/ui/widget/TwitterButton;


# direct methods
.method public constructor <init>(Lcom/twitter/ui/widget/TwitterButton;)V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/util/t;-><init>(Lcom/twitter/ui/widget/TwitterButton;Z)V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/twitter/ui/widget/TwitterButton;Z)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p2}, Lcom/twitter/library/util/s;-><init>(Z)V

    .line 24
    iput-object p1, p0, Lcom/twitter/library/util/t;->a:Lcom/twitter/ui/widget/TwitterButton;

    .line 25
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/twitter/library/util/t;->a:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/ui/widget/TwitterButton;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 30
    invoke-super {p0, p1, p2}, Lcom/twitter/library/util/s;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
