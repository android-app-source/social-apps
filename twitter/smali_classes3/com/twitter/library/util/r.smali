.class public Lcom/twitter/library/util/r;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/library/util/r;


# instance fields
.field private final b:Landroid/media/AudioManager;

.field private c:Landroid/media/SoundPool;

.field private d:[I

.field private e:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-string/jumbo v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/twitter/library/util/r;->b:Landroid/media/AudioManager;

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/util/r;->e:I

    .line 39
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/util/r;
    .locals 4

    .prologue
    .line 47
    const-class v1, Lcom/twitter/library/util/r;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 48
    sget-object v2, Lcom/twitter/library/util/r;->a:Lcom/twitter/library/util/r;

    if-nez v2, :cond_0

    .line 49
    new-instance v2, Lcom/twitter/library/util/r;

    invoke-direct {v2, v0}, Lcom/twitter/library/util/r;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/twitter/library/util/r;->a:Lcom/twitter/library/util/r;

    .line 50
    const-class v2, Lcom/twitter/library/util/r;

    invoke-static {v2}, Lcru;->a(Ljava/lang/Class;)V

    .line 52
    :cond_0
    sget-object v2, Lcom/twitter/library/util/r;->a:Lcom/twitter/library/util/r;

    .line 53
    iget v3, v2, Lcom/twitter/library/util/r;->e:I

    if-nez v3, :cond_1

    .line 54
    invoke-direct {v2, v0}, Lcom/twitter/library/util/r;->b(Landroid/content/Context;)V

    .line 56
    :cond_1
    iget v0, v2, Lcom/twitter/library/util/r;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/twitter/library/util/r;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    monitor-exit v1

    return-object v2

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Landroid/media/SoundPool;)V
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/twitter/library/util/r$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/util/r$1;-><init>(Landroid/media/SoundPool;)V

    invoke-static {v0}, Lcre;->a(Lrx/functions/a;)Lrx/a;

    .line 125
    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 67
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x5

    invoke-direct {v0, v5, v1, v3}, Landroid/media/SoundPool;-><init>(III)V

    .line 68
    const/4 v1, 0x4

    new-array v1, v1, [I

    sget v2, Lazw$j;->psst1:I

    .line 69
    invoke-virtual {v0, p1, v2, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v1, v3

    sget v2, Lazw$j;->psst2:I

    .line 70
    invoke-virtual {v0, p1, v2, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v1, v4

    sget v2, Lazw$j;->pop:I

    .line 71
    invoke-virtual {v0, p1, v2, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v1, v5

    const/4 v2, 0x3

    sget v3, Lazw$j;->tick:I

    .line 72
    invoke-virtual {v0, p1, v3, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v3

    aput v3, v1, v2

    iput-object v1, p0, Lcom/twitter/library/util/r;->d:[I

    .line 74
    iput-object v0, p0, Lcom/twitter/library/util/r;->c:Landroid/media/SoundPool;

    .line 75
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 105
    iget-object v0, p0, Lcom/twitter/library/util/r;->c:Landroid/media/SoundPool;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/util/r;->b:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/util/r;->b:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/util/r;->c:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/twitter/library/util/r;->d:[I

    aget v1, v1, p1

    move v3, v2

    move v5, v4

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    move-result v4

    .line 108
    :cond_1
    return v4
.end method

.method public a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 82
    iget v0, p0, Lcom/twitter/library/util/r;->e:I

    if-lez v0, :cond_0

    .line 83
    iget v0, p0, Lcom/twitter/library/util/r;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/twitter/library/util/r;->e:I

    .line 85
    :cond_0
    iget v0, p0, Lcom/twitter/library/util/r;->e:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/util/r;->c:Landroid/media/SoundPool;

    if-eqz v0, :cond_2

    .line 86
    iget-object v1, p0, Lcom/twitter/library/util/r;->d:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 87
    iget-object v4, p0, Lcom/twitter/library/util/r;->c:Landroid/media/SoundPool;

    invoke-virtual {v4, v3}, Landroid/media/SoundPool;->unload(I)Z

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/util/r;->c:Landroid/media/SoundPool;

    invoke-static {v0}, Lcom/twitter/library/util/r;->a(Landroid/media/SoundPool;)V

    .line 94
    iput-object v5, p0, Lcom/twitter/library/util/r;->c:Landroid/media/SoundPool;

    .line 95
    iput-object v5, p0, Lcom/twitter/library/util/r;->d:[I

    .line 97
    :cond_2
    return-void
.end method
