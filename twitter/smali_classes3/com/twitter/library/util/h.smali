.class public Lcom/twitter/library/util/h;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/library/util/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/library/util/h;->a:Lcom/twitter/library/util/g;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/library/util/g;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 41
    sget-object v0, Lcom/twitter/library/util/h;->a:Lcom/twitter/library/util/g;

    if-nez v0, :cond_1

    .line 42
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/android/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    new-instance v0, Lcom/twitter/library/util/i;

    invoke-direct {v0, p0}, Lcom/twitter/library/util/i;-><init>(Landroid/content/Context;)V

    .line 48
    :goto_0
    return-object v0

    .line 45
    :cond_0
    new-instance v0, Lcom/twitter/library/util/j;

    invoke-direct {v0, p0}, Lcom/twitter/library/util/j;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 48
    :cond_1
    sget-object v0, Lcom/twitter/library/util/h;->a:Lcom/twitter/library/util/g;

    goto :goto_0
.end method
