.class public Lcom/twitter/library/util/b$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lakn$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/util/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lakm;)V
    .locals 4

    .prologue
    .line 451
    invoke-static {p1}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 452
    if-eqz v0, :cond_0

    .line 453
    new-instance v1, Lcnz;

    iget-wide v2, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct {v1, v2, v3}, Lcnz;-><init>(J)V

    .line 454
    invoke-virtual {v1}, Lcnz;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    invoke-virtual {p1, v1}, Lakm;->a(Lcnz;)V

    .line 458
    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 431
    const/4 v0, 0x2

    return v0
.end method

.method public a(Lakm;II)V
    .locals 1

    .prologue
    .line 436
    .line 437
    if-ge p2, p3, :cond_0

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 438
    invoke-direct {p0, p1}, Lcom/twitter/library/util/b$a;->a(Lakm;)V

    .line 439
    add-int/lit8 p2, p2, 0x1

    .line 446
    :cond_0
    invoke-virtual {p1, p2}, Lakm;->a(I)V

    .line 447
    return-void
.end method
