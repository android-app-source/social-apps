.class public Lcom/twitter/library/util/j;
.super Lcom/twitter/library/util/a;
.source "Twttr"


# static fields
.field private static b:Z

.field private static c:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/twitter/library/util/j;->c:J

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/twitter/library/util/a;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method


# virtual methods
.method public bridge synthetic a()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 13
    invoke-super {p0}, Lcom/twitter/library/util/a;->a()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/util/Map;)Lbmo;
    .locals 1

    .prologue
    .line 13
    invoke-super {p0, p1}, Lcom/twitter/library/util/a;->a(Ljava/util/Map;)Lbmo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Landroid/database/Cursor;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 13
    invoke-super {p0, p1}, Lcom/twitter/library/util/a;->a(Landroid/database/Cursor;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/util/Map;Lcom/twitter/library/util/k;)V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0, p1, p2}, Lcom/twitter/library/util/a;->a(Ljava/util/Map;Lcom/twitter/library/util/k;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/util/Map;Lcom/twitter/library/util/k;Z)V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/util/a;->a(Ljava/util/Map;Lcom/twitter/library/util/k;Z)V

    return-void
.end method

.method public bridge synthetic a(Ljava/util/Set;Lcom/twitter/library/util/k;)V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0, p1, p2}, Lcom/twitter/library/util/a;->a(Ljava/util/Set;Lcom/twitter/library/util/k;)V

    return-void
.end method

.method public b()Z
    .locals 5

    .prologue
    .line 41
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 43
    sget-wide v2, Lcom/twitter/library/util/j;->c:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 44
    sget-boolean v0, Lcom/twitter/library/util/j;->b:Z

    .line 51
    :goto_0
    return v0

    .line 46
    :cond_0
    new-instance v2, Lcom/twitter/util/a;

    iget-object v3, p0, Lcom/twitter/library/util/j;->a:Landroid/content/Context;

    const-string/jumbo v4, "ContactLoaderHelper"

    invoke-direct {v2, v3, v0, v1, v4}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    .line 47
    const-string/jumbo v3, "contacts_uploaded"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/twitter/util/a;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Lcom/twitter/library/util/j;->b:Z

    .line 48
    sput-wide v0, Lcom/twitter/library/util/j;->c:J

    .line 49
    const-class v0, Lcom/twitter/library/util/j;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 51
    sget-boolean v0, Lcom/twitter/library/util/j;->b:Z

    goto :goto_0
.end method

.method public c()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 56
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 58
    new-instance v2, Lcom/twitter/util/a;

    iget-object v3, p0, Lcom/twitter/library/util/j;->a:Landroid/content/Context;

    const-string/jumbo v4, "ContactLoaderHelper"

    invoke-direct {v2, v3, v0, v1, v4}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    .line 59
    invoke-virtual {v2}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    const-string/jumbo v3, "contacts_uploaded"

    invoke-virtual {v2, v3, v5}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/util/a$a;->commit()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    sput-boolean v5, Lcom/twitter/library/util/j;->b:Z

    .line 61
    sput-wide v0, Lcom/twitter/library/util/j;->c:J

    .line 62
    const-class v0, Lcom/twitter/library/util/j;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 64
    :cond_0
    return-void
.end method
