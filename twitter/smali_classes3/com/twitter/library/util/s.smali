.class public abstract Lcom/twitter/library/util/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:Z

.field private b:F

.field private c:F

.field private d:Z

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/library/util/s;-><init>(Z)V

    .line 27
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-boolean p1, p0, Lcom/twitter/library/util/s;->a:Z

    .line 35
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/twitter/library/util/s;->e:Landroid/view/View;

    .line 89
    return-void
.end method

.method public abstract a(Landroid/view/View;Landroid/view/MotionEvent;)V
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/twitter/library/util/s;->a:Z

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 43
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 74
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/library/util/s;->d:Z

    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 75
    iget-object v0, p0, Lcom/twitter/library/util/s;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/twitter/library/util/s;->e:Landroid/view/View;

    iget-boolean v1, p0, Lcom/twitter/library/util/s;->d:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 78
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/library/util/s;->a:Z

    return v0

    .line 45
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/util/s;->b:F

    .line 46
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/util/s;->c:F

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/util/s;->d:Z

    goto :goto_0

    .line 51
    :pswitch_1
    iget-boolean v0, p0, Lcom/twitter/library/util/s;->d:Z

    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/util/s;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 53
    iput-boolean v3, p0, Lcom/twitter/library/util/s;->d:Z

    goto :goto_0

    .line 58
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lcom/twitter/library/util/s;->b:F

    sub-float/2addr v0, v1

    .line 59
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lcom/twitter/library/util/s;->c:F

    sub-float/2addr v1, v2

    .line 60
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    .line 61
    invoke-static {}, Lcom/twitter/util/z;->f()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 62
    iput-boolean v3, p0, Lcom/twitter/library/util/s;->d:Z

    goto :goto_0

    .line 67
    :pswitch_3
    iput-boolean v3, p0, Lcom/twitter/library/util/s;->d:Z

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
