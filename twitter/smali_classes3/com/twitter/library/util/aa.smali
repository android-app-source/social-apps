.class public Lcom/twitter/library/util/aa;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 32
    const-string/jumbo v0, "br"

    const/16 v1, 0x2e

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "in"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "ar"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "bo"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "az"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string/jumbo v3, "bt"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "bq"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "cw"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "fj"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "ge"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "ht"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "iq"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "im"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "it"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string/jumbo v3, "jm"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string/jumbo v3, "kz"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string/jumbo v3, "ke"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string/jumbo v3, "lr"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string/jumbo v3, "my"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string/jumbo v3, "mv"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string/jumbo v3, "mx"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string/jumbo v3, "me"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string/jumbo v3, "nr"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string/jumbo v3, "nz"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string/jumbo v3, "ng"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string/jumbo v3, "om"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string/jumbo v3, "pk"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-string/jumbo v3, "ps"

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string/jumbo v3, "pa"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string/jumbo v3, "pg"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-string/jumbo v3, "py"

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string/jumbo v3, "ph"

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const-string/jumbo v3, "qa"

    aput-object v3, v1, v2

    const/16 v2, 0x21

    const-string/jumbo v3, "rw"

    aput-object v3, v1, v2

    const/16 v2, 0x22

    const-string/jumbo v3, "sa"

    aput-object v3, v1, v2

    const/16 v2, 0x23

    const-string/jumbo v3, "rs"

    aput-object v3, v1, v2

    const/16 v2, 0x24

    const-string/jumbo v3, "sc"

    aput-object v3, v1, v2

    const/16 v2, 0x25

    const-string/jumbo v3, "lk"

    aput-object v3, v1, v2

    const/16 v2, 0x26

    const-string/jumbo v3, "to"

    aput-object v3, v1, v2

    const/16 v2, 0x27

    const-string/jumbo v3, "tn"

    aput-object v3, v1, v2

    const/16 v2, 0x28

    const-string/jumbo v3, "tr"

    aput-object v3, v1, v2

    const/16 v2, 0x29

    const-string/jumbo v3, "ae"

    aput-object v3, v1, v2

    const/16 v2, 0x2a

    const-string/jumbo v3, "gb"

    aput-object v3, v1, v2

    const/16 v2, 0x2b

    const-string/jumbo v3, "uy"

    aput-object v3, v1, v2

    const/16 v2, 0x2c

    const-string/jumbo v3, "uz"

    aput-object v3, v1, v2

    const/16 v2, 0x2d

    const-string/jumbo v3, "ye"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/util/aa;->a:Ljava/util/List;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/util/aa;->b:Landroid/content/Context;

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/util/aa;->b:Landroid/content/Context;

    .line 98
    return-void
.end method

.method public static a()Lcom/twitter/library/util/aa;
    .locals 1

    .prologue
    .line 87
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->ad()Lcom/twitter/library/util/aa;

    move-result-object v0

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/twitter/library/util/aa;->l()Ljava/lang/String;

    move-result-object v0

    .line 158
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/util/aa;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private l()Ljava/lang/String;
    .locals 3

    .prologue
    .line 167
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/twitter/library/util/aa;->b:Landroid/content/Context;

    const-string/jumbo v1, "SIM_OVERRIDE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 169
    const-string/jumbo v1, "CountryKey"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 177
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/library/util/aa;->j()Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 178
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    .line 179
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 180
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 191
    :goto_0
    return-object v0

    .line 181
    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    if-eq v1, v3, :cond_1

    .line 183
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 186
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    .line 191
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/twitter/library/util/aa;->d:Z

    if-nez v0, :cond_0

    .line 103
    invoke-direct {p0}, Lcom/twitter/library/util/aa;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/util/aa;->c:Ljava/lang/String;

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/util/aa;->d:Z

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/util/aa;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/twitter/library/util/aa;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/util/aa;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/twitter/library/util/aa;->j()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    return v0
.end method

.method public e()I
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v0, 0x0

    .line 120
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/library/util/aa;->d()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/twitter/library/util/aa;->j()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 127
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v3, :cond_0

    .line 128
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 134
    :cond_0
    :goto_0
    return v0

    .line 131
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/twitter/library/util/aa;->e()I

    move-result v0

    .line 139
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/util/aa;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x1b8

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 143
    const-string/jumbo v0, "sms_notifications_opt_in_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/util/aa;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 147
    sget-object v0, Lcom/twitter/library/util/aa;->a:Ljava/util/List;

    invoke-virtual {p0}, Lcom/twitter/library/util/aa;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/twitter/library/util/aa;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method j()Landroid/telephony/TelephonyManager;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/twitter/library/util/aa;->b:Landroid/content/Context;

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    return-object v0
.end method
