.class public Lcom/twitter/library/util/ae;
.super Lcre;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/library/service/s;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/twitter/library/service/s;",
            ">(TR;)",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<TR;",
            "Lcom/twitter/library/service/u;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Lcom/twitter/library/util/ae$2;

    invoke-direct {v0, p0}, Lcom/twitter/library/util/ae$2;-><init>(Lcom/twitter/library/service/s;)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcta;Landroid/support/v4/app/FragmentManager;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/dialog/BaseDialogFragment;",
            ">;",
            "Landroid/support/v4/app/FragmentManager;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lcom/twitter/library/util/ae$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/util/ae$1;-><init>(Lcta;Landroid/support/v4/app/FragmentManager;)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/twitter/library/service/s;)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/twitter/library/service/s;",
            ">(TR;)",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<TR;",
            "Lcom/twitter/library/service/u;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 99
    invoke-static {p0}, Lcom/twitter/library/util/ae;->a(Lcom/twitter/library/service/s;)Lrx/g;

    move-result-object v0

    .line 100
    invoke-static {}, Lcom/twitter/async/service/c;->a()Lcom/twitter/async/service/c;

    move-result-object v1

    sget-object v2, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->a:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    .line 101
    invoke-virtual {v1, v2}, Lcom/twitter/async/service/c;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 100
    invoke-static {v1}, Lcws;->a(Ljava/util/concurrent/Executor;)Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->b(Lrx/f;)Lrx/g;

    move-result-object v0

    .line 99
    return-object v0
.end method
