.class public Lcom/twitter/library/util/m;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/evernote/android/job/d;


# direct methods
.method public constructor <init>(Lcom/evernote/android/job/d;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/twitter/library/util/m;->a:Lcom/evernote/android/job/d;

    .line 27
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 32
    iget-object v0, p0, Lcom/twitter/library/util/m;->a:Lcom/evernote/android/job/d;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/d;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 37
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 38
    iget-object v0, p0, Lcom/twitter/library/util/m;->a:Lcom/evernote/android/job/d;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/d;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 39
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/android/job/Job;

    .line 40
    invoke-virtual {v0}, Lcom/evernote/android/job/Job;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    const/4 v0, 0x1

    .line 44
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 50
    iget-object v0, p0, Lcom/twitter/library/util/m;->a:Lcom/evernote/android/job/d;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/d;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
