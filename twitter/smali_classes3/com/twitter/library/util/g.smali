.class public interface abstract Lcom/twitter/library/util/g;
.super Ljava/lang/Object;
.source "Twttr"


# virtual methods
.method public abstract a()Landroid/database/Cursor;
.end method

.method public abstract a(Ljava/util/Map;)Lbmo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/nio/ByteBuffer;",
            ">;)",
            "Lbmo;"
        }
    .end annotation
.end method

.method public abstract a(Landroid/database/Cursor;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/Map;Lcom/twitter/library/util/k;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/nio/ByteBuffer;",
            ">;",
            "Lcom/twitter/library/util/k;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/Map;Lcom/twitter/library/util/k;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/nio/ByteBuffer;",
            ">;",
            "Lcom/twitter/library/util/k;",
            "Z)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/Set;Lcom/twitter/library/util/k;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/twitter/library/util/k;",
            ")V"
        }
    .end annotation
.end method

.method public abstract b()Z
.end method

.method public abstract c()V
.end method
