.class public Lcom/twitter/library/util/q;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/u;


# static fields
.field private static a:Lcom/twitter/library/util/q;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Landroid/content/SharedPreferences;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/util/q;->b:Landroid/content/Context;

    .line 27
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/util/q;
    .locals 2

    .prologue
    .line 33
    const-class v1, Lcom/twitter/library/util/q;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/util/q;->a:Lcom/twitter/library/util/q;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/twitter/library/util/q;

    invoke-direct {v0, p0}, Lcom/twitter/library/util/q;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/util/q;->a:Lcom/twitter/library/util/q;

    .line 35
    sget-object v0, Lcom/twitter/library/util/q;->a:Lcom/twitter/library/util/q;

    invoke-direct {v0}, Lcom/twitter/library/util/q;->e()V

    .line 36
    const-class v0, Lcom/twitter/library/util/q;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 38
    :cond_0
    sget-object v0, Lcom/twitter/library/util/q;->a:Lcom/twitter/library/util/q;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private d()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/library/util/q;->c:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/twitter/library/util/q;->b:Landroid/content/Context;

    const-string/jumbo v1, "logged_out_settings"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/util/q;->c:Landroid/content/SharedPreferences;

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/util/q;->c:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 57
    invoke-direct {p0}, Lcom/twitter/library/util/q;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "email_disco"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/util/q;->a(Z)V

    .line 58
    invoke-direct {p0}, Lcom/twitter/library/util/q;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "phone_disco"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/util/q;->b(Z)V

    .line 59
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    .prologue
    .line 66
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/util/q;->d:Ljava/lang/Boolean;

    .line 67
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/library/util/q;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 74
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/util/q;->e:Ljava/lang/Boolean;

    .line 75
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/library/util/q;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/twitter/library/util/q;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "email_disco"

    iget-object v2, p0, Lcom/twitter/library/util/q;->d:Ljava/lang/Boolean;

    .line 93
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "phone_disco"

    iget-object v2, p0, Lcom/twitter/library/util/q;->e:Ljava/lang/Boolean;

    .line 94
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 95
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 96
    return-void
.end method
