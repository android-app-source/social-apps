.class public Lcom/twitter/library/util/w;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;ZLcom/twitter/model/profile/TranslatorType;ZIIII)V
    .locals 6

    .prologue
    const/4 v0, -0x2

    const/4 v5, 0x0

    .line 23
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 24
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 25
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 28
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 30
    invoke-static {p0}, Lcom/twitter/util/b;->a(Landroid/content/Context;)Z

    move-result v0

    .line 31
    sget v4, Lazw$e;->icon_spacing:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 32
    if-eqz v0, :cond_6

    .line 33
    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 38
    :goto_0
    if-eqz p2, :cond_1

    .line 39
    sget v0, Lazw$h;->profile_icon_verified:I

    invoke-virtual {v1, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 40
    if-eqz p6, :cond_0

    .line 41
    invoke-virtual {v2, p6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 43
    :cond_0
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 44
    new-instance v4, Lcom/twitter/library/util/w$1;

    invoke-direct {v4, p0}, Lcom/twitter/library/util/w$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 52
    :cond_1
    if-eqz p4, :cond_3

    .line 53
    sget v0, Lazw$h;->profile_icon_protected:I

    invoke-virtual {v1, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 54
    if-eqz p5, :cond_2

    .line 55
    invoke-virtual {v2, p5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 57
    :cond_2
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 61
    :cond_3
    sget-object v0, Lcom/twitter/model/profile/TranslatorType;->c:Lcom/twitter/model/profile/TranslatorType;

    if-ne p3, v0, :cond_7

    .line 62
    sget v0, Lazw$h;->profile_icon_translator:I

    .line 63
    invoke-virtual {v1, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 64
    if-eqz p7, :cond_4

    .line 65
    invoke-virtual {v2, p7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 67
    :cond_4
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 78
    :cond_5
    :goto_1
    return-void

    .line 35
    :cond_6
    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_0

    .line 69
    :cond_7
    sget-object v0, Lcom/twitter/model/profile/TranslatorType;->d:Lcom/twitter/model/profile/TranslatorType;

    if-ne p3, v0, :cond_5

    .line 70
    sget v0, Lazw$h;->profile_icon_translator_moderator:I

    invoke-virtual {v1, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 72
    if-eqz p8, :cond_8

    .line 73
    invoke-virtual {v2, p8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 75
    :cond_8
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1
.end method
