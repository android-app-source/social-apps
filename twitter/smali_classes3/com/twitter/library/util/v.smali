.class public Lcom/twitter/library/util/v;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/library/util/v;


# instance fields
.field private final b:Landroid/content/SharedPreferences;

.field private c:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/util/v;->b:Landroid/content/SharedPreferences;

    .line 36
    iget-object v0, p0, Lcom/twitter/library/util/v;->b:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "phone_verified"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/util/v;->c:Z

    .line 37
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/library/util/v;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/twitter/library/util/v;->a:Lcom/twitter/library/util/v;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/twitter/library/util/v;

    invoke-direct {v0, p0}, Lcom/twitter/library/util/v;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/util/v;->a:Lcom/twitter/library/util/v;

    .line 42
    const-class v0, Lcom/twitter/library/util/v;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 44
    :cond_0
    sget-object v0, Lcom/twitter/library/util/v;->a:Lcom/twitter/library/util/v;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/library/util/v;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "phone_verified"

    .line 89
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "last_phone_verified_request"

    .line 90
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/util/v;->c:Z

    .line 92
    return-void
.end method

.method public a(ZZ)V
    .locals 2

    .prologue
    .line 67
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/twitter/library/util/v;->a(ZZJ)V

    .line 68
    return-void
.end method

.method public a(ZZJ)V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/library/util/v;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "phone_verified"

    .line 80
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "last_phone_verified_request"

    .line 81
    invoke-interface {v0, v1, p3, p4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 82
    if-eqz p2, :cond_0

    .line 83
    iput-boolean p1, p0, Lcom/twitter/library/util/v;->c:Z

    .line 85
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/twitter/library/util/v;->c:Z

    return v0
.end method

.method public c()Z
    .locals 4

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/library/util/v;->b:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "last_phone_verified_request"

    const-wide/16 v2, 0x0

    .line 100
    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 101
    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
