.class public Lcom/twitter/library/revenue/b;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(I)I
    .locals 1

    .prologue
    .line 35
    const-string/jumbo v0, "ad_formats_realtime_ads_cache_size"

    invoke-static {v0, p0}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static a(J)J
    .locals 4

    .prologue
    .line 39
    const-string/jumbo v0, "ad_formats_realtime_ads_cache_ttl"

    invoke-static {v0, p0, p1}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 18
    const-string/jumbo v0, "ad_formats_video_value_reporting_logging_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ad_formats_suppress_promoter_for_dynamic_ads_enabled"

    .line 27
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 26
    :goto_0
    return v0

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 22
    const-string/jumbo v0, "ad_formats_video_type_engagement_metadata_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 31
    const-string/jumbo v0, "ad_formats_realtime_ads_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
