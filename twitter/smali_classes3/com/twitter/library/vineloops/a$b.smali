.class Lcom/twitter/library/vineloops/a$b;
.super Lcom/twitter/async/service/AsyncOperation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/vineloops/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Ljava/lang/Void;",
        "Lcom/twitter/library/service/u;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/vineloops/a;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/twitter/library/vineloops/a;Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 118
    iput-object p1, p0, Lcom/twitter/library/vineloops/a$b;->a:Lcom/twitter/library/vineloops/a;

    .line 119
    const-class v0, Lcom/twitter/library/vineloops/a$b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/vineloops/a$b;->b:Landroid/content/Context;

    .line 121
    iget-object v0, p0, Lcom/twitter/library/vineloops/a$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v0

    .line 122
    iget-object v1, v0, Lcom/twitter/library/network/ab;->c:Lcom/twitter/library/network/ae;

    invoke-virtual {v1}, Lcom/twitter/library/network/ae;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/vineloops/a$b;->c:Ljava/lang/String;

    .line 123
    const-string/jumbo v1, "%s/%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "tw_android"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v0, v0, Lcom/twitter/library/network/ab;->e:Ljava/lang/String;

    aput-object v0, v2, v3

    .line 124
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/vineloops/a$b;->g:Ljava/lang/String;

    .line 125
    new-instance v0, Lcom/twitter/library/service/h;

    const/4 v1, 0x5

    const-wide/16 v2, 0x2710

    const-wide/32 v4, 0x927c0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v7, Lcom/twitter/library/vineloops/a;->a:Ljava/util/Collection;

    sget-object v8, Lcom/twitter/library/vineloops/a;->b:Ljava/util/Collection;

    invoke-direct/range {v0 .. v8}, Lcom/twitter/library/service/h;-><init>(IJJLjava/util/concurrent/TimeUnit;Ljava/util/Collection;Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/vineloops/a$b;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 128
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/u;
    .locals 8

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/library/vineloops/a$b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/vineloops/VineLoopAggregator;->a(Landroid/content/Context;)Lcom/twitter/library/vineloops/VineLoopAggregator;

    move-result-object v1

    .line 133
    invoke-virtual {v1}, Lcom/twitter/library/vineloops/VineLoopAggregator;->b()Ljava/util/List;

    move-result-object v2

    .line 134
    new-instance v0, Lcom/twitter/library/service/u;

    invoke-direct {v0}, Lcom/twitter/library/service/u;-><init>()V

    .line 137
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/twitter/library/service/u;->a(Z)V

    .line 139
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 180
    :goto_0
    return-object v0

    .line 145
    :cond_0
    :try_start_0
    invoke-static {v2}, Lcom/twitter/library/vineloops/a;->a(Ljava/util/List;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 152
    new-instance v4, Lcom/twitter/library/vineloops/b;

    invoke-direct {v4}, Lcom/twitter/library/vineloops/b;-><init>()V

    .line 154
    :try_start_1
    invoke-virtual {p0, v3, v4}, Lcom/twitter/library/vineloops/a$b;->a(Lorg/json/JSONObject;Lcom/twitter/library/vineloops/b;)Lcom/twitter/network/HttpOperation;

    move-result-object v5

    .line 155
    const-string/jumbo v6, "User-Agent"

    iget-object v7, p0, Lcom/twitter/library/vineloops/a$b;->c:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;

    .line 156
    const-string/jumbo v6, "X-Vine-Client"

    iget-object v7, p0, Lcom/twitter/library/vineloops/a$b;->g:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 162
    invoke-virtual {v5}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    .line 164
    invoke-virtual {v5}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v6

    .line 165
    invoke-virtual {v5}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v5

    if-nez v5, :cond_2

    .line 166
    iget-boolean v4, v6, Lcom/twitter/network/l;->d:Z

    if-nez v4, :cond_1

    iget v4, v6, Lcom/twitter/network/l;->a:I

    if-eqz v4, :cond_1

    .line 168
    new-instance v4, Lcpb;

    invoke-direct {v4}, Lcpb;-><init>()V

    const-string/jumbo v5, "statusCode"

    iget v7, v6, Lcom/twitter/network/l;->a:I

    .line 169
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v4

    const-string/jumbo v5, "json"

    .line 170
    invoke-virtual {v4, v5, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    .line 171
    invoke-virtual {v3, v4}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v3

    .line 168
    invoke-static {v3}, Lcpd;->c(Lcpb;)V

    .line 173
    :cond_1
    invoke-virtual {v1, v2}, Lcom/twitter/library/vineloops/VineLoopAggregator;->a(Ljava/util/List;)V

    .line 174
    iget-object v1, p0, Lcom/twitter/library/vineloops/a$b;->a:Lcom/twitter/library/vineloops/a;

    const/16 v2, 0x2710

    invoke-virtual {v1, v2}, Lcom/twitter/library/vineloops/a;->a(I)V

    .line 175
    iget v1, v6, Lcom/twitter/network/l;->a:I

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/u;->a(I)V

    goto :goto_0

    .line 146
    :catch_0
    move-exception v1

    .line 147
    new-instance v3, Lcpb;

    invoke-direct {v3}, Lcpb;-><init>()V

    const-string/jumbo v4, "records"

    invoke-virtual {v3, v4, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v1

    invoke-static {v1}, Lcpd;->c(Lcpb;)V

    goto :goto_0

    .line 157
    :catch_1
    move-exception v1

    .line 158
    new-instance v2, Lcpb;

    invoke-direct {v2}, Lcpb;-><init>()V

    const-string/jumbo v4, "json"

    invoke-virtual {v2, v4, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v1

    invoke-static {v1}, Lcpd;->c(Lcpb;)V

    goto/16 :goto_0

    .line 179
    :cond_2
    iget-object v1, p0, Lcom/twitter/library/vineloops/a$b;->a:Lcom/twitter/library/vineloops/a;

    invoke-virtual {v4}, Lcom/twitter/library/vineloops/b;->d()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/vineloops/a;->a(I)V

    goto/16 :goto_0
.end method

.method a(Lorg/json/JSONObject;Lcom/twitter/library/vineloops/b;)Lcom/twitter/network/HttpOperation;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 193
    new-instance v0, Lcom/twitter/network/apache/entity/c;

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/network/apache/a;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2}, Lcom/twitter/network/apache/entity/c;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 194
    const-string/jumbo v1, "application/json"

    invoke-virtual {v0, v1}, Lcom/twitter/network/apache/entity/c;->a(Ljava/lang/String;)V

    .line 196
    new-instance v1, Lcom/twitter/library/network/k;

    iget-object v2, p0, Lcom/twitter/library/vineloops/a$b;->b:Landroid/content/Context;

    const-string/jumbo v3, "https://api.vineapp.com/loops"

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/network/k;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v2, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 197
    invoke-virtual {v1, v2}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/k;

    move-result-object v1

    .line 198
    invoke-virtual {v1, v0}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/apache/e;)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 199
    invoke-virtual {v0, p2}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;

    move-result-object v0

    const/4 v1, 0x0

    .line 200
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->c(Z)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 196
    return-object v0
.end method

.method protected b()Lcom/twitter/library/service/u;
    .locals 2

    .prologue
    .line 185
    new-instance v0, Lcom/twitter/library/service/u;

    invoke-direct {v0}, Lcom/twitter/library/service/u;-><init>()V

    .line 186
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/u;->a(Z)V

    .line 187
    return-object v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/twitter/library/vineloops/a$b;->b()Lcom/twitter/library/service/u;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/twitter/library/vineloops/a$b;->a()Lcom/twitter/library/service/u;

    move-result-object v0

    return-object v0
.end method
