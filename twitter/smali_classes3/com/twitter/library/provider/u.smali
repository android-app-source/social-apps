.class public Lcom/twitter/library/provider/u;
.super Laus;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/provider/u$a;
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/twitter/database/model/j;Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Laus;-><init>(Lcom/twitter/database/model/j;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 72
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 76
    const/16 v0, 0x2a

    return v0
.end method

.method protected b()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Laus$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v8, 0xb

    const/16 v7, 0xa

    const/4 v6, 0x6

    const/4 v5, 0x4

    const/4 v4, 0x3

    .line 82
    sget-object v0, Lcom/twitter/library/provider/u;->a:Laus$a;

    const/16 v1, 0x28

    new-array v1, v1, [Laus$a;

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/library/provider/u$1;

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$1;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/twitter/library/provider/u$12;

    invoke-direct {v3, p0, v5}, Lcom/twitter/library/provider/u$12;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/twitter/library/provider/u;->a:Laus$a;

    aput-object v3, v1, v2

    new-instance v2, Lcom/twitter/library/provider/u$23;

    invoke-direct {v2, p0, v6}, Lcom/twitter/library/provider/u$23;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v2, v1, v4

    sget-object v2, Lcom/twitter/library/provider/u;->a:Laus$a;

    aput-object v2, v1, v5

    const/4 v2, 0x5

    sget-object v3, Lcom/twitter/library/provider/u;->a:Laus$a;

    aput-object v3, v1, v2

    sget-object v2, Lcom/twitter/library/provider/u;->a:Laus$a;

    aput-object v2, v1, v6

    const/4 v2, 0x7

    new-instance v3, Lcom/twitter/library/provider/u$24;

    invoke-direct {v3, p0, v7}, Lcom/twitter/library/provider/u$24;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-instance v3, Lcom/twitter/library/provider/u$25;

    invoke-direct {v3, p0, v8}, Lcom/twitter/library/provider/u$25;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-instance v3, Lcom/twitter/library/provider/u$26;

    const/16 v4, 0xc

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$26;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    new-instance v2, Lcom/twitter/library/provider/u$27;

    const/16 v3, 0xd

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/provider/u$27;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/twitter/library/provider/u$28;

    const/16 v3, 0xe

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/provider/u$28;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v2, v1, v8

    const/16 v2, 0xc

    sget-object v3, Lcom/twitter/library/provider/u;->a:Laus$a;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    new-instance v3, Lcom/twitter/library/provider/u$29;

    const/16 v4, 0x10

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$29;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0xe

    sget-object v3, Lcom/twitter/library/provider/u;->a:Laus$a;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    new-instance v3, Lcom/twitter/library/provider/u$2;

    const/16 v4, 0x12

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$2;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x10

    new-instance v3, Lcom/twitter/library/provider/u$3;

    const/16 v4, 0x13

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$3;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x11

    new-instance v3, Lcom/twitter/library/provider/u$4;

    const/16 v4, 0x14

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$4;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x12

    sget-object v3, Lcom/twitter/library/provider/u;->a:Laus$a;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    new-instance v3, Lcom/twitter/library/provider/u$5;

    const/16 v4, 0x16

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$5;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x14

    new-instance v3, Lcom/twitter/library/provider/u$6;

    const/16 v4, 0x17

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$6;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x15

    new-instance v3, Lcom/twitter/library/provider/u$7;

    const/16 v4, 0x18

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$7;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x16

    new-instance v3, Lcom/twitter/library/provider/u$8;

    const/16 v4, 0x19

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$8;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x17

    new-instance v3, Lcom/twitter/library/provider/u$9;

    const/16 v4, 0x1a

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$9;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x18

    new-instance v3, Lcom/twitter/library/provider/u$10;

    const/16 v4, 0x1b

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$10;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x19

    new-instance v3, Lcom/twitter/library/provider/u$11;

    const/16 v4, 0x1c

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$11;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    new-instance v3, Lcom/twitter/library/provider/u$13;

    const/16 v4, 0x1d

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$13;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    sget-object v3, Lcom/twitter/library/provider/u;->a:Laus$a;

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    new-instance v3, Lcom/twitter/library/provider/u$14;

    const/16 v4, 0x1f

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$14;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    new-instance v3, Lcom/twitter/library/provider/u$15;

    const/16 v4, 0x20

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$15;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    new-instance v3, Lcom/twitter/library/provider/u$16;

    const/16 v4, 0x21

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$16;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    sget-object v3, Lcom/twitter/library/provider/u;->a:Laus$a;

    aput-object v3, v1, v2

    const/16 v2, 0x20

    sget-object v3, Lcom/twitter/library/provider/u;->a:Laus$a;

    aput-object v3, v1, v2

    const/16 v2, 0x21

    new-instance v3, Lcom/twitter/library/provider/u$17;

    const/16 v4, 0x24

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$17;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x22

    new-instance v3, Lcom/twitter/library/provider/u$18;

    const/16 v4, 0x25

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$18;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x23

    sget-object v3, Lcom/twitter/library/provider/u;->a:Laus$a;

    aput-object v3, v1, v2

    const/16 v2, 0x24

    new-instance v3, Lcom/twitter/library/provider/u$19;

    const/16 v4, 0x27

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$19;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x25

    new-instance v3, Lcom/twitter/library/provider/u$20;

    const/16 v4, 0x28

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$20;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x26

    new-instance v3, Lcom/twitter/library/provider/u$21;

    const/16 v4, 0x29

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$21;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x27

    new-instance v3, Lcom/twitter/library/provider/u$22;

    const/16 v4, 0x2a

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/u$22;-><init>(Lcom/twitter/library/provider/u;I)V

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
