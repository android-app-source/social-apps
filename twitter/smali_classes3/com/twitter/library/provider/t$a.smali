.class Lcom/twitter/library/provider/t$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/provider/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field public final a:J

.field public final b:I

.field public final c:J

.field public final d:J

.field public final e:J

.field public final f:I

.field public final g:I

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final i:I

.field public final j:I

.field public final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final m:I

.field public final n:I

.field public final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbzv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(IILjava/util/List;IILjava/util/List;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 13560
    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v15, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v13, p1

    move/from16 v14, p2

    move-object/from16 v16, p3

    move/from16 v17, p4

    move/from16 v18, p5

    move-object/from16 v19, p6

    invoke-direct/range {v0 .. v20}, Lcom/twitter/library/provider/t$a;-><init>(JIJJJIILjava/util/List;IILjava/util/List;Ljava/util/List;IILjava/util/List;Ljava/util/List;)V

    .line 13563
    return-void
.end method

.method constructor <init>(JIJJJIILjava/util/List;IILjava/util/List;Ljava/util/List;IILjava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIJJJII",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbzv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 13539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13540
    iput-wide p1, p0, Lcom/twitter/library/provider/t$a;->a:J

    .line 13541
    iput p3, p0, Lcom/twitter/library/provider/t$a;->b:I

    .line 13542
    iput-wide p4, p0, Lcom/twitter/library/provider/t$a;->c:J

    .line 13543
    iput-wide p6, p0, Lcom/twitter/library/provider/t$a;->d:J

    .line 13544
    iput-wide p8, p0, Lcom/twitter/library/provider/t$a;->e:J

    .line 13545
    iput p10, p0, Lcom/twitter/library/provider/t$a;->f:I

    .line 13546
    iput p11, p0, Lcom/twitter/library/provider/t$a;->g:I

    .line 13547
    iput-object p12, p0, Lcom/twitter/library/provider/t$a;->h:Ljava/util/List;

    .line 13548
    iput p13, p0, Lcom/twitter/library/provider/t$a;->i:I

    .line 13549
    move/from16 v0, p14

    iput v0, p0, Lcom/twitter/library/provider/t$a;->j:I

    .line 13550
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/library/provider/t$a;->k:Ljava/util/List;

    .line 13551
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/twitter/library/provider/t$a;->l:Ljava/util/List;

    .line 13552
    move/from16 v0, p17

    iput v0, p0, Lcom/twitter/library/provider/t$a;->m:I

    .line 13553
    move/from16 v0, p18

    iput v0, p0, Lcom/twitter/library/provider/t$a;->n:I

    .line 13554
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/twitter/library/provider/t$a;->o:Ljava/util/List;

    .line 13555
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/twitter/library/provider/t$a;->p:Ljava/util/List;

    .line 13556
    return-void
.end method


# virtual methods
.method a()J
    .locals 2

    .prologue
    .line 13566
    iget-wide v0, p0, Lcom/twitter/library/provider/t$a;->d:J

    return-wide v0
.end method
