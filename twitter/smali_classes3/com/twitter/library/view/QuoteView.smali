.class public Lcom/twitter/library/view/QuoteView;
.super Landroid/view/ViewGroup;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/ui/image/a;


# static fields
.field private static final b:Landroid/text/TextPaint;


# instance fields
.field private A:Z

.field private B:Z

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:F

.field private I:F

.field private J:F

.field private K:Z

.field private L:I

.field protected a:Lcom/twitter/model/core/r;

.field private final c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/graphics/Rect;

.field private final f:Landroid/graphics/RectF;

.field private final g:Lcom/twitter/ui/widget/i;

.field private final h:Lcom/twitter/ui/widget/TweetHeaderView;

.field private final i:Lbxj;

.field private final j:Lcom/twitter/ui/widget/TextLayoutView;

.field private final k:F

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:I

.field private final s:I

.field private final t:I

.field private final u:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private final v:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private w:Ljava/lang/String;

.field private x:Z

.field private y:Landroid/text/StaticLayout;

.field private z:Landroid/text/StaticLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 61
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/library/view/QuoteView;->b:Landroid/text/TextPaint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/view/QuoteView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 119
    sget v0, Lazw$c;->quoteViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/view/QuoteView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 120
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 123
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/view/QuoteView;->e:Landroid/graphics/Rect;

    .line 73
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/view/QuoteView;->f:Landroid/graphics/RectF;

    .line 100
    iput-boolean v4, p0, Lcom/twitter/library/view/QuoteView;->B:Z

    .line 112
    iput v3, p0, Lcom/twitter/library/view/QuoteView;->L:I

    .line 124
    invoke-virtual {p0, v3}, Lcom/twitter/library/view/QuoteView;->setWillNotDraw(Z)V

    .line 126
    sget-object v0, Lazw$l;->QuoteView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 129
    sget v1, Lazw$l;->QuoteView_android_lineSpacingExtra:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->J:F

    .line 130
    sget v1, Lazw$l;->QuoteView_borderColor:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->p:I

    .line 131
    sget v1, Lazw$l;->QuoteView_borderCornerRadius:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->G:I

    .line 132
    sget v1, Lazw$l;->QuoteView_contentColor:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->n:I

    .line 133
    sget v1, Lazw$l;->QuoteView_bylineColor:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->o:I

    .line 135
    invoke-static {p1}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/view/QuoteView;->g:Lcom/twitter/ui/widget/i;

    .line 137
    new-instance v1, Lcom/twitter/ui/widget/TweetHeaderView;

    sget v2, Lazw$l;->QuoteView_quoteViewHeaderStyle:I

    .line 138
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-direct {v1, p1, v2}, Lcom/twitter/ui/widget/TweetHeaderView;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    .line 139
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    invoke-virtual {v1, v3}, Lcom/twitter/ui/widget/TweetHeaderView;->setShowTimestamp(Z)V

    .line 140
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    invoke-virtual {p0, v1}, Lcom/twitter/library/view/QuoteView;->addView(Landroid/view/View;)V

    .line 142
    new-instance v1, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-direct {v1, p1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    .line 143
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v1, v4}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->c(Z)V

    .line 144
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v1, v3}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setShowPlayerOverlay(Z)V

    .line 145
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    sget v2, Lazw$l;->QuoteView_mediaPlaceholder:I

    .line 146
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 145
    invoke-virtual {v1, v2}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setMediaPlaceholder(I)V

    .line 147
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v1, v3}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setBackgroundResource(I)V

    .line 148
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    sget v2, Lazw$l;->QuoteView_mediaDividerSize:I

    .line 149
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 148
    invoke-virtual {v1, v2}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setMediaDividerSize(I)V

    .line 150
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {p0, v1}, Lcom/twitter/library/view/QuoteView;->addView(Landroid/view/View;)V

    .line 152
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/library/view/QuoteView;->d:Landroid/widget/ImageView;

    .line 153
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->d:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 154
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->d:Landroid/widget/ImageView;

    sget v2, Lazw$f;->bg_quoted_media_warning:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 155
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->d:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Lcom/twitter/library/view/QuoteView;->addView(Landroid/view/View;)V

    .line 157
    sget v1, Lazw$l;->QuoteView_sensitiveMediaCoverDrawable:I

    .line 158
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->u:I

    .line 159
    sget v1, Lazw$l;->QuoteView_sensitiveMediaCoverSmallDrawable:I

    .line 160
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->v:I

    .line 162
    sget v1, Lazw$l;->QuoteView_borderWidth:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->q:I

    .line 163
    sget v1, Lazw$l;->QuoteView_mediaTextGap:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->r:I

    .line 164
    sget v1, Lazw$l;->QuoteView_contentPaddingTop:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->s:I

    .line 165
    sget v1, Lazw$l;->QuoteView_compactMediaWidth:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->t:I

    .line 167
    sget v1, Lazw$l;->QuoteView_interstitialTextSize:I

    .line 168
    invoke-static {}, Lcni;->a()F

    move-result v2

    .line 167
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->k:F

    .line 169
    sget v1, Lazw$l;->QuoteView_interstitialTextColor:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->l:I

    .line 170
    sget v1, Lazw$l;->QuoteView_interstitialBackgroundColor:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->m:I

    .line 173
    new-instance v1, Lcom/twitter/ui/widget/TextLayoutView;

    sget v2, Lazw$l;->QuoteView_quoteViewReplyContextStyle:I

    .line 174
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-direct {v1, p1, v2}, Lcom/twitter/ui/widget/TextLayoutView;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    .line 175
    new-instance v1, Lbxj;

    iget-object v2, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lbxj;-><init>(Lcom/twitter/ui/widget/TextLayoutView;Landroid/content/res/Resources;)V

    iput-object v1, p0, Lcom/twitter/library/view/QuoteView;->i:Lbxj;

    .line 176
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {p0, v1}, Lcom/twitter/library/view/QuoteView;->addView(Landroid/view/View;)V

    .line 178
    sget v1, Lazw$l;->QuoteView_contentSize:I

    sget v2, Lcni;->a:F

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    .line 179
    sget v2, Lazw$l;->QuoteView_bylineSize:I

    .line 180
    invoke-static {v1}, Lcni;->a(F)F

    move-result v3

    .line 179
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/twitter/library/view/QuoteView;->a(FF)V

    .line 182
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 184
    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->a()V

    .line 185
    return-void
.end method

.method private a(Landroid/text/Layout;Ljava/lang/String;Landroid/graphics/Paint;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 621
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/library/view/QuoteView;->e:Landroid/graphics/Rect;

    invoke-virtual {p3, p2, v0, v1, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 622
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->e:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-nez v1, :cond_0

    .line 625
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, v0}, Landroid/text/Layout;->getLineAscent(I)I

    move-result v0

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;ILandroid/text/TextPaint;I)Landroid/text/StaticLayout;
    .locals 12

    .prologue
    .line 640
    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v6, 0x3f800000    # 1.0f

    iget v7, p0, Lcom/twitter/library/view/QuoteView;->J:F

    const/4 v8, 0x0

    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v0, p1

    move-object v3, p3

    move v4, p2

    move v10, p2

    move/from16 v11, p4

    invoke-static/range {v0 .. v11}, Lcom/twitter/util/ui/k;->a(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;II)Landroid/text/StaticLayout;

    move-result-object v0

    .line 643
    if-nez v0, :cond_0

    .line 644
    new-instance v0, Landroid/text/StaticLayout;

    const/4 v2, 0x0

    .line 645
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000    # 1.0f

    iget v8, p0, Lcom/twitter/library/view/QuoteView;->J:F

    const/4 v9, 0x0

    move-object v1, p1

    move-object v4, p3

    move v5, p2

    invoke-direct/range {v0 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 648
    :cond_0
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 597
    iget v0, p0, Lcom/twitter/library/view/QuoteView;->L:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 598
    :goto_0
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v1, v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->a(Z)V

    .line 599
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v1, v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setShowMediaBadge(Z)V

    .line 601
    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/library/view/QuoteView;->u:I

    .line 602
    :goto_1
    if-lez v0, :cond_0

    .line 603
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->d:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 605
    :cond_0
    return-void

    .line 597
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 601
    :cond_2
    iget v0, p0, Lcom/twitter/library/view/QuoteView;->v:I

    goto :goto_1
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->a:Lcom/twitter/model/core/r;

    iget-boolean v0, v0, Lcom/twitter/model/core/r;->l:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/view/QuoteView;->K:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 612
    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 652
    iput-object v0, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    .line 653
    iput-object v0, p0, Lcom/twitter/library/view/QuoteView;->z:Landroid/text/StaticLayout;

    .line 654
    return-void
.end method

.method private getApplicableMediaView()Landroid/view/View;
    .locals 1

    .prologue
    .line 617
    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->d:Landroid/widget/ImageView;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    goto :goto_0
.end method

.method private getInterstitialString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 339
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lazw$k;->quote_tweet_interstitial_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getOwnerId()J
    .locals 2

    .prologue
    .line 629
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 630
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(FF)V
    .locals 4

    .prologue
    .line 212
    iget v0, p0, Lcom/twitter/library/view/QuoteView;->H:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/view/QuoteView;->I:F

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_1

    .line 213
    :cond_0
    iput p1, p0, Lcom/twitter/library/view/QuoteView;->H:F

    .line 214
    iput p2, p0, Lcom/twitter/library/view/QuoteView;->I:F

    .line 215
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    iget v1, p0, Lcom/twitter/library/view/QuoteView;->H:F

    iget v2, p0, Lcom/twitter/library/view/QuoteView;->I:F

    iget v3, p0, Lcom/twitter/library/view/QuoteView;->I:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/ui/widget/TweetHeaderView;->a(FFF)V

    .line 216
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0, p2}, Lcom/twitter/ui/widget/TextLayoutView;->a(F)Lcom/twitter/ui/widget/TextLayoutView;

    .line 217
    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->g()V

    .line 218
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->requestLayout()V

    .line 219
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->invalidate()V

    .line 221
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/model/core/r;Z)V
    .locals 10

    .prologue
    .line 228
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/view/QuoteView;->a(Z)V

    .line 229
    iput-object p1, p0, Lcom/twitter/library/view/QuoteView;->a:Lcom/twitter/model/core/r;

    .line 230
    if-eqz p1, :cond_9

    .line 231
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TweetHeaderView;->setVisibility(I)V

    .line 233
    invoke-static {}, Lcmj;->a()Z

    move-result v4

    .line 235
    invoke-static {}, Lcmj;->b()Z

    move-result v5

    .line 236
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    iget-object v1, p1, Lcom/twitter/model/core/r;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    const/4 v3, 0x0

    iget-boolean v6, p1, Lcom/twitter/model/core/r;->t:Z

    if-eqz v6, :cond_0

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    iget-boolean v6, p1, Lcom/twitter/model/core/r;->s:Z

    if-eqz v6, :cond_1

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    :goto_1
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/ui/widget/TweetHeaderView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 239
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->i:Lbxj;

    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->getOwnerId()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lbxj;->a(Lcom/twitter/model/core/r;J)V

    .line 240
    iget-boolean v0, p0, Lcom/twitter/library/view/QuoteView;->B:Z

    if-eqz v0, :cond_2

    .line 241
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->a:Lcom/twitter/model/core/r;

    invoke-static {v0}, Lcom/twitter/model/util/a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/model/util/a;

    move-result-object v0

    const/4 v1, 0x1

    .line 242
    invoke-virtual {v0, v1}, Lcom/twitter/model/util/a;->a(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    const/4 v1, 0x1

    .line 243
    invoke-virtual {v0, v1}, Lcom/twitter/model/util/a;->b(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    const/4 v1, 0x0

    .line 244
    invoke-virtual {v0, v1}, Lcom/twitter/model/util/a;->e(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    .line 245
    invoke-virtual {v0}, Lcom/twitter/model/util/a;->a()Lcom/twitter/model/core/e;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/e;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/view/QuoteView;->w:Ljava/lang/String;

    .line 250
    :goto_2
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0, p2}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setFromMemoryOnly(Z)V

    .line 253
    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 254
    const/4 v3, 0x0

    .line 255
    const/4 v2, 0x0

    .line 256
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 257
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setVisibility(I)V

    .line 289
    :goto_3
    iget-object v4, p1, Lcom/twitter/model/core/r;->c:Ljava/lang/String;

    iget-object v5, p1, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/library/view/QuoteView;->w:Ljava/lang/String;

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v9}, Lbxs;->a(Landroid/view/View;Lcax;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 303
    :goto_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/view/QuoteView;->x:Z

    .line 304
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->invalidate()V

    .line 305
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->requestLayout()V

    .line 306
    return-void

    .line 236
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 248
    :cond_2
    iget-object v0, p1, Lcom/twitter/model/core/r;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/view/QuoteView;->w:Ljava/lang/String;

    goto :goto_2

    .line 259
    :cond_3
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->d:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 260
    iget-object v2, p1, Lcom/twitter/model/core/r;->k:Lcax;

    .line 261
    iget-object v0, p1, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    .line 262
    invoke-static {v0}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 263
    iget-object v1, p1, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-object v1, v1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    .line 264
    invoke-static {v1}, Lcom/twitter/model/util/c;->e(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v1

    .line 265
    iget-object v3, p1, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-object v3, v3, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    sget-object v4, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    .line 266
    invoke-static {v3, v4}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v3

    .line 267
    iget-object v4, p0, Lcom/twitter/library/view/QuoteView;->a:Lcom/twitter/model/core/r;

    iget-object v4, v4, Lcom/twitter/model/core/r;->p:Ljava/lang/String;

    .line 268
    if-eqz v0, :cond_4

    iget-boolean v5, p0, Lcom/twitter/library/view/QuoteView;->B:Z

    if-eqz v5, :cond_4

    .line 269
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0, v4}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->a(Ljava/lang/Iterable;Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setVisibility(I)V

    goto :goto_3

    .line 271
    :cond_4
    if-eqz v1, :cond_5

    iget-boolean v0, p0, Lcom/twitter/library/view/QuoteView;->B:Z

    if-eqz v0, :cond_5

    .line 272
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-static {v1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->a(Ljava/lang/Iterable;Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setVisibility(I)V

    goto :goto_3

    .line 274
    :cond_5
    invoke-static {v3}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/twitter/library/view/QuoteView;->B:Z

    if-eqz v0, :cond_7

    .line 275
    iget v0, p0, Lcom/twitter/library/view/QuoteView;->L:I

    if-nez v0, :cond_6

    .line 276
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0, v3, v4}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->a(Ljava/lang/Iterable;Ljava/lang/String;)V

    .line 280
    :goto_5
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setVisibility(I)V

    goto/16 :goto_3

    .line 278
    :cond_6
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    const/4 v1, 0x0

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->a(Ljava/lang/Iterable;Ljava/lang/String;)V

    goto :goto_5

    .line 281
    :cond_7
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcax;->q()Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/twitter/library/view/QuoteView;->B:Z

    if-eqz v0, :cond_8

    .line 283
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0, v2}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setCard(Lcax;)V

    .line 284
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setVisibility(I)V

    goto/16 :goto_3

    .line 286
    :cond_8
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setVisibility(I)V

    goto/16 :goto_3

    .line 298
    :cond_9
    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->getInterstitialString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/view/QuoteView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 299
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TweetHeaderView;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->d:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 301
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setVisibility(I)V

    goto/16 :goto_4
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 188
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->a:Lcom/twitter/model/core/r;

    if-eqz v0, :cond_0

    .line 189
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/view/QuoteView;->x:Z

    .line 190
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->invalidate()V

    .line 191
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->requestLayout()V

    .line 193
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/view/QuoteView;->a:Lcom/twitter/model/core/r;

    .line 194
    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->g()V

    .line 195
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->d()V

    .line 196
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TextLayoutView;->setVisibility(I)V

    .line 199
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->d()V

    .line 586
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->e()V

    .line 323
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->f()V

    .line 328
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 532
    sget-object v0, Lcom/twitter/library/view/QuoteView;->b:Landroid/text/TextPaint;

    .line 533
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    .line 534
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    .line 536
    iget-object v3, p0, Lcom/twitter/library/view/QuoteView;->a:Lcom/twitter/model/core/r;

    if-nez v3, :cond_1

    .line 537
    iget v3, p0, Lcom/twitter/library/view/QuoteView;->m:I

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 538
    iget-object v3, p0, Lcom/twitter/library/view/QuoteView;->f:Landroid/graphics/RectF;

    invoke-virtual {v3, v4, v4, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 539
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->f:Landroid/graphics/RectF;

    iget v2, p0, Lcom/twitter/library/view/QuoteView;->G:I

    int-to-float v2, v2

    iget v3, p0, Lcom/twitter/library/view/QuoteView;->G:I

    int-to-float v3, v3

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 540
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->z:Landroid/text/StaticLayout;

    if-eqz v1, :cond_0

    .line 541
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 542
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 543
    iget v1, p0, Lcom/twitter/library/view/QuoteView;->k:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 544
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->g:Lcom/twitter/ui/widget/i;

    iget-object v1, v1, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 545
    iget v1, p0, Lcom/twitter/library/view/QuoteView;->l:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 546
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->z:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 547
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 550
    :cond_1
    iget v3, p0, Lcom/twitter/library/view/QuoteView;->q:I

    int-to-float v3, v3

    .line 551
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v3, v4

    .line 553
    iget v5, p0, Lcom/twitter/library/view/QuoteView;->q:I

    if-lez v5, :cond_2

    .line 554
    iget v5, p0, Lcom/twitter/library/view/QuoteView;->p:I

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->setColor(I)V

    .line 555
    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 556
    iget-object v3, p0, Lcom/twitter/library/view/QuoteView;->f:Landroid/graphics/RectF;

    sub-float/2addr v1, v4

    sub-float/2addr v2, v4

    invoke-virtual {v3, v4, v4, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 558
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 559
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->f:Landroid/graphics/RectF;

    iget v2, p0, Lcom/twitter/library/view/QuoteView;->G:I

    int-to-float v2, v2

    iget v3, p0, Lcom/twitter/library/view/QuoteView;->G:I

    int-to-float v3, v3

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 560
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 563
    :cond_2
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    if-eqz v1, :cond_0

    .line 564
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 565
    iget v1, p0, Lcom/twitter/library/view/QuoteView;->C:I

    int-to-float v1, v1

    iget v2, p0, Lcom/twitter/library/view/QuoteView;->D:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 566
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v1

    iget v2, p0, Lcom/twitter/library/view/QuoteView;->F:I

    if-le v1, v2, :cond_3

    .line 572
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    iget v3, p0, Lcom/twitter/library/view/QuoteView;->F:I

    .line 573
    invoke-virtual {v2, v3}, Landroid/text/StaticLayout;->getLineTop(I)I

    move-result v2

    .line 572
    invoke-virtual {p1, v6, v6, v1, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 575
    :cond_3
    iget v1, p0, Lcom/twitter/library/view/QuoteView;->H:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 576
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->g:Lcom/twitter/ui/widget/i;

    iget-object v1, v1, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 577
    iget v1, p0, Lcom/twitter/library/view/QuoteView;->n:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 578
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 579
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 468
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/view/QuoteView;->x:Z

    if-eqz v0, :cond_3

    .line 469
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/view/QuoteView;->x:Z

    .line 471
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->a:Lcom/twitter/model/core/r;

    if-eqz v0, :cond_3

    .line 472
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingTop()I

    move-result v0

    iget v1, p0, Lcom/twitter/library/view/QuoteView;->q:I

    add-int v3, v0, v1

    .line 473
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/twitter/library/view/QuoteView;->q:I

    add-int/2addr v1, v0

    .line 475
    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->getApplicableMediaView()Landroid/view/View;

    move-result-object v4

    .line 479
    iget-boolean v0, p0, Lcom/twitter/library/view/QuoteView;->A:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/twitter/library/view/QuoteView;->L:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 480
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v1

    iget v2, p0, Lcom/twitter/library/view/QuoteView;->r:I

    add-int/2addr v0, v2

    .line 485
    :goto_0
    iget-object v2, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    iget-object v5, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    .line 486
    invoke-virtual {v5}, Lcom/twitter/ui/widget/TweetHeaderView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v0

    iget-object v6, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    .line 487
    invoke-virtual {v6}, Lcom/twitter/ui/widget/TweetHeaderView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v3

    .line 485
    invoke-virtual {v2, v0, v3, v5, v6}, Lcom/twitter/ui/widget/TweetHeaderView;->layout(IIII)V

    .line 490
    iget-object v2, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v2}, Lcom/twitter/ui/widget/TextLayoutView;->getVisibility()I

    move-result v2

    const/16 v5, 0x8

    if-eq v2, v5, :cond_5

    .line 491
    iget-object v2, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    invoke-virtual {v2}, Lcom/twitter/ui/widget/TweetHeaderView;->getBottom()I

    move-result v2

    .line 492
    iget-object v5, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    iget-object v6, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    .line 493
    invoke-virtual {v6}, Lcom/twitter/ui/widget/TextLayoutView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v0

    iget-object v7, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    .line 494
    invoke-virtual {v7}, Lcom/twitter/ui/widget/TextLayoutView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v2

    .line 492
    invoke-virtual {v5, v0, v2, v6, v7}, Lcom/twitter/ui/widget/TextLayoutView;->layout(IIII)V

    .line 495
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TextLayoutView;->getBottom()I

    move-result v0

    iget v2, p0, Lcom/twitter/library/view/QuoteView;->s:I

    add-int/2addr v0, v2

    .line 502
    :goto_1
    iget-boolean v5, p0, Lcom/twitter/library/view/QuoteView;->A:Z

    iget v2, p0, Lcom/twitter/library/view/QuoteView;->L:I

    if-nez v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    xor-int/2addr v2, v5

    .line 504
    if-eqz v2, :cond_7

    .line 506
    iget-object v2, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    if-eqz v2, :cond_1

    .line 507
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getWidth()I

    move-result v2

    iget v5, p0, Lcom/twitter/library/view/QuoteView;->q:I

    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    invoke-virtual {v6}, Landroid/text/StaticLayout;->getWidth()I

    move-result v6

    add-int/2addr v5, v6

    sub-int/2addr v2, v5

    iput v2, p0, Lcom/twitter/library/view/QuoteView;->C:I

    .line 514
    :cond_1
    :goto_3
    iget v2, p0, Lcom/twitter/library/view/QuoteView;->E:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/twitter/library/view/QuoteView;->D:I

    .line 515
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    .line 516
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-lez v2, :cond_3

    .line 518
    iget v2, p0, Lcom/twitter/library/view/QuoteView;->L:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_2

    .line 519
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TextLayoutView;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_8

    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TextLayoutView;->getTop()I

    move-result v0

    .line 523
    :cond_2
    :goto_4
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v1

    .line 524
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    .line 523
    invoke-virtual {v4, v1, v0, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 528
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 482
    goto/16 :goto_0

    .line 497
    :cond_5
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TweetHeaderView;->getBottom()I

    move-result v0

    iget v2, p0, Lcom/twitter/library/view/QuoteView;->s:I

    add-int/2addr v0, v2

    goto :goto_1

    .line 502
    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    .line 510
    :cond_7
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getWidth()I

    move-result v2

    iget v5, p0, Lcom/twitter/library/view/QuoteView;->q:I

    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    sub-int/2addr v2, v5

    .line 511
    iput v1, p0, Lcom/twitter/library/view/QuoteView;->C:I

    move v1, v2

    goto :goto_3

    :cond_8
    move v0, v3

    .line 519
    goto :goto_4
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    .line 344
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 345
    sget-object v2, Lcom/twitter/library/view/QuoteView;->b:Landroid/text/TextPaint;

    .line 346
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 347
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 348
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v10

    .line 349
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    .line 353
    const/high16 v3, 0x40000000    # 2.0f

    if-ne v1, v3, :cond_3

    move v8, v0

    .line 359
    :goto_0
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->a:Lcom/twitter/model/core/r;

    if-nez v0, :cond_4

    .line 360
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingLeft()I

    move-result v0

    sub-int v0, v8, v0

    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingRight()I

    move-result v1

    sub-int v3, v0, v1

    .line 361
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->z:Landroid/text/StaticLayout;

    if-nez v0, :cond_0

    if-lez v3, :cond_0

    .line 362
    iget v0, p0, Lcom/twitter/library/view/QuoteView;->k:F

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 363
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->g:Lcom/twitter/ui/widget/i;

    iget-object v0, v0, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 364
    new-instance v0, Landroid/text/StaticLayout;

    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->getInterstitialString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    iget v6, p0, Lcom/twitter/library/view/QuoteView;->J:F

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/twitter/library/view/QuoteView;->z:Landroid/text/StaticLayout;

    .line 367
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 368
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->z:Landroid/text/StaticLayout;

    if-eqz v1, :cond_1

    .line 369
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->z:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 457
    :cond_1
    :goto_1
    const/high16 v1, -0x80000000

    if-ne v10, v1, :cond_11

    .line 458
    invoke-static {v0, v9}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 463
    :cond_2
    :goto_2
    invoke-virtual {p0, v8, v0}, Lcom/twitter/library/view/QuoteView;->setMeasuredDimension(II)V

    .line 464
    return-void

    .line 356
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getMeasuredWidth()I

    move-result v0

    move v8, v0

    goto :goto_0

    .line 372
    :cond_4
    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->d()Z

    move-result v4

    .line 373
    iget-object v5, p0, Lcom/twitter/library/view/QuoteView;->w:Ljava/lang/String;

    .line 374
    const/4 v0, 0x0

    iget v1, p0, Lcom/twitter/library/view/QuoteView;->q:I

    mul-int/lit8 v1, v1, 0x2

    .line 375
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingLeft()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingRight()I

    move-result v3

    add-int/2addr v1, v3

    sub-int v1, v8, v1

    .line 374
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 379
    iget v0, p0, Lcom/twitter/library/view/QuoteView;->L:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_9

    if-eqz v4, :cond_9

    .line 380
    const/4 v0, 0x0

    iget v3, p0, Lcom/twitter/library/view/QuoteView;->t:I

    sub-int v3, v1, v3

    iget v6, p0, Lcom/twitter/library/view/QuoteView;->r:I

    sub-int/2addr v3, v6

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 384
    :goto_3
    iget-object v3, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Lcom/twitter/ui/widget/TweetHeaderView;->measure(II)V

    .line 388
    iget-object v3, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v3}, Lcom/twitter/ui/widget/TextLayoutView;->getVisibility()I

    move-result v3

    const/16 v6, 0x8

    if-eq v3, v6, :cond_5

    .line 389
    iget-object v3, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Lcom/twitter/ui/widget/TextLayoutView;->measure(II)V

    .line 395
    :cond_5
    if-eqz v4, :cond_c

    .line 397
    iget v3, p0, Lcom/twitter/library/view/QuoteView;->L:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_a

    .line 399
    iget v3, p0, Lcom/twitter/library/view/QuoteView;->t:I

    .line 400
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    move v11, v1

    move v1, v3

    move v3, v0

    move v0, v11

    .line 411
    :goto_4
    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->getApplicableMediaView()Landroid/view/View;

    move-result-object v6

    .line 412
    const/high16 v7, 0x40000000    # 2.0f

    .line 413
    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 412
    invoke-virtual {v6, v1, v0}, Landroid/view/View;->measure(II)V

    .line 415
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 424
    :goto_5
    iget v1, p0, Lcom/twitter/library/view/QuoteView;->H:F

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 425
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->g:Lcom/twitter/ui/widget/i;

    iget-object v1, v1, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 426
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    if-nez v1, :cond_6

    invoke-static {v5}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    if-lez v3, :cond_6

    .line 427
    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontSpacing()F

    move-result v1

    .line 428
    iget v6, p0, Lcom/twitter/library/view/QuoteView;->L:I

    if-nez v6, :cond_d

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v6, v1, v6

    if-ltz v6, :cond_d

    if-eqz v4, :cond_d

    .line 429
    const/4 v4, 0x1

    int-to-float v6, v0

    div-float v1, v6, v1

    float-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v1, v6

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->F:I

    .line 434
    :goto_6
    iget v1, p0, Lcom/twitter/library/view/QuoteView;->F:I

    invoke-direct {p0, v5, v3, v2, v1}, Lcom/twitter/library/view/QuoteView;->a(Ljava/lang/String;ILandroid/text/TextPaint;I)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    .line 436
    :cond_6
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    if-eqz v1, :cond_7

    .line 437
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    invoke-direct {p0, v1, v5, v2}, Lcom/twitter/library/view/QuoteView;->a(Landroid/text/Layout;Ljava/lang/String;Landroid/graphics/Paint;)I

    move-result v1

    neg-int v1, v1

    iput v1, p0, Lcom/twitter/library/view/QuoteView;->E:I

    .line 439
    :cond_7
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    if-nez v1, :cond_f

    const/4 v1, 0x0

    .line 442
    :goto_7
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, p0, Lcom/twitter/library/view/QuoteView;->q:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 443
    iget v3, p0, Lcom/twitter/library/view/QuoteView;->L:I

    if-nez v3, :cond_10

    .line 444
    iget-object v3, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    invoke-virtual {v3}, Lcom/twitter/ui/widget/TweetHeaderView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v2, v3

    .line 445
    if-gtz v1, :cond_8

    if-lez v0, :cond_12

    .line 446
    :cond_8
    iget v3, p0, Lcom/twitter/library/view/QuoteView;->s:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    .line 452
    :goto_8
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TextLayoutView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    .line 453
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->j:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TextLayoutView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    goto/16 :goto_1

    :cond_9
    move v0, v1

    .line 382
    goto/16 :goto_3

    .line 402
    :cond_a
    const/4 v0, 0x0

    iget v3, p0, Lcom/twitter/library/view/QuoteView;->r:I

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 404
    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->c()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 405
    int-to-float v0, v1

    const/high16 v3, 0x3f400000    # 0.75f

    mul-float/2addr v0, v3

    .line 406
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    const/high16 v3, 0x40000000    # 2.0f

    .line 405
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    move v3, v1

    goto/16 :goto_4

    .line 408
    :cond_b
    const/4 v0, 0x0

    move v3, v1

    goto/16 :goto_4

    .line 418
    :cond_c
    const/4 v0, 0x0

    .line 420
    iget-object v3, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v3}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->d()V

    move v3, v1

    goto/16 :goto_5

    .line 431
    :cond_d
    iget v1, p0, Lcom/twitter/library/view/QuoteView;->L:I

    const/4 v4, 0x1

    if-ne v1, v4, :cond_e

    const/4 v1, 0x1

    :goto_9
    iput v1, p0, Lcom/twitter/library/view/QuoteView;->F:I

    goto/16 :goto_6

    :cond_e
    const/4 v1, 0x5

    goto :goto_9

    .line 439
    :cond_f
    iget-object v1, p0, Lcom/twitter/library/view/QuoteView;->y:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/twitter/library/view/QuoteView;->E:I

    add-int/2addr v1, v2

    goto :goto_7

    .line 448
    :cond_10
    iget v3, p0, Lcom/twitter/library/view/QuoteView;->L:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_12

    .line 449
    iget-object v3, p0, Lcom/twitter/library/view/QuoteView;->h:Lcom/twitter/ui/widget/TweetHeaderView;

    .line 450
    invoke-virtual {v3}, Lcom/twitter/ui/widget/TweetHeaderView;->getMeasuredHeight()I

    move-result v3

    iget v4, p0, Lcom/twitter/library/view/QuoteView;->s:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 449
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v2

    goto :goto_8

    .line 459
    :cond_11
    const/high16 v1, 0x40000000    # 2.0f

    if-ne v10, v1, :cond_2

    move v0, v9

    .line 460
    goto/16 :goto_2

    :cond_12
    move v0, v2

    goto :goto_8
.end method

.method public setAlwaysExpandMedia(Z)V
    .locals 1

    .prologue
    .line 314
    iget-boolean v0, p0, Lcom/twitter/library/view/QuoteView;->B:Z

    if-eq v0, p1, :cond_0

    .line 315
    iput-boolean p1, p0, Lcom/twitter/library/view/QuoteView;->B:Z

    .line 316
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->requestLayout()V

    .line 318
    :cond_0
    return-void
.end method

.method public setBorderCornerRadius(I)V
    .locals 0

    .prologue
    .line 661
    iput p1, p0, Lcom/twitter/library/view/QuoteView;->G:I

    .line 662
    return-void
.end method

.method public setDisplayMode(I)V
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/twitter/library/view/QuoteView;->L:I

    if-eq v0, p1, :cond_0

    .line 206
    iput p1, p0, Lcom/twitter/library/view/QuoteView;->L:I

    .line 207
    invoke-direct {p0}, Lcom/twitter/library/view/QuoteView;->a()V

    .line 209
    :cond_0
    return-void
.end method

.method public setDisplaySensitiveMedia(Z)V
    .locals 1

    .prologue
    .line 589
    iget-boolean v0, p0, Lcom/twitter/library/view/QuoteView;->K:Z

    if-eq p1, v0, :cond_0

    .line 590
    iput-boolean p1, p0, Lcom/twitter/library/view/QuoteView;->K:Z

    .line 591
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->invalidate()V

    .line 592
    invoke-virtual {p0}, Lcom/twitter/library/view/QuoteView;->requestLayout()V

    .line 594
    :cond_0
    return-void
.end method

.method public setMediaFromMemoryOnly(Z)V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setFromMemoryOnly(Z)V

    .line 332
    return-void
.end method

.method public setOnImageLoadedListener(Lcom/twitter/library/media/widget/TweetMediaView$a;)V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/twitter/library/view/QuoteView;->c:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setOnImageLoadedListener(Lcom/twitter/library/media/widget/TweetMediaView$a;)V

    .line 336
    return-void
.end method

.method public setQuoteData(Lcom/twitter/model/core/r;)V
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/view/QuoteView;->a(Lcom/twitter/model/core/r;Z)V

    .line 225
    return-void
.end method

.method public setRenderRtl(Z)V
    .locals 0

    .prologue
    .line 657
    iput-boolean p1, p0, Lcom/twitter/library/view/QuoteView;->A:Z

    .line 658
    return-void
.end method
