.class Lcom/twitter/library/view/b$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/request/a$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/view/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/view/b$a;

.field private final b:Lcom/twitter/ui/widget/h;


# direct methods
.method constructor <init>(Lcom/twitter/library/view/b$a;Lcom/twitter/ui/widget/h;)V
    .locals 0

    .prologue
    .line 490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491
    iput-object p1, p0, Lcom/twitter/library/view/b$c;->a:Lcom/twitter/library/view/b$a;

    .line 492
    iput-object p2, p0, Lcom/twitter/library/view/b$c;->b:Lcom/twitter/ui/widget/h;

    .line 493
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/media/request/ImageResponse;)V
    .locals 4

    .prologue
    .line 497
    iget-object v0, p0, Lcom/twitter/library/view/b$c;->a:Lcom/twitter/library/view/b$a;

    invoke-virtual {v0}, Lcom/twitter/library/view/b$a;->a()Landroid/content/Context;

    move-result-object v0

    .line 498
    if-eqz v0, :cond_0

    .line 499
    iget-object v1, p0, Lcom/twitter/library/view/b$c;->b:Lcom/twitter/ui/widget/h;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v2, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/h;->b(Landroid/graphics/drawable/Drawable;)V

    .line 500
    iget-object v1, p0, Lcom/twitter/library/view/b$c;->a:Lcom/twitter/library/view/b$a;

    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a;

    invoke-virtual {v0}, Lcom/twitter/media/request/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/view/b$a;->b(Ljava/lang/String;)V

    .line 502
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/media/request/ResourceResponse;)V
    .locals 0

    .prologue
    .line 485
    check-cast p1, Lcom/twitter/media/request/ImageResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/library/view/b$c;->a(Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method
