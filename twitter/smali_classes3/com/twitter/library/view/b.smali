.class public Lcom/twitter/library/view/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/view/b$c;,
        Lcom/twitter/library/view/b$a;,
        Lcom/twitter/library/view/b$d;,
        Lcom/twitter/library/view/b$b;,
        Lcom/twitter/library/view/b$e;,
        Lcom/twitter/library/view/b$f;
    }
.end annotation


# static fields
.field private static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/view/b$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lcom/twitter/util/math/Size;

.field private static f:Z

.field private static g:Landroid/content/Context;

.field private static h:Ljava/lang/String;

.field private static i:I

.field private static final j:Lcoj$a;


# instance fields
.field public final a:Ljava/lang/String;

.field public b:I

.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xc8

    .line 56
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/twitter/library/view/b;->d:Ljava/util/Map;

    .line 57
    invoke-static {v1, v1}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/view/b;->e:Lcom/twitter/util/math/Size;

    .line 66
    const/4 v0, 0x0

    sput v0, Lcom/twitter/library/view/b;->i:I

    .line 68
    new-instance v0, Lcom/twitter/library/view/b$1;

    invoke-direct {v0}, Lcom/twitter/library/view/b$1;-><init>()V

    sput-object v0, Lcom/twitter/library/view/b;->j:Lcoj$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/twitter/library/view/b;->a:Ljava/lang/String;

    .line 93
    iput p2, p0, Lcom/twitter/library/view/b;->b:I

    .line 94
    sget-object v0, Lcom/twitter/library/view/b;->d:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/view/b$b;

    iget-object v0, v0, Lcom/twitter/library/view/b$b;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/view/b;->c:Ljava/lang/String;

    .line 95
    return-void
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 48
    sput p0, Lcom/twitter/library/view/b;->i:I

    return p0
.end method

.method public static a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Lcom/twitter/library/view/b;Landroid/view/View;Z)I
    .locals 6

    .prologue
    .line 177
    new-instance v1, Lcom/twitter/library/view/b$e;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/twitter/library/view/b$e;-><init>(Lcom/twitter/library/view/b$1;)V

    new-instance v4, Lcom/twitter/library/view/b$a;

    invoke-direct {v4, p0, p3}, Lcom/twitter/library/view/b$a;-><init>(Landroid/content/Context;Landroid/view/View;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/view/b;->a(Landroid/content/Context;Lcom/twitter/library/view/b$f;Landroid/text/SpannableStringBuilder;Lcom/twitter/library/view/b;Lcom/twitter/library/view/b$a;Z)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/library/view/b$f;Landroid/text/SpannableStringBuilder;Lcom/twitter/library/view/b;Lcom/twitter/library/view/b$a;Z)I
    .locals 6

    .prologue
    .line 188
    iget v1, p3, Lcom/twitter/library/view/b;->b:I

    .line 189
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    .line 190
    iget-object v0, p3, Lcom/twitter/library/view/b;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-gt v1, v0, :cond_2

    .line 191
    new-instance v3, Lcom/twitter/ui/widget/h;

    invoke-direct {v3}, Lcom/twitter/ui/widget/h;-><init>()V

    .line 193
    new-instance v0, Lcom/twitter/media/request/a$a;

    iget-object v4, p3, Lcom/twitter/library/view/b;->c:Ljava/lang/String;

    invoke-direct {v0, v4}, Lcom/twitter/media/request/a$a;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/twitter/library/view/b;->e:Lcom/twitter/util/math/Size;

    .line 194
    invoke-virtual {v0, v4}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 196
    new-instance v4, Lcom/twitter/library/view/b$c;

    invoke-direct {v4, p4, v3}, Lcom/twitter/library/view/b$c;-><init>(Lcom/twitter/library/view/b$a;Lcom/twitter/ui/widget/h;)V

    .line 197
    if-eqz p5, :cond_0

    .line 198
    invoke-virtual {v0, v4}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/b$b;)Lcom/twitter/media/request/b$a;

    .line 201
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/twitter/library/view/b$f;->a(Lcom/twitter/media/request/a;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 202
    iget-object v5, p3, Lcom/twitter/library/view/b;->c:Ljava/lang/String;

    invoke-virtual {p4, v5}, Lcom/twitter/library/view/b$a;->a(Ljava/lang/String;)V

    .line 204
    :try_start_0
    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_1

    .line 206
    :try_start_1
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/ImageResponse;

    invoke-virtual {v4, v0}, Lcom/twitter/library/view/b$c;->a(Lcom/twitter/media/request/ImageResponse;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    :cond_1
    :goto_0
    invoke-virtual {p4}, Lcom/twitter/library/view/b$a;->b()V

    .line 214
    const-string/jumbo v0, "\u202f"

    invoke-virtual {p2, v1, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 215
    const-string/jumbo v0, "\u202f"

    invoke-virtual {p2, v1, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 216
    new-instance v0, Lcom/twitter/library/view/b$d;

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4}, Lcom/twitter/library/view/b$d;-><init>(Landroid/graphics/drawable/Drawable;I)V

    const-string/jumbo v3, "\u202f"

    .line 218
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v1

    const/16 v4, 0x21

    .line 216
    invoke-virtual {p2, v0, v1, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 220
    const-string/jumbo v0, "\u202f"

    invoke-virtual {p2, v1, v0}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 222
    :cond_2
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    sub-int/2addr v0, v2

    return v0

    .line 211
    :catchall_0
    move-exception v0

    invoke-virtual {p4}, Lcom/twitter/library/view/b$a;->b()V

    throw v0

    .line 207
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Iterable;Landroid/text/SpannableStringBuilder;Z)Landroid/text/SpannableStringBuilder;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/h;",
            ">;",
            "Landroid/text/SpannableStringBuilder;",
            "Z)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 127
    new-instance v4, Lcom/twitter/library/view/b$e;

    invoke-direct {v4, v5}, Lcom/twitter/library/view/b$e;-><init>(Lcom/twitter/library/view/b$1;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/view/b;->a(Landroid/content/Context;Ljava/lang/Iterable;Landroid/text/SpannableStringBuilder;ZLcom/twitter/library/view/b$f;Landroid/view/View;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Iterable;Landroid/text/SpannableStringBuilder;ZLandroid/view/View;)Landroid/text/SpannableStringBuilder;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/h;",
            ">;",
            "Landroid/text/SpannableStringBuilder;",
            "Z",
            "Landroid/view/View;",
            ")",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .prologue
    .line 135
    new-instance v4, Lcom/twitter/library/view/b$e;

    const/4 v0, 0x0

    invoke-direct {v4, v0}, Lcom/twitter/library/view/b$e;-><init>(Lcom/twitter/library/view/b$1;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/view/b;->a(Landroid/content/Context;Ljava/lang/Iterable;Landroid/text/SpannableStringBuilder;ZLcom/twitter/library/view/b$f;Landroid/view/View;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Iterable;Landroid/text/SpannableStringBuilder;ZLcom/twitter/library/view/b$f;Landroid/view/View;)Landroid/text/SpannableStringBuilder;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/h;",
            ">;",
            "Landroid/text/SpannableStringBuilder;",
            "Z",
            "Lcom/twitter/library/view/b$f;",
            "Landroid/view/View;",
            ")",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 143
    invoke-static {p1, p3}, Lcom/twitter/library/view/b;->a(Ljava/lang/Iterable;Z)Ljava/util/List;

    move-result-object v0

    .line 145
    if-eqz p5, :cond_0

    .line 146
    invoke-static {p5}, Lcom/twitter/library/view/b$a;->a(Landroid/view/View;)V

    .line 149
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_2

    .line 160
    :cond_1
    return-object p2

    .line 155
    :cond_2
    new-instance v4, Lcom/twitter/library/view/b$a;

    invoke-direct {v4, p0, p5}, Lcom/twitter/library/view/b$a;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 156
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v6, v7

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/view/b;

    .line 157
    iget v0, v3, Lcom/twitter/library/view/b;->b:I

    add-int/2addr v0, v6

    iput v0, v3, Lcom/twitter/library/view/b;->b:I

    .line 158
    if-eqz p5, :cond_3

    const/4 v5, 0x1

    :goto_1
    move-object v0, p0

    move-object v1, p4

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/view/b;->a(Landroid/content/Context;Lcom/twitter/library/view/b$f;Landroid/text/SpannableStringBuilder;Lcom/twitter/library/view/b;Lcom/twitter/library/view/b$a;Z)I

    move-result v0

    add-int/2addr v0, v6

    move v6, v0

    .line 159
    goto :goto_0

    :cond_3
    move v5, v7

    .line 158
    goto :goto_1
.end method

.method private static a(Ljava/lang/Iterable;Z)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/h;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/view/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 337
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/h;

    .line 338
    iget-object v1, v0, Lcom/twitter/model/core/h;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/library/view/b;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 339
    if-eqz p1, :cond_1

    iget v1, v0, Lcom/twitter/model/core/h;->k:I

    .line 340
    :goto_1
    new-instance v4, Lcom/twitter/library/view/b;

    iget-object v0, v0, Lcom/twitter/model/core/h;->c:Ljava/lang/String;

    invoke-direct {v4, v0, v1}, Lcom/twitter/library/view/b;-><init>(Ljava/lang/String;I)V

    .line 341
    invoke-virtual {v2, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 339
    :cond_1
    iget v1, v0, Lcom/twitter/model/core/h;->h:I

    goto :goto_1

    .line 344
    :cond_2
    return-object v2
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 102
    sput-object p0, Lcom/twitter/library/view/b;->g:Landroid/content/Context;

    .line 103
    const-string/jumbo v0, "hashflags_settings_version"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/twitter/library/view/b;->i:I

    .line 104
    const-string/jumbo v0, "hashflags_settings_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/twitter/library/view/b;->f:Z

    .line 105
    const-string/jumbo v0, "hashflags_settings_location_prefix"

    invoke-static {v0}, Lcoj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/view/b;->h:Ljava/lang/String;

    .line 106
    const-class v0, Lcom/twitter/library/view/b;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 108
    invoke-static {}, Lcom/twitter/library/view/b;->g()V

    .line 109
    invoke-static {}, Lcom/twitter/library/view/b;->h()V

    .line 111
    sget-object v0, Lcom/twitter/library/view/b;->j:Lcoj$a;

    invoke-static {v0}, Lcoj;->a(Lcoj$a;)V

    .line 112
    return-void
.end method

.method public static declared-synchronized a()Z
    .locals 2

    .prologue
    .line 237
    const-class v1, Lcom/twitter/library/view/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/view/b;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 229
    const-class v1, Lcom/twitter/library/view/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/view/b;->d:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/view/b$b;

    .line 230
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/view/b$b;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 48
    sput-boolean p0, Lcom/twitter/library/view/b;->f:Z

    return p0
.end method

.method static synthetic b(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 48
    sput-object p0, Lcom/twitter/library/view/b;->h:Ljava/lang/String;

    return-object p0
.end method

.method public static declared-synchronized b()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/view/b$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 245
    const-class v1, Lcom/twitter/library/view/b;

    monitor-enter v1

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    sget-object v0, Lcom/twitter/library/view/b;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 246
    sget-object v0, Lcom/twitter/library/view/b;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 247
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 249
    :cond_0
    monitor-exit v1

    return-object v2
.end method

.method static synthetic c()I
    .locals 1

    .prologue
    .line 48
    sget v0, Lcom/twitter/library/view/b;->i:I

    return v0
.end method

.method static synthetic d()V
    .locals 0

    .prologue
    .line 48
    invoke-static {}, Lcom/twitter/library/view/b;->g()V

    return-void
.end method

.method static synthetic e()V
    .locals 0

    .prologue
    .line 48
    invoke-static {}, Lcom/twitter/library/view/b;->h()V

    return-void
.end method

.method static synthetic f()Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/twitter/library/view/b;->e:Lcom/twitter/util/math/Size;

    return-object v0
.end method

.method private static declared-synchronized g()V
    .locals 18

    .prologue
    .line 253
    const-class v11, Lcom/twitter/library/view/b;

    monitor-enter v11

    :try_start_0
    sget-object v2, Lcom/twitter/library/view/b;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 254
    sget-boolean v2, Lcom/twitter/library/view/b;->f:Z

    if-eqz v2, :cond_0

    .line 255
    const-string/jumbo v2, "hashflags_settings_groups"

    invoke-static {v2}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 256
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    .line 303
    :cond_0
    monitor-exit v11

    return-void

    .line 259
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_2
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 261
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "hashflags_settings_group_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "_enabled"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 262
    invoke-static {v3}, Lcoj;->a(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    .line 267
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "hashflags_settings_group_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "_start"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 268
    invoke-static {v3}, Lcoj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 271
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "hashflags_settings_group_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "_end"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 272
    invoke-static {v3}, Lcoj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v8

    .line 279
    :try_start_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "hashflags_settings_group_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "_keys"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 280
    invoke-static {v3}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 282
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "hashflags_settings_group_"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "_values"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 283
    invoke-static {v3}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v13

    .line 284
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v5

    if-ne v3, v5, :cond_2

    .line 285
    const/4 v3, 0x0

    .line 286
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move v5, v3

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    .line 287
    add-int/lit8 v10, v5, 0x1

    invoke-interface {v13, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 288
    const/16 v5, 0x2e

    invoke-virtual {v3, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 289
    const/4 v15, -0x1

    if-ne v5, v15, :cond_3

    move v5, v10

    .line 290
    goto :goto_1

    .line 292
    :cond_3
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v3, v5, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 293
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v17, Lcom/twitter/library/view/b;->h:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string/jumbo v17, "/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x0

    .line 294
    move/from16 v0, v17

    invoke-virtual {v3, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 296
    sget-object v15, Lcom/twitter/library/view/b;->d:Ljava/util/Map;

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v16

    new-instance v3, Lcom/twitter/library/view/b$b;

    .line 297
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v3 .. v9}, Lcom/twitter/library/view/b$b;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 296
    move-object/from16 v0, v16

    invoke-interface {v15, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v5, v10

    .line 298
    goto :goto_1

    .line 253
    :catchall_0
    move-exception v2

    monitor-exit v11

    throw v2

    .line 273
    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method

.method private static h()V
    .locals 6

    .prologue
    .line 306
    const-string/jumbo v0, "hashflags_settings_preload_images"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    sget-object v0, Lcom/twitter/library/view/b;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v0

    .line 308
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/g;->d()Lcom/twitter/library/media/manager/e;

    move-result-object v2

    .line 309
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 310
    invoke-static {}, Lcom/twitter/library/view/b;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 311
    new-instance v1, Lcom/twitter/library/view/b$2;

    invoke-direct {v1, v3, v2, v0}, Lcom/twitter/library/view/b$2;-><init>(Ljava/util/Iterator;Lcom/twitter/library/media/manager/e;Ljava/util/Timer;)V

    .line 329
    const-wide/32 v2, 0x11170

    const-wide/16 v4, 0xc8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 331
    :cond_0
    return-void
.end method
