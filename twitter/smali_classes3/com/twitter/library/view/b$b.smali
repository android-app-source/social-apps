.class public Lcom/twitter/library/view/b$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/view/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private final c:J

.field private final d:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 1

    .prologue
    .line 364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365
    iput-object p1, p0, Lcom/twitter/library/view/b$b;->a:Ljava/lang/String;

    .line 366
    iput-object p2, p0, Lcom/twitter/library/view/b$b;->b:Ljava/lang/String;

    .line 367
    iput-wide p3, p0, Lcom/twitter/library/view/b$b;->c:J

    .line 368
    iput-wide p5, p0, Lcom/twitter/library/view/b$b;->d:J

    .line 369
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 4

    .prologue
    .line 372
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/view/b$b;->a(J)Z

    move-result v0

    return v0
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 376
    iget-wide v0, p0, Lcom/twitter/library/view/b$b;->c:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/library/view/b$b;->d:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
