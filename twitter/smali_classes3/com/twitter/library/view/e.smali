.class public Lcom/twitter/library/view/e;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;Landroid/content/res/Resources;Z)Ljava/lang/String;
    .locals 4

    .prologue
    .line 53
    invoke-static {p0}, Lcom/twitter/library/view/e;->c(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/y;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 55
    if-eqz v0, :cond_2

    .line 56
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->s()Z

    move-result v2

    if-nez v2, :cond_3

    .line 57
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/twitter/model/core/Tweet;->c:Z

    if-eqz v2, :cond_3

    :cond_0
    if-nez p2, :cond_1

    if-nez v1, :cond_2

    .line 59
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->S()Z

    move-result v1

    if-nez v1, :cond_2

    .line 60
    invoke-static {p0}, Lcom/twitter/library/revenue/b;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 62
    :cond_2
    sget v0, Lazw$k;->promoted_without_advertiser:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    :cond_3
    sget v1, Lazw$k;->promoted_by:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(ILcom/twitter/ui/widget/TweetHeaderView;)V
    .locals 1
    .param p0    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 80
    invoke-virtual {p1}, Lcom/twitter/ui/widget/TweetHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 81
    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/TweetHeaderView;->setTimestampColor(Landroid/content/res/ColorStateList;)V

    .line 82
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/TweetHeaderView;->setUseTimestampColorForUsername(Z)V

    .line 83
    return-void
.end method

.method public static a(Landroid/widget/TextView;Lcom/twitter/model/core/Tweet;)V
    .locals 4

    .prologue
    .line 86
    invoke-virtual {p0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 87
    iget v0, p1, Lcom/twitter/model/core/Tweet;->o:I

    .line 88
    if-lez v0, :cond_0

    int-to-long v2, v0

    invoke-static {v1, v2, v3}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    sget v0, Lazw$d;->white:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 90
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    return-void

    .line 88
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/TweetView;Lcom/twitter/ui/widget/TweetHeaderView;)V
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbps;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    sget v0, Lazw$d;->medium_green:I

    invoke-static {v0, p2}, Lcom/twitter/library/view/e;->a(ILcom/twitter/ui/widget/TweetHeaderView;)V

    .line 77
    :goto_0
    return-void

    .line 74
    :cond_0
    invoke-virtual {p2}, Lcom/twitter/ui/widget/TweetHeaderView;->c()V

    .line 75
    invoke-virtual {p1}, Lcom/twitter/library/widget/TweetView;->a()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/ui/widget/TweetHeaderView;->setUseTimestampColorForUsername(Z)V

    goto :goto_0
.end method

.method public static b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    goto :goto_0
.end method

.method public static c(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcgi;->g:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->z:Ljava/lang/String;

    goto :goto_0
.end method
