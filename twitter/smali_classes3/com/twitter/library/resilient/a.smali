.class public Lcom/twitter/library/resilient/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/async/service/AsyncOperation$b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/async/service/AsyncOperation$b",
        "<",
        "Landroid/os/Bundle;",
        "Lcom/twitter/library/service/s;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/library/resilient/a;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/twitter/library/resilient/a;->b:Landroid/content/Context;

    .line 26
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/resilient/a;
    .locals 3

    .prologue
    .line 29
    const-class v1, Lcom/twitter/library/resilient/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/resilient/a;->a:Lcom/twitter/library/resilient/a;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/twitter/library/resilient/a;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/library/resilient/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/resilient/a;->a:Lcom/twitter/library/resilient/a;

    .line 31
    const-class v0, Lcom/twitter/library/resilient/a;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 34
    :cond_0
    sget-object v0, Lcom/twitter/library/resilient/a;->a:Lcom/twitter/library/resilient/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/library/resilient/a;->b(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 1

    .prologue
    .line 48
    instance-of v0, p1, Lcom/twitter/library/resilient/b;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/twitter/library/resilient/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/resilient/f;->a(Landroid/content/Context;)Lcom/twitter/library/resilient/f;

    move-result-object v0

    .line 50
    check-cast p1, Lcom/twitter/library/resilient/b;

    invoke-virtual {v0, p1}, Lcom/twitter/library/resilient/f;->b(Lcom/twitter/library/resilient/b;)V

    .line 52
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Landroid/os/Bundle;

    check-cast p2, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/resilient/a;->a(Landroid/os/Bundle;Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public synthetic b(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/library/resilient/a;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public b(Lcom/twitter/library/service/s;)V
    .locals 1

    .prologue
    .line 67
    instance-of v0, p1, Lcom/twitter/library/resilient/b;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/twitter/library/resilient/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/resilient/f;->a(Landroid/content/Context;)Lcom/twitter/library/resilient/f;

    move-result-object v0

    .line 69
    check-cast p1, Lcom/twitter/library/resilient/b;

    invoke-virtual {v0, p1}, Lcom/twitter/library/resilient/f;->a(Lcom/twitter/library/resilient/b;)V

    .line 71
    :cond_0
    return-void
.end method
