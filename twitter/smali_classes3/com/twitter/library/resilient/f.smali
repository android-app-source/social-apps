.class public Lcom/twitter/library/resilient/f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/resilient/f$a;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/library/resilient/f;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/library/resilient/e;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/resilient/c;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "Lcom/twitter/library/resilient/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/twitter/library/resilient/f;->b:Landroid/content/Context;

    .line 49
    invoke-static {}, Lcom/twitter/library/resilient/e;->c()Lcom/twitter/library/resilient/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/resilient/f;->c:Lcom/twitter/library/resilient/e;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/resilient/f;->d:Ljava/util/Map;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/resilient/f;->e:Ljava/util/Map;

    .line 52
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/resilient/f;)Lcom/twitter/library/resilient/e;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/library/resilient/f;->c:Lcom/twitter/library/resilient/e;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/resilient/f;
    .locals 3

    .prologue
    .line 55
    const-class v1, Lcom/twitter/library/resilient/f;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/resilient/f;->a:Lcom/twitter/library/resilient/f;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/twitter/library/resilient/f;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/library/resilient/f;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/resilient/f;->a:Lcom/twitter/library/resilient/f;

    .line 57
    const-class v0, Lcom/twitter/library/resilient/f;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 60
    :cond_0
    sget-object v0, Lcom/twitter/library/resilient/f;->a:Lcom/twitter/library/resilient/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/twitter/library/resilient/f;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/library/resilient/f;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/twitter/library/client/Session;Lcom/twitter/library/resilient/d;)Lcom/twitter/library/resilient/b;
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, Lcom/twitter/library/resilient/f;->d:Ljava/util/Map;

    iget-object v1, p2, Lcom/twitter/library/resilient/d;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/resilient/c;

    .line 163
    if-nez v0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No job builder registered for type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/twitter/library/resilient/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/twitter/library/resilient/f;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/resilient/c;->b(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/resilient/d;)Lcom/twitter/library/resilient/b;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 177
    :goto_0
    return-object v0

    .line 170
    :catch_0
    move-exception v0

    .line 173
    iget-object v1, p0, Lcom/twitter/library/resilient/f;->c:Lcom/twitter/library/resilient/e;

    iget-object v2, p0, Lcom/twitter/library/resilient/f;->b:Landroid/content/Context;

    invoke-virtual {v1, v2, p2}, Lcom/twitter/library/resilient/e;->a(Landroid/content/Context;Lcom/twitter/library/resilient/d;)Z

    .line 174
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 177
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/resilient/b;IJ)Lcom/twitter/library/resilient/d;
    .locals 11

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/library/resilient/f;->e:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/twitter/library/resilient/c;

    .line 91
    if-nez v2, :cond_0

    .line 92
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must first be registered as persistent job."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 98
    :try_start_0
    new-instance v0, Lcom/twitter/library/resilient/d;

    iget-object v2, v2, Lcom/twitter/library/resilient/c;->b:Ljava/lang/String;

    .line 103
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    check-cast v10, Ljava/lang/String;

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v10}, Lcom/twitter/library/resilient/d;-><init>(Ljava/lang/String;Ljava/lang/String;IJJJLjava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    return-object v0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 152
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/resilient/f$a;

    invoke-direct {v1, p0}, Lcom/twitter/library/resilient/f$a;-><init>(Lcom/twitter/library/resilient/f;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 153
    return-void
.end method

.method public a(Lcom/twitter/library/resilient/b;)V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/library/resilient/f;->c:Lcom/twitter/library/resilient/e;

    iget-object v1, p0, Lcom/twitter/library/resilient/f;->b:Landroid/content/Context;

    invoke-interface {p1}, Lcom/twitter/library/resilient/b;->j()Lcom/twitter/library/resilient/d;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/resilient/e;->a(Landroid/content/Context;Lcom/twitter/library/resilient/d;)Z

    .line 113
    return-void
.end method

.method public declared-synchronized a(Lcom/twitter/library/resilient/c;)V
    .locals 5

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/resilient/f;->d:Ljava/util/Map;

    iget-object v1, p1, Lcom/twitter/library/resilient/c;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/resilient/c;

    .line 127
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/library/resilient/c;->a:Ljava/lang/Class;

    iget-object v2, p1, Lcom/twitter/library/resilient/c;->a:Ljava/lang/Class;

    if-eq v1, v2, :cond_0

    .line 128
    const-string/jumbo v1, "Persistent job type %s can not be registered at key %s since it is already registered to %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/twitter/library/resilient/c;->a:Ljava/lang/Class;

    .line 132
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p1, Lcom/twitter/library/resilient/c;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 134
    invoke-virtual {v0}, Lcom/twitter/library/resilient/c;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 129
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 135
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 138
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/resilient/f;->d:Ljava/util/Map;

    iget-object v1, p1, Lcom/twitter/library/resilient/c;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v0, p0, Lcom/twitter/library/resilient/f;->e:Ljava/util/Map;

    iget-object v1, p1, Lcom/twitter/library/resilient/c;->a:Ljava/lang/Class;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    monitor-exit p0

    return-void
.end method

.method public b(Lcom/twitter/library/resilient/b;)V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/library/resilient/f;->c:Lcom/twitter/library/resilient/e;

    iget-object v1, p0, Lcom/twitter/library/resilient/f;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/resilient/e;->a(Landroid/content/Context;Lcom/twitter/library/resilient/b;)V

    .line 117
    return-void
.end method

.method protected c(Lcom/twitter/library/resilient/b;)Z
    .locals 3

    .prologue
    .line 184
    invoke-interface {p1}, Lcom/twitter/library/resilient/b;->j()Lcom/twitter/library/resilient/d;

    move-result-object v0

    .line 185
    if-nez v0, :cond_1

    .line 186
    const/4 v0, 0x0

    .line 192
    :cond_0
    :goto_0
    return v0

    .line 188
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/resilient/f;->c:Lcom/twitter/library/resilient/e;

    iget-object v2, p0, Lcom/twitter/library/resilient/f;->b:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/resilient/e;->a(Landroid/content/Context;Lcom/twitter/library/resilient/d;)Z

    move-result v0

    .line 189
    if-eqz v0, :cond_0

    .line 190
    iget-object v1, p0, Lcom/twitter/library/resilient/f;->b:Landroid/content/Context;

    invoke-interface {p1, v1}, Lcom/twitter/library/resilient/b;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
