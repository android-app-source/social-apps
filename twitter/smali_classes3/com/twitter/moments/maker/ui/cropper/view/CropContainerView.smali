.class public Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;
.super Landroid/widget/FrameLayout;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView$a;
    }
.end annotation


# instance fields
.field private final a:Lcmk;

.field private final b:Lcir;

.field private final c:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/view/ScaleGestureDetector;

.field private final e:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

.field private f:Landroid/view/View;

.field private g:Lcis;

.field private h:Lcit;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 75
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->c:Lrx/subjects/PublishSubject;

    .line 41
    new-instance v0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView$1;

    invoke-direct {v0, p0}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView$1;-><init>(Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;)V

    iput-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->e:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    .line 77
    invoke-virtual {p0}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 78
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 79
    sget v2, Lciq$b;->defaultCropAspectRatio:I

    invoke-virtual {v0, v2, v1, v8}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 80
    invoke-virtual {v1}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    .line 81
    sget v2, Lciq$b;->defaultCropWidth:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 82
    sget v3, Lciq$b;->defaultCornerRadius:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 83
    sget v4, Lciq$b;->defaultStrokeWidth:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 84
    sget v5, Lciq$a;->defaultStrokeColor:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 85
    sget v6, Lciq$a;->defaultOverlayColor:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 87
    sget-object v6, Lciq$c;->CropContainerView:[I

    const/4 v7, 0x0

    invoke-virtual {p1, p2, v6, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 88
    sget v7, Lciq$c;->CropContainerView_cropper_cropWidth:I

    .line 89
    invoke-virtual {v6, v7, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 90
    sget v7, Lciq$c;->CropContainerView_cropper_cropAspectRatio:I

    .line 91
    invoke-virtual {v6, v7, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    .line 92
    sget v7, Lciq$c;->CropContainerView_cropper_cornerRadius:I

    .line 93
    invoke-virtual {v6, v7, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 94
    sget v7, Lciq$c;->CropContainerView_cropper_strokeWidth:I

    .line 95
    invoke-virtual {v6, v7, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 96
    sget v7, Lciq$c;->CropContainerView_cropper_strokeColor:I

    invoke-virtual {v6, v7, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    .line 97
    sget v7, Lciq$c;->CropContainerView_cropper_overlayColor:I

    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 98
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 100
    new-instance v6, Lcir$a;

    invoke-direct {v6}, Lcir$a;-><init>()V

    .line 101
    invoke-virtual {v6, v2}, Lcir$a;->a(I)Lcir$a;

    move-result-object v2

    .line 102
    invoke-virtual {v2, v1}, Lcir$a;->a(F)Lcir$a;

    move-result-object v1

    int-to-float v2, v3

    .line 103
    invoke-virtual {v1, v2}, Lcir$a;->b(F)Lcir$a;

    move-result-object v1

    .line 104
    invoke-virtual {v1, v4}, Lcir$a;->b(I)Lcir$a;

    move-result-object v1

    .line 105
    invoke-virtual {v1, v5}, Lcir$a;->c(I)Lcir$a;

    move-result-object v1

    .line 106
    invoke-virtual {v1, v0}, Lcir$a;->d(I)Lcir$a;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lcir$a;->e()Lcir;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->b:Lcir;

    .line 109
    new-instance v0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView$a;-><init>(Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView$1;)V

    invoke-static {p0, v0}, Lcmk;->b(Landroid/view/ViewGroup;Lcmk$a;)Lcmk;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->a:Lcmk;

    .line 110
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->e:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->d:Landroid/view/ScaleGestureDetector;

    .line 111
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->d:Landroid/view/ScaleGestureDetector;

    invoke-static {v0, v8}, Landroid/support/v4/view/ScaleGestureDetectorCompat;->setQuickScaleEnabled(Ljava/lang/Object;Z)V

    .line 112
    return-void
.end method

.method static synthetic a(Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;)Lcit;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->h:Lcit;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;)Lcis;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->g:Lcis;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->h:Lcit;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->h:Lcit;

    invoke-virtual {v0}, Lcit;->b()V

    .line 148
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Lcom/twitter/util/math/c;ZZ)V
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->removeAllViews()V

    .line 124
    invoke-virtual {p0, p1}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->addView(Landroid/view/View;)V

    .line 125
    iput-object p1, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->f:Landroid/view/View;

    .line 126
    new-instance v0, Lcis;

    iget-object v1, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->b:Lcir;

    invoke-virtual {v1}, Lcir;->a()Landroid/graphics/RectF;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcis;-><init>(Landroid/view/View;Landroid/graphics/RectF;)V

    iput-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->g:Lcis;

    .line 127
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->b:Lcir;

    invoke-virtual {v0}, Lcir;->a()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->g:Lcis;

    invoke-static {p1, v0, v1}, Lcit;->a(Landroid/view/View;Landroid/graphics/RectF;Lcis;)Lcit;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->h:Lcit;

    .line 128
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->h:Lcit;

    invoke-virtual {v0, p2, p3, p4}, Lcit;->a(Lcom/twitter/util/math/c;ZZ)V

    .line 129
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->h:Lcit;

    invoke-virtual {v0}, Lcit;->c()Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->c:Lrx/subjects/PublishSubject;

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    .line 130
    return-void
.end method

.method public b()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->c:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 181
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 183
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->b:Lcir;

    invoke-virtual {p0}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->getHeight()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lcir;->a(Landroid/graphics/Canvas;II)V

    .line 184
    return-void
.end method

.method public getCurrentCropRect()Lcom/twitter/util/math/c;
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->h:Lcit;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcit;

    invoke-virtual {v0}, Lcit;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const/4 v0, 0x0

    .line 141
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->g:Lcis;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcis;

    invoke-virtual {v0}, Lcis;->a()Lcom/twitter/util/math/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/math/c;->c:Lcom/twitter/util/math/c;

    invoke-virtual {v0, v1}, Lcom/twitter/util/math/c;->a(Lcom/twitter/util/math/c;)Lcom/twitter/util/math/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 157
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    .line 158
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->a:Lcmk;

    invoke-virtual {v0}, Lcmk;->a()V

    .line 160
    const/4 v0, 0x0

    .line 162
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->a:Lcmk;

    invoke-virtual {v0, p1}, Lcmk;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 174
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->b:Lcir;

    invoke-virtual {v0, p1, p2}, Lcir;->a(II)V

    .line 177
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->a:Lcmk;

    invoke-virtual {v0, p1}, Lcmk;->b(Landroid/view/MotionEvent;)V

    .line 168
    iget-object v0, p0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->d:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 169
    const/4 v0, 0x1

    return v0
.end method
