.class public abstract Lcom/twitter/metrics/f;
.super Lcom/twitter/metrics/g;
.source "Twttr"


# instance fields
.field private a:Z

.field private final b:Lcom/twitter/metrics/h;

.field protected final f:J

.field protected final g:Ljava/lang/String;

.field protected h:J

.field protected i:Z

.field j:Z

.field k:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;)V
    .locals 6

    .prologue
    .line 55
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/metrics/f;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;Z)V

    .line 56
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;Z)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/twitter/metrics/g;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;)V

    .line 69
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/metrics/f;->p:Ljava/lang/String;

    .line 70
    iput-object p3, p0, Lcom/twitter/metrics/f;->g:Ljava/lang/String;

    .line 71
    invoke-virtual {p0}, Lcom/twitter/metrics/f;->y()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/metrics/f;->f:J

    .line 72
    iput-object p4, p0, Lcom/twitter/metrics/f;->b:Lcom/twitter/metrics/h;

    .line 73
    iput-boolean p5, p0, Lcom/twitter/metrics/f;->j:Z

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/metrics/f;->i:Z

    .line 76
    if-eqz p5, :cond_0

    if-eqz p4, :cond_0

    .line 77
    invoke-interface {p4}, Lcom/twitter/metrics/h;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->a(Landroid/content/SharedPreferences;)V

    .line 79
    :cond_0
    return-void
.end method

.method protected static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/content/SharedPreferences$Editor;)V
    .locals 4

    .prologue
    .line 214
    const-string/jumbo v0, "starttime"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/metrics/f;->u:J

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 215
    const-string/jumbo v0, "measuring"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/metrics/f;->a:Z

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 216
    const-string/jumbo v0, "duration"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/metrics/f;->t:J

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 217
    const-string/jumbo v0, "ready"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/metrics/f;->k:Z

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 218
    const-string/jumbo v0, "last_report"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/metrics/f;->h:J

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 219
    return-void
.end method

.method protected a(Landroid/content/SharedPreferences;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 237
    const-string/jumbo v0, "starttime"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/metrics/f;->u:J

    .line 238
    const-string/jumbo v0, "measuring"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/metrics/f;->a:Z

    .line 239
    const-string/jumbo v0, "duration"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/metrics/f;->t:J

    .line 240
    const-string/jumbo v0, "ready"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/metrics/f;->k:Z

    .line 241
    const-string/jumbo v0, "last_report"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/metrics/f;->h:J

    .line 242
    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 256
    return-void
.end method

.method protected b(Landroid/content/SharedPreferences$Editor;)V
    .locals 1

    .prologue
    .line 226
    const-string/jumbo v0, "starttime"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 227
    const-string/jumbo v0, "measuring"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 228
    const-string/jumbo v0, "duration"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 229
    const-string/jumbo v0, "ready"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 230
    const-string/jumbo v0, "last_report"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 231
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 199
    iput-boolean p1, p0, Lcom/twitter/metrics/f;->k:Z

    .line 200
    return-void
.end method

.method protected br_()V
    .locals 0

    .prologue
    .line 274
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 262
    return-void
.end method

.method protected d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/metrics/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized i()V
    .locals 2

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/metrics/f;->i:Z

    if-eqz v0, :cond_0

    .line 99
    const-string/jumbo v0, "MetricsManager"

    const-string/jumbo v1, "attempting to start a metric which has already been destroyed"

    invoke-static {v0, v1}, Lcqj;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/metrics/f;->i:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/metrics/f;->a:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/metrics/f;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 112
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 106
    :cond_2
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/twitter/metrics/f;->a:Z

    .line 109
    invoke-virtual {p0}, Lcom/twitter/metrics/f;->x()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/metrics/f;->u:J

    .line 111
    invoke-virtual {p0}, Lcom/twitter/metrics/f;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j()V
    .locals 6

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/metrics/f;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 127
    :goto_0
    monitor-exit p0

    return-void

    .line 123
    :cond_0
    :try_start_1
    iget-wide v0, p0, Lcom/twitter/metrics/f;->t:J

    invoke-virtual {p0}, Lcom/twitter/metrics/f;->x()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/metrics/f;->u:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/metrics/f;->t:J

    .line 125
    invoke-virtual {p0}, Lcom/twitter/metrics/f;->c()V

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/metrics/f;->a:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()V
    .locals 1

    .prologue
    .line 133
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/metrics/f;->s()V

    .line 134
    invoke-virtual {p0}, Lcom/twitter/metrics/f;->n()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    monitor-exit p0

    return-void

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/twitter/metrics/f;->a:Z

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/twitter/metrics/f;->i:Z

    return v0
.end method

.method protected final n()V
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/metrics/f;->i:Z

    .line 160
    iget-object v0, p0, Lcom/twitter/metrics/f;->b:Lcom/twitter/metrics/h;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/twitter/metrics/f;->b:Lcom/twitter/metrics/h;

    invoke-interface {v0, p0}, Lcom/twitter/metrics/h;->b(Lcom/twitter/metrics/f;)V

    .line 163
    :cond_0
    return-void
.end method

.method protected final o()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/metrics/f;->b:Lcom/twitter/metrics/h;

    if-eqz v0, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/twitter/metrics/f;->x()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/metrics/f;->h:J

    .line 168
    iget-object v0, p0, Lcom/twitter/metrics/f;->b:Lcom/twitter/metrics/h;

    invoke-interface {v0, p0}, Lcom/twitter/metrics/h;->a(Lcom/twitter/metrics/f;)V

    .line 170
    :cond_0
    return-void
.end method

.method protected final p()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 173
    iget-boolean v0, p0, Lcom/twitter/metrics/f;->a:Z

    if-eqz v0, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/twitter/metrics/f;->x()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/metrics/f;->u:J

    .line 178
    :goto_0
    iput-wide v2, p0, Lcom/twitter/metrics/f;->t:J

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/metrics/f;->k:Z

    .line 180
    invoke-virtual {p0}, Lcom/twitter/metrics/f;->br_()V

    .line 181
    return-void

    .line 176
    :cond_0
    iput-wide v2, p0, Lcom/twitter/metrics/f;->u:J

    goto :goto_0
.end method

.method protected final q()V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/twitter/metrics/f;->b:Lcom/twitter/metrics/h;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/twitter/metrics/f;->b:Lcom/twitter/metrics/h;

    invoke-interface {v0, p0}, Lcom/twitter/metrics/h;->c(Lcom/twitter/metrics/f;)V

    .line 187
    :cond_0
    return-void
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/twitter/metrics/f;->k:Z

    return v0
.end method

.method protected s()V
    .locals 0

    .prologue
    .line 268
    return-void
.end method
