.class public Lcom/twitter/metrics/n;
.super Lcom/twitter/metrics/f;
.source "Twttr"


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/metrics/f;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;)V

    .line 78
    return-void
.end method

.method public static b(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;
    .locals 6

    .prologue
    .line 50
    const-string/jumbo v0, "TimingMetric"

    invoke-static {v0, p0}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;Ljava/lang/String;)Lcom/twitter/metrics/n;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;Ljava/lang/String;)Lcom/twitter/metrics/n;
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p1, p5}, Lcom/twitter/metrics/j;->a(Ljava/lang/String;)Lcom/twitter/metrics/f;

    move-result-object v0

    .line 68
    if-nez v0, :cond_0

    .line 69
    new-instance v0, Lcom/twitter/metrics/n;

    invoke-direct {v0, p0, p4, p5, p1}, Lcom/twitter/metrics/n;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;)V

    invoke-virtual {p1, v0}, Lcom/twitter/metrics/j;->d(Lcom/twitter/metrics/f;)Lcom/twitter/metrics/f;

    move-result-object v0

    .line 70
    invoke-virtual {v0, p2, p3}, Lcom/twitter/metrics/f;->b(J)V

    .line 72
    :cond_0
    check-cast v0, Lcom/twitter/metrics/n;

    return-object v0
.end method

.method public static b(Ljava/lang/String;Lcom/twitter/metrics/j;Lcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;
    .locals 6

    .prologue
    .line 31
    const-wide/16 v2, 0x0

    const-string/jumbo v0, "TimingMetric"

    invoke-static {v0, p0}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;Ljava/lang/String;)Lcom/twitter/metrics/n;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected b()V
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/twitter/metrics/n;->y()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/metrics/n;->u:J

    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/n;->b(Z)V

    .line 84
    return-void
.end method

.method protected br_()V
    .locals 0

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/twitter/metrics/n;->n()V

    .line 97
    return-void
.end method

.method protected c()V
    .locals 4

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/twitter/metrics/n;->y()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/metrics/n;->u:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/metrics/n;->t:J

    .line 89
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/n;->b(Z)V

    .line 90
    invoke-virtual {p0}, Lcom/twitter/metrics/n;->o()V

    .line 91
    invoke-virtual {p0}, Lcom/twitter/metrics/n;->n()V

    .line 92
    return-void
.end method
