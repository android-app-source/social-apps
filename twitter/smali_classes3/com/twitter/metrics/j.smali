.class public Lcom/twitter/metrics/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/metrics/h;
.implements Lcom/twitter/metrics/k;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/metrics/f;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/twitter/metrics/f;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/SharedPreferences;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/metrics/k;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private final h:Lcom/twitter/metrics/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Lcom/twitter/metrics/j$1;

    invoke-direct {v0, p0}, Lcom/twitter/metrics/j$1;-><init>(Lcom/twitter/metrics/j;)V

    iput-object v0, p0, Lcom/twitter/metrics/j;->c:Ljava/util/Comparator;

    .line 60
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/metrics/j;->e:Ljava/util/Set;

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/metrics/j;->f:Z

    .line 62
    iput-boolean v1, p0, Lcom/twitter/metrics/j;->g:Z

    .line 80
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/metrics/j;->a:Ljava/util/Map;

    .line 81
    iput-object p1, p0, Lcom/twitter/metrics/j;->b:Landroid/content/Context;

    .line 82
    const-string/jumbo v0, "metrics"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/metrics/j;->d:Landroid/content/SharedPreferences;

    .line 83
    new-instance v0, Lcom/twitter/metrics/d;

    iget-object v1, p0, Lcom/twitter/metrics/j;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/metrics/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/metrics/j;->h:Lcom/twitter/metrics/d;

    .line 84
    return-void
.end method

.method public static declared-synchronized b()Lcom/twitter/metrics/j;
    .locals 2

    .prologue
    .line 68
    const-class v1, Lcom/twitter/metrics/j;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcot;->ak()Lcot;

    move-result-object v0

    check-cast v0, Lbzm;

    invoke-interface {v0}, Lbzm;->G()Lcom/twitter/metrics/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/twitter/metrics/j;->d:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/metrics/f;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/metrics/j;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/metrics/f;

    return-object v0
.end method

.method public a(Lcom/twitter/metrics/f;)V
    .locals 0

    .prologue
    .line 113
    invoke-virtual {p0, p1}, Lcom/twitter/metrics/j;->a(Lcom/twitter/metrics/g;)V

    .line 114
    return-void
.end method

.method public a(Lcom/twitter/metrics/g;)V
    .locals 2

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/twitter/metrics/j;->g:Z

    if-eqz v0, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/twitter/metrics/j;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/metrics/k;

    .line 202
    invoke-interface {v0, p1}, Lcom/twitter/metrics/k;->a(Lcom/twitter/metrics/g;)V

    goto :goto_1

    .line 204
    :cond_2
    instance-of v0, p1, Lcom/twitter/metrics/f;

    if-eqz v0, :cond_0

    .line 205
    check-cast p1, Lcom/twitter/metrics/f;

    invoke-virtual {p1}, Lcom/twitter/metrics/f;->p()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/metrics/k;)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/twitter/metrics/j;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 185
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/twitter/metrics/j;->f:Z

    .line 167
    if-eqz p1, :cond_0

    .line 168
    invoke-virtual {p0}, Lcom/twitter/metrics/j;->e()V

    .line 170
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/metrics/f;)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/metrics/j;->a:Ljava/util/Map;

    iget-object v1, p1, Lcom/twitter/metrics/f;->g:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    iget-boolean v0, p1, Lcom/twitter/metrics/f;->j:Z

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/twitter/metrics/j;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 121
    invoke-virtual {p1, v0}, Lcom/twitter/metrics/f;->b(Landroid/content/SharedPreferences$Editor;)V

    .line 122
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 124
    :cond_0
    return-void
.end method

.method public declared-synchronized b(Z)V
    .locals 1

    .prologue
    .line 255
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/twitter/metrics/j;->g:Z

    .line 256
    if-eqz p1, :cond_0

    .line 257
    invoke-virtual {p0}, Lcom/twitter/metrics/j;->g()V

    .line 258
    invoke-virtual {p0}, Lcom/twitter/metrics/j;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    :cond_0
    monitor-exit p0

    return-void

    .line 255
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/metrics/j;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 76
    return-void
.end method

.method public c(Lcom/twitter/metrics/f;)V
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p1, Lcom/twitter/metrics/f;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/metrics/j;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/metrics/j;->g:Z

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/twitter/metrics/j;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 138
    invoke-virtual {p1, v0}, Lcom/twitter/metrics/f;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 139
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 141
    :cond_0
    return-void
.end method

.method public d()Lcom/twitter/metrics/d;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/twitter/metrics/j;->h:Lcom/twitter/metrics/d;

    return-object v0
.end method

.method public d(Lcom/twitter/metrics/f;)Lcom/twitter/metrics/f;
    .locals 2

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/twitter/metrics/j;->g:Z

    if-eqz v0, :cond_0

    .line 102
    :goto_0
    return-object p1

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/twitter/metrics/j;->a:Ljava/util/Map;

    iget-object v1, p1, Lcom/twitter/metrics/f;->g:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/metrics/f;

    .line 98
    if-eqz v0, :cond_1

    move-object p1, v0

    .line 99
    goto :goto_0

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/twitter/metrics/j;->a:Ljava/util/Map;

    iget-object v1, p1, Lcom/twitter/metrics/f;->g:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/twitter/metrics/j;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/metrics/j;->g:Z

    if-eqz v0, :cond_1

    .line 157
    :cond_0
    return-void

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/twitter/metrics/j;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 152
    iget-object v2, p0, Lcom/twitter/metrics/j;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/metrics/f;

    .line 153
    if-eqz v0, :cond_2

    iget-boolean v2, v0, Lcom/twitter/metrics/f;->j:Z

    if-eqz v2, :cond_2

    .line 154
    invoke-virtual {p0, v0}, Lcom/twitter/metrics/j;->c(Lcom/twitter/metrics/f;)V

    goto :goto_0
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/metrics/j;->b:Landroid/content/Context;

    return-object v0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/metrics/j;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 248
    return-void
.end method
