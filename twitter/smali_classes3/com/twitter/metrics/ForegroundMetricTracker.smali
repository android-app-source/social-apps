.class public Lcom/twitter/metrics/ForegroundMetricTracker;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/metrics/ForegroundMetricTracker$a;,
        Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/metrics/ForegroundMetricTracker;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/metrics/f;",
            "Lcom/twitter/metrics/ForegroundMetricTracker$a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/util/android/b$a;

.field private final d:Lcom/twitter/util/android/b;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/twitter/util/android/b;)V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->b:Ljava/util/Map;

    .line 50
    new-instance v0, Lcom/twitter/metrics/ForegroundMetricTracker$1;

    invoke-direct {v0, p0}, Lcom/twitter/metrics/ForegroundMetricTracker$1;-><init>(Lcom/twitter/metrics/ForegroundMetricTracker;)V

    iput-object v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->c:Lcom/twitter/util/android/b$a;

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->e:Z

    .line 106
    iput-object p1, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->d:Lcom/twitter/util/android/b;

    .line 107
    return-void
.end method

.method public static declared-synchronized a()Lcom/twitter/metrics/ForegroundMetricTracker;
    .locals 3

    .prologue
    .line 98
    const-class v1, Lcom/twitter/metrics/ForegroundMetricTracker;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/metrics/ForegroundMetricTracker;->a:Lcom/twitter/metrics/ForegroundMetricTracker;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Lcom/twitter/metrics/ForegroundMetricTracker;

    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/metrics/ForegroundMetricTracker;-><init>(Lcom/twitter/util/android/b;)V

    sput-object v0, Lcom/twitter/metrics/ForegroundMetricTracker;->a:Lcom/twitter/metrics/ForegroundMetricTracker;

    .line 100
    const-class v0, Lcom/twitter/metrics/ForegroundMetricTracker;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 102
    :cond_0
    sget-object v0, Lcom/twitter/metrics/ForegroundMetricTracker;->a:Lcom/twitter/metrics/ForegroundMetricTracker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/twitter/metrics/ForegroundMetricTracker;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->b:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/metrics/f;)V
    .locals 3

    .prologue
    .line 137
    iget-object v1, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->b:Ljava/util/Map;

    monitor-enter v1

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->d:Lcom/twitter/util/android/b;

    iget-object v2, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->c:Lcom/twitter/util/android/b$a;

    invoke-virtual {v0, v2}, Lcom/twitter/util/android/b;->b(Lcom/twitter/util/android/b$a;)Z

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->e:Z

    .line 143
    :cond_0
    monitor-exit v1

    .line 144
    return-void

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/twitter/metrics/f;Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;)V
    .locals 3

    .prologue
    .line 122
    iget-object v1, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->b:Ljava/util/Map;

    monitor-enter v1

    .line 123
    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->e:Z

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->d:Lcom/twitter/util/android/b;

    iget-object v2, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->c:Lcom/twitter/util/android/b$a;

    invoke-virtual {v0, v2}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->e:Z

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker;->b:Ljava/util/Map;

    new-instance v2, Lcom/twitter/metrics/ForegroundMetricTracker$a;

    invoke-direct {v2, p2}, Lcom/twitter/metrics/ForegroundMetricTracker$a;-><init>(Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    monitor-exit v1

    .line 129
    return-void

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
