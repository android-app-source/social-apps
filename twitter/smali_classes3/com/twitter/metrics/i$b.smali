.class public Lcom/twitter/metrics/i$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/metrics/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I


# direct methods
.method public constructor <init>(IIIIII)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput p1, p0, Lcom/twitter/metrics/i$b;->a:I

    .line 114
    iput p2, p0, Lcom/twitter/metrics/i$b;->b:I

    .line 115
    iput p3, p0, Lcom/twitter/metrics/i$b;->c:I

    .line 117
    iput p4, p0, Lcom/twitter/metrics/i$b;->d:I

    .line 118
    iput p5, p0, Lcom/twitter/metrics/i$b;->e:I

    .line 119
    iput p6, p0, Lcom/twitter/metrics/i$b;->f:I

    .line 120
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 124
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v0

    .line 125
    :cond_1
    check-cast p1, Lcom/twitter/metrics/i$b;

    .line 126
    iget v1, p0, Lcom/twitter/metrics/i$b;->a:I

    iget v2, p1, Lcom/twitter/metrics/i$b;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/metrics/i$b;->b:I

    iget v2, p1, Lcom/twitter/metrics/i$b;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/metrics/i$b;->c:I

    iget v2, p1, Lcom/twitter/metrics/i$b;->c:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/metrics/i$b;->d:I

    iget v2, p1, Lcom/twitter/metrics/i$b;->d:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/metrics/i$b;->e:I

    iget v2, p1, Lcom/twitter/metrics/i$b;->e:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/metrics/i$b;->f:I

    iget v2, p1, Lcom/twitter/metrics/i$b;->f:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 136
    iget v0, p0, Lcom/twitter/metrics/i$b;->a:I

    .line 137
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/metrics/i$b;->b:I

    add-int/2addr v0, v1

    .line 138
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/metrics/i$b;->c:I

    add-int/2addr v0, v1

    .line 139
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/metrics/i$b;->d:I

    add-int/2addr v0, v1

    .line 140
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/metrics/i$b;->e:I

    add-int/2addr v0, v1

    .line 141
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/metrics/i$b;->f:I

    add-int/2addr v0, v1

    .line 142
    return v0
.end method
