.class public Lcom/twitter/metrics/l;
.super Lcom/twitter/metrics/m;
.source "Twttr"


# instance fields
.field protected a:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZI)V
    .locals 0

    .prologue
    .line 22
    invoke-direct/range {p0 .. p7}, Lcom/twitter/metrics/m;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZI)V

    .line 23
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/l;
    .locals 8

    .prologue
    .line 28
    const-string/jumbo v0, "PeakValueMetric"

    invoke-static {v0, p0}, Lcom/twitter/metrics/l;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/metrics/j;->a(Ljava/lang/String;)Lcom/twitter/metrics/f;

    move-result-object v0

    .line 29
    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/twitter/metrics/l;

    invoke-virtual {p2}, Lcom/twitter/metrics/j;->f()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "PeakValueMetric"

    .line 31
    invoke-static {v2, p0}, Lcom/twitter/metrics/l;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v2, p0

    move-object v3, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/metrics/l;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZI)V

    .line 30
    invoke-virtual {p2, v0}, Lcom/twitter/metrics/j;->d(Lcom/twitter/metrics/f;)Lcom/twitter/metrics/f;

    move-result-object v0

    .line 33
    :cond_0
    check-cast v0, Lcom/twitter/metrics/l;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 37
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/metrics/l;->l()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 45
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 40
    :cond_1
    :try_start_1
    iget-wide v0, p0, Lcom/twitter/metrics/l;->a:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 41
    iput-wide p1, p0, Lcom/twitter/metrics/l;->a:J

    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/l;->b(Z)V

    .line 43
    invoke-virtual {p0}, Lcom/twitter/metrics/l;->q()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(Landroid/content/SharedPreferences$Editor;)V
    .locals 4

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 63
    const-string/jumbo v0, "peak"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/l;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/metrics/l;->a:J

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    monitor-exit p0

    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Landroid/content/SharedPreferences;)V
    .locals 4

    .prologue
    .line 74
    const-string/jumbo v0, "peak"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/l;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/metrics/l;->a:J

    .line 75
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->a(Landroid/content/SharedPreferences;)V

    .line 76
    return-void
.end method

.method protected b(Landroid/content/SharedPreferences$Editor;)V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->b(Landroid/content/SharedPreferences$Editor;)V

    .line 69
    const-string/jumbo v0, "peak"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/l;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 70
    return-void
.end method

.method protected declared-synchronized br_()V
    .locals 2

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/twitter/metrics/m;->br_()V

    .line 56
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/metrics/l;->a:J

    .line 57
    invoke-virtual {p0}, Lcom/twitter/metrics/l;->q()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    monitor-exit p0

    return-void

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 50
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/twitter/metrics/l;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
