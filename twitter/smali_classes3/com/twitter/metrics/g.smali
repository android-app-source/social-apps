.class public Lcom/twitter/metrics/g;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/metrics/g$a;,
        Lcom/twitter/metrics/g$b;
    }
.end annotation


# static fields
.field public static final l:Lcom/twitter/metrics/g$b;

.field public static final m:Lcom/twitter/metrics/g$b;

.field public static final n:Lcom/twitter/metrics/g$b;


# instance fields
.field protected o:Ljava/lang/String;

.field protected p:Ljava/lang/String;

.field protected q:Ljava/lang/String;

.field protected r:Lcom/twitter/metrics/g$b;

.field protected s:J

.field protected t:J

.field protected u:J

.field protected v:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/metrics/g$1;

    invoke-direct {v0}, Lcom/twitter/metrics/g$1;-><init>()V

    sput-object v0, Lcom/twitter/metrics/g;->l:Lcom/twitter/metrics/g$b;

    .line 38
    new-instance v0, Lcom/twitter/metrics/g$2;

    invoke-direct {v0}, Lcom/twitter/metrics/g$2;-><init>()V

    sput-object v0, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    .line 56
    new-instance v0, Lcom/twitter/metrics/g$3;

    invoke-direct {v0}, Lcom/twitter/metrics/g$3;-><init>()V

    sput-object v0, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;)V
    .locals 2

    .prologue
    .line 101
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/twitter/metrics/g;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;J)V

    .line 102
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;J)V
    .locals 3

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/twitter/metrics/g;->o:Ljava/lang/String;

    .line 115
    iput-object p2, p0, Lcom/twitter/metrics/g;->r:Lcom/twitter/metrics/g$b;

    .line 116
    iput-wide p3, p0, Lcom/twitter/metrics/g;->t:J

    .line 117
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/metrics/g;->u:J

    .line 118
    const-string/jumbo v0, "PerfMetric"

    iput-object v0, p0, Lcom/twitter/metrics/g;->p:Ljava/lang/String;

    .line 119
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/metrics/g;->o:Ljava/lang/String;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 175
    iput-wide p1, p0, Lcom/twitter/metrics/g;->s:J

    .line 176
    return-void
.end method

.method public d()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/metrics/g;->v:Ljava/lang/Long;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/twitter/metrics/g;->q:Ljava/lang/String;

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/twitter/metrics/g;->p:Ljava/lang/String;

    .line 180
    return-void
.end method

.method public declared-synchronized i()V
    .locals 2

    .prologue
    .line 122
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/metrics/g;->y()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/metrics/g;->u:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    monitor-exit p0

    return-void

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized j()V
    .locals 4

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/metrics/g;->y()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/metrics/g;->u:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/metrics/g;->t:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    monitor-exit p0

    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 148
    iget-wide v0, p0, Lcom/twitter/metrics/g;->t:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/twitter/metrics/g;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " duration="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/metrics/g;->t:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 186
    iget-object v1, p0, Lcom/twitter/metrics/g;->v:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 187
    const-string/jumbo v1, " value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/metrics/g;->v:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 189
    :cond_0
    iget-object v1, p0, Lcom/twitter/metrics/g;->q:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 190
    const-string/jumbo v1, " metadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/metrics/g;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/twitter/metrics/g;->p:Ljava/lang/String;

    return-object v0
.end method

.method public v()Lcom/twitter/metrics/g$b;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/twitter/metrics/g;->r:Lcom/twitter/metrics/g$b;

    return-object v0
.end method

.method public w()J
    .locals 2

    .prologue
    .line 171
    iget-wide v0, p0, Lcom/twitter/metrics/g;->s:J

    return-wide v0
.end method

.method protected x()J
    .locals 2

    .prologue
    .line 196
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    invoke-interface {v0}, Lcqt;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method protected y()J
    .locals 2

    .prologue
    .line 200
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    return-wide v0
.end method
