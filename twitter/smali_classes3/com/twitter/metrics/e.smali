.class public Lcom/twitter/metrics/e;
.super Lcom/twitter/metrics/n;
.source "Twttr"


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/metrics/n;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;)V

    .line 39
    invoke-static {}, Lcom/twitter/metrics/ForegroundMetricTracker;->a()Lcom/twitter/metrics/ForegroundMetricTracker;

    move-result-object v0

    sget-object v1, Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;->a:Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/metrics/ForegroundMetricTracker;->a(Lcom/twitter/metrics/f;Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;)V

    .line 41
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;)Lcom/twitter/metrics/e;
    .locals 6

    .prologue
    .line 21
    const-string/jumbo v0, "ForegroundTimingMetric"

    invoke-static {v0, p0}, Lcom/twitter/metrics/e;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/metrics/e;->a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;Ljava/lang/String;)Lcom/twitter/metrics/e;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;Ljava/lang/String;)Lcom/twitter/metrics/e;
    .locals 2

    .prologue
    .line 27
    invoke-virtual {p1, p5}, Lcom/twitter/metrics/j;->a(Ljava/lang/String;)Lcom/twitter/metrics/f;

    move-result-object v0

    .line 28
    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/twitter/metrics/e;

    invoke-direct {v0, p0, p4, p5, p1}, Lcom/twitter/metrics/e;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;)V

    .line 30
    invoke-virtual {p1, v0}, Lcom/twitter/metrics/j;->d(Lcom/twitter/metrics/f;)Lcom/twitter/metrics/f;

    move-result-object v0

    .line 31
    invoke-virtual {v0, p2, p3}, Lcom/twitter/metrics/f;->b(J)V

    .line 33
    :cond_0
    check-cast v0, Lcom/twitter/metrics/e;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/metrics/j;Lcom/twitter/metrics/g$b;)Lcom/twitter/metrics/e;
    .locals 6

    .prologue
    .line 15
    const-wide/16 v2, 0x0

    const-string/jumbo v0, "ForegroundTimingMetric"

    invoke-static {v0, p0}, Lcom/twitter/metrics/e;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/metrics/e;->a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;Ljava/lang/String;)Lcom/twitter/metrics/e;

    move-result-object v0

    return-object v0
.end method
