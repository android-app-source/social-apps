.class public Lcom/twitter/metrics/c;
.super Lcom/twitter/metrics/m;
.source "Twttr"


# instance fields
.field private a:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;I)V
    .locals 10

    .prologue
    .line 83
    const-wide/16 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move/from16 v7, p6

    invoke-direct/range {v1 .. v9}, Lcom/twitter/metrics/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;IJ)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;IJ)V
    .locals 11

    .prologue
    .line 94
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v7, p6

    move-wide/from16 v8, p7

    invoke-direct/range {v0 .. v9}, Lcom/twitter/metrics/m;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZIJ)V

    .line 95
    iget-object v0, p0, Lcom/twitter/metrics/c;->a:Ljava/util/concurrent/atomic/AtomicLong;

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/twitter/metrics/c;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 98
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;I)Lcom/twitter/metrics/c;
    .locals 8

    .prologue
    .line 46
    const-string/jumbo v0, "CounterMetric"

    invoke-static {v0, p0}, Lcom/twitter/metrics/c;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    invoke-static/range {v0 .. v6}, Lcom/twitter/metrics/c;->a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;ILjava/lang/String;)Lcom/twitter/metrics/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;ILjava/lang/String;)Lcom/twitter/metrics/c;
    .locals 12

    .prologue
    .line 67
    move-object/from16 v0, p6

    invoke-virtual {p1, v0}, Lcom/twitter/metrics/j;->a(Ljava/lang/String;)Lcom/twitter/metrics/f;

    move-result-object v2

    .line 68
    if-nez v2, :cond_0

    .line 69
    new-instance v3, Lcom/twitter/metrics/c;

    invoke-virtual {p1}, Lcom/twitter/metrics/j;->f()Landroid/content/Context;

    move-result-object v4

    move-object v5, p0

    move-object/from16 v6, p4

    move-object/from16 v7, p6

    move-object v8, p1

    move/from16 v9, p5

    move-wide v10, p2

    invoke-direct/range {v3 .. v11}, Lcom/twitter/metrics/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;IJ)V

    invoke-virtual {p1, v3}, Lcom/twitter/metrics/j;->d(Lcom/twitter/metrics/f;)Lcom/twitter/metrics/f;

    move-result-object v2

    .line 72
    :cond_0
    check-cast v2, Lcom/twitter/metrics/c;

    return-object v2
.end method

.method private d(J)V
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/twitter/metrics/c;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/twitter/metrics/c;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 141
    invoke-virtual {p0}, Lcom/twitter/metrics/c;->q()V

    goto :goto_0
.end method


# virtual methods
.method public a(J)V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Lcom/twitter/metrics/c;->d(J)V

    .line 114
    return-void
.end method

.method protected a(Landroid/content/SharedPreferences$Editor;)V
    .locals 4

    .prologue
    .line 160
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 161
    const-string/jumbo v0, "usage"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/c;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/metrics/c;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 162
    return-void
.end method

.method protected a(Landroid/content/SharedPreferences;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 172
    iget-object v0, p0, Lcom/twitter/metrics/c;->a:Ljava/util/concurrent/atomic/AtomicLong;

    if-nez v0, :cond_0

    .line 173
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-string/jumbo v1, "usage"

    invoke-virtual {p0, v1}, Lcom/twitter/metrics/c;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/twitter/metrics/c;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 177
    :goto_0
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->a(Landroid/content/SharedPreferences;)V

    .line 178
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/twitter/metrics/c;->a:Ljava/util/concurrent/atomic/AtomicLong;

    const-string/jumbo v1, "usage"

    invoke-virtual {p0, v1}, Lcom/twitter/metrics/c;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/c;->b(Z)V

    .line 147
    return-void
.end method

.method protected b(Landroid/content/SharedPreferences$Editor;)V
    .locals 1

    .prologue
    .line 166
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->b(Landroid/content/SharedPreferences$Editor;)V

    .line 167
    const-string/jumbo v0, "usage"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/c;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 168
    return-void
.end method

.method protected br_()V
    .locals 4

    .prologue
    .line 151
    iget-object v0, p0, Lcom/twitter/metrics/c;->a:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 152
    invoke-virtual {p0}, Lcom/twitter/metrics/c;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/c;->b(Z)V

    .line 155
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/metrics/c;->q()V

    .line 156
    return-void
.end method

.method public d()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/metrics/c;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 104
    const-wide/16 v0, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/metrics/c;->d(J)V

    .line 105
    return-void
.end method
