.class public Lcom/twitter/metrics/a;
.super Lcom/twitter/metrics/m;
.source "Twttr"


# instance fields
.field protected a:J

.field protected b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZI)V
    .locals 0

    .prologue
    .line 25
    invoke-direct/range {p0 .. p7}, Lcom/twitter/metrics/m;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZI)V

    .line 26
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/a;
    .locals 8

    .prologue
    .line 31
    const-string/jumbo v0, "AverageValueMetric"

    invoke-static {v0, p0}, Lcom/twitter/metrics/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/metrics/j;->a(Ljava/lang/String;)Lcom/twitter/metrics/f;

    move-result-object v0

    .line 32
    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/twitter/metrics/a;

    invoke-virtual {p2}, Lcom/twitter/metrics/j;->f()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "AverageValueMetric"

    .line 34
    invoke-static {v2, p0}, Lcom/twitter/metrics/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v2, p0

    move-object v3, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/metrics/a;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZI)V

    .line 33
    invoke-virtual {p2, v0}, Lcom/twitter/metrics/j;->d(Lcom/twitter/metrics/f;)Lcom/twitter/metrics/f;

    move-result-object v0

    .line 36
    :cond_0
    check-cast v0, Lcom/twitter/metrics/a;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/metrics/a;->l()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 48
    :goto_0
    monitor-exit p0

    return-void

    .line 44
    :cond_0
    :try_start_1
    iget-wide v0, p0, Lcom/twitter/metrics/a;->a:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/twitter/metrics/a;->a:J

    .line 45
    iget v0, p0, Lcom/twitter/metrics/a;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/metrics/a;->b:I

    .line 46
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/a;->b(Z)V

    .line 47
    invoke-virtual {p0}, Lcom/twitter/metrics/a;->q()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(Landroid/content/SharedPreferences$Editor;)V
    .locals 4

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 67
    const-string/jumbo v0, "total"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/a;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/metrics/a;->a:J

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 68
    const-string/jumbo v0, "count"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/a;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/twitter/metrics/a;->b:I

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    monitor-exit p0

    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Landroid/content/SharedPreferences;)V
    .locals 4

    .prologue
    .line 80
    const-string/jumbo v0, "total"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/a;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/metrics/a;->a:J

    .line 81
    const-string/jumbo v0, "count"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/a;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/metrics/a;->b:I

    .line 82
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->a(Landroid/content/SharedPreferences;)V

    .line 83
    return-void
.end method

.method protected b(Landroid/content/SharedPreferences$Editor;)V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->b(Landroid/content/SharedPreferences$Editor;)V

    .line 74
    const-string/jumbo v0, "total"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/a;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 75
    const-string/jumbo v0, "count"

    invoke-virtual {p0, v0}, Lcom/twitter/metrics/a;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 76
    return-void
.end method

.method protected declared-synchronized br_()V
    .locals 2

    .prologue
    .line 58
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/twitter/metrics/m;->br_()V

    .line 59
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/metrics/a;->a:J

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/metrics/a;->b:I

    .line 61
    invoke-virtual {p0}, Lcom/twitter/metrics/a;->q()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    monitor-exit p0

    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()Ljava/lang/Long;
    .locals 4

    .prologue
    .line 53
    iget v0, p0, Lcom/twitter/metrics/a;->b:I

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/metrics/a;->a:J

    iget v2, p0, Lcom/twitter/metrics/a;->b:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
