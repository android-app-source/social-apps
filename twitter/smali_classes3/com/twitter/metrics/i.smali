.class public Lcom/twitter/metrics/i;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/metrics/i$a;,
        Lcom/twitter/metrics/i$b;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;

.field private static final b:Lcom/twitter/metrics/i$b;

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/metrics/i$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private static d:Lcom/twitter/metrics/i$b;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v1, 0x64

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/twitter/metrics/i;->a:Ljava/lang/Object;

    .line 34
    new-instance v0, Lcom/twitter/metrics/i$b;

    const/16 v3, 0x2710

    const v4, 0x927c0

    const v5, 0x36ee80

    const v6, 0x5265c00

    move v2, v1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/metrics/i$b;-><init>(IIIIII)V

    sput-object v0, Lcom/twitter/metrics/i;->b:Lcom/twitter/metrics/i$b;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/twitter/metrics/i;->c:Ljava/util/List;

    .line 45
    sget-object v0, Lcom/twitter/metrics/i;->b:Lcom/twitter/metrics/i$b;

    sput-object v0, Lcom/twitter/metrics/i;->d:Lcom/twitter/metrics/i$b;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/twitter/metrics/i$b;
    .locals 2

    .prologue
    .line 49
    sget-object v1, Lcom/twitter/metrics/i;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 50
    :try_start_0
    sget-object v0, Lcom/twitter/metrics/i;->d:Lcom/twitter/metrics/i$b;

    monitor-exit v1

    return-object v0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Lcom/twitter/metrics/i$a;)V
    .locals 3

    .prologue
    .line 82
    sget-object v1, Lcom/twitter/metrics/i;->c:Ljava/util/List;

    monitor-enter v1

    .line 83
    :try_start_0
    sget-object v0, Lcom/twitter/metrics/i;->c:Ljava/util/List;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    const-class v0, Lcom/twitter/metrics/i;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 85
    monitor-exit v1

    .line 86
    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Lcom/twitter/metrics/i$b;)V
    .locals 3

    .prologue
    .line 55
    sget-object v1, Lcom/twitter/metrics/i;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 56
    :try_start_0
    sget-object v0, Lcom/twitter/metrics/i;->d:Lcom/twitter/metrics/i$b;

    invoke-virtual {v0, p0}, Lcom/twitter/metrics/i$b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    monitor-exit v1

    .line 73
    :goto_0
    return-void

    .line 59
    :cond_0
    sput-object p0, Lcom/twitter/metrics/i;->d:Lcom/twitter/metrics/i$b;

    .line 60
    const-class v0, Lcom/twitter/metrics/i;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 61
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 62
    sget-object v1, Lcom/twitter/metrics/i;->c:Ljava/util/List;

    monitor-enter v1

    .line 63
    :try_start_1
    sget-object v0, Lcom/twitter/metrics/i;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 64
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/metrics/i$a;

    .line 66
    if-nez v0, :cond_1

    .line 67
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 61
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 69
    :cond_1
    :try_start_3
    invoke-interface {v0}, Lcom/twitter/metrics/i$a;->aV_()V

    goto :goto_1

    .line 72
    :cond_2
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
