.class Lcom/twitter/metrics/ForegroundMetricTracker$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/android/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/metrics/ForegroundMetricTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/metrics/ForegroundMetricTracker;


# direct methods
.method constructor <init>(Lcom/twitter/metrics/ForegroundMetricTracker;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/twitter/metrics/ForegroundMetricTracker$1;->a:Lcom/twitter/metrics/ForegroundMetricTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker$1;->a:Lcom/twitter/metrics/ForegroundMetricTracker;

    invoke-static {v0}, Lcom/twitter/metrics/ForegroundMetricTracker;->a(Lcom/twitter/metrics/ForegroundMetricTracker;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    .line 56
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    iget-object v2, p0, Lcom/twitter/metrics/ForegroundMetricTracker$1;->a:Lcom/twitter/metrics/ForegroundMetricTracker;

    invoke-static {v2}, Lcom/twitter/metrics/ForegroundMetricTracker;->a(Lcom/twitter/metrics/ForegroundMetricTracker;)Ljava/util/Map;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 57
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 59
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/metrics/f;

    .line 60
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/metrics/ForegroundMetricTracker$a;

    .line 61
    iget-object v3, v0, Lcom/twitter/metrics/ForegroundMetricTracker$a;->a:Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;

    sget-object v4, Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;->a:Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;

    if-ne v3, v4, :cond_0

    .line 62
    invoke-virtual {v1}, Lcom/twitter/metrics/f;->k()V

    .line 63
    iget-object v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker$1;->a:Lcom/twitter/metrics/ForegroundMetricTracker;

    invoke-virtual {v0, v1}, Lcom/twitter/metrics/ForegroundMetricTracker;->a(Lcom/twitter/metrics/f;)V

    goto :goto_0

    .line 57
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 65
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/metrics/f;->l()Z

    move-result v3

    iput-boolean v3, v0, Lcom/twitter/metrics/ForegroundMetricTracker$a;->b:Z

    .line 66
    invoke-virtual {v1}, Lcom/twitter/metrics/f;->j()V

    goto :goto_0

    .line 69
    :cond_1
    return-void
.end method

.method public b(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker$1;->a:Lcom/twitter/metrics/ForegroundMetricTracker;

    invoke-static {v0}, Lcom/twitter/metrics/ForegroundMetricTracker;->a(Lcom/twitter/metrics/ForegroundMetricTracker;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    .line 75
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    iget-object v2, p0, Lcom/twitter/metrics/ForegroundMetricTracker$1;->a:Lcom/twitter/metrics/ForegroundMetricTracker;

    invoke-static {v2}, Lcom/twitter/metrics/ForegroundMetricTracker;->a(Lcom/twitter/metrics/ForegroundMetricTracker;)Ljava/util/Map;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 76
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 79
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/metrics/f;

    .line 80
    invoke-virtual {v1}, Lcom/twitter/metrics/f;->m()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 81
    iget-object v0, p0, Lcom/twitter/metrics/ForegroundMetricTracker$1;->a:Lcom/twitter/metrics/ForegroundMetricTracker;

    invoke-virtual {v0, v1}, Lcom/twitter/metrics/ForegroundMetricTracker;->a(Lcom/twitter/metrics/f;)V

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 84
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/metrics/ForegroundMetricTracker$a;

    .line 85
    iget-object v3, v0, Lcom/twitter/metrics/ForegroundMetricTracker$a;->a:Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;

    sget-object v4, Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;->c:Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;

    if-ne v3, v4, :cond_0

    iget-boolean v0, v0, Lcom/twitter/metrics/ForegroundMetricTracker$a;->b:Z

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {v1}, Lcom/twitter/metrics/f;->i()V

    goto :goto_0

    .line 89
    :cond_2
    return-void
.end method
