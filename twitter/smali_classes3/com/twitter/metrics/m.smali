.class public abstract Lcom/twitter/metrics/m;
.super Lcom/twitter/metrics/f;
.source "Twttr"

# interfaces
.implements Lcom/twitter/metrics/i$a;


# instance fields
.field private final a:I

.field private final b:Landroid/os/Handler;

.field private final c:Ljava/lang/Runnable;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZI)V
    .locals 10

    .prologue
    .line 65
    const-wide/16 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v9}, Lcom/twitter/metrics/m;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZIJ)V

    .line 66
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZIJ)V
    .locals 6

    .prologue
    .line 71
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/twitter/metrics/f;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;Z)V

    .line 53
    new-instance v0, Lcom/twitter/metrics/m$1;

    invoke-direct {v0, p0}, Lcom/twitter/metrics/m$1;-><init>(Lcom/twitter/metrics/m;)V

    iput-object v0, p0, Lcom/twitter/metrics/m;->c:Ljava/lang/Runnable;

    .line 72
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/metrics/m;->b:Landroid/os/Handler;

    .line 73
    iput p7, p0, Lcom/twitter/metrics/m;->a:I

    .line 74
    iget v0, p0, Lcom/twitter/metrics/m;->a:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/metrics/m;->a:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_1

    .line 75
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Invalid reporting interval, please see PeriodicMetric for valid intervals."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_1
    iput-wide p8, p0, Lcom/twitter/metrics/m;->s:J

    .line 79
    invoke-static {p0}, Lcom/twitter/metrics/i;->a(Lcom/twitter/metrics/i$a;)V

    .line 80
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->A()V

    .line 81
    return-void
.end method

.method static synthetic a(Lcom/twitter/metrics/m;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/twitter/metrics/m;->b:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected A()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 109
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->B()V

    .line 110
    iget-boolean v0, p0, Lcom/twitter/metrics/m;->i:Z

    if-eqz v0, :cond_0

    .line 159
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->z()I

    move-result v0

    int-to-long v4, v0

    .line 115
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->x()J

    move-result-wide v2

    .line 116
    iget-wide v0, p0, Lcom/twitter/metrics/m;->h:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_2

    move-wide v0, v2

    :goto_1
    add-long/2addr v0, v4

    .line 121
    iget-wide v6, p0, Lcom/twitter/metrics/m;->h:J

    cmp-long v6, v6, v2

    if-gtz v6, :cond_1

    sub-long v6, v2, v0

    const-wide/16 v8, 0x7

    mul-long/2addr v8, v4

    cmp-long v6, v6, v8

    if-ltz v6, :cond_3

    .line 123
    :cond_1
    iput-wide v10, p0, Lcom/twitter/metrics/m;->h:J

    .line 124
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->p()V

    .line 125
    invoke-virtual {p0, v4, v5}, Lcom/twitter/metrics/m;->c(J)V

    goto :goto_0

    .line 116
    :cond_2
    iget-wide v0, p0, Lcom/twitter/metrics/m;->h:J

    goto :goto_1

    .line 131
    :cond_3
    iget-wide v6, p0, Lcom/twitter/metrics/m;->h:J

    cmp-long v6, v6, v10

    if-nez v6, :cond_4

    .line 132
    const-wide/16 v6, 0x1

    sub-long v6, v2, v6

    iput-wide v6, p0, Lcom/twitter/metrics/m;->h:J

    .line 135
    :cond_4
    cmp-long v6, v0, v2

    if-lez v6, :cond_5

    .line 136
    sub-long/2addr v0, v2

    .line 157
    :goto_2
    sub-long/2addr v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/twitter/metrics/m;->u:J

    .line 158
    invoke-virtual {p0, v0, v1}, Lcom/twitter/metrics/m;->c(J)V

    goto :goto_0

    .line 138
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->r()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 142
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->l()Z

    move-result v6

    .line 143
    if-eqz v6, :cond_6

    .line 146
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->j()V

    .line 148
    :cond_6
    iput-wide v4, p0, Lcom/twitter/metrics/m;->t:J

    .line 149
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->o()V

    .line 150
    if-eqz v6, :cond_7

    .line 151
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->i()V

    .line 154
    :cond_7
    sub-long v0, v2, v0

    rem-long/2addr v0, v4

    sub-long v0, v4, v0

    goto :goto_2
.end method

.method protected B()V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/metrics/m;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/metrics/m;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 184
    return-void
.end method

.method public aV_()V
    .locals 0

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->A()V

    .line 180
    return-void
.end method

.method protected c(J)V
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/twitter/metrics/m;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/metrics/m;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 188
    return-void
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    return v0
.end method

.method protected s()V
    .locals 0

    .prologue
    .line 104
    invoke-super {p0}, Lcom/twitter/metrics/f;->s()V

    .line 105
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->B()V

    .line 106
    return-void
.end method

.method public t()J
    .locals 4

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/twitter/metrics/m;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iget-wide v0, p0, Lcom/twitter/metrics/m;->t:J

    .line 168
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/twitter/metrics/m;->t:J

    invoke-virtual {p0}, Lcom/twitter/metrics/m;->x()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/twitter/metrics/m;->u:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method protected z()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/twitter/metrics/m;->a:I

    packed-switch v0, :pswitch_data_0

    .line 95
    invoke-static {}, Lcom/twitter/metrics/i;->a()Lcom/twitter/metrics/i$b;

    move-result-object v0

    iget v0, v0, Lcom/twitter/metrics/i$b;->f:I

    .line 99
    :goto_0
    return v0

    .line 87
    :pswitch_0
    invoke-static {}, Lcom/twitter/metrics/i;->a()Lcom/twitter/metrics/i$b;

    move-result-object v0

    iget v0, v0, Lcom/twitter/metrics/i$b;->d:I

    goto :goto_0

    .line 91
    :pswitch_1
    invoke-static {}, Lcom/twitter/metrics/i;->a()Lcom/twitter/metrics/i$b;

    move-result-object v0

    iget v0, v0, Lcom/twitter/metrics/i$b;->e:I

    goto :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
