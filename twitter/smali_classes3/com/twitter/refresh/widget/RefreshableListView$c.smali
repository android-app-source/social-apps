.class Lcom/twitter/refresh/widget/RefreshableListView$c;
.super Landroid/widget/HeaderViewListAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/refresh/widget/RefreshableListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/refresh/widget/RefreshableListView;

.field private final b:Lcom/twitter/refresh/widget/RefreshableListView$a;

.field private c:Z

.field private final d:Landroid/widget/ListAdapter;

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/refresh/widget/RefreshableListView;Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;Lcom/twitter/refresh/widget/RefreshableListView$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;",
            "Landroid/widget/ListAdapter;",
            "Lcom/twitter/refresh/widget/RefreshableListView$a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1021
    iput-object p1, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    .line 1022
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/HeaderViewListAdapter;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;)V

    .line 1023
    iput-object p4, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->d:Landroid/widget/ListAdapter;

    .line 1024
    iput-object p2, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->e:Ljava/util/ArrayList;

    .line 1025
    iput-object p3, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->f:Ljava/util/ArrayList;

    .line 1026
    iput-object p5, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->b:Lcom/twitter/refresh/widget/RefreshableListView$a;

    .line 1027
    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView$c;->b()V

    .line 1028
    return-void
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1122
    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView$c;->getHeadersCount()I

    move-result v0

    .line 1123
    if-ge p1, v0, :cond_0

    .line 1124
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView$FixedViewInfo;

    iget-object v0, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    .line 1145
    :goto_0
    return-object v0

    .line 1127
    :cond_0
    sub-int v2, p1, v0

    .line 1129
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    .line 1130
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    .line 1131
    if-ge v2, v0, :cond_2

    .line 1132
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0, v2, p2, p3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1140
    :cond_2
    sub-int v0, v2, v0

    .line 1141
    iget-object v2, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    if-gez v0, :cond_4

    .line 1145
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView$FixedViewInfo;

    iget-object v0, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    goto :goto_0

    :cond_4
    move v1, v0

    goto :goto_1
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 1062
    invoke-super {p0}, Landroid/widget/HeaderViewListAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1042
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1163
    iget-boolean v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->c:Z

    if-nez v0, :cond_0

    .line 1164
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->b:Lcom/twitter/refresh/widget/RefreshableListView$a;

    invoke-super {p0, v0}, Landroid/widget/HeaderViewListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->c:Z

    .line 1167
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1170
    iget-boolean v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->c:Z

    if-eqz v0, :cond_0

    .line 1171
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->b:Lcom/twitter/refresh/widget/RefreshableListView$a;

    invoke-super {p0, v0}, Landroid/widget/HeaderViewListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->c:Z

    .line 1174
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 1067
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    iget v0, v0, Lcom/twitter/refresh/widget/RefreshableListView;->j:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v0

    .line 1073
    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-static {v1}, Lcom/twitter/refresh/widget/RefreshableListView;->e(Lcom/twitter/refresh/widget/RefreshableListView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1074
    invoke-super {p0, p1}, Landroid/widget/HeaderViewListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1080
    :goto_0
    return-object v0

    .line 1075
    :cond_0
    if-ne p1, v0, :cond_1

    .line 1076
    const/4 v0, 0x0

    goto :goto_0

    .line 1077
    :cond_1
    if-ge p1, v0, :cond_2

    .line 1078
    invoke-super {p0, p1}, Landroid/widget/HeaderViewListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 1080
    :cond_2
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Landroid/widget/HeaderViewListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 1086
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v0

    .line 1087
    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-static {v1}, Lcom/twitter/refresh/widget/RefreshableListView;->e(Lcom/twitter/refresh/widget/RefreshableListView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1088
    invoke-super {p0, p1}, Landroid/widget/HeaderViewListAdapter;->getItemId(I)J

    move-result-wide v0

    .line 1094
    :goto_0
    return-wide v0

    .line 1089
    :cond_0
    if-ne p1, v0, :cond_1

    .line 1090
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 1091
    :cond_1
    if-ge p1, v0, :cond_2

    .line 1092
    invoke-super {p0, p1}, Landroid/widget/HeaderViewListAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0

    .line 1094
    :cond_2
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Landroid/widget/HeaderViewListAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v0

    .line 1151
    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-static {v1}, Lcom/twitter/refresh/widget/RefreshableListView;->e(Lcom/twitter/refresh/widget/RefreshableListView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1152
    invoke-super {p0, p1}, Landroid/widget/HeaderViewListAdapter;->getItemViewType(I)I

    move-result v0

    .line 1158
    :goto_0
    return v0

    .line 1153
    :cond_0
    if-ne p1, v0, :cond_1

    .line 1154
    const/4 v0, -0x1

    goto :goto_0

    .line 1155
    :cond_1
    if-ge p1, v0, :cond_2

    .line 1156
    invoke-super {p0, p1}, Landroid/widget/HeaderViewListAdapter;->getItemViewType(I)I

    move-result v0

    goto :goto_0

    .line 1158
    :cond_2
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Landroid/widget/HeaderViewListAdapter;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1100
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v0

    .line 1101
    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-static {v1}, Lcom/twitter/refresh/widget/RefreshableListView;->e(Lcom/twitter/refresh/widget/RefreshableListView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1102
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/refresh/widget/RefreshableListView$c;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1112
    :goto_0
    return-object v0

    .line 1103
    :cond_0
    if-ne p1, v0, :cond_2

    .line 1104
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1105
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    iget-object v0, v0, Lcom/twitter/refresh/widget/RefreshableListView;->i:Landroid/widget/RelativeLayout;

    goto :goto_0

    .line 1107
    :cond_1
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-static {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->f(Lcom/twitter/refresh/widget/RefreshableListView;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1109
    :cond_2
    if-ge p1, v0, :cond_3

    .line 1110
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/refresh/widget/RefreshableListView$c;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1112
    :cond_3
    add-int/lit8 v0, p1, -0x1

    invoke-direct {p0, v0, p2, p3}, Lcom/twitter/refresh/widget/RefreshableListView$c;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1047
    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-static {v1}, Lcom/twitter/refresh/widget/RefreshableListView;->e(Lcom/twitter/refresh/widget/RefreshableListView;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1048
    invoke-super {p0, p1}, Landroid/widget/HeaderViewListAdapter;->isEnabled(I)Z

    move-result v0

    .line 1054
    :cond_0
    :goto_0
    return v0

    .line 1049
    :cond_1
    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v1}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v1

    if-eq p1, v1, :cond_0

    .line 1051
    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v1}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v1

    if-ge p1, v1, :cond_2

    .line 1052
    invoke-super {p0, p1}, Landroid/widget/HeaderViewListAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0

    .line 1054
    :cond_2
    if-lez p1, :cond_0

    add-int/lit8 v1, p1, -0x1

    invoke-super {p0, v1}, Landroid/widget/HeaderViewListAdapter;->isEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->b:Lcom/twitter/refresh/widget/RefreshableListView$a;

    invoke-virtual {v0, p1}, Lcom/twitter/refresh/widget/RefreshableListView$a;->a(Landroid/database/DataSetObserver;)V

    .line 1033
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView$c;->b:Lcom/twitter/refresh/widget/RefreshableListView$a;

    invoke-virtual {v0, p1}, Lcom/twitter/refresh/widget/RefreshableListView$a;->b(Landroid/database/DataSetObserver;)V

    .line 1038
    return-void
.end method
