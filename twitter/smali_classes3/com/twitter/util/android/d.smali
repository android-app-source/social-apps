.class public Lcom/twitter/util/android/d;
.super Landroid/support/v4/content/CursorLoader;
.source "Twttr"


# instance fields
.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct/range {p0 .. p6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/util/android/d;->a:Z

    .line 26
    return-void
.end method


# virtual methods
.method public a(Z)Lcom/twitter/util/android/d;
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/twitter/util/android/d;->a:Z

    .line 49
    return-object p0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/twitter/util/android/d;->b:Z

    return v0
.end method

.method public deliverResult(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/util/android/d;->b:Z

    .line 74
    invoke-super {p0, p1}, Landroid/support/v4/content/CursorLoader;->deliverResult(Landroid/database/Cursor;)V

    .line 75
    return-void
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/util/android/d;->deliverResult(Landroid/database/Cursor;)V

    return-void
.end method

.method public loadInBackground()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/twitter/util/android/d;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelLoad()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/util/android/d;->b:Z

    .line 68
    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->onCancelLoad()Z

    move-result v0

    return v0
.end method

.method public onContentChanged()V
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/twitter/util/android/d;->a:Z

    if-eqz v0, :cond_0

    .line 55
    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->onContentChanged()V

    .line 57
    :cond_0
    return-void
.end method

.method protected onForceLoad()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->onForceLoad()V

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/util/android/d;->b:Z

    .line 63
    return-void
.end method
