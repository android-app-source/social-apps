.class public Lcom/twitter/util/android/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/util/android/b$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/util/android/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private volatile b:I

.field private c:J

.field private d:J

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/util/android/b;->a:Ljava/util/Set;

    .line 32
    new-instance v0, Lcom/twitter/util/android/b$1;

    invoke-direct {v0, p0}, Lcom/twitter/util/android/b$1;-><init>(Lcom/twitter/util/android/b;)V

    invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 44
    return-void
.end method

.method public static a()Lcom/twitter/util/android/b;
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcot;->ak()Lcot;

    move-result-object v0

    check-cast v0, Lcor;

    invoke-interface {v0}, Lcor;->E()Lcom/twitter/util/android/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    iget v0, p0, Lcom/twitter/util/android/b;->b:I

    if-nez v0, :cond_2

    .line 70
    iget v0, p0, Lcom/twitter/util/android/b;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/util/android/b;->b:I

    .line 71
    iget-boolean v0, p0, Lcom/twitter/util/android/b;->e:Z

    if-nez v0, :cond_0

    .line 72
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/util/android/b;->d:J

    .line 74
    monitor-enter p0

    .line 75
    :try_start_0
    iget-object v0, p0, Lcom/twitter/util/android/b;->a:Ljava/util/Set;

    iget-object v2, p0, Lcom/twitter/util/android/b;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    new-array v2, v2, [Lcom/twitter/util/android/b$a;

    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/util/android/b$a;

    .line 76
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 78
    invoke-interface {v3, p1}, Lcom/twitter/util/android/b$a;->b(Landroid/app/Activity;)V

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 81
    :cond_0
    iput-boolean v1, p0, Lcom/twitter/util/android/b;->e:Z

    .line 86
    :cond_1
    :goto_1
    return-void

    .line 84
    :cond_2
    iget v0, p0, Lcom/twitter/util/android/b;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/util/android/b;->b:I

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/util/android/b;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/twitter/util/android/b;->a(Landroid/app/Activity;)V

    return-void
.end method

.method private b(Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 89
    iget v0, p0, Lcom/twitter/util/android/b;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/twitter/util/android/b;->b:I

    .line 90
    iget v0, p0, Lcom/twitter/util/android/b;->b:I

    if-nez v0, :cond_1

    .line 92
    invoke-virtual {p1}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_2

    .line 94
    monitor-enter p0

    .line 95
    :try_start_0
    iget-object v0, p0, Lcom/twitter/util/android/b;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/twitter/util/android/b;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Lcom/twitter/util/android/b$a;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/util/android/b$a;

    .line 96
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 98
    invoke-interface {v3, p1}, Lcom/twitter/util/android/b$a;->a(Landroid/app/Activity;)V

    .line 97
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 100
    :cond_0
    iget-wide v0, p0, Lcom/twitter/util/android/b;->c:J

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/util/android/b;->d:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/util/android/b;->c:J

    .line 101
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/util/android/b;->d:J

    .line 106
    :cond_1
    :goto_1
    return-void

    .line 103
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/util/android/b;->e:Z

    goto :goto_1
.end method

.method static synthetic b(Lcom/twitter/util/android/b;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/twitter/util/android/b;->b(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lcom/twitter/util/android/b$a;)V
    .locals 1

    .prologue
    .line 47
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/util/android/b;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    monitor-exit p0

    return-void

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/twitter/util/android/b;->b:I

    if-gtz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/util/android/b;->e:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized b(Lcom/twitter/util/android/b$a;)Z
    .locals 1

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/util/android/b;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()J
    .locals 4

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/twitter/util/android/b;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/util/android/b;->c:J

    .line 64
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/twitter/util/android/b;->d:J

    sub-long/2addr v0, v2

    .line 63
    :goto_0
    return-wide v0

    .line 64
    :cond_0
    iget-wide v0, p0, Lcom/twitter/util/android/b;->c:J

    goto :goto_0
.end method
