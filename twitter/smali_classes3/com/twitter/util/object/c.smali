.class public Lcom/twitter/util/object/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/j",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/object/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/j",
            "<TV;>;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/util/object/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/object/j",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/twitter/util/object/c;->a:Lcom/twitter/util/object/j;

    .line 16
    return-void
.end method


# virtual methods
.method public declared-synchronized b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 21
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/util/object/c;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/twitter/util/object/c;->a:Lcom/twitter/util/object/j;

    invoke-interface {v0}, Lcom/twitter/util/object/j;->b()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/util/object/c;->b:Ljava/lang/Object;

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/twitter/util/object/c;->b:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
