.class public Lcom/twitter/util/x;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Landroid/os/StrictMode$ThreadPolicy;

.field private static b:Landroid/os/StrictMode$VmPolicy;

.field private static c:Z

.field private static d:Z

.field private static e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    sget-object v0, Landroid/os/StrictMode$ThreadPolicy;->LAX:Landroid/os/StrictMode$ThreadPolicy;

    sput-object v0, Lcom/twitter/util/x;->a:Landroid/os/StrictMode$ThreadPolicy;

    .line 20
    sget-object v0, Landroid/os/StrictMode$VmPolicy;->LAX:Landroid/os/StrictMode$VmPolicy;

    sput-object v0, Lcom/twitter/util/x;->b:Landroid/os/StrictMode$VmPolicy;

    .line 22
    sput-boolean v1, Lcom/twitter/util/x;->c:Z

    .line 23
    sput-boolean v1, Lcom/twitter/util/x;->d:Z

    .line 24
    sput v1, Lcom/twitter/util/x;->e:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/twitter/util/concurrent/i;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/twitter/util/concurrent/i",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 77
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 78
    const-class v0, Lcom/twitter/util/x;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 79
    const-class v1, Lcom/twitter/util/x;

    monitor-enter v1

    .line 82
    :try_start_0
    sget v0, Lcom/twitter/util/x;->e:I

    add-int/lit8 v2, v0, 0x1

    sput v2, Lcom/twitter/util/x;->e:I

    if-nez v0, :cond_0

    .line 83
    sget-object v0, Landroid/os/StrictMode$VmPolicy;->LAX:Landroid/os/StrictMode$VmPolicy;

    invoke-static {v0}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 85
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    :try_start_1
    invoke-interface {p0}, Lcom/twitter/util/concurrent/i;->call()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v0

    .line 89
    const-class v1, Lcom/twitter/util/x;

    monitor-enter v1

    .line 90
    :try_start_2
    sget v2, Lcom/twitter/util/x;->e:I

    add-int/lit8 v2, v2, -0x1

    sput v2, Lcom/twitter/util/x;->e:I

    if-nez v2, :cond_1

    sget-boolean v2, Lcom/twitter/util/x;->d:Z

    if-eqz v2, :cond_1

    .line 91
    sget-object v2, Lcom/twitter/util/x;->b:Landroid/os/StrictMode$VmPolicy;

    invoke-static {v2}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 93
    :cond_1
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 96
    :goto_0
    return-object v0

    .line 85
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 93
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 89
    :catchall_2
    move-exception v0

    const-class v1, Lcom/twitter/util/x;

    monitor-enter v1

    .line 90
    :try_start_5
    sget v2, Lcom/twitter/util/x;->e:I

    add-int/lit8 v2, v2, -0x1

    sput v2, Lcom/twitter/util/x;->e:I

    if-nez v2, :cond_2

    sget-boolean v2, Lcom/twitter/util/x;->d:Z

    if-eqz v2, :cond_2

    .line 91
    sget-object v2, Lcom/twitter/util/x;->b:Landroid/os/StrictMode$VmPolicy;

    invoke-static {v2}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 93
    :cond_2
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v0

    :catchall_3
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v0

    .line 96
    :cond_3
    invoke-interface {p0}, Lcom/twitter/util/concurrent/i;->call()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/os/StrictMode$ThreadPolicy;)V
    .locals 2

    .prologue
    .line 38
    const-class v1, Lcom/twitter/util/x;

    monitor-enter v1

    :try_start_0
    sput-object p0, Lcom/twitter/util/x;->a:Landroid/os/StrictMode$ThreadPolicy;

    .line 39
    sget-boolean v0, Lcom/twitter/util/x;->c:Z

    if-eqz v0, :cond_0

    .line 40
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/util/x;->a(Z)V

    .line 42
    :cond_0
    const-class v0, Lcom/twitter/util/x;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    monitor-exit v1

    return-void

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Landroid/os/StrictMode$VmPolicy;)V
    .locals 2

    .prologue
    .line 46
    const-class v1, Lcom/twitter/util/x;

    monitor-enter v1

    :try_start_0
    sput-object p0, Lcom/twitter/util/x;->b:Landroid/os/StrictMode$VmPolicy;

    .line 47
    sget-boolean v0, Lcom/twitter/util/x;->d:Z

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/util/x;->b(Z)V

    .line 50
    :cond_0
    const-class v0, Lcom/twitter/util/x;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    monitor-exit v1

    return-void

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 54
    const-class v1, Lcom/twitter/util/x;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    sput-boolean p0, Lcom/twitter/util/x;->c:Z

    .line 56
    if-eqz p0, :cond_1

    sget-object v0, Lcom/twitter/util/x;->a:Landroid/os/StrictMode$ThreadPolicy;

    :goto_0
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 57
    const-class v0, Lcom/twitter/util/x;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :cond_0
    monitor-exit v1

    return-void

    .line 56
    :cond_1
    :try_start_1
    sget-object v0, Landroid/os/StrictMode$ThreadPolicy;->LAX:Landroid/os/StrictMode$ThreadPolicy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(Z)V
    .locals 2

    .prologue
    .line 62
    const-class v1, Lcom/twitter/util/x;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    sput-boolean p0, Lcom/twitter/util/x;->d:Z

    .line 64
    sget v0, Lcom/twitter/util/x;->e:I

    if-nez v0, :cond_0

    .line 65
    if-eqz p0, :cond_2

    sget-object v0, Lcom/twitter/util/x;->b:Landroid/os/StrictMode$VmPolicy;

    :goto_0
    invoke-static {v0}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 67
    :cond_0
    const-class v0, Lcom/twitter/util/x;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    :cond_1
    monitor-exit v1

    return-void

    .line 65
    :cond_2
    :try_start_1
    sget-object v0, Landroid/os/StrictMode$VmPolicy;->LAX:Landroid/os/StrictMode$VmPolicy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
