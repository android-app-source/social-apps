.class final Lcom/twitter/util/serialization/a$1;
.super Lcom/twitter/util/serialization/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/util/serialization/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/r",
        "<",
        "Landroid/graphics/RectF;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/util/serialization/r;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic a(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/twitter/util/serialization/a$1;->c(Lcom/twitter/util/serialization/n;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Landroid/graphics/RectF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    iget v0, p2, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Landroid/graphics/RectF;->top:F

    .line 19
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Landroid/graphics/RectF;->right:F

    .line 20
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    .line 21
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    .line 22
    return-void
.end method

.method protected synthetic b_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    check-cast p2, Landroid/graphics/RectF;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/util/serialization/a$1;->a(Lcom/twitter/util/serialization/o;Landroid/graphics/RectF;)V

    return-void
.end method

.method protected c(Lcom/twitter/util/serialization/n;)Landroid/graphics/RectF;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v1

    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v2

    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v3

    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method
