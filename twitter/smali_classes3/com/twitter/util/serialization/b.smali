.class public abstract Lcom/twitter/util/serialization/b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "B:",
        "Lcom/twitter/util/object/i",
        "<TT;>;>",
        "Lcom/twitter/util/serialization/i",
        "<TT;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    .line 21
    return-void
.end method

.method protected constructor <init>(I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/twitter/util/serialization/i;-><init>(I)V

    .line 25
    return-void
.end method

.method private b(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TB;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 54
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/util/serialization/b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    :try_end_0
    .catch Ljava/io/OptionalDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/twitter/util/serialization/OptionalFieldException; {:try_start_0 .. :try_end_0} :catch_2

    .line 59
    :goto_0
    return-void

    .line 55
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TB;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation
.end method

.method protected abstract b()Lcom/twitter/util/object/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TB;"
        }
    .end annotation
.end method

.method protected final b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "I)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/twitter/util/serialization/b;->b()Lcom/twitter/util/object/i;

    move-result-object v0

    .line 46
    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V

    .line 47
    invoke-virtual {v0}, Lcom/twitter/util/object/i;->q()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
