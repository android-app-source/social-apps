.class public Lcom/twitter/util/serialization/k;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static volatile a:Lcom/twitter/util/collection/l$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/l$b",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([B",
            "Lcom/twitter/util/serialization/l",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 85
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 96
    :goto_0
    return-object v0

    .line 88
    :cond_1
    new-instance v2, Lcom/twitter/util/serialization/d;

    invoke-direct {v2, p0}, Lcom/twitter/util/serialization/d;-><init>([B)V

    .line 90
    :try_start_0
    invoke-virtual {p1, v2}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    goto :goto_0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    :goto_1
    new-instance v3, Lcpb;

    invoke-direct {v3}, Lcpb;-><init>()V

    const-string/jumbo v4, "data"

    .line 94
    invoke-virtual {v2}, Lcom/twitter/util/serialization/n;->b()I

    move-result v2

    const/4 v5, 0x0

    invoke-static {p0, v2, v5}, Lcom/twitter/util/serialization/k;->a([BIZ)Ljava/lang/String;

    move-result-object v2

    .line 93
    invoke-virtual {v3, v4, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    .line 95
    invoke-virtual {v2, v0}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v0

    .line 92
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    move-object v0, v1

    .line 96
    goto :goto_0

    .line 91
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public static a(Lcom/twitter/util/serialization/n;IZ)Ljava/lang/String;
    .locals 11

    .prologue
    const/16 v10, 0x7d

    const/16 v2, 0x20

    const/4 v1, 0x0

    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x7b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/twitter/util/y;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 253
    :try_start_0
    const-string/jumbo v3, "    "

    .line 254
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    move-object v4, v3

    move v5, v1

    move v3, v0

    .line 256
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->o()B

    move-result v0

    const/16 v7, 0xc

    if-eq v0, v7, :cond_d

    .line 257
    const/16 v7, 0xb

    if-ne v0, v7, :cond_2

    .line 258
    add-int/lit8 v5, v5, -0x1

    .line 259
    if-gez v5, :cond_1

    .line 260
    new-instance v0, Lcom/twitter/util/serialization/SerializationException;

    const-string/jumbo v1, "Object end with no matching object start."

    invoke-direct {v0, v1}, Lcom/twitter/util/serialization/SerializationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 383
    :catch_0
    move-exception v0

    .line 384
    const-string/jumbo v1, "ERROR: "

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 386
    :goto_2
    return-object v0

    :cond_0
    move v0, v1

    .line 254
    goto :goto_0

    .line 262
    :cond_1
    :try_start_1
    const-string/jumbo v0, "    "

    add-int/lit8 v4, v5, 0x1

    invoke-static {v0, v4}, Lcom/twitter/util/y;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 263
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->m()V

    .line 264
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v7, 0x7d

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 374
    :goto_3
    if-eqz v3, :cond_f

    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->b()I

    move-result v0

    if-ge p1, v0, :cond_f

    .line 375
    const-string/jumbo v0, " <<<"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 378
    :goto_4
    invoke-static {}, Lcom/twitter/util/y;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v3, v0

    goto :goto_1

    .line 266
    :cond_2
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    packed-switch v0, :pswitch_data_0

    .line 370
    :pswitch_0
    new-instance v1, Lcom/twitter/util/serialization/SerializationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unknown type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/twitter/util/serialization/m;->c(B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/util/serialization/SerializationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 269
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->c()B

    move-result v0

    .line 270
    if-eqz p2, :cond_3

    .line 271
    const-string/jumbo v7, "Byte: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 273
    :cond_3
    const-string/jumbo v0, "Byte"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 278
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    .line 279
    if-eqz p2, :cond_4

    .line 280
    const-string/jumbo v7, "Integer: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 282
    :cond_4
    const-string/jumbo v0, "Integer"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 287
    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v8

    .line 288
    if-eqz p2, :cond_5

    .line 289
    const-string/jumbo v0, "Long: "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 291
    :cond_5
    const-string/jumbo v0, "Long"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 296
    :pswitch_4
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->g()F

    move-result v0

    .line 297
    if-eqz p2, :cond_6

    .line 298
    const-string/jumbo v7, "Float: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 300
    :cond_6
    const-string/jumbo v0, "Float"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 305
    :pswitch_5
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->h()D

    move-result-wide v8

    .line 306
    if-eqz p2, :cond_7

    .line 307
    const-string/jumbo v0, "Double: "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 309
    :cond_7
    const-string/jumbo v0, "Double"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 314
    :pswitch_6
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v0

    .line 315
    if-eqz p2, :cond_8

    .line 316
    const-string/jumbo v7, "Boolean: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 318
    :cond_8
    const-string/jumbo v0, "Boolean"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 323
    :pswitch_7
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->n()V

    .line 324
    const-string/jumbo v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 329
    :pswitch_8
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    .line 330
    if-eqz p2, :cond_9

    .line 331
    const-string/jumbo v7, "String: \""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 332
    invoke-static {}, Lcom/twitter/util/y;->a()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "\\n"

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v7, 0x22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 334
    :cond_9
    const-string/jumbo v7, "String ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v7, 0x29

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 339
    :pswitch_9
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->q()[B

    move-result-object v7

    .line 340
    if-eqz p2, :cond_c

    .line 341
    array-length v0, v7

    if-le v0, v2, :cond_b

    move v0, v2

    .line 342
    :goto_5
    const-string/jumbo v8, "byte[]: \""

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    .line 343
    invoke-static {v7, v9, v0}, Lcom/twitter/util/h;->a([BII)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    array-length v7, v7

    sub-int v0, v7, v0

    .line 345
    if-lez v0, :cond_a

    .line 346
    const-string/jumbo v7, "... "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v7, " more bytes"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    :cond_a
    const/16 v0, 0x22

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 341
    :cond_b
    array-length v0, v7

    goto :goto_5

    .line 350
    :cond_c
    const-string/jumbo v0, "byte[] ("

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v7, v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v7, 0x29

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 355
    :pswitch_a
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->k()I

    move-result v0

    .line 356
    const-string/jumbo v4, "Object: Unknown type, v"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, " {"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 357
    add-int/lit8 v5, v5, 0x1

    .line 358
    const-string/jumbo v0, "    "

    add-int/lit8 v4, v5, 0x1

    invoke-static {v0, v4}, Lcom/twitter/util/y;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 362
    :pswitch_b
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->l()Lcom/twitter/util/collection/Pair;

    move-result-object v4

    .line 363
    const-string/jumbo v0, "Object: "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/twitter/util/collection/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v7, ", v"

    .line 364
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Lcom/twitter/util/collection/j;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, " {"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    add-int/lit8 v5, v5, 0x1

    .line 366
    const-string/jumbo v0, "    "

    add-int/lit8 v4, v5, 0x1

    invoke-static {v0, v4}, Lcom/twitter/util/y;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 380
    :cond_d
    if-lez v5, :cond_e

    .line 381
    new-instance v0, Lcom/twitter/util/serialization/SerializationException;

    const-string/jumbo v1, "Object start with no matching object end."

    invoke-direct {v0, v1}, Lcom/twitter/util/serialization/SerializationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 386
    :cond_e
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_f
    move v0, v3

    goto/16 :goto_4

    .line 267
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static a([BIZ)Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    new-instance v0, Lcom/twitter/util/serialization/d;

    invoke-direct {v0, p0}, Lcom/twitter/util/serialization/d;-><init>([B)V

    invoke-static {v0, p1, p2}, Lcom/twitter/util/serialization/k;->a(Lcom/twitter/util/serialization/n;IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/twitter/util/collection/l$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/l$b",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 36
    sput-object p0, Lcom/twitter/util/serialization/k;->a:Lcom/twitter/util/collection/l$b;

    .line 37
    const-class v0, Lcom/twitter/util/serialization/k;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 38
    return-void
.end method

.method private static a(Lcom/twitter/util/serialization/n;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 390
    const/4 v0, 0x0

    .line 392
    if-eqz p1, :cond_2

    .line 393
    invoke-static {p0}, Lcom/twitter/util/serialization/k;->a(Lcom/twitter/util/serialization/n;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 467
    :cond_0
    return-void

    .line 396
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->o()B

    move-result v1

    .line 397
    const/16 v2, 0x9

    if-eq v1, v2, :cond_2

    const/16 v2, 0xa

    if-eq v1, v2, :cond_2

    .line 398
    new-instance v0, Lcom/twitter/util/serialization/SerializationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Method skipObject can only be used to skip Objects in deserialization, expected start object header but found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 400
    invoke-static {v1}, Lcom/twitter/util/serialization/m;->c(B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/util/serialization/SerializationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 406
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->c()B

    .line 403
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->o()B

    move-result v1

    const/16 v2, 0xc

    if-eq v1, v2, :cond_4

    .line 404
    packed-switch v1, :pswitch_data_0

    .line 460
    :pswitch_1
    new-instance v0, Lcom/twitter/util/serialization/SerializationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unknown type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Lcom/twitter/util/serialization/m;->c(B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/util/serialization/SerializationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->e()I

    goto :goto_0

    .line 414
    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->f()J

    goto :goto_0

    .line 418
    :pswitch_4
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->g()F

    goto :goto_0

    .line 422
    :pswitch_5
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->h()D

    goto :goto_0

    .line 426
    :pswitch_6
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->d()Z

    goto :goto_0

    .line 430
    :pswitch_7
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->n()V

    goto :goto_0

    .line 435
    :pswitch_8
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    goto :goto_0

    .line 439
    :pswitch_9
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->j()[B

    goto :goto_0

    .line 444
    :pswitch_a
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->k()I

    .line 445
    add-int/lit8 v0, v0, 0x1

    .line 446
    goto :goto_0

    .line 449
    :pswitch_b
    add-int/lit8 v0, v0, -0x1

    .line 450
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->m()V

    .line 451
    if-eqz p1, :cond_3

    if-eqz v0, :cond_0

    .line 454
    :cond_3
    if-gez v0, :cond_2

    .line 455
    new-instance v0, Lcom/twitter/util/serialization/SerializationException;

    const-string/jumbo v1, "Object end with no matching object start."

    invoke-direct {v0, v1}, Lcom/twitter/util/serialization/SerializationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 464
    :cond_4
    if-lez v0, :cond_0

    .line 465
    new-instance v0, Lcom/twitter/util/serialization/SerializationException;

    const-string/jumbo v1, "Object start with no matching object end."

    invoke-direct {v0, v1}, Lcom/twitter/util/serialization/SerializationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 404
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_1
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static a(Lcom/twitter/util/serialization/n;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 180
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 181
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->o()B

    move-result v2

    const/4 v3, 0x7

    if-ne v2, v3, :cond_1

    .line 182
    :goto_0
    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->n()V

    .line 191
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 181
    goto :goto_0

    .line 187
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->c()B

    move-result v2

    .line 188
    if-eqz v2, :cond_0

    .line 190
    if-ne v2, v0, :cond_3

    move v0, v1

    .line 191
    goto :goto_1

    .line 193
    :cond_3
    new-instance v0, Lcom/twitter/util/serialization/SerializationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Invalid null indicator found: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/util/serialization/SerializationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 164
    if-nez p1, :cond_1

    .line 165
    invoke-virtual {p0}, Lcom/twitter/util/serialization/o;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/twitter/util/serialization/o;->g()Lcom/twitter/util/serialization/o;

    .line 175
    :goto_0
    return v0

    .line 168
    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/util/serialization/o;->b(B)Lcom/twitter/util/serialization/o;

    goto :goto_0

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/util/serialization/o;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 173
    invoke-virtual {p0, v0}, Lcom/twitter/util/serialization/o;->b(B)Lcom/twitter/util/serialization/o;

    :cond_2
    move v0, v1

    .line 175
    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/twitter/util/serialization/l",
            "<TT;>;)[B"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 47
    if-nez p0, :cond_0

    .line 48
    sget-object v0, Lcom/twitter/util/h;->a:[B

    .line 64
    :goto_0
    return-object v0

    .line 50
    :cond_0
    sget-object v3, Lcom/twitter/util/serialization/k;->a:Lcom/twitter/util/collection/l$b;

    .line 51
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/twitter/util/collection/l$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    move-object v1, v0

    .line 52
    :goto_1
    if-eqz v1, :cond_2

    .line 57
    :try_start_0
    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 58
    :try_start_1
    invoke-static {p0, p1, v1}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;[B)[B

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/l$b;->a(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 51
    goto :goto_1

    .line 59
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 61
    :catchall_1
    move-exception v0

    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/l$b;->a(Ljava/lang/Object;)Z

    throw v0

    .line 64
    :cond_2
    invoke-static {p0, p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;[B)[B

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;[B)[B
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/twitter/util/serialization/l",
            "<TT;>;[B)[B"
        }
    .end annotation

    .prologue
    .line 70
    if-nez p0, :cond_0

    .line 71
    sget-object v0, Lcom/twitter/util/h;->a:[B

    .line 79
    :goto_0
    return-object v0

    .line 73
    :cond_0
    new-instance v0, Lcom/twitter/util/serialization/e;

    invoke-direct {v0, p2}, Lcom/twitter/util/serialization/e;-><init>([B)V

    .line 75
    :try_start_0
    invoke-virtual {p1, v0, p0}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_1
    invoke-virtual {v0}, Lcom/twitter/util/serialization/e;->b()[B

    move-result-object v0

    goto :goto_0

    .line 76
    :catch_0
    move-exception v1

    .line 77
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static b([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([B",
            "Lcom/twitter/util/serialization/l",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 142
    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    .line 151
    :cond_0
    :goto_0
    return-object v0

    .line 147
    :cond_1
    :try_start_0
    new-instance v2, Ljava/io/ObjectInputStream;

    new-instance v1, Ljava/util/zip/GZIPInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v3}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    :try_start_1
    new-instance v1, Lcom/twitter/util/serialization/g;

    invoke-direct {v1, v2}, Lcom/twitter/util/serialization/g;-><init>(Ljava/io/ObjectInput;)V

    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 153
    if-eqz v2, :cond_0

    .line 155
    :try_start_2
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 156
    :catch_0
    move-exception v1

    goto :goto_0

    .line 149
    :catch_1
    move-exception v1

    move-object v2, v0

    .line 150
    :goto_1
    :try_start_3
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 153
    if-eqz v2, :cond_0

    .line 155
    :try_start_4
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 156
    :catch_2
    move-exception v1

    goto :goto_0

    .line 153
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_2

    .line 155
    :try_start_5
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 156
    :cond_2
    :goto_3
    throw v0

    :catch_3
    move-exception v1

    goto :goto_3

    .line 153
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 149
    :catch_4
    move-exception v1

    goto :goto_1

    :catch_5
    move-exception v1

    move-object v2, v0

    goto :goto_1

    :catch_6
    move-exception v1

    goto :goto_1

    :catch_7
    move-exception v1

    move-object v2, v0

    goto :goto_1

    :catch_8
    move-exception v1

    goto :goto_1
.end method

.method public static b(Lcom/twitter/util/serialization/n;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/twitter/util/serialization/k;->a(Lcom/twitter/util/serialization/n;Z)V

    .line 218
    return-void
.end method

.method public static b(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/twitter/util/serialization/l",
            "<TT;>;)[B"
        }
    .end annotation

    .prologue
    .line 112
    if-nez p0, :cond_0

    .line 113
    sget-object v0, Lcom/twitter/util/h;->a:[B

    .line 129
    :goto_0
    return-object v0

    .line 115
    :cond_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 116
    const/4 v2, 0x0

    .line 118
    :try_start_0
    new-instance v1, Ljava/io/ObjectOutputStream;

    new-instance v0, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v0, v3}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    :try_start_1
    new-instance v0, Lcom/twitter/util/serialization/h;

    invoke-direct {v0, v1}, Lcom/twitter/util/serialization/h;-><init>(Ljava/io/ObjectOutput;)V

    invoke-virtual {p1, v0, p0}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 123
    if-eqz v1, :cond_1

    .line 125
    :try_start_2
    invoke-interface {v1}, Ljava/io/ObjectOutput;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 129
    :cond_1
    :goto_1
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 121
    :goto_2
    :try_start_3
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 123
    if-eqz v1, :cond_1

    .line 125
    :try_start_4
    invoke-interface {v1}, Ljava/io/ObjectOutput;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 126
    :catch_1
    move-exception v0

    goto :goto_1

    .line 123
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_2

    .line 125
    :try_start_5
    invoke-interface {v1}, Ljava/io/ObjectOutput;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 126
    :cond_2
    :goto_4
    throw v0

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_4

    .line 123
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 120
    :catch_4
    move-exception v0

    goto :goto_2
.end method
