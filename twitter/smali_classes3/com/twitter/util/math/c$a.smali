.class Lcom/twitter/util/math/c$a;
.super Lcom/twitter/util/serialization/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/util/math/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/r",
        "<",
        "Lcom/twitter/util/math/c;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/twitter/util/serialization/r;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/util/math/c$1;)V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/twitter/util/math/c$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic a(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 207
    invoke-virtual {p0, p1}, Lcom/twitter/util/math/c$a;->c(Lcom/twitter/util/serialization/n;)Lcom/twitter/util/math/c;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/util/math/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    iget v0, p2, Lcom/twitter/util/math/c;->d:F

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/util/math/c;->e:F

    .line 212
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/util/math/c;->f:F

    .line 213
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/util/math/c;->g:F

    .line 214
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    .line 215
    return-void
.end method

.method protected synthetic b_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    check-cast p2, Lcom/twitter/util/math/c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/util/math/c$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/util/math/c;)V

    return-void
.end method

.method protected c(Lcom/twitter/util/serialization/n;)Lcom/twitter/util/math/c;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 221
    new-instance v0, Lcom/twitter/util/math/c;

    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v1

    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v2

    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v3

    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/util/math/c;-><init>(FFFFLcom/twitter/util/math/c$1;)V

    return-object v0
.end method
