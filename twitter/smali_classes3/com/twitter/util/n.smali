.class public Lcom/twitter/util/n;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/util/n$a;
    }
.end annotation


# static fields
.field private static a:I

.field private static b:I


# instance fields
.field private final c:Landroid/view/OrientationEventListener;

.field private d:I

.field private e:I

.field private f:I

.field private g:Lcom/twitter/util/n$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 24
    sput v0, Lcom/twitter/util/n;->a:I

    .line 25
    sput v0, Lcom/twitter/util/n;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput v0, p0, Lcom/twitter/util/n;->d:I

    .line 30
    iput v0, p0, Lcom/twitter/util/n;->e:I

    .line 35
    new-instance v0, Lcom/twitter/util/n$1;

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/util/n$1;-><init>(Lcom/twitter/util/n;Landroid/content/Context;)V

    .line 43
    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->canDetectOrientation()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 45
    iput-object v0, p0, Lcom/twitter/util/n;->c:Landroid/view/OrientationEventListener;

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/util/n;->c:Landroid/view/OrientationEventListener;

    goto :goto_0
.end method

.method public static a(II)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 187
    const/4 v1, -0x1

    if-ne p1, v1, :cond_2

    .line 194
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    add-int/lit8 v0, p0, 0x2d

    div-int/lit8 v0, v0, 0x5a

    mul-int/lit8 v0, v0, 0x5a

    rem-int/lit16 p1, v0, 0x168

    :cond_1
    return p1

    .line 190
    :cond_2
    sub-int v1, p0, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 191
    rsub-int v2, v1, 0x168

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 192
    const/16 v2, 0x32

    if-ge v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 122
    const-string/jumbo v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 123
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 124
    mul-int/lit8 v0, v0, 0x5a

    return v0
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 1

    .prologue
    .line 110
    invoke-static {p0}, Lcom/twitter/util/n;->a(Landroid/content/Context;)I

    move-result v0

    sub-int/2addr v0, p1

    invoke-static {v0}, Lcom/twitter/util/ui/k;->a(I)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 149
    sget v0, Lcom/twitter/util/n;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 150
    invoke-virtual {p0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v0

    .line 151
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 152
    invoke-static {p0}, Lcom/twitter/util/n;->a(Landroid/content/Context;)I

    move-result v1

    sput v1, Lcom/twitter/util/n;->a:I

    .line 153
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 154
    invoke-static {p0}, Lcom/twitter/util/n;->a(Landroid/content/Context;)I

    move-result v1

    sput v1, Lcom/twitter/util/n;->b:I

    .line 155
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 156
    const-class v0, Lcom/twitter/util/n;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 158
    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;)I
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 134
    const-string/jumbo v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 135
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 136
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    .line 138
    if-eqz v0, :cond_0

    if-ne v0, v2, :cond_1

    :cond_0
    if-eq v3, v1, :cond_3

    :cond_1
    if-eq v0, v1, :cond_2

    const/4 v4, 0x3

    if-ne v0, v4, :cond_4

    :cond_2
    if-ne v3, v2, :cond_4

    :cond_3
    move v0, v1

    .line 145
    :goto_0
    return v0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)I
    .locals 3

    .prologue
    const/16 v2, 0xb4

    .line 161
    invoke-static {p0}, Lcom/twitter/util/n;->a(Landroid/content/Context;)I

    move-result v0

    .line 162
    sget v1, Lcom/twitter/util/n;->b:I

    if-ne v0, v1, :cond_0

    .line 163
    const/4 v0, 0x1

    .line 174
    :goto_0
    return v0

    .line 165
    :cond_0
    sget v1, Lcom/twitter/util/n;->a:I

    if-ne v0, v1, :cond_1

    .line 166
    const/4 v0, 0x0

    goto :goto_0

    .line 168
    :cond_1
    sget v1, Lcom/twitter/util/n;->b:I

    sub-int v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 169
    const/16 v0, 0x9

    goto :goto_0

    .line 171
    :cond_2
    sget v1, Lcom/twitter/util/n;->a:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 172
    const/16 v0, 0x8

    goto :goto_0

    .line 174
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/util/n;->c:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/twitter/util/n;->c:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 89
    :cond_0
    return-void
.end method

.method a(I)V
    .locals 3

    .prologue
    .line 58
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    iget v0, p0, Lcom/twitter/util/n;->d:I

    invoke-static {p1, v0}, Lcom/twitter/util/n;->a(II)I

    move-result v0

    .line 64
    iget v1, p0, Lcom/twitter/util/n;->d:I

    if-eq v0, v1, :cond_0

    .line 65
    iget v1, p0, Lcom/twitter/util/n;->e:I

    if-ne v0, v1, :cond_2

    .line 67
    iget v1, p0, Lcom/twitter/util/n;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/twitter/util/n;->f:I

    .line 68
    iget v1, p0, Lcom/twitter/util/n;->f:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 69
    iput v0, p0, Lcom/twitter/util/n;->d:I

    .line 70
    iget-object v1, p0, Lcom/twitter/util/n;->g:Lcom/twitter/util/n$a;

    if-eqz v1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/twitter/util/n;->g:Lcom/twitter/util/n$a;

    invoke-interface {v1, v0}, Lcom/twitter/util/n$a;->e_(I)V

    goto :goto_0

    .line 75
    :cond_2
    iput v0, p0, Lcom/twitter/util/n;->e:I

    .line 76
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/util/n;->f:I

    goto :goto_0
.end method

.method public a(Lcom/twitter/util/n$a;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/twitter/util/n;->g:Lcom/twitter/util/n$a;

    .line 83
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/util/n;->c:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/twitter/util/n;->c:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 95
    :cond_0
    return-void
.end method
