.class final Lcom/twitter/util/collection/d$3;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/util/collection/d;->b(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Ljava/util/Set",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/util/serialization/l;


# direct methods
.method constructor <init>(Lcom/twitter/util/serialization/l;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/twitter/util/collection/d$3;->a:Lcom/twitter/util/serialization/l;

    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "I)",
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/util/collection/d$3;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/collection/d;->d(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "Ljava/util/Set",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 228
    iget-object v0, p0, Lcom/twitter/util/collection/d$3;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, p2, v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Set;Lcom/twitter/util/serialization/l;)V

    .line 229
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    check-cast p2, Ljava/util/Set;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/util/collection/d$3;->a(Lcom/twitter/util/serialization/o;Ljava/util/Set;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 224
    invoke-virtual {p0, p1, p2}, Lcom/twitter/util/collection/d$3;->a(Lcom/twitter/util/serialization/n;I)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
