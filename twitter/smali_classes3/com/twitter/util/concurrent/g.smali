.class public interface abstract Lcom/twitter/util/concurrent/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/concurrent/Future;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Future",
        "<TV;>;"
    }
.end annotation


# virtual methods
.method public abstract a(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/concurrent/d",
            "<",
            "Ljava/lang/Void;",
            ">;)",
            "Lcom/twitter/util/concurrent/g",
            "<TV;>;"
        }
    .end annotation
.end method

.method public abstract b(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/concurrent/d",
            "<TV;>;)",
            "Lcom/twitter/util/concurrent/g",
            "<TV;>;"
        }
    .end annotation
.end method

.method public abstract c(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/concurrent/d",
            "<",
            "Ljava/lang/Exception;",
            ">;)",
            "Lcom/twitter/util/concurrent/g",
            "<TV;>;"
        }
    .end annotation
.end method

.method public abstract d(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/concurrent/d",
            "<",
            "Ljava/lang/Void;",
            ">;)",
            "Lcom/twitter/util/concurrent/g",
            "<TV;>;"
        }
    .end annotation
.end method
