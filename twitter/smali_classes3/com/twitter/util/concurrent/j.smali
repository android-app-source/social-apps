.class public Lcom/twitter/util/concurrent/j;
.super Ljava/util/concurrent/AbstractExecutorService;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/util/concurrent/j$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/concurrent/j;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/util/concurrent/j;

    invoke-direct {v0}, Lcom/twitter/util/concurrent/j;-><init>()V

    sput-object v0, Lcom/twitter/util/concurrent/j;->a:Lcom/twitter/util/concurrent/j;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/util/concurrent/AbstractExecutorService;-><init>()V

    .line 30
    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Lcom/twitter/util/collection/MutableList;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/util/concurrent/j;->b:Ljava/util/List;

    .line 30
    return-void
.end method


# virtual methods
.method public awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/twitter/util/concurrent/j;->d:Z

    return v0
.end method

.method public declared-synchronized execute(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/util/concurrent/j;->d:Z

    if-nez v0, :cond_0

    .line 89
    iget-boolean v0, p0, Lcom/twitter/util/concurrent/j;->c:Z

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/twitter/util/concurrent/j;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 92
    :cond_1
    :try_start_1
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isShutdown()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/twitter/util/concurrent/j;->d:Z

    return v0
.end method

.method public isTerminated()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/twitter/util/concurrent/j;->d:Z

    return v0
.end method

.method protected newTaskFor(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/RunnableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Ljava/util/concurrent/RunnableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 131
    new-instance v0, Lcom/twitter/util/concurrent/j$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/util/concurrent/j$a;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method protected newTaskFor(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/RunnableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Ljava/util/concurrent/RunnableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 137
    new-instance v0, Lcom/twitter/util/concurrent/j$a;

    invoke-direct {v0, p1}, Lcom/twitter/util/concurrent/j$a;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method

.method public declared-synchronized shutdown()V
    .locals 1

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/util/concurrent/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/util/concurrent/j;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    monitor-exit p0

    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/util/concurrent/j;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 109
    invoke-virtual {p0}, Lcom/twitter/util/concurrent/j;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    monitor-exit p0

    return-object v0

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
