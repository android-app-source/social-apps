.class public final enum Lcom/twitter/util/connectivity/TwRadioType;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/util/connectivity/TwRadioType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum b:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum c:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum d:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum e:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum f:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum g:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum h:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum i:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum j:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum k:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum l:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum m:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum n:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum o:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum p:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum q:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum r:Lcom/twitter/util/connectivity/TwRadioType;

.field public static final enum s:Lcom/twitter/util/connectivity/TwRadioType;

.field private static final synthetic t:[Lcom/twitter/util/connectivity/TwRadioType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->a:Lcom/twitter/util/connectivity/TwRadioType;

    .line 9
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->b:Lcom/twitter/util/connectivity/TwRadioType;

    .line 10
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "GPRS"

    invoke-direct {v0, v1, v5}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->c:Lcom/twitter/util/connectivity/TwRadioType;

    .line 11
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "EDGE"

    invoke-direct {v0, v1, v6}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->d:Lcom/twitter/util/connectivity/TwRadioType;

    .line 12
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "UMTS"

    invoke-direct {v0, v1, v7}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->e:Lcom/twitter/util/connectivity/TwRadioType;

    .line 13
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "CDMA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->f:Lcom/twitter/util/connectivity/TwRadioType;

    .line 14
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "EVDO_0"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->g:Lcom/twitter/util/connectivity/TwRadioType;

    .line 15
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "EVDO_A"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->h:Lcom/twitter/util/connectivity/TwRadioType;

    .line 16
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "X1RTT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->i:Lcom/twitter/util/connectivity/TwRadioType;

    .line 17
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "HSDPA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->j:Lcom/twitter/util/connectivity/TwRadioType;

    .line 18
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "HSUPA"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->k:Lcom/twitter/util/connectivity/TwRadioType;

    .line 19
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "HSPA"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->l:Lcom/twitter/util/connectivity/TwRadioType;

    .line 20
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "IDEN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->m:Lcom/twitter/util/connectivity/TwRadioType;

    .line 21
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "EVDO_B"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->n:Lcom/twitter/util/connectivity/TwRadioType;

    .line 22
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "LTE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->o:Lcom/twitter/util/connectivity/TwRadioType;

    .line 23
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "EHRPD"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->p:Lcom/twitter/util/connectivity/TwRadioType;

    .line 24
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "HSPAP"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->q:Lcom/twitter/util/connectivity/TwRadioType;

    .line 25
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "GSM"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->r:Lcom/twitter/util/connectivity/TwRadioType;

    .line 26
    new-instance v0, Lcom/twitter/util/connectivity/TwRadioType;

    const-string/jumbo v1, "WIFI"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/connectivity/TwRadioType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->s:Lcom/twitter/util/connectivity/TwRadioType;

    .line 7
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/twitter/util/connectivity/TwRadioType;

    sget-object v1, Lcom/twitter/util/connectivity/TwRadioType;->a:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/util/connectivity/TwRadioType;->b:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/util/connectivity/TwRadioType;->c:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/util/connectivity/TwRadioType;->d:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/util/connectivity/TwRadioType;->e:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->f:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->g:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->h:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->i:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->j:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->k:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->l:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->m:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->n:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->o:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->p:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->q:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->r:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/twitter/util/connectivity/TwRadioType;->s:Lcom/twitter/util/connectivity/TwRadioType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/util/connectivity/TwRadioType;->t:[Lcom/twitter/util/connectivity/TwRadioType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/util/connectivity/TwRadioType;
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/twitter/util/connectivity/TwRadioType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/connectivity/TwRadioType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/util/connectivity/TwRadioType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->t:[Lcom/twitter/util/connectivity/TwRadioType;

    invoke-virtual {v0}, [Lcom/twitter/util/connectivity/TwRadioType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/util/connectivity/TwRadioType;

    return-object v0
.end method
