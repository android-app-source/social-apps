.class public Lcom/twitter/util/connectivity/a;
.super Lcom/twitter/util/p;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/p",
        "<",
        "Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/util/connectivity/TwRadioType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/util/p;-><init>()V

    .line 15
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->b:Lcom/twitter/util/connectivity/TwRadioType;

    iput-object v0, p0, Lcom/twitter/util/connectivity/a;->a:Lcom/twitter/util/connectivity/TwRadioType;

    return-void
.end method

.method public static a()Lcom/twitter/util/connectivity/a;
    .locals 1

    .prologue
    .line 20
    invoke-static {}, Lcom/twitter/util/m;->ai()Lcom/twitter/util/m$a;

    move-result-object v0

    check-cast v0, Lcos;

    invoke-interface {v0}, Lcos;->A()Lcom/twitter/util/connectivity/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;)V
    .locals 1

    .prologue
    .line 25
    if-eqz p1, :cond_0

    .line 26
    invoke-interface {p1}, Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;->a()Lcom/twitter/util/connectivity/TwRadioType;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/util/connectivity/a;->a:Lcom/twitter/util/connectivity/TwRadioType;

    .line 28
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;

    invoke-virtual {p0, p1}, Lcom/twitter/util/connectivity/a;->a(Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;)V

    return-void
.end method

.method public b()Lcom/twitter/util/connectivity/TwRadioType;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/util/connectivity/a;->a:Lcom/twitter/util/connectivity/TwRadioType;

    return-object v0
.end method
