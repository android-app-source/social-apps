.class Lcom/twitter/util/connectivity/b$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/util/connectivity/b;-><init>(Lcqs;Lcom/twitter/util/p;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/util/connectivity/b;


# direct methods
.method constructor <init>(Lcom/twitter/util/connectivity/b;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/twitter/util/connectivity/b$2;->a:Lcom/twitter/util/connectivity/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;)V
    .locals 3

    .prologue
    .line 47
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;->a()Lcom/twitter/util/connectivity/TwRadioType;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/connectivity/TwRadioType;->s:Lcom/twitter/util/connectivity/TwRadioType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 48
    :goto_0
    iget-object v1, p0, Lcom/twitter/util/connectivity/b$2;->a:Lcom/twitter/util/connectivity/b;

    iget-object v2, p0, Lcom/twitter/util/connectivity/b$2;->a:Lcom/twitter/util/connectivity/b;

    invoke-static {v2}, Lcom/twitter/util/connectivity/b;->b(Lcom/twitter/util/connectivity/b;)Z

    move-result v2

    invoke-static {v1, v2, v0}, Lcom/twitter/util/connectivity/b;->a(Lcom/twitter/util/connectivity/b;ZZ)V

    .line 49
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p1, Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;

    invoke-virtual {p0, p1}, Lcom/twitter/util/connectivity/b$2;->onEvent(Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;)V

    return-void
.end method
