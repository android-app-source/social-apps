.class public Lcom/twitter/util/connectivity/b;
.super Lcom/twitter/util/p;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/p",
        "<",
        "Lcom/twitter/util/connectivity/WifiOnlyModeEvent;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>(Lcqs;Lcom/twitter/util/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcqs;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Lcom/twitter/util/p;-><init>()V

    .line 22
    iput-boolean v1, p0, Lcom/twitter/util/connectivity/b;->a:Z

    .line 23
    iput-boolean v1, p0, Lcom/twitter/util/connectivity/b;->b:Z

    .line 32
    const-string/jumbo v0, "wifi_only_mode"

    invoke-interface {p1, v0, v1}, Lcqs;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/util/connectivity/b;->a:Z

    .line 34
    new-instance v0, Lcom/twitter/util/connectivity/b$1;

    invoke-direct {v0, p0}, Lcom/twitter/util/connectivity/b$1;-><init>(Lcom/twitter/util/connectivity/b;)V

    invoke-interface {p1, v0}, Lcqs;->a(Lcqs$a;)V

    .line 44
    new-instance v0, Lcom/twitter/util/connectivity/b$2;

    invoke-direct {v0, p0}, Lcom/twitter/util/connectivity/b$2;-><init>(Lcom/twitter/util/connectivity/b;)V

    invoke-virtual {p2, v0}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 51
    return-void
.end method

.method public static declared-synchronized a()Lcom/twitter/util/connectivity/b;
    .locals 2

    .prologue
    .line 27
    const-class v1, Lcom/twitter/util/connectivity/b;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/twitter/util/m;->ai()Lcom/twitter/util/m$a;

    move-result-object v0

    check-cast v0, Lcos;

    invoke-interface {v0}, Lcos;->B()Lcom/twitter/util/connectivity/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/twitter/util/connectivity/b;ZZ)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/twitter/util/connectivity/b;->a(ZZ)V

    return-void
.end method

.method private a(ZZ)V
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/twitter/util/connectivity/b;->b()Z

    move-result v0

    .line 56
    iput-boolean p1, p0, Lcom/twitter/util/connectivity/b;->a:Z

    .line 57
    iput-boolean p2, p0, Lcom/twitter/util/connectivity/b;->b:Z

    .line 59
    invoke-virtual {p0}, Lcom/twitter/util/connectivity/b;->b()Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 60
    new-instance v0, Lcom/twitter/util/connectivity/WifiOnlyModeEvent;

    invoke-virtual {p0}, Lcom/twitter/util/connectivity/b;->b()Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/twitter/util/connectivity/WifiOnlyModeEvent;-><init>(Z)V

    invoke-virtual {p0, v0}, Lcom/twitter/util/connectivity/b;->a(Ljava/lang/Object;)V

    .line 62
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/util/connectivity/b;)Z
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/twitter/util/connectivity/b;->b:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/util/connectivity/b;)Z
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/twitter/util/connectivity/b;->a:Z

    return v0
.end method


# virtual methods
.method public b()Z
    .locals 1

    .prologue
    .line 65
    const-string/jumbo v0, "wifi_only_mode"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    .line 71
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/util/connectivity/b;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/util/connectivity/b;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
