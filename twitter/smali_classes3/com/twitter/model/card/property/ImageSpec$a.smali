.class Lcom/twitter/model/card/property/ImageSpec$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/card/property/ImageSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/card/property/ImageSpec;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/card/property/ImageSpec$1;)V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/twitter/model/card/property/ImageSpec$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/card/property/ImageSpec;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v2

    .line 141
    sget-object v0, Lcom/twitter/model/card/property/Vector2F;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/card/property/Vector2F;

    .line 142
    const/4 v1, 0x0

    .line 144
    :try_start_0
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 148
    :goto_0
    new-instance v3, Lcom/twitter/model/card/property/ImageSpec;

    invoke-direct {v3, v2, v0, v1}, Lcom/twitter/model/card/property/ImageSpec;-><init>(Ljava/lang/String;Lcom/twitter/model/card/property/Vector2F;Ljava/lang/String;)V

    return-object v3

    .line 145
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/card/property/ImageSpec;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p2, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 132
    sget-object v0, Lcom/twitter/model/card/property/Vector2F;->a:Lcom/twitter/util/serialization/l;

    iget-object v1, p2, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 133
    iget-object v0, p2, Lcom/twitter/model/card/property/ImageSpec;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 134
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    check-cast p2, Lcom/twitter/model/card/property/ImageSpec;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/card/property/ImageSpec$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/card/property/ImageSpec;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 128
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/card/property/ImageSpec$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    return-object v0
.end method
