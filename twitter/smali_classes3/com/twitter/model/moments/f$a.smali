.class Lcom/twitter/model/moments/f$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/moments/f;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/i;-><init>(I)V

    .line 36
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/moments/f;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 51
    const-class v0, Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 52
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 53
    sget-object v1, Lcom/twitter/util/serialization/f;->b:Lcom/twitter/util/serialization/l;

    .line 54
    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 55
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v2

    .line 56
    new-instance v3, Lcom/twitter/model/moments/f;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/model/moments/f;-><init>(Lcom/twitter/model/moments/MomentVisibilityMode;Ljava/lang/Boolean;Z)V

    return-object v3
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/f;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p2, Lcom/twitter/model/moments/f;->c:Lcom/twitter/model/moments/MomentVisibilityMode;

    const-class v1, Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 42
    invoke-static {v1}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 41
    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 43
    iget-object v0, p2, Lcom/twitter/model/moments/f;->d:Ljava/lang/Boolean;

    sget-object v1, Lcom/twitter/util/serialization/f;->b:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 44
    iget-boolean v0, p2, Lcom/twitter/model/moments/f;->e:Z

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 45
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    check-cast p2, Lcom/twitter/model/moments/f;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/f$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/f;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/f$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/moments/f;

    move-result-object v0

    return-object v0
.end method
