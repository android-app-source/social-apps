.class public Lcom/twitter/model/moments/s;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/s$a;,
        Lcom/twitter/model/moments/s$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/moments/s;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/twitter/model/moments/MomentSocialProofType;

.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/model/moments/s$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/moments/s$b;-><init>(Lcom/twitter/model/moments/s$1;)V

    sput-object v0, Lcom/twitter/model/moments/s;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/moments/s$a;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iget-object v0, p1, Lcom/twitter/model/moments/s$a;->a:Lcom/twitter/model/moments/MomentSocialProofType;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/MomentSocialProofType;

    iput-object v0, p0, Lcom/twitter/model/moments/s;->b:Lcom/twitter/model/moments/MomentSocialProofType;

    .line 25
    iget-object v0, p1, Lcom/twitter/model/moments/s$a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/moments/s;->c:Ljava/lang/String;

    .line 26
    return-void
.end method
