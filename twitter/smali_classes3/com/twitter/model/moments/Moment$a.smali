.class public final Lcom/twitter/model/moments/Moment$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/Moment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/moments/Moment;",
        ">;"
    }
.end annotation


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Lcom/twitter/model/moments/a;

.field private o:Lcgi;

.field private p:Lcom/twitter/model/moments/g;

.field private q:J

.field private r:Lcom/twitter/model/moments/f;

.field private s:Z

.field private t:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/moments/Moment$a;)J
    .locals 2

    .prologue
    .line 150
    iget-wide v0, p0, Lcom/twitter/model/moments/Moment$a;->a:J

    return-wide v0
.end method

.method public static a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/Moment$a;
    .locals 4

    .prologue
    .line 174
    new-instance v0, Lcom/twitter/model/moments/Moment$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/Moment$a;-><init>()V

    iget-wide v2, p0, Lcom/twitter/model/moments/Moment;->b:J

    .line 175
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/moments/Moment$a;->a(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    .line 176
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/moments/Moment;->d:Z

    .line 177
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/moments/Moment;->e:Z

    .line 178
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->b(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/moments/Moment;->f:Z

    .line 179
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->c(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->g:Ljava/lang/String;

    .line 180
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->b(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->h:Ljava/lang/String;

    .line 181
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->c(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->i:Ljava/lang/String;

    .line 182
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->d(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->j:Ljava/lang/String;

    .line 183
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->e(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/moments/Moment;->k:Z

    .line 184
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->d(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->l:Ljava/lang/String;

    .line 185
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->f(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/moments/Moment;->m:I

    .line 186
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(I)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->n:Ljava/lang/String;

    .line 187
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->g(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    .line 188
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/a;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->p:Lcgi;

    .line 189
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Lcgi;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->q:Lcom/twitter/model/moments/g;

    .line 190
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/g;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/moments/Moment;->s:J

    .line 191
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/moments/Moment$a;->b(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    .line 192
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/f;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/moments/Moment;->t:Z

    .line 193
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->e(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/moments/Moment;->u:J

    .line 194
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/moments/Moment$a;->c(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 174
    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/model/moments/Moment$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/moments/Moment$a;)Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/twitter/model/moments/Moment$a;->c:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/model/moments/Moment$a;)Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/twitter/model/moments/Moment$a;->d:Z

    return v0
.end method

.method static synthetic e(Lcom/twitter/model/moments/Moment$a;)Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/twitter/model/moments/Moment$a;->e:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/model/moments/Moment$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/model/moments/Moment$a;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/model/moments/Moment$a;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/model/moments/Moment$a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/model/moments/Moment$a;)Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/twitter/model/moments/Moment$a;->j:Z

    return v0
.end method

.method static synthetic k(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/model/moments/Moment$a;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/model/moments/Moment$a;)I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/twitter/model/moments/Moment$a;->l:I

    return v0
.end method

.method static synthetic m(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/model/moments/Moment$a;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/model/moments/Moment$a;)Lcom/twitter/model/moments/a;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/model/moments/Moment$a;->n:Lcom/twitter/model/moments/a;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/model/moments/Moment$a;)Lcgi;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/model/moments/Moment$a;->o:Lcgi;

    return-object v0
.end method

.method static synthetic p(Lcom/twitter/model/moments/Moment$a;)Lcom/twitter/model/moments/g;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/model/moments/Moment$a;->p:Lcom/twitter/model/moments/g;

    return-object v0
.end method

.method static synthetic q(Lcom/twitter/model/moments/Moment$a;)J
    .locals 2

    .prologue
    .line 150
    iget-wide v0, p0, Lcom/twitter/model/moments/Moment$a;->q:J

    return-wide v0
.end method

.method static synthetic r(Lcom/twitter/model/moments/Moment$a;)Lcom/twitter/model/moments/f;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/model/moments/Moment$a;->r:Lcom/twitter/model/moments/f;

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/model/moments/Moment$a;)Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/twitter/model/moments/Moment$a;->s:Z

    return v0
.end method

.method static synthetic t(Lcom/twitter/model/moments/Moment$a;)J
    .locals 2

    .prologue
    .line 150
    iget-wide v0, p0, Lcom/twitter/model/moments/Moment$a;->t:J

    return-wide v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 265
    iput p1, p0, Lcom/twitter/model/moments/Moment$a;->l:I

    .line 266
    return-object p0
.end method

.method public a(J)Lcom/twitter/model/moments/Moment$a;
    .locals 1

    .prologue
    .line 199
    iput-wide p1, p0, Lcom/twitter/model/moments/Moment$a;->a:J

    .line 200
    return-object p0
.end method

.method public a(Lcgi;)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/twitter/model/moments/Moment$a;->o:Lcgi;

    .line 278
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/a;)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/twitter/model/moments/Moment$a;->n:Lcom/twitter/model/moments/a;

    .line 284
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/f;)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/twitter/model/moments/Moment$a;->r:Lcom/twitter/model/moments/f;

    .line 302
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/g;)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/twitter/model/moments/Moment$a;->p:Lcom/twitter/model/moments/g;

    .line 290
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/twitter/model/moments/Moment$a;->b:Ljava/lang/String;

    .line 206
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 211
    iput-boolean p1, p0, Lcom/twitter/model/moments/Moment$a;->c:Z

    .line 212
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/moments/Moment$a;
    .locals 1

    .prologue
    .line 295
    iput-wide p1, p0, Lcom/twitter/model/moments/Moment$a;->q:J

    .line 296
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/twitter/model/moments/Moment$a;->f:Ljava/lang/String;

    .line 230
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 217
    iput-boolean p1, p0, Lcom/twitter/model/moments/Moment$a;->d:Z

    .line 218
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/twitter/model/moments/Moment$a;->e()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/twitter/model/moments/Moment$a;
    .locals 1

    .prologue
    .line 313
    iput-wide p1, p0, Lcom/twitter/model/moments/Moment$a;->t:J

    .line 314
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/twitter/model/moments/Moment$a;->g:Ljava/lang/String;

    .line 236
    return-object p0
.end method

.method public c(Z)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 223
    iput-boolean p1, p0, Lcom/twitter/model/moments/Moment$a;->e:Z

    .line 224
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/twitter/model/moments/Moment$a;->h:Ljava/lang/String;

    .line 242
    return-object p0
.end method

.method public d(Z)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 253
    iput-boolean p1, p0, Lcom/twitter/model/moments/Moment$a;->j:Z

    .line 254
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/twitter/model/moments/Moment$a;->i:Ljava/lang/String;

    .line 248
    return-object p0
.end method

.method public e(Z)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 307
    iput-boolean p1, p0, Lcom/twitter/model/moments/Moment$a;->s:Z

    .line 308
    return-object p0
.end method

.method protected e()Lcom/twitter/model/moments/Moment;
    .locals 2

    .prologue
    .line 320
    new-instance v0, Lcom/twitter/model/moments/Moment;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/moments/Moment;-><init>(Lcom/twitter/model/moments/Moment$a;Lcom/twitter/model/moments/Moment$1;)V

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/twitter/model/moments/Moment$a;->k:Ljava/lang/String;

    .line 260
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/twitter/model/moments/Moment$a;->m:Ljava/lang/String;

    .line 272
    return-object p0
.end method
