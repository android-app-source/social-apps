.class public Lcom/twitter/model/moments/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/d$b;,
        Lcom/twitter/model/moments/d$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/moments/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:Lcom/twitter/util/math/Size;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/model/moments/d$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/moments/d$b;-><init>(Lcom/twitter/model/moments/d$1;)V

    sput-object v0, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/moments/d$a;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iget v0, p1, Lcom/twitter/model/moments/d$a;->a:I

    iput v0, p0, Lcom/twitter/model/moments/d;->b:I

    .line 32
    iget v0, p1, Lcom/twitter/model/moments/d$a;->b:I

    iput v0, p0, Lcom/twitter/model/moments/d;->c:I

    .line 33
    iget v0, p1, Lcom/twitter/model/moments/d$a;->c:I

    iput v0, p0, Lcom/twitter/model/moments/d;->d:I

    .line 34
    iget v0, p1, Lcom/twitter/model/moments/d$a;->d:I

    iput v0, p0, Lcom/twitter/model/moments/d;->e:I

    .line 35
    iget-object v0, p1, Lcom/twitter/model/moments/d$a;->e:Lcom/twitter/util/math/Size;

    iput-object v0, p0, Lcom/twitter/model/moments/d;->f:Lcom/twitter/util/math/Size;

    .line 36
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/moments/d$a;Lcom/twitter/model/moments/d$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/model/moments/d;-><init>(Lcom/twitter/model/moments/d$a;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 68
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/twitter/model/moments/d;->b:I

    iget v2, p0, Lcom/twitter/model/moments/d;->c:I

    iget v3, p0, Lcom/twitter/model/moments/d;->b:I

    iget v4, p0, Lcom/twitter/model/moments/d;->d:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/twitter/model/moments/d;->c:I

    iget v5, p0, Lcom/twitter/model/moments/d;->e:I

    add-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method public b()F
    .locals 2

    .prologue
    .line 72
    iget v0, p0, Lcom/twitter/model/moments/d;->d:I

    int-to-float v0, v0

    iget v1, p0, Lcom/twitter/model/moments/d;->e:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 40
    if-ne p0, p1, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v0

    .line 43
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 44
    goto :goto_0

    .line 47
    :cond_3
    check-cast p1, Lcom/twitter/model/moments/d;

    .line 49
    iget v2, p0, Lcom/twitter/model/moments/d;->b:I

    iget v3, p1, Lcom/twitter/model/moments/d;->b:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/twitter/model/moments/d;->c:I

    iget v3, p1, Lcom/twitter/model/moments/d;->c:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/twitter/model/moments/d;->d:I

    iget v3, p1, Lcom/twitter/model/moments/d;->d:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/twitter/model/moments/d;->e:I

    iget v3, p1, Lcom/twitter/model/moments/d;->e:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/twitter/model/moments/d;->f:Lcom/twitter/util/math/Size;

    iget-object v3, p1, Lcom/twitter/model/moments/d;->f:Lcom/twitter/util/math/Size;

    .line 53
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 58
    iget v0, p0, Lcom/twitter/model/moments/d;->b:I

    .line 59
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/moments/d;->c:I

    .line 60
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, Lcom/twitter/model/moments/d;->d:I

    .line 61
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p0, Lcom/twitter/model/moments/d;->e:I

    .line 62
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/model/moments/d;->f:Lcom/twitter/util/math/Size;

    .line 58
    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
