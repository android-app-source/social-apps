.class public Lcom/twitter/model/moments/v$d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/v$d$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/moments/v$d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/twitter/model/moments/v$c;

.field public final c:Lcom/twitter/model/moments/v$b;

.field public final d:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 109
    new-instance v0, Lcom/twitter/model/moments/v$d$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/moments/v$d$a;-><init>(Lcom/twitter/model/moments/v$1;)V

    sput-object v0, Lcom/twitter/model/moments/v$d;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/moments/v$c;Lcom/twitter/model/moments/v$b;J)V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput-object p1, p0, Lcom/twitter/model/moments/v$d;->b:Lcom/twitter/model/moments/v$c;

    .line 118
    iput-object p2, p0, Lcom/twitter/model/moments/v$d;->c:Lcom/twitter/model/moments/v$b;

    .line 119
    iput-wide p3, p0, Lcom/twitter/model/moments/v$d;->d:J

    .line 120
    return-void
.end method
