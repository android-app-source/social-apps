.class Lcom/twitter/model/moments/v$e;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/moments/v;",
        "Lcom/twitter/model/moments/v$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/moments/v$1;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/twitter/model/moments/v$e;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/moments/v$a;
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/twitter/model/moments/v$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/v$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/moments/v$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 54
    sget-object v0, Lcom/twitter/model/moments/v$c;->a:Lcom/twitter/util/serialization/l;

    .line 55
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 54
    invoke-virtual {p2, v0}, Lcom/twitter/model/moments/v$a;->a(Ljava/util/List;)Lcom/twitter/model/moments/v$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/moments/v$b;->a:Lcom/twitter/util/serialization/l;

    .line 56
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/v$b;

    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/v$a;->a(Lcom/twitter/model/moments/v$b;)Lcom/twitter/model/moments/v$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/timeline/k;->a:Lcom/twitter/util/serialization/l;

    .line 57
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/k;

    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/v$a;->a(Lcom/twitter/model/timeline/k;)Lcom/twitter/model/moments/v$a;

    .line 58
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 42
    check-cast p2, Lcom/twitter/model/moments/v$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/moments/v$e;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/moments/v$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/v;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p2, Lcom/twitter/model/moments/v;->c:Ljava/util/List;

    sget-object v1, Lcom/twitter/model/moments/v$c;->a:Lcom/twitter/util/serialization/l;

    .line 64
    invoke-static {v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 63
    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/v;->d:Lcom/twitter/model/moments/v$b;

    sget-object v2, Lcom/twitter/model/moments/v$b;->a:Lcom/twitter/util/serialization/l;

    .line 65
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/v;->e:Lcom/twitter/model/timeline/k;

    sget-object v2, Lcom/twitter/model/timeline/k;->a:Lcom/twitter/util/serialization/l;

    .line 66
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 67
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    check-cast p2, Lcom/twitter/model/moments/v;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/v$e;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/v;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/twitter/model/moments/v$e;->a()Lcom/twitter/model/moments/v$a;

    move-result-object v0

    return-object v0
.end method
