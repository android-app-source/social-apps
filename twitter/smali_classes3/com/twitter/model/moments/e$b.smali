.class Lcom/twitter/model/moments/e$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/moments/e;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/i;-><init>(I)V

    .line 218
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/moments/e;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 235
    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    .line 236
    new-instance v0, Lcom/twitter/model/moments/e;

    sget-object v1, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    .line 237
    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/moments/d;

    sget-object v2, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    .line 238
    invoke-virtual {p1, v2}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/d;

    sget-object v3, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    .line 239
    invoke-virtual {p1, v3}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/moments/d;

    sget-object v4, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    .line 240
    invoke-virtual {p1, v4}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/moments/d;

    .line 241
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v5

    sget-object v6, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    invoke-direct/range {v0 .. v7}, Lcom/twitter/model/moments/e;-><init>(Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;ZLcom/twitter/util/math/Size;Lcom/twitter/model/moments/e$1;)V

    .line 244
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/model/moments/e;

    sget-object v1, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    .line 245
    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/moments/d;

    sget-object v2, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    .line 246
    invoke-virtual {p1, v2}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/d;

    sget-object v3, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    .line 247
    invoke-virtual {p1, v3}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/moments/d;

    sget-object v4, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    .line 248
    invoke-virtual {p1, v4}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/moments/d;

    .line 249
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v5

    sget-object v6, Lcom/twitter/util/math/Size;->a:Lcom/twitter/util/serialization/l;

    .line 250
    invoke-virtual {p1, v6}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/util/math/Size;

    invoke-direct/range {v0 .. v7}, Lcom/twitter/model/moments/e;-><init>(Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;ZLcom/twitter/util/math/Size;Lcom/twitter/model/moments/e$1;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/e;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    iget-object v0, p2, Lcom/twitter/model/moments/e;->c:Lcom/twitter/model/moments/d;

    sget-object v1, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/e;->d:Lcom/twitter/model/moments/d;

    sget-object v2, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    .line 224
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/e;->e:Lcom/twitter/model/moments/d;

    sget-object v2, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    .line 225
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/e;->f:Lcom/twitter/model/moments/d;

    sget-object v2, Lcom/twitter/model/moments/d;->a:Lcom/twitter/util/serialization/l;

    .line 226
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/moments/e;->h:Z

    .line 227
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/e;->g:Lcom/twitter/util/math/Size;

    sget-object v2, Lcom/twitter/util/math/Size;->a:Lcom/twitter/util/serialization/l;

    .line 228
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 229
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    check-cast p2, Lcom/twitter/model/moments/e;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/e$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/e;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 214
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/e$b;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/moments/e;

    move-result-object v0

    return-object v0
.end method
