.class Lcom/twitter/model/moments/v$c$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/v$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/moments/v$c;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/moments/v$1;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/twitter/model/moments/v$c$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/moments/v$c;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 95
    new-instance v2, Lcom/twitter/model/moments/v$c;

    sget-object v0, Lcom/twitter/model/moments/Moment;->a:Lcom/twitter/util/serialization/l;

    .line 96
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    sget-object v1, Lceo;->a:Lcom/twitter/util/serialization/l;

    .line 97
    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lceo;

    invoke-direct {v2, v0, v1}, Lcom/twitter/model/moments/v$c;-><init>(Lcom/twitter/model/moments/Moment;Lceo;)V

    .line 95
    return-object v2
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/v$c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p2, Lcom/twitter/model/moments/v$c;->b:Lcom/twitter/model/moments/Moment;

    sget-object v1, Lcom/twitter/model/moments/Moment;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/v$c;->c:Lceo;

    sget-object v2, Lceo;->a:Lcom/twitter/util/serialization/l;

    .line 88
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 89
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    check-cast p2, Lcom/twitter/model/moments/v$c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/v$c$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/v$c;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/v$c$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/moments/v$c;

    move-result-object v0

    return-object v0
.end method
