.class public Lcom/twitter/model/moments/n;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/n$b;,
        Lcom/twitter/model/moments/n$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/moments/n;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/model/moments/n$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/moments/n$b;-><init>(Lcom/twitter/model/moments/n$1;)V

    sput-object v0, Lcom/twitter/model/moments/n;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/moments/n$a;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iget-object v0, p1, Lcom/twitter/model/moments/n$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/n;->b:Ljava/lang/String;

    .line 28
    iget-object v0, p1, Lcom/twitter/model/moments/n$a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/n;->c:Ljava/lang/String;

    .line 29
    iget-object v0, p1, Lcom/twitter/model/moments/n$a;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/n;->d:Ljava/lang/String;

    .line 30
    iget-object v0, p1, Lcom/twitter/model/moments/n$a;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/n;->e:Ljava/lang/String;

    .line 31
    return-void
.end method
