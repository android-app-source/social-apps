.class Lcom/twitter/model/moments/g$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/moments/g;",
        "Lcom/twitter/model/moments/g$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/moments/g$1;)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/twitter/model/moments/g$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/moments/g$a;
    .locals 1

    .prologue
    .line 104
    new-instance v0, Lcom/twitter/model/moments/g$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/g$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/moments/g$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 110
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/moments/g$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/g$a;

    move-result-object v0

    .line 111
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/g$a;->b(Ljava/lang/String;)Lcom/twitter/model/moments/g$a;

    .line 112
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 100
    check-cast p2, Lcom/twitter/model/moments/g$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/moments/g$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/moments/g$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/g;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p2, Lcom/twitter/model/moments/g;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/g;->c:Ljava/lang/String;

    .line 117
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 118
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    check-cast p2, Lcom/twitter/model/moments/g;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/g$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/g;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/twitter/model/moments/g$b;->a()Lcom/twitter/model/moments/g$a;

    move-result-object v0

    return-object v0
.end method
