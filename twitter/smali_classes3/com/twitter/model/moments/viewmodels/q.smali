.class public Lcom/twitter/model/moments/viewmodels/q;
.super Lcom/twitter/model/moments/viewmodels/n;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/moments/viewmodels/r;
.implements Lcom/twitter/model/moments/viewmodels/s;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/viewmodels/q$b;,
        Lcom/twitter/model/moments/viewmodels/q$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/moments/w;


# direct methods
.method protected constructor <init>(Lcom/twitter/model/moments/viewmodels/q$a;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/model/moments/viewmodels/n;-><init>(Lcom/twitter/model/moments/viewmodels/n$a;)V

    .line 21
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/q$a;->k:Lcom/twitter/model/moments/w;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/q;->a:Lcom/twitter/model/moments/w;

    .line 22
    return-void
.end method

.method static synthetic a(Lcom/twitter/model/moments/viewmodels/q;)Lcom/twitter/model/moments/w;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/q;->a:Lcom/twitter/model/moments/w;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/w;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/twitter/model/moments/viewmodels/q$b;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/viewmodels/q$b;-><init>(Lcom/twitter/model/moments/viewmodels/q;)V

    invoke-virtual {v0, p1}, Lcom/twitter/model/moments/viewmodels/q$b;->a(Lcom/twitter/model/moments/w;)Lcom/twitter/model/moments/viewmodels/q$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/q$b;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/q$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    return-object v0
.end method

.method public e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->a:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    return-object v0
.end method

.method public m()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 46
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 47
    invoke-super {p0}, Lcom/twitter/model/moments/viewmodels/n;->m()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 48
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/q;->w()Lcom/twitter/model/moments/w;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 46
    return-object v0
.end method

.method public synthetic p()Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/q;->s()Lcom/twitter/model/moments/viewmodels/q$a;

    move-result-object v0

    return-object v0
.end method

.method public s()Lcom/twitter/model/moments/viewmodels/q$a;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/twitter/model/moments/viewmodels/q$b;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/viewmodels/q$b;-><init>(Lcom/twitter/model/moments/viewmodels/q;)V

    return-object v0
.end method

.method public w()Lcom/twitter/model/moments/w;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/q;->a:Lcom/twitter/model/moments/w;

    return-object v0
.end method
