.class public abstract Lcom/twitter/model/moments/viewmodels/MomentPage$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/viewmodels/MomentPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/model/moments/viewmodels/MomentPage;",
        "B:",
        "Lcom/twitter/model/moments/viewmodels/MomentPage$a",
        "<TT;TB;>;>",
        "Lcom/twitter/util/object/i",
        "<TT;>;"
    }
.end annotation


# instance fields
.field b:Lcom/twitter/model/moments/Moment;

.field c:Lcom/twitter/model/moments/r;

.field d:Lcom/twitter/model/moments/MomentPageDisplayMode;

.field e:Lcom/twitter/model/moments/n;

.field f:Lcom/twitter/model/moments/o;

.field g:Lcom/twitter/model/moments/s;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 128
    sget-object v0, Lcom/twitter/model/moments/MomentPageDisplayMode;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->d:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 134
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 128
    sget-object v0, Lcom/twitter/model/moments/MomentPageDisplayMode;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->d:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 137
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->f()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->b:Lcom/twitter/model/moments/Moment;

    .line 138
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->c:Lcom/twitter/model/moments/r;

    .line 139
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->h()Lcom/twitter/model/moments/MomentPageDisplayMode;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->d:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 140
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->q()Lcom/twitter/model/moments/n;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->e:Lcom/twitter/model/moments/n;

    .line 141
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->r()Lcom/twitter/model/moments/o;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->f:Lcom/twitter/model/moments/o;

    .line 142
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->n()Lcom/twitter/model/moments/s;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->g:Lcom/twitter/model/moments/s;

    .line 143
    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->c:Lcom/twitter/model/moments/r;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/Moment;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 147
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->b:Lcom/twitter/model/moments/Moment;

    .line 148
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/MomentPageDisplayMode;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 159
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->d:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 160
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/n;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/n;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 165
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->e:Lcom/twitter/model/moments/n;

    .line 166
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/o;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/o;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 171
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->f:Lcom/twitter/model/moments/o;

    .line 172
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/r;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 153
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->c:Lcom/twitter/model/moments/r;

    .line 154
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/s;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/s;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 177
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->g:Lcom/twitter/model/moments/s;

    .line 178
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    return-object v0
.end method
