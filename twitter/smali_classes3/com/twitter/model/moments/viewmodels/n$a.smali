.class public abstract Lcom/twitter/model/moments/viewmodels/n$a;
.super Lcom/twitter/model/moments/viewmodels/HydratableMomentPage$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/viewmodels/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/model/moments/viewmodels/n;",
        "B:",
        "Lcom/twitter/model/moments/viewmodels/n$a",
        "<TT;TB;>;>",
        "Lcom/twitter/model/moments/viewmodels/HydratableMomentPage$a",
        "<TT;TB;>;"
    }
.end annotation


# instance fields
.field h:Lcom/twitter/model/core/Tweet;

.field i:Lcom/twitter/model/moments/l;

.field j:J


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage$a;-><init>()V

    .line 64
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/model/moments/viewmodels/n;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage$a;-><init>(Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;)V

    .line 68
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/n;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/n$a;->h:Lcom/twitter/model/core/Tweet;

    .line 69
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/n;->v()Lcom/twitter/model/moments/l;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/n$a;->i:Lcom/twitter/model/moments/l;

    .line 70
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/n;->t()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/moments/viewmodels/n$a;->j:J

    .line 71
    return-void
.end method


# virtual methods
.method public a(J)Lcom/twitter/model/moments/viewmodels/n$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TB;"
        }
    .end annotation

    .prologue
    .line 91
    iput-wide p1, p0, Lcom/twitter/model/moments/viewmodels/n$a;->j:J

    .line 92
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/n$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/moments/viewmodels/n$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 85
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/n$a;->h:Lcom/twitter/model/core/Tweet;

    .line 86
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/n$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/Tweet;J)Lcom/twitter/model/moments/viewmodels/n$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "J)TB;"
        }
    .end annotation

    .prologue
    .line 75
    if-eqz p1, :cond_0

    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->G:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 76
    invoke-virtual {p0, p1}, Lcom/twitter/model/moments/viewmodels/n$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/moments/viewmodels/n$a;

    .line 80
    :goto_0
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/n$a;

    return-object v0

    .line 78
    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/twitter/model/moments/viewmodels/n$a;->a(J)Lcom/twitter/model/moments/viewmodels/n$a;

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/moments/l;)Lcom/twitter/model/moments/viewmodels/n$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/l;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 97
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/n$a;->i:Lcom/twitter/model/moments/l;

    .line 98
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/n$a;

    return-object v0
.end method

.method protected c_()V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage$a;->c_()V

    .line 104
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/n$a;->h:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/n$a;->h:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    iput-wide v0, p0, Lcom/twitter/model/moments/viewmodels/n$a;->j:J

    .line 107
    :cond_0
    return-void
.end method
