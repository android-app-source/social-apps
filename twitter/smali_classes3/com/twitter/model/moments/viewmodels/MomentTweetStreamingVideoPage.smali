.class public Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;
.super Lcom/twitter/model/moments/viewmodels/n;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/moments/viewmodels/c;
.implements Lcom/twitter/model/moments/viewmodels/e;
.implements Lcom/twitter/model/moments/viewmodels/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;,
        Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

.field public final b:Lcom/twitter/model/moments/e;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/twitter/model/moments/viewmodels/n;-><init>(Lcom/twitter/model/moments/viewmodels/n$a;)V

    .line 31
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->k:Lcom/twitter/model/moments/e;

    sget-object v1, Lcom/twitter/model/moments/e;->b:Lcom/twitter/model/moments/e;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/e;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->b:Lcom/twitter/model/moments/e;

    .line 32
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->l:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;-><init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;)V

    invoke-static {p0, v0, p1}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/viewmodels/MomentPage$a;Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->s()Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    return-object v0
.end method

.method public c()Lcom/twitter/model/moments/e;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->b:Lcom/twitter/model/moments/e;

    return-object v0
.end method

.method public e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->c:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    return-object v0
.end method

.method public m()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 80
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 81
    invoke-super {p0}, Lcom/twitter/model/moments/viewmodels/n;->m()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->b:Lcom/twitter/model/moments/e;

    .line 82
    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 80
    return-object v0
.end method

.method public synthetic p()Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->s()Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    move-result-object v0

    return-object v0
.end method

.method public s()Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;-><init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;)V

    return-object v0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->b:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->c:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public x()Z
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->c:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->b:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->e:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
