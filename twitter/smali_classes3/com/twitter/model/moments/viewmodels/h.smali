.class public Lcom/twitter/model/moments/viewmodels/h;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 17
    instance-of v0, p0, Lcom/twitter/model/moments/viewmodels/n;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/twitter/model/moments/viewmodels/n;

    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/n;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/twitter/model/moments/e;Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/e;
    .locals 2

    .prologue
    .line 60
    sget-object v0, Lcom/twitter/model/moments/MomentPageDisplayMode;->a:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/twitter/model/moments/MomentPageDisplayMode;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-ne p1, v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/model/moments/e;->h:Z

    if-eqz v0, :cond_1

    .line 62
    invoke-static {p0}, Lcom/twitter/model/moments/e$a;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/e$a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/e$a;->a(Z)Lcom/twitter/model/moments/e$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/e;

    .line 64
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 33
    instance-of v0, p0, Lcom/twitter/model/moments/viewmodels/c;

    if-eqz v0, :cond_0

    .line 34
    check-cast p0, Lcom/twitter/model/moments/viewmodels/c;

    invoke-interface {p0, p1}, Lcom/twitter/model/moments/viewmodels/c;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object p0

    .line 36
    :cond_0
    return-object p0
.end method

.method public static a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/viewmodels/MomentPage$a;Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ":",
            "Lcom/twitter/model/moments/viewmodels/e;",
            "B:",
            "Lcom/twitter/model/moments/viewmodels/MomentPage$a",
            "<TP;TB;>;:",
            "Lcom/twitter/model/moments/viewmodels/d",
            "<TP;TB;>;>(TP;TB;",
            "Lcom/twitter/model/moments/MomentPageDisplayMode;",
            ")TP;"
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/moments/r$a;->a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/r$a;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/model/moments/r$a;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/r$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/r$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/r;

    .line 53
    check-cast p0, Lcom/twitter/model/moments/viewmodels/e;

    invoke-interface {p0}, Lcom/twitter/model/moments/viewmodels/e;->c()Lcom/twitter/model/moments/e;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/e;Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/e;

    move-result-object v1

    .line 54
    invoke-virtual {p1, p2}, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/d;

    invoke-interface {v0, v1}, Lcom/twitter/model/moments/viewmodels/d;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    return-object v0
.end method

.method public static a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/w;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 41
    instance-of v0, p0, Lcom/twitter/model/moments/viewmodels/s;

    if-eqz v0, :cond_0

    .line 42
    check-cast p0, Lcom/twitter/model/moments/viewmodels/s;

    invoke-interface {p0, p1}, Lcom/twitter/model/moments/viewmodels/s;->a(Lcom/twitter/model/moments/w;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object p0

    .line 44
    :cond_0
    return-object p0
.end method

.method public static b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 22
    instance-of v0, p0, Lcom/twitter/model/moments/viewmodels/o;

    if-eqz v0, :cond_0

    .line 23
    check-cast p0, Lcom/twitter/model/moments/viewmodels/o;

    .line 24
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/o;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/o;->s()Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    iget-wide v0, v0, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 28
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
