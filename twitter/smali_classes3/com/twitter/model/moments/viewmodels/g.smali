.class public Lcom/twitter/model/moments/viewmodels/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/twitter/model/moments/viewmodels/MomentPage;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 33
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    .line 34
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/moments/r;I)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lcom/twitter/model/moments/viewmodels/g;->f(Lcom/twitter/model/moments/r;)I

    move-result v0

    add-int/2addr v0, p2

    .line 105
    iget-object v1, p0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_0

    if-ltz v0, :cond_0

    .line 106
    iget-object v1, p0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 108
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static varargs a([Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/g;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/model/moments/viewmodels/g;

    invoke-static {p0}, Lcom/twitter/util/collection/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/model/moments/viewmodels/g;-><init>(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 2

    .prologue
    .line 72
    invoke-static {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->a(Lcom/twitter/model/moments/r;)Lcpv;

    move-result-object v0

    invoke-static {p0, v0}, Lcpt;->c(Ljava/lang/Iterable;Lcpv;)I

    move-result v0

    .line 73
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lcom/twitter/model/moments/MomentPageDisplayMode;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->h()Lcom/twitter/model/moments/MomentPageDisplayMode;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/model/moments/r;)Z
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/model/moments/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c(Lcom/twitter/model/moments/r;)Z
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/model/moments/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/model/moments/viewmodels/g;->a(Lcom/twitter/model/moments/r;I)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    return-object v0
.end method

.method public e(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 91
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/model/moments/viewmodels/g;->a(Lcom/twitter/model/moments/r;I)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 48
    if-ne p0, p1, :cond_0

    .line 49
    const/4 v0, 0x1

    .line 56
    :goto_0
    return v0

    .line 51
    :cond_0
    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/g;

    if-nez v0, :cond_1

    .line 52
    const/4 v0, 0x0

    goto :goto_0

    .line 55
    :cond_1
    check-cast p1, Lcom/twitter/model/moments/viewmodels/g;

    .line 56
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public f(Lcom/twitter/model/moments/r;)I
    .locals 1

    .prologue
    .line 99
    invoke-static {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->a(Lcom/twitter/model/moments/r;)Lcpv;

    move-result-object v0

    invoke-static {p0, v0}, Lcpt;->c(Ljava/lang/Iterable;Lcpv;)I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/util/List;)I

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
