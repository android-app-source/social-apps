.class public Lcom/twitter/model/moments/viewmodels/p;
.super Lcom/twitter/model/moments/viewmodels/n;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/moments/viewmodels/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/viewmodels/p$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/moments/e;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/p$a;)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/twitter/model/moments/viewmodels/n;-><init>(Lcom/twitter/model/moments/viewmodels/n$a;)V

    .line 18
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/p$a;->k:Lcom/twitter/model/moments/e;

    sget-object v1, Lcom/twitter/model/moments/e;->b:Lcom/twitter/model/moments/e;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/e;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/p;->a:Lcom/twitter/model/moments/e;

    .line 19
    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/twitter/model/moments/viewmodels/p;->b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/p;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/p;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/p;->c()Lcom/twitter/model/moments/viewmodels/p$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/model/moments/viewmodels/p$a;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/p$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/p$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/p;

    return-object v0
.end method

.method public c()Lcom/twitter/model/moments/viewmodels/p$a;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/twitter/model/moments/viewmodels/p$a;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/viewmodels/p$a;-><init>(Lcom/twitter/model/moments/viewmodels/p;)V

    return-object v0
.end method

.method public e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->d:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    return-object v0
.end method

.method public m()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 43
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 44
    invoke-super {p0}, Lcom/twitter/model/moments/viewmodels/n;->m()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/viewmodels/p;->a:Lcom/twitter/model/moments/e;

    .line 45
    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 43
    return-object v0
.end method

.method public synthetic p()Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/p;->c()Lcom/twitter/model/moments/viewmodels/p$a;

    move-result-object v0

    return-object v0
.end method
