.class public Lcom/twitter/model/moments/viewmodels/a$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/viewmodels/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Lcom/twitter/model/moments/Moment;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcfn;

.field private d:Lcom/twitter/model/moments/viewmodels/MomentPage;

.field private e:Lcom/twitter/model/moments/viewmodels/MomentPage;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/moments/viewmodels/a$a;)Lcom/twitter/model/moments/Moment;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/a$a;->a:Lcom/twitter/model/moments/Moment;

    return-object v0
.end method

.method public static a(Lcom/twitter/model/moments/viewmodels/a;)Lcom/twitter/model/moments/viewmodels/a$a;
    .locals 2

    .prologue
    .line 163
    new-instance v0, Lcom/twitter/model/moments/viewmodels/a$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/viewmodels/a$a;-><init>()V

    .line 164
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    .line 165
    invoke-static {p0}, Lcom/twitter/model/moments/viewmodels/a;->a(Lcom/twitter/model/moments/viewmodels/a;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Ljava/util/List;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    .line 166
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/a;->e()Lcfn;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Lcfn;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    .line 167
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/a;->c()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    .line 168
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/a;->d()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/a$a;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    .line 163
    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/moments/viewmodels/a$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/a$a;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/moments/viewmodels/a$a;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/a$a;->d:Lcom/twitter/model/moments/viewmodels/MomentPage;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/moments/viewmodels/a$a;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/a$a;->e:Lcom/twitter/model/moments/viewmodels/MomentPage;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/model/moments/viewmodels/a$a;)Lcfn;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/a$a;->c:Lcfn;

    return-object v0
.end method


# virtual methods
.method public a(Lcfn;)Lcom/twitter/model/moments/viewmodels/a$a;
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/a$a;->c:Lcfn;

    .line 198
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/a$a;
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/a$a;->a:Lcom/twitter/model/moments/Moment;

    .line 174
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/a$a;
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/a$a;->d:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 186
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/moments/viewmodels/a$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;)",
            "Lcom/twitter/model/moments/viewmodels/a$a;"
        }
    .end annotation

    .prologue
    .line 179
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/a$a;->b:Ljava/util/List;

    .line 180
    return-object p0
.end method

.method public a()Lcom/twitter/model/moments/viewmodels/a;
    .locals 2

    .prologue
    .line 203
    new-instance v0, Lcom/twitter/model/moments/viewmodels/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/moments/viewmodels/a;-><init>(Lcom/twitter/model/moments/viewmodels/a$a;Lcom/twitter/model/moments/viewmodels/a$1;)V

    return-object v0
.end method

.method public b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/a$a;
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/a$a;->e:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 192
    return-object p0
.end method
