.class public final Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;
.super Lcom/twitter/model/moments/viewmodels/n$a;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/moments/viewmodels/d;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NullableEnum"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/moments/viewmodels/n$a",
        "<",
        "Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;",
        "Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;",
        ">;",
        "Lcom/twitter/model/moments/viewmodels/d",
        "<",
        "Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;",
        "Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;",
        ">;"
    }
.end annotation


# instance fields
.field k:Lcom/twitter/model/moments/e;

.field l:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/twitter/model/moments/viewmodels/n$a;-><init>()V

    .line 94
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/twitter/model/moments/viewmodels/n$a;-><init>(Lcom/twitter/model/moments/viewmodels/n;)V

    .line 98
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->b:Lcom/twitter/model/moments/e;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->k:Lcom/twitter/model/moments/e;

    .line 99
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->l:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    .line 100
    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->l:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    .line 118
    return-object p0
.end method

.method public b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->k:Lcom/twitter/model/moments/e;

    .line 112
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->e()Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;-><init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;)V

    return-object v0
.end method
