.class public abstract Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;
.super Lcom/twitter/model/moments/viewmodels/MomentPage;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/viewmodels/HydratableMomentPage$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method protected constructor <init>(Lcom/twitter/model/moments/viewmodels/HydratableMomentPage$a;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;-><init>(Lcom/twitter/model/moments/viewmodels/MomentPage$a;)V

    .line 15
    new-instance v0, Lcom/twitter/util/p;

    invoke-direct {v0}, Lcom/twitter/util/p;-><init>()V

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->a:Lcom/twitter/util/p;

    .line 21
    iget-boolean v0, p1, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage$a;->a:Z

    iput-boolean v0, p0, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->b:Z

    .line 22
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/q;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    :cond_0
    invoke-interface {p1, p0}, Lcom/twitter/util/q;->onEvent(Ljava/lang/Object;)V

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->a:Lcom/twitter/util/p;

    invoke-virtual {v0, p1}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->b:Z

    .line 32
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->d()V

    .line 33
    return-void
.end method

.method public abstract a()Z
.end method

.method public b(Lcom/twitter/util/q;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->a:Lcom/twitter/util/p;

    invoke-virtual {v0, p1}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 68
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->b:Z

    return v0
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->a:Lcom/twitter/util/p;

    invoke-virtual {v0, p0}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    .line 37
    return-void
.end method
