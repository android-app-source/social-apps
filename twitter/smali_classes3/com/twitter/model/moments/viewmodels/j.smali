.class public Lcom/twitter/model/moments/viewmodels/j;
.super Lcom/twitter/model/moments/viewmodels/MomentPage;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/viewmodels/j$b;,
        Lcom/twitter/model/moments/viewmodels/j$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/twitter/util/math/Size;

.field public final c:Lcom/twitter/model/moments/e;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/j$a;)V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;-><init>(Lcom/twitter/model/moments/viewmodels/MomentPage$a;)V

    .line 19
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/j$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/j;->a:Ljava/lang/String;

    .line 20
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/j$a;->h:Lcom/twitter/util/math/Size;

    sget-object v1, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/math/Size;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/j;->b:Lcom/twitter/util/math/Size;

    .line 21
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/j$a;->i:Lcom/twitter/model/moments/e;

    sget-object v1, Lcom/twitter/model/moments/e;->b:Lcom/twitter/model/moments/e;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/e;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/j;->c:Lcom/twitter/model/moments/e;

    .line 22
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/viewmodels/j$a;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/twitter/model/moments/viewmodels/j$b;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/viewmodels/j$b;-><init>(Lcom/twitter/model/moments/viewmodels/j;)V

    return-object v0
.end method

.method public e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->f:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    return-object v0
.end method

.method public synthetic p()Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/j;->a()Lcom/twitter/model/moments/viewmodels/j$a;

    move-result-object v0

    return-object v0
.end method
