.class public final Lcom/twitter/model/moments/viewmodels/o$a;
.super Lcom/twitter/model/moments/viewmodels/n$a;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/moments/viewmodels/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/viewmodels/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/moments/viewmodels/n$a",
        "<",
        "Lcom/twitter/model/moments/viewmodels/o;",
        "Lcom/twitter/model/moments/viewmodels/o$a;",
        ">;",
        "Lcom/twitter/model/moments/viewmodels/d",
        "<",
        "Lcom/twitter/model/moments/viewmodels/o;",
        "Lcom/twitter/model/moments/viewmodels/o$a;",
        ">;"
    }
.end annotation


# instance fields
.field k:J

.field l:Lcom/twitter/model/moments/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/twitter/model/moments/viewmodels/n$a;-><init>()V

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/o;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/twitter/model/moments/viewmodels/n$a;-><init>(Lcom/twitter/model/moments/viewmodels/n;)V

    .line 90
    iget-wide v0, p1, Lcom/twitter/model/moments/viewmodels/o;->a:J

    iput-wide v0, p0, Lcom/twitter/model/moments/viewmodels/o$a;->k:J

    .line 91
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/o;->b:Lcom/twitter/model/moments/e;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/o$a;->l:Lcom/twitter/model/moments/e;

    .line 92
    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/twitter/model/moments/viewmodels/o$a;->b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/o$a;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/twitter/model/moments/viewmodels/o$a;
    .locals 1

    .prologue
    .line 102
    iput-wide p1, p0, Lcom/twitter/model/moments/viewmodels/o$a;->k:J

    .line 103
    return-object p0
.end method

.method public b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/o$a;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/twitter/model/moments/viewmodels/o$a;->l:Lcom/twitter/model/moments/e;

    .line 110
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/o$a;->e()Lcom/twitter/model/moments/viewmodels/o;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/moments/viewmodels/o;
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/twitter/model/moments/viewmodels/o;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/viewmodels/o;-><init>(Lcom/twitter/model/moments/viewmodels/o$a;)V

    return-object v0
.end method
