.class public Lcom/twitter/model/moments/viewmodels/o;
.super Lcom/twitter/model/moments/viewmodels/n;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/moments/viewmodels/c;
.implements Lcom/twitter/model/moments/viewmodels/e;
.implements Lcom/twitter/model/moments/viewmodels/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/viewmodels/o$a;
    }
.end annotation


# static fields
.field static final synthetic c:Z


# instance fields
.field public final a:J

.field public final b:Lcom/twitter/model/moments/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/twitter/model/moments/viewmodels/o;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/model/moments/viewmodels/o;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/o$a;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/twitter/model/moments/viewmodels/n;-><init>(Lcom/twitter/model/moments/viewmodels/n$a;)V

    .line 24
    iget-wide v0, p1, Lcom/twitter/model/moments/viewmodels/o$a;->k:J

    iput-wide v0, p0, Lcom/twitter/model/moments/viewmodels/o;->a:J

    .line 25
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/o$a;->l:Lcom/twitter/model/moments/e;

    sget-object v1, Lcom/twitter/model/moments/e;->b:Lcom/twitter/model/moments/e;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/e;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/o;->b:Lcom/twitter/model/moments/e;

    .line 26
    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/twitter/model/moments/viewmodels/o;->b(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/o;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/twitter/model/moments/viewmodels/o;->b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/o;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/o;
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/twitter/model/moments/viewmodels/o$a;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/viewmodels/o$a;-><init>(Lcom/twitter/model/moments/viewmodels/o;)V

    invoke-static {p0, v0, p1}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/viewmodels/MomentPage$a;Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/o;

    return-object v0
.end method

.method public b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/o;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/o;->w()Lcom/twitter/model/moments/viewmodels/o$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/model/moments/viewmodels/o$a;->b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/o$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/o$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/o;

    return-object v0
.end method

.method public c()Lcom/twitter/model/moments/e;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/o;->b:Lcom/twitter/model/moments/e;

    return-object v0
.end method

.method public e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->b:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    return-object v0
.end method

.method public m()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 73
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 74
    invoke-super {p0}, Lcom/twitter/model/moments/viewmodels/n;->m()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/viewmodels/o;->b:Lcom/twitter/model/moments/e;

    .line 75
    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 73
    return-object v0
.end method

.method public synthetic p()Lcom/twitter/model/moments/viewmodels/MomentPage$a;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/o;->w()Lcom/twitter/model/moments/viewmodels/o$a;

    move-result-object v0

    return-object v0
.end method

.method public s()Lcom/twitter/model/core/MediaEntity;
    .locals 4

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/o;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/o;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 38
    sget-boolean v1, Lcom/twitter/model/moments/viewmodels/o;->c:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/o;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    .line 40
    iget-wide v2, p0, Lcom/twitter/model/moments/viewmodels/o;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/k;->a(J)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 42
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Lcom/twitter/model/moments/viewmodels/o$a;
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/twitter/model/moments/viewmodels/o$a;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/viewmodels/o$a;-><init>(Lcom/twitter/model/moments/viewmodels/o;)V

    return-object v0
.end method
