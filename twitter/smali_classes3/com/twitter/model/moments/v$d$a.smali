.class Lcom/twitter/model/moments/v$d$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/v$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/moments/v$d;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/moments/v$1;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/twitter/model/moments/v$d$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/moments/v$d;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 136
    new-instance v2, Lcom/twitter/model/moments/v$d;

    sget-object v0, Lcom/twitter/model/moments/v$c;->a:Lcom/twitter/util/serialization/l;

    .line 137
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/v$c;

    sget-object v1, Lcom/twitter/model/moments/v$b;->a:Lcom/twitter/util/serialization/l;

    .line 138
    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/moments/v$b;

    .line 139
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v4

    invoke-direct {v2, v0, v1, v4, v5}, Lcom/twitter/model/moments/v$d;-><init>(Lcom/twitter/model/moments/v$c;Lcom/twitter/model/moments/v$b;J)V

    .line 136
    return-object v2
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/v$d;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p2, Lcom/twitter/model/moments/v$d;->b:Lcom/twitter/model/moments/v$c;

    sget-object v1, Lcom/twitter/model/moments/v$c;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/v$d;->c:Lcom/twitter/model/moments/v$b;

    sget-object v2, Lcom/twitter/model/moments/v$b;->a:Lcom/twitter/util/serialization/l;

    .line 128
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/moments/v$d;->d:J

    .line 129
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 130
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    check-cast p2, Lcom/twitter/model/moments/v$d;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/v$d$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/v$d;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 122
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/v$d$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/moments/v$d;

    move-result-object v0

    return-object v0
.end method
