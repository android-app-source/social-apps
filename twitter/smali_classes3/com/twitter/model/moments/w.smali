.class public Lcom/twitter/model/moments/w;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/w$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/moments/w;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:I

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/model/moments/w$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/moments/w$a;-><init>(Lcom/twitter/model/moments/w$1;)V

    sput-object v0, Lcom/twitter/model/moments/w;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/twitter/model/moments/w;->c:I

    .line 26
    iput p2, p0, Lcom/twitter/model/moments/w;->b:I

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 36
    if-ne p0, p1, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v0

    .line 39
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 40
    goto :goto_0

    .line 43
    :cond_3
    check-cast p1, Lcom/twitter/model/moments/w;

    .line 45
    iget v2, p0, Lcom/twitter/model/moments/w;->b:I

    iget v3, p1, Lcom/twitter/model/moments/w;->b:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/twitter/model/moments/w;->c:I

    iget v3, p1, Lcom/twitter/model/moments/w;->c:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 50
    iget v0, p0, Lcom/twitter/model/moments/w;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/moments/w;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
