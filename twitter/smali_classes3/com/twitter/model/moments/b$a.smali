.class public final Lcom/twitter/model/moments/b$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/moments/b;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/model/moments/b$a;->f:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/moments/b$a;)I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/twitter/model/moments/b$a;->f:I

    return v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/model/moments/b$a;
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lcom/twitter/model/moments/b$a;->f:I

    .line 113
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/moments/b$a;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/twitter/model/moments/b$a;->a:Ljava/lang/String;

    .line 83
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/moments/b$a;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/twitter/model/moments/b$a;->b:Ljava/lang/String;

    .line 89
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/twitter/model/moments/b$a;->e()Lcom/twitter/model/moments/b;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/moments/b$a;
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/twitter/model/moments/b$a;->c:Ljava/lang/String;

    .line 95
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/moments/b$a;
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/twitter/model/moments/b$a;->d:Ljava/lang/String;

    .line 101
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/moments/b$a;
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/twitter/model/moments/b$a;->e:Ljava/lang/String;

    .line 107
    return-object p0
.end method

.method protected e()Lcom/twitter/model/moments/b;
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/twitter/model/moments/b;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/b;-><init>(Lcom/twitter/model/moments/b$a;)V

    return-object v0
.end method
