.class public Lcom/twitter/model/moments/f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/f$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/moments/f;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/twitter/model/moments/f;


# instance fields
.field public final c:Lcom/twitter/model/moments/MomentVisibilityMode;

.field public final d:Ljava/lang/Boolean;

.field public final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 15
    new-instance v0, Lcom/twitter/model/moments/f$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/f$a;-><init>()V

    sput-object v0, Lcom/twitter/model/moments/f;->a:Lcom/twitter/util/serialization/l;

    .line 16
    new-instance v0, Lcom/twitter/model/moments/f;

    sget-object v1, Lcom/twitter/model/moments/MomentVisibilityMode;->b:Lcom/twitter/model/moments/MomentVisibilityMode;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/model/moments/f;-><init>(Lcom/twitter/model/moments/MomentVisibilityMode;Ljava/lang/Boolean;Z)V

    sput-object v0, Lcom/twitter/model/moments/f;->b:Lcom/twitter/model/moments/f;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/moments/MomentVisibilityMode;Ljava/lang/Boolean;Z)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/twitter/model/moments/f;->c:Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 27
    iput-object p2, p0, Lcom/twitter/model/moments/f;->d:Ljava/lang/Boolean;

    .line 28
    iput-boolean p3, p0, Lcom/twitter/model/moments/f;->e:Z

    .line 29
    return-void
.end method
