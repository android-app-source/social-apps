.class public final Lcom/twitter/model/moments/d$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/moments/d;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:Lcom/twitter/util/math/Size;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 81
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    iput-object v0, p0, Lcom/twitter/model/moments/d$a;->e:Lcom/twitter/util/math/Size;

    return-void
.end method

.method public static a(Landroid/graphics/Rect;Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/d$a;
    .locals 2

    .prologue
    .line 85
    new-instance v0, Lcom/twitter/model/moments/d$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/d$a;-><init>()V

    iget v1, p0, Landroid/graphics/Rect;->left:I

    .line 86
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/d$a;->a(I)Lcom/twitter/model/moments/d$a;

    move-result-object v0

    iget v1, p0, Landroid/graphics/Rect;->top:I

    .line 87
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/d$a;->b(I)Lcom/twitter/model/moments/d$a;

    move-result-object v0

    .line 88
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/d$a;->c(I)Lcom/twitter/model/moments/d$a;

    move-result-object v0

    .line 89
    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/d$a;->d(I)Lcom/twitter/model/moments/d$a;

    move-result-object v0

    .line 90
    invoke-virtual {v0, p1}, Lcom/twitter/model/moments/d$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/d$a;

    move-result-object v0

    .line 85
    return-object v0
.end method

.method public static a(Lcom/twitter/model/moments/d;)Lcom/twitter/model/moments/d$a;
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/twitter/model/moments/d;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/d;->f:Lcom/twitter/util/math/Size;

    invoke-static {v0, v1}, Lcom/twitter/model/moments/d$a;->a(Landroid/graphics/Rect;Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/d$a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/model/moments/d$a;
    .locals 0

    .prologue
    .line 100
    iput p1, p0, Lcom/twitter/model/moments/d$a;->a:I

    .line 101
    return-object p0
.end method

.method public a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/d$a;
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/twitter/model/moments/d$a;->e:Lcom/twitter/util/math/Size;

    .line 125
    return-object p0
.end method

.method public b(I)Lcom/twitter/model/moments/d$a;
    .locals 0

    .prologue
    .line 106
    iput p1, p0, Lcom/twitter/model/moments/d$a;->b:I

    .line 107
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/twitter/model/moments/d$a;->e()Lcom/twitter/model/moments/d;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/twitter/model/moments/d$a;
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lcom/twitter/model/moments/d$a;->c:I

    .line 113
    return-object p0
.end method

.method public d(I)Lcom/twitter/model/moments/d$a;
    .locals 0

    .prologue
    .line 118
    iput p1, p0, Lcom/twitter/model/moments/d$a;->d:I

    .line 119
    return-object p0
.end method

.method protected e()Lcom/twitter/model/moments/d;
    .locals 2

    .prologue
    .line 131
    new-instance v0, Lcom/twitter/model/moments/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/moments/d;-><init>(Lcom/twitter/model/moments/d$a;Lcom/twitter/model/moments/d$1;)V

    return-object v0
.end method
