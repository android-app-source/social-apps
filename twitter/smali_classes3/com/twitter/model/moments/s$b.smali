.class Lcom/twitter/model/moments/s$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/moments/s;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/moments/s$1;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/twitter/model/moments/s$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/moments/s;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 41
    new-instance v1, Lcom/twitter/model/moments/s$a;

    invoke-direct {v1}, Lcom/twitter/model/moments/s$a;-><init>()V

    const-class v0, Lcom/twitter/model/moments/MomentSocialProofType;

    .line 43
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/MomentSocialProofType;

    .line 42
    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/s$a;->a(Lcom/twitter/model/moments/MomentSocialProofType;)Lcom/twitter/model/moments/s$a;

    move-result-object v0

    .line 44
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/s$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/s$a;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcom/twitter/model/moments/s$a;->e()Lcom/twitter/model/moments/s;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/s;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p2, Lcom/twitter/model/moments/s;->b:Lcom/twitter/model/moments/MomentSocialProofType;

    const-class v1, Lcom/twitter/model/moments/MomentSocialProofType;

    .line 33
    invoke-static {v1}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 32
    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/s;->c:Ljava/lang/String;

    .line 34
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 35
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p2, Lcom/twitter/model/moments/s;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/s$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/s;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/s$b;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/moments/s;

    move-result-object v0

    return-object v0
.end method
