.class public final Lcom/twitter/model/moments/e$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/moments/e;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/twitter/model/moments/d$a;

.field b:Lcom/twitter/model/moments/d$a;

.field c:Lcom/twitter/model/moments/d$a;

.field d:Lcom/twitter/model/moments/d$a;

.field e:Lcom/twitter/util/math/Size;

.field f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 127
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    iput-object v0, p0, Lcom/twitter/model/moments/e$a;->e:Lcom/twitter/util/math/Size;

    return-void
.end method

.method private static a(Lcom/twitter/model/moments/d;)Lcom/twitter/model/moments/d$a;
    .locals 1

    .prologue
    .line 207
    if-eqz p0, :cond_0

    .line 208
    invoke-static {p0}, Lcom/twitter/model/moments/d$a;->a(Lcom/twitter/model/moments/d;)Lcom/twitter/model/moments/d$a;

    move-result-object v0

    .line 210
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/e$a;
    .locals 2

    .prologue
    .line 141
    new-instance v0, Lcom/twitter/model/moments/e$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/e$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/moments/e;->f:Lcom/twitter/model/moments/d;

    .line 142
    invoke-static {v1}, Lcom/twitter/model/moments/e$a;->a(Lcom/twitter/model/moments/d;)Lcom/twitter/model/moments/d$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/e$a;->d(Lcom/twitter/model/moments/d$a;)Lcom/twitter/model/moments/e$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/e;->c:Lcom/twitter/model/moments/d;

    .line 143
    invoke-static {v1}, Lcom/twitter/model/moments/e$a;->a(Lcom/twitter/model/moments/d;)Lcom/twitter/model/moments/d$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/e$a;->a(Lcom/twitter/model/moments/d$a;)Lcom/twitter/model/moments/e$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/e;->d:Lcom/twitter/model/moments/d;

    .line 144
    invoke-static {v1}, Lcom/twitter/model/moments/e$a;->a(Lcom/twitter/model/moments/d;)Lcom/twitter/model/moments/d$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/e$a;->b(Lcom/twitter/model/moments/d$a;)Lcom/twitter/model/moments/e$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/e;->e:Lcom/twitter/model/moments/d;

    .line 145
    invoke-static {v1}, Lcom/twitter/model/moments/e$a;->a(Lcom/twitter/model/moments/d;)Lcom/twitter/model/moments/d$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/e$a;->c(Lcom/twitter/model/moments/d$a;)Lcom/twitter/model/moments/e$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/moments/e;->h:Z

    .line 146
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/e$a;->a(Z)Lcom/twitter/model/moments/e$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/moments/e;->g:Lcom/twitter/util/math/Size;

    .line 147
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/e$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/e$a;

    move-result-object v0

    .line 141
    return-object v0
.end method

.method public static a(Lcom/twitter/util/math/c;Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/e$a;
    .locals 2

    .prologue
    .line 133
    invoke-virtual {p0, p1}, Lcom/twitter/util/math/c;->a(Lcom/twitter/util/math/Size;)Landroid/graphics/Rect;

    move-result-object v0

    .line 134
    new-instance v1, Lcom/twitter/model/moments/e$a;

    invoke-direct {v1}, Lcom/twitter/model/moments/e$a;-><init>()V

    .line 135
    invoke-virtual {v1, p1}, Lcom/twitter/model/moments/e$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/e$a;

    move-result-object v1

    .line 136
    invoke-static {v0, p1}, Lcom/twitter/model/moments/d$a;->a(Landroid/graphics/Rect;Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/d$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/e$a;->c(Lcom/twitter/model/moments/d$a;)Lcom/twitter/model/moments/e$a;

    move-result-object v0

    .line 134
    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/d$a;)Lcom/twitter/model/moments/e$a;
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/twitter/model/moments/e$a;->a:Lcom/twitter/model/moments/d$a;

    .line 153
    return-object p0
.end method

.method public a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/e$a;
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/twitter/model/moments/e$a;->e:Lcom/twitter/util/math/Size;

    .line 177
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/moments/e$a;
    .locals 0

    .prologue
    .line 182
    iput-boolean p1, p0, Lcom/twitter/model/moments/e$a;->f:Z

    .line 183
    return-object p0
.end method

.method public b(Lcom/twitter/model/moments/d$a;)Lcom/twitter/model/moments/e$a;
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/twitter/model/moments/e$a;->b:Lcom/twitter/model/moments/d$a;

    .line 159
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/twitter/model/moments/e$a;->e()Lcom/twitter/model/moments/e;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcom/twitter/model/moments/d$a;)Lcom/twitter/model/moments/e$a;
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/twitter/model/moments/e$a;->c:Lcom/twitter/model/moments/d$a;

    .line 165
    return-object p0
.end method

.method public d(Lcom/twitter/model/moments/d$a;)Lcom/twitter/model/moments/e$a;
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/twitter/model/moments/e$a;->d:Lcom/twitter/model/moments/d$a;

    .line 171
    return-object p0
.end method

.method protected e()Lcom/twitter/model/moments/e;
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/model/moments/e$a;->a:Lcom/twitter/model/moments/d$a;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/twitter/model/moments/e$a;->a:Lcom/twitter/model/moments/d$a;

    iget-object v1, p0, Lcom/twitter/model/moments/e$a;->e:Lcom/twitter/util/math/Size;

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/d$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/d$a;

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/twitter/model/moments/e$a;->b:Lcom/twitter/model/moments/d$a;

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/twitter/model/moments/e$a;->b:Lcom/twitter/model/moments/d$a;

    iget-object v1, p0, Lcom/twitter/model/moments/e$a;->e:Lcom/twitter/util/math/Size;

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/d$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/d$a;

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/moments/e$a;->c:Lcom/twitter/model/moments/d$a;

    if-eqz v0, :cond_2

    .line 196
    iget-object v0, p0, Lcom/twitter/model/moments/e$a;->c:Lcom/twitter/model/moments/d$a;

    iget-object v1, p0, Lcom/twitter/model/moments/e$a;->e:Lcom/twitter/util/math/Size;

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/d$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/d$a;

    .line 198
    :cond_2
    iget-object v0, p0, Lcom/twitter/model/moments/e$a;->d:Lcom/twitter/model/moments/d$a;

    if-eqz v0, :cond_3

    .line 199
    iget-object v0, p0, Lcom/twitter/model/moments/e$a;->d:Lcom/twitter/model/moments/d$a;

    iget-object v1, p0, Lcom/twitter/model/moments/e$a;->e:Lcom/twitter/util/math/Size;

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/d$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/d$a;

    .line 202
    :cond_3
    new-instance v0, Lcom/twitter/model/moments/e;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/e;-><init>(Lcom/twitter/model/moments/e$a;)V

    return-object v0
.end method
