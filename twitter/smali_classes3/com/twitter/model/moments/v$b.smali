.class public Lcom/twitter/model/moments/v$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/v$b$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/moments/v$b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Lcom/twitter/model/timeline/r;

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 146
    new-instance v0, Lcom/twitter/model/moments/v$b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/moments/v$b$a;-><init>(Lcom/twitter/model/moments/v$1;)V

    sput-object v0, Lcom/twitter/model/moments/v$b;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/twitter/model/timeline/r;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    invoke-static {p1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/v$b;->b:Ljava/lang/String;

    .line 154
    iput-object p2, p0, Lcom/twitter/model/moments/v$b;->c:Lcom/twitter/model/timeline/r;

    .line 155
    iput-object p3, p0, Lcom/twitter/model/moments/v$b;->d:Ljava/lang/String;

    .line 156
    return-void
.end method
