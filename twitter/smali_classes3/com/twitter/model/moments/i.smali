.class public Lcom/twitter/model/moments/i;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/i$b;,
        Lcom/twitter/model/moments/i$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/moments/i;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:I

.field public final f:Z

.field public final g:Ljava/lang/String;

.field public final h:J

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/topic/trends/TrendBadge;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/lang/String;

.field public final l:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/twitter/model/moments/i$b;

    invoke-direct {v0}, Lcom/twitter/model/moments/i$b;-><init>()V

    sput-object v0, Lcom/twitter/model/moments/i;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/moments/i$a;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iget-object v0, p1, Lcom/twitter/model/moments/i$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/moments/i;->b:Ljava/lang/String;

    .line 39
    iget-object v0, p1, Lcom/twitter/model/moments/i$a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/moments/i;->c:Ljava/lang/String;

    .line 40
    iget-object v0, p1, Lcom/twitter/model/moments/i$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/moments/i;->d:Ljava/lang/String;

    .line 41
    iget v0, p1, Lcom/twitter/model/moments/i$a;->d:I

    iput v0, p0, Lcom/twitter/model/moments/i;->e:I

    .line 42
    iget-boolean v0, p1, Lcom/twitter/model/moments/i$a;->e:Z

    iput-boolean v0, p0, Lcom/twitter/model/moments/i;->f:Z

    .line 43
    iget-wide v0, p1, Lcom/twitter/model/moments/i$a;->g:J

    iput-wide v0, p0, Lcom/twitter/model/moments/i;->h:J

    .line 44
    iget-object v0, p1, Lcom/twitter/model/moments/i$a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/moments/i;->g:Ljava/lang/String;

    .line 45
    iget-object v0, p1, Lcom/twitter/model/moments/i$a;->h:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/i;->i:Ljava/util/List;

    .line 46
    iget-object v0, p1, Lcom/twitter/model/moments/i$a;->i:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/i;->j:Ljava/util/List;

    .line 47
    iget-object v0, p1, Lcom/twitter/model/moments/i$a;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/i;->k:Ljava/lang/String;

    .line 48
    iget-wide v0, p1, Lcom/twitter/model/moments/i$a;->k:J

    iput-wide v0, p0, Lcom/twitter/model/moments/i;->l:J

    .line 49
    return-void
.end method
