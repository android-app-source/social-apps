.class Lcom/twitter/model/moments/i$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/moments/i;",
        "Lcom/twitter/model/moments/i$a;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/b;-><init>(I)V

    .line 140
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/moments/i$a;
    .locals 1

    .prologue
    .line 162
    new-instance v0, Lcom/twitter/model/moments/i$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/i$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/moments/i$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/moments/i$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    .line 169
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->b(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    .line 170
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->c(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    .line 171
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->a(I)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    .line 172
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->a(Z)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    .line 173
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->d(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    .line 174
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/moments/i$a;->a(J)Lcom/twitter/model/moments/i$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/topic/trends/TrendBadge;->d:Lcom/twitter/util/serialization/l;

    .line 176
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 175
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/i$a;->a(Ljava/util/List;)Lcom/twitter/model/moments/i$a;

    .line 177
    iget v0, p0, Lcom/twitter/model/moments/i$b;->c:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 178
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    .line 180
    :cond_0
    sget-object v0, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 181
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 180
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p2, v0}, Lcom/twitter/model/moments/i$a;->b(Ljava/util/List;)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    .line 182
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->e(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    .line 183
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/moments/i$a;->b(J)Lcom/twitter/model/moments/i$a;

    .line 184
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 137
    check-cast p2, Lcom/twitter/model/moments/i$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/moments/i$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/moments/i$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/i;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p2, Lcom/twitter/model/moments/i;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/i;->c:Ljava/lang/String;

    .line 146
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/i;->d:Ljava/lang/String;

    .line 147
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/moments/i;->e:I

    .line 148
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/moments/i;->f:Z

    .line 149
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/i;->g:Ljava/lang/String;

    .line 150
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/moments/i;->h:J

    .line 151
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/i;->i:Ljava/util/List;

    sget-object v2, Lcom/twitter/model/topic/trends/TrendBadge;->d:Lcom/twitter/util/serialization/l;

    .line 152
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/i;->j:Ljava/util/List;

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 154
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 153
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/i;->k:Ljava/lang/String;

    .line 155
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/moments/i;->l:J

    .line 156
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 157
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    check-cast p2, Lcom/twitter/model/moments/i;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/i$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/i;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/twitter/model/moments/i$b;->a()Lcom/twitter/model/moments/i$a;

    move-result-object v0

    return-object v0
.end method
