.class final Lcom/twitter/model/moments/p$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/e",
        "<",
        "Lcom/twitter/model/moments/Moment;",
        "Lcom/twitter/model/moments/Moment;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/Moment;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 36
    if-eqz p1, :cond_0

    iget-wide v0, p2, Lcom/twitter/model/moments/Moment;->u:J

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->u:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    check-cast p1, Lcom/twitter/model/moments/Moment;

    check-cast p2, Lcom/twitter/model/moments/Moment;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/p$3;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/Moment;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
