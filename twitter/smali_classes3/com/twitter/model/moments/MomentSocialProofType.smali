.class public final enum Lcom/twitter/model/moments/MomentSocialProofType;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/model/moments/MomentSocialProofType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/model/moments/MomentSocialProofType;

.field private static final synthetic b:[Lcom/twitter/model/moments/MomentSocialProofType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/twitter/model/moments/MomentSocialProofType;

    const-string/jumbo v1, "FIRST_ORDER"

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/moments/MomentSocialProofType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/moments/MomentSocialProofType;->a:Lcom/twitter/model/moments/MomentSocialProofType;

    .line 3
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/twitter/model/moments/MomentSocialProofType;

    sget-object v1, Lcom/twitter/model/moments/MomentSocialProofType;->a:Lcom/twitter/model/moments/MomentSocialProofType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/twitter/model/moments/MomentSocialProofType;->b:[Lcom/twitter/model/moments/MomentSocialProofType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/model/moments/MomentSocialProofType;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/twitter/model/moments/MomentSocialProofType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/MomentSocialProofType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/model/moments/MomentSocialProofType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/twitter/model/moments/MomentSocialProofType;->b:[Lcom/twitter/model/moments/MomentSocialProofType;

    invoke-virtual {v0}, [Lcom/twitter/model/moments/MomentSocialProofType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/model/moments/MomentSocialProofType;

    return-object v0
.end method
