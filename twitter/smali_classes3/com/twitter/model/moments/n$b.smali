.class Lcom/twitter/model/moments/n$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/moments/n;",
        "Lcom/twitter/model/moments/n$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/moments/n$1;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/twitter/model/moments/n$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/moments/n$a;
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/twitter/model/moments/n$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/n$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/moments/n$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/moments/n$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/n$a;

    move-result-object v0

    .line 91
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/n$a;->b(Ljava/lang/String;)Lcom/twitter/model/moments/n$a;

    move-result-object v0

    .line 92
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/n$a;->c(Ljava/lang/String;)Lcom/twitter/model/moments/n$a;

    move-result-object v0

    .line 93
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/n$a;->d(Ljava/lang/String;)Lcom/twitter/model/moments/n$a;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/twitter/model/moments/n$a;->e()Lcom/twitter/model/moments/n;

    .line 95
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 71
    check-cast p2, Lcom/twitter/model/moments/n$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/moments/n$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/moments/n$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/n;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p2, Lcom/twitter/model/moments/n;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/n;->c:Ljava/lang/String;

    .line 76
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/n;->d:Ljava/lang/String;

    .line 77
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/n;->e:Ljava/lang/String;

    .line 78
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 79
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    check-cast p2, Lcom/twitter/model/moments/n;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/n$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/n;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/twitter/model/moments/n$b;->a()Lcom/twitter/model/moments/n$a;

    move-result-object v0

    return-object v0
.end method
