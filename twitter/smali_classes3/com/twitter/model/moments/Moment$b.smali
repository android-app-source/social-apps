.class Lcom/twitter/model/moments/Moment$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/Moment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/moments/Moment;",
        "Lcom/twitter/model/moments/Moment$a;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/b;-><init>(I)V

    .line 77
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/moments/Moment$a;
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lcom/twitter/model/moments/Moment$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/Moment$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/moments/Moment$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 114
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 115
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->d(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 116
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->e(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 117
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 118
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->b(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 119
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->c(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 120
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->b(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 121
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->c(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 122
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->d(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 123
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->f(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 124
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(I)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 125
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->g(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/moments/a;->a:Lcom/twitter/util/serialization/l;

    .line 126
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/a;

    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/a;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v1

    sget-object v0, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 127
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/Moment$a;->a(Lcgi;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/moments/g;->a:Lcom/twitter/util/serialization/l;

    .line 128
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/g;

    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/g;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 129
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/moments/Moment$a;->b(J)Lcom/twitter/model/moments/Moment$a;

    .line 130
    const/4 v0, 0x1

    if-ne p3, v0, :cond_1

    .line 135
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->o()B

    move-result v0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    .line 136
    invoke-static {p1}, Lcom/twitter/util/serialization/k;->b(Lcom/twitter/util/serialization/n;)V

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    const/4 v0, 0x3

    if-lt p3, v0, :cond_2

    .line 141
    sget-object v0, Lcom/twitter/model/moments/f;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/f;

    invoke-virtual {p2, v0}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/f;)Lcom/twitter/model/moments/Moment$a;

    .line 143
    :cond_2
    const/4 v0, 0x4

    if-lt p3, v0, :cond_0

    .line 144
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/moments/Moment$a;->e(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 145
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/moments/Moment$a;->c(J)Lcom/twitter/model/moments/Moment$a;

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 73
    check-cast p2, Lcom/twitter/model/moments/Moment$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/moments/Moment$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/moments/Moment$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/Moment;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    iget-wide v0, p2, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    .line 83
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->i:Ljava/lang/String;

    .line 84
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->j:Ljava/lang/String;

    .line 85
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/moments/Moment;->d:Z

    .line 86
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/moments/Moment;->e:Z

    .line 87
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/moments/Moment;->f:Z

    .line 88
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->g:Ljava/lang/String;

    .line 89
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->h:Ljava/lang/String;

    .line 90
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/moments/Moment;->k:Z

    .line 91
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->l:Ljava/lang/String;

    .line 92
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/moments/Moment;->m:I

    .line 93
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->n:Ljava/lang/String;

    .line 94
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    sget-object v2, Lcom/twitter/model/moments/a;->a:Lcom/twitter/util/serialization/l;

    .line 95
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->p:Lcgi;

    sget-object v2, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 96
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->q:Lcom/twitter/model/moments/g;

    sget-object v2, Lcom/twitter/model/moments/g;->a:Lcom/twitter/util/serialization/l;

    .line 97
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/moments/Moment;->s:J

    .line 98
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    sget-object v2, Lcom/twitter/model/moments/f;->a:Lcom/twitter/util/serialization/l;

    .line 99
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/moments/Moment;->t:Z

    .line 100
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/moments/Moment;->u:J

    .line 101
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 102
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    check-cast p2, Lcom/twitter/model/moments/Moment;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/moments/Moment$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/moments/Moment;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/twitter/model/moments/Moment$b;->a()Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    return-object v0
.end method
