.class public final Lcom/twitter/model/moments/v$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/moments/v;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/v$c;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/twitter/model/moments/v$b;

.field private c:Lcom/twitter/model/timeline/k;

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/timeline/r;",
            ">;"
        }
    .end annotation
.end field

.field private e:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/moments/v$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/model/moments/v$a;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/moments/v$a;)Lcom/twitter/model/moments/v$b;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/model/moments/v$a;->b:Lcom/twitter/model/moments/v$b;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/moments/v$a;)Lcom/twitter/model/timeline/k;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/model/moments/v$a;->c:Lcom/twitter/model/timeline/k;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/moments/v$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/model/moments/v$a;->d:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/model/moments/v$a;)J
    .locals 2

    .prologue
    .line 180
    iget-wide v0, p0, Lcom/twitter/model/moments/v$a;->e:J

    return-wide v0
.end method


# virtual methods
.method public a(J)Lcom/twitter/model/moments/v$a;
    .locals 1

    .prologue
    .line 220
    iput-wide p1, p0, Lcom/twitter/model/moments/v$a;->e:J

    .line 221
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/v$b;)Lcom/twitter/model/moments/v$a;
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/twitter/model/moments/v$a;->b:Lcom/twitter/model/moments/v$b;

    .line 197
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/k;)Lcom/twitter/model/moments/v$a;
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/twitter/model/moments/v$a;->c:Lcom/twitter/model/timeline/k;

    .line 209
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/moments/v$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/v$c;",
            ">;)",
            "Lcom/twitter/model/moments/v$a;"
        }
    .end annotation

    .prologue
    .line 202
    iput-object p1, p0, Lcom/twitter/model/moments/v$a;->a:Ljava/util/List;

    .line 203
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcom/twitter/model/moments/v$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/timeline/r;",
            ">;)",
            "Lcom/twitter/model/moments/v$a;"
        }
    .end annotation

    .prologue
    .line 214
    iput-object p1, p0, Lcom/twitter/model/moments/v$a;->d:Ljava/util/Map;

    .line 215
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/twitter/model/moments/v$a;->e()Lcom/twitter/model/moments/v;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/moments/v;
    .locals 1

    .prologue
    .line 191
    new-instance v0, Lcom/twitter/model/moments/v;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/v;-><init>(Lcom/twitter/model/moments/v$a;)V

    return-object v0
.end method
