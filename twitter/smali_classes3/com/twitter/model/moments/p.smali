.class public Lcom/twitter/model/moments/p;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/moments/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/model/moments/q",
        "<",
        "Lcom/twitter/model/moments/Moment;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/object/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/f",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;",
            "Lcom/twitter/model/moments/p;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lrx/functions/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/e",
            "<-",
            "Lcom/twitter/model/moments/Moment;",
            "-",
            "Lcom/twitter/model/moments/Moment;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lrx/functions/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/e",
            "<-",
            "Lcom/twitter/model/moments/Moment;",
            "-",
            "Lcom/twitter/model/moments/Moment;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/twitter/model/moments/Moment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/twitter/model/moments/p$1;

    invoke-direct {v0}, Lcom/twitter/model/moments/p$1;-><init>()V

    sput-object v0, Lcom/twitter/model/moments/p;->a:Lcom/twitter/util/object/f;

    .line 23
    new-instance v0, Lcom/twitter/model/moments/p$2;

    invoke-direct {v0}, Lcom/twitter/model/moments/p$2;-><init>()V

    sput-object v0, Lcom/twitter/model/moments/p;->b:Lrx/functions/e;

    .line 31
    new-instance v0, Lcom/twitter/model/moments/p$3;

    invoke-direct {v0}, Lcom/twitter/model/moments/p$3;-><init>()V

    sput-object v0, Lcom/twitter/model/moments/p;->c:Lrx/functions/e;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/moments/Moment;Lrx/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/Moment;",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/twitter/model/moments/p;->g:Lcom/twitter/model/moments/Moment;

    .line 48
    iput-object p2, p0, Lcom/twitter/model/moments/p;->d:Lrx/c;

    .line 49
    sget-object v0, Lcom/twitter/model/moments/p;->b:Lrx/functions/e;

    invoke-virtual {p2, v0}, Lrx/c;->a(Lrx/functions/e;)Lrx/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/p;->e:Lrx/c;

    .line 50
    sget-object v0, Lcom/twitter/model/moments/p;->c:Lrx/functions/e;

    invoke-virtual {p2, v0}, Lrx/c;->a(Lrx/functions/e;)Lrx/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/p;->f:Lrx/c;

    .line 51
    return-void
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/model/moments/p;->f:Lrx/c;

    return-object v0
.end method

.method public b()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/model/moments/p;->e:Lrx/c;

    return-object v0
.end method
