.class public final Lcom/twitter/model/moments/i$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/moments/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/moments/i;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:I

.field e:Z

.field f:Ljava/lang/String;

.field g:J

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/topic/trends/TrendBadge;",
            ">;"
        }
    .end annotation
.end field

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field j:Ljava/lang/String;

.field k:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/model/moments/i$a;
    .locals 0

    .prologue
    .line 102
    iput p1, p0, Lcom/twitter/model/moments/i$a;->d:I

    .line 103
    return-object p0
.end method

.method public a(J)Lcom/twitter/model/moments/i$a;
    .locals 1

    .prologue
    .line 90
    iput-wide p1, p0, Lcom/twitter/model/moments/i$a;->g:J

    .line 91
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/twitter/model/moments/i$a;->a:Ljava/lang/String;

    .line 67
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/moments/i$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/topic/trends/TrendBadge;",
            ">;)",
            "Lcom/twitter/model/moments/i$a;"
        }
    .end annotation

    .prologue
    .line 108
    iput-object p1, p0, Lcom/twitter/model/moments/i$a;->h:Ljava/util/List;

    .line 109
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/moments/i$a;
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/twitter/model/moments/i$a;->e:Z

    .line 85
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/moments/i$a;
    .locals 1

    .prologue
    .line 126
    iput-wide p1, p0, Lcom/twitter/model/moments/i$a;->k:J

    .line 127
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/twitter/model/moments/i$a;->b:Ljava/lang/String;

    .line 73
    return-object p0
.end method

.method public b(Ljava/util/List;)Lcom/twitter/model/moments/i$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/twitter/model/moments/i$a;"
        }
    .end annotation

    .prologue
    .line 114
    iput-object p1, p0, Lcom/twitter/model/moments/i$a;->i:Ljava/util/List;

    .line 115
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/twitter/model/moments/i$a;->e()Lcom/twitter/model/moments/i;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/twitter/model/moments/i$a;->c:Ljava/lang/String;

    .line 79
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/twitter/model/moments/i$a;->f:Ljava/lang/String;

    .line 97
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/twitter/model/moments/i$a;->j:Ljava/lang/String;

    .line 121
    return-object p0
.end method

.method protected e()Lcom/twitter/model/moments/i;
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lcom/twitter/model/moments/i;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/i;-><init>(Lcom/twitter/model/moments/i$a;)V

    return-object v0
.end method
