.class public Lcom/twitter/model/moments/e;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/e$b;,
        Lcom/twitter/model/moments/e$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/moments/e;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/twitter/model/moments/e;


# instance fields
.field public final c:Lcom/twitter/model/moments/d;

.field public final d:Lcom/twitter/model/moments/d;

.field public final e:Lcom/twitter/model/moments/d;

.field public final f:Lcom/twitter/model/moments/d;

.field public final g:Lcom/twitter/util/math/Size;

.field public final h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/twitter/model/moments/e$b;

    invoke-direct {v0}, Lcom/twitter/model/moments/e$b;-><init>()V

    sput-object v0, Lcom/twitter/model/moments/e;->a:Lcom/twitter/util/serialization/l;

    .line 24
    new-instance v0, Lcom/twitter/model/moments/e$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/e$a;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/model/moments/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/e;

    sput-object v0, Lcom/twitter/model/moments/e;->b:Lcom/twitter/model/moments/e;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;ZLcom/twitter/util/math/Size;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/twitter/model/moments/e;->c:Lcom/twitter/model/moments/d;

    .line 46
    iput-object p2, p0, Lcom/twitter/model/moments/e;->d:Lcom/twitter/model/moments/d;

    .line 47
    iput-object p3, p0, Lcom/twitter/model/moments/e;->e:Lcom/twitter/model/moments/d;

    .line 48
    iput-object p4, p0, Lcom/twitter/model/moments/e;->f:Lcom/twitter/model/moments/d;

    .line 49
    iput-boolean p5, p0, Lcom/twitter/model/moments/e;->h:Z

    .line 50
    iput-object p6, p0, Lcom/twitter/model/moments/e;->g:Lcom/twitter/util/math/Size;

    .line 51
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;ZLcom/twitter/util/math/Size;Lcom/twitter/model/moments/e$1;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct/range {p0 .. p6}, Lcom/twitter/model/moments/e;-><init>(Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;Lcom/twitter/model/moments/d;ZLcom/twitter/util/math/Size;)V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/moments/e$a;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iget-object v0, p1, Lcom/twitter/model/moments/e$a;->a:Lcom/twitter/model/moments/d$a;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/twitter/model/moments/e;->c:Lcom/twitter/model/moments/d;

    .line 35
    iget-object v0, p1, Lcom/twitter/model/moments/e$a;->b:Lcom/twitter/model/moments/d$a;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/twitter/model/moments/e;->d:Lcom/twitter/model/moments/d;

    .line 36
    iget-object v0, p1, Lcom/twitter/model/moments/e$a;->c:Lcom/twitter/model/moments/d$a;

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lcom/twitter/model/moments/e;->e:Lcom/twitter/model/moments/d;

    .line 37
    iget-object v0, p1, Lcom/twitter/model/moments/e$a;->d:Lcom/twitter/model/moments/d$a;

    if-nez v0, :cond_3

    :goto_3
    iput-object v1, p0, Lcom/twitter/model/moments/e;->f:Lcom/twitter/model/moments/d;

    .line 38
    iget-boolean v0, p1, Lcom/twitter/model/moments/e$a;->f:Z

    iput-boolean v0, p0, Lcom/twitter/model/moments/e;->h:Z

    .line 39
    iget-object v0, p1, Lcom/twitter/model/moments/e$a;->e:Lcom/twitter/util/math/Size;

    iput-object v0, p0, Lcom/twitter/model/moments/e;->g:Lcom/twitter/util/math/Size;

    .line 40
    return-void

    .line 34
    :cond_0
    iget-object v0, p1, Lcom/twitter/model/moments/e$a;->a:Lcom/twitter/model/moments/d$a;

    invoke-virtual {v0}, Lcom/twitter/model/moments/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/d;

    goto :goto_0

    .line 35
    :cond_1
    iget-object v0, p1, Lcom/twitter/model/moments/e$a;->b:Lcom/twitter/model/moments/d$a;

    invoke-virtual {v0}, Lcom/twitter/model/moments/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/d;

    goto :goto_1

    .line 36
    :cond_2
    iget-object v0, p1, Lcom/twitter/model/moments/e$a;->c:Lcom/twitter/model/moments/d$a;

    invoke-virtual {v0}, Lcom/twitter/model/moments/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/d;

    goto :goto_2

    .line 37
    :cond_3
    iget-object v0, p1, Lcom/twitter/model/moments/e$a;->d:Lcom/twitter/model/moments/d$a;

    invoke-virtual {v0}, Lcom/twitter/model/moments/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/d;

    move-object v1, v0

    goto :goto_3
.end method

.method private static varargs a(F[Lcom/twitter/model/moments/d;)Lcom/twitter/model/moments/d;
    .locals 7

    .prologue
    .line 102
    const v3, 0x7f7fffff    # Float.MAX_VALUE

    .line 103
    const/4 v1, 0x0

    .line 104
    array-length v5, p1

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v0, p1, v4

    .line 105
    if-eqz v0, :cond_1

    .line 106
    invoke-virtual {v0}, Lcom/twitter/model/moments/d;->b()F

    move-result v2

    sub-float/2addr v2, p0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 107
    cmpg-float v6, v2, v3

    if-gez v6, :cond_1

    move v1, v2

    .line 104
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v1

    move-object v1, v0

    goto :goto_0

    .line 113
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    move v1, v3

    goto :goto_1
.end method

.method public static a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/d;
    .locals 1

    .prologue
    .line 118
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, v0}, Lcom/twitter/model/moments/e;->a(Lcom/twitter/model/moments/e;F)Lcom/twitter/model/moments/d;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/twitter/model/moments/e;F)Lcom/twitter/model/moments/d;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 85
    if-eqz p0, :cond_0

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    invoke-static {p1}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    :cond_0
    const/4 v0, 0x0

    .line 97
    :goto_0
    return-object v0

    .line 89
    :cond_1
    cmpl-float v0, p1, v1

    if-nez v0, :cond_2

    .line 90
    iget-object v0, p0, Lcom/twitter/model/moments/e;->c:Lcom/twitter/model/moments/d;

    goto :goto_0

    .line 93
    :cond_2
    cmpl-float v0, p1, v1

    if-lez v0, :cond_3

    .line 94
    new-array v0, v4, [Lcom/twitter/model/moments/d;

    iget-object v1, p0, Lcom/twitter/model/moments/e;->f:Lcom/twitter/model/moments/d;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/twitter/model/moments/e;->c:Lcom/twitter/model/moments/d;

    aput-object v1, v0, v3

    invoke-static {p1, v0}, Lcom/twitter/model/moments/e;->a(F[Lcom/twitter/model/moments/d;)Lcom/twitter/model/moments/d;

    move-result-object v0

    goto :goto_0

    .line 97
    :cond_3
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/twitter/model/moments/d;

    iget-object v1, p0, Lcom/twitter/model/moments/e;->e:Lcom/twitter/model/moments/d;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/twitter/model/moments/e;->d:Lcom/twitter/model/moments/d;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/twitter/model/moments/e;->c:Lcom/twitter/model/moments/d;

    aput-object v1, v0, v4

    invoke-static {p1, v0}, Lcom/twitter/model/moments/e;->a(F[Lcom/twitter/model/moments/d;)Lcom/twitter/model/moments/d;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 55
    if-ne p0, p1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 59
    goto :goto_0

    .line 62
    :cond_3
    check-cast p1, Lcom/twitter/model/moments/e;

    .line 64
    iget-boolean v2, p0, Lcom/twitter/model/moments/e;->h:Z

    iget-boolean v3, p1, Lcom/twitter/model/moments/e;->h:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/twitter/model/moments/e;->c:Lcom/twitter/model/moments/d;

    iget-object v3, p1, Lcom/twitter/model/moments/e;->c:Lcom/twitter/model/moments/d;

    .line 65
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/moments/e;->d:Lcom/twitter/model/moments/d;

    iget-object v3, p1, Lcom/twitter/model/moments/e;->d:Lcom/twitter/model/moments/d;

    .line 66
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/moments/e;->e:Lcom/twitter/model/moments/d;

    iget-object v3, p1, Lcom/twitter/model/moments/e;->e:Lcom/twitter/model/moments/d;

    .line 67
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/moments/e;->f:Lcom/twitter/model/moments/d;

    iget-object v3, p1, Lcom/twitter/model/moments/e;->f:Lcom/twitter/model/moments/d;

    .line 68
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/moments/e;->g:Lcom/twitter/util/math/Size;

    iget-object v3, p1, Lcom/twitter/model/moments/e;->g:Lcom/twitter/util/math/Size;

    .line 69
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/model/moments/e;->c:Lcom/twitter/model/moments/d;

    iget-boolean v1, p0, Lcom/twitter/model/moments/e;->h:Z

    .line 76
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/model/moments/e;->d:Lcom/twitter/model/moments/d;

    iget-object v3, p0, Lcom/twitter/model/moments/e;->e:Lcom/twitter/model/moments/d;

    iget-object v4, p0, Lcom/twitter/model/moments/e;->f:Lcom/twitter/model/moments/d;

    iget-object v5, p0, Lcom/twitter/model/moments/e;->g:Lcom/twitter/util/math/Size;

    .line 74
    invoke-static/range {v0 .. v5}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
