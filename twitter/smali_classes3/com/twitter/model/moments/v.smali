.class public Lcom/twitter/model/moments/v;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/v$a;,
        Lcom/twitter/model/moments/v$b;,
        Lcom/twitter/model/moments/v$d;,
        Lcom/twitter/model/moments/v$c;,
        Lcom/twitter/model/moments/v$e;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/moments/v;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/timeline/r;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/v$c;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/twitter/model/moments/v$b;

.field public final e:Lcom/twitter/model/timeline/k;

.field public final f:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/model/moments/v$e;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/moments/v$e;-><init>(Lcom/twitter/model/moments/v$1;)V

    sput-object v0, Lcom/twitter/model/moments/v;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/moments/v$a;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lcom/twitter/model/moments/v$a;->a(Lcom/twitter/model/moments/v$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/v;->c:Ljava/util/List;

    .line 36
    invoke-static {p1}, Lcom/twitter/model/moments/v$a;->b(Lcom/twitter/model/moments/v$a;)Lcom/twitter/model/moments/v$b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/v;->d:Lcom/twitter/model/moments/v$b;

    .line 37
    invoke-static {p1}, Lcom/twitter/model/moments/v$a;->c(Lcom/twitter/model/moments/v$a;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/v;->e:Lcom/twitter/model/timeline/k;

    .line 38
    invoke-static {p1}, Lcom/twitter/model/moments/v$a;->d(Lcom/twitter/model/moments/v$a;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/v;->b:Ljava/util/Map;

    .line 39
    invoke-static {p1}, Lcom/twitter/model/moments/v$a;->e(Lcom/twitter/model/moments/v$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/moments/v;->f:J

    .line 40
    return-void
.end method
