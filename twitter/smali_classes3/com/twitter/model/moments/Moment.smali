.class public Lcom/twitter/model/moments/Moment;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/Moment$a;,
        Lcom/twitter/model/moments/Moment$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Z

.field public final l:Ljava/lang/String;

.field public final m:I

.field public final n:Ljava/lang/String;

.field public final o:Lcom/twitter/model/moments/a;

.field public final p:Lcgi;

.field public final q:Lcom/twitter/model/moments/g;

.field public final r:Lcom/twitter/model/moments/f;

.field public final s:J

.field public final t:Z

.field public final u:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/twitter/model/moments/Moment$b;

    invoke-direct {v0}, Lcom/twitter/model/moments/Moment$b;-><init>()V

    sput-object v0, Lcom/twitter/model/moments/Moment;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/moments/Moment$a;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/Moment$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/moments/Moment;->b:J

    .line 48
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->b(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    .line 49
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->c(Lcom/twitter/model/moments/Moment$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/moments/Moment;->d:Z

    .line 50
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->d(Lcom/twitter/model/moments/Moment$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/moments/Moment;->e:Z

    .line 51
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->e(Lcom/twitter/model/moments/Moment$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/moments/Moment;->f:Z

    .line 52
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->f(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/Moment;->g:Ljava/lang/String;

    .line 53
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->g(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/Moment;->h:Ljava/lang/String;

    .line 54
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->h(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/Moment;->i:Ljava/lang/String;

    .line 55
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->i(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/Moment;->j:Ljava/lang/String;

    .line 56
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->j(Lcom/twitter/model/moments/Moment$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/moments/Moment;->k:Z

    .line 57
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->k(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/Moment;->l:Ljava/lang/String;

    .line 58
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->l(Lcom/twitter/model/moments/Moment$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/moments/Moment;->m:I

    .line 59
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->m(Lcom/twitter/model/moments/Moment$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/Moment;->n:Ljava/lang/String;

    .line 60
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->n(Lcom/twitter/model/moments/Moment$a;)Lcom/twitter/model/moments/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    .line 61
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->o(Lcom/twitter/model/moments/Moment$a;)Lcgi;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/Moment;->p:Lcgi;

    .line 62
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->p(Lcom/twitter/model/moments/Moment$a;)Lcom/twitter/model/moments/g;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/Moment;->q:Lcom/twitter/model/moments/g;

    .line 63
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->q(Lcom/twitter/model/moments/Moment$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/moments/Moment;->s:J

    .line 64
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->r(Lcom/twitter/model/moments/Moment$a;)Lcom/twitter/model/moments/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    .line 65
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->s(Lcom/twitter/model/moments/Moment$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/moments/Moment;->t:Z

    .line 66
    invoke-static {p1}, Lcom/twitter/model/moments/Moment$a;->t(Lcom/twitter/model/moments/Moment$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/moments/Moment;->u:J

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/moments/Moment$a;Lcom/twitter/model/moments/Moment$1;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/twitter/model/moments/Moment;-><init>(Lcom/twitter/model/moments/Moment$a;)V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/model/moments/Moment;->p:Lcgi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/moments/Moment;->p:Lcgi;

    invoke-virtual {v0}, Lcgi;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
