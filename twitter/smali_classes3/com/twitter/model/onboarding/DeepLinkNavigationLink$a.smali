.class final Lcom/twitter/model/onboarding/DeepLinkNavigationLink$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/onboarding/DeepLinkNavigationLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/onboarding/DeepLinkNavigationLink;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/onboarding/DeepLinkNavigationLink$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/twitter/model/onboarding/DeepLinkNavigationLink$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/onboarding/DeepLinkNavigationLink;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/twitter/model/onboarding/DeepLinkNavigationLink;

    invoke-direct {v1, v0}, Lcom/twitter/model/onboarding/DeepLinkNavigationLink;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/onboarding/DeepLinkNavigationLink;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    iget-object v0, p2, Lcom/twitter/model/onboarding/DeepLinkNavigationLink;->deepLink:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 25
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    check-cast p2, Lcom/twitter/model/onboarding/DeepLinkNavigationLink;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/onboarding/DeepLinkNavigationLink$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/onboarding/DeepLinkNavigationLink;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/onboarding/DeepLinkNavigationLink$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/onboarding/DeepLinkNavigationLink;

    move-result-object v0

    return-object v0
.end method
