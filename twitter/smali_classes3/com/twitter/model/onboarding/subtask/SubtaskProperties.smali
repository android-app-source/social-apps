.class public Lcom/twitter/model/onboarding/subtask/SubtaskProperties;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BlacklistedInterface"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/onboarding/subtask/SubtaskProperties$b;,
        Lcom/twitter/model/onboarding/subtask/SubtaskProperties$a;
    }
.end annotation


# instance fields
.field private final mCancelLink:Lcom/twitter/model/onboarding/UiLink;

.field private final mContinueLink:Lcom/twitter/model/onboarding/UiLink;

.field private final mSkipLink:Lcom/twitter/model/onboarding/UiLink;


# direct methods
.method public constructor <init>(Lcom/twitter/model/onboarding/subtask/SubtaskProperties$a;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iget-object v0, p1, Lcom/twitter/model/onboarding/subtask/SubtaskProperties$a;->a:Lcom/twitter/model/onboarding/UiLink;

    iput-object v0, p0, Lcom/twitter/model/onboarding/subtask/SubtaskProperties;->mContinueLink:Lcom/twitter/model/onboarding/UiLink;

    .line 26
    iget-object v0, p1, Lcom/twitter/model/onboarding/subtask/SubtaskProperties$a;->b:Lcom/twitter/model/onboarding/UiLink;

    iput-object v0, p0, Lcom/twitter/model/onboarding/subtask/SubtaskProperties;->mSkipLink:Lcom/twitter/model/onboarding/UiLink;

    .line 27
    iget-object v0, p1, Lcom/twitter/model/onboarding/subtask/SubtaskProperties$a;->c:Lcom/twitter/model/onboarding/UiLink;

    iput-object v0, p0, Lcom/twitter/model/onboarding/subtask/SubtaskProperties;->mCancelLink:Lcom/twitter/model/onboarding/UiLink;

    .line 28
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/onboarding/UiLink;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/model/onboarding/subtask/SubtaskProperties;->mContinueLink:Lcom/twitter/model/onboarding/UiLink;

    return-object v0
.end method

.method public b()Lcom/twitter/model/onboarding/UiLink;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/model/onboarding/subtask/SubtaskProperties;->mSkipLink:Lcom/twitter/model/onboarding/UiLink;

    return-object v0
.end method

.method public c()Lcom/twitter/model/onboarding/UiLink;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/model/onboarding/subtask/SubtaskProperties;->mCancelLink:Lcom/twitter/model/onboarding/UiLink;

    return-object v0
.end method
