.class public Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties;
.super Lcom/twitter/model/onboarding/subtask/SubtaskProperties;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;
    }
.end annotation


# instance fields
.field private final mHint:Ljava/lang/String;

.field private final mSubtitle:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/twitter/model/onboarding/subtask/SubtaskProperties;-><init>(Lcom/twitter/model/onboarding/subtask/SubtaskProperties$a;)V

    .line 13
    invoke-static {p1}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->a(Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties;->mTitle:Ljava/lang/String;

    .line 14
    invoke-static {p1}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->b(Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties;->mSubtitle:Ljava/lang/String;

    .line 15
    invoke-static {p1}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->c(Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties;->mHint:Ljava/lang/String;

    .line 16
    return-void
.end method
