.class public Lcom/twitter/model/onboarding/UiLink;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BlacklistedInterface"
    }
.end annotation


# instance fields
.field public final label:Ljava/lang/String;

.field public final navigationLink:Lcom/twitter/model/onboarding/NavigationLink;


# direct methods
.method public constructor <init>(Lcom/twitter/model/onboarding/NavigationLink;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/twitter/model/onboarding/UiLink;->navigationLink:Lcom/twitter/model/onboarding/NavigationLink;

    .line 20
    iput-object p2, p0, Lcom/twitter/model/onboarding/UiLink;->label:Ljava/lang/String;

    .line 21
    return-void
.end method
