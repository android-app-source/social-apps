.class public Lcom/twitter/model/onboarding/TaskContext;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BlacklistedInterface"
    }
.end annotation


# instance fields
.field private final mCurrentSubtask:Lcom/twitter/model/onboarding/Subtask;

.field private final mTask:Lcom/twitter/model/onboarding/Task;


# direct methods
.method public constructor <init>(Lcom/twitter/model/onboarding/Task;Lcom/twitter/model/onboarding/Subtask;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/twitter/model/onboarding/TaskContext;->mTask:Lcom/twitter/model/onboarding/Task;

    .line 23
    iput-object p2, p0, Lcom/twitter/model/onboarding/TaskContext;->mCurrentSubtask:Lcom/twitter/model/onboarding/Subtask;

    .line 24
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/onboarding/Task;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/model/onboarding/TaskContext;->mTask:Lcom/twitter/model/onboarding/Task;

    return-object v0
.end method

.method public b()Lcom/twitter/model/onboarding/Subtask;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/model/onboarding/TaskContext;->mCurrentSubtask:Lcom/twitter/model/onboarding/Subtask;

    return-object v0
.end method
