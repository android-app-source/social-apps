.class public Lcom/twitter/model/onboarding/c;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/model/onboarding/NavigationLink;
    .locals 5

    .prologue
    .line 18
    packed-switch p0, :pswitch_data_0

    .line 32
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v1, "Unsupported navigation link %d, %s %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 33
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    .line 32
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/g;->a(Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/twitter/model/onboarding/NoopNavigationLink;

    invoke-direct {v0}, Lcom/twitter/model/onboarding/NoopNavigationLink;-><init>()V

    :goto_0
    return-object v0

    .line 20
    :pswitch_0
    new-instance v0, Lcom/twitter/model/onboarding/DeepLinkNavigationLink;

    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/model/onboarding/DeepLinkNavigationLink;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 23
    :pswitch_1
    new-instance v0, Lcom/twitter/model/onboarding/SubtaskNavigationLink;

    invoke-static {p2}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/model/onboarding/SubtaskNavigationLink;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 26
    :pswitch_2
    new-instance v0, Lcom/twitter/model/onboarding/FinishFlowNavigationLink;

    invoke-direct {v0}, Lcom/twitter/model/onboarding/FinishFlowNavigationLink;-><init>()V

    goto :goto_0

    .line 29
    :pswitch_3
    new-instance v0, Lcom/twitter/model/onboarding/GetNextTaskNavigationLink;

    invoke-direct {v0}, Lcom/twitter/model/onboarding/GetNextTaskNavigationLink;-><init>()V

    goto :goto_0

    .line 18
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
