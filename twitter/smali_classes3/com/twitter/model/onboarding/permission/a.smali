.class public Lcom/twitter/model/onboarding/permission/a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/onboarding/permission/a$a;
    }
.end annotation


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:J

.field public final h:Lcom/twitter/model/onboarding/permission/SystemPermissionState;

.field public final i:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

.field public final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/twitter/model/onboarding/permission/a$a;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Lcom/twitter/model/onboarding/permission/a$a;->a(Lcom/twitter/model/onboarding/permission/a$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/onboarding/permission/a;->a:J

    .line 23
    invoke-static {p1}, Lcom/twitter/model/onboarding/permission/a$a;->b(Lcom/twitter/model/onboarding/permission/a$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/onboarding/permission/a;->b:Ljava/lang/String;

    .line 24
    invoke-static {p1}, Lcom/twitter/model/onboarding/permission/a$a;->c(Lcom/twitter/model/onboarding/permission/a$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/onboarding/permission/a;->c:Ljava/lang/String;

    .line 25
    invoke-static {p1}, Lcom/twitter/model/onboarding/permission/a$a;->d(Lcom/twitter/model/onboarding/permission/a$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/onboarding/permission/a;->d:Ljava/lang/String;

    .line 26
    invoke-static {p1}, Lcom/twitter/model/onboarding/permission/a$a;->e(Lcom/twitter/model/onboarding/permission/a$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/onboarding/permission/a;->e:Ljava/lang/String;

    .line 27
    invoke-static {p1}, Lcom/twitter/model/onboarding/permission/a$a;->f(Lcom/twitter/model/onboarding/permission/a$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/onboarding/permission/a;->f:Ljava/lang/String;

    .line 28
    invoke-static {p1}, Lcom/twitter/model/onboarding/permission/a$a;->g(Lcom/twitter/model/onboarding/permission/a$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/onboarding/permission/a;->g:J

    .line 29
    invoke-static {p1}, Lcom/twitter/model/onboarding/permission/a$a;->h(Lcom/twitter/model/onboarding/permission/a$a;)Lcom/twitter/model/onboarding/permission/SystemPermissionState;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/onboarding/permission/a;->h:Lcom/twitter/model/onboarding/permission/SystemPermissionState;

    .line 30
    invoke-static {p1}, Lcom/twitter/model/onboarding/permission/a$a;->i(Lcom/twitter/model/onboarding/permission/a$a;)Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/onboarding/permission/a;->i:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    .line 31
    invoke-static {p1}, Lcom/twitter/model/onboarding/permission/a$a;->j(Lcom/twitter/model/onboarding/permission/a$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/onboarding/permission/a;->j:Ljava/lang/String;

    .line 32
    return-void
.end method
