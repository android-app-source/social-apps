.class public final Lcom/twitter/model/onboarding/permission/a$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/onboarding/permission/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/onboarding/permission/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/onboarding/permission/SystemPermissionState;

.field private final b:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

.field private c:J

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:J

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/twitter/model/onboarding/permission/SystemPermissionState;Lcom/twitter/model/onboarding/permission/InAppPermissionState;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/twitter/model/onboarding/permission/a$a;->a:Lcom/twitter/model/onboarding/permission/SystemPermissionState;

    .line 49
    iput-object p2, p0, Lcom/twitter/model/onboarding/permission/a$a;->b:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/twitter/model/onboarding/permission/a$a;)J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->c:J

    return-wide v0
.end method

.method static synthetic b(Lcom/twitter/model/onboarding/permission/a$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/onboarding/permission/a$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/onboarding/permission/a$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/model/onboarding/permission/a$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/model/onboarding/permission/a$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/model/onboarding/permission/a$a;)J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->i:J

    return-wide v0
.end method

.method static synthetic h(Lcom/twitter/model/onboarding/permission/a$a;)Lcom/twitter/model/onboarding/permission/SystemPermissionState;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->a:Lcom/twitter/model/onboarding/permission/SystemPermissionState;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/model/onboarding/permission/a$a;)Lcom/twitter/model/onboarding/permission/InAppPermissionState;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->b:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/model/onboarding/permission/a$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Lcom/twitter/util/object/i;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->f:Ljava/lang/String;

    .line 103
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->h:Ljava/lang/String;

    .line 104
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->a:Lcom/twitter/model/onboarding/permission/SystemPermissionState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/onboarding/permission/a$a;->b:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 102
    :goto_0
    return v0

    .line 104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcom/twitter/model/onboarding/permission/a$a;
    .locals 1

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/twitter/model/onboarding/permission/a$a;->c:J

    .line 55
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/onboarding/permission/a$a;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/twitter/model/onboarding/permission/a$a;->d:Ljava/lang/String;

    .line 61
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/onboarding/permission/a$a;
    .locals 1

    .prologue
    .line 90
    iput-wide p1, p0, Lcom/twitter/model/onboarding/permission/a$a;->i:J

    .line 91
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/onboarding/permission/a$a;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/twitter/model/onboarding/permission/a$a;->e:Ljava/lang/String;

    .line 67
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/twitter/model/onboarding/permission/a$a;->e()Lcom/twitter/model/onboarding/permission/a;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/onboarding/permission/a$a;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/twitter/model/onboarding/permission/a$a;->f:Ljava/lang/String;

    .line 73
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/onboarding/permission/a$a;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/twitter/model/onboarding/permission/a$a;->g:Ljava/lang/String;

    .line 79
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/onboarding/permission/a$a;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/twitter/model/onboarding/permission/a$a;->h:Ljava/lang/String;

    .line 85
    return-object p0
.end method

.method protected e()Lcom/twitter/model/onboarding/permission/a;
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/twitter/model/onboarding/permission/a;

    invoke-direct {v0, p0}, Lcom/twitter/model/onboarding/permission/a;-><init>(Lcom/twitter/model/onboarding/permission/a$a;)V

    return-object v0
.end method
