.class public final enum Lcom/twitter/model/onboarding/permission/InAppPermissionState;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/model/onboarding/permission/InAppPermissionState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

.field public static final enum b:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

.field public static final enum c:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

.field public static final enum d:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

.field private static final synthetic e:[Lcom/twitter/model/onboarding/permission/InAppPermissionState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    const-string/jumbo v1, "Undetermined"

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/onboarding/permission/InAppPermissionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;->a:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    new-instance v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    const-string/jumbo v1, "On"

    invoke-direct {v0, v1, v3}, Lcom/twitter/model/onboarding/permission/InAppPermissionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;->b:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    new-instance v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    const-string/jumbo v1, "Skipped"

    invoke-direct {v0, v1, v4}, Lcom/twitter/model/onboarding/permission/InAppPermissionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;->c:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    new-instance v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    const-string/jumbo v1, "NotApplicable"

    invoke-direct {v0, v1, v5}, Lcom/twitter/model/onboarding/permission/InAppPermissionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;->d:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    .line 3
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    sget-object v1, Lcom/twitter/model/onboarding/permission/InAppPermissionState;->a:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/model/onboarding/permission/InAppPermissionState;->b:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/model/onboarding/permission/InAppPermissionState;->c:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/model/onboarding/permission/InAppPermissionState;->d:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;->e:[Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/model/onboarding/permission/InAppPermissionState;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    return-object v0
.end method

.method public static values()[Lcom/twitter/model/onboarding/permission/InAppPermissionState;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;->e:[Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    invoke-virtual {v0}, [Lcom/twitter/model/onboarding/permission/InAppPermissionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    return-object v0
.end method
