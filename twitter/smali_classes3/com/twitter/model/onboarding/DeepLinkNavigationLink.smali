.class public Lcom/twitter/model/onboarding/DeepLinkNavigationLink;
.super Lcom/twitter/model/onboarding/NavigationLink;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/onboarding/DeepLinkNavigationLink$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/i",
            "<",
            "Lcom/twitter/model/onboarding/DeepLinkNavigationLink;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final deepLink:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    new-instance v0, Lcom/twitter/model/onboarding/DeepLinkNavigationLink$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/onboarding/DeepLinkNavigationLink$a;-><init>(Lcom/twitter/model/onboarding/DeepLinkNavigationLink$1;)V

    sput-object v0, Lcom/twitter/model/onboarding/DeepLinkNavigationLink;->a:Lcom/twitter/util/serialization/i;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/model/onboarding/NavigationLink;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/twitter/model/onboarding/DeepLinkNavigationLink;->deepLink:Ljava/lang/String;

    .line 18
    return-void
.end method
