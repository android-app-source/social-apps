.class public abstract Lcom/twitter/model/onboarding/Subtask;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BlacklistedInterface"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Lcom/twitter/model/onboarding/subtask/SubtaskProperties;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final id:Ljava/lang/String;

.field private final mProperties:Lcom/twitter/model/onboarding/subtask/SubtaskProperties;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lcom/twitter/model/onboarding/subtask/SubtaskProperties;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TP;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/twitter/model/onboarding/Subtask;->id:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/twitter/model/onboarding/Subtask;->mProperties:Lcom/twitter/model/onboarding/subtask/SubtaskProperties;

    .line 22
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/onboarding/subtask/SubtaskProperties;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/model/onboarding/Subtask;->mProperties:Lcom/twitter/model/onboarding/subtask/SubtaskProperties;

    return-object v0
.end method
