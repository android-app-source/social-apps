.class public Lcom/twitter/model/search/e;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/search/e$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/i;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Z

.field public final c:Z

.field public d:Ljava/lang/String;

.field public e:Lcom/twitter/model/core/TwitterSocialProof;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/twitter/model/search/e$a;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iget-object v0, p1, Lcom/twitter/model/search/e$a;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/search/e;->a:Ljava/util/List;

    .line 32
    iget-boolean v0, p1, Lcom/twitter/model/search/e$a;->b:Z

    iput-boolean v0, p0, Lcom/twitter/model/search/e;->b:Z

    .line 33
    iget-boolean v0, p1, Lcom/twitter/model/search/e$a;->c:Z

    iput-boolean v0, p0, Lcom/twitter/model/search/e;->c:Z

    .line 34
    iget-object v0, p1, Lcom/twitter/model/search/e$a;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/search/e;->d:Ljava/lang/String;

    .line 35
    iget-object v0, p1, Lcom/twitter/model/search/e$a;->e:Lcom/twitter/model/core/TwitterSocialProof;

    iput-object v0, p0, Lcom/twitter/model/search/e;->e:Lcom/twitter/model/core/TwitterSocialProof;

    .line 36
    iget-object v0, p1, Lcom/twitter/model/search/e$a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/search/e;->f:Ljava/lang/String;

    .line 37
    iget-object v0, p1, Lcom/twitter/model/search/e$a;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/search/e;->g:Ljava/lang/String;

    .line 38
    return-void
.end method
