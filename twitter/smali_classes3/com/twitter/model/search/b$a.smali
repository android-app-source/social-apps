.class public final Lcom/twitter/model/search/b$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/search/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/search/b;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/Float;

.field d:Ljava/lang/Float;

.field e:Ljava/lang/Float;

.field f:Ljava/lang/String;

.field g:J

.field h:J

.field i:Lcgi;

.field j:Z

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 87
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/search/b$a;->m:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/twitter/model/search/b$a;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/search/b$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcom/twitter/model/search/b$a;
    .locals 1

    .prologue
    .line 127
    iput-wide p1, p0, Lcom/twitter/model/search/b$a;->g:J

    .line 128
    return-object p0
.end method

.method public a(Lcgi;)Lcom/twitter/model/search/b$a;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/twitter/model/search/b$a;->i:Lcgi;

    .line 140
    return-object p0
.end method

.method public a(Ljava/lang/Float;)Lcom/twitter/model/search/b$a;
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/twitter/model/search/b$a;->c:Ljava/lang/Float;

    .line 104
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/search/b$a;
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/twitter/model/search/b$a;->a:Ljava/lang/String;

    .line 92
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/search/b$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcom/twitter/model/search/b$a;"
        }
    .end annotation

    .prologue
    .line 163
    if-eqz p1, :cond_0

    .line 164
    iget-object v0, p0, Lcom/twitter/model/search/b$a;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 165
    iget-object v0, p0, Lcom/twitter/model/search/b$a;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 167
    :cond_0
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/search/b$a;
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/twitter/model/search/b$a;->j:Z

    .line 146
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/search/b$a;
    .locals 1

    .prologue
    .line 133
    iput-wide p1, p0, Lcom/twitter/model/search/b$a;->h:J

    .line 134
    return-object p0
.end method

.method public b(Ljava/lang/Float;)Lcom/twitter/model/search/b$a;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/twitter/model/search/b$a;->d:Ljava/lang/Float;

    .line 110
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/search/b$a;
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/twitter/model/search/b$a;->b:Ljava/lang/String;

    .line 98
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/twitter/model/search/b$a;->e()Lcom/twitter/model/search/b;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Float;)Lcom/twitter/model/search/b$a;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/twitter/model/search/b$a;->e:Ljava/lang/Float;

    .line 116
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/search/b$a;
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/twitter/model/search/b$a;->f:Ljava/lang/String;

    .line 122
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/search/b$a;
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/twitter/model/search/b$a;->k:Ljava/lang/String;

    .line 152
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/search/b$a;
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/twitter/model/search/b$a;->l:Ljava/lang/String;

    .line 158
    return-object p0
.end method

.method protected e()Lcom/twitter/model/search/b;
    .locals 1

    .prologue
    .line 178
    new-instance v0, Lcom/twitter/model/search/b;

    invoke-direct {v0, p0}, Lcom/twitter/model/search/b;-><init>(Lcom/twitter/model/search/b$a;)V

    return-object v0
.end method
