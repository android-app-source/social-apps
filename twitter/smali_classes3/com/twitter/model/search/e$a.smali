.class public final Lcom/twitter/model/search/e$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/search/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/search/e;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/i;",
            ">;"
        }
    .end annotation
.end field

.field b:Z

.field c:Z

.field d:Ljava/lang/String;

.field e:Lcom/twitter/model/core/TwitterSocialProof;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/TwitterSocialProof;)Lcom/twitter/model/search/e$a;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/twitter/model/search/e$a;->e:Lcom/twitter/model/core/TwitterSocialProof;

    .line 81
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/search/e$a;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/twitter/model/search/e$a;->d:Ljava/lang/String;

    .line 75
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/search/e$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/i;",
            ">;)",
            "Lcom/twitter/model/search/e$a;"
        }
    .end annotation

    .prologue
    .line 56
    iput-object p1, p0, Lcom/twitter/model/search/e$a;->a:Ljava/util/List;

    .line 57
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/search/e$a;
    .locals 0

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/twitter/model/search/e$a;->b:Z

    .line 63
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/search/e$a;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/twitter/model/search/e$a;->f:Ljava/lang/String;

    .line 87
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/search/e$a;
    .locals 0

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/twitter/model/search/e$a;->c:Z

    .line 69
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/twitter/model/search/e$a;->e()Lcom/twitter/model/search/e;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/search/e$a;
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/twitter/model/search/e$a;->g:Ljava/lang/String;

    .line 93
    return-object p0
.end method

.method protected e()Lcom/twitter/model/search/e;
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lcom/twitter/model/search/e;

    invoke-direct {v0, p0}, Lcom/twitter/model/search/e;-><init>(Lcom/twitter/model/search/e$a;)V

    return-object v0
.end method
