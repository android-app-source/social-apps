.class public Lcom/twitter/model/search/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/search/b$b;,
        Lcom/twitter/model/search/b$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/search/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/Float;

.field public final e:Ljava/lang/Float;

.field public final f:Ljava/lang/Float;

.field public final g:Ljava/lang/String;

.field public final h:J

.field public i:J

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Lcgi;

.field public final l:Z

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/twitter/model/search/b$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/search/b$b;-><init>(Lcom/twitter/model/search/b$1;)V

    sput-object v0, Lcom/twitter/model/search/b;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/search/b$a;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iget-object v0, p1, Lcom/twitter/model/search/b$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/search/b;->b:Ljava/lang/String;

    .line 59
    iget-object v0, p1, Lcom/twitter/model/search/b$a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/search/b;->c:Ljava/lang/String;

    .line 60
    iget-object v0, p1, Lcom/twitter/model/search/b$a;->c:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/model/search/b;->d:Ljava/lang/Float;

    .line 61
    iget-object v0, p1, Lcom/twitter/model/search/b$a;->d:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/model/search/b;->e:Ljava/lang/Float;

    .line 62
    iget-object v0, p1, Lcom/twitter/model/search/b$a;->e:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/model/search/b;->f:Ljava/lang/Float;

    .line 63
    iget-object v0, p1, Lcom/twitter/model/search/b$a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/search/b;->g:Ljava/lang/String;

    .line 64
    iget-wide v0, p1, Lcom/twitter/model/search/b$a;->h:J

    iput-wide v0, p0, Lcom/twitter/model/search/b;->i:J

    .line 65
    iget-wide v0, p1, Lcom/twitter/model/search/b$a;->g:J

    iput-wide v0, p0, Lcom/twitter/model/search/b;->h:J

    .line 66
    iget-object v0, p1, Lcom/twitter/model/search/b$a;->m:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/search/b;->j:Ljava/util/List;

    .line 67
    iget-object v0, p1, Lcom/twitter/model/search/b$a;->i:Lcgi;

    iput-object v0, p0, Lcom/twitter/model/search/b;->k:Lcgi;

    .line 68
    iget-boolean v0, p1, Lcom/twitter/model/search/b$a;->j:Z

    iput-boolean v0, p0, Lcom/twitter/model/search/b;->l:Z

    .line 69
    iget-object v0, p1, Lcom/twitter/model/search/b$a;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/search/b;->m:Ljava/lang/String;

    .line 70
    iget-object v0, p1, Lcom/twitter/model/search/b$a;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/search/b;->n:Ljava/lang/String;

    .line 71
    return-void
.end method
