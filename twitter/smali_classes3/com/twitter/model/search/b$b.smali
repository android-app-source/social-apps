.class Lcom/twitter/model/search/b$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/search/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/search/b;",
        "Lcom/twitter/model/search/b$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/search/b$1;)V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/twitter/model/search/b$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/search/b$a;
    .locals 1

    .prologue
    .line 206
    new-instance v0, Lcom/twitter/model/search/b$a;

    invoke-direct {v0}, Lcom/twitter/model/search/b$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/search/b$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 212
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/search/b$a;->a(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v0

    .line 213
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/b$a;->b(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->g:Lcom/twitter/util/serialization/l;

    .line 214
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v1, v0}, Lcom/twitter/model/search/b$a;->a(Ljava/lang/Float;)Lcom/twitter/model/search/b$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->g:Lcom/twitter/util/serialization/l;

    .line 215
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v1, v0}, Lcom/twitter/model/search/b$a;->b(Ljava/lang/Float;)Lcom/twitter/model/search/b$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->g:Lcom/twitter/util/serialization/l;

    .line 216
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v1, v0}, Lcom/twitter/model/search/b$a;->c(Ljava/lang/Float;)Lcom/twitter/model/search/b$a;

    move-result-object v0

    .line 217
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/b$a;->c(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v0

    .line 218
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/search/b$a;->a(J)Lcom/twitter/model/search/b$a;

    move-result-object v0

    .line 219
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/search/b$a;->b(J)Lcom/twitter/model/search/b$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    .line 220
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/model/search/b$a;->a(Ljava/util/List;)Lcom/twitter/model/search/b$a;

    move-result-object v1

    sget-object v0, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 221
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    invoke-virtual {v1, v0}, Lcom/twitter/model/search/b$a;->a(Lcgi;)Lcom/twitter/model/search/b$a;

    move-result-object v0

    .line 222
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/b$a;->a(Z)Lcom/twitter/model/search/b$a;

    move-result-object v0

    .line 223
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/b$a;->d(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v0

    .line 224
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/b$a;->e(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    .line 225
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 182
    check-cast p2, Lcom/twitter/model/search/b$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/search/b$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/search/b$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/search/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    iget-object v0, p2, Lcom/twitter/model/search/b;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/b;->c:Ljava/lang/String;

    .line 188
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/b;->d:Ljava/lang/Float;

    sget-object v2, Lcom/twitter/util/serialization/f;->g:Lcom/twitter/util/serialization/l;

    .line 189
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/b;->e:Ljava/lang/Float;

    sget-object v2, Lcom/twitter/util/serialization/f;->g:Lcom/twitter/util/serialization/l;

    .line 190
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/b;->f:Ljava/lang/Float;

    sget-object v2, Lcom/twitter/util/serialization/f;->g:Lcom/twitter/util/serialization/l;

    .line 191
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/b;->g:Ljava/lang/String;

    .line 192
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/search/b;->h:J

    .line 193
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/search/b;->i:J

    .line 194
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/b;->j:Ljava/util/List;

    sget-object v2, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    .line 196
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 195
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/b;->k:Lcgi;

    sget-object v2, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 197
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/search/b;->l:Z

    .line 198
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/b;->m:Ljava/lang/String;

    .line 199
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/b;->n:Ljava/lang/String;

    .line 200
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 201
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    check-cast p2, Lcom/twitter/model/search/b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/search/b$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/search/b;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/twitter/model/search/b$b;->a()Lcom/twitter/model/search/b$a;

    move-result-object v0

    return-object v0
.end method
