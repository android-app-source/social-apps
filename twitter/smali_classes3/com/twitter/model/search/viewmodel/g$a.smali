.class public final Lcom/twitter/model/search/viewmodel/g$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/search/viewmodel/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/search/viewmodel/g;",
        ">;"
    }
.end annotation


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 80
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/model/search/viewmodel/g$a;->a:J

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/search/viewmodel/g$a;)J
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/twitter/model/search/viewmodel/g$a;->a:J

    return-wide v0
.end method

.method static synthetic b(Lcom/twitter/model/search/viewmodel/g$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/g$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/search/viewmodel/g$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/g$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/search/viewmodel/g$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/g$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/model/search/viewmodel/g$a;)Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/twitter/model/search/viewmodel/g$a;->e:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/model/search/viewmodel/g$a;)I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/twitter/model/search/viewmodel/g$a;->f:I

    return v0
.end method

.method static synthetic g(Lcom/twitter/model/search/viewmodel/g$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/g$a;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/model/search/viewmodel/g$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/g$a;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/model/search/viewmodel/g$a;
    .locals 0

    .prologue
    .line 123
    iput p1, p0, Lcom/twitter/model/search/viewmodel/g$a;->f:I

    .line 124
    return-object p0
.end method

.method public a(J)Lcom/twitter/model/search/viewmodel/g$a;
    .locals 1

    .prologue
    .line 93
    iput-wide p1, p0, Lcom/twitter/model/search/viewmodel/g$a;->a:J

    .line 94
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/twitter/model/search/viewmodel/g$a;->b:Ljava/lang/String;

    .line 100
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/search/viewmodel/g$a;
    .locals 0

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/twitter/model/search/viewmodel/g$a;->e:Z

    .line 118
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/twitter/model/search/viewmodel/g$a;->c:Ljava/lang/String;

    .line 106
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/twitter/model/search/viewmodel/g$a;->e()Lcom/twitter/model/search/viewmodel/g;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/twitter/model/search/viewmodel/g$a;->d:Ljava/lang/String;

    .line 112
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/twitter/model/search/viewmodel/g$a;->g:Ljava/lang/String;

    .line 130
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/twitter/model/search/viewmodel/g$a;->h:Ljava/lang/String;

    .line 136
    return-object p0
.end method

.method protected e()Lcom/twitter/model/search/viewmodel/g;
    .locals 1

    .prologue
    .line 142
    new-instance v0, Lcom/twitter/model/search/viewmodel/g;

    invoke-direct {v0, p0}, Lcom/twitter/model/search/viewmodel/g;-><init>(Lcom/twitter/model/search/viewmodel/g$a;)V

    return-object v0
.end method
