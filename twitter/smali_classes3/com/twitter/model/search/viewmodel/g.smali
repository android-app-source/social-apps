.class public Lcom/twitter/model/search/viewmodel/g;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/search/viewmodel/g$a;,
        Lcom/twitter/model/search/viewmodel/g$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/search/viewmodel/g;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Z

.field public final g:I

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/model/search/viewmodel/g$b;

    invoke-direct {v0}, Lcom/twitter/model/search/viewmodel/g$b;-><init>()V

    sput-object v0, Lcom/twitter/model/search/viewmodel/g;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/search/viewmodel/g$a;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/twitter/model/search/viewmodel/g$a;->a(Lcom/twitter/model/search/viewmodel/g$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/search/viewmodel/g;->b:J

    .line 31
    invoke-static {p1}, Lcom/twitter/model/search/viewmodel/g$a;->b(Lcom/twitter/model/search/viewmodel/g$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/search/viewmodel/g;->c:Ljava/lang/String;

    .line 32
    invoke-static {p1}, Lcom/twitter/model/search/viewmodel/g$a;->c(Lcom/twitter/model/search/viewmodel/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/search/viewmodel/g;->d:Ljava/lang/String;

    .line 33
    invoke-static {p1}, Lcom/twitter/model/search/viewmodel/g$a;->d(Lcom/twitter/model/search/viewmodel/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/search/viewmodel/g;->e:Ljava/lang/String;

    .line 34
    invoke-static {p1}, Lcom/twitter/model/search/viewmodel/g$a;->e(Lcom/twitter/model/search/viewmodel/g$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/search/viewmodel/g;->f:Z

    .line 35
    invoke-static {p1}, Lcom/twitter/model/search/viewmodel/g$a;->f(Lcom/twitter/model/search/viewmodel/g$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/search/viewmodel/g;->g:I

    .line 36
    invoke-static {p1}, Lcom/twitter/model/search/viewmodel/g$a;->g(Lcom/twitter/model/search/viewmodel/g$a;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 37
    invoke-static {p1}, Lcom/twitter/model/search/viewmodel/g$a;->g(Lcom/twitter/model/search/viewmodel/g$a;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/twitter/model/search/viewmodel/g;->h:Ljava/lang/String;

    .line 39
    invoke-static {p1}, Lcom/twitter/model/search/viewmodel/g$a;->h(Lcom/twitter/model/search/viewmodel/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/search/viewmodel/g;->i:Ljava/lang/String;

    .line 40
    return-void

    .line 37
    :cond_0
    const-string/jumbo v0, "none"

    goto :goto_0
.end method
