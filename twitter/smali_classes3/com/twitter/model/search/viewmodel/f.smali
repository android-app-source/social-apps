.class public Lcom/twitter/model/search/viewmodel/f;
.super Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;
.source "Twttr"


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 18
    sget-object v4, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->c:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    move-object v3, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-wide/from16 v10, p6

    invoke-direct/range {v3 .. v11}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;-><init>(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;J)V

    .line 19
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/twitter/model/search/viewmodel/f;->b:Ljava/lang/String;

    .line 20
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/twitter/model/search/viewmodel/f;->c:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/f;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/f;->c:Ljava/lang/String;

    return-object v0
.end method
