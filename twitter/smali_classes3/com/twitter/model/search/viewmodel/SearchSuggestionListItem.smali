.class public Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;
    }
.end annotation


# static fields
.field public static final a:I


# instance fields
.field private final b:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Landroid/net/Uri;

.field private final g:Ljava/lang/String;

.field private final h:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->values()[Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    move-result-object v0

    array-length v0, v0

    sput v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->b:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 33
    iput-object p2, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->c:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->d:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->e:Ljava/lang/String;

    .line 36
    iput-object p5, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->f:Landroid/net/Uri;

    .line 37
    iput-object p6, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->g:Ljava/lang/String;

    .line 38
    iput-wide p7, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->h:J

    .line 39
    return-void
.end method


# virtual methods
.method public c()Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->b:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->d:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->e:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->h:J

    return-wide v0
.end method
