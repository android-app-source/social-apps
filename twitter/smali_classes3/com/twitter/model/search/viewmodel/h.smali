.class public Lcom/twitter/model/search/viewmodel/h;
.super Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;
.source "Twttr"


# instance fields
.field private final b:Lcom/twitter/model/search/viewmodel/g;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;JLcom/twitter/model/search/viewmodel/g;)V
    .locals 12

    .prologue
    .line 19
    sget-object v4, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->b:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    move-object v3, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-wide/from16 v10, p6

    invoke-direct/range {v3 .. v11}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;-><init>(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;J)V

    .line 20
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/twitter/model/search/viewmodel/h;->b:Lcom/twitter/model/search/viewmodel/g;

    .line 21
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/search/viewmodel/g;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/h;->b:Lcom/twitter/model/search/viewmodel/g;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/h;->b:Lcom/twitter/model/search/viewmodel/g;

    iget-object v0, v0, Lcom/twitter/model/search/viewmodel/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/h;->b:Lcom/twitter/model/search/viewmodel/g;

    iget-object v0, v0, Lcom/twitter/model/search/viewmodel/g;->d:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/h;->b:Lcom/twitter/model/search/viewmodel/g;

    iget-object v0, v0, Lcom/twitter/model/search/viewmodel/g;->e:Ljava/lang/String;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/h;->b:Lcom/twitter/model/search/viewmodel/g;

    iget-boolean v0, v0, Lcom/twitter/model/search/viewmodel/g;->f:Z

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/h;->b:Lcom/twitter/model/search/viewmodel/g;

    iget v0, v0, Lcom/twitter/model/search/viewmodel/g;->g:I

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/h;->b:Lcom/twitter/model/search/viewmodel/g;

    iget-object v0, v0, Lcom/twitter/model/search/viewmodel/g;->h:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/h;->b:Lcom/twitter/model/search/viewmodel/g;

    iget-object v0, v0, Lcom/twitter/model/search/viewmodel/g;->i:Ljava/lang/String;

    return-object v0
.end method
