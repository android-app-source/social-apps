.class public final enum Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

.field public static final enum b:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

.field public static final enum c:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

.field public static final enum d:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

.field public static final enum e:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

.field public static final enum f:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

.field public static final enum g:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

.field public static final enum h:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

.field private static final synthetic i:[Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    const-string/jumbo v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->a:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 13
    new-instance v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    const-string/jumbo v1, "USER"

    invoke-direct {v0, v1, v4}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->b:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 14
    new-instance v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    const-string/jumbo v1, "TREND"

    invoke-direct {v0, v1, v5}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->c:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 15
    new-instance v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    const-string/jumbo v1, "SAVED"

    invoke-direct {v0, v1, v6}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->d:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 16
    new-instance v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    const-string/jumbo v1, "RECENT"

    invoke-direct {v0, v1, v7}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->e:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 17
    new-instance v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    const-string/jumbo v1, "REALTIME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->f:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 18
    new-instance v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    const-string/jumbo v1, "HEADER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->g:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 19
    new-instance v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    const-string/jumbo v1, "DIVIDER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->h:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    .line 11
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    sget-object v1, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->a:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->b:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->c:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->d:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->e:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->f:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->g:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->h:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->i:[Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    return-object v0
.end method

.method public static values()[Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->i:[Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    invoke-virtual {v0}, [Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    return-object v0
.end method
