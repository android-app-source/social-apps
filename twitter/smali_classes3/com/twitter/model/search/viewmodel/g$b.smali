.class public Lcom/twitter/model/search/viewmodel/g$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/search/viewmodel/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/search/viewmodel/g;",
        "Lcom/twitter/model/search/viewmodel/g$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/search/viewmodel/g$a;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/twitter/model/search/viewmodel/g$a;

    invoke-direct {v0}, Lcom/twitter/model/search/viewmodel/g$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/search/viewmodel/g$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/search/viewmodel/g$a;->a(J)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v0

    .line 68
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/viewmodel/g$a;->a(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v0

    .line 69
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/viewmodel/g$a;->b(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v0

    .line 70
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/viewmodel/g$a;->c(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v0

    .line 71
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/viewmodel/g$a;->a(Z)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v0

    .line 72
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/viewmodel/g$a;->a(I)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v0

    .line 73
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/viewmodel/g$a;->d(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v0

    .line 74
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/viewmodel/g$a;->e(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;

    .line 75
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 42
    check-cast p2, Lcom/twitter/model/search/viewmodel/g$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/search/viewmodel/g$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/search/viewmodel/g$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/search/viewmodel/g;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-wide v0, p2, Lcom/twitter/model/search/viewmodel/g;->b:J

    .line 48
    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/viewmodel/g;->c:Ljava/lang/String;

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/viewmodel/g;->d:Ljava/lang/String;

    .line 50
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/viewmodel/g;->e:Ljava/lang/String;

    .line 51
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/search/viewmodel/g;->f:Z

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/search/viewmodel/g;->g:I

    .line 53
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/viewmodel/g;->h:Ljava/lang/String;

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/search/viewmodel/g;->i:Ljava/lang/String;

    .line 55
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 56
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    check-cast p2, Lcom/twitter/model/search/viewmodel/g;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/search/viewmodel/g$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/search/viewmodel/g;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/twitter/model/search/viewmodel/g$b;->a()Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v0

    return-object v0
.end method
