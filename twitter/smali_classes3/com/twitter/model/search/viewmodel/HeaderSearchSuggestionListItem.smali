.class public Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;
.super Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;
    }
.end annotation


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 10
    sget-object v2, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->g:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    const-string/jumbo v3, ""

    const-string/jumbo v4, ""

    const-wide/16 v8, 0x0

    move-object v1, p0

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v1 .. v9}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;-><init>(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;J)V

    .line 11
    iput-object p1, p0, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;->b:Ljava/lang/String;

    .line 12
    iput-object p2, p0, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;->c:Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;

    .line 13
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;->c:Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;

    return-object v0
.end method
