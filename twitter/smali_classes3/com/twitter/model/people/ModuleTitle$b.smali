.class Lcom/twitter/model/people/ModuleTitle$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/people/ModuleTitle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/people/ModuleTitle;",
        "Lcom/twitter/model/people/ModuleTitle$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/people/ModuleTitle$1;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/twitter/model/people/ModuleTitle$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/people/ModuleTitle$a;
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/twitter/model/people/ModuleTitle$a;

    invoke-direct {v0}, Lcom/twitter/model/people/ModuleTitle$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/people/ModuleTitle$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/people/ModuleTitle$a;->a(Ljava/lang/String;)Lcom/twitter/model/people/ModuleTitle$a;

    .line 50
    const-class v0, Lcom/twitter/model/people/ModuleTitle$Icon;

    .line 51
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/people/ModuleTitle$Icon;->a:Lcom/twitter/model/people/ModuleTitle$Icon;

    .line 50
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/ModuleTitle$Icon;

    invoke-virtual {p2, v0}, Lcom/twitter/model/people/ModuleTitle$a;->a(Lcom/twitter/model/people/ModuleTitle$Icon;)Lcom/twitter/model/people/ModuleTitle$a;

    .line 52
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p2, Lcom/twitter/model/people/ModuleTitle$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/people/ModuleTitle$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/people/ModuleTitle$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/ModuleTitle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p2, Lcom/twitter/model/people/ModuleTitle;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 37
    iget-object v0, p2, Lcom/twitter/model/people/ModuleTitle;->d:Lcom/twitter/model/people/ModuleTitle$Icon;

    const-class v1, Lcom/twitter/model/people/ModuleTitle$Icon;

    invoke-static {v1}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 38
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p2, Lcom/twitter/model/people/ModuleTitle;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/people/ModuleTitle$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/ModuleTitle;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/twitter/model/people/ModuleTitle$b;->a()Lcom/twitter/model/people/ModuleTitle$a;

    move-result-object v0

    return-object v0
.end method
