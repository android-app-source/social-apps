.class public abstract Lcom/twitter/model/people/a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/people/a$a;,
        Lcom/twitter/model/people/a$d;,
        Lcom/twitter/model/people/a$c;,
        Lcom/twitter/model/people/a$b;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/people/d;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/twitter/model/people/k;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/people/l;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/twitter/model/people/i;


# direct methods
.method protected constructor <init>(Lcom/twitter/model/people/a$a;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lcom/twitter/model/people/a$a;->a(Lcom/twitter/model/people/a$a;)Lcom/twitter/model/people/d;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/people/d;->b:Lcom/twitter/model/people/d;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/d;

    iput-object v0, p0, Lcom/twitter/model/people/a;->a:Lcom/twitter/model/people/d;

    .line 33
    invoke-static {p1}, Lcom/twitter/model/people/a$a;->b(Lcom/twitter/model/people/a$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/people/a;->b:Ljava/lang/String;

    .line 34
    invoke-static {p1}, Lcom/twitter/model/people/a$a;->c(Lcom/twitter/model/people/a$a;)Lcom/twitter/model/people/k;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/people/k;->b:Lcom/twitter/model/people/k;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/k;

    iput-object v0, p0, Lcom/twitter/model/people/a;->c:Lcom/twitter/model/people/k;

    .line 35
    invoke-static {p1}, Lcom/twitter/model/people/a$a;->d(Lcom/twitter/model/people/a$a;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/people/a;->d:Ljava/util/List;

    .line 36
    invoke-static {p1}, Lcom/twitter/model/people/a$a;->e(Lcom/twitter/model/people/a$a;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/people/a;->e:Ljava/util/List;

    .line 37
    invoke-static {p1}, Lcom/twitter/model/people/a$a;->f(Lcom/twitter/model/people/a$a;)Lcom/twitter/model/people/i;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/i;

    iput-object v0, p0, Lcom/twitter/model/people/a;->f:Lcom/twitter/model/people/i;

    .line 38
    return-void
.end method
