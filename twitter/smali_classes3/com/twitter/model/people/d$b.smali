.class Lcom/twitter/model/people/d$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/people/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/people/d;",
        "Lcom/twitter/model/people/d$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/people/d$1;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/twitter/model/people/d$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/people/d$a;
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/twitter/model/people/d$a;

    invoke-direct {v0}, Lcom/twitter/model/people/d$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/people/d$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 56
    sget-object v0, Lcom/twitter/model/people/ModuleTitle;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/ModuleTitle;

    invoke-virtual {p2, v0}, Lcom/twitter/model/people/d$a;->a(Lcom/twitter/model/people/ModuleTitle;)Lcom/twitter/model/people/d$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/people/ModuleTitle;->a:Lcom/twitter/util/serialization/l;

    .line 57
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/ModuleTitle;

    invoke-virtual {v1, v0}, Lcom/twitter/model/people/d$a;->b(Lcom/twitter/model/people/ModuleTitle;)Lcom/twitter/model/people/d$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/people/k;->a:Lcom/twitter/util/serialization/l;

    .line 58
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/k;

    invoke-virtual {v1, v0}, Lcom/twitter/model/people/d$a;->a(Lcom/twitter/model/people/k;)Lcom/twitter/model/people/d$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/people/c;->a:Lcom/twitter/util/serialization/l;

    .line 59
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/c;

    invoke-virtual {v1, v0}, Lcom/twitter/model/people/d$a;->a(Lcom/twitter/model/people/c;)Lcom/twitter/model/people/d$a;

    .line 60
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 37
    check-cast p2, Lcom/twitter/model/people/d$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/people/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/people/d$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p2, Lcom/twitter/model/people/d;->c:Lcom/twitter/model/people/ModuleTitle;

    sget-object v1, Lcom/twitter/model/people/ModuleTitle;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 42
    iget-object v0, p2, Lcom/twitter/model/people/d;->d:Lcom/twitter/model/people/ModuleTitle;

    sget-object v1, Lcom/twitter/model/people/ModuleTitle;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 43
    iget-object v0, p2, Lcom/twitter/model/people/d;->e:Lcom/twitter/model/people/k;

    sget-object v1, Lcom/twitter/model/people/k;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 44
    iget-object v0, p2, Lcom/twitter/model/people/d;->f:Lcom/twitter/model/people/c;

    sget-object v1, Lcom/twitter/model/people/c;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 45
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    check-cast p2, Lcom/twitter/model/people/d;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/people/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/d;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/twitter/model/people/d$b;->a()Lcom/twitter/model/people/d$a;

    move-result-object v0

    return-object v0
.end method
