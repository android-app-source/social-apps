.class abstract Lcom/twitter/model/people/a$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/people/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<B:",
        "Lcom/twitter/model/people/a$a",
        "<TB;TT;>;T:",
        "Lcom/twitter/model/people/a;",
        ">",
        "Lcom/twitter/util/object/i",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/model/people/d;

.field private b:Ljava/lang/String;

.field private c:Lcom/twitter/model/people/k;

.field private d:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/people/l;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/twitter/model/people/i;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/people/a$a;)Lcom/twitter/model/people/d;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/model/people/a$a;->a:Lcom/twitter/model/people/d;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/people/a$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/model/people/a$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/people/a$a;)Lcom/twitter/model/people/k;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/model/people/a$a;->c:Lcom/twitter/model/people/k;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/people/a$a;)Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/model/people/a$a;->d:Ljava/lang/Iterable;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/model/people/a$a;)Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/model/people/a$a;->e:Ljava/lang/Iterable;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/model/people/a$a;)Lcom/twitter/model/people/i;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/model/people/a$a;->f:Lcom/twitter/model/people/i;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/people/d;)Lcom/twitter/model/people/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/d;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 135
    iput-object p1, p0, Lcom/twitter/model/people/a$a;->a:Lcom/twitter/model/people/d;

    .line 136
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/a$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/people/i;)Lcom/twitter/model/people/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/i;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 165
    iput-object p1, p0, Lcom/twitter/model/people/a$a;->f:Lcom/twitter/model/people/i;

    .line 166
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/a$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/people/k;)Lcom/twitter/model/people/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/k;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 147
    iput-object p1, p0, Lcom/twitter/model/people/a$a;->c:Lcom/twitter/model/people/k;

    .line 148
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/a$a;

    return-object v0
.end method

.method public a(Ljava/lang/Iterable;)Lcom/twitter/model/people/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/people/l;",
            ">;)TB;"
        }
    .end annotation

    .prologue
    .line 153
    iput-object p1, p0, Lcom/twitter/model/people/a$a;->d:Ljava/lang/Iterable;

    .line 154
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/a$a;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/people/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 141
    iput-object p1, p0, Lcom/twitter/model/people/a$a;->b:Ljava/lang/String;

    .line 142
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/a$a;

    return-object v0
.end method

.method public b(Ljava/lang/Iterable;)Lcom/twitter/model/people/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;)TB;"
        }
    .end annotation

    .prologue
    .line 159
    iput-object p1, p0, Lcom/twitter/model/people/a$a;->e:Ljava/lang/Iterable;

    .line 160
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/a$a;

    return-object v0
.end method
