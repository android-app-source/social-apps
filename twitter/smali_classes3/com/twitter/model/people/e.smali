.class public Lcom/twitter/model/people/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/people/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/people/e$b;,
        Lcom/twitter/model/people/e$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/twitter/model/people/f;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/model/people/e$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/people/e$b;-><init>(Lcom/twitter/model/people/e$1;)V

    sput-object v0, Lcom/twitter/model/people/e;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/people/e$a;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/twitter/model/people/e$a;->a(Lcom/twitter/model/people/e$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/people/e;->b:Ljava/lang/String;

    .line 32
    invoke-static {p1}, Lcom/twitter/model/people/e$a;->b(Lcom/twitter/model/people/e$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/people/e;->c:Ljava/lang/String;

    .line 33
    invoke-static {p1}, Lcom/twitter/model/people/e$a;->c(Lcom/twitter/model/people/e$a;)Lcom/twitter/model/people/f;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/f;

    iput-object v0, p0, Lcom/twitter/model/people/e;->d:Lcom/twitter/model/people/f;

    .line 34
    invoke-static {p1}, Lcom/twitter/model/people/e$a;->d(Lcom/twitter/model/people/e$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/people/e;->e:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/model/people/e;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/model/people/e;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lcom/twitter/model/people/f;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/model/people/e;->d:Lcom/twitter/model/people/f;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/model/people/e;->e:Ljava/lang/String;

    return-object v0
.end method
