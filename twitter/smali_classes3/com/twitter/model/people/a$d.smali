.class Lcom/twitter/model/people/a$d;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/people/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/people/l;",
        "Lcom/twitter/model/people/l$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/people/a$1;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/twitter/model/people/a$d;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/people/l$a;
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lcom/twitter/model/people/l$a;

    invoke-direct {v0}, Lcom/twitter/model/people/l$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/people/l$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 115
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    .line 117
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 116
    invoke-virtual {p2, v0}, Lcom/twitter/model/people/l$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/people/l$a;

    move-result-object v0

    .line 119
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/l$a;->a(Ljava/lang/String;)Lcom/twitter/model/people/l$a;

    move-result-object v0

    .line 120
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/l$a;->b(Ljava/lang/String;)Lcom/twitter/model/people/l$a;

    move-result-object v0

    .line 121
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/l$a;->a(Z)Lcom/twitter/model/people/l$a;

    .line 122
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 95
    check-cast p2, Lcom/twitter/model/people/l$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/people/a$d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/people/l$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/l;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p2, Lcom/twitter/model/people/l;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/people/l;->b:Ljava/lang/String;

    .line 101
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/people/l;->c:Ljava/lang/String;

    .line 102
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/people/l;->d:Z

    .line 103
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 104
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    check-cast p2, Lcom/twitter/model/people/l;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/people/a$d;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/l;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/twitter/model/people/a$d;->a()Lcom/twitter/model/people/l$a;

    move-result-object v0

    return-object v0
.end method
