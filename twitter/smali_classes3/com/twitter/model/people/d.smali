.class public Lcom/twitter/model/people/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/people/d$a;,
        Lcom/twitter/model/people/d$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/people/d;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/twitter/model/people/d;


# instance fields
.field public final c:Lcom/twitter/model/people/ModuleTitle;

.field public final d:Lcom/twitter/model/people/ModuleTitle;

.field public final e:Lcom/twitter/model/people/k;

.field public final f:Lcom/twitter/model/people/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/model/people/d$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/people/d$b;-><init>(Lcom/twitter/model/people/d$1;)V

    sput-object v0, Lcom/twitter/model/people/d;->a:Lcom/twitter/util/serialization/l;

    .line 19
    new-instance v0, Lcom/twitter/model/people/d$a;

    invoke-direct {v0}, Lcom/twitter/model/people/d$a;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/model/people/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/d;

    sput-object v0, Lcom/twitter/model/people/d;->b:Lcom/twitter/model/people/d;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/people/d$a;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/twitter/model/people/d$a;->a(Lcom/twitter/model/people/d$a;)Lcom/twitter/model/people/ModuleTitle;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/people/ModuleTitle;->b:Lcom/twitter/model/people/ModuleTitle;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/ModuleTitle;

    iput-object v0, p0, Lcom/twitter/model/people/d;->c:Lcom/twitter/model/people/ModuleTitle;

    .line 32
    invoke-static {p1}, Lcom/twitter/model/people/d$a;->b(Lcom/twitter/model/people/d$a;)Lcom/twitter/model/people/ModuleTitle;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/people/ModuleTitle;->b:Lcom/twitter/model/people/ModuleTitle;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/ModuleTitle;

    iput-object v0, p0, Lcom/twitter/model/people/d;->d:Lcom/twitter/model/people/ModuleTitle;

    .line 33
    invoke-static {p1}, Lcom/twitter/model/people/d$a;->c(Lcom/twitter/model/people/d$a;)Lcom/twitter/model/people/k;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/people/k;->b:Lcom/twitter/model/people/k;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/k;

    iput-object v0, p0, Lcom/twitter/model/people/d;->e:Lcom/twitter/model/people/k;

    .line 34
    invoke-static {p1}, Lcom/twitter/model/people/d$a;->d(Lcom/twitter/model/people/d$a;)Lcom/twitter/model/people/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/people/c;->b:Lcom/twitter/model/people/c;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/c;

    iput-object v0, p0, Lcom/twitter/model/people/d;->f:Lcom/twitter/model/people/c;

    .line 35
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/people/d$a;Lcom/twitter/model/people/d$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/twitter/model/people/d;-><init>(Lcom/twitter/model/people/d$a;)V

    return-void
.end method
