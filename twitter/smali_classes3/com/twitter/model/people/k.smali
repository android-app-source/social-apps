.class public Lcom/twitter/model/people/k;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/people/k$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/people/k;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/twitter/model/people/k;


# instance fields
.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    new-instance v0, Lcom/twitter/model/people/k$a;

    invoke-direct {v0, v1}, Lcom/twitter/model/people/k$a;-><init>(Lcom/twitter/model/people/k$1;)V

    sput-object v0, Lcom/twitter/model/people/k;->a:Lcom/twitter/util/serialization/l;

    .line 18
    new-instance v0, Lcom/twitter/model/people/k;

    invoke-direct {v0, v1, v1}, Lcom/twitter/model/people/k;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/twitter/model/people/k;->b:Lcom/twitter/model/people/k;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/people/k;->c:Ljava/lang/String;

    .line 27
    invoke-static {p2}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/people/k;->d:Ljava/lang/String;

    .line 28
    return-void
.end method
