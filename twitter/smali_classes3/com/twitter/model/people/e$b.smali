.class Lcom/twitter/model/people/e$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/people/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/people/b;",
        "Lcom/twitter/model/people/e$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/people/e$1;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/twitter/model/people/e$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/people/e$a;
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/twitter/model/people/e$a;

    invoke-direct {v0}, Lcom/twitter/model/people/e$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/people/e$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 121
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/people/e$a;->b(Ljava/lang/String;)Lcom/twitter/model/people/e$a;

    move-result-object v0

    .line 122
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/e$a;->a(Ljava/lang/String;)Lcom/twitter/model/people/e$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/people/f;->a:Lcom/twitter/util/serialization/l;

    .line 123
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/f;

    invoke-virtual {v1, v0}, Lcom/twitter/model/people/e$a;->a(Lcom/twitter/model/people/f;)Lcom/twitter/model/people/e$a;

    move-result-object v0

    .line 124
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/e$a;->c(Ljava/lang/String;)Lcom/twitter/model/people/e$a;

    .line 125
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 103
    check-cast p2, Lcom/twitter/model/people/e$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/people/e$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/people/e$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-interface {p2}, Lcom/twitter/model/people/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 107
    invoke-interface {p2}, Lcom/twitter/model/people/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 108
    invoke-interface {p2}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/people/f;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 109
    invoke-interface {p2}, Lcom/twitter/model/people/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 110
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    check-cast p2, Lcom/twitter/model/people/b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/people/e$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/b;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/twitter/model/people/e$b;->a()Lcom/twitter/model/people/e$a;

    move-result-object v0

    return-object v0
.end method
