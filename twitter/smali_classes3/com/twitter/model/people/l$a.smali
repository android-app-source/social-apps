.class public final Lcom/twitter/model/people/l$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/people/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/people/l;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/model/core/TwitterUser;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/people/l$a;)Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/model/people/l$a;->a:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/people/l$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/model/people/l$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/people/l$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/model/people/l$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/people/l$a;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/twitter/model/people/l$a;->d:Z

    return v0
.end method


# virtual methods
.method protected T_()Z
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Lcom/twitter/util/object/i;->T_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/people/l$a;->a:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/people/l$a;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/twitter/model/people/l$a;->a:Lcom/twitter/model/core/TwitterUser;

    .line 34
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/people/l$a;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/twitter/model/people/l$a;->b:Ljava/lang/String;

    .line 40
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/people/l$a;
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/twitter/model/people/l$a;->d:Z

    .line 52
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/people/l$a;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/twitter/model/people/l$a;->c:Ljava/lang/String;

    .line 46
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/twitter/model/people/l$a;->e()Lcom/twitter/model/people/l;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/people/l;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/twitter/model/people/l;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/people/l;-><init>(Lcom/twitter/model/people/l$a;Lcom/twitter/model/people/l$1;)V

    return-object v0
.end method
