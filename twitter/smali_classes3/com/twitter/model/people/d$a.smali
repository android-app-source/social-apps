.class public final Lcom/twitter/model/people/d$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/people/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/people/d;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/model/people/ModuleTitle;

.field private b:Lcom/twitter/model/people/ModuleTitle;

.field private c:Lcom/twitter/model/people/k;

.field private d:Lcom/twitter/model/people/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/people/d$a;)Lcom/twitter/model/people/ModuleTitle;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/model/people/d$a;->a:Lcom/twitter/model/people/ModuleTitle;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/people/d$a;)Lcom/twitter/model/people/ModuleTitle;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/model/people/d$a;->b:Lcom/twitter/model/people/ModuleTitle;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/people/d$a;)Lcom/twitter/model/people/k;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/model/people/d$a;->c:Lcom/twitter/model/people/k;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/people/d$a;)Lcom/twitter/model/people/c;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/model/people/d$a;->d:Lcom/twitter/model/people/c;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/people/ModuleTitle;)Lcom/twitter/model/people/d$a;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/model/people/d$a;->a:Lcom/twitter/model/people/ModuleTitle;

    .line 76
    return-object p0
.end method

.method public a(Lcom/twitter/model/people/c;)Lcom/twitter/model/people/d$a;
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/twitter/model/people/d$a;->d:Lcom/twitter/model/people/c;

    .line 94
    return-object p0
.end method

.method public a(Lcom/twitter/model/people/k;)Lcom/twitter/model/people/d$a;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/twitter/model/people/d$a;->c:Lcom/twitter/model/people/k;

    .line 88
    return-object p0
.end method

.method public b(Lcom/twitter/model/people/ModuleTitle;)Lcom/twitter/model/people/d$a;
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/twitter/model/people/d$a;->b:Lcom/twitter/model/people/ModuleTitle;

    .line 82
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/twitter/model/people/d$a;->e()Lcom/twitter/model/people/d;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/people/d;
    .locals 2

    .prologue
    .line 100
    new-instance v0, Lcom/twitter/model/people/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/people/d;-><init>(Lcom/twitter/model/people/d$a;Lcom/twitter/model/people/d$1;)V

    return-object v0
.end method
