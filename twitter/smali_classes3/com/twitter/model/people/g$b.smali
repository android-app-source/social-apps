.class Lcom/twitter/model/people/g$b;
.super Lcom/twitter/model/people/h$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/people/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/model/people/h$b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/people/g$1;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/model/people/g$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/people/g$a;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/twitter/model/people/g$a;

    invoke-direct {v0}, Lcom/twitter/model/people/g$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/people/a$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/model/people/h$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/people/a$a;I)V

    .line 42
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/g$a;

    .line 44
    sget-object v1, Lcom/twitter/model/people/h;->i:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/g$a;->c(Ljava/lang/Iterable;)Lcom/twitter/model/people/g$a;

    .line 45
    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Lcom/twitter/model/people/h$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/a;)V

    .line 52
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/g;

    .line 54
    iget-object v0, v0, Lcom/twitter/model/people/g;->h:Ljava/lang/Iterable;

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 55
    sget-object v1, Lcom/twitter/model/people/h;->i:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 56
    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/twitter/model/people/g$b;->a()Lcom/twitter/model/people/g$a;

    move-result-object v0

    return-object v0
.end method
