.class abstract Lcom/twitter/model/people/a$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/people/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Lcom/twitter/model/people/a;",
        "PB:",
        "Lcom/twitter/model/people/a$a",
        "<TPB;TP;>;>",
        "Lcom/twitter/util/serialization/b",
        "<TP;TPB;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/b;-><init>(I)V

    .line 46
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/people/a$a;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TPB;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 51
    sget-object v0, Lcom/twitter/model/people/d;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/d;

    invoke-virtual {p2, v0}, Lcom/twitter/model/people/a$a;->a(Lcom/twitter/model/people/d;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    .line 52
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/a$a;->a(Ljava/lang/String;)Lcom/twitter/model/people/a$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/people/k;->a:Lcom/twitter/util/serialization/l;

    .line 53
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/k;

    invoke-virtual {v1, v0}, Lcom/twitter/model/people/a$a;->a(Lcom/twitter/model/people/k;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/model/people/a$d;

    invoke-direct {v1, v2}, Lcom/twitter/model/people/a$d;-><init>(Lcom/twitter/model/people/a$1;)V

    .line 55
    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/a$a;->a(Ljava/lang/Iterable;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/model/people/a$c;

    invoke-direct {v1, v2}, Lcom/twitter/model/people/a$c;-><init>(Lcom/twitter/model/people/a$1;)V

    .line 56
    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/a$a;->b(Ljava/lang/Iterable;)Lcom/twitter/model/people/a$a;

    .line 58
    const/4 v0, 0x1

    if-ge p3, v0, :cond_0

    .line 60
    invoke-static {p1}, Lcom/twitter/util/serialization/k;->b(Lcom/twitter/util/serialization/n;)V

    .line 62
    :cond_0
    sget-object v0, Lcom/twitter/model/people/i;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/i;

    invoke-virtual {p2, v0}, Lcom/twitter/model/people/a$a;->a(Lcom/twitter/model/people/i;)Lcom/twitter/model/people/a$a;

    .line 63
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 40
    check-cast p2, Lcom/twitter/model/people/a$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/people/a$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/people/a$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "TP;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 67
    iget-object v0, p2, Lcom/twitter/model/people/a;->a:Lcom/twitter/model/people/d;

    sget-object v1, Lcom/twitter/model/people/d;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/people/a;->b:Ljava/lang/String;

    .line 68
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/people/a;->c:Lcom/twitter/model/people/k;

    sget-object v2, Lcom/twitter/model/people/k;->a:Lcom/twitter/util/serialization/l;

    .line 69
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 71
    iget-object v0, p2, Lcom/twitter/model/people/a;->d:Ljava/util/List;

    .line 72
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 73
    new-instance v1, Lcom/twitter/model/people/a$d;

    invoke-direct {v1, v3}, Lcom/twitter/model/people/a$d;-><init>(Lcom/twitter/model/people/a$1;)V

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 75
    iget-object v0, p2, Lcom/twitter/model/people/a;->e:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 76
    new-instance v1, Lcom/twitter/model/people/a$c;

    invoke-direct {v1, v3}, Lcom/twitter/model/people/a$c;-><init>(Lcom/twitter/model/people/a$1;)V

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 77
    iget-object v0, p2, Lcom/twitter/model/people/a;->f:Lcom/twitter/model/people/i;

    sget-object v1, Lcom/twitter/model/people/i;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 78
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    check-cast p2, Lcom/twitter/model/people/a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/people/a$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/a;)V

    return-void
.end method
