.class public Lcom/twitter/model/people/i;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/people/i$a;,
        Lcom/twitter/model/people/i$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/people/i;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:I

.field public final c:I

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/model/people/i$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/people/i$b;-><init>(Lcom/twitter/model/people/i$1;)V

    sput-object v0, Lcom/twitter/model/people/i;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput p1, p0, Lcom/twitter/model/people/i;->b:I

    .line 27
    iput p2, p0, Lcom/twitter/model/people/i;->c:I

    .line 28
    invoke-static {p3}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/people/i;->d:Ljava/lang/String;

    .line 29
    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/people/i$a;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lcom/twitter/model/people/i$a;->a(Lcom/twitter/model/people/i$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/people/i;->b:I

    .line 33
    invoke-static {p1}, Lcom/twitter/model/people/i$a;->b(Lcom/twitter/model/people/i$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/people/i;->c:I

    .line 34
    invoke-static {p1}, Lcom/twitter/model/people/i$a;->c(Lcom/twitter/model/people/i$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/people/i;->d:Ljava/lang/String;

    .line 35
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/people/i$a;Lcom/twitter/model/people/i$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/twitter/model/people/i;-><init>(Lcom/twitter/model/people/i$a;)V

    return-void
.end method
