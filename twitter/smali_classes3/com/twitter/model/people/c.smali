.class public Lcom/twitter/model/people/c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/people/c$b;,
        Lcom/twitter/model/people/c$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/people/c;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/twitter/model/people/c;


# instance fields
.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    new-instance v0, Lcom/twitter/model/people/c$a;

    invoke-direct {v0, v1}, Lcom/twitter/model/people/c$a;-><init>(Lcom/twitter/model/people/c$1;)V

    sput-object v0, Lcom/twitter/model/people/c;->a:Lcom/twitter/util/serialization/l;

    .line 21
    new-instance v0, Lcom/twitter/model/people/c;

    invoke-direct {v0, v1}, Lcom/twitter/model/people/c;-><init>(Ljava/util/List;)V

    sput-object v0, Lcom/twitter/model/people/c;->b:Lcom/twitter/model/people/c;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/people/c;->c:Ljava/util/List;

    .line 28
    return-void
.end method
