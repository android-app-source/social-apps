.class public final Lcom/twitter/model/people/e$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/people/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/people/b;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lcom/twitter/model/people/f;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/people/e$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/model/people/e$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/people/e$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/model/people/e$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/people/e$a;)Lcom/twitter/model/people/f;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/model/people/e$a;->c:Lcom/twitter/model/people/f;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/people/e$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/model/people/e$a;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/model/people/e$a;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/people/e$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/people/e$a;->c:Lcom/twitter/model/people/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/people/f;)Lcom/twitter/model/people/e$a;
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/twitter/model/people/e$a;->c:Lcom/twitter/model/people/f;

    .line 82
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/people/e$a;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/twitter/model/people/e$a;->b:Ljava/lang/String;

    .line 70
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/people/e$a;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/model/people/e$a;->a:Ljava/lang/String;

    .line 76
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/twitter/model/people/e$a;->e()Lcom/twitter/model/people/b;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/people/e$a;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/twitter/model/people/e$a;->d:Ljava/lang/String;

    .line 88
    return-object p0
.end method

.method protected e()Lcom/twitter/model/people/b;
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lcom/twitter/model/people/e;

    invoke-direct {v0, p0}, Lcom/twitter/model/people/e;-><init>(Lcom/twitter/model/people/e$a;)V

    return-object v0
.end method
