.class public Lcom/twitter/model/people/g;
.super Lcom/twitter/model/people/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/people/g$a;,
        Lcom/twitter/model/people/g$b;
    }
.end annotation


# static fields
.field public static final g:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/people/g;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final h:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/people/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/model/people/g$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/people/g$b;-><init>(Lcom/twitter/model/people/g$1;)V

    sput-object v0, Lcom/twitter/model/people/g;->g:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/people/g$a;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/twitter/model/people/h;-><init>(Lcom/twitter/model/people/h$a;)V

    .line 27
    invoke-static {p1}, Lcom/twitter/model/people/g$a;->a(Lcom/twitter/model/people/g$a;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/people/g;->h:Ljava/lang/Iterable;

    .line 28
    return-void
.end method
