.class Lcom/twitter/model/people/c$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/people/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/people/c;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/people/c$1;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/model/people/c$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/people/c;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Lcom/twitter/model/people/c;

    new-instance v1, Lcom/twitter/model/people/c$b;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/twitter/model/people/c$b;-><init>(Lcom/twitter/model/people/c$1;)V

    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/model/people/c;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p2, Lcom/twitter/model/people/c;->c:Ljava/util/List;

    new-instance v1, Lcom/twitter/model/people/c$b;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/twitter/model/people/c$b;-><init>(Lcom/twitter/model/people/c$1;)V

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 36
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    check-cast p2, Lcom/twitter/model/people/c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/people/c$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/people/c;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/people/c$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/people/c;

    move-result-object v0

    return-object v0
.end method
