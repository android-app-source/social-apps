.class public final Lcom/twitter/model/timeline/ah$a;
.super Lcom/twitter/model/timeline/y$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/ah;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/timeline/y$a",
        "<",
        "Lcom/twitter/model/timeline/ah;",
        "Lcom/twitter/model/timeline/ah$a;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/twitter/model/core/ac;

.field p:Ljava/lang/String;

.field q:Lcgi;

.field r:Lcom/twitter/model/revenue/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/twitter/model/timeline/y$a;-><init>()V

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 2

    .prologue
    .line 140
    invoke-super {p0}, Lcom/twitter/model/timeline/y$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "Moments"

    iget-object v1, p0, Lcom/twitter/model/timeline/ah$a;->m:Ljava/lang/String;

    .line 141
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/timeline/ah$a;->l:Lcom/twitter/model/moments/x;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 140
    :goto_0
    return v0

    .line 141
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcgi;)Lcom/twitter/model/timeline/ah$a;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/twitter/model/timeline/ah$a;->q:Lcgi;

    .line 100
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/timeline/ah$a;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    .line 88
    return-object p0
.end method

.method public a(Lcom/twitter/model/revenue/d;)Lcom/twitter/model/timeline/ah$a;
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/twitter/model/timeline/ah$a;->r:Lcom/twitter/model/revenue/d;

    .line 106
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/twitter/model/timeline/ah$a;->f()Lcom/twitter/model/timeline/ah;

    move-result-object v0

    return-object v0
.end method

.method public c_()V
    .locals 3

    .prologue
    .line 116
    invoke-super {p0}, Lcom/twitter/model/timeline/y$a;->c_()V

    .line 117
    iget-object v0, p0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/twitter/model/timeline/ah$a;->k:Lcom/twitter/model/core/TwitterSocialProof;

    if-eqz v0, :cond_3

    .line 120
    iget-object v0, p0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    if-nez v0, :cond_2

    .line 121
    iget-object v1, p0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    new-instance v0, Lcom/twitter/model/search/e$a;

    invoke-direct {v0}, Lcom/twitter/model/search/e$a;-><init>()V

    iget-object v2, p0, Lcom/twitter/model/timeline/ah$a;->k:Lcom/twitter/model/core/TwitterSocialProof;

    .line 122
    invoke-virtual {v0, v2}, Lcom/twitter/model/search/e$a;->a(Lcom/twitter/model/core/TwitterSocialProof;)Lcom/twitter/model/search/e$a;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lcom/twitter/model/search/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/e;

    iput-object v0, v1, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    .line 132
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/model/timeline/ah$a;->q:Lcgi;

    if-eqz v0, :cond_1

    .line 133
    new-instance v0, Lcom/twitter/model/core/ac$a;

    iget-object v1, p0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    invoke-direct {v0, v1}, Lcom/twitter/model/core/ac$a;-><init>(Lcom/twitter/model/core/ac;)V

    iget-object v1, p0, Lcom/twitter/model/timeline/ah$a;->q:Lcgi;

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/ac$a;->a(Lcgi;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/ac$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    iput-object v0, p0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    .line 136
    :cond_1
    return-void

    .line 125
    :cond_2
    iget-object v0, p0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    iget-object v1, p0, Lcom/twitter/model/timeline/ah$a;->k:Lcom/twitter/model/core/TwitterSocialProof;

    iput-object v1, v0, Lcom/twitter/model/search/e;->e:Lcom/twitter/model/core/TwitterSocialProof;

    goto :goto_0

    .line 127
    :cond_3
    iget-object v0, p0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    iget-object v0, v0, Lcom/twitter/model/search/e;->e:Lcom/twitter/model/core/TwitterSocialProof;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    iget-object v0, v0, Lcom/twitter/model/search/e;->e:Lcom/twitter/model/core/TwitterSocialProof;

    iput-object v0, p0, Lcom/twitter/model/timeline/ah$a;->k:Lcom/twitter/model/core/TwitterSocialProof;

    goto :goto_0
.end method

.method public e()Lcom/twitter/model/core/ac;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/timeline/ah$a;
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/twitter/model/timeline/ah$a;->p:Ljava/lang/String;

    .line 94
    return-object p0
.end method

.method public f()Lcom/twitter/model/timeline/ah;
    .locals 3

    .prologue
    .line 147
    new-instance v0, Lcom/twitter/model/timeline/ah;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/model/timeline/ah;-><init>(Lcom/twitter/model/timeline/ah$a;ILcom/twitter/model/timeline/ah$1;)V

    return-object v0
.end method
