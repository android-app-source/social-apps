.class public Lcom/twitter/model/timeline/f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/f$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/timeline/f$a;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/model/timeline/f$a;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/f$a;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/twitter/model/timeline/f;->a:Lcom/twitter/model/timeline/f$a;

    .line 25
    iput-object p2, p0, Lcom/twitter/model/timeline/f;->b:Ljava/util/List;

    .line 26
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/core/ac;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/model/timeline/f;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/model/timeline/f;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/model/timeline/f;->a:Lcom/twitter/model/timeline/f$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/timeline/f;->a:Lcom/twitter/model/timeline/f$a;

    iget v0, v0, Lcom/twitter/model/timeline/f$a;->c:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
