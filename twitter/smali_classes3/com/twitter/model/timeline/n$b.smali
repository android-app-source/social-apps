.class Lcom/twitter/model/timeline/n$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/timeline/n;",
        "Lcom/twitter/model/timeline/n$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/b;-><init>(I)V

    .line 414
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/n$1;)V
    .locals 0

    .prologue
    .line 411
    invoke-direct {p0}, Lcom/twitter/model/timeline/n$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/timeline/n$a;
    .locals 1

    .prologue
    .line 448
    new-instance v0, Lcom/twitter/model/timeline/n$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/n$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/timeline/n$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 455
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/timeline/n$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 456
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->a(I)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 457
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->o(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 458
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->b(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 459
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 460
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->d(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 461
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->e(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 462
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->f(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 463
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->b(I)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 464
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->g(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 465
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/n$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/timeline/n$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 466
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/n$a;->b(Lcom/twitter/model/core/v;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 467
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->h(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 468
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/timeline/n$a;->a(J)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 469
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->c(I)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 470
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->a(Z)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 471
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->i(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 472
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->b(Z)Lcom/twitter/model/timeline/n$a;

    .line 473
    const/4 v0, 0x1

    if-ge p3, v0, :cond_0

    .line 475
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    .line 477
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/timeline/n$a;->j(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 478
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->k(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 479
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->l(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 480
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->m(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 481
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->n(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    .line 482
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/n$a;->p(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    .line 483
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 411
    check-cast p2, Lcom/twitter/model/timeline/n$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/timeline/n$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/timeline/n$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/n;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 419
    iget-object v0, p2, Lcom/twitter/model/timeline/n;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/timeline/n;->c:I

    .line 420
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->d:Ljava/lang/String;

    .line 421
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->e:Ljava/lang/String;

    .line 422
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->f:Ljava/lang/String;

    .line 423
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->g:Ljava/lang/String;

    .line 424
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->h:Ljava/lang/String;

    .line 425
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->i:Ljava/lang/String;

    .line 426
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/timeline/n;->j:I

    .line 427
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->k:Ljava/lang/String;

    .line 428
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->l:Lcom/twitter/model/core/v;

    sget-object v2, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 429
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->m:Lcom/twitter/model/core/v;

    sget-object v2, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 430
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->n:Ljava/lang/String;

    .line 431
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/timeline/n;->o:J

    .line 432
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/timeline/n;->p:I

    .line 433
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/timeline/n;->q:Z

    .line 434
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->r:Ljava/lang/String;

    .line 435
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/timeline/n;->s:Z

    .line 436
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->t:Ljava/lang/String;

    .line 437
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->u:Ljava/lang/String;

    .line 438
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->v:Ljava/lang/String;

    .line 439
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->w:Ljava/lang/String;

    .line 440
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->x:Ljava/lang/String;

    .line 441
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/n;->y:Ljava/lang/String;

    .line 442
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 443
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 411
    check-cast p2, Lcom/twitter/model/timeline/n;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/timeline/n$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/n;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/twitter/model/timeline/n$b;->a()Lcom/twitter/model/timeline/n$a;

    move-result-object v0

    return-object v0
.end method
