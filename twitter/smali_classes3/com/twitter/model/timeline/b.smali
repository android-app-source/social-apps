.class public final Lcom/twitter/model/timeline/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/b$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/timeline/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/model/timeline/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/timeline/b$a;-><init>(Lcom/twitter/model/timeline/b$1;)V

    sput-object v0, Lcom/twitter/model/timeline/b;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/twitter/model/timeline/b;->b:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/twitter/model/timeline/b;->c:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/twitter/model/timeline/b;->d:Ljava/lang/String;

    .line 45
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/timeline/b$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/model/timeline/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/model/timeline/b;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/twitter/model/timeline/b;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/model/timeline/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 69
    if-ne p0, p1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v0

    .line 72
    :cond_1
    instance-of v2, p1, Lcom/twitter/model/timeline/b;

    if-eqz v2, :cond_3

    .line 73
    check-cast p1, Lcom/twitter/model/timeline/b;

    .line 74
    iget-object v2, p0, Lcom/twitter/model/timeline/b;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/timeline/b;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/model/timeline/b;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/timeline/b;->c:Ljava/lang/String;

    .line 75
    invoke-static {v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/model/timeline/b;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/timeline/b;->d:Ljava/lang/String;

    .line 76
    invoke-static {v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 78
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/model/timeline/b;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/timeline/b;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/timeline/b;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
