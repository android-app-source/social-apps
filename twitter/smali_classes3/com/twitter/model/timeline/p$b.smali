.class Lcom/twitter/model/timeline/p$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/timeline/p;",
        "Lcom/twitter/model/timeline/p$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/b;-><init>(I)V

    .line 88
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/p$1;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/twitter/model/timeline/p$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/timeline/p$a;
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lcom/twitter/model/timeline/p$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/p$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/timeline/p$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/timeline/p$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/p$a;

    move-result-object v0

    .line 107
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/p$a;->b(Ljava/lang/String;)Lcom/twitter/model/timeline/p$a;

    .line 108
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 84
    check-cast p2, Lcom/twitter/model/timeline/p$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/timeline/p$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/timeline/p$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p2, Lcom/twitter/model/timeline/p;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/p;->c:Ljava/lang/String;

    .line 94
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 95
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    check-cast p2, Lcom/twitter/model/timeline/p;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/timeline/p$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/p;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/twitter/model/timeline/p$b;->a()Lcom/twitter/model/timeline/p$a;

    move-result-object v0

    return-object v0
.end method
