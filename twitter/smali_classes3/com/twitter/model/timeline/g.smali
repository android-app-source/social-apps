.class public Lcom/twitter/model/timeline/g;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/g$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/timeline/g;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/model/timeline/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/timeline/g$a;-><init>(Lcom/twitter/model/timeline/g$1;)V

    sput-object v0, Lcom/twitter/model/timeline/g;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/twitter/model/timeline/g;->b:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/twitter/model/timeline/g;->c:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/twitter/model/timeline/g;->d:Ljava/lang/String;

    .line 34
    iput-boolean p4, p0, Lcom/twitter/model/timeline/g;->e:Z

    .line 35
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/model/timeline/g;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lcom/twitter/model/timeline/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/twitter/model/timeline/g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/twitter/model/timeline/g;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/twitter/model/timeline/g;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/twitter/model/timeline/g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/timeline/g;)Z
    .locals 2

    .prologue
    .line 43
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/g;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/g;->b:Ljava/lang/String;

    .line 44
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/g;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/g;->c:Ljava/lang/String;

    .line 45
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/g;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/g;->d:Ljava/lang/String;

    .line 46
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/model/timeline/g;->e:Z

    iget-boolean v1, p1, Lcom/twitter/model/timeline/g;->e:Z

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 43
    :goto_0
    return v0

    .line 46
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 39
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/timeline/g;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/timeline/g;

    invoke-virtual {p0, p1}, Lcom/twitter/model/timeline/g;->a(Lcom/twitter/model/timeline/g;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/model/timeline/g;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v0

    .line 53
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/g;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/g;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/twitter/model/timeline/g;->e:Z

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    return v0
.end method
