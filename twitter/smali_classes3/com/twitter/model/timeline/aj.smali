.class public Lcom/twitter/model/timeline/aj;
.super Lcom/twitter/model/timeline/y;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/timeline/y$b;
.implements Lcom/twitter/model/timeline/y$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/aj$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/topic/TwitterTopic;


# direct methods
.method private constructor <init>(Lcom/twitter/model/timeline/aj$a;I)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/y;-><init>(Lcom/twitter/model/timeline/y$a;I)V

    .line 19
    iget-object v0, p1, Lcom/twitter/model/timeline/aj$a;->a:Lcom/twitter/model/topic/TwitterTopic;

    iput-object v0, p0, Lcom/twitter/model/timeline/aj;->a:Lcom/twitter/model/topic/TwitterTopic;

    .line 20
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/aj$a;ILcom/twitter/model/timeline/aj$1;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/aj;-><init>(Lcom/twitter/model/timeline/aj$a;I)V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/topic/TwitterTopic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/model/timeline/aj;->a:Lcom/twitter/model/topic/TwitterTopic;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcgi;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/model/timeline/aj;->a:Lcom/twitter/model/topic/TwitterTopic;

    invoke-virtual {v0}, Lcom/twitter/model/topic/TwitterTopic;->q()Lcgi;

    move-result-object v0

    return-object v0
.end method
