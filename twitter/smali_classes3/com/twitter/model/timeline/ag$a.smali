.class public final Lcom/twitter/model/timeline/ag$a;
.super Lcom/twitter/model/timeline/y$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/ag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/timeline/y$a",
        "<",
        "Lcom/twitter/model/timeline/ag;",
        "Lcom/twitter/model/timeline/ag$a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/ah$a;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcom/twitter/model/timeline/o;

.field private q:Lcom/twitter/model/timeline/m;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/twitter/model/timeline/y$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/timeline/ag$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/model/timeline/ag$a;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/timeline/ag$a;)Lcom/twitter/model/timeline/o;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/model/timeline/ag$a;->p:Lcom/twitter/model/timeline/o;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/timeline/ag$a;)Lcom/twitter/model/timeline/m;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/model/timeline/ag$a;->q:Lcom/twitter/model/timeline/m;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Lcom/twitter/model/timeline/y$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/timeline/ag$a;->p:Lcom/twitter/model/timeline/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/timeline/ag$a;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/timeline/m;)Lcom/twitter/model/timeline/ag$a;
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/twitter/model/timeline/ag$a;->q:Lcom/twitter/model/timeline/m;

    .line 107
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/o;)Lcom/twitter/model/timeline/ag$a;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/twitter/model/timeline/ag$a;->p:Lcom/twitter/model/timeline/o;

    .line 96
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/timeline/ag$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/ah$a;",
            ">;)",
            "Lcom/twitter/model/timeline/ag$a;"
        }
    .end annotation

    .prologue
    .line 89
    iput-object p1, p0, Lcom/twitter/model/timeline/ag$a;->a:Ljava/util/List;

    .line 90
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/twitter/model/timeline/ag$a;->e()Lcom/twitter/model/timeline/ag;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/twitter/model/timeline/ag;
    .locals 3

    .prologue
    .line 113
    new-instance v0, Lcom/twitter/model/timeline/ag;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/model/timeline/ag;-><init>(Lcom/twitter/model/timeline/ag$a;ILcom/twitter/model/timeline/ag$1;)V

    return-object v0
.end method
