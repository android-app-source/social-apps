.class public abstract Lcom/twitter/model/timeline/y;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/y$f;,
        Lcom/twitter/model/timeline/y$g;,
        Lcom/twitter/model/timeline/y$b;,
        Lcom/twitter/model/timeline/y$c;,
        Lcom/twitter/model/timeline/y$e;,
        Lcom/twitter/model/timeline/y$d;,
        Lcom/twitter/model/timeline/y$a;
    }
.end annotation


# instance fields
.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:I

.field public final f:I

.field public final g:J

.field public final h:Ljava/lang/String;

.field public final i:Lcom/twitter/model/timeline/r;

.field public final j:Lcom/twitter/model/moments/x;

.field public final k:Lcom/twitter/model/timeline/k;

.field public final l:Lcom/twitter/model/timeline/h;

.field public final m:Ljava/lang/String;

.field public final n:Lcom/twitter/model/core/TwitterSocialProof;

.field public final o:I

.field public final p:Z

.field public q:J


# direct methods
.method protected constructor <init>(Lcom/twitter/model/timeline/y$a;I)V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iget-object v0, p1, Lcom/twitter/model/timeline/y$a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/y;->c:Ljava/lang/String;

    .line 80
    iget-object v0, p1, Lcom/twitter/model/timeline/y$a;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/y;->d:Ljava/lang/String;

    .line 81
    iget v0, p1, Lcom/twitter/model/timeline/y$a;->d:I

    iput v0, p0, Lcom/twitter/model/timeline/y;->f:I

    .line 82
    iget-wide v0, p1, Lcom/twitter/model/timeline/y$a;->e:J

    iput-wide v0, p0, Lcom/twitter/model/timeline/y;->g:J

    .line 83
    iget-wide v0, p1, Lcom/twitter/model/timeline/y$a;->f:J

    iput-wide v0, p0, Lcom/twitter/model/timeline/y;->q:J

    .line 84
    iget-object v0, p1, Lcom/twitter/model/timeline/y$a;->h:Lcom/twitter/model/timeline/k;

    iput-object v0, p0, Lcom/twitter/model/timeline/y;->k:Lcom/twitter/model/timeline/k;

    .line 85
    iget-object v0, p1, Lcom/twitter/model/timeline/y$a;->i:Lcom/twitter/model/timeline/h;

    iput-object v0, p0, Lcom/twitter/model/timeline/y;->l:Lcom/twitter/model/timeline/h;

    .line 86
    iget-object v0, p1, Lcom/twitter/model/timeline/y$a;->g:Lcom/twitter/model/timeline/r;

    iput-object v0, p0, Lcom/twitter/model/timeline/y;->i:Lcom/twitter/model/timeline/r;

    .line 87
    iget-object v0, p1, Lcom/twitter/model/timeline/y$a;->k:Lcom/twitter/model/core/TwitterSocialProof;

    iput-object v0, p0, Lcom/twitter/model/timeline/y;->n:Lcom/twitter/model/core/TwitterSocialProof;

    .line 88
    iget-object v0, p1, Lcom/twitter/model/timeline/y$a;->l:Lcom/twitter/model/moments/x;

    iput-object v0, p0, Lcom/twitter/model/timeline/y;->j:Lcom/twitter/model/moments/x;

    .line 89
    iget-object v0, p1, Lcom/twitter/model/timeline/y$a;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/y;->h:Ljava/lang/String;

    .line 90
    iput p2, p0, Lcom/twitter/model/timeline/y;->e:I

    .line 91
    iget v0, p1, Lcom/twitter/model/timeline/y$a;->n:I

    iput v0, p0, Lcom/twitter/model/timeline/y;->o:I

    .line 92
    iget-boolean v0, p1, Lcom/twitter/model/timeline/y$a;->o:Z

    iput-boolean v0, p0, Lcom/twitter/model/timeline/y;->p:Z

    .line 93
    iget-object v0, p1, Lcom/twitter/model/timeline/y$a;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/y;->m:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public static a(Lcom/twitter/model/timeline/y;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/y;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    instance-of v0, p0, Lcom/twitter/model/timeline/y$d;

    if-eqz v0, :cond_0

    .line 107
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$d;

    .line 108
    invoke-interface {v0}, Lcom/twitter/model/timeline/y$d;->a()Ljava/util/List;

    move-result-object v0

    .line 110
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<B:",
            "Lcom/twitter/model/timeline/y$a;",
            ">(",
            "Ljava/util/List",
            "<TB;>;)V"
        }
    .end annotation

    .prologue
    .line 151
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p0, v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;I)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    .line 152
    invoke-virtual {v1}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    .line 153
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/y$a;->b(I)Lcom/twitter/model/timeline/y$a;

    goto :goto_0

    .line 156
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    .line 157
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/twitter/model/timeline/y$a;->b(I)Lcom/twitter/model/timeline/y$a;

    goto :goto_1

    .line 159
    :cond_1
    return-void
.end method

.method public static b(Lcom/twitter/model/timeline/y;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/y;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    instance-of v0, p0, Lcom/twitter/model/timeline/y$e;

    if-eqz v0, :cond_0

    .line 116
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$e;

    .line 117
    invoke-interface {v0}, Lcom/twitter/model/timeline/y$e;->c()Ljava/util/List;

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lcom/twitter/model/timeline/y;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/y;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/topic/TwitterTopic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    instance-of v0, p0, Lcom/twitter/model/timeline/y$c;

    if-eqz v0, :cond_0

    .line 125
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$c;

    .line 126
    invoke-interface {v0}, Lcom/twitter/model/timeline/y$c;->a()Ljava/util/List;

    move-result-object v0

    .line 128
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lcom/twitter/model/timeline/y;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    instance-of v0, p0, Lcom/twitter/model/timeline/y$g;

    if-eqz v0, :cond_0

    .line 134
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$g;

    .line 135
    invoke-interface {v0}, Lcom/twitter/model/timeline/y$g;->c()Ljava/lang/String;

    move-result-object v0

    .line 137
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/twitter/model/timeline/y;)Lcgi;
    .locals 1

    .prologue
    .line 142
    instance-of v0, p0, Lcom/twitter/model/timeline/y$b;

    if-eqz v0, :cond_0

    .line 143
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$b;

    .line 144
    invoke-interface {v0}, Lcom/twitter/model/timeline/y$b;->b()Lcgi;

    move-result-object v0

    .line 146
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public e()Z
    .locals 2

    .prologue
    .line 97
    const-string/jumbo v0, "RecosTweet"

    iget-object v1, p0, Lcom/twitter/model/timeline/y;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 101
    const-string/jumbo v0, "Moments"

    iget-object v1, p0, Lcom/twitter/model/timeline/y;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
