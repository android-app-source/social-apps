.class public Lcom/twitter/model/timeline/r;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/r$b;,
        Lcom/twitter/model/timeline/r$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/timeline/r;",
            "Lcom/twitter/model/timeline/r$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/model/timeline/r$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/timeline/r$b;-><init>(Lcom/twitter/model/timeline/r$1;)V

    sput-object v0, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/timeline/r$a;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iget-object v0, p1, Lcom/twitter/model/timeline/r$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    .line 36
    iget-object v0, p1, Lcom/twitter/model/timeline/r$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/r;->c:Ljava/lang/String;

    .line 37
    iget-object v0, p1, Lcom/twitter/model/timeline/r$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/r;->d:Ljava/lang/String;

    .line 38
    iget-object v0, p1, Lcom/twitter/model/timeline/r$a;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    .line 39
    iget-object v0, p1, Lcom/twitter/model/timeline/r$a;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/r;->f:Ljava/lang/String;

    .line 40
    iget-object v0, p1, Lcom/twitter/model/timeline/r$a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/r;->g:Ljava/lang/String;

    .line 41
    iget-object v0, p1, Lcom/twitter/model/timeline/r$a;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/r;->h:Ljava/lang/String;

    .line 42
    iget-object v0, p1, Lcom/twitter/model/timeline/r$a;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/r;->i:Ljava/lang/String;

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/r$a;Lcom/twitter/model/timeline/r$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/twitter/model/timeline/r;-><init>(Lcom/twitter/model/timeline/r$a;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/timeline/r;)Z
    .locals 2

    .prologue
    .line 52
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    .line 53
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/r;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/r;->c:Ljava/lang/String;

    .line 54
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/r;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/r;->d:Ljava/lang/String;

    .line 55
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    .line 56
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/r;->f:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/r;->f:Ljava/lang/String;

    .line 57
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/r;->g:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/r;->g:Ljava/lang/String;

    .line 58
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/r;->h:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/r;->h:Ljava/lang/String;

    .line 59
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/r;->i:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/r;->i:Ljava/lang/String;

    .line 60
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    .line 60
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 47
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/timeline/r;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/timeline/r;

    invoke-virtual {p0, p1}, Lcom/twitter/model/timeline/r;->a(Lcom/twitter/model/timeline/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v0

    .line 66
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/r;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/r;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/r;->g:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/r;->f:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/r;->h:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/r;->i:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    return v0
.end method
