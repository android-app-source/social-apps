.class public Lcom/twitter/model/timeline/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/core/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/n$b;,
        Lcom/twitter/model/timeline/n$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/timeline/n;",
            "Lcom/twitter/model/timeline/n$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:I

.field public final k:Ljava/lang/String;

.field public final l:Lcom/twitter/model/core/v;

.field public final m:Lcom/twitter/model/core/v;

.field public final n:Ljava/lang/String;

.field public o:J

.field public p:I

.field public q:Z

.field public final r:Ljava/lang/String;

.field public final s:Z

.field public final t:Ljava/lang/String;

.field public final u:Ljava/lang/String;

.field public final v:Ljava/lang/String;

.field public final w:Ljava/lang/String;

.field public final x:Ljava/lang/String;

.field public final y:Ljava/lang/String;

.field private final z:Landroid/text/format/Time;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/model/timeline/n$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/timeline/n$b;-><init>(Lcom/twitter/model/timeline/n$1;)V

    sput-object v0, Lcom/twitter/model/timeline/n;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/timeline/n$a;)V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->z:Landroid/text/format/Time;

    .line 93
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->b:Ljava/lang/String;

    .line 94
    iget v0, p1, Lcom/twitter/model/timeline/n$a;->b:I

    iput v0, p0, Lcom/twitter/model/timeline/n;->c:I

    .line 95
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->d:Ljava/lang/String;

    .line 96
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->e:Ljava/lang/String;

    .line 97
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->f:Ljava/lang/String;

    .line 98
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->g:Ljava/lang/String;

    .line 99
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->h:Ljava/lang/String;

    .line 100
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->i:Ljava/lang/String;

    .line 101
    iget v0, p1, Lcom/twitter/model/timeline/n$a;->i:I

    iput v0, p0, Lcom/twitter/model/timeline/n;->j:I

    .line 102
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->k:Ljava/lang/String;

    .line 103
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->k:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->l:Lcom/twitter/model/core/v;

    .line 104
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->l:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->m:Lcom/twitter/model/core/v;

    .line 105
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->n:Ljava/lang/String;

    .line 106
    iget-wide v0, p1, Lcom/twitter/model/timeline/n$a;->n:J

    iput-wide v0, p0, Lcom/twitter/model/timeline/n;->o:J

    .line 107
    iget v0, p1, Lcom/twitter/model/timeline/n$a;->o:I

    iput v0, p0, Lcom/twitter/model/timeline/n;->p:I

    .line 108
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->r:Ljava/lang/String;

    .line 109
    iget-boolean v0, p1, Lcom/twitter/model/timeline/n$a;->r:Z

    iput-boolean v0, p0, Lcom/twitter/model/timeline/n;->s:Z

    .line 110
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->t:Ljava/lang/String;

    .line 111
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->t:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->u:Ljava/lang/String;

    .line 112
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->u:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->v:Ljava/lang/String;

    .line 113
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->w:Ljava/lang/String;

    .line 114
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->x:Ljava/lang/String;

    .line 115
    iget-object v0, p1, Lcom/twitter/model/timeline/n$a;->x:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/n;->y:Ljava/lang/String;

    .line 116
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/n$a;Lcom/twitter/model/timeline/n$1;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/twitter/model/timeline/n;-><init>(Lcom/twitter/model/timeline/n$a;)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 119
    iget v0, p0, Lcom/twitter/model/timeline/n;->c:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/twitter/model/timeline/n;->c:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/twitter/model/timeline/n;->t:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/timeline/n;->u:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 202
    const-string/jumbo v0, "fullscreen_takeover"

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 206
    const-string/jumbo v0, "inline_tooltip_prompt"

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 159
    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    .line 174
    :cond_0
    :goto_0
    return v0

    .line 160
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 161
    check-cast p1, Lcom/twitter/model/timeline/n;

    .line 162
    iget v1, p0, Lcom/twitter/model/timeline/n;->c:I

    iget v2, p1, Lcom/twitter/model/timeline/n;->c:I

    if-ne v1, v2, :cond_0

    .line 163
    iget-object v1, p0, Lcom/twitter/model/timeline/n;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/model/timeline/n;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Lcom/twitter/model/timeline/n;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/model/timeline/n;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    iget-object v1, p0, Lcom/twitter/model/timeline/n;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/model/timeline/n;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/twitter/model/timeline/n;->h:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/model/timeline/n;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    iget-object v1, p0, Lcom/twitter/model/timeline/n;->k:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/model/timeline/n;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    iget-boolean v1, p0, Lcom/twitter/model/timeline/n;->s:Z

    iget-boolean v2, p1, Lcom/twitter/model/timeline/n;->s:Z

    if-ne v1, v2, :cond_0

    .line 169
    iget-object v1, p0, Lcom/twitter/model/timeline/n;->t:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/model/timeline/n;->t:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p0, Lcom/twitter/model/timeline/n;->u:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/model/timeline/n;->u:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    iget-object v1, p0, Lcom/twitter/model/timeline/n;->v:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/model/timeline/n;->v:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/twitter/model/timeline/n;->w:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/model/timeline/n;->w:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/twitter/model/timeline/n;->x:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/model/timeline/n;->x:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    iget-object v0, p0, Lcom/twitter/model/timeline/n;->y:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/n;->y:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/twitter/model/timeline/n;->s:Z

    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 214
    const-string/jumbo v0, "inline_prompt"

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/model/timeline/n;->q:Z

    .line 219
    return-void
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 179
    iget v0, p0, Lcom/twitter/model/timeline/n;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/model/timeline/n;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/model/timeline/n;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/twitter/model/timeline/n;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/twitter/model/timeline/n;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/twitter/model/timeline/n;->k:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/twitter/model/timeline/n;->s:Z

    .line 180
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/twitter/model/timeline/n;->t:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/twitter/model/timeline/n;->u:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/twitter/model/timeline/n;->v:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/twitter/model/timeline/n;->w:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/twitter/model/timeline/n;->x:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    iget-object v3, p0, Lcom/twitter/model/timeline/n;->y:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 179
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", header="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", actionText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", actionUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", trigger="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", icon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", format="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", backgroundImageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", persistence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/timeline/n;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", entities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->l:Lcom/twitter/model/core/v;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", headerEntities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->m:Lcom/twitter/model/core/v;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/timeline/n;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", template="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", statusId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/timeline/n;->o:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", insertionIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/timeline/n;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", tooltipTarget="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mDeadline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->z:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", isAppGraphPrompt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/timeline/n;->s:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", clientExperimentKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", clientExperimentBucket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", displayLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", typoCorrectedEmail="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", phoneNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/timeline/n;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
