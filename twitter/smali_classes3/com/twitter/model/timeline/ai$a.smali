.class public final Lcom/twitter/model/timeline/ai$a;
.super Lcom/twitter/model/timeline/y$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/ai;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/timeline/y$a",
        "<",
        "Lcom/twitter/model/timeline/ai;",
        "Lcom/twitter/model/timeline/ai$a;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/twitter/model/moments/v;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/twitter/model/timeline/y$a;-><init>()V

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Lcom/twitter/model/timeline/y$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/timeline/ai$a;->a:Lcom/twitter/model/moments/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/timeline/ai$a;->a:Lcom/twitter/model/moments/v;

    iget-object v0, v0, Lcom/twitter/model/moments/v;->c:Ljava/util/List;

    .line 36
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/moments/v;)Lcom/twitter/model/timeline/ai$a;
    .locals 1

    .prologue
    .line 26
    iput-object p1, p0, Lcom/twitter/model/timeline/ai$a;->a:Lcom/twitter/model/moments/v;

    .line 27
    if-eqz p1, :cond_0

    .line 28
    iget-object v0, p1, Lcom/twitter/model/moments/v;->e:Lcom/twitter/model/timeline/k;

    invoke-virtual {p0, v0}, Lcom/twitter/model/timeline/ai$a;->a(Lcom/twitter/model/timeline/k;)Lcom/twitter/model/timeline/y$a;

    .line 30
    :cond_0
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/twitter/model/timeline/ai$a;->e()Lcom/twitter/model/timeline/ai;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/twitter/model/timeline/ai;
    .locals 3

    .prologue
    .line 42
    new-instance v0, Lcom/twitter/model/timeline/ai;

    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/model/timeline/ai;-><init>(Lcom/twitter/model/timeline/ai$a;ILcom/twitter/model/timeline/ai$1;)V

    return-object v0
.end method
