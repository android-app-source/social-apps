.class Lcom/twitter/model/timeline/f$a$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/f$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/timeline/f$a;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/twitter/util/serialization/i;-><init>(I)V

    .line 79
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/timeline/f$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    .line 92
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v2

    .line 93
    if-nez p2, :cond_0

    .line 95
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    .line 96
    invoke-static {p1}, Lcom/twitter/util/serialization/k;->b(Lcom/twitter/util/serialization/n;)V

    .line 98
    :cond_0
    new-instance v3, Lcom/twitter/model/timeline/f$a;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/model/timeline/f$a;-><init>(JI)V

    return-object v3
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/f$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    iget-wide v0, p2, Lcom/twitter/model/timeline/f$a;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/timeline/f$a;->c:I

    .line 84
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 85
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    check-cast p2, Lcom/twitter/model/timeline/f$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/timeline/f$a$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/f$a;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/timeline/f$a$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/timeline/f$a;

    move-result-object v0

    return-object v0
.end method
