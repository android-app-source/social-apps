.class Lcom/twitter/model/timeline/i$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/timeline/i;",
        "Lcom/twitter/model/timeline/i$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/i$1;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/twitter/model/timeline/i$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/timeline/i$a;
    .locals 1

    .prologue
    .line 77
    new-instance v0, Lcom/twitter/model/timeline/i$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/i$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/timeline/i$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p2, v0}, Lcom/twitter/model/timeline/i$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/timeline/i$a;

    .line 85
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 66
    check-cast p2, Lcom/twitter/model/timeline/i$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/timeline/i$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/timeline/i$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p2, Lcom/twitter/model/timeline/i;->b:Lcom/twitter/model/core/TwitterUser;

    sget-object v1, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 72
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    check-cast p2, Lcom/twitter/model/timeline/i;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/timeline/i$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/i;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/twitter/model/timeline/i$b;->a()Lcom/twitter/model/timeline/i$a;

    move-result-object v0

    return-object v0
.end method
