.class public Lcom/twitter/model/timeline/an;
.super Lcom/twitter/model/timeline/y;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/timeline/y$b;
.implements Lcom/twitter/model/timeline/y$e;
.implements Lcom/twitter/model/timeline/y$f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/an$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/core/TwitterUser;

.field public final b:Ljava/lang/String;

.field private final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/model/timeline/an$a;I)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/y;-><init>(Lcom/twitter/model/timeline/y$a;I)V

    .line 30
    iget-object v0, p1, Lcom/twitter/model/timeline/an$a;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/model/timeline/an;->a:Lcom/twitter/model/core/TwitterUser;

    .line 31
    iget-object v0, p1, Lcom/twitter/model/timeline/an$a;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/an;->b:Ljava/lang/String;

    .line 32
    iget-object v0, p0, Lcom/twitter/model/timeline/an;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/an;->r:Ljava/util/List;

    .line 33
    return-void
.end method


# virtual methods
.method public b()Lcgi;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/model/timeline/an;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/model/timeline/an;->r:Ljava/util/List;

    return-object v0
.end method
