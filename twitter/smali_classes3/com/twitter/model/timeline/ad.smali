.class public Lcom/twitter/model/timeline/ad;
.super Lcom/twitter/model/timeline/y;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/timeline/y$d;
.implements Lcom/twitter/model/timeline/y$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/ad$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcha;

.field public final r:Lcom/twitter/model/timeline/m;

.field public final s:I

.field private final t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/twitter/model/timeline/ad$a;I)V
    .locals 5

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/y;-><init>(Lcom/twitter/model/timeline/y$a;I)V

    .line 39
    invoke-static {p1}, Lcom/twitter/model/timeline/ad$a;->a(Lcom/twitter/model/timeline/ad$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/ad;->a:Ljava/util/List;

    .line 40
    invoke-static {p1}, Lcom/twitter/model/timeline/ad$a;->b(Lcom/twitter/model/timeline/ad$a;)Lcha;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/ad;->b:Lcha;

    .line 41
    invoke-static {p1}, Lcom/twitter/model/timeline/ad$a;->c(Lcom/twitter/model/timeline/ad$a;)Lcom/twitter/model/timeline/m;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/ad;->r:Lcom/twitter/model/timeline/m;

    .line 42
    invoke-static {p1}, Lcom/twitter/model/timeline/ad$a;->d(Lcom/twitter/model/timeline/ad$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/timeline/ad;->s:I

    .line 44
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 45
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 46
    iget-object v0, p0, Lcom/twitter/model/timeline/ad;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y;

    .line 47
    invoke-static {v0}, Lcom/twitter/model/timeline/y;->b(Lcom/twitter/model/timeline/y;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    .line 48
    invoke-static {v0}, Lcom/twitter/model/timeline/y;->a(Lcom/twitter/model/timeline/y;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 50
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/timeline/ad;->t:Ljava/util/List;

    .line 51
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/timeline/ad;->u:Ljava/util/List;

    .line 52
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/ad$a;ILcom/twitter/model/timeline/ad$1;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/ad;-><init>(Lcom/twitter/model/timeline/ad$a;I)V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/model/timeline/ad;->u:Ljava/util/List;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/model/timeline/ad;->t:Ljava/util/List;

    return-object v0
.end method
