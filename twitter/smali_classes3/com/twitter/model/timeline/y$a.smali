.class public abstract Lcom/twitter/model/timeline/y$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/model/timeline/y;",
        "B:",
        "Lcom/twitter/model/timeline/y$a;",
        ">",
        "Lcom/twitter/util/object/i",
        "<TT;>;"
    }
.end annotation


# instance fields
.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:I

.field e:J

.field f:J

.field g:Lcom/twitter/model/timeline/r;

.field h:Lcom/twitter/model/timeline/k;

.field i:Lcom/twitter/model/timeline/h;

.field j:Ljava/lang/String;

.field k:Lcom/twitter/model/core/TwitterSocialProof;

.field l:Lcom/twitter/model/moments/x;

.field m:Ljava/lang/String;

.field n:I

.field o:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 167
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/model/timeline/y$a;->e:J

    .line 168
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/model/timeline/y$a;->f:J

    .line 183
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/model/timeline/y$a;->n:I

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 4
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 284
    iget-object v0, p0, Lcom/twitter/model/timeline/y$a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/timeline/y$a;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/timeline/y$a;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .prologue
    .line 225
    iput p1, p0, Lcom/twitter/model/timeline/y$a;->d:I

    .line 226
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public a(J)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TB;"
        }
    .end annotation

    .prologue
    .line 231
    iput-wide p1, p0, Lcom/twitter/model/timeline/y$a;->e:J

    .line 232
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/TwitterSocialProof;)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/TwitterSocialProof;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 249
    iput-object p1, p0, Lcom/twitter/model/timeline/y$a;->k:Lcom/twitter/model/core/TwitterSocialProof;

    .line 250
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/x;)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/x;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 255
    iput-object p1, p0, Lcom/twitter/model/timeline/y$a;->l:Lcom/twitter/model/moments/x;

    .line 256
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/timeline/h;)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/h;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 195
    iput-object p1, p0, Lcom/twitter/model/timeline/y$a;->i:Lcom/twitter/model/timeline/h;

    .line 196
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/timeline/k;)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/k;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 189
    iput-object p1, p0, Lcom/twitter/model/timeline/y$a;->h:Lcom/twitter/model/timeline/k;

    .line 190
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/r;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 243
    iput-object p1, p0, Lcom/twitter/model/timeline/y$a;->g:Lcom/twitter/model/timeline/r;

    .line 244
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 201
    iput-object p1, p0, Lcom/twitter/model/timeline/y$a;->j:Ljava/lang/String;

    .line 202
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public a(Z)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .prologue
    .line 267
    iput-boolean p1, p0, Lcom/twitter/model/timeline/y$a;->o:Z

    .line 268
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public b(I)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .prologue
    .line 261
    iput p1, p0, Lcom/twitter/model/timeline/y$a;->n:I

    .line 262
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public b(J)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TB;"
        }
    .end annotation

    .prologue
    .line 237
    iput-wide p1, p0, Lcom/twitter/model/timeline/y$a;->f:J

    .line 238
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 207
    iput-object p1, p0, Lcom/twitter/model/timeline/y$a;->m:Ljava/lang/String;

    .line 208
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 213
    iput-object p1, p0, Lcom/twitter/model/timeline/y$a;->b:Ljava/lang/String;

    .line 214
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method

.method public c_()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 276
    iget-object v0, p0, Lcom/twitter/model/timeline/y$a;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/twitter/model/timeline/y$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/y$a;->c:Ljava/lang/String;

    .line 279
    :cond_0
    return-void
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 219
    iput-object p1, p0, Lcom/twitter/model/timeline/y$a;->c:Ljava/lang/String;

    .line 220
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y$a;

    return-object v0
.end method
