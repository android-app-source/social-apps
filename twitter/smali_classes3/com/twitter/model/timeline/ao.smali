.class public Lcom/twitter/model/timeline/ao;
.super Lcom/twitter/model/timeline/y;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/timeline/y$b;
.implements Lcom/twitter/model/timeline/y$d;
.implements Lcom/twitter/model/timeline/y$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/ao$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/timeline/at;

.field public final b:Lcom/twitter/model/timeline/m;


# direct methods
.method private constructor <init>(Lcom/twitter/model/timeline/ao$a;I)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/y;-><init>(Lcom/twitter/model/timeline/y$a;I)V

    .line 21
    iget-object v0, p1, Lcom/twitter/model/timeline/ao$a;->a:Lcom/twitter/model/timeline/at;

    iput-object v0, p0, Lcom/twitter/model/timeline/ao;->a:Lcom/twitter/model/timeline/at;

    .line 22
    iget-object v0, p1, Lcom/twitter/model/timeline/ao$a;->p:Lcom/twitter/model/timeline/m;

    iput-object v0, p0, Lcom/twitter/model/timeline/ao;->b:Lcom/twitter/model/timeline/m;

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/ao$a;ILcom/twitter/model/timeline/ao$1;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/ao;-><init>(Lcom/twitter/model/timeline/ao$a;I)V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/model/timeline/ao;->a:Lcom/twitter/model/timeline/at;

    iget-object v0, v0, Lcom/twitter/model/timeline/at;->b:Ljava/util/List;

    return-object v0
.end method

.method public b()Lcgi;
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/model/timeline/ao;->a:Lcom/twitter/model/timeline/at;

    iget-object v0, v0, Lcom/twitter/model/timeline/at;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    .line 41
    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    iget-object v0, v0, Lcom/twitter/model/core/ac;->w:Lcgi;

    .line 52
    :goto_0
    return-object v0

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/timeline/ao;->a:Lcom/twitter/model/timeline/at;

    iget-object v0, v0, Lcom/twitter/model/timeline/at;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 47
    iget-object v2, v0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    if-eqz v2, :cond_2

    .line 48
    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    goto :goto_0

    .line 52
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/model/timeline/ao;->a:Lcom/twitter/model/timeline/at;

    iget-object v0, v0, Lcom/twitter/model/timeline/at;->a:Ljava/util/List;

    return-object v0
.end method
