.class public Lcom/twitter/model/timeline/o;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/o$b;,
        Lcom/twitter/model/timeline/o$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/timeline/o;",
            "Lcom/twitter/model/timeline/o$a;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/twitter/model/timeline/o;


# instance fields
.field public final c:Ljava/lang/String;

.field public final d:Lcom/twitter/model/timeline/r;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/model/timeline/o$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/timeline/o$b;-><init>(Lcom/twitter/model/timeline/o$1;)V

    sput-object v0, Lcom/twitter/model/timeline/o;->a:Lcom/twitter/util/serialization/b;

    .line 22
    new-instance v0, Lcom/twitter/model/timeline/o$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/o$a;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/model/timeline/o$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/o;

    sput-object v0, Lcom/twitter/model/timeline/o;->b:Lcom/twitter/model/timeline/o;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/timeline/o$a;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iget-object v0, p1, Lcom/twitter/model/timeline/o$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/o;->c:Ljava/lang/String;

    .line 52
    iget-object v0, p1, Lcom/twitter/model/timeline/o$a;->b:Lcom/twitter/model/timeline/r;

    iput-object v0, p0, Lcom/twitter/model/timeline/o;->d:Lcom/twitter/model/timeline/r;

    .line 53
    iget-object v0, p1, Lcom/twitter/model/timeline/o$a;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/o;->e:Ljava/util/List;

    .line 54
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/o$a;Lcom/twitter/model/timeline/o$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/model/timeline/o;-><init>(Lcom/twitter/model/timeline/o$a;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/timeline/o;)Z
    .locals 2

    .prologue
    .line 36
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/o;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/o;->c:Ljava/lang/String;

    .line 37
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/o;->d:Lcom/twitter/model/timeline/r;

    iget-object v1, p1, Lcom/twitter/model/timeline/o;->d:Lcom/twitter/model/timeline/r;

    .line 38
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/o;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/timeline/o;->e:Ljava/util/List;

    .line 39
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 36
    :goto_0
    return v0

    .line 39
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 32
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/timeline/o;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/timeline/o;

    invoke-virtual {p0, p1}, Lcom/twitter/model/timeline/o;->a(Lcom/twitter/model/timeline/o;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/model/timeline/o;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v0

    .line 45
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/o;->d:Lcom/twitter/model/timeline/r;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/o;->e:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    return v0
.end method
