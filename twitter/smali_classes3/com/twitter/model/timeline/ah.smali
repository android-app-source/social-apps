.class public Lcom/twitter/model/timeline/ah;
.super Lcom/twitter/model/timeline/y;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/timeline/y$b;
.implements Lcom/twitter/model/timeline/y$d;
.implements Lcom/twitter/model/timeline/y$f;
.implements Lcom/twitter/model/timeline/y$g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/ah$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/core/ac;

.field public final b:Ljava/lang/String;

.field private final r:Lcom/twitter/model/revenue/d;


# direct methods
.method private constructor <init>(Lcom/twitter/model/timeline/ah$a;I)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/y;-><init>(Lcom/twitter/model/timeline/y$a;I)V

    .line 30
    iget-object v0, p1, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    iput-object v0, p0, Lcom/twitter/model/timeline/ah;->a:Lcom/twitter/model/core/ac;

    .line 32
    iget-object v0, p1, Lcom/twitter/model/timeline/ah$a;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/ah;->b:Ljava/lang/String;

    .line 33
    iget-object v0, p1, Lcom/twitter/model/timeline/ah$a;->r:Lcom/twitter/model/revenue/d;

    iput-object v0, p0, Lcom/twitter/model/timeline/ah;->r:Lcom/twitter/model/revenue/d;

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/ah$a;ILcom/twitter/model/timeline/ah$1;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/ah;-><init>(Lcom/twitter/model/timeline/ah$a;I)V

    return-void
.end method

.method public static a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/timeline/ah$a;
    .locals 4

    .prologue
    .line 47
    new-instance v0, Lcom/twitter/model/timeline/ah$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/ah$a;-><init>()V

    .line 48
    invoke-virtual {v0, p0}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/timeline/ah$a;

    move-result-object v0

    const/4 v1, 0x0

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ah$a;->a(I)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    iget-wide v2, p0, Lcom/twitter/model/core/ac;->M:J

    .line 50
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/timeline/ah$a;->b(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    iget-wide v2, p0, Lcom/twitter/model/core/ac;->a:J

    .line 51
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/timeline/ah$a;->a(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    .line 52
    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ah$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    .line 47
    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/model/timeline/ah;->a:Lcom/twitter/model/core/ac;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcgi;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/model/timeline/ah;->a:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->w:Lcgi;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/model/timeline/ah;->a:Lcom/twitter/model/core/ac;

    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/twitter/model/revenue/d;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/model/timeline/ah;->r:Lcom/twitter/model/revenue/d;

    return-object v0
.end method
