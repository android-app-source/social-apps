.class public final Lcom/twitter/model/timeline/an$a;
.super Lcom/twitter/model/timeline/y$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/an;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/timeline/y$a",
        "<",
        "Lcom/twitter/model/timeline/an;",
        "Lcom/twitter/model/timeline/an$a;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/twitter/model/core/TwitterUser;

.field p:Ljava/lang/String;

.field q:Lcgi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/twitter/model/timeline/y$a;-><init>()V

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Lcom/twitter/model/timeline/y$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/timeline/an$a;->a:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcgi;)Lcom/twitter/model/timeline/an$a;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/twitter/model/timeline/an$a;->q:Lcgi;

    .line 70
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/timeline/an$a;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/twitter/model/timeline/an$a;->a:Lcom/twitter/model/core/TwitterUser;

    .line 58
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/twitter/model/timeline/an$a;->e()Lcom/twitter/model/timeline/an;

    move-result-object v0

    return-object v0
.end method

.method public c_()V
    .locals 2

    .prologue
    .line 75
    invoke-super {p0}, Lcom/twitter/model/timeline/y$a;->c_()V

    .line 76
    iget-object v0, p0, Lcom/twitter/model/timeline/an$a;->a:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/timeline/an$a;->q:Lcgi;

    if-eqz v0, :cond_0

    .line 77
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    iget-object v1, p0, Lcom/twitter/model/timeline/an$a;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;-><init>(Lcom/twitter/model/core/TwitterUser;)V

    iget-object v1, p0, Lcom/twitter/model/timeline/an$a;->q:Lcgi;

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcgi;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/model/timeline/an$a;->a:Lcom/twitter/model/core/TwitterUser;

    .line 79
    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/timeline/an$a;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/twitter/model/timeline/an$a;->p:Ljava/lang/String;

    .line 64
    return-object p0
.end method

.method public e()Lcom/twitter/model/timeline/an;
    .locals 2

    .prologue
    .line 89
    new-instance v0, Lcom/twitter/model/timeline/an;

    const/16 v1, 0x12

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/timeline/an;-><init>(Lcom/twitter/model/timeline/an$a;I)V

    return-object v0
.end method
