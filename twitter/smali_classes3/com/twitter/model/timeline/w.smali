.class public Lcom/twitter/model/timeline/w;
.super Lcom/twitter/model/timeline/y;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/timeline/y$b;
.implements Lcom/twitter/model/timeline/y$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/w$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/ah;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/twitter/model/timeline/f$a;

.field private final r:Lcom/twitter/model/timeline/f;


# direct methods
.method private constructor <init>(Lcom/twitter/model/timeline/w$a;I)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/y;-><init>(Lcom/twitter/model/timeline/y$a;I)V

    .line 26
    iget-object v0, p1, Lcom/twitter/model/timeline/w$a;->a:Lcom/twitter/model/timeline/f;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/f;

    iput-object v0, p0, Lcom/twitter/model/timeline/w;->r:Lcom/twitter/model/timeline/f;

    .line 27
    iget-object v0, p0, Lcom/twitter/model/timeline/w;->r:Lcom/twitter/model/timeline/f;

    iget-object v0, v0, Lcom/twitter/model/timeline/f;->b:Ljava/util/List;

    new-instance v1, Lcom/twitter/model/timeline/w$1;

    invoke-direct {v1, p0}, Lcom/twitter/model/timeline/w$1;-><init>(Lcom/twitter/model/timeline/w;)V

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/w;->a:Ljava/util/List;

    .line 40
    iget-object v0, p0, Lcom/twitter/model/timeline/w;->r:Lcom/twitter/model/timeline/f;

    iget-object v0, v0, Lcom/twitter/model/timeline/f;->a:Lcom/twitter/model/timeline/f$a;

    iput-object v0, p0, Lcom/twitter/model/timeline/w;->b:Lcom/twitter/model/timeline/f$a;

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/w$a;ILcom/twitter/model/timeline/w$1;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/w;-><init>(Lcom/twitter/model/timeline/w$a;I)V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/model/timeline/w;->r:Lcom/twitter/model/timeline/f;

    iget-object v0, v0, Lcom/twitter/model/timeline/f;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcgi;
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/model/timeline/w;->r:Lcom/twitter/model/timeline/f;

    iget-object v0, v0, Lcom/twitter/model/timeline/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    .line 53
    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    iget-object v0, v0, Lcom/twitter/model/core/ac;->w:Lcgi;

    .line 57
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/model/timeline/w;->r:Lcom/twitter/model/timeline/f;

    invoke-virtual {v0}, Lcom/twitter/model/timeline/f;->b()Z

    move-result v0

    return v0
.end method

.method public d()Lcom/twitter/model/core/ac;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/model/timeline/w;->r:Lcom/twitter/model/timeline/f;

    invoke-virtual {v0}, Lcom/twitter/model/timeline/f;->a()Lcom/twitter/model/core/ac;

    move-result-object v0

    return-object v0
.end method
