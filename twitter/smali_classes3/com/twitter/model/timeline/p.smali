.class public Lcom/twitter/model/timeline/p;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/p$b;,
        Lcom/twitter/model/timeline/p$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/timeline/p;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/model/timeline/p$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/timeline/p$b;-><init>(Lcom/twitter/model/timeline/p$1;)V

    sput-object v0, Lcom/twitter/model/timeline/p;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/timeline/p$a;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iget-object v0, p1, Lcom/twitter/model/timeline/p$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/p;->b:Ljava/lang/String;

    .line 35
    iget-object v0, p1, Lcom/twitter/model/timeline/p$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/timeline/p;->c:Ljava/lang/String;

    .line 36
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/p$a;Lcom/twitter/model/timeline/p$1;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/twitter/model/timeline/p;-><init>(Lcom/twitter/model/timeline/p$a;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/model/timeline/p;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/timeline/p;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 49
    const/4 v0, 0x4

    .line 55
    :goto_0
    return v0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/twitter/model/timeline/p;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x2

    goto :goto_0

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/timeline/p;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 53
    const/4 v0, 0x3

    goto :goto_0

    .line 55
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
