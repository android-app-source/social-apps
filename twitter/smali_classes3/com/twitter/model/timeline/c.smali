.class public Lcom/twitter/model/timeline/c;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:I

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/twitter/model/timeline/c;->a:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/twitter/model/timeline/c;->b:Ljava/lang/String;

    .line 52
    iput-object p3, p0, Lcom/twitter/model/timeline/c;->c:Ljava/util/List;

    .line 53
    iput p4, p0, Lcom/twitter/model/timeline/c;->d:I

    .line 54
    iput-object p5, p0, Lcom/twitter/model/timeline/c;->e:Ljava/util/List;

    .line 55
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/timeline/c;)Z
    .locals 2

    .prologue
    .line 64
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/c;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/c;->a:Ljava/lang/String;

    .line 65
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/c;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/c;->b:Ljava/lang/String;

    .line 66
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/c;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/timeline/c;->c:Ljava/util/List;

    .line 67
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/model/timeline/c;->d:I

    .line 68
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p1, Lcom/twitter/model/timeline/c;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/c;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/timeline/c;->e:Ljava/util/List;

    .line 69
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 64
    :goto_0
    return v0

    .line 69
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 59
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/timeline/c;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/timeline/c;

    invoke-virtual {p0, p1}, Lcom/twitter/model/timeline/c;->a(Lcom/twitter/model/timeline/c;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/model/timeline/c;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/timeline/c;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/timeline/c;->c:Ljava/util/List;

    iget v3, p0, Lcom/twitter/model/timeline/c;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/model/timeline/c;->e:Ljava/util/List;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
