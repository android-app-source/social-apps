.class Lcom/twitter/model/timeline/j$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/timeline/j;",
        "Lcom/twitter/model/timeline/j$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/j$1;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/twitter/model/timeline/j$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/timeline/j$a;
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/twitter/model/timeline/j$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/j$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/timeline/j$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 100
    sget-object v0, Lcom/twitter/model/timeline/i;->a:Lcom/twitter/util/serialization/l;

    .line 101
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 100
    invoke-virtual {p2, v0}, Lcom/twitter/model/timeline/j$a;->a(Ljava/util/List;)Lcom/twitter/model/timeline/j$a;

    .line 102
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 80
    check-cast p2, Lcom/twitter/model/timeline/j$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/timeline/j$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/timeline/j$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p2, Lcom/twitter/model/timeline/j;->b:Ljava/util/List;

    sget-object v1, Lcom/twitter/model/timeline/i;->a:Lcom/twitter/util/serialization/l;

    .line 87
    invoke-static {v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 86
    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 88
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    check-cast p2, Lcom/twitter/model/timeline/j;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/timeline/j$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/j;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/twitter/model/timeline/j$b;->a()Lcom/twitter/model/timeline/j$a;

    move-result-object v0

    return-object v0
.end method
