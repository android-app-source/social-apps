.class Lcom/twitter/model/timeline/u$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/timeline/u;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/u$1;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/twitter/model/timeline/u$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/timeline/u;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 92
    const-class v0, Lcom/twitter/model/timeline/AlertType;

    .line 93
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/timeline/AlertType;

    .line 94
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v3

    .line 95
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v4

    .line 96
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v6

    .line 97
    new-instance v1, Lcom/twitter/model/timeline/u;

    invoke-direct/range {v1 .. v7}, Lcom/twitter/model/timeline/u;-><init>(Lcom/twitter/model/timeline/AlertType;Ljava/lang/String;JJ)V

    return-object v1
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/u;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p2, Lcom/twitter/model/timeline/u;->b:Lcom/twitter/model/timeline/AlertType;

    const-class v1, Lcom/twitter/model/timeline/AlertType;

    invoke-static {v1}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/u;->c:Ljava/lang/String;

    .line 83
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/timeline/u;->d:J

    .line 84
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/timeline/u;->e:J

    .line 85
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 86
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    check-cast p2, Lcom/twitter/model/timeline/u;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/timeline/u$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/u;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/timeline/u$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/timeline/u;

    move-result-object v0

    return-object v0
.end method
