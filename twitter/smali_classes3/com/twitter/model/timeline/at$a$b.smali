.class Lcom/twitter/model/timeline/at$a$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/at$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/timeline/at$a;",
        "Lcom/twitter/model/timeline/at$a$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/b;-><init>(I)V

    .line 184
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/at$1;)V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/twitter/model/timeline/at$a$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/timeline/at$a$a;
    .locals 1

    .prologue
    .line 205
    new-instance v0, Lcom/twitter/model/timeline/at$a$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/at$a$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/timeline/at$a$a;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 212
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/timeline/at$a$a;->a(I)Lcom/twitter/model/timeline/at$a$a;

    move-result-object v0

    .line 213
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/at$a$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/at$a$a;

    move-result-object v0

    .line 214
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/at$a$a;->b(Ljava/lang/String;)Lcom/twitter/model/timeline/at$a$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    .line 215
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/at$a$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/at$a$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/model/core/TwitterSocialProof;->a:Lcom/twitter/util/serialization/b;

    .line 217
    invoke-static {v0, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 216
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/at$a$a;->a(Ljava/util/Map;)Lcom/twitter/model/timeline/at$a$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    .line 220
    invoke-static {v0, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 219
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/at$a$a;->b(Ljava/util/Map;)Lcom/twitter/model/timeline/at$a$a;

    move-result-object v2

    sget-object v0, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 221
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    sget-object v1, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 222
    invoke-static {v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 221
    invoke-virtual {v2, v0, v1}, Lcom/twitter/model/timeline/at$a$a;->a(Ljava/util/List;Ljava/util/List;)Lcom/twitter/model/timeline/at$a$a;

    .line 224
    const/4 v0, 0x1

    if-ge p3, v0, :cond_0

    .line 225
    invoke-static {p1}, Lcom/twitter/util/serialization/k;->b(Lcom/twitter/util/serialization/n;)V

    .line 227
    :cond_0
    sget-object v0, Lcom/twitter/model/timeline/j;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/j;

    invoke-virtual {p2, v0}, Lcom/twitter/model/timeline/at$a$a;->a(Lcom/twitter/model/timeline/j;)Lcom/twitter/model/timeline/at$a$a;

    .line 228
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 180
    check-cast p2, Lcom/twitter/model/timeline/at$a$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/timeline/at$a$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/timeline/at$a$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/at$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 189
    iget v0, p2, Lcom/twitter/model/timeline/at$a;->b:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/at$a;->c:Ljava/lang/String;

    .line 190
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/at$a;->d:Ljava/lang/String;

    .line 191
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/at$a;->e:Lcom/twitter/model/timeline/r;

    sget-object v2, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    .line 192
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/at$a;->f:Ljava/util/Map;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v3, Lcom/twitter/model/core/TwitterSocialProof;->a:Lcom/twitter/util/serialization/b;

    .line 193
    invoke-static {v2, v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/at$a;->g:Ljava/util/Map;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v3, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    .line 195
    invoke-static {v2, v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/at$a;->h:Ljava/util/List;

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 197
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/at$a;->i:Ljava/util/List;

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 198
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/at$a;->j:Lcom/twitter/model/timeline/j;

    sget-object v2, Lcom/twitter/model/timeline/j;->a:Lcom/twitter/util/serialization/l;

    .line 199
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 200
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    check-cast p2, Lcom/twitter/model/timeline/at$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/timeline/at$a$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/at$a;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/twitter/model/timeline/at$a$b;->a()Lcom/twitter/model/timeline/at$a$a;

    move-result-object v0

    return-object v0
.end method
