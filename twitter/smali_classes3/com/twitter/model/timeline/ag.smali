.class public Lcom/twitter/model/timeline/ag;
.super Lcom/twitter/model/timeline/y;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/timeline/y$b;
.implements Lcom/twitter/model/timeline/y$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/ag$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/ah;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/twitter/model/timeline/o;

.field public final r:Lcom/twitter/model/timeline/m;


# direct methods
.method private constructor <init>(Lcom/twitter/model/timeline/ag$a;I)V
    .locals 6

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/y;-><init>(Lcom/twitter/model/timeline/y$a;I)V

    .line 34
    iget v0, p0, Lcom/twitter/model/timeline/ag;->o:I

    if-eqz v0, :cond_0

    .line 35
    invoke-static {p1}, Lcom/twitter/model/timeline/ag$a;->a(Lcom/twitter/model/timeline/ag$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/timeline/ag;->a(Ljava/util/List;)V

    .line 37
    :cond_0
    invoke-static {p1}, Lcom/twitter/model/timeline/ag$a;->a(Lcom/twitter/model/timeline/ag$a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 38
    invoke-static {p1}, Lcom/twitter/model/timeline/ag$a;->a(Lcom/twitter/model/timeline/ag$a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    .line 39
    iget-object v1, v0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    if-eqz v1, :cond_1

    .line 40
    iget-object v1, v0, Lcom/twitter/model/timeline/ah$a;->b:Ljava/lang/String;

    .line 44
    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "client-generated-id-recaptweet-"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, v0, Lcom/twitter/model/timeline/ah$a;->a:Lcom/twitter/model/core/ac;

    iget-wide v4, v4, Lcom/twitter/model/core/ac;->a:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ah$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    iget-object v1, p0, Lcom/twitter/model/timeline/ag;->d:Ljava/lang/String;

    .line 50
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ah$a;->d(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    .line 51
    invoke-virtual {v0}, Lcom/twitter/model/timeline/ah$a;->q()Ljava/lang/Object;

    move-result-object v0

    .line 40
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 44
    :cond_2
    iget-object v1, v0, Lcom/twitter/model/timeline/ah$a;->b:Ljava/lang/String;

    goto :goto_1

    .line 54
    :cond_3
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/timeline/ag;->a:Ljava/util/List;

    .line 55
    invoke-static {p1}, Lcom/twitter/model/timeline/ag$a;->b(Lcom/twitter/model/timeline/ag$a;)Lcom/twitter/model/timeline/o;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/ag;->b:Lcom/twitter/model/timeline/o;

    .line 56
    invoke-static {p1}, Lcom/twitter/model/timeline/ag$a;->c(Lcom/twitter/model/timeline/ag$a;)Lcom/twitter/model/timeline/m;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/ag;->r:Lcom/twitter/model/timeline/m;

    .line 57
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/ag$a;ILcom/twitter/model/timeline/ag$1;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/timeline/ag;-><init>(Lcom/twitter/model/timeline/ag$a;I)V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/model/timeline/ag;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 63
    iget-object v0, p0, Lcom/twitter/model/timeline/ag;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah;

    .line 64
    iget-object v0, v0, Lcom/twitter/model/timeline/ah;->a:Lcom/twitter/model/core/ac;

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 66
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public b()Lcgi;
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/model/timeline/ag;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah;

    .line 73
    iget-object v0, v0, Lcom/twitter/model/timeline/ah;->a:Lcom/twitter/model/core/ac;

    .line 74
    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75
    iget-object v0, v0, Lcom/twitter/model/core/ac;->w:Lcgi;

    .line 78
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
