.class public Lcom/twitter/model/timeline/f$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/f$a$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/timeline/f$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:J

.field public c:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/twitter/model/timeline/f$a$a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/twitter/model/timeline/f$a$a;-><init>(I)V

    sput-object v0, Lcom/twitter/model/timeline/f$a;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(JI)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-wide p1, p0, Lcom/twitter/model/timeline/f$a;->b:J

    .line 53
    iput p3, p0, Lcom/twitter/model/timeline/f$a;->c:I

    .line 54
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 58
    if-ne p0, p1, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v0

    .line 61
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 62
    goto :goto_0

    .line 65
    :cond_3
    check-cast p1, Lcom/twitter/model/timeline/f$a;

    .line 67
    iget v2, p0, Lcom/twitter/model/timeline/f$a;->c:I

    iget v3, p1, Lcom/twitter/model/timeline/f$a;->c:I

    if-ne v2, v3, :cond_4

    iget-wide v2, p0, Lcom/twitter/model/timeline/f$a;->b:J

    iget-wide v4, p1, Lcom/twitter/model/timeline/f$a;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/twitter/model/timeline/f$a;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/timeline/f$a;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
