.class public Lcom/twitter/model/timeline/k;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/k$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/timeline/k;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/twitter/model/timeline/g;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/g;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/model/timeline/k$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/timeline/k$a;-><init>(Lcom/twitter/model/timeline/k$1;)V

    sput-object v0, Lcom/twitter/model/timeline/k;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/timeline/g;Ljava/util/List;Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/g;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/g;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    .line 49
    invoke-static {p2}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/k;->c:Ljava/util/List;

    .line 50
    iput-object p3, p0, Lcom/twitter/model/timeline/k;->d:Ljava/lang/String;

    .line 51
    iput p4, p0, Lcom/twitter/model/timeline/k;->e:I

    .line 52
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/g;Ljava/util/List;Ljava/lang/String;ILcom/twitter/model/timeline/k$1;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/model/timeline/k;-><init>(Lcom/twitter/model/timeline/g;Ljava/util/List;Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Lcom/twitter/model/timeline/g;Ljava/util/List;)Lcom/twitter/model/timeline/k;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/g;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/g;",
            ">;)",
            "Lcom/twitter/model/timeline/k;"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v0, Lcom/twitter/model/timeline/k;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/twitter/model/timeline/k;-><init>(Lcom/twitter/model/timeline/g;Ljava/util/List;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static a(Lcom/twitter/model/timeline/g;Ljava/util/List;Ljava/lang/String;)Lcom/twitter/model/timeline/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/timeline/g;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/g;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/twitter/model/timeline/k;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lcom/twitter/model/timeline/k;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/twitter/model/timeline/k;-><init>(Lcom/twitter/model/timeline/g;Ljava/util/List;Ljava/lang/String;I)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/timeline/k;)Z
    .locals 2

    .prologue
    .line 60
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    iget-object v1, p1, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    .line 61
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/k;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/timeline/k;->c:Ljava/util/List;

    .line 62
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/k;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/k;->d:Ljava/lang/String;

    .line 63
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/model/timeline/k;->e:I

    iget v1, p1, Lcom/twitter/model/timeline/k;->e:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 60
    :goto_0
    return v0

    .line 63
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 56
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/timeline/k;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/timeline/k;

    invoke-virtual {p0, p1}, Lcom/twitter/model/timeline/k;->a(Lcom/twitter/model/timeline/k;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v0

    .line 70
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/k;->c:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/k;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/timeline/k;->e:I

    add-int/2addr v0, v1

    .line 73
    return v0
.end method
