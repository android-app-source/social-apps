.class public final Lcom/twitter/model/timeline/n$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/timeline/n;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:I

.field public j:Ljava/lang/String;

.field public k:Lcom/twitter/model/core/v;

.field public l:Lcom/twitter/model/core/v;

.field public m:Ljava/lang/String;

.field public n:J

.field public o:I

.field public p:Z

.field public q:Ljava/lang/String;

.field public r:Z

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 233
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 262
    iput p1, p0, Lcom/twitter/model/timeline/n$a;->b:I

    .line 263
    return-object p0
.end method

.method public a(J)Lcom/twitter/model/timeline/n$a;
    .locals 1

    .prologue
    .line 334
    iput-wide p1, p0, Lcom/twitter/model/timeline/n$a;->n:J

    .line 335
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/v;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->k:Lcom/twitter/model/core/v;

    .line 317
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->a:Ljava/lang/String;

    .line 269
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 346
    iput-boolean p1, p0, Lcom/twitter/model/timeline/n$a;->p:Z

    .line 347
    return-object p0
.end method

.method public b(I)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 304
    iput p1, p0, Lcom/twitter/model/timeline/n$a;->i:I

    .line 305
    return-object p0
.end method

.method public b(Lcom/twitter/model/core/v;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->l:Lcom/twitter/model/core/v;

    .line 323
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->d:Ljava/lang/String;

    .line 275
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 358
    iput-boolean p1, p0, Lcom/twitter/model/timeline/n$a;->r:Z

    .line 359
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/twitter/model/timeline/n$a;->e()Lcom/twitter/model/timeline/n;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 340
    iput p1, p0, Lcom/twitter/model/timeline/n$a;->o:I

    .line 341
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->e:Ljava/lang/String;

    .line 281
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->f:Ljava/lang/String;

    .line 287
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->g:Ljava/lang/String;

    .line 293
    return-object p0
.end method

.method public e()Lcom/twitter/model/timeline/n;
    .locals 2

    .prologue
    .line 407
    new-instance v0, Lcom/twitter/model/timeline/n;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/timeline/n;-><init>(Lcom/twitter/model/timeline/n$a;Lcom/twitter/model/timeline/n$1;)V

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->h:Ljava/lang/String;

    .line 299
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->j:Ljava/lang/String;

    .line 311
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->m:Ljava/lang/String;

    .line 329
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 352
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->q:Ljava/lang/String;

    .line 353
    return-object p0
.end method

.method public j(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->s:Ljava/lang/String;

    .line 365
    return-object p0
.end method

.method public k(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->t:Ljava/lang/String;

    .line 371
    return-object p0
.end method

.method public l(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 376
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->u:Ljava/lang/String;

    .line 377
    return-object p0
.end method

.method public m(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->v:Ljava/lang/String;

    .line 383
    return-object p0
.end method

.method public n(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 388
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->w:Ljava/lang/String;

    .line 389
    return-object p0
.end method

.method public o(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 394
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->c:Ljava/lang/String;

    .line 395
    return-object p0
.end method

.method public p(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;
    .locals 0

    .prologue
    .line 400
    iput-object p1, p0, Lcom/twitter/model/timeline/n$a;->x:Ljava/lang/String;

    .line 401
    return-object p0
.end method
