.class public Lcom/twitter/model/timeline/at$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/at;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/at$a$b;,
        Lcom/twitter/model/timeline/at$a$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/timeline/at$a;",
            "Lcom/twitter/model/timeline/at$a$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/twitter/model/timeline/r;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterSocialProof;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/timeline/r;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Lcom/twitter/model/timeline/j;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/twitter/model/timeline/at$a$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/timeline/at$a$b;-><init>(Lcom/twitter/model/timeline/at$1;)V

    sput-object v0, Lcom/twitter/model/timeline/at$a;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/timeline/at$a$a;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lcom/twitter/model/timeline/at$a$a;->a(Lcom/twitter/model/timeline/at$a$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/timeline/at$a;->b:I

    .line 56
    invoke-static {p1}, Lcom/twitter/model/timeline/at$a$a;->b(Lcom/twitter/model/timeline/at$a$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/at$a;->c:Ljava/lang/String;

    .line 57
    invoke-static {p1}, Lcom/twitter/model/timeline/at$a$a;->c(Lcom/twitter/model/timeline/at$a$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/at$a;->d:Ljava/lang/String;

    .line 58
    invoke-static {p1}, Lcom/twitter/model/timeline/at$a$a;->d(Lcom/twitter/model/timeline/at$a$a;)Lcom/twitter/model/timeline/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/at$a;->e:Lcom/twitter/model/timeline/r;

    .line 59
    invoke-static {p1}, Lcom/twitter/model/timeline/at$a$a;->e(Lcom/twitter/model/timeline/at$a$a;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/at$a;->f:Ljava/util/Map;

    .line 60
    invoke-static {p1}, Lcom/twitter/model/timeline/at$a$a;->f(Lcom/twitter/model/timeline/at$a$a;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/at$a;->g:Ljava/util/Map;

    .line 61
    invoke-static {p1}, Lcom/twitter/model/timeline/at$a$a;->g(Lcom/twitter/model/timeline/at$a$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/at$a;->h:Ljava/util/List;

    .line 62
    invoke-static {p1}, Lcom/twitter/model/timeline/at$a$a;->h(Lcom/twitter/model/timeline/at$a$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/at$a;->i:Ljava/util/List;

    .line 63
    invoke-static {p1}, Lcom/twitter/model/timeline/at$a$a;->i(Lcom/twitter/model/timeline/at$a$a;)Lcom/twitter/model/timeline/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/at$a;->j:Lcom/twitter/model/timeline/j;

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/at$a$a;Lcom/twitter/model/timeline/at$1;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/twitter/model/timeline/at$a;-><init>(Lcom/twitter/model/timeline/at$a$a;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/timeline/at$a;)Z
    .locals 2

    .prologue
    .line 72
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/twitter/model/timeline/at$a;->b:I

    iget v1, p1, Lcom/twitter/model/timeline/at$a;->b:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/at$a;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/at$a;->c:Ljava/lang/String;

    .line 74
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/at$a;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/timeline/at$a;->d:Ljava/lang/String;

    .line 75
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/at$a;->h:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/timeline/at$a;->h:Ljava/util/List;

    .line 76
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/at$a;->i:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/timeline/at$a;->i:Ljava/util/List;

    .line 77
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/at$a;->f:Ljava/util/Map;

    iget-object v1, p1, Lcom/twitter/model/timeline/at$a;->f:Ljava/util/Map;

    .line 78
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/at$a;->j:Lcom/twitter/model/timeline/j;

    iget-object v1, p1, Lcom/twitter/model/timeline/at$a;->j:Lcom/twitter/model/timeline/j;

    .line 79
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 72
    :goto_0
    return v0

    .line 79
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 68
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/timeline/at$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/timeline/at$a;

    invoke-virtual {p0, p1}, Lcom/twitter/model/timeline/at$a;->a(Lcom/twitter/model/timeline/at$a;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lcom/twitter/model/timeline/at$a;->b:I

    .line 85
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/at$a;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/at$a;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/at$a;->h:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/at$a;->i:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/at$a;->f:Ljava/util/Map;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/timeline/at$a;->j:Lcom/twitter/model/timeline/j;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    return v0
.end method
