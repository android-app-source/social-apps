.class public final enum Lcom/twitter/model/timeline/AlertType;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/model/timeline/AlertType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/model/timeline/AlertType;

.field public static final enum b:Lcom/twitter/model/timeline/AlertType;

.field private static final synthetic c:[Lcom/twitter/model/timeline/AlertType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10
    new-instance v0, Lcom/twitter/model/timeline/AlertType;

    const-string/jumbo v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/timeline/AlertType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/timeline/AlertType;->a:Lcom/twitter/model/timeline/AlertType;

    .line 15
    new-instance v0, Lcom/twitter/model/timeline/AlertType;

    const-string/jumbo v1, "NEW_TWEETS"

    invoke-direct {v0, v1, v3}, Lcom/twitter/model/timeline/AlertType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/model/timeline/AlertType;->b:Lcom/twitter/model/timeline/AlertType;

    .line 6
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/model/timeline/AlertType;

    sget-object v1, Lcom/twitter/model/timeline/AlertType;->a:Lcom/twitter/model/timeline/AlertType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/model/timeline/AlertType;->b:Lcom/twitter/model/timeline/AlertType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/twitter/model/timeline/AlertType;->c:[Lcom/twitter/model/timeline/AlertType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/model/timeline/AlertType;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lcom/twitter/model/timeline/AlertType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/AlertType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/model/timeline/AlertType;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/twitter/model/timeline/AlertType;->c:[Lcom/twitter/model/timeline/AlertType;

    invoke-virtual {v0}, [Lcom/twitter/model/timeline/AlertType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/model/timeline/AlertType;

    return-object v0
.end method
