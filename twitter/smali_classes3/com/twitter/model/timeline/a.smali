.class public final Lcom/twitter/model/timeline/a;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Lcom/twitter/model/timeline/b;

.field public final b:Lcom/twitter/model/timeline/h;

.field public final c:Lcom/twitter/model/timeline/r;


# direct methods
.method private constructor <init>(Lcom/twitter/model/timeline/b;Lcom/twitter/model/timeline/h;Lcom/twitter/model/timeline/r;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/twitter/model/timeline/a;->a:Lcom/twitter/model/timeline/b;

    .line 42
    iput-object p2, p0, Lcom/twitter/model/timeline/a;->b:Lcom/twitter/model/timeline/h;

    .line 43
    iput-object p3, p0, Lcom/twitter/model/timeline/a;->c:Lcom/twitter/model/timeline/r;

    .line 44
    return-void
.end method

.method public static a(Lcom/twitter/model/timeline/b;Lcom/twitter/model/timeline/h;Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/a;
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/twitter/model/timeline/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/model/timeline/a;-><init>(Lcom/twitter/model/timeline/b;Lcom/twitter/model/timeline/h;Lcom/twitter/model/timeline/r;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 76
    if-ne p0, p1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v0

    .line 79
    :cond_1
    instance-of v2, p1, Lcom/twitter/model/timeline/a;

    if-eqz v2, :cond_3

    .line 80
    check-cast p1, Lcom/twitter/model/timeline/a;

    .line 81
    iget-object v2, p0, Lcom/twitter/model/timeline/a;->a:Lcom/twitter/model/timeline/b;

    iget-object v3, p1, Lcom/twitter/model/timeline/a;->a:Lcom/twitter/model/timeline/b;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/model/timeline/a;->b:Lcom/twitter/model/timeline/h;

    iget-object v3, p1, Lcom/twitter/model/timeline/a;->b:Lcom/twitter/model/timeline/h;

    .line 82
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/model/timeline/a;->c:Lcom/twitter/model/timeline/r;

    iget-object v3, p1, Lcom/twitter/model/timeline/a;->c:Lcom/twitter/model/timeline/r;

    .line 83
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 85
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/model/timeline/a;->a:Lcom/twitter/model/timeline/b;

    iget-object v1, p0, Lcom/twitter/model/timeline/a;->b:Lcom/twitter/model/timeline/h;

    iget-object v2, p0, Lcom/twitter/model/timeline/a;->c:Lcom/twitter/model/timeline/r;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
