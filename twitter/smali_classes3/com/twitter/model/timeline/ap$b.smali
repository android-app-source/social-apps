.class Lcom/twitter/model/timeline/ap$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/ap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/timeline/ap;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/ap$1;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/twitter/model/timeline/ap$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/timeline/ap;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    .line 126
    new-instance v1, Lcom/twitter/model/timeline/ap$a;

    invoke-direct {v1}, Lcom/twitter/model/timeline/ap$a;-><init>()V

    .line 127
    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/ap$a;->a(I)Lcom/twitter/model/timeline/ap$a;

    move-result-object v0

    .line 128
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ap$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/ap$a;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lcom/twitter/model/timeline/ap$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ap;

    .line 126
    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/ap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    iget v0, p2, Lcom/twitter/model/timeline/ap;->b:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 117
    iget-object v0, p2, Lcom/twitter/model/timeline/ap;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 118
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    check-cast p2, Lcom/twitter/model/timeline/ap;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/timeline/ap$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/ap;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 112
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/timeline/ap$b;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/timeline/ap;

    move-result-object v0

    return-object v0
.end method
