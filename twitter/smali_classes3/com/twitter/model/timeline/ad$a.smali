.class public final Lcom/twitter/model/timeline/ad$a;
.super Lcom/twitter/model/timeline/y$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/timeline/y$a",
        "<",
        "Lcom/twitter/model/timeline/ad;",
        "Lcom/twitter/model/timeline/ad$a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcha;

.field private q:Lcom/twitter/model/timeline/m;

.field private r:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/twitter/model/timeline/y$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/timeline/ad$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/model/timeline/ad$a;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/timeline/ad$a;)Lcha;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/model/timeline/ad$a;->p:Lcha;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/timeline/ad$a;)Lcom/twitter/model/timeline/m;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/model/timeline/ad$a;->q:Lcom/twitter/model/timeline/m;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/timeline/ad$a;)I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/twitter/model/timeline/ad$a;->r:I

    return v0
.end method


# virtual methods
.method public R_()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 103
    invoke-super {p0}, Lcom/twitter/model/timeline/y$a;->R_()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/model/timeline/ad$a;->a:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/twitter/model/timeline/ad$a;->r:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/twitter/model/timeline/ad$a;->r:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcha;)Lcom/twitter/model/timeline/ad$a;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/twitter/model/timeline/ad$a;->p:Lcha;

    .line 86
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/m;)Lcom/twitter/model/timeline/ad$a;
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/twitter/model/timeline/ad$a;->q:Lcom/twitter/model/timeline/m;

    .line 92
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/timeline/ad$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;)",
            "Lcom/twitter/model/timeline/ad$a;"
        }
    .end annotation

    .prologue
    .line 79
    iput-object p1, p0, Lcom/twitter/model/timeline/ad$a;->a:Ljava/util/List;

    .line 80
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/twitter/model/timeline/ad$a;->e()Lcom/twitter/model/timeline/ad;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/twitter/model/timeline/ad$a;
    .locals 0

    .prologue
    .line 97
    iput p1, p0, Lcom/twitter/model/timeline/ad$a;->r:I

    .line 98
    return-object p0
.end method

.method public e()Lcom/twitter/model/timeline/ad;
    .locals 3

    .prologue
    .line 110
    new-instance v0, Lcom/twitter/model/timeline/ad;

    const/16 v1, 0xf

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/model/timeline/ad;-><init>(Lcom/twitter/model/timeline/ad$a;ILcom/twitter/model/timeline/ad$1;)V

    return-object v0
.end method
