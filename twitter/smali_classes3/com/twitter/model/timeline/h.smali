.class public final Lcom/twitter/model/timeline/h;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/timeline/h$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/timeline/h;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/model/timeline/h$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/timeline/h$a;-><init>(Lcom/twitter/model/timeline/h$1;)V

    sput-object v0, Lcom/twitter/model/timeline/h;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/timeline/h;->b:Ljava/util/List;

    .line 32
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/twitter/model/timeline/h$1;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/twitter/model/timeline/h;-><init>(Ljava/util/List;)V

    return-void
.end method

.method public static a(Ljava/util/List;)Lcom/twitter/model/timeline/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/g;",
            ">;)",
            "Lcom/twitter/model/timeline/h;"
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lcom/twitter/model/timeline/h;

    invoke-direct {v0, p0}, Lcom/twitter/model/timeline/h;-><init>(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/timeline/h;)Z
    .locals 2

    .prologue
    .line 40
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/timeline/h;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/timeline/h;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 36
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/timeline/h;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/timeline/h;

    invoke-virtual {p0, p1}, Lcom/twitter/model/timeline/h;->a(Lcom/twitter/model/timeline/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/model/timeline/h;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/util/List;)I

    move-result v0

    return v0
.end method
