.class Lcom/twitter/model/timeline/r$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/timeline/r;",
        "Lcom/twitter/model/timeline/r$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/timeline/r$1;)V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/twitter/model/timeline/r$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/timeline/r$a;
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lcom/twitter/model/timeline/r$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/r$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/timeline/r$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 177
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/timeline/r$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    .line 178
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->b(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    .line 179
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    .line 180
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->e(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    .line 181
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->f(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    .line 182
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->d(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    .line 183
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->g(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    .line 184
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->h(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    .line 185
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 153
    check-cast p2, Lcom/twitter/model/timeline/r$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/timeline/r$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/timeline/r$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/r;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p2, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/r;->c:Ljava/lang/String;

    .line 159
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    .line 160
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/r;->d:Ljava/lang/String;

    .line 161
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/r;->g:Ljava/lang/String;

    .line 162
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/r;->f:Ljava/lang/String;

    .line 163
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/r;->h:Ljava/lang/String;

    .line 164
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/timeline/r;->i:Ljava/lang/String;

    .line 165
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 166
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    check-cast p2, Lcom/twitter/model/timeline/r;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/timeline/r$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/timeline/r;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/twitter/model/timeline/r$b;->a()Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    return-object v0
.end method
