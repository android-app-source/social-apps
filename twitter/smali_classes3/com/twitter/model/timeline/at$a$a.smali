.class public final Lcom/twitter/model/timeline/at$a$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/at$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/timeline/at$a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lcom/twitter/model/timeline/r;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterSocialProof;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/timeline/r;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/twitter/model/timeline/j;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 95
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/model/timeline/at$a$a;->a:I

    .line 114
    return-void
.end method

.method static synthetic a(Lcom/twitter/model/timeline/at$a$a;)I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/twitter/model/timeline/at$a$a;->a:I

    return v0
.end method

.method static synthetic b(Lcom/twitter/model/timeline/at$a$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/model/timeline/at$a$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/timeline/at$a$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/model/timeline/at$a$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/timeline/at$a$a;)Lcom/twitter/model/timeline/r;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/model/timeline/at$a$a;->d:Lcom/twitter/model/timeline/r;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/model/timeline/at$a$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/model/timeline/at$a$a;->e:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/model/timeline/at$a$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/model/timeline/at$a$a;->f:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/model/timeline/at$a$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/model/timeline/at$a$a;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/model/timeline/at$a$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/model/timeline/at$a$a;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/model/timeline/at$a$a;)Lcom/twitter/model/timeline/j;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/model/timeline/at$a$a;->i:Lcom/twitter/model/timeline/j;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/model/timeline/at$a$a;
    .locals 0

    .prologue
    .line 126
    iput p1, p0, Lcom/twitter/model/timeline/at$a$a;->a:I

    .line 127
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/j;)Lcom/twitter/model/timeline/at$a$a;
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/twitter/model/timeline/at$a$a;->i:Lcom/twitter/model/timeline/j;

    .line 163
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/at$a$a;
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/twitter/model/timeline/at$a$a;->d:Lcom/twitter/model/timeline/r;

    .line 145
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/timeline/at$a$a;
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/twitter/model/timeline/at$a$a;->b:Ljava/lang/String;

    .line 133
    return-object p0
.end method

.method public a(Ljava/util/List;Ljava/util/List;)Lcom/twitter/model/timeline/at$a$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/twitter/model/timeline/at$a$a;"
        }
    .end annotation

    .prologue
    .line 168
    iput-object p1, p0, Lcom/twitter/model/timeline/at$a$a;->g:Ljava/util/List;

    .line 169
    iput-object p2, p0, Lcom/twitter/model/timeline/at$a$a;->h:Ljava/util/List;

    .line 170
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcom/twitter/model/timeline/at$a$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterSocialProof;",
            ">;)",
            "Lcom/twitter/model/timeline/at$a$a;"
        }
    .end annotation

    .prologue
    .line 150
    iput-object p1, p0, Lcom/twitter/model/timeline/at$a$a;->e:Ljava/util/Map;

    .line 151
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/timeline/at$a$a;
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/twitter/model/timeline/at$a$a;->c:Ljava/lang/String;

    .line 139
    return-object p0
.end method

.method public b(Ljava/util/Map;)Lcom/twitter/model/timeline/at$a$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/timeline/r;",
            ">;)",
            "Lcom/twitter/model/timeline/at$a$a;"
        }
    .end annotation

    .prologue
    .line 156
    iput-object p1, p0, Lcom/twitter/model/timeline/at$a$a;->f:Ljava/util/Map;

    .line 157
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/twitter/model/timeline/at$a$a;->e()Lcom/twitter/model/timeline/at$a;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/timeline/at$a;
    .locals 2

    .prologue
    .line 176
    new-instance v0, Lcom/twitter/model/timeline/at$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/timeline/at$a;-><init>(Lcom/twitter/model/timeline/at$a$a;Lcom/twitter/model/timeline/at$1;)V

    return-object v0
.end method
