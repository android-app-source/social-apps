.class public final Lcom/twitter/model/av/Video$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/av/Video;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/av/Video;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:J

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Lcom/twitter/model/av/VideoCta;

.field private h:Z

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 250
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 251
    iput-object v3, p0, Lcom/twitter/model/av/Video$a;->a:Ljava/lang/String;

    .line 252
    iput-object v3, p0, Lcom/twitter/model/av/Video$a;->b:Ljava/lang/String;

    .line 253
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/model/av/Video$a;->c:J

    .line 255
    iput-boolean v2, p0, Lcom/twitter/model/av/Video$a;->e:Z

    .line 256
    sget-object v0, Ljava/util/Collections;->EMPTY_MAP:Ljava/util/Map;

    iput-object v0, p0, Lcom/twitter/model/av/Video$a;->f:Ljava/util/Map;

    .line 257
    iput-object v3, p0, Lcom/twitter/model/av/Video$a;->g:Lcom/twitter/model/av/VideoCta;

    .line 258
    iput-boolean v2, p0, Lcom/twitter/model/av/Video$a;->h:Z

    .line 259
    iput-boolean v2, p0, Lcom/twitter/model/av/Video$a;->i:Z

    .line 260
    iput-boolean v2, p0, Lcom/twitter/model/av/Video$a;->j:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/av/Video$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/twitter/model/av/Video$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/av/Video$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/twitter/model/av/Video$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/av/Video$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/twitter/model/av/Video$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/av/Video$a;)J
    .locals 2

    .prologue
    .line 250
    iget-wide v0, p0, Lcom/twitter/model/av/Video$a;->c:J

    return-wide v0
.end method

.method static synthetic e(Lcom/twitter/model/av/Video$a;)Z
    .locals 1

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/twitter/model/av/Video$a;->e:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/model/av/Video$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/twitter/model/av/Video$a;->f:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/model/av/Video$a;)Lcom/twitter/model/av/VideoCta;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/twitter/model/av/Video$a;->g:Lcom/twitter/model/av/VideoCta;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/model/av/Video$a;)Z
    .locals 1

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/twitter/model/av/Video$a;->h:Z

    return v0
.end method

.method static synthetic i(Lcom/twitter/model/av/Video$a;)Z
    .locals 1

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/twitter/model/av/Video$a;->i:Z

    return v0
.end method

.method static synthetic j(Lcom/twitter/model/av/Video$a;)Z
    .locals 1

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/twitter/model/av/Video$a;->j:Z

    return v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/twitter/model/av/Video$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/av/Video$a;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/av/Video$a;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcom/twitter/model/av/Video$a;
    .locals 1

    .prologue
    .line 276
    iput-wide p1, p0, Lcom/twitter/model/av/Video$a;->c:J

    .line 277
    return-object p0
.end method

.method public a(Lcom/twitter/model/av/VideoCta;)Lcom/twitter/model/av/Video$a;
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/twitter/model/av/Video$a;->g:Lcom/twitter/model/av/VideoCta;

    .line 313
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/av/Video$a;
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/twitter/model/av/Video$a;->a:Ljava/lang/String;

    .line 265
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcom/twitter/model/av/Video$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/twitter/model/av/Video$a;"
        }
    .end annotation

    .prologue
    .line 294
    iput-object p1, p0, Lcom/twitter/model/av/Video$a;->f:Ljava/util/Map;

    .line 295
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/av/Video$a;
    .locals 0

    .prologue
    .line 288
    iput-boolean p1, p0, Lcom/twitter/model/av/Video$a;->e:Z

    .line 289
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/av/Video$a;
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/twitter/model/av/Video$a;->b:Ljava/lang/String;

    .line 271
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/av/Video$a;
    .locals 0

    .prologue
    .line 300
    iput-boolean p1, p0, Lcom/twitter/model/av/Video$a;->h:Z

    .line 301
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/twitter/model/av/Video$a;->e()Lcom/twitter/model/av/Video;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/av/Video$a;
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/twitter/model/av/Video$a;->d:Ljava/lang/String;

    .line 283
    return-object p0
.end method

.method public c(Z)Lcom/twitter/model/av/Video$a;
    .locals 0

    .prologue
    .line 306
    iput-boolean p1, p0, Lcom/twitter/model/av/Video$a;->i:Z

    .line 307
    return-object p0
.end method

.method public d(Z)Lcom/twitter/model/av/Video$a;
    .locals 0

    .prologue
    .line 318
    iput-boolean p1, p0, Lcom/twitter/model/av/Video$a;->j:Z

    .line 319
    return-object p0
.end method

.method protected e()Lcom/twitter/model/av/Video;
    .locals 2

    .prologue
    .line 330
    new-instance v0, Lcom/twitter/model/av/Video;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/av/Video;-><init>(Lcom/twitter/model/av/Video$a;Lcom/twitter/model/av/Video$1;)V

    return-object v0
.end method
