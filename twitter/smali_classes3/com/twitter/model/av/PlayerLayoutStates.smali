.class public Lcom/twitter/model/av/PlayerLayoutStates;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/av/PlayerLayoutStates$a;,
        Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;
    }
.end annotation


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;",
            "Lcom/twitter/model/av/PlayerLayoutStates$a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Z

.field private d:J


# direct methods
.method public constructor <init>(ZZ)V
    .locals 4

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->a:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    new-instance v2, Lcom/twitter/model/av/PlayerLayoutStates$a;

    sget-object v3, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->a:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    invoke-direct {v2, v3}, Lcom/twitter/model/av/PlayerLayoutStates$a;-><init>(Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;)V

    .line 52
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    new-instance v2, Lcom/twitter/model/av/PlayerLayoutStates$a;

    sget-object v3, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    invoke-direct {v2, v3}, Lcom/twitter/model/av/PlayerLayoutStates$a;-><init>(Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;)V

    .line 54
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->c:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    new-instance v2, Lcom/twitter/model/av/PlayerLayoutStates$a;

    sget-object v3, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->c:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    invoke-direct {v2, v3}, Lcom/twitter/model/av/PlayerLayoutStates$a;-><init>(Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;)V

    .line 56
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->d:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    new-instance v2, Lcom/twitter/model/av/PlayerLayoutStates$a;

    sget-object v3, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->d:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    invoke-direct {v2, v3}, Lcom/twitter/model/av/PlayerLayoutStates$a;-><init>(Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;)V

    .line 58
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->a:Ljava/util/Map;

    .line 67
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/av/PlayerLayoutStates;->a(ZZ)V

    .line 68
    return-void
.end method

.method private a(Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;J)V
    .locals 4

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/PlayerLayoutStates$a;

    .line 117
    if-eqz v0, :cond_0

    .line 118
    iget-wide v2, v0, Lcom/twitter/model/av/PlayerLayoutStates$a;->c:J

    add-long/2addr v2, p2

    iput-wide v2, v0, Lcom/twitter/model/av/PlayerLayoutStates$a;->c:J

    .line 120
    :cond_0
    return-void
.end method

.method private a(ZZ)V
    .locals 6

    .prologue
    .line 91
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 93
    iget-wide v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->d:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 94
    iget-wide v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->d:J

    sub-long v4, v2, v0

    .line 96
    iget-boolean v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->c:Z

    if-eqz v0, :cond_2

    .line 97
    iget-boolean v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->b:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 103
    :goto_0
    invoke-direct {p0, v0, v4, v5}, Lcom/twitter/model/av/PlayerLayoutStates;->a(Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;J)V

    .line 105
    :cond_0
    iput-boolean p1, p0, Lcom/twitter/model/av/PlayerLayoutStates;->b:Z

    .line 106
    iput-boolean p2, p0, Lcom/twitter/model/av/PlayerLayoutStates;->c:Z

    .line 107
    iput-wide v2, p0, Lcom/twitter/model/av/PlayerLayoutStates;->d:J

    .line 108
    return-void

    .line 97
    :cond_1
    sget-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->d:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    goto :goto_0

    .line 100
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->b:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->a:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->c:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    goto :goto_0
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->b:Z

    invoke-direct {p0, v0, p1}, Lcom/twitter/model/av/PlayerLayoutStates;->a(ZZ)V

    .line 80
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->b:Z

    return v0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->c:Z

    invoke-direct {p0, p1, v0}, Lcom/twitter/model/av/PlayerLayoutStates;->a(ZZ)V

    .line 84
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->c:Z

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->b:Z

    iget-boolean v1, p0, Lcom/twitter/model/av/PlayerLayoutStates;->b:Z

    invoke-direct {p0, v0, v1}, Lcom/twitter/model/av/PlayerLayoutStates;->a(ZZ)V

    .line 88
    return-void
.end method

.method public d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/av/PlayerLayoutStates$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/model/av/PlayerLayoutStates;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
