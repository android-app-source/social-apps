.class public final Lcom/twitter/model/av/i$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/av/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/av/i;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/av/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/av/i$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/model/av/i$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/av/i$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/model/av/i$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/av/i$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/model/av/i$a;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/av/i$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/model/av/i$a;->d:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/twitter/model/av/i$a;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/twitter/model/av/i$a;->a:Ljava/lang/String;

    .line 65
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/av/i$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;)",
            "Lcom/twitter/model/av/i$a;"
        }
    .end annotation

    .prologue
    .line 76
    iput-object p1, p0, Lcom/twitter/model/av/i$a;->c:Ljava/util/List;

    .line 77
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcom/twitter/model/av/i$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/av/j;",
            ">;)",
            "Lcom/twitter/model/av/i$a;"
        }
    .end annotation

    .prologue
    .line 82
    iput-object p1, p0, Lcom/twitter/model/av/i$a;->d:Ljava/util/Map;

    .line 83
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/av/i$a;
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/twitter/model/av/i$a;->b:Ljava/lang/String;

    .line 71
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/twitter/model/av/i$a;->e()Lcom/twitter/model/av/i;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/av/i;
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lcom/twitter/model/av/i;

    invoke-direct {v0, p0}, Lcom/twitter/model/av/i;-><init>(Lcom/twitter/model/av/i$a;)V

    return-object v0
.end method
