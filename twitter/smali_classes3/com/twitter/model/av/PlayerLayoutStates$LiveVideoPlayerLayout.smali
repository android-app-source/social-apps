.class public final enum Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/av/PlayerLayoutStates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LiveVideoPlayerLayout"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

.field public static final enum b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

.field public static final enum c:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

.field public static final enum d:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

.field public static final enum e:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

.field public static final enum f:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

.field public static final enum g:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

.field public static final enum h:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

.field static final i:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic j:[Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;


# instance fields
.field public final index:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 31
    new-instance v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    const-string/jumbo v1, "FULLSCREEN_PORTRAIT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->a:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 32
    new-instance v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    const-string/jumbo v1, "FULLSCREEN_LANDSCAPE"

    invoke-direct {v0, v1, v4, v5}, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 33
    new-instance v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    const-string/jumbo v1, "NONFULLSCREEN_PORTRAIT"

    invoke-direct {v0, v1, v5, v6}, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->c:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 34
    new-instance v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    const-string/jumbo v1, "NONFULLSCREEN_LANDSCAPE"

    invoke-direct {v0, v1, v6, v7}, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->d:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 35
    new-instance v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    const-string/jumbo v1, "NARROW_SHORT"

    invoke-direct {v0, v1, v7, v8}, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->e:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 36
    new-instance v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    const-string/jumbo v1, "NARROW_TALL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->f:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 37
    new-instance v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    const-string/jumbo v1, "WIDE"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->g:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 38
    new-instance v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    const-string/jumbo v1, "FULLSCREEN"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->h:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 30
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    const/4 v1, 0x0

    sget-object v2, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->a:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    aput-object v2, v0, v1

    sget-object v1, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->c:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->d:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->e:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    aput-object v1, v0, v7

    sget-object v1, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->f:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->g:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->h:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->j:[Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 40
    const-class v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 41
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->i:Lcom/twitter/util/serialization/l;

    .line 40
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->index:I

    .line 47
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    return-object v0
.end method

.method public static values()[Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->j:[Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    invoke-virtual {v0}, [Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    return-object v0
.end method
