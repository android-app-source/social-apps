.class final Lcom/twitter/model/av/VideoCta$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/av/VideoCta;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/av/VideoCta;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/av/VideoCtaType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    .line 123
    const-class v0, Lcom/twitter/model/av/VideoCtaType;

    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/av/VideoCta$a;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/av/VideoCta$1;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/twitter/model/av/VideoCta$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/av/VideoCta;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/twitter/model/av/VideoCta$a;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/VideoCtaType;

    .line 136
    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Ljava/util/Map;

    move-result-object v1

    .line 138
    new-instance v2, Lcom/twitter/model/av/VideoCta;

    invoke-direct {v2, v0, v1}, Lcom/twitter/model/av/VideoCta;-><init>(Lcom/twitter/model/av/VideoCtaType;Ljava/util/Map;)V

    return-object v2
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/av/VideoCta;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/model/av/VideoCta$a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p2}, Lcom/twitter/model/av/VideoCta;->a(Lcom/twitter/model/av/VideoCta;)Lcom/twitter/model/av/VideoCtaType;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 128
    invoke-static {p2}, Lcom/twitter/model/av/VideoCta;->b(Lcom/twitter/model/av/VideoCta;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Map;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)V

    .line 129
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    check-cast p2, Lcom/twitter/model/av/VideoCta;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/av/VideoCta$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/av/VideoCta;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 122
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/av/VideoCta$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/av/VideoCta;

    move-result-object v0

    return-object v0
.end method
