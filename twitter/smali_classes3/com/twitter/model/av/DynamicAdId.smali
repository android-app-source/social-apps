.class public Lcom/twitter/model/av/DynamicAdId;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/twitter/model/av/DynamicAdId;


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 16
    new-instance v0, Lcom/twitter/model/av/DynamicAdId;

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/twitter/model/av/DynamicAdId;-><init>(JLjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/av/DynamicAdId;->a:Lcom/twitter/model/av/DynamicAdId;

    .line 18
    new-instance v0, Lcom/twitter/model/av/DynamicAdId$1;

    invoke-direct {v0}, Lcom/twitter/model/av/DynamicAdId$1;-><init>()V

    sput-object v0, Lcom/twitter/model/av/DynamicAdId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-wide p1, p0, Lcom/twitter/model/av/DynamicAdId;->b:J

    .line 37
    iput-object p3, p0, Lcom/twitter/model/av/DynamicAdId;->c:Ljava/lang/String;

    .line 38
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/av/DynamicAdId;->b:J

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/av/DynamicAdId;->c:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public static a(JLcgi;)Lcom/twitter/model/av/DynamicAdId;
    .locals 2

    .prologue
    .line 94
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcgi;->c:Ljava/lang/String;

    .line 95
    :goto_0
    new-instance v1, Lcom/twitter/model/av/DynamicAdId;

    invoke-direct {v1, p0, p1, v0}, Lcom/twitter/model/av/DynamicAdId;-><init>(JLjava/lang/String;)V

    return-object v1

    .line 94
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(JLjava/lang/String;)Lcom/twitter/model/av/DynamicAdId;
    .locals 2

    .prologue
    .line 88
    new-instance v0, Lcom/twitter/model/av/DynamicAdId;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/model/av/DynamicAdId;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public static b(JLjava/lang/String;)Lcom/twitter/model/av/DynamicAdId;
    .locals 2

    .prologue
    .line 104
    invoke-static {p0, p1, p2}, Lcom/twitter/model/av/DynamicAdId;->a(JLjava/lang/String;)Lcom/twitter/model/av/DynamicAdId;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 4

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/twitter/model/av/DynamicAdId;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 58
    if-ne p0, p1, :cond_1

    .line 59
    const/4 v0, 0x1

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 61
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 65
    check-cast p1, Lcom/twitter/model/av/DynamicAdId;

    .line 67
    iget-wide v2, p0, Lcom/twitter/model/av/DynamicAdId;->b:J

    iget-wide v4, p1, Lcom/twitter/model/av/DynamicAdId;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 71
    iget-object v0, p0, Lcom/twitter/model/av/DynamicAdId;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/av/DynamicAdId;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/twitter/model/av/DynamicAdId;->b:J

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v0

    .line 77
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/av/DynamicAdId;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/twitter/model/av/DynamicAdId;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 53
    iget-object v0, p0, Lcom/twitter/model/av/DynamicAdId;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 54
    return-void
.end method
