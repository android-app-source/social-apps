.class public Lcom/twitter/model/av/LiveVideoPlaylist;
.super Lcom/twitter/model/av/AVMediaPlaylist;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/model/av/LiveVideoPlaylist;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Lcom/twitter/model/av/AVMedia;

.field private final f:Lcom/twitter/model/av/AVMedia;

.field private final g:Lcom/twitter/model/av/DynamicAdInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/twitter/model/av/LiveVideoPlaylist$1;

    invoke-direct {v0}, Lcom/twitter/model/av/LiveVideoPlaylist$1;-><init>()V

    sput-object v0, Lcom/twitter/model/av/LiveVideoPlaylist;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/twitter/model/av/AVMediaPlaylist;-><init>(Landroid/os/Parcel;)V

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->d:Ljava/lang/String;

    .line 40
    const-class v0, Lcom/twitter/model/av/LiveVideoMedia;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/AVMedia;

    iput-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->e:Lcom/twitter/model/av/AVMedia;

    .line 41
    const-class v0, Lcom/twitter/model/av/Video;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/AVMedia;

    iput-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->f:Lcom/twitter/model/av/AVMedia;

    .line 42
    const-class v0, Lcom/twitter/model/av/DynamicAdInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/DynamicAdInfo;

    iput-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->g:Lcom/twitter/model/av/DynamicAdInfo;

    .line 43
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p2, p3}, Lcom/twitter/model/av/AVMediaPlaylist;-><init>(ILjava/lang/String;)V

    .line 55
    iput-object p1, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->d:Ljava/lang/String;

    .line 56
    iput-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->e:Lcom/twitter/model/av/AVMedia;

    .line 57
    iput-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->f:Lcom/twitter/model/av/AVMedia;

    .line 58
    iput-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->g:Lcom/twitter/model/av/DynamicAdInfo;

    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/DynamicAdInfo;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/twitter/model/av/AVMediaPlaylist;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->d:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->e:Lcom/twitter/model/av/AVMedia;

    .line 49
    iput-object p3, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->f:Lcom/twitter/model/av/AVMedia;

    .line 50
    iput-object p4, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->g:Lcom/twitter/model/av/DynamicAdInfo;

    .line 51
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/av/DynamicAdInfo;Lcom/twitter/util/collection/k;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/twitter/model/av/AVMediaPlaylist;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p1, Lcom/twitter/model/av/DynamicAdInfo;->a:Lcom/twitter/model/av/DynamicAd;

    if-eqz v0, :cond_0

    iget-object v1, p1, Lcom/twitter/model/av/DynamicAdInfo;->a:Lcom/twitter/model/av/DynamicAd;

    const-string/jumbo v0, ""

    .line 90
    invoke-virtual {p2, v0}, Lcom/twitter/util/collection/k;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/model/av/DynamicAd;->a(Ljava/lang/String;)Lcom/twitter/model/av/Video;

    move-result-object v0

    .line 93
    :goto_0
    new-instance v1, Lcom/twitter/model/av/LiveVideoPlaylist;

    iget-object v2, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->e:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v1, v2, v3, v0, p1}, Lcom/twitter/model/av/LiveVideoPlaylist;-><init>(Ljava/lang/String;Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/DynamicAdInfo;)V

    return-object v1

    .line 90
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->a:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->e:Lcom/twitter/model/av/AVMedia;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lcom/twitter/model/av/AVMedia;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->e:Lcom/twitter/model/av/AVMedia;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 113
    if-ne p0, p1, :cond_1

    .line 123
    :cond_0
    :goto_0
    return v0

    .line 114
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 115
    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/model/av/AVMediaPlaylist;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    .line 117
    :cond_4
    check-cast p1, Lcom/twitter/model/av/LiveVideoPlaylist;

    .line 119
    iget-object v2, p1, Lcom/twitter/model/av/LiveVideoPlaylist;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    .line 120
    :cond_5
    iget-object v2, p1, Lcom/twitter/model/av/LiveVideoPlaylist;->e:Lcom/twitter/model/av/AVMedia;

    iget-object v3, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->e:Lcom/twitter/model/av/AVMedia;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    .line 121
    :cond_6
    iget-object v2, p1, Lcom/twitter/model/av/LiveVideoPlaylist;->f:Lcom/twitter/model/av/AVMedia;

    iget-object v3, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->f:Lcom/twitter/model/av/AVMedia;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    .line 122
    :cond_7
    iget-object v2, p1, Lcom/twitter/model/av/LiveVideoPlaylist;->g:Lcom/twitter/model/av/DynamicAdInfo;

    iget-object v3, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->g:Lcom/twitter/model/av/DynamicAdInfo;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 128
    invoke-super {p0}, Lcom/twitter/model/av/AVMediaPlaylist;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->e:Lcom/twitter/model/av/AVMedia;

    iget-object v3, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->f:Lcom/twitter/model/av/AVMedia;

    iget-object v4, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->g:Lcom/twitter/model/av/DynamicAdInfo;

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public i()Lcom/twitter/model/av/DynamicAdInfo;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->g:Lcom/twitter/model/av/DynamicAdInfo;

    return-object v0
.end method

.method public k()Lcom/twitter/model/av/AVMedia;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->f:Lcom/twitter/model/av/AVMedia;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0, p1, p2}, Lcom/twitter/model/av/AVMediaPlaylist;->a(Landroid/os/Parcel;I)V

    .line 105
    iget-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->e:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 107
    iget-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->f:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 108
    iget-object v0, p0, Lcom/twitter/model/av/LiveVideoPlaylist;->g:Lcom/twitter/model/av/DynamicAdInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 109
    return-void
.end method
