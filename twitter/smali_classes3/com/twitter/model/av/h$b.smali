.class Lcom/twitter/model/av/h$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/av/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/av/h;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/av/h$1;)V
    .locals 0

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/twitter/model/av/h$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/av/h;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 321
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v0

    .line 322
    sget-object v1, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    .line 323
    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->d(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/Set;

    move-result-object v1

    .line 324
    sget-object v2, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    .line 325
    invoke-static {p1, v2}, Lcom/twitter/util/collection/d;->d(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/Set;

    move-result-object v2

    .line 326
    sget-object v3, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v3}, Lcom/twitter/util/collection/d;->d(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/Set;

    move-result-object v3

    .line 328
    sget-object v4, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    .line 329
    invoke-static {p1, v4}, Lcom/twitter/util/collection/d;->d(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/Set;

    move-result-object v4

    .line 330
    sget-object v5, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v5}, Lcom/twitter/util/collection/d;->d(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/Set;

    move-result-object v5

    .line 333
    new-instance v6, Lcom/twitter/model/av/h$a;

    invoke-direct {v6}, Lcom/twitter/model/av/h$a;-><init>()V

    .line 334
    invoke-virtual {v6, v0}, Lcom/twitter/model/av/h$a;->a(Z)Lcom/twitter/model/av/h$a;

    move-result-object v0

    .line 335
    invoke-virtual {v0, v1}, Lcom/twitter/model/av/h$a;->a(Ljava/util/Collection;)Lcom/twitter/model/av/h$a;

    move-result-object v0

    .line 336
    invoke-virtual {v0, v2}, Lcom/twitter/model/av/h$a;->b(Ljava/util/Collection;)Lcom/twitter/model/av/h$a;

    move-result-object v0

    .line 337
    invoke-virtual {v0, v3}, Lcom/twitter/model/av/h$a;->c(Ljava/util/Collection;)Lcom/twitter/model/av/h$a;

    move-result-object v0

    .line 338
    invoke-virtual {v0, v4}, Lcom/twitter/model/av/h$a;->d(Ljava/util/Collection;)Lcom/twitter/model/av/h$a;

    move-result-object v0

    .line 339
    invoke-virtual {v0, v5}, Lcom/twitter/model/av/h$a;->e(Ljava/util/Collection;)Lcom/twitter/model/av/h$a;

    move-result-object v0

    .line 340
    invoke-virtual {v0}, Lcom/twitter/model/av/h$a;->c()Lcom/twitter/model/av/h;

    move-result-object v0

    .line 333
    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/av/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 306
    invoke-virtual {p2}, Lcom/twitter/model/av/h;->a()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 307
    invoke-virtual {p2}, Lcom/twitter/model/av/h;->b()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Set;Lcom/twitter/util/serialization/l;)V

    .line 309
    invoke-virtual {p2}, Lcom/twitter/model/av/h;->c()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Set;Lcom/twitter/util/serialization/l;)V

    .line 310
    invoke-virtual {p2}, Lcom/twitter/model/av/h;->d()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Set;Lcom/twitter/util/serialization/l;)V

    .line 312
    invoke-virtual {p2}, Lcom/twitter/model/av/h;->e()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Set;Lcom/twitter/util/serialization/l;)V

    .line 313
    invoke-virtual {p2}, Lcom/twitter/model/av/h;->f()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Set;Lcom/twitter/util/serialization/l;)V

    .line 315
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    check-cast p2, Lcom/twitter/model/av/h;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/av/h$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/av/h;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 302
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/av/h$b;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/av/h;

    move-result-object v0

    return-object v0
.end method
