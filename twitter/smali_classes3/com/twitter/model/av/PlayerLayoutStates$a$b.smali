.class Lcom/twitter/model/av/PlayerLayoutStates$a$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/av/PlayerLayoutStates$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/av/PlayerLayoutStates$a;",
        "Lcom/twitter/model/av/PlayerLayoutStates$a$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/av/PlayerLayoutStates$1;)V
    .locals 0

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/twitter/model/av/PlayerLayoutStates$a$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/av/PlayerLayoutStates$a$a;
    .locals 1

    .prologue
    .line 197
    new-instance v0, Lcom/twitter/model/av/PlayerLayoutStates$a$a;

    invoke-direct {v0}, Lcom/twitter/model/av/PlayerLayoutStates$a$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/av/PlayerLayoutStates$a$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 210
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/av/PlayerLayoutStates$a$a;->a(J)Lcom/twitter/model/av/PlayerLayoutStates$a$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->i:Lcom/twitter/util/serialization/l;

    .line 211
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    invoke-virtual {v1, v0}, Lcom/twitter/model/av/PlayerLayoutStates$a$a;->a(Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;)Lcom/twitter/model/av/PlayerLayoutStates$a$a;

    .line 212
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 192
    check-cast p2, Lcom/twitter/model/av/PlayerLayoutStates$a$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/av/PlayerLayoutStates$a$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/av/PlayerLayoutStates$a$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/av/PlayerLayoutStates$a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 203
    iget-wide v0, p2, Lcom/twitter/model/av/PlayerLayoutStates$a;->c:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/av/PlayerLayoutStates$a;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    sget-object v2, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->i:Lcom/twitter/util/serialization/l;

    .line 204
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 205
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    check-cast p2, Lcom/twitter/model/av/PlayerLayoutStates$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/av/PlayerLayoutStates$a$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/av/PlayerLayoutStates$a;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/twitter/model/av/PlayerLayoutStates$a$b;->a()Lcom/twitter/model/av/PlayerLayoutStates$a$a;

    move-result-object v0

    return-object v0
.end method
