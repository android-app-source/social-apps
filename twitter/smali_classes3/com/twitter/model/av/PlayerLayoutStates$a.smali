.class public Lcom/twitter/model/av/PlayerLayoutStates$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/av/PlayerLayoutStates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/av/PlayerLayoutStates$a$b;,
        Lcom/twitter/model/av/PlayerLayoutStates$a$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/av/PlayerLayoutStates$a;",
            "Lcom/twitter/model/av/PlayerLayoutStates$a$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

.field public c:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 131
    new-instance v0, Lcom/twitter/model/av/PlayerLayoutStates$a$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/av/PlayerLayoutStates$a$b;-><init>(Lcom/twitter/model/av/PlayerLayoutStates$1;)V

    sput-object v0, Lcom/twitter/model/av/PlayerLayoutStates$a;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;)V
    .locals 2

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput-object p1, p0, Lcom/twitter/model/av/PlayerLayoutStates$a;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 145
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/model/av/PlayerLayoutStates$a;->c:J

    .line 146
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/av/PlayerLayoutStates$a$a;)V
    .locals 2

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iget-object v0, p1, Lcom/twitter/model/av/PlayerLayoutStates$a$a;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    iput-object v0, p0, Lcom/twitter/model/av/PlayerLayoutStates$a;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    .line 140
    iget-wide v0, p1, Lcom/twitter/model/av/PlayerLayoutStates$a$a;->a:J

    iput-wide v0, p0, Lcom/twitter/model/av/PlayerLayoutStates$a;->c:J

    .line 141
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 174
    if-ne p0, p1, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v0

    .line 177
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 178
    goto :goto_0

    .line 181
    :cond_3
    check-cast p1, Lcom/twitter/model/av/PlayerLayoutStates$a;

    .line 183
    iget-object v2, p0, Lcom/twitter/model/av/PlayerLayoutStates$a;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    iget-object v3, p1, Lcom/twitter/model/av/PlayerLayoutStates$a;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p0, Lcom/twitter/model/av/PlayerLayoutStates$a;->c:J

    .line 184
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lcom/twitter/model/av/PlayerLayoutStates$a;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/model/av/PlayerLayoutStates$a;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    iget-wide v2, p0, Lcom/twitter/model/av/PlayerLayoutStates$a;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
