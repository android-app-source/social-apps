.class public Lcom/twitter/model/av/Audio;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/av/AVMedia;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/model/av/Audio;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Ljava/lang/String;

.field protected final c:Ljava/lang/String;

.field protected final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field protected final e:Ljava/lang/String;

.field protected final f:Ljava/lang/String;

.field protected final g:Ljava/lang/String;

.field protected final h:Ljava/lang/String;

.field protected final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/model/av/Audio$1;

    invoke-direct {v0}, Lcom/twitter/model/av/Audio$1;-><init>()V

    sput-object v0, Lcom/twitter/model/av/Audio;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/av/Audio;->a:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/av/Audio;->b:Ljava/lang/String;

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/av/Audio;->c:Ljava/lang/String;

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/av/Audio;->e:Ljava/lang/String;

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/av/Audio;->f:Ljava/lang/String;

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/av/Audio;->g:Ljava/lang/String;

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/av/Audio;->h:Ljava/lang/String;

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/av/Audio;->i:Ljava/lang/String;

    .line 83
    invoke-static {p1}, Lcom/twitter/util/l;->b(Landroid/os/Parcel;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/av/Audio;->d:Ljava/util/Map;

    .line 84
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/twitter/model/av/Audio;->a:Ljava/lang/String;

    .line 60
    iput-object p2, p0, Lcom/twitter/model/av/Audio;->b:Ljava/lang/String;

    .line 61
    iput-object p3, p0, Lcom/twitter/model/av/Audio;->c:Ljava/lang/String;

    .line 62
    iput-object p4, p0, Lcom/twitter/model/av/Audio;->d:Ljava/util/Map;

    .line 63
    iput-object p5, p0, Lcom/twitter/model/av/Audio;->e:Ljava/lang/String;

    .line 64
    iput-object p6, p0, Lcom/twitter/model/av/Audio;->f:Ljava/lang/String;

    .line 65
    iput-object p7, p0, Lcom/twitter/model/av/Audio;->g:Ljava/lang/String;

    .line 66
    iput-object p8, p0, Lcom/twitter/model/av/Audio;->h:Ljava/lang/String;

    .line 67
    iput-object p9, p0, Lcom/twitter/model/av/Audio;->i:Ljava/lang/String;

    .line 68
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->d:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->h:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    const-string/jumbo v0, "audio"

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->g:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 229
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 252
    :cond_0
    :goto_0
    return v1

    .line 230
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 232
    check-cast p1, Lcom/twitter/model/av/Audio;

    .line 234
    iget-object v2, p0, Lcom/twitter/model/av/Audio;->b:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/twitter/model/av/Audio;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/av/Audio;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 236
    :cond_2
    iget-object v2, p0, Lcom/twitter/model/av/Audio;->c:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/twitter/model/av/Audio;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/av/Audio;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 238
    :cond_3
    iget-object v2, p0, Lcom/twitter/model/av/Audio;->e:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/twitter/model/av/Audio;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/av/Audio;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 241
    :cond_4
    iget-object v2, p0, Lcom/twitter/model/av/Audio;->f:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/twitter/model/av/Audio;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/av/Audio;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 244
    :cond_5
    iget-object v2, p0, Lcom/twitter/model/av/Audio;->i:Ljava/lang/String;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/twitter/model/av/Audio;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/av/Audio;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 246
    :cond_6
    iget-object v2, p0, Lcom/twitter/model/av/Audio;->d:Ljava/util/Map;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/twitter/model/av/Audio;->d:Ljava/util/Map;

    iget-object v3, p1, Lcom/twitter/model/av/Audio;->d:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 249
    :cond_7
    iget-object v2, p0, Lcom/twitter/model/av/Audio;->h:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/twitter/model/av/Audio;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/av/Audio;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 251
    :cond_8
    iget-object v2, p0, Lcom/twitter/model/av/Audio;->a:Ljava/lang/String;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/twitter/model/av/Audio;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/av/Audio;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 252
    :cond_9
    iget-object v2, p0, Lcom/twitter/model/av/Audio;->g:Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->g:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/av/Audio;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_a
    :goto_1
    move v1, v0

    goto/16 :goto_0

    .line 234
    :cond_b
    iget-object v2, p1, Lcom/twitter/model/av/Audio;->b:Ljava/lang/String;

    if-eqz v2, :cond_2

    goto/16 :goto_0

    .line 236
    :cond_c
    iget-object v2, p1, Lcom/twitter/model/av/Audio;->c:Ljava/lang/String;

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 238
    :cond_d
    iget-object v2, p1, Lcom/twitter/model/av/Audio;->e:Ljava/lang/String;

    if-eqz v2, :cond_4

    goto/16 :goto_0

    .line 241
    :cond_e
    iget-object v2, p1, Lcom/twitter/model/av/Audio;->f:Ljava/lang/String;

    if-eqz v2, :cond_5

    goto/16 :goto_0

    .line 244
    :cond_f
    iget-object v2, p1, Lcom/twitter/model/av/Audio;->i:Ljava/lang/String;

    if-eqz v2, :cond_6

    goto/16 :goto_0

    .line 246
    :cond_10
    iget-object v2, p1, Lcom/twitter/model/av/Audio;->d:Ljava/util/Map;

    if-eqz v2, :cond_7

    goto/16 :goto_0

    .line 249
    :cond_11
    iget-object v2, p1, Lcom/twitter/model/av/Audio;->h:Ljava/lang/String;

    if-eqz v2, :cond_8

    goto/16 :goto_0

    .line 251
    :cond_12
    iget-object v2, p1, Lcom/twitter/model/av/Audio;->a:Ljava/lang/String;

    if-eqz v2, :cond_9

    goto/16 :goto_0

    .line 252
    :cond_13
    iget-object v2, p1, Lcom/twitter/model/av/Audio;->g:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_1
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    return v0
.end method

.method public g()Lcom/twitter/model/av/a;
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x4

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 257
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 258
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 259
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 260
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->d:Ljava/util/Map;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 261
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 262
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->f:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 263
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->g:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    .line 264
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->h:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/twitter/model/av/Audio;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    .line 265
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/model/av/Audio;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/model/av/Audio;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 266
    return v0

    :cond_1
    move v0, v1

    .line 257
    goto :goto_0

    :cond_2
    move v0, v1

    .line 258
    goto :goto_1

    :cond_3
    move v0, v1

    .line 259
    goto :goto_2

    :cond_4
    move v0, v1

    .line 260
    goto :goto_3

    :cond_5
    move v0, v1

    .line 261
    goto :goto_4

    :cond_6
    move v0, v1

    .line 262
    goto :goto_5

    :cond_7
    move v0, v1

    .line 263
    goto :goto_6

    :cond_8
    move v0, v1

    .line 264
    goto :goto_7
.end method

.method public i()J
    .locals 2

    .prologue
    .line 219
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->b:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->c:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->e:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->f:Ljava/lang/String;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->i:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/twitter/model/av/Audio;->d:Ljava/util/Map;

    invoke-static {p1, v0}, Lcom/twitter/util/l;->b(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 102
    return-void
.end method
