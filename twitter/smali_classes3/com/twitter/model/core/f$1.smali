.class final Lcom/twitter/model/core/f$1;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/model/core/f;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/core/f",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/util/serialization/l;


# direct methods
.method constructor <init>(Lcom/twitter/util/serialization/l;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/twitter/model/core/f$1;->a:Lcom/twitter/util/serialization/l;

    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/core/f;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "I)",
            "Lcom/twitter/model/core/f",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 251
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v2

    .line 252
    new-instance v3, Lcom/twitter/model/core/f$b;

    invoke-direct {v3}, Lcom/twitter/model/core/f$b;-><init>()V

    .line 253
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 254
    iget-object v0, p0, Lcom/twitter/model/core/f$1;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d;

    invoke-virtual {v3, v0}, Lcom/twitter/model/core/f$b;->a(Lcom/twitter/model/core/d;)Lcom/twitter/model/core/f$a;

    .line 253
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 256
    :cond_0
    invoke-virtual {v3}, Lcom/twitter/model/core/f$b;->g()Lcom/twitter/model/core/f;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/f;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "Lcom/twitter/model/core/f",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    invoke-virtual {p2}, Lcom/twitter/model/core/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 242
    invoke-virtual {p2}, Lcom/twitter/model/core/f;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d;

    .line 243
    iget-object v2, p0, Lcom/twitter/model/core/f$1;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v2, p1, v0}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    goto :goto_0

    .line 245
    :cond_0
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 237
    check-cast p2, Lcom/twitter/model/core/f;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/f$1;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/f;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 237
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/f$1;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/core/f;

    move-result-object v0

    return-object v0
.end method
