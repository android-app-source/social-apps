.class public final Lcom/twitter/model/core/f$b;
.super Lcom/twitter/model/core/f$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/model/core/d;",
        ">",
        "Lcom/twitter/model/core/f$a",
        "<TT;",
        "Lcom/twitter/model/core/f",
        "<TT;>;",
        "Lcom/twitter/model/core/f$b",
        "<TT;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0}, Lcom/twitter/model/core/f$a;-><init>()V

    .line 217
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0, p1}, Lcom/twitter/model/core/f$a;-><init>(I)V

    .line 221
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/f",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 224
    invoke-direct {p0, p1}, Lcom/twitter/model/core/f$a;-><init>(Lcom/twitter/model/core/f;)V

    .line 225
    return-void
.end method


# virtual methods
.method protected a(Ljava/util/List;)Lcom/twitter/model/core/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)",
            "Lcom/twitter/model/core/f",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 230
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/model/core/f;

    invoke-direct {v0, p1}, Lcom/twitter/model/core/f;-><init>(Ljava/util/List;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/model/core/f;->a()Lcom/twitter/model/core/f;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic e()Z
    .locals 1

    .prologue
    .line 215
    invoke-super {p0}, Lcom/twitter/model/core/f$a;->e()Z

    move-result v0

    return v0
.end method
