.class public final Lcom/twitter/model/core/ac$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/ac;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/core/ac;",
        ">;"
    }
.end annotation


# instance fields
.field A:Lcom/twitter/model/geo/b;

.field B:Lcom/twitter/model/geo/TwitterPlace;

.field C:Lcom/twitter/model/core/ac;

.field D:J

.field E:Z

.field F:Lcax;

.field G:Lcom/twitter/model/search/e;

.field H:Lcbc;

.field I:I

.field J:Z

.field K:J

.field L:I

.field a:J

.field b:Lcom/twitter/model/core/TwitterUser;

.field c:Lcom/twitter/model/core/TwitterUser;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Lcom/twitter/model/core/v;

.field h:J

.field i:Ljava/lang/String;

.field j:J

.field k:J

.field l:Ljava/lang/String;

.field m:Z

.field n:J

.field o:Lcom/twitter/model/core/ac;

.field p:Z

.field q:I

.field r:I

.field s:J

.field t:I

.field u:Ljava/lang/String;

.field v:Ljava/lang/String;

.field w:Z

.field x:Z

.field y:Z

.field z:Lcgi;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 235
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 194
    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->a:J

    .line 205
    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->j:J

    .line 206
    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->k:J

    .line 209
    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->n:J

    .line 214
    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->s:J

    .line 230
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/model/core/ac$a;->I:I

    .line 236
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/ac;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 238
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 194
    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->a:J

    .line 205
    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->j:J

    .line 206
    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->k:J

    .line 209
    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->n:J

    .line 214
    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->s:J

    .line 230
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/model/core/ac$a;->I:I

    .line 239
    iget-wide v0, p1, Lcom/twitter/model/core/ac;->a:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->a:J

    .line 240
    iget-object v0, p1, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->c:Lcom/twitter/model/core/TwitterUser;

    .line 241
    iget-object v0, p1, Lcom/twitter/model/core/ac;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->f:Ljava/lang/String;

    .line 242
    iget-object v0, p1, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->g:Lcom/twitter/model/core/v;

    .line 243
    iget-wide v0, p1, Lcom/twitter/model/core/ac;->g:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->h:J

    .line 244
    iget-object v0, p1, Lcom/twitter/model/core/ac;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->i:Ljava/lang/String;

    .line 245
    iget-wide v0, p1, Lcom/twitter/model/core/ac;->i:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->j:J

    .line 246
    iget-wide v0, p1, Lcom/twitter/model/core/ac;->j:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->k:J

    .line 247
    iget-object v0, p1, Lcom/twitter/model/core/ac;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->l:Ljava/lang/String;

    .line 248
    iget-boolean v0, p1, Lcom/twitter/model/core/ac;->l:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac$a;->m:Z

    .line 249
    iget-wide v0, p1, Lcom/twitter/model/core/ac;->m:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->n:J

    .line 250
    iget-object v0, p1, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->o:Lcom/twitter/model/core/ac;

    .line 251
    iget-boolean v0, p1, Lcom/twitter/model/core/ac;->G:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac$a;->p:Z

    .line 252
    iget v0, p1, Lcom/twitter/model/core/ac;->o:I

    iput v0, p0, Lcom/twitter/model/core/ac$a;->q:I

    .line 253
    iget v0, p1, Lcom/twitter/model/core/ac;->H:I

    iput v0, p0, Lcom/twitter/model/core/ac$a;->r:I

    .line 254
    iget-wide v0, p1, Lcom/twitter/model/core/ac;->p:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->s:J

    .line 255
    iget v0, p1, Lcom/twitter/model/core/ac;->q:I

    iput v0, p0, Lcom/twitter/model/core/ac$a;->t:I

    .line 256
    iget-object v0, p1, Lcom/twitter/model/core/ac;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->u:Ljava/lang/String;

    .line 257
    iget-object v0, p1, Lcom/twitter/model/core/ac;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->v:Ljava/lang/String;

    .line 258
    iget-boolean v0, p1, Lcom/twitter/model/core/ac;->s:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac$a;->w:Z

    .line 259
    iget-boolean v0, p1, Lcom/twitter/model/core/ac;->t:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac$a;->x:Z

    .line 260
    iget-boolean v0, p1, Lcom/twitter/model/core/ac;->u:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac$a;->y:Z

    .line 261
    iget-object v0, p1, Lcom/twitter/model/core/ac;->x:Lcom/twitter/model/geo/b;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->A:Lcom/twitter/model/geo/b;

    .line 262
    iget-object v0, p1, Lcom/twitter/model/core/ac;->w:Lcgi;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->z:Lcgi;

    .line 263
    iget-object v0, p1, Lcom/twitter/model/core/ac;->y:Lcom/twitter/model/geo/TwitterPlace;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->B:Lcom/twitter/model/geo/TwitterPlace;

    .line 264
    iget-object v0, p1, Lcom/twitter/model/core/ac;->I:Lcom/twitter/model/core/ac;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->C:Lcom/twitter/model/core/ac;

    .line 265
    iget-wide v0, p1, Lcom/twitter/model/core/ac;->J:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->D:J

    .line 266
    iget-boolean v0, p1, Lcom/twitter/model/core/ac;->K:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac$a;->E:Z

    .line 267
    iget-object v0, p1, Lcom/twitter/model/core/ac;->z:Lcax;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->F:Lcax;

    .line 268
    iget-object v0, p1, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->G:Lcom/twitter/model/search/e;

    .line 269
    iget-object v0, p1, Lcom/twitter/model/core/ac;->A:Lcbc;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->H:Lcbc;

    .line 270
    iget v0, p1, Lcom/twitter/model/core/ac;->B:I

    iput v0, p0, Lcom/twitter/model/core/ac$a;->I:I

    .line 271
    iget-boolean v0, p1, Lcom/twitter/model/core/ac;->C:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac$a;->J:Z

    .line 272
    iget-wide v0, p1, Lcom/twitter/model/core/ac;->D:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->K:J

    .line 273
    iget v0, p1, Lcom/twitter/model/core/ac;->E:I

    iput v0, p0, Lcom/twitter/model/core/ac$a;->L:I

    .line 274
    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 4

    .prologue
    .line 609
    iget-wide v0, p0, Lcom/twitter/model/core/ac$a;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->c:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->o:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected T_()Z
    .locals 4

    .prologue
    .line 614
    invoke-super {p0}, Lcom/twitter/util/object/i;->T_()Z

    move-result v0

    if-nez v0, :cond_3

    .line 615
    iget-wide v0, p0, Lcom/twitter/model/core/ac$a;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 616
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Tried to build tweet with an invalid id."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 623
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 625
    :goto_1
    return v0

    .line 617
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->c:Lcom/twitter/model/core/TwitterUser;

    if-nez v0, :cond_2

    .line 618
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Tried to build tweet with an invalid user."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 619
    :cond_2
    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->o:Lcom/twitter/model/core/ac;

    if-nez v0, :cond_0

    .line 620
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Tried to build a tweet that indicated it was a Retweet but did not contain a retweeted status."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 625
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public a(I)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 457
    iput p1, p0, Lcom/twitter/model/core/ac$a;->q:I

    .line 458
    return-object p0
.end method

.method public a(J)Lcom/twitter/model/core/ac$a;
    .locals 1

    .prologue
    .line 282
    iput-wide p1, p0, Lcom/twitter/model/core/ac$a;->a:J

    .line 283
    return-object p0
.end method

.method public a(Lcax;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 568
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->F:Lcax;

    .line 569
    return-object p0
.end method

.method public a(Lcbc;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->H:Lcbc;

    .line 312
    return-object p0
.end method

.method public a(Lcgi;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 516
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->z:Lcgi;

    .line 517
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->b:Lcom/twitter/model/core/TwitterUser;

    .line 296
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 445
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->o:Lcom/twitter/model/core/ac;

    .line 446
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 373
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->g:Lcom/twitter/model/core/v;

    .line 374
    return-object p0
.end method

.method public a(Lcom/twitter/model/geo/TwitterPlace;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->B:Lcom/twitter/model/geo/TwitterPlace;

    .line 529
    return-object p0
.end method

.method public a(Lcom/twitter/model/geo/b;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 522
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->A:Lcom/twitter/model/geo/b;

    .line 523
    return-object p0
.end method

.method public a(Lcom/twitter/model/search/e;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 579
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->G:Lcom/twitter/model/search/e;

    .line 580
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->d:Ljava/lang/String;

    .line 338
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 427
    iput-boolean p1, p0, Lcom/twitter/model/core/ac$a;->m:Z

    .line 428
    return-object p0
.end method

.method public b(I)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 463
    iput p1, p0, Lcom/twitter/model/core/ac$a;->r:I

    .line 464
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/core/ac$a;
    .locals 1

    .prologue
    .line 384
    iput-wide p1, p0, Lcom/twitter/model/core/ac$a;->h:J

    .line 385
    return-object p0
.end method

.method public b(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->c:Lcom/twitter/model/core/TwitterUser;

    .line 318
    return-object p0
.end method

.method public b(Lcom/twitter/model/core/ac;)Lcom/twitter/model/core/ac$a;
    .locals 2

    .prologue
    .line 534
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->C:Lcom/twitter/model/core/ac;

    .line 535
    if-eqz p1, :cond_0

    .line 536
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/model/core/ac$a;->E:Z

    .line 537
    iget-wide v0, p1, Lcom/twitter/model/core/ac;->a:J

    invoke-virtual {p0, v0, v1}, Lcom/twitter/model/core/ac$a;->g(J)Lcom/twitter/model/core/ac$a;

    .line 539
    :cond_0
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->e:Ljava/lang/String;

    .line 352
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 451
    iput-boolean p1, p0, Lcom/twitter/model/core/ac$a;->p:Z

    .line 452
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/twitter/model/core/ac$a;->j()Lcom/twitter/model/core/ac;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 475
    iput p1, p0, Lcom/twitter/model/core/ac$a;->t:I

    .line 476
    return-object p0
.end method

.method public c(J)Lcom/twitter/model/core/ac$a;
    .locals 1

    .prologue
    .line 396
    iput-wide p1, p0, Lcom/twitter/model/core/ac$a;->j:J

    .line 397
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->f:Ljava/lang/String;

    .line 363
    return-object p0
.end method

.method public c(Z)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 493
    iput-boolean p1, p0, Lcom/twitter/model/core/ac$a;->w:Z

    .line 494
    return-object p0
.end method

.method public d(I)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 585
    iput p1, p0, Lcom/twitter/model/core/ac$a;->I:I

    .line 586
    return-object p0
.end method

.method public d(J)Lcom/twitter/model/core/ac$a;
    .locals 1

    .prologue
    .line 410
    iput-wide p1, p0, Lcom/twitter/model/core/ac$a;->k:J

    .line 411
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->i:Ljava/lang/String;

    .line 391
    return-object p0
.end method

.method public d(Z)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 499
    iput-boolean p1, p0, Lcom/twitter/model/core/ac$a;->x:Z

    .line 500
    return-object p0
.end method

.method public e()Lcbc;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->H:Lcbc;

    return-object v0
.end method

.method public e(I)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 603
    iput p1, p0, Lcom/twitter/model/core/ac$a;->L:I

    .line 604
    return-object p0
.end method

.method public e(J)Lcom/twitter/model/core/ac$a;
    .locals 1

    .prologue
    .line 439
    iput-wide p1, p0, Lcom/twitter/model/core/ac$a;->n:J

    .line 440
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 416
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->l:Ljava/lang/String;

    .line 417
    return-object p0
.end method

.method public e(Z)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 505
    iput-boolean p1, p0, Lcom/twitter/model/core/ac$a;->y:Z

    .line 506
    return-object p0
.end method

.method public f(J)Lcom/twitter/model/core/ac$a;
    .locals 1

    .prologue
    .line 469
    iput-wide p1, p0, Lcom/twitter/model/core/ac$a;->s:J

    .line 470
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 481
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->u:Ljava/lang/String;

    .line 482
    return-object p0
.end method

.method public f(Z)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 557
    iput-boolean p1, p0, Lcom/twitter/model/core/ac$a;->E:Z

    .line 558
    return-object p0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->c:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->c:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public g(J)Lcom/twitter/model/core/ac$a;
    .locals 3

    .prologue
    .line 548
    iput-wide p1, p0, Lcom/twitter/model/core/ac$a;->D:J

    .line 549
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 550
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/model/core/ac$a;->E:Z

    .line 552
    :cond_0
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 487
    iput-object p1, p0, Lcom/twitter/model/core/ac$a;->v:Ljava/lang/String;

    .line 488
    return-object p0
.end method

.method public g(Z)Lcom/twitter/model/core/ac$a;
    .locals 0

    .prologue
    .line 591
    iput-boolean p1, p0, Lcom/twitter/model/core/ac$a;->J:Z

    .line 592
    return-object p0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->o:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->o:Lcom/twitter/model/core/ac;

    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public h()Lcgi;
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->z:Lcgi;

    return-object v0
.end method

.method public h(J)Lcom/twitter/model/core/ac$a;
    .locals 1

    .prologue
    .line 597
    iput-wide p1, p0, Lcom/twitter/model/core/ac$a;->K:J

    .line 598
    return-object p0
.end method

.method public i()J
    .locals 2

    .prologue
    .line 543
    iget-wide v0, p0, Lcom/twitter/model/core/ac$a;->D:J

    return-wide v0
.end method

.method protected j()Lcom/twitter/model/core/ac;
    .locals 4

    .prologue
    .line 633
    iget-wide v0, p0, Lcom/twitter/model/core/ac$a;->n:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->b:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->o:Lcom/twitter/model/core/ac;

    if-nez v0, :cond_0

    .line 634
    new-instance v0, Lcom/twitter/model/core/ac;

    invoke-direct {v0, p0}, Lcom/twitter/model/core/ac;-><init>(Lcom/twitter/model/core/ac$a;)V

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->o:Lcom/twitter/model/core/ac;

    .line 635
    iget-wide v0, p0, Lcom/twitter/model/core/ac$a;->n:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac$a;->a:J

    .line 636
    iget-object v0, p0, Lcom/twitter/model/core/ac$a;->b:Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/model/core/ac$a;->c:Lcom/twitter/model/core/TwitterUser;

    .line 638
    :cond_0
    new-instance v0, Lcom/twitter/model/core/ac;

    invoke-direct {v0, p0}, Lcom/twitter/model/core/ac;-><init>(Lcom/twitter/model/core/ac$a;)V

    return-object v0
.end method
