.class public Lcom/twitter/model/core/y;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/y$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/core/y;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:J

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/twitter/model/core/y$a;

    invoke-direct {v0}, Lcom/twitter/model/core/y$a;-><init>()V

    sput-object v0, Lcom/twitter/model/core/y;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;JLjava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput p1, p0, Lcom/twitter/model/core/y;->b:I

    .line 31
    iput-object p2, p0, Lcom/twitter/model/core/y;->c:Ljava/lang/String;

    .line 32
    iput-wide p3, p0, Lcom/twitter/model/core/y;->d:J

    .line 33
    iput-object p5, p0, Lcom/twitter/model/core/y;->e:Ljava/lang/String;

    .line 34
    iput p6, p0, Lcom/twitter/model/core/y;->f:I

    .line 35
    iput-object p7, p0, Lcom/twitter/model/core/y;->g:Ljava/lang/String;

    .line 36
    return-void
.end method
