.class public Lcom/twitter/model/core/q;
.super Lcom/twitter/model/core/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/q$b;,
        Lcom/twitter/model/core/q$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:J

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/twitter/model/core/q$b;

    invoke-direct {v0}, Lcom/twitter/model/core/q$b;-><init>()V

    sput-object v0, Lcom/twitter/model/core/q;->a:Lcom/twitter/util/serialization/l;

    .line 15
    sget-object v0, Lcom/twitter/model/core/q;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/model/core/f;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/core/q;->b:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method constructor <init>(Lcom/twitter/model/core/q$a;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/twitter/model/core/d;-><init>(Lcom/twitter/model/core/d$a;)V

    .line 27
    iget-wide v0, p1, Lcom/twitter/model/core/q$a;->a:J

    iput-wide v0, p0, Lcom/twitter/model/core/q;->c:J

    .line 28
    iget-object v0, p1, Lcom/twitter/model/core/q$a;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/q;->j:Ljava/lang/String;

    .line 29
    iget-object v0, p1, Lcom/twitter/model/core/q$a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/q;->k:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/core/q$a;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/model/core/q$a;

    invoke-direct {v0, p0}, Lcom/twitter/model/core/q$a;-><init>(Lcom/twitter/model/core/q;)V

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/q;)Z
    .locals 4

    .prologue
    .line 44
    if-eq p0, p1, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/model/core/d;->a(Lcom/twitter/model/core/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/q;->c:J

    iget-wide v2, p1, Lcom/twitter/model/core/q;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic bt_()Lcom/twitter/model/core/d$a;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/model/core/q;->a()Lcom/twitter/model/core/q$a;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/model/core/q;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/q;->k:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/model/core/q;->j:Ljava/lang/String;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 40
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/core/q;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/core/q;

    invoke-virtual {p0, p1}, Lcom/twitter/model/core/q;->a(Lcom/twitter/model/core/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 49
    invoke-super {p0}, Lcom/twitter/model/core/d;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/model/core/q;->c:J

    iget-wide v4, p0, Lcom/twitter/model/core/q;->c:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    return v0
.end method
