.class public abstract Lcom/twitter/model/core/ad$b;
.super Lcom/twitter/model/core/d$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/twitter/model/core/ad;",
        "B:",
        "Lcom/twitter/model/core/ad$a",
        "<TE;TB;>;>",
        "Lcom/twitter/model/core/d$b",
        "<TE;TB;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/model/core/d$b;-><init>(I)V

    .line 183
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/ad$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TB;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 200
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/model/core/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V

    .line 201
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/core/ad$a;->e(Ljava/lang/String;)Lcom/twitter/model/core/ad$a;

    move-result-object v0

    .line 202
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/ad$a;->f(Ljava/lang/String;)Lcom/twitter/model/core/ad$a;

    move-result-object v0

    .line 203
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/ad$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/ad$a;

    move-result-object v0

    .line 204
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/ad$a;->d(I)Lcom/twitter/model/core/ad$a;

    move-result-object v0

    .line 205
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/ad$a;->e(I)Lcom/twitter/model/core/ad$a;

    .line 206
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 179
    check-cast p2, Lcom/twitter/model/core/ad$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/ad$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/ad$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 179
    check-cast p2, Lcom/twitter/model/core/ad$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/ad$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/ad$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/ad;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "TE;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 188
    invoke-super {p0, p1, p2}, Lcom/twitter/model/core/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V

    .line 189
    iget-object v0, p2, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    .line 190
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    .line 191
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/ad;->H:I

    .line 192
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/ad;->I:I

    .line 193
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 194
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    check-cast p2, Lcom/twitter/model/core/ad;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/ad$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/ad;)V

    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    check-cast p2, Lcom/twitter/model/core/ad;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/ad$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/ad;)V

    return-void
.end method
