.class Lcom/twitter/model/core/y$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/core/y;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/i;-><init>(I)V

    .line 43
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/core/y;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v2

    .line 62
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v3

    .line 63
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v4

    .line 64
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v6

    .line 65
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v7

    .line 66
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v8

    .line 67
    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    .line 68
    invoke-static {p1}, Lcom/twitter/util/serialization/k;->b(Lcom/twitter/util/serialization/n;)V

    .line 70
    :cond_0
    new-instance v1, Lcom/twitter/model/core/y;

    invoke-direct/range {v1 .. v8}, Lcom/twitter/model/core/y;-><init>(ILjava/lang/String;JLjava/lang/String;ILjava/lang/String;)V

    return-object v1
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/y;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget v0, p2, Lcom/twitter/model/core/y;->b:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/y;->c:Ljava/lang/String;

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/core/y;->d:J

    .line 50
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/y;->e:Ljava/lang/String;

    .line 51
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/y;->f:I

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/y;->g:Ljava/lang/String;

    .line 53
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 54
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    check-cast p2, Lcom/twitter/model/core/y;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/y$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/y;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 38
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/y$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/core/y;

    move-result-object v0

    return-object v0
.end method
