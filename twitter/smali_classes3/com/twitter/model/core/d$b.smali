.class public abstract Lcom/twitter/model/core/d$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/twitter/model/core/d;",
        "B:",
        "Lcom/twitter/model/core/d$a",
        "<TE;TB;>;>",
        "Lcom/twitter/util/serialization/b",
        "<TE;TB;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>(I)V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/twitter/util/serialization/b;-><init>(I)V

    .line 144
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TB;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 156
    const/4 v0, 0x2

    if-lt p3, v0, :cond_0

    .line 157
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/core/d$a;->c(I)Lcom/twitter/model/core/d$a;

    .line 159
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/core/d$a;->a(I)Lcom/twitter/model/core/d$a;

    move-result-object v0

    .line 160
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/d$a;->b(I)Lcom/twitter/model/core/d$a;

    .line 161
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 137
    check-cast p2, Lcom/twitter/model/core/d$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "TE;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    iget v0, p2, Lcom/twitter/model/core/d;->f:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/d;->g:I

    .line 149
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/d;->h:I

    .line 150
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 151
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    check-cast p2, Lcom/twitter/model/core/d;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V

    return-void
.end method
