.class public Lcom/twitter/model/core/TwitterSocialProof;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/TwitterSocialProof$b;,
        Lcom/twitter/model/core/TwitterSocialProof$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/model/core/TwitterSocialProof;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/core/TwitterSocialProof;",
            "Lcom/twitter/model/core/TwitterSocialProof$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:Ljava/lang/String;

.field public final i:I

.field public final j:Ljava/lang/String;

.field public final k:I

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/model/core/TwitterSocialProof$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$b;-><init>(Lcom/twitter/model/core/TwitterSocialProof$1;)V

    sput-object v0, Lcom/twitter/model/core/TwitterSocialProof;->a:Lcom/twitter/util/serialization/b;

    .line 24
    new-instance v0, Lcom/twitter/model/core/TwitterSocialProof$1;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterSocialProof$1;-><init>()V

    sput-object v0, Lcom/twitter/model/core/TwitterSocialProof;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->b:I

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->c:Ljava/lang/String;

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->d:I

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->e:I

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->f:I

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->g:I

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->h:Ljava/lang/String;

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->i:I

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->j:Ljava/lang/String;

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->k:I

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->l:Ljava/lang/String;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->m:Ljava/lang/String;

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->n:Ljava/util/List;

    .line 111
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/TwitterSocialProof$a;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->a:I

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->b:I

    .line 81
    iget-object v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->c:Ljava/lang/String;

    .line 82
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->b:I

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->d:I

    .line 83
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->c:I

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->e:I

    .line 84
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->d:I

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->f:I

    .line 85
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->f:I

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->g:I

    .line 86
    iget-object v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->h:Ljava/lang/String;

    .line 87
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->h:I

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->i:I

    .line 88
    iget-object v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->j:Ljava/lang/String;

    .line 89
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->j:I

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->k:I

    .line 90
    iget-object v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->l:Ljava/lang/String;

    .line 91
    iget-object v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->m:Ljava/lang/String;

    .line 92
    iget-object v0, p1, Lcom/twitter/model/core/TwitterSocialProof$a;->m:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->n:Ljava/util/List;

    .line 93
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/TwitterSocialProof;)Z
    .locals 2

    .prologue
    .line 142
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->b:I

    iget v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->b:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->c:Ljava/lang/String;

    .line 145
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->d:I

    iget v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->d:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->f:I

    iget v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->f:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->e:I

    iget v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->e:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->g:I

    iget v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->g:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->h:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->h:Ljava/lang/String;

    .line 150
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->i:I

    iget v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->i:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->j:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->j:Ljava/lang/String;

    .line 152
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->k:I

    iget v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->k:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->l:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->l:Ljava/lang/String;

    .line 154
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->m:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->m:Ljava/lang/String;

    .line 155
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->n:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->n:Ljava/util/List;

    .line 156
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 142
    :goto_0
    return v0

    .line 156
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 137
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/core/TwitterSocialProof;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/core/TwitterSocialProof;

    invoke-virtual {p0, p1}, Lcom/twitter/model/core/TwitterSocialProof;->a(Lcom/twitter/model/core/TwitterSocialProof;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 161
    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->b:I

    .line 162
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/core/TwitterSocialProof;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/core/TwitterSocialProof;->d:I

    add-int/2addr v0, v1

    .line 164
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/core/TwitterSocialProof;->e:I

    add-int/2addr v0, v1

    .line 165
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/core/TwitterSocialProof;->f:I

    add-int/2addr v0, v1

    .line 166
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/core/TwitterSocialProof;->g:I

    add-int/2addr v0, v1

    .line 167
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/core/TwitterSocialProof;->h:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/core/TwitterSocialProof;->i:I

    add-int/2addr v0, v1

    .line 169
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/core/TwitterSocialProof;->j:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/core/TwitterSocialProof;->k:I

    add-int/2addr v0, v1

    .line 171
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/core/TwitterSocialProof;->l:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/core/TwitterSocialProof;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 173
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/core/TwitterSocialProof;->n:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 116
    iget-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 117
    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 118
    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 120
    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 121
    iget-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122
    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    iget-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 124
    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 125
    iget-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/twitter/model/core/TwitterSocialProof;->n:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 128
    return-void
.end method
