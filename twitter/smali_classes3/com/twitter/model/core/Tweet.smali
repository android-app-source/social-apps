.class public Lcom/twitter/model/core/Tweet;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/twitter/model/core/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/Tweet$b;,
        Lcom/twitter/model/core/Tweet$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field private static final ah:Lcok$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcok$b",
            "<",
            "Lcom/twitter/model/core/Tweet$b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Ljava/lang/String;

.field public final B:Ljava/lang/String;

.field public final C:J

.field public final D:Ljava/lang/String;

.field public final E:J

.field public final F:Z

.field public final G:J

.field public final H:Z

.field public final I:D

.field public final J:D

.field public final K:Z

.field public final L:Z

.field public final M:Lcom/twitter/model/geo/TwitterPlace;

.field public final N:I

.field public final O:J

.field public final P:I

.field public final Q:I

.field public final R:Ljava/lang/String;

.field public final S:Ljava/lang/String;

.field public final T:Z

.field public final U:[Lcom/twitter/model/core/d;

.field public final V:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/media/EditableMedia;",
            ">;"
        }
    .end annotation
.end field

.field public final W:I

.field public final X:I

.field public final Y:I

.field public final Z:Ljava/lang/String;

.field public a:Z

.field public final aa:I

.field public final ab:Z

.field public final ac:J

.field public final ad:Lcom/twitter/model/timeline/r;

.field public final ae:I

.field public final af:J

.field public final ag:J

.field private final ai:Lcgi;

.field private final aj:Lcax;

.field private final ak:Lcaq;

.field private final al:Lcom/twitter/model/core/v;

.field private final am:Ljava/lang/String;

.field private final an:Ljava/lang/String;

.field private final ao:I

.field private final ap:J

.field private aq:Z

.field private final ar:I

.field private final as:Ljava/lang/String;

.field public b:J

.field public c:Z

.field public d:I

.field public e:Z

.field public f:I

.field public g:Ljava/lang/String;

.field public final h:I

.field public final i:Ljava/lang/String;

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:I

.field public l:I

.field public m:J

.field public n:I

.field public o:I

.field public final p:Ljava/lang/String;

.field public final q:J

.field public final r:Ljava/lang/String;

.field public final s:J

.field public final t:J

.field public final u:J

.field public final v:Ljava/lang/String;

.field public final w:Lcom/twitter/model/core/r;

.field public final x:J

.field public final y:Ljava/lang/Long;

.field public final z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/twitter/model/core/Tweet$1;

    invoke-direct {v0}, Lcom/twitter/model/core/Tweet$1;-><init>()V

    sput-object v0, Lcom/twitter/model/core/Tweet;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 60
    invoke-static {}, Lcom/twitter/model/core/Tweet$b;->a()Lcom/twitter/util/object/j;

    move-result-object v0

    invoke-static {v0}, Lcok;->a(Lcom/twitter/util/object/j;)Lcok$b;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/core/Tweet;->ah:Lcok$b;

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    .line 230
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->G:J

    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->t:J

    .line 232
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->u:J

    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->b:J

    .line 234
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->p:Ljava/lang/String;

    .line 235
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->z:Ljava/lang/String;

    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->s:J

    .line 237
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    .line 238
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    .line 239
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->am:Ljava/lang/String;

    .line 240
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->an:Ljava/lang/String;

    .line 241
    sget-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    .line 242
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->q:J

    .line 243
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->B:Ljava/lang/String;

    .line 244
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->C:J

    .line 245
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->D:Ljava/lang/String;

    .line 246
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->E:J

    .line 247
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    .line 248
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->aq:Z

    .line 249
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->c:Z

    .line 250
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->K:Z

    .line 251
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->ab:Z

    .line 252
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->a:Z

    .line 253
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->n:I

    .line 254
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->k:I

    .line 255
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->ac:J

    .line 256
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->l:I

    .line 257
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->m:J

    .line 258
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->R:Ljava/lang/String;

    .line 259
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->S:Ljava/lang/String;

    .line 261
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    .line 263
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->d:I

    .line 265
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->P:I

    .line 266
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->Q:I

    .line 267
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->F:Z

    .line 268
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->L:Z

    .line 269
    sget-object v0, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->ai:Lcgi;

    .line 270
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->H:Z

    .line 271
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->I:D

    .line 272
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->J:D

    .line 273
    sget-object v0, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/TwitterPlace;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    .line 274
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->T:Z

    .line 275
    sget-object v0, Lcom/twitter/model/core/r;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/r;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    .line 276
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/twitter/model/core/Tweet;->x:J

    .line 277
    sget-object v0, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    .line 278
    sget-object v0, Lcaq;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcaq;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->ak:Lcaq;

    .line 279
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 280
    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_9

    move-object v0, v3

    :goto_9
    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    .line 281
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->V:Ljava/util/List;

    .line 282
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->O:J

    .line 283
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a

    :goto_a
    iput-boolean v1, p0, Lcom/twitter/model/core/Tweet;->e:Z

    .line 285
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->f:I

    .line 286
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->g:Ljava/lang/String;

    .line 287
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->W:I

    .line 288
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->Y:I

    .line 289
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->Z:Ljava/lang/String;

    .line 290
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->X:I

    .line 291
    sget-object v0, Lcom/twitter/model/core/i;->a:Lcom/twitter/util/serialization/l;

    .line 292
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 291
    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 293
    if-nez v0, :cond_b

    .line 294
    :goto_b
    iput-object v3, p0, Lcom/twitter/model/core/Tweet;->U:[Lcom/twitter/model/core/d;

    .line 296
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->aa:I

    .line 297
    sget-object v0, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    .line 298
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->ae:I

    .line 299
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/core/Tweet;->af:J

    .line 301
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->ao:I

    .line 302
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->o:I

    .line 303
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/core/Tweet;->ag:J

    .line 305
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->h:I

    .line 306
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->i:Ljava/lang/String;

    .line 307
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/core/Tweet;->ap:J

    .line 308
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/Tweet;->ar:I

    .line 309
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->as:Ljava/lang/String;

    .line 310
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->j:Ljava/util/List;

    .line 311
    return-void

    :cond_0
    move v0, v2

    .line 248
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 249
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 250
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 251
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 252
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 267
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 268
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 270
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 274
    goto/16 :goto_8

    .line 280
    :cond_9
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_9

    :cond_a
    move v1, v2

    .line 283
    goto/16 :goto_a

    .line 294
    :cond_b
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/twitter/model/core/d;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/model/core/d;

    move-object v3, v0

    goto/16 :goto_b
.end method

.method private constructor <init>(Lcom/twitter/model/core/Tweet$a;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->t:J

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->G:J

    .line 155
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->o:J

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->t:J

    .line 156
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->z:J

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->u:J

    .line 157
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->q:J

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->b:J

    .line 158
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->p:Ljava/lang/String;

    .line 159
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->z:Ljava/lang/String;

    .line 160
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->n:J

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->s:J

    .line 161
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    .line 162
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    .line 163
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->x:Lcom/twitter/model/core/v;

    sget-object v3, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    invoke-static {v0, v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    .line 164
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->f:J

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->q:J

    .line 165
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->B:Ljava/lang/String;

    .line 166
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->h:J

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->C:J

    .line 167
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->D:Ljava/lang/String;

    .line 168
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->j:J

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->E:J

    .line 169
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    .line 170
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet$a;->r:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->aq:Z

    .line 171
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet$a;->s:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->c:Z

    .line 172
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet$a;->y:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->K:Z

    .line 173
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet$a;->ag:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->ab:Z

    .line 174
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet$a;->l:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->a:Z

    .line 175
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->P:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->n:I

    .line 176
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->L:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->k:I

    .line 177
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->ah:J

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->ac:J

    .line 178
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->N:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->l:I

    .line 179
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->O:J

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->m:J

    .line 180
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->Q:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->R:Ljava/lang/String;

    .line 181
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->ai:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->S:Ljava/lang/String;

    .line 182
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->D:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    .line 183
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->E:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->d:I

    .line 184
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->I:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->P:I

    .line 185
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->M:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->Q:I

    .line 186
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet$a;->m:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->F:Z

    .line 187
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet$a;->A:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->L:Z

    .line 188
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->F:Lcgi;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->ai:Lcgi;

    .line 189
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet$a;->u:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->H:Z

    .line 190
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->v:D

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->I:D

    .line 191
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->w:D

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->J:D

    .line 192
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->B:Lcom/twitter/model/geo/TwitterPlace;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    .line 193
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    iget-object v0, v0, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->T:Z

    .line 194
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->S:Lcom/twitter/model/core/r;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    .line 195
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->T:J

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->x:J

    .line 196
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->J:Lcax;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    .line 197
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->K:Lcaq;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->ak:Lcaq;

    .line 198
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->U:Ljava/lang/Long;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    .line 199
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->W:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->V:Ljava/util/List;

    .line 200
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->R:Lcom/twitter/model/timeline/r;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    .line 201
    iget-wide v4, p1, Lcom/twitter/model/core/Tweet$a;->G:J

    iput-wide v4, p0, Lcom/twitter/model/core/Tweet;->O:J

    .line 202
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet$a;->H:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet;->e:Z

    .line 203
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->X:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->f:I

    .line 204
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->Y:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->g:Ljava/lang/String;

    .line 205
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->Z:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->W:I

    .line 206
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->ab:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->Y:I

    .line 207
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->ac:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->Z:Ljava/lang/String;

    .line 208
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->aa:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->X:I

    .line 209
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->V:[Lcom/twitter/model/core/d;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->U:[Lcom/twitter/model/core/d;

    .line 210
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->af:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->aa:I

    .line 211
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v3, p1, Lcom/twitter/model/core/Tweet$a;->b:I

    iget-object v4, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v5, p0, Lcom/twitter/model/core/Tweet;->U:[Lcom/twitter/model/core/d;

    invoke-static {v0, v3, v4, v5, v1}, Lcom/twitter/model/core/Tweet;->a(Ljava/lang/String;ILcom/twitter/model/core/v;[Lcom/twitter/model/core/d;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->am:Ljava/lang/String;

    .line 213
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->C:Ljava/lang/String;

    iget v1, p1, Lcom/twitter/model/core/Tweet$a;->b:I

    iget-object v3, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v4, p0, Lcom/twitter/model/core/Tweet;->U:[Lcom/twitter/model/core/d;

    invoke-static {v0, v1, v3, v4, v2}, Lcom/twitter/model/core/Tweet;->a(Ljava/lang/String;ILcom/twitter/model/core/v;[Lcom/twitter/model/core/d;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->an:Ljava/lang/String;

    .line 215
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->b:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->ae:I

    .line 216
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet$a;->am:J

    iput-wide v0, p0, Lcom/twitter/model/core/Tweet;->af:J

    .line 217
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->aj:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->ao:I

    .line 218
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->ak:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->o:I

    .line 219
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet$a;->al:J

    iput-wide v0, p0, Lcom/twitter/model/core/Tweet;->ag:J

    .line 220
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->ad:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->h:I

    .line 221
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->ae:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->i:Ljava/lang/String;

    .line 222
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet$a;->an:J

    iput-wide v0, p0, Lcom/twitter/model/core/Tweet;->ap:J

    .line 223
    iget v0, p1, Lcom/twitter/model/core/Tweet$a;->ao:I

    iput v0, p0, Lcom/twitter/model/core/Tweet;->ar:I

    .line 224
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->ap:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->as:Ljava/lang/String;

    .line 225
    iget-object v0, p1, Lcom/twitter/model/core/Tweet$a;->aq:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet;->j:Ljava/util/List;

    .line 226
    return-void

    :cond_0
    move v0, v2

    .line 193
    goto/16 :goto_0
.end method

.method synthetic constructor <init>(Lcom/twitter/model/core/Tweet$a;Lcom/twitter/model/core/Tweet$1;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/twitter/model/core/Tweet;-><init>(Lcom/twitter/model/core/Tweet$a;)V

    return-void
.end method

.method public static a(JLjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 768
    const-string/jumbo v0, "https://twitter.com/%1$s/status/%2$s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;ILcom/twitter/model/core/v;[Lcom/twitter/model/core/d;Z)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 319
    if-eqz p4, :cond_1

    iget-boolean v0, p2, Lcom/twitter/model/core/v;->h:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v4, v0

    .line 320
    :goto_0
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 321
    new-instance v0, Lcpb;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Display start is greater than tweet text length"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 324
    :cond_0
    if-eqz p0, :cond_6

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt p1, v0, :cond_6

    if-lez p1, :cond_6

    .line 326
    invoke-virtual {p0, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 327
    if-eqz v4, :cond_2

    .line 329
    invoke-virtual {p2, p1}, Lcom/twitter/model/core/v;->a(I)V

    .line 330
    if-eqz p3, :cond_2

    .line 331
    array-length v2, p3

    move v1, v3

    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v5, p3, v1

    .line 332
    invoke-virtual {v5, p1}, Lcom/twitter/model/core/d;->a(I)V

    .line 331
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v4, v3

    .line 319
    goto :goto_0

    .line 337
    :cond_2
    invoke-static {p0, p2}, Lcom/twitter/model/core/v;->b(Ljava/lang/CharSequence;Lcom/twitter/model/core/v;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 339
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x200f

    invoke-virtual {v1, v3, v0}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 340
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 341
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 342
    if-eqz v4, :cond_3

    .line 343
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz p3, :cond_5

    .line 344
    invoke-static {p3}, Lcom/twitter/util/collection/ImmutableList;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 343
    :goto_2
    invoke-static {v0, p2, v1}, Lcom/twitter/model/util/f;->b(Ljava/util/List;Lcom/twitter/model/core/v;Ljava/lang/Iterable;)V

    :cond_3
    move-object v0, v2

    .line 349
    :cond_4
    :goto_3
    return-object v0

    .line 344
    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    :cond_6
    move-object v0, p0

    .line 349
    goto :goto_3
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 554
    and-int/lit8 v0, p0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static aw()Z
    .locals 1

    .prologue
    .line 1466
    sget-object v0, Lcom/twitter/model/core/Tweet;->ah:Lcok$b;

    invoke-virtual {v0}, Lcok$b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet$b;

    iget-boolean v0, v0, Lcom/twitter/model/core/Tweet$b;->a:Z

    return v0
.end method

.method public static b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 773
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/twitter/model/core/Tweet;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 785
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->N()Lcom/twitter/model/core/MediaEntity;

    move-result-object v1

    .line 786
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->N()Lcom/twitter/model/core/MediaEntity;

    move-result-object v2

    .line 787
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 788
    iget-object v3, v1, Lcom/twitter/model/core/MediaEntity;->q:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget-object v4, v2, Lcom/twitter/model/core/MediaEntity;->q:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 795
    :cond_0
    :goto_0
    return v0

    .line 791
    :cond_1
    iget-object v1, v1, Lcom/twitter/model/core/MediaEntity;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, v2, Lcom/twitter/model/core/MediaEntity;->s:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 795
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public A()Z
    .locals 2

    .prologue
    .line 628
    iget v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public B()Z
    .locals 1

    .prologue
    .line 632
    iget v0, p0, Lcom/twitter/model/core/Tweet;->aa:I

    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->b(I)Z

    move-result v0

    return v0
.end method

.method public C()Z
    .locals 1

    .prologue
    .line 636
    iget v0, p0, Lcom/twitter/model/core/Tweet;->aa:I

    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->d(I)Z

    move-result v0

    return v0
.end method

.method public D()Z
    .locals 1

    .prologue
    .line 643
    iget v0, p0, Lcom/twitter/model/core/Tweet;->aa:I

    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->c(I)Z

    move-result v0

    return v0
.end method

.method public E()Z
    .locals 1

    .prologue
    .line 647
    iget v0, p0, Lcom/twitter/model/core/Tweet;->aa:I

    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->n(I)Z

    move-result v0

    return v0
.end method

.method public F()Z
    .locals 1

    .prologue
    .line 651
    iget v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public G()Z
    .locals 1

    .prologue
    .line 655
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final H()Z
    .locals 2

    .prologue
    .line 659
    iget v0, p0, Lcom/twitter/model/core/Tweet;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->i:Ljava/lang/String;

    .line 660
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 659
    :goto_0
    return v0

    .line 660
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public I()Z
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0}, Lcom/twitter/model/util/c;->f(Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public J()Z
    .locals 1

    .prologue
    .line 674
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0}, Lcom/twitter/model/util/c;->g(Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public K()Z
    .locals 1

    .prologue
    .line 681
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0}, Lcom/twitter/model/util/c;->h(Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public L()Z
    .locals 1

    .prologue
    .line 688
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0}, Lcom/twitter/model/util/c;->i(Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public M()Z
    .locals 1

    .prologue
    .line 692
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->N()Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 693
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public N()Lcom/twitter/model/core/MediaEntity;
    .locals 2

    .prologue
    .line 698
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    sget-object v1, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    invoke-static {v0, v1}, Lcom/twitter/model/util/c;->b(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    return-object v0
.end method

.method public O()Lcom/twitter/model/core/MediaEntity;
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    return-object v0
.end method

.method public P()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 708
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    return-object v0
.end method

.method public Q()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/ad;",
            ">;"
        }
    .end annotation

    .prologue
    .line 713
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    return-object v0
.end method

.method public R()Z
    .locals 1

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v0}, Lcom/twitter/model/core/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public S()Z
    .locals 1

    .prologue
    .line 739
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->aq:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ai:Lcgi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ai:Lcgi;

    iget-boolean v0, v0, Lcgi;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ai:Lcgi;

    .line 740
    invoke-virtual {v0}, Lcgi;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 739
    :goto_0
    return v0

    .line 740
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public T()Z
    .locals 1

    .prologue
    .line 746
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->aq:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ai:Lcgi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ai:Lcgi;

    iget-boolean v0, v0, Lcgi;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public U()Z
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public V()Ljava/lang/String;
    .locals 1

    .prologue
    .line 755
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    .line 758
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "tweet"

    goto :goto_0
.end method

.method public W()Ljava/lang/String;
    .locals 3

    .prologue
    .line 763
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->t:J

    iget-object v2, p0, Lcom/twitter/model/core/Tweet;->p:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/model/core/Tweet;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public X()Z
    .locals 4

    .prologue
    .line 777
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Y()Z
    .locals 2

    .prologue
    .line 781
    iget v0, p0, Lcom/twitter/model/core/Tweet;->ao:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 799
    iget v0, p0, Lcom/twitter/model/core/Tweet;->d:I

    invoke-static {v0}, Lcom/twitter/model/core/Tweet;->a(I)Z

    move-result v0

    return v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 508
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->G:J

    return-wide v0
.end method

.method public a(J)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 718
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 719
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v0}, Lcom/twitter/model/core/k;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 720
    iget-wide v4, v0, Lcom/twitter/model/core/MediaEntity;->j:J

    cmp-long v3, v4, p1

    if-nez v3, :cond_0

    .line 721
    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 724
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 807
    iput-boolean p1, p0, Lcom/twitter/model/core/Tweet;->aq:Z

    .line 808
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Z
    .locals 4

    .prologue
    .line 465
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->G:J

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->G:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->z:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->z:Ljava/lang/String;

    .line 468
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    .line 469
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->a:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/Tweet;->a:Z

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->c:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/Tweet;->c:Z

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->aq:Z

    .line 472
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/Tweet;->W:I

    iget v1, p1, Lcom/twitter/model/core/Tweet;->W:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/Tweet;->Y:I

    iget v1, p1, Lcom/twitter/model/core/Tweet;->Y:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/Tweet;->k:I

    iget v1, p1, Lcom/twitter/model/core/Tweet;->k:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/Tweet;->n:I

    iget v1, p1, Lcom/twitter/model/core/Tweet;->n:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/Tweet;->o:I

    iget v1, p1, Lcom/twitter/model/core/Tweet;->o:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    iget v1, p1, Lcom/twitter/model/core/Tweet;->N:I

    if-ne v0, v1, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->ac:J

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->ac:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/model/core/Tweet;->aa:I

    iget v1, p1, Lcom/twitter/model/core/Tweet;->aa:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/Tweet;->ae:I

    iget v1, p1, Lcom/twitter/model/core/Tweet;->ae:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    .line 484
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    .line 487
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 488
    invoke-direct {p0, p1}, Lcom/twitter/model/core/Tweet;->c(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 465
    :goto_0
    return v0

    .line 488
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)Z
    .locals 4

    .prologue
    .line 602
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->b:J

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->L:J

    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->G:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aa()Z
    .locals 1

    .prologue
    .line 803
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->aq:Z

    return v0
.end method

.method public ab()Lcom/twitter/model/core/v;
    .locals 1

    .prologue
    .line 812
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    return-object v0
.end method

.method public ac()Lcgi;
    .locals 1

    .prologue
    .line 817
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ai:Lcgi;

    return-object v0
.end method

.method public ad()Lcax;
    .locals 1

    .prologue
    .line 822
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    return-object v0
.end method

.method public ae()Lcaq;
    .locals 1

    .prologue
    .line 827
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ak:Lcaq;

    return-object v0
.end method

.method public af()J
    .locals 2

    .prologue
    .line 841
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->s:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->b:J

    goto :goto_0
.end method

.method public ag()Z
    .locals 1

    .prologue
    .line 847
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    invoke-virtual {v0}, Lcax;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ah()Z
    .locals 1

    .prologue
    .line 855
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    invoke-virtual {v0}, Lcax;->p()Z

    move-result v0

    if-nez v0, :cond_1

    .line 856
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v0}, Lcom/twitter/model/core/k;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 855
    :goto_0
    return v0

    .line 856
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ai()Z
    .locals 1

    .prologue
    .line 863
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    invoke-virtual {v0}, Lcax;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aj()Z
    .locals 1

    .prologue
    .line 870
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    invoke-virtual {v0}, Lcax;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ak()Z
    .locals 1

    .prologue
    .line 874
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 875
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcax;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public al()Z
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    invoke-virtual {v0}, Lcax;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public am()Z
    .locals 1

    .prologue
    .line 883
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    invoke-virtual {v0}, Lcax;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public an()Z
    .locals 1

    .prologue
    .line 887
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ao()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->aq()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->as()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ar()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ao()Z
    .locals 1

    .prologue
    .line 891
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    invoke-virtual {v0}, Lcax;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ap()Z
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    invoke-virtual {v0}, Lcax;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aq()Z
    .locals 1

    .prologue
    .line 899
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    invoke-virtual {v0}, Lcax;->D()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    .line 900
    invoke-virtual {v0}, Lcax;->F()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 899
    :goto_0
    return v0

    .line 900
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ar()Z
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    invoke-virtual {v0}, Lcax;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public as()Z
    .locals 1

    .prologue
    .line 908
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    invoke-virtual {v0}, Lcax;->G()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    .line 909
    invoke-virtual {v0}, Lcax;->H()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 908
    :goto_0
    return v0

    .line 909
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public at()Ljava/lang/String;
    .locals 2

    .prologue
    .line 922
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    const-string/jumbo v1, "app_id"

    invoke-virtual {v0, v1}, Lcax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public au()Ljava/lang/String;
    .locals 3

    .prologue
    .line 927
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ap()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 928
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    const-string/jumbo v1, "recipient"

    const-class v2, Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcax;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcax;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 930
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public av()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 934
    .line 935
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ap()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 936
    const/4 v1, 0x1

    .line 937
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->au()Ljava/lang/String;

    move-result-object v2

    .line 938
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->af()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 939
    invoke-static {v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 943
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 514
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 442
    iget v0, p0, Lcom/twitter/model/core/Tweet;->Q:I

    invoke-static {v0}, Lcom/twitter/model/core/ae$a;->f(I)Z

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 433
    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->z:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->p:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->z:Ljava/lang/String;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 457
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->G:J

    check-cast p1, Lcom/twitter/model/core/Tweet;

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->G:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->am:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 503
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->an:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/core/Tweet;->am:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 518
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->ap:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 493
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->G:J

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 522
    iget v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 526
    iget v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->V:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 534
    iget v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 538
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->j()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 546
    iget v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 4

    .prologue
    .line 550
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->C:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 558
    iget v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    const/high16 v1, 0xa0000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 562
    iget v0, p0, Lcom/twitter/model/core/Tweet;->d:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 566
    iget v0, p0, Lcom/twitter/model/core/Tweet;->d:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 570
    iget v0, p0, Lcom/twitter/model/core/Tweet;->d:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 582
    iget v0, p0, Lcom/twitter/model/core/Tweet;->aa:I

    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->i(I)Z

    move-result v0

    return v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 590
    iget v0, p0, Lcom/twitter/model/core/Tweet;->aa:I

    invoke-static {v0}, Lcom/twitter/model/timeline/z$a;->o(I)Z

    move-result v0

    return v0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 594
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "RankedTimelineTweet"

    iget-object v1, p0, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    iget-object v1, v1, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 355
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->G:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 356
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->t:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 357
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->u:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 358
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->b:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 359
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 360
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 361
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 362
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 363
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 364
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->am:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 365
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->an:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->al:Lcom/twitter/model/core/v;

    sget-object v3, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 367
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->q:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 368
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->B:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 369
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->C:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 370
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->D:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 371
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->E:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 372
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 373
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->aq:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 374
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 375
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->K:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 376
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->ab:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 377
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->a:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 378
    iget v0, p0, Lcom/twitter/model/core/Tweet;->n:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 379
    iget v0, p0, Lcom/twitter/model/core/Tweet;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 380
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->ac:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 381
    iget v0, p0, Lcom/twitter/model/core/Tweet;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 382
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->m:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 383
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->R:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->S:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 385
    iget v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 386
    iget v0, p0, Lcom/twitter/model/core/Tweet;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 387
    iget v0, p0, Lcom/twitter/model/core/Tweet;->P:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 388
    iget v0, p0, Lcom/twitter/model/core/Tweet;->Q:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 389
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->F:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 390
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->L:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 391
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ai:Lcgi;

    sget-object v3, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 392
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->H:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 393
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->I:D

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 394
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->J:D

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 395
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->M:Lcom/twitter/model/geo/TwitterPlace;

    sget-object v3, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 396
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->T:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 397
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    sget-object v3, Lcom/twitter/model/core/r;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 398
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->x:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 399
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->aj:Lcax;

    sget-object v3, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 400
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ak:Lcaq;

    sget-object v3, Lcaq;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 401
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    :goto_9
    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 402
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->V:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 403
    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->O:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 404
    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->e:Z

    if-eqz v0, :cond_a

    :goto_a
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 405
    iget v0, p0, Lcom/twitter/model/core/Tweet;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 406
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 407
    iget v0, p0, Lcom/twitter/model/core/Tweet;->W:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 408
    iget v0, p0, Lcom/twitter/model/core/Tweet;->Y:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 409
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->Z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 410
    iget v0, p0, Lcom/twitter/model/core/Tweet;->X:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 411
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->U:[Lcom/twitter/model/core/d;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 413
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/twitter/model/core/i;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 412
    invoke-static {v0}, Lcom/twitter/util/collection/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 414
    sget-object v1, Lcom/twitter/model/core/i;->a:Lcom/twitter/util/serialization/l;

    .line 415
    invoke-static {v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 414
    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 416
    iget v0, p0, Lcom/twitter/model/core/Tweet;->aa:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 417
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    sget-object v1, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 418
    iget v0, p0, Lcom/twitter/model/core/Tweet;->ae:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 419
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->af:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 420
    iget v0, p0, Lcom/twitter/model/core/Tweet;->ao:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 421
    iget v0, p0, Lcom/twitter/model/core/Tweet;->o:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 422
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->ag:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 423
    iget v0, p0, Lcom/twitter/model/core/Tweet;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 424
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 425
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->ap:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 426
    iget v0, p0, Lcom/twitter/model/core/Tweet;->ar:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 427
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->as:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 428
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->j:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 429
    return-void

    :cond_0
    move v0, v2

    .line 373
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 374
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 375
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 376
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 377
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 389
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 390
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 392
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 396
    goto/16 :goto_8

    .line 401
    :cond_9
    const-wide/16 v4, 0x0

    goto/16 :goto_9

    :cond_a
    move v1, v2

    .line 404
    goto/16 :goto_a
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 616
    iget v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()Z
    .locals 1

    .prologue
    .line 620
    iget v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()Z
    .locals 1

    .prologue
    .line 624
    iget v0, p0, Lcom/twitter/model/core/Tweet;->N:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
