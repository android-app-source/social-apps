.class public final Lcom/twitter/model/core/h$a;
.super Lcom/twitter/model/core/d$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/core/d$a",
        "<",
        "Lcom/twitter/model/core/h;",
        "Lcom/twitter/model/core/h$a;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field e:I

.field f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 73
    invoke-direct {p0}, Lcom/twitter/model/core/d$a;-><init>()V

    .line 70
    iput v0, p0, Lcom/twitter/model/core/h$a;->e:I

    .line 71
    iput v0, p0, Lcom/twitter/model/core/h$a;->f:I

    .line 74
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/h;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 77
    invoke-direct {p0, p1}, Lcom/twitter/model/core/d$a;-><init>(Lcom/twitter/model/core/d;)V

    .line 70
    iput v0, p0, Lcom/twitter/model/core/h$a;->e:I

    .line 71
    iput v0, p0, Lcom/twitter/model/core/h$a;->f:I

    .line 78
    iget-object v0, p1, Lcom/twitter/model/core/h;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/h$a;->a:Ljava/lang/String;

    .line 79
    iget v0, p1, Lcom/twitter/model/core/h;->j:I

    iput v0, p0, Lcom/twitter/model/core/h$a;->e:I

    .line 80
    iget v0, p1, Lcom/twitter/model/core/h;->k:I

    iput v0, p0, Lcom/twitter/model/core/h$a;->f:I

    .line 81
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/twitter/model/core/h$a;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/twitter/model/core/h$a;->a:Ljava/lang/String;

    .line 86
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/twitter/model/core/h$a;->e()Lcom/twitter/model/core/h;

    move-result-object v0

    return-object v0
.end method

.method protected c_()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 103
    invoke-super {p0}, Lcom/twitter/model/core/d$a;->c_()V

    .line 104
    iget v0, p0, Lcom/twitter/model/core/h$a;->c:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/twitter/model/core/h$a;->d:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/h$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 106
    iget v0, p0, Lcom/twitter/model/core/h$a;->c:I

    iget-object v1, p0, Lcom/twitter/model/core/h$a;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/model/core/h$a;->d:I

    .line 108
    :cond_0
    iget v0, p0, Lcom/twitter/model/core/h$a;->e:I

    if-eq v0, v2, :cond_1

    iget v0, p0, Lcom/twitter/model/core/h$a;->f:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/h$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 110
    iget v0, p0, Lcom/twitter/model/core/h$a;->e:I

    iget-object v1, p0, Lcom/twitter/model/core/h$a;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/model/core/h$a;->f:I

    .line 112
    :cond_1
    return-void
.end method

.method public d(I)Lcom/twitter/model/core/h$a;
    .locals 0

    .prologue
    .line 91
    iput p1, p0, Lcom/twitter/model/core/h$a;->e:I

    .line 92
    return-object p0
.end method

.method public e(I)Lcom/twitter/model/core/h$a;
    .locals 0

    .prologue
    .line 97
    iput p1, p0, Lcom/twitter/model/core/h$a;->f:I

    .line 98
    return-object p0
.end method

.method protected e()Lcom/twitter/model/core/h;
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcom/twitter/model/core/h;

    invoke-direct {v0, p0}, Lcom/twitter/model/core/h;-><init>(Lcom/twitter/model/core/h$a;)V

    return-object v0
.end method

.method public bridge synthetic f()I
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lcom/twitter/model/core/d$a;->f()I

    move-result v0

    return v0
.end method

.method public bridge synthetic g()I
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lcom/twitter/model/core/d$a;->g()I

    move-result v0

    return v0
.end method
