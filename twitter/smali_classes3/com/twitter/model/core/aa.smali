.class public Lcom/twitter/model/core/aa;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/core/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/aa$a;
    }
.end annotation


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:I

.field public final d:I

.field public final e:J

.field public final f:J

.field public final g:J

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Lcom/twitter/model/core/TwitterUser;


# direct methods
.method public constructor <init>(Lcom/twitter/model/core/aa$a;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->a(Lcom/twitter/model/core/aa$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/core/aa;->a:Z

    .line 41
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->b(Lcom/twitter/model/core/aa$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/core/aa;->b:Z

    .line 42
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->c(Lcom/twitter/model/core/aa$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/aa;->c:I

    .line 43
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->d(Lcom/twitter/model/core/aa$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/aa;->d:I

    .line 44
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->e(Lcom/twitter/model/core/aa$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/core/aa;->e:J

    .line 45
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->f(Lcom/twitter/model/core/aa$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/core/aa;->f:J

    .line 46
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->g(Lcom/twitter/model/core/aa$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/core/aa;->g:J

    .line 47
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->h(Lcom/twitter/model/core/aa$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/aa;->l:Ljava/lang/String;

    .line 48
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->i(Lcom/twitter/model/core/aa$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/aa;->h:Ljava/lang/String;

    .line 49
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->j(Lcom/twitter/model/core/aa$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/aa;->i:Ljava/lang/String;

    .line 50
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->k(Lcom/twitter/model/core/aa$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/aa;->j:Ljava/lang/String;

    .line 51
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->l(Lcom/twitter/model/core/aa$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/aa;->k:Ljava/lang/String;

    .line 52
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->m(Lcom/twitter/model/core/aa$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/aa;->m:Ljava/lang/String;

    .line 53
    invoke-static {p1}, Lcom/twitter/model/core/aa$a;->n(Lcom/twitter/model/core/aa$a;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/aa;->n:Lcom/twitter/model/core/TwitterUser;

    .line 54
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/twitter/model/core/aa;->e:J

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/twitter/model/core/aa;->e:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
