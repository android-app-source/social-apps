.class Lcom/twitter/model/core/s$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/core/s;",
        "Lcom/twitter/model/core/s$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/core/s$1;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/twitter/model/core/s$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/core/s$a;
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lcom/twitter/model/core/s$a;

    invoke-direct {v0}, Lcom/twitter/model/core/s$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/s$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 130
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/core/s$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/s$a;

    move-result-object v0

    .line 131
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/s$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/s$a;

    move-result-object v0

    .line 132
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/s$a;->a(Z)Lcom/twitter/model/core/s$a;

    move-result-object v0

    .line 133
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/s$a;->b(Z)Lcom/twitter/model/core/s$a;

    .line 134
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 109
    check-cast p2, Lcom/twitter/model/core/s$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/s$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/s$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/s;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p2, Lcom/twitter/model/core/s;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/s;->c:Ljava/lang/String;

    .line 115
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/s;->d:Z

    .line 116
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/s;->e:Z

    .line 117
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 118
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    check-cast p2, Lcom/twitter/model/core/s;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/s$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/s;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/twitter/model/core/s$b;->a()Lcom/twitter/model/core/s$a;

    move-result-object v0

    return-object v0
.end method
