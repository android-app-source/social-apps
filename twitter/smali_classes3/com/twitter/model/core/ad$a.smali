.class public abstract Lcom/twitter/model/core/ad$a;
.super Lcom/twitter/model/core/d$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/twitter/model/core/ad;",
        "B:",
        "Lcom/twitter/model/core/ad$a",
        "<TE;TB;>;>",
        "Lcom/twitter/model/core/d$a",
        "<TE;TB;>;"
    }
.end annotation


# instance fields
.field A:I

.field B:I

.field x:Ljava/lang/String;

.field y:Ljava/lang/String;

.field z:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 92
    invoke-direct {p0}, Lcom/twitter/model/core/d$a;-><init>()V

    .line 89
    iput v0, p0, Lcom/twitter/model/core/ad$a;->A:I

    .line 90
    iput v0, p0, Lcom/twitter/model/core/ad$a;->B:I

    .line 93
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/model/core/ad;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 96
    invoke-direct {p0, p1}, Lcom/twitter/model/core/d$a;-><init>(Lcom/twitter/model/core/d;)V

    .line 89
    iput v0, p0, Lcom/twitter/model/core/ad$a;->A:I

    .line 90
    iput v0, p0, Lcom/twitter/model/core/ad$a;->B:I

    .line 97
    iget-object v0, p1, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ad$a;->x:Ljava/lang/String;

    .line 98
    iget-object v0, p1, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ad$a;->y:Ljava/lang/String;

    .line 99
    iget-object v0, p1, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ad$a;->z:Ljava/lang/String;

    .line 100
    iget v0, p1, Lcom/twitter/model/core/ad;->H:I

    iput v0, p0, Lcom/twitter/model/core/ad$a;->A:I

    .line 101
    iget v0, p1, Lcom/twitter/model/core/ad;->I:I

    iput v0, p0, Lcom/twitter/model/core/ad$a;->B:I

    .line 102
    return-void
.end method


# virtual methods
.method protected c_()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 141
    invoke-super {p0}, Lcom/twitter/model/core/d$a;->c_()V

    .line 144
    iget-object v0, p0, Lcom/twitter/model/core/ad$a;->x:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 145
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/model/core/ad$a;->x:Ljava/lang/String;

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/twitter/model/core/ad$a;->y:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/twitter/model/core/ad$a;->x:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ad$a;->y:Ljava/lang/String;

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/core/ad$a;->z:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 151
    iget-object v0, p0, Lcom/twitter/model/core/ad$a;->y:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ad$a;->z:Ljava/lang/String;

    .line 153
    :cond_2
    iget v0, p0, Lcom/twitter/model/core/ad$a;->c:I

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/twitter/model/core/ad$a;->d:I

    if-ne v0, v1, :cond_3

    .line 155
    iget v0, p0, Lcom/twitter/model/core/ad$a;->c:I

    iget-object v1, p0, Lcom/twitter/model/core/ad$a;->x:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/model/core/ad$a;->d:I

    .line 157
    :cond_3
    iget v0, p0, Lcom/twitter/model/core/ad$a;->B:I

    if-nez v0, :cond_4

    .line 159
    iget v0, p0, Lcom/twitter/model/core/ad$a;->A:I

    iget-object v1, p0, Lcom/twitter/model/core/ad$a;->z:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/model/core/ad$a;->B:I

    .line 161
    :cond_4
    return-void
.end method

.method public d(I)Lcom/twitter/model/core/ad$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .prologue
    .line 129
    iput p1, p0, Lcom/twitter/model/core/ad$a;->A:I

    .line 130
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad$a;

    return-object v0
.end method

.method public e(I)Lcom/twitter/model/core/ad$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .prologue
    .line 135
    iput p1, p0, Lcom/twitter/model/core/ad$a;->B:I

    .line 136
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad$a;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/core/ad$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 111
    iput-object p1, p0, Lcom/twitter/model/core/ad$a;->x:Ljava/lang/String;

    .line 112
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad$a;

    return-object v0
.end method

.method public bridge synthetic f()I
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Lcom/twitter/model/core/d$a;->f()I

    move-result v0

    return v0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/model/core/ad$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 117
    iput-object p1, p0, Lcom/twitter/model/core/ad$a;->y:Ljava/lang/String;

    .line 118
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad$a;

    return-object v0
.end method

.method public bridge synthetic g()I
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Lcom/twitter/model/core/d$a;->g()I

    move-result v0

    return v0
.end method

.method public g(Ljava/lang/String;)Lcom/twitter/model/core/ad$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 123
    iput-object p1, p0, Lcom/twitter/model/core/ad$a;->z:Ljava/lang/String;

    .line 124
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad$a;

    return-object v0
.end method
