.class public Lcom/twitter/model/core/v;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/v$b;,
        Lcom/twitter/model/core/v$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/model/core/v;

.field public static final b:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/core/v;",
            "Lcom/twitter/model/core/v$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Lcom/twitter/model/core/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/ad;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/twitter/model/core/k;

.field public final e:Lcom/twitter/model/core/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/twitter/model/core/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/h;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/twitter/model/core/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/b;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/model/core/v$a;

    invoke-direct {v0}, Lcom/twitter/model/core/v$a;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/model/core/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    sput-object v0, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    .line 26
    new-instance v0, Lcom/twitter/model/core/v$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/core/v$b;-><init>(Lcom/twitter/model/core/v$1;)V

    sput-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/v$a;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/model/core/v;->h:Z

    .line 47
    iget-object v0, p1, Lcom/twitter/model/core/v$a;->a:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0}, Lcom/twitter/model/core/f$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/f;

    iput-object v0, p0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    .line 48
    iget-object v0, p1, Lcom/twitter/model/core/v$a;->b:Lcom/twitter/model/core/k$a;

    invoke-virtual {v0}, Lcom/twitter/model/core/k$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/k;

    iput-object v0, p0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    .line 49
    iget-object v0, p1, Lcom/twitter/model/core/v$a;->c:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0}, Lcom/twitter/model/core/f$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/f;

    iput-object v0, p0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    .line 50
    iget-object v0, p1, Lcom/twitter/model/core/v$a;->d:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0}, Lcom/twitter/model/core/f$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/f;

    iput-object v0, p0, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    .line 51
    iget-object v0, p1, Lcom/twitter/model/core/v$a;->e:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0}, Lcom/twitter/model/core/f$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/f;

    iput-object v0, p0, Lcom/twitter/model/core/v;->g:Lcom/twitter/model/core/f;

    .line 52
    return-void
.end method

.method public static a(Ljava/lang/CharSequence;Lcom/twitter/model/core/v;)I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 215
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    .line 217
    invoke-static {p1}, Lcom/twitter/model/core/v;->b(Lcom/twitter/model/core/v;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/Pair;

    .line 218
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v1, v4, :cond_1

    .line 219
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, v1, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v2, v0

    .line 221
    goto :goto_0

    .line 222
    :cond_0
    sub-int v0, v4, v2

    return v0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public static a([B)Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    .line 56
    sget-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {p0, v0}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    return-object v0
.end method

.method public static a(Ljava/lang/StringBuilder;Lcom/twitter/model/core/v;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 170
    if-nez p1, :cond_0

    .line 171
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 211
    :goto_0
    return-object v0

    .line 173
    :cond_0
    iget-object v0, p1, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/h;

    .line 174
    iget v2, v0, Lcom/twitter/model/core/h;->g:I

    iput v2, v0, Lcom/twitter/model/core/h;->j:I

    .line 175
    iget v2, v0, Lcom/twitter/model/core/h;->h:I

    iput v2, v0, Lcom/twitter/model/core/h;->k:I

    goto :goto_1

    .line 177
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/model/core/v;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 178
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 180
    :cond_2
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    .line 181
    const/4 v0, 0x0

    .line 182
    invoke-virtual {p1}, Lcom/twitter/model/core/v;->b()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 183
    iget v5, v0, Lcom/twitter/model/core/ad;->g:I

    .line 184
    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/ad;

    .line 185
    if-eqz v1, :cond_4

    .line 186
    iget v5, v1, Lcom/twitter/model/core/ad;->H:I

    iput v5, v0, Lcom/twitter/model/core/ad;->H:I

    .line 187
    iget v1, v1, Lcom/twitter/model/core/ad;->I:I

    iput v1, v0, Lcom/twitter/model/core/ad;->I:I

    goto :goto_2

    .line 190
    :cond_4
    invoke-virtual {v3, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 192
    sub-int v1, v5, v2

    .line 193
    iget v5, v0, Lcom/twitter/model/core/ad;->h:I

    sub-int/2addr v5, v2

    .line 194
    if-ltz v1, :cond_5

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-gt v5, v6, :cond_5

    .line 195
    iget-object v6, v0, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    .line 196
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 197
    invoke-virtual {p0, v1, v5, v6}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v1

    .line 199
    sub-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 200
    iput v1, v0, Lcom/twitter/model/core/ad;->H:I

    .line 201
    iput v6, v0, Lcom/twitter/model/core/ad;->I:I

    .line 205
    :cond_5
    iget-object v1, p1, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-virtual {v1}, Lcom/twitter/model/core/f;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/h;

    .line 206
    iget v6, v0, Lcom/twitter/model/core/ad;->H:I

    if-ltz v6, :cond_6

    iget v6, v0, Lcom/twitter/model/core/ad;->H:I

    iget v7, v1, Lcom/twitter/model/core/h;->j:I

    if-ge v6, v7, :cond_6

    .line 207
    neg-int v6, v2

    invoke-virtual {v1, v6}, Lcom/twitter/model/core/h;->d(I)V

    goto :goto_3

    .line 211
    :cond_7
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private static b(Lcom/twitter/model/core/v;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/v;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 227
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Iterable;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/twitter/model/core/v;->g:Lcom/twitter/model/core/f;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcpt;->a([Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    .line 229
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v1

    .line 230
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d;

    .line 231
    iget v3, v0, Lcom/twitter/model/core/d;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v0, v0, Lcom/twitter/model/core/d;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto :goto_0

    .line 233
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public static b(Ljava/lang/CharSequence;Lcom/twitter/model/core/v;)Z
    .locals 9

    .prologue
    const v8, 0x3e99999a    # 0.3f

    const/4 v3, 0x0

    .line 237
    sget-boolean v0, Lcom/twitter/util/b;->a:Z

    if-nez v0, :cond_1

    .line 261
    :cond_0
    :goto_0
    return v3

    .line 241
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v5

    .line 242
    if-eqz v5, :cond_0

    invoke-static {p0}, Lcom/twitter/util/s;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    sget-object v0, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    invoke-static {p1, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    invoke-static {p0, v0}, Lcom/twitter/model/core/v;->a(Ljava/lang/CharSequence;Lcom/twitter/model/core/v;)I

    move-result v2

    .line 250
    int-to-float v0, v2

    mul-float/2addr v0, v8

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    move v4, v3

    move v1, v3

    .line 251
    :goto_1
    if-ge v4, v5, :cond_4

    if-ge v1, v0, :cond_4

    .line 252
    invoke-interface {p0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    .line 253
    invoke-static {v6}, Lcom/twitter/util/s;->a(C)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 254
    add-int/lit8 v1, v1, 0x1

    .line 251
    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 255
    :cond_3
    invoke-static {v6}, Ljava/lang/Character;->getType(C)I

    move-result v6

    const/16 v7, 0x10

    if-ne v6, v7, :cond_2

    .line 257
    add-int/lit8 v2, v2, -0x1

    .line 258
    int-to-float v0, v2

    mul-float/2addr v0, v8

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v0, v6

    goto :goto_2

    .line 261
    :cond_4
    if-lt v1, v0, :cond_5

    const/4 v0, 0x1

    :goto_3
    move v3, v0

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_3
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/twitter/model/core/v;->h:Z

    if-nez v0, :cond_0

    .line 102
    const/4 v0, -0x1

    neg-int v1, p1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/model/core/v;->a(II)V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/model/core/v;->h:Z

    .line 105
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/core/f;->a(II)V

    .line 109
    iget-object v0, p0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/core/k;->a(II)V

    .line 110
    iget-object v0, p0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/core/f;->a(II)V

    .line 111
    iget-object v0, p0, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/core/f;->a(II)V

    .line 112
    iget-object v0, p0, Lcom/twitter/model/core/v;->g:Lcom/twitter/model/core/f;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/core/f;->a(II)V

    .line 113
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v0}, Lcom/twitter/model/core/k;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Z
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/v;->b(J)Lcom/twitter/model/core/q;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/v;)Z
    .locals 2

    .prologue
    .line 148
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    iget-object v1, p1, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    iget-object v1, p1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    .line 149
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    iget-object v1, p1, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    iget-object v1, p1, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    .line 150
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/v;->g:Lcom/twitter/model/core/f;

    iget-object v1, p1, Lcom/twitter/model/core/v;->g:Lcom/twitter/model/core/f;

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 148
    :goto_0
    return v0

    .line 150
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)Lcom/twitter/model/core/q;
    .locals 5

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    .line 78
    iget-wide v2, v0, Lcom/twitter/model/core/q;->c:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 82
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/ad;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    iget-object v1, p0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    sget-object v2, Lcom/twitter/model/core/d;->e:Ljava/util/Comparator;

    invoke-static {v0, v1, v2}, Lcpt;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public b(II)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/core/f;->a(II)V

    .line 129
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 144
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/core/v;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/core/v;

    invoke-virtual {p0, p1}, Lcom/twitter/model/core/v;->a(Lcom/twitter/model/core/v;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->hashCode()I

    move-result v0

    .line 156
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v1}, Lcom/twitter/model/core/k;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v1}, Lcom/twitter/model/core/f;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-virtual {v1}, Lcom/twitter/model/core/f;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/core/v;->g:Lcom/twitter/model/core/f;

    invoke-virtual {v1}, Lcom/twitter/model/core/f;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/core/v;->g:Lcom/twitter/model/core/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
