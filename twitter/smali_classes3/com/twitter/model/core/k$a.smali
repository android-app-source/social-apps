.class public final Lcom/twitter/model/core/k$a;
.super Lcom/twitter/model/core/f$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/core/f$a",
        "<",
        "Lcom/twitter/model/core/MediaEntity;",
        "Lcom/twitter/model/core/k;",
        "Lcom/twitter/model/core/k$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/twitter/model/core/f$a;-><init>()V

    .line 69
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/twitter/model/core/f$a;-><init>(I)V

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/k;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/twitter/model/core/f$a;-><init>(Lcom/twitter/model/core/f;)V

    .line 77
    return-void
.end method


# virtual methods
.method protected synthetic a(Ljava/util/List;)Lcom/twitter/model/core/f;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0, p1}, Lcom/twitter/model/core/k$a;->b(Ljava/util/List;)Lcom/twitter/model/core/k;

    move-result-object v0

    return-object v0
.end method

.method protected b(Ljava/util/List;)Lcom/twitter/model/core/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)",
            "Lcom/twitter/model/core/k;"
        }
    .end annotation

    .prologue
    .line 82
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/model/core/k;

    invoke-direct {v0, p1}, Lcom/twitter/model/core/k;-><init>(Ljava/util/List;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/model/core/k;->e()Lcom/twitter/model/core/k;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic e()Z
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/twitter/model/core/f$a;->e()Z

    move-result v0

    return v0
.end method
