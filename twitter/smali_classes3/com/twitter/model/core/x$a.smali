.class Lcom/twitter/model/core/x$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/core/x;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/core/x$1;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/model/core/x$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/core/x;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lcom/twitter/model/core/x;

    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v1

    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v2

    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v3

    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/model/core/x;-><init>(FFFF)V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/x;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    iget v0, p2, Lcom/twitter/model/core/x;->b:F

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/x;->c:F

    .line 44
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/x;->d:F

    .line 45
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/x;->e:F

    .line 46
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    .line 47
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    check-cast p2, Lcom/twitter/model/core/x;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/x$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/x;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/x$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/core/x;

    move-result-object v0

    return-object v0
.end method
