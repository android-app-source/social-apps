.class Lcom/twitter/model/core/r$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/core/r;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/core/r$1;)V
    .locals 0

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/twitter/model/core/r$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/core/r;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 218
    new-instance v1, Lcom/twitter/model/core/r;

    invoke-direct {v1}, Lcom/twitter/model/core/r;-><init>()V

    .line 219
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/twitter/model/core/r;->b:J

    .line 220
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/model/core/r;->c:Ljava/lang/String;

    .line 221
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    .line 222
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/twitter/model/core/r;->e:J

    .line 223
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/model/core/r;->f:Ljava/lang/String;

    .line 224
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/model/core/r;->g:Ljava/lang/String;

    .line 225
    sget-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    .line 226
    if-eqz v0, :cond_0

    :goto_0
    iput-object v0, v1, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    .line 227
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/twitter/model/core/r;->i:J

    .line 228
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/model/core/r;->j:Ljava/lang/String;

    .line 229
    sget-object v0, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    iput-object v0, v1, Lcom/twitter/model/core/r;->k:Lcax;

    .line 230
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v0

    iput-boolean v0, v1, Lcom/twitter/model/core/r;->l:Z

    .line 231
    sget-object v0, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    iput-object v0, v1, Lcom/twitter/model/core/r;->m:Lcgi;

    .line 232
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v0

    iput-boolean v0, v1, Lcom/twitter/model/core/r;->n:Z

    .line 234
    :try_start_0
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v0

    iput-boolean v0, v1, Lcom/twitter/model/core/r;->o:Z
    :try_end_0
    .catch Lcom/twitter/util/serialization/OptionalFieldException; {:try_start_0 .. :try_end_0} :catch_5

    .line 238
    :goto_1
    :try_start_1
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/model/core/r;->p:Ljava/lang/String;
    :try_end_1
    .catch Lcom/twitter/util/serialization/OptionalFieldException; {:try_start_1 .. :try_end_1} :catch_4

    .line 242
    :goto_2
    :try_start_2
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/twitter/model/core/r;->q:J
    :try_end_2
    .catch Lcom/twitter/util/serialization/OptionalFieldException; {:try_start_2 .. :try_end_2} :catch_3

    .line 246
    :goto_3
    :try_start_3
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/twitter/model/core/r;->r:J
    :try_end_3
    .catch Lcom/twitter/util/serialization/OptionalFieldException; {:try_start_3 .. :try_end_3} :catch_2

    .line 250
    :goto_4
    :try_start_4
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v0

    iput-boolean v0, v1, Lcom/twitter/model/core/r;->s:Z
    :try_end_4
    .catch Lcom/twitter/util/serialization/OptionalFieldException; {:try_start_4 .. :try_end_4} :catch_1

    .line 254
    :goto_5
    :try_start_5
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v0

    iput-boolean v0, v1, Lcom/twitter/model/core/r;->t:Z
    :try_end_5
    .catch Lcom/twitter/util/serialization/OptionalFieldException; {:try_start_5 .. :try_end_5} :catch_0

    .line 257
    :goto_6
    return-object v1

    .line 226
    :cond_0
    sget-object v0, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    goto :goto_0

    .line 255
    :catch_0
    move-exception v0

    goto :goto_6

    .line 251
    :catch_1
    move-exception v0

    goto :goto_5

    .line 247
    :catch_2
    move-exception v0

    goto :goto_4

    .line 243
    :catch_3
    move-exception v0

    goto :goto_3

    .line 239
    :catch_4
    move-exception v0

    goto :goto_2

    .line 235
    :catch_5
    move-exception v0

    goto :goto_1
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/r;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    iget-wide v0, p2, Lcom/twitter/model/core/r;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/r;->c:Ljava/lang/String;

    .line 194
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    .line 195
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/core/r;->e:J

    .line 196
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/r;->f:Ljava/lang/String;

    .line 197
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/r;->g:Ljava/lang/String;

    .line 198
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    sget-object v2, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 199
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/core/r;->i:J

    .line 200
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/r;->j:Ljava/lang/String;

    .line 201
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/r;->k:Lcax;

    sget-object v2, Lcax;->a:Lcom/twitter/util/serialization/l;

    .line 202
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/r;->l:Z

    .line 203
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/r;->m:Lcgi;

    sget-object v2, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 204
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/r;->n:Z

    .line 205
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/r;->o:Z

    .line 206
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/r;->p:Ljava/lang/String;

    .line 207
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/core/r;->q:J

    .line 208
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/core/r;->r:J

    .line 209
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/r;->s:Z

    .line 210
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/r;->t:Z

    .line 211
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 212
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 189
    check-cast p2, Lcom/twitter/model/core/r;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/r$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/r;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 189
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/r$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/core/r;

    move-result-object v0

    return-object v0
.end method
