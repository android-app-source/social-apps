.class Lcom/twitter/model/core/TwitterUser$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/TwitterUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/core/TwitterUser;",
        "Lcom/twitter/model/core/TwitterUser$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 898
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/core/TwitterUser$1;)V
    .locals 0

    .prologue
    .line 898
    invoke-direct {p0}, Lcom/twitter/model/core/TwitterUser$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/core/TwitterUser$a;
    .locals 1

    .prologue
    .line 952
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/TwitterUser$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 958
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 959
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 960
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 961
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 962
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->i(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 963
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->e(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 964
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 965
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->f(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 966
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 967
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->b(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 968
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->b(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 969
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 970
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->h(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    .line 972
    invoke-static {v0}, Lcom/twitter/util/collection/d;->c(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    .line 971
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/util/collection/k;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 973
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->e(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 974
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->b(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 975
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->f(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 976
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 977
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 978
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 979
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->c(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 980
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->d(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 981
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->h(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget-object v0, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 982
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcgi;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 983
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 984
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->b(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 985
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->d(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 986
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->j(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 987
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->e(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 988
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->d(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 989
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->h(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 990
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->i(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 991
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->j(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 992
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->d(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    .line 993
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/profile/ExtendedProfile;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 994
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->f(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    const-class v0, Lcom/twitter/model/ads/AdvertiserType;

    .line 995
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 996
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/ads/AdvertiserType;

    .line 995
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/ads/AdvertiserType;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    .line 997
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 998
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->e(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget-object v0, Lcbw;->a:Lcom/twitter/util/serialization/b;

    .line 999
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbw;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcbw;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    const-class v0, Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 1000
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 1001
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 1000
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/businessprofiles/BusinessProfileState;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 1002
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->j(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 1003
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/model/businessprofiles/h$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    const-class v0, Lcom/twitter/model/profile/TranslatorType;

    .line 1005
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/TranslatorType;

    .line 1004
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/profile/TranslatorType;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->i:Lcom/twitter/util/serialization/l;

    .line 1007
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1006
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Ljava/util/List;)Lcom/twitter/model/core/TwitterUser$a;

    .line 1008
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 898
    check-cast p2, Lcom/twitter/model/core/TwitterUser$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/TwitterUser$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/TwitterUser$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/TwitterUser;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 901
    iget-wide v0, p2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    .line 902
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    .line 903
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    .line 904
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->F:Ljava/lang/String;

    .line 905
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    .line 906
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->R:I

    .line 907
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->g:Ljava/lang/String;

    .line 908
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->h:I

    .line 909
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->i:I

    .line 910
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/TwitterUser;->l:Z

    .line 911
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/TwitterUser;->m:Z

    .line 912
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->p:Ljava/lang/String;

    .line 913
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->q:Lcom/twitter/util/collection/k;

    sget-object v2, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    .line 915
    invoke-static {v2}, Lcom/twitter/util/collection/d;->c(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 914
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->u:I

    .line 916
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/core/TwitterUser;->S:J

    .line 917
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->v:I

    .line 918
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->w:I

    .line 919
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/TwitterUser;->x:Z

    .line 920
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 921
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/core/TwitterUser;->y:J

    .line 922
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/core/TwitterUser;->B:J

    .line 923
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->z:I

    .line 924
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    sget-object v2, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 925
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    sget-object v2, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 926
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->D:Lcom/twitter/model/core/v;

    sget-object v2, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 927
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->e:Ljava/lang/String;

    .line 928
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->G:Ljava/lang/String;

    .line 929
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/TwitterUser;->o:Z

    .line 930
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/TwitterUser;->n:Z

    .line 931
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/TwitterUser;->H:Z

    .line 932
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/TwitterUser;->I:Z

    .line 933
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->J:I

    .line 934
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterUser;->t:I

    .line 935
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->s:Lcom/twitter/model/profile/ExtendedProfile;

    sget-object v2, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    .line 936
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/TwitterUser;->r:Z

    .line 937
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->K:Lcom/twitter/model/ads/AdvertiserType;

    const-class v2, Lcom/twitter/model/ads/AdvertiserType;

    .line 938
    invoke-static {v2}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    sget-object v2, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    .line 939
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/core/TwitterUser;->L:J

    .line 940
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->E:Lcbw;

    sget-object v2, Lcbw;->a:Lcom/twitter/util/serialization/b;

    .line 941
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->M:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    const-class v2, Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 943
    invoke-static {v2}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 942
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/TwitterUser;->N:Z

    .line 944
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->O:Ljava/lang/String;

    .line 945
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterUser;->P:Lcom/twitter/model/profile/TranslatorType;

    const-class v2, Lcom/twitter/model/profile/TranslatorType;

    .line 946
    invoke-static {v2}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 947
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 898
    check-cast p2, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/TwitterUser$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/TwitterUser;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 898
    invoke-virtual {p0}, Lcom/twitter/model/core/TwitterUser$b;->a()Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    return-object v0
.end method
