.class public Lcom/twitter/model/core/i$b;
.super Lcom/twitter/model/core/d$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/core/d$b",
        "<",
        "Lcom/twitter/model/core/i;",
        "Lcom/twitter/model/core/i$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/model/core/d$b;-><init>(I)V

    .line 51
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/core/i$a;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/twitter/model/core/i$a;

    invoke-direct {v0}, Lcom/twitter/model/core/i$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 48
    check-cast p2, Lcom/twitter/model/core/i$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/i$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/i$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/i$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/model/core/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V

    .line 71
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 48
    check-cast p2, Lcom/twitter/model/core/i$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/i$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/i$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    check-cast p2, Lcom/twitter/model/core/i;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/i$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/i;)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-super {p0, p1, p2}, Lcom/twitter/model/core/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V

    .line 58
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    check-cast p2, Lcom/twitter/model/core/i;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/i$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/i;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/twitter/model/core/i$b;->a()Lcom/twitter/model/core/i$a;

    move-result-object v0

    return-object v0
.end method
