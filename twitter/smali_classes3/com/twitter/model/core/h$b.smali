.class public Lcom/twitter/model/core/h$b;
.super Lcom/twitter/model/core/d$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/core/d$b",
        "<",
        "Lcom/twitter/model/core/h;",
        "Lcom/twitter/model/core/h$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/model/core/d$b;-><init>(I)V

    .line 124
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/core/h$a;
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lcom/twitter/model/core/h$a;

    invoke-direct {v0}, Lcom/twitter/model/core/h$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 121
    check-cast p2, Lcom/twitter/model/core/h$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/h$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/h$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/h$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 146
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/model/core/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V

    .line 147
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/core/h$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/h$a;

    move-result-object v0

    .line 148
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/h$a;->d(I)Lcom/twitter/model/core/h$a;

    move-result-object v0

    .line 149
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/h$a;->e(I)Lcom/twitter/model/core/h$a;

    .line 150
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 121
    check-cast p2, Lcom/twitter/model/core/h$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/h$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/h$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    check-cast p2, Lcom/twitter/model/core/h;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/h$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/h;)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    invoke-super {p0, p1, p2}, Lcom/twitter/model/core/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V

    .line 131
    iget-object v0, p2, Lcom/twitter/model/core/h;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/h;->j:I

    .line 132
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/h;->k:I

    .line 133
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 134
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    check-cast p2, Lcom/twitter/model/core/h;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/h$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/h;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/twitter/model/core/h$b;->a()Lcom/twitter/model/core/h$a;

    move-result-object v0

    return-object v0
.end method
