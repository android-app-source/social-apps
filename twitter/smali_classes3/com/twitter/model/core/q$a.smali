.class public final Lcom/twitter/model/core/q$a;
.super Lcom/twitter/model/core/d$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/core/d$a",
        "<",
        "Lcom/twitter/model/core/q;",
        "Lcom/twitter/model/core/q$a;",
        ">;"
    }
.end annotation


# instance fields
.field a:J

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/twitter/model/core/d$a;-><init>()V

    .line 63
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/q;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/twitter/model/core/d$a;-><init>(Lcom/twitter/model/core/d;)V

    .line 67
    iget-wide v0, p1, Lcom/twitter/model/core/q;->c:J

    iput-wide v0, p0, Lcom/twitter/model/core/q$a;->a:J

    .line 68
    iget-object v0, p1, Lcom/twitter/model/core/q;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/q$a;->e:Ljava/lang/String;

    .line 69
    iget-object v0, p1, Lcom/twitter/model/core/q;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/q$a;->f:Ljava/lang/String;

    .line 70
    return-void
.end method


# virtual methods
.method public a(J)Lcom/twitter/model/core/q$a;
    .locals 1

    .prologue
    .line 74
    iput-wide p1, p0, Lcom/twitter/model/core/q$a;->a:J

    .line 75
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/core/q$a;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/twitter/model/core/q$a;->e:Ljava/lang/String;

    .line 81
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/core/q$a;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/twitter/model/core/q$a;->f:Ljava/lang/String;

    .line 87
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/twitter/model/core/q$a;->e()Lcom/twitter/model/core/q;

    move-result-object v0

    return-object v0
.end method

.method protected c_()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 92
    invoke-super {p0}, Lcom/twitter/model/core/d$a;->c_()V

    .line 93
    iget v0, p0, Lcom/twitter/model/core/q$a;->c:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/model/core/q$a;->d:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/q$a;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 95
    iget v0, p0, Lcom/twitter/model/core/q$a;->c:I

    iget-object v1, p0, Lcom/twitter/model/core/q$a;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/model/core/q$a;->d:I

    .line 97
    :cond_0
    return-void
.end method

.method protected e()Lcom/twitter/model/core/q;
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/twitter/model/core/q;

    invoke-direct {v0, p0}, Lcom/twitter/model/core/q;-><init>(Lcom/twitter/model/core/q$a;)V

    return-object v0
.end method

.method public bridge synthetic f()I
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Lcom/twitter/model/core/d$a;->f()I

    move-result v0

    return v0
.end method

.method public bridge synthetic g()I
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Lcom/twitter/model/core/d$a;->g()I

    move-result v0

    return v0
.end method
