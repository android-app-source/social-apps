.class public Lcom/twitter/model/core/ad;
.super Lcom/twitter/model/core/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/ad$d;,
        Lcom/twitter/model/core/ad$b;,
        Lcom/twitter/model/core/ad$c;,
        Lcom/twitter/model/core/ad$a;
    }
.end annotation


# static fields
.field public static final C:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/core/ad;",
            ">;"
        }
    .end annotation
.end field

.field public static final D:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/ad;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final E:Ljava/lang/String;

.field public final F:Ljava/lang/String;

.field public final G:Ljava/lang/String;

.field public H:I

.field public I:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/model/core/ad$d;

    invoke-direct {v0}, Lcom/twitter/model/core/ad$d;-><init>()V

    sput-object v0, Lcom/twitter/model/core/ad;->C:Lcom/twitter/util/serialization/l;

    .line 20
    sget-object v0, Lcom/twitter/model/core/ad;->C:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/model/core/f;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/core/ad;->D:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method constructor <init>(Lcom/twitter/model/core/ad$a;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/twitter/model/core/d;-><init>(Lcom/twitter/model/core/d$a;)V

    .line 33
    iget-object v0, p1, Lcom/twitter/model/core/ad$a;->x:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    .line 34
    iget-object v0, p1, Lcom/twitter/model/core/ad$a;->y:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    .line 35
    iget-object v0, p1, Lcom/twitter/model/core/ad$a;->z:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    .line 36
    iget v0, p1, Lcom/twitter/model/core/ad$a;->A:I

    iput v0, p0, Lcom/twitter/model/core/ad;->H:I

    .line 37
    iget v0, p1, Lcom/twitter/model/core/ad$a;->B:I

    iput v0, p0, Lcom/twitter/model/core/ad;->I:I

    .line 38
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/ad;)Z
    .locals 2

    .prologue
    .line 70
    if-eq p0, p1, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/model/core/d;->a(Lcom/twitter/model/core/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/twitter/model/core/d;->b(I)V

    .line 49
    iget v0, p0, Lcom/twitter/model/core/ad;->H:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/twitter/model/core/ad;->I:I

    if-ltz v0, :cond_0

    .line 50
    iget v0, p0, Lcom/twitter/model/core/ad;->H:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/model/core/ad;->H:I

    .line 51
    iget v0, p0, Lcom/twitter/model/core/ad;->I:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/model/core/ad;->I:I

    .line 53
    :cond_0
    return-void
.end method

.method public synthetic bt_()Lcom/twitter/model/core/d$a;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/twitter/model/core/ad;->d()Lcom/twitter/model/core/ad$a;

    move-result-object v0

    return-object v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/twitter/model/core/d;->c(I)V

    .line 58
    iget v0, p0, Lcom/twitter/model/core/ad;->I:I

    if-ltz v0, :cond_0

    .line 59
    iget v0, p0, Lcom/twitter/model/core/ad;->I:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/model/core/ad;->I:I

    .line 61
    :cond_0
    return-void
.end method

.method public d()Lcom/twitter/model/core/ad$a;
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/twitter/model/core/ad$c;

    invoke-direct {v0, p0}, Lcom/twitter/model/core/ad$c;-><init>(Lcom/twitter/model/core/ad;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 65
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/core/ad;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/core/ad;

    invoke-virtual {p0, p1}, Lcom/twitter/model/core/ad;->a(Lcom/twitter/model/core/ad;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 75
    invoke-super {p0}, Lcom/twitter/model/core/d;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    return-object v0
.end method
