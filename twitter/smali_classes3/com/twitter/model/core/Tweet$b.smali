.class Lcom/twitter/model/core/Tweet$b;
.super Lcok$d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/Tweet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field final a:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 1471
    invoke-direct {p0}, Lcok$d;-><init>()V

    .line 1472
    const-string/jumbo v0, "conversations_autopopulate_reply_android_4275"

    const-string/jumbo v1, "unassigned"

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1474
    const-string/jumbo v1, "replies_only"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "replies_plus_grouping"

    .line 1475
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/model/core/Tweet$b;->a:Z

    .line 1476
    return-void

    .line 1475
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a()Lcom/twitter/util/object/j;
    .locals 1

    .prologue
    .line 1469
    invoke-static {}, Lcom/twitter/model/core/Tweet$b;->b()Lcom/twitter/util/object/j;

    move-result-object v0

    return-object v0
.end method

.method private static b()Lcom/twitter/util/object/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/object/j",
            "<",
            "Lcom/twitter/model/core/Tweet$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1480
    new-instance v0, Lcom/twitter/model/core/Tweet$b$1;

    invoke-direct {v0}, Lcom/twitter/model/core/Tweet$b$1;-><init>()V

    return-object v0
.end method
