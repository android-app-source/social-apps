.class public Lcom/twitter/model/core/z;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/z$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/twitter/model/core/y;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:[I

.field public static final c:[Ljava/lang/String;

.field public static final d:[I


# instance fields
.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/twitter/model/core/z$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/core/z$a;-><init>(Lcom/twitter/model/core/z$1;)V

    sput-object v0, Lcom/twitter/model/core/z;->a:Lcom/twitter/util/serialization/l;

    .line 27
    new-array v0, v2, [I

    sput-object v0, Lcom/twitter/model/core/z;->b:[I

    .line 28
    new-array v0, v2, [Ljava/lang/String;

    sput-object v0, Lcom/twitter/model/core/z;->c:[Ljava/lang/String;

    .line 29
    new-array v0, v2, [I

    sput-object v0, Lcom/twitter/model/core/z;->d:[I

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/y;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/z;->e:Ljava/util/List;

    .line 36
    return-void
.end method

.method public static a(Lcom/twitter/model/core/z;)[I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 50
    if-eqz p0, :cond_0

    iget-object v1, p0, Lcom/twitter/model/core/z;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 51
    :goto_0
    if-eqz v1, :cond_2

    .line 52
    new-array v2, v1, [I

    .line 54
    invoke-virtual {p0}, Lcom/twitter/model/core/z;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/y;

    .line 55
    iget v0, v0, Lcom/twitter/model/core/y;->b:I

    aput v0, v2, v1

    .line 56
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 57
    goto :goto_1

    :cond_0
    move v1, v0

    .line 50
    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 60
    :goto_2
    return-object v0

    :cond_2
    sget-object v0, Lcom/twitter/model/core/z;->b:[I

    goto :goto_2
.end method

.method static synthetic b(Lcom/twitter/model/core/z;)Ljava/util/List;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/model/core/z;->e:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/twitter/model/core/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/model/core/z;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
