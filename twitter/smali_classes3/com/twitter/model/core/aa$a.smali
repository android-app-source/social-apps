.class public final Lcom/twitter/model/core/aa$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/aa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/core/aa;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field private c:I

.field private d:I

.field private e:J

.field private f:J

.field private g:J

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Lcom/twitter/model/core/TwitterUser;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/core/aa$a;)Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/twitter/model/core/aa$a;->a:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/model/core/aa$a;)Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/twitter/model/core/aa$a;->b:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/model/core/aa$a;)I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/twitter/model/core/aa$a;->c:I

    return v0
.end method

.method static synthetic d(Lcom/twitter/model/core/aa$a;)I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/twitter/model/core/aa$a;->d:I

    return v0
.end method

.method static synthetic e(Lcom/twitter/model/core/aa$a;)J
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/twitter/model/core/aa$a;->e:J

    return-wide v0
.end method

.method static synthetic f(Lcom/twitter/model/core/aa$a;)J
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/twitter/model/core/aa$a;->f:J

    return-wide v0
.end method

.method static synthetic g(Lcom/twitter/model/core/aa$a;)J
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/twitter/model/core/aa$a;->g:J

    return-wide v0
.end method

.method static synthetic h(Lcom/twitter/model/core/aa$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/model/core/aa$a;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/model/core/aa$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/model/core/aa$a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/model/core/aa$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/model/core/aa$a;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/model/core/aa$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/model/core/aa$a;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/model/core/aa$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/model/core/aa$a;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/model/core/aa$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/model/core/aa$a;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/model/core/aa$a;)Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/model/core/aa$a;->n:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/twitter/model/core/aa$a;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public a(I)Lcom/twitter/model/core/aa$a;
    .locals 0

    .prologue
    .line 157
    iput p1, p0, Lcom/twitter/model/core/aa$a;->c:I

    .line 158
    return-object p0
.end method

.method public a(J)Lcom/twitter/model/core/aa$a;
    .locals 1

    .prologue
    .line 169
    iput-wide p1, p0, Lcom/twitter/model/core/aa$a;->e:J

    .line 170
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/aa$a;
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/twitter/model/core/aa$a;->n:Lcom/twitter/model/core/TwitterUser;

    .line 224
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/core/aa$a;
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/twitter/model/core/aa$a;->h:Ljava/lang/String;

    .line 188
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/core/aa$a;
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/twitter/model/core/aa$a;->a:Z

    .line 146
    return-object p0
.end method

.method public b(I)Lcom/twitter/model/core/aa$a;
    .locals 0

    .prologue
    .line 163
    iput p1, p0, Lcom/twitter/model/core/aa$a;->d:I

    .line 164
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/core/aa$a;
    .locals 1

    .prologue
    .line 175
    iput-wide p1, p0, Lcom/twitter/model/core/aa$a;->f:J

    .line 176
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/core/aa$a;
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/twitter/model/core/aa$a;->i:Ljava/lang/String;

    .line 194
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/core/aa$a;
    .locals 0

    .prologue
    .line 151
    iput-boolean p1, p0, Lcom/twitter/model/core/aa$a;->b:Z

    .line 152
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/twitter/model/core/aa$a;->e()Lcom/twitter/model/core/aa;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/twitter/model/core/aa$a;
    .locals 1

    .prologue
    .line 181
    iput-wide p1, p0, Lcom/twitter/model/core/aa$a;->g:J

    .line 182
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/core/aa$a;
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/twitter/model/core/aa$a;->j:Ljava/lang/String;

    .line 200
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/core/aa$a;
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/twitter/model/core/aa$a;->k:Ljava/lang/String;

    .line 206
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/core/aa$a;
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/twitter/model/core/aa$a;->l:Ljava/lang/String;

    .line 212
    return-object p0
.end method

.method protected e()Lcom/twitter/model/core/aa;
    .locals 1

    .prologue
    .line 235
    new-instance v0, Lcom/twitter/model/core/aa;

    invoke-direct {v0, p0}, Lcom/twitter/model/core/aa;-><init>(Lcom/twitter/model/core/aa$a;)V

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/model/core/aa$a;
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/twitter/model/core/aa$a;->m:Ljava/lang/String;

    .line 218
    return-object p0
.end method
