.class public Lcom/twitter/model/core/r;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/r$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/core/r;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:J

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Lcom/twitter/model/core/v;

.field public i:J

.field public j:Ljava/lang/String;

.field public k:Lcax;

.field public l:Z

.field public m:Lcgi;

.field public n:Z

.field public o:Z

.field public p:Ljava/lang/String;

.field public q:J

.field public r:J

.field public s:Z

.field public t:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/model/core/r$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/core/r$a;-><init>(Lcom/twitter/model/core/r$1;)V

    sput-object v0, Lcom/twitter/model/core/r;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget-object v0, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget-object v0, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    .line 84
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->t:J

    iput-wide v0, p0, Lcom/twitter/model/core/r;->e:J

    .line 85
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->s:J

    iput-wide v0, p0, Lcom/twitter/model/core/r;->b:J

    .line 86
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/r;->c:Ljava/lang/String;

    .line 87
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    .line 88
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/r;->f:Ljava/lang/String;

    .line 89
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/r;->g:Ljava/lang/String;

    .line 90
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    .line 91
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->q:J

    iput-wide v0, p0, Lcom/twitter/model/core/r;->i:J

    .line 92
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/r;->j:Ljava/lang/String;

    .line 93
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/r;->k:Lcax;

    .line 94
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->x()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/core/r;->l:Z

    .line 95
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/r;->m:Lcgi;

    .line 96
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->F:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/model/core/r;->n:Z

    .line 97
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->a:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/r;->o:Z

    .line 98
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/r;->p:Ljava/lang/String;

    .line 99
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->C:J

    iput-wide v0, p0, Lcom/twitter/model/core/r;->q:J

    .line 100
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->E:J

    iput-wide v0, p0, Lcom/twitter/model/core/r;->r:J

    .line 101
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->F:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/r;->s:Z

    .line 102
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->L:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/r;->t:Z

    .line 103
    return-void

    .line 96
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/twitter/model/core/ac;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget-object v0, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    .line 51
    invoke-virtual {p1}, Lcom/twitter/model/core/ac;->c()Lcom/twitter/model/core/ac;

    move-result-object v3

    .line 52
    iget-object v4, v3, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    .line 54
    invoke-static {}, Lcom/twitter/model/core/Tweet;->aw()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55
    iget v0, v3, Lcom/twitter/model/core/ac;->E:I

    .line 59
    :goto_0
    iget-wide v6, v3, Lcom/twitter/model/core/ac;->a:J

    iput-wide v6, p0, Lcom/twitter/model/core/r;->e:J

    .line 60
    iget-wide v6, v4, Lcom/twitter/model/core/TwitterUser;->b:J

    iput-wide v6, p0, Lcom/twitter/model/core/r;->b:J

    .line 61
    invoke-virtual {v4}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/twitter/model/core/r;->c:Ljava/lang/String;

    .line 62
    iget-object v5, v4, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iput-object v5, p0, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    .line 63
    iget-object v5, v3, Lcom/twitter/model/core/ac;->b:Ljava/lang/String;

    iget-object v6, v3, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    invoke-static {v5, v0, v6, v8, v2}, Lcom/twitter/model/core/Tweet;->a(Ljava/lang/String;ILcom/twitter/model/core/v;[Lcom/twitter/model/core/d;Z)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/twitter/model/core/r;->f:Ljava/lang/String;

    .line 64
    iget-object v5, v3, Lcom/twitter/model/core/ac;->e:Ljava/lang/String;

    iget-object v6, v3, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    invoke-static {v5, v0, v6, v8, v1}, Lcom/twitter/model/core/Tweet;->a(Ljava/lang/String;ILcom/twitter/model/core/v;[Lcom/twitter/model/core/d;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/r;->g:Ljava/lang/String;

    .line 66
    iget-object v0, v3, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    .line 67
    iget-wide v6, v3, Lcom/twitter/model/core/ac;->g:J

    iput-wide v6, p0, Lcom/twitter/model/core/r;->i:J

    .line 68
    iget-object v0, v3, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/r;->j:Ljava/lang/String;

    .line 69
    iget-object v0, v3, Lcom/twitter/model/core/ac;->z:Lcax;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, v3, Lcom/twitter/model/core/ac;->z:Lcax;

    iput-object v0, p0, Lcom/twitter/model/core/r;->k:Lcax;

    .line 72
    :cond_0
    iget-boolean v0, v3, Lcom/twitter/model/core/ac;->s:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/r;->l:Z

    .line 73
    iget-object v0, v3, Lcom/twitter/model/core/ac;->w:Lcgi;

    iput-object v0, p0, Lcom/twitter/model/core/r;->m:Lcgi;

    .line 74
    iget-boolean v0, v4, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-nez v0, :cond_1

    move v1, v2

    :cond_1
    iput-boolean v1, p0, Lcom/twitter/model/core/r;->n:Z

    .line 75
    iget-boolean v0, p1, Lcom/twitter/model/core/ac;->G:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/r;->o:Z

    .line 76
    iget-object v0, v3, Lcom/twitter/model/core/ac;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/r;->p:Ljava/lang/String;

    .line 77
    iget-wide v0, v3, Lcom/twitter/model/core/ac;->j:J

    iput-wide v0, p0, Lcom/twitter/model/core/r;->q:J

    .line 78
    iget-wide v0, v3, Lcom/twitter/model/core/ac;->i:J

    iput-wide v0, p0, Lcom/twitter/model/core/r;->r:J

    .line 79
    iget-boolean v0, v4, Lcom/twitter/model/core/TwitterUser;->l:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/r;->s:Z

    .line 80
    iget-boolean v0, v4, Lcom/twitter/model/core/TwitterUser;->m:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/r;->t:Z

    .line 81
    return-void

    :cond_2
    move v0, v1

    .line 57
    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 160
    iget-wide v0, p0, Lcom/twitter/model/core/r;->e:J

    iget-object v2, p0, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/model/core/Tweet;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JLjava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/model/core/t;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    iget-wide v0, p0, Lcom/twitter/model/core/r;->b:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 170
    new-instance v0, Lcom/twitter/model/core/t;

    iget-object v1, p0, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/model/core/r;->b:J

    iget-object v4, p0, Lcom/twitter/model/core/r;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/model/core/t;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    .line 174
    iget-wide v2, v0, Lcom/twitter/model/core/q;->c:J

    cmp-long v2, v2, p1

    if-eqz v2, :cond_1

    .line 175
    invoke-static {v0}, Lcom/twitter/model/core/t;->a(Lcom/twitter/model/core/q;)Lcom/twitter/model/core/t;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v0}, Lcom/twitter/model/core/k;->c()Z

    move-result v0

    if-nez v0, :cond_4

    .line 180
    iget-object v0, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0}, Lcom/twitter/model/util/d;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 181
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/n;

    .line 182
    iget-wide v2, v0, Lcom/twitter/model/core/n;->b:J

    cmp-long v2, p1, v2

    if-eqz v2, :cond_3

    .line 183
    invoke-static {v0}, Lcom/twitter/model/core/t;->a(Lcom/twitter/model/core/n;)Lcom/twitter/model/core/t;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 187
    :cond_4
    return-void
.end method

.method public a(Lcom/twitter/model/core/r;)Z
    .locals 4

    .prologue
    .line 112
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/r;->b:J

    iget-wide v2, p1, Lcom/twitter/model/core/r;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/r;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/r;->c:Ljava/lang/String;

    .line 114
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    .line 115
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/r;->e:J

    iget-wide v2, p1, Lcom/twitter/model/core/r;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/r;->f:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/r;->f:Ljava/lang/String;

    .line 117
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/r;->g:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/r;->g:Ljava/lang/String;

    .line 118
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-object v1, p1, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    .line 119
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/v;->a(Lcom/twitter/model/core/v;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/r;->i:J

    iget-wide v2, p1, Lcom/twitter/model/core/r;->i:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/r;->j:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/r;->j:Ljava/lang/String;

    .line 121
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/r;->k:Lcax;

    iget-object v1, p1, Lcom/twitter/model/core/r;->k:Lcax;

    .line 122
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/model/core/r;->l:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/r;->l:Z

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/r;->m:Lcgi;

    iget-object v1, p1, Lcom/twitter/model/core/r;->m:Lcgi;

    .line 124
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/model/core/r;->n:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/r;->n:Z

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/twitter/model/core/r;->o:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/r;->o:Z

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/r;->p:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/r;->p:Ljava/lang/String;

    .line 127
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/r;->q:J

    iget-wide v2, p1, Lcom/twitter/model/core/r;->q:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/r;->r:J

    iget-wide v2, p1, Lcom/twitter/model/core/r;->r:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/model/core/r;->s:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/r;->s:Z

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/twitter/model/core/r;->t:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/r;->t:Z

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 112
    :goto_0
    return v0

    .line 127
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 4

    .prologue
    .line 164
    iget-wide v0, p0, Lcom/twitter/model/core/r;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 107
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/twitter/model/core/r;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/core/r;

    .line 108
    invoke-virtual {p0, p1}, Lcom/twitter/model/core/r;->a(Lcom/twitter/model/core/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 107
    :goto_0
    return v0

    .line 108
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 136
    iget-wide v4, p0, Lcom/twitter/model/core/r;->b:J

    invoke-static {v4, v5}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v0

    .line 137
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/core/r;->c:Ljava/lang/String;

    invoke-static {v3}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 138
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    invoke-static {v3}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 139
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/twitter/model/core/r;->e:J

    invoke-static {v4, v5}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v3

    add-int/2addr v0, v3

    .line 140
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/core/r;->f:Ljava/lang/String;

    invoke-static {v3}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 141
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/core/r;->g:Ljava/lang/String;

    invoke-static {v3}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 142
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    invoke-virtual {v3}, Lcom/twitter/model/core/v;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 143
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/twitter/model/core/r;->i:J

    invoke-static {v4, v5}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v3

    add-int/2addr v0, v3

    .line 144
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/core/r;->j:Ljava/lang/String;

    invoke-static {v3}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 145
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/core/r;->k:Lcax;

    invoke-static {v3}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 146
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/r;->l:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 147
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/core/r;->m:Lcgi;

    invoke-static {v3}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 148
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/r;->n:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 149
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/r;->o:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    .line 150
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/core/r;->p:Ljava/lang/String;

    invoke-static {v3}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 151
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/twitter/model/core/r;->q:J

    invoke-static {v4, v5}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v3

    add-int/2addr v0, v3

    .line 152
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/twitter/model/core/r;->r:J

    invoke-static {v4, v5}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v3

    add-int/2addr v0, v3

    .line 153
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/r;->s:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v3

    .line 154
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/twitter/model/core/r;->t:Z

    if-eqz v3, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 155
    return v0

    :cond_0
    move v0, v2

    .line 146
    goto :goto_0

    :cond_1
    move v0, v2

    .line 148
    goto :goto_1

    :cond_2
    move v0, v2

    .line 149
    goto :goto_2

    :cond_3
    move v0, v2

    .line 153
    goto :goto_3

    :cond_4
    move v1, v2

    .line 154
    goto :goto_4
.end method
