.class Lcom/twitter/model/core/v$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/core/v;",
        "Lcom/twitter/model/core/v$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 433
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/core/v$1;)V
    .locals 0

    .prologue
    .line 433
    invoke-direct {p0}, Lcom/twitter/model/core/v$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/core/v$a;
    .locals 1

    .prologue
    .line 448
    new-instance v0, Lcom/twitter/model/core/v$a;

    invoke-direct {v0}, Lcom/twitter/model/core/v$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/v$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 454
    sget-object v0, Lcom/twitter/model/core/ad;->D:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/f;

    invoke-virtual {p2, v0}, Lcom/twitter/model/core/v$a;->a(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/v$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/MediaEntity;->b:Lcom/twitter/util/serialization/l;

    .line 455
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/k;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/v$a;->a(Lcom/twitter/model/core/k;)Lcom/twitter/model/core/v$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/q;->b:Lcom/twitter/util/serialization/l;

    .line 456
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/f;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/v$a;->b(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/v$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/h;->b:Lcom/twitter/util/serialization/l;

    .line 457
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/f;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/v$a;->c(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/v$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/b;->b:Lcom/twitter/util/serialization/l;

    .line 458
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/f;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/v$a;->d(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/v$a;

    .line 459
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 433
    check-cast p2, Lcom/twitter/model/core/v$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/v$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/v$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/v;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 438
    sget-object v0, Lcom/twitter/model/core/ad;->D:Lcom/twitter/util/serialization/l;

    iget-object v1, p2, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 439
    sget-object v0, Lcom/twitter/model/core/MediaEntity;->b:Lcom/twitter/util/serialization/l;

    iget-object v1, p2, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 440
    sget-object v0, Lcom/twitter/model/core/q;->b:Lcom/twitter/util/serialization/l;

    iget-object v1, p2, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 441
    sget-object v0, Lcom/twitter/model/core/h;->b:Lcom/twitter/util/serialization/l;

    iget-object v1, p2, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 442
    sget-object v0, Lcom/twitter/model/core/b;->b:Lcom/twitter/util/serialization/l;

    iget-object v1, p2, Lcom/twitter/model/core/v;->g:Lcom/twitter/model/core/f;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 443
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 433
    check-cast p2, Lcom/twitter/model/core/v;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/v$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/v;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 433
    invoke-virtual {p0}, Lcom/twitter/model/core/v$b;->a()Lcom/twitter/model/core/v$a;

    move-result-object v0

    return-object v0
.end method
