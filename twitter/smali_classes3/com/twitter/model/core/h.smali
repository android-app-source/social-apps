.class public Lcom/twitter/model/core/h;
.super Lcom/twitter/model/core/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/h$b;,
        Lcom/twitter/model/core/h$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/core/h;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/h;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Ljava/lang/String;

.field public j:I

.field public k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/twitter/model/core/h$b;

    invoke-direct {v0}, Lcom/twitter/model/core/h$b;-><init>()V

    sput-object v0, Lcom/twitter/model/core/h;->a:Lcom/twitter/util/serialization/l;

    .line 16
    sget-object v0, Lcom/twitter/model/core/h;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/model/core/f;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/core/h;->b:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method constructor <init>(Lcom/twitter/model/core/h$a;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/twitter/model/core/d;-><init>(Lcom/twitter/model/core/d$a;)V

    .line 25
    iget-object v0, p1, Lcom/twitter/model/core/h$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/h;->c:Ljava/lang/String;

    .line 26
    iget v0, p1, Lcom/twitter/model/core/h$a;->e:I

    iput v0, p0, Lcom/twitter/model/core/h;->j:I

    .line 27
    iget v0, p1, Lcom/twitter/model/core/h$a;->f:I

    iput v0, p0, Lcom/twitter/model/core/h;->k:I

    .line 28
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/core/h$a;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/twitter/model/core/h$a;

    invoke-direct {v0, p0}, Lcom/twitter/model/core/h$a;-><init>(Lcom/twitter/model/core/h;)V

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/h;)Z
    .locals 2

    .prologue
    .line 60
    if-eq p0, p1, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/model/core/d;->a(Lcom/twitter/model/core/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/h;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/h;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/twitter/model/core/d;->b(I)V

    .line 44
    iget v0, p0, Lcom/twitter/model/core/h;->j:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/model/core/h;->j:I

    .line 45
    iget v0, p0, Lcom/twitter/model/core/h;->k:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/model/core/h;->k:I

    .line 46
    return-void
.end method

.method public synthetic bt_()Lcom/twitter/model/core/d$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/twitter/model/core/h;->a()Lcom/twitter/model/core/h$a;

    move-result-object v0

    return-object v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/twitter/model/core/d;->c(I)V

    .line 51
    iget v0, p0, Lcom/twitter/model/core/h;->k:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/model/core/h;->k:I

    .line 52
    return-void
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/twitter/model/core/h;->j:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/model/core/h;->j:I

    .line 38
    iget v0, p0, Lcom/twitter/model/core/h;->k:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/model/core/h;->k:I

    .line 39
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 56
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/core/h;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/core/h;

    invoke-virtual {p0, p1}, Lcom/twitter/model/core/h;->a(Lcom/twitter/model/core/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 65
    invoke-super {p0}, Lcom/twitter/model/core/d;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/core/h;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
