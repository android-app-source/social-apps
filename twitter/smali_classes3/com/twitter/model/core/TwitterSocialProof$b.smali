.class Lcom/twitter/model/core/TwitterSocialProof$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/TwitterSocialProof;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/core/TwitterSocialProof;",
        "Lcom/twitter/model/core/TwitterSocialProof$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/core/TwitterSocialProof$1;)V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Lcom/twitter/model/core/TwitterSocialProof$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 1

    .prologue
    .line 341
    new-instance v0, Lcom/twitter/model/core/TwitterSocialProof$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterSocialProof$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/TwitterSocialProof$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 348
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 349
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->b(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 350
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->c(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 351
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->d(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 352
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 353
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->e(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 354
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 355
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->g(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 356
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 357
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->h(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 358
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->d(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 359
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->e(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 361
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 360
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(Ljava/util/List;)Lcom/twitter/model/core/TwitterSocialProof$a;

    .line 362
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 318
    check-cast p2, Lcom/twitter/model/core/TwitterSocialProof$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/TwitterSocialProof$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/TwitterSocialProof$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/TwitterSocialProof;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 322
    iget v0, p2, Lcom/twitter/model/core/TwitterSocialProof;->b:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterSocialProof;->d:I

    .line 323
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterSocialProof;->e:I

    .line 324
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterSocialProof;->f:I

    .line 325
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterSocialProof;->c:Ljava/lang/String;

    .line 326
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterSocialProof;->g:I

    .line 327
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterSocialProof;->h:Ljava/lang/String;

    .line 328
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterSocialProof;->i:I

    .line 329
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterSocialProof;->j:Ljava/lang/String;

    .line 330
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/TwitterSocialProof;->k:I

    .line 331
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterSocialProof;->l:Ljava/lang/String;

    .line 332
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterSocialProof;->m:Ljava/lang/String;

    .line 333
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/TwitterSocialProof;->n:Ljava/util/List;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 335
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 334
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 336
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 318
    check-cast p2, Lcom/twitter/model/core/TwitterSocialProof;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/TwitterSocialProof$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/TwitterSocialProof;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/twitter/model/core/TwitterSocialProof$b;->a()Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    return-object v0
.end method
