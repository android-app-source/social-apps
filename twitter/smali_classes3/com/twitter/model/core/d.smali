.class public abstract Lcom/twitter/model/core/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/d$c;,
        Lcom/twitter/model/core/d$b;,
        Lcom/twitter/model/core/d$a;
    }
.end annotation


# static fields
.field public static final d:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/core/d;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/twitter/model/core/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final f:I

.field public g:I

.field public h:I

.field public i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 20
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/twitter/util/serialization/j;

    const/4 v1, 0x0

    const-class v2, Lcom/twitter/model/core/MediaEntity;

    new-instance v3, Lcom/twitter/model/core/MediaEntity$b;

    invoke-direct {v3}, Lcom/twitter/model/core/MediaEntity$b;-><init>()V

    .line 22
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/twitter/model/core/ad;

    new-instance v3, Lcom/twitter/model/core/ad$d;

    invoke-direct {v3}, Lcom/twitter/model/core/ad$d;-><init>()V

    .line 23
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/twitter/model/core/q;

    new-instance v3, Lcom/twitter/model/core/q$b;

    invoke-direct {v3}, Lcom/twitter/model/core/q$b;-><init>()V

    .line 24
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/twitter/model/core/h;

    new-instance v3, Lcom/twitter/model/core/h$b;

    invoke-direct {v3}, Lcom/twitter/model/core/h$b;-><init>()V

    .line 25
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/twitter/model/core/b;

    new-instance v3, Lcom/twitter/model/core/b$b;

    invoke-direct {v3}, Lcom/twitter/model/core/b$b;-><init>()V

    .line 26
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lcom/twitter/model/core/i;

    new-instance v3, Lcom/twitter/model/core/i$b;

    invoke-direct {v3}, Lcom/twitter/model/core/i$b;-><init>()V

    .line 27
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    .line 21
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a([Lcom/twitter/util/serialization/j;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/core/d;->d:Lcom/twitter/util/serialization/l;

    .line 33
    sget-object v0, Lcom/twitter/model/core/d$c;->a:Ljava/util/Comparator;

    sput-object v0, Lcom/twitter/model/core/d;->e:Ljava/util/Comparator;

    return-void
.end method

.method protected constructor <init>(Lcom/twitter/model/core/d$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput v0, p0, Lcom/twitter/model/core/d;->g:I

    .line 39
    iput v0, p0, Lcom/twitter/model/core/d;->h:I

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/model/core/d;->i:Z

    .line 43
    iget v0, p1, Lcom/twitter/model/core/d$a;->b:I

    iput v0, p0, Lcom/twitter/model/core/d;->f:I

    .line 44
    iget v0, p1, Lcom/twitter/model/core/d$a;->c:I

    iput v0, p0, Lcom/twitter/model/core/d;->g:I

    .line 45
    iget v0, p1, Lcom/twitter/model/core/d$a;->d:I

    iput v0, p0, Lcom/twitter/model/core/d;->h:I

    .line 46
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/twitter/model/core/d;->i:Z

    if-nez v0, :cond_0

    .line 69
    neg-int v0, p1

    invoke-virtual {p0, v0}, Lcom/twitter/model/core/d;->b(I)V

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/model/core/d;->i:Z

    .line 72
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/core/d;)Z
    .locals 2

    .prologue
    .line 58
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/d;->g:I

    iget v1, p1, Lcom/twitter/model/core/d;->g:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/model/core/d;->h:I

    iget v1, p1, Lcom/twitter/model/core/d;->h:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    iget v0, p0, Lcom/twitter/model/core/d;->g:I

    add-int/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/d;->g:I

    .line 76
    iget v0, p0, Lcom/twitter/model/core/d;->h:I

    add-int/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/d;->h:I

    .line 77
    return-void
.end method

.method public abstract bt_()Lcom/twitter/model/core/d$a;
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 80
    iget v0, p0, Lcom/twitter/model/core/d;->h:I

    add-int/2addr v0, p1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/d;->h:I

    .line 81
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lcom/twitter/model/core/d;->g:I

    iget v1, p0, Lcom/twitter/model/core/d;->h:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 53
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/core/d;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/core/d;

    invoke-virtual {p0, p1}, Lcom/twitter/model/core/d;->a(Lcom/twitter/model/core/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 64
    iget v0, p0, Lcom/twitter/model/core/d;->g:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/core/d;->h:I

    add-int/2addr v0, v1

    return v0
.end method
