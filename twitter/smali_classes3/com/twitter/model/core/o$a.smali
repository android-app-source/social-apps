.class Lcom/twitter/model/core/o$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/core/o;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/core/o$1;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/twitter/model/core/o$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/core/o;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v0

    .line 68
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v1

    .line 69
    sget-object v2, Lcom/twitter/model/core/p;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v2

    .line 71
    new-instance v3, Lcom/twitter/model/core/o;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/model/core/o;-><init>(FFLjava/util/List;)V

    return-object v3
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    iget v0, p2, Lcom/twitter/model/core/o;->b:F

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/core/o;->c:F

    .line 59
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    .line 60
    iget-object v0, p2, Lcom/twitter/model/core/o;->d:Ljava/util/List;

    sget-object v1, Lcom/twitter/model/core/p;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 61
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    check-cast p2, Lcom/twitter/model/core/o;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/o$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/o;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/o$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/core/o;

    move-result-object v0

    return-object v0
.end method
