.class public Lcom/twitter/model/core/MediaEntity$b;
.super Lcom/twitter/model/core/ad$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/MediaEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/core/ad$b",
        "<",
        "Lcom/twitter/model/core/MediaEntity;",
        "Lcom/twitter/model/core/MediaEntity$a;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 316
    invoke-direct {p0}, Lcom/twitter/model/core/ad$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/core/MediaEntity$a;
    .locals 1

    .prologue
    .line 347
    new-instance v0, Lcom/twitter/model/core/MediaEntity$a;

    invoke-direct {v0}, Lcom/twitter/model/core/MediaEntity$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/MediaEntity$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 354
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/model/core/ad$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/ad$a;I)V

    .line 355
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/core/MediaEntity$a;->a(J)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    .line 356
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/MediaEntity$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    .line 357
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-static {v1}, Lcom/twitter/model/core/MediaEntity$Type;->a(I)Lcom/twitter/model/core/MediaEntity$Type;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/MediaEntity$a;->a(Lcom/twitter/model/core/MediaEntity$Type;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/math/Size;->a:Lcom/twitter/util/serialization/l;

    .line 358
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/math/Size;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/MediaEntity$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    .line 359
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/MediaEntity$a;->b(J)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    .line 360
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/MediaEntity$a;->c(J)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/x;->a:Lcom/twitter/util/serialization/l;

    .line 361
    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/MediaEntity$a;->a(Ljava/util/List;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/n;->a:Lcom/twitter/util/serialization/l;

    .line 362
    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/MediaEntity$a;->b(Ljava/util/List;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/o;->a:Lcom/twitter/util/serialization/l;

    .line 363
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/o;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/MediaEntity$a;->a(Lcom/twitter/model/core/o;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/av/VideoCta;->a:Lcom/twitter/util/serialization/l;

    .line 364
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/VideoCta;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/MediaEntity$a;->a(Lcom/twitter/model/av/VideoCta;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    .line 365
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/MediaEntity$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    .line 366
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/MediaEntity$a;->a(Z)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    .line 367
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/MediaEntity$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    .line 368
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/MediaEntity$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    .line 369
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/MediaEntity$a;->b(Z)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    sget-object v1, Lcds;->a:Lcom/twitter/util/serialization/l;

    .line 370
    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/MediaEntity$a;->c(Ljava/util/List;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    .line 371
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/MediaEntity$a;->d(Ljava/lang/String;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/l;->a:Lcom/twitter/util/serialization/l;

    .line 372
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/l;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/MediaEntity$a;->a(Lcom/twitter/model/core/l;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/m;->a:Lcom/twitter/util/serialization/l;

    .line 373
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/m;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/MediaEntity$a;->a(Lcom/twitter/model/core/m;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/media/a;->a:Lcom/twitter/util/serialization/l;

    .line 374
    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/MediaEntity$a;->d(Ljava/util/List;)Lcom/twitter/model/core/MediaEntity$a;

    .line 375
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/ad$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 316
    check-cast p2, Lcom/twitter/model/core/MediaEntity$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/MediaEntity$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/MediaEntity$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 316
    check-cast p2, Lcom/twitter/model/core/MediaEntity$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/MediaEntity$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/MediaEntity$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 316
    check-cast p2, Lcom/twitter/model/core/MediaEntity$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/MediaEntity$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/MediaEntity$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/MediaEntity;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 321
    invoke-super {p0, p1, p2}, Lcom/twitter/model/core/ad$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/ad;)V

    .line 322
    iget-wide v0, p2, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/MediaEntity;->m:Ljava/lang/String;

    .line 323
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    iget v1, v1, Lcom/twitter/model/core/MediaEntity$Type;->typeId:I

    .line 324
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/MediaEntity;->o:Lcom/twitter/util/math/Size;

    sget-object v2, Lcom/twitter/util/math/Size;->a:Lcom/twitter/util/serialization/l;

    .line 325
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/core/MediaEntity;->j:J

    .line 326
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/core/MediaEntity;->k:J

    .line 327
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 328
    iget-object v0, p2, Lcom/twitter/model/core/MediaEntity;->r:Ljava/util/List;

    sget-object v1, Lcom/twitter/model/core/x;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 329
    iget-object v0, p2, Lcom/twitter/model/core/MediaEntity;->q:Ljava/util/List;

    sget-object v1, Lcom/twitter/model/core/n;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 330
    iget-object v0, p2, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    sget-object v1, Lcom/twitter/model/core/o;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/MediaEntity;->t:Lcom/twitter/model/av/VideoCta;

    sget-object v2, Lcom/twitter/model/av/VideoCta;->a:Lcom/twitter/util/serialization/l;

    .line 331
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/MediaEntity;->u:Ljava/lang/String;

    .line 332
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/MediaEntity;->v:Z

    .line 333
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/MediaEntity;->w:Ljava/lang/String;

    .line 334
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/MediaEntity;->l:Lcom/twitter/model/core/TwitterUser;

    sget-object v2, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    .line 335
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/core/MediaEntity;->x:Z

    .line 336
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 337
    iget-object v0, p2, Lcom/twitter/model/core/MediaEntity;->s:Ljava/util/List;

    sget-object v1, Lcds;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 338
    iget-object v0, p2, Lcom/twitter/model/core/MediaEntity;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 339
    iget-object v0, p2, Lcom/twitter/model/core/MediaEntity;->z:Lcom/twitter/model/core/l;

    sget-object v1, Lcom/twitter/model/core/l;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 340
    iget-object v0, p2, Lcom/twitter/model/core/MediaEntity;->A:Lcom/twitter/model/core/m;

    sget-object v1, Lcom/twitter/model/core/m;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 341
    iget-object v0, p2, Lcom/twitter/model/core/MediaEntity;->B:Ljava/util/List;

    sget-object v1, Lcom/twitter/model/media/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 342
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 316
    check-cast p2, Lcom/twitter/model/core/MediaEntity;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/MediaEntity$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/MediaEntity;)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 316
    check-cast p2, Lcom/twitter/model/core/MediaEntity;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/MediaEntity$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/MediaEntity;)V

    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 316
    check-cast p2, Lcom/twitter/model/core/MediaEntity;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/MediaEntity$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/MediaEntity;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/twitter/model/core/MediaEntity$b;->a()Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    return-object v0
.end method
