.class public Lcom/twitter/model/core/TwitterUser;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/twitter/model/core/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/TwitterUser$b;,
        Lcom/twitter/model/core/TwitterUser$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            "Lcom/twitter/model/core/TwitterUser$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Lcgi;

.field public final B:J

.field public final C:Lcom/twitter/model/core/v;

.field public final D:Lcom/twitter/model/core/v;

.field public final E:Lcbw;

.field public final F:Ljava/lang/String;

.field public final G:Ljava/lang/String;

.field public final H:Z

.field public final I:Z

.field public final J:I

.field public final K:Lcom/twitter/model/ads/AdvertiserType;

.field public final L:J

.field public final M:Lcom/twitter/model/businessprofiles/BusinessProfileState;

.field public final N:Z

.field public final O:Ljava/lang/String;

.field public final P:Lcom/twitter/model/profile/TranslatorType;

.field public final Q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;",
            ">;"
        }
    .end annotation
.end field

.field public transient R:I

.field public transient S:J

.field public transient T:Lcom/twitter/model/search/TwitterUserMetadata;

.field public transient U:I

.field public transient V:Lcom/twitter/model/timeline/r;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:I

.field public final i:I

.field public final j:Ljava/lang/String;

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:Z

.field public final p:Ljava/lang/String;

.field public final q:Lcom/twitter/util/collection/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Z

.field public final s:Lcom/twitter/model/profile/ExtendedProfile;

.field public final t:I

.field public final u:I

.field public final v:I

.field public final w:I

.field public final x:Z

.field public final y:J

.field public final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/twitter/model/core/TwitterUser$1;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$1;-><init>()V

    sput-object v0, Lcom/twitter/model/core/TwitterUser;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 55
    new-instance v0, Lcom/twitter/model/core/TwitterUser$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/core/TwitterUser$b;-><init>(Lcom/twitter/model/core/TwitterUser$1;)V

    sput-object v0, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 188
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    .line 189
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    .line 190
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    .line 191
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->F:Ljava/lang/String;

    .line 192
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    .line 193
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->R:I

    .line 194
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->t:I

    .line 195
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->g:Ljava/lang/String;

    .line 196
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->h:I

    .line 197
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->i:I

    .line 198
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->k:Z

    .line 199
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->l:Z

    .line 200
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->m:Z

    .line 201
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->p:Ljava/lang/String;

    .line 202
    sget-object v0, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    .line 203
    invoke-static {v0}, Lcom/twitter/util/collection/d;->c(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 202
    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->q:Lcom/twitter/util/collection/k;

    .line 204
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->u:I

    .line 205
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->S:J

    .line 206
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->v:I

    .line 207
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->w:I

    .line 208
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->x:Z

    .line 209
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 210
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->y:J

    .line 211
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->B:J

    .line 212
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->z:I

    .line 213
    sget-object v0, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    .line 214
    sget-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    .line 215
    sget-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->D:Lcom/twitter/model/core/v;

    .line 216
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/TwitterUserMetadata;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    .line 217
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->e:Ljava/lang/String;

    .line 218
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->G:Ljava/lang/String;

    .line 219
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->o:Z

    .line 220
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->n:Z

    .line 221
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->H:Z

    .line 222
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->I:Z

    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->J:I

    .line 224
    sget-object v0, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->s:Lcom/twitter/model/profile/ExtendedProfile;

    .line 225
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->r:Z

    .line 226
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/ads/AdvertiserType;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->K:Lcom/twitter/model/ads/AdvertiserType;

    .line 227
    sget-object v0, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    .line 228
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->L:J

    .line 229
    sget-object v0, Lcbw;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbw;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->E:Lcbw;

    .line 230
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/BusinessProfileState;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->M:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    :goto_9
    iput-boolean v1, p0, Lcom/twitter/model/core/TwitterUser;->N:Z

    .line 232
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/businessprofiles/h$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->O:Ljava/lang/String;

    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/TranslatorType;

    sget-object v1, Lcom/twitter/model/profile/TranslatorType;->a:Lcom/twitter/model/profile/TranslatorType;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/TranslatorType;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->P:Lcom/twitter/model/profile/TranslatorType;

    .line 234
    sget-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->i:Lcom/twitter/util/serialization/l;

    .line 235
    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 234
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->Q:Ljava/util/List;

    .line 236
    return-void

    :cond_0
    move v0, v2

    .line 198
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 199
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 200
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 208
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 219
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 220
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 221
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 222
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 225
    goto :goto_8

    :cond_9
    move v1, v2

    .line 231
    goto :goto_9
.end method

.method private constructor <init>(Lcom/twitter/model/core/TwitterUser$a;)V
    .locals 2

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser$a;->a:J

    iput-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 138
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    .line 139
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    .line 140
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    .line 141
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->G:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->F:Ljava/lang/String;

    .line 142
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    .line 143
    iget v0, p1, Lcom/twitter/model/core/TwitterUser$a;->s:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->R:I

    .line 144
    iget v0, p1, Lcom/twitter/model/core/TwitterUser$a;->t:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->t:I

    .line 145
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->g:Ljava/lang/String;

    .line 146
    iget v0, p1, Lcom/twitter/model/core/TwitterUser$a;->g:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->h:I

    .line 147
    iget v0, p1, Lcom/twitter/model/core/TwitterUser$a;->h:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->i:I

    .line 148
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser$a;->j:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->k:Z

    .line 149
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser$a;->k:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->l:Z

    .line 150
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser$a;->l:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->m:Z

    .line 151
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->p:Ljava/lang/String;

    .line 152
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->p:Lcom/twitter/util/collection/k;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->q:Lcom/twitter/util/collection/k;

    .line 153
    iget v0, p1, Lcom/twitter/model/core/TwitterUser$a;->u:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->u:I

    .line 154
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser$a;->v:J

    iput-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->S:J

    .line 155
    iget v0, p1, Lcom/twitter/model/core/TwitterUser$a;->w:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->v:I

    .line 156
    iget v0, p1, Lcom/twitter/model/core/TwitterUser$a;->x:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->w:I

    .line 157
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser$a;->y:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->x:Z

    .line 158
    iget v0, p1, Lcom/twitter/model/core/TwitterUser$a;->I:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 159
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser$a;->z:J

    iput-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->y:J

    .line 160
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser$a;->C:J

    iput-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->B:J

    .line 161
    iget v0, p1, Lcom/twitter/model/core/TwitterUser$a;->A:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->z:I

    .line 162
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->B:Lcgi;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    .line 163
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->D:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    .line 164
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->E:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->D:Lcom/twitter/model/core/v;

    .line 165
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->F:Lcom/twitter/model/search/TwitterUserMetadata;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    .line 166
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->e:Ljava/lang/String;

    .line 167
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->G:Ljava/lang/String;

    .line 168
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser$a;->n:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->o:Z

    .line 169
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser$a;->m:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->n:Z

    .line 170
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser$a;->J:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->H:Z

    .line 171
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser$a;->K:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->I:Z

    .line 172
    iget v0, p1, Lcom/twitter/model/core/TwitterUser$a;->L:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser;->J:I

    .line 173
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->r:Lcom/twitter/model/profile/ExtendedProfile;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->s:Lcom/twitter/model/profile/ExtendedProfile;

    .line 174
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser$a;->q:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->r:Z

    .line 175
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->M:Lcom/twitter/model/ads/AdvertiserType;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->K:Lcom/twitter/model/ads/AdvertiserType;

    .line 176
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->N:Lcom/twitter/model/timeline/r;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    .line 177
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser$a;->O:J

    iput-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->L:J

    .line 178
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->P:Lcbw;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->E:Lcbw;

    .line 179
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->Q:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->M:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 180
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser$a;->R:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->N:Z

    .line 181
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->S:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->O:Ljava/lang/String;

    .line 182
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->T:Lcom/twitter/model/profile/TranslatorType;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->P:Lcom/twitter/model/profile/TranslatorType;

    .line 183
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser$a;->U:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser;->Q:Ljava/util/List;

    .line 184
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/core/TwitterUser$a;Lcom/twitter/model/core/TwitterUser$1;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/twitter/model/core/TwitterUser;-><init>(Lcom/twitter/model/core/TwitterUser$a;)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 425
    iget-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->b:J

    return-wide v0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)Z
    .locals 4

    .prologue
    .line 322
    if-eq p0, p1, :cond_1

    if-eqz p1, :cond_2

    iget-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->S:J

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->S:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->z:I

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->z:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->R:I

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->R:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->t:I

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->t:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->u:I

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->u:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->U:I

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->U:I

    if-ne v0, v1, :cond_2

    iget-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->y:J

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->y:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->x:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/TwitterUser;->x:Z

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->k:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/TwitterUser;->k:Z

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->H:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/TwitterUser;->H:Z

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->I:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/TwitterUser;->I:Z

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->l:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->v:I

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->v:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->w:I

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->w:I

    if-ne v0, v1, :cond_2

    iget-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->m:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->n:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/TwitterUser;->n:Z

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->p:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->p:Ljava/lang/String;

    .line 340
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->q:Lcom/twitter/util/collection/k;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->q:Lcom/twitter/util/collection/k;

    .line 341
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->s:Lcom/twitter/model/profile/ExtendedProfile;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->s:Lcom/twitter/model/profile/ExtendedProfile;

    .line 342
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->r:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/TwitterUser;->r:Z

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    .line 344
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    .line 345
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->e:Ljava/lang/String;

    .line 346
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->G:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->G:Ljava/lang/String;

    .line 347
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->g:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->g:Ljava/lang/String;

    .line 348
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->h:I

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->h:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->i:I

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->i:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    .line 351
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    .line 352
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    .line 353
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->D:Lcom/twitter/model/core/v;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->D:Lcom/twitter/model/core/v;

    .line 354
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    .line 355
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->o:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/TwitterUser;->o:Z

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->K:Lcom/twitter/model/ads/AdvertiserType;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->K:Lcom/twitter/model/ads/AdvertiserType;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    .line 358
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->L:J

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->L:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->E:Lcbw;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->E:Lcbw;

    .line 360
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->M:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->M:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->N:Z

    iget-boolean v1, p1, Lcom/twitter/model/core/TwitterUser;->N:Z

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->O:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->O:Ljava/lang/String;

    .line 363
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->P:Lcom/twitter/model/profile/TranslatorType;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->P:Lcom/twitter/model/profile/TranslatorType;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->Q:Ljava/util/List;

    .line 365
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->Q:Ljava/util/List;

    .line 366
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->Q:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->Q:Ljava/util/List;

    .line 367
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 322
    :goto_0
    return v0

    .line 367
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 431
    iget-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->J:I

    invoke-static {v0}, Lcom/twitter/model/core/ae$a;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 420
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 4

    .prologue
    .line 309
    iget-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->S:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 318
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p0, p1}, Lcom/twitter/model/core/TwitterUser;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 313
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/model/core/TwitterUser;->S:J

    .line 314
    return-void
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 441
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->e:Ljava/lang/String;

    const-string/jumbo v1, "/sticky/default_profile_images/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    iget-object v0, v0, Lcom/twitter/model/search/TwitterUserMetadata;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 372
    iget-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-wide v6, p0, Lcom/twitter/model/core/TwitterUser;->b:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 373
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v3

    .line 374
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    .line 375
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->G:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->G:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v3

    .line 376
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v3

    .line 377
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v3

    .line 378
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/model/core/TwitterUser;->h:I

    add-int/2addr v0, v3

    .line 379
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/model/core/TwitterUser;->i:I

    add-int/2addr v0, v3

    .line 380
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v3

    .line 381
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->k:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_6
    add-int/2addr v0, v3

    .line 382
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->H:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_7
    add-int/2addr v0, v3

    .line 383
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->I:Z

    if-eqz v0, :cond_9

    move v0, v2

    :goto_8
    add-int/2addr v0, v3

    .line 384
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-eqz v0, :cond_a

    move v0, v2

    :goto_9
    add-int/2addr v0, v3

    .line 385
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v0, :cond_b

    move v0, v2

    :goto_a
    add-int/2addr v0, v3

    .line 386
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->n:Z

    if-eqz v0, :cond_c

    move v0, v2

    :goto_b
    add-int/2addr v0, v3

    .line 387
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->p:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_c
    add-int/2addr v0, v3

    .line 388
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->q:Lcom/twitter/util/collection/k;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->q:Lcom/twitter/util/collection/k;

    invoke-virtual {v0}, Lcom/twitter/util/collection/k;->hashCode()I

    move-result v0

    :goto_d
    add-int/2addr v0, v3

    .line 389
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->s:Lcom/twitter/model/profile/ExtendedProfile;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->s:Lcom/twitter/model/profile/ExtendedProfile;

    invoke-virtual {v0}, Lcom/twitter/model/profile/ExtendedProfile;->hashCode()I

    move-result v0

    :goto_e
    add-int/2addr v0, v3

    .line 390
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->r:Z

    if-eqz v0, :cond_10

    move v0, v2

    :goto_f
    add-int/2addr v0, v3

    .line 391
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/model/core/TwitterUser;->R:I

    add-int/2addr v0, v3

    .line 392
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/model/core/TwitterUser;->t:I

    add-int/2addr v0, v3

    .line 393
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/model/core/TwitterUser;->u:I

    add-int/2addr v0, v3

    .line 394
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->S:J

    iget-wide v6, p0, Lcom/twitter/model/core/TwitterUser;->S:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 395
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/model/core/TwitterUser;->v:I

    add-int/2addr v0, v3

    .line 396
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/model/core/TwitterUser;->w:I

    add-int/2addr v0, v3

    .line 397
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->x:Z

    if-eqz v0, :cond_11

    move v0, v2

    :goto_10
    add-int/2addr v0, v3

    .line 398
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/model/core/TwitterUser;->U:I

    add-int/2addr v0, v3

    .line 399
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/model/core/TwitterUser;->z:I

    add-int/2addr v0, v3

    .line 400
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    invoke-virtual {v0}, Lcgi;->hashCode()I

    move-result v0

    :goto_11
    add-int/2addr v0, v3

    .line 401
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    invoke-virtual {v0}, Lcom/twitter/model/core/v;->hashCode()I

    move-result v0

    :goto_12
    add-int/2addr v0, v3

    .line 402
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->D:Lcom/twitter/model/core/v;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->D:Lcom/twitter/model/core/v;

    invoke-virtual {v0}, Lcom/twitter/model/core/v;->hashCode()I

    move-result v0

    :goto_13
    add-int/2addr v0, v3

    .line 403
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    invoke-virtual {v0}, Lcom/twitter/model/search/TwitterUserMetadata;->hashCode()I

    move-result v0

    :goto_14
    add-int/2addr v0, v3

    .line 404
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->o:Z

    if-eqz v0, :cond_16

    move v0, v2

    :goto_15
    add-int/2addr v0, v3

    .line 405
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->B:J

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 406
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/core/TwitterUser;->K:Lcom/twitter/model/ads/AdvertiserType;

    invoke-virtual {v3}, Lcom/twitter/model/ads/AdvertiserType;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 407
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    invoke-virtual {v0}, Lcom/twitter/model/timeline/r;->hashCode()I

    move-result v0

    :goto_16
    add-int/2addr v0, v3

    .line 408
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->L:J

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 409
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->E:Lcbw;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->E:Lcbw;

    invoke-virtual {v0}, Lcbw;->hashCode()I

    move-result v0

    :goto_17
    add-int/2addr v0, v3

    .line 410
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/core/TwitterUser;->M:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    invoke-virtual {v3}, Lcom/twitter/model/businessprofiles/BusinessProfileState;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 411
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/twitter/model/core/TwitterUser;->N:Z

    if-eqz v3, :cond_19

    :goto_18
    add-int/2addr v0, v2

    .line 412
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/model/core/TwitterUser;->O:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 413
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/model/core/TwitterUser;->P:Lcom/twitter/model/profile/TranslatorType;

    invoke-virtual {v2}, Lcom/twitter/model/profile/TranslatorType;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 414
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/model/core/TwitterUser;->Q:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/model/core/TwitterUser;->Q:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 415
    return v0

    :cond_1
    move v0, v1

    .line 373
    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 374
    goto/16 :goto_1

    :cond_3
    move v0, v1

    .line 375
    goto/16 :goto_2

    :cond_4
    move v0, v1

    .line 376
    goto/16 :goto_3

    :cond_5
    move v0, v1

    .line 377
    goto/16 :goto_4

    :cond_6
    move v0, v1

    .line 380
    goto/16 :goto_5

    :cond_7
    move v0, v1

    .line 381
    goto/16 :goto_6

    :cond_8
    move v0, v1

    .line 382
    goto/16 :goto_7

    :cond_9
    move v0, v1

    .line 383
    goto/16 :goto_8

    :cond_a
    move v0, v1

    .line 384
    goto/16 :goto_9

    :cond_b
    move v0, v1

    .line 385
    goto/16 :goto_a

    :cond_c
    move v0, v1

    .line 386
    goto/16 :goto_b

    :cond_d
    move v0, v1

    .line 387
    goto/16 :goto_c

    :cond_e
    move v0, v1

    .line 388
    goto/16 :goto_d

    :cond_f
    move v0, v1

    .line 389
    goto/16 :goto_e

    :cond_10
    move v0, v1

    .line 390
    goto/16 :goto_f

    :cond_11
    move v0, v1

    .line 397
    goto/16 :goto_10

    :cond_12
    move v0, v1

    .line 400
    goto/16 :goto_11

    :cond_13
    move v0, v1

    .line 401
    goto/16 :goto_12

    :cond_14
    move v0, v1

    .line 402
    goto/16 :goto_13

    :cond_15
    move v0, v1

    .line 403
    goto/16 :goto_14

    :cond_16
    move v0, v1

    .line 404
    goto/16 :goto_15

    :cond_17
    move v0, v1

    .line 407
    goto/16 :goto_16

    :cond_18
    move v0, v1

    .line 409
    goto :goto_17

    :cond_19
    move v2, v1

    .line 411
    goto :goto_18
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 240
    iget-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 241
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 244
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->F:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 246
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->R:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 247
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->t:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 248
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 249
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 250
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 251
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->k:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 252
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 253
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 254
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->q:Lcom/twitter/util/collection/k;

    sget-object v3, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    .line 256
    invoke-static {v3}, Lcom/twitter/util/collection/d;->c(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 255
    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 257
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->u:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 258
    iget-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->S:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 259
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->v:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 260
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->w:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 261
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->x:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 262
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 263
    iget-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->y:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 264
    iget-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->B:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 265
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->z:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 266
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    sget-object v3, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 267
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    sget-object v3, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 268
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->D:Lcom/twitter/model/core/v;

    sget-object v3, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 269
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 270
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->G:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 272
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->o:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 273
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->n:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 274
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->H:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 275
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->I:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 276
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->J:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 277
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->s:Lcom/twitter/model/profile/ExtendedProfile;

    sget-object v3, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 278
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->r:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 279
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->K:Lcom/twitter/model/ads/AdvertiserType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 280
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    sget-object v3, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 281
    iget-wide v4, p0, Lcom/twitter/model/core/TwitterUser;->L:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 282
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->E:Lcbw;

    sget-object v3, Lcbw;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 283
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->M:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 284
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->N:Z

    if-eqz v0, :cond_9

    :goto_9
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 285
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->O:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 286
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->P:Lcom/twitter/model/profile/TranslatorType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 287
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->Q:Ljava/util/List;

    sget-object v1, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->i:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 289
    return-void

    :cond_0
    move v0, v2

    .line 251
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 252
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 253
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 261
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 272
    goto :goto_4

    :cond_5
    move v0, v2

    .line 273
    goto :goto_5

    :cond_6
    move v0, v2

    .line 274
    goto :goto_6

    :cond_7
    move v0, v2

    .line 275
    goto :goto_7

    :cond_8
    move v0, v2

    .line 278
    goto :goto_8

    :cond_9
    move v1, v2

    .line 284
    goto :goto_9
.end method
