.class public Lcom/twitter/model/core/ac;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/core/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/ac$a;
    }
.end annotation


# instance fields
.field public final A:Lcbc;

.field public final B:I

.field public final C:Z

.field public final D:J

.field public final E:I

.field public F:Lcom/twitter/model/core/TwitterUser;

.field public G:Z

.field public H:I

.field public I:Lcom/twitter/model/core/ac;

.field public J:J

.field public K:Z

.field public L:Lcom/twitter/model/search/e;

.field public M:J

.field public final a:J

.field public final b:Ljava/lang/String;

.field public c:Z

.field public final d:Lcom/twitter/model/core/v;

.field public final e:Ljava/lang/String;

.field public final f:Z

.field public final g:J

.field public final h:Ljava/lang/String;

.field public final i:J

.field public final j:J

.field public final k:Ljava/lang/String;

.field public final l:Z

.field public final m:J

.field public final n:Lcom/twitter/model/core/ac;

.field public final o:I

.field public final p:J

.field public final q:I

.field public final r:Ljava/lang/String;

.field public final s:Z

.field public final t:Z

.field public final u:Z

.field public final v:Ljava/lang/String;

.field public final w:Lcgi;

.field public final x:Lcom/twitter/model/geo/b;

.field public final y:Lcom/twitter/model/geo/TwitterPlace;

.field public final z:Lcax;


# direct methods
.method constructor <init>(Lcom/twitter/model/core/ac$a;)V
    .locals 2

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iget-wide v0, p1, Lcom/twitter/model/core/ac$a;->a:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac;->a:J

    .line 95
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->c:Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    .line 96
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->g:Lcom/twitter/model/core/v;

    sget-object v1, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    .line 97
    iget-wide v0, p1, Lcom/twitter/model/core/ac$a;->h:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac;->g:J

    .line 98
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->h:Ljava/lang/String;

    .line 99
    iget-wide v0, p1, Lcom/twitter/model/core/ac$a;->j:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac;->i:J

    .line 100
    iget-wide v0, p1, Lcom/twitter/model/core/ac$a;->k:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac;->j:J

    .line 101
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->k:Ljava/lang/String;

    .line 102
    iget-boolean v0, p1, Lcom/twitter/model/core/ac$a;->m:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac;->l:Z

    .line 103
    iget-wide v0, p1, Lcom/twitter/model/core/ac$a;->n:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac;->m:J

    .line 104
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->o:Lcom/twitter/model/core/ac;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    .line 105
    iget-boolean v0, p1, Lcom/twitter/model/core/ac$a;->p:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac;->G:Z

    .line 106
    iget v0, p1, Lcom/twitter/model/core/ac$a;->q:I

    iput v0, p0, Lcom/twitter/model/core/ac;->o:I

    .line 107
    iget v0, p1, Lcom/twitter/model/core/ac$a;->r:I

    iput v0, p0, Lcom/twitter/model/core/ac;->H:I

    .line 108
    iget-wide v0, p1, Lcom/twitter/model/core/ac$a;->s:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac;->p:J

    .line 109
    iget v0, p1, Lcom/twitter/model/core/ac$a;->t:I

    iput v0, p0, Lcom/twitter/model/core/ac;->q:I

    .line 110
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->u:Ljava/lang/String;

    const-string/jumbo v1, "und"

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->r:Ljava/lang/String;

    .line 111
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->v:Ljava/lang/String;

    .line 112
    iget-boolean v0, p1, Lcom/twitter/model/core/ac$a;->w:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac;->s:Z

    .line 113
    iget-boolean v0, p1, Lcom/twitter/model/core/ac$a;->x:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac;->t:Z

    .line 114
    iget-boolean v0, p1, Lcom/twitter/model/core/ac$a;->y:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac;->u:Z

    .line 115
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->z:Lcgi;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->w:Lcgi;

    .line 116
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->A:Lcom/twitter/model/geo/b;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->x:Lcom/twitter/model/geo/b;

    .line 117
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->B:Lcom/twitter/model/geo/TwitterPlace;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->y:Lcom/twitter/model/geo/TwitterPlace;

    .line 118
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->C:Lcom/twitter/model/core/ac;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->I:Lcom/twitter/model/core/ac;

    .line 119
    iget-wide v0, p1, Lcom/twitter/model/core/ac$a;->D:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac;->J:J

    .line 120
    iget-boolean v0, p1, Lcom/twitter/model/core/ac$a;->E:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac;->K:Z

    .line 122
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->F:Lcax;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->z:Lcax;

    :goto_0
    iput-object v0, p0, Lcom/twitter/model/core/ac;->z:Lcax;

    .line 124
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->G:Lcom/twitter/model/search/e;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    .line 125
    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/ac;->w:Lcgi;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/core/ac;->w:Lcgi;

    invoke-virtual {v0}, Lcgi;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 126
    :cond_0
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/core/ac;->M:J

    .line 130
    :goto_1
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->H:Lcbc;

    iput-object v0, p0, Lcom/twitter/model/core/ac;->A:Lcbc;

    .line 131
    iget v0, p1, Lcom/twitter/model/core/ac$a;->I:I

    iput v0, p0, Lcom/twitter/model/core/ac;->B:I

    .line 132
    iget-boolean v0, p1, Lcom/twitter/model/core/ac$a;->J:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/ac;->C:Z

    .line 133
    iget-wide v0, p1, Lcom/twitter/model/core/ac$a;->K:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac;->D:J

    .line 135
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/ac;->b:Ljava/lang/String;

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/twitter/model/core/ac;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    invoke-static {v0, v1}, Lcom/twitter/model/core/v;->a(Ljava/lang/StringBuilder;Lcom/twitter/model/core/v;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/ac;->e:Ljava/lang/String;

    .line 139
    iget-object v0, p0, Lcom/twitter/model/core/ac;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    invoke-static {v0, v1}, Lcom/twitter/model/core/v;->b(Ljava/lang/CharSequence;Lcom/twitter/model/core/v;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/core/ac;->f:Z

    .line 140
    iget v0, p1, Lcom/twitter/model/core/ac$a;->L:I

    iput v0, p0, Lcom/twitter/model/core/ac;->E:I

    .line 141
    return-void

    .line 122
    :cond_1
    iget-object v0, p1, Lcom/twitter/model/core/ac$a;->F:Lcax;

    goto :goto_0

    .line 128
    :cond_2
    iget-wide v0, p0, Lcom/twitter/model/core/ac;->g:J

    iput-wide v0, p0, Lcom/twitter/model/core/ac;->M:J

    goto :goto_1
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    iget-wide v0, v0, Lcom/twitter/model/core/ac;->a:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/twitter/model/core/ac;->a:J

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/ac;)Z
    .locals 4

    .prologue
    .line 149
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/ac;->a:J

    iget-wide v2, p1, Lcom/twitter/model/core/ac;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/twitter/model/core/ac;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    :cond_0
    return-object p0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "popular"

    iget-object v1, p0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    iget-object v1, v1, Lcom/twitter/model/search/e;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "news"

    iget-object v1, p0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    iget-object v1, v1, Lcom/twitter/model/search/e;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 145
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/core/ac;

    invoke-virtual {p0, p1}, Lcom/twitter/model/core/ac;->a(Lcom/twitter/model/core/ac;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/twitter/model/core/ac;->w:Lcgi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 4

    .prologue
    .line 186
    iget-object v0, p0, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/core/ac;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/twitter/model/core/ac;->A:Lcbc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 154
    iget-wide v0, p0, Lcom/twitter/model/core/ac;->a:J

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v0

    return v0
.end method
