.class public final Lcom/twitter/model/core/TwitterSocialProof$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/TwitterSocialProof;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/core/TwitterSocialProof;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:Ljava/lang/String;

.field f:I

.field g:Ljava/lang/String;

.field h:I

.field i:Ljava/lang/String;

.field j:I

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 193
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 178
    iput v1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->a:I

    .line 184
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->f:I

    .line 188
    iput v1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->j:I

    .line 194
    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 307
    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->a:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->f:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->j:I

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0

    .prologue
    .line 217
    iput p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->a:I

    .line 218
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->e:Ljava/lang/String;

    .line 224
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/twitter/model/core/TwitterSocialProof$a;"
        }
    .end annotation

    .prologue
    .line 301
    iput-object p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->m:Ljava/util/List;

    .line 302
    return-object p0
.end method

.method public b(I)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0

    .prologue
    .line 229
    iput p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->b:I

    .line 230
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->g:Ljava/lang/String;

    .line 266
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/twitter/model/core/TwitterSocialProof$a;->e()Lcom/twitter/model/core/TwitterSocialProof;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0

    .prologue
    .line 235
    iput p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->c:I

    .line 236
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->i:Ljava/lang/String;

    .line 278
    return-object p0
.end method

.method public d(I)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0

    .prologue
    .line 241
    iput p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->d:I

    .line 242
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->k:Ljava/lang/String;

    .line 290
    return-object p0
.end method

.method public e(I)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0

    .prologue
    .line 247
    iput p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->f:I

    .line 248
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->l:Ljava/lang/String;

    .line 296
    return-object p0
.end method

.method protected e()Lcom/twitter/model/core/TwitterSocialProof;
    .locals 1

    .prologue
    .line 314
    new-instance v0, Lcom/twitter/model/core/TwitterSocialProof;

    invoke-direct {v0, p0}, Lcom/twitter/model/core/TwitterSocialProof;-><init>(Lcom/twitter/model/core/TwitterSocialProof$a;)V

    return-object v0
.end method

.method public f(I)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->f:I

    invoke-static {v0, p1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->f:I

    .line 254
    return-object p0
.end method

.method public g(I)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0

    .prologue
    .line 271
    iput p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->h:I

    .line 272
    return-object p0
.end method

.method public h(I)Lcom/twitter/model/core/TwitterSocialProof$a;
    .locals 0

    .prologue
    .line 283
    iput p1, p0, Lcom/twitter/model/core/TwitterSocialProof$a;->j:I

    .line 284
    return-object p0
.end method
