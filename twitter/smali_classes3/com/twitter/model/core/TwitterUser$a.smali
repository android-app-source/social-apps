.class public final Lcom/twitter/model/core/TwitterUser$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/TwitterUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/core/TwitterUser;",
        ">;"
    }
.end annotation


# instance fields
.field A:I

.field B:Lcgi;

.field C:J

.field D:Lcom/twitter/model/core/v;

.field E:Lcom/twitter/model/core/v;

.field F:Lcom/twitter/model/search/TwitterUserMetadata;

.field G:Ljava/lang/String;

.field H:Ljava/lang/String;

.field I:I

.field J:Z

.field K:Z

.field L:I

.field M:Lcom/twitter/model/ads/AdvertiserType;

.field N:Lcom/twitter/model/timeline/r;

.field O:J

.field P:Lcbw;

.field Q:Lcom/twitter/model/businessprofiles/BusinessProfileState;

.field R:Z

.field S:Ljava/lang/String;

.field T:Lcom/twitter/model/profile/TranslatorType;

.field U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;",
            ">;"
        }
    .end annotation
.end field

.field a:J

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:I

.field h:I

.field i:Ljava/lang/String;

.field j:Z

.field k:Z

.field l:Z

.field m:Z

.field n:Z

.field o:Ljava/lang/String;

.field p:Lcom/twitter/util/collection/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation
.end field

.field q:Z

.field r:Lcom/twitter/model/profile/ExtendedProfile;

.field s:I

.field t:I

.field u:I

.field v:J

.field w:I

.field x:I

.field y:Z

.field z:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 504
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 450
    iput-wide v2, p0, Lcom/twitter/model/core/TwitterUser$a;->a:J

    .line 473
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->x:I

    .line 484
    const/16 v0, 0x80

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->I:I

    .line 488
    sget-object v0, Lcom/twitter/model/ads/AdvertiserType;->a:Lcom/twitter/model/ads/AdvertiserType;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->M:Lcom/twitter/model/ads/AdvertiserType;

    .line 491
    iput-wide v2, p0, Lcom/twitter/model/core/TwitterUser$a;->O:J

    .line 493
    sget-object v0, Lcom/twitter/model/businessprofiles/BusinessProfileState;->a:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->Q:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 496
    const-string/jumbo v0, "none"

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->S:Ljava/lang/String;

    .line 499
    sget-object v0, Lcom/twitter/model/profile/TranslatorType;->a:Lcom/twitter/model/profile/TranslatorType;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->T:Lcom/twitter/model/profile/TranslatorType;

    .line 505
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->U:Ljava/util/List;

    .line 506
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/TwitterUser;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 508
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 450
    iput-wide v2, p0, Lcom/twitter/model/core/TwitterUser$a;->a:J

    .line 473
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->x:I

    .line 484
    const/16 v0, 0x80

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->I:I

    .line 488
    sget-object v0, Lcom/twitter/model/ads/AdvertiserType;->a:Lcom/twitter/model/ads/AdvertiserType;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->M:Lcom/twitter/model/ads/AdvertiserType;

    .line 491
    iput-wide v2, p0, Lcom/twitter/model/core/TwitterUser$a;->O:J

    .line 493
    sget-object v0, Lcom/twitter/model/businessprofiles/BusinessProfileState;->a:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->Q:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 496
    const-string/jumbo v0, "none"

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->S:Ljava/lang/String;

    .line 499
    sget-object v0, Lcom/twitter/model/profile/TranslatorType;->a:Lcom/twitter/model/profile/TranslatorType;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->T:Lcom/twitter/model/profile/TranslatorType;

    .line 509
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    iput-wide v0, p0, Lcom/twitter/model/core/TwitterUser$a;->a:J

    .line 510
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->b:Ljava/lang/String;

    .line 511
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->c:Ljava/lang/String;

    .line 512
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->d:Ljava/lang/String;

    .line 513
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->e:Ljava/lang/String;

    .line 514
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->f:Ljava/lang/String;

    .line 515
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->h:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->g:I

    .line 516
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->i:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->h:I

    .line 517
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->i:Ljava/lang/String;

    .line 518
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->k:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser$a;->j:Z

    .line 519
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->l:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser$a;->k:Z

    .line 520
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->m:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser$a;->l:Z

    .line 521
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->n:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser$a;->m:Z

    .line 522
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->o:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser$a;->n:Z

    .line 523
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->o:Ljava/lang/String;

    .line 524
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->q:Lcom/twitter/util/collection/k;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->p:Lcom/twitter/util/collection/k;

    .line 525
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->r:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser$a;->q:Z

    .line 526
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->s:Lcom/twitter/model/profile/ExtendedProfile;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->r:Lcom/twitter/model/profile/ExtendedProfile;

    .line 527
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->R:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->s:I

    .line 528
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->t:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->t:I

    .line 529
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->u:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->u:I

    .line 530
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->S:J

    iput-wide v0, p0, Lcom/twitter/model/core/TwitterUser$a;->v:J

    .line 531
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->v:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->w:I

    .line 532
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->w:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->x:I

    .line 533
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->x:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser$a;->y:Z

    .line 534
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->y:J

    iput-wide v0, p0, Lcom/twitter/model/core/TwitterUser$a;->z:J

    .line 535
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->z:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->A:I

    .line 536
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->B:Lcgi;

    .line 537
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->B:J

    iput-wide v0, p0, Lcom/twitter/model/core/TwitterUser$a;->C:J

    .line 538
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->D:Lcom/twitter/model/core/v;

    .line 539
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->D:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->E:Lcom/twitter/model/core/v;

    .line 540
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->F:Lcom/twitter/model/search/TwitterUserMetadata;

    .line 541
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->G:Ljava/lang/String;

    .line 542
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->G:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->H:Ljava/lang/String;

    .line 543
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->U:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->I:I

    .line 544
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->H:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser$a;->J:Z

    .line 545
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->I:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser$a;->K:Z

    .line 546
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->J:I

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->L:I

    .line 547
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->K:Lcom/twitter/model/ads/AdvertiserType;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->M:Lcom/twitter/model/ads/AdvertiserType;

    .line 548
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->N:Lcom/twitter/model/timeline/r;

    .line 549
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->L:J

    iput-wide v0, p0, Lcom/twitter/model/core/TwitterUser$a;->O:J

    .line 550
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->E:Lcbw;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->P:Lcbw;

    .line 551
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->M:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->Q:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 552
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->N:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/TwitterUser$a;->R:Z

    .line 553
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->O:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->S:Ljava/lang/String;

    .line 554
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->P:Lcom/twitter/model/profile/TranslatorType;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->T:Lcom/twitter/model/profile/TranslatorType;

    .line 555
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->Q:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->U:Ljava/util/List;

    .line 556
    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 4

    .prologue
    .line 879
    iget-wide v0, p0, Lcom/twitter/model/core/TwitterUser$a;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected T_()Z
    .locals 2

    .prologue
    .line 884
    invoke-super {p0}, Lcom/twitter/util/object/i;->T_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 885
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Tried to build user with an invalid id."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 886
    const/4 v0, 0x0

    .line 888
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 613
    iput p1, p0, Lcom/twitter/model/core/TwitterUser$a;->g:I

    .line 614
    return-object p0
.end method

.method public a(J)Lcom/twitter/model/core/TwitterUser$a;
    .locals 1

    .prologue
    .line 570
    iput-wide p1, p0, Lcom/twitter/model/core/TwitterUser$a;->a:J

    .line 571
    return-object p0
.end method

.method public a(Lcbw;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 842
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->P:Lcbw;

    .line 843
    return-object p0
.end method

.method public a(Lcgi;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 748
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->B:Lcgi;

    .line 749
    return-object p0
.end method

.method public a(Lcom/twitter/model/ads/AdvertiserType;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 824
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->M:Lcom/twitter/model/ads/AdvertiserType;

    .line 825
    return-object p0
.end method

.method public a(Lcom/twitter/model/businessprofiles/BusinessProfileState;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 848
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->Q:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 849
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 760
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->D:Lcom/twitter/model/core/v;

    .line 761
    return-object p0
.end method

.method public a(Lcom/twitter/model/profile/ExtendedProfile;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 682
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->r:Lcom/twitter/model/profile/ExtendedProfile;

    .line 683
    return-object p0
.end method

.method public a(Lcom/twitter/model/profile/TranslatorType;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 860
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->T:Lcom/twitter/model/profile/TranslatorType;

    .line 861
    return-object p0
.end method

.method public a(Lcom/twitter/model/search/TwitterUserMetadata;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 772
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->F:Lcom/twitter/model/search/TwitterUserMetadata;

    .line 773
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 830
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->N:Lcom/twitter/model/timeline/r;

    .line 831
    return-object p0
.end method

.method public a(Lcom/twitter/util/collection/k;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;)",
            "Lcom/twitter/model/core/TwitterUser$a;"
        }
    .end annotation

    .prologue
    .line 676
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->p:Lcom/twitter/util/collection/k;

    .line 677
    return-object p0
.end method

.method public a(Ljava/lang/Boolean;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 1

    .prologue
    .line 873
    iget v0, p0, Lcom/twitter/model/core/TwitterUser$a;->L:I

    invoke-static {v0, p1}, Lcom/twitter/model/core/ae$a;->a(ILjava/lang/Boolean;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/core/TwitterUser$a;->L:I

    .line 874
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 564
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->S:Ljava/lang/String;

    .line 565
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;",
            ">;)",
            "Lcom/twitter/model/core/TwitterUser$a;"
        }
    .end annotation

    .prologue
    .line 867
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->U:Ljava/util/List;

    .line 868
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 636
    iput-boolean p1, p0, Lcom/twitter/model/core/TwitterUser$a;->j:Z

    .line 637
    return-object p0
.end method

.method public b(I)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 619
    iput p1, p0, Lcom/twitter/model/core/TwitterUser$a;->h:I

    .line 620
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/core/TwitterUser$a;
    .locals 1

    .prologue
    .line 712
    iput-wide p1, p0, Lcom/twitter/model/core/TwitterUser$a;->v:J

    .line 713
    return-object p0
.end method

.method public b(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 766
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->E:Lcom/twitter/model/core/v;

    .line 767
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 576
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->b:Ljava/lang/String;

    .line 577
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 646
    iput-boolean p1, p0, Lcom/twitter/model/core/TwitterUser$a;->k:Z

    .line 647
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 449
    invoke-virtual {p0}, Lcom/twitter/model/core/TwitterUser$a;->i()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 694
    iput p1, p0, Lcom/twitter/model/core/TwitterUser$a;->s:I

    .line 695
    return-object p0
.end method

.method public c(J)Lcom/twitter/model/core/TwitterUser$a;
    .locals 1

    .prologue
    .line 736
    iput-wide p1, p0, Lcom/twitter/model/core/TwitterUser$a;->z:J

    .line 737
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 1

    .prologue
    .line 582
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->c:Ljava/lang/String;

    .line 583
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->d:Ljava/lang/String;

    .line 584
    return-object p0

    .line 583
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Z)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 652
    iput-boolean p1, p0, Lcom/twitter/model/core/TwitterUser$a;->l:Z

    .line 653
    return-object p0
.end method

.method public d(I)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 700
    iput p1, p0, Lcom/twitter/model/core/TwitterUser$a;->t:I

    .line 701
    return-object p0
.end method

.method public d(J)Lcom/twitter/model/core/TwitterUser$a;
    .locals 1

    .prologue
    .line 754
    iput-wide p1, p0, Lcom/twitter/model/core/TwitterUser$a;->C:J

    .line 755
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 589
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->d:Ljava/lang/String;

    .line 590
    return-object p0
.end method

.method public d(Z)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 658
    iput-boolean p1, p0, Lcom/twitter/model/core/TwitterUser$a;->m:Z

    .line 659
    return-object p0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 559
    iget-wide v0, p0, Lcom/twitter/model/core/TwitterUser$a;->a:J

    return-wide v0
.end method

.method public e(I)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 706
    iput p1, p0, Lcom/twitter/model/core/TwitterUser$a;->u:I

    .line 707
    return-object p0
.end method

.method public e(J)Lcom/twitter/model/core/TwitterUser$a;
    .locals 1

    .prologue
    .line 836
    iput-wide p1, p0, Lcom/twitter/model/core/TwitterUser$a;->O:J

    .line 837
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 1

    .prologue
    .line 595
    if-eqz p1, :cond_0

    const-string/jumbo v0, "null"

    .line 596
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->e:Ljava/lang/String;

    .line 597
    return-object p0
.end method

.method public e(Z)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 664
    iput-boolean p1, p0, Lcom/twitter/model/core/TwitterUser$a;->n:Z

    .line 665
    return-object p0
.end method

.method public f(I)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 718
    iput p1, p0, Lcom/twitter/model/core/TwitterUser$a;->w:I

    .line 719
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 1

    .prologue
    .line 607
    if-eqz p1, :cond_0

    const-string/jumbo v0, "null"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->f:Ljava/lang/String;

    .line 608
    return-object p0
.end method

.method public f(Z)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 688
    iput-boolean p1, p0, Lcom/twitter/model/core/TwitterUser$a;->q:Z

    .line 689
    return-object p0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 641
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser$a;->k:Z

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 791
    iget v0, p0, Lcom/twitter/model/core/TwitterUser$a;->I:I

    return v0
.end method

.method public g(I)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 724
    iput p1, p0, Lcom/twitter/model/core/TwitterUser$a;->x:I

    .line 725
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 630
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->i:Ljava/lang/String;

    .line 631
    return-object p0
.end method

.method public g(Z)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 730
    iput-boolean p1, p0, Lcom/twitter/model/core/TwitterUser$a;->y:Z

    .line 731
    return-object p0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 813
    iget v0, p0, Lcom/twitter/model/core/TwitterUser$a;->L:I

    return v0
.end method

.method public h(I)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 742
    iput p1, p0, Lcom/twitter/model/core/TwitterUser$a;->A:I

    .line 743
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 1

    .prologue
    .line 670
    if-eqz p1, :cond_0

    const-string/jumbo v0, "null"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->o:Ljava/lang/String;

    .line 671
    return-object p0
.end method

.method public h(Z)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 802
    iput-boolean p1, p0, Lcom/twitter/model/core/TwitterUser$a;->J:Z

    .line 803
    return-object p0
.end method

.method public i(I)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 796
    iput p1, p0, Lcom/twitter/model/core/TwitterUser$a;->I:I

    .line 797
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 1

    .prologue
    .line 778
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->G:Ljava/lang/String;

    .line 779
    if-eqz p1, :cond_0

    .line 780
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/twitter/model/core/TwitterUser$a;->H:Ljava/lang/String;

    .line 781
    return-object p0

    .line 780
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i(Z)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 808
    iput-boolean p1, p0, Lcom/twitter/model/core/TwitterUser$a;->K:Z

    .line 809
    return-object p0
.end method

.method protected i()Lcom/twitter/model/core/TwitterUser;
    .locals 2

    .prologue
    .line 894
    new-instance v0, Lcom/twitter/model/core/TwitterUser;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/core/TwitterUser;-><init>(Lcom/twitter/model/core/TwitterUser$a;Lcom/twitter/model/core/TwitterUser$1;)V

    return-object v0
.end method

.method public j(I)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 818
    iput p1, p0, Lcom/twitter/model/core/TwitterUser$a;->L:I

    .line 819
    return-object p0
.end method

.method public j(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 786
    iput-object p1, p0, Lcom/twitter/model/core/TwitterUser$a;->H:Ljava/lang/String;

    .line 787
    return-object p0
.end method

.method public j(Z)Lcom/twitter/model/core/TwitterUser$a;
    .locals 0

    .prologue
    .line 854
    iput-boolean p1, p0, Lcom/twitter/model/core/TwitterUser$a;->R:Z

    .line 855
    return-object p0
.end method
