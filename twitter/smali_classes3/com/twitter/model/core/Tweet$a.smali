.class public Lcom/twitter/model/core/Tweet$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/Tweet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public A:Z

.field public B:Lcom/twitter/model/geo/TwitterPlace;

.field public C:Ljava/lang/String;

.field public D:I

.field public E:I

.field public F:Lcgi;

.field public G:J

.field public H:Z

.field public I:I

.field public J:Lcax;

.field public K:Lcaq;

.field public L:I

.field public M:I

.field public N:I

.field public O:J

.field public P:I

.field public Q:Ljava/lang/String;

.field public R:Lcom/twitter/model/timeline/r;

.field public S:Lcom/twitter/model/core/r;

.field public T:J

.field public U:Ljava/lang/Long;

.field public V:[Lcom/twitter/model/core/d;

.field public W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/media/EditableMedia;",
            ">;"
        }
    .end annotation
.end field

.field public X:I

.field public Y:Ljava/lang/String;

.field public Z:I

.field public a:Ljava/lang/String;

.field public aa:I

.field public ab:I

.field public ac:Ljava/lang/String;

.field public ad:I

.field public ae:Ljava/lang/String;

.field public af:I

.field public ag:Z

.field public ah:J

.field public ai:Ljava/lang/String;

.field public aj:I

.field public ak:I

.field public al:J

.field public am:J

.field public an:J

.field public ao:I

.field public ap:Ljava/lang/String;

.field public aq:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:Ljava/lang/String;

.field public h:J

.field public i:Ljava/lang/String;

.field public j:J

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:Z

.field public n:J

.field public o:J

.field public p:Ljava/lang/String;

.field public q:J

.field public r:Z

.field public s:Z

.field public t:J

.field public u:Z

.field public v:D

.field public w:D

.field public x:Lcom/twitter/model/core/v;

.field public y:Z

.field public z:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, -0x1

    .line 952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 958
    iput-wide v2, p0, Lcom/twitter/model/core/Tweet$a;->f:J

    .line 960
    iput-wide v2, p0, Lcom/twitter/model/core/Tweet$a;->h:J

    .line 1002
    iput v0, p0, Lcom/twitter/model/core/Tweet$a;->X:I

    .line 1008
    iput v0, p0, Lcom/twitter/model/core/Tweet$a;->ad:I

    .line 1014
    iput v0, p0, Lcom/twitter/model/core/Tweet$a;->aj:I

    .line 1018
    iput-wide v2, p0, Lcom/twitter/model/core/Tweet$a;->an:J

    .line 1019
    iput v0, p0, Lcom/twitter/model/core/Tweet$a;->ao:I

    return-void
.end method


# virtual methods
.method public a(D)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1145
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->v:D

    .line 1146
    return-object p0
.end method

.method public a(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1193
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->D:I

    .line 1194
    return-object p0
.end method

.method public a(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1049
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->f:J

    .line 1050
    return-object p0
.end method

.method public a(Lcaq;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1235
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->K:Lcaq;

    .line 1236
    return-object p0
.end method

.method public a(Lcax;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1229
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->J:Lcax;

    .line 1230
    return-object p0
.end method

.method public a(Lcgi;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1205
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->F:Lcgi;

    .line 1206
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/TwitterSocialProof;)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1419
    if-eqz p1, :cond_0

    .line 1420
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->b:I

    iput v0, p0, Lcom/twitter/model/core/Tweet$a;->X:I

    .line 1421
    iget-object v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet$a;->Y:Ljava/lang/String;

    .line 1422
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->d:I

    iput v0, p0, Lcom/twitter/model/core/Tweet$a;->Z:I

    .line 1423
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->i:I

    iput v0, p0, Lcom/twitter/model/core/Tweet$a;->aa:I

    .line 1424
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->e:I

    iput v0, p0, Lcom/twitter/model/core/Tweet$a;->ab:I

    .line 1425
    iget-object v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet$a;->ac:Ljava/lang/String;

    .line 1426
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->k:I

    iput v0, p0, Lcom/twitter/model/core/Tweet$a;->ad:I

    .line 1427
    iget-object v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet$a;->ae:Ljava/lang/String;

    .line 1428
    iget-object v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->n:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/core/Tweet$a;->aq:Ljava/util/List;

    .line 1430
    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/r;)Lcom/twitter/model/core/Tweet$a;
    .locals 2

    .prologue
    .line 1307
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->S:Lcom/twitter/model/core/r;

    .line 1308
    if-eqz p1, :cond_0

    .line 1309
    iget-wide v0, p1, Lcom/twitter/model/core/r;->e:J

    iput-wide v0, p0, Lcom/twitter/model/core/Tweet$a;->T:J

    .line 1310
    iget v0, p0, Lcom/twitter/model/core/Tweet$a;->D:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/twitter/model/core/Tweet$a;->D:I

    .line 1312
    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1157
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->x:Lcom/twitter/model/core/v;

    .line 1158
    return-object p0
.end method

.method public a(Lcom/twitter/model/geo/TwitterPlace;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1181
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->B:Lcom/twitter/model/geo/TwitterPlace;

    .line 1182
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1301
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->R:Lcom/twitter/model/timeline/r;

    .line 1302
    return-object p0
.end method

.method public a(Ljava/lang/Long;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1323
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->U:Ljava/lang/Long;

    .line 1324
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1025
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->a:Ljava/lang/String;

    .line 1026
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/core/Tweet$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/media/EditableMedia;",
            ">;)",
            "Lcom/twitter/model/core/Tweet$a;"
        }
    .end annotation

    .prologue
    .line 1335
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->W:Ljava/util/List;

    .line 1336
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1085
    iput-boolean p1, p0, Lcom/twitter/model/core/Tweet$a;->l:Z

    .line 1086
    return-object p0
.end method

.method public a([Lcom/twitter/model/core/d;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1329
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->V:[Lcom/twitter/model/core/d;

    .line 1330
    return-object p0
.end method

.method public a()Lcom/twitter/model/core/Tweet;
    .locals 2

    .prologue
    .line 1461
    new-instance v0, Lcom/twitter/model/core/Tweet;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/core/Tweet;-><init>(Lcom/twitter/model/core/Tweet$a;Lcom/twitter/model/core/Tweet$1;)V

    return-object v0
.end method

.method public b(D)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1151
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->w:D

    .line 1152
    return-object p0
.end method

.method public b(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1199
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->E:I

    .line 1200
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1061
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->h:J

    .line 1062
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1031
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->c:Ljava/lang/String;

    .line 1032
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1091
    iput-boolean p1, p0, Lcom/twitter/model/core/Tweet$a;->m:Z

    .line 1092
    return-object p0
.end method

.method public c(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1223
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->I:I

    .line 1224
    return-object p0
.end method

.method public c(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1073
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->j:J

    .line 1074
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1037
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->d:Ljava/lang/String;

    .line 1038
    return-object p0
.end method

.method public c(Z)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1121
    iput-boolean p1, p0, Lcom/twitter/model/core/Tweet$a;->r:Z

    .line 1122
    return-object p0
.end method

.method public d(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1241
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->L:I

    .line 1242
    return-object p0
.end method

.method public d(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1097
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->n:J

    .line 1098
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1043
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->e:Ljava/lang/String;

    .line 1044
    return-object p0
.end method

.method public d(Z)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1127
    iput-boolean p1, p0, Lcom/twitter/model/core/Tweet$a;->s:Z

    .line 1128
    return-object p0
.end method

.method public e(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1247
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->ak:I

    .line 1248
    return-object p0
.end method

.method public e(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1103
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->o:J

    .line 1104
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1055
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->g:Ljava/lang/String;

    .line 1056
    return-object p0
.end method

.method public e(Z)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1139
    iput-boolean p1, p0, Lcom/twitter/model/core/Tweet$a;->u:Z

    .line 1140
    return-object p0
.end method

.method public f(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1259
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->M:I

    .line 1260
    return-object p0
.end method

.method public f(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1115
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->q:J

    .line 1116
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1067
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->i:Ljava/lang/String;

    .line 1068
    return-object p0
.end method

.method public f(Z)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1163
    iput-boolean p1, p0, Lcom/twitter/model/core/Tweet$a;->y:Z

    .line 1164
    return-object p0
.end method

.method public g(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1265
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->N:I

    .line 1266
    return-object p0
.end method

.method public g(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1133
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->t:J

    .line 1134
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1079
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->k:Ljava/lang/String;

    .line 1080
    return-object p0
.end method

.method public g(Z)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1175
    iput-boolean p1, p0, Lcom/twitter/model/core/Tweet$a;->A:Z

    .line 1176
    return-object p0
.end method

.method public h(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1277
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->P:I

    .line 1278
    return-object p0
.end method

.method public h(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1169
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->z:J

    .line 1170
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1109
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->p:Ljava/lang/String;

    .line 1110
    return-object p0
.end method

.method public h(Z)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1217
    iput-boolean p1, p0, Lcom/twitter/model/core/Tweet$a;->H:Z

    .line 1218
    return-object p0
.end method

.method public i(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1295
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->aj:I

    .line 1296
    return-object p0
.end method

.method public i(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1211
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->G:J

    .line 1212
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1187
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->C:Ljava/lang/String;

    .line 1188
    return-object p0
.end method

.method public i(Z)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1395
    iput-boolean p1, p0, Lcom/twitter/model/core/Tweet$a;->ag:Z

    .line 1396
    return-object p0
.end method

.method public j(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1341
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->X:I

    .line 1342
    return-object p0
.end method

.method public j(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1253
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->al:J

    .line 1254
    return-object p0
.end method

.method public j(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1283
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->Q:Ljava/lang/String;

    .line 1284
    return-object p0
.end method

.method public k(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1353
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->Z:I

    .line 1354
    return-object p0
.end method

.method public k(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1271
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->O:J

    .line 1272
    return-object p0
.end method

.method public k(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1289
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->ai:Ljava/lang/String;

    .line 1290
    return-object p0
.end method

.method public l(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1359
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->aa:I

    .line 1360
    return-object p0
.end method

.method public l(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1317
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->T:J

    .line 1318
    return-object p0
.end method

.method public l(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1347
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->Y:Ljava/lang/String;

    .line 1348
    return-object p0
.end method

.method public m(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1365
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->ab:I

    .line 1366
    return-object p0
.end method

.method public m(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1401
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->ah:J

    .line 1402
    return-object p0
.end method

.method public m(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1371
    iput-object p1, p0, Lcom/twitter/model/core/Tweet$a;->ac:Ljava/lang/String;

    .line 1372
    return-object p0
.end method

.method public n(I)Lcom/twitter/model/core/Tweet$a;
    .locals 0

    .prologue
    .line 1389
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->af:I

    .line 1390
    return-object p0
.end method

.method public n(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1435
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->an:J

    .line 1436
    return-object p0
.end method

.method public o(I)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1441
    invoke-static {}, Lcom/twitter/model/core/Tweet;->aw()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1442
    iput p1, p0, Lcom/twitter/model/core/Tweet$a;->b:I

    .line 1444
    :cond_0
    return-object p0
.end method

.method public o(J)Lcom/twitter/model/core/Tweet$a;
    .locals 1

    .prologue
    .line 1449
    iput-wide p1, p0, Lcom/twitter/model/core/Tweet$a;->am:J

    .line 1450
    return-object p0
.end method
