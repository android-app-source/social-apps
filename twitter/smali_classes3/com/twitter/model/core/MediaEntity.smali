.class public Lcom/twitter/model/core/MediaEntity;
.super Lcom/twitter/model/core/ad;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/core/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/core/MediaEntity$b;,
        Lcom/twitter/model/core/MediaEntity$a;,
        Lcom/twitter/model/core/MediaEntity$Type;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/core/k;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Lcom/twitter/model/core/m;

.field public final B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/media/a;",
            ">;"
        }
    .end annotation
.end field

.field public final c:J

.field public final j:J

.field public final k:J

.field public final l:Lcom/twitter/model/core/TwitterUser;

.field public final m:Ljava/lang/String;

.field public final n:Lcom/twitter/model/core/MediaEntity$Type;

.field public final o:Lcom/twitter/util/math/Size;

.field public final p:Lcom/twitter/model/core/o;

.field public final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/n;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/x;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcds;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Lcom/twitter/model/av/VideoCta;

.field public final u:Ljava/lang/String;

.field public final v:Z

.field public final w:Ljava/lang/String;

.field public final x:Z

.field public final y:Ljava/lang/String;

.field public final z:Lcom/twitter/model/core/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/model/core/MediaEntity$b;

    invoke-direct {v0}, Lcom/twitter/model/core/MediaEntity$b;-><init>()V

    sput-object v0, Lcom/twitter/model/core/MediaEntity;->a:Lcom/twitter/util/serialization/l;

    .line 23
    sget-object v0, Lcom/twitter/model/core/k;->a:Lcom/twitter/util/serialization/l;

    sput-object v0, Lcom/twitter/model/core/MediaEntity;->b:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/MediaEntity$a;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/twitter/model/core/ad;-><init>(Lcom/twitter/model/core/ad$a;)V

    .line 85
    iget-wide v0, p1, Lcom/twitter/model/core/MediaEntity$a;->a:J

    iput-wide v0, p0, Lcom/twitter/model/core/MediaEntity;->c:J

    .line 86
    iget-wide v0, p1, Lcom/twitter/model/core/MediaEntity$a;->e:J

    iput-wide v0, p0, Lcom/twitter/model/core/MediaEntity;->j:J

    .line 87
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/core/MediaEntity;->E:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->m:Ljava/lang/String;

    .line 88
    iget-wide v0, p1, Lcom/twitter/model/core/MediaEntity$a;->f:J

    iput-wide v0, p0, Lcom/twitter/model/core/MediaEntity;->k:J

    .line 89
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->h:Lcom/twitter/model/core/MediaEntity$Type;

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    .line 90
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->i:Lcom/twitter/util/math/Size;

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->o:Lcom/twitter/util/math/Size;

    .line 91
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->j:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->r:Ljava/util/List;

    .line 92
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->l:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->q:Ljava/util/List;

    .line 93
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->m:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->s:Ljava/util/List;

    .line 94
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->k:Lcom/twitter/model/core/o;

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    .line 95
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->n:Lcom/twitter/model/av/VideoCta;

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->t:Lcom/twitter/model/av/VideoCta;

    .line 96
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->o:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->u:Ljava/lang/String;

    .line 97
    iget-boolean v0, p1, Lcom/twitter/model/core/MediaEntity$a;->p:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/MediaEntity;->v:Z

    .line 98
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->q:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->w:Ljava/lang/String;

    .line 99
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->r:Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->l:Lcom/twitter/model/core/TwitterUser;

    .line 100
    iget-boolean v0, p1, Lcom/twitter/model/core/MediaEntity$a;->s:Z

    iput-boolean v0, p0, Lcom/twitter/model/core/MediaEntity;->x:Z

    .line 101
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->t:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->y:Ljava/lang/String;

    .line 102
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->u:Lcom/twitter/model/core/l;

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->z:Lcom/twitter/model/core/l;

    .line 103
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->v:Lcom/twitter/model/core/m;

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->A:Lcom/twitter/model/core/m;

    .line 104
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity$a;->w:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/core/MediaEntity;->B:Ljava/util/List;

    .line 105
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/core/MediaEntity$a;
    .locals 1

    .prologue
    .line 110
    new-instance v0, Lcom/twitter/model/core/MediaEntity$a;

    invoke-direct {v0, p0}, Lcom/twitter/model/core/MediaEntity$a;-><init>(Lcom/twitter/model/core/MediaEntity;)V

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/MediaEntity;)Z
    .locals 4

    .prologue
    .line 119
    if-eq p0, p1, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/model/core/ad;->a(Lcom/twitter/model/core/ad;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/MediaEntity;->c:J

    iget-wide v2, p1, Lcom/twitter/model/core/MediaEntity;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic bt_()Lcom/twitter/model/core/d$a;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/model/core/MediaEntity;->a()Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    return-object v0
.end method

.method public bv_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->y:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic d()Lcom/twitter/model/core/ad$a;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/model/core/MediaEntity;->a()Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 115
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/core/MediaEntity;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/core/MediaEntity;

    invoke-virtual {p0, p1}, Lcom/twitter/model/core/MediaEntity;->a(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 130
    invoke-super {p0}, Lcom/twitter/model/core/ad;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
