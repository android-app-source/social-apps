.class public Lcom/twitter/model/core/b$b;
.super Lcom/twitter/model/core/d$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/core/d$b",
        "<",
        "Lcom/twitter/model/core/b;",
        "Lcom/twitter/model/core/b$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/model/core/d$b;-><init>(I)V

    .line 82
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/core/b$a;
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/twitter/model/core/b$a;

    invoke-direct {v0}, Lcom/twitter/model/core/b$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/b$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 102
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/model/core/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V

    .line 103
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/core/b$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/b$a;

    .line 104
    const/4 v0, 0x1

    if-ge p3, v0, :cond_0

    .line 106
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    .line 107
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    .line 109
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 79
    check-cast p2, Lcom/twitter/model/core/b$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/b$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/b$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 79
    check-cast p2, Lcom/twitter/model/core/b$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/b$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/b$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Lcom/twitter/model/core/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V

    .line 89
    iget-object v0, p2, Lcom/twitter/model/core/b;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 90
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    check-cast p2, Lcom/twitter/model/core/b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/b$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/b;)V

    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    check-cast p2, Lcom/twitter/model/core/b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/b$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/b;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/twitter/model/core/b$b;->a()Lcom/twitter/model/core/b$a;

    move-result-object v0

    return-object v0
.end method
