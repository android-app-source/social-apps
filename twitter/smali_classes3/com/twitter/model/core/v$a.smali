.class public final Lcom/twitter/model/core/v$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/core/v;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lcom/twitter/model/core/f$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/model/core/f$b",
            "<",
            "Lcom/twitter/model/core/ad;",
            ">;"
        }
    .end annotation
.end field

.field final b:Lcom/twitter/model/core/k$a;

.field final c:Lcom/twitter/model/core/f$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/model/core/f$b",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;"
        }
    .end annotation
.end field

.field final d:Lcom/twitter/model/core/f$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/model/core/f$b",
            "<",
            "Lcom/twitter/model/core/h;",
            ">;"
        }
    .end annotation
.end field

.field final e:Lcom/twitter/model/core/f$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/model/core/f$b",
            "<",
            "Lcom/twitter/model/core/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 277
    new-instance v0, Lcom/twitter/model/core/f$b;

    invoke-direct {v0}, Lcom/twitter/model/core/f$b;-><init>()V

    iput-object v0, p0, Lcom/twitter/model/core/v$a;->a:Lcom/twitter/model/core/f$b;

    .line 278
    new-instance v0, Lcom/twitter/model/core/k$a;

    invoke-direct {v0}, Lcom/twitter/model/core/k$a;-><init>()V

    iput-object v0, p0, Lcom/twitter/model/core/v$a;->b:Lcom/twitter/model/core/k$a;

    .line 279
    new-instance v0, Lcom/twitter/model/core/f$b;

    invoke-direct {v0}, Lcom/twitter/model/core/f$b;-><init>()V

    iput-object v0, p0, Lcom/twitter/model/core/v$a;->c:Lcom/twitter/model/core/f$b;

    .line 280
    new-instance v0, Lcom/twitter/model/core/f$b;

    invoke-direct {v0}, Lcom/twitter/model/core/f$b;-><init>()V

    iput-object v0, p0, Lcom/twitter/model/core/v$a;->d:Lcom/twitter/model/core/f$b;

    .line 281
    new-instance v0, Lcom/twitter/model/core/f$b;

    invoke-direct {v0}, Lcom/twitter/model/core/f$b;-><init>()V

    iput-object v0, p0, Lcom/twitter/model/core/v$a;->e:Lcom/twitter/model/core/f$b;

    .line 282
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/v;)V
    .locals 2

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 285
    new-instance v0, Lcom/twitter/model/core/f$b;

    iget-object v1, p1, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-direct {v0, v1}, Lcom/twitter/model/core/f$b;-><init>(Lcom/twitter/model/core/f;)V

    iput-object v0, p0, Lcom/twitter/model/core/v$a;->a:Lcom/twitter/model/core/f$b;

    .line 286
    new-instance v0, Lcom/twitter/model/core/k$a;

    iget-object v1, p1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-direct {v0, v1}, Lcom/twitter/model/core/k$a;-><init>(Lcom/twitter/model/core/k;)V

    iput-object v0, p0, Lcom/twitter/model/core/v$a;->b:Lcom/twitter/model/core/k$a;

    .line 287
    new-instance v0, Lcom/twitter/model/core/f$b;

    iget-object v1, p1, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-direct {v0, v1}, Lcom/twitter/model/core/f$b;-><init>(Lcom/twitter/model/core/f;)V

    iput-object v0, p0, Lcom/twitter/model/core/v$a;->c:Lcom/twitter/model/core/f$b;

    .line 288
    new-instance v0, Lcom/twitter/model/core/f$b;

    iget-object v1, p1, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-direct {v0, v1}, Lcom/twitter/model/core/f$b;-><init>(Lcom/twitter/model/core/f;)V

    iput-object v0, p0, Lcom/twitter/model/core/v$a;->d:Lcom/twitter/model/core/f$b;

    .line 289
    new-instance v0, Lcom/twitter/model/core/f$b;

    iget-object v1, p1, Lcom/twitter/model/core/v;->g:Lcom/twitter/model/core/f;

    invoke-direct {v0, v1}, Lcom/twitter/model/core/f$b;-><init>(Lcom/twitter/model/core/f;)V

    iput-object v0, p0, Lcom/twitter/model/core/v$a;->e:Lcom/twitter/model/core/f$b;

    .line 290
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/model/core/v$a;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->b:Lcom/twitter/model/core/k$a;

    invoke-virtual {v0, p1}, Lcom/twitter/model/core/k$a;->b(Lcom/twitter/model/core/d;)Lcom/twitter/model/core/f$a;

    .line 369
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/ad;)Lcom/twitter/model/core/v$a;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->a:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0, p1}, Lcom/twitter/model/core/f$b;->a(Lcom/twitter/model/core/d;)Lcom/twitter/model/core/f$a;

    .line 345
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/v$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/ad;",
            ">;)",
            "Lcom/twitter/model/core/v$a;"
        }
    .end annotation

    .prologue
    .line 338
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->a:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0, p1}, Lcom/twitter/model/core/f$b;->a(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/f$a;

    .line 339
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/k;)Lcom/twitter/model/core/v$a;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->b:Lcom/twitter/model/core/k$a;

    invoke-virtual {v0, p1}, Lcom/twitter/model/core/k$a;->a(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/f$a;

    .line 357
    return-object p0
.end method

.method public b(Lcom/twitter/model/core/ad;)Lcom/twitter/model/core/v$a;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->a:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0, p1}, Lcom/twitter/model/core/f$b;->b(Lcom/twitter/model/core/d;)Lcom/twitter/model/core/f$a;

    .line 351
    return-object p0
.end method

.method public b(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/v$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;)",
            "Lcom/twitter/model/core/v$a;"
        }
    .end annotation

    .prologue
    .line 374
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->c:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0, p1}, Lcom/twitter/model/core/f$b;->a(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/f$a;

    .line 375
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/twitter/model/core/v$a;->g()Lcom/twitter/model/core/v;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/v$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/h;",
            ">;)",
            "Lcom/twitter/model/core/v$a;"
        }
    .end annotation

    .prologue
    .line 392
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->d:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0, p1}, Lcom/twitter/model/core/f$b;->a(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/f$a;

    .line 393
    return-object p0
.end method

.method public d(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/v$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/b;",
            ">;)",
            "Lcom/twitter/model/core/v$a;"
        }
    .end annotation

    .prologue
    .line 410
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->e:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0, p1}, Lcom/twitter/model/core/f$b;->a(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/f$a;

    .line 411
    return-object p0
.end method

.method public e()Lcom/twitter/model/core/v$a;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->a:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0}, Lcom/twitter/model/core/f$b;->f()Lcom/twitter/model/core/f$a;

    .line 299
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->b:Lcom/twitter/model/core/k$a;

    invoke-virtual {v0}, Lcom/twitter/model/core/k$a;->f()Lcom/twitter/model/core/f$a;

    .line 300
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->c:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0}, Lcom/twitter/model/core/f$b;->f()Lcom/twitter/model/core/f$a;

    .line 301
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->d:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0}, Lcom/twitter/model/core/f$b;->f()Lcom/twitter/model/core/f$a;

    .line 302
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->e:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0}, Lcom/twitter/model/core/f$b;->f()Lcom/twitter/model/core/f$a;

    .line 303
    return-object p0
.end method

.method public f()Lcom/twitter/model/core/v$a;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/twitter/model/core/v$a;->d:Lcom/twitter/model/core/f$b;

    invoke-virtual {v0}, Lcom/twitter/model/core/f$b;->f()Lcom/twitter/model/core/f$a;

    .line 327
    return-object p0
.end method

.method protected g()Lcom/twitter/model/core/v;
    .locals 1

    .prologue
    .line 429
    new-instance v0, Lcom/twitter/model/core/v;

    invoke-direct {v0, p0}, Lcom/twitter/model/core/v;-><init>(Lcom/twitter/model/core/v$a;)V

    return-object v0
.end method
