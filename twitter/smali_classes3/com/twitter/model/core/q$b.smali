.class public Lcom/twitter/model/core/q$b;
.super Lcom/twitter/model/core/d$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/core/d$b",
        "<",
        "Lcom/twitter/model/core/q;",
        "Lcom/twitter/model/core/q$a;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/model/core/d$b;-><init>(I)V

    .line 109
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/core/q$a;
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lcom/twitter/model/core/q$a;

    invoke-direct {v0}, Lcom/twitter/model/core/q$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 106
    check-cast p2, Lcom/twitter/model/core/q$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/q$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/q$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/q$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/model/core/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/d$a;I)V

    .line 132
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/core/q$a;->a(J)Lcom/twitter/model/core/q$a;

    move-result-object v0

    .line 133
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/q$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/q$a;

    move-result-object v0

    .line 134
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/q$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/q$a;

    .line 135
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 106
    check-cast p2, Lcom/twitter/model/core/q$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/core/q$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/core/q$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    check-cast p2, Lcom/twitter/model/core/q;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/q$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/q;)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/q;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    invoke-super {p0, p1, p2}, Lcom/twitter/model/core/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/d;)V

    .line 116
    iget-wide v0, p2, Lcom/twitter/model/core/q;->c:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/q;->j:Ljava/lang/String;

    .line 117
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/core/q;->k:Ljava/lang/String;

    .line 118
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 119
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    check-cast p2, Lcom/twitter/model/core/q;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/q$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/core/q;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/twitter/model/core/q$b;->a()Lcom/twitter/model/core/q$a;

    move-result-object v0

    return-object v0
.end method
