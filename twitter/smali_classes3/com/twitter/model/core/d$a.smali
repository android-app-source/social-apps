.class public abstract Lcom/twitter/model/core/d$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/core/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/twitter/model/core/d;",
        "B:",
        "Lcom/twitter/model/core/d$a",
        "<TE;TB;>;>",
        "Lcom/twitter/util/object/i",
        "<TE;>;"
    }
.end annotation


# instance fields
.field b:I

.field c:I

.field d:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 93
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 89
    iput v0, p0, Lcom/twitter/model/core/d$a;->b:I

    .line 90
    iput v0, p0, Lcom/twitter/model/core/d$a;->c:I

    .line 91
    iput v0, p0, Lcom/twitter/model/core/d$a;->d:I

    .line 94
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/model/core/d;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 96
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 89
    iput v0, p0, Lcom/twitter/model/core/d$a;->b:I

    .line 90
    iput v0, p0, Lcom/twitter/model/core/d$a;->c:I

    .line 91
    iput v0, p0, Lcom/twitter/model/core/d$a;->d:I

    .line 97
    iget v0, p1, Lcom/twitter/model/core/d;->f:I

    iput v0, p0, Lcom/twitter/model/core/d$a;->b:I

    .line 98
    iget v0, p1, Lcom/twitter/model/core/d;->g:I

    iput v0, p0, Lcom/twitter/model/core/d$a;->c:I

    .line 99
    iget v0, p1, Lcom/twitter/model/core/d;->h:I

    iput v0, p0, Lcom/twitter/model/core/d$a;->d:I

    .line 100
    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/model/core/d$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .prologue
    .line 108
    iput p1, p0, Lcom/twitter/model/core/d$a;->c:I

    .line 109
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d$a;

    return-object v0
.end method

.method public b(I)Lcom/twitter/model/core/d$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .prologue
    .line 118
    iput p1, p0, Lcom/twitter/model/core/d$a;->d:I

    .line 119
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d$a;

    return-object v0
.end method

.method public c(I)Lcom/twitter/model/core/d$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .prologue
    .line 124
    iput p1, p0, Lcom/twitter/model/core/d$a;->b:I

    .line 125
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d$a;

    return-object v0
.end method

.method protected c_()V
    .locals 2

    .prologue
    .line 130
    invoke-super {p0}, Lcom/twitter/util/object/i;->c_()V

    .line 131
    iget v0, p0, Lcom/twitter/model/core/d$a;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 132
    iget v0, p0, Lcom/twitter/model/core/d$a;->c:I

    iput v0, p0, Lcom/twitter/model/core/d$a;->b:I

    .line 134
    :cond_0
    return-void
.end method

.method public f()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/twitter/model/core/d$a;->d:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/twitter/model/core/d$a;->c:I

    return v0
.end method
