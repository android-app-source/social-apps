.class Lcom/twitter/model/ads/b$a;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/ads/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/ads/b;",
        "Lcom/twitter/model/ads/b$b;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/ads/b$1;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/twitter/model/ads/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/ads/b$b;
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/twitter/model/ads/b$b;

    invoke-direct {v0}, Lcom/twitter/model/ads/b$b;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/ads/b$b;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    sget-object v1, Lcom/twitter/model/ads/a;->a:Lcom/twitter/util/serialization/b;

    .line 121
    invoke-static {v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 120
    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/ads/b$b;->a(Ljava/util/Map;)Lcom/twitter/model/ads/b$b;

    .line 122
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 103
    check-cast p2, Lcom/twitter/model/ads/b$b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/ads/b$a;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/ads/b$b;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/ads/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p2, Lcom/twitter/model/ads/b;->b:Ljava/util/Map;

    sget-object v1, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/model/ads/a;->a:Lcom/twitter/util/serialization/b;

    .line 108
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 107
    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Map;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)V

    .line 109
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    check-cast p2, Lcom/twitter/model/ads/b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/ads/b$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/ads/b;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/twitter/model/ads/b$a;->a()Lcom/twitter/model/ads/b$b;

    move-result-object v0

    return-object v0
.end method
