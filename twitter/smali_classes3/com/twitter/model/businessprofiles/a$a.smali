.class Lcom/twitter/model/businessprofiles/a$a;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/businessprofiles/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/businessprofiles/a;",
        "Lcom/twitter/model/businessprofiles/a$b;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/businessprofiles/a$1;)V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/twitter/model/businessprofiles/a$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/businessprofiles/a$b;
    .locals 1

    .prologue
    .line 152
    new-instance v0, Lcom/twitter/model/businessprofiles/a$b;

    invoke-direct {v0}, Lcom/twitter/model/businessprofiles/a$b;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/a$b;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 158
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/businessprofiles/a$b;->a(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    .line 159
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->b(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    .line 160
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->c(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    .line 161
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->d(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    .line 162
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->e(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    .line 163
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->f(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    .line 164
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->g(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/geo/b;->a:Lcom/twitter/util/serialization/l;

    .line 165
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/b;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/a$b;->a(Lcom/twitter/model/geo/b;)Lcom/twitter/model/businessprofiles/a$b;

    .line 166
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 136
    check-cast p2, Lcom/twitter/model/businessprofiles/a$b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/businessprofiles/a$a;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/a$b;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p2, Lcom/twitter/model/businessprofiles/a;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/a;->c:Ljava/lang/String;

    .line 140
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/a;->d:Ljava/lang/String;

    .line 141
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/a;->e:Ljava/lang/String;

    .line 142
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/a;->f:Ljava/lang/String;

    .line 143
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/a;->g:Ljava/lang/String;

    .line 144
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/a;->h:Ljava/lang/String;

    .line 145
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/a;->i:Lcom/twitter/model/geo/b;

    sget-object v2, Lcom/twitter/model/geo/b;->a:Lcom/twitter/util/serialization/l;

    .line 146
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 147
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    check-cast p2, Lcom/twitter/model/businessprofiles/a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/businessprofiles/a$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/a;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/twitter/model/businessprofiles/a$a;->a()Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    return-object v0
.end method
