.class Lcom/twitter/model/businessprofiles/g$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/businessprofiles/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/businessprofiles/g;",
        "Lcom/twitter/model/businessprofiles/g$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/businessprofiles/g$1;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/twitter/model/businessprofiles/g$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/businessprofiles/g$a;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/twitter/model/businessprofiles/g$a;

    invoke-direct {v0}, Lcom/twitter/model/businessprofiles/g$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/g$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 86
    sget-object v0, Lcom/twitter/model/businessprofiles/a;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/a;

    invoke-virtual {p2, v0}, Lcom/twitter/model/businessprofiles/g$a;->a(Lcom/twitter/model/businessprofiles/a;)Lcom/twitter/model/businessprofiles/g$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/businessprofiles/l;->a:Lcom/twitter/util/serialization/l;

    .line 87
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/g$a;->a(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;)Lcom/twitter/model/businessprofiles/g$a;

    .line 88
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 69
    check-cast p2, Lcom/twitter/model/businessprofiles/g$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/businessprofiles/g$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/g$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/g;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p2, Lcom/twitter/model/businessprofiles/g;->b:Lcom/twitter/model/businessprofiles/a;

    sget-object v1, Lcom/twitter/model/businessprofiles/a;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/g;->c:Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    sget-object v2, Lcom/twitter/model/businessprofiles/l;->a:Lcom/twitter/util/serialization/l;

    .line 74
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 75
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    check-cast p2, Lcom/twitter/model/businessprofiles/g;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/businessprofiles/g$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/g;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/twitter/model/businessprofiles/g$b;->a()Lcom/twitter/model/businessprofiles/g$a;

    move-result-object v0

    return-object v0
.end method
