.class Lcom/twitter/model/businessprofiles/c$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/businessprofiles/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/businessprofiles/c;",
        "Lcom/twitter/model/businessprofiles/c$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/businessprofiles/j;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 305
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    .line 306
    sget-object v0, Lcom/twitter/model/businessprofiles/j;->a:Lcom/twitter/util/serialization/l;

    .line 307
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 306
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/businessprofiles/c$1;)V
    .locals 0

    .prologue
    .line 305
    invoke-direct {p0}, Lcom/twitter/model/businessprofiles/c$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/businessprofiles/c$a;
    .locals 1

    .prologue
    .line 325
    new-instance v0, Lcom/twitter/model/businessprofiles/c$a;

    invoke-direct {v0}, Lcom/twitter/model/businessprofiles/c$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/c$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 331
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/businessprofiles/c$a;->a(Ljava/util/TimeZone;)Lcom/twitter/model/businessprofiles/c$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 332
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/c$a;->a(Ljava/util/List;)Lcom/twitter/model/businessprofiles/c$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 333
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/c$a;->b(Ljava/util/List;)Lcom/twitter/model/businessprofiles/c$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 334
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/c$a;->c(Ljava/util/List;)Lcom/twitter/model/businessprofiles/c$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 335
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/c$a;->d(Ljava/util/List;)Lcom/twitter/model/businessprofiles/c$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 336
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/c$a;->e(Ljava/util/List;)Lcom/twitter/model/businessprofiles/c$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 337
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/c$a;->f(Ljava/util/List;)Lcom/twitter/model/businessprofiles/c$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 338
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/c$a;->g(Ljava/util/List;)Lcom/twitter/model/businessprofiles/c$a;

    .line 339
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 305
    check-cast p2, Lcom/twitter/model/businessprofiles/c$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/businessprofiles/c$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/c$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 312
    iget-object v0, p2, Lcom/twitter/model/businessprofiles/c;->c:Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/c;->d:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 313
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/c;->e:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 314
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/c;->f:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 315
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/c;->g:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 316
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/c;->h:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 317
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/c;->i:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 318
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/c;->j:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/c$b;->a:Lcom/twitter/util/serialization/l;

    .line 319
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 320
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 305
    check-cast p2, Lcom/twitter/model/businessprofiles/c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/businessprofiles/c$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/c;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/twitter/model/businessprofiles/c$b;->a()Lcom/twitter/model/businessprofiles/c$a;

    move-result-object v0

    return-object v0
.end method
