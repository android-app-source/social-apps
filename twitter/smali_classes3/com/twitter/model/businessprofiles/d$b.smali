.class Lcom/twitter/model/businessprofiles/d$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/businessprofiles/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/businessprofiles/d;",
        "Lcom/twitter/model/businessprofiles/d$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/businessprofiles/d$1;)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/twitter/model/businessprofiles/d$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/businessprofiles/d$a;
    .locals 1

    .prologue
    .line 140
    new-instance v0, Lcom/twitter/model/businessprofiles/d$a;

    invoke-direct {v0}, Lcom/twitter/model/businessprofiles/d$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/d$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 146
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/businessprofiles/d$a;->a(J)Lcom/twitter/model/businessprofiles/d$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/businessprofiles/g;->a:Lcom/twitter/util/serialization/b;

    .line 147
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/g;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/d$a;->a(Lcom/twitter/model/businessprofiles/g;)Lcom/twitter/model/businessprofiles/d$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/businessprofiles/c;->a:Lcom/twitter/util/serialization/b;

    .line 148
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/c;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/d$a;->a(Lcom/twitter/model/businessprofiles/c;)Lcom/twitter/model/businessprofiles/d$a;

    move-result-object v1

    const-class v0, Lcom/twitter/model/businessprofiles/KeyEngagementType;

    .line 149
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 150
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/KeyEngagementType;

    .line 149
    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/d$a;->a(Lcom/twitter/model/businessprofiles/KeyEngagementType;)Lcom/twitter/model/businessprofiles/d$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/businessprofiles/b;->a:Lcom/twitter/util/serialization/b;

    .line 151
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/b;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/d$a;->a(Lcom/twitter/model/businessprofiles/b;)Lcom/twitter/model/businessprofiles/d$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/businessprofiles/f;->a:Lcom/twitter/util/serialization/b;

    .line 152
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/f;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/d$a;->a(Lcom/twitter/model/businessprofiles/f;)Lcom/twitter/model/businessprofiles/d$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/businessprofiles/e;->a:Lcom/twitter/util/serialization/l;

    .line 153
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/e;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/d$a;->a(Lcom/twitter/model/businessprofiles/e;)Lcom/twitter/model/businessprofiles/d$a;

    .line 154
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 123
    check-cast p2, Lcom/twitter/model/businessprofiles/d$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/businessprofiles/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/d$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    iget-wide v0, p2, Lcom/twitter/model/businessprofiles/d;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/d;->c:Lcom/twitter/model/businessprofiles/g;

    sget-object v2, Lcom/twitter/model/businessprofiles/g;->a:Lcom/twitter/util/serialization/b;

    .line 128
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/d;->d:Lcom/twitter/model/businessprofiles/c;

    sget-object v2, Lcom/twitter/model/businessprofiles/c;->a:Lcom/twitter/util/serialization/b;

    .line 129
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/d;->e:Lcom/twitter/model/businessprofiles/KeyEngagementType;

    const-class v2, Lcom/twitter/model/businessprofiles/KeyEngagementType;

    .line 131
    invoke-static {v2}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 130
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/d;->f:Lcom/twitter/model/businessprofiles/b;

    sget-object v2, Lcom/twitter/model/businessprofiles/b;->a:Lcom/twitter/util/serialization/b;

    .line 132
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/d;->g:Lcom/twitter/model/businessprofiles/f;

    sget-object v2, Lcom/twitter/model/businessprofiles/f;->a:Lcom/twitter/util/serialization/b;

    .line 133
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/d;->h:Lcom/twitter/model/businessprofiles/e;

    sget-object v2, Lcom/twitter/model/businessprofiles/e;->a:Lcom/twitter/util/serialization/l;

    .line 134
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 135
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    check-cast p2, Lcom/twitter/model/businessprofiles/d;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/businessprofiles/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/d;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/twitter/model/businessprofiles/d$b;->a()Lcom/twitter/model/businessprofiles/d$a;

    move-result-object v0

    return-object v0
.end method
