.class Lcom/twitter/model/businessprofiles/e$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/businessprofiles/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/businessprofiles/e;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/businessprofiles/e$1;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/twitter/model/businessprofiles/e$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/businessprofiles/e;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 72
    sget-object v0, Lcom/twitter/model/businessprofiles/k;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/k;

    .line 73
    const-class v1, Lcom/twitter/model/businessprofiles/ResponsivenessLevel;

    invoke-static {v1}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 74
    invoke-virtual {v1, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/businessprofiles/ResponsivenessLevel;

    .line 75
    sget-object v2, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {v2, p1}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/TwitterUser;

    .line 76
    new-instance v3, Lcom/twitter/model/businessprofiles/e;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/model/businessprofiles/e;-><init>(Lcom/twitter/model/businessprofiles/k;Lcom/twitter/model/businessprofiles/ResponsivenessLevel;Lcom/twitter/model/core/TwitterUser;)V

    return-object v3
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/e;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p2, Lcom/twitter/model/businessprofiles/e;->b:Lcom/twitter/model/businessprofiles/k;

    sget-object v1, Lcom/twitter/model/businessprofiles/k;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/e;->c:Lcom/twitter/model/businessprofiles/ResponsivenessLevel;

    const-class v2, Lcom/twitter/model/businessprofiles/ResponsivenessLevel;

    .line 64
    invoke-static {v2}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 63
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/e;->d:Lcom/twitter/model/core/TwitterUser;

    sget-object v2, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    .line 65
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 66
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    check-cast p2, Lcom/twitter/model/businessprofiles/e;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/businessprofiles/e$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/e;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/businessprofiles/e$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/businessprofiles/e;

    move-result-object v0

    return-object v0
.end method
