.class Lcom/twitter/model/businessprofiles/b$a;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/businessprofiles/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/businessprofiles/b;",
        "Lcom/twitter/model/businessprofiles/b$b;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/businessprofiles/b$1;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/twitter/model/businessprofiles/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/businessprofiles/b$b;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/twitter/model/businessprofiles/b$b;

    invoke-direct {v0}, Lcom/twitter/model/businessprofiles/b$b;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/b$b;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/businessprofiles/b$b;->a(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/b$b;

    move-result-object v0

    .line 112
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/b$b;->b(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/b$b;

    move-result-object v0

    .line 113
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/b$b;->c(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/b$b;

    move-result-object v1

    sget-object v0, Lcax;->a:Lcom/twitter/util/serialization/l;

    .line 114
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/b$b;->a(Lcax;)Lcom/twitter/model/businessprofiles/b$b;

    .line 115
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 92
    check-cast p2, Lcom/twitter/model/businessprofiles/b$b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/businessprofiles/b$a;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/b$b;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p2, Lcom/twitter/model/businessprofiles/b;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/b;->c:Ljava/lang/String;

    .line 97
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/b;->d:Ljava/lang/String;

    .line 98
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/b;->e:Lcax;

    sget-object v2, Lcax;->a:Lcom/twitter/util/serialization/l;

    .line 99
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 100
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    check-cast p2, Lcom/twitter/model/businessprofiles/b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/businessprofiles/b$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/b;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/twitter/model/businessprofiles/b$a;->a()Lcom/twitter/model/businessprofiles/b$b;

    move-result-object v0

    return-object v0
.end method
