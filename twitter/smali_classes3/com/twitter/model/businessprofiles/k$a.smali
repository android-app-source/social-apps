.class Lcom/twitter/model/businessprofiles/k$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/businessprofiles/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/businessprofiles/k;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/businessprofiles/o;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    .line 55
    sget-object v0, Lcom/twitter/model/businessprofiles/o;->a:Lcom/twitter/util/serialization/l;

    .line 56
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/businessprofiles/k$a;->a:Lcom/twitter/util/serialization/l;

    .line 55
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/businessprofiles/k$1;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/twitter/model/businessprofiles/k$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/businessprofiles/k;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 69
    sget-object v0, Lcom/twitter/model/businessprofiles/c;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/c;

    .line 70
    iget-object v1, p0, Lcom/twitter/model/businessprofiles/k$a;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 71
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v2

    .line 72
    new-instance v3, Lcom/twitter/model/businessprofiles/k;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/model/businessprofiles/k;-><init>(Lcom/twitter/model/businessprofiles/c;Ljava/util/List;Z)V

    return-object v3
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p2, Lcom/twitter/model/businessprofiles/k;->b:Lcom/twitter/model/businessprofiles/c;

    sget-object v1, Lcom/twitter/model/businessprofiles/c;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/k;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/k$a;->a:Lcom/twitter/util/serialization/l;

    .line 61
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 62
    invoke-static {p2}, Lcom/twitter/model/businessprofiles/k;->a(Lcom/twitter/model/businessprofiles/k;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 63
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    check-cast p2, Lcom/twitter/model/businessprofiles/k;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/businessprofiles/k$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/k;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/businessprofiles/k$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/businessprofiles/k;

    move-result-object v0

    return-object v0
.end method
