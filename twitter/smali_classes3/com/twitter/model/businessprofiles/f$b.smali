.class Lcom/twitter/model/businessprofiles/f$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/businessprofiles/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/businessprofiles/f;",
        "Lcom/twitter/model/businessprofiles/f$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/businessprofiles/f$1;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/twitter/model/businessprofiles/f$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/businessprofiles/f$a;
    .locals 1

    .prologue
    .line 113
    new-instance v0, Lcom/twitter/model/businessprofiles/f$a;

    invoke-direct {v0}, Lcom/twitter/model/businessprofiles/f$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/f$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 119
    sget-object v0, Lcom/twitter/model/businessprofiles/n;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/n;

    invoke-virtual {p2, v0}, Lcom/twitter/model/businessprofiles/f$a;->a(Lcom/twitter/model/businessprofiles/n;)Lcom/twitter/model/businessprofiles/f$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/businessprofiles/n;->a:Lcom/twitter/util/serialization/l;

    .line 120
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/n;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/f$a;->b(Lcom/twitter/model/businessprofiles/n;)Lcom/twitter/model/businessprofiles/f$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/businessprofiles/n;->a:Lcom/twitter/util/serialization/l;

    .line 121
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/n;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/f$a;->c(Lcom/twitter/model/businessprofiles/n;)Lcom/twitter/model/businessprofiles/f$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/businessprofiles/n;->a:Lcom/twitter/util/serialization/l;

    .line 122
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/n;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/f$a;->d(Lcom/twitter/model/businessprofiles/n;)Lcom/twitter/model/businessprofiles/f$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/businessprofiles/n;->a:Lcom/twitter/util/serialization/l;

    .line 123
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/n;

    invoke-virtual {v1, v0}, Lcom/twitter/model/businessprofiles/f$a;->e(Lcom/twitter/model/businessprofiles/n;)Lcom/twitter/model/businessprofiles/f$a;

    .line 124
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 99
    check-cast p2, Lcom/twitter/model/businessprofiles/f$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/businessprofiles/f$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/f$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/f;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p2, Lcom/twitter/model/businessprofiles/f;->b:Lcom/twitter/model/businessprofiles/n;

    sget-object v1, Lcom/twitter/model/businessprofiles/n;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/f;->c:Lcom/twitter/model/businessprofiles/n;

    sget-object v2, Lcom/twitter/model/businessprofiles/n;->a:Lcom/twitter/util/serialization/l;

    .line 104
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/f;->d:Lcom/twitter/model/businessprofiles/n;

    sget-object v2, Lcom/twitter/model/businessprofiles/n;->a:Lcom/twitter/util/serialization/l;

    .line 105
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/f;->e:Lcom/twitter/model/businessprofiles/n;

    sget-object v2, Lcom/twitter/model/businessprofiles/n;->a:Lcom/twitter/util/serialization/l;

    .line 106
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/businessprofiles/f;->f:Lcom/twitter/model/businessprofiles/n;

    sget-object v2, Lcom/twitter/model/businessprofiles/n;->a:Lcom/twitter/util/serialization/l;

    .line 107
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 108
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    check-cast p2, Lcom/twitter/model/businessprofiles/f;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/businessprofiles/f$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/f;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/twitter/model/businessprofiles/f$b;->a()Lcom/twitter/model/businessprofiles/f$a;

    move-result-object v0

    return-object v0
.end method
