.class public Lcom/twitter/model/businessprofiles/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/businessprofiles/d$b;,
        Lcom/twitter/model/businessprofiles/d$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/businessprofiles/d;",
            "Lcom/twitter/model/businessprofiles/d$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:Lcom/twitter/model/businessprofiles/g;

.field public final d:Lcom/twitter/model/businessprofiles/c;

.field public final e:Lcom/twitter/model/businessprofiles/KeyEngagementType;

.field public final f:Lcom/twitter/model/businessprofiles/b;

.field public final g:Lcom/twitter/model/businessprofiles/f;

.field public final h:Lcom/twitter/model/businessprofiles/e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/model/businessprofiles/d$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/businessprofiles/d$b;-><init>(Lcom/twitter/model/businessprofiles/d$1;)V

    sput-object v0, Lcom/twitter/model/businessprofiles/d;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/businessprofiles/d$a;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iget-wide v0, p1, Lcom/twitter/model/businessprofiles/d$a;->a:J

    iput-wide v0, p0, Lcom/twitter/model/businessprofiles/d;->b:J

    .line 30
    iget-object v0, p1, Lcom/twitter/model/businessprofiles/d$a;->b:Lcom/twitter/model/businessprofiles/g;

    iput-object v0, p0, Lcom/twitter/model/businessprofiles/d;->c:Lcom/twitter/model/businessprofiles/g;

    .line 31
    iget-object v0, p1, Lcom/twitter/model/businessprofiles/d$a;->c:Lcom/twitter/model/businessprofiles/c;

    iput-object v0, p0, Lcom/twitter/model/businessprofiles/d;->d:Lcom/twitter/model/businessprofiles/c;

    .line 32
    iget-object v0, p1, Lcom/twitter/model/businessprofiles/d$a;->d:Lcom/twitter/model/businessprofiles/KeyEngagementType;

    iput-object v0, p0, Lcom/twitter/model/businessprofiles/d;->e:Lcom/twitter/model/businessprofiles/KeyEngagementType;

    .line 33
    iget-object v0, p1, Lcom/twitter/model/businessprofiles/d$a;->e:Lcom/twitter/model/businessprofiles/b;

    iput-object v0, p0, Lcom/twitter/model/businessprofiles/d;->f:Lcom/twitter/model/businessprofiles/b;

    .line 34
    iget-object v0, p1, Lcom/twitter/model/businessprofiles/d$a;->f:Lcom/twitter/model/businessprofiles/f;

    iput-object v0, p0, Lcom/twitter/model/businessprofiles/d;->g:Lcom/twitter/model/businessprofiles/f;

    .line 35
    iget-object v0, p1, Lcom/twitter/model/businessprofiles/d$a;->g:Lcom/twitter/model/businessprofiles/e;

    iput-object v0, p0, Lcom/twitter/model/businessprofiles/d;->h:Lcom/twitter/model/businessprofiles/e;

    .line 36
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 40
    if-ne p0, p1, :cond_1

    .line 44
    :cond_0
    :goto_0
    return v0

    .line 41
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 43
    :cond_3
    check-cast p1, Lcom/twitter/model/businessprofiles/d;

    .line 44
    iget-wide v2, p0, Lcom/twitter/model/businessprofiles/d;->b:J

    iget-wide v4, p1, Lcom/twitter/model/businessprofiles/d;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/d;->c:Lcom/twitter/model/businessprofiles/g;

    iget-object v3, p1, Lcom/twitter/model/businessprofiles/d;->c:Lcom/twitter/model/businessprofiles/g;

    .line 45
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/d;->d:Lcom/twitter/model/businessprofiles/c;

    iget-object v3, p1, Lcom/twitter/model/businessprofiles/d;->d:Lcom/twitter/model/businessprofiles/c;

    .line 46
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/d;->e:Lcom/twitter/model/businessprofiles/KeyEngagementType;

    iget-object v3, p1, Lcom/twitter/model/businessprofiles/d;->e:Lcom/twitter/model/businessprofiles/KeyEngagementType;

    .line 47
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/d;->f:Lcom/twitter/model/businessprofiles/b;

    iget-object v3, p1, Lcom/twitter/model/businessprofiles/d;->f:Lcom/twitter/model/businessprofiles/b;

    .line 48
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/d;->g:Lcom/twitter/model/businessprofiles/f;

    iget-object v3, p1, Lcom/twitter/model/businessprofiles/d;->g:Lcom/twitter/model/businessprofiles/f;

    .line 49
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/businessprofiles/d;->h:Lcom/twitter/model/businessprofiles/e;

    iget-object v3, p1, Lcom/twitter/model/businessprofiles/d;->h:Lcom/twitter/model/businessprofiles/e;

    .line 50
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/twitter/model/businessprofiles/d;->b:J

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v0

    .line 56
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/businessprofiles/d;->c:Lcom/twitter/model/businessprofiles/g;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/businessprofiles/d;->d:Lcom/twitter/model/businessprofiles/c;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/businessprofiles/d;->e:Lcom/twitter/model/businessprofiles/KeyEngagementType;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/businessprofiles/d;->f:Lcom/twitter/model/businessprofiles/b;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/businessprofiles/d;->g:Lcom/twitter/model/businessprofiles/f;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/businessprofiles/d;->h:Lcom/twitter/model/businessprofiles/e;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    return v0
.end method
