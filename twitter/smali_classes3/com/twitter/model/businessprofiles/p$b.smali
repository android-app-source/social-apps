.class Lcom/twitter/model/businessprofiles/p$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/businessprofiles/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/businessprofiles/p;",
        "Lcom/twitter/model/businessprofiles/p$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/businessprofiles/p$1;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/twitter/model/businessprofiles/p$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/businessprofiles/p$a;
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/twitter/model/businessprofiles/p$a;

    invoke-direct {v0}, Lcom/twitter/model/businessprofiles/p$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/p$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 100
    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 101
    invoke-static {p1, v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v0

    .line 100
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p2, v0}, Lcom/twitter/model/businessprofiles/p$a;->a(Ljava/util/List;)Lcom/twitter/model/businessprofiles/p$a;

    move-result-object v0

    .line 102
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/p$a;->a(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/p$a;

    .line 103
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 83
    check-cast p2, Lcom/twitter/model/businessprofiles/p$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/businessprofiles/p$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/businessprofiles/p$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p2, Lcom/twitter/model/businessprofiles/p;->b:Ljava/util/List;

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 88
    iget-object v0, p2, Lcom/twitter/model/businessprofiles/p;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 89
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    check-cast p2, Lcom/twitter/model/businessprofiles/p;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/businessprofiles/p$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/businessprofiles/p;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/twitter/model/businessprofiles/p$b;->a()Lcom/twitter/model/businessprofiles/p$a;

    move-result-object v0

    return-object v0
.end method
