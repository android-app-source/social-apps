.class public final Lcom/twitter/model/businessprofiles/d$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/businessprofiles/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/businessprofiles/d;",
        ">;"
    }
.end annotation


# instance fields
.field a:J

.field b:Lcom/twitter/model/businessprofiles/g;

.field c:Lcom/twitter/model/businessprofiles/c;

.field d:Lcom/twitter/model/businessprofiles/KeyEngagementType;

.field e:Lcom/twitter/model/businessprofiles/b;

.field f:Lcom/twitter/model/businessprofiles/f;

.field g:Lcom/twitter/model/businessprofiles/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 69
    sget-object v0, Lcom/twitter/model/businessprofiles/KeyEngagementType;->a:Lcom/twitter/model/businessprofiles/KeyEngagementType;

    iput-object v0, p0, Lcom/twitter/model/businessprofiles/d$a;->d:Lcom/twitter/model/businessprofiles/KeyEngagementType;

    return-void
.end method


# virtual methods
.method public a(J)Lcom/twitter/model/businessprofiles/d$a;
    .locals 1

    .prologue
    .line 76
    iput-wide p1, p0, Lcom/twitter/model/businessprofiles/d$a;->a:J

    .line 77
    return-object p0
.end method

.method public a(Lcom/twitter/model/businessprofiles/KeyEngagementType;)Lcom/twitter/model/businessprofiles/d$a;
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/twitter/model/businessprofiles/d$a;->d:Lcom/twitter/model/businessprofiles/KeyEngagementType;

    .line 95
    return-object p0
.end method

.method public a(Lcom/twitter/model/businessprofiles/b;)Lcom/twitter/model/businessprofiles/d$a;
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/twitter/model/businessprofiles/d$a;->e:Lcom/twitter/model/businessprofiles/b;

    .line 101
    return-object p0
.end method

.method public a(Lcom/twitter/model/businessprofiles/c;)Lcom/twitter/model/businessprofiles/d$a;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/twitter/model/businessprofiles/d$a;->c:Lcom/twitter/model/businessprofiles/c;

    .line 89
    return-object p0
.end method

.method public a(Lcom/twitter/model/businessprofiles/e;)Lcom/twitter/model/businessprofiles/d$a;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/twitter/model/businessprofiles/d$a;->g:Lcom/twitter/model/businessprofiles/e;

    .line 113
    return-object p0
.end method

.method public a(Lcom/twitter/model/businessprofiles/f;)Lcom/twitter/model/businessprofiles/d$a;
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/twitter/model/businessprofiles/d$a;->f:Lcom/twitter/model/businessprofiles/f;

    .line 107
    return-object p0
.end method

.method public a(Lcom/twitter/model/businessprofiles/g;)Lcom/twitter/model/businessprofiles/d$a;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/twitter/model/businessprofiles/d$a;->b:Lcom/twitter/model/businessprofiles/g;

    .line 83
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/twitter/model/businessprofiles/d$a;->e()Lcom/twitter/model/businessprofiles/d;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/businessprofiles/d;
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/twitter/model/businessprofiles/d;

    invoke-direct {v0, p0}, Lcom/twitter/model/businessprofiles/d;-><init>(Lcom/twitter/model/businessprofiles/d$a;)V

    return-object v0
.end method
