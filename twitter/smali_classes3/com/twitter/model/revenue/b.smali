.class public Lcom/twitter/model/revenue/b;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Lcom/twitter/util/collection/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/a",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/util/collection/a;

    invoke-direct {v0}, Lcom/twitter/util/collection/a;-><init>()V

    sput-object v0, Lcom/twitter/model/revenue/b;->a:Lcom/twitter/util/collection/a;

    .line 24
    sget-object v0, Lcom/twitter/model/revenue/b;->a:Lcom/twitter/util/collection/a;

    const/16 v1, 0x4000

    .line 25
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->b:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object v0, Lcom/twitter/model/revenue/b;->a:Lcom/twitter/util/collection/a;

    const v1, 0x8000

    .line 27
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->c:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/twitter/model/revenue/b;->a:Lcom/twitter/util/collection/a;

    const/high16 v1, 0x10000

    .line 29
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->d:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/twitter/model/revenue/b;->a:Lcom/twitter/util/collection/a;

    const/high16 v1, 0x20000

    .line 31
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->e:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/twitter/model/revenue/b;->a:Lcom/twitter/util/collection/a;

    const/high16 v1, 0x40000

    .line 33
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->f:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/twitter/model/revenue/b;->a:Lcom/twitter/util/collection/a;

    const/high16 v1, 0x80000

    .line 35
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->g:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/twitter/model/revenue/b;->a:Lcom/twitter/util/collection/a;

    const/high16 v1, 0x100000

    .line 37
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->h:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    invoke-static {}, Lcom/twitter/model/revenue/b;->a()I

    move-result v0

    sput v0, Lcom/twitter/model/revenue/b;->b:I

    return-void
.end method

.method private static a()I
    .locals 3

    .prologue
    .line 91
    const/4 v0, 0x0

    .line 92
    sget-object v1, Lcom/twitter/model/revenue/b;->a:Lcom/twitter/util/collection/a;

    invoke-virtual {v1}, Lcom/twitter/util/collection/a;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 93
    or-int/2addr v0, v1

    move v1, v0

    .line 94
    goto :goto_0

    .line 95
    :cond_0
    return v1
.end method

.method public static a(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-static {p0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    :goto_0
    return v0

    .line 82
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    .line 83
    sget-object v3, Lcom/twitter/model/revenue/b;->a:Lcom/twitter/util/collection/a;

    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/a;->containsValue(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 84
    sget-object v3, Lcom/twitter/model/revenue/b;->a:Lcom/twitter/util/collection/a;

    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    or-int/2addr v0, v1

    :goto_2
    move v1, v0

    .line 86
    goto :goto_1

    :cond_1
    move v0, v1

    .line 87
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 44
    sget v0, Lcom/twitter/model/revenue/b;->b:I

    and-int/2addr v0, p0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 56
    sget-object v0, Lcom/twitter/model/revenue/b;->a:Lcom/twitter/util/collection/a;

    invoke-virtual {v0}, Lcom/twitter/util/collection/a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 57
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    and-int v4, p0, v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v4, v1, :cond_0

    .line 58
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 61
    :cond_1
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static c(I)I
    .locals 1

    .prologue
    .line 71
    sget v0, Lcom/twitter/model/revenue/b;->b:I

    and-int/2addr v0, p0

    return v0
.end method
