.class public final Lcom/twitter/model/revenue/a$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/revenue/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/revenue/a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/twitter/model/revenue/a$b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/revenue/a$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/model/revenue/a$a;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/revenue/a$a;)Lcom/twitter/model/revenue/a$b;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/model/revenue/a$a;->b:Lcom/twitter/model/revenue/a$b;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 67
    iget-object v0, p0, Lcom/twitter/model/revenue/a$a;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/revenue/a$a;->b:Lcom/twitter/model/revenue/a$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/revenue/a$a;->b:Lcom/twitter/model/revenue/a$b;

    iget-object v0, v0, Lcom/twitter/model/revenue/a$b;->c:Ljava/util/List;

    .line 68
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 71
    :goto_0
    return v0

    :cond_1
    const-string/jumbo v0, "carousel"

    iget-object v2, p0, Lcom/twitter/model/revenue/a$a;->b:Lcom/twitter/model/revenue/a$b;

    iget-object v2, v2, Lcom/twitter/model/revenue/a$b;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "tweet"

    iget-object v2, p0, Lcom/twitter/model/revenue/a$a;->b:Lcom/twitter/model/revenue/a$b;

    iget-object v2, v2, Lcom/twitter/model/revenue/a$b;->b:Ljava/lang/String;

    .line 72
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/model/revenue/a$a;->b:Lcom/twitter/model/revenue/a$b;

    iget-object v0, v0, Lcom/twitter/model/revenue/a$b;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/twitter/model/revenue/a$a;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    iget-wide v4, v0, Lcom/twitter/model/core/ac;->a:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/revenue/a$b;)Lcom/twitter/model/revenue/a$a;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/twitter/model/revenue/a$a;->b:Lcom/twitter/model/revenue/a$b;

    .line 62
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/revenue/a$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;)",
            "Lcom/twitter/model/revenue/a$a;"
        }
    .end annotation

    .prologue
    .line 55
    iput-object p1, p0, Lcom/twitter/model/revenue/a$a;->a:Ljava/util/List;

    .line 56
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/twitter/model/revenue/a$a;->e()Lcom/twitter/model/revenue/a;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/revenue/a;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/twitter/model/revenue/a;

    invoke-direct {v0, p0}, Lcom/twitter/model/revenue/a;-><init>(Lcom/twitter/model/revenue/a$a;)V

    return-object v0
.end method
