.class public Lcom/twitter/model/revenue/a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/revenue/a$b;,
        Lcom/twitter/model/revenue/a$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/twitter/model/revenue/a$b;


# direct methods
.method public constructor <init>(Lcom/twitter/model/revenue/a$a;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lcom/twitter/model/revenue/a$a;->a(Lcom/twitter/model/revenue/a$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/revenue/a;->a:Ljava/util/List;

    .line 28
    invoke-static {p1}, Lcom/twitter/model/revenue/a$a;->b(Lcom/twitter/model/revenue/a$a;)Lcom/twitter/model/revenue/a$b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/revenue/a;->b:Lcom/twitter/model/revenue/a$b;

    .line 29
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 38
    if-ne p0, p1, :cond_0

    .line 39
    const/4 v0, 0x1

    .line 46
    :goto_0
    return v0

    .line 41
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 42
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 45
    :cond_2
    check-cast p1, Lcom/twitter/model/revenue/a;

    .line 46
    iget-object v0, p0, Lcom/twitter/model/revenue/a;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/revenue/a;->a:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/model/revenue/a;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/util/List;)I

    move-result v0

    return v0
.end method
