.class public final Lcom/twitter/model/revenue/a$b$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/revenue/a$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/revenue/a$b;",
        ">;"
    }
.end annotation


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/revenue/a$b$a;)J
    .locals 2

    .prologue
    .line 142
    iget-wide v0, p0, Lcom/twitter/model/revenue/a$b$a;->a:J

    return-wide v0
.end method

.method static synthetic b(Lcom/twitter/model/revenue/a$b$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/twitter/model/revenue/a$b$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/revenue/a$b$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/twitter/model/revenue/a$b$a;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/revenue/a$b$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/twitter/model/revenue/a$b$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/model/revenue/a$b$a;)J
    .locals 2

    .prologue
    .line 142
    iget-wide v0, p0, Lcom/twitter/model/revenue/a$b$a;->e:J

    return-wide v0
.end method

.method static synthetic f(Lcom/twitter/model/revenue/a$b$a;)J
    .locals 2

    .prologue
    .line 142
    iget-wide v0, p0, Lcom/twitter/model/revenue/a$b$a;->f:J

    return-wide v0
.end method


# virtual methods
.method public R_()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 188
    const-string/jumbo v1, "tweet"

    iget-object v2, p0, Lcom/twitter/model/revenue/a$b$a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/model/revenue/a$b$a;->c:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/Collection;)I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-wide v2, p0, Lcom/twitter/model/revenue/a$b$a;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    :cond_0
    const-string/jumbo v1, "carousel"

    iget-object v2, p0, Lcom/twitter/model/revenue/a$b$a;->b:Ljava/lang/String;

    .line 189
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/model/revenue/a$b$a;->c:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/Collection;)I

    move-result v1

    if-lez v1, :cond_2

    .line 188
    :cond_1
    :goto_0
    return v0

    .line 189
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcom/twitter/model/revenue/a$b$a;
    .locals 1

    .prologue
    .line 152
    iput-wide p1, p0, Lcom/twitter/model/revenue/a$b$a;->a:J

    .line 153
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/revenue/a$b$a;
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/twitter/model/revenue/a$b$a;->b:Ljava/lang/String;

    .line 159
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/revenue/a$b$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/twitter/model/revenue/a$b$a;"
        }
    .end annotation

    .prologue
    .line 164
    iput-object p1, p0, Lcom/twitter/model/revenue/a$b$a;->c:Ljava/util/List;

    .line 165
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/revenue/a$b$a;
    .locals 1

    .prologue
    .line 176
    iput-wide p1, p0, Lcom/twitter/model/revenue/a$b$a;->e:J

    .line 177
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/revenue/a$b$a;
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/twitter/model/revenue/a$b$a;->d:Ljava/lang/String;

    .line 171
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/twitter/model/revenue/a$b$a;->e()Lcom/twitter/model/revenue/a$b;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/twitter/model/revenue/a$b$a;
    .locals 1

    .prologue
    .line 182
    iput-wide p1, p0, Lcom/twitter/model/revenue/a$b$a;->f:J

    .line 183
    return-object p0
.end method

.method protected e()Lcom/twitter/model/revenue/a$b;
    .locals 1

    .prologue
    .line 195
    new-instance v0, Lcom/twitter/model/revenue/a$b;

    invoke-direct {v0, p0}, Lcom/twitter/model/revenue/a$b;-><init>(Lcom/twitter/model/revenue/a$b$a;)V

    return-object v0
.end method
