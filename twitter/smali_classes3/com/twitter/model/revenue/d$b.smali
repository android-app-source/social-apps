.class Lcom/twitter/model/revenue/d$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/revenue/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/revenue/d;",
        "Lcom/twitter/model/revenue/d$a;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/b;-><init>(I)V

    .line 140
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/revenue/d$a;
    .locals 1

    .prologue
    .line 156
    new-instance v0, Lcom/twitter/model/revenue/d$a;

    invoke-direct {v0}, Lcom/twitter/model/revenue/d$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/revenue/d$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 163
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/revenue/d$a;->a(Ljava/lang/String;)Lcom/twitter/model/revenue/d$a;

    move-result-object v0

    .line 164
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/revenue/d$a;->b(Ljava/lang/String;)Lcom/twitter/model/revenue/d$a;

    move-result-object v0

    .line 165
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/revenue/d$a;->c(Ljava/lang/String;)Lcom/twitter/model/revenue/d$a;

    move-result-object v0

    .line 166
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->g()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/revenue/d$a;->a(F)Lcom/twitter/model/revenue/d$a;

    move-result-object v0

    .line 167
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/revenue/d$a;->b(J)Lcom/twitter/model/revenue/d$a;

    move-result-object v0

    .line 168
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/revenue/d$a;->a(J)Lcom/twitter/model/revenue/d$a;

    .line 169
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 135
    check-cast p2, Lcom/twitter/model/revenue/d$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/revenue/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/revenue/d$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/revenue/d;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p2, Lcom/twitter/model/revenue/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/revenue/d;->c:Ljava/lang/String;

    .line 146
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/revenue/d;->d:Ljava/lang/String;

    .line 147
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/revenue/d;->e:F

    .line 148
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(F)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/revenue/d;->g:J

    .line 149
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/revenue/d;->f:J

    .line 150
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 151
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    check-cast p2, Lcom/twitter/model/revenue/d;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/revenue/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/revenue/d;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/twitter/model/revenue/d$b;->a()Lcom/twitter/model/revenue/d$a;

    move-result-object v0

    return-object v0
.end method
