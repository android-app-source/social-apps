.class public Lcom/twitter/model/revenue/a$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/revenue/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/revenue/a$b$a;
    }
.end annotation


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:J


# direct methods
.method public constructor <init>(Lcom/twitter/model/revenue/a$b$a;)V
    .locals 2

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    invoke-static {p1}, Lcom/twitter/model/revenue/a$b$a;->a(Lcom/twitter/model/revenue/a$b$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/revenue/a$b;->a:J

    .line 113
    invoke-static {p1}, Lcom/twitter/model/revenue/a$b$a;->b(Lcom/twitter/model/revenue/a$b$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/revenue/a$b;->b:Ljava/lang/String;

    .line 114
    invoke-static {p1}, Lcom/twitter/model/revenue/a$b$a;->c(Lcom/twitter/model/revenue/a$b$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/revenue/a$b;->c:Ljava/util/List;

    .line 115
    invoke-static {p1}, Lcom/twitter/model/revenue/a$b$a;->d(Lcom/twitter/model/revenue/a$b$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/revenue/a$b;->d:Ljava/lang/String;

    .line 116
    invoke-static {p1}, Lcom/twitter/model/revenue/a$b$a;->e(Lcom/twitter/model/revenue/a$b$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/revenue/a$b;->e:J

    .line 117
    invoke-static {p1}, Lcom/twitter/model/revenue/a$b$a;->f(Lcom/twitter/model/revenue/a$b$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/revenue/a$b;->f:J

    .line 118
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    if-ne p0, p1, :cond_1

    .line 135
    :cond_0
    :goto_0
    return v0

    .line 130
    :cond_1
    if-eqz p1, :cond_2

    instance-of v2, p1, Lcom/twitter/model/revenue/a$b;

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 131
    goto :goto_0

    .line 134
    :cond_3
    check-cast p1, Lcom/twitter/model/revenue/a$b;

    .line 135
    iget-wide v2, p0, Lcom/twitter/model/revenue/a$b;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lcom/twitter/model/revenue/a$b;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/revenue/a$b;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/revenue/a$b;->b:Ljava/lang/String;

    .line 136
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/revenue/a$b;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/revenue/a$b;->d:Ljava/lang/String;

    .line 137
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p0, Lcom/twitter/model/revenue/a$b;->e:J

    .line 138
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lcom/twitter/model/revenue/a$b;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p0, Lcom/twitter/model/revenue/a$b;->f:J

    .line 139
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lcom/twitter/model/revenue/a$b;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/twitter/model/revenue/a$b;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/revenue/a$b;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/revenue/a$b;->d:Ljava/lang/String;

    iget-wide v4, p0, Lcom/twitter/model/revenue/a$b;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/model/revenue/a$b;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
