.class public Lcom/twitter/model/revenue/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/revenue/d$b;,
        Lcom/twitter/model/revenue/d$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/revenue/d;",
            "Lcom/twitter/model/revenue/d$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:F

.field public final f:J

.field public final g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/model/revenue/d$b;

    invoke-direct {v0}, Lcom/twitter/model/revenue/d$b;-><init>()V

    sput-object v0, Lcom/twitter/model/revenue/d;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/revenue/d$a;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iget-object v0, p1, Lcom/twitter/model/revenue/d$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/revenue/d;->b:Ljava/lang/String;

    .line 37
    iget-object v0, p1, Lcom/twitter/model/revenue/d$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/revenue/d;->c:Ljava/lang/String;

    .line 38
    iget-object v0, p1, Lcom/twitter/model/revenue/d$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/revenue/d;->d:Ljava/lang/String;

    .line 39
    iget v0, p1, Lcom/twitter/model/revenue/d$a;->d:F

    iput v0, p0, Lcom/twitter/model/revenue/d;->e:F

    .line 40
    iget-wide v0, p1, Lcom/twitter/model/revenue/d$a;->f:J

    iput-wide v0, p0, Lcom/twitter/model/revenue/d;->g:J

    .line 41
    iget-wide v0, p1, Lcom/twitter/model/revenue/d$a;->e:J

    iput-wide v0, p0, Lcom/twitter/model/revenue/d;->f:J

    .line 42
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46
    if-ne p0, p1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v0

    .line 49
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 50
    goto :goto_0

    .line 52
    :cond_3
    check-cast p1, Lcom/twitter/model/revenue/d;

    .line 53
    iget-object v2, p0, Lcom/twitter/model/revenue/d;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/revenue/d;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/revenue/d;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/revenue/d;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/model/revenue/d;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/revenue/d;->d:Ljava/lang/String;

    .line 54
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/twitter/model/revenue/d;->e:F

    iget v3, p1, Lcom/twitter/model/revenue/d;->e:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcom/twitter/model/revenue/d;->g:J

    iget-wide v4, p1, Lcom/twitter/model/revenue/d;->g:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcom/twitter/model/revenue/d;->f:J

    iget-wide v4, p1, Lcom/twitter/model/revenue/d;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/model/revenue/d;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/revenue/d;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/revenue/d;->d:Ljava/lang/String;

    iget v3, p0, Lcom/twitter/model/revenue/d;->e:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/model/revenue/d;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-wide v6, p0, Lcom/twitter/model/revenue/d;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
