.class public final enum Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

.field public static final enum b:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

.field public static final enum c:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

.field public static final enum d:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

.field public static final enum e:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

.field public static final enum f:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

.field public static final enum g:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

.field public static final enum h:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

.field public static final i:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final synthetic j:[Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    const-string/jumbo v1, "NONE"

    const-string/jumbo v2, "none"

    invoke-direct {v0, v1, v4, v2}, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->a:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    .line 15
    new-instance v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    const-string/jumbo v1, "SMB"

    const-string/jumbo v2, "smb"

    invoke-direct {v0, v1, v5, v2}, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->b:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    .line 16
    new-instance v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    const-string/jumbo v1, "DSO"

    const-string/jumbo v2, "dso"

    invoke-direct {v0, v1, v6, v2}, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->c:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    .line 17
    new-instance v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    const-string/jumbo v1, "MMS"

    const-string/jumbo v2, "mms"

    invoke-direct {v0, v1, v7, v2}, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->d:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    .line 18
    new-instance v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    const-string/jumbo v1, "RESELLER"

    const-string/jumbo v2, "reseller"

    invoke-direct {v0, v1, v8, v2}, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->e:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    .line 19
    new-instance v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    const-string/jumbo v1, "ANALYTICS"

    const/4 v2, 0x5

    const-string/jumbo v3, "analytics"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->f:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    .line 20
    new-instance v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    const-string/jumbo v1, "PARTNER_MANAGED"

    const/4 v2, 0x6

    const-string/jumbo v3, "partner_managed"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->g:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    .line 21
    new-instance v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    const-string/jumbo v1, "SUBSCRIPTION"

    const/4 v2, 0x7

    const-string/jumbo v3, "subscription"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->h:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    .line 12
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    sget-object v1, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->a:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->b:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->c:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->d:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    aput-object v1, v0, v7

    sget-object v1, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->e:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->f:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->g:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->h:Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->j:[Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    .line 23
    const-class v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    .line 25
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->i:Lcom/twitter/util/serialization/l;

    .line 23
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput-object p3, p0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->mName:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    return-object v0
.end method

.method public static values()[Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->j:[Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    invoke-virtual {v0}, [Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;->mName:Ljava/lang/String;

    return-object v0
.end method
