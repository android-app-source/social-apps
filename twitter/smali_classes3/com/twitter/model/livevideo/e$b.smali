.class Lcom/twitter/model/livevideo/e$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/livevideo/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/livevideo/e;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/livevideo/e$1;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/twitter/model/livevideo/e$b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/livevideo/e;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    .line 110
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    .line 111
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v2

    .line 112
    new-instance v3, Lcom/twitter/model/livevideo/e$a;

    invoke-direct {v3}, Lcom/twitter/model/livevideo/e$a;-><init>()V

    .line 113
    invoke-virtual {v3, v0}, Lcom/twitter/model/livevideo/e$a;->a(Ljava/lang/String;)Lcom/twitter/model/livevideo/e$a;

    move-result-object v0

    .line 114
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/e$a;->b(Ljava/lang/String;)Lcom/twitter/model/livevideo/e$a;

    move-result-object v0

    .line 115
    invoke-virtual {v0, v2}, Lcom/twitter/model/livevideo/e$a;->c(Ljava/lang/String;)Lcom/twitter/model/livevideo/e$a;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lcom/twitter/model/livevideo/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/e;

    .line 112
    return-object v0
.end method

.method public a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/livevideo/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p2, Lcom/twitter/model/livevideo/e;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 101
    iget-object v0, p2, Lcom/twitter/model/livevideo/e;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 102
    iget-object v0, p2, Lcom/twitter/model/livevideo/e;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 103
    return-void
.end method

.method public synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    check-cast p2, Lcom/twitter/model/livevideo/e;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/livevideo/e$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/livevideo/e;)V

    return-void
.end method

.method public synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/livevideo/e$b;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/livevideo/e;

    move-result-object v0

    return-object v0
.end method
