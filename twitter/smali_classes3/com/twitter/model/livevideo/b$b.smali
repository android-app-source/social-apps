.class public Lcom/twitter/model/livevideo/b$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/livevideo/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/livevideo/b;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 233
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/livevideo/b;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 257
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v6

    .line 258
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v8

    .line 259
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v10

    .line 260
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v12

    .line 261
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v14

    .line 262
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v15

    .line 263
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v16

    .line 264
    sget-object v2, Lcom/twitter/model/livevideo/a;->a:Lcom/twitter/util/serialization/l;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/livevideo/a;

    .line 265
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v17

    .line 266
    sget-object v3, Lcom/twitter/model/card/property/ImageSpec;->a:Lcom/twitter/util/serialization/l;

    .line 267
    invoke-static {v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 268
    sget-object v4, Lcom/twitter/model/livevideo/e;->a:Lcom/twitter/util/serialization/l;

    .line 269
    invoke-static {v4}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 270
    sget-object v5, Lcom/twitter/model/livevideo/d;->a:Lcom/twitter/util/serialization/l;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/model/livevideo/d;

    .line 271
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v18

    .line 272
    new-instance v19, Lcom/twitter/model/livevideo/b$a;

    invoke-direct/range {v19 .. v19}, Lcom/twitter/model/livevideo/b$a;-><init>()V

    .line 273
    move-object/from16 v0, v19

    invoke-virtual {v0, v6, v7}, Lcom/twitter/model/livevideo/b$a;->a(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v6

    .line 274
    invoke-virtual {v6, v8, v9}, Lcom/twitter/model/livevideo/b$a;->b(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v6

    .line 275
    invoke-virtual {v6, v10, v11}, Lcom/twitter/model/livevideo/b$a;->c(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v6

    .line 276
    invoke-virtual {v6, v12, v13}, Lcom/twitter/model/livevideo/b$a;->d(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v6

    .line 277
    invoke-virtual {v6, v14}, Lcom/twitter/model/livevideo/b$a;->b(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v6

    .line 278
    invoke-virtual {v6, v15}, Lcom/twitter/model/livevideo/b$a;->c(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v6

    .line 279
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/twitter/model/livevideo/b$a;->a(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v6

    .line 280
    invoke-virtual {v6, v2}, Lcom/twitter/model/livevideo/b$a;->a(Lcom/twitter/model/livevideo/a;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    .line 281
    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/twitter/model/livevideo/b$a;->d(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    .line 282
    invoke-static {v3}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/model/livevideo/b$a;->a(Ljava/util/List;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    .line 283
    invoke-static {v4}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/model/livevideo/b$a;->b(Ljava/util/List;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    .line 284
    invoke-virtual {v2, v5}, Lcom/twitter/model/livevideo/b$a;->a(Lcom/twitter/model/livevideo/d;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    .line 285
    move/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/twitter/model/livevideo/b$a;->a(Z)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    .line 286
    invoke-virtual {v2}, Lcom/twitter/model/livevideo/b$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/livevideo/b;

    .line 272
    return-object v2
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/livevideo/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 237
    iget-wide v0, p2, Lcom/twitter/model/livevideo/b;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/livevideo/b;->c:J

    .line 238
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/livevideo/b;->d:J

    .line 239
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/livevideo/b;->e:J

    .line 240
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/livevideo/b;->f:Ljava/lang/String;

    .line 241
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/livevideo/b;->g:Ljava/lang/String;

    .line 242
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/livevideo/b;->h:Ljava/lang/String;

    .line 243
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    sget-object v2, Lcom/twitter/model/livevideo/a;->a:Lcom/twitter/util/serialization/l;

    .line 244
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/livevideo/b;->j:Ljava/lang/String;

    .line 245
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/livevideo/b;->k:Ljava/util/List;

    sget-object v2, Lcom/twitter/model/card/property/ImageSpec;->a:Lcom/twitter/util/serialization/l;

    .line 247
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 246
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/livevideo/b;->l:Ljava/util/List;

    sget-object v2, Lcom/twitter/model/livevideo/e;->a:Lcom/twitter/util/serialization/l;

    .line 248
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/livevideo/b;->m:Lcom/twitter/model/livevideo/d;

    sget-object v2, Lcom/twitter/model/livevideo/d;->a:Lcom/twitter/util/serialization/l;

    .line 249
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/livevideo/b;->n:Z

    .line 250
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 251
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 233
    check-cast p2, Lcom/twitter/model/livevideo/b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/livevideo/b$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/livevideo/b;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 233
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/livevideo/b$b;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/livevideo/b;

    move-result-object v0

    return-object v0
.end method
