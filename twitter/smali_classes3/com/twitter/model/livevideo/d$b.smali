.class Lcom/twitter/model/livevideo/d$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/livevideo/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/livevideo/d;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/livevideo/d$1;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/twitter/model/livevideo/d$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/livevideo/d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 84
    new-instance v1, Lcom/twitter/model/livevideo/d$a;

    invoke-direct {v1}, Lcom/twitter/model/livevideo/d$a;-><init>()V

    sget-object v0, Lcom/twitter/model/livevideo/c;->a:Lcom/twitter/util/serialization/l;

    .line 85
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/c;

    invoke-virtual {v1, v0}, Lcom/twitter/model/livevideo/d$a;->a(Lcom/twitter/model/livevideo/c;)Lcom/twitter/model/livevideo/d$a;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lcom/twitter/model/livevideo/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/d;

    .line 84
    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/livevideo/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p2, Lcom/twitter/model/livevideo/d;->b:Lcom/twitter/model/livevideo/c;

    sget-object v1, Lcom/twitter/model/livevideo/c;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 78
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    check-cast p2, Lcom/twitter/model/livevideo/d;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/livevideo/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/livevideo/d;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/livevideo/d$b;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/livevideo/d;

    move-result-object v0

    return-object v0
.end method
