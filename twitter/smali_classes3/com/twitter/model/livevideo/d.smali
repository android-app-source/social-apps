.class public Lcom/twitter/model/livevideo/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/livevideo/d$b;,
        Lcom/twitter/model/livevideo/d$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/livevideo/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/twitter/model/livevideo/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcom/twitter/model/livevideo/d$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/livevideo/d$b;-><init>(Lcom/twitter/model/livevideo/d$1;)V

    sput-object v0, Lcom/twitter/model/livevideo/d;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/livevideo/d$a;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iget-object v0, p1, Lcom/twitter/model/livevideo/d$a;->a:Lcom/twitter/model/livevideo/c;

    iput-object v0, p0, Lcom/twitter/model/livevideo/d;->b:Lcom/twitter/model/livevideo/c;

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 31
    if-ne p0, p1, :cond_0

    .line 32
    const/4 v0, 0x1

    .line 40
    :goto_0
    return v0

    .line 34
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 35
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 38
    :cond_2
    check-cast p1, Lcom/twitter/model/livevideo/d;

    .line 40
    iget-object v0, p0, Lcom/twitter/model/livevideo/d;->b:Lcom/twitter/model/livevideo/c;

    iget-object v1, p1, Lcom/twitter/model/livevideo/d;->b:Lcom/twitter/model/livevideo/c;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/model/livevideo/d;->b:Lcom/twitter/model/livevideo/c;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Subscriptions{remindMe="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/livevideo/d;->b:Lcom/twitter/model/livevideo/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
