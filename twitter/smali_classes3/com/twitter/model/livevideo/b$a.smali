.class public final Lcom/twitter/model/livevideo/b$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/livevideo/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/livevideo/b;",
        ">;"
    }
.end annotation


# instance fields
.field a:J

.field b:J

.field c:J

.field d:J

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Lcom/twitter/model/livevideo/a;

.field i:Ljava/lang/String;

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/card/property/ImageSpec;",
            ">;"
        }
    .end annotation
.end field

.field k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/livevideo/e;",
            ">;"
        }
    .end annotation
.end field

.field l:Lcom/twitter/model/livevideo/d;

.field m:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 138
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/livevideo/b$a;->j:Ljava/util/List;

    .line 141
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/livevideo/b$a;->k:Ljava/util/List;

    .line 140
    return-void
.end method


# virtual methods
.method public a(J)Lcom/twitter/model/livevideo/b$a;
    .locals 1

    .prologue
    .line 150
    iput-wide p1, p0, Lcom/twitter/model/livevideo/b$a;->a:J

    .line 151
    return-object p0
.end method

.method public a(Lcom/twitter/model/livevideo/a;)Lcom/twitter/model/livevideo/b$a;
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/twitter/model/livevideo/b$a;->h:Lcom/twitter/model/livevideo/a;

    .line 193
    return-object p0
.end method

.method public a(Lcom/twitter/model/livevideo/d;)Lcom/twitter/model/livevideo/b$a;
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/twitter/model/livevideo/b$a;->l:Lcom/twitter/model/livevideo/d;

    .line 217
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/twitter/model/livevideo/b$a;->g:Ljava/lang/String;

    .line 175
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/livevideo/b$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/card/property/ImageSpec;",
            ">;)",
            "Lcom/twitter/model/livevideo/b$a;"
        }
    .end annotation

    .prologue
    .line 204
    iput-object p1, p0, Lcom/twitter/model/livevideo/b$a;->j:Ljava/util/List;

    .line 205
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/livevideo/b$a;
    .locals 0

    .prologue
    .line 222
    iput-boolean p1, p0, Lcom/twitter/model/livevideo/b$a;->m:Z

    .line 223
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/livevideo/b$a;
    .locals 1

    .prologue
    .line 156
    iput-wide p1, p0, Lcom/twitter/model/livevideo/b$a;->b:J

    .line 157
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/twitter/model/livevideo/b$a;->e:Ljava/lang/String;

    .line 181
    return-object p0
.end method

.method public b(Ljava/util/List;)Lcom/twitter/model/livevideo/b$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/livevideo/e;",
            ">;)",
            "Lcom/twitter/model/livevideo/b$a;"
        }
    .end annotation

    .prologue
    .line 210
    iput-object p1, p0, Lcom/twitter/model/livevideo/b$a;->k:Ljava/util/List;

    .line 211
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/twitter/model/livevideo/b$a;->e()Lcom/twitter/model/livevideo/b;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/twitter/model/livevideo/b$a;
    .locals 1

    .prologue
    .line 162
    iput-wide p1, p0, Lcom/twitter/model/livevideo/b$a;->c:J

    .line 163
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/twitter/model/livevideo/b$a;->f:Ljava/lang/String;

    .line 187
    return-object p0
.end method

.method public d(J)Lcom/twitter/model/livevideo/b$a;
    .locals 1

    .prologue
    .line 168
    iput-wide p1, p0, Lcom/twitter/model/livevideo/b$a;->d:J

    .line 169
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/twitter/model/livevideo/b$a;->i:Ljava/lang/String;

    .line 199
    return-object p0
.end method

.method protected e()Lcom/twitter/model/livevideo/b;
    .locals 1

    .prologue
    .line 229
    new-instance v0, Lcom/twitter/model/livevideo/b;

    invoke-direct {v0, p0}, Lcom/twitter/model/livevideo/b;-><init>(Lcom/twitter/model/livevideo/b$a;)V

    return-object v0
.end method
