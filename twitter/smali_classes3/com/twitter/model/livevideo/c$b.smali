.class public Lcom/twitter/model/livevideo/c$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/livevideo/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/livevideo/c;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/livevideo/c;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v0

    .line 109
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    .line 110
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v2

    .line 111
    new-instance v3, Lcom/twitter/model/livevideo/c$a;

    invoke-direct {v3}, Lcom/twitter/model/livevideo/c$a;-><init>()V

    .line 112
    invoke-virtual {v3, v0}, Lcom/twitter/model/livevideo/c$a;->a(Z)Lcom/twitter/model/livevideo/c$a;

    move-result-object v0

    .line 113
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/c$a;->b(Z)Lcom/twitter/model/livevideo/c$a;

    move-result-object v0

    .line 114
    invoke-virtual {v0, v2}, Lcom/twitter/model/livevideo/c$a;->a(Ljava/lang/String;)Lcom/twitter/model/livevideo/c$a;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Lcom/twitter/model/livevideo/c$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/c;

    .line 111
    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/livevideo/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    iget-boolean v0, p2, Lcom/twitter/model/livevideo/c;->b:Z

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 100
    iget-boolean v0, p2, Lcom/twitter/model/livevideo/c;->c:Z

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 101
    iget-object v0, p2, Lcom/twitter/model/livevideo/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 102
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    check-cast p2, Lcom/twitter/model/livevideo/c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/livevideo/c$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/livevideo/c;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/livevideo/c$b;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/livevideo/c;

    move-result-object v0

    return-object v0
.end method
