.class public Lcom/twitter/model/json/people/JsonModuleShowMore;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/people/i;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/people/i$a;
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/twitter/model/people/i$a;

    invoke-direct {v0}, Lcom/twitter/model/people/i$a;-><init>()V

    iget v1, p0, Lcom/twitter/model/json/people/JsonModuleShowMore;->a:I

    .line 25
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/i$a;->a(I)Lcom/twitter/model/people/i$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/people/JsonModuleShowMore;->b:I

    .line 26
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/i$a;->b(I)Lcom/twitter/model/people/i$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModuleShowMore;->c:Ljava/lang/String;

    .line 27
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/i$a;->a(Ljava/lang/String;)Lcom/twitter/model/people/i$a;

    move-result-object v0

    .line 24
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/people/JsonModuleShowMore;->a()Lcom/twitter/model/people/i$a;

    move-result-object v0

    return-object v0
.end method
