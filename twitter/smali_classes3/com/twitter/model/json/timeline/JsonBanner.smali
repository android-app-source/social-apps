.class public Lcom/twitter/model/json/timeline/JsonBanner;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/timeline/a;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/model/timeline/b;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Lcom/twitter/model/timeline/h;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcom/twitter/model/timeline/r;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/timeline/a;
    .locals 3

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/model/json/timeline/JsonBanner;->a:Lcom/twitter/model/timeline/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/json/timeline/JsonBanner;->b:Lcom/twitter/model/timeline/h;

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/twitter/model/json/timeline/JsonBanner;->a:Lcom/twitter/model/timeline/b;

    iget-object v1, p0, Lcom/twitter/model/json/timeline/JsonBanner;->b:Lcom/twitter/model/timeline/h;

    iget-object v2, p0, Lcom/twitter/model/json/timeline/JsonBanner;->c:Lcom/twitter/model/timeline/r;

    invoke-static {v0, v1, v2}, Lcom/twitter/model/timeline/a;->a(Lcom/twitter/model/timeline/b;Lcom/twitter/model/timeline/h;Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/a;

    move-result-object v0

    .line 29
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/JsonBanner;->a()Lcom/twitter/model/timeline/a;

    move-result-object v0

    return-object v0
.end method
