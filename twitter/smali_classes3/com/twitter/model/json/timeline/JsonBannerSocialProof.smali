.class public Lcom/twitter/model/json/timeline/JsonBannerSocialProof;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/timeline/c;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/twitter/model/timeline/d;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/timeline/c;
    .locals 6

    .prologue
    .line 34
    const/4 v4, 0x0

    .line 35
    const/4 v5, 0x0

    .line 36
    iget-object v0, p0, Lcom/twitter/model/json/timeline/JsonBannerSocialProof;->d:Lcom/twitter/model/timeline/d;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/twitter/model/json/timeline/JsonBannerSocialProof;->d:Lcom/twitter/model/timeline/d;

    iget v4, v0, Lcom/twitter/model/timeline/d;->a:I

    .line 38
    iget-object v0, p0, Lcom/twitter/model/json/timeline/JsonBannerSocialProof;->d:Lcom/twitter/model/timeline/d;

    iget-object v5, v0, Lcom/twitter/model/timeline/d;->b:Ljava/util/List;

    .line 40
    :cond_0
    new-instance v0, Lcom/twitter/model/timeline/c;

    iget-object v1, p0, Lcom/twitter/model/json/timeline/JsonBannerSocialProof;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/json/timeline/JsonBannerSocialProof;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/model/json/timeline/JsonBannerSocialProof;->c:Ljava/util/List;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/model/timeline/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/util/List;)V

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/JsonBannerSocialProof;->a()Lcom/twitter/model/timeline/c;

    move-result-object v0

    return-object v0
.end method
