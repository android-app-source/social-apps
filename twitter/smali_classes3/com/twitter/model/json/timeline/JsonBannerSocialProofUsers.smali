.class public Lcom/twitter/model/json/timeline/JsonBannerSocialProofUsers;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/timeline/d;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/timeline/d;
    .locals 3

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/model/timeline/d;

    iget v1, p0, Lcom/twitter/model/json/timeline/JsonBannerSocialProofUsers;->a:I

    iget-object v2, p0, Lcom/twitter/model/json/timeline/JsonBannerSocialProofUsers;->b:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/timeline/d;-><init>(ILjava/util/List;)V

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/JsonBannerSocialProofUsers;->a()Lcom/twitter/model/timeline/d;

    move-result-object v0

    return-object v0
.end method
