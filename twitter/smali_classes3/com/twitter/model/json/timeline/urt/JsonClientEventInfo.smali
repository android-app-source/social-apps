.class public Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTrendDetails;,
        Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTimelinesDetails;,
        Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/timeline/r;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "component"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "element"
        }
    .end annotation
.end field

.field public c:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "details"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/timeline/r;
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lcom/twitter/model/timeline/r$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/r$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;->a:Ljava/lang/String;

    .line 33
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;->b:Ljava/lang/String;

    .line 34
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->d(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;->c:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;

    if-eqz v1, :cond_1

    .line 36
    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;->c:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;

    iget-object v1, v1, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;->a:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTimelinesDetails;

    if-eqz v1, :cond_0

    .line 37
    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;->c:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;

    iget-object v1, v1, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;->a:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTimelinesDetails;

    iget-object v1, v1, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTimelinesDetails;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;->c:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;

    iget-object v2, v2, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;->a:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTimelinesDetails;

    iget-object v2, v2, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTimelinesDetails;->b:Ljava/lang/String;

    .line 38
    invoke-virtual {v1, v2}, Lcom/twitter/model/timeline/r$a;->b(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;->c:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;

    iget-object v2, v2, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;->a:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTimelinesDetails;

    iget-object v2, v2, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTimelinesDetails;->c:Ljava/lang/String;

    .line 39
    invoke-virtual {v1, v2}, Lcom/twitter/model/timeline/r$a;->e(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    .line 41
    :cond_0
    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;->c:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;

    iget-object v1, v1, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;->b:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTrendDetails;

    if-eqz v1, :cond_1

    .line 42
    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;->c:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;

    iget-object v1, v1, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;->b:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTrendDetails;

    iget-object v1, v1, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTrendDetails;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->g(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;->c:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;

    iget-object v2, v2, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonClientEventDetails;->b:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTrendDetails;

    iget-object v2, v2, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo$JsonTrendDetails;->b:Ljava/lang/String;

    .line 43
    invoke-virtual {v1, v2}, Lcom/twitter/model/timeline/r$a;->h(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    .line 46
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/model/timeline/r$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;->a()Lcom/twitter/model/timeline/r;

    move-result-object v0

    return-object v0
.end method
