.class public Lcom/twitter/model/json/timeline/urt/JsonShowAlertInstruction;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lche;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/model/timeline/AlertType;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "alertType"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "text"
        }
    .end annotation
.end field

.field public c:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "triggerDelayMs"
        }
    .end annotation
.end field

.field public d:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "displayDurationMs"
        }
    .end annotation
.end field

.field public e:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "clientEventInfo"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lche;
    .locals 8

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonShowAlertInstruction;->a:Lcom/twitter/model/timeline/AlertType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonShowAlertInstruction;->a:Lcom/twitter/model/timeline/AlertType;

    sget-object v1, Lcom/twitter/model/timeline/AlertType;->a:Lcom/twitter/model/timeline/AlertType;

    if-eq v0, v1, :cond_0

    .line 36
    new-instance v1, Lcom/twitter/model/timeline/u;

    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonShowAlertInstruction;->a:Lcom/twitter/model/timeline/AlertType;

    iget-object v3, p0, Lcom/twitter/model/json/timeline/urt/JsonShowAlertInstruction;->b:Ljava/lang/String;

    iget-wide v4, p0, Lcom/twitter/model/json/timeline/urt/JsonShowAlertInstruction;->c:J

    iget-wide v6, p0, Lcom/twitter/model/json/timeline/urt/JsonShowAlertInstruction;->d:J

    invoke-direct/range {v1 .. v7}, Lcom/twitter/model/timeline/u;-><init>(Lcom/twitter/model/timeline/AlertType;Ljava/lang/String;JJ)V

    .line 38
    new-instance v2, Lche;

    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonShowAlertInstruction;->e:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;

    invoke-static {v0}, Lcom/twitter/model/json/common/f;->a(Lcom/twitter/model/json/common/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    invoke-direct {v2, v1, v0}, Lche;-><init>(Lcom/twitter/model/timeline/u;Lcom/twitter/model/timeline/r;)V

    move-object v0, v2

    .line 40
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/urt/JsonShowAlertInstruction;->a()Lche;

    move-result-object v0

    return-object v0
.end method
