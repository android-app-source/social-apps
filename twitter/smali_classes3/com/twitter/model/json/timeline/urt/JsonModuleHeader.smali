.class public Lcom/twitter/model/json/timeline/urt/JsonModuleHeader;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lchs;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "text"
        }
    .end annotation
.end field

.field public b:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "sticky"
        }
    .end annotation
.end field

.field public c:Lchl;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "context"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lchs;
    .locals 4

    .prologue
    .line 27
    new-instance v0, Lchs;

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonModuleHeader;->a:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/twitter/model/json/timeline/urt/JsonModuleHeader;->b:Z

    iget-object v3, p0, Lcom/twitter/model/json/timeline/urt/JsonModuleHeader;->c:Lchl;

    invoke-direct {v0, v1, v2, v3}, Lchs;-><init>(Ljava/lang/String;ZLchl;)V

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/urt/JsonModuleHeader;->a()Lchs;

    move-result-object v0

    return-object v0
.end method
