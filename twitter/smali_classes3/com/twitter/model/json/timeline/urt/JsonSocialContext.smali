.class public Lcom/twitter/model/json/timeline/urt/JsonSocialContext;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/timeline/urt/JsonSocialContext$JsonGeneralContext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/core/TwitterSocialProof;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/model/json/timeline/urt/JsonSocialContext$JsonGeneralContext;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "generalContext"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/core/TwitterSocialProof;
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonSocialContext;->a:Lcom/twitter/model/json/timeline/urt/JsonSocialContext$JsonGeneralContext;

    if-eqz v0, :cond_0

    .line 25
    new-instance v0, Lcom/twitter/model/core/TwitterSocialProof$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterSocialProof$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonSocialContext;->a:Lcom/twitter/model/json/timeline/urt/JsonSocialContext$JsonGeneralContext;

    iget-object v1, v1, Lcom/twitter/model/json/timeline/urt/JsonSocialContext$JsonGeneralContext;->a:Lcom/twitter/model/json/core/b;

    iget v1, v1, Lcom/twitter/model/json/core/b;->b:I

    .line 26
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->h(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonSocialContext;->a:Lcom/twitter/model/json/timeline/urt/JsonSocialContext$JsonGeneralContext;

    iget-object v1, v1, Lcom/twitter/model/json/timeline/urt/JsonSocialContext$JsonGeneralContext;->b:Ljava/lang/String;

    .line 27
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->d(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonSocialContext;->a:Lcom/twitter/model/json/timeline/urt/JsonSocialContext$JsonGeneralContext;

    iget-object v1, v1, Lcom/twitter/model/json/timeline/urt/JsonSocialContext$JsonGeneralContext;->c:Ljava/lang/String;

    .line 28
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->e(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterSocialProof$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterSocialProof;

    .line 31
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/urt/JsonSocialContext;->a()Lcom/twitter/model/core/TwitterSocialProof;

    move-result-object v0

    return-object v0
.end method
