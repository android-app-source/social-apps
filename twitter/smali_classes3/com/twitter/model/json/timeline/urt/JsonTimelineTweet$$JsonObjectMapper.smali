.class public final Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet$$JsonObjectMapper;
.super Lcom/bluelinelabs/logansquare/JsonMapper;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bluelinelabs/logansquare/JsonMapper",
        "<",
        "Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/bluelinelabs/logansquare/JsonMapper;-><init>()V

    return-void
.end method

.method public static _parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;

    invoke-direct {v0}, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;-><init>()V

    .line 20
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    if-nez v1, :cond_0

    .line 21
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 23
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_2

    .line 24
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 25
    const/4 v0, 0x0

    .line 33
    :cond_1
    return-object v0

    .line 27
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 28
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    .line 29
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 30
    invoke-static {v0, v1, p0}, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet$$JsonObjectMapper;->parseField(Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;Ljava/lang/String;Lcom/fasterxml/jackson/core/JsonParser;)V

    .line 31
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_0
.end method

.method public static _serialize(Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 56
    if-eqz p2, :cond_0

    .line 57
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->e:Lcom/twitter/model/json/revenue/JsonCampaignMetadata;

    if-eqz v0, :cond_1

    .line 60
    const-string/jumbo v0, "campaignMetadata"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->e:Lcom/twitter/model/json/revenue/JsonCampaignMetadata;

    invoke-static {v0, p1, v2}, Lcom/twitter/model/json/revenue/JsonCampaignMetadata$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/revenue/JsonCampaignMetadata;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 63
    :cond_1
    const-string/jumbo v0, "displayType"

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string/jumbo v0, "id"

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->c:Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;

    if-eqz v0, :cond_2

    .line 66
    const-string/jumbo v0, "promotedMetadata"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->c:Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;

    invoke-static {v0, p1, v2}, Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->d:Lcom/twitter/model/json/timeline/urt/JsonSocialContext;

    if-eqz v0, :cond_3

    .line 70
    const-string/jumbo v0, "socialContext"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->d:Lcom/twitter/model/json/timeline/urt/JsonSocialContext;

    invoke-static {v0, p1, v2}, Lcom/twitter/model/json/timeline/urt/JsonSocialContext$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/timeline/urt/JsonSocialContext;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 73
    :cond_3
    if-eqz p2, :cond_4

    .line 74
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 76
    :cond_4
    return-void
.end method

.method public static parseField(Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;Ljava/lang/String;Lcom/fasterxml/jackson/core/JsonParser;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 37
    const-string/jumbo v0, "campaignMetadata"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38
    invoke-static {p2}, Lcom/twitter/model/json/revenue/JsonCampaignMetadata$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/revenue/JsonCampaignMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->e:Lcom/twitter/model/json/revenue/JsonCampaignMetadata;

    .line 48
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    const-string/jumbo v0, "displayType"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 40
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->b:Ljava/lang/String;

    goto :goto_0

    .line 41
    :cond_2
    const-string/jumbo v0, "id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 42
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->a:Ljava/lang/String;

    goto :goto_0

    .line 43
    :cond_3
    const-string/jumbo v0, "promotedMetadata"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 44
    invoke-static {p2}, Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->c:Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;

    goto :goto_0

    .line 45
    :cond_4
    const-string/jumbo v0, "socialContext"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-static {p2}, Lcom/twitter/model/json/timeline/urt/JsonSocialContext$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/timeline/urt/JsonSocialContext;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->d:Lcom/twitter/model/json/timeline/urt/JsonSocialContext;

    goto :goto_0
.end method


# virtual methods
.method public parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    invoke-static {p1}, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet$$JsonObjectMapper;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;

    move-result-object v0

    return-object v0
.end method

.method public serialize(Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-static {p1, p2, p3}, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 53
    return-void
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12
    check-cast p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet$$JsonObjectMapper;->serialize(Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    return-void
.end method
