.class public Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcgi;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/util/Map;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgi;
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 32
    new-instance v0, Lcgi$a;

    invoke-direct {v0}, Lcgi$a;-><init>()V

    iget-wide v4, p0, Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;->a:J

    .line 33
    invoke-virtual {v0, v4, v5}, Lcgi$a;->b(J)Lcgi$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;->b:Ljava/lang/String;

    .line 34
    invoke-virtual {v0, v1}, Lcgi$a;->a(Ljava/lang/String;)Lcgi$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;->c:Ljava/lang/String;

    .line 35
    invoke-virtual {v0, v1}, Lcgi$a;->b(Ljava/lang/String;)Lcgi$a;

    move-result-object v0

    iget-wide v4, p0, Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;->e:J

    .line 36
    invoke-virtual {v0, v4, v5}, Lcgi$a;->a(J)Lcgi$a;

    move-result-object v4

    .line 38
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;->d:Ljava/util/Map;

    .line 39
    if-eqz v0, :cond_1

    .line 40
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 41
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 42
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 43
    const/4 v2, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_0
    move v1, v2

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 45
    :pswitch_0
    const-string/jumbo v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v4, v0}, Lcgi$a;->a(Z)Lcgi$a;

    goto :goto_0

    .line 43
    :sswitch_0
    const-string/jumbo v6, "pac_in_timeline"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v6, "suppress_media_forward"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v3

    goto :goto_1

    :sswitch_2
    const-string/jumbo v6, "slot_type"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    .line 49
    :pswitch_1
    const-string/jumbo v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v4, v0}, Lcgi$a;->b(Z)Lcgi$a;

    goto :goto_0

    .line 53
    :pswitch_2
    invoke-virtual {v4, v3}, Lcgi$a;->c(Z)Lcgi$a;

    goto :goto_0

    .line 63
    :cond_1
    invoke-virtual {v4}, Lcgi$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    return-object v0

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        -0x70664a12 -> :sswitch_0
        -0x1ad562a0 -> :sswitch_1
        0x28f3ecdb -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;->a()Lcgi;

    move-result-object v0

    return-object v0
.end method
