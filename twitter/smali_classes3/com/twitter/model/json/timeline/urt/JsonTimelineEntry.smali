.class public Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lchi;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "entryId"
        }
    .end annotation
.end field

.field public b:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "sortIndex"
        }
    .end annotation
.end field

.field public c:Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "content"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;Ljava/lang/String;)Lcho;
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->a(Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;Ljava/lang/String;)Lcho;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;Ljava/lang/String;)Lcho;
    .locals 7

    .prologue
    .line 67
    iget-object v1, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;->a:Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;

    .line 68
    iget-object v0, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;->c:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;

    invoke-static {v0}, Lcom/twitter/model/json/common/f;->a(Lcom/twitter/model/json/common/d;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/timeline/r;

    .line 69
    iget-object v0, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->a:Lcia;

    if-eqz v0, :cond_0

    .line 70
    new-instance v0, Lcib;

    iget-wide v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->b:J

    iget-object v5, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;->b:Lchj;

    iget-object v6, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->a:Lcia;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcib;-><init>(Ljava/lang/String;JLcom/twitter/model/timeline/r;Lchj;Lcia;)V

    .line 90
    :goto_0
    return-object v0

    .line 72
    :cond_0
    iget-object v0, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->b:Lcgv;

    if-eqz v0, :cond_1

    .line 73
    new-instance v0, Lchg;

    iget-wide v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->b:J

    iget-object v1, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->b:Lcgv;

    invoke-direct {v0, p2, v2, v3, v1}, Lchg;-><init>(Ljava/lang/String;JLcgv;)V

    goto :goto_0

    .line 74
    :cond_1
    iget-object v0, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->c:Lcgy;

    if-eqz v0, :cond_2

    .line 75
    new-instance v0, Lchm;

    iget-wide v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->b:J

    iget-object v4, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->c:Lcgy;

    iget-object v5, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;->b:Lchj;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lchm;-><init>(Ljava/lang/String;JLcgy;Lchj;)V

    goto :goto_0

    .line 77
    :cond_2
    iget-object v0, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->d:Lcic;

    if-eqz v0, :cond_3

    .line 78
    new-instance v0, Lcid;

    iget-wide v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->b:J

    iget-object v5, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;->b:Lchj;

    iget-object v6, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->d:Lcic;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcid;-><init>(Ljava/lang/String;JLcom/twitter/model/timeline/r;Lchj;Lcic;)V

    goto :goto_0

    .line 80
    :cond_3
    iget-object v0, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->e:Lchy;

    if-eqz v0, :cond_4

    .line 81
    new-instance v0, Lchz;

    iget-wide v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->b:J

    iget-object v5, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;->b:Lchj;

    iget-object v6, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->e:Lchy;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lchz;-><init>(Ljava/lang/String;JLcom/twitter/model/timeline/r;Lchj;Lchy;)V

    goto :goto_0

    .line 83
    :cond_4
    iget-object v0, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->f:Lcht;

    if-eqz v0, :cond_5

    .line 84
    new-instance v0, Lchv;

    iget-wide v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->b:J

    iget-object v5, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;->b:Lchj;

    iget-object v6, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->f:Lcht;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lchv;-><init>(Ljava/lang/String;JLcom/twitter/model/timeline/r;Lchj;Lcht;)V

    goto :goto_0

    .line 86
    :cond_5
    iget-object v0, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->g:Lchp;

    if-eqz v0, :cond_6

    .line 87
    new-instance v0, Lchq;

    iget-wide v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->b:J

    iget-object v5, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;->b:Lchj;

    iget-object v6, v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;->g:Lchp;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lchq;-><init>(Ljava/lang/String;JLcom/twitter/model/timeline/r;Lchj;Lchp;)V

    goto :goto_0

    .line 90
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;)Lchr;
    .locals 4

    .prologue
    .line 95
    iget-object v0, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lchr;->a:Ljava/util/Set;

    iget-object v1, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;->d:Ljava/lang/String;

    .line 96
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;->a:Ljava/util/List;

    new-instance v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$1;

    invoke-direct {v1, p0}, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$1;-><init>(Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;)V

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v0

    .line 113
    new-instance v1, Lchr$a;

    invoke-direct {v1}, Lchr$a;-><init>()V

    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->a:Ljava/lang/String;

    .line 114
    invoke-virtual {v1, v2}, Lchr$a;->a(Ljava/lang/String;)Lchr$a;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->b:J

    .line 115
    invoke-virtual {v1, v2, v3}, Lchr$a;->a(J)Lchr$a;

    move-result-object v1

    .line 116
    invoke-virtual {v1, v0}, Lchr$a;->a(Ljava/util/List;)Lchr$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;->d:Ljava/lang/String;

    .line 117
    invoke-virtual {v0, v1}, Lchr$a;->b(Ljava/lang/String;)Lchr$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;->b:Lchs;

    .line 118
    invoke-virtual {v0, v1}, Lchr$a;->a(Lchs;)Lchr$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;->c:Lcom/twitter/model/timeline/m;

    .line 119
    invoke-virtual {v0, v1}, Lchr$a;->a(Lcom/twitter/model/timeline/m;)Lchr$a;

    move-result-object v1

    iget-object v0, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;->e:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;

    .line 120
    invoke-static {v0}, Lcom/twitter/model/json/common/f;->a(Lcom/twitter/model/json/common/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    invoke-virtual {v1, v0}, Lchr$a;->a(Lcom/twitter/model/timeline/r;)Lchr$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;->f:Lchj;

    .line 121
    invoke-virtual {v0, v1}, Lchr$a;->a(Lchj;)Lchr$a;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Lchr$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchr;

    .line 124
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/json/timeline/urt/JsonTimelineOperation;)Lchw;
    .locals 5

    .prologue
    .line 130
    iget-object v0, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineOperation;->a:Lcom/twitter/model/timeline/ar;

    if-eqz v0, :cond_0

    .line 131
    new-instance v0, Lchh;

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->a:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->b:J

    iget-object v4, p1, Lcom/twitter/model/json/timeline/urt/JsonTimelineOperation;->a:Lcom/twitter/model/timeline/ar;

    invoke-direct {v0, v1, v2, v3, v4}, Lchh;-><init>(Ljava/lang/String;JLcom/twitter/model/timeline/ar;)V

    .line 133
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lchi;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 48
    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 49
    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;

    iget-object v2, v2, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;->a:Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;

    if-eqz v2, :cond_1

    .line 50
    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;

    iget-object v2, v2, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;->b:Lcom/twitter/model/json/timeline/urt/JsonTimelineOperation;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;

    iget-object v2, v2, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;

    if-nez v2, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 51
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;

    iget-object v0, v0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;->a:Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->a(Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;Ljava/lang/String;)Lcho;

    move-result-object v0

    .line 61
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 50
    goto :goto_0

    .line 52
    :cond_1
    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;

    iget-object v2, v2, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;->b:Lcom/twitter/model/json/timeline/urt/JsonTimelineOperation;

    if-eqz v2, :cond_3

    .line 53
    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;

    iget-object v2, v2, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;

    if-nez v2, :cond_2

    :goto_2
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 54
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;

    iget-object v0, v0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;->b:Lcom/twitter/model/json/timeline/urt/JsonTimelineOperation;

    invoke-direct {p0, v0}, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->a(Lcom/twitter/model/json/timeline/urt/JsonTimelineOperation;)Lchw;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 53
    goto :goto_2

    .line 55
    :cond_3
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;

    iget-object v0, v0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;

    if-eqz v0, :cond_5

    .line 56
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;

    iget-object v0, v0, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry$JsonTimelineEntryContent;->c:Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;

    invoke-direct {p0, v0}, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->a(Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;)Lchr;

    move-result-object v0

    goto :goto_1

    .line 59
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "A JsonTimelineEntry must have a non-null ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 61
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;->a()Lchi;

    move-result-object v0

    return-object v0
.end method
