.class public Lcom/twitter/model/json/timeline/urt/JsonTimelineFeedbackInfo;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lchj;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "feedbackKeys"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "feedbackMetadata"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lchj;
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineFeedbackInfo;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lchj;

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineFeedbackInfo;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineFeedbackInfo;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lchj;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 32
    :goto_0
    return-object v0

    .line 30
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "A JsonTimelineDismissInfo must have a non-null feedbackActionKeys"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 32
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/urt/JsonTimelineFeedbackInfo;->a()Lchj;

    move-result-object v0

    return-object v0
.end method
