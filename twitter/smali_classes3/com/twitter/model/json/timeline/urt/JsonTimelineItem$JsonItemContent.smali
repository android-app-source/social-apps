.class public Lcom/twitter/model/json/timeline/urt/JsonTimelineItem$JsonItemContent;
.super Lcom/twitter/model/json/common/a;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/json/timeline/urt/JsonTimelineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "JsonItemContent"
.end annotation


# instance fields
.field public a:Lcia;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "tweet"
        }
    .end annotation
.end field

.field public b:Lcgv;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "conversationThread"
        }
    .end annotation
.end field

.field public c:Lcgy;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "homeConversation"
        }
    .end annotation
.end field

.field public d:Lcic;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "user"
        }
    .end annotation
.end field

.field public e:Lchy;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "trend"
        }
    .end annotation
.end field

.field public f:Lcht;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "moment"
        }
    .end annotation
.end field

.field public g:Lchp;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "label"
        }
    .end annotation
.end field

.field public h:Lcgo;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "spelling"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/model/json/common/a;-><init>()V

    return-void
.end method
