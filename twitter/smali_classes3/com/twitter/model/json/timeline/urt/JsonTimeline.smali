.class public Lcom/twitter/model/json/timeline/urt/JsonTimeline;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lchf;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "id"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "instructions"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lchn;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lchc;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "responseObjects"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lchf;
    .locals 5

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimeline;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 34
    new-instance v2, Lchf;

    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimeline;->a:Ljava/lang/String;

    const-string/jumbo v1, "no-timeline-id"

    .line 35
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonTimeline;->b:Ljava/util/List;

    .line 36
    invoke-static {v1}, Lcom/twitter/util/collection/ImmutableList;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonTimeline;->c:Lchc;

    sget-object v4, Lchc;->a:Lchc;

    .line 37
    invoke-static {v1, v4}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lchc;

    invoke-direct {v2, v0, v3, v1}, Lchf;-><init>(Ljava/lang/String;Ljava/util/List;Lchc;)V

    move-object v0, v2

    .line 42
    :goto_0
    return-object v0

    .line 39
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "A JsonTimeline must have a list of timeline instructions. ID: %s, List<TimelineInstruction>: %s, ResponseObjects: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/model/json/timeline/urt/JsonTimeline;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/twitter/model/json/timeline/urt/JsonTimeline;->b:Ljava/util/List;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/twitter/model/json/timeline/urt/JsonTimeline;->c:Lchc;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 42
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/urt/JsonTimeline;->a()Lchf;

    move-result-object v0

    return-object v0
.end method
