.class public Lcom/twitter/model/json/timeline/urt/JsonTimelineModule;
.super Lcom/twitter/model/json/common/a;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "items"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/json/timeline/urt/JsonTimelineModuleItem;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lchs;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "header"
        }
    .end annotation
.end field

.field public c:Lcom/twitter/model/timeline/m;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "footer"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "displayType"
        }
    .end annotation
.end field

.field public e:Lcom/twitter/model/json/timeline/urt/JsonClientEventInfo;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "clientEventInfo"
        }
    .end annotation
.end field

.field public f:Lchj;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "feedbackInfo"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/model/json/common/a;-><init>()V

    return-void
.end method
