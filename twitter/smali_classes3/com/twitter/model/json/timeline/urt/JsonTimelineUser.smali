.class public Lcom/twitter/model/json/timeline/urt/JsonTimelineUser;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcic;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Lcom/twitter/model/json/timeline/urt/JsonSocialContext;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "socialContext"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcic;
    .locals 5

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineUser;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcic;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineUser;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineUser;->c:Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;

    invoke-static {v0}, Lcom/twitter/model/json/common/f;->a(Lcom/twitter/model/json/common/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    .line 34
    new-instance v2, Lcic;

    iget-object v3, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineUser;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineUser;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineUser;->d:Lcom/twitter/model/json/timeline/urt/JsonSocialContext;

    .line 35
    invoke-static {v1}, Lcom/twitter/model/json/common/f;->a(Lcom/twitter/model/json/common/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/TwitterSocialProof;

    invoke-direct {v2, v3, v4, v0, v1}, Lcic;-><init>(Ljava/lang/String;Ljava/lang/String;Lcgi;Lcom/twitter/model/core/TwitterSocialProof;)V

    move-object v0, v2

    .line 39
    :goto_0
    return-object v0

    .line 37
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "A JsonTimelineUser must have a non-null IDand a valid display type. ID: %s, DisplayType: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineUser;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineUser;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 39
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/urt/JsonTimelineUser;->a()Lcic;

    move-result-object v0

    return-object v0
.end method
