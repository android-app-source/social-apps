.class public Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcia;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Lcom/twitter/model/json/timeline/urt/JsonSocialContext;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Lcom/twitter/model/json/revenue/JsonCampaignMetadata;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcia;
    .locals 6

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcia;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->c:Lcom/twitter/model/json/timeline/urt/JsonPromotedContentUrt;

    invoke-static {v0}, Lcom/twitter/model/json/common/f;->a(Lcom/twitter/model/json/common/d;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcgi;

    .line 39
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->e:Lcom/twitter/model/json/revenue/JsonCampaignMetadata;

    invoke-static {v0}, Lcom/twitter/model/json/common/f;->a(Lcom/twitter/model/json/common/d;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/model/revenue/d;

    .line 40
    new-instance v0, Lcia;

    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->d:Lcom/twitter/model/json/timeline/urt/JsonSocialContext;

    .line 41
    invoke-static {v4}, Lcom/twitter/model/json/common/f;->a(Lcom/twitter/model/json/common/d;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/core/TwitterSocialProof;

    invoke-direct/range {v0 .. v5}, Lcia;-><init>(Ljava/lang/String;Ljava/lang/String;Lcgi;Lcom/twitter/model/core/TwitterSocialProof;Lcom/twitter/model/revenue/d;)V

    .line 45
    :goto_0
    return-object v0

    .line 43
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "A JsonTimelineTweet must have a non-null IDand a valid display type. ID: %s, DisplayType: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 45
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;->a()Lcia;

    move-result-object v0

    return-object v0
.end method
