.class Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpp;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects;->a()Lcgx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcpp",
        "<",
        "Lcom/twitter/model/core/ac$a;",
        "Lcom/twitter/model/core/ac;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects;


# direct methods
.method constructor <init>(Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->a:Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;Lcom/twitter/model/core/ac$a;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->b(Lcom/twitter/model/core/ac$a;)V

    return-void
.end method

.method private b(Lcom/twitter/model/core/ac$a;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->a:Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects;

    iget-object v0, v0, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects;->b:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/twitter/model/core/ac$a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p1, v0}, Lcom/twitter/model/core/ac$a;->b(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/ac$a;

    .line 70
    return-void
.end method

.method static synthetic b(Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;Lcom/twitter/model/core/ac$a;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->c(Lcom/twitter/model/core/ac$a;)V

    return-void
.end method

.method private c(Lcom/twitter/model/core/ac$a;)V
    .locals 4

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/twitter/model/core/ac$a;->i()J

    move-result-wide v0

    .line 74
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 75
    iget-object v2, p0, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->a:Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects;

    iget-object v2, v2, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects;->a:Ljava/util/Map;

    .line 76
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac$a;

    .line 77
    if-eqz v0, :cond_0

    .line 78
    invoke-direct {p0, v0}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->b(Lcom/twitter/model/core/ac$a;)V

    .line 79
    invoke-direct {p0, v0}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->d(Lcom/twitter/model/core/ac$a;)V

    .line 80
    invoke-direct {p0, v0}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->e(Lcom/twitter/model/core/ac$a;)V

    .line 81
    invoke-virtual {v0}, Lcom/twitter/model/core/ac$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    invoke-virtual {p1, v0}, Lcom/twitter/model/core/ac$a;->b(Lcom/twitter/model/core/ac;)Lcom/twitter/model/core/ac$a;

    .line 84
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;Lcom/twitter/model/core/ac$a;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->d(Lcom/twitter/model/core/ac$a;)V

    return-void
.end method

.method private d(Lcom/twitter/model/core/ac$a;)V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p1}, Lcom/twitter/model/core/ac$a;->g()Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->a:Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects;

    iget-object v1, v1, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects;->a:Ljava/util/Map;

    .line 90
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac$a;

    .line 91
    if-eqz v0, :cond_0

    .line 92
    invoke-direct {p0, v0}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->b(Lcom/twitter/model/core/ac$a;)V

    .line 93
    invoke-direct {p0, v0}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->c(Lcom/twitter/model/core/ac$a;)V

    .line 94
    invoke-direct {p0, v0}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->e(Lcom/twitter/model/core/ac$a;)V

    .line 95
    invoke-virtual {v0}, Lcom/twitter/model/core/ac$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    invoke-virtual {p1, v0}, Lcom/twitter/model/core/ac$a;->a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/core/ac$a;

    .line 98
    :cond_0
    return-void
.end method

.method private e(Lcom/twitter/model/core/ac$a;)V
    .locals 6

    .prologue
    .line 101
    invoke-virtual {p1}, Lcom/twitter/model/core/ac$a;->e()Lcbc;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_0

    .line 103
    iget-object v1, v0, Lcbc;->b:Ljava/util/List;

    new-instance v2, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1$1;

    invoke-direct {v2, p0}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1$1;-><init>(Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;)V

    invoke-static {v1, v2}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v1

    .line 134
    new-instance v2, Lcbc$a;

    invoke-direct {v2}, Lcbc$a;-><init>()V

    iget-wide v4, v0, Lcbc;->a:J

    .line 135
    invoke-virtual {v2, v4, v5}, Lcbc$a;->a(J)Lcbc$a;

    move-result-object v2

    iget-object v3, v0, Lcbc;->c:Ljava/lang/String;

    .line 136
    invoke-virtual {v2, v3}, Lcbc$a;->a(Ljava/lang/String;)Lcbc$a;

    move-result-object v2

    iget v0, v0, Lcbc;->d:I

    .line 137
    invoke-virtual {v2, v0}, Lcbc$a;->a(I)Lcbc$a;

    move-result-object v0

    .line 138
    invoke-virtual {v0, v1}, Lcbc$a;->a(Ljava/util/List;)Lcbc$a;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lcbc$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbc;

    .line 140
    invoke-virtual {p1, v0}, Lcom/twitter/model/core/ac$a;->a(Lcbc;)Lcom/twitter/model/core/ac$a;

    .line 142
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/ac$a;)Lcom/twitter/model/core/ac;
    .locals 1

    .prologue
    .line 58
    if-eqz p1, :cond_0

    .line 59
    invoke-direct {p0, p1}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->c(Lcom/twitter/model/core/ac$a;)V

    .line 60
    invoke-direct {p0, p1}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->d(Lcom/twitter/model/core/ac$a;)V

    .line 61
    invoke-direct {p0, p1}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->b(Lcom/twitter/model/core/ac$a;)V

    .line 62
    invoke-direct {p0, p1}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->e(Lcom/twitter/model/core/ac$a;)V

    .line 63
    invoke-virtual {p1}, Lcom/twitter/model/core/ac$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    .line 65
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    check-cast p1, Lcom/twitter/model/core/ac$a;

    invoke-virtual {p0, p1}, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects$1;->a(Lcom/twitter/model/core/ac$a;)Lcom/twitter/model/core/ac;

    move-result-object v0

    return-object v0
.end method
