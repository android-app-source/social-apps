.class public Lcom/twitter/model/json/timeline/urt/JsonAddToThreadInstruction;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcgs;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lchi;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgs;
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonAddToThreadInstruction;->a:Lchi;

    instance-of v0, v0, Lchg;

    if-eqz v0, :cond_0

    .line 22
    new-instance v1, Lcgs;

    iget-object v0, p0, Lcom/twitter/model/json/timeline/urt/JsonAddToThreadInstruction;->a:Lchi;

    check-cast v0, Lchg;

    invoke-direct {v1, v0}, Lcgs;-><init>(Lchg;)V

    move-object v0, v1

    .line 24
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/model/json/timeline/urt/JsonAddToThreadInstruction;->a()Lcgs;

    move-result-object v0

    return-object v0
.end method
