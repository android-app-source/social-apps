.class public Lcom/twitter/model/json/timeline/a;
.super Lcom/twitter/model/json/common/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/i",
        "<",
        "Lcom/twitter/model/timeline/AlertType;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    .line 11
    sget-object v0, Lcom/twitter/model/timeline/AlertType;->a:Lcom/twitter/model/timeline/AlertType;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/util/Map$Entry;

    const/4 v2, 0x0

    const-string/jumbo v3, "new_tweets"

    sget-object v4, Lcom/twitter/model/timeline/AlertType;->b:Lcom/twitter/model/timeline/AlertType;

    .line 12
    invoke-static {v3, v4}, Lcom/twitter/model/json/timeline/a;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "NewTweets"

    sget-object v4, Lcom/twitter/model/timeline/AlertType;->b:Lcom/twitter/model/timeline/AlertType;

    .line 13
    invoke-static {v3, v4}, Lcom/twitter/model/json/timeline/a;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    aput-object v3, v1, v2

    .line 11
    invoke-direct {p0, v0, v1}, Lcom/twitter/model/json/common/i;-><init>(Ljava/lang/Object;[Ljava/util/Map$Entry;)V

    .line 14
    return-void
.end method
