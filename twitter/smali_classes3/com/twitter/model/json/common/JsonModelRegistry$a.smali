.class public Lcom/twitter/model/json/common/JsonModelRegistry$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/json/common/JsonModelRegistry$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/json/common/JsonModelRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TM;>;",
            "Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter",
            "<TM;>;)V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-static {p1, p2}, Lcom/twitter/model/json/common/e;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 63
    return-void
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            "J:",
            "Lcom/twitter/model/json/common/d",
            "<TM;>;>(",
            "Ljava/lang/Class",
            "<TM;>;",
            "Ljava/lang/Class",
            "<TJ;>;)V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-static {p1, p2}, Lcom/twitter/model/json/common/e;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 75
    return-void
.end method

.method public b(Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            "B:",
            "Lcom/twitter/util/object/i",
            "<TM;>;J:",
            "Lcom/twitter/model/json/common/c",
            "<TM;>;>(",
            "Ljava/lang/Class",
            "<TB;>;",
            "Ljava/lang/Class",
            "<TJ;>;)V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-static {p1, p2}, Lcom/twitter/model/json/common/e;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 81
    return-void
.end method
