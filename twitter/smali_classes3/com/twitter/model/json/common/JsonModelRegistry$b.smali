.class public interface abstract Lcom/twitter/model/json/common/JsonModelRegistry$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/json/common/JsonModelRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "b"
.end annotation


# virtual methods
.method public abstract a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TM;>;",
            "Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter",
            "<TM;>;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Class;Ljava/lang/Class;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            "J:",
            "Lcom/twitter/model/json/common/d",
            "<TM;>;>(",
            "Ljava/lang/Class",
            "<TM;>;",
            "Ljava/lang/Class",
            "<TJ;>;)V"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/Class;Ljava/lang/Class;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            "B:",
            "Lcom/twitter/util/object/i",
            "<TM;>;J:",
            "Lcom/twitter/model/json/common/c",
            "<TM;>;>(",
            "Ljava/lang/Class",
            "<TB;>;",
            "Ljava/lang/Class",
            "<TJ;>;)V"
        }
    .end annotation
.end method
