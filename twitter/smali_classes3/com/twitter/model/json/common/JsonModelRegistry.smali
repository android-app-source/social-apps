.class public Lcom/twitter/model/json/common/JsonModelRegistry;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/common/JsonModelRegistry$a;,
        Lcom/twitter/model/json/common/JsonModelRegistry$Registrar;,
        Lcom/twitter/model/json/common/JsonModelRegistry$b;
    }
.end annotation


# static fields
.field private static volatile a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()V
    .locals 4

    .prologue
    .line 25
    sget-boolean v0, Lcom/twitter/model/json/common/JsonModelRegistry;->a:Z

    if-nez v0, :cond_2

    .line 26
    const-class v1, Lcom/twitter/model/json/common/JsonModelRegistry;

    monitor-enter v1

    .line 27
    :try_start_0
    sget-boolean v0, Lcom/twitter/model/json/common/JsonModelRegistry;->a:Z

    if-nez v0, :cond_1

    .line 28
    new-instance v2, Lcom/twitter/model/json/common/JsonModelRegistry$a;

    invoke-direct {v2}, Lcom/twitter/model/json/common/JsonModelRegistry$a;-><init>()V

    .line 29
    invoke-static {}, Lcrl;->a()Lcrl;

    move-result-object v0

    const-class v3, Lcom/twitter/model/json/common/JsonModelRegistry$Registrar;

    invoke-virtual {v0, v3}, Lcrl;->b(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/common/JsonModelRegistry$Registrar;

    .line 30
    invoke-interface {v0, v2}, Lcom/twitter/model/json/common/JsonModelRegistry$Registrar;->a(Lcom/twitter/model/json/common/JsonModelRegistry$b;)V

    goto :goto_0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 32
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, Lcom/twitter/model/json/common/JsonModelRegistry;->a:Z

    .line 33
    const-class v0, Lcom/twitter/model/json/common/JsonModelRegistry;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 35
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 37
    :cond_2
    return-void
.end method
