.class final Lcom/twitter/model/json/common/e$1;
.super Lcom/twitter/model/json/common/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/model/json/common/e;->a(Ljava/lang/Class;Ljava/lang/Class;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/h",
        "<TM;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Class;

.field private b:Lcom/bluelinelabs/logansquare/JsonMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bluelinelabs/logansquare/JsonMapper",
            "<TJ;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Class;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/model/json/common/e$1;->a:Ljava/lang/Class;

    invoke-direct {p0}, Lcom/twitter/model/json/common/h;-><init>()V

    return-void
.end method


# virtual methods
.method public parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/model/json/common/e$1;->b:Lcom/bluelinelabs/logansquare/JsonMapper;

    if-nez v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/twitter/model/json/common/e$1;->a:Ljava/lang/Class;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->mapperFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/JsonMapper;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/common/e$1;->b:Lcom/bluelinelabs/logansquare/JsonMapper;

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/twitter/model/json/common/e$1;->b:Lcom/bluelinelabs/logansquare/JsonMapper;

    invoke-virtual {v0, p1}, Lcom/bluelinelabs/logansquare/JsonMapper;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/common/d;

    invoke-static {v0}, Lcom/twitter/model/json/common/f;->a(Lcom/twitter/model/json/common/d;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
