.class public Lcom/twitter/model/json/core/JsonTwitterAccountUser;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/core/TwitterUser;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "url_https"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public j:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public k:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public l:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public m:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public n:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public o:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "protected"
        }
    .end annotation
.end field

.field public p:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public q:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public r:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public s:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public t:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public u:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public v:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "has_extended_profile"
        }
    .end annotation
.end field

.field public w:Lcom/twitter/model/profile/TranslatorType;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        typeConverter = Lcom/twitter/model/json/profiles/b;
    .end annotation
.end field

.field public x:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "advertiser_account_service_levels"
        }
        typeConverter = Lcom/twitter/model/json/revenue/d;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->n:I

    return-void
.end method

.method public static a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/json/core/JsonTwitterAccountUser;
    .locals 4

    .prologue
    .line 119
    new-instance v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;

    invoke-direct {v1}, Lcom/twitter/model/json/core/JsonTwitterAccountUser;-><init>()V

    .line 120
    invoke-virtual {p0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->a:J

    .line 121
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->b:Ljava/lang/String;

    .line 122
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->c:Ljava/lang/String;

    .line 123
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->d:Ljava/lang/String;

    .line 124
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->F:Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->e:Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->f:Ljava/lang/String;

    .line 126
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->g:Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->g:Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->p:Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->h:Ljava/lang/String;

    .line 128
    iget-wide v2, p0, Lcom/twitter/model/core/TwitterUser;->S:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->i:Ljava/lang/String;

    .line 129
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->u:I

    iput v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->l:I

    .line 130
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->R:I

    iput v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->j:I

    .line 131
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->t:I

    iput v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->k:I

    .line 132
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->v:I

    iput v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->m:I

    .line 133
    iget v0, p0, Lcom/twitter/model/core/TwitterUser;->w:I

    iput v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->n:I

    .line 134
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->x:Z

    iput-boolean v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->p:Z

    .line 135
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->l:Z

    iput-boolean v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->o:Z

    .line 136
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->o:Z

    iput-boolean v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->s:Z

    .line 137
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->m:Z

    iput-boolean v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->q:Z

    .line 138
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->n:Z

    iput-boolean v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->r:Z

    .line 139
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->k:Z

    iput-boolean v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->t:Z

    .line 140
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->H:Z

    iput-boolean v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->u:Z

    .line 141
    iget-boolean v0, p0, Lcom/twitter/model/core/TwitterUser;->r:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->v:Z

    .line 142
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->P:Lcom/twitter/model/profile/TranslatorType;

    iput-object v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->w:Lcom/twitter/model/profile/TranslatorType;

    .line 143
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->Q:Ljava/util/List;

    iput-object v0, v1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->x:Ljava/util/List;

    .line 144
    return-object v1

    .line 141
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public c()Lcom/twitter/util/object/i;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/object/i",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    iget-wide v2, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->a:J

    .line 85
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->b:Ljava/lang/String;

    .line 86
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->c:Ljava/lang/String;

    .line 87
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->d:Ljava/lang/String;

    .line 88
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->e:Ljava/lang/String;

    .line 89
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->i(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->f:Ljava/lang/String;

    .line 90
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->e(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->g:Ljava/lang/String;

    .line 91
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->f(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->h:Ljava/lang/String;

    .line 92
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->h(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->l:I

    .line 93
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->e(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->j:I

    .line 94
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->k:I

    .line 95
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->d(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->m:I

    .line 96
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->f(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->n:I

    .line 97
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->p:Z

    .line 98
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->o:Z

    .line 99
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->b(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->s:Z

    .line 100
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->e(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->q:Z

    .line 101
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->r:Z

    .line 102
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->d(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->t:Z

    .line 103
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->u:Z

    .line 104
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->h(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    iget-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 105
    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->f(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->w:Lcom/twitter/model/profile/TranslatorType;

    sget-object v2, Lcom/twitter/model/profile/TranslatorType;->a:Lcom/twitter/model/profile/TranslatorType;

    .line 106
    invoke-static {v0, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/TranslatorType;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/profile/TranslatorType;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->x:Ljava/util/List;

    .line 107
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(Ljava/util/List;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 110
    :try_start_0
    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->i:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->b(J)Lcom/twitter/model/core/TwitterUser$a;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :goto_1
    return-object v0

    .line 104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 111
    :catch_0
    move-exception v1

    .line 112
    sget-object v1, Lcom/twitter/util/aa;->b:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/aa;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->b(J)Lcom/twitter/model/core/TwitterUser$a;

    goto :goto_1
.end method
