.class public final Lcom/twitter/model/json/core/JsonTwitterAccountUser$$JsonObjectMapper;
.super Lcom/bluelinelabs/logansquare/JsonMapper;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bluelinelabs/logansquare/JsonMapper",
        "<",
        "Lcom/twitter/model/json/core/JsonTwitterAccountUser;",
        ">;"
    }
.end annotation


# static fields
.field protected static final JSON_LIST_ADVERTISER_ACCOUNT_SERVICE_LEVEL_CONVERTER:Lcom/twitter/model/json/revenue/d;

.field protected static final JSON_TRANSLATOR_TYPE_CONVERTER:Lcom/twitter/model/json/profiles/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/twitter/model/json/revenue/d;

    invoke-direct {v0}, Lcom/twitter/model/json/revenue/d;-><init>()V

    sput-object v0, Lcom/twitter/model/json/core/JsonTwitterAccountUser$$JsonObjectMapper;->JSON_LIST_ADVERTISER_ACCOUNT_SERVICE_LEVEL_CONVERTER:Lcom/twitter/model/json/revenue/d;

    .line 16
    new-instance v0, Lcom/twitter/model/json/profiles/b;

    invoke-direct {v0}, Lcom/twitter/model/json/profiles/b;-><init>()V

    sput-object v0, Lcom/twitter/model/json/core/JsonTwitterAccountUser$$JsonObjectMapper;->JSON_TRANSLATOR_TYPE_CONVERTER:Lcom/twitter/model/json/profiles/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/bluelinelabs/logansquare/JsonMapper;-><init>()V

    return-void
.end method

.method public static _parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/core/JsonTwitterAccountUser;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    new-instance v0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;

    invoke-direct {v0}, Lcom/twitter/model/json/core/JsonTwitterAccountUser;-><init>()V

    .line 25
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    if-nez v1, :cond_0

    .line 26
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 28
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_2

    .line 29
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 30
    const/4 v0, 0x0

    .line 38
    :cond_1
    return-object v0

    .line 32
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 33
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    .line 34
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 35
    invoke-static {v0, v1, p0}, Lcom/twitter/model/json/core/JsonTwitterAccountUser$$JsonObjectMapper;->parseField(Lcom/twitter/model/json/core/JsonTwitterAccountUser;Ljava/lang/String;Lcom/fasterxml/jackson/core/JsonParser;)V

    .line 36
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_0
.end method

.method public static _serialize(Lcom/twitter/model/json/core/JsonTwitterAccountUser;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 99
    if-eqz p2, :cond_0

    .line 100
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->x:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 103
    sget-object v0, Lcom/twitter/model/json/core/JsonTwitterAccountUser$$JsonObjectMapper;->JSON_LIST_ADVERTISER_ACCOUNT_SERVICE_LEVEL_CONVERTER:Lcom/twitter/model/json/revenue/d;

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->x:Ljava/util/List;

    const-string/jumbo v2, "advertiser_account_service_levels"

    invoke-virtual {v0, v1, v2, v4, p1}, Lcom/twitter/model/json/revenue/d;->a(Ljava/util/List;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 105
    :cond_1
    const-string/jumbo v0, "created_at"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string/jumbo v0, "description"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string/jumbo v0, "fast_followers_count"

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 108
    const-string/jumbo v0, "followers_count"

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 109
    const-string/jumbo v0, "friends_count"

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 110
    const-string/jumbo v0, "geo_enabled"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->p:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 111
    const-string/jumbo v0, "has_extended_profile"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->v:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 112
    const-string/jumbo v0, "id"

    iget-wide v2, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 113
    const-string/jumbo v0, "is_lifeline_institution"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->s:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 114
    const-string/jumbo v0, "protected"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 115
    const-string/jumbo v0, "is_translator"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->r:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 116
    const-string/jumbo v0, "location"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string/jumbo v0, "media_count"

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 118
    const-string/jumbo v0, "name"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string/jumbo v0, "needs_phone_verification"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->u:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 120
    const-string/jumbo v0, "profile_banner_url"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string/jumbo v0, "profile_image_url_https"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string/jumbo v0, "screen_name"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string/jumbo v0, "statuses_count"

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->m:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 124
    const-string/jumbo v0, "suspended"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->t:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 125
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->w:Lcom/twitter/model/profile/TranslatorType;

    if-eqz v0, :cond_2

    .line 126
    sget-object v0, Lcom/twitter/model/json/core/JsonTwitterAccountUser$$JsonObjectMapper;->JSON_TRANSLATOR_TYPE_CONVERTER:Lcom/twitter/model/json/profiles/b;

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->w:Lcom/twitter/model/profile/TranslatorType;

    const-string/jumbo v2, "translator_type"

    invoke-virtual {v0, v1, v2, v4, p1}, Lcom/twitter/model/json/profiles/b;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 128
    :cond_2
    const-string/jumbo v0, "url_https"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string/jumbo v0, "verified"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->q:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 130
    if-eqz p2, :cond_3

    .line 131
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 133
    :cond_3
    return-void
.end method

.method public static parseField(Lcom/twitter/model/json/core/JsonTwitterAccountUser;Ljava/lang/String;Lcom/fasterxml/jackson/core/JsonParser;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 42
    const-string/jumbo v0, "advertiser_account_service_levels"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    sget-object v0, Lcom/twitter/model/json/core/JsonTwitterAccountUser$$JsonObjectMapper;->JSON_LIST_ADVERTISER_ACCOUNT_SERVICE_LEVEL_CONVERTER:Lcom/twitter/model/json/revenue/d;

    invoke-virtual {v0, p2}, Lcom/twitter/model/json/revenue/d;->a(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->x:Ljava/util/List;

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    const-string/jumbo v0, "created_at"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 45
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->i:Ljava/lang/String;

    goto :goto_0

    .line 46
    :cond_2
    const-string/jumbo v0, "description"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 47
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->f:Ljava/lang/String;

    goto :goto_0

    .line 48
    :cond_3
    const-string/jumbo v0, "fast_followers_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 49
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->k:I

    goto :goto_0

    .line 50
    :cond_4
    const-string/jumbo v0, "followers_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 51
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->j:I

    goto :goto_0

    .line 52
    :cond_5
    const-string/jumbo v0, "friends_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 53
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->l:I

    goto :goto_0

    .line 54
    :cond_6
    const-string/jumbo v0, "geo_enabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 55
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->p:Z

    goto :goto_0

    .line 56
    :cond_7
    const-string/jumbo v0, "has_extended_profile"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 57
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->v:Z

    goto :goto_0

    .line 58
    :cond_8
    const-string/jumbo v0, "id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 59
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->a:J

    goto :goto_0

    .line 60
    :cond_9
    const-string/jumbo v0, "is_lifeline_institution"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 61
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->s:Z

    goto/16 :goto_0

    .line 62
    :cond_a
    const-string/jumbo v0, "protected"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 63
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->o:Z

    goto/16 :goto_0

    .line 64
    :cond_b
    const-string/jumbo v0, "is_translator"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 65
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->r:Z

    goto/16 :goto_0

    .line 66
    :cond_c
    const-string/jumbo v0, "location"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 67
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 68
    :cond_d
    const-string/jumbo v0, "media_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 69
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->n:I

    goto/16 :goto_0

    .line 70
    :cond_e
    const-string/jumbo v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 71
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 72
    :cond_f
    const-string/jumbo v0, "needs_phone_verification"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 73
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->u:Z

    goto/16 :goto_0

    .line 74
    :cond_10
    const-string/jumbo v0, "profile_banner_url"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 75
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 76
    :cond_11
    const-string/jumbo v0, "profile_image_url_https"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 77
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 78
    :cond_12
    const-string/jumbo v0, "screen_name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 79
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 80
    :cond_13
    const-string/jumbo v0, "statuses_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 81
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->m:I

    goto/16 :goto_0

    .line 82
    :cond_14
    const-string/jumbo v0, "suspended"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 83
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->t:Z

    goto/16 :goto_0

    .line 84
    :cond_15
    const-string/jumbo v0, "translator_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 85
    sget-object v0, Lcom/twitter/model/json/core/JsonTwitterAccountUser$$JsonObjectMapper;->JSON_TRANSLATOR_TYPE_CONVERTER:Lcom/twitter/model/json/profiles/b;

    invoke-virtual {v0, p2}, Lcom/twitter/model/json/profiles/b;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/TranslatorType;

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->w:Lcom/twitter/model/profile/TranslatorType;

    goto/16 :goto_0

    .line 86
    :cond_16
    const-string/jumbo v0, "url_https"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 87
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 88
    :cond_17
    const-string/jumbo v0, "verified"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->q:Z

    goto/16 :goto_0
.end method


# virtual methods
.method public parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/core/JsonTwitterAccountUser;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-static {p1}, Lcom/twitter/model/json/core/JsonTwitterAccountUser$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/core/JsonTwitterAccountUser;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/core/JsonTwitterAccountUser$$JsonObjectMapper;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/core/JsonTwitterAccountUser;

    move-result-object v0

    return-object v0
.end method

.method public serialize(Lcom/twitter/model/json/core/JsonTwitterAccountUser;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-static {p1, p2, p3}, Lcom/twitter/model/json/core/JsonTwitterAccountUser$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/core/JsonTwitterAccountUser;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 96
    return-void
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13
    check-cast p1, Lcom/twitter/model/json/core/JsonTwitterAccountUser;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/json/core/JsonTwitterAccountUser$$JsonObjectMapper;->serialize(Lcom/twitter/model/json/core/JsonTwitterAccountUser;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    return-void
.end method
