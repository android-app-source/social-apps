.class public Lcom/twitter/model/json/core/JsonTwitterStatus;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities;,
        Lcom/twitter/model/json/core/JsonTwitterStatus$StatusCoordinateArray;,
        Lcom/twitter/model/json/core/JsonTwitterStatus$UserRetweetId;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/core/ac;",
        ">;"
    }
.end annotation


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public B:Lcom/twitter/model/core/ac;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public C:Lcom/twitter/model/core/v$a;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public D:Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public E:Lcom/twitter/model/search/e;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public F:Lcgi;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public G:Lcax;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public H:Lcom/twitter/model/stratostore/f;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public I:Lcbc;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public J:Lcom/twitter/model/json/core/d;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public K:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public L:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public M:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public k:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public l:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public n:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public o:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public p:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public q:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public r:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public s:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public t:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public u:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public v:Lcom/twitter/model/json/core/JsonTwitterStatus$UserRetweetId;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public w:Lcom/twitter/model/json/core/JsonTwitterStatus$StatusCoordinateArray;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public x:Lcom/twitter/model/core/TwitterUser;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public y:Lcom/twitter/model/geo/TwitterPlace;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public z:Lcom/twitter/model/core/ac;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 35
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    .line 43
    iput-wide v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->a:J

    .line 63
    iput-wide v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->k:J

    .line 65
    iput-wide v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->l:J

    .line 119
    iput-wide v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->M:J

    return-void
.end method

.method private static a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/core/ac;
    .locals 1

    .prologue
    .line 276
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 257
    .line 258
    if-eqz p0, :cond_0

    const-string/jumbo v0, "<a"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    const/16 v0, 0x3e

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 260
    if-eq v0, v2, :cond_0

    .line 261
    const/16 v1, 0x3c

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 262
    if-eq v1, v2, :cond_0

    .line 263
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 267
    :cond_0
    return-object p0
.end method


# virtual methods
.method public a()Lcom/twitter/model/core/ac$a;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 135
    new-instance v8, Lcom/twitter/model/core/ac$a;

    invoke-direct {v8}, Lcom/twitter/model/core/ac$a;-><init>()V

    .line 136
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->G:Lcax;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->G:Lcax;

    invoke-virtual {v8, v0}, Lcom/twitter/model/core/ac$a;->a(Lcax;)Lcom/twitter/model/core/ac$a;

    .line 138
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->G:Lcax;

    invoke-virtual {v0}, Lcax;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->e:Ljava/lang/String;

    .line 140
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->f:Ljava/lang/String;

    .line 141
    iput-object v6, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->C:Lcom/twitter/model/core/v$a;

    .line 142
    iput-object v6, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->D:Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities;

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->g:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->C:Lcom/twitter/model/core/v$a;

    if-eqz v0, :cond_3

    .line 155
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->D:Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities;

    if-eqz v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->C:Lcom/twitter/model/core/v$a;

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->D:Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities;

    invoke-virtual {v1}, Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities;->a()Lcom/twitter/model/core/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/v$a;->a(Lcom/twitter/model/core/k;)Lcom/twitter/model/core/v$a;

    .line 161
    :cond_2
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->C:Lcom/twitter/model/core/v$a;

    invoke-virtual {v0}, Lcom/twitter/model/core/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    .line 162
    const/4 v1, -0x1

    neg-int v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/twitter/model/core/v;->a(II)V

    move-object v1, v0

    .line 168
    :goto_0
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->E:Lcom/twitter/model/search/e;

    if-eqz v0, :cond_4

    .line 169
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->E:Lcom/twitter/model/search/e;

    iget-object v0, v0, Lcom/twitter/model/search/e;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d;

    .line 170
    neg-int v3, v7

    invoke-virtual {v0, v3}, Lcom/twitter/model/core/d;->b(I)V

    goto :goto_1

    .line 164
    :cond_3
    sget-object v1, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    goto :goto_0

    .line 176
    :cond_4
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->f:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->f:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 177
    :goto_2
    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->e:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->E:Lcom/twitter/model/search/e;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->E:Lcom/twitter/model/search/e;

    iget-object v2, v2, Lcom/twitter/model/search/e;->a:Ljava/util/List;

    :goto_3
    iget-object v3, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->g:Ljava/util/List;

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/twitter/model/util/f;->a(Ljava/lang/String;Lcom/twitter/model/core/v;Ljava/lang/Iterable;Ljava/util/List;ZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->e:Ljava/lang/String;

    .line 181
    iget-wide v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->a:J

    invoke-virtual {v8, v2, v3}, Lcom/twitter/model/core/ac$a;->a(J)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->e:Ljava/lang/String;

    .line 182
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->i:Ljava/lang/String;

    .line 183
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->f(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->j:Ljava/lang/String;

    .line 184
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->k:J

    .line 185
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/ac$a;->c(J)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->l:J

    .line 186
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/ac$a;->d(J)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->m:Ljava/lang/String;

    .line 187
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->e(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->o:I

    .line 188
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->b(I)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->L:I

    .line 189
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->c(I)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->n:J

    .line 190
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/ac$a;->g(J)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-boolean v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->p:Z

    .line 191
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->a(Z)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-boolean v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->q:Z

    .line 192
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->b(Z)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-boolean v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->r:Z

    .line 193
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->c(Z)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-boolean v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->s:Z

    .line 194
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->d(Z)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-boolean v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->t:Z

    .line 195
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->e(Z)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-boolean v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->u:Z

    .line 196
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->f(Z)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->x:Lcom/twitter/model/core/TwitterUser;

    .line 197
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->b(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/ac$a;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->x:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->x:Lcom/twitter/model/core/TwitterUser;

    .line 198
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->b()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-virtual {v2, v0}, Lcom/twitter/model/core/ac$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->A:Ljava/lang/String;

    .line 199
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->y:Lcom/twitter/model/geo/TwitterPlace;

    .line 200
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->a(Lcom/twitter/model/geo/TwitterPlace;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->z:Lcom/twitter/model/core/ac;

    .line 201
    invoke-static {v2}, Lcom/twitter/model/json/core/JsonTwitterStatus;->a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/core/ac;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->E:Lcom/twitter/model/search/e;

    .line 202
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->a(Lcom/twitter/model/search/e;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->v:Lcom/twitter/model/json/core/JsonTwitterStatus$UserRetweetId;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->v:Lcom/twitter/model/json/core/JsonTwitterStatus$UserRetweetId;

    iget-wide v2, v2, Lcom/twitter/model/json/core/JsonTwitterStatus$UserRetweetId;->a:J

    .line 203
    :goto_5
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/ac$a;->e(J)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->F:Lcgi;

    .line 205
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->a(Lcgi;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->d:Ljava/lang/String;

    .line 206
    invoke-static {v2}, Lcom/twitter/model/json/core/JsonTwitterStatus;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->d(Ljava/lang/String;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-boolean v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->K:Z

    .line 207
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/ac$a;->g(Z)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->M:J

    .line 208
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/ac$a;->h(J)Lcom/twitter/model/core/ac$a;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->g:Ljava/util/List;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->g:Ljava/util/List;

    .line 209
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_6
    invoke-virtual {v2, v0}, Lcom/twitter/model/core/ac$a;->e(I)Lcom/twitter/model/core/ac$a;

    .line 211
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->H:Lcom/twitter/model/stratostore/f;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->H:Lcom/twitter/model/stratostore/f;

    const-class v2, Lcom/twitter/model/stratostore/g;

    .line 212
    invoke-virtual {v0, v2}, Lcom/twitter/model/stratostore/f;->a(Ljava/lang/Class;)Lcom/twitter/model/stratostore/e$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/stratostore/g;

    .line 213
    :goto_7
    if-eqz v0, :cond_5

    .line 214
    iget-wide v2, v0, Lcom/twitter/model/stratostore/g;->a:J

    invoke-virtual {v8, v2, v3}, Lcom/twitter/model/core/ac$a;->f(J)Lcom/twitter/model/core/ac$a;

    .line 217
    :cond_5
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->c:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 218
    sget-object v0, Lcom/twitter/util/aa;->b:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/twitter/util/aa;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v8, v2, v3}, Lcom/twitter/model/core/ac$a;->b(J)Lcom/twitter/model/core/ac$a;

    .line 221
    :cond_6
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->h:Ljava/lang/String;

    const-string/jumbo v2, "100+"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 222
    const/16 v0, 0x64

    invoke-virtual {v8, v0}, Lcom/twitter/model/core/ac$a;->a(I)Lcom/twitter/model/core/ac$a;

    .line 230
    :cond_7
    :goto_8
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->w:Lcom/twitter/model/json/core/JsonTwitterStatus$StatusCoordinateArray;

    if-eqz v0, :cond_8

    .line 231
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->w:Lcom/twitter/model/json/core/JsonTwitterStatus$StatusCoordinateArray;

    invoke-virtual {v0}, Lcom/twitter/model/json/core/JsonTwitterStatus$StatusCoordinateArray;->a()Lcom/twitter/model/geo/b;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/twitter/model/core/ac$a;->a(Lcom/twitter/model/geo/b;)Lcom/twitter/model/core/ac$a;

    .line 233
    :cond_8
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->B:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_9

    .line 234
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->B:Lcom/twitter/model/core/ac;

    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->c()Lcom/twitter/model/core/ac;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/json/core/JsonTwitterStatus;->a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/core/ac;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/twitter/model/core/ac$a;->b(Lcom/twitter/model/core/ac;)Lcom/twitter/model/core/ac$a;

    .line 237
    :cond_9
    invoke-virtual {v8, v1}, Lcom/twitter/model/core/ac$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/ac$a;

    .line 239
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->I:Lcbc;

    invoke-virtual {v8, v0}, Lcom/twitter/model/core/ac$a;->a(Lcbc;)Lcom/twitter/model/core/ac$a;

    .line 241
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->J:Lcom/twitter/model/json/core/d;

    if-eqz v0, :cond_a

    .line 242
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->J:Lcom/twitter/model/json/core/d;

    iget v0, v0, Lcom/twitter/model/json/core/d;->b:I

    invoke-virtual {v8, v0}, Lcom/twitter/model/core/ac$a;->d(I)Lcom/twitter/model/core/ac$a;

    .line 245
    :cond_a
    return-object v8

    :cond_b
    move-object v0, v6

    .line 176
    goto/16 :goto_2

    :cond_c
    move-object v2, v6

    .line 177
    goto/16 :goto_3

    .line 198
    :cond_d
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->b:Ljava/lang/String;

    goto/16 :goto_4

    .line 202
    :cond_e
    const-wide/16 v2, -0x1

    goto/16 :goto_5

    :cond_f
    move v0, v7

    .line 209
    goto :goto_6

    :cond_10
    move-object v0, v6

    .line 212
    goto :goto_7

    .line 224
    :cond_11
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 226
    :try_start_0
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->h:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/twitter/model/core/ac$a;->a(I)Lcom/twitter/model/core/ac$a;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_8

    .line 227
    :catch_0
    move-exception v0

    goto :goto_8
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/twitter/model/json/core/JsonTwitterStatus;->a()Lcom/twitter/model/core/ac$a;

    move-result-object v0

    return-object v0
.end method
