.class public Lcom/twitter/model/json/core/JsonTwitterUser;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserPhone;,
        Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserEntities;,
        Lcom/twitter/model/json/core/JsonTwitterUser$JsonActionsArray;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/core/TwitterUser;",
        ">;"
    }
.end annotation


# static fields
.field private static final ad:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final ae:Ljava/util/regex/Pattern;

.field private static final af:Ljava/util/regex/Pattern;


# instance fields
.field public A:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public B:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public C:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public D:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public E:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public F:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public G:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public H:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public I:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public J:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public K:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public L:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public M:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public N:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public O:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public P:Lcgi;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public Q:Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserEntities;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public R:Lcom/twitter/model/json/core/JsonTwitterUser$JsonActionsArray;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public S:Lcom/twitter/model/geo/TwitterPlace;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public T:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public U:Lcom/twitter/model/profile/ExtendedProfile;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public V:Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserPhone;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public W:Lcom/twitter/model/businessprofiles/BusinessProfileState;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public X:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public Y:Lcom/twitter/model/analytics/AnalyticsType;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public Z:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public aa:Lcom/twitter/model/profile/TranslatorType;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public ab:Ljava/lang/Boolean;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public ac:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "advertiser_account_service_levels"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "url_https"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "url"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public m:Lcom/twitter/model/ads/AdvertiserType;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public n:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public o:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public p:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public q:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public r:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public s:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public t:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "protected"
        }
    .end annotation
.end field

.field public u:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public v:Ljava/lang/Boolean;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public w:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "has_extended_profile"
        }
    .end annotation
.end field

.field public x:Ljava/lang/Boolean;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public y:Ljava/lang/Boolean;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public z:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "mute"

    const/16 v2, 0x40

    .line 49
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "block"

    const/16 v2, 0x80

    .line 50
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "report_spam"

    const/16 v2, 0x100

    .line 51
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lcom/twitter/model/json/core/JsonTwitterUser;->ad:Ljava/util/Map;

    .line 54
    const-string/jumbo v0, "(?:^|\\s)(@\\w{1,15})"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/json/core/JsonTwitterUser;->ae:Ljava/util/regex/Pattern;

    .line 55
    const-string/jumbo v0, "#\\w+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/json/core/JsonTwitterUser;->af:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    .line 91
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->r:I

    return-void
.end method

.method static synthetic d()Ljava/util/Map;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/twitter/model/json/core/JsonTwitterUser;->ad:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/model/core/TwitterUser$a;
    .locals 12

    .prologue
    const/high16 v10, -0x1000000

    const/4 v2, 0x0

    const/16 v8, 0x10

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 171
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    iget-wide v6, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->a:J

    .line 172
    invoke-virtual {v0, v6, v7}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->b:Ljava/lang/String;

    .line 173
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->c:Ljava/lang/String;

    .line 174
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->d:Ljava/lang/String;

    .line 175
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->e:Ljava/lang/String;

    .line 176
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->i(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->h:Ljava/lang/String;

    .line 177
    invoke-static {v0, v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->f(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->i:Ljava/lang/String;

    .line 178
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->h(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->n:I

    .line 179
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->o:I

    .line 180
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->d(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->p:I

    .line 181
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->e(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->q:I

    .line 182
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->f(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->r:I

    .line 183
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->s:I

    .line 184
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->h(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->t:Z

    .line 185
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->b(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->u:Z

    .line 186
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    iget-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->w:Z

    if-nez v0, :cond_e

    move v0, v4

    .line 187
    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->f(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->z:Z

    .line 188
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->A:Z

    .line 189
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->d(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->B:Z

    .line 190
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->e(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->C:Z

    .line 191
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->K:Z

    .line 192
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->h(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->N:Z

    .line 193
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->i(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->P:Lcgi;

    .line 194
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcgi;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->ab:Ljava/lang/Boolean;

    .line 195
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(Ljava/lang/Boolean;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v7

    .line 197
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->T:Ljava/util/List;

    if-eqz v0, :cond_10

    .line 198
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->T:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_f

    .line 199
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->T:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->e(J)Lcom/twitter/model/core/TwitterUser$a;

    .line 206
    :goto_1
    const/16 v0, 0x80

    .line 207
    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->v:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 208
    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->v:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-static {v0, v4}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 211
    :cond_0
    :goto_2
    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->x:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 212
    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->x:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_12

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 215
    :cond_1
    :goto_3
    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->y:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 216
    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->y:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_13

    const/16 v1, 0x4000

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 219
    :cond_2
    :goto_4
    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->D:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 220
    :cond_3
    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->E:Z

    if-eqz v1, :cond_4

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 221
    :cond_4
    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->F:Z

    if-eqz v1, :cond_5

    invoke-static {v0, v8}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 222
    :cond_5
    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->G:Z

    if-eqz v1, :cond_6

    const/16 v1, 0x800

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 223
    :cond_6
    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->H:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x100

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 224
    :cond_7
    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->I:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x200

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 225
    :cond_8
    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->J:Z

    if-eqz v1, :cond_9

    const/16 v1, 0x1000

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 226
    :cond_9
    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->L:Z

    if-eqz v1, :cond_a

    const/16 v1, 0x400

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 227
    :cond_a
    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->M:Z

    if-eqz v1, :cond_b

    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 228
    :cond_b
    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->O:Z

    if-eqz v1, :cond_14

    const v1, 0x8000

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    move v6, v0

    .line 230
    :goto_5
    invoke-virtual {v7, v6}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    .line 233
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->Q:Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserEntities;

    if-eqz v0, :cond_1c

    .line 234
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->Q:Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserEntities;

    iget-object v0, v0, Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserEntities;->b:Lcom/twitter/model/core/v;

    sget-object v1, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->b(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/TwitterUser$a;

    .line 235
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->Q:Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserEntities;

    iget-object v0, v0, Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserEntities;->a:Lcom/twitter/model/core/v;

    sget-object v1, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    move-object v1, v0

    .line 239
    :goto_6
    :try_start_0
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->j:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/twitter/model/core/TwitterUser$a;->b(J)Lcom/twitter/model/core/TwitterUser$a;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    :goto_7
    :try_start_1
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->k:Ljava/lang/String;

    const/16 v3, 0x10

    invoke-static {v0, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    or-int/2addr v0, v10

    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(I)Lcom/twitter/model/core/TwitterUser$a;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2

    .line 250
    :goto_8
    :try_start_2
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->l:Ljava/lang/String;

    const/16 v3, 0x10

    invoke-static {v0, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    or-int/2addr v0, v10

    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->b(I)Lcom/twitter/model/core/TwitterUser$a;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    .line 254
    :goto_9
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->R:Lcom/twitter/model/json/core/JsonTwitterUser$JsonActionsArray;

    if-eqz v0, :cond_c

    .line 255
    invoke-virtual {v7}, Lcom/twitter/model/core/TwitterUser$a;->h()I

    move-result v0

    iget-object v3, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->R:Lcom/twitter/model/json/core/JsonTwitterUser$JsonActionsArray;

    invoke-virtual {v3}, Lcom/twitter/model/json/core/JsonTwitterUser$JsonActionsArray;->a()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    or-int/2addr v0, v3

    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->j(I)Lcom/twitter/model/core/TwitterUser$a;

    .line 258
    :cond_c
    iget-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->Z:Z

    if-eqz v0, :cond_d

    .line 259
    invoke-virtual {v7}, Lcom/twitter/model/core/TwitterUser$a;->h()I

    move-result v0

    or-int/lit16 v0, v0, 0x800

    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->j(I)Lcom/twitter/model/core/TwitterUser$a;

    .line 262
    :cond_d
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->S:Lcom/twitter/model/geo/TwitterPlace;

    invoke-static {v0}, Lcom/twitter/util/collection/k;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/util/collection/k;)Lcom/twitter/model/core/TwitterUser$a;

    .line 264
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->U:Lcom/twitter/model/profile/ExtendedProfile;

    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/profile/ExtendedProfile;)Lcom/twitter/model/core/TwitterUser$a;

    .line 266
    if-eqz v1, :cond_19

    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 267
    new-instance v3, Lcom/twitter/model/core/v$a;

    invoke-direct {v3, v1}, Lcom/twitter/model/core/v$a;-><init>(Lcom/twitter/model/core/v;)V

    .line 269
    new-instance v1, Lcom/twitter/model/core/f$b;

    invoke-direct {v1}, Lcom/twitter/model/core/f$b;-><init>()V

    .line 270
    new-instance v8, Lcom/twitter/model/core/f$b;

    invoke-direct {v8}, Lcom/twitter/model/core/f$b;-><init>()V

    .line 272
    sget-object v0, Lcom/twitter/model/json/core/JsonTwitterUser;->ae:Ljava/util/regex/Pattern;

    iget-object v9, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->f:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    .line 273
    sget-object v0, Lcom/twitter/model/json/core/JsonTwitterUser;->af:Ljava/util/regex/Pattern;

    iget-object v10, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->f:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v10

    .line 275
    :goto_a
    invoke-virtual {v9}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 276
    new-instance v0, Lcom/twitter/model/core/q$a;

    invoke-direct {v0}, Lcom/twitter/model/core/q$a;-><init>()V

    .line 277
    invoke-virtual {v9, v4}, Ljava/util/regex/Matcher;->start(I)I

    move-result v11

    invoke-virtual {v0, v11}, Lcom/twitter/model/core/q$a;->a(I)Lcom/twitter/model/core/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q$a;

    .line 278
    invoke-virtual {v9, v4}, Ljava/util/regex/Matcher;->end(I)I

    move-result v11

    invoke-virtual {v0, v11}, Lcom/twitter/model/core/q$a;->b(I)Lcom/twitter/model/core/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q$a;

    .line 279
    invoke-virtual {v9, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/twitter/model/core/q$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/q$a;

    move-result-object v0

    .line 280
    invoke-virtual {v0}, Lcom/twitter/model/core/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d;

    .line 276
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/f$b;->a(Lcom/twitter/model/core/d;)Lcom/twitter/model/core/f$a;

    goto :goto_a

    :cond_e
    move v0, v5

    .line 186
    goto/16 :goto_0

    .line 201
    :cond_f
    const-wide/16 v0, 0x0

    invoke-virtual {v7, v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->e(J)Lcom/twitter/model/core/TwitterUser$a;

    goto/16 :goto_1

    .line 204
    :cond_10
    const-wide/16 v0, -0x1

    invoke-virtual {v7, v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->e(J)Lcom/twitter/model/core/TwitterUser$a;

    goto/16 :goto_1

    .line 209
    :cond_11
    invoke-static {v0, v4}, Lcom/twitter/model/core/g;->b(II)I

    move-result v0

    goto/16 :goto_2

    .line 212
    :cond_12
    const/4 v1, 0x2

    .line 213
    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->b(II)I

    move-result v0

    goto/16 :goto_3

    .line 216
    :cond_13
    const/16 v1, 0x4000

    .line 217
    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->b(II)I

    move-result v0

    goto/16 :goto_4

    :cond_14
    move v6, v0

    .line 228
    goto/16 :goto_5

    .line 240
    :catch_0
    move-exception v0

    .line 241
    sget-object v0, Lcom/twitter/util/aa;->b:Ljava/text/SimpleDateFormat;

    iget-object v3, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->j:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/twitter/util/aa;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/twitter/model/core/TwitterUser$a;->b(J)Lcom/twitter/model/core/TwitterUser$a;

    goto/16 :goto_7

    .line 282
    :cond_15
    invoke-virtual {v1}, Lcom/twitter/model/core/f$b;->e()Z

    move-result v0

    if-nez v0, :cond_16

    .line 283
    invoke-virtual {v1}, Lcom/twitter/model/core/f$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/f;

    invoke-virtual {v3, v0}, Lcom/twitter/model/core/v$a;->b(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/v$a;

    .line 286
    :cond_16
    :goto_b
    invoke-virtual {v10}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 287
    new-instance v0, Lcom/twitter/model/core/h$a;

    invoke-direct {v0}, Lcom/twitter/model/core/h$a;-><init>()V

    .line 288
    invoke-virtual {v10}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/h$a;->a(I)Lcom/twitter/model/core/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/h$a;

    .line 289
    invoke-virtual {v10}, Ljava/util/regex/Matcher;->end()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/h$a;->b(I)Lcom/twitter/model/core/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/h$a;

    .line 290
    invoke-virtual {v10}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/h$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/h$a;

    move-result-object v0

    .line 291
    invoke-virtual {v0}, Lcom/twitter/model/core/h$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d;

    .line 287
    invoke-virtual {v8, v0}, Lcom/twitter/model/core/f$b;->a(Lcom/twitter/model/core/d;)Lcom/twitter/model/core/f$a;

    goto :goto_b

    .line 293
    :cond_17
    invoke-virtual {v8}, Lcom/twitter/model/core/f$b;->e()Z

    move-result v0

    if-nez v0, :cond_18

    .line 294
    invoke-virtual {v8}, Lcom/twitter/model/core/f$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/f;

    invoke-virtual {v3, v0}, Lcom/twitter/model/core/v$a;->c(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/v$a;

    .line 297
    :cond_18
    invoke-virtual {v3}, Lcom/twitter/model/core/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    move-object v1, v0

    .line 301
    :cond_19
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->f:Ljava/lang/String;

    move-object v3, v2

    invoke-static/range {v0 .. v5}, Lcom/twitter/model/util/f;->a(Ljava/lang/String;Lcom/twitter/model/core/v;Ljava/lang/Iterable;Ljava/util/List;ZZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->e(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 302
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 303
    invoke-virtual {v0, v6}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    .line 305
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->V:Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserPhone;

    if-eqz v0, :cond_1a

    .line 306
    new-instance v0, Lcbw$a;

    invoke-direct {v0}, Lcbw$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->V:Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserPhone;

    iget-object v1, v1, Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserPhone;->a:Ljava/lang/String;

    .line 308
    invoke-virtual {v0, v1}, Lcbw$a;->a(Ljava/lang/String;)Lcbw$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->V:Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserPhone;

    iget-boolean v1, v1, Lcom/twitter/model/json/core/JsonTwitterUser$JsonUserPhone;->b:Z

    .line 309
    invoke-virtual {v0, v1}, Lcbw$a;->a(Z)Lcbw$a;

    move-result-object v0

    .line 310
    invoke-virtual {v0}, Lcbw$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbw;

    .line 306
    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcbw;)Lcom/twitter/model/core/TwitterUser$a;

    .line 313
    :cond_1a
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->m:Lcom/twitter/model/ads/AdvertiserType;

    sget-object v1, Lcom/twitter/model/ads/AdvertiserType;->a:Lcom/twitter/model/ads/AdvertiserType;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/ads/AdvertiserType;

    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/ads/AdvertiserType;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->W:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    sget-object v2, Lcom/twitter/model/businessprofiles/BusinessProfileState;->a:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 315
    invoke-static {v0, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 314
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/businessprofiles/BusinessProfileState;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->Y:Lcom/twitter/model/analytics/AnalyticsType;

    sget-object v2, Lcom/twitter/model/analytics/AnalyticsType;->a:Lcom/twitter/model/analytics/AnalyticsType;

    if-ne v1, v2, :cond_1b

    .line 316
    :goto_c
    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->j(Z)Lcom/twitter/model/core/TwitterUser$a;

    .line 318
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->X:Ljava/lang/String;

    const-string/jumbo v1, "none"

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 320
    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    .line 322
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->aa:Lcom/twitter/model/profile/TranslatorType;

    sget-object v1, Lcom/twitter/model/profile/TranslatorType;->a:Lcom/twitter/model/profile/TranslatorType;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/TranslatorType;

    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/profile/TranslatorType;)Lcom/twitter/model/core/TwitterUser$a;

    .line 323
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterUser;->ac:Ljava/util/List;

    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Ljava/util/List;)Lcom/twitter/model/core/TwitterUser$a;

    .line 324
    return-object v7

    :cond_1b
    move v4, v5

    .line 314
    goto :goto_c

    .line 251
    :catch_1
    move-exception v0

    goto/16 :goto_9

    .line 246
    :catch_2
    move-exception v0

    goto/16 :goto_8

    :cond_1c
    move-object v1, v2

    goto/16 :goto_6
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/twitter/model/json/core/JsonTwitterUser;->a()Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    return-object v0
.end method
