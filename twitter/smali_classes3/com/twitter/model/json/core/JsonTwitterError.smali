.class public Lcom/twitter/model/json/core/JsonTwitterError;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/core/y;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/core/y;
    .locals 9

    .prologue
    .line 30
    iget v0, p0, Lcom/twitter/model/json/core/JsonTwitterError;->a:I

    if-lez v0, :cond_0

    .line 31
    new-instance v1, Lcom/twitter/model/core/y;

    iget v2, p0, Lcom/twitter/model/json/core/JsonTwitterError;->a:I

    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterError;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/model/json/core/JsonTwitterError;->d:J

    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterError;->e:Ljava/lang/String;

    .line 32
    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/twitter/model/json/core/JsonTwitterError;->b:I

    iget-object v8, p0, Lcom/twitter/model/json/core/JsonTwitterError;->f:Ljava/lang/String;

    invoke-direct/range {v1 .. v8}, Lcom/twitter/model/core/y;-><init>(ILjava/lang/String;JLjava/lang/String;ILjava/lang/String;)V

    .line 34
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/model/json/core/JsonTwitterError;->a()Lcom/twitter/model/core/y;

    move-result-object v0

    return-object v0
.end method
