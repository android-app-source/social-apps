.class public final Lcom/twitter/model/json/core/JsonTwitterStatus$$JsonObjectMapper;
.super Lcom/bluelinelabs/logansquare/JsonMapper;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bluelinelabs/logansquare/JsonMapper",
        "<",
        "Lcom/twitter/model/json/core/JsonTwitterStatus;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/bluelinelabs/logansquare/JsonMapper;-><init>()V

    return-void
.end method

.method public static _parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/core/JsonTwitterStatus;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lcom/twitter/model/json/core/JsonTwitterStatus;

    invoke-direct {v0}, Lcom/twitter/model/json/core/JsonTwitterStatus;-><init>()V

    .line 32
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    if-nez v1, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_2

    .line 36
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 37
    const/4 v0, 0x0

    .line 45
    :cond_1
    return-object v0

    .line 39
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 40
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    .line 41
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 42
    invoke-static {v0, v1, p0}, Lcom/twitter/model/json/core/JsonTwitterStatus$$JsonObjectMapper;->parseField(Lcom/twitter/model/json/core/JsonTwitterStatus;Ljava/lang/String;Lcom/fasterxml/jackson/core/JsonParser;)V

    .line 43
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_0
.end method

.method public static _serialize(Lcom/twitter/model/json/core/JsonTwitterStatus;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 148
    if-eqz p2, :cond_0

    .line 149
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->G:Lcax;

    if-eqz v0, :cond_1

    .line 152
    const-class v0, Lcax;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->G:Lcax;

    const-string/jumbo v2, "card"

    invoke-interface {v0, v1, v2, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->I:Lcbc;

    if-eqz v0, :cond_2

    .line 155
    const-class v0, Lcbc;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->I:Lcbc;

    const-string/jumbo v2, "collection"

    invoke-interface {v0, v1, v2, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 157
    :cond_2
    const-string/jumbo v0, "conversation_id"

    iget-wide v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->M:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 158
    const-string/jumbo v0, "conversation_muted"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->K:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 159
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->w:Lcom/twitter/model/json/core/JsonTwitterStatus$StatusCoordinateArray;

    if-eqz v0, :cond_3

    .line 160
    const-string/jumbo v0, "coordinates"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->w:Lcom/twitter/model/json/core/JsonTwitterStatus$StatusCoordinateArray;

    invoke-static {v0, p1, v4}, Lcom/twitter/model/json/core/JsonTwitterStatus$StatusCoordinateArray$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/core/JsonTwitterStatus$StatusCoordinateArray;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 163
    :cond_3
    const-string/jumbo v0, "created_at"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->v:Lcom/twitter/model/json/core/JsonTwitterStatus$UserRetweetId;

    if-eqz v0, :cond_4

    .line 165
    const-string/jumbo v0, "current_user_retweet"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->v:Lcom/twitter/model/json/core/JsonTwitterStatus$UserRetweetId;

    invoke-static {v0, p1, v4}, Lcom/twitter/model/json/core/JsonTwitterStatus$UserRetweetId$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/core/JsonTwitterStatus$UserRetweetId;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 168
    :cond_4
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->g:Ljava/util/List;

    .line 169
    if-eqz v0, :cond_6

    .line 170
    const-string/jumbo v1, "display_text_range"

    invoke-virtual {p1, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 171
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a()V

    .line 172
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 173
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->b(I)V

    goto :goto_0

    .line 175
    :cond_5
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    .line 177
    :cond_6
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->C:Lcom/twitter/model/core/v$a;

    if-eqz v0, :cond_7

    .line 178
    const-class v0, Lcom/twitter/model/core/v$a;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->C:Lcom/twitter/model/core/v$a;

    const-string/jumbo v2, "entities"

    invoke-interface {v0, v1, v2, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 180
    :cond_7
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->H:Lcom/twitter/model/stratostore/f;

    if-eqz v0, :cond_8

    .line 181
    const-class v0, Lcom/twitter/model/stratostore/f;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->H:Lcom/twitter/model/stratostore/f;

    const-string/jumbo v2, "ext"

    invoke-interface {v0, v1, v2, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 183
    :cond_8
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->D:Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities;

    if-eqz v0, :cond_9

    .line 184
    const-string/jumbo v0, "extended_entities"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->D:Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities;

    invoke-static {v0, p1, v4}, Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 187
    :cond_9
    const-string/jumbo v0, "favorite_count"

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 188
    const-string/jumbo v0, "favorited"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->q:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 189
    const-string/jumbo v0, "full_text"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string/jumbo v0, "id"

    iget-wide v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 191
    const-string/jumbo v0, "in_reply_to_screen_name"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const-string/jumbo v0, "in_reply_to_status_id"

    iget-wide v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->l:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 193
    const-string/jumbo v0, "in_reply_to_user_id"

    iget-wide v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->k:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 194
    const-string/jumbo v0, "is_emergency"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->t:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 195
    const-string/jumbo v0, "is_quote_status"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->u:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 196
    const-string/jumbo v0, "lang"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->E:Lcom/twitter/model/search/e;

    if-eqz v0, :cond_a

    .line 198
    const-class v0, Lcom/twitter/model/search/e;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->E:Lcom/twitter/model/search/e;

    const-string/jumbo v2, "metadata"

    invoke-interface {v0, v1, v2, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 200
    :cond_a
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->y:Lcom/twitter/model/geo/TwitterPlace;

    if-eqz v0, :cond_b

    .line 201
    const-class v0, Lcom/twitter/model/geo/TwitterPlace;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->y:Lcom/twitter/model/geo/TwitterPlace;

    const-string/jumbo v2, "place"

    invoke-interface {v0, v1, v2, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 203
    :cond_b
    const-string/jumbo v0, "possibly_sensitive"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->r:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 204
    const-string/jumbo v0, "possibly_sensitive_appealable"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->s:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 205
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->F:Lcgi;

    if-eqz v0, :cond_c

    .line 206
    const-class v0, Lcgi;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->F:Lcgi;

    const-string/jumbo v2, "promoted_content"

    invoke-interface {v0, v1, v2, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 208
    :cond_c
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->B:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_d

    .line 209
    const-class v0, Lcom/twitter/model/core/ac;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->B:Lcom/twitter/model/core/ac;

    const-string/jumbo v2, "quoted_status"

    invoke-interface {v0, v1, v2, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 211
    :cond_d
    const-string/jumbo v0, "quoted_status_id"

    iget-wide v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->n:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 212
    const-string/jumbo v0, "reply_count"

    iget v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->L:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 213
    const-string/jumbo v0, "retweet_count"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string/jumbo v0, "retweeted"

    iget-boolean v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->p:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 215
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->z:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_e

    .line 216
    const-class v0, Lcom/twitter/model/core/ac;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->z:Lcom/twitter/model/core/ac;

    const-string/jumbo v2, "retweeted_status"

    invoke-interface {v0, v1, v2, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 218
    :cond_e
    const-string/jumbo v0, "retweeted_status_id_str"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string/jumbo v0, "source"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string/jumbo v0, "supplemental_language"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string/jumbo v0, "text"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->x:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_f

    .line 223
    const-class v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->x:Lcom/twitter/model/core/TwitterUser;

    const-string/jumbo v2, "user"

    invoke-interface {v0, v1, v2, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 225
    :cond_f
    const-string/jumbo v0, "user_id_str"

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->J:Lcom/twitter/model/json/core/d;

    if-eqz v0, :cond_10

    .line 227
    const-class v0, Lcom/twitter/model/json/core/d;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->J:Lcom/twitter/model/json/core/d;

    const-string/jumbo v2, "withheld_scope"

    invoke-interface {v0, v1, v2, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 229
    :cond_10
    if-eqz p2, :cond_11

    .line 230
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 232
    :cond_11
    return-void
.end method

.method public static parseField(Lcom/twitter/model/json/core/JsonTwitterStatus;Ljava/lang/String;Lcom/fasterxml/jackson/core/JsonParser;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 49
    const-string/jumbo v0, "card"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    const-class v0, Lcax;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->G:Lcax;

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    const-string/jumbo v0, "collection"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    const-class v0, Lcbc;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbc;

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->I:Lcbc;

    goto :goto_0

    .line 53
    :cond_2
    const-string/jumbo v0, "conversation_id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 54
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->M:J

    goto :goto_0

    .line 55
    :cond_3
    const-string/jumbo v0, "conversation_muted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 56
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->K:Z

    goto :goto_0

    .line 57
    :cond_4
    const-string/jumbo v0, "coordinates"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 58
    invoke-static {p2}, Lcom/twitter/model/json/core/JsonTwitterStatus$StatusCoordinateArray$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/core/JsonTwitterStatus$StatusCoordinateArray;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->w:Lcom/twitter/model/json/core/JsonTwitterStatus$StatusCoordinateArray;

    goto :goto_0

    .line 59
    :cond_5
    const-string/jumbo v0, "created_at"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 60
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->c:Ljava/lang/String;

    goto :goto_0

    .line 61
    :cond_6
    const-string/jumbo v0, "current_user_retweet"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 62
    invoke-static {p2}, Lcom/twitter/model/json/core/JsonTwitterStatus$UserRetweetId$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/core/JsonTwitterStatus$UserRetweetId;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->v:Lcom/twitter/model/json/core/JsonTwitterStatus$UserRetweetId;

    goto :goto_0

    .line 63
    :cond_7
    const-string/jumbo v0, "display_text_range"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 64
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_b

    .line 65
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 66
    :cond_8
    :goto_1
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_a

    .line 68
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->m:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v3, :cond_9

    move-object v0, v1

    .line 69
    :goto_2
    if-eqz v0, :cond_8

    .line 70
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 68
    :cond_9
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    .line 73
    :cond_a
    iput-object v2, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->g:Ljava/util/List;

    goto/16 :goto_0

    .line 75
    :cond_b
    iput-object v1, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->g:Ljava/util/List;

    goto/16 :goto_0

    .line 77
    :cond_c
    const-string/jumbo v0, "entities"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 78
    const-class v0, Lcom/twitter/model/core/v$a;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v$a;

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->C:Lcom/twitter/model/core/v$a;

    goto/16 :goto_0

    .line 79
    :cond_d
    const-string/jumbo v0, "ext"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 80
    const-class v0, Lcom/twitter/model/stratostore/f;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/stratostore/f;

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->H:Lcom/twitter/model/stratostore/f;

    goto/16 :goto_0

    .line 81
    :cond_e
    const-string/jumbo v0, "extended_entities"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 82
    invoke-static {p2}, Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->D:Lcom/twitter/model/json/core/JsonTwitterStatus$ExtendedTweetEntities;

    goto/16 :goto_0

    .line 83
    :cond_f
    const-string/jumbo v0, "favorite_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 84
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->o:I

    goto/16 :goto_0

    .line 85
    :cond_10
    const-string/jumbo v0, "favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 86
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->q:Z

    goto/16 :goto_0

    .line 87
    :cond_11
    const-string/jumbo v0, "full_text"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 88
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 89
    :cond_12
    const-string/jumbo v0, "id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 90
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->a:J

    goto/16 :goto_0

    .line 91
    :cond_13
    const-string/jumbo v0, "in_reply_to_screen_name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 92
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 93
    :cond_14
    const-string/jumbo v0, "in_reply_to_status_id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 94
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->l:J

    goto/16 :goto_0

    .line 95
    :cond_15
    const-string/jumbo v0, "in_reply_to_user_id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 96
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->k:J

    goto/16 :goto_0

    .line 97
    :cond_16
    const-string/jumbo v0, "is_emergency"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 98
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->t:Z

    goto/16 :goto_0

    .line 99
    :cond_17
    const-string/jumbo v0, "is_quote_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 100
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->u:Z

    goto/16 :goto_0

    .line 101
    :cond_18
    const-string/jumbo v0, "lang"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 102
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 103
    :cond_19
    const-string/jumbo v0, "metadata"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 104
    const-class v0, Lcom/twitter/model/search/e;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/e;

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->E:Lcom/twitter/model/search/e;

    goto/16 :goto_0

    .line 105
    :cond_1a
    const-string/jumbo v0, "place"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 106
    const-class v0, Lcom/twitter/model/geo/TwitterPlace;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/TwitterPlace;

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->y:Lcom/twitter/model/geo/TwitterPlace;

    goto/16 :goto_0

    .line 107
    :cond_1b
    const-string/jumbo v0, "possibly_sensitive"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 108
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->r:Z

    goto/16 :goto_0

    .line 109
    :cond_1c
    const-string/jumbo v0, "possibly_sensitive_appealable"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 110
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->s:Z

    goto/16 :goto_0

    .line 111
    :cond_1d
    const-string/jumbo v0, "promoted_content"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 112
    const-class v0, Lcgi;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->F:Lcgi;

    goto/16 :goto_0

    .line 113
    :cond_1e
    const-string/jumbo v0, "quoted_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 114
    const-class v0, Lcom/twitter/model/core/ac;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->B:Lcom/twitter/model/core/ac;

    goto/16 :goto_0

    .line 115
    :cond_1f
    const-string/jumbo v0, "quoted_status_id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 116
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->n:J

    goto/16 :goto_0

    .line 117
    :cond_20
    const-string/jumbo v0, "reply_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 118
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->L:I

    goto/16 :goto_0

    .line 119
    :cond_21
    const-string/jumbo v0, "retweet_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 120
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 121
    :cond_22
    const-string/jumbo v0, "retweeted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 122
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->p:Z

    goto/16 :goto_0

    .line 123
    :cond_23
    const-string/jumbo v0, "retweeted_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 124
    const-class v0, Lcom/twitter/model/core/ac;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->z:Lcom/twitter/model/core/ac;

    goto/16 :goto_0

    .line 125
    :cond_24
    const-string/jumbo v0, "retweeted_status_id_str"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 126
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->A:Ljava/lang/String;

    goto/16 :goto_0

    .line 127
    :cond_25
    const-string/jumbo v0, "source"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 128
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 129
    :cond_26
    const-string/jumbo v0, "supplemental_language"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 130
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 131
    :cond_27
    const-string/jumbo v0, "text"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 132
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 133
    :cond_28
    const-string/jumbo v0, "user"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 134
    const-class v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->x:Lcom/twitter/model/core/TwitterUser;

    goto/16 :goto_0

    .line 135
    :cond_29
    const-string/jumbo v0, "user_id_str"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 136
    invoke-virtual {p2, v1}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 137
    :cond_2a
    const-string/jumbo v0, "withheld_scope"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    const-class v0, Lcom/twitter/model/json/core/d;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/core/d;

    iput-object v0, p0, Lcom/twitter/model/json/core/JsonTwitterStatus;->J:Lcom/twitter/model/json/core/d;

    goto/16 :goto_0
.end method


# virtual methods
.method public parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/core/JsonTwitterStatus;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-static {p1}, Lcom/twitter/model/json/core/JsonTwitterStatus$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/core/JsonTwitterStatus;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/core/JsonTwitterStatus$$JsonObjectMapper;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/core/JsonTwitterStatus;

    move-result-object v0

    return-object v0
.end method

.method public serialize(Lcom/twitter/model/json/core/JsonTwitterStatus;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    invoke-static {p1, p2, p3}, Lcom/twitter/model/json/core/JsonTwitterStatus$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/core/JsonTwitterStatus;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 145
    return-void
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    check-cast p1, Lcom/twitter/model/json/core/JsonTwitterStatus;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/json/core/JsonTwitterStatus$$JsonObjectMapper;->serialize(Lcom/twitter/model/json/core/JsonTwitterStatus;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    return-void
.end method
