.class public Lcom/twitter/model/json/search/JsonTwitterSearchQuery;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/search/b;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/search/b$a;
    .locals 4

    .prologue
    .line 30
    new-instance v0, Lcom/twitter/model/search/b$a;

    invoke-direct {v0}, Lcom/twitter/model/search/b$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/search/JsonTwitterSearchQuery;->a:Ljava/lang/String;

    .line 31
    invoke-virtual {v0, v1}, Lcom/twitter/model/search/b$a;->a(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/search/JsonTwitterSearchQuery;->b:Ljava/lang/String;

    .line 32
    invoke-virtual {v0, v1}, Lcom/twitter/model/search/b$a;->b(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/search/JsonTwitterSearchQuery;->c:J

    .line 33
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/search/b$a;->a(J)Lcom/twitter/model/search/b$a;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/model/json/search/JsonTwitterSearchQuery;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/util/aa;->b:Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/twitter/model/json/search/JsonTwitterSearchQuery;->d:Ljava/lang/String;

    .line 34
    invoke-static {v0, v1}, Lcom/twitter/util/aa;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;)J

    move-result-wide v0

    :goto_0
    invoke-virtual {v2, v0, v1}, Lcom/twitter/model/search/b$a;->b(J)Lcom/twitter/model/search/b$a;

    move-result-object v0

    .line 30
    return-object v0

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/model/json/search/JsonTwitterSearchQuery;->a()Lcom/twitter/model/search/b$a;

    move-result-object v0

    return-object v0
.end method
