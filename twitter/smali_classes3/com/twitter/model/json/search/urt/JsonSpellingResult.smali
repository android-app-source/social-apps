.class public Lcom/twitter/model/json/search/urt/JsonSpellingResult;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcgp;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "text"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "hitHighlights"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcgq;",
            ">;"
        }
    .end annotation
.end field

.field public c:F
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "score"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgp;
    .locals 4

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/model/json/search/urt/JsonSpellingResult;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/json/search/urt/JsonSpellingResult;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 31
    new-instance v0, Lcgp;

    iget-object v1, p0, Lcom/twitter/model/json/search/urt/JsonSpellingResult;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/json/search/urt/JsonSpellingResult;->b:Ljava/util/List;

    iget v3, p0, Lcom/twitter/model/json/search/urt/JsonSpellingResult;->c:F

    invoke-direct {v0, v1, v2, v3}, Lcgp;-><init>(Ljava/lang/String;Ljava/util/List;F)V

    .line 33
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/model/json/search/urt/JsonSpellingResult;->a()Lcgp;

    move-result-object v0

    return-object v0
.end method
