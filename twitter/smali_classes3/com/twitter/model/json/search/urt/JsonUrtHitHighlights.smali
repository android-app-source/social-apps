.class public Lcom/twitter/model/json/search/urt/JsonUrtHitHighlights;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcgq;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "startIndex"
        }
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "endIndex"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    .line 14
    iput v0, p0, Lcom/twitter/model/json/search/urt/JsonUrtHitHighlights;->a:I

    .line 17
    iput v0, p0, Lcom/twitter/model/json/search/urt/JsonUrtHitHighlights;->b:I

    return-void
.end method


# virtual methods
.method public a()Lcgq;
    .locals 3

    .prologue
    .line 23
    iget v0, p0, Lcom/twitter/model/json/search/urt/JsonUrtHitHighlights;->a:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/model/json/search/urt/JsonUrtHitHighlights;->a:I

    iget v1, p0, Lcom/twitter/model/json/search/urt/JsonUrtHitHighlights;->b:I

    if-gt v0, v1, :cond_0

    .line 24
    new-instance v0, Lcgq;

    iget v1, p0, Lcom/twitter/model/json/search/urt/JsonUrtHitHighlights;->a:I

    iget v2, p0, Lcom/twitter/model/json/search/urt/JsonUrtHitHighlights;->b:I

    invoke-direct {v0, v1, v2}, Lcgq;-><init>(II)V

    .line 26
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/search/urt/JsonUrtHitHighlights;->a()Lcgq;

    move-result-object v0

    return-object v0
.end method
