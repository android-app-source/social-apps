.class public Lcom/twitter/model/json/search/urt/JsonSpelling;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcgo;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcgp;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "spellingResult"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "spellingAction"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgo;
    .locals 3

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/model/json/search/urt/JsonSpelling;->a:Lcgp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/json/search/urt/JsonSpelling;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcgo;

    iget-object v1, p0, Lcom/twitter/model/json/search/urt/JsonSpelling;->a:Lcgp;

    iget-object v2, p0, Lcom/twitter/model/json/search/urt/JsonSpelling;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcgo;-><init>(Lcgp;Ljava/lang/String;)V

    .line 28
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/model/json/search/urt/JsonSpelling;->a()Lcgo;

    move-result-object v0

    return-object v0
.end method
