.class public Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/search/e;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/i;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/twitter/model/json/search/JsonSearchSocialProof;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Lcom/twitter/model/json/search/JsonSearchHighlightContext;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Lcom/twitter/model/json/search/JsonTwitterStatusReason;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/search/e$a;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 37
    new-instance v0, Lcom/twitter/model/search/e$a;

    invoke-direct {v0}, Lcom/twitter/model/search/e$a;-><init>()V

    iget-object v2, p0, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->a:Ljava/lang/String;

    .line 38
    invoke-virtual {v0, v2}, Lcom/twitter/model/search/e$a;->a(Ljava/lang/String;)Lcom/twitter/model/search/e$a;

    move-result-object v0

    iget-boolean v2, p0, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->b:Z

    .line 39
    invoke-virtual {v0, v2}, Lcom/twitter/model/search/e$a;->a(Z)Lcom/twitter/model/search/e$a;

    move-result-object v0

    iget-boolean v2, p0, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->c:Z

    .line 40
    invoke-virtual {v0, v2}, Lcom/twitter/model/search/e$a;->b(Z)Lcom/twitter/model/search/e$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->d:Ljava/util/List;

    .line 41
    invoke-static {v2}, Lcom/twitter/util/collection/ImmutableList;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/model/search/e$a;->a(Ljava/util/List;)Lcom/twitter/model/search/e$a;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->g:Lcom/twitter/model/json/search/JsonTwitterStatusReason;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->g:Lcom/twitter/model/json/search/JsonTwitterStatusReason;

    iget-object v0, v0, Lcom/twitter/model/json/search/JsonTwitterStatusReason;->a:Ljava/lang/String;

    .line 42
    :goto_0
    invoke-virtual {v2, v0}, Lcom/twitter/model/search/e$a;->b(Ljava/lang/String;)Lcom/twitter/model/search/e$a;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->g:Lcom/twitter/model/json/search/JsonTwitterStatusReason;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->g:Lcom/twitter/model/json/search/JsonTwitterStatusReason;

    iget-object v0, v0, Lcom/twitter/model/json/search/JsonTwitterStatusReason;->b:Ljava/lang/String;

    .line 43
    :goto_1
    invoke-virtual {v2, v0}, Lcom/twitter/model/search/e$a;->c(Ljava/lang/String;)Lcom/twitter/model/search/e$a;

    move-result-object v2

    .line 46
    iget-object v0, p0, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->e:Lcom/twitter/model/json/search/JsonSearchSocialProof;

    if-eqz v0, :cond_3

    .line 47
    iget-object v0, p0, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->e:Lcom/twitter/model/json/search/JsonSearchSocialProof;

    invoke-virtual {v0}, Lcom/twitter/model/json/search/JsonSearchSocialProof;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterSocialProof;

    .line 49
    :goto_2
    iget-object v1, p0, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->f:Lcom/twitter/model/json/search/JsonSearchHighlightContext;

    if-eqz v1, :cond_0

    .line 50
    iget-object v0, p0, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->f:Lcom/twitter/model/json/search/JsonSearchHighlightContext;

    invoke-virtual {v0}, Lcom/twitter/model/json/search/JsonSearchHighlightContext;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterSocialProof;

    .line 52
    :cond_0
    invoke-virtual {v2, v0}, Lcom/twitter/model/search/e$a;->a(Lcom/twitter/model/core/TwitterSocialProof;)Lcom/twitter/model/search/e$a;

    .line 53
    return-object v2

    :cond_1
    move-object v0, v1

    .line 41
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 42
    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;->a()Lcom/twitter/model/search/e$a;

    move-result-object v0

    return-object v0
.end method
