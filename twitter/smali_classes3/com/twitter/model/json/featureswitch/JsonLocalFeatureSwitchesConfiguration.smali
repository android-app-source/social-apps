.class public Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration$JsonFeatureSwitchesDebug;,
        Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration$JsonFeatureSwitchesDefault;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcdf;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lccy;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "default"
        }
    .end annotation
.end field

.field public b:Ljava/util/Set;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Set;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "experiment_names"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lccx;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lccz;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcdh;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcdf$a;
    .locals 4

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->a:Lccy;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v1, "\'default\' does not exist in manifest."

    invoke-direct {v0, v1}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 49
    new-instance v0, Lcdf$a;

    invoke-direct {v0}, Lcdf$a;-><init>()V

    .line 72
    :goto_0
    return-object v0

    .line 52
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v2

    .line 53
    iget-object v0, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->e:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccz;

    .line 55
    iget-object v3, v0, Lccz;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    goto :goto_1

    .line 59
    :cond_1
    new-instance v0, Lcdd$a;

    invoke-direct {v0}, Lcdd$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->a:Lccy;

    iget-object v1, v1, Lccy;->a:Lccw;

    iget-object v1, v1, Lccw;->a:Ljava/util/Map;

    .line 60
    invoke-virtual {v0, v1}, Lcdd$a;->a(Ljava/util/Map;)Lcdd$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->a:Lccy;

    iget-object v1, v1, Lccy;->b:Ljava/util/Set;

    .line 61
    invoke-virtual {v0, v1}, Lcdd$a;->a(Ljava/util/Set;)Lcdd$a;

    move-result-object v3

    .line 63
    iget-object v0, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->f:Lcdh;

    if-eqz v0, :cond_2

    .line 64
    iget-object v0, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->f:Lcdh;

    iget-object v0, v0, Lcdh;->b:Ljava/lang/String;

    .line 65
    invoke-virtual {v3, v0}, Lcdd$a;->a(Ljava/lang/String;)Lcdd$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->f:Lcdh;

    iget-object v1, v1, Lcdh;->c:Ljava/lang/String;

    .line 66
    invoke-virtual {v0, v1}, Lcdd$a;->b(Ljava/lang/String;)Lcdd$a;

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->d:Lccx;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->d:Lccx;

    iget-object v0, v0, Lccx;->a:Ljava/util/Map;

    move-object v1, v0

    .line 72
    :goto_2
    new-instance v0, Lcdf$a;

    invoke-direct {v0}, Lcdf$a;-><init>()V

    .line 73
    invoke-virtual {v0, v3}, Lcdf$a;->a(Lcdd$a;)Lcdf$a;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->a:Lccy;

    iget-object v3, v3, Lccy;->c:Ljava/lang/String;

    .line 74
    invoke-virtual {v0, v3}, Lcdf$a;->a(Ljava/lang/String;)Lcdf$a;

    move-result-object v3

    .line 75
    invoke-virtual {v2}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v3, v0}, Lcdf$a;->a(Ljava/util/Map;)Lcdf$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->b:Ljava/util/Set;

    .line 76
    invoke-static {v2}, Lcom/twitter/util/collection/ImmutableSet;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcdf$a;->a(Ljava/util/Set;)Lcdf$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->c:Ljava/util/Set;

    .line 77
    invoke-static {v2}, Lcom/twitter/util/collection/ImmutableSet;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcdf$a;->a(Ljava/util/Map;Ljava/util/Set;)Lcdf$a;

    move-result-object v0

    goto/16 :goto_0

    .line 70
    :cond_3
    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v0

    move-object v1, v0

    goto :goto_2
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;->a()Lcdf$a;

    move-result-object v0

    return-object v0
.end method
