.class public Lcom/twitter/model/json/featureswitch/JsonServerFeatureSwitchesConfiguration;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcdg;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lccw;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/util/Set;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcdc$a;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Set;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcdh;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcdg$a;
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/model/json/featureswitch/JsonServerFeatureSwitchesConfiguration;->a:Lccw;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v1, "Invalid feature switch Configs"

    invoke-direct {v0, v1}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 36
    new-instance v0, Lcdg$a;

    invoke-direct {v0}, Lcdg$a;-><init>()V

    .line 49
    :goto_0
    return-object v0

    .line 39
    :cond_0
    new-instance v0, Lcdd$a;

    invoke-direct {v0}, Lcdd$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/featureswitch/JsonServerFeatureSwitchesConfiguration;->a:Lccw;

    iget-object v1, v1, Lccw;->a:Ljava/util/Map;

    .line 40
    invoke-virtual {v0, v1}, Lcdd$a;->a(Ljava/util/Map;)Lcdd$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/featureswitch/JsonServerFeatureSwitchesConfiguration;->b:Ljava/util/Set;

    .line 41
    invoke-virtual {v0, v1}, Lcdd$a;->a(Ljava/util/Set;)Lcdd$a;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/twitter/model/json/featureswitch/JsonServerFeatureSwitchesConfiguration;->d:Lcdh;

    if-eqz v1, :cond_1

    .line 44
    iget-object v1, p0, Lcom/twitter/model/json/featureswitch/JsonServerFeatureSwitchesConfiguration;->d:Lcdh;

    iget-object v1, v1, Lcdh;->b:Ljava/lang/String;

    .line 45
    invoke-virtual {v0, v1}, Lcdd$a;->a(Ljava/lang/String;)Lcdd$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/model/json/featureswitch/JsonServerFeatureSwitchesConfiguration;->d:Lcdh;

    iget-object v2, v2, Lcdh;->c:Ljava/lang/String;

    .line 46
    invoke-virtual {v1, v2}, Lcdd$a;->b(Ljava/lang/String;)Lcdd$a;

    .line 49
    :cond_1
    new-instance v1, Lcdg$a;

    invoke-direct {v1}, Lcdg$a;-><init>()V

    .line 50
    invoke-virtual {v0}, Lcdd$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdd;

    invoke-virtual {v1, v0}, Lcdg$a;->a(Lcdd;)Lcdg$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/featureswitch/JsonServerFeatureSwitchesConfiguration;->c:Ljava/util/Set;

    .line 51
    invoke-static {v1}, Lcom/twitter/util/collection/o;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcdg$a;->a(Ljava/util/Set;)Lcdg$a;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/twitter/model/json/featureswitch/JsonServerFeatureSwitchesConfiguration;->a()Lcdg$a;

    move-result-object v0

    return-object v0
.end method
