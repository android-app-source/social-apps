.class public Lcom/twitter/model/json/safety/JsonMutedKeyword;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcgm;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgm;
    .locals 11

    .prologue
    .line 27
    new-instance v1, Lcgm;

    iget-wide v2, p0, Lcom/twitter/model/json/safety/JsonMutedKeyword;->a:J

    iget-object v4, p0, Lcom/twitter/model/json/safety/JsonMutedKeyword;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/model/json/safety/JsonMutedKeyword;->c:Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/model/json/safety/JsonMutedKeyword;->d:J

    iget-wide v8, p0, Lcom/twitter/model/json/safety/JsonMutedKeyword;->e:J

    const/4 v10, 0x1

    invoke-direct/range {v1 .. v10}, Lcgm;-><init>(JLjava/lang/String;Ljava/lang/String;JJZ)V

    return-object v1
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/safety/JsonMutedKeyword;->a()Lcgm;

    move-result-object v0

    return-object v0
.end method
