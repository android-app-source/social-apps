.class public final Lcom/twitter/model/json/TwitterRegistrar;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/json/common/JsonModelRegistry$Registrar;


# annotations
.annotation build Lcod;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/json/common/JsonModelRegistry$b;)V
    .locals 2

    .prologue
    .line 569
    const-class v0, Lcom/twitter/model/revenue/c;

    const-class v1, Lcom/twitter/model/json/revenue/JsonAdSlot;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 570
    const-class v0, Lcgr;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonAddEntriesInstruction;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 571
    const-class v0, Lcgs;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonAddToThreadInstruction;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 572
    const-class v0, Lcom/twitter/model/businessprofiles/a;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonAddress;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 573
    const-class v0, Lcom/twitter/model/businessprofiles/a$b;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonAddress;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 574
    const-class v0, Lcom/twitter/model/ads/a;

    const-class v1, Lcom/twitter/model/json/ads/JsonAdsAccount;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 575
    const-class v0, Lcom/twitter/model/ads/a$b;

    const-class v1, Lcom/twitter/model/json/ads/JsonAdsAccount;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 576
    const-class v0, Lcom/twitter/model/ads/b;

    const-class v1, Lcom/twitter/model/json/ads/JsonAdsAccountPermission;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 577
    const-class v0, Lcom/twitter/model/ads/b$b;

    const-class v1, Lcom/twitter/model/json/ads/JsonAdsAccountPermission;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 578
    const-class v0, Lcom/twitter/model/av/d;

    const-class v1, Lcom/twitter/model/json/av/JsonVideoAnalyticsScribe;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 579
    const-class v0, Lcom/twitter/model/businessprofiles/b;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonAppWithCard;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 580
    const-class v0, Lcom/twitter/model/businessprofiles/b$b;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonAppWithCard;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 581
    const-class v0, Lcgj;

    const-class v1, Lcom/twitter/model/json/periscope/JsonAuthenticatePeriscopeResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 582
    const-class v0, Lcgj$a;

    const-class v1, Lcom/twitter/model/json/periscope/JsonAuthenticatePeriscopeResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 583
    const-class v0, Lcom/twitter/model/moments/a;

    const-class v1, Lcom/twitter/model/json/moments/JsonAuthorInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 584
    const-class v0, Lcom/twitter/model/account/a;

    const-class v1, Lcom/twitter/model/json/account/JsonAvailability;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 585
    const-class v0, Lcom/twitter/model/account/b;

    const-class v1, Lcom/twitter/model/json/account/JsonBackupCodeRequest;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 586
    const-class v0, Lcom/twitter/model/timeline/a;

    const-class v1, Lcom/twitter/model/json/timeline/JsonBanner;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 587
    const-class v0, Lcom/twitter/model/timeline/b;

    const-class v1, Lcom/twitter/model/json/timeline/JsonBannerPrompt;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 588
    const-class v0, Lcom/twitter/model/timeline/c;

    const-class v1, Lcom/twitter/model/json/timeline/JsonBannerSocialProof;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 589
    const-class v0, Lcom/twitter/model/timeline/d;

    const-class v1, Lcom/twitter/model/json/timeline/JsonBannerSocialProofUsers;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 590
    const-class v0, Lcby;

    const-class v1, Lcom/twitter/model/json/dms/JsonDMLocationAttachment;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 591
    const-class v0, Lcck;

    const-class v1, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyConfig;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 592
    const-class v0, Lcaw;

    const-class v1, Lcom/twitter/model/json/card/JsonBindingValue;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 593
    const-class v0, Lcom/twitter/model/livevideo/a;

    const-class v1, Lcom/twitter/model/json/livevideo/JsonBroadcast;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 594
    const-class v0, Lcom/twitter/model/livevideo/a$b;

    const-class v1, Lcom/twitter/model/json/livevideo/JsonBroadcast;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 595
    const-class v0, Lcom/twitter/model/businessprofiles/c;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonBusinessHours;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 596
    const-class v0, Lcom/twitter/model/businessprofiles/c$a;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonBusinessHours;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 597
    const-class v0, Lcom/twitter/model/businessprofiles/d;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonBusinessProfile;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 598
    const-class v0, Lcom/twitter/model/businessprofiles/d$a;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonBusinessProfile;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 599
    const-class v0, Lcom/twitter/model/businessprofiles/e;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonBusinessResponseData;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 600
    const-class v0, Lcom/twitter/model/businessprofiles/f;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonBusinessUrls;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 601
    const-class v0, Lcom/twitter/model/businessprofiles/f$a;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonBusinessUrls;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 602
    const-class v0, Lceg;

    const-class v1, Lcom/twitter/model/json/moments/JsonCTA;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 603
    const-class v0, Lceg$a;

    const-class v1, Lcom/twitter/model/json/moments/JsonCTA;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 604
    const-class v0, Lcom/twitter/model/revenue/d;

    const-class v1, Lcom/twitter/model/json/revenue/JsonCampaignMetadata;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 605
    const-class v0, Lcom/twitter/model/moments/b;

    const-class v1, Lcom/twitter/model/json/moments/JsonCapsuleError;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 606
    const-class v0, Lcom/twitter/model/moments/b$a;

    const-class v1, Lcom/twitter/model/json/moments/JsonCapsuleError;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 607
    const-class v0, Lcom/twitter/model/moments/c;

    const-class v1, Lcom/twitter/model/json/moments/JsonCapsuleErrors;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 608
    const-class v0, Lcax;

    const-class v1, Lcom/twitter/model/json/card/JsonCardInstanceData;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 609
    const-class v0, Lcom/twitter/model/core/b;

    const-class v1, Lcom/twitter/model/json/core/JsonCashtagEntity;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 610
    const-class v0, Lcom/twitter/model/core/b$a;

    const-class v1, Lcom/twitter/model/json/core/JsonCashtagEntity;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 611
    const-class v0, Lcom/twitter/model/media/a;

    const-class v1, Lcom/twitter/model/json/stratostore/JsonMediaEntityColorPalette$JsonMediaEntityColorDescriptor;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 612
    const-class v0, Lcom/twitter/model/livepipeline/a;

    const-class v1, Lcom/twitter/model/json/livepipeline/JsonConfigEventBuilder;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 613
    const-class v0, Lcom/twitter/model/livepipeline/a$a;

    const-class v1, Lcom/twitter/model/json/livepipeline/JsonConfigEventBuilder;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 614
    const-class v0, Lcom/twitter/model/businessprofiles/g;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonContactInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 615
    const-class v0, Lcom/twitter/model/businessprofiles/g$a;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonContactInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 616
    const-class v0, Lcgb$b;

    const-class v1, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonControlType;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 617
    const-class v0, Lcgb$b$a;

    const-class v1, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonControlType;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 618
    const-class v0, Lcgt;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonConversationComponent;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 619
    const-class v0, Lcom/twitter/model/dms/j;

    const-class v1, Lcom/twitter/model/json/dms/JsonConversationCreateEntry;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 620
    const-class v0, Lcom/twitter/model/dms/j$a;

    const-class v1, Lcom/twitter/model/json/dms/JsonConversationCreateEntry;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 621
    const-class v0, Lcom/twitter/model/dms/l;

    const-class v1, Lcom/twitter/model/json/dms/JsonConversationInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 622
    const-class v0, Lcom/twitter/model/dms/l$a;

    const-class v1, Lcom/twitter/model/json/dms/JsonConversationInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 623
    const-class v0, Lcgv;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonConversationThread;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 624
    const-class v0, Lcgw;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonConversationTweet;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 625
    const-class v0, Lcom/twitter/model/geo/b;

    const-class v1, Lcom/twitter/model/json/geo/JsonCoordinate;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 626
    const-class v0, Lcom/twitter/model/moments/d;

    const-class v1, Lcom/twitter/model/json/moments/JsonCropData;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 627
    const-class v0, Lcom/twitter/model/moments/d$a;

    const-class v1, Lcom/twitter/model/json/moments/JsonCropData;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 628
    const-class v0, Lcom/twitter/model/moments/e;

    const-class v1, Lcom/twitter/model/json/moments/JsonCropHint;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 629
    const-class v0, Lcom/twitter/model/moments/e$a;

    const-class v1, Lcom/twitter/model/json/moments/JsonCropHint;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 630
    const-class v0, Lcom/twitter/model/moments/f;

    const-class v1, Lcom/twitter/model/json/moments/JsonCurationMetadata;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 631
    const-class v0, Lcom/twitter/model/core/c;

    const-class v1, Lcom/twitter/model/json/core/JsonCursorTimestamp;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 632
    const-class v0, Lcom/twitter/model/dms/o;

    const-class v1, Lcom/twitter/model/json/dms/JsonDMAgentProfile;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 633
    const-class v0, Lcom/twitter/model/dms/o$a;

    const-class v1, Lcom/twitter/model/json/dms/JsonDMAgentProfile;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 634
    const-class v0, Lcom/twitter/model/dms/t;

    const-class v1, Lcom/twitter/model/json/dms/JsonDMPermission;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 635
    const-class v0, Lccm;

    const-class v1, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyEncryptedTextInput;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 636
    const-class v0, Lccm$a;

    const-class v1, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyEncryptedTextInput;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 637
    const-class v0, Lccp;

    const-class v1, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyOption;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 638
    const-class v0, Lccp$a;

    const-class v1, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyOption;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 639
    const-class v0, Lccs;

    const-class v1, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyTextInput;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 640
    const-class v0, Lccs$c;

    const-class v1, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyTextInput;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 641
    const-class v0, Lcch;

    const-class v1, Lcom/twitter/model/json/dms/JsonStickerAttachment;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 642
    const-class v0, Lcch$a;

    const-class v1, Lcom/twitter/model/json/dms/JsonStickerAttachment;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 643
    const-class v0, Lcom/twitter/model/dms/v;

    const-class v1, Lcom/twitter/model/json/dms/JsonDeleteConversationEvent;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 644
    const-class v0, Lcom/twitter/model/dms/v$a;

    const-class v1, Lcom/twitter/model/json/dms/JsonDeleteConversationEvent;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 645
    const-class v0, Lcom/twitter/model/dms/w;

    const-class v1, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 646
    const-class v0, Lcgl;

    const-class v1, Lcom/twitter/model/json/safety/JsonDiscouragedKeywords;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 647
    const-class v0, Lcad;

    const-class v1, Lcom/twitter/model/json/activity/JsonDismissAction;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 648
    const-class v0, Lcae;

    const-class v1, Lcom/twitter/model/json/activity/JsonDismissMenu;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 649
    const-class v0, Lcaf;

    const-class v1, Lcom/twitter/model/json/activity/JsonDismissMenuOption;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 650
    const-class v0, Lcag;

    const-class v1, Lcom/twitter/model/json/activity/JsonDisplayText;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 651
    const-class v0, Lcom/twitter/model/livepipeline/b;

    const-class v1, Lcom/twitter/model/json/livepipeline/JsonDmUpdateEventBuilder;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 652
    const-class v0, Lcom/twitter/model/livepipeline/b$a;

    const-class v1, Lcom/twitter/model/json/livepipeline/JsonDmUpdateEventBuilder;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 653
    const-class v0, Lcom/twitter/model/av/e;

    const-class v1, Lcom/twitter/model/json/av/JsonVideoAd;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 654
    const-class v0, Lcom/twitter/model/av/DynamicAdMediaInfo;

    const-class v1, Lcom/twitter/model/json/av/JsonMediaInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 655
    const-class v0, Lcom/twitter/model/stratostore/e$c;

    const-class v1, Lcom/twitter/model/json/stratostore/JsonStratostoreError;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 656
    const-class v0, Lcom/twitter/model/moments/g;

    const-class v1, Lcom/twitter/model/json/moments/JsonEvent;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 657
    const-class v0, Lcom/twitter/model/moments/g$a;

    const-class v1, Lcom/twitter/model/json/moments/JsonEvent;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 658
    const-class v0, Lcom/twitter/model/profile/ExtendedProfile;

    const-class v1, Lcom/twitter/model/json/profiles/JsonExtendedProfile;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 659
    const-class v0, Lcom/twitter/model/profile/ExtendedProfile$a;

    const-class v1, Lcom/twitter/model/json/profiles/JsonExtendedProfile;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 660
    const-class v0, Lccz$a;

    const-class v1, Lcom/twitter/model/json/featureswitch/JsonFeatureSwitchesEmbeddedExperiment$JsonFeatureSwitchesBucket;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 661
    const-class v0, Lccx;

    const-class v1, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration$JsonFeatureSwitchesDebug;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 662
    const-class v0, Lccy;

    const-class v1, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration$JsonFeatureSwitchesDefault;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 663
    const-class v0, Lccz;

    const-class v1, Lcom/twitter/model/json/featureswitch/JsonFeatureSwitchesEmbeddedExperiment;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 664
    const-class v0, Lcda;

    const-class v1, Lcom/twitter/model/json/featureswitch/JsonFeatureSwitchesFacet;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 665
    const-class v0, Lcdc$a;

    const-class v1, Lcom/twitter/model/json/featureswitch/JsonFeatureSwitchesImpression;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 666
    const-class v0, Lcdb;

    const-class v1, Lcom/twitter/model/json/featureswitch/JsonFeatureSwitchesParameter;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 667
    const-class v0, Lcom/twitter/model/timeline/g;

    const-class v1, Lcom/twitter/model/json/timeline/JsonFeedbackAction;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 668
    const-class v0, Lcom/twitter/model/timeline/h;

    const-class v1, Lcom/twitter/model/json/timeline/JsonFeedbackInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 669
    const-class v0, Lcom/twitter/model/media/foundmedia/a;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonFoundMediaCursor;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 670
    const-class v0, Lcom/twitter/model/media/foundmedia/b;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonFoundMediaData;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 671
    const-class v0, Lcom/twitter/model/media/foundmedia/c;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonFoundMediaGroup;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 672
    const-class v0, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonFoundMediaImageVariant;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 673
    const-class v0, Lcom/twitter/model/media/foundmedia/d;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonFoundMediaItem;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 674
    const-class v0, Lcom/twitter/model/media/foundmedia/FoundMediaOrigin;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonFoundMediaOrigin;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 675
    const-class v0, Lcom/twitter/model/media/foundmedia/FoundMediaProvider;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonFoundMediaProvider;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 676
    const-class v0, Lcom/twitter/model/media/foundmedia/e;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonFoundMediaResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 677
    const-class v0, Lcom/twitter/model/geo/VendorInfo$a;

    const-class v1, Lcom/twitter/model/json/geo/JsonVendorInfo$JsonFourSquareInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 678
    const-class v0, Lcaj;

    const-class v1, Lcom/twitter/model/json/activity/JsonGenericActivity;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 679
    const-class v0, Lcom/twitter/model/media/foundmedia/f;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonGiphyCategories;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 680
    const-class v0, Lcom/twitter/model/media/foundmedia/g;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonGiphyCategory;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 681
    const-class v0, Lcom/twitter/model/media/foundmedia/h;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonGiphyImage;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 682
    const-class v0, Lcom/twitter/model/media/foundmedia/i;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonGiphyImages;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 683
    const-class v0, Lcom/twitter/model/media/foundmedia/j;

    const-class v1, Lcom/twitter/model/json/media/foundmedia/JsonGiphyPagination;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 684
    const-class v0, Lcgx;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonGlobalObjects;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 685
    const-class v0, Lcom/twitter/model/moments/GuideCategories;

    const-class v1, Lcom/twitter/model/json/moments/JsonGuideCategories;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 686
    const-class v0, Lcom/twitter/model/moments/h;

    const-class v1, Lcom/twitter/model/json/moments/JsonGuideCategory;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 687
    const-class v0, Lcei;

    const-class v1, Lcom/twitter/model/json/moments/JsonGuide;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 688
    const-class v0, Lcom/twitter/model/moments/i;

    const-class v1, Lcom/twitter/model/json/moments/JsonGuideTrend;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 689
    const-class v0, Lcom/twitter/model/moments/i$a;

    const-class v1, Lcom/twitter/model/json/moments/JsonGuideTrend;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 690
    const-class v0, Lcom/twitter/model/moments/j;

    const-class v1, Lcom/twitter/model/json/moments/JsonGuideTrendSection;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 691
    const-class v0, Lcom/twitter/model/moments/k;

    const-class v1, Lcom/twitter/model/json/moments/JsonGuideTrendSectionMetadata;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 692
    const-class v0, Lcom/twitter/model/core/h;

    const-class v1, Lcom/twitter/model/json/core/JsonHashtagEntity;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 693
    const-class v0, Lcom/twitter/model/core/h$a;

    const-class v1, Lcom/twitter/model/json/core/JsonHashtagEntity;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 694
    const-class v0, Lcom/twitter/model/moments/l;

    const-class v1, Lcom/twitter/model/json/moments/JsonHideUrlEntities;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 695
    const-class v0, Lcom/twitter/model/core/i;

    const-class v1, Lcom/twitter/model/json/search/JsonHitHighlight;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 696
    const-class v0, Lcom/twitter/model/core/i$a;

    const-class v1, Lcom/twitter/model/json/search/JsonHitHighlight;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 697
    const-class v0, Lcgy;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonHomeConversation;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 698
    const-class v0, Lcom/twitter/model/businessprofiles/i;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonHourMinute;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 699
    const-class v0, Lcom/twitter/model/businessprofiles/j;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonHourMinuteRange;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 700
    const-class v0, Lcom/twitter/model/businessprofiles/k;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonHydratedBusinessHours;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 701
    const-class v0, Lcom/twitter/model/card/property/ImageSpec;

    const-class v1, Lcom/twitter/model/json/card/JsonImageSpec;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 702
    const-class v0, Lcom/twitter/model/moments/m;

    const-class v1, Lcom/twitter/model/json/moments/JsonMomentInfoBadge;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 703
    const-class v0, Lcom/twitter/model/timeline/k;

    const-class v1, Lcom/twitter/model/json/timeline/JsonDismissInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 704
    const-class v0, Lcdk;

    const-class v1, Lcom/twitter/model/json/irs/JsonInstallReferrer;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 705
    const-class v0, Lcdq;

    const-class v1, Lcom/twitter/model/json/media/stickers/JsonInvalidSticker;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 706
    const-class v0, Lcdr;

    const-class v1, Lcom/twitter/model/json/media/stickers/JsonInvalidStickersResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 707
    const-class v0, Lcom/twitter/model/moments/n;

    const-class v1, Lcom/twitter/model/json/moments/JsonLinkTitleCard;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 708
    const-class v0, Lcom/twitter/model/livevideo/b;

    const-class v1, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 709
    const-class v0, Lcom/twitter/model/livevideo/b$a;

    const-class v1, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 710
    const-class v0, Lcej;

    const-class v1, Lcom/twitter/model/json/moments/JsonLiveVideoHero;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 711
    const-class v0, Lcom/twitter/model/av/g;

    const-class v1, Lcom/twitter/model/json/livevideo/JsonLiveVideoStream;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 712
    const-class v0, Lcdf;

    const-class v1, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 713
    const-class v0, Lcdf$a;

    const-class v1, Lcom/twitter/model/json/featureswitch/JsonLocalFeatureSwitchesConfiguration;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 714
    const-class v0, Lcom/twitter/model/account/LoginResponse;

    const-class v1, Lcom/twitter/model/json/account/JsonLoginResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 715
    const-class v0, Lcom/twitter/model/account/LoginVerificationRequest;

    const-class v1, Lcom/twitter/model/json/account/JsonLoginVerificationRequest;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 716
    const-class v0, Lcom/twitter/model/account/LvEligibilityResponse;

    const-class v1, Lcom/twitter/model/json/account/JsonLoginVerificationEligibility;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 717
    const-class v0, Lcfo$b;

    const-class v1, Lcom/twitter/model/json/moments/sports/JsonMomentSportsParticipant$JsonParticipantMedia;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 718
    const-class v0, Lcom/twitter/model/stratostore/MediaColorData;

    const-class v1, Lcom/twitter/model/json/stratostore/JsonMediaEntityColorPalette;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 719
    const-class v0, Lcom/twitter/model/core/MediaEntity;

    const-class v1, Lcom/twitter/model/json/core/JsonMediaEntity;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 720
    const-class v0, Lcom/twitter/model/core/MediaEntity$a;

    const-class v1, Lcom/twitter/model/json/core/JsonMediaEntity;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 721
    const-class v0, Lcom/twitter/model/stratostore/b;

    const-class v1, Lcom/twitter/model/json/stratostore/JsonMediaEntityRestrictions;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 722
    const-class v0, Lcom/twitter/model/stratostore/c;

    const-class v1, Lcom/twitter/model/json/stratostore/JsonMediaEntityStats;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 723
    const-class v0, Lcom/twitter/model/av/h;

    const-class v1, Lcom/twitter/model/json/av/JsonMediaMonetizationMetadata;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 724
    const-class v0, Lcom/twitter/model/core/o;

    const-class v1, Lcom/twitter/model/json/core/JsonMediaVideoInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 725
    const-class v0, Lcom/twitter/model/core/p;

    const-class v1, Lcom/twitter/model/json/core/JsonMediaVideoVariant;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 726
    const-class v0, Lcom/twitter/model/core/q;

    const-class v1, Lcom/twitter/model/json/core/JsonMentionEntity;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 727
    const-class v0, Lcom/twitter/model/core/q$a;

    const-class v1, Lcom/twitter/model/json/core/JsonMentionEntity;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 728
    const-class v0, Lcom/twitter/model/timeline/l;

    const-class v1, Lcom/twitter/model/json/timeline/JsonMessageAction;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 729
    const-class v0, Lcom/twitter/model/dms/ac;

    const-class v1, Lcom/twitter/model/json/dms/JsonMessageCreateInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 730
    const-class v0, Lcom/twitter/model/revenue/a$b;

    const-class v1, Lcom/twitter/model/json/revenue/JsonAdMetadata;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 731
    const-class v0, Lcom/twitter/model/revenue/a$b$a;

    const-class v1, Lcom/twitter/model/json/revenue/JsonAdMetadata;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 732
    const-class v0, Lcfq;

    const-class v1, Lcom/twitter/model/json/notifications/JsonMobileSettingsResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 733
    const-class v0, Lcfq$a;

    const-class v1, Lcom/twitter/model/json/notifications/JsonMobileSettingsResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 734
    const-class v0, Lcom/twitter/model/people/c;

    const-class v1, Lcom/twitter/model/json/people/JsonAvatars;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 735
    const-class v0, Lcom/twitter/model/timeline/m;

    const-class v1, Lcom/twitter/model/json/timeline/JsonModuleFooter;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 736
    const-class v0, Lcom/twitter/model/people/d;

    const-class v1, Lcom/twitter/model/json/people/JsonModuleHeader;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 737
    const-class v0, Lcom/twitter/model/people/d$a;

    const-class v1, Lcom/twitter/model/json/people/JsonModuleHeader;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 738
    const-class v0, Lcom/twitter/model/people/i;

    const-class v1, Lcom/twitter/model/json/people/JsonModuleShowMore;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 739
    const-class v0, Lcom/twitter/model/people/i$a;

    const-class v1, Lcom/twitter/model/json/people/JsonModuleShowMore;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 740
    const-class v0, Lcom/twitter/model/people/ModuleTitle;

    const-class v1, Lcom/twitter/model/json/people/JsonModuleTitle;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 741
    const-class v0, Lcom/twitter/model/people/ModuleTitle$a;

    const-class v1, Lcom/twitter/model/json/people/JsonModuleTitle;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 742
    const-class v0, Lcom/twitter/model/moments/Moment;

    const-class v1, Lcom/twitter/model/json/moments/JsonMoment;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 743
    const-class v0, Lcom/twitter/model/moments/Moment$a;

    const-class v1, Lcom/twitter/model/json/moments/JsonMoment;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 744
    const-class v0, Lcek;

    const-class v1, Lcom/twitter/model/json/moments/JsonGuideSection;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 745
    const-class v0, Lcek$a;

    const-class v1, Lcom/twitter/model/json/moments/JsonGuideSection;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 746
    const-class v0, Lcel;

    const-class v1, Lcom/twitter/model/json/moments/JsonMomentLikeResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 747
    const-class v0, Lcem;

    const-class v1, Lcom/twitter/model/json/moments/JsonMomentMedia;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 748
    const-class v0, Lcen;

    const-class v1, Lcom/twitter/model/json/moments/JsonMomentModule;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 749
    const-class v0, Lcen$a;

    const-class v1, Lcom/twitter/model/json/moments/JsonMomentModule;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 750
    const-class v0, Lceo;

    const-class v1, Lcom/twitter/model/json/moments/JsonMomentPage;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 751
    const-class v0, Lceo$a;

    const-class v1, Lcom/twitter/model/json/moments/JsonMomentPage;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 752
    const-class v0, Lcfn;

    const-class v1, Lcom/twitter/model/json/moments/sports/JsonMomentSportsEvent;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 753
    const-class v0, Lcfn$a;

    const-class v1, Lcom/twitter/model/json/moments/sports/JsonMomentSportsEvent;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 754
    const-class v0, Lcfo;

    const-class v1, Lcom/twitter/model/json/moments/sports/JsonMomentSportsParticipant;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 755
    const-class v0, Lcfo$a;

    const-class v1, Lcom/twitter/model/json/moments/sports/JsonMomentSportsParticipant;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 756
    const-class v0, Lcfp;

    const-class v1, Lcom/twitter/model/json/moments/sports/JsonMomentSportsResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 757
    const-class v0, Lcgm;

    const-class v1, Lcom/twitter/model/json/safety/JsonMutedKeyword;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 758
    const-class v0, Lcgn;

    const-class v1, Lcom/twitter/model/json/safety/JsonMutedKeywords;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 759
    const-class v0, Lcom/twitter/model/onboarding/NavigationLink;

    const-class v1, Lcom/twitter/model/json/onboarding/JsonNavigationLink;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 760
    const-class v0, Lcfr;

    const-class v1, Lcom/twitter/model/json/notifications/JsonNotificationAction;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 761
    const-class v0, Lcfs;

    const-class v1, Lcom/twitter/model/json/notifications/JsonNotificationContextUser;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 762
    const-class v0, Lcan;

    const-class v1, Lcom/twitter/model/json/activity/JsonNotificationIcon;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 763
    const-class v0, Lcgb$c;

    const-class v1, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonNotificationSetting;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 764
    const-class v0, Lcgb$c$a;

    const-class v1, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonNotificationSetting;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 765
    const-class v0, Lcfu;

    const-class v1, Lcom/twitter/model/json/notifications/JsonNotificationTweet;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 766
    const-class v0, Lcfv;

    const-class v1, Lcom/twitter/model/json/notifications/JsonNotificationUser;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 767
    const-class v0, Lcfw;

    const-class v1, Lcom/twitter/model/json/notifications/JsonNotificationUsers;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 768
    const-class v0, Lcom/twitter/model/login/OneFactorEligibleFactor;

    const-class v1, Lcom/twitter/model/json/login/JsonOneFactorEligibleFactor;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 769
    const-class v0, Lcom/twitter/model/dms/Participant;

    const-class v1, Lcom/twitter/model/json/dms/JsonParticipant;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 770
    const-class v0, Lcom/twitter/model/dms/Participant$a;

    const-class v1, Lcom/twitter/model/json/dms/JsonParticipant;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 771
    const-class v0, Lcfn$c;

    const-class v1, Lcom/twitter/model/json/moments/sports/JsonMomentSportsEvent$JsonParticipantScore;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 772
    const-class v0, Lcgk;

    const-class v1, Lcom/twitter/model/json/periscope/JsonPeriscopeAuthConsentState;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 773
    const-class v0, Lcgk$a;

    const-class v1, Lcom/twitter/model/json/periscope/JsonPeriscopeAuthConsentState;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 774
    const-class v0, Lcom/twitter/model/onboarding/permission/a;

    const-class v1, Lcom/twitter/model/json/onboarding/JsonPermissionReport;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 775
    const-class v0, Lcbv;

    const-class v1, Lcom/twitter/model/json/device/JsonDevice;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 776
    const-class v0, Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonPhoneNumber;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 777
    const-class v0, Lcom/twitter/model/people/k;

    const-class v1, Lcom/twitter/model/json/people/JsonPivotAction;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 778
    const-class v0, Lceq;

    const-class v1, Lcom/twitter/model/json/moments/JsonMomentPivotResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 779
    const-class v0, Lcom/twitter/model/geo/d$a;

    const-class v1, Lcom/twitter/model/json/geo/JsonPlacePageResponse$JsonPlacePageHeader;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 780
    const-class v0, Lcom/twitter/model/geo/d$b;

    const-class v1, Lcom/twitter/model/json/geo/JsonPlacePageResponse$JsonPlacePageTimeline;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 781
    const-class v0, Lcav;

    const-class v1, Lcom/twitter/model/json/card/JsonPollCompose;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 782
    const-class v0, Lcgi;

    const-class v1, Lcom/twitter/model/json/pc/JsonPromotedContent;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 783
    const-class v0, Lcgi$a;

    const-class v1, Lcom/twitter/model/json/pc/JsonPromotedContent;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 784
    const-class v0, Lchb;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonPromotedTrendMetadata;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 785
    const-class v0, Lcfy;

    const-class v1, Lcom/twitter/model/json/notifications/JsonPushDeviceErrorResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 786
    const-class v0, Lcfz;

    const-class v1, Lcom/twitter/model/json/notifications/JsonPushDeviceResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 787
    const-class v0, Lcom/twitter/model/core/s;

    const-class v1, Lcom/twitter/model/json/core/JsonRecommendationReason;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 788
    const-class v0, Lcom/twitter/model/core/s$a;

    const-class v1, Lcom/twitter/model/json/core/JsonRecommendationReason;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 789
    const-class v0, Lcfh;

    const-class v1, Lcom/twitter/model/json/moments/maker/JsonRecommendationsResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 790
    const-class v0, Lcom/twitter/model/livevideo/c;

    const-class v1, Lcom/twitter/model/json/livevideo/JsonRemindMe;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 791
    const-class v0, Lchc;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonResponseObjects;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 792
    const-class v0, Lcom/twitter/model/timeline/q;

    const-class v1, Lcom/twitter/model/json/timeline/JsonRichTimelineResponseCursor;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 793
    const-class v0, Lcom/twitter/model/timeline/r;

    const-class v1, Lcom/twitter/model/json/timeline/JsonScribeInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 794
    const-class v0, Lcom/twitter/model/timeline/r$a;

    const-class v1, Lcom/twitter/model/json/timeline/JsonScribeInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 795
    const-class v0, Lcdg;

    const-class v1, Lcom/twitter/model/json/featureswitch/JsonServerFeatureSwitchesConfiguration;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 796
    const-class v0, Lcdg$a;

    const-class v1, Lcom/twitter/model/json/featureswitch/JsonServerFeatureSwitchesConfiguration;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 797
    const-class v0, Lcdh;

    const-class v1, Lcom/twitter/model/json/featureswitch/JsonSettingVersionDetails;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 798
    const-class v0, Lcgb;

    const-class v1, Lcom/twitter/model/json/notifications/JsonSettingsTemplate;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 799
    const-class v0, Lcgb$a;

    const-class v1, Lcom/twitter/model/json/notifications/JsonSettingsTemplate;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 800
    const-class v0, Lcgb$d;

    const-class v1, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateContents;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 801
    const-class v0, Lcgb$d$a;

    const-class v1, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateContents;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 802
    const-class v0, Lcgb$e;

    const-class v1, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateDoc;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 803
    const-class v0, Lcgb$e$a;

    const-class v1, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateDoc;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 804
    const-class v0, Lcom/twitter/model/businessprofiles/n;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonShortenedUrl;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 805
    const-class v0, Lche;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonShowAlertInstruction;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 806
    const-class v0, Lcom/twitter/model/account/UserSettings$b;

    const-class v1, Lcom/twitter/model/json/account/JsonUserSettingsSleepTime;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 807
    const-class v0, Lcom/twitter/model/moments/s;

    const-class v1, Lcom/twitter/model/json/moments/JsonSocialProof;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 808
    const-class v0, Lcgo;

    const-class v1, Lcom/twitter/model/json/search/urt/JsonSpelling;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 809
    const-class v0, Lcgp;

    const-class v1, Lcom/twitter/model/json/search/urt/JsonSpellingResult;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 810
    const-class v0, Lcdo;

    const-class v1, Lcom/twitter/model/json/media/sru/JsonSruError;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 811
    const-class v0, Lcdp;

    const-class v1, Lcom/twitter/model/json/media/sru/JsonSruResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 812
    const-class v0, Lcdu;

    const-class v1, Lcom/twitter/model/json/media/stickers/JsonSticker;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 813
    const-class v0, Lcdu$a;

    const-class v1, Lcom/twitter/model/json/media/stickers/JsonSticker;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 814
    const-class v0, Lcdv;

    const-class v1, Lcom/twitter/model/json/media/stickers/JsonStickerCatalogResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 815
    const-class v0, Lcdw;

    const-class v1, Lcom/twitter/model/json/media/stickers/JsonStickerImage;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 816
    const-class v0, Lcom/twitter/model/stratostore/d;

    const-class v1, Lcom/twitter/model/json/stratostore/JsonStickerInfoMetadata;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 817
    const-class v0, Lcdx;

    const-class v1, Lcom/twitter/model/json/media/stickers/JsonStickerResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 818
    const-class v0, Lcea;

    const-class v1, Lcom/twitter/model/json/media/stickers/JsonStickerVariants;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 819
    const-class v0, Lceb;

    const-class v1, Lcom/twitter/model/json/media/stickers/JsonStickerAuthor;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 820
    const-class v0, Lcec;

    const-class v1, Lcom/twitter/model/json/media/stickers/JsonStickerCategory;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 821
    const-class v0, Lcee;

    const-class v1, Lcom/twitter/model/json/media/stickers/JsonStickerItem;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 822
    const-class v0, Lcom/twitter/model/livepipeline/e;

    const-class v1, Lcom/twitter/model/json/livepipeline/JsonSubscriptionError;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 823
    const-class v0, Lcom/twitter/model/livepipeline/f;

    const-class v1, Lcom/twitter/model/json/livepipeline/JsonSubscriptionEventBuilder;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 824
    const-class v0, Lcom/twitter/model/livepipeline/f$a;

    const-class v1, Lcom/twitter/model/json/livepipeline/JsonSubscriptionEventBuilder;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 825
    const-class v0, Lcer;

    const-class v1, Lcom/twitter/model/json/moments/JsonSubscriptionStatuses;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 826
    const-class v0, Lcom/twitter/model/livevideo/d;

    const-class v1, Lcom/twitter/model/json/livevideo/JsonSubscriptions;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 827
    const-class v0, Lcom/twitter/model/moments/v;

    const-class v1, Lcom/twitter/model/json/moments/JsonSuggestedMomentsInjection;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 828
    const-class v0, Lcom/twitter/model/moments/v$a;

    const-class v1, Lcom/twitter/model/json/moments/JsonSuggestedMomentsInjection;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 829
    const-class v0, Lcom/twitter/model/account/c;

    const-class v1, Lcom/twitter/model/json/account/JsonTemporaryAppPwRequest;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 830
    const-class v0, Lcom/twitter/model/moments/w;

    const-class v1, Lcom/twitter/model/json/moments/JsonThemeData;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 831
    const-class v0, Lcom/twitter/model/businessprofiles/o;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonTimeRange;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 832
    const-class v0, Lchf;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonTimeline;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 833
    const-class v0, Lcom/twitter/model/timeline/u;

    const-class v1, Lcom/twitter/model/json/timeline/JsonRichTimelineNotification;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 834
    const-class v0, Lchi;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineEntry;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 835
    const-class v0, Lchj;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineFeedbackInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 836
    const-class v0, Lchk;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonHeaderAvatar;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 837
    const-class v0, Lchl;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonHeaderContext;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 838
    const-class v0, Lcom/twitter/model/livevideo/e;

    const-class v1, Lcom/twitter/model/json/livevideo/JsonTimelineInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 839
    const-class v0, Lcom/twitter/model/livevideo/e$a;

    const-class v1, Lcom/twitter/model/json/livevideo/JsonTimelineInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 840
    const-class v0, Lchn;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineInstruction;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 841
    const-class v0, Lchp;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineLabel;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 842
    const-class v0, Lchs;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonModuleHeader;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 843
    const-class v0, Lcht;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineMoment;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 844
    const-class v0, Lchx;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 845
    const-class v0, Lchx$a;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 846
    const-class v0, Lchy;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineTrend;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 847
    const-class v0, Lcia;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineTweet;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 848
    const-class v0, Lcic;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineUser;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 849
    const-class v0, Lcdj;

    const-class v1, Lcom/twitter/model/json/interestpicker/JsonTopicList;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 850
    const-class v0, Lcom/twitter/model/account/d;

    const-class v1, Lcom/twitter/model/json/account/JsonTotpRequest;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 851
    const-class v0, Lcom/twitter/model/core/Translation;

    const-class v1, Lcom/twitter/model/json/core/JsonTranslatedTweet;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 852
    const-class v0, Lcom/twitter/model/account/UserSettings$c;

    const-class v1, Lcom/twitter/model/json/account/JsonUserSettingsTrendLocation;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 853
    const-class v0, Lcom/twitter/model/core/v;

    const-class v1, Lcom/twitter/model/json/core/JsonTweetEntities;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 854
    const-class v0, Lcom/twitter/model/core/v$a;

    const-class v1, Lcom/twitter/model/json/core/JsonTweetEntities;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 855
    const-class v0, Lcom/twitter/model/businessprofiles/p;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonTweetList;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 856
    const-class v0, Lcom/twitter/model/businessprofiles/p$a;

    const-class v1, Lcom/twitter/model/json/businessprofiles/JsonTweetList;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 857
    const-class v0, Lcom/twitter/model/stratostore/g;

    const-class v1, Lcom/twitter/model/json/stratostore/JsonTweetViewCountData;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 858
    const-class v0, Lcom/twitter/model/core/y;

    const-class v1, Lcom/twitter/model/json/core/JsonTwitterError;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 859
    const-class v0, Lcom/twitter/model/core/z;

    const-class v1, Lcom/twitter/model/json/core/JsonTwitterErrors;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 860
    const-class v0, Lcom/twitter/model/core/aa;

    const-class v1, Lcom/twitter/model/json/core/JsonTwitterList;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 861
    const-class v0, Lcom/twitter/model/core/aa$a;

    const-class v1, Lcom/twitter/model/json/core/JsonTwitterList;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 862
    const-class v0, Lcom/twitter/model/core/ab;

    const-class v1, Lcom/twitter/model/json/core/JsonTwitterListsResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 863
    const-class v0, Lcom/twitter/model/geo/TwitterPlace;

    const-class v1, Lcom/twitter/model/json/geo/JsonTwitterPlace;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 864
    const-class v0, Lcom/twitter/model/geo/d;

    const-class v1, Lcom/twitter/model/json/geo/JsonPlacePageResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 865
    const-class v0, Lcom/twitter/model/search/b;

    const-class v1, Lcom/twitter/model/json/search/JsonTwitterSearchQuery;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 866
    const-class v0, Lcom/twitter/model/search/b$a;

    const-class v1, Lcom/twitter/model/json/search/JsonTwitterSearchQuery;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 867
    const-class v0, Lcom/twitter/model/core/ac;

    const-class v1, Lcom/twitter/model/json/core/JsonTwitterStatus;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 868
    const-class v0, Lcom/twitter/model/core/ac$a;

    const-class v1, Lcom/twitter/model/json/core/JsonTwitterStatus;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 869
    const-class v0, Lcom/twitter/model/search/e;

    const-class v1, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 870
    const-class v0, Lcom/twitter/model/search/e$a;

    const-class v1, Lcom/twitter/model/json/search/JsonTwitterStatusMetadata;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 871
    const-class v0, Lcom/twitter/model/core/TwitterUser;

    const-class v1, Lcom/twitter/model/json/core/JsonTwitterUser;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 872
    const-class v0, Lcom/twitter/model/core/TwitterUser$a;

    const-class v1, Lcom/twitter/model/json/core/JsonTwitterUser;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 873
    const-class v0, Lcom/twitter/model/livepipeline/g;

    const-class v1, Lcom/twitter/model/json/livepipeline/JsonTypingIndicatorEventBuilder;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 874
    const-class v0, Lcom/twitter/model/livepipeline/g$a;

    const-class v1, Lcom/twitter/model/json/livepipeline/JsonTypingIndicatorEventBuilder;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 875
    const-class v0, Lcom/twitter/model/timeline/ar;

    const-class v1, Lcom/twitter/model/json/timeline/urt/JsonTimelineRequestCursor;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 876
    const-class v0, Lcom/twitter/model/url/UnwrappedTcoLinkResponse;

    const-class v1, Lcom/twitter/model/json/url/JsonUnwrappedTcoLink;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 877
    const-class v0, Lcfm;

    const-class v1, Lcom/twitter/model/json/moments/maker/JsonUpdateMomentResponse;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 878
    const-class v0, Lcom/twitter/model/client/UrlConfiguration;

    const-class v1, Lcom/twitter/model/json/client/JsonUrlConfiguration;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 879
    const-class v0, Lcom/twitter/model/core/ad;

    const-class v1, Lcom/twitter/model/json/core/JsonUrlEntity;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 880
    const-class v0, Lcom/twitter/model/core/ad$c;

    const-class v1, Lcom/twitter/model/json/core/JsonUrlEntity;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 881
    const-class v0, Lcgq;

    const-class v1, Lcom/twitter/model/json/search/urt/JsonUrtHitHighlights;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 882
    const-class v0, Lcgc;

    const-class v1, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 883
    const-class v0, Lcgc$a;

    const-class v1, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 884
    const-class v0, Lcgd;

    const-class v1, Lcom/twitter/model/json/notifications/JsonNotificationSettingsApiResult;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 885
    const-class v0, Lcgd$a;

    const-class v1, Lcom/twitter/model/json/notifications/JsonNotificationSettingsApiResult;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 886
    const-class v0, Lcom/twitter/model/people/l;

    const-class v1, Lcom/twitter/model/json/people/JsonUserRecommendation;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 887
    const-class v0, Lcom/twitter/model/people/l$a;

    const-class v1, Lcom/twitter/model/json/people/JsonUserRecommendation;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 888
    const-class v0, Lcom/twitter/model/account/UserSettings;

    const-class v1, Lcom/twitter/model/json/account/JsonUserSettings;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 889
    const-class v0, Lcay;

    const-class v1, Lcom/twitter/model/json/card/JsonUserValue;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 890
    const-class v0, Lcom/twitter/model/geo/VendorInfo;

    const-class v1, Lcom/twitter/model/json/geo/JsonVendorInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 891
    const-class v0, Lcom/twitter/model/av/VideoCta;

    const-class v1, Lcom/twitter/model/json/av/JsonCallToAction;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 892
    const-class v0, Lcom/twitter/model/profile/a;

    const-class v1, Lcom/twitter/model/json/profiles/JsonVineProfile;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 893
    const-class v0, Lcom/twitter/model/profile/a$a;

    const-class v1, Lcom/twitter/model/json/profiles/JsonVineProfile;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->b(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 894
    const-class v0, Lcom/twitter/model/av/i;

    const-class v1, Lcom/twitter/model/json/watchmode/JsonWatchModeInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 895
    const-class v0, Lcom/twitter/model/av/j;

    const-class v1, Lcom/twitter/model/json/watchmode/JsonWatchModeSectionStatusMetadata;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 896
    const-class v0, Lcom/twitter/model/geo/VendorInfo$YelpInfo;

    const-class v1, Lcom/twitter/model/json/geo/JsonVendorInfo$JsonYelpInfo;

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 897
    const-class v0, Lcom/twitter/model/dms/b;

    new-instance v1, Lcom/twitter/model/json/dms/b;

    invoke-direct {v1}, Lcom/twitter/model/json/dms/b;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 898
    const-class v0, Lcom/twitter/model/revenue/AdvertiserAccountServiceLevel;

    new-instance v1, Lcom/twitter/model/json/revenue/a;

    invoke-direct {v1}, Lcom/twitter/model/json/revenue/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 899
    const-class v0, Lcom/twitter/model/ads/AdvertiserType;

    new-instance v1, Lcom/twitter/model/json/ads/a;

    invoke-direct {v1}, Lcom/twitter/model/json/ads/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 900
    const-class v0, Lcom/twitter/model/timeline/AlertType;

    new-instance v1, Lcom/twitter/model/json/timeline/a;

    invoke-direct {v1}, Lcom/twitter/model/json/timeline/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 901
    const-class v0, Lcom/twitter/model/analytics/AnalyticsType;

    new-instance v1, Lcdl;

    invoke-direct {v1}, Lcdl;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 902
    const-class v0, Lcom/twitter/model/dms/d;

    new-instance v1, Lcom/twitter/model/json/dms/e;

    invoke-direct {v1}, Lcom/twitter/model/json/dms/e;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 903
    const-class v0, Lcbx;

    new-instance v1, Lcom/twitter/model/json/dms/c;

    invoke-direct {v1}, Lcom/twitter/model/json/dms/c;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 904
    const-class v0, Lcom/twitter/model/businessprofiles/BusinessProfileState;

    new-instance v1, Lcom/twitter/model/json/businessprofiles/a;

    invoke-direct {v1}, Lcom/twitter/model/json/businessprofiles/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 905
    const-class v0, Lcba;

    new-instance v1, Lcom/twitter/model/json/revenue/c;

    invoke-direct {v1}, Lcom/twitter/model/json/revenue/c;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 906
    const-class v0, Lcom/twitter/model/dms/k;

    new-instance v1, Lcom/twitter/model/json/dms/d;

    invoke-direct {v1}, Lcom/twitter/model/json/dms/d;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 907
    const-class v0, Lcbz;

    new-instance v1, Lcom/twitter/model/json/dms/h;

    invoke-direct {v1}, Lcom/twitter/model/json/dms/h;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 908
    const-class v0, Lcom/twitter/model/dms/s;

    new-instance v1, Lcom/twitter/model/json/dms/m;

    invoke-direct {v1}, Lcom/twitter/model/json/dms/m;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 909
    const-class v0, Lcom/twitter/model/dms/u;

    new-instance v1, Lcom/twitter/model/json/dms/p;

    invoke-direct {v1}, Lcom/twitter/model/json/dms/p;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 910
    const-class v0, Lcom/twitter/model/moments/DisplayStyle;

    new-instance v1, Lcom/twitter/model/json/moments/b;

    invoke-direct {v1}, Lcom/twitter/model/json/moments/b;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 911
    const-class v0, Lcom/twitter/model/login/OneFactorEligibleFactor$FactorType;

    new-instance v1, Lcom/twitter/model/json/login/a;

    invoke-direct {v1}, Lcom/twitter/model/json/login/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 912
    const-class v0, Lccw;

    new-instance v1, Lcom/twitter/model/json/featureswitch/b;

    invoke-direct {v1}, Lcom/twitter/model/json/featureswitch/b;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 913
    const-class v0, Lcde;

    new-instance v1, Lcom/twitter/model/json/featureswitch/c;

    invoke-direct {v1}, Lcom/twitter/model/json/featureswitch/c;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 914
    const-class v0, Lcom/twitter/model/people/ModuleTitle$Icon;

    new-instance v1, Lcom/twitter/model/json/people/a;

    invoke-direct {v1}, Lcom/twitter/model/json/people/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 915
    const-class v0, Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    new-instance v1, Lcom/twitter/model/json/onboarding/a;

    invoke-direct {v1}, Lcom/twitter/model/json/onboarding/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 916
    const-class v0, Lcdm;

    new-instance v1, Lcdn;

    invoke-direct {v1}, Lcdn;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 917
    const-class v0, Lcom/twitter/model/json/core/b;

    new-instance v1, Lcom/twitter/model/json/core/c;

    invoke-direct {v1}, Lcom/twitter/model/json/core/c;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 918
    const-class v0, Lcom/twitter/model/json/core/d;

    new-instance v1, Lcom/twitter/model/json/core/e;

    invoke-direct {v1}, Lcom/twitter/model/json/core/e;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 919
    const-class v0, Lcom/twitter/model/businessprofiles/KeyEngagementType;

    new-instance v1, Lcom/twitter/model/json/businessprofiles/b;

    invoke-direct {v1}, Lcom/twitter/model/json/businessprofiles/b;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 920
    const-class v0, Lcom/twitter/model/stratostore/a;

    new-instance v1, Lcom/twitter/model/json/stratostore/a;

    invoke-direct {v1}, Lcom/twitter/model/json/stratostore/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 921
    const-class v0, Lcom/twitter/model/moments/MomentGuideSectionType;

    new-instance v1, Lcom/twitter/model/json/moments/c;

    invoke-direct {v1}, Lcom/twitter/model/json/moments/c;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 922
    const-class v0, Lcom/twitter/model/moments/MomentPageType;

    new-instance v1, Lcom/twitter/model/json/moments/d;

    invoke-direct {v1}, Lcom/twitter/model/json/moments/d;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 923
    const-class v0, Lcom/twitter/model/moments/MomentVisibilityMode;

    new-instance v1, Lcom/twitter/model/json/moments/a;

    invoke-direct {v1}, Lcom/twitter/model/json/moments/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 924
    const-class v0, Lcom/twitter/model/dms/ad;

    new-instance v1, Lcom/twitter/model/json/dms/n;

    invoke-direct {v1}, Lcom/twitter/model/json/dms/n;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 925
    const-class v0, Lcom/twitter/model/dms/ah;

    new-instance v1, Lcom/twitter/model/json/dms/j;

    invoke-direct {v1}, Lcom/twitter/model/json/dms/j;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 926
    const-class v0, Lcom/twitter/model/livepipeline/d;

    new-instance v1, Lcom/twitter/model/json/livepipeline/a;

    invoke-direct {v1}, Lcom/twitter/model/json/livepipeline/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 927
    const-class v0, Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    new-instance v1, Lcom/twitter/model/json/geo/a;

    invoke-direct {v1}, Lcom/twitter/model/json/geo/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 928
    const-class v0, Lcom/twitter/model/businessprofiles/ResponsivenessLevel;

    new-instance v1, Lcom/twitter/model/json/businessprofiles/c;

    invoke-direct {v1}, Lcom/twitter/model/json/businessprofiles/c;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 929
    const-class v0, Lcom/twitter/model/stratostore/SourceLocation;

    new-instance v1, Lcom/twitter/model/json/stratostore/b;

    invoke-direct {v1}, Lcom/twitter/model/json/stratostore/b;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 930
    const-class v0, Lcom/twitter/model/stratostore/f;

    new-instance v1, Lcom/twitter/model/json/stratostore/d;

    invoke-direct {v1}, Lcom/twitter/model/json/stratostore/d;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 931
    const-class v0, Lcom/twitter/model/onboarding/permission/SystemPermissionState;

    new-instance v1, Lcom/twitter/model/json/onboarding/c;

    invoke-direct {v1}, Lcom/twitter/model/json/onboarding/c;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 932
    const-class v0, Lcom/twitter/model/profile/TranslatorType;

    new-instance v1, Lcom/twitter/model/json/profiles/b;

    invoke-direct {v1}, Lcom/twitter/model/json/profiles/b;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 933
    const-class v0, Lcom/twitter/model/topic/trends/TrendBadge;

    new-instance v1, Lcom/twitter/model/json/moments/e;

    invoke-direct {v1}, Lcom/twitter/model/json/moments/e;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 934
    const-class v0, Lcbc;

    new-instance v1, Lcom/twitter/model/json/revenue/b;

    invoke-direct {v1}, Lcom/twitter/model/json/revenue/b;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 935
    const-class v0, Lcom/twitter/model/core/MediaEntity$Type;

    new-instance v1, Lcom/twitter/model/json/core/a;

    invoke-direct {v1}, Lcom/twitter/model/json/core/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 936
    const-class v0, Lcom/twitter/model/profile/ExtendedProfile$Visibility;

    new-instance v1, Lcom/twitter/model/json/profiles/a;

    invoke-direct {v1}, Lcom/twitter/model/json/profiles/a;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 937
    const-class v0, Lcom/twitter/model/dms/am;

    new-instance v1, Lcom/twitter/model/json/dms/q;

    invoke-direct {v1}, Lcom/twitter/model/json/dms/q;-><init>()V

    invoke-interface {p1, v0, v1}, Lcom/twitter/model/json/common/JsonModelRegistry$b;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 938
    return-void
.end method
