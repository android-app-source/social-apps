.class public Lcom/twitter/model/json/livepipeline/JsonConfigEventBuilder;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/livepipeline/a;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/Long;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/Long;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/livepipeline/a$a;
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/model/livepipeline/a$a;

    invoke-direct {v0}, Lcom/twitter/model/livepipeline/a$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/livepipeline/JsonConfigEventBuilder;->a:Ljava/lang/String;

    .line 22
    invoke-virtual {v0, v1}, Lcom/twitter/model/livepipeline/a$a;->a(Ljava/lang/String;)Lcom/twitter/model/livepipeline/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/livepipeline/JsonConfigEventBuilder;->b:Ljava/lang/Long;

    .line 23
    invoke-virtual {v0, v1}, Lcom/twitter/model/livepipeline/a$a;->a(Ljava/lang/Long;)Lcom/twitter/model/livepipeline/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/livepipeline/JsonConfigEventBuilder;->c:Ljava/lang/Long;

    .line 24
    invoke-virtual {v0, v1}, Lcom/twitter/model/livepipeline/a$a;->b(Ljava/lang/Long;)Lcom/twitter/model/livepipeline/a$a;

    move-result-object v0

    .line 21
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/livepipeline/JsonConfigEventBuilder;->a()Lcom/twitter/model/livepipeline/a$a;

    move-result-object v0

    return-object v0
.end method
