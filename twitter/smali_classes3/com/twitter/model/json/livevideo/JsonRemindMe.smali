.class public Lcom/twitter/model/json/livevideo/JsonRemindMe;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/livevideo/c;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "toggle_visible"
        }
    .end annotation
.end field

.field public b:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "notification_id"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/livevideo/c;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/twitter/model/livevideo/c$a;

    invoke-direct {v0}, Lcom/twitter/model/livevideo/c$a;-><init>()V

    iget-boolean v1, p0, Lcom/twitter/model/json/livevideo/JsonRemindMe;->a:Z

    .line 28
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/c$a;->a(Z)Lcom/twitter/model/livevideo/c$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/livevideo/JsonRemindMe;->b:Z

    .line 29
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/c$a;->b(Z)Lcom/twitter/model/livevideo/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/livevideo/JsonRemindMe;->c:Ljava/lang/String;

    .line 30
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/c$a;->a(Ljava/lang/String;)Lcom/twitter/model/livevideo/c$a;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/twitter/model/livevideo/c$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/c;

    .line 27
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/twitter/model/json/livevideo/JsonRemindMe;->a()Lcom/twitter/model/livevideo/c;

    move-result-object v0

    return-object v0
.end method
