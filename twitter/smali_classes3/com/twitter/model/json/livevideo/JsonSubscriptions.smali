.class public Lcom/twitter/model/json/livevideo/JsonSubscriptions;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/livevideo/d;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/model/livevideo/c;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "remind_me"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/livevideo/d;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/twitter/model/livevideo/d$a;

    invoke-direct {v0}, Lcom/twitter/model/livevideo/d$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/livevideo/JsonSubscriptions;->a:Lcom/twitter/model/livevideo/c;

    .line 24
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/d$a;->a(Lcom/twitter/model/livevideo/c;)Lcom/twitter/model/livevideo/d$a;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/twitter/model/livevideo/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/d;

    .line 23
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/model/json/livevideo/JsonSubscriptions;->a()Lcom/twitter/model/livevideo/d;

    move-result-object v0

    return-object v0
.end method
