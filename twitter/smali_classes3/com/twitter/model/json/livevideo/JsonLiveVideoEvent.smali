.class public Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/livevideo/b;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "owner_user_id"
        }
    .end annotation
.end field

.field public c:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "start_time"
        }
    .end annotation
.end field

.field public d:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "end_time"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "host_name"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public h:Lcom/twitter/model/livevideo/a;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "compose_semantic_core_id"
        }
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "placeholder_variants"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/json/card/JsonImageSpec;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/json/livevideo/JsonTimelineInfo;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/twitter/model/livevideo/d;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public m:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "docking_enabled"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/livevideo/b$a;
    .locals 6

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->j:Ljava/util/List;

    .line 66
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent$1;

    invoke-direct {v1, p0}, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent$1;-><init>(Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;)V

    .line 65
    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->k:Ljava/util/List;

    .line 74
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent$2;

    invoke-direct {v2, p0}, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent$2;-><init>(Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;)V

    .line 73
    invoke-static {v1, v2}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v1

    .line 81
    new-instance v2, Lcom/twitter/model/livevideo/b$a;

    invoke-direct {v2}, Lcom/twitter/model/livevideo/b$a;-><init>()V

    iget-wide v4, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->a:J

    .line 82
    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/livevideo/b$a;->a(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    iget-wide v4, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->b:J

    .line 83
    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/livevideo/b$a;->b(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    iget-wide v4, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->c:J

    .line 84
    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/livevideo/b$a;->c(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    iget-wide v4, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->d:J

    .line 85
    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/livevideo/b$a;->d(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->e:Ljava/lang/String;

    .line 86
    invoke-virtual {v2, v3}, Lcom/twitter/model/livevideo/b$a;->b(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->f:Ljava/lang/String;

    .line 87
    invoke-virtual {v2, v3}, Lcom/twitter/model/livevideo/b$a;->c(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->g:Ljava/lang/String;

    .line 88
    invoke-virtual {v2, v3}, Lcom/twitter/model/livevideo/b$a;->a(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->h:Lcom/twitter/model/livevideo/a;

    .line 89
    invoke-virtual {v2, v3}, Lcom/twitter/model/livevideo/b$a;->a(Lcom/twitter/model/livevideo/a;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->i:Ljava/lang/String;

    .line 90
    invoke-virtual {v2, v3}, Lcom/twitter/model/livevideo/b$a;->d(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    .line 91
    invoke-virtual {v2, v0}, Lcom/twitter/model/livevideo/b$a;->a(Ljava/util/List;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 92
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->b(Ljava/util/List;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->l:Lcom/twitter/model/livevideo/d;

    .line 93
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->a(Lcom/twitter/model/livevideo/d;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->m:Z

    .line 94
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->a(Z)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 81
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->a()Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    return-object v0
.end method
