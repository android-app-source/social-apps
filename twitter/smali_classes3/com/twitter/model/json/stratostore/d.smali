.class public Lcom/twitter/model/json/stratostore/d;
.super Lcom/twitter/model/json/common/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/h",
        "<",
        "Lcom/twitter/model/stratostore/f;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/model/stratostore/e$b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "tweet_views"

    const-class v2, Lcom/twitter/model/stratostore/g;

    .line 29
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "stickerInfo"

    const-class v2, Lcom/twitter/model/stratostore/d;

    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "altText"

    const-class v2, Lcom/twitter/model/stratostore/a;

    .line 31
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "mediaRestrictions"

    const-class v2, Lcom/twitter/model/stratostore/b;

    .line 32
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "mediaStats"

    const-class v2, Lcom/twitter/model/stratostore/c;

    .line 33
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "mediaColor"

    const-class v2, Lcom/twitter/model/stratostore/MediaColorData;

    .line 34
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lcom/twitter/model/json/stratostore/d;->a:Ljava/util/Map;

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/model/json/common/h;-><init>()V

    return-void
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/String;Ljava/lang/Class;)Lcom/twitter/model/stratostore/e;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/model/stratostore/e$b;",
            ">(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/twitter/model/stratostore/e;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    new-instance v0, Lcom/twitter/model/stratostore/e$a;

    invoke-direct {v0, p1}, Lcom/twitter/model/stratostore/e$a;-><init>(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    if-nez v1, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 70
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 71
    const/4 v0, 0x0

    .line 83
    :goto_0
    return-object v0

    .line 73
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_4

    .line 74
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    .line 75
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 76
    const-string/jumbo v2, "r"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 77
    invoke-static {p0, v0, p2}, Lcom/twitter/model/json/stratostore/d;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/stratostore/e$a;Ljava/lang/Class;)V

    .line 81
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 78
    :cond_3
    const-string/jumbo v2, "ttl"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/stratostore/e$a;->a(I)Lcom/twitter/model/stratostore/e$a;

    goto :goto_2

    .line 83
    :cond_4
    invoke-virtual {v0}, Lcom/twitter/model/stratostore/e$a;->a()Lcom/twitter/model/stratostore/e;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/stratostore/e$a;Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Lcom/twitter/model/stratostore/e$a;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/model/stratostore/e$b;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    if-nez v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_2

    .line 92
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 109
    :cond_1
    return-void

    .line 95
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    .line 96
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 97
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 98
    const-string/jumbo v1, "err"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 99
    const-class v0, Lcom/twitter/model/stratostore/e$c;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/stratostore/e$c;

    invoke-virtual {p1, v0}, Lcom/twitter/model/stratostore/e$a;->a(Lcom/twitter/model/stratostore/e$c;)Lcom/twitter/model/stratostore/e$a;

    .line 100
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/twitter/model/stratostore/e$a;->b(I)Lcom/twitter/model/stratostore/e$a;

    .line 107
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_0

    .line 101
    :cond_4
    const-string/jumbo v1, "missing"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 102
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/twitter/model/stratostore/e$a;->b(I)Lcom/twitter/model/stratostore/e$a;

    goto :goto_1

    .line 103
    :cond_5
    const-string/jumbo v1, "ok"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 104
    invoke-static {p0, p2}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/stratostore/e$b;

    invoke-virtual {p1, v0}, Lcom/twitter/model/stratostore/e$a;->a(Lcom/twitter/model/stratostore/e$b;)Lcom/twitter/model/stratostore/e$a;

    .line 105
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/twitter/model/stratostore/e$a;->b(I)Lcom/twitter/model/stratostore/e$a;

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/stratostore/f;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    new-instance v2, Lcom/twitter/model/stratostore/f$a;

    invoke-direct {v2}, Lcom/twitter/model/stratostore/f$a;-><init>()V

    .line 41
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    if-nez v0, :cond_0

    .line 42
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 44
    :cond_0
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    .line 45
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 46
    const/4 v0, 0x0

    .line 58
    :goto_0
    return-object v0

    .line 48
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_3

    .line 49
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    .line 50
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 51
    sget-object v0, Lcom/twitter/model/json/stratostore/d;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    sget-object v0, Lcom/twitter/model/json/stratostore/d;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    sget-object v1, Lcom/twitter/model/json/stratostore/d;->a:Ljava/util/Map;

    .line 53
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    invoke-static {p1, v3, v1}, Lcom/twitter/model/json/stratostore/d;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/String;Ljava/lang/Class;)Lcom/twitter/model/stratostore/e;

    move-result-object v1

    .line 52
    invoke-virtual {v2, v0, v1}, Lcom/twitter/model/stratostore/f$a;->a(Ljava/lang/Class;Lcom/twitter/model/stratostore/e;)Lcom/twitter/model/stratostore/f$a;

    .line 56
    :cond_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 58
    :cond_3
    invoke-virtual {v2}, Lcom/twitter/model/stratostore/f$a;->a()Lcom/twitter/model/stratostore/f;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/stratostore/d;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/stratostore/f;

    move-result-object v0

    return-object v0
.end method
