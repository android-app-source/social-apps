.class public Lcom/twitter/model/json/pc/JsonPromotedContent;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/pc/JsonPromotedContent$PromotedContext;,
        Lcom/twitter/model/json/pc/JsonPromotedContent$PromotedContentTrend;,
        Lcom/twitter/model/json/pc/JsonPromotedContent$PromotedContentAdvertiser;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcgi;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/util/Map;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/twitter/model/json/pc/JsonPromotedContent$PromotedContentAdvertiser;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Lcom/twitter/model/json/pc/JsonPromotedContent$PromotedContentTrend;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public h:Lcom/twitter/model/json/pc/JsonPromotedContent$PromotedContext;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public i:[Lcom/twitter/model/json/core/JsonUserName;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public j:Ljava/util/Set;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgi$a;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 63
    new-instance v0, Lcgi$a;

    invoke-direct {v0}, Lcgi$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/pc/JsonPromotedContent;->a:Ljava/lang/String;

    .line 64
    invoke-virtual {v0, v1}, Lcgi$a;->a(Ljava/lang/String;)Lcgi$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/pc/JsonPromotedContent;->b:Ljava/lang/String;

    .line 65
    invoke-virtual {v0, v1}, Lcgi$a;->b(Ljava/lang/String;)Lcgi$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/pc/JsonPromotedContent;->c:Ljava/lang/String;

    .line 66
    invoke-virtual {v0, v1}, Lcgi$a;->d(Ljava/lang/String;)Lcgi$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/pc/JsonPromotedContent;->j:Ljava/util/Set;

    .line 67
    invoke-virtual {v0, v1}, Lcgi$a;->a(Ljava/util/Set;)Lcgi$a;

    move-result-object v5

    .line 69
    iget-object v0, p0, Lcom/twitter/model/json/pc/JsonPromotedContent;->e:Ljava/util/Map;

    .line 70
    if-eqz v0, :cond_1

    .line 71
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 72
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 73
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 74
    const/4 v3, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_0
    move v1, v3

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 76
    :pswitch_0
    const-string/jumbo v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v5, v0}, Lcgi$a;->a(Z)Lcgi$a;

    goto :goto_0

    .line 74
    :sswitch_0
    const-string/jumbo v7, "pac_in_timeline"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_1

    :sswitch_1
    const-string/jumbo v7, "suppress_media_forward"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v4

    goto :goto_1

    :sswitch_2
    const-string/jumbo v7, "slot_type"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    .line 80
    :pswitch_1
    const-string/jumbo v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v5, v0}, Lcgi$a;->b(Z)Lcgi$a;

    goto :goto_0

    .line 84
    :pswitch_2
    invoke-virtual {v5, v4}, Lcgi$a;->c(Z)Lcgi$a;

    goto :goto_0

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/json/pc/JsonPromotedContent;->f:Lcom/twitter/model/json/pc/JsonPromotedContent$PromotedContentAdvertiser;

    if-eqz v0, :cond_2

    .line 94
    iget-object v0, p0, Lcom/twitter/model/json/pc/JsonPromotedContent;->f:Lcom/twitter/model/json/pc/JsonPromotedContent$PromotedContentAdvertiser;

    iget-wide v0, v0, Lcom/twitter/model/json/pc/JsonPromotedContent$PromotedContentAdvertiser;->a:J

    invoke-virtual {v5, v0, v1}, Lcgi$a;->b(J)Lcgi$a;

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/twitter/model/json/pc/JsonPromotedContent;->g:Lcom/twitter/model/json/pc/JsonPromotedContent$PromotedContentTrend;

    .line 98
    if-eqz v0, :cond_3

    .line 99
    iget-wide v6, v0, Lcom/twitter/model/json/pc/JsonPromotedContent$PromotedContentTrend;->a:J

    invoke-virtual {v5, v6, v7}, Lcgi$a;->a(J)Lcgi$a;

    .line 100
    iget-object v0, v0, Lcom/twitter/model/json/pc/JsonPromotedContent$PromotedContentTrend;->b:Ljava/lang/String;

    invoke-virtual {v5, v0}, Lcgi$a;->c(Ljava/lang/String;)Lcgi$a;

    .line 103
    :cond_3
    iget-object v1, p0, Lcom/twitter/model/json/pc/JsonPromotedContent;->i:[Lcom/twitter/model/json/core/JsonUserName;

    .line 104
    if-eqz v1, :cond_5

    .line 105
    array-length v3, v1

    move v0, v2

    :goto_2
    if-ge v0, v3, :cond_5

    aget-object v2, v1, v0

    .line 106
    invoke-virtual {v5}, Lcgi$a;->e()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    .line 107
    invoke-virtual {v2}, Lcom/twitter/model/json/core/JsonUserName;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcgi$a;->c(Ljava/lang/String;)Lcgi$a;

    .line 105
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 112
    :cond_5
    return-object v5

    .line 74
    nop

    :sswitch_data_0
    .sparse-switch
        -0x70664a12 -> :sswitch_0
        -0x1ad562a0 -> :sswitch_1
        0x28f3ecdb -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/model/json/pc/JsonPromotedContent;->a()Lcgi$a;

    move-result-object v0

    return-object v0
.end method
