.class public Lcom/twitter/model/json/moments/JsonGuideTrendSection;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/moments/JsonGuideTrendSection$JsonGuideTrendModule;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/moments/j;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Lcom/twitter/model/moments/k;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/json/moments/JsonGuideTrendSection$JsonGuideTrendModule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/j;
    .locals 6

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrendSection;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 32
    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrendSection;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/moments/JsonGuideTrendSection$JsonGuideTrendModule;

    .line 33
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    iget-object v3, v0, Lcom/twitter/model/json/moments/JsonGuideTrendSection$JsonGuideTrendModule;->b:Lcom/twitter/model/json/moments/JsonGuideTrend;

    if-eqz v3, :cond_0

    .line 35
    iget-object v3, v0, Lcom/twitter/model/json/moments/JsonGuideTrendSection$JsonGuideTrendModule;->b:Lcom/twitter/model/json/moments/JsonGuideTrend;

    iget-wide v4, p0, Lcom/twitter/model/json/moments/JsonGuideTrendSection;->a:J

    iput-wide v4, v3, Lcom/twitter/model/json/moments/JsonGuideTrend;->k:J

    .line 36
    iget-object v0, v0, Lcom/twitter/model/json/moments/JsonGuideTrendSection$JsonGuideTrendModule;->b:Lcom/twitter/model/json/moments/JsonGuideTrend;

    invoke-virtual {v0}, Lcom/twitter/model/json/moments/JsonGuideTrend;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 38
    :cond_0
    iget-object v3, v0, Lcom/twitter/model/json/moments/JsonGuideTrendSection$JsonGuideTrendModule;->a:Lcom/twitter/model/json/moments/JsonGuideTrend;

    iget-wide v4, p0, Lcom/twitter/model/json/moments/JsonGuideTrendSection;->a:J

    iput-wide v4, v3, Lcom/twitter/model/json/moments/JsonGuideTrend;->k:J

    .line 39
    iget-object v0, v0, Lcom/twitter/model/json/moments/JsonGuideTrendSection$JsonGuideTrendModule;->a:Lcom/twitter/model/json/moments/JsonGuideTrend;

    invoke-virtual {v0}, Lcom/twitter/model/json/moments/JsonGuideTrend;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 42
    :cond_1
    new-instance v2, Lcom/twitter/model/moments/j;

    iget-wide v4, p0, Lcom/twitter/model/json/moments/JsonGuideTrendSection;->a:J

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrendSection;->b:Lcom/twitter/model/moments/k;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/k;

    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {v2, v4, v5, v0, v1}, Lcom/twitter/model/moments/j;-><init>(JLcom/twitter/model/moments/k;Ljava/util/List;)V

    return-object v2
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/twitter/model/json/moments/JsonGuideTrendSection;->a()Lcom/twitter/model/moments/j;

    move-result-object v0

    return-object v0
.end method
