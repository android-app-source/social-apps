.class public Lcom/twitter/model/json/moments/JsonSocialProof;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/moments/JsonSocialProof$JsonFirstOrderSocialProof;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/moments/s;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/model/json/moments/JsonSocialProof$JsonFirstOrderSocialProof;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/s;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/model/moments/s;

    new-instance v1, Lcom/twitter/model/moments/s$a;

    invoke-direct {v1}, Lcom/twitter/model/moments/s$a;-><init>()V

    iget-object v2, p0, Lcom/twitter/model/json/moments/JsonSocialProof;->a:Lcom/twitter/model/json/moments/JsonSocialProof$JsonFirstOrderSocialProof;

    iget-object v2, v2, Lcom/twitter/model/json/moments/JsonSocialProof$JsonFirstOrderSocialProof;->a:Ljava/lang/String;

    .line 22
    invoke-virtual {v1, v2}, Lcom/twitter/model/moments/s$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/s$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/moments/MomentSocialProofType;->a:Lcom/twitter/model/moments/MomentSocialProofType;

    invoke-virtual {v1, v2}, Lcom/twitter/model/moments/s$a;->a(Lcom/twitter/model/moments/MomentSocialProofType;)Lcom/twitter/model/moments/s$a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/model/moments/s;-><init>(Lcom/twitter/model/moments/s$a;)V

    .line 21
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/model/json/moments/JsonSocialProof;->a()Lcom/twitter/model/moments/s;

    move-result-object v0

    return-object v0
.end method
