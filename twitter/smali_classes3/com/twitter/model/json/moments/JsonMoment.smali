.class public Lcom/twitter/model/json/moments/JsonMoment;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/moments/Moment;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "sensitive"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public j:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public k:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public l:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public n:Lcom/twitter/model/moments/a;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public o:Lcgi;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public p:Lcom/twitter/model/moments/g;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public q:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public r:Ljava/util/Map;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lcom/twitter/model/moments/f;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public t:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public u:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/Moment$a;
    .locals 4

    .prologue
    .line 67
    new-instance v0, Lcom/twitter/model/moments/Moment$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/Moment$a;-><init>()V

    iget-wide v2, p0, Lcom/twitter/model/json/moments/JsonMoment;->a:J

    .line 68
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/moments/Moment$a;->a(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->b:Ljava/lang/String;

    .line 69
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->c:Ljava/lang/String;

    .line 70
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->f(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->d:Z

    .line 71
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->b(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->e:Z

    .line 72
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->c(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->f:Ljava/lang/String;

    .line 73
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->b(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->g:Ljava/lang/String;

    .line 74
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->c(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->h:Ljava/lang/String;

    .line 75
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->d(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->i:Ljava/lang/String;

    .line 76
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->e(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->j:Z

    .line 77
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->k:Z

    .line 78
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->d(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->l:I

    .line 79
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(I)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->m:Ljava/lang/String;

    .line 80
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->g(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->n:Lcom/twitter/model/moments/a;

    .line 81
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/a;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->o:Lcgi;

    .line 82
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Lcgi;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->p:Lcom/twitter/model/moments/g;

    .line 83
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/g;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/moments/JsonMoment;->q:J

    .line 84
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/moments/Moment$a;->b(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->s:Lcom/twitter/model/moments/f;

    .line 85
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/f;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/moments/JsonMoment;->t:Z

    .line 86
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/Moment$a;->e(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/moments/JsonMoment;->u:J

    .line 87
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/moments/Moment$a;->c(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 67
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/twitter/model/json/moments/JsonMoment;->a()Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    return-object v0
.end method
