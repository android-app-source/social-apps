.class public Lcom/twitter/model/json/moments/JsonCropData;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/moments/d;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/d$a;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/model/moments/d$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/d$a;-><init>()V

    iget v1, p0, Lcom/twitter/model/json/moments/JsonCropData;->a:I

    .line 27
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/d$a;->a(I)Lcom/twitter/model/moments/d$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/moments/JsonCropData;->b:I

    .line 28
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/d$a;->b(I)Lcom/twitter/model/moments/d$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/moments/JsonCropData;->c:I

    .line 29
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/d$a;->c(I)Lcom/twitter/model/moments/d$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/moments/JsonCropData;->d:I

    .line 30
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/d$a;->d(I)Lcom/twitter/model/moments/d$a;

    move-result-object v0

    .line 26
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/model/json/moments/JsonCropData;->a()Lcom/twitter/model/moments/d$a;

    move-result-object v0

    return-object v0
.end method
