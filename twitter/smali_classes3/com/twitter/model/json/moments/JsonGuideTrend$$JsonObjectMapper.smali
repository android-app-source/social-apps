.class public final Lcom/twitter/model/json/moments/JsonGuideTrend$$JsonObjectMapper;
.super Lcom/bluelinelabs/logansquare/JsonMapper;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bluelinelabs/logansquare/JsonMapper",
        "<",
        "Lcom/twitter/model/json/moments/JsonGuideTrend;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/bluelinelabs/logansquare/JsonMapper;-><init>()V

    return-void
.end method

.method public static _parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/moments/JsonGuideTrend;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/model/json/moments/JsonGuideTrend;

    invoke-direct {v0}, Lcom/twitter/model/json/moments/JsonGuideTrend;-><init>()V

    .line 23
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    if-nez v1, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 26
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_2

    .line 27
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 28
    const/4 v0, 0x0

    .line 36
    :cond_1
    return-object v0

    .line 30
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 31
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    .line 32
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 33
    invoke-static {v0, v1, p0}, Lcom/twitter/model/json/moments/JsonGuideTrend$$JsonObjectMapper;->parseField(Lcom/twitter/model/json/moments/JsonGuideTrend;Ljava/lang/String;Lcom/fasterxml/jackson/core/JsonParser;)V

    .line 34
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_0
.end method

.method public static _serialize(Lcom/twitter/model/json/moments/JsonGuideTrend;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 81
    if-eqz p2, :cond_0

    .line 82
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 84
    :cond_0
    const-string/jumbo v0, "advertiser_name"

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->i:Ljava/util/List;

    .line 86
    if-eqz v0, :cond_3

    .line 87
    const-string/jumbo v1, "badges"

    invoke-virtual {p1, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a()V

    .line 89
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/trends/TrendBadge;

    .line 90
    if-eqz v0, :cond_1

    .line 91
    const-class v2, Lcom/twitter/model/topic/trends/TrendBadge;

    invoke-static {v2}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v2

    const-string/jumbo v3, "lslocalbadgesElement"

    const/4 v4, 0x0

    invoke-interface {v2, v0, v3, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    goto :goto_0

    .line 94
    :cond_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    .line 96
    :cond_3
    const-string/jumbo v0, "description"

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->g:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonDisclosure;

    if-eqz v0, :cond_4

    .line 98
    const-string/jumbo v0, "disclosure"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->g:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonDisclosure;

    invoke-static {v0, p1, v5}, Lcom/twitter/model/json/moments/JsonGuideTrend$JsonDisclosure$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/moments/JsonGuideTrend$JsonDisclosure;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 101
    :cond_4
    const-string/jumbo v0, "meta_description"

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string/jumbo v0, "name"

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string/jumbo v0, "id"

    iget-wide v2, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->h:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 104
    const-string/jumbo v0, "rank"

    iget v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 105
    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->b:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget;

    if-eqz v0, :cond_5

    .line 106
    const-string/jumbo v0, "target"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->b:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget;

    invoke-static {v0, p1, v5}, Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 109
    :cond_5
    const-string/jumbo v0, "token"

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    if-eqz p2, :cond_6

    .line 111
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 113
    :cond_6
    return-void
.end method

.method public static parseField(Lcom/twitter/model/json/moments/JsonGuideTrend;Ljava/lang/String;Lcom/fasterxml/jackson/core/JsonParser;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 40
    const-string/jumbo v0, "advertiser_name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->f:Ljava/lang/String;

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    const-string/jumbo v0, "badges"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 43
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_4

    .line 44
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 45
    :cond_2
    :goto_1
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_3

    .line 47
    const-class v0, Lcom/twitter/model/topic/trends/TrendBadge;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/trends/TrendBadge;

    .line 48
    if-eqz v0, :cond_2

    .line 49
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 52
    :cond_3
    iput-object v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->i:Ljava/util/List;

    goto :goto_0

    .line 54
    :cond_4
    iput-object v2, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->i:Ljava/util/List;

    goto :goto_0

    .line 56
    :cond_5
    const-string/jumbo v0, "description"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 57
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->e:Ljava/lang/String;

    goto :goto_0

    .line 58
    :cond_6
    const-string/jumbo v0, "disclosure"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 59
    invoke-static {p2}, Lcom/twitter/model/json/moments/JsonGuideTrend$JsonDisclosure$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/moments/JsonGuideTrend$JsonDisclosure;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->g:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonDisclosure;

    goto :goto_0

    .line 60
    :cond_7
    const-string/jumbo v0, "meta_description"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 61
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->d:Ljava/lang/String;

    goto :goto_0

    .line 62
    :cond_8
    const-string/jumbo v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 63
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->a:Ljava/lang/String;

    goto :goto_0

    .line 64
    :cond_9
    const-string/jumbo v0, "id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 65
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->h:J

    goto/16 :goto_0

    .line 66
    :cond_a
    const-string/jumbo v0, "rank"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 67
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->c:I

    goto/16 :goto_0

    .line 68
    :cond_b
    const-string/jumbo v0, "target"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 69
    invoke-static {p2}, Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->b:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget;

    goto/16 :goto_0

    .line 70
    :cond_c
    const-string/jumbo v0, "token"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->j:Ljava/lang/String;

    goto/16 :goto_0
.end method


# virtual methods
.method public parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/moments/JsonGuideTrend;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-static {p1}, Lcom/twitter/model/json/moments/JsonGuideTrend$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/moments/JsonGuideTrend;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/moments/JsonGuideTrend$$JsonObjectMapper;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/moments/JsonGuideTrend;

    move-result-object v0

    return-object v0
.end method

.method public serialize(Lcom/twitter/model/json/moments/JsonGuideTrend;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-static {p1, p2, p3}, Lcom/twitter/model/json/moments/JsonGuideTrend$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/moments/JsonGuideTrend;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 78
    return-void
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    check-cast p1, Lcom/twitter/model/json/moments/JsonGuideTrend;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/json/moments/JsonGuideTrend$$JsonObjectMapper;->serialize(Lcom/twitter/model/json/moments/JsonGuideTrend;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    return-void
.end method
