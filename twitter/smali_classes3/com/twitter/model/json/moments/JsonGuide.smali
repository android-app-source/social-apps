.class public Lcom/twitter/model/json/moments/JsonGuide;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcei;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/model/json/moments/JsonNavigationBar;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Lcom/twitter/model/moments/h;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcek;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/twitter/model/moments/j;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Lcfp;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public h:Lcej;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcei;
    .locals 10

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuide;->e:Lcfp;

    sget-object v1, Lcfp;->a:Lcfp;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcfp;

    .line 44
    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuide;->a:Lcom/twitter/model/json/moments/JsonNavigationBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuide;->a:Lcom/twitter/model/json/moments/JsonNavigationBar;

    .line 45
    invoke-virtual {v0}, Lcom/twitter/model/json/moments/JsonNavigationBar;->a()Lcom/twitter/model/moments/GuideCategories;

    move-result-object v1

    .line 46
    :goto_0
    new-instance v0, Lcei;

    iget-object v2, p0, Lcom/twitter/model/json/moments/JsonGuide;->b:Lcom/twitter/model/moments/h;

    new-instance v3, Lcom/twitter/model/moments/h;

    const-string/jumbo v4, ""

    const-string/jumbo v6, ""

    invoke-direct {v3, v4, v6}, Lcom/twitter/model/moments/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-static {v2, v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/h;

    iget-object v3, p0, Lcom/twitter/model/json/moments/JsonGuide;->c:Ljava/util/List;

    .line 48
    invoke-static {v3}, Lcom/twitter/util/collection/ImmutableList;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/model/json/moments/JsonGuide;->d:Lcom/twitter/model/moments/j;

    iget-wide v6, p0, Lcom/twitter/model/json/moments/JsonGuide;->f:J

    iget-object v8, p0, Lcom/twitter/model/json/moments/JsonGuide;->g:Ljava/lang/String;

    iget-object v9, p0, Lcom/twitter/model/json/moments/JsonGuide;->h:Lcej;

    invoke-direct/range {v0 .. v9}, Lcei;-><init>(Lcom/twitter/model/moments/GuideCategories;Lcom/twitter/model/moments/h;Ljava/util/List;Lcom/twitter/model/moments/j;Lcfp;JLjava/lang/String;Lcej;)V

    .line 46
    return-object v0

    .line 45
    :cond_0
    sget-object v1, Lcom/twitter/model/moments/GuideCategories;->a:Lcom/twitter/model/moments/GuideCategories;

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/model/json/moments/JsonGuide;->a()Lcei;

    move-result-object v0

    return-object v0
.end method
