.class public Lcom/twitter/model/json/moments/JsonGuideTrendSectionMetadata;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/moments/k;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/k;
    .locals 7

    .prologue
    .line 24
    new-instance v1, Lcom/twitter/model/moments/k;

    iget-wide v2, p0, Lcom/twitter/model/json/moments/JsonGuideTrendSectionMetadata;->a:J

    iget-wide v4, p0, Lcom/twitter/model/json/moments/JsonGuideTrendSectionMetadata;->b:J

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrendSectionMetadata;->c:Ljava/lang/String;

    .line 25
    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/twitter/model/moments/k;-><init>(JJLjava/lang/String;)V

    .line 24
    return-object v1
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/model/json/moments/JsonGuideTrendSectionMetadata;->a()Lcom/twitter/model/moments/k;

    move-result-object v0

    return-object v0
.end method
