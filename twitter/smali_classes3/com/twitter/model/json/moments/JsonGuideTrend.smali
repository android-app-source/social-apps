.class public Lcom/twitter/model/json/moments/JsonGuideTrend;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/moments/JsonGuideTrend$JsonDisclosure;,
        Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/moments/i;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonDisclosure;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public h:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "id"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/topic/trends/TrendBadge;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public k:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/i$a;
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->b:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    new-instance v0, Lcom/twitter/model/moments/i$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/i$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->a:Ljava/lang/String;

    .line 46
    invoke-static {v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->b:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget;

    iget-object v1, v1, Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget;->a:Ljava/lang/String;

    .line 47
    invoke-static {v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->b(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->d:Ljava/lang/String;

    .line 48
    invoke-static {v0, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/i$a;->c(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->c:I

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->a(I)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->i:Ljava/util/List;

    .line 50
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->a(Ljava/util/List;)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->b:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget;

    iget-object v1, v1, Lcom/twitter/model/json/moments/JsonGuideTrend$JsonTarget;->b:Ljava/util/List;

    .line 51
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->b(Ljava/util/List;)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->j:Ljava/lang/String;

    .line 52
    invoke-static {v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->e(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->k:J

    .line 53
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/moments/i$a;->b(J)Lcom/twitter/model/moments/i$a;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->g:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonDisclosure;

    if-eqz v1, :cond_0

    const-string/jumbo v1, "promoted"

    iget-object v2, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->g:Lcom/twitter/model/json/moments/JsonGuideTrend$JsonDisclosure;

    iget-object v2, v2, Lcom/twitter/model/json/moments/JsonGuideTrend$JsonDisclosure;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->a(Z)Lcom/twitter/model/moments/i$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->f:Ljava/lang/String;

    .line 56
    invoke-virtual {v1, v2}, Lcom/twitter/model/moments/i$a;->d(Ljava/lang/String;)Lcom/twitter/model/moments/i$a;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/model/json/moments/JsonGuideTrend;->h:J

    .line 57
    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/moments/i$a;->a(J)Lcom/twitter/model/moments/i$a;

    .line 61
    :goto_0
    return-object v0

    .line 59
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/i$a;->a(Z)Lcom/twitter/model/moments/i$a;

    goto :goto_0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/twitter/model/json/moments/JsonGuideTrend;->a()Lcom/twitter/model/moments/i$a;

    move-result-object v0

    return-object v0
.end method
