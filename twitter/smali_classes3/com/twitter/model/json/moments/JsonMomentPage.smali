.class public Lcom/twitter/model/json/moments/JsonMomentPage;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lceo;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcem;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Lcom/twitter/model/json/moments/JsonRenderData;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Lcom/twitter/model/moments/n;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Lcom/twitter/model/moments/MomentPageType;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Lcom/twitter/model/moments/l;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public h:Lcom/twitter/model/json/moments/JsonMomentModule;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public i:Lcom/twitter/model/moments/s;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lceo$a;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 46
    new-instance v0, Lceo$a;

    invoke-direct {v0}, Lceo$a;-><init>()V

    iget-object v2, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->a:Ljava/lang/String;

    .line 47
    invoke-virtual {v0, v2}, Lceo$a;->a(Ljava/lang/String;)Lceo$a;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->f:Lcom/twitter/model/moments/MomentPageType;

    sget-object v3, Lcom/twitter/model/moments/MomentPageType;->a:Lcom/twitter/model/moments/MomentPageType;

    .line 48
    invoke-static {v0, v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/MomentPageType;

    invoke-virtual {v2, v0}, Lceo$a;->a(Lcom/twitter/model/moments/MomentPageType;)Lceo$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->c:Lcem;

    .line 49
    invoke-virtual {v0, v2}, Lceo$a;->a(Lcem;)Lceo$a;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->d:Lcom/twitter/model/json/moments/JsonRenderData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->c:Lcem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->d:Lcom/twitter/model/json/moments/JsonRenderData;

    iget-object v3, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->c:Lcem;

    iget-object v3, v3, Lcem;->c:Lcom/twitter/util/math/Size;

    .line 51
    invoke-virtual {v0, v3}, Lcom/twitter/model/json/moments/JsonRenderData;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/e;

    move-result-object v0

    .line 50
    :goto_0
    invoke-virtual {v2, v0}, Lceo$a;->a(Lcom/twitter/model/moments/e;)Lceo$a;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->d:Lcom/twitter/model/json/moments/JsonRenderData;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->d:Lcom/twitter/model/json/moments/JsonRenderData;

    iget-object v0, v0, Lcom/twitter/model/json/moments/JsonRenderData;->b:Lcom/twitter/model/moments/w;

    .line 52
    :goto_1
    invoke-virtual {v2, v0}, Lceo$a;->a(Lcom/twitter/model/moments/w;)Lceo$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->b:J

    .line 53
    invoke-virtual {v0, v2, v3}, Lceo$a;->a(J)Lceo$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->e:Lcom/twitter/model/moments/n;

    .line 54
    invoke-virtual {v0, v2}, Lceo$a;->a(Lcom/twitter/model/moments/n;)Lceo$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->g:Lcom/twitter/model/moments/l;

    .line 55
    invoke-virtual {v0, v2}, Lceo$a;->a(Lcom/twitter/model/moments/l;)Lceo$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->h:Lcom/twitter/model/json/moments/JsonMomentModule;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->h:Lcom/twitter/model/json/moments/JsonMomentModule;

    .line 56
    invoke-virtual {v1}, Lcom/twitter/model/json/moments/JsonMomentModule;->d()Lcom/twitter/model/moments/o;

    move-result-object v1

    :cond_0
    invoke-virtual {v0, v1}, Lceo$a;->a(Lcom/twitter/model/moments/o;)Lceo$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->i:Lcom/twitter/model/moments/s;

    .line 57
    invoke-virtual {v0, v1}, Lceo$a;->a(Lcom/twitter/model/moments/s;)Lceo$a;

    move-result-object v0

    .line 46
    return-object v0

    :cond_1
    move-object v0, v1

    .line 51
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 50
    goto :goto_1
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/twitter/model/json/moments/JsonMomentPage;->a()Lceo$a;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->h:Lcom/twitter/model/json/moments/JsonMomentModule;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonMomentPage;->h:Lcom/twitter/model/json/moments/JsonMomentModule;

    iget-object v0, v0, Lcom/twitter/model/json/moments/JsonMomentModule;->b:Ljava/util/Map;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method
