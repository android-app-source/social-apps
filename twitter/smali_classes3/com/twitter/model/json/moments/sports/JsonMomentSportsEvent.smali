.class public Lcom/twitter/model/json/moments/sports/JsonMomentSportsEvent;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/moments/sports/JsonMomentSportsEvent$JsonParticipantScore;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcfn;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcfn$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcfn$a;
    .locals 4

    .prologue
    .line 34
    new-instance v0, Lcfn$a;

    invoke-direct {v0}, Lcfn$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/moments/sports/JsonMomentSportsEvent;->a:Ljava/lang/String;

    .line 35
    invoke-virtual {v0, v1}, Lcfn$a;->a(Ljava/lang/String;)Lcfn$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/sports/JsonMomentSportsEvent;->b:Ljava/lang/String;

    .line 36
    invoke-virtual {v0, v1}, Lcfn$a;->b(Ljava/lang/String;)Lcfn$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/sports/JsonMomentSportsEvent;->c:Ljava/util/List;

    .line 37
    invoke-virtual {v0, v1}, Lcfn$a;->a(Ljava/util/List;)Lcfn$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/moments/sports/JsonMomentSportsEvent;->d:J

    .line 38
    invoke-virtual {v0, v2, v3}, Lcfn$a;->a(J)Lcfn$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/moments/sports/JsonMomentSportsEvent;->e:Ljava/util/List;

    .line 39
    invoke-virtual {v0, v1}, Lcfn$a;->b(Ljava/util/List;)Lcfn$a;

    move-result-object v0

    .line 34
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/model/json/moments/sports/JsonMomentSportsEvent;->a()Lcfn$a;

    move-result-object v0

    return-object v0
.end method
