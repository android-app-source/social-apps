.class public Lcom/twitter/model/json/moments/maker/JsonDeleteMomentRequest;
.super Lcom/twitter/model/json/common/a;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation


# instance fields
.field public a:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/twitter/model/json/common/a;-><init>()V

    return-void
.end method

.method public static a()Lcom/twitter/model/json/moments/maker/JsonDeleteMomentRequest;
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/model/json/moments/maker/JsonDeleteMomentRequest;

    invoke-direct {v0}, Lcom/twitter/model/json/moments/maker/JsonDeleteMomentRequest;-><init>()V

    .line 19
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/model/json/moments/maker/JsonDeleteMomentRequest;->a:Z

    .line 20
    return-object v0
.end method
