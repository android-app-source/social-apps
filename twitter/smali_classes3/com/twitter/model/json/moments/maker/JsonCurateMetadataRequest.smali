.class public Lcom/twitter/model/json/moments/maker/JsonCurateMetadataRequest;
.super Lcom/twitter/model/json/common/a;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/json/moments/maker/JsonTweetMetadata;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/model/json/common/a;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;)Lcom/twitter/model/json/moments/maker/JsonCurateMetadataRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcfa;",
            ">;)",
            "Lcom/twitter/model/json/moments/maker/JsonCurateMetadataRequest;"
        }
    .end annotation

    .prologue
    .line 21
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 22
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfa;

    .line 23
    iget-object v0, v0, Lcfa;->a:Lcez;

    invoke-static {v0}, Lcom/twitter/model/json/moments/maker/JsonTweetMetadata;->a(Lcez;)Lcom/twitter/model/json/moments/maker/JsonTweetMetadata;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 25
    :cond_0
    new-instance v2, Lcom/twitter/model/json/moments/maker/JsonCurateMetadataRequest;

    invoke-direct {v2}, Lcom/twitter/model/json/moments/maker/JsonCurateMetadataRequest;-><init>()V

    .line 26
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, v2, Lcom/twitter/model/json/moments/maker/JsonCurateMetadataRequest;->a:Ljava/util/List;

    .line 27
    return-object v2
.end method
