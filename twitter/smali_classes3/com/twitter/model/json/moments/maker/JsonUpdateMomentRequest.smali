.class public Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;
.super Lcom/twitter/model/json/common/a;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcdm;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Lcom/twitter/model/json/moments/maker/JsonCoverData;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Lcom/twitter/model/json/moments/JsonThemeData;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Lcom/twitter/model/moments/MomentVisibilityMode;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Lcdm;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/model/json/common/a;-><init>()V

    return-void
.end method

.method public static a(Lcex;)Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;

    invoke-direct {v0}, Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;-><init>()V

    .line 34
    iget-object v1, p0, Lcex;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;->a:Ljava/lang/String;

    .line 35
    iget-object v1, p0, Lcex;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;->b:Ljava/lang/String;

    .line 36
    iget-object v1, p0, Lcex;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 37
    iget-object v1, p0, Lcex;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Lcdm;->a(Z)Lcdm;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;->c:Lcdm;

    .line 39
    :cond_0
    iget-object v1, p0, Lcex;->d:Lcez;

    if-eqz v1, :cond_1

    .line 40
    iget-object v1, p0, Lcex;->d:Lcez;

    invoke-static {v1}, Lcom/twitter/model/json/moments/maker/JsonCoverData;->a(Lcez;)Lcom/twitter/model/json/moments/maker/JsonCoverData;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;->d:Lcom/twitter/model/json/moments/maker/JsonCoverData;

    .line 42
    :cond_1
    iget-object v1, p0, Lcex;->e:Lcom/twitter/model/moments/w;

    if-eqz v1, :cond_2

    .line 43
    iget-object v1, p0, Lcex;->e:Lcom/twitter/model/moments/w;

    invoke-static {v1}, Lcom/twitter/model/json/moments/JsonThemeData;->a(Lcom/twitter/model/moments/w;)Lcom/twitter/model/json/moments/JsonThemeData;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;->e:Lcom/twitter/model/json/moments/JsonThemeData;

    .line 45
    :cond_2
    iget-object v1, p0, Lcex;->f:Lcom/twitter/model/moments/MomentVisibilityMode;

    iput-object v1, v0, Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;->f:Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 46
    iget-object v1, p0, Lcex;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 47
    iget-object v1, p0, Lcex;->g:Ljava/lang/Boolean;

    .line 48
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Lcdm;->a(Z)Lcdm;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;->g:Lcdm;

    .line 50
    :cond_3
    return-object v0
.end method
