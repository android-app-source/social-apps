.class public Lcom/twitter/model/json/moments/JsonCTA;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lceg;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcom/twitter/model/moments/m;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lceg$a;
    .locals 2

    .prologue
    .line 25
    new-instance v1, Lceg$a;

    invoke-direct {v1}, Lceg$a;-><init>()V

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonCTA;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lceg$a;->a(Ljava/lang/String;)Lceg$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonCTA;->b:Ljava/lang/String;

    .line 26
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lceg$a;->b(Ljava/lang/String;)Lceg$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonCTA;->c:Lcom/twitter/model/moments/m;

    .line 27
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/m;

    invoke-virtual {v1, v0}, Lceg$a;->a(Lcom/twitter/model/moments/m;)Lceg$a;

    move-result-object v0

    .line 25
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/model/json/moments/JsonCTA;->a()Lceg$a;

    move-result-object v0

    return-object v0
.end method
