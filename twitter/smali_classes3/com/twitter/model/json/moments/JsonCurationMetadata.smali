.class public Lcom/twitter/model/json/moments/JsonCurationMetadata;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/moments/f;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/model/moments/MomentVisibilityMode;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Lcdm;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/f;
    .locals 4

    .prologue
    .line 26
    new-instance v2, Lcom/twitter/model/moments/f;

    iget-object v0, p0, Lcom/twitter/model/json/moments/JsonCurationMetadata;->a:Lcom/twitter/model/moments/MomentVisibilityMode;

    sget-object v1, Lcom/twitter/model/moments/MomentVisibilityMode;->a:Lcom/twitter/model/moments/MomentVisibilityMode;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/MomentVisibilityMode;

    iget-object v1, p0, Lcom/twitter/model/json/moments/JsonCurationMetadata;->b:Lcdm;

    iget-boolean v1, v1, Lcdm;->a:Z

    .line 27
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    iget-boolean v3, p0, Lcom/twitter/model/json/moments/JsonCurationMetadata;->c:Z

    invoke-direct {v2, v0, v1, v3}, Lcom/twitter/model/moments/f;-><init>(Lcom/twitter/model/moments/MomentVisibilityMode;Ljava/lang/Boolean;Z)V

    .line 26
    return-object v2
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/twitter/model/json/moments/JsonCurationMetadata;->a()Lcom/twitter/model/moments/f;

    move-result-object v0

    return-object v0
.end method
