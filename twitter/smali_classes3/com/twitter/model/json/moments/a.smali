.class public Lcom/twitter/model/json/moments/a;
.super Lcom/twitter/model/json/common/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/i",
        "<",
        "Lcom/twitter/model/moments/MomentVisibilityMode;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    .line 12
    sget-object v0, Lcom/twitter/model/moments/MomentVisibilityMode;->a:Lcom/twitter/model/moments/MomentVisibilityMode;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/util/Map$Entry;

    const/4 v2, 0x0

    const-string/jumbo v3, "private"

    sget-object v4, Lcom/twitter/model/moments/MomentVisibilityMode;->b:Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 13
    invoke-static {v3, v4}, Lcom/twitter/model/json/moments/a;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "public"

    sget-object v4, Lcom/twitter/model/moments/MomentVisibilityMode;->a:Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 14
    invoke-static {v3, v4}, Lcom/twitter/model/json/moments/a;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "unlisted"

    sget-object v4, Lcom/twitter/model/moments/MomentVisibilityMode;->c:Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 15
    invoke-static {v3, v4}, Lcom/twitter/model/json/moments/a;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    aput-object v3, v1, v2

    .line 12
    invoke-direct {p0, v0, v1}, Lcom/twitter/model/json/common/i;-><init>(Ljava/lang/Object;[Ljava/util/Map$Entry;)V

    .line 16
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/twitter/model/moments/MomentVisibilityMode;
    .locals 1

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/twitter/model/json/common/i;->getFromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/MomentVisibilityMode;

    return-object v0
.end method

.method public synthetic getFromString(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/moments/a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/MomentVisibilityMode;

    move-result-object v0

    return-object v0
.end method
