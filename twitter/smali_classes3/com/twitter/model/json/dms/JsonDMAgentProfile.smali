.class public Lcom/twitter/model/json/dms/JsonDMAgentProfile;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/dms/JsonDMAgentProfile$JsonAgentProfileMedia;,
        Lcom/twitter/model/json/dms/JsonDMAgentProfile$JsonAgentProfileAvatar;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/dms/o;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcom/twitter/model/json/dms/JsonDMAgentProfile$JsonAgentProfileAvatar;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/dms/o$a;
    .locals 4

    .prologue
    .line 24
    new-instance v0, Lcom/twitter/model/dms/o$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/o$a;-><init>()V

    iget-wide v2, p0, Lcom/twitter/model/json/dms/JsonDMAgentProfile;->a:J

    .line 25
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/o$a;->a(J)Lcom/twitter/model/dms/o$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonDMAgentProfile;->b:Ljava/lang/String;

    .line 26
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/o$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/o$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonDMAgentProfile;->c:Lcom/twitter/model/json/dms/JsonDMAgentProfile$JsonAgentProfileAvatar;

    iget-object v1, v1, Lcom/twitter/model/json/dms/JsonDMAgentProfile$JsonAgentProfileAvatar;->a:Lcom/twitter/model/json/dms/JsonDMAgentProfile$JsonAgentProfileMedia;

    iget-object v1, v1, Lcom/twitter/model/json/dms/JsonDMAgentProfile$JsonAgentProfileMedia;->a:Ljava/lang/String;

    .line 27
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/o$a;->b(Ljava/lang/String;)Lcom/twitter/model/dms/o$a;

    move-result-object v0

    .line 24
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/model/json/dms/JsonDMAgentProfile;->a()Lcom/twitter/model/dms/o$a;

    move-result-object v0

    return-object v0
.end method
