.class abstract Lcom/twitter/model/json/dms/a;
.super Lcom/twitter/model/json/common/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/twitter/model/dms/a;",
        "B:",
        "Lcom/twitter/model/dms/a$a",
        "<TE;TB;>;>",
        "Lcom/twitter/model/json/common/h",
        "<TE;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/twitter/model/json/common/h;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Lcom/twitter/model/dms/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TB;"
        }
    .end annotation
.end method

.method public a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/dms/a;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    const-string/jumbo v0, "dm_agent_personalization_enabled"

    .line 41
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v2

    .line 43
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 45
    invoke-virtual {p0}, Lcom/twitter/model/json/dms/a;->a()Lcom/twitter/model/dms/a$a;

    move-result-object v3

    .line 46
    new-instance v4, Lcom/twitter/model/dms/a$c$a;

    invoke-direct {v4}, Lcom/twitter/model/dms/a$c$a;-><init>()V

    .line 48
    const/4 v1, 0x0

    .line 49
    :goto_0
    if-eqz v0, :cond_d

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_d

    .line 50
    sget-object v5, Lcom/twitter/model/json/dms/a$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    .line 143
    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 53
    :pswitch_0
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 54
    const-string/jumbo v5, "id"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 55
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/twitter/model/dms/a$a;->b(J)Lcom/twitter/model/dms/d$a;

    goto :goto_1

    .line 56
    :cond_1
    const-string/jumbo v5, "conversation_id"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 57
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/model/dms/a$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    goto :goto_1

    .line 58
    :cond_2
    const-string/jumbo v5, "request_id"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 59
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/model/dms/a$c$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/e$b$b;

    goto :goto_1

    .line 60
    :cond_3
    if-eqz v2, :cond_0

    const-string/jumbo v5, "custom_profile_id"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/twitter/model/dms/a$c$a;->a(J)Lcom/twitter/model/dms/a$c$a;

    goto :goto_1

    .line 66
    :pswitch_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 67
    const-string/jumbo v5, "marked_as_spam"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 68
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/twitter/model/dms/a$c$a;->a(Z)Lcom/twitter/model/dms/e$b$b;

    goto :goto_1

    .line 69
    :cond_4
    const-string/jumbo v5, "affects_sort"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    .line 71
    invoke-virtual {v3, v0}, Lcom/twitter/model/dms/a$a;->a(Z)Lcom/twitter/model/dms/c$b;

    goto :goto_1

    .line 76
    :pswitch_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 80
    :pswitch_3
    const-string/jumbo v0, "message_data"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 81
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 82
    :goto_2
    if-eqz v0, :cond_0

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_0

    .line 83
    sget-object v5, Lcom/twitter/model/json/dms/a$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_1

    :cond_5
    move-object v0, v1

    .line 128
    :goto_3
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    goto :goto_2

    .line 86
    :pswitch_4
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 87
    const-string/jumbo v5, "id"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 88
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/twitter/model/dms/a$c$a;->b(J)Lcom/twitter/model/dms/e$b$b;

    move-object v0, v1

    goto :goto_3

    .line 89
    :cond_6
    const-string/jumbo v5, "time"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 90
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/twitter/model/dms/a$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-object v0, v1

    goto :goto_3

    .line 91
    :cond_7
    const-string/jumbo v5, "text"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 92
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-virtual {v4, v0}, Lcom/twitter/model/dms/a$c$a;->c(Ljava/lang/String;)Lcom/twitter/model/dms/e$b$b;

    move-object v0, v1

    .line 94
    goto :goto_3

    :cond_8
    const-string/jumbo v5, "sender_id"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 95
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/twitter/model/dms/a$a;->a(J)Lcom/twitter/model/dms/c$b;

    move-object v0, v1

    goto :goto_3

    .line 100
    :pswitch_5
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 104
    :pswitch_6
    const-string/jumbo v0, "entities"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 105
    const-class v0, Lcom/twitter/model/core/v;

    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    invoke-virtual {v4, v0}, Lcom/twitter/model/dms/a$c$a;->b(Lcom/twitter/model/core/v;)Lcom/twitter/model/dms/e$b$b;

    move-object v0, v1

    goto :goto_3

    .line 107
    :cond_9
    const-string/jumbo v0, "attachment"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 108
    const-class v0, Lcbx;

    .line 109
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->f(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Iterable;

    move-result-object v0

    .line 108
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx;

    .line 110
    invoke-virtual {v4, v0}, Lcom/twitter/model/dms/a$c$a;->a(Lcbx;)Lcom/twitter/model/dms/e$b$b;

    move-object v0, v1

    .line 111
    goto/16 :goto_3

    :cond_a
    const-string/jumbo v0, "quick_reply"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 112
    const-class v0, Lcck;

    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcck;

    .line 114
    invoke-virtual {v4, v0}, Lcom/twitter/model/dms/a$c$a;->a(Lcck;)Lcom/twitter/model/dms/a$c$a;

    move-object v0, v1

    .line 115
    goto/16 :goto_3

    .line 116
    :cond_b
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    .line 118
    goto/16 :goto_3

    .line 121
    :pswitch_7
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    .line 122
    goto/16 :goto_3

    .line 131
    :cond_c
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 136
    :pswitch_8
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 147
    :cond_d
    invoke-virtual {v4}, Lcom/twitter/model/dms/a$c$a;->q()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/model/dms/a$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/a$a;

    .line 148
    invoke-virtual {v0}, Lcom/twitter/model/dms/a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/a;

    .line 146
    return-object v0

    .line 50
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_8
        :pswitch_1
    .end packed-switch

    .line 83
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/dms/a;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/dms/a;

    move-result-object v0

    return-object v0
.end method
