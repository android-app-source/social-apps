.class public Lcom/twitter/model/json/dms/c;
.super Lcom/twitter/model/json/common/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/h",
        "<",
        "Lcbx;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/model/json/common/h;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonParser;)Lcbx;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 30
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 31
    sget-object v1, Lccf;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    const-class v0, Lcom/twitter/model/core/MediaEntity;

    .line 33
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 32
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 34
    new-instance v1, Lccf$a;

    invoke-direct {v1}, Lccf$a;-><init>()V

    .line 35
    invoke-virtual {v1, v0}, Lccf$a;->a(Lcom/twitter/model/core/MediaEntity;)Lccf$a;

    move-result-object v0

    .line 36
    invoke-virtual {v0}, Lccf$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx;

    .line 74
    :goto_0
    return-object v0

    .line 37
    :cond_0
    const-string/jumbo v1, "tweet"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 38
    const-class v0, Lcom/twitter/model/json/dms/JsonTweetAttachment;

    .line 39
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 38
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonTweetAttachment;

    .line 40
    iget-object v2, v0, Lcom/twitter/model/json/dms/JsonTweetAttachment;->f:Lcom/twitter/model/core/ac;

    .line 41
    if-eqz v2, :cond_2

    new-instance v1, Lcom/twitter/model/core/r;

    invoke-direct {v1, v2}, Lcom/twitter/model/core/r;-><init>(Lcom/twitter/model/core/ac;)V

    .line 43
    :goto_1
    iget-object v2, v0, Lcom/twitter/model/json/dms/JsonTweetAttachment;->d:[I

    .line 45
    new-instance v3, Lcci$a;

    invoke-direct {v3}, Lcci$a;-><init>()V

    .line 46
    invoke-virtual {v3, v1}, Lcci$a;->a(Lcom/twitter/model/core/r;)Lcci$a;

    move-result-object v1

    iget-wide v4, v0, Lcom/twitter/model/json/dms/JsonTweetAttachment;->e:J

    .line 47
    invoke-virtual {v1, v4, v5}, Lcci$a;->a(J)Lcci$a;

    move-result-object v1

    iget-object v3, v0, Lcom/twitter/model/json/dms/JsonTweetAttachment;->a:Ljava/lang/String;

    .line 48
    invoke-virtual {v1, v3}, Lcci$a;->a(Ljava/lang/String;)Lcbx$a;

    move-result-object v1

    check-cast v1, Lcci$a;

    iget-object v3, v0, Lcom/twitter/model/json/dms/JsonTweetAttachment;->b:Ljava/lang/String;

    .line 49
    invoke-virtual {v1, v3}, Lcci$a;->b(Ljava/lang/String;)Lcbx$a;

    move-result-object v1

    check-cast v1, Lcci$a;

    iget-object v0, v0, Lcom/twitter/model/json/dms/JsonTweetAttachment;->c:Ljava/lang/String;

    .line 50
    invoke-virtual {v1, v0}, Lcci$a;->c(Ljava/lang/String;)Lcbx$a;

    move-result-object v0

    check-cast v0, Lcci$a;

    .line 51
    if-eqz v2, :cond_1

    array-length v1, v2

    if-ne v1, v8, :cond_1

    .line 52
    aget v1, v2, v6

    invoke-virtual {v0, v1}, Lcci$a;->a(I)Lcbx$a;

    move-result-object v1

    check-cast v1, Lcci$a;

    aget v2, v2, v7

    .line 53
    invoke-virtual {v1, v2}, Lcci$a;->b(I)Lcbx$a;

    .line 56
    :cond_1
    invoke-virtual {v0}, Lcci$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx;

    goto :goto_0

    .line 41
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 57
    :cond_3
    const-string/jumbo v1, "card"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 58
    const-class v0, Lcbz;

    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx;

    goto :goto_0

    .line 59
    :cond_4
    const-string/jumbo v1, "sticker"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 60
    const-class v0, Lcch;

    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx;

    goto/16 :goto_0

    .line 61
    :cond_5
    const-string/jumbo v1, "location"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 62
    const-class v0, Lcby;

    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx;

    goto/16 :goto_0

    .line 64
    :cond_6
    const-class v0, Lcom/twitter/model/json/dms/JsonUnknownAttachment;

    .line 65
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 64
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonUnknownAttachment;

    .line 67
    new-instance v2, Lccj$a;

    invoke-direct {v2}, Lccj$a;-><init>()V

    .line 69
    iget-object v3, v0, Lcom/twitter/model/json/dms/JsonUnknownAttachment;->d:[I

    .line 70
    if-eqz v3, :cond_7

    array-length v1, v3

    if-ne v1, v8, :cond_7

    .line 71
    aget v1, v3, v6

    invoke-virtual {v2, v1}, Lccj$a;->a(I)Lcbx$a;

    move-result-object v1

    check-cast v1, Lccj$a;

    aget v3, v3, v7

    .line 72
    invoke-virtual {v1, v3}, Lccj$a;->b(I)Lcbx$a;

    .line 74
    :cond_7
    iget-object v1, v0, Lcom/twitter/model/json/dms/JsonUnknownAttachment;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lccj$a;->a(Ljava/lang/String;)Lcbx$a;

    move-result-object v1

    check-cast v1, Lccj$a;

    iget-object v2, v0, Lcom/twitter/model/json/dms/JsonUnknownAttachment;->b:Ljava/lang/String;

    .line 75
    invoke-virtual {v1, v2}, Lccj$a;->b(Ljava/lang/String;)Lcbx$a;

    move-result-object v1

    check-cast v1, Lccj$a;

    iget-object v0, v0, Lcom/twitter/model/json/dms/JsonUnknownAttachment;->c:Ljava/lang/String;

    .line 76
    invoke-virtual {v1, v0}, Lccj$a;->c(Ljava/lang/String;)Lcbx$a;

    move-result-object v0

    check-cast v0, Lccj$a;

    .line 77
    invoke-virtual {v0}, Lccj$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx;

    goto/16 :goto_0
.end method

.method public synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/dms/c;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcbx;

    move-result-object v0

    return-object v0
.end method
