.class public Lcom/twitter/model/json/dms/JsonParticipant;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/dms/Participant;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/dms/Participant$a;
    .locals 4

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/model/dms/Participant$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/Participant$a;-><init>()V

    iget-wide v2, p0, Lcom/twitter/model/json/dms/JsonParticipant;->a:J

    .line 26
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/Participant$a;->a(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/dms/JsonParticipant;->b:J

    .line 27
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/Participant$a;->b(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/dms/JsonParticipant;->c:J

    .line 28
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/Participant$a;->c(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/dms/JsonParticipant;->d:J

    .line 29
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/Participant$a;->d(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    .line 25
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/dms/JsonParticipant;->a()Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    return-object v0
.end method
