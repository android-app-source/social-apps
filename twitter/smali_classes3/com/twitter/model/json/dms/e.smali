.class public Lcom/twitter/model/json/dms/e;
.super Lcom/twitter/model/json/common/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/h",
        "<",
        "Lcom/twitter/model/dms/d;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/twitter/model/json/common/h;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/dms/d;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 46
    const-string/jumbo v0, "b2c_feedback_submitted_dm_event_enabled"

    .line 47
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v3

    .line 48
    const-string/jumbo v0, "b2c_feedback_dismissed_dm_event_enabled"

    .line 49
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v4

    .line 51
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v1, v2

    .line 55
    :goto_0
    if-eqz v0, :cond_13

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_13

    .line 56
    sget-object v5, Lcom/twitter/model/json/dms/e$2;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v2

    .line 252
    :goto_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object v10, v0

    move-object v0, v2

    move-object v2, v10

    goto :goto_0

    .line 58
    :pswitch_0
    const-string/jumbo v0, "message"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-class v0, Lcom/twitter/model/dms/s;

    .line 60
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    goto :goto_1

    .line 61
    :cond_0
    invoke-static {}, Lcom/twitter/model/dms/u;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "welcome_message_create"

    .line 62
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    const-class v0, Lcom/twitter/model/dms/u;

    .line 64
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    goto :goto_1

    .line 65
    :cond_1
    const-string/jumbo v0, "conversation_create"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    const-class v0, Lcom/twitter/model/dms/j;

    .line 67
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    goto :goto_1

    .line 68
    :cond_2
    const-string/jumbo v0, "remove_conversation"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    const-class v0, Lcom/twitter/model/dms/v;

    .line 70
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    goto :goto_1

    .line 71
    :cond_3
    const-string/jumbo v0, "join_conversation"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 72
    const-class v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;

    .line 73
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;

    .line 75
    iget-object v1, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->a:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 76
    iget-object v1, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->a:Ljava/util/List;

    new-instance v5, Lcom/twitter/model/json/dms/e$1;

    invoke-direct {v5, p0}, Lcom/twitter/model/json/dms/e$1;-><init>(Lcom/twitter/model/json/dms/e;)V

    invoke-static {v1, v5}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    .line 87
    :goto_2
    new-instance v5, Lcom/twitter/model/dms/z$a;

    invoke-direct {v5}, Lcom/twitter/model/dms/z$a;-><init>()V

    .line 88
    invoke-virtual {v5, v1}, Lcom/twitter/model/dms/z$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/z$a;

    iget-boolean v5, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->i:Z

    .line 89
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/z$a;->a(Z)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/z$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->e:J

    .line 90
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/z$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/z$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->f:Ljava/lang/String;

    .line 91
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/z$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/z$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->g:J

    .line 92
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/z$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/z$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->h:J

    .line 93
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/z$a;->a(J)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/z$a;

    .line 94
    invoke-virtual {v0}, Lcom/twitter/model/dms/z$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    .line 95
    goto/16 :goto_1

    .line 85
    :cond_4
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    goto :goto_2

    .line 95
    :cond_5
    const-string/jumbo v0, "message_delete"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 96
    const-class v0, Lcom/twitter/model/dms/w;

    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    goto/16 :goto_1

    .line 97
    :cond_6
    const-string/jumbo v0, "conversation_name_update"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 98
    const-class v0, Lcom/twitter/model/json/dms/JsonUpdateConversationNameEntry;

    .line 99
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonUpdateConversationNameEntry;

    .line 100
    if-eqz v0, :cond_17

    .line 101
    new-instance v1, Lcom/twitter/model/dms/ak$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/ak$a;-><init>()V

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationNameEntry;->a:Ljava/lang/String;

    .line 102
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/ak$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ak$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationNameEntry;->b:J

    .line 103
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/ak$a;->a(J)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ak$a;

    iget-boolean v5, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationNameEntry;->d:Z

    .line 104
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/ak$a;->a(Z)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ak$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationNameEntry;->e:J

    .line 105
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/ak$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ak$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationNameEntry;->f:Ljava/lang/String;

    .line 106
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/ak$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ak$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationNameEntry;->g:J

    .line 107
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/ak$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/ak$a;

    .line 108
    invoke-virtual {v0}, Lcom/twitter/model/dms/ak$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    :goto_3
    move-object v1, v0

    move-object v0, v2

    .line 110
    goto/16 :goto_1

    :cond_7
    const-string/jumbo v0, "conversation_avatar_update"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 111
    const-class v0, Lcom/twitter/model/json/dms/JsonUpdateConversationAvatarEntry;

    .line 112
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonUpdateConversationAvatarEntry;

    .line 113
    if-eqz v0, :cond_16

    .line 114
    new-instance v1, Lcom/twitter/model/dms/ai$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/ai$a;-><init>()V

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationAvatarEntry;->a:Ljava/lang/String;

    .line 115
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/ai$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ai$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationAvatarEntry;->b:J

    .line 116
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/ai$a;->a(J)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ai$a;

    iget-boolean v5, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationAvatarEntry;->d:Z

    .line 117
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/ai$a;->a(Z)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ai$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationAvatarEntry;->e:J

    .line 118
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/ai$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ai$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationAvatarEntry;->f:Ljava/lang/String;

    .line 119
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/ai$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ai$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationAvatarEntry;->g:J

    .line 120
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/ai$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/ai$a;

    .line 121
    invoke-virtual {v0}, Lcom/twitter/model/dms/ai$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    :goto_4
    move-object v1, v0

    move-object v0, v2

    .line 123
    goto/16 :goto_1

    :cond_8
    const-string/jumbo v0, "participants_join"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 124
    const-class v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;

    .line 125
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;

    .line 126
    new-instance v1, Lcom/twitter/model/dms/ae$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/ae$a;-><init>()V

    new-instance v5, Lcom/twitter/model/dms/ae$b$a;

    invoke-direct {v5}, Lcom/twitter/model/dms/ae$b$a;-><init>()V

    iget-object v6, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->a:Ljava/util/List;

    .line 128
    invoke-virtual {v5, v6}, Lcom/twitter/model/dms/ae$b$a;->a(Ljava/util/List;)Lcom/twitter/model/dms/ae$b$a;

    move-result-object v5

    .line 129
    invoke-virtual {v5}, Lcom/twitter/model/dms/ae$b$a;->q()Ljava/lang/Object;

    move-result-object v5

    .line 127
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/ae$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ae$a;

    iget-boolean v5, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->i:Z

    .line 130
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/ae$a;->a(Z)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ae$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->e:J

    .line 131
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/ae$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ae$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->f:Ljava/lang/String;

    .line 132
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/ae$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ae$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->g:J

    .line 133
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/ae$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ae$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->h:J

    .line 134
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/ae$a;->a(J)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/ae$a;

    .line 135
    invoke-virtual {v0}, Lcom/twitter/model/dms/ae$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    .line 136
    goto/16 :goto_1

    :cond_9
    const-string/jumbo v0, "participants_leave"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 137
    const-class v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;

    .line 138
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;

    .line 139
    new-instance v1, Lcom/twitter/model/dms/af$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/af$a;-><init>()V

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->a:Ljava/util/List;

    .line 140
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/af$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/af$a;

    iget-boolean v5, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->i:Z

    .line 141
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/af$a;->a(Z)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/af$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->e:J

    .line 142
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/af$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/af$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->f:Ljava/lang/String;

    .line 143
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/af$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/af$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->g:J

    .line 144
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/af$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/af$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonParticipantsEntry;->h:J

    .line 145
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/af$a;->a(J)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/af$a;

    .line 146
    invoke-virtual {v0}, Lcom/twitter/model/dms/af$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    .line 147
    goto/16 :goto_1

    :cond_a
    const-string/jumbo v0, "conversation_read"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 148
    const-class v0, Lcom/twitter/model/json/dms/JsonReadStateEvent;

    .line 149
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonReadStateEvent;

    .line 150
    new-instance v1, Lcom/twitter/model/dms/ab$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/ab$a;-><init>()V

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonReadStateEvent;->e:J

    .line 151
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/ab$a;->a(J)Lcom/twitter/model/dms/f$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ab$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonReadStateEvent;->a:J

    .line 152
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/ab$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ab$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonReadStateEvent;->c:Ljava/lang/String;

    .line 153
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/ab$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/ab$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonReadStateEvent;->b:J

    .line 154
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/ab$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/ab$a;

    .line 155
    invoke-virtual {v0}, Lcom/twitter/model/dms/ab$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    .line 156
    goto/16 :goto_1

    :cond_b
    const-string/jumbo v0, "mark_all_as_read"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 157
    const-class v0, Lcom/twitter/model/json/dms/JsonReadStateEvent;

    .line 158
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonReadStateEvent;

    .line 159
    new-instance v1, Lcom/twitter/model/dms/aa$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/aa$a;-><init>()V

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonReadStateEvent;->e:J

    .line 160
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/aa$a;->a(J)Lcom/twitter/model/dms/f$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/aa$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonReadStateEvent;->a:J

    .line 161
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/aa$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/aa$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonReadStateEvent;->c:Ljava/lang/String;

    .line 162
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/aa$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/aa$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonReadStateEvent;->b:J

    .line 163
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/aa$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/aa$a;

    .line 164
    invoke-virtual {v0}, Lcom/twitter/model/dms/aa$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    .line 165
    goto/16 :goto_1

    :cond_c
    const-string/jumbo v0, "disable_notifications"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 166
    const-class v0, Lcom/twitter/model/json/dms/JsonUpdateConversationMuteStateEvent;

    .line 167
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 166
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonUpdateConversationMuteStateEvent;

    .line 168
    new-instance v1, Lcom/twitter/model/dms/aj$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/aj$a;-><init>()V

    .line 169
    invoke-virtual {v1, v9}, Lcom/twitter/model/dms/aj$a;->a(Z)Lcom/twitter/model/dms/aj$a;

    move-result-object v1

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationMuteStateEvent;->a:J

    .line 170
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/aj$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/aj$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationMuteStateEvent;->c:Ljava/lang/String;

    .line 171
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/aj$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/aj$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationMuteStateEvent;->b:J

    .line 172
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/aj$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/aj$a;

    .line 173
    invoke-virtual {v0}, Lcom/twitter/model/dms/aj$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    .line 174
    goto/16 :goto_1

    :cond_d
    const-string/jumbo v0, "enable_notifications"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 175
    const-class v0, Lcom/twitter/model/json/dms/JsonUpdateConversationMuteStateEvent;

    .line 176
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 175
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonUpdateConversationMuteStateEvent;

    .line 177
    new-instance v1, Lcom/twitter/model/dms/aj$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/aj$a;-><init>()V

    .line 178
    invoke-virtual {v1, v8}, Lcom/twitter/model/dms/aj$a;->a(Z)Lcom/twitter/model/dms/aj$a;

    move-result-object v1

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationMuteStateEvent;->a:J

    .line 179
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/aj$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/aj$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationMuteStateEvent;->c:Ljava/lang/String;

    .line 180
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/aj$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/aj$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonUpdateConversationMuteStateEvent;->b:J

    .line 181
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/aj$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/aj$a;

    .line 182
    invoke-virtual {v0}, Lcom/twitter/model/dms/aj$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    .line 183
    goto/16 :goto_1

    :cond_e
    const-string/jumbo v0, "message_mark_as_spam"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 184
    const-class v0, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;

    .line 185
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;

    .line 186
    new-instance v1, Lcom/twitter/model/dms/al$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/al$a;-><init>()V

    .line 187
    invoke-virtual {v1, v9}, Lcom/twitter/model/dms/al$a;->a(Z)Lcom/twitter/model/dms/al$a;

    move-result-object v1

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;->f:Ljava/util/List;

    .line 188
    invoke-static {v5}, Lcom/twitter/model/dms/w;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/al$a;->a(Ljava/util/List;)Lcom/twitter/model/dms/w$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/al$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;->a:J

    .line 189
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/al$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/w$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;->c:Ljava/lang/String;

    .line 190
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/w$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/w$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;->b:J

    .line 191
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/w$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/w$a;

    .line 192
    invoke-virtual {v0}, Lcom/twitter/model/dms/w$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    .line 193
    goto/16 :goto_1

    :cond_f
    const-string/jumbo v0, "message_unmark_as_spam"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 194
    const-class v0, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;

    .line 195
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;

    .line 196
    new-instance v1, Lcom/twitter/model/dms/al$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/al$a;-><init>()V

    .line 197
    invoke-virtual {v1, v8}, Lcom/twitter/model/dms/al$a;->a(Z)Lcom/twitter/model/dms/al$a;

    move-result-object v1

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;->f:Ljava/util/List;

    .line 198
    invoke-static {v5}, Lcom/twitter/model/dms/w;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/al$a;->a(Ljava/util/List;)Lcom/twitter/model/dms/w$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/al$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;->a:J

    .line 199
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/al$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/w$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;->c:Ljava/lang/String;

    .line 200
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/w$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/w$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonDeleteMessageEvent;->b:J

    .line 201
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/w$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/w$a;

    .line 202
    invoke-virtual {v0}, Lcom/twitter/model/dms/w$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    move-object v1, v0

    move-object v0, v2

    .line 203
    goto/16 :goto_1

    :cond_10
    const-string/jumbo v0, "cs_feedback_submitted"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    if-eqz v3, :cond_11

    .line 204
    const-class v0, Lcom/twitter/model/json/dms/JsonCSFeedbackSubmittedEntry;

    .line 205
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonCSFeedbackSubmittedEntry;

    .line 206
    if-eqz v0, :cond_15

    .line 207
    new-instance v1, Lcom/twitter/model/dms/h$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/h$a;-><init>()V

    new-instance v5, Lcom/twitter/model/dms/h$c$a;

    invoke-direct {v5}, Lcom/twitter/model/dms/h$c$a;-><init>()V

    iget-object v6, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackSubmittedEntry;->a:Ljava/lang/String;

    .line 209
    invoke-virtual {v5, v6}, Lcom/twitter/model/dms/h$c$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/h$c$a;

    move-result-object v5

    iget-object v6, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackSubmittedEntry;->b:Ljava/lang/Integer;

    .line 210
    invoke-virtual {v5, v6}, Lcom/twitter/model/dms/h$c$a;->a(Ljava/lang/Integer;)Lcom/twitter/model/dms/h$c$a;

    move-result-object v5

    .line 211
    invoke-virtual {v5}, Lcom/twitter/model/dms/h$c$a;->q()Ljava/lang/Object;

    move-result-object v5

    .line 208
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/h$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/h$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackSubmittedEntry;->c:Ljava/lang/String;

    .line 212
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/h$a;->b(Ljava/lang/String;)Lcom/twitter/model/dms/h$a;

    move-result-object v1

    iget-boolean v5, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackSubmittedEntry;->d:Z

    .line 213
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/h$a;->a(Z)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/h$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackSubmittedEntry;->e:J

    .line 214
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/h$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/h$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackSubmittedEntry;->f:Ljava/lang/String;

    .line 215
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/h$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/h$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackSubmittedEntry;->g:J

    .line 216
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/h$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/h$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackSubmittedEntry;->h:J

    .line 217
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/h$a;->a(J)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/h$a;

    .line 218
    invoke-virtual {v0}, Lcom/twitter/model/dms/h$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    :goto_5
    move-object v1, v0

    move-object v0, v2

    .line 220
    goto/16 :goto_1

    :cond_11
    const-string/jumbo v0, "cs_feedback_dismissed"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    if-eqz v4, :cond_12

    .line 221
    const-class v0, Lcom/twitter/model/json/dms/JsonCSFeedbackDismissedEntry;

    .line 222
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/dms/JsonCSFeedbackDismissedEntry;

    .line 223
    if-eqz v0, :cond_14

    .line 224
    new-instance v1, Lcom/twitter/model/dms/g$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/g$a;-><init>()V

    new-instance v5, Lcom/twitter/model/dms/g$c$a;

    invoke-direct {v5}, Lcom/twitter/model/dms/g$c$a;-><init>()V

    iget-object v6, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackDismissedEntry;->a:Ljava/lang/String;

    .line 226
    invoke-virtual {v5, v6}, Lcom/twitter/model/dms/g$c$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/g$c$a;

    move-result-object v5

    .line 227
    invoke-virtual {v5}, Lcom/twitter/model/dms/g$c$a;->q()Ljava/lang/Object;

    move-result-object v5

    .line 225
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/g$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/g$a;

    iget-boolean v5, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackDismissedEntry;->d:Z

    .line 228
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/g$a;->a(Z)Lcom/twitter/model/dms/c$b;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/g$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackDismissedEntry;->e:J

    .line 229
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/g$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/g$a;

    iget-object v5, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackDismissedEntry;->f:Ljava/lang/String;

    .line 230
    invoke-virtual {v1, v5}, Lcom/twitter/model/dms/g$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/g$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackDismissedEntry;->g:J

    .line 231
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/g$a;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/g$a;

    iget-wide v6, v0, Lcom/twitter/model/json/dms/JsonCSFeedbackDismissedEntry;->h:J

    .line 232
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/dms/g$a;->a(J)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/g$a;

    .line 233
    invoke-virtual {v0}, Lcom/twitter/model/dms/g$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    :goto_6
    move-object v1, v0

    move-object v0, v2

    .line 235
    goto/16 :goto_1

    .line 236
    :cond_12
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v2

    .line 238
    goto/16 :goto_1

    .line 241
    :pswitch_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 245
    :pswitch_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v2

    .line 246
    goto/16 :goto_1

    .line 255
    :cond_13
    return-object v1

    :cond_14
    move-object v0, v1

    goto :goto_6

    :cond_15
    move-object v0, v1

    goto :goto_5

    :cond_16
    move-object v0, v1

    goto/16 :goto_4

    :cond_17
    move-object v0, v1

    goto/16 :goto_3

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/dms/e;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/dms/d;

    move-result-object v0

    return-object v0
.end method
