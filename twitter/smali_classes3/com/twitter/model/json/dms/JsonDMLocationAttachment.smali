.class public Lcom/twitter/model/json/dms/JsonDMLocationAttachment;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;,
        Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;,
        Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcby;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcby;
    .locals 2

    .prologue
    .line 43
    const-string/jumbo v0, "shared_place"

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    new-instance v0, Lcce$a;

    invoke-direct {v0}, Lcce$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment;->b:Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;

    iget-object v1, v1, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;->a:Lcom/twitter/model/geo/TwitterPlace;

    .line 45
    invoke-virtual {v0, v1}, Lcce$a;->a(Lcom/twitter/model/geo/TwitterPlace;)Lcce$a;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcce$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcby;

    .line 53
    :goto_0
    return-object v0

    .line 47
    :cond_0
    const-string/jumbo v0, "shared_coordinate"

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    new-instance v0, Lccd$a;

    invoke-direct {v0}, Lccd$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment;->c:Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;

    iget-object v1, v1, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;->a:Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;

    .line 49
    invoke-virtual {v1}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;->a()Lcom/twitter/model/geo/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lccd$a;->a(Lcom/twitter/model/geo/b;)Lccd$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment;->c:Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;

    .line 50
    invoke-virtual {v1}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lccd$a;->a(Ljava/util/List;)Lccd$a;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lccd$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcby;

    goto :goto_0

    .line 53
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment;->a()Lcby;

    move-result-object v0

    return-object v0
.end method
