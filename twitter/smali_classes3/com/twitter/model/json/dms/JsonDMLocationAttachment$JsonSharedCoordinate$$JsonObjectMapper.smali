.class public final Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate$$JsonObjectMapper;
.super Lcom/bluelinelabs/logansquare/JsonMapper;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bluelinelabs/logansquare/JsonMapper",
        "<",
        "Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/bluelinelabs/logansquare/JsonMapper;-><init>()V

    return-void
.end method

.method public static _parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    new-instance v0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;

    invoke-direct {v0}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;-><init>()V

    .line 21
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    if-nez v1, :cond_0

    .line 22
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 24
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_2

    .line 25
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 26
    const/4 v0, 0x0

    .line 34
    :cond_1
    return-object v0

    .line 28
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 29
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    .line 30
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 31
    invoke-static {v0, v1, p0}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate$$JsonObjectMapper;->parseField(Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;Ljava/lang/String;Lcom/fasterxml/jackson/core/JsonParser;)V

    .line 32
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_0
.end method

.method public static _serialize(Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 64
    if-eqz p2, :cond_0

    .line 65
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;->a:Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;

    if-eqz v0, :cond_1

    .line 68
    const-string/jumbo v0, "coordinates"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;->a:Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;

    invoke-static {v0, p1, v4}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 71
    :cond_1
    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;->b:[Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;

    .line 72
    if-eqz v1, :cond_4

    .line 73
    const-string/jumbo v0, "nearby_places"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a()V

    .line 75
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 76
    if-eqz v3, :cond_2

    .line 77
    invoke-static {v3, p1, v4}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 75
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_3
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    .line 82
    :cond_4
    if-eqz p2, :cond_5

    .line 83
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 85
    :cond_5
    return-void
.end method

.method public static parseField(Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;Ljava/lang/String;Lcom/fasterxml/jackson/core/JsonParser;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    const-string/jumbo v0, "coordinates"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 39
    invoke-static {p2}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;->a:Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 40
    :cond_1
    const-string/jumbo v0, "nearby_places"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_4

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 43
    :cond_2
    :goto_1
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_3

    .line 45
    invoke-static {p2}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;

    move-result-object v1

    .line 46
    if-eqz v1, :cond_2

    .line 47
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 50
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;

    .line 51
    iput-object v0, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;->b:[Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;

    goto :goto_0

    .line 53
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;->b:[Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;

    goto :goto_0
.end method


# virtual methods
.method public parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-static {p1}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate$$JsonObjectMapper;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;

    move-result-object v0

    return-object v0
.end method

.method public serialize(Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {p1, p2, p3}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 61
    return-void
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13
    check-cast p1, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate$$JsonObjectMapper;->serialize(Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    return-void
.end method
