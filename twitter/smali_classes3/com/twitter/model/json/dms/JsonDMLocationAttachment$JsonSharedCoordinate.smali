.class public Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;
.super Lcom/twitter/model/json/common/a;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/json/dms/JsonDMLocationAttachment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "JsonSharedCoordinate"
.end annotation


# instance fields
.field public a:Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:[Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/twitter/model/json/common/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;->b:[Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;

    if-eqz v0, :cond_1

    .line 74
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 75
    iget-object v2, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonSharedCoordinate;->b:[Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 76
    iget-object v4, v4, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonTwitterPlaceContainer;->a:Lcom/twitter/model/geo/TwitterPlace;

    invoke-virtual {v1, v4}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 80
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
