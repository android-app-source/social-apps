.class public Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyConfig;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcck;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lccp;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lccs;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Lccm;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcck;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 38
    iget-object v2, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyConfig;->a:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 69
    :cond_1
    :goto_1
    return-object v0

    .line 38
    :sswitch_0
    const-string/jumbo v3, "options"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v3, "text_input"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v3, "location"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string/jumbo v3, "encrypted_text_input"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string/jumbo v3, "unknown"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    .line 40
    :pswitch_0
    new-instance v0, Lccr$a;

    invoke-direct {v0}, Lccr$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyConfig;->b:Ljava/lang/String;

    .line 41
    invoke-virtual {v0, v1}, Lccr$a;->a(Ljava/lang/String;)Lcck$a;

    move-result-object v0

    check-cast v0, Lccr$a;

    iget-object v1, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyConfig;->c:Ljava/util/List;

    .line 42
    invoke-virtual {v0, v1}, Lccr$a;->a(Ljava/util/List;)Lccr$a;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lccr$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcck;

    goto :goto_1

    .line 46
    :pswitch_1
    new-instance v0, Lccu$a;

    invoke-direct {v0}, Lccu$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyConfig;->b:Ljava/lang/String;

    .line 47
    invoke-virtual {v0, v1}, Lccu$a;->a(Ljava/lang/String;)Lcck$a;

    move-result-object v0

    check-cast v0, Lccu$a;

    iget-object v1, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyConfig;->d:Lccs;

    .line 48
    invoke-virtual {v0, v1}, Lccu$a;->a(Lccs;)Lccu$a;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lccu$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcck;

    goto :goto_1

    .line 52
    :pswitch_2
    new-instance v0, Lcco$a;

    invoke-direct {v0}, Lcco$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyConfig;->b:Ljava/lang/String;

    .line 53
    invoke-virtual {v0, v1}, Lcco$a;->a(Ljava/lang/String;)Lcck$a;

    move-result-object v0

    check-cast v0, Lcco$a;

    .line 54
    invoke-virtual {v0}, Lcco$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcck;

    goto :goto_1

    .line 57
    :pswitch_3
    const-string/jumbo v1, "dm_quick_reply_encrypted_text_input_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59
    new-instance v0, Lccn$a;

    invoke-direct {v0}, Lccn$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyConfig;->b:Ljava/lang/String;

    .line 60
    invoke-virtual {v0, v1}, Lccn$a;->a(Ljava/lang/String;)Lcck$a;

    move-result-object v0

    check-cast v0, Lccn$a;

    iget-object v1, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyConfig;->e:Lccm;

    .line 61
    invoke-virtual {v0, v1}, Lccn$a;->a(Lccm;)Lccn$a;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lccn$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcck;

    goto/16 :goto_1

    .line 38
    :sswitch_data_0
    .sparse-switch
        -0x7dc155c8 -> :sswitch_1
        -0x4a797962 -> :sswitch_0
        -0x293e3c6d -> :sswitch_3
        -0x10fa53b6 -> :sswitch_4
        0x714f9fb5 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyConfig;->a()Lcck;

    move-result-object v0

    return-object v0
.end method
