.class public Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyTextInput;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lccs;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lccs$c;
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lccs$c;

    invoke-direct {v0}, Lccs$c;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyTextInput;->a:Ljava/lang/String;

    .line 22
    invoke-virtual {v0, v1}, Lccs$c;->b(Ljava/lang/String;)Lccs$a;

    move-result-object v0

    check-cast v0, Lccs$c;

    iget-object v1, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyTextInput;->b:Ljava/lang/String;

    .line 23
    invoke-virtual {v0, v1}, Lccs$c;->c(Ljava/lang/String;)Lccs$a;

    move-result-object v0

    check-cast v0, Lccs$c;

    .line 21
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyTextInput;->a()Lccs$c;

    move-result-object v0

    return-object v0
.end method
