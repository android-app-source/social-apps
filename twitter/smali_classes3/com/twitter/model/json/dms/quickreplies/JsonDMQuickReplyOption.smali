.class public Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyOption;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lccp;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "label"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lccp$a;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lccp$a;

    invoke-direct {v0}, Lccp$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyOption;->a:Ljava/lang/String;

    .line 24
    invoke-virtual {v0, v1}, Lccp$a;->a(Ljava/lang/String;)Lccp$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyOption;->b:Ljava/lang/String;

    .line 25
    invoke-virtual {v0, v1}, Lccp$a;->b(Ljava/lang/String;)Lccp$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyOption;->c:Ljava/lang/String;

    .line 26
    invoke-virtual {v0, v1}, Lccp$a;->c(Ljava/lang/String;)Lccp$a;

    move-result-object v0

    .line 23
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/dms/quickreplies/JsonDMQuickReplyOption;->a()Lccp$a;

    move-result-object v0

    return-object v0
.end method
