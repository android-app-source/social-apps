.class public Lcom/twitter/model/json/dms/d;
.super Lcom/twitter/model/json/common/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/h",
        "<",
        "Lcom/twitter/model/dms/k;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/model/json/common/h;-><init>()V

    return-void
.end method

.method private a(Lcom/fasterxml/jackson/core/JsonParser;I)Lcom/twitter/model/dms/k;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    const-string/jumbo v2, "dm_agent_personalization_enabled"

    .line 76
    invoke-static {v2}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v10

    .line 78
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v11

    .line 79
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v12

    .line 80
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v13

    .line 81
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v14

    .line 82
    const/4 v7, 0x0

    .line 83
    const-wide/16 v8, -0x1

    .line 84
    const/4 v6, 0x0

    .line 85
    const-wide/16 v4, -0x1

    .line 87
    const/4 v3, 0x0

    .line 88
    invoke-virtual/range {p1 .. p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object/from16 v16, v2

    move-object v2, v3

    move-object/from16 v3, v16

    .line 89
    :goto_0
    if-eqz v3, :cond_9

    sget-object v15, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v15, :cond_9

    .line 90
    sget-object v15, Lcom/twitter/model/json/dms/d$1;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v15, v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    move-object v3, v6

    move-wide/from16 v16, v8

    move v8, v7

    move-wide/from16 v6, v16

    .line 132
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v9

    move-object/from16 v16, v9

    move-object/from16 v17, v3

    move-object/from16 v3, v16

    move-wide/from16 v18, v6

    move-object/from16 v6, v17

    move v7, v8

    move-wide/from16 v8, v18

    goto :goto_0

    .line 92
    :pswitch_0
    const-string/jumbo v3, "users"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 93
    const-class v3, Lcom/twitter/model/core/TwitterUser;

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/twitter/model/json/common/e;->f(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Iterable;

    move-result-object v3

    invoke-virtual {v12, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-object v3, v6

    move-wide/from16 v16, v8

    move v8, v7

    move-wide/from16 v6, v16

    goto :goto_1

    .line 94
    :cond_1
    const-string/jumbo v3, "conversations"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 95
    const-class v3, Lcom/twitter/model/dms/l;

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/twitter/model/json/common/e;->f(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Iterable;

    move-result-object v3

    invoke-virtual {v13, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-object v3, v6

    move-wide/from16 v16, v8

    move v8, v7

    move-wide/from16 v6, v16

    goto :goto_1

    .line 96
    :cond_2
    if-eqz v10, :cond_3

    const-string/jumbo v3, "custom_profiles"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 97
    const-class v3, Lcom/twitter/model/dms/o;

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/twitter/model/json/common/e;->f(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Iterable;

    move-result-object v3

    invoke-virtual {v14, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-object v3, v6

    move-wide/from16 v16, v8

    move v8, v7

    move-wide/from16 v6, v16

    goto :goto_1

    .line 99
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v3, v6

    move-wide/from16 v16, v8

    move v8, v7

    move-wide/from16 v6, v16

    .line 101
    goto :goto_1

    .line 105
    :pswitch_1
    const-string/jumbo v3, "status"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 106
    invoke-static/range {p1 .. p1}, Lcom/twitter/model/json/dms/d;->b(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v3

    move-object/from16 v16, v6

    move-wide v6, v8

    move v8, v3

    move-object/from16 v3, v16

    goto/16 :goto_1

    .line 107
    :cond_4
    const-string/jumbo v3, "min_entry_id"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 108
    invoke-virtual/range {p1 .. p1}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v8

    move-object v3, v6

    move-wide/from16 v16, v8

    move v8, v7

    move-wide/from16 v6, v16

    goto/16 :goto_1

    .line 109
    :cond_5
    const-string/jumbo v3, "cursor"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 110
    invoke-virtual/range {p1 .. p1}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v3

    move-wide/from16 v16, v8

    move v8, v7

    move-wide/from16 v6, v16

    goto/16 :goto_1

    .line 111
    :cond_6
    const-string/jumbo v3, "last_seen_event_id"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 112
    invoke-virtual/range {p1 .. p1}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v4

    move-object v3, v6

    move-wide/from16 v16, v8

    move v8, v7

    move-wide/from16 v6, v16

    goto/16 :goto_1

    .line 117
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move-object v3, v6

    move-wide/from16 v16, v8

    move v8, v7

    move-wide/from16 v6, v16

    .line 118
    goto/16 :goto_1

    .line 121
    :pswitch_3
    const-string/jumbo v3, "entries"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, "requests"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 122
    :cond_7
    new-instance v3, Lcom/twitter/model/json/dms/f;

    invoke-direct {v3}, Lcom/twitter/model/json/dms/f;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/twitter/model/json/dms/f;->a(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-object v3, v6

    move-wide/from16 v16, v8

    move v8, v7

    move-wide/from16 v6, v16

    goto/16 :goto_1

    .line 124
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v3, v6

    move-wide/from16 v16, v8

    move v8, v7

    move-wide/from16 v6, v16

    .line 126
    goto/16 :goto_1

    .line 135
    :cond_9
    new-instance v2, Lcom/twitter/model/dms/k$a;

    invoke-direct {v2}, Lcom/twitter/model/dms/k$a;-><init>()V

    .line 136
    move/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/twitter/model/dms/k$a;->a(I)Lcom/twitter/model/dms/k$a;

    move-result-object v3

    .line 137
    invoke-virtual {v11}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual {v3, v2}, Lcom/twitter/model/dms/k$a;->c(Ljava/util/List;)Lcom/twitter/model/dms/p$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/dms/k$a;

    .line 138
    invoke-virtual {v12}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/twitter/model/dms/k$a;->d(Ljava/util/List;)Lcom/twitter/model/dms/p$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/dms/k$a;

    .line 139
    invoke-virtual {v13}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/twitter/model/dms/k$a;->a(Ljava/util/List;)Lcom/twitter/model/dms/k$a;

    move-result-object v3

    .line 140
    invoke-virtual {v14}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual {v3, v2}, Lcom/twitter/model/dms/k$a;->b(Ljava/util/List;)Lcom/twitter/model/dms/k$a;

    move-result-object v2

    .line 141
    invoke-virtual {v2, v7}, Lcom/twitter/model/dms/k$a;->b(I)Lcom/twitter/model/dms/k$a;

    move-result-object v2

    .line 142
    invoke-virtual {v2, v8, v9}, Lcom/twitter/model/dms/k$a;->b(J)Lcom/twitter/model/dms/k$a;

    move-result-object v2

    .line 143
    invoke-virtual {v2, v6}, Lcom/twitter/model/dms/k$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/k$a;

    move-result-object v2

    .line 144
    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/dms/k$a;->a(J)Lcom/twitter/model/dms/k$a;

    move-result-object v2

    .line 145
    invoke-virtual {v2}, Lcom/twitter/model/dms/k$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/dms/k;

    .line 135
    return-object v2

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    new-instance v0, Lcom/twitter/model/json/dms/i;

    invoke-direct {v0}, Lcom/twitter/model/json/dms/i;-><init>()V

    .line 151
    invoke-virtual {v0, p0}, Lcom/twitter/model/json/dms/i;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 152
    return v0
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/dms/k;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 41
    .line 42
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v1, v2

    .line 43
    :goto_0
    if-eqz v0, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_1

    .line 44
    sget-object v3, Lcom/twitter/model/json/dms/d$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 65
    :goto_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0

    .line 46
    :pswitch_0
    new-instance v0, Lcom/twitter/model/json/dms/k;

    invoke-direct {v0}, Lcom/twitter/model/json/dms/k;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/model/json/dms/k;->getFromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 47
    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 48
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    .line 49
    goto :goto_1

    .line 51
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/twitter/model/json/dms/d;->a(Lcom/fasterxml/jackson/core/JsonParser;I)Lcom/twitter/model/dms/k;

    move-result-object v0

    .line 69
    :goto_2
    return-object v0

    .line 54
    :pswitch_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 58
    :pswitch_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    .line 59
    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 69
    goto :goto_2

    .line 44
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/dms/d;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/dms/k;

    move-result-object v0

    return-object v0
.end method
