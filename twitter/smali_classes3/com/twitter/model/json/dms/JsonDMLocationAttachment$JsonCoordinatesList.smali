.class public Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;
.super Lcom/twitter/model/json/common/a;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/json/dms/JsonDMLocationAttachment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "JsonCoordinatesList"
.end annotation


# instance fields
.field public a:[D
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/twitter/model/json/common/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/geo/b;
    .locals 6

    .prologue
    .line 94
    const-string/jumbo v0, "Point"

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 95
    iget-object v0, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;->a:[D

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;->a:[D

    array-length v0, v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/twitter/model/geo/b;

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;->a:[D

    const/4 v2, 0x1

    aget-wide v2, v1, v2

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonDMLocationAttachment$JsonCoordinatesList;->a:[D

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/twitter/model/geo/b;-><init>(DD)V

    goto :goto_0
.end method
