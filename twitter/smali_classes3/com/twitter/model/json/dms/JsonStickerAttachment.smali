.class public Lcom/twitter/model/json/dms/JsonStickerAttachment;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcch;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:[I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    .line 22
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/twitter/model/json/dms/JsonStickerAttachment;->e:[I

    return-void

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method


# virtual methods
.method public a()Lcch$a;
    .locals 4

    .prologue
    .line 28
    new-instance v0, Lcch$a;

    invoke-direct {v0}, Lcch$a;-><init>()V

    iget-wide v2, p0, Lcom/twitter/model/json/dms/JsonStickerAttachment;->a:J

    .line 29
    invoke-virtual {v0, v2, v3}, Lcch$a;->a(J)Lcch$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonStickerAttachment;->d:Ljava/lang/String;

    .line 30
    invoke-virtual {v0, v1}, Lcch$a;->a(Ljava/lang/String;)Lcbx$a;

    move-result-object v0

    check-cast v0, Lcch$a;

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonStickerAttachment;->b:Ljava/lang/String;

    .line 31
    invoke-virtual {v0, v1}, Lcch$a;->b(Ljava/lang/String;)Lcbx$a;

    move-result-object v0

    check-cast v0, Lcch$a;

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonStickerAttachment;->c:Ljava/lang/String;

    .line 32
    invoke-virtual {v0, v1}, Lcch$a;->c(Ljava/lang/String;)Lcbx$a;

    move-result-object v0

    check-cast v0, Lcch$a;

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonStickerAttachment;->e:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    .line 33
    invoke-virtual {v0, v1}, Lcch$a;->a(I)Lcbx$a;

    move-result-object v0

    check-cast v0, Lcch$a;

    iget-object v1, p0, Lcom/twitter/model/json/dms/JsonStickerAttachment;->e:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 34
    invoke-virtual {v0, v1}, Lcch$a;->b(I)Lcbx$a;

    move-result-object v0

    check-cast v0, Lcch$a;

    .line 28
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/dms/JsonStickerAttachment;->a()Lcch$a;

    move-result-object v0

    return-object v0
.end method
