.class public final Lcom/twitter/model/json/account/JsonUserSettings$$JsonObjectMapper;
.super Lcom/bluelinelabs/logansquare/JsonMapper;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bluelinelabs/logansquare/JsonMapper",
        "<",
        "Lcom/twitter/model/json/account/JsonUserSettings;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/bluelinelabs/logansquare/JsonMapper;-><init>()V

    return-void
.end method

.method public static _parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/account/JsonUserSettings;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/model/json/account/JsonUserSettings;

    invoke-direct {v0}, Lcom/twitter/model/json/account/JsonUserSettings;-><init>()V

    .line 23
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    if-nez v1, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 26
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_2

    .line 27
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 28
    const/4 v0, 0x0

    .line 36
    :cond_1
    return-object v0

    .line 30
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 31
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    .line 32
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 33
    invoke-static {v0, v1, p0}, Lcom/twitter/model/json/account/JsonUserSettings$$JsonObjectMapper;->parseField(Lcom/twitter/model/json/account/JsonUserSettings;Ljava/lang/String;Lcom/fasterxml/jackson/core/JsonParser;)V

    .line 34
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_0
.end method

.method public static _serialize(Lcom/twitter/model/json/account/JsonUserSettings;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    if-eqz p2, :cond_0

    .line 112
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 114
    :cond_0
    const-string/jumbo v0, "address_book_live_sync_enabled"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->q:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 115
    const-string/jumbo v0, "allow_ads_personalization"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 116
    const-string/jumbo v0, "allow_authenticated_periscope_requests"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->y:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 117
    const-string/jumbo v0, "smart_mute"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->n:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 118
    const-string/jumbo v0, "allow_dms_from"

    iget-object v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string/jumbo v0, "allow_media_tagging"

    iget-object v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string/jumbo v0, "alt_text_compose_enabled"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->t:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 121
    const-string/jumbo v0, "country_code"

    iget-object v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string/jumbo v0, "discoverable_by_email"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 123
    const-string/jumbo v0, "discoverable_by_mobile_phone"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 124
    const-string/jumbo v0, "display_sensitive_media"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 125
    const-string/jumbo v0, "dm_receipt_setting"

    iget-object v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string/jumbo v0, "email_follow_enabled"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->k:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 127
    const-string/jumbo v0, "geo_enabled"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 128
    const-string/jumbo v0, "language"

    iget-object v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string/jumbo v0, "mention_filter"

    iget-object v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->v:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string/jumbo v0, "personalized_trends"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 131
    const-string/jumbo v0, "protected"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 132
    const-string/jumbo v0, "ranked_timeline_eligible"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->p:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 133
    const-string/jumbo v0, "ranked_timeline_setting"

    iget v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 134
    const-string/jumbo v0, "screen_name"

    iget-object v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->x:Lcom/twitter/model/account/UserSettings$b;

    if-eqz v0, :cond_1

    .line 136
    const-class v0, Lcom/twitter/model/account/UserSettings$b;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->x:Lcom/twitter/model/account/UserSettings$b;

    const-string/jumbo v2, "sleep_time"

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->w:Ljava/util/List;

    .line 139
    if-eqz v0, :cond_4

    .line 140
    const-string/jumbo v1, "trend_location"

    invoke-virtual {p1, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a()V

    .line 142
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserSettings$c;

    .line 143
    if-eqz v0, :cond_2

    .line 144
    const-class v2, Lcom/twitter/model/account/UserSettings$c;

    invoke-static {v2}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v2

    const-string/jumbo v3, "lslocaltrend_locationElement"

    const/4 v4, 0x0

    invoke-interface {v2, v0, v3, v4, p1}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->serialize(Ljava/lang/Object;Ljava/lang/String;ZLcom/fasterxml/jackson/core/JsonGenerator;)V

    goto :goto_0

    .line 147
    :cond_3
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    .line 149
    :cond_4
    const-string/jumbo v0, "universal_quality_filtering_enabled"

    iget-object v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string/jumbo v0, "use_cookie_personalization"

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 151
    if-eqz p2, :cond_5

    .line 152
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 154
    :cond_5
    return-void
.end method

.method public static parseField(Lcom/twitter/model/json/account/JsonUserSettings;Ljava/lang/String;Lcom/fasterxml/jackson/core/JsonParser;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 40
    const-string/jumbo v0, "address_book_live_sync_enabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->q:Z

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    const-string/jumbo v0, "allow_ads_personalization"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 43
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->l:Z

    goto :goto_0

    .line 44
    :cond_2
    const-string/jumbo v0, "allow_authenticated_periscope_requests"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 45
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->y:Z

    goto :goto_0

    .line 46
    :cond_3
    const-string/jumbo v0, "smart_mute"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 47
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->n:Z

    goto :goto_0

    .line 48
    :cond_4
    const-string/jumbo v0, "allow_dms_from"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 49
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->m:Ljava/lang/String;

    goto :goto_0

    .line 50
    :cond_5
    const-string/jumbo v0, "allow_media_tagging"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 51
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->j:Ljava/lang/String;

    goto :goto_0

    .line 52
    :cond_6
    const-string/jumbo v0, "alt_text_compose_enabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 53
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->t:Z

    goto :goto_0

    .line 54
    :cond_7
    const-string/jumbo v0, "country_code"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 55
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->r:Ljava/lang/String;

    goto :goto_0

    .line 56
    :cond_8
    const-string/jumbo v0, "discoverable_by_email"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 57
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->f:Z

    goto :goto_0

    .line 58
    :cond_9
    const-string/jumbo v0, "discoverable_by_mobile_phone"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 59
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->h:Z

    goto/16 :goto_0

    .line 60
    :cond_a
    const-string/jumbo v0, "display_sensitive_media"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 61
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->g:Z

    goto/16 :goto_0

    .line 62
    :cond_b
    const-string/jumbo v0, "dm_receipt_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 63
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 64
    :cond_c
    const-string/jumbo v0, "email_follow_enabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 65
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->k:Z

    goto/16 :goto_0

    .line 66
    :cond_d
    const-string/jumbo v0, "geo_enabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 67
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->a:Z

    goto/16 :goto_0

    .line 68
    :cond_e
    const-string/jumbo v0, "language"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 69
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 70
    :cond_f
    const-string/jumbo v0, "mention_filter"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 71
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->v:Ljava/lang/String;

    goto/16 :goto_0

    .line 72
    :cond_10
    const-string/jumbo v0, "personalized_trends"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 73
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->c:Z

    goto/16 :goto_0

    .line 74
    :cond_11
    const-string/jumbo v0, "protected"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 75
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->e:Z

    goto/16 :goto_0

    .line 76
    :cond_12
    const-string/jumbo v0, "ranked_timeline_eligible"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 77
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->p:Z

    goto/16 :goto_0

    .line 78
    :cond_13
    const-string/jumbo v0, "ranked_timeline_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 79
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->o:I

    goto/16 :goto_0

    .line 80
    :cond_14
    const-string/jumbo v0, "screen_name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 81
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 82
    :cond_15
    const-string/jumbo v0, "sleep_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 83
    const-class v0, Lcom/twitter/model/account/UserSettings$b;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserSettings$b;

    iput-object v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->x:Lcom/twitter/model/account/UserSettings$b;

    goto/16 :goto_0

    .line 84
    :cond_16
    const-string/jumbo v0, "trend_location"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 85
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_19

    .line 86
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 87
    :cond_17
    :goto_1
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_18

    .line 89
    const-class v0, Lcom/twitter/model/account/UserSettings$c;

    invoke-static {v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->typeConverterFor(Ljava/lang/Class;)Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserSettings$c;

    .line 90
    if-eqz v0, :cond_17

    .line 91
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 94
    :cond_18
    iput-object v1, p0, Lcom/twitter/model/json/account/JsonUserSettings;->w:Ljava/util/List;

    goto/16 :goto_0

    .line 96
    :cond_19
    iput-object v2, p0, Lcom/twitter/model/json/account/JsonUserSettings;->w:Ljava/util/List;

    goto/16 :goto_0

    .line 98
    :cond_1a
    const-string/jumbo v0, "universal_quality_filtering_enabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 99
    invoke-virtual {p2, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->u:Ljava/lang/String;

    goto/16 :goto_0

    .line 100
    :cond_1b
    const-string/jumbo v0, "use_cookie_personalization"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/json/account/JsonUserSettings;->b:Z

    goto/16 :goto_0
.end method


# virtual methods
.method public parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/account/JsonUserSettings;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-static {p1}, Lcom/twitter/model/json/account/JsonUserSettings$$JsonObjectMapper;->_parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/account/JsonUserSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/account/JsonUserSettings$$JsonObjectMapper;->parse(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/json/account/JsonUserSettings;

    move-result-object v0

    return-object v0
.end method

.method public serialize(Lcom/twitter/model/json/account/JsonUserSettings;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-static {p1, p2, p3}, Lcom/twitter/model/json/account/JsonUserSettings$$JsonObjectMapper;->_serialize(Lcom/twitter/model/json/account/JsonUserSettings;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    .line 108
    return-void
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    check-cast p1, Lcom/twitter/model/json/account/JsonUserSettings;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/json/account/JsonUserSettings$$JsonObjectMapper;->serialize(Lcom/twitter/model/json/account/JsonUserSettings;Lcom/fasterxml/jackson/core/JsonGenerator;Z)V

    return-void
.end method
