.class public Lcom/twitter/model/json/account/JsonAvailability;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/account/a;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/account/a;
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/model/account/a;

    iget-boolean v1, p0, Lcom/twitter/model/json/account/JsonAvailability;->a:Z

    iget-object v2, p0, Lcom/twitter/model/json/account/JsonAvailability;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/account/a;-><init>(ZLjava/lang/String;)V

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/account/JsonAvailability;->a()Lcom/twitter/model/account/a;

    move-result-object v0

    return-object v0
.end method
