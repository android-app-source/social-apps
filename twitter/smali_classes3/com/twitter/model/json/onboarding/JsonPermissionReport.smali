.class public Lcom/twitter/model/json/onboarding/JsonPermissionReport;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/onboarding/permission/a;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public h:Lcom/twitter/model/onboarding/permission/SystemPermissionState;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public i:Lcom/twitter/model/onboarding/permission/InAppPermissionState;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 78
    invoke-static {p0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 81
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/twitter/model/onboarding/permission/a;
    .locals 4

    .prologue
    .line 50
    new-instance v0, Lcom/twitter/model/onboarding/permission/a$a;

    iget-object v1, p0, Lcom/twitter/model/json/onboarding/JsonPermissionReport;->h:Lcom/twitter/model/onboarding/permission/SystemPermissionState;

    iget-object v2, p0, Lcom/twitter/model/json/onboarding/JsonPermissionReport;->i:Lcom/twitter/model/onboarding/permission/InAppPermissionState;

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/onboarding/permission/a$a;-><init>(Lcom/twitter/model/onboarding/permission/SystemPermissionState;Lcom/twitter/model/onboarding/permission/InAppPermissionState;)V

    iget-object v1, p0, Lcom/twitter/model/json/onboarding/JsonPermissionReport;->a:Ljava/lang/String;

    .line 51
    invoke-static {v1}, Lcom/twitter/model/json/onboarding/JsonPermissionReport;->a(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/onboarding/permission/a$a;->a(J)Lcom/twitter/model/onboarding/permission/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/onboarding/JsonPermissionReport;->b:Ljava/lang/String;

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/model/onboarding/permission/a$a;->a(Ljava/lang/String;)Lcom/twitter/model/onboarding/permission/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/onboarding/JsonPermissionReport;->c:Ljava/lang/String;

    .line 53
    invoke-virtual {v0, v1}, Lcom/twitter/model/onboarding/permission/a$a;->b(Ljava/lang/String;)Lcom/twitter/model/onboarding/permission/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/onboarding/JsonPermissionReport;->d:Ljava/lang/String;

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/model/onboarding/permission/a$a;->c(Ljava/lang/String;)Lcom/twitter/model/onboarding/permission/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/onboarding/JsonPermissionReport;->e:Ljava/lang/String;

    .line 55
    invoke-virtual {v0, v1}, Lcom/twitter/model/onboarding/permission/a$a;->d(Ljava/lang/String;)Lcom/twitter/model/onboarding/permission/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/onboarding/JsonPermissionReport;->f:Ljava/lang/String;

    .line 56
    invoke-virtual {v0, v1}, Lcom/twitter/model/onboarding/permission/a$a;->e(Ljava/lang/String;)Lcom/twitter/model/onboarding/permission/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/onboarding/JsonPermissionReport;->g:Ljava/lang/String;

    .line 57
    invoke-static {v1}, Lcom/twitter/model/json/onboarding/JsonPermissionReport;->a(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/onboarding/permission/a$a;->b(J)Lcom/twitter/model/onboarding/permission/a$a;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/twitter/model/onboarding/permission/a$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/permission/a;

    .line 50
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/model/json/onboarding/JsonPermissionReport;->a()Lcom/twitter/model/onboarding/permission/a;

    move-result-object v0

    return-object v0
.end method
