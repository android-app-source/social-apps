.class public Lcom/twitter/model/json/onboarding/JsonNavigationLink;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/onboarding/NavigationLink;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        typeConverter = Lcom/twitter/model/json/onboarding/b;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/onboarding/NavigationLink;
    .locals 3

    .prologue
    .line 27
    iget v0, p0, Lcom/twitter/model/json/onboarding/JsonNavigationLink;->a:I

    iget-object v1, p0, Lcom/twitter/model/json/onboarding/JsonNavigationLink;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/json/onboarding/JsonNavigationLink;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/model/onboarding/c;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/model/onboarding/NavigationLink;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/model/json/onboarding/JsonNavigationLink;->a()Lcom/twitter/model/onboarding/NavigationLink;

    move-result-object v0

    return-object v0
.end method
