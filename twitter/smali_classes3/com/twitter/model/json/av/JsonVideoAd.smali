.class public Lcom/twitter/model/json/av/JsonVideoAd;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/av/e;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcgi;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Lcom/twitter/model/av/DynamicAdMediaInfo;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/av/e;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-wide v2, p0, Lcom/twitter/model/json/av/JsonVideoAd;->a:J

    iget-object v0, p0, Lcom/twitter/model/json/av/JsonVideoAd;->b:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/twitter/model/av/DynamicAdId;->b(JLjava/lang/String;)Lcom/twitter/model/av/DynamicAdId;

    move-result-object v2

    .line 35
    invoke-virtual {v2}, Lcom/twitter/model/av/DynamicAdId;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/json/av/JsonVideoAd;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 44
    :goto_0
    return-object v0

    .line 40
    :cond_1
    :try_start_0
    new-instance v0, Lcom/twitter/model/av/e;

    new-instance v3, Lcom/twitter/model/av/DynamicAd;

    iget-object v4, p0, Lcom/twitter/model/json/av/JsonVideoAd;->d:Ljava/lang/String;

    iget v5, p0, Lcom/twitter/model/json/av/JsonVideoAd;->e:I

    iget-object v6, p0, Lcom/twitter/model/json/av/JsonVideoAd;->c:Lcgi;

    iget-object v7, p0, Lcom/twitter/model/json/av/JsonVideoAd;->f:Lcom/twitter/model/av/DynamicAdMediaInfo;

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/twitter/model/av/DynamicAd;-><init>(Ljava/lang/String;ILcgi;Lcom/twitter/model/av/DynamicAdMediaInfo;)V

    invoke-direct {v0, v3, v2}, Lcom/twitter/model/av/e;-><init>(Lcom/twitter/model/av/DynamicAd;Lcom/twitter/model/av/DynamicAdId;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 44
    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/model/json/av/JsonVideoAd;->a()Lcom/twitter/model/av/e;

    move-result-object v0

    return-object v0
.end method
