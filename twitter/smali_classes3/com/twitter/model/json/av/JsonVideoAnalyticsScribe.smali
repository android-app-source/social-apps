.class public Lcom/twitter/model/json/av/JsonVideoAnalyticsScribe;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/av/d;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/av/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 24
    iget-wide v2, p0, Lcom/twitter/model/json/av/JsonVideoAnalyticsScribe;->a:J

    iget-object v0, p0, Lcom/twitter/model/json/av/JsonVideoAnalyticsScribe;->b:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/twitter/model/av/DynamicAdId;->b(JLjava/lang/String;)Lcom/twitter/model/av/DynamicAdId;

    move-result-object v2

    .line 26
    invoke-virtual {v2}, Lcom/twitter/model/av/DynamicAdId;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/json/av/JsonVideoAnalyticsScribe;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 33
    :goto_0
    return-object v0

    .line 31
    :cond_1
    :try_start_0
    new-instance v0, Lcom/twitter/model/av/d;

    iget-object v3, p0, Lcom/twitter/model/json/av/JsonVideoAnalyticsScribe;->c:Ljava/lang/String;

    invoke-direct {v0, v3, v2}, Lcom/twitter/model/av/d;-><init>(Ljava/lang/String;Lcom/twitter/model/av/DynamicAdId;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 32
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 33
    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/model/json/av/JsonVideoAnalyticsScribe;->a()Lcom/twitter/model/av/d;

    move-result-object v0

    return-object v0
.end method
