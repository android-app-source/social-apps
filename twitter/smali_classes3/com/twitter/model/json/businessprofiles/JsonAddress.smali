.class public Lcom/twitter/model/json/businessprofiles/JsonAddress;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcom/twitter/model/businessprofiles/a;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "address_line_1"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "address_line_2"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public h:Lcom/twitter/model/geo/b;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/businessprofiles/a$b;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/twitter/model/businessprofiles/a$b;

    invoke-direct {v0}, Lcom/twitter/model/businessprofiles/a$b;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/businessprofiles/JsonAddress;->a:Ljava/lang/String;

    .line 43
    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->a(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/businessprofiles/JsonAddress;->b:Ljava/lang/String;

    .line 44
    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->b(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/businessprofiles/JsonAddress;->c:Ljava/lang/String;

    .line 45
    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->c(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/businessprofiles/JsonAddress;->d:Ljava/lang/String;

    .line 46
    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->d(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/businessprofiles/JsonAddress;->e:Ljava/lang/String;

    .line 47
    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->e(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/businessprofiles/JsonAddress;->f:Ljava/lang/String;

    .line 48
    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->f(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/businessprofiles/JsonAddress;->g:Ljava/lang/String;

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->g(Ljava/lang/String;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/businessprofiles/JsonAddress;->h:Lcom/twitter/model/geo/b;

    .line 50
    invoke-virtual {v0, v1}, Lcom/twitter/model/businessprofiles/a$b;->a(Lcom/twitter/model/geo/b;)Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    .line 42
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/model/json/businessprofiles/JsonAddress;->a()Lcom/twitter/model/businessprofiles/a$b;

    move-result-object v0

    return-object v0
.end method
