.class public Lcom/twitter/model/json/notifications/JsonMobileSettingsResponse;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcfq;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/Map;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "_smsSettings"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcgb;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "_smsSettingsTemplate"
        }
    .end annotation
.end field

.field public c:Ljava/util/Map;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "_pushSettings"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcgb;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "_pushSettingsTemplate"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "_result"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcgc;",
            ">;"
        }
    .end annotation
.end field

.field public f:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "_code"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "_method"
        }
    .end annotation
.end field

.field public h:D
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "_checkinTimeDeltaHrs"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcfq$a;
    .locals 4

    .prologue
    .line 45
    new-instance v0, Lcfq$a;

    invoke-direct {v0}, Lcfq$a;-><init>()V

    iget-wide v2, p0, Lcom/twitter/model/json/notifications/JsonMobileSettingsResponse;->h:D

    .line 46
    invoke-virtual {v0, v2, v3}, Lcfq$a;->a(D)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonMobileSettingsResponse;->g:Ljava/lang/String;

    .line 47
    invoke-virtual {v0, v1}, Lcfq$a;->c(Ljava/lang/String;)Lcfq$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/notifications/JsonMobileSettingsResponse;->f:I

    .line 48
    invoke-virtual {v0, v1}, Lcfq$a;->a(I)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonMobileSettingsResponse;->e:Ljava/util/List;

    .line 49
    invoke-virtual {v0, v1}, Lcfq$a;->a(Ljava/util/List;)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonMobileSettingsResponse;->d:Lcgb;

    .line 50
    invoke-virtual {v0, v1}, Lcfq$a;->b(Lcgb;)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonMobileSettingsResponse;->c:Ljava/util/Map;

    .line 51
    invoke-virtual {v0, v1}, Lcfq$a;->b(Ljava/util/Map;)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonMobileSettingsResponse;->b:Lcgb;

    .line 52
    invoke-virtual {v0, v1}, Lcfq$a;->a(Lcgb;)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonMobileSettingsResponse;->a:Ljava/util/Map;

    .line 53
    invoke-virtual {v0, v1}, Lcfq$a;->a(Ljava/util/Map;)Lcfq$a;

    move-result-object v0

    .line 45
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/twitter/model/json/notifications/JsonMobileSettingsResponse;->a()Lcfq$a;

    move-result-object v0

    return-object v0
.end method
