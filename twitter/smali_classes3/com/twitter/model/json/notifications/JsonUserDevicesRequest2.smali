.class public Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcgc;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Ljava/lang/Boolean;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public h:Ljava/lang/Integer;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public i:Ljava/lang/Integer;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "messagingDeviceType"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "phoneNumber"
        }
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "operationResult"
        }
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "exString"
        }
    .end annotation
.end field

.field public n:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "createDevice"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgc$a;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Lcgc$a;

    invoke-direct {v0}, Lcgc$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->a:Ljava/lang/String;

    .line 59
    invoke-virtual {v0, v1}, Lcgc$a;->e(Ljava/lang/String;)Lcgc$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->b:Ljava/lang/String;

    .line 60
    invoke-virtual {v0, v1}, Lcgc$a;->f(Ljava/lang/String;)Lcgc$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->c:Ljava/lang/String;

    .line 61
    invoke-virtual {v0, v1}, Lcgc$a;->g(Ljava/lang/String;)Lcgc$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->d:Ljava/lang/String;

    .line 62
    invoke-virtual {v0, v1}, Lcgc$a;->h(Ljava/lang/String;)Lcgc$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->e:Ljava/lang/String;

    .line 63
    invoke-virtual {v0, v1}, Lcgc$a;->i(Ljava/lang/String;)Lcgc$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->f:Ljava/lang/String;

    .line 64
    invoke-virtual {v0, v1}, Lcgc$a;->j(Ljava/lang/String;)Lcgc$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->g:Ljava/lang/Boolean;

    .line 65
    invoke-virtual {v0, v1}, Lcgc$a;->a(Ljava/lang/Boolean;)Lcgc$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->h:Ljava/lang/Integer;

    .line 66
    invoke-virtual {v0, v1}, Lcgc$a;->a(Ljava/lang/Integer;)Lcgc$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->i:Ljava/lang/Integer;

    .line 67
    invoke-virtual {v0, v1}, Lcgc$a;->b(Ljava/lang/Integer;)Lcgc$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->j:Ljava/lang/String;

    .line 68
    invoke-virtual {v0, v1}, Lcgc$a;->a(Ljava/lang/String;)Lcgc$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->k:Ljava/lang/String;

    .line 69
    invoke-virtual {v0, v1}, Lcgc$a;->b(Ljava/lang/String;)Lcgc$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->l:Ljava/lang/String;

    .line 70
    invoke-virtual {v0, v1}, Lcgc$a;->c(Ljava/lang/String;)Lcgc$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->m:Ljava/lang/String;

    .line 71
    invoke-virtual {v0, v1}, Lcgc$a;->d(Ljava/lang/String;)Lcgc$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->n:Z

    .line 72
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcgc$a;->b(Ljava/lang/Boolean;)Lcgc$a;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->a()Lcgc$a;

    move-result-object v0

    return-object v0
.end method
