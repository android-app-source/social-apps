.class public Lcom/twitter/model/json/notifications/JsonNotificationTweet;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcfu;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/json/notifications/JsonNotificationMentionEntity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcfu;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 39
    iget-wide v2, p0, Lcom/twitter/model/json/notifications/JsonNotificationTweet;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 40
    new-instance v1, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v2, "Missing id"

    invoke-direct {v1, v2}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 50
    :goto_0
    return-object v0

    .line 42
    :cond_0
    iget-wide v2, p0, Lcom/twitter/model/json/notifications/JsonNotificationTweet;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 43
    new-instance v1, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v2, "Missing created_at"

    invoke-direct {v1, v2}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 45
    :cond_1
    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonNotificationTweet;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 46
    new-instance v1, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v2, "Missing text"

    invoke-direct {v1, v2}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 49
    :cond_2
    iget-object v0, p0, Lcom/twitter/model/json/notifications/JsonNotificationTweet;->g:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 50
    new-instance v1, Lcfu$a;

    invoke-direct {v1}, Lcfu$a;-><init>()V

    iget-wide v2, p0, Lcom/twitter/model/json/notifications/JsonNotificationTweet;->a:J

    .line 51
    invoke-virtual {v1, v2, v3}, Lcfu$a;->a(J)Lcfu$a;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/model/json/notifications/JsonNotificationTweet;->b:J

    .line 52
    invoke-virtual {v1, v2, v3}, Lcfu$a;->b(J)Lcfu$a;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/model/json/notifications/JsonNotificationTweet;->c:J

    .line 53
    invoke-virtual {v1, v2, v3}, Lcfu$a;->c(J)Lcfu$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/model/json/notifications/JsonNotificationTweet;->d:Ljava/lang/String;

    .line 54
    invoke-virtual {v1, v2}, Lcfu$a;->a(Ljava/lang/String;)Lcfu$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/model/json/notifications/JsonNotificationTweet;->e:Ljava/lang/String;

    .line 55
    invoke-virtual {v1, v2}, Lcfu$a;->b(Ljava/lang/String;)Lcfu$a;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/model/json/notifications/JsonNotificationTweet;->f:Z

    .line 56
    invoke-virtual {v1, v2}, Lcfu$a;->a(Z)Lcfu$a;

    move-result-object v1

    .line 57
    invoke-static {v0}, Lcom/twitter/model/json/common/f;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/core/f;->a(Ljava/util/List;)Lcom/twitter/model/core/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcfu$a;->a(Lcom/twitter/model/core/f;)Lcfu$a;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcfu$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfu;

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/twitter/model/json/notifications/JsonNotificationTweet;->a()Lcfu;

    move-result-object v0

    return-object v0
.end method
