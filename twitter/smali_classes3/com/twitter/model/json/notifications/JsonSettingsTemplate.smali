.class public Lcom/twitter/model/json/notifications/JsonSettingsTemplate;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonControlType;,
        Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonNotificationSetting;,
        Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateDoc;,
        Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateContents;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcgb;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcgb$d;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgb$a;
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcgb$a;

    invoke-direct {v0}, Lcgb$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate;->a:Lcgb$d;

    .line 23
    invoke-virtual {v0, v1}, Lcgb$a;->a(Lcgb$d;)Lcgb$a;

    move-result-object v0

    .line 22
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/twitter/model/json/notifications/JsonSettingsTemplate;->a()Lcgb$a;

    move-result-object v0

    return-object v0
.end method
