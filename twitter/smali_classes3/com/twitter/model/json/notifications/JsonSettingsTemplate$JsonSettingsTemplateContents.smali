.class public Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateContents;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/json/notifications/JsonSettingsTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "JsonSettingsTemplateContents"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcgb$d;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcgb$e;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/util/Map;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcgb$b;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcgb$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgb$d$a;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcgb$d$a;

    invoke-direct {v0}, Lcgb$d$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateContents;->a:Lcgb$e;

    .line 43
    invoke-virtual {v0, v1}, Lcgb$d$a;->a(Lcgb$e;)Lcgb$d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateContents;->b:Ljava/util/Map;

    .line 44
    invoke-virtual {v0, v1}, Lcgb$d$a;->a(Ljava/util/Map;)Lcgb$d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateContents;->c:Ljava/util/List;

    .line 45
    invoke-virtual {v0, v1}, Lcgb$d$a;->a(Ljava/util/List;)Lcgb$d$a;

    move-result-object v0

    .line 42
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateContents;->a()Lcgb$d$a;

    move-result-object v0

    return-object v0
.end method
