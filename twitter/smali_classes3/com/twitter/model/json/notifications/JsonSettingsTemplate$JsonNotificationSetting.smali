.class public Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonNotificationSetting;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/json/notifications/JsonSettingsTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "JsonNotificationSetting"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcgb$c;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "default"
        }
    .end annotation
.end field

.field public g:Ljava/util/Map;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgb$c$a;
    .locals 2

    .prologue
    .line 96
    new-instance v0, Lcgb$c$a;

    invoke-direct {v0}, Lcgb$c$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonNotificationSetting;->b:Ljava/lang/String;

    .line 97
    invoke-virtual {v0, v1}, Lcgb$c$a;->b(Ljava/lang/String;)Lcgb$c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonNotificationSetting;->a:Ljava/lang/String;

    .line 98
    invoke-virtual {v0, v1}, Lcgb$c$a;->a(Ljava/lang/String;)Lcgb$c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonNotificationSetting;->c:Ljava/lang/String;

    .line 99
    invoke-virtual {v0, v1}, Lcgb$c$a;->c(Ljava/lang/String;)Lcgb$c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonNotificationSetting;->d:Ljava/lang/String;

    .line 100
    invoke-virtual {v0, v1}, Lcgb$c$a;->d(Ljava/lang/String;)Lcgb$c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonNotificationSetting;->e:Ljava/lang/String;

    .line 101
    invoke-virtual {v0, v1}, Lcgb$c$a;->e(Ljava/lang/String;)Lcgb$c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonNotificationSetting;->f:Ljava/lang/String;

    .line 102
    invoke-virtual {v0, v1}, Lcgb$c$a;->f(Ljava/lang/String;)Lcgb$c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonNotificationSetting;->g:Ljava/util/Map;

    .line 103
    invoke-virtual {v0, v1}, Lcgb$c$a;->a(Ljava/util/Map;)Lcgb$c$a;

    move-result-object v0

    .line 96
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonNotificationSetting;->a()Lcgb$c$a;

    move-result-object v0

    return-object v0
.end method
