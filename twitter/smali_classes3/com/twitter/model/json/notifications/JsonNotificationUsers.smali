.class public Lcom/twitter/model/json/notifications/JsonNotificationUsers;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcfw;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcfv;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Lcfv;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcfv;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "context"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcfs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcfw;
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/model/json/notifications/JsonNotificationUsers;->a:Lcfv;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v1, "Missing recipient"

    invoke-direct {v0, v1}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 33
    const/4 v0, 0x0

    .line 35
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcfw$a;

    invoke-direct {v0}, Lcfw$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonNotificationUsers;->a:Lcfv;

    .line 36
    invoke-virtual {v0, v1}, Lcfw$a;->a(Lcfv;)Lcfw$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonNotificationUsers;->b:Lcfv;

    .line 37
    invoke-virtual {v0, v1}, Lcfw$a;->b(Lcfv;)Lcfw$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonNotificationUsers;->c:Lcfv;

    .line 38
    invoke-virtual {v0, v1}, Lcfw$a;->c(Lcfv;)Lcfw$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonNotificationUsers;->d:Ljava/util/List;

    .line 39
    invoke-virtual {v0, v1}, Lcfw$a;->a(Ljava/util/List;)Lcfw$a;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcfw$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfw;

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/model/json/notifications/JsonNotificationUsers;->a()Lcfw;

    move-result-object v0

    return-object v0
.end method
