.class public Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonControlType;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/json/notifications/JsonSettingsTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "JsonControlType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcgb$b;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "default"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgb$b$a;
    .locals 2

    .prologue
    .line 118
    new-instance v0, Lcgb$b$a;

    invoke-direct {v0}, Lcgb$b$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonControlType;->a:Ljava/lang/String;

    .line 119
    invoke-virtual {v0, v1}, Lcgb$b$a;->a(Ljava/lang/String;)Lcgb$b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonControlType;->b:Ljava/util/List;

    .line 120
    invoke-virtual {v0, v1}, Lcgb$b$a;->a(Ljava/util/List;)Lcgb$b$a;

    move-result-object v0

    .line 118
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonControlType;->a()Lcgb$b$a;

    move-result-object v0

    return-object v0
.end method
