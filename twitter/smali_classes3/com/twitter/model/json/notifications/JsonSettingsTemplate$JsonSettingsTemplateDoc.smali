.class public Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateDoc;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/json/notifications/JsonSettingsTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "JsonSettingsTemplateDoc"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcgb$e;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgb$e$a;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcgb$e$a;

    invoke-direct {v0}, Lcgb$e$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateDoc;->c:Ljava/lang/String;

    .line 64
    invoke-virtual {v0, v1}, Lcgb$e$a;->c(Ljava/lang/String;)Lcgb$e$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateDoc;->b:Ljava/lang/String;

    .line 65
    invoke-virtual {v0, v1}, Lcgb$e$a;->b(Ljava/lang/String;)Lcgb$e$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateDoc;->a:Ljava/lang/String;

    .line 66
    invoke-virtual {v0, v1}, Lcgb$e$a;->a(Ljava/lang/String;)Lcgb$e$a;

    move-result-object v0

    .line 63
    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/twitter/model/json/notifications/JsonSettingsTemplate$JsonSettingsTemplateDoc;->a()Lcgb$e$a;

    move-result-object v0

    return-object v0
.end method
