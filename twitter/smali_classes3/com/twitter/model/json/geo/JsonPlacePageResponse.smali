.class public Lcom/twitter/model/json/geo/JsonPlacePageResponse;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/json/geo/JsonPlacePageResponse$JsonPlacePageTimeline;,
        Lcom/twitter/model/json/geo/JsonPlacePageResponse$JsonPlacePageHeader;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/geo/d;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/model/geo/d$a;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Lcom/twitter/model/geo/d$b;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcom/twitter/model/geo/d$b;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/geo/d;
    .locals 4

    .prologue
    .line 27
    new-instance v0, Lcom/twitter/model/geo/d;

    iget-object v1, p0, Lcom/twitter/model/json/geo/JsonPlacePageResponse;->a:Lcom/twitter/model/geo/d$a;

    iget-object v2, p0, Lcom/twitter/model/json/geo/JsonPlacePageResponse;->b:Lcom/twitter/model/geo/d$b;

    iget-object v3, p0, Lcom/twitter/model/json/geo/JsonPlacePageResponse;->c:Lcom/twitter/model/geo/d$b;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/model/geo/d;-><init>(Lcom/twitter/model/geo/d$a;Lcom/twitter/model/geo/d$b;Lcom/twitter/model/geo/d$b;)V

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/model/json/geo/JsonPlacePageResponse;->a()Lcom/twitter/model/geo/d;

    move-result-object v0

    return-object v0
.end method
