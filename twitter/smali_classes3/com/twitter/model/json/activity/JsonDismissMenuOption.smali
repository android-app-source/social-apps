.class public Lcom/twitter/model/json/activity/JsonDismissMenuOption;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcaf;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcad;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcag;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Lcag;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcaf;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 29
    new-instance v2, Lcaf$a;

    invoke-direct {v2}, Lcaf$a;-><init>()V

    iget-object v0, p0, Lcom/twitter/model/json/activity/JsonDismissMenuOption;->c:Lcag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/json/activity/JsonDismissMenuOption;->c:Lcag;

    iget-object v0, v0, Lcag;->a:Ljava/lang/String;

    .line 30
    :goto_0
    invoke-virtual {v2, v0}, Lcaf$a;->a(Ljava/lang/String;)Lcaf$a;

    move-result-object v0

    iget v2, p0, Lcom/twitter/model/json/activity/JsonDismissMenuOption;->b:I

    .line 31
    invoke-virtual {v0, v2}, Lcaf$a;->a(I)Lcaf$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/model/json/activity/JsonDismissMenuOption;->d:Lcag;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonDismissMenuOption;->d:Lcag;

    iget-object v1, v1, Lcag;->a:Ljava/lang/String;

    .line 32
    :cond_0
    invoke-virtual {v0, v1}, Lcaf$a;->b(Ljava/lang/String;)Lcaf$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonDismissMenuOption;->a:Lcad;

    .line 33
    invoke-virtual {v0, v1}, Lcaf$a;->a(Lcad;)Lcaf$a;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcaf$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcaf;

    .line 29
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/model/json/activity/JsonDismissMenuOption;->a()Lcaf;

    move-result-object v0

    return-object v0
.end method
