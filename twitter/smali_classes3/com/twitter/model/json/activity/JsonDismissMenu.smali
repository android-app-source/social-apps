.class public Lcom/twitter/model/json/activity/JsonDismissMenu;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcae;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcaf;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcad;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcae;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcae$a;

    invoke-direct {v0}, Lcae$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonDismissMenu;->a:Ljava/lang/String;

    .line 29
    invoke-virtual {v0, v1}, Lcae$a;->a(Ljava/lang/String;)Lcae$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonDismissMenu;->b:Ljava/util/List;

    .line 30
    invoke-static {v1}, Lcom/twitter/util/collection/h;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcae$a;->a(Ljava/util/List;)Lcae$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonDismissMenu;->c:Lcad;

    .line 31
    invoke-virtual {v0, v1}, Lcae$a;->a(Lcad;)Lcae$a;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcae$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcae;

    .line 28
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/twitter/model/json/activity/JsonDismissMenu;->a()Lcae;

    move-result-object v0

    return-object v0
.end method
