.class public Lcom/twitter/model/json/activity/JsonGenericActivity;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcaj;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Lcag;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Lcag;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:Lcae;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public h:Lcan;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcaj;
    .locals 4

    .prologue
    .line 55
    new-instance v0, Lcaj$a;

    invoke-direct {v0}, Lcaj$a;-><init>()V

    iget-wide v2, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->a:J

    .line 56
    invoke-virtual {v0, v2, v3}, Lcaj$a;->a(J)Lcaj$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->a:J

    .line 57
    invoke-virtual {v0, v2, v3}, Lcaj$a;->b(J)Lcaj$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->a:J

    .line 58
    invoke-virtual {v0, v2, v3}, Lcaj$a;->c(J)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->b:Lcag;

    .line 59
    invoke-virtual {v0, v1}, Lcaj$a;->a(Lcag;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->c:Lcag;

    .line 60
    invoke-virtual {v0, v1}, Lcaj$a;->b(Lcag;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->d:Ljava/lang/String;

    .line 61
    invoke-virtual {v0, v1}, Lcaj$a;->a(Ljava/lang/String;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->g:Ljava/lang/String;

    .line 62
    invoke-virtual {v0, v1}, Lcaj$a;->c(Ljava/lang/String;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->h:Lcan;

    .line 63
    invoke-virtual {v0, v1}, Lcaj$a;->a(Lcan;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->e:Ljava/lang/String;

    .line 64
    invoke-virtual {v0, v1}, Lcaj$a;->b(Ljava/lang/String;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->k:Ljava/util/List;

    .line 65
    invoke-virtual {v0, v1}, Lcaj$a;->a(Ljava/util/List;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->m:Ljava/util/List;

    .line 66
    invoke-virtual {v0, v1}, Lcaj$a;->b(Ljava/util/List;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->l:Ljava/util/List;

    .line 67
    invoke-virtual {v0, v1}, Lcaj$a;->c(Ljava/util/List;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->n:Ljava/lang/String;

    .line 68
    invoke-static {v1}, Lcom/twitter/model/util/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcaj$a;->f(Ljava/lang/String;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->f:Lcae;

    .line 69
    invoke-virtual {v0, v1}, Lcaj$a;->a(Lcae;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->j:Ljava/lang/String;

    .line 70
    invoke-virtual {v0, v1}, Lcaj$a;->d(Ljava/lang/String;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->o:Ljava/lang/String;

    .line 71
    invoke-virtual {v0, v1}, Lcaj$a;->e(Ljava/lang/String;)Lcaj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/activity/JsonGenericActivity;->i:Ljava/lang/String;

    .line 72
    invoke-virtual {v0, v1}, Lcaj$a;->g(Ljava/lang/String;)Lcaj$a;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcaj$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcaj;

    .line 55
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/twitter/model/json/activity/JsonGenericActivity;->a()Lcaj;

    move-result-object v0

    return-object v0
.end method
