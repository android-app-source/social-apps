.class public Lcom/twitter/model/json/activity/JsonDisplayText;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcag;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Lcpp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpp",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/twitter/model/core/i;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "bold_indexes"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/model/json/activity/JsonDisplayText$1;

    invoke-direct {v0}, Lcom/twitter/model/json/activity/JsonDisplayText$1;-><init>()V

    sput-object v0, Lcom/twitter/model/json/activity/JsonDisplayText;->c:Lcpp;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcag;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 42
    iget-object v0, p0, Lcom/twitter/model/json/activity/JsonDisplayText;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/json/activity/JsonDisplayText;->b:Ljava/util/List;

    sget-object v2, Lcom/twitter/model/json/activity/JsonDisplayText;->c:Lcpp;

    .line 43
    invoke-static {v0, v2}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    .line 44
    :goto_0
    new-instance v6, Lcag$a;

    invoke-direct {v6}, Lcag$a;-><init>()V

    iget-object v0, p0, Lcom/twitter/model/json/activity/JsonDisplayText;->a:Ljava/lang/String;

    move-object v3, v1

    move v5, v4

    .line 45
    invoke-static/range {v0 .. v5}, Lcom/twitter/model/util/f;->a(Ljava/lang/String;Lcom/twitter/model/core/v;Ljava/lang/Iterable;Ljava/util/List;ZZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcag$a;->a(Ljava/lang/String;)Lcag$a;

    move-result-object v0

    .line 46
    invoke-virtual {v0, v2}, Lcag$a;->a(Ljava/util/List;)Lcag$a;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcag$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcag;

    .line 44
    return-object v0

    :cond_0
    move-object v2, v1

    .line 43
    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/twitter/model/json/activity/JsonDisplayText;->a()Lcag;

    move-result-object v0

    return-object v0
.end method
