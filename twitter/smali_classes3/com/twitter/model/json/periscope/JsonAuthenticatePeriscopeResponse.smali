.class public Lcom/twitter/model/json/periscope/JsonAuthenticatePeriscopeResponse;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
    fieldNamingPolicy = .enum Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/bluelinelabs/logansquare/annotation/JsonObject$FieldNamingPolicy;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcgj;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "token"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgj$a;
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcgj$a;

    invoke-direct {v0}, Lcgj$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/periscope/JsonAuthenticatePeriscopeResponse;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcgj$a;->a(Ljava/lang/String;)Lcgj$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/periscope/JsonAuthenticatePeriscopeResponse;->a()Lcgj$a;

    move-result-object v0

    return-object v0
.end method
