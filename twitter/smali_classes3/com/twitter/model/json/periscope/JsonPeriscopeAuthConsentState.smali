.class public Lcom/twitter/model/json/periscope/JsonPeriscopeAuthConsentState;
.super Lcom/twitter/model/json/common/c;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/c",
        "<",
        "Lcgk;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Z
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
        name = {
            "periscopeAuthConsentGranted"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcgk$a;
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcgk$a;

    invoke-direct {v0}, Lcgk$a;-><init>()V

    iget-boolean v1, p0, Lcom/twitter/model/json/periscope/JsonPeriscopeAuthConsentState;->a:Z

    invoke-virtual {v0, v1}, Lcgk$a;->a(Z)Lcgk$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/periscope/JsonPeriscopeAuthConsentState;->a()Lcgk$a;

    move-result-object v0

    return-object v0
.end method
