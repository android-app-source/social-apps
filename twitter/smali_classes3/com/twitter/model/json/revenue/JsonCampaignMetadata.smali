.class public Lcom/twitter/model/json/revenue/JsonCampaignMetadata;
.super Lcom/twitter/model/json/common/d;
.source "Twttr"


# annotations
.annotation build Lcom/bluelinelabs/logansquare/annotation/JsonObject;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/d",
        "<",
        "Lcom/twitter/model/revenue/d;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public d:F
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public e:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field

.field public f:J
    .annotation build Lcom/bluelinelabs/logansquare/annotation/JsonField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/model/json/common/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/revenue/d;
    .locals 4

    .prologue
    .line 24
    new-instance v0, Lcom/twitter/model/revenue/d$a;

    invoke-direct {v0}, Lcom/twitter/model/revenue/d$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/revenue/JsonCampaignMetadata;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/model/revenue/d$a;->a(Ljava/lang/String;)Lcom/twitter/model/revenue/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/revenue/JsonCampaignMetadata;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/model/revenue/d$a;->b(Ljava/lang/String;)Lcom/twitter/model/revenue/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/revenue/JsonCampaignMetadata;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/model/revenue/d$a;->c(Ljava/lang/String;)Lcom/twitter/model/revenue/d$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/model/json/revenue/JsonCampaignMetadata;->d:F

    .line 25
    invoke-virtual {v0, v1}, Lcom/twitter/model/revenue/d$a;->a(F)Lcom/twitter/model/revenue/d$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/revenue/JsonCampaignMetadata;->e:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/revenue/d$a;->a(J)Lcom/twitter/model/revenue/d$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/json/revenue/JsonCampaignMetadata;->f:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/revenue/d$a;->b(J)Lcom/twitter/model/revenue/d$a;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lcom/twitter/model/revenue/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/revenue/d;

    .line 24
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/model/json/revenue/JsonCampaignMetadata;->a()Lcom/twitter/model/revenue/d;

    move-result-object v0

    return-object v0
.end method
