.class public Lcom/twitter/model/json/revenue/b;
.super Lcom/twitter/model/json/common/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/h",
        "<",
        "Lcbc;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/twitter/model/json/common/h;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonParser;)Lcbc;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 44
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    if-nez v0, :cond_0

    .line 45
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 47
    :cond_0
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    .line 48
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 101
    :goto_0
    return-object v1

    .line 52
    :cond_1
    new-instance v4, Lcbc$a;

    invoke-direct {v4}, Lcbc$a;-><init>()V

    move-object v0, v1

    .line 54
    :goto_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v5, :cond_5

    .line 55
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v5

    .line 56
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 57
    const/4 v2, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v2, :pswitch_data_0

    .line 96
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 57
    :sswitch_0
    const-string/jumbo v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v2, 0x0

    goto :goto_2

    :sswitch_1
    const-string/jumbo v6, "version"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :sswitch_2
    const-string/jumbo v6, "url"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v2, v3

    goto :goto_2

    :sswitch_3
    const-string/jumbo v6, "items"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v2, 0x3

    goto :goto_2

    .line 59
    :pswitch_0
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->j()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcbc$a;->a(J)Lcbc$a;

    goto :goto_1

    .line 63
    :pswitch_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v4, v2}, Lcbc$a;->a(I)Lcbc$a;

    goto :goto_1

    .line 68
    :pswitch_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcbc$a;->a(Ljava/lang/String;)Lcbc$a;

    goto :goto_1

    .line 74
    :pswitch_3
    if-nez v0, :cond_4

    .line 75
    const-class v2, Lcom/twitter/model/core/ac;

    .line 76
    invoke-static {p1, v2}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    .line 77
    new-instance v5, Lcom/twitter/model/json/revenue/b$1;

    invoke-direct {v5, p0}, Lcom/twitter/model/json/revenue/b$1;-><init>(Lcom/twitter/model/json/revenue/b;)V

    invoke-static {v2, v5}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v2

    .line 90
    :goto_3
    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 91
    invoke-static {}, Lcom/twitter/util/collection/CollectionUtils;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 90
    :cond_3
    invoke-virtual {v4, v2}, Lcbc$a;->a(Ljava/util/List;)Lcbc$a;

    goto/16 :goto_1

    .line 87
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v3, :cond_7

    .line 88
    const-class v2, Lcba;

    invoke-static {p1, v2}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    goto :goto_3

    .line 101
    :cond_5
    invoke-virtual {v4}, Lcbc$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v4}, Lcbc$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbc;

    :goto_4
    move-object v1, v0

    goto/16 :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_4

    :cond_7
    move-object v2, v1

    goto :goto_3

    .line 57
    nop

    :sswitch_data_0
    .sparse-switch
        0xd1b -> :sswitch_0
        0x1c56f -> :sswitch_2
        0x5fde7c0 -> :sswitch_3
        0x14f51cd8 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/revenue/b;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcbc;

    move-result-object v0

    return-object v0
.end method
