.class public Lcom/twitter/model/json/revenue/c;
.super Lcom/twitter/model/json/common/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/json/common/h",
        "<",
        "Lcba;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/twitter/model/json/common/h;-><init>()V

    return-void
.end method

.method private b(Lcom/fasterxml/jackson/core/JsonParser;)Lcba;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    const-string/jumbo v0, "Tried to build collection with invalid json."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 95
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonParser;)Lcba;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    .line 46
    invoke-direct {p0, p1}, Lcom/twitter/model/json/revenue/c;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcba;

    move-result-object v1

    .line 88
    :cond_0
    :goto_0
    return-object v1

    .line 48
    :cond_1
    const/4 v1, 0x0

    .line 49
    :goto_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    .line 50
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 51
    if-nez v2, :cond_2

    .line 52
    invoke-direct {p0, p1}, Lcom/twitter/model/json/revenue/c;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcba;

    move-result-object v1

    goto :goto_0

    .line 54
    :cond_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 55
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_3
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 83
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_4
    move-object v0, v1

    :goto_3
    move-object v1, v0

    .line 87
    goto :goto_1

    .line 55
    :sswitch_0
    const-string/jumbo v3, "tweet"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string/jumbo v3, "card"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v0, 0x1

    goto :goto_2

    .line 58
    :pswitch_0
    const-class v0, Lcom/twitter/model/json/core/JsonTwitterStatus;

    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/core/JsonTwitterStatus;

    .line 59
    if-eqz v0, :cond_4

    .line 60
    invoke-virtual {v0}, Lcom/twitter/model/json/core/JsonTwitterStatus;->a()Lcom/twitter/model/core/ac$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/core/ac$a;->R_()Z

    move-result v1

    .line 61
    if-eqz v1, :cond_5

    .line 62
    new-instance v1, Lcbb$a;

    invoke-direct {v1}, Lcbb$a;-><init>()V

    .line 63
    invoke-virtual {v0}, Lcom/twitter/model/json/core/JsonTwitterStatus;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    invoke-virtual {v1, v0}, Lcbb$a;->a(Lcom/twitter/model/core/ac;)Lcbb$a;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcbb$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcba;

    goto :goto_3

    .line 67
    :cond_5
    new-instance v1, Lcbd$a;

    invoke-direct {v1}, Lcbd$a;-><init>()V

    iget-wide v2, v0, Lcom/twitter/model/json/core/JsonTwitterStatus;->a:J

    .line 68
    invoke-virtual {v1, v2, v3}, Lcbd$a;->a(J)Lcbd$a;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcbd$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcba;

    goto :goto_3

    .line 76
    :pswitch_1
    new-instance v1, Lcaz$a;

    invoke-direct {v1}, Lcaz$a;-><init>()V

    .line 77
    const-class v0, Lcax;

    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    invoke-virtual {v1, v0}, Lcaz$a;->a(Lcax;)Lcaz$a;

    .line 78
    invoke-virtual {v1}, Lcaz$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcba;

    goto :goto_3

    .line 55
    :sswitch_data_0
    .sparse-switch
        0x2e7b10 -> :sswitch_1
        0x69a4671 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public synthetic parse(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/twitter/model/json/revenue/c;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcba;

    move-result-object v0

    return-object v0
.end method
