.class public abstract Lcom/twitter/model/livepipeline/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/livepipeline/d$a;
    }
.end annotation


# static fields
.field public static final d:Lcom/twitter/model/livepipeline/h;


# instance fields
.field public final e:Lcom/twitter/model/livepipeline/PipelineEventType;

.field public final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/twitter/model/livepipeline/h$a;

    invoke-direct {v0}, Lcom/twitter/model/livepipeline/h$a;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/model/livepipeline/h$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livepipeline/h;

    sput-object v0, Lcom/twitter/model/livepipeline/d;->d:Lcom/twitter/model/livepipeline/h;

    return-void
.end method

.method constructor <init>(Lcom/twitter/model/livepipeline/d$a;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iget-object v0, p1, Lcom/twitter/model/livepipeline/d$a;->d:Lcom/twitter/model/livepipeline/PipelineEventType;

    iput-object v0, p0, Lcom/twitter/model/livepipeline/d;->e:Lcom/twitter/model/livepipeline/PipelineEventType;

    .line 24
    iget-object v0, p1, Lcom/twitter/model/livepipeline/d$a;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/livepipeline/d;->f:Ljava/lang/String;

    .line 25
    return-void
.end method
