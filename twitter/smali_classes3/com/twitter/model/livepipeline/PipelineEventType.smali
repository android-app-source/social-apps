.class public final enum Lcom/twitter/model/livepipeline/PipelineEventType;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/model/livepipeline/PipelineEventType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/model/livepipeline/PipelineEventType;

.field public static final enum b:Lcom/twitter/model/livepipeline/PipelineEventType;

.field public static final enum c:Lcom/twitter/model/livepipeline/PipelineEventType;

.field public static final enum d:Lcom/twitter/model/livepipeline/PipelineEventType;

.field public static final enum e:Lcom/twitter/model/livepipeline/PipelineEventType;

.field public static final enum f:Lcom/twitter/model/livepipeline/PipelineEventType;

.field public static final enum g:Lcom/twitter/model/livepipeline/PipelineEventType;

.field private static final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/model/livepipeline/PipelineEventType;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic j:[Lcom/twitter/model/livepipeline/PipelineEventType;


# instance fields
.field public final builder:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/model/livepipeline/d$a;",
            ">;"
        }
    .end annotation
.end field

.field public final canonicalName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 15
    new-instance v0, Lcom/twitter/model/livepipeline/PipelineEventType;

    const-string/jumbo v2, "NO_TYPE"

    const-string/jumbo v3, "no_type"

    const/4 v4, 0x0

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/twitter/model/livepipeline/PipelineEventType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->a:Lcom/twitter/model/livepipeline/PipelineEventType;

    .line 16
    new-instance v0, Lcom/twitter/model/livepipeline/PipelineEventType;

    const-string/jumbo v2, "CONFIG"

    const-string/jumbo v3, "config"

    const-class v4, Lcom/twitter/model/livepipeline/a$a;

    invoke-direct {v0, v2, v6, v3, v4}, Lcom/twitter/model/livepipeline/PipelineEventType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->b:Lcom/twitter/model/livepipeline/PipelineEventType;

    .line 17
    new-instance v0, Lcom/twitter/model/livepipeline/PipelineEventType;

    const-string/jumbo v2, "SUBSCRIPTION"

    const-string/jumbo v3, "subscriptions"

    const-class v4, Lcom/twitter/model/livepipeline/f$a;

    invoke-direct {v0, v2, v7, v3, v4}, Lcom/twitter/model/livepipeline/PipelineEventType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->c:Lcom/twitter/model/livepipeline/PipelineEventType;

    .line 18
    new-instance v0, Lcom/twitter/model/livepipeline/PipelineEventType;

    const-string/jumbo v2, "TYPING_INDICATOR"

    const-string/jumbo v3, "dm_typing"

    const-class v4, Lcom/twitter/model/livepipeline/g$a;

    invoke-direct {v0, v2, v8, v3, v4}, Lcom/twitter/model/livepipeline/PipelineEventType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->d:Lcom/twitter/model/livepipeline/PipelineEventType;

    .line 19
    new-instance v0, Lcom/twitter/model/livepipeline/PipelineEventType;

    const-string/jumbo v2, "DM_UPDATE"

    const-string/jumbo v3, "dm_update"

    const-class v4, Lcom/twitter/model/livepipeline/b$a;

    invoke-direct {v0, v2, v9, v3, v4}, Lcom/twitter/model/livepipeline/PipelineEventType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->e:Lcom/twitter/model/livepipeline/PipelineEventType;

    .line 20
    new-instance v0, Lcom/twitter/model/livepipeline/PipelineEventType;

    const-string/jumbo v2, "TWEET_ENGAGEMENT"

    const/4 v3, 0x5

    const-string/jumbo v4, "tweet_engagement"

    const-class v5, Lcom/twitter/model/livepipeline/c$a;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/twitter/model/livepipeline/PipelineEventType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->f:Lcom/twitter/model/livepipeline/PipelineEventType;

    .line 21
    new-instance v0, Lcom/twitter/model/livepipeline/PipelineEventType;

    const-string/jumbo v2, "TEST"

    const/4 v3, 0x6

    const-string/jumbo v4, "test"

    const-class v5, Lcom/twitter/model/livepipeline/c$a;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/twitter/model/livepipeline/PipelineEventType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->g:Lcom/twitter/model/livepipeline/PipelineEventType;

    .line 14
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/twitter/model/livepipeline/PipelineEventType;

    sget-object v2, Lcom/twitter/model/livepipeline/PipelineEventType;->a:Lcom/twitter/model/livepipeline/PipelineEventType;

    aput-object v2, v0, v1

    sget-object v2, Lcom/twitter/model/livepipeline/PipelineEventType;->b:Lcom/twitter/model/livepipeline/PipelineEventType;

    aput-object v2, v0, v6

    sget-object v2, Lcom/twitter/model/livepipeline/PipelineEventType;->c:Lcom/twitter/model/livepipeline/PipelineEventType;

    aput-object v2, v0, v7

    sget-object v2, Lcom/twitter/model/livepipeline/PipelineEventType;->d:Lcom/twitter/model/livepipeline/PipelineEventType;

    aput-object v2, v0, v8

    sget-object v2, Lcom/twitter/model/livepipeline/PipelineEventType;->e:Lcom/twitter/model/livepipeline/PipelineEventType;

    aput-object v2, v0, v9

    const/4 v2, 0x5

    sget-object v3, Lcom/twitter/model/livepipeline/PipelineEventType;->f:Lcom/twitter/model/livepipeline/PipelineEventType;

    aput-object v3, v0, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/twitter/model/livepipeline/PipelineEventType;->g:Lcom/twitter/model/livepipeline/PipelineEventType;

    aput-object v3, v0, v2

    sput-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->j:[Lcom/twitter/model/livepipeline/PipelineEventType;

    .line 24
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v0

    new-array v2, v8, [Lcom/twitter/model/livepipeline/PipelineEventType;

    sget-object v3, Lcom/twitter/model/livepipeline/PipelineEventType;->a:Lcom/twitter/model/livepipeline/PipelineEventType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/twitter/model/livepipeline/PipelineEventType;->b:Lcom/twitter/model/livepipeline/PipelineEventType;

    aput-object v3, v2, v6

    sget-object v3, Lcom/twitter/model/livepipeline/PipelineEventType;->c:Lcom/twitter/model/livepipeline/PipelineEventType;

    aput-object v3, v2, v7

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/o;->a([Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    sput-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->h:Ljava/util/Set;

    .line 30
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v2

    .line 31
    invoke-static {}, Lcom/twitter/model/livepipeline/PipelineEventType;->values()[Lcom/twitter/model/livepipeline/PipelineEventType;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v1, v3, v0

    .line 32
    iget-object v5, v1, Lcom/twitter/model/livepipeline/PipelineEventType;->builder:Ljava/lang/Class;

    const-class v6, Lcom/twitter/model/livepipeline/c$a;

    if-ne v5, v6, :cond_0

    .line 33
    iget-object v1, v1, Lcom/twitter/model/livepipeline/PipelineEventType;->canonicalName:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 31
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_1
    invoke-virtual {v2}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    sput-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->i:Ljava/util/Set;

    .line 38
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/model/livepipeline/d$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput-object p3, p0, Lcom/twitter/model/livepipeline/PipelineEventType;->canonicalName:Ljava/lang/String;

    .line 52
    iput-object p4, p0, Lcom/twitter/model/livepipeline/PipelineEventType;->builder:Ljava/lang/Class;

    .line 53
    return-void
.end method

.method public static a(Lcom/twitter/model/livepipeline/PipelineEventType;)Z
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->h:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->i:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static c(Ljava/lang/String;)Lcom/twitter/model/livepipeline/PipelineEventType;
    .locals 5

    .prologue
    .line 84
    invoke-static {}, Lcom/twitter/model/livepipeline/PipelineEventType;->values()[Lcom/twitter/model/livepipeline/PipelineEventType;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 85
    invoke-virtual {v0}, Lcom/twitter/model/livepipeline/PipelineEventType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 89
    :goto_1
    return-object v0

    .line 84
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 89
    :cond_1
    sget-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->a:Lcom/twitter/model/livepipeline/PipelineEventType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/model/livepipeline/PipelineEventType;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/twitter/model/livepipeline/PipelineEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livepipeline/PipelineEventType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/model/livepipeline/PipelineEventType;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->j:[Lcom/twitter/model/livepipeline/PipelineEventType;

    invoke-virtual {v0}, [Lcom/twitter/model/livepipeline/PipelineEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/model/livepipeline/PipelineEventType;

    return-object v0
.end method


# virtual methods
.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/livepipeline/PipelineEventType;->canonicalName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/model/livepipeline/PipelineEventType;->canonicalName:Ljava/lang/String;

    return-object v0
.end method
