.class public Lcom/twitter/model/topic/c;
.super Lcom/twitter/model/topic/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/topic/c$a;
    }
.end annotation


# static fields
.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/topic/c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:I

.field public d:I

.field public e:Z

.field public f:I

.field public g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/twitter/model/topic/c$a;

    invoke-direct {v0}, Lcom/twitter/model/topic/c$a;-><init>()V

    sput-object v0, Lcom/twitter/model/topic/c;->b:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/twitter/model/topic/b;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(IIZIJ)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/twitter/model/topic/b;-><init>()V

    .line 29
    iput p1, p0, Lcom/twitter/model/topic/c;->c:I

    .line 30
    iput p2, p0, Lcom/twitter/model/topic/c;->d:I

    .line 31
    iput-boolean p3, p0, Lcom/twitter/model/topic/c;->e:Z

    .line 32
    iput p4, p0, Lcom/twitter/model/topic/c;->f:I

    .line 33
    iput-wide p5, p0, Lcom/twitter/model/topic/c;->g:J

    .line 34
    return-void
.end method
