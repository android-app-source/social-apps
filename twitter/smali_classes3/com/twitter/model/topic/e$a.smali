.class public Lcom/twitter/model/topic/e$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/topic/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/topic/e;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/i;-><init>(I)V

    .line 95
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/topic/e;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 116
    new-instance v1, Lcom/twitter/model/topic/e;

    invoke-direct {v1}, Lcom/twitter/model/topic/e;-><init>()V

    .line 117
    sget-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/model/topic/e;->f:Ljava/util/List;

    .line 118
    sget-object v0, Lcom/twitter/model/topic/trends/a;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/trends/a;

    iput-object v0, v1, Lcom/twitter/model/topic/e;->c:Lcom/twitter/model/topic/trends/a;

    .line 119
    sget-object v0, Lcom/twitter/model/topic/trends/d;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/trends/d;

    iput-object v0, v1, Lcom/twitter/model/topic/e;->d:Lcom/twitter/model/topic/trends/d;

    .line 120
    sget-object v0, Lcom/twitter/model/topic/trends/b;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/trends/b;

    iput-object v0, v1, Lcom/twitter/model/topic/e;->e:Lcom/twitter/model/topic/trends/b;

    .line 121
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    iput v0, v1, Lcom/twitter/model/topic/e;->g:I

    .line 122
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/model/topic/e;->h:Ljava/lang/String;

    .line 123
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    iput v0, v1, Lcom/twitter/model/topic/e;->i:I

    .line 124
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v0

    iput-boolean v0, v1, Lcom/twitter/model/topic/e;->j:Z

    .line 125
    const/4 v0, 0x1

    if-lt p2, v0, :cond_0

    .line 126
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    iput v0, v1, Lcom/twitter/model/topic/e;->k:I

    .line 128
    :cond_0
    sget-object v0, Lcom/twitter/model/topic/trends/TrendBadge;->d:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/model/topic/e;->l:Ljava/util/List;

    .line 129
    return-object v1
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/topic/e;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p2, Lcom/twitter/model/topic/e;->f:Ljava/util/List;

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 101
    iget-object v0, p2, Lcom/twitter/model/topic/e;->c:Lcom/twitter/model/topic/trends/a;

    sget-object v1, Lcom/twitter/model/topic/trends/a;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/topic/e;->d:Lcom/twitter/model/topic/trends/d;

    sget-object v2, Lcom/twitter/model/topic/trends/d;->a:Lcom/twitter/util/serialization/l;

    .line 102
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/topic/e;->e:Lcom/twitter/model/topic/trends/b;

    sget-object v2, Lcom/twitter/model/topic/trends/b;->a:Lcom/twitter/util/serialization/l;

    .line 103
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/topic/e;->g:I

    .line 104
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/topic/e;->h:Ljava/lang/String;

    .line 105
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/topic/e;->i:I

    .line 106
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/topic/e;->j:Z

    .line 107
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/model/topic/e;->k:I

    .line 108
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 109
    iget-object v0, p2, Lcom/twitter/model/topic/e;->l:Ljava/util/List;

    sget-object v1, Lcom/twitter/model/topic/trends/TrendBadge;->d:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 110
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    check-cast p2, Lcom/twitter/model/topic/e;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/topic/e$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/topic/e;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/topic/e$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/topic/e;

    move-result-object v0

    return-object v0
.end method
