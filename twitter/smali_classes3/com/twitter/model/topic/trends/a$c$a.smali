.class Lcom/twitter/model/topic/trends/a$c$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/topic/trends/a$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/topic/trends/a$c;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 188
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/topic/trends/a$1;)V
    .locals 0

    .prologue
    .line 188
    invoke-direct {p0}, Lcom/twitter/model/topic/trends/a$c$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/topic/trends/a$c;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 200
    new-instance v0, Lcom/twitter/model/topic/trends/a$c;

    invoke-direct {v0}, Lcom/twitter/model/topic/trends/a$c;-><init>()V

    .line 201
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    iput v1, v0, Lcom/twitter/model/topic/trends/a$c;->b:I

    .line 202
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/twitter/model/topic/trends/a$c;->c:J

    .line 203
    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/topic/trends/a$c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    iget v0, p2, Lcom/twitter/model/topic/trends/a$c;->b:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 193
    iget-wide v0, p2, Lcom/twitter/model/topic/trends/a$c;->c:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 194
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 188
    check-cast p2, Lcom/twitter/model/topic/trends/a$c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/topic/trends/a$c$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/topic/trends/a$c;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 188
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/topic/trends/a$c$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/topic/trends/a$c;

    move-result-object v0

    return-object v0
.end method
