.class Lcom/twitter/model/topic/trends/TrendBadge$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/topic/trends/TrendBadge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/topic/trends/TrendBadge;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/topic/trends/TrendBadge$1;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/twitter/model/topic/trends/TrendBadge$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/topic/trends/TrendBadge;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    .line 40
    invoke-static {v0}, Lcom/twitter/model/topic/trends/TrendBadge;->valueOf(Ljava/lang/String;)Lcom/twitter/model/topic/trends/TrendBadge;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/topic/trends/TrendBadge;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p2}, Lcom/twitter/model/topic/trends/TrendBadge;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 33
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p2, Lcom/twitter/model/topic/trends/TrendBadge;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/topic/trends/TrendBadge$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/topic/trends/TrendBadge;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/topic/trends/TrendBadge$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/topic/trends/TrendBadge;

    move-result-object v0

    return-object v0
.end method
