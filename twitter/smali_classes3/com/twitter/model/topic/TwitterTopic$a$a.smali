.class Lcom/twitter/model/topic/TwitterTopic$a$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/topic/TwitterTopic$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/topic/TwitterTopic$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 412
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/topic/TwitterTopic$1;)V
    .locals 0

    .prologue
    .line 412
    invoke-direct {p0}, Lcom/twitter/model/topic/TwitterTopic$a$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/topic/TwitterTopic$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 425
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    .line 426
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    .line 427
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v2

    .line 428
    new-instance v3, Lcom/twitter/model/topic/TwitterTopic$a;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/model/topic/TwitterTopic$a;-><init>(ILjava/lang/String;Z)V

    return-object v3
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/topic/TwitterTopic$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 416
    iget v0, p2, Lcom/twitter/model/topic/TwitterTopic$a;->b:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/topic/TwitterTopic$a;->c:Ljava/lang/String;

    .line 417
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/topic/TwitterTopic$a;->d:Z

    .line 418
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 419
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 412
    check-cast p2, Lcom/twitter/model/topic/TwitterTopic$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/topic/TwitterTopic$a$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/topic/TwitterTopic$a;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 412
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/topic/TwitterTopic$a$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/topic/TwitterTopic$a;

    move-result-object v0

    return-object v0
.end method
