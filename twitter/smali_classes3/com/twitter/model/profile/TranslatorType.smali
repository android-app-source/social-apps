.class public final enum Lcom/twitter/model/profile/TranslatorType;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/model/profile/TranslatorType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/model/profile/TranslatorType;

.field public static final enum b:Lcom/twitter/model/profile/TranslatorType;

.field public static final enum c:Lcom/twitter/model/profile/TranslatorType;

.field public static final enum d:Lcom/twitter/model/profile/TranslatorType;

.field private static final synthetic e:[Lcom/twitter/model/profile/TranslatorType;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lcom/twitter/model/profile/TranslatorType;

    const-string/jumbo v1, "NONE"

    const-string/jumbo v2, "none"

    invoke-direct {v0, v1, v3, v2}, Lcom/twitter/model/profile/TranslatorType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/profile/TranslatorType;->a:Lcom/twitter/model/profile/TranslatorType;

    .line 10
    new-instance v0, Lcom/twitter/model/profile/TranslatorType;

    const-string/jumbo v1, "REGULAR"

    const-string/jumbo v2, "regular"

    invoke-direct {v0, v1, v4, v2}, Lcom/twitter/model/profile/TranslatorType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/profile/TranslatorType;->b:Lcom/twitter/model/profile/TranslatorType;

    .line 11
    new-instance v0, Lcom/twitter/model/profile/TranslatorType;

    const-string/jumbo v1, "BADGED"

    const-string/jumbo v2, "badged"

    invoke-direct {v0, v1, v5, v2}, Lcom/twitter/model/profile/TranslatorType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/profile/TranslatorType;->c:Lcom/twitter/model/profile/TranslatorType;

    .line 12
    new-instance v0, Lcom/twitter/model/profile/TranslatorType;

    const-string/jumbo v1, "MODERATOR"

    const-string/jumbo v2, "moderator"

    invoke-direct {v0, v1, v6, v2}, Lcom/twitter/model/profile/TranslatorType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/model/profile/TranslatorType;->d:Lcom/twitter/model/profile/TranslatorType;

    .line 7
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/model/profile/TranslatorType;

    sget-object v1, Lcom/twitter/model/profile/TranslatorType;->a:Lcom/twitter/model/profile/TranslatorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/model/profile/TranslatorType;->b:Lcom/twitter/model/profile/TranslatorType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/model/profile/TranslatorType;->c:Lcom/twitter/model/profile/TranslatorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/model/profile/TranslatorType;->d:Lcom/twitter/model/profile/TranslatorType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/model/profile/TranslatorType;->e:[Lcom/twitter/model/profile/TranslatorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput-object p3, p0, Lcom/twitter/model/profile/TranslatorType;->mName:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/model/profile/TranslatorType;
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/twitter/model/profile/TranslatorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/TranslatorType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/model/profile/TranslatorType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/twitter/model/profile/TranslatorType;->e:[Lcom/twitter/model/profile/TranslatorType;

    invoke-virtual {v0}, [Lcom/twitter/model/profile/TranslatorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/model/profile/TranslatorType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/model/profile/TranslatorType;->mName:Ljava/lang/String;

    return-object v0
.end method
