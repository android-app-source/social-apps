.class public Lcom/twitter/model/util/f;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 144
    sget-object v1, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v3, v2

    invoke-static/range {v0 .. v5}, Lcom/twitter/model/util/f;->a(Ljava/lang/String;Lcom/twitter/model/core/v;Ljava/lang/Iterable;Ljava/util/List;ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/model/core/v;Ljava/lang/Iterable;Ljava/util/List;ZZ)Ljava/lang/String;
    .locals 9
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Size;
            value = 0x2L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/v;",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/twitter/model/core/d;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;ZZ)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 95
    if-nez p0, :cond_0

    .line 96
    const/4 v0, 0x0

    .line 131
    :goto_0
    return-object v0

    .line 99
    :cond_0
    if-eqz p4, :cond_1

    if-eqz p5, :cond_1

    .line 100
    invoke-static {p0, p1, p2}, Lcom/twitter/model/util/f;->a(Ljava/lang/CharSequence;Lcom/twitter/model/core/v;Ljava/lang/Iterable;)V

    .line 103
    :cond_1
    sget-object v0, Lcqa;->d:Lcqa;

    invoke-virtual {v0, p0}, Lcqa;->b(Ljava/lang/String;)Lcqa$d;

    move-result-object v0

    .line 104
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v1, v0, Lcqa$d;->a:Ljava/lang/String;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 105
    if-eqz p4, :cond_2

    if-eqz p1, :cond_2

    iget-object v1, v0, Lcqa$d;->b:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 106
    iget-object v0, v0, Lcqa$d;->b:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/twitter/model/util/f;->a(Ljava/util/List;Lcom/twitter/model/core/v;Ljava/lang/Iterable;)V

    .line 109
    :cond_2
    invoke-static {v4, p1}, Lcom/twitter/model/util/f;->a(Ljava/lang/StringBuilder;Lcom/twitter/model/core/v;)Ljava/util/List;

    move-result-object v2

    .line 110
    if-eqz p4, :cond_5

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 111
    invoke-static {v2, p1, p2}, Lcom/twitter/model/util/f;->b(Ljava/util/List;Lcom/twitter/model/core/v;Ljava/lang/Iterable;)V

    .line 112
    if-eqz p3, :cond_5

    .line 113
    invoke-interface {p3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 114
    invoke-interface {p3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 115
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    move v3, v1

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 120
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {p3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v6, v1, :cond_3

    invoke-interface {p3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eqz v1, :cond_3

    .line 121
    add-int/lit8 v3, v3, 0x1

    move v0, v2

    move v1, v3

    :goto_2
    move v2, v0

    move v3, v1

    .line 125
    goto :goto_1

    .line 122
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v1, v0, :cond_6

    .line 123
    add-int/lit8 v2, v2, 0x1

    move v0, v2

    move v1, v3

    goto :goto_2

    .line 126
    :cond_4
    invoke-interface {p3}, Ljava/util/List;->clear()V

    .line 127
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move v0, v2

    move v1, v3

    goto :goto_2
.end method

.method public static a(Ljava/lang/StringBuilder;Lcom/twitter/model/core/v;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/twitter/model/core/v;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    invoke-static {p0, p1}, Lcom/twitter/model/core/v;->b(Ljava/lang/CharSequence;Lcom/twitter/model/core/v;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/twitter/model/util/f;->b(Ljava/lang/StringBuilder;Lcom/twitter/model/core/v;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/CharSequence;Lcom/twitter/model/core/v;Ljava/lang/Iterable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Lcom/twitter/model/core/v;",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/twitter/model/core/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 149
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 157
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    .line 158
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    .line 159
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    add-int/lit8 v3, v0, 0x1

    invoke-interface {p0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 160
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 158
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 163
    :cond_3
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 165
    if-eqz p1, :cond_4

    .line 166
    iget-object v1, p1, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-static {v1, v0}, Lcom/twitter/model/util/f;->c(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 167
    iget-object v1, p1, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-static {v1, v0}, Lcom/twitter/model/util/f;->c(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 168
    iget-object v1, p1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v1, v0}, Lcom/twitter/model/util/f;->c(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 169
    iget-object v1, p1, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-static {v1, v0}, Lcom/twitter/model/util/f;->c(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 170
    iget-object v1, p1, Lcom/twitter/model/core/v;->g:Lcom/twitter/model/core/f;

    invoke-static {v1, v0}, Lcom/twitter/model/util/f;->c(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 172
    :cond_4
    if-eqz p2, :cond_0

    .line 173
    invoke-static {p2, v0}, Lcom/twitter/model/util/f;->c(Ljava/lang/Iterable;Ljava/util/List;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Iterable;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/twitter/model/core/d;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 276
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d;

    .line 277
    invoke-virtual {v0}, Lcom/twitter/model/core/d;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 281
    iget v4, v0, Lcom/twitter/model/core/d;->g:I

    .line 282
    const/4 v1, 0x0

    .line 283
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 284
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v1, v2

    if-gt v1, v4, :cond_1

    .line 285
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    .line 289
    goto :goto_1

    .line 290
    :cond_1
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/d;->b(I)V

    goto :goto_0

    .line 292
    :cond_2
    return-void
.end method

.method public static a(Ljava/util/List;Lcom/twitter/model/core/v;Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[I>;",
            "Lcom/twitter/model/core/v;",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/twitter/model/core/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p1, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-static {v0, p0}, Lcom/twitter/model/util/f;->b(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 26
    iget-object v0, p1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0, p0}, Lcom/twitter/model/util/f;->b(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 27
    iget-object v0, p1, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-static {v0, p0}, Lcom/twitter/model/util/f;->b(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 28
    iget-object v0, p1, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-static {v0, p0}, Lcom/twitter/model/util/f;->b(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 29
    iget-object v0, p1, Lcom/twitter/model/core/v;->g:Lcom/twitter/model/core/f;

    invoke-static {v0, p0}, Lcom/twitter/model/util/f;->b(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 31
    invoke-static {p2}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    invoke-static {p2, p0}, Lcom/twitter/model/util/f;->b(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 34
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/StringBuilder;Lcom/twitter/model/core/v;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/twitter/model/core/v;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v6, 0x200e

    const/4 v1, 0x0

    .line 215
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 222
    const/16 v0, 0x200f

    invoke-virtual {p0, v1, v0}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 223
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 224
    const/4 v0, 0x1

    .line 226
    if-eqz p1, :cond_3

    .line 227
    iget-object v1, p1, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    iget-object v3, p1, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    sget-object v4, Lcom/twitter/model/core/d;->e:Ljava/util/Comparator;

    invoke-static {v1, v3, v4}, Lcpt;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d;

    .line 228
    iget v4, v0, Lcom/twitter/model/core/d;->g:I

    if-ltz v4, :cond_0

    iget v4, v0, Lcom/twitter/model/core/d;->g:I

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-ge v4, v5, :cond_0

    iget v4, v0, Lcom/twitter/model/core/d;->h:I

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-gt v4, v5, :cond_0

    .line 233
    instance-of v4, v0, Lcom/twitter/model/core/q;

    if-eqz v4, :cond_1

    .line 235
    iget v0, v0, Lcom/twitter/model/core/d;->g:I

    add-int/2addr v0, v1

    .line 241
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 242
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-ne v0, v4, :cond_2

    .line 243
    invoke-virtual {p0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 247
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 248
    goto :goto_0

    .line 239
    :cond_1
    iget v0, v0, Lcom/twitter/model/core/d;->h:I

    add-int/2addr v0, v1

    goto :goto_1

    .line 245
    :cond_2
    invoke-virtual {p0, v0, v6}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 250
    :cond_3
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/twitter/model/core/d;",
            ">;",
            "Ljava/util/List",
            "<[I>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    :cond_0
    return-void

    .line 40
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    .line 51
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    move v3, v2

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d;

    move v6, v3

    move v5, v2

    move v4, v3

    move v3, v1

    .line 54
    :goto_1
    if-ge v6, v7, :cond_3

    .line 55
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    .line 56
    aget v9, v1, v2

    .line 57
    const/4 v10, 0x1

    aget v1, v1, v10

    .line 59
    sub-int v9, v1, v9

    .line 60
    iget v10, v0, Lcom/twitter/model/core/d;->g:I

    if-ge v1, v10, :cond_2

    .line 62
    add-int/2addr v3, v9

    .line 63
    add-int/lit8 v4, v4, 0x1

    move v1, v5

    .line 54
    :goto_2
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v1

    goto :goto_1

    .line 64
    :cond_2
    iget v10, v0, Lcom/twitter/model/core/d;->h:I

    if-ge v1, v10, :cond_5

    .line 65
    add-int v1, v5, v9

    goto :goto_2

    .line 69
    :cond_3
    iget v1, v0, Lcom/twitter/model/core/d;->g:I

    sub-int/2addr v1, v3

    iput v1, v0, Lcom/twitter/model/core/d;->g:I

    .line 70
    iget v1, v0, Lcom/twitter/model/core/d;->h:I

    add-int/2addr v5, v3

    sub-int/2addr v1, v5

    iput v1, v0, Lcom/twitter/model/core/d;->h:I

    .line 71
    iget v1, v0, Lcom/twitter/model/core/d;->g:I

    iget v5, v0, Lcom/twitter/model/core/d;->h:I

    if-le v1, v5, :cond_4

    .line 72
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Invalid entity indices: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/twitter/model/core/d;->g:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/twitter/model/core/d;->h:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 74
    iget v1, v0, Lcom/twitter/model/core/d;->g:I

    iput v1, v0, Lcom/twitter/model/core/d;->h:I

    :cond_4
    move v1, v3

    move v3, v4

    .line 76
    goto :goto_0

    :cond_5
    move v1, v5

    goto :goto_2
.end method

.method public static b(Ljava/util/List;Lcom/twitter/model/core/v;Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/twitter/model/core/v;",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/twitter/model/core/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 262
    if-eqz p1, :cond_0

    .line 263
    iget-object v0, p1, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-static {v0, p0}, Lcom/twitter/model/util/f;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 264
    iget-object v0, p1, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-static {v0, p0}, Lcom/twitter/model/util/f;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 265
    iget-object v0, p1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0, p0}, Lcom/twitter/model/util/f;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 266
    iget-object v0, p1, Lcom/twitter/model/core/v;->f:Lcom/twitter/model/core/f;

    invoke-static {v0, p0}, Lcom/twitter/model/util/f;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 267
    iget-object v0, p1, Lcom/twitter/model/core/v;->g:Lcom/twitter/model/core/f;

    invoke-static {v0, p0}, Lcom/twitter/model/util/f;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 269
    :cond_0
    if-eqz p2, :cond_1

    .line 270
    invoke-static {p2, p0}, Lcom/twitter/model/util/f;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 272
    :cond_1
    return-void
.end method

.method private static c(Ljava/lang/Iterable;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/twitter/model/core/d;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 179
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/d;

    .line 181
    iget v5, v0, Lcom/twitter/model/core/d;->g:I

    .line 183
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 184
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v1, v2

    if-ge v1, v5, :cond_0

    .line 185
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    .line 189
    goto :goto_1

    .line 190
    :cond_0
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/d;->b(I)V

    .line 195
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 196
    iget v6, v0, Lcom/twitter/model/core/d;->h:I

    add-int/2addr v6, v2

    if-ge v1, v6, :cond_1

    .line 197
    iget v6, v0, Lcom/twitter/model/core/d;->g:I

    if-lt v1, v6, :cond_3

    .line 198
    add-int/lit8 v1, v2, 0x1

    :goto_3
    move v2, v1

    .line 203
    goto :goto_2

    .line 204
    :cond_1
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/d;->c(I)V

    goto :goto_0

    .line 206
    :cond_2
    return-void

    :cond_3
    move v1, v2

    goto :goto_3
.end method
