.class public Lcom/twitter/model/util/e;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcbc;)I
    .locals 2

    .prologue
    .line 83
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcbc;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/high16 v0, 0x80000

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x20000

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcba;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/model/util/e$1;

    invoke-direct {v0}, Lcom/twitter/model/util/e$1;-><init>()V

    invoke-static {p0, v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 88
    const/high16 v0, 0x20000

    and-int/2addr v0, p0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/ac;)Z
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->c()Lcom/twitter/model/core/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcba;)[B
    .locals 2

    .prologue
    .line 42
    invoke-virtual {p0}, Lcba;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 48
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 44
    :pswitch_0
    check-cast p0, Lcaz;

    .line 45
    iget-object v0, p0, Lcaz;->a:Lcax;

    sget-object v1, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v0

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Lcom/twitter/model/core/ac;)I
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/ac;->A:Lcbc;

    invoke-static {v0}, Lcom/twitter/model/util/e;->a(Lcbc;)I

    move-result v0

    :goto_0
    return v0

    .line 72
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->c()Lcom/twitter/model/core/ac;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/ac;->A:Lcbc;

    invoke-static {v0}, Lcom/twitter/model/util/e;->a(Lcbc;)I

    move-result v0

    goto :goto_0
.end method

.method public static c(Lcom/twitter/model/core/ac;)J
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/twitter/model/core/ac;->A:Lcbc;

    iget-wide v0, v0, Lcbc;->a:J

    .line 104
    :goto_0
    return-wide v0

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->c()Lcom/twitter/model/core/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->c()Lcom/twitter/model/core/ac;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/ac;->A:Lcbc;

    iget-wide v0, v0, Lcbc;->a:J

    goto :goto_0

    .line 104
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/twitter/model/core/ac;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/ac;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcba;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/twitter/model/core/ac;->A:Lcbc;

    iget-object v0, v0, Lcbc;->b:Ljava/util/List;

    .line 121
    :goto_0
    return-object v0

    .line 117
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->c()Lcom/twitter/model/core/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    invoke-virtual {p0}, Lcom/twitter/model/core/ac;->c()Lcom/twitter/model/core/ac;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/ac;->A:Lcbc;

    iget-object v0, v0, Lcbc;->b:Ljava/util/List;

    goto :goto_0

    .line 121
    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
