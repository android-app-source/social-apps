.class public Lcom/twitter/model/util/h;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcax;)I
    .locals 2

    .prologue
    const/16 v0, 0x8

    .line 97
    const/4 v1, 0x0

    .line 99
    if-eqz p0, :cond_6

    .line 100
    invoke-virtual {p0}, Lcax;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 101
    const/16 v0, 0x4004

    .line 113
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcax;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 114
    const v1, 0x8000

    or-int/2addr v0, v1

    .line 116
    :cond_1
    invoke-virtual {p0}, Lcax;->p()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 117
    or-int/lit16 v0, v0, 0x1000

    .line 120
    :cond_2
    :goto_1
    return v0

    .line 102
    :cond_3
    invoke-virtual {p0}, Lcax;->r()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 103
    const/4 v0, 0x4

    goto :goto_0

    .line 104
    :cond_4
    invoke-virtual {p0}, Lcax;->x()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 105
    const/16 v0, 0x20

    goto :goto_0

    .line 106
    :cond_5
    invoke-virtual {p0}, Lcax;->v()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcax;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public static a(Lcom/twitter/model/core/ac;)I
    .locals 2

    .prologue
    .line 21
    const/4 v0, 0x0

    .line 22
    iget-object v1, p0, Lcom/twitter/model/core/ac;->w:Lcgi;

    if-eqz v1, :cond_2

    .line 23
    const/4 v0, 0x1

    .line 24
    iget-object v1, p0, Lcom/twitter/model/core/ac;->w:Lcgi;

    invoke-virtual {v1}, Lcgi;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    const/4 v0, 0x5

    .line 27
    :cond_0
    iget-object v1, p0, Lcom/twitter/model/core/ac;->w:Lcgi;

    invoke-virtual {v1}, Lcgi;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 28
    or-int/lit8 v0, v0, 0x2

    .line 30
    :cond_1
    iget-object v1, p0, Lcom/twitter/model/core/ac;->w:Lcgi;

    iget-boolean v1, v1, Lcgi;->l:Z

    if-eqz v1, :cond_2

    .line 31
    or-int/lit8 v0, v0, 0x10

    .line 34
    :cond_2
    iget-object v1, p0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    iget-boolean v1, v1, Lcom/twitter/model/search/e;->b:Z

    if-eqz v1, :cond_3

    .line 35
    or-int/lit8 v0, v0, 0x8

    .line 37
    :cond_3
    return v0
.end method

.method public static a(Lcom/twitter/model/core/ac;J)I
    .locals 5

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 44
    iget-object v1, p0, Lcom/twitter/model/core/ac;->z:Lcax;

    .line 45
    if-eqz v1, :cond_0

    .line 46
    invoke-static {v1}, Lcom/twitter/model/util/h;->a(Lcax;)I

    move-result v1

    or-int/2addr v0, v1

    .line 49
    :cond_0
    iget-boolean v1, p0, Lcom/twitter/model/core/ac;->f:Z

    if-eqz v1, :cond_1

    .line 50
    or-int/lit8 v0, v0, 0x10

    .line 52
    :cond_1
    iget-object v1, p0, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    .line 53
    invoke-virtual {v1, p1, p2}, Lcom/twitter/model/core/v;->a(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 54
    or-int/lit8 v0, v0, 0x2

    .line 56
    :cond_2
    iget-object v2, v1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    sget-object v3, Lcom/twitter/model/core/MediaEntity$Type;->b:Lcom/twitter/model/core/MediaEntity$Type;

    invoke-virtual {v2, v3}, Lcom/twitter/model/core/k;->a(Lcom/twitter/model/core/MediaEntity$Type;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 57
    or-int/lit8 v0, v0, 0x1

    .line 59
    :cond_3
    iget-object v2, v1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    sget-object v3, Lcom/twitter/model/core/MediaEntity$Type;->c:Lcom/twitter/model/core/MediaEntity$Type;

    invoke-virtual {v2, v3}, Lcom/twitter/model/core/k;->a(Lcom/twitter/model/core/MediaEntity$Type;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 60
    or-int/lit16 v0, v0, 0x200

    .line 62
    :cond_4
    iget-object v2, v1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    sget-object v3, Lcom/twitter/model/core/MediaEntity$Type;->d:Lcom/twitter/model/core/MediaEntity$Type;

    invoke-virtual {v2, v3}, Lcom/twitter/model/core/k;->a(Lcom/twitter/model/core/MediaEntity$Type;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 63
    or-int/lit16 v0, v0, 0x400

    .line 65
    :cond_5
    iget-object v1, v1, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v1}, Lcom/twitter/model/core/k;->f()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 66
    or-int/lit16 v0, v0, 0x1000

    .line 69
    :cond_6
    iget-boolean v1, p0, Lcom/twitter/model/core/ac;->s:Z

    if-eqz v1, :cond_7

    .line 70
    or-int/lit8 v0, v0, 0x40

    .line 73
    :cond_7
    iget-boolean v1, p0, Lcom/twitter/model/core/ac;->t:Z

    if-eqz v1, :cond_8

    .line 74
    or-int/lit16 v0, v0, 0x800

    .line 77
    :cond_8
    iget-boolean v1, p0, Lcom/twitter/model/core/ac;->u:Z

    if-eqz v1, :cond_9

    .line 78
    or-int/lit16 v0, v0, 0x80

    .line 81
    :cond_9
    iget-boolean v1, p0, Lcom/twitter/model/core/ac;->K:Z

    if-eqz v1, :cond_a

    .line 82
    or-int/lit16 v0, v0, 0x2000

    .line 85
    :cond_a
    invoke-static {p0}, Lcom/twitter/model/util/e;->a(Lcom/twitter/model/core/ac;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 86
    iget-object v1, p0, Lcom/twitter/model/core/ac;->A:Lcbc;

    invoke-static {v1}, Lcom/twitter/model/util/e;->a(Lcbc;)I

    move-result v1

    or-int/2addr v0, v1

    .line 89
    :cond_b
    iget-boolean v1, p0, Lcom/twitter/model/core/ac;->c:Z

    if-eqz v1, :cond_c

    .line 90
    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    .line 92
    :cond_c
    return v0
.end method
