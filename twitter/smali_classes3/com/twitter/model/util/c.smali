.class public Lcom/twitter/model/util/c;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity$Type;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lcpv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpv",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Lcpv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpv",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Lcpv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpv",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39
    const-string/jumbo v0, "application/x-mpegURL"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "application/dash+xml"

    aput-object v2, v1, v4

    const-string/jumbo v2, "video/mp4"

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-string/jumbo v3, "video/webm"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/util/c;->a:Ljava/util/List;

    .line 46
    sget-object v0, Lcom/twitter/model/core/MediaEntity$Type;->d:Lcom/twitter/model/core/MediaEntity$Type;

    new-array v1, v5, [Lcom/twitter/model/core/MediaEntity$Type;

    sget-object v2, Lcom/twitter/model/core/MediaEntity$Type;->c:Lcom/twitter/model/core/MediaEntity$Type;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/util/c;->b:Ljava/util/List;

    .line 51
    sget-object v0, Lcom/twitter/model/core/MediaEntity$Type;->d:Lcom/twitter/model/core/MediaEntity$Type;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/util/c;->c:Ljava/util/List;

    .line 55
    sget-object v0, Lcom/twitter/model/core/MediaEntity$Type;->c:Lcom/twitter/model/core/MediaEntity$Type;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/util/c;->d:Ljava/util/List;

    .line 59
    new-instance v0, Lcom/twitter/model/util/c$1;

    invoke-direct {v0}, Lcom/twitter/model/util/c$1;-><init>()V

    sput-object v0, Lcom/twitter/model/util/c;->e:Lcpv;

    .line 67
    new-instance v0, Lcom/twitter/model/util/c$2;

    invoke-direct {v0}, Lcom/twitter/model/util/c$2;-><init>()V

    sput-object v0, Lcom/twitter/model/util/c;->f:Lcpv;

    .line 75
    new-instance v0, Lcom/twitter/model/util/c$3;

    invoke-direct {v0}, Lcom/twitter/model/util/c$3;-><init>()V

    sput-object v0, Lcom/twitter/model/util/c;->g:Lcpv;

    return-void
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)",
            "Lcom/twitter/model/core/MediaEntity;"
        }
    .end annotation

    .prologue
    .line 129
    invoke-static {p0}, Lcom/twitter/model/util/c;->b(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    invoke-static {p0, v0}, Lcom/twitter/model/util/c;->b(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Iterable;Ljava/util/List;)Lcom/twitter/model/core/MediaEntity;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity$Type;",
            ">;)",
            "Lcom/twitter/model/core/MediaEntity;"
        }
    .end annotation

    .prologue
    .line 194
    const/4 v1, 0x0

    .line 195
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 196
    invoke-static {v0}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    invoke-interface {p1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 197
    iget-wide v4, v0, Lcom/twitter/model/core/MediaEntity;->j:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 205
    :goto_1
    return-object v0

    .line 199
    :cond_0
    if-nez v1, :cond_2

    :goto_2
    move-object v1, v0

    .line 203
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 205
    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method public static varargs a(Ljava/lang/Iterable;[Lcom/twitter/media/model/MediaType;)Lcom/twitter/model/media/EditableMedia;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/media/EditableMedia;",
            ">;[",
            "Lcom/twitter/media/model/MediaType;",
            ")",
            "Lcom/twitter/model/media/EditableMedia;"
        }
    .end annotation

    .prologue
    .line 409
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableMedia;

    .line 410
    invoke-virtual {v0}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v3

    .line 411
    array-length v4, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, p1, v1

    .line 412
    if-ne v3, v5, :cond_1

    .line 417
    :goto_1
    return-object v0

    .line 411
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 417
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a()Lcpv;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/twitter/model/util/c;->e:Lcpv;

    return-object v0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;JLcom/twitter/util/math/Size;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "J",
            "Lcom/twitter/util/math/Size;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/core/Tweet;->a(J)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/util/math/Size;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0, p1}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;",
            "Lcom/twitter/util/math/Size;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    invoke-static {p0}, Lcom/twitter/model/util/c;->b(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 120
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 123
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;Lcpv;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;",
            "Lcom/twitter/util/math/Size;",
            "Lcpv",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 286
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v6

    .line 290
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v2, v4

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 291
    invoke-interface {p2, v0}, Lcpv;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {v0, p1}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/MediaEntity;Lcom/twitter/util/math/Size;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 292
    iget-wide v8, v0, Lcom/twitter/model/core/MediaEntity;->j:J

    cmp-long v1, v8, v4

    if-nez v1, :cond_0

    .line 293
    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-wide v0, v2

    :goto_1
    move-wide v2, v0

    .line 298
    goto :goto_0

    .line 294
    :cond_0
    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    .line 295
    iget-wide v2, v0, Lcom/twitter/model/core/MediaEntity;->j:J

    move-wide v0, v2

    goto :goto_1

    .line 300
    :cond_1
    invoke-virtual {v6}, Lcom/twitter/util/collection/h;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    .line 302
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 303
    invoke-interface {p2, v0}, Lcpv;->a(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v0, p1}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/MediaEntity;Lcom/twitter/util/math/Size;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 304
    iget-wide v4, v0, Lcom/twitter/model/core/MediaEntity;->j:J

    cmp-long v4, v4, v2

    if-nez v4, :cond_2

    .line 305
    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_2

    .line 311
    :cond_3
    invoke-virtual {v6}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0

    :cond_4
    move-wide v0, v2

    goto :goto_1
.end method

.method public static a(Lcom/twitter/model/core/MediaEntity$Type;)Z
    .locals 1

    .prologue
    .line 376
    sget-object v0, Lcom/twitter/model/core/MediaEntity$Type;->d:Lcom/twitter/model/core/MediaEntity$Type;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/twitter/model/core/MediaEntity$Type;->c:Lcom/twitter/model/core/MediaEntity$Type;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/MediaEntity;)Z
    .locals 2

    .prologue
    .line 258
    invoke-static {p0}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    sget-object v1, Lcom/twitter/model/core/MediaEntity$Type;->c:Lcom/twitter/model/core/MediaEntity$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/MediaEntity;Lcom/twitter/util/math/Size;)Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->o:Lcom/twitter/util/math/Size;

    invoke-virtual {v0, p1}, Lcom/twitter/util/math/Size;->b(Lcom/twitter/util/math/Size;)Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 262
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    invoke-static {p0, v0}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->I()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)",
            "Lcom/twitter/model/core/MediaEntity;"
        }
    .end annotation

    .prologue
    .line 139
    const/4 v1, 0x0

    .line 140
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 141
    iget-wide v4, v0, Lcom/twitter/model/core/MediaEntity;->j:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 149
    :goto_1
    return-object v0

    .line 144
    :cond_0
    if-nez v1, :cond_2

    :goto_2
    move-object v1, v0

    .line 147
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 149
    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method public static b(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Lcom/twitter/model/core/MediaEntity;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;",
            "Lcom/twitter/util/math/Size;",
            ")",
            "Lcom/twitter/model/core/MediaEntity;"
        }
    .end annotation

    .prologue
    .line 160
    const/4 v1, 0x0

    .line 161
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 162
    invoke-static {v0}, Lcom/twitter/model/util/c;->b(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v0, p1}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/MediaEntity;Lcom/twitter/util/math/Size;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 165
    :cond_1
    iget-wide v4, v0, Lcom/twitter/model/core/MediaEntity;->j:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    .line 173
    :goto_1
    return-object v0

    .line 168
    :cond_2
    if-nez v1, :cond_4

    :goto_2
    move-object v1, v0

    .line 171
    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 173
    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method static synthetic b()Lcpv;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/twitter/model/util/c;->f:Lcpv;

    return-object v0
.end method

.method public static b(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/util/math/Size;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0, p1}, Lcom/twitter/model/util/c;->e(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/twitter/model/core/MediaEntity;)Z
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    sget-object v1, Lcom/twitter/model/core/MediaEntity$Type;->b:Lcom/twitter/model/core/MediaEntity$Type;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->m:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)",
            "Lcom/twitter/model/core/MediaEntity;"
        }
    .end annotation

    .prologue
    .line 178
    sget-object v0, Lcom/twitter/model/util/c;->b:Ljava/util/List;

    invoke-static {p0, v0}, Lcom/twitter/model/util/c;->a(Ljava/lang/Iterable;Ljava/util/List;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/twitter/model/core/MediaEntity;)Z
    .locals 3

    .prologue
    .line 330
    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    invoke-static {v0}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/MediaEntity$Type;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    iget-object v0, v0, Lcom/twitter/model/core/o;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/p;

    .line 332
    sget-object v2, Lcom/twitter/model/util/c;->a:Ljava/util/List;

    iget-object v0, v0, Lcom/twitter/model/core/p;->d:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    const/4 v0, 0x1

    .line 337
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Z
    .locals 1

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0, p1}, Lcom/twitter/model/util/c;->c(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Z

    move-result v0

    return v0
.end method

.method public static c(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;",
            "Lcom/twitter/util/math/Size;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 209
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 210
    invoke-static {v0}, Lcom/twitter/model/util/c;->b(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0, p1}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/MediaEntity;Lcom/twitter/util/math/Size;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    const/4 v0, 0x1

    .line 214
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)",
            "Lcom/twitter/model/core/MediaEntity;"
        }
    .end annotation

    .prologue
    .line 183
    sget-object v0, Lcom/twitter/model/util/c;->c:Ljava/util/List;

    invoke-static {p0, v0}, Lcom/twitter/model/util/c;->a(Ljava/lang/Iterable;Ljava/util/List;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;",
            "Lcom/twitter/util/math/Size;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    sget-object v0, Lcom/twitter/model/util/c;->e:Lcpv;

    invoke-static {p0, p1, v0}, Lcom/twitter/model/util/c;->a(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;Lcpv;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lcom/twitter/model/core/MediaEntity;)Z
    .locals 2

    .prologue
    .line 341
    const-string/jumbo v0, "android_looping_video_5578"

    const-string/jumbo v1, "looping_enabled"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    invoke-static {p0}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    iget v0, v0, Lcom/twitter/model/core/o;->c:F

    const/high16 v1, 0x40d00000    # 6.5f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    .line 341
    :goto_0
    return v0

    .line 342
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)",
            "Lcom/twitter/model/core/MediaEntity;"
        }
    .end annotation

    .prologue
    .line 188
    sget-object v0, Lcom/twitter/model/util/c;->d:Ljava/util/List;

    invoke-static {p0, v0}, Lcom/twitter/model/util/c;->a(Ljava/lang/Iterable;Ljava/util/List;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lcom/twitter/model/core/MediaEntity;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const v8, 0xf4240

    const/4 v2, 0x0

    .line 348
    invoke-static {p0}, Lcom/twitter/model/util/c;->d(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    if-nez v1, :cond_2

    :cond_0
    move-object v1, v0

    .line 372
    :cond_1
    :goto_0
    return-object v1

    .line 354
    :cond_2
    iget-object v1, p0, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    iget-object v1, v1, Lcom/twitter/model/core/o;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    move-object v3, v0

    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/p;

    .line 355
    const-string/jumbo v4, "video/mp4"

    iget-object v7, v0, Lcom/twitter/model/core/p;->d:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 359
    if-gt v1, v8, :cond_6

    iget v4, v0, Lcom/twitter/model/core/p;->b:I

    if-le v4, v1, :cond_5

    iget v4, v0, Lcom/twitter/model/core/p;->b:I

    if-gt v4, v8, :cond_5

    move v4, v5

    .line 362
    :goto_2
    if-eqz v1, :cond_4

    if-eqz v4, :cond_8

    .line 363
    :cond_4
    iget v1, v0, Lcom/twitter/model/core/p;->b:I

    .line 364
    iget-object v3, v0, Lcom/twitter/model/core/p;->c:Ljava/lang/String;

    move v0, v1

    move-object v1, v3

    .line 367
    :goto_3
    if-eq v0, v8, :cond_1

    move-object v3, v1

    move v1, v0

    .line 370
    goto :goto_1

    :cond_5
    move v4, v2

    .line 359
    goto :goto_2

    :cond_6
    iget v4, v0, Lcom/twitter/model/core/p;->b:I

    if-ge v4, v1, :cond_7

    move v4, v5

    goto :goto_2

    :cond_7
    move v4, v2

    goto :goto_2

    :cond_8
    move v0, v1

    move-object v1, v3

    goto :goto_3

    :cond_9
    move-object v1, v3

    goto :goto_0
.end method

.method public static e(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;",
            "Lcom/twitter/util/math/Size;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 274
    sget-object v0, Lcom/twitter/model/util/c;->f:Lcpv;

    invoke-static {p0, p1, v0}, Lcom/twitter/model/util/c;->a(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;Lcpv;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static f(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;",
            "Lcom/twitter/util/math/Size;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 280
    sget-object v0, Lcom/twitter/model/util/c;->g:Lcpv;

    invoke-static {p0, p1, v0}, Lcom/twitter/model/util/c;->a(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;Lcpv;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static f(Lcom/twitter/model/core/MediaEntity;)Z
    .locals 1

    .prologue
    .line 421
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->z:Lcom/twitter/model/core/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->z:Lcom/twitter/model/core/l;

    iget-boolean v0, v0, Lcom/twitter/model/core/l;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Ljava/lang/Iterable;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 222
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 223
    invoke-static {v0}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    const/4 v0, 0x1

    .line 227
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Ljava/lang/Iterable;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 231
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 232
    invoke-static {v0}, Lcom/twitter/model/util/c;->d(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    sget-object v2, Lcom/twitter/model/core/MediaEntity$Type;->d:Lcom/twitter/model/core/MediaEntity$Type;

    if-ne v0, v2, :cond_0

    .line 233
    const/4 v0, 0x1

    .line 236
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Ljava/lang/Iterable;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 240
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 241
    invoke-static {v0}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    sget-object v2, Lcom/twitter/model/core/MediaEntity$Type;->d:Lcom/twitter/model/core/MediaEntity$Type;

    if-ne v0, v2, :cond_0

    .line 242
    const/4 v0, 0x1

    .line 245
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Ljava/lang/Iterable;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 249
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 250
    invoke-static {v0}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    const/4 v0, 0x1

    .line 254
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 316
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 317
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 318
    iget-object v3, v0, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    sget-object v4, Lcom/twitter/model/core/MediaEntity$Type;->b:Lcom/twitter/model/core/MediaEntity$Type;

    if-ne v3, v4, :cond_0

    .line 319
    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 322
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static k(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381
    invoke-static {p0}, Lcom/twitter/model/util/c;->j(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 382
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 383
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 384
    iget-wide v4, v0, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 386
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static l(Ljava/lang/Iterable;)Lcom/twitter/model/media/EditableMedia;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/media/EditableMedia;",
            ">;)",
            "Lcom/twitter/model/media/EditableMedia;"
        }
    .end annotation

    .prologue
    .line 402
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/twitter/media/model/MediaType;

    const/4 v1, 0x0

    sget-object v2, Lcom/twitter/media/model/MediaType;->d:Lcom/twitter/media/model/MediaType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/twitter/media/model/MediaType;->f:Lcom/twitter/media/model/MediaType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/twitter/media/model/MediaType;->c:Lcom/twitter/media/model/MediaType;

    aput-object v2, v0, v1

    invoke-static {p0, v0}, Lcom/twitter/model/util/c;->a(Ljava/lang/Iterable;[Lcom/twitter/media/model/MediaType;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    return-object v0
.end method
