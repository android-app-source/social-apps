.class public Lcom/twitter/model/util/a;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Lcom/twitter/model/core/v;

.field private final e:Lcax;

.field private final f:Z

.field private final g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/twitter/model/util/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/model/util/a;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;ZLcom/twitter/model/core/v;Lcax;ZZ)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/model/util/a;->m:Z

    .line 95
    iput-object p1, p0, Lcom/twitter/model/util/a;->b:Ljava/lang/String;

    .line 96
    iput-boolean p2, p0, Lcom/twitter/model/util/a;->c:Z

    .line 97
    iput-object p3, p0, Lcom/twitter/model/util/a;->d:Lcom/twitter/model/core/v;

    .line 98
    iput-object p4, p0, Lcom/twitter/model/util/a;->e:Lcax;

    .line 99
    iput-boolean p5, p0, Lcom/twitter/model/util/a;->f:Z

    .line 100
    iput-boolean p6, p0, Lcom/twitter/model/util/a;->g:Z

    .line 101
    return-void
.end method

.method static a(Lcax;Ljava/lang/Iterable;)Lcom/twitter/model/core/ad;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcax;",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/ad;",
            ">;)",
            "Lcom/twitter/model/core/ad;"
        }
    .end annotation

    .prologue
    .line 284
    if-eqz p0, :cond_3

    .line 285
    invoke-virtual {p0}, Lcax;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "amplify"

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    invoke-virtual {p0}, Lcax;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "video"

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 287
    :goto_0
    if-eqz v0, :cond_3

    .line 288
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 289
    iget-object v2, v0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    invoke-virtual {p0}, Lcax;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 295
    :goto_1
    return-object v0

    .line 286
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 295
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/ad;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/ad;",
            ">;)",
            "Lcom/twitter/model/core/ad;"
        }
    .end annotation

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/twitter/model/core/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 311
    invoke-virtual {p0, v1}, Lcom/twitter/model/core/f;->a(I)Lcom/twitter/model/core/d;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 312
    sget-object v2, Lcom/twitter/model/util/g;->g:Ljava/util/regex/Pattern;

    iget-object v3, v0, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/twitter/model/util/g;->f:Ljava/util/regex/Pattern;

    iget-object v3, v0, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    .line 313
    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 317
    :cond_0
    :goto_1
    return-object v0

    .line 310
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 317
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/twitter/model/core/ad;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/ad;",
            ">;)",
            "Lcom/twitter/model/core/ad;"
        }
    .end annotation

    .prologue
    .line 300
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 301
    iget-object v2, v0, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    const-string/jumbo v3, "cards.twitter.com/cards/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 305
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/util/a;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 51
    new-instance v0, Lcom/twitter/model/util/a;

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v3

    .line 52
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->R()Z

    move-result v5

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/model/util/a;-><init>(Ljava/lang/String;ZLcom/twitter/model/core/v;Lcax;ZZ)V

    .line 51
    return-object v0
.end method

.method public static a(Lcom/twitter/model/core/r;)Lcom/twitter/model/util/a;
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 88
    new-instance v0, Lcom/twitter/model/util/a;

    iget-object v1, p0, Lcom/twitter/model/core/r;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-object v4, p0, Lcom/twitter/model/core/r;->k:Lcax;

    const/4 v5, 0x0

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/model/util/a;-><init>(Ljava/lang/String;ZLcom/twitter/model/core/v;Lcax;ZZ)V

    .line 90
    invoke-virtual {v0, v2}, Lcom/twitter/model/util/a;->f(Z)Lcom/twitter/model/util/a;

    move-result-object v0

    .line 88
    return-object v0
.end method

.method static a(Ljava/lang/String;Lcom/twitter/model/core/ad;Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 270
    if-eqz p2, :cond_0

    iget-object v0, p1, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    .line 272
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 270
    :cond_0
    iget-object v0, p1, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 323
    const-string/jumbo v0, "cards.twitter.com/cards/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private b()I
    .locals 4

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/model/util/a;->d:Lcom/twitter/model/core/v;

    iget-object v1, v0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    .line 154
    const/4 v0, 0x0

    .line 155
    invoke-virtual {v1}, Lcom/twitter/model/core/f;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 156
    sget-object v3, Lcom/twitter/model/util/g;->e:Ljava/util/regex/Pattern;

    iget-object v0, v0, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 159
    goto :goto_0

    .line 160
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static b(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/util/a;
    .locals 7

    .prologue
    .line 63
    new-instance v0, Lcom/twitter/model/util/a;

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->g()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v3

    .line 64
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->R()Z

    move-result v5

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/model/util/a;-><init>(Ljava/lang/String;ZLcom/twitter/model/core/v;Lcax;ZZ)V

    .line 63
    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/model/core/e;
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 165
    iget-object v0, p0, Lcom/twitter/model/util/a;->d:Lcom/twitter/model/core/v;

    iget-object v3, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    .line 168
    invoke-static {v3}, Lcom/twitter/model/util/c;->a(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v8

    .line 169
    iget-object v5, p0, Lcom/twitter/model/util/a;->b:Ljava/lang/String;

    .line 171
    iget-boolean v0, p0, Lcom/twitter/model/util/a;->h:Z

    if-eqz v0, :cond_3

    if-nez v8, :cond_0

    iget-object v0, p0, Lcom/twitter/model/util/a;->e:Lcax;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/model/util/a;->e:Lcax;

    .line 172
    invoke-virtual {v0}, Lcax;->t()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/util/a;->e:Lcax;

    invoke-virtual {v0}, Lcax;->B()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v1

    .line 174
    :goto_0
    invoke-static {v3}, Lcom/twitter/model/util/c;->f(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/twitter/model/util/a;->e:Lcax;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/twitter/model/util/a;->e:Lcax;

    .line 175
    invoke-virtual {v3}, Lcax;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_1
    const-string/jumbo v3, "strip_amplify_urls_from_tweet_text_enabled"

    .line 176
    invoke-static {v3}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v1

    .line 177
    :goto_1
    iget-boolean v4, p0, Lcom/twitter/model/util/a;->k:Z

    if-eqz v4, :cond_5

    iget-boolean v4, p0, Lcom/twitter/model/util/a;->f:Z

    if-eqz v4, :cond_5

    move v7, v1

    .line 181
    :goto_2
    if-nez v0, :cond_2

    if-nez v3, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/util/a;->i:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/util/a;->l:Z

    if-nez v0, :cond_2

    if-nez v7, :cond_2

    iget-boolean v0, p0, Lcom/twitter/model/util/a;->n:Z

    if-eqz v0, :cond_6

    :cond_2
    move v0, v1

    .line 183
    :goto_3
    iget-object v4, p0, Lcom/twitter/model/util/a;->d:Lcom/twitter/model/core/v;

    invoke-virtual {v4}, Lcom/twitter/model/core/v;->a()Z

    move-result v4

    if-eqz v4, :cond_17

    .line 184
    if-eqz v0, :cond_1c

    .line 186
    iget-object v0, p0, Lcom/twitter/model/util/a;->d:Lcom/twitter/model/core/v;

    invoke-virtual {v0}, Lcom/twitter/model/core/v;->b()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 187
    sget-boolean v4, Lcom/twitter/model/util/a;->a:Z

    if-nez v4, :cond_7

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    move v0, v2

    .line 172
    goto :goto_0

    :cond_4
    move v3, v2

    .line 176
    goto :goto_1

    :cond_5
    move v7, v2

    .line 177
    goto :goto_2

    :cond_6
    move v0, v2

    .line 181
    goto :goto_3

    .line 189
    :cond_7
    const/16 v4, 0x200e

    invoke-static {v4}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 191
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v5, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 194
    :goto_4
    iget-object v9, p0, Lcom/twitter/model/util/a;->e:Lcax;

    if-eqz v9, :cond_d

    iget-object v9, v0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    iget-object v10, p0, Lcom/twitter/model/util/a;->e:Lcax;

    .line 195
    invoke-virtual {v10}, Lcax;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_8

    iget-object v9, v0, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    iget-object v10, p0, Lcom/twitter/model/util/a;->e:Lcax;

    invoke-virtual {v10}, Lcax;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    :cond_8
    iget-boolean v9, p0, Lcom/twitter/model/util/a;->g:Z

    if-eqz v9, :cond_9

    iget-object v9, v0, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    .line 196
    invoke-static {v9}, Lcom/twitter/model/util/a;->a(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_d

    :cond_9
    move v9, v1

    .line 197
    :goto_5
    if-eqz v8, :cond_e

    iget-object v10, v0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    iget-object v8, v8, Lcom/twitter/model/core/MediaEntity;->E:Ljava/lang/String;

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    move v8, v1

    .line 198
    :goto_6
    if-eqz v7, :cond_f

    invoke-direct {p0}, Lcom/twitter/model/util/a;->b()I

    move-result v7

    if-ne v7, v1, :cond_f

    sget-object v7, Lcom/twitter/model/util/g;->e:Ljava/util/regex/Pattern;

    iget-object v10, v0, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    .line 199
    invoke-virtual {v7, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_f

    move v7, v1

    .line 201
    :goto_7
    if-nez v8, :cond_a

    if-nez v9, :cond_a

    if-eqz v7, :cond_10

    .line 203
    :cond_a
    :goto_8
    if-eqz v1, :cond_b

    iget-boolean v1, p0, Lcom/twitter/model/util/a;->c:Z

    if-eqz v1, :cond_11

    iget-object v1, v0, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    :goto_9
    invoke-virtual {v4, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 206
    iget-boolean v1, p0, Lcom/twitter/model/util/a;->c:Z

    if-eqz v1, :cond_12

    iget v0, v0, Lcom/twitter/model/core/ad;->H:I

    :goto_a
    invoke-virtual {v4, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 211
    :cond_b
    iget-boolean v0, p0, Lcom/twitter/model/util/a;->n:Z

    if-eqz v0, :cond_c

    .line 212
    iget-object v0, p0, Lcom/twitter/model/util/a;->d:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    .line 213
    invoke-static {v0}, Lcom/twitter/model/util/a;->a(Lcom/twitter/model/core/f;)Lcom/twitter/model/core/ad;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_c

    .line 215
    iget-boolean v1, p0, Lcom/twitter/model/util/a;->c:Z

    invoke-static {v5, v0, v1}, Lcom/twitter/model/util/a;->a(Ljava/lang/String;Lcom/twitter/model/core/ad;Z)Ljava/lang/String;

    move-result-object v5

    .line 220
    :cond_c
    if-eqz v3, :cond_13

    iget-object v0, p0, Lcom/twitter/model/util/a;->e:Lcax;

    iget-object v1, p0, Lcom/twitter/model/util/a;->d:Lcom/twitter/model/core/v;

    iget-object v1, v1, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    .line 221
    invoke-static {v0, v1}, Lcom/twitter/model/util/a;->a(Lcax;Ljava/lang/Iterable;)Lcom/twitter/model/core/ad;

    move-result-object v0

    move-object v3, v0

    .line 223
    :goto_b
    if-eqz v3, :cond_1c

    iget-object v0, p0, Lcom/twitter/model/util/a;->e:Lcax;

    if-eqz v0, :cond_1c

    .line 225
    iget-boolean v0, p0, Lcom/twitter/model/util/a;->c:Z

    invoke-static {v5, v3, v0}, Lcom/twitter/model/util/a;->a(Ljava/lang/String;Lcom/twitter/model/core/ad;Z)Ljava/lang/String;

    move-result-object v5

    .line 226
    iget-boolean v0, p0, Lcom/twitter/model/util/a;->m:Z

    if-eqz v0, :cond_1c

    .line 227
    iget-boolean v0, p0, Lcom/twitter/model/util/a;->c:Z

    if-eqz v0, :cond_14

    iget-object v0, v3, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    move-object v1, v0

    .line 229
    :goto_c
    new-instance v0, Lcom/twitter/model/core/v$a;

    iget-object v4, p0, Lcom/twitter/model/util/a;->d:Lcom/twitter/model/core/v;

    invoke-direct {v0, v4}, Lcom/twitter/model/core/v$a;-><init>(Lcom/twitter/model/core/v;)V

    .line 230
    invoke-virtual {v0, v3}, Lcom/twitter/model/core/v$a;->b(Lcom/twitter/model/core/ad;)Lcom/twitter/model/core/v$a;

    move-result-object v0

    .line 231
    invoke-virtual {v0}, Lcom/twitter/model/core/v$a;->e()Lcom/twitter/model/core/v$a;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Lcom/twitter/model/core/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    .line 233
    iget-boolean v4, p0, Lcom/twitter/model/util/a;->c:Z

    if-eqz v4, :cond_15

    iget v3, v3, Lcom/twitter/model/core/ad;->H:I

    .line 234
    :goto_d
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    neg-int v1, v1

    .line 233
    invoke-virtual {v0, v3, v1}, Lcom/twitter/model/core/v;->a(II)V

    move-object v1, v0

    .line 239
    :goto_e
    iget-boolean v0, p0, Lcom/twitter/model/util/a;->j:Z

    if-eqz v0, :cond_18

    .line 240
    iget-object v0, p0, Lcom/twitter/model/util/a;->d:Lcom/twitter/model/core/v;

    invoke-virtual {v0}, Lcom/twitter/model/core/v;->b()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 241
    iget-boolean v4, p0, Lcom/twitter/model/util/a;->c:Z

    if-eqz v4, :cond_16

    iget-object v4, v0, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    .line 242
    :goto_10
    iget-boolean v4, p0, Lcom/twitter/model/util/a;->c:Z

    invoke-static {v5, v0, v4}, Lcom/twitter/model/util/a;->a(Ljava/lang/String;Lcom/twitter/model/core/ad;Z)Ljava/lang/String;

    move-result-object v5

    goto :goto_f

    :cond_d
    move v9, v2

    .line 196
    goto/16 :goto_5

    :cond_e
    move v8, v2

    .line 197
    goto/16 :goto_6

    :cond_f
    move v7, v2

    .line 199
    goto/16 :goto_7

    :cond_10
    move v1, v2

    .line 201
    goto/16 :goto_8

    .line 203
    :cond_11
    iget-object v1, v0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    goto/16 :goto_9

    .line 206
    :cond_12
    iget v0, v0, Lcom/twitter/model/core/ad;->g:I

    goto/16 :goto_a

    .line 221
    :cond_13
    iget-object v0, p0, Lcom/twitter/model/util/a;->d:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    .line 222
    invoke-static {v0}, Lcom/twitter/model/util/a;->a(Ljava/lang/Iterable;)Lcom/twitter/model/core/ad;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_b

    .line 227
    :cond_14
    iget-object v0, v3, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    move-object v1, v0

    goto :goto_c

    .line 233
    :cond_15
    iget v3, v3, Lcom/twitter/model/core/ad;->g:I

    goto :goto_d

    .line 241
    :cond_16
    iget-object v4, v0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    goto :goto_10

    :cond_17
    move-object v1, v6

    .line 247
    :cond_18
    invoke-static {v5}, Lcom/twitter/util/y;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 249
    iget-boolean v3, p0, Lcom/twitter/model/util/a;->j:Z

    if-eqz v3, :cond_19

    const-string/jumbo v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 250
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 252
    :cond_19
    iget-boolean v2, p0, Lcom/twitter/model/util/a;->m:Z

    if-nez v2, :cond_1a

    .line 253
    new-instance v1, Lcom/twitter/model/core/e;

    invoke-direct {v1, v0, v6}, Lcom/twitter/model/core/e;-><init>(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    move-object v0, v1

    .line 255
    :goto_11
    return-object v0

    :cond_1a
    new-instance v2, Lcom/twitter/model/core/e;

    if-eqz v1, :cond_1b

    :goto_12
    invoke-direct {v2, v0, v1}, Lcom/twitter/model/core/e;-><init>(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    move-object v0, v2

    goto :goto_11

    :cond_1b
    iget-object v1, p0, Lcom/twitter/model/util/a;->d:Lcom/twitter/model/core/v;

    goto :goto_12

    :cond_1c
    move-object v1, v6

    goto/16 :goto_e

    :cond_1d
    move-object v4, v5

    goto/16 :goto_4
.end method

.method public a(Z)Lcom/twitter/model/util/a;
    .locals 0

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/twitter/model/util/a;->h:Z

    .line 106
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/util/a;
    .locals 0

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/twitter/model/util/a;->i:Z

    .line 112
    return-object p0
.end method

.method public c(Z)Lcom/twitter/model/util/a;
    .locals 0

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/twitter/model/util/a;->n:Z

    .line 118
    return-object p0
.end method

.method public d(Z)Lcom/twitter/model/util/a;
    .locals 0

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/twitter/model/util/a;->k:Z

    .line 137
    return-object p0
.end method

.method public e(Z)Lcom/twitter/model/util/a;
    .locals 0

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/twitter/model/util/a;->m:Z

    .line 143
    return-object p0
.end method

.method public f(Z)Lcom/twitter/model/util/a;
    .locals 0

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/twitter/model/util/a;->l:Z

    .line 149
    return-object p0
.end method
