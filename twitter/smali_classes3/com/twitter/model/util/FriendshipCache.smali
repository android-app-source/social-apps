.class public Lcom/twitter/model/util/FriendshipCache;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x6503bc469157f0d1L


# instance fields
.field private final mCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    .line 24
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    .line 28
    return-void
.end method

.method private d(JI)V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 180
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 181
    :goto_0
    invoke-static {v0, p3}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    .line 182
    iget-object v1, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    return-void

    .line 180
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method private e(JI)V
    .locals 3

    .prologue
    .line 192
    iget-object v0, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 193
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 194
    :goto_0
    invoke-static {v0, p3}, Lcom/twitter/model/core/g;->b(II)I

    move-result v0

    .line 195
    iget-object v1, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    return-void

    .line 193
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method private f(JI)Z
    .locals 3

    .prologue
    .line 206
    iget-object v0, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 207
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/2addr v0, p3

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;)V
    .locals 3

    .prologue
    .line 165
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->s:J

    iget v2, p1, Lcom/twitter/model/core/Tweet;->l:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/model/util/FriendshipCache;->c(JI)V

    .line 166
    return-void
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 3

    .prologue
    .line 169
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    iget v2, p1, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/model/util/FriendshipCache;->c(JI)V

    .line 170
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(JI)Z
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 57
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 40
    return-void
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/model/util/FriendshipCache;->d(JI)V

    .line 62
    return-void
.end method

.method public b(JI)V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    return-void
.end method

.method public c(J)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/model/util/FriendshipCache;->e(JI)V

    .line 66
    return-void
.end method

.method public c(JI)V
    .locals 3

    .prologue
    .line 147
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->a(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 148
    and-int/lit16 v0, p3, 0x80

    if-eqz v0, :cond_0

    .line 162
    :goto_0
    return-void

    .line 151
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 152
    and-int/lit8 v1, p3, 0x1

    if-eqz v1, :cond_1

    .line 153
    or-int/lit8 v0, v0, 0x1

    .line 157
    :goto_1
    and-int/lit16 v0, v0, -0x81

    .line 158
    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/model/util/FriendshipCache;->b(JI)V

    goto :goto_0

    .line 155
    :cond_1
    and-int/lit8 v0, v0, -0x2

    goto :goto_1

    .line 160
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->b(JI)V

    goto :goto_0
.end method

.method public d(J)V
    .locals 1

    .prologue
    .line 69
    const/16 v0, 0x10

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/model/util/FriendshipCache;->d(JI)V

    .line 70
    return-void
.end method

.method public e(J)V
    .locals 1

    .prologue
    .line 73
    const/16 v0, 0x10

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/model/util/FriendshipCache;->e(JI)V

    .line 74
    return-void
.end method

.method public f(J)V
    .locals 1

    .prologue
    .line 77
    const/16 v0, 0x2000

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/model/util/FriendshipCache;->d(JI)V

    .line 78
    return-void
.end method

.method public g(J)V
    .locals 1

    .prologue
    .line 81
    const/16 v0, 0x2000

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/model/util/FriendshipCache;->e(JI)V

    .line 82
    return-void
.end method

.method public h(J)V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/model/util/FriendshipCache;->d(JI)V

    .line 86
    return-void
.end method

.method public i(J)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/model/util/FriendshipCache;->e(JI)V

    .line 90
    return-void
.end method

.method public j(J)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/model/util/FriendshipCache;->mCache:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public k(J)Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/model/util/FriendshipCache;->f(JI)Z

    move-result v0

    return v0
.end method

.method public l(J)Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/model/util/FriendshipCache;->f(JI)Z

    move-result v0

    return v0
.end method
