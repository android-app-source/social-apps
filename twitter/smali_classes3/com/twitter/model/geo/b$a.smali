.class Lcom/twitter/model/geo/b$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/geo/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/geo/b;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/geo/b$1;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/twitter/model/geo/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/geo/b;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->h()D

    move-result-wide v0

    .line 96
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->h()D

    move-result-wide v2

    .line 97
    new-instance v4, Lcom/twitter/model/geo/b;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/twitter/model/geo/b;-><init>(DD)V

    return-object v4
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/geo/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p2}, Lcom/twitter/model/geo/b;->a()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(D)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 88
    invoke-virtual {p2}, Lcom/twitter/model/geo/b;->b()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(D)Lcom/twitter/util/serialization/o;

    .line 89
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    check-cast p2, Lcom/twitter/model/geo/b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/geo/b$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/geo/b;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/geo/b$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/geo/b;

    move-result-object v0

    return-object v0
.end method
