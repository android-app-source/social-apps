.class Lcom/twitter/model/geo/TwitterPlace$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/geo/TwitterPlace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/geo/TwitterPlace;",
        "Lcom/twitter/model/geo/TwitterPlace$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/geo/TwitterPlace$1;)V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/twitter/model/geo/TwitterPlace$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/geo/TwitterPlace$a;
    .locals 1

    .prologue
    .line 222
    new-instance v0, Lcom/twitter/model/geo/TwitterPlace$a;

    invoke-direct {v0}, Lcom/twitter/model/geo/TwitterPlace$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/geo/TwitterPlace$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 228
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/geo/TwitterPlace$a;->a(Ljava/lang/String;)Lcom/twitter/model/geo/TwitterPlace$a;

    move-result-object v1

    const-class v0, Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    .line 229
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    invoke-virtual {v1, v0}, Lcom/twitter/model/geo/TwitterPlace$a;->a(Lcom/twitter/model/geo/TwitterPlace$PlaceType;)Lcom/twitter/model/geo/TwitterPlace$a;

    move-result-object v0

    .line 230
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/geo/TwitterPlace$a;->b(Ljava/lang/String;)Lcom/twitter/model/geo/TwitterPlace$a;

    move-result-object v0

    .line 231
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/geo/TwitterPlace$a;->c(Ljava/lang/String;)Lcom/twitter/model/geo/TwitterPlace$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/geo/a;->a:Lcom/twitter/util/serialization/l;

    .line 232
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/a;

    invoke-virtual {v1, v0}, Lcom/twitter/model/geo/TwitterPlace$a;->a(Lcom/twitter/model/geo/a;)Lcom/twitter/model/geo/TwitterPlace$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/geo/b;->a:Lcom/twitter/util/serialization/l;

    .line 233
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/b;

    invoke-virtual {v1, v0}, Lcom/twitter/model/geo/TwitterPlace$a;->a(Lcom/twitter/model/geo/b;)Lcom/twitter/model/geo/TwitterPlace$a;

    move-result-object v0

    .line 234
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/geo/TwitterPlace$a;->d(Ljava/lang/String;)Lcom/twitter/model/geo/TwitterPlace$a;

    move-result-object v0

    .line 235
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/geo/TwitterPlace$a;->e(Ljava/lang/String;)Lcom/twitter/model/geo/TwitterPlace$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    .line 236
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/TwitterPlace;

    invoke-virtual {v1, v0}, Lcom/twitter/model/geo/TwitterPlace$a;->a(Lcom/twitter/model/geo/TwitterPlace;)Lcom/twitter/model/geo/TwitterPlace$a;

    move-result-object v0

    .line 237
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/geo/TwitterPlace$a;->f(Ljava/lang/String;)Lcom/twitter/model/geo/TwitterPlace$a;

    move-result-object v0

    .line 238
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/geo/TwitterPlace$a;->g(Ljava/lang/String;)Lcom/twitter/model/geo/TwitterPlace$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/geo/VendorInfo;->a:Lcom/twitter/util/serialization/l;

    .line 239
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/VendorInfo;

    invoke-virtual {v1, v0}, Lcom/twitter/model/geo/TwitterPlace$a;->a(Lcom/twitter/model/geo/VendorInfo;)Lcom/twitter/model/geo/TwitterPlace$a;

    .line 240
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 201
    check-cast p2, Lcom/twitter/model/geo/TwitterPlace$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/geo/TwitterPlace$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/geo/TwitterPlace$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/geo/TwitterPlace;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p2, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/geo/TwitterPlace;->c:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    const-class v2, Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    .line 206
    invoke-static {v2}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/geo/TwitterPlace;->d:Ljava/lang/String;

    .line 207
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/geo/TwitterPlace;->f:Ljava/lang/String;

    .line 208
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/geo/TwitterPlace;->g:Lcom/twitter/model/geo/a;

    sget-object v2, Lcom/twitter/model/geo/a;->a:Lcom/twitter/util/serialization/l;

    .line 209
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/geo/TwitterPlace;->h:Lcom/twitter/model/geo/b;

    sget-object v2, Lcom/twitter/model/geo/b;->a:Lcom/twitter/util/serialization/l;

    .line 210
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/geo/TwitterPlace;->i:Ljava/lang/String;

    .line 211
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/geo/TwitterPlace;->j:Ljava/lang/String;

    .line 212
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/geo/TwitterPlace;->k:Lcom/twitter/model/geo/TwitterPlace;

    sget-object v2, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    .line 213
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/geo/TwitterPlace;->l:Ljava/lang/String;

    .line 214
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/geo/TwitterPlace;->m:Ljava/lang/String;

    .line 215
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/geo/TwitterPlace;->e:Lcom/twitter/model/geo/VendorInfo;

    sget-object v2, Lcom/twitter/model/geo/VendorInfo;->a:Lcom/twitter/util/serialization/l;

    .line 216
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 217
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 201
    check-cast p2, Lcom/twitter/model/geo/TwitterPlace;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/geo/TwitterPlace$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/geo/TwitterPlace;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/twitter/model/geo/TwitterPlace$b;->a()Lcom/twitter/model/geo/TwitterPlace$a;

    move-result-object v0

    return-object v0
.end method
