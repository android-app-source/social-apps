.class Lcom/twitter/model/geo/c$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/geo/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/model/geo/c;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/geo/c$1;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/twitter/model/geo/c$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/geo/c;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 99
    sget-object v0, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/TwitterPlace;

    .line 100
    sget-object v1, Lcom/twitter/model/geo/b;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v1, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/geo/b;

    .line 101
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v2

    .line 102
    new-instance v3, Lcom/twitter/model/geo/c;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/model/geo/c;-><init>(Lcom/twitter/model/geo/TwitterPlace;Lcom/twitter/model/geo/b;Ljava/lang/String;)V

    return-object v3
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/geo/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    sget-object v0, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {p2}, Lcom/twitter/model/geo/c;->a()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/b;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 91
    sget-object v0, Lcom/twitter/model/geo/b;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p2}, Lcom/twitter/model/geo/c;->b()Lcom/twitter/model/geo/b;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 92
    invoke-virtual {p2}, Lcom/twitter/model/geo/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 93
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    check-cast p2, Lcom/twitter/model/geo/c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/geo/c$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/geo/c;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/geo/c$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/model/geo/c;

    move-result-object v0

    return-object v0
.end method
