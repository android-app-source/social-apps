.class public Lcom/twitter/model/media/foundmedia/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/media/foundmedia/d$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/media/foundmedia/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/twitter/model/media/foundmedia/FoundMediaProvider;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/twitter/model/media/foundmedia/FoundMediaOrigin;

.field public final f:Ljava/lang/String;

.field public final g:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/twitter/model/media/foundmedia/d$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/media/foundmedia/d$a;-><init>(Lcom/twitter/model/media/foundmedia/d$1;)V

    sput-object v0, Lcom/twitter/model/media/foundmedia/d;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/media/foundmedia/FoundMediaProvider;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/media/foundmedia/FoundMediaOrigin;Ljava/lang/String;Landroid/util/SparseArray;Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/media/foundmedia/FoundMediaProvider;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/twitter/model/media/foundmedia/FoundMediaOrigin;",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;",
            ">;",
            "Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/twitter/model/media/foundmedia/d;->b:Lcom/twitter/model/media/foundmedia/FoundMediaProvider;

    .line 46
    iput-object p2, p0, Lcom/twitter/model/media/foundmedia/d;->c:Ljava/lang/String;

    .line 47
    iput-object p3, p0, Lcom/twitter/model/media/foundmedia/d;->d:Ljava/lang/String;

    .line 48
    iput-object p4, p0, Lcom/twitter/model/media/foundmedia/d;->e:Lcom/twitter/model/media/foundmedia/FoundMediaOrigin;

    .line 49
    iput-object p5, p0, Lcom/twitter/model/media/foundmedia/d;->f:Ljava/lang/String;

    .line 50
    iput-object p6, p0, Lcom/twitter/model/media/foundmedia/d;->g:Landroid/util/SparseArray;

    .line 51
    iput-object p7, p0, Lcom/twitter/model/media/foundmedia/d;->h:Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;

    .line 52
    return-void
.end method

.method constructor <init>(Lcom/twitter/util/serialization/n;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    sget-object v0, Lcom/twitter/model/media/foundmedia/FoundMediaProvider;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/foundmedia/FoundMediaProvider;

    iput-object v0, p0, Lcom/twitter/model/media/foundmedia/d;->b:Lcom/twitter/model/media/foundmedia/FoundMediaProvider;

    .line 56
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/media/foundmedia/d;->c:Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/media/foundmedia/d;->d:Ljava/lang/String;

    .line 58
    sget-object v0, Lcom/twitter/model/media/foundmedia/FoundMediaOrigin;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/foundmedia/FoundMediaOrigin;

    iput-object v0, p0, Lcom/twitter/model/media/foundmedia/d;->e:Lcom/twitter/model/media/foundmedia/FoundMediaOrigin;

    .line 59
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/media/foundmedia/d;->f:Ljava/lang/String;

    .line 60
    sget-object v0, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;->a:Lcom/twitter/util/serialization/l;

    .line 61
    invoke-static {p1, v0}, Lcom/twitter/util/serialization/a;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Landroid/util/SparseArray;

    move-result-object v0

    .line 60
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    iput-object v0, p0, Lcom/twitter/model/media/foundmedia/d;->g:Landroid/util/SparseArray;

    .line 62
    sget-object v0, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;

    iput-object v0, p0, Lcom/twitter/model/media/foundmedia/d;->h:Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;

    .line 63
    return-void
.end method


# virtual methods
.method a(Lcom/twitter/util/serialization/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    sget-object v0, Lcom/twitter/model/media/foundmedia/FoundMediaProvider;->a:Lcom/twitter/util/serialization/l;

    iget-object v1, p0, Lcom/twitter/model/media/foundmedia/d;->b:Lcom/twitter/model/media/foundmedia/FoundMediaProvider;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lcom/twitter/model/media/foundmedia/d;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 68
    iget-object v0, p0, Lcom/twitter/model/media/foundmedia/d;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 69
    sget-object v0, Lcom/twitter/model/media/foundmedia/FoundMediaOrigin;->a:Lcom/twitter/util/serialization/l;

    iget-object v1, p0, Lcom/twitter/model/media/foundmedia/d;->e:Lcom/twitter/model/media/foundmedia/FoundMediaOrigin;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 70
    iget-object v0, p0, Lcom/twitter/model/media/foundmedia/d;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 71
    iget-object v0, p0, Lcom/twitter/model/media/foundmedia/d;->g:Landroid/util/SparseArray;

    sget-object v1, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/serialization/a;->a(Lcom/twitter/util/serialization/o;Landroid/util/SparseArray;Lcom/twitter/util/serialization/l;)V

    .line 72
    sget-object v0, Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;->a:Lcom/twitter/util/serialization/l;

    iget-object v1, p0, Lcom/twitter/model/media/foundmedia/d;->h:Lcom/twitter/model/media/foundmedia/FoundMediaImageVariant;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 73
    return-void
.end method
