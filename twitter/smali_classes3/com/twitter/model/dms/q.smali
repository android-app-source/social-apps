.class public Lcom/twitter/model/dms/q;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/q$a;,
        Lcom/twitter/model/dms/q$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/dms/q;",
            "Lcom/twitter/model/dms/q$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Z

.field public final g:J

.field public final h:Z

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Z

.field public final k:Z

.field public final l:Lcom/twitter/model/dms/m;

.field public final m:Lcom/twitter/model/dms/x;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/model/dms/q$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/dms/q$b;-><init>(Lcom/twitter/model/dms/q$1;)V

    sput-object v0, Lcom/twitter/model/dms/q;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/q$a;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, Lcom/twitter/model/dms/q$a;->a(Lcom/twitter/model/dms/q$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    .line 42
    invoke-static {p1}, Lcom/twitter/model/dms/q$a;->b(Lcom/twitter/model/dms/q$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/q;->c:Ljava/lang/String;

    .line 43
    invoke-static {p1}, Lcom/twitter/model/dms/q$a;->c(Lcom/twitter/model/dms/q$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/q;->d:Ljava/lang/String;

    .line 44
    invoke-static {p1}, Lcom/twitter/model/dms/q$a;->d(Lcom/twitter/model/dms/q$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/q;->e:Ljava/lang/String;

    .line 45
    invoke-static {p1}, Lcom/twitter/model/dms/q$a;->e(Lcom/twitter/model/dms/q$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/q;->f:Z

    .line 46
    invoke-static {p1}, Lcom/twitter/model/dms/q$a;->f(Lcom/twitter/model/dms/q$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/q;->g:J

    .line 47
    invoke-static {p1}, Lcom/twitter/model/dms/q$a;->g(Lcom/twitter/model/dms/q$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/q;->h:Z

    .line 48
    invoke-static {p1}, Lcom/twitter/model/dms/q$a;->h(Lcom/twitter/model/dms/q$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    .line 49
    invoke-static {p1}, Lcom/twitter/model/dms/q$a;->i(Lcom/twitter/model/dms/q$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/q;->j:Z

    .line 50
    invoke-static {p1}, Lcom/twitter/model/dms/q$a;->j(Lcom/twitter/model/dms/q$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/q;->k:Z

    .line 51
    invoke-static {p1}, Lcom/twitter/model/dms/q$a;->k(Lcom/twitter/model/dms/q$a;)Lcom/twitter/model/dms/m;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/q;->l:Lcom/twitter/model/dms/m;

    .line 52
    invoke-static {p1}, Lcom/twitter/model/dms/q$a;->l(Lcom/twitter/model/dms/q$a;)Lcom/twitter/model/dms/x;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/q;->m:Lcom/twitter/model/dms/x;

    .line 53
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/q$a;Lcom/twitter/model/dms/q$1;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/q;-><init>(Lcom/twitter/model/dms/q$a;)V

    return-void
.end method

.method private a(Lcom/twitter/model/dms/q;)Z
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/q;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/q;->c:Ljava/lang/String;

    .line 62
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/q;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/q;->d:Ljava/lang/String;

    .line 63
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/q;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/q;->e:Ljava/lang/String;

    .line 64
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/dms/q;->f:Z

    iget-boolean v1, p1, Lcom/twitter/model/dms/q;->f:Z

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/dms/q;->g:J

    iget-wide v2, p1, Lcom/twitter/model/dms/q;->g:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/dms/q;->h:Z

    iget-boolean v1, p1, Lcom/twitter/model/dms/q;->h:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    .line 68
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/dms/q;->j:Z

    iget-boolean v1, p1, Lcom/twitter/model/dms/q;->j:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/dms/q;->k:Z

    iget-boolean v1, p1, Lcom/twitter/model/dms/q;->k:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/q;->l:Lcom/twitter/model/dms/m;

    iget-object v1, p1, Lcom/twitter/model/dms/q;->l:Lcom/twitter/model/dms/m;

    .line 71
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/q;->m:Lcom/twitter/model/dms/x;

    iget-object v1, p1, Lcom/twitter/model/dms/q;->m:Lcom/twitter/model/dms/x;

    .line 72
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    .line 72
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 57
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/q;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/q;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/q;->a(Lcom/twitter/model/dms/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 77
    iget-object v0, p0, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 78
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/q;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/dms/q;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v3

    .line 79
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/q;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/dms/q;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    .line 80
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/q;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/model/dms/q;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v3

    .line 81
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/dms/q;->f:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    add-int/2addr v0, v3

    .line 82
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/twitter/model/dms/q;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 83
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/dms/q;->h:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_4
    add-int/2addr v0, v3

    .line 84
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 85
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/dms/q;->j:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_5
    add-int/2addr v0, v3

    .line 86
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/twitter/model/dms/q;->k:Z

    if-eqz v3, :cond_7

    :goto_6
    add-int/2addr v0, v2

    .line 87
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/q;->l:Lcom/twitter/model/dms/m;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/twitter/model/dms/q;->l:Lcom/twitter/model/dms/m;

    invoke-virtual {v0}, Lcom/twitter/model/dms/m;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    .line 88
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/model/dms/q;->m:Lcom/twitter/model/dms/x;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/model/dms/q;->m:Lcom/twitter/model/dms/x;

    invoke-virtual {v1}, Lcom/twitter/model/dms/x;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 89
    return v0

    :cond_1
    move v0, v1

    .line 78
    goto :goto_0

    :cond_2
    move v0, v1

    .line 79
    goto :goto_1

    :cond_3
    move v0, v1

    .line 80
    goto :goto_2

    :cond_4
    move v0, v1

    .line 81
    goto :goto_3

    :cond_5
    move v0, v1

    .line 83
    goto :goto_4

    :cond_6
    move v0, v1

    .line 85
    goto :goto_5

    :cond_7
    move v2, v1

    .line 86
    goto :goto_6

    :cond_8
    move v0, v1

    .line 87
    goto :goto_7
.end method
