.class public abstract Lcom/twitter/model/dms/c;
.super Lcom/twitter/model/dms/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/c$a;,
        Lcom/twitter/model/dms/c$c;,
        Lcom/twitter/model/dms/c$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/model/dms/d;"
    }
.end annotation


# static fields
.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:J

.field public final c:Z

.field public final d:Z

.field private final h:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/twitter/util/serialization/j;

    const/4 v1, 0x0

    const-class v2, Lcom/twitter/model/dms/j;

    new-instance v3, Lcom/twitter/model/dms/j$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/j$b;-><init>()V

    .line 25
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/twitter/model/dms/z;

    new-instance v3, Lcom/twitter/model/dms/z$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/z$b;-><init>()V

    .line 27
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/twitter/model/dms/ag;

    new-instance v3, Lcom/twitter/model/dms/ag$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/ag$b;-><init>()V

    .line 29
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/twitter/model/dms/ai;

    new-instance v3, Lcom/twitter/model/dms/ai$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/ai$b;-><init>()V

    .line 31
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/twitter/model/dms/ak;

    new-instance v3, Lcom/twitter/model/dms/ak$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/ak$b;-><init>()V

    .line 33
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lcom/twitter/model/dms/s;

    new-instance v3, Lcom/twitter/model/dms/s$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/s$b;-><init>()V

    .line 35
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-class v2, Lcom/twitter/model/dms/r;

    new-instance v3, Lcom/twitter/model/dms/r$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/r$b;-><init>()V

    .line 37
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-class v2, Lcom/twitter/model/dms/ae;

    new-instance v3, Lcom/twitter/model/dms/ae$c;

    invoke-direct {v3}, Lcom/twitter/model/dms/ae$c;-><init>()V

    .line 39
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-class v2, Lcom/twitter/model/dms/af;

    new-instance v3, Lcom/twitter/model/dms/af$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/af$b;-><init>()V

    .line 41
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-class v2, Lcom/twitter/model/dms/h;

    new-instance v3, Lcom/twitter/model/dms/h$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/h$b;-><init>()V

    .line 43
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-class v2, Lcom/twitter/model/dms/g;

    new-instance v3, Lcom/twitter/model/dms/g$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/g$b;-><init>()V

    .line 45
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-class v2, Lcom/twitter/model/dms/u;

    new-instance v3, Lcom/twitter/model/dms/u$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/u$b;-><init>()V

    .line 47
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    .line 24
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a([Lcom/twitter/util/serialization/j;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/dms/c;->b:Lcom/twitter/util/serialization/l;

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/twitter/model/dms/c$b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/dms/c$b",
            "<**TT;>;)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/d;-><init>(Lcom/twitter/model/dms/d$a;)V

    .line 59
    iget-boolean v0, p1, Lcom/twitter/model/dms/c$b;->a:Z

    iput-boolean v0, p0, Lcom/twitter/model/dms/c;->c:Z

    .line 60
    iget-boolean v0, p1, Lcom/twitter/model/dms/c$b;->b:Z

    iput-boolean v0, p0, Lcom/twitter/model/dms/c;->d:Z

    .line 61
    invoke-static {p1}, Lcom/twitter/model/dms/c$b;->a(Lcom/twitter/model/dms/c$b;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/c;->a:J

    .line 62
    iget-object v0, p1, Lcom/twitter/model/dms/c$b;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/twitter/model/dms/c;->h:Ljava/lang/Object;

    .line 63
    return-void
.end method

.method private a(Lcom/twitter/model/dms/c;)Z
    .locals 4

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/twitter/model/dms/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/dms/c;->c:Z

    iget-boolean v1, p1, Lcom/twitter/model/dms/c;->c:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/dms/c;->d:Z

    iget-boolean v1, p1, Lcom/twitter/model/dms/c;->d:Z

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/dms/c;->a:J

    .line 104
    invoke-virtual {p1}, Lcom/twitter/model/dms/c;->l()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/c;->h:Ljava/lang/Object;

    .line 105
    invoke-virtual {p1}, Lcom/twitter/model/dms/c;->k()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 101
    :goto_0
    return v0

    .line 105
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(J)Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    return v0
.end method

.method public abstract c()Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/serialization/l",
            "<TT;>;"
        }
    .end annotation
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 97
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/c;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/c;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/c;->a(Lcom/twitter/model/dms/c;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 110
    invoke-super {p0}, Lcom/twitter/model/dms/d;->hashCode()I

    move-result v0

    .line 111
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/dms/c;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 112
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/twitter/model/dms/c;->d:Z

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 113
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/model/dms/c;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/dms/c;->h:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    return v0

    :cond_0
    move v0, v2

    .line 111
    goto :goto_0

    :cond_1
    move v1, v2

    .line 112
    goto :goto_1
.end method

.method public final j()[B
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/twitter/model/dms/c;->k()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/model/dms/c;->c()Lcom/twitter/util/serialization/l;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/model/dms/c;->h:Ljava/lang/Object;

    return-object v0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/twitter/model/dms/c;->a:J

    return-wide v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 88
    sget-object v0, Lcbu$a;->b:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/twitter/model/dms/c;->o()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 92
    sget-object v0, Lcbu$a;->a:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/twitter/model/dms/c;->o()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
