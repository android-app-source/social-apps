.class public Lcom/twitter/model/dms/g$c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/g$c$b;,
        Lcom/twitter/model/dms/g$c$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/model/dms/g$c$b;


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lcom/twitter/model/dms/g$c$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/dms/g$c$b;-><init>(Lcom/twitter/model/dms/g$1;)V

    sput-object v0, Lcom/twitter/model/dms/g$c;->a:Lcom/twitter/model/dms/g$c$b;

    return-void
.end method

.method protected constructor <init>(Lcom/twitter/model/dms/g$c$a;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {p1}, Lcom/twitter/model/dms/g$c$a;->a(Lcom/twitter/model/dms/g$c$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/g$c;->b:Ljava/lang/String;

    .line 57
    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/g$c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/model/dms/g$c;->b:Ljava/lang/String;

    return-object v0
.end method

.method private b(Lcom/twitter/model/dms/g$c;)Z
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/model/dms/g$c;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/g$c;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 66
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/g$c;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/g$c;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/g$c;->b(Lcom/twitter/model/dms/g$c;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/model/dms/g$c;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/g$c;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
