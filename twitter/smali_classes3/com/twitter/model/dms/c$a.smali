.class public abstract Lcom/twitter/model/dms/c$a;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        "E:",
        "Lcom/twitter/model/dms/c;",
        "B:",
        "Lcom/twitter/model/dms/c$b",
        "<TE;TB;TD;>;>",
        "Lcom/twitter/util/serialization/b",
        "<TE;TB;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<TD;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/twitter/util/serialization/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/l",
            "<TD;>;)V"
        }
    .end annotation

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    .line 202
    iput-object p1, p0, Lcom/twitter/model/dms/c$a;->a:Lcom/twitter/util/serialization/l;

    .line 203
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/c$b;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TB;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 208
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/dms/c$b;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c$b;

    .line 209
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/c$b;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c$b;

    .line 210
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/c$b;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c$b;

    .line 211
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/c$b;->a(J)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    .line 212
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/c$b;->a(Z)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/dms/c$a;->a:Lcom/twitter/util/serialization/l;

    .line 213
    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/c$b;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    .line 214
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 197
    check-cast p2, Lcom/twitter/model/dms/c$b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/c$a;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/c$b;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/c;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "TE;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    iget-wide v0, p2, Lcom/twitter/model/dms/c;->e:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/dms/c;->f:Ljava/lang/String;

    .line 220
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/dms/c;->g:J

    .line 221
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 222
    invoke-virtual {p2}, Lcom/twitter/model/dms/c;->l()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/dms/c;->c:Z

    .line 223
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 224
    invoke-virtual {p2}, Lcom/twitter/model/dms/c;->k()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/model/dms/c$a;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 225
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    check-cast p2, Lcom/twitter/model/dms/c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/c$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/c;)V

    return-void
.end method
