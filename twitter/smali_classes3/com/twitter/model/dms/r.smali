.class public Lcom/twitter/model/dms/r;
.super Lcom/twitter/model/dms/e;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/r$b;,
        Lcom/twitter/model/dms/r$c;,
        Lcom/twitter/model/dms/r$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/e",
        "<",
        "Lcom/twitter/model/dms/r$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/twitter/model/dms/r$b;

    invoke-direct {v0}, Lcom/twitter/model/dms/r$b;-><init>()V

    sput-object v0, Lcom/twitter/model/dms/r;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/r$a;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/e;-><init>(Lcom/twitter/model/dms/e$a;)V

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/r$a;Lcom/twitter/model/dms/r$1;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/r;-><init>(Lcom/twitter/model/dms/r$a;)V

    return-void
.end method


# virtual methods
.method public G()I
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/twitter/model/dms/r;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r$c;

    iget v0, v0, Lcom/twitter/model/dms/r$c;->m:I

    return v0
.end method

.method public c()Lcom/twitter/util/serialization/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/r$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    sget-object v0, Lcom/twitter/model/dms/r$c;->a:Lcom/twitter/util/serialization/l;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 52
    invoke-virtual {p0}, Lcom/twitter/model/dms/r;->g()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/dms/r;->g()I

    move-result v1

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/twitter/model/dms/r;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r$c;

    iget v0, v0, Lcom/twitter/model/dms/r$c;->l:I

    return v0
.end method

.method public h()Lcom/twitter/model/drafts/DraftAttachment;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/twitter/model/dms/r;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r$c;

    iget-object v0, v0, Lcom/twitter/model/dms/r$c;->n:Lcom/twitter/model/drafts/DraftAttachment;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/twitter/model/dms/r;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r$c;

    iget-object v0, v0, Lcom/twitter/model/dms/r$c;->o:Ljava/lang/String;

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    return v0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/twitter/model/dms/r;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/model/dms/r;->D()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "dm_cancel_messages_enabled"

    .line 63
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 62
    :goto_0
    return v0

    .line 63
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
