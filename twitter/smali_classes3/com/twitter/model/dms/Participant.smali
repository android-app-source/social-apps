.class public Lcom/twitter/model/dms/Participant;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/Participant$a;,
        Lcom/twitter/model/dms/Participant$b;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:J

.field public final d:J

.field public final e:J

.field public final f:Ljava/lang/String;

.field public final g:Lcom/twitter/model/core/TwitterUser;

.field public final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/twitter/model/dms/Participant$1;

    invoke-direct {v0}, Lcom/twitter/model/dms/Participant$1;-><init>()V

    sput-object v0, Lcom/twitter/model/dms/Participant;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 41
    new-instance v0, Lcom/twitter/model/dms/Participant$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/dms/Participant$b;-><init>(Lcom/twitter/model/dms/Participant$1;)V

    sput-object v0, Lcom/twitter/model/dms/Participant;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/Participant;->b:J

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/Participant;->c:J

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/Participant;->d:J

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/Participant;->e:J

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/Participant;->f:Ljava/lang/String;

    .line 67
    sget-object v0, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/model/dms/Participant;->g:Lcom/twitter/model/core/TwitterUser;

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 70
    iput v0, p0, Lcom/twitter/model/dms/Participant;->h:I

    .line 71
    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/Participant$a;)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {p1}, Lcom/twitter/model/dms/Participant$a;->a(Lcom/twitter/model/dms/Participant$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/Participant;->b:J

    .line 53
    invoke-static {p1}, Lcom/twitter/model/dms/Participant$a;->b(Lcom/twitter/model/dms/Participant$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/Participant;->c:J

    .line 54
    invoke-static {p1}, Lcom/twitter/model/dms/Participant$a;->c(Lcom/twitter/model/dms/Participant$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/Participant;->d:J

    .line 55
    invoke-static {p1}, Lcom/twitter/model/dms/Participant$a;->d(Lcom/twitter/model/dms/Participant$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/Participant;->e:J

    .line 56
    invoke-static {p1}, Lcom/twitter/model/dms/Participant$a;->e(Lcom/twitter/model/dms/Participant$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/Participant;->f:Ljava/lang/String;

    .line 57
    invoke-static {p1}, Lcom/twitter/model/dms/Participant$a;->f(Lcom/twitter/model/dms/Participant$a;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/Participant;->g:Lcom/twitter/model/core/TwitterUser;

    .line 58
    invoke-static {p1}, Lcom/twitter/model/dms/Participant$a;->g(Lcom/twitter/model/dms/Participant$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/dms/Participant;->h:I

    .line 59
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/Participant$a;Lcom/twitter/model/dms/Participant$1;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/Participant;-><init>(Lcom/twitter/model/dms/Participant$a;)V

    return-void
.end method

.method private a(Lcom/twitter/model/dms/Participant;)Z
    .locals 4

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/twitter/model/dms/Participant;->b:J

    iget-wide v2, p1, Lcom/twitter/model/dms/Participant;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/dms/Participant;->c:J

    iget-wide v2, p1, Lcom/twitter/model/dms/Participant;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/dms/Participant;->d:J

    iget-wide v2, p1, Lcom/twitter/model/dms/Participant;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/dms/Participant;->e:J

    iget-wide v2, p1, Lcom/twitter/model/dms/Participant;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/Participant;->f:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/Participant;->f:Ljava/lang/String;

    .line 83
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/Participant;->g:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, p1, Lcom/twitter/model/dms/Participant;->g:Lcom/twitter/model/core/TwitterUser;

    .line 84
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/model/dms/Participant;->h:I

    iget v1, p1, Lcom/twitter/model/dms/Participant;->h:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 79
    :goto_0
    return v0

    .line 84
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(J)Z
    .locals 3

    .prologue
    .line 117
    iget-wide v0, p0, Lcom/twitter/model/dms/Participant;->d:J

    cmp-long v0, v0, p1

    if-ltz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/Participant;->b(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)Z
    .locals 3

    .prologue
    .line 124
    iget-wide v0, p0, Lcom/twitter/model/dms/Participant;->e:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 75
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/Participant;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/Participant;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/Participant;->a(Lcom/twitter/model/dms/Participant;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 90
    iget-wide v2, p0, Lcom/twitter/model/dms/Participant;->b:J

    iget-wide v4, p0, Lcom/twitter/model/dms/Participant;->b:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 91
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/model/dms/Participant;->c:J

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 92
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/model/dms/Participant;->d:J

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 93
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/model/dms/Participant;->e:J

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 94
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/Participant;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/dms/Participant;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 95
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/model/dms/Participant;->g:Lcom/twitter/model/core/TwitterUser;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/model/dms/Participant;->g:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 96
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/dms/Participant;->h:I

    add-int/2addr v0, v1

    .line 97
    return v0

    :cond_1
    move v0, v1

    .line 94
    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/twitter/model/dms/Participant;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 108
    iget-wide v0, p0, Lcom/twitter/model/dms/Participant;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 109
    iget-wide v0, p0, Lcom/twitter/model/dms/Participant;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 110
    iget-wide v0, p0, Lcom/twitter/model/dms/Participant;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 111
    iget-object v0, p0, Lcom/twitter/model/dms/Participant;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/twitter/model/dms/Participant;->g:Lcom/twitter/model/core/TwitterUser;

    sget-object v1, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 113
    iget v0, p0, Lcom/twitter/model/dms/Participant;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    return-void
.end method
