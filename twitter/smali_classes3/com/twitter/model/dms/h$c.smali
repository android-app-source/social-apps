.class public Lcom/twitter/model/dms/h$c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/h$c$b;,
        Lcom/twitter/model/dms/h$c$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/model/dms/h$c$b;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:I

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 87
    new-instance v0, Lcom/twitter/model/dms/h$c$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/dms/h$c$b;-><init>(Lcom/twitter/model/dms/h$1;)V

    sput-object v0, Lcom/twitter/model/dms/h$c;->a:Lcom/twitter/model/dms/h$c$b;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/h$c$a;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    invoke-static {p1}, Lcom/twitter/model/dms/h$c$a;->a(Lcom/twitter/model/dms/h$c$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/h$c;->b:Ljava/lang/String;

    .line 95
    invoke-static {p1}, Lcom/twitter/model/dms/h$c$a;->b(Lcom/twitter/model/dms/h$c$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/dms/h$c;->c:I

    .line 96
    invoke-static {p1}, Lcom/twitter/model/dms/h$c$a;->c(Lcom/twitter/model/dms/h$c$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/h$c;->d:Ljava/lang/String;

    .line 97
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/h$c$a;Lcom/twitter/model/dms/h$1;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/h$c;-><init>(Lcom/twitter/model/dms/h$c$a;)V

    return-void
.end method

.method private a(Lcom/twitter/model/dms/h$c;)Z
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/model/dms/h$c;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/h$c;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/model/dms/h$c;->c:I

    iget v1, p1, Lcom/twitter/model/dms/h$c;->c:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/h$c;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/h$c;->d:Ljava/lang/String;

    .line 107
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 105
    :goto_0
    return v0

    .line 107
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 101
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/h$c;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/h$c;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/h$c;->a(Lcom/twitter/model/dms/h$c;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 112
    iget-object v0, p0, Lcom/twitter/model/dms/h$c;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/dms/h$c;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 113
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/model/dms/h$c;->c:I

    add-int/2addr v0, v2

    .line 114
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/model/dms/h$c;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/model/dms/h$c;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 115
    return v0

    :cond_1
    move v0, v1

    .line 112
    goto :goto_0
.end method
