.class public Lcom/twitter/model/dms/h;
.super Lcom/twitter/model/dms/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/h$b;,
        Lcom/twitter/model/dms/h$a;,
        Lcom/twitter/model/dms/h$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/c",
        "<",
        "Lcom/twitter/model/dms/h$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/h;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/model/dms/h$b;

    invoke-direct {v0}, Lcom/twitter/model/dms/h$b;-><init>()V

    sput-object v0, Lcom/twitter/model/dms/h;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/h$a;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/c;-><init>(Lcom/twitter/model/dms/c$b;)V

    .line 40
    invoke-static {p1}, Lcom/twitter/model/dms/h$a;->a(Lcom/twitter/model/dms/h$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/h;->h:Ljava/lang/String;

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/h$a;Lcom/twitter/model/dms/h$1;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/h;-><init>(Lcom/twitter/model/dms/h$a;)V

    return-void
.end method

.method private a(Lcom/twitter/model/dms/h;)Z
    .locals 2

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/twitter/model/dms/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/h;->h:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/h;->h:Ljava/lang/String;

    .line 76
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 75
    :goto_0
    return v0

    .line 76
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public c()Lcom/twitter/util/serialization/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/h$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    sget-object v0, Lcom/twitter/model/dms/h$c;->a:Lcom/twitter/model/dms/h$c$b;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/twitter/model/dms/h;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/h$c;

    iget-object v0, v0, Lcom/twitter/model/dms/h$c;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/twitter/model/dms/h;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/h$c;

    iget-object v0, v0, Lcom/twitter/model/dms/h$c;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 71
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/h;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/h;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/h;->a(Lcom/twitter/model/dms/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/twitter/model/dms/h;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/h$c;

    iget v0, v0, Lcom/twitter/model/dms/h$c;->c:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 81
    invoke-super {p0}, Lcom/twitter/model/dms/c;->hashCode()I

    move-result v0

    .line 82
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/h;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/h;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    .line 83
    return v0

    .line 82
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 60
    const/16 v0, 0x15

    return v0
.end method
