.class public final Lcom/twitter/model/dms/m$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/dms/m;",
        ">;"
    }
.end annotation


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Lcom/twitter/model/dms/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/m$a;)J
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/twitter/model/dms/m$a;->a:J

    return-wide v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/m$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/model/dms/m$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/dms/m$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/model/dms/m$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/dms/m$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/model/dms/m$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/model/dms/m$a;)Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/twitter/model/dms/m$a;->e:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/model/dms/m$a;)Lcom/twitter/model/dms/c;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/model/dms/m$a;->f:Lcom/twitter/model/dms/c;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/model/dms/m$a;->f:Lcom/twitter/model/dms/c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcom/twitter/model/dms/m$a;
    .locals 1

    .prologue
    .line 89
    iput-wide p1, p0, Lcom/twitter/model/dms/m$a;->a:J

    .line 90
    return-object p0
.end method

.method public a(Lcom/twitter/model/dms/c;)Lcom/twitter/model/dms/m$a;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/twitter/model/dms/m$a;->f:Lcom/twitter/model/dms/c;

    .line 120
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/dms/m$a;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/twitter/model/dms/m$a;->b:Ljava/lang/String;

    .line 96
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/dms/m$a;
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/twitter/model/dms/m$a;->e:Z

    .line 114
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/dms/m$a;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/twitter/model/dms/m$a;->c:Ljava/lang/String;

    .line 102
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/twitter/model/dms/m$a;->e()Lcom/twitter/model/dms/m;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/dms/m$a;
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/twitter/model/dms/m$a;->d:Ljava/lang/String;

    .line 108
    return-object p0
.end method

.method protected e()Lcom/twitter/model/dms/m;
    .locals 1

    .prologue
    .line 131
    new-instance v0, Lcom/twitter/model/dms/m;

    invoke-direct {v0, p0}, Lcom/twitter/model/dms/m;-><init>(Lcom/twitter/model/dms/m$a;)V

    return-object v0
.end method
