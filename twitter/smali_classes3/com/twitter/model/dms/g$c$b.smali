.class Lcom/twitter/model/dms/g$c$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/g$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/dms/g$c;",
        "Lcom/twitter/model/dms/g$c$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/g$1;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/twitter/model/dms/g$c$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/dms/g$c$a;
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/twitter/model/dms/g$c$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/g$c$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/g$c$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/g$c$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/g$c$a;

    .line 106
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 94
    check-cast p2, Lcom/twitter/model/dms/g$c$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/g$c$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/g$c$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/g$c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    invoke-static {p2}, Lcom/twitter/model/dms/g$c;->a(Lcom/twitter/model/dms/g$c;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 99
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    check-cast p2, Lcom/twitter/model/dms/g$c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/g$c$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/g$c;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/twitter/model/dms/g$c$b;->a()Lcom/twitter/model/dms/g$c$a;

    move-result-object v0

    return-object v0
.end method
