.class public final Lcom/twitter/model/dms/ae$b$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/ae$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/dms/ae$b;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/ae$b$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/model/dms/ae$b$a;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/ae$b$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/model/dms/ae$b$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/dms/ae$b$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/model/dms/ae$b$a;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/twitter/model/dms/ae$b$a;
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/twitter/model/dms/ae$b$a;->b:Ljava/lang/String;

    .line 115
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/dms/ae$b$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;)",
            "Lcom/twitter/model/dms/ae$b$a;"
        }
    .end annotation

    .prologue
    .line 108
    iput-object p1, p0, Lcom/twitter/model/dms/ae$b$a;->a:Ljava/util/List;

    .line 109
    return-object p0
.end method

.method public b(Ljava/util/List;)Lcom/twitter/model/dms/ae$b$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/twitter/model/dms/ae$b$a;"
        }
    .end annotation

    .prologue
    .line 120
    iput-object p1, p0, Lcom/twitter/model/dms/ae$b$a;->c:Ljava/util/List;

    .line 121
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/twitter/model/dms/ae$b$a;->e()Lcom/twitter/model/dms/ae$b;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/dms/ae$b;
    .locals 2

    .prologue
    .line 127
    new-instance v0, Lcom/twitter/model/dms/ae$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/dms/ae$b;-><init>(Lcom/twitter/model/dms/ae$b$a;Lcom/twitter/model/dms/ae$1;)V

    return-object v0
.end method
