.class abstract Lcom/twitter/model/dms/e$b$a;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/e$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/twitter/model/dms/e$b;",
        "B:",
        "Lcom/twitter/model/dms/e$b$b",
        "<TB;TE;>;>",
        "Lcom/twitter/util/serialization/b",
        "<TE;TB;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 370
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    .line 371
    return-void
.end method

.method protected constructor <init>(I)V
    .locals 0

    .prologue
    .line 374
    invoke-direct {p0, p1}, Lcom/twitter/util/serialization/b;-><init>(I)V

    .line 375
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/e$b$b;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TB;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 394
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/dms/e$b$b;->b(J)Lcom/twitter/model/dms/e$b$b;

    move-result-object v0

    .line 395
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/e$b$b;->b(Ljava/lang/String;)Lcom/twitter/model/dms/e$b$b;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 396
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/e$b$b;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/dms/e$b$b;

    move-result-object v1

    sget-object v0, Lcbx;->a:Lcom/twitter/util/serialization/l;

    .line 397
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx;

    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/e$b$b;->a(Lcbx;)Lcom/twitter/model/dms/e$b$b;

    move-result-object v0

    .line 398
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/e$b$b;->a(Ljava/lang/String;)Lcom/twitter/model/dms/e$b$b;

    move-result-object v0

    .line 399
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/e$b$b;->a(Z)Lcom/twitter/model/dms/e$b$b;

    move-result-object v0

    .line 400
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/e$b$b;->b(Z)Lcom/twitter/model/dms/e$b$b;

    move-result-object v0

    .line 401
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/e$b$b;->a(I)Lcom/twitter/model/dms/e$b$b;

    move-result-object v0

    .line 402
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/e$b$b;->c(Ljava/lang/String;)Lcom/twitter/model/dms/e$b$b;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 403
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/e$b$b;->b(Lcom/twitter/model/core/v;)Lcom/twitter/model/dms/e$b$b;

    .line 404
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 368
    check-cast p2, Lcom/twitter/model/dms/e$b$b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/e$b$a;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/e$b$b;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/e$b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "TE;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 379
    iget-wide v0, p2, Lcom/twitter/model/dms/e$b;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 380
    iget-object v0, p2, Lcom/twitter/model/dms/e$b;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 381
    iget-object v0, p2, Lcom/twitter/model/dms/e$b;->d:Lcom/twitter/model/core/v;

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 382
    iget-object v0, p2, Lcom/twitter/model/dms/e$b;->g:Lcbx;

    sget-object v1, Lcbx;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 383
    iget-object v0, p2, Lcom/twitter/model/dms/e$b;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 384
    iget-boolean v0, p2, Lcom/twitter/model/dms/e$b;->i:Z

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 385
    iget-boolean v0, p2, Lcom/twitter/model/dms/e$b;->j:Z

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 386
    iget v0, p2, Lcom/twitter/model/dms/e$b;->k:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 387
    iget-object v0, p2, Lcom/twitter/model/dms/e$b;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 388
    iget-object v0, p2, Lcom/twitter/model/dms/e$b;->f:Lcom/twitter/model/core/v;

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 389
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 368
    check-cast p2, Lcom/twitter/model/dms/e$b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/e$b$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/e$b;)V

    return-void
.end method
