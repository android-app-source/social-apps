.class Lcom/twitter/model/dms/a$c$b;
.super Lcom/twitter/model/dms/e$b$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/a$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/e$b$a",
        "<",
        "Lcom/twitter/model/dms/a$c;",
        "Lcom/twitter/model/dms/a$c$a;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/model/dms/e$b$a;-><init>(I)V

    .line 337
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/dms/a$c$a;
    .locals 1

    .prologue
    .line 366
    new-instance v0, Lcom/twitter/model/dms/a$c$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/a$c$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/a$c$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 352
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/model/dms/e$b$a;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/e$b$b;I)V

    .line 353
    sget-object v0, Lcck;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcck;

    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/a$c$a;->a(Lcck;)Lcom/twitter/model/dms/a$c$a;

    .line 355
    const/4 v0, 0x1

    if-lt p3, v0, :cond_0

    .line 356
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    .line 360
    :goto_0
    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/dms/a$c$a;->a(J)Lcom/twitter/model/dms/a$c$a;

    .line 361
    return-void

    .line 358
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/e$b$b;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 332
    check-cast p2, Lcom/twitter/model/dms/a$c$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/a$c$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/a$c$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 332
    check-cast p2, Lcom/twitter/model/dms/a$c$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/a$c$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/a$c$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/a$c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 343
    invoke-super {p0, p1, p2}, Lcom/twitter/model/dms/e$b$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/e$b;)V

    .line 344
    invoke-static {p2}, Lcom/twitter/model/dms/a$c;->a(Lcom/twitter/model/dms/a$c;)Lcck;

    move-result-object v0

    sget-object v1, Lcck;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 345
    invoke-static {p2}, Lcom/twitter/model/dms/a$c;->b(Lcom/twitter/model/dms/a$c;)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 346
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/e$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 332
    check-cast p2, Lcom/twitter/model/dms/a$c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/a$c$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/a$c;)V

    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 332
    check-cast p2, Lcom/twitter/model/dms/a$c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/a$c$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/a$c;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/twitter/model/dms/a$c$b;->a()Lcom/twitter/model/dms/a$c$a;

    move-result-object v0

    return-object v0
.end method
