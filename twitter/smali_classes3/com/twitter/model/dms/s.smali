.class public Lcom/twitter/model/dms/s;
.super Lcom/twitter/model/dms/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/s$b;,
        Lcom/twitter/model/dms/s$a;
    }
.end annotation


# static fields
.field public static final i:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/s;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/model/dms/s$b;

    invoke-direct {v0}, Lcom/twitter/model/dms/s$b;-><init>()V

    sput-object v0, Lcom/twitter/model/dms/s;->i:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method protected constructor <init>(Lcom/twitter/model/dms/s$a;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/a;-><init>(Lcom/twitter/model/dms/a$a;)V

    .line 25
    invoke-static {p1}, Lcom/twitter/model/dms/s$a;->a(Lcom/twitter/model/dms/s$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/dms/s;->j:I

    .line 26
    return-void
.end method

.method private a(Lcom/twitter/model/dms/s;)Z
    .locals 2

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/twitter/model/dms/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/model/dms/s;->j:I

    .line 47
    invoke-virtual {p1}, Lcom/twitter/model/dms/s;->o()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 46
    :goto_0
    return v0

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected G()Lcom/twitter/model/dms/s$a;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/twitter/model/dms/s$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/dms/s$a;-><init>(Lcom/twitter/model/dms/s;Lcom/twitter/model/dms/s$1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 42
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/s;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/s;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/s;->a(Lcom/twitter/model/dms/s;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 52
    invoke-super {p0}, Lcom/twitter/model/dms/a;->hashCode()I

    move-result v0

    .line 53
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/dms/s;->j:I

    add-int/2addr v0, v1

    .line 54
    return v0
.end method

.method protected synthetic i()Lcom/twitter/model/dms/a$a;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/model/dms/s;->G()Lcom/twitter/model/dms/s$a;

    move-result-object v0

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/twitter/model/dms/s;->j:I

    return v0
.end method
