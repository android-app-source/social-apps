.class abstract Lcom/twitter/model/dms/p$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/twitter/model/dms/p;",
        "B:",
        "Lcom/twitter/model/dms/p$a",
        "<TM;TB;>;>",
        "Lcom/twitter/util/object/i",
        "<TM;>;"
    }
.end annotation


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/l;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/d;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/p$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/model/dms/p$a;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/p$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/model/dms/p$a;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public c(Ljava/util/List;)Lcom/twitter/model/dms/p$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/d;",
            ">;)TB;"
        }
    .end annotation

    .prologue
    .line 66
    iput-object p1, p0, Lcom/twitter/model/dms/p$a;->b:Ljava/util/List;

    .line 67
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/p$a;

    return-object v0
.end method

.method public d(Ljava/util/List;)Lcom/twitter/model/dms/p$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)TB;"
        }
    .end annotation

    .prologue
    .line 72
    iput-object p1, p0, Lcom/twitter/model/dms/p$a;->c:Ljava/util/List;

    .line 73
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/p$a;

    return-object v0
.end method
