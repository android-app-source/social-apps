.class public Lcom/twitter/model/dms/ad;
.super Lcom/twitter/model/dms/p;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/ad$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/twitter/model/dms/ad$a;)V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/twitter/model/dms/p;-><init>(Lcom/twitter/model/dms/p$a;I)V

    .line 22
    invoke-static {p1}, Lcom/twitter/model/dms/ad$a;->a(Lcom/twitter/model/dms/ad$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/ad;->a:Ljava/util/List;

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/ad$a;Lcom/twitter/model/dms/ad$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/ad;-><init>(Lcom/twitter/model/dms/ad$a;)V

    return-void
.end method


# virtual methods
.method public g()Lcom/twitter/model/dms/s;
    .locals 3

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/twitter/model/dms/ad;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    .line 28
    invoke-virtual {v0}, Lcom/twitter/model/dms/d;->o()I

    move-result v2

    if-nez v2, :cond_0

    .line 29
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/s;

    .line 33
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
