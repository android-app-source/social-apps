.class public final Lcom/twitter/model/dms/r$c$a;
.super Lcom/twitter/model/dms/e$b$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/r$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/e$b$b",
        "<",
        "Lcom/twitter/model/dms/r$c$a;",
        "Lcom/twitter/model/dms/r$c;",
        ">;"
    }
.end annotation


# instance fields
.field private f:I

.field private g:I

.field private h:Lcom/twitter/model/drafts/DraftAttachment;

.field private i:Ljava/lang/String;

.field private j:Lccl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Lcom/twitter/model/dms/e$b$b;-><init>()V

    .line 173
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/dms/r$c;)V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/e$b$b;-><init>(Lcom/twitter/model/dms/e$b;)V

    .line 177
    iget v0, p1, Lcom/twitter/model/dms/r$c;->l:I

    iput v0, p0, Lcom/twitter/model/dms/r$c$a;->f:I

    .line 178
    iget v0, p1, Lcom/twitter/model/dms/r$c;->m:I

    iput v0, p0, Lcom/twitter/model/dms/r$c$a;->g:I

    .line 179
    iget-object v0, p1, Lcom/twitter/model/dms/r$c;->n:Lcom/twitter/model/drafts/DraftAttachment;

    iput-object v0, p0, Lcom/twitter/model/dms/r$c$a;->h:Lcom/twitter/model/drafts/DraftAttachment;

    .line 180
    iget-object v0, p1, Lcom/twitter/model/dms/r$c;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/dms/r$c$a;->i:Ljava/lang/String;

    .line 181
    iget-object v0, p1, Lcom/twitter/model/dms/r$c;->p:Lccl;

    iput-object v0, p0, Lcom/twitter/model/dms/r$c$a;->j:Lccl;

    .line 182
    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/r$c$a;)I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/twitter/model/dms/r$c$a;->f:I

    return v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/r$c$a;)I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/twitter/model/dms/r$c$a;->g:I

    return v0
.end method

.method static synthetic c(Lcom/twitter/model/dms/r$c$a;)Lcom/twitter/model/drafts/DraftAttachment;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/model/dms/r$c$a;->h:Lcom/twitter/model/drafts/DraftAttachment;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/dms/r$c$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/model/dms/r$c$a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/model/dms/r$c$a;)Lccl;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/model/dms/r$c$a;->j:Lccl;

    return-object v0
.end method


# virtual methods
.method public a(Lccl;)Lcom/twitter/model/dms/r$c$a;
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/twitter/model/dms/r$c$a;->j:Lccl;

    .line 211
    return-object p0
.end method

.method public a(Lcom/twitter/model/drafts/DraftAttachment;)Lcom/twitter/model/dms/r$c$a;
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/twitter/model/dms/r$c$a;->h:Lcom/twitter/model/drafts/DraftAttachment;

    .line 199
    return-object p0
.end method

.method public b(I)Lcom/twitter/model/dms/r$c$a;
    .locals 0

    .prologue
    .line 192
    iput p1, p0, Lcom/twitter/model/dms/r$c$a;->f:I

    .line 193
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/twitter/model/dms/r$c$a;->e()Lcom/twitter/model/dms/r$c;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/twitter/model/dms/r$c$a;
    .locals 0

    .prologue
    .line 204
    iput p1, p0, Lcom/twitter/model/dms/r$c$a;->g:I

    .line 205
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/dms/r$c$a;
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/twitter/model/dms/r$c$a;->i:Ljava/lang/String;

    .line 187
    return-object p0
.end method

.method protected e()Lcom/twitter/model/dms/r$c;
    .locals 2

    .prologue
    .line 217
    new-instance v0, Lcom/twitter/model/dms/r$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/dms/r$c;-><init>(Lcom/twitter/model/dms/r$c$a;Lcom/twitter/model/dms/r$1;)V

    return-object v0
.end method
