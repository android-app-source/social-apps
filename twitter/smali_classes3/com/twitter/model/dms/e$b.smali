.class public abstract Lcom/twitter/model/dms/e$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/e$b$a;,
        Lcom/twitter/model/dms/e$b$b;
    }
.end annotation


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Lcom/twitter/model/core/v;

.field public final e:Ljava/lang/String;

.field public final f:Lcom/twitter/model/core/v;

.field public final g:Lcbx;

.field public final h:Ljava/lang/String;

.field public final i:Z

.field public final j:Z

.field public final k:I


# direct methods
.method protected constructor <init>(Lcom/twitter/model/dms/e$b$b;)V
    .locals 2

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    invoke-static {p1}, Lcom/twitter/model/dms/e$b$b;->a(Lcom/twitter/model/dms/e$b$b;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/e$b;->b:J

    .line 195
    iget-object v0, p1, Lcom/twitter/model/dms/e$b$b;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/dms/e$b;->c:Ljava/lang/String;

    .line 196
    iget-object v0, p1, Lcom/twitter/model/dms/e$b$b;->b:Lcom/twitter/model/core/v;

    sget-object v1, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/dms/e$b;->d:Lcom/twitter/model/core/v;

    .line 197
    iget-object v0, p1, Lcom/twitter/model/dms/e$b$b;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/dms/e$b;->e:Ljava/lang/String;

    .line 198
    iget-object v0, p1, Lcom/twitter/model/dms/e$b$b;->d:Lcom/twitter/model/core/v;

    sget-object v1, Lcom/twitter/model/core/v;->a:Lcom/twitter/model/core/v;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/dms/e$b;->f:Lcom/twitter/model/core/v;

    .line 199
    iget-object v0, p1, Lcom/twitter/model/dms/e$b$b;->e:Lcbx;

    iput-object v0, p0, Lcom/twitter/model/dms/e$b;->g:Lcbx;

    .line 200
    invoke-static {p1}, Lcom/twitter/model/dms/e$b$b;->b(Lcom/twitter/model/dms/e$b$b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/e$b;->h:Ljava/lang/String;

    .line 201
    invoke-static {p1}, Lcom/twitter/model/dms/e$b$b;->c(Lcom/twitter/model/dms/e$b$b;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/e$b;->i:Z

    .line 202
    invoke-static {p1}, Lcom/twitter/model/dms/e$b$b;->d(Lcom/twitter/model/dms/e$b$b;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/e$b;->j:Z

    .line 203
    invoke-static {p1}, Lcom/twitter/model/dms/e$b$b;->e(Lcom/twitter/model/dms/e$b$b;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/dms/e$b;->k:I

    .line 204
    return-void
.end method

.method private a(Lcom/twitter/model/dms/e$b;)Z
    .locals 4

    .prologue
    .line 212
    iget-wide v0, p0, Lcom/twitter/model/dms/e$b;->b:J

    iget-wide v2, p1, Lcom/twitter/model/dms/e$b;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/e$b;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/e$b;->c:Ljava/lang/String;

    .line 213
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/e$b;->d:Lcom/twitter/model/core/v;

    iget-object v1, p1, Lcom/twitter/model/dms/e$b;->d:Lcom/twitter/model/core/v;

    .line 214
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/v;->a(Lcom/twitter/model/core/v;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/e$b;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/e$b;->e:Ljava/lang/String;

    .line 215
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/e$b;->f:Lcom/twitter/model/core/v;

    iget-object v1, p1, Lcom/twitter/model/dms/e$b;->f:Lcom/twitter/model/core/v;

    .line 216
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/v;->a(Lcom/twitter/model/core/v;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/e$b;->g:Lcbx;

    iget-object v1, p1, Lcom/twitter/model/dms/e$b;->g:Lcbx;

    .line 217
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/e$b;->h:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/e$b;->h:Ljava/lang/String;

    .line 218
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/dms/e$b;->i:Z

    iget-boolean v1, p1, Lcom/twitter/model/dms/e$b;->i:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/dms/e$b;->j:Z

    iget-boolean v1, p1, Lcom/twitter/model/dms/e$b;->j:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/model/dms/e$b;->k:I

    iget v1, p1, Lcom/twitter/model/dms/e$b;->k:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 212
    :goto_0
    return v0

    .line 218
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 208
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/e$b;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/e$b;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/e$b;->a(Lcom/twitter/model/dms/e$b;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 226
    iget-wide v4, p0, Lcom/twitter/model/dms/e$b;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    .line 227
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/dms/e$b;->c:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 228
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/dms/e$b;->d:Lcom/twitter/model/core/v;

    invoke-virtual {v3}, Lcom/twitter/model/core/v;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 229
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/e$b;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/e$b;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v3

    .line 230
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/twitter/model/dms/e$b;->f:Lcom/twitter/model/core/v;

    invoke-virtual {v3}, Lcom/twitter/model/core/v;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 231
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/e$b;->g:Lcbx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/dms/e$b;->g:Lcbx;

    invoke-virtual {v0}, Lcbx;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    .line 232
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/e$b;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/dms/e$b;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v3

    .line 233
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/model/dms/e$b;->i:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v3

    .line 234
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/twitter/model/dms/e$b;->j:Z

    if-eqz v3, :cond_4

    :goto_4
    add-int/2addr v0, v2

    .line 235
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/dms/e$b;->k:I

    add-int/2addr v0, v1

    .line 236
    return v0

    :cond_0
    move v0, v1

    .line 229
    goto :goto_0

    :cond_1
    move v0, v1

    .line 231
    goto :goto_1

    :cond_2
    move v0, v1

    .line 232
    goto :goto_2

    :cond_3
    move v0, v1

    .line 233
    goto :goto_3

    :cond_4
    move v2, v1

    .line 234
    goto :goto_4
.end method
