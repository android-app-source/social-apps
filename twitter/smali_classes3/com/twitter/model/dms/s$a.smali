.class public final Lcom/twitter/model/dms/s$a;
.super Lcom/twitter/model/dms/a$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/a$a",
        "<",
        "Lcom/twitter/model/dms/s;",
        "Lcom/twitter/model/dms/s$a;",
        ">;"
    }
.end annotation


# instance fields
.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/twitter/model/dms/a$a;-><init>()V

    .line 62
    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/s;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/a$a;-><init>(Lcom/twitter/model/dms/a;)V

    .line 66
    invoke-virtual {p1}, Lcom/twitter/model/dms/s;->o()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/dms/s$a;->d:I

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/s;Lcom/twitter/model/dms/s$1;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/s$a;-><init>(Lcom/twitter/model/dms/s;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/s$a;)I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/twitter/model/dms/s$a;->d:I

    return v0
.end method


# virtual methods
.method public b(Z)Lcom/twitter/model/dms/s$a;
    .locals 1

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/twitter/model/dms/s$a;->e:Z

    .line 72
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/s$a;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/twitter/model/dms/s$a;->e()Lcom/twitter/model/dms/s;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/twitter/model/dms/s;
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/twitter/model/dms/s$a;->e:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x13

    :goto_0
    iput v0, p0, Lcom/twitter/model/dms/s$a;->d:I

    .line 81
    new-instance v0, Lcom/twitter/model/dms/s;

    invoke-direct {v0, p0}, Lcom/twitter/model/dms/s;-><init>(Lcom/twitter/model/dms/s$a;)V

    return-object v0

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
