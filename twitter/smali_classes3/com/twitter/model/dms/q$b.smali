.class Lcom/twitter/model/dms/q$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/dms/q;",
        "Lcom/twitter/model/dms/q$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/q$1;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/twitter/model/dms/q$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/dms/q$a;
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/twitter/model/dms/q$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/q$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/q$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 121
    .line 122
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/q$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 123
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/model/dms/q$a;->a(Lcom/twitter/model/dms/q$a;Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 124
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->c(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 125
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->a(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 126
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/q$a;->a(J)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 127
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->b(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    .line 129
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 128
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/q$a;->a(Ljava/util/List;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 130
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->c(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 131
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->d(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/dms/m;->a:Lcom/twitter/util/serialization/b;

    .line 132
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/m;

    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/q$a;->a(Lcom/twitter/model/dms/m;)Lcom/twitter/model/dms/q$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/dms/x;->a:Lcom/twitter/util/serialization/l;

    .line 133
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/x;

    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/q$a;->a(Lcom/twitter/model/dms/x;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 134
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->b(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    .line 135
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 92
    check-cast p2, Lcom/twitter/model/dms/q$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/q$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/q$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/q;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p2, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    .line 97
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/dms/q;->d:Ljava/lang/String;

    .line 98
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/dms/q;->e:Ljava/lang/String;

    .line 99
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/dms/q;->f:Z

    .line 100
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/dms/q;->g:J

    .line 101
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/dms/q;->h:Z

    .line 102
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    sget-object v2, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    .line 104
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 103
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/dms/q;->j:Z

    .line 105
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/dms/q;->k:Z

    .line 106
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/dms/q;->l:Lcom/twitter/model/dms/m;

    sget-object v2, Lcom/twitter/model/dms/m;->a:Lcom/twitter/util/serialization/b;

    .line 107
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/dms/q;->m:Lcom/twitter/model/dms/x;

    sget-object v2, Lcom/twitter/model/dms/x;->a:Lcom/twitter/util/serialization/l;

    .line 108
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/dms/q;->c:Ljava/lang/String;

    .line 109
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 110
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    check-cast p2, Lcom/twitter/model/dms/q;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/q$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/q;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/twitter/model/dms/q$b;->a()Lcom/twitter/model/dms/q$a;

    move-result-object v0

    return-object v0
.end method
