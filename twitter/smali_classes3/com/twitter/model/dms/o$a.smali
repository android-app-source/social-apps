.class public final Lcom/twitter/model/dms/o$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/dms/o;",
        ">;"
    }
.end annotation


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/o$a;)J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/twitter/model/dms/o$a;->a:J

    return-wide v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/model/dms/o$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/dms/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/model/dms/o$a;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 4

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/twitter/model/dms/o$a;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/o$a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/o$a;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcom/twitter/model/dms/o$a;
    .locals 1

    .prologue
    .line 65
    iput-wide p1, p0, Lcom/twitter/model/dms/o$a;->a:J

    .line 66
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/dms/o$a;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/twitter/model/dms/o$a;->b:Ljava/lang/String;

    .line 72
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/dms/o$a;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/twitter/model/dms/o$a;->c:Ljava/lang/String;

    .line 78
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/twitter/model/dms/o$a;->e()Lcom/twitter/model/dms/o;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/dms/o;
    .locals 2

    .prologue
    .line 89
    new-instance v0, Lcom/twitter/model/dms/o;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/dms/o;-><init>(Lcom/twitter/model/dms/o$a;Lcom/twitter/model/dms/o$1;)V

    return-object v0
.end method
