.class public abstract Lcom/twitter/model/dms/e$b$b;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/e$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<B:",
        "Lcom/twitter/model/dms/e$b$b",
        "<TB;TD;>;D:",
        "Lcom/twitter/model/dms/e$b;",
        ">",
        "Lcom/twitter/util/object/i",
        "<TD;>;"
    }
.end annotation


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Lcom/twitter/model/core/v;

.field protected c:Ljava/lang/String;

.field protected d:Lcom/twitter/model/core/v;

.field protected e:Lcbx;

.field private f:J

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 317
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 318
    return-void
.end method

.method constructor <init>(Lcom/twitter/model/dms/e$b;)V
    .locals 2

    .prologue
    .line 320
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 321
    iget-wide v0, p1, Lcom/twitter/model/dms/e$b;->b:J

    iput-wide v0, p0, Lcom/twitter/model/dms/e$b$b;->f:J

    .line 322
    iget-object v0, p1, Lcom/twitter/model/dms/e$b;->g:Lcbx;

    iput-object v0, p0, Lcom/twitter/model/dms/e$b$b;->e:Lcbx;

    .line 323
    iget-object v0, p1, Lcom/twitter/model/dms/e$b;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/dms/e$b$b;->g:Ljava/lang/String;

    .line 324
    iget-boolean v0, p1, Lcom/twitter/model/dms/e$b;->i:Z

    iput-boolean v0, p0, Lcom/twitter/model/dms/e$b$b;->h:Z

    .line 325
    iget-object v0, p1, Lcom/twitter/model/dms/e$b;->f:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/dms/e$b$b;->d:Lcom/twitter/model/core/v;

    .line 326
    iget-object v0, p1, Lcom/twitter/model/dms/e$b;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/dms/e$b$b;->c:Ljava/lang/String;

    .line 327
    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/e$b$b;)J
    .locals 2

    .prologue
    .line 239
    iget-wide v0, p0, Lcom/twitter/model/dms/e$b$b;->f:J

    return-wide v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/e$b$b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/twitter/model/dms/e$b$b;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/dms/e$b$b;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/twitter/model/dms/e$b$b;->h:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/model/dms/e$b$b;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/twitter/model/dms/e$b$b;->i:Z

    return v0
.end method

.method static synthetic e(Lcom/twitter/model/dms/e$b$b;)I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/twitter/model/dms/e$b$b;->j:I

    return v0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/twitter/model/dms/e$b$b;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 358
    iget-object v0, p0, Lcom/twitter/model/dms/e$b$b;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/e$b$b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x14

    if-gt v0, v1, :cond_0

    .line 359
    iget-object v0, p0, Lcom/twitter/model/dms/e$b$b;->a:Ljava/lang/String;

    invoke-static {v0}, Lcoz;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/e$b$b;->i:Z

    .line 360
    iget-object v0, p0, Lcom/twitter/model/dms/e$b$b;->a:Ljava/lang/String;

    invoke-static {v0}, Lcoz;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/dms/e$b$b;->j:I

    .line 365
    :goto_0
    return-void

    .line 362
    :cond_0
    iput-boolean v2, p0, Lcom/twitter/model/dms/e$b$b;->i:Z

    .line 363
    iput v2, p0, Lcom/twitter/model/dms/e$b$b;->j:I

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lcom/twitter/model/dms/e$b$b;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .prologue
    .line 313
    iput p1, p0, Lcom/twitter/model/dms/e$b$b;->j:I

    .line 314
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b$b;

    return-object v0
.end method

.method public a(Lcbx;)Lcom/twitter/model/dms/e$b$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbx;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 267
    iput-object p1, p0, Lcom/twitter/model/dms/e$b$b;->e:Lcbx;

    .line 268
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b$b;

    return-object v0
.end method

.method a(Lcom/twitter/model/core/v;)Lcom/twitter/model/dms/e$b$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/v;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 275
    iput-object p1, p0, Lcom/twitter/model/dms/e$b$b;->b:Lcom/twitter/model/core/v;

    .line 276
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b$b;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/dms/e$b$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 261
    iput-object p1, p0, Lcom/twitter/model/dms/e$b$b;->g:Ljava/lang/String;

    .line 262
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b$b;

    return-object v0
.end method

.method public a(Z)Lcom/twitter/model/dms/e$b$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .prologue
    .line 255
    iput-boolean p1, p0, Lcom/twitter/model/dms/e$b$b;->h:Z

    .line 256
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b$b;

    return-object v0
.end method

.method public b(J)Lcom/twitter/model/dms/e$b$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TB;"
        }
    .end annotation

    .prologue
    .line 299
    iput-wide p1, p0, Lcom/twitter/model/dms/e$b$b;->f:J

    .line 300
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b$b;

    return-object v0
.end method

.method public b(Lcom/twitter/model/core/v;)Lcom/twitter/model/dms/e$b$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/v;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 287
    iput-object p1, p0, Lcom/twitter/model/dms/e$b$b;->d:Lcom/twitter/model/core/v;

    .line 288
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b$b;

    return-object v0
.end method

.method b(Ljava/lang/String;)Lcom/twitter/model/dms/e$b$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 281
    iput-object p1, p0, Lcom/twitter/model/dms/e$b$b;->a:Ljava/lang/String;

    .line 282
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b$b;

    return-object v0
.end method

.method public b(Z)Lcom/twitter/model/dms/e$b$b;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .prologue
    .line 306
    iput-boolean p1, p0, Lcom/twitter/model/dms/e$b$b;->i:Z

    .line 307
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b$b;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/dms/e$b$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 293
    iput-object p1, p0, Lcom/twitter/model/dms/e$b$b;->c:Ljava/lang/String;

    .line 294
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b$b;

    return-object v0
.end method

.method protected c_()V
    .locals 1

    .prologue
    .line 331
    invoke-super {p0}, Lcom/twitter/util/object/i;->c_()V

    .line 332
    invoke-direct {p0}, Lcom/twitter/model/dms/e$b$b;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 333
    invoke-virtual {p0}, Lcom/twitter/model/dms/e$b$b;->f()V

    .line 335
    :cond_0
    invoke-direct {p0}, Lcom/twitter/model/dms/e$b$b;->g()V

    .line 336
    return-void
.end method

.method protected f()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 340
    invoke-direct {p0}, Lcom/twitter/model/dms/e$b$b;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 341
    iget-object v0, p0, Lcom/twitter/model/dms/e$b$b;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/e$b$b;->a:Ljava/lang/String;

    .line 342
    iget-object v0, p0, Lcom/twitter/model/dms/e$b$b;->d:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/dms/e$b$b;->b:Lcom/twitter/model/core/v;

    .line 343
    return-void

    .line 340
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
