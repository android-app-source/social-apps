.class public abstract Lcom/twitter/model/dms/c$b;
.super Lcom/twitter/model/dms/d$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/twitter/model/dms/c;",
        "B:",
        "Lcom/twitter/model/dms/c$b",
        "<TE;TB;TD;>;D:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/model/dms/d$a",
        "<TE;TB;>;"
    }
.end annotation


# instance fields
.field a:Z

.field b:Z

.field c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field private d:J


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/twitter/model/dms/d$a;-><init>()V

    .line 123
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/model/dms/c$b;->d:J

    .line 125
    return-void
.end method

.method constructor <init>(Lcom/twitter/model/dms/c;)V
    .locals 2

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/d$a;-><init>(Lcom/twitter/model/dms/d;)V

    .line 123
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/model/dms/c$b;->d:J

    .line 133
    iget-boolean v0, p1, Lcom/twitter/model/dms/c;->c:Z

    iput-boolean v0, p0, Lcom/twitter/model/dms/c$b;->a:Z

    .line 134
    invoke-virtual {p1}, Lcom/twitter/model/dms/c;->l()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/c$b;->d:J

    .line 135
    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/c$b;)J
    .locals 2

    .prologue
    .line 118
    iget-wide v0, p0, Lcom/twitter/model/dms/c$b;->d:J

    return-wide v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/model/dms/c$b;->c:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcom/twitter/model/dms/c$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TB;"
        }
    .end annotation

    .prologue
    .line 156
    iput-wide p1, p0, Lcom/twitter/model/dms/c$b;->d:J

    .line 157
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c$b;

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)TB;"
        }
    .end annotation

    .prologue
    .line 162
    iput-object p1, p0, Lcom/twitter/model/dms/c$b;->c:Ljava/lang/Object;

    .line 163
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c$b;

    return-object v0
.end method

.method public a(Z)Lcom/twitter/model/dms/c$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/twitter/model/dms/c$b;->a:Z

    .line 151
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c$b;

    return-object v0
.end method

.method protected c_()V
    .locals 4

    .prologue
    .line 139
    invoke-super {p0}, Lcom/twitter/model/dms/d$a;->c_()V

    .line 140
    iget-wide v0, p0, Lcom/twitter/model/dms/c$b;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 142
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/model/dms/c$b;->d:J

    .line 145
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/model/dms/c$b;->a:Z

    iput-boolean v0, p0, Lcom/twitter/model/dms/c$b;->b:Z

    .line 146
    return-void
.end method
