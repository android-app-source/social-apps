.class Lcom/twitter/model/dms/h$c$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/h$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/dms/h$c;",
        "Lcom/twitter/model/dms/h$c$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/h$1;)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/twitter/model/dms/h$c$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/dms/h$c$a;
    .locals 1

    .prologue
    .line 170
    new-instance v0, Lcom/twitter/model/dms/h$c$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/h$c$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/h$c$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 162
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/h$c$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/h$c$a;

    move-result-object v0

    .line 163
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/h$c$a;->a(Ljava/lang/Integer;)Lcom/twitter/model/dms/h$c$a;

    move-result-object v0

    .line 164
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/h$c$a;->b(Ljava/lang/String;)Lcom/twitter/model/dms/h$c$a;

    .line 165
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 148
    check-cast p2, Lcom/twitter/model/dms/h$c$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/h$c$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/h$c$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/h$c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p2, Lcom/twitter/model/dms/h$c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 153
    iget v0, p2, Lcom/twitter/model/dms/h$c;->c:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 154
    iget-object v0, p2, Lcom/twitter/model/dms/h$c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 155
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    check-cast p2, Lcom/twitter/model/dms/h$c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/h$c$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/h$c;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/twitter/model/dms/h$c$b;->a()Lcom/twitter/model/dms/h$c$a;

    move-result-object v0

    return-object v0
.end method
