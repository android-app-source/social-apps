.class public final Lcom/twitter/model/dms/k$a;
.super Lcom/twitter/model/dms/p$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/p$a",
        "<",
        "Lcom/twitter/model/dms/k;",
        "Lcom/twitter/model/dms/k$a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:I

.field private c:J

.field private d:J

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/twitter/model/dms/p$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/k$a;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/twitter/model/dms/k$a;->b:I

    return v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/k$a;)J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/twitter/model/dms/k$a;->c:J

    return-wide v0
.end method

.method static synthetic c(Lcom/twitter/model/dms/k$a;)J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/twitter/model/dms/k$a;->d:J

    return-wide v0
.end method

.method static synthetic d(Lcom/twitter/model/dms/k$a;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/twitter/model/dms/k$a;->f:I

    return v0
.end method

.method static synthetic e(Lcom/twitter/model/dms/k$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/model/dms/k$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/model/dms/k$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/model/dms/k$a;->g:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/model/dms/k$a;
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/twitter/model/dms/k$a;->b:I

    .line 83
    return-object p0
.end method

.method public a(J)Lcom/twitter/model/dms/k$a;
    .locals 1

    .prologue
    .line 70
    iput-wide p1, p0, Lcom/twitter/model/dms/k$a;->d:J

    .line 71
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/dms/k$a;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/twitter/model/dms/k$a;->e:Ljava/lang/String;

    .line 53
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/dms/k$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/l;",
            ">;)",
            "Lcom/twitter/model/dms/k$a;"
        }
    .end annotation

    .prologue
    .line 58
    iput-object p1, p0, Lcom/twitter/model/dms/k$a;->a:Ljava/util/List;

    .line 59
    return-object p0
.end method

.method public b(I)Lcom/twitter/model/dms/k$a;
    .locals 0

    .prologue
    .line 88
    iput p1, p0, Lcom/twitter/model/dms/k$a;->f:I

    .line 89
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/dms/k$a;
    .locals 1

    .prologue
    .line 76
    iput-wide p1, p0, Lcom/twitter/model/dms/k$a;->c:J

    .line 77
    return-object p0
.end method

.method public b(Ljava/util/List;)Lcom/twitter/model/dms/k$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/o;",
            ">;)",
            "Lcom/twitter/model/dms/k$a;"
        }
    .end annotation

    .prologue
    .line 64
    iput-object p1, p0, Lcom/twitter/model/dms/k$a;->g:Ljava/util/List;

    .line 65
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/twitter/model/dms/k$a;->e()Lcom/twitter/model/dms/k;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/dms/k;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Lcom/twitter/model/dms/k;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/dms/k;-><init>(Lcom/twitter/model/dms/k$a;Lcom/twitter/model/dms/k$1;)V

    return-object v0
.end method
