.class public final Lcom/twitter/model/dms/Participant$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/Participant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/dms/Participant;",
        ">;"
    }
.end annotation


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:J

.field private e:Ljava/lang/String;

.field private f:Lcom/twitter/model/core/TwitterUser;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 168
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/model/dms/Participant$a;->g:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/Participant$a;)J
    .locals 2

    .prologue
    .line 161
    iget-wide v0, p0, Lcom/twitter/model/dms/Participant$a;->a:J

    return-wide v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/Participant$a;)J
    .locals 2

    .prologue
    .line 161
    iget-wide v0, p0, Lcom/twitter/model/dms/Participant$a;->b:J

    return-wide v0
.end method

.method static synthetic c(Lcom/twitter/model/dms/Participant$a;)J
    .locals 2

    .prologue
    .line 161
    iget-wide v0, p0, Lcom/twitter/model/dms/Participant$a;->c:J

    return-wide v0
.end method

.method static synthetic d(Lcom/twitter/model/dms/Participant$a;)J
    .locals 2

    .prologue
    .line 161
    iget-wide v0, p0, Lcom/twitter/model/dms/Participant$a;->d:J

    return-wide v0
.end method

.method static synthetic e(Lcom/twitter/model/dms/Participant$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/twitter/model/dms/Participant$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/model/dms/Participant$a;)Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/twitter/model/dms/Participant$a;->f:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/model/dms/Participant$a;)I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/twitter/model/dms/Participant$a;->g:I

    return v0
.end method


# virtual methods
.method public R_()Z
    .locals 4

    .prologue
    .line 214
    iget-wide v0, p0, Lcom/twitter/model/dms/Participant$a;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/dms/Participant$a;->f:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/Participant$a;->f:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-wide v2, p0, Lcom/twitter/model/dms/Participant$a;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/model/dms/Participant$a;
    .locals 0

    .prologue
    .line 208
    iput p1, p0, Lcom/twitter/model/dms/Participant$a;->g:I

    .line 209
    return-object p0
.end method

.method public a(J)Lcom/twitter/model/dms/Participant$a;
    .locals 1

    .prologue
    .line 172
    iput-wide p1, p0, Lcom/twitter/model/dms/Participant$a;->a:J

    .line 173
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/dms/Participant$a;
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/twitter/model/dms/Participant$a;->f:Lcom/twitter/model/core/TwitterUser;

    .line 203
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/dms/Participant$a;
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/twitter/model/dms/Participant$a;->e:Ljava/lang/String;

    .line 197
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/dms/Participant$a;
    .locals 1

    .prologue
    .line 178
    iput-wide p1, p0, Lcom/twitter/model/dms/Participant$a;->b:J

    .line 179
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/twitter/model/dms/Participant$a;->e()Lcom/twitter/model/dms/Participant;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/twitter/model/dms/Participant$a;
    .locals 1

    .prologue
    .line 184
    iput-wide p1, p0, Lcom/twitter/model/dms/Participant$a;->c:J

    .line 185
    return-object p0
.end method

.method public d(J)Lcom/twitter/model/dms/Participant$a;
    .locals 1

    .prologue
    .line 190
    iput-wide p1, p0, Lcom/twitter/model/dms/Participant$a;->d:J

    .line 191
    return-object p0
.end method

.method protected e()Lcom/twitter/model/dms/Participant;
    .locals 2

    .prologue
    .line 220
    new-instance v0, Lcom/twitter/model/dms/Participant;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/dms/Participant;-><init>(Lcom/twitter/model/dms/Participant$a;Lcom/twitter/model/dms/Participant$1;)V

    return-object v0
.end method
