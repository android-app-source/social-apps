.class Lcom/twitter/model/dms/o$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/dms/o;",
        "Lcom/twitter/model/dms/o$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/o$1;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/twitter/model/dms/o$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/dms/o$a;
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/twitter/model/dms/o$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/o$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/o$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/dms/o$a;->a(J)Lcom/twitter/model/dms/o$a;

    move-result-object v0

    .line 105
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/o$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/o$a;

    move-result-object v0

    .line 106
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/o$a;->b(Ljava/lang/String;)Lcom/twitter/model/dms/o$a;

    .line 107
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 93
    check-cast p2, Lcom/twitter/model/dms/o$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/o$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/o$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iget-wide v0, p2, Lcom/twitter/model/dms/o;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 113
    iget-object v0, p2, Lcom/twitter/model/dms/o;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 114
    iget-object v0, p2, Lcom/twitter/model/dms/o;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 115
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    check-cast p2, Lcom/twitter/model/dms/o;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/o$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/o;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/twitter/model/dms/o$b;->a()Lcom/twitter/model/dms/o$a;

    move-result-object v0

    return-object v0
.end method
