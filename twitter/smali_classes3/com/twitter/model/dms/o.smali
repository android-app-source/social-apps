.class public Lcom/twitter/model/dms/o;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/o$b;,
        Lcom/twitter/model/dms/o$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/dms/o;",
            "Lcom/twitter/model/dms/o$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/model/dms/o$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/dms/o$b;-><init>(Lcom/twitter/model/dms/o$1;)V

    sput-object v0, Lcom/twitter/model/dms/o;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/o$a;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lcom/twitter/model/dms/o$a;->a(Lcom/twitter/model/dms/o$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/o;->b:J

    .line 36
    invoke-static {p1}, Lcom/twitter/model/dms/o$a;->b(Lcom/twitter/model/dms/o$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/o;->c:Ljava/lang/String;

    .line 37
    invoke-static {p1}, Lcom/twitter/model/dms/o$a;->c(Lcom/twitter/model/dms/o$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/o;->d:Ljava/lang/String;

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/o$a;Lcom/twitter/model/dms/o$1;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/o;-><init>(Lcom/twitter/model/dms/o$a;)V

    return-void
.end method

.method private a(Lcom/twitter/model/dms/o;)Z
    .locals 4

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/twitter/model/dms/o;->b:J

    iget-wide v2, p1, Lcom/twitter/model/dms/o;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/o;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/o;->c:Ljava/lang/String;

    .line 54
    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/o;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/o;->d:Ljava/lang/String;

    .line 55
    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 53
    :goto_0
    return v0

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 49
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/o;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/o;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/o;->a(Lcom/twitter/model/dms/o;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/twitter/model/dms/o;->b:J

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v0

    .line 43
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/dms/o;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/dms/o;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 44
    return v0
.end method
