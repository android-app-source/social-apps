.class public abstract Lcom/twitter/model/dms/e;
.super Lcom/twitter/model/dms/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/e$b;,
        Lcom/twitter/model/dms/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/model/dms/e$b;",
        ">",
        "Lcom/twitter/model/dms/c",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public h:J


# direct methods
.method protected constructor <init>(Lcom/twitter/model/dms/e$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/dms/e$a",
            "<**TT;>;)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/c;-><init>(Lcom/twitter/model/dms/c$b;)V

    .line 35
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/model/dms/e;->h:J

    .line 36
    return-void
.end method

.method private b(Lcom/twitter/model/dms/e;)Z
    .locals 4

    .prologue
    .line 159
    invoke-super {p0, p1}, Lcom/twitter/model/dms/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/dms/e;->h:J

    iget-wide v2, p1, Lcom/twitter/model/dms/e;->h:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/model/dms/e;->a(I)Z

    move-result v0

    return v0
.end method

.method public B()Z
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/model/dms/e;->a(I)Z

    move-result v0

    return v0
.end method

.method public C()Z
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/model/dms/e;->a(I)Z

    move-result v0

    return v0
.end method

.method public D()Z
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->z()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public E()Z
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->B()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public F()Z
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/twitter/model/dms/e;->a(I)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/twitter/model/dms/d;)J
    .locals 4

    .prologue
    .line 150
    iget-wide v0, p1, Lcom/twitter/model/dms/d;->g:J

    iget-wide v2, p0, Lcom/twitter/model/dms/e;->g:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    return-wide v0
.end method

.method protected a(I)Z
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    invoke-virtual {v0}, Lcbx;->a()I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/dms/e;)Z
    .locals 4

    .prologue
    .line 139
    invoke-virtual {p1}, Lcom/twitter/model/dms/e;->l()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->l()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 140
    invoke-virtual {p0, p1}, Lcom/twitter/model/dms/e;->a(Lcom/twitter/model/dms/d;)J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->x()Z

    move-result v0

    invoke-virtual {p1}, Lcom/twitter/model/dms/e;->x()Z

    move-result v1

    if-ne v0, v1, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->w()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    invoke-virtual {p1}, Lcom/twitter/model/dms/e;->w()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 139
    :goto_0
    return v0

    .line 143
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(ZZZ)Lcom/twitter/model/dms/e;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ)",
            "Lcom/twitter/model/dms/e",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 101
    return-object p0
.end method

.method public abstract d()Z
.end method

.method public abstract e()Z
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 155
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/e;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/e;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/e;->b(Lcom/twitter/model/dms/e;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 165
    invoke-super {p0}, Lcom/twitter/model/dms/c;->hashCode()I

    move-result v0

    .line 166
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/model/dms/e;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    return v0
.end method

.method public abstract o()I
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public final q()J
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b;

    iget-wide v0, v0, Lcom/twitter/model/dms/e$b;->b:J

    return-wide v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b;

    iget-object v0, v0, Lcom/twitter/model/dms/e$b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Lcom/twitter/model/core/v;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b;

    iget-object v0, v0, Lcom/twitter/model/dms/e$b;->d:Lcom/twitter/model/core/v;

    return-object v0
.end method

.method public final u()Lcbx;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b;

    iget-object v0, v0, Lcom/twitter/model/dms/e$b;->g:Lcbx;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b;

    iget-object v0, v0, Lcom/twitter/model/dms/e$b;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b;

    iget-boolean v0, v0, Lcom/twitter/model/dms/e$b;->i:Z

    return v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b;

    iget-boolean v0, v0, Lcom/twitter/model/dms/e$b;->j:Z

    return v0
.end method

.method public final y()I
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e$b;

    iget v0, v0, Lcom/twitter/model/dms/e$b;->k:I

    return v0
.end method

.method public z()Z
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/model/dms/e;->a(I)Z

    move-result v0

    return v0
.end method
