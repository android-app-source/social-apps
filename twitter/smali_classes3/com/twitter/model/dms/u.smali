.class public Lcom/twitter/model/dms/u;
.super Lcom/twitter/model/dms/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/u$b;,
        Lcom/twitter/model/dms/u$a;
    }
.end annotation


# static fields
.field public static final i:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/twitter/model/dms/u$b;

    invoke-direct {v0}, Lcom/twitter/model/dms/u$b;-><init>()V

    sput-object v0, Lcom/twitter/model/dms/u;->i:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/u$a;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/a;-><init>(Lcom/twitter/model/dms/a$a;)V

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/u$a;Lcom/twitter/model/dms/u$1;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/u;-><init>(Lcom/twitter/model/dms/u$a;)V

    return-void
.end method

.method public static G()Z
    .locals 1

    .prologue
    .line 32
    const-string/jumbo v0, "b2c_welcome_message_create_dm_event_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected H()Lcom/twitter/model/dms/u$a;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/twitter/model/dms/u$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/dms/u$a;-><init>(Lcom/twitter/model/dms/u;Lcom/twitter/model/dms/u$1;)V

    return-object v0
.end method

.method protected synthetic i()Lcom/twitter/model/dms/a$a;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/model/dms/u;->H()Lcom/twitter/model/dms/u$a;

    move-result-object v0

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method
