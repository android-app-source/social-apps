.class public final Lcom/twitter/model/dms/h$c$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/h$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/dms/h$c;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 121
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/model/dms/h$c$a;->c:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/h$c$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/model/dms/h$c$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/h$c$a;)I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/twitter/model/dms/h$c$a;->c:I

    return v0
.end method

.method static synthetic c(Lcom/twitter/model/dms/h$c$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/model/dms/h$c$a;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Integer;)Lcom/twitter/model/dms/h$c$a;
    .locals 1

    .prologue
    .line 131
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/dms/h$c$a;->c:I

    .line 132
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/dms/h$c$a;
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/twitter/model/dms/h$c$a;->a:Ljava/lang/String;

    .line 126
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/dms/h$c$a;
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/twitter/model/dms/h$c$a;->b:Ljava/lang/String;

    .line 138
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/twitter/model/dms/h$c$a;->e()Lcom/twitter/model/dms/h$c;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/dms/h$c;
    .locals 2

    .prologue
    .line 144
    new-instance v0, Lcom/twitter/model/dms/h$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/dms/h$c;-><init>(Lcom/twitter/model/dms/h$c$a;Lcom/twitter/model/dms/h$1;)V

    return-object v0
.end method
