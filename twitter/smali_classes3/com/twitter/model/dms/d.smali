.class public abstract Lcom/twitter/model/dms/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/core/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/d$a;
    }
.end annotation


# instance fields
.field public final e:J

.field public final f:Ljava/lang/String;

.field public final g:J


# direct methods
.method constructor <init>(Lcom/twitter/model/dms/d$a;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Lcom/twitter/model/dms/d$a;->a(Lcom/twitter/model/dms/d$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/d;->e:J

    .line 23
    invoke-static {p1}, Lcom/twitter/model/dms/d$a;->b(Lcom/twitter/model/dms/d$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/d;->f:Ljava/lang/String;

    .line 24
    invoke-static {p1}, Lcom/twitter/model/dms/d$a;->c(Lcom/twitter/model/dms/d$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/d;->g:J

    .line 25
    return-void
.end method

.method private a(Lcom/twitter/model/dms/d;)Z
    .locals 4

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/twitter/model/dms/d;->e:J

    iget-wide v2, p1, Lcom/twitter/model/dms/d;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/d;->f:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/d;->f:Ljava/lang/String;

    .line 56
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/dms/d;->g:J

    iget-wide v2, p1, Lcom/twitter/model/dms/d;->g:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 55
    :goto_0
    return v0

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/twitter/model/dms/d;->e:J

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/twitter/model/dms/d;->e:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Z
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/twitter/model/dms/d;->l()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 51
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/d;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/d;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/d;->a(Lcom/twitter/model/dms/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/twitter/model/dms/d;->e:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    .line 63
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/d;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/d;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    .line 64
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/model/dms/d;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    return v0

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 46
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public abstract o()I
.end method
