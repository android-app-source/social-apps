.class public Lcom/twitter/model/dms/i;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/i$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:J

.field public final f:J

.field public final g:J

.field public final h:J

.field public final i:Z

.field public final j:J

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Lcom/twitter/model/dms/x;

.field public final o:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/twitter/model/dms/i$a;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->a(Lcom/twitter/model/dms/i$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/i;->a:Ljava/lang/String;

    .line 32
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->b(Lcom/twitter/model/dms/i$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/i;->b:Ljava/lang/String;

    .line 33
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->c(Lcom/twitter/model/dms/i$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/i;->c:Ljava/lang/String;

    .line 34
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->d(Lcom/twitter/model/dms/i$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/dms/i;->d:I

    .line 35
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->e(Lcom/twitter/model/dms/i$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/i;->e:J

    .line 36
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->f(Lcom/twitter/model/dms/i$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/i;->f:J

    .line 37
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->g(Lcom/twitter/model/dms/i$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/i;->g:J

    .line 38
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->h(Lcom/twitter/model/dms/i$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/i;->h:J

    .line 39
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->i(Lcom/twitter/model/dms/i$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/i;->i:Z

    .line 40
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->j(Lcom/twitter/model/dms/i$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/i;->j:J

    .line 41
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->k(Lcom/twitter/model/dms/i$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/i;->k:Z

    .line 42
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->l(Lcom/twitter/model/dms/i$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/i;->l:Z

    .line 43
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->m(Lcom/twitter/model/dms/i$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/i;->m:Z

    .line 44
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->n(Lcom/twitter/model/dms/i$a;)Lcom/twitter/model/dms/x;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/i;->n:Lcom/twitter/model/dms/x;

    .line 45
    invoke-static {p1}, Lcom/twitter/model/dms/i$a;->o(Lcom/twitter/model/dms/i$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/i;->o:Ljava/lang/String;

    .line 46
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/i$a;Lcom/twitter/model/dms/i$1;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/i;-><init>(Lcom/twitter/model/dms/i$a;)V

    return-void
.end method
