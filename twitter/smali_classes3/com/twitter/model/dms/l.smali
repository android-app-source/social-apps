.class public Lcom/twitter/model/dms/l;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/l$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:J

.field public final d:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public final h:J

.field public final i:J

.field public final j:J

.field public final k:J

.field public final l:Z


# direct methods
.method private constructor <init>(Lcom/twitter/model/dms/l$a;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lcom/twitter/model/dms/l$a;->a(Lcom/twitter/model/dms/l$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/l;->a:Ljava/lang/String;

    .line 36
    invoke-static {p1}, Lcom/twitter/model/dms/l$a;->b(Lcom/twitter/model/dms/l$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/l;->e:Ljava/lang/String;

    .line 37
    invoke-static {p1}, Lcom/twitter/model/dms/l$a;->c(Lcom/twitter/model/dms/l$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/dms/l;->b:I

    .line 38
    invoke-static {p1}, Lcom/twitter/model/dms/l$a;->d(Lcom/twitter/model/dms/l$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/l;->c:J

    .line 39
    invoke-static {p1}, Lcom/twitter/model/dms/l$a;->e(Lcom/twitter/model/dms/l$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/l;->g:Z

    .line 40
    invoke-static {p1}, Lcom/twitter/model/dms/l$a;->f(Lcom/twitter/model/dms/l$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/l;->h:J

    .line 41
    invoke-static {p1}, Lcom/twitter/model/dms/l$a;->g(Lcom/twitter/model/dms/l$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/l;->i:J

    .line 42
    invoke-static {p1}, Lcom/twitter/model/dms/l$a;->h(Lcom/twitter/model/dms/l$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/l;->j:J

    .line 43
    invoke-static {p1}, Lcom/twitter/model/dms/l$a;->i(Lcom/twitter/model/dms/l$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/l;->k:J

    .line 44
    invoke-static {p1}, Lcom/twitter/model/dms/l$a;->j(Lcom/twitter/model/dms/l$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/l;->l:Z

    .line 45
    invoke-static {p1}, Lcom/twitter/model/dms/l$a;->k(Lcom/twitter/model/dms/l$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/l;->f:Ljava/lang/String;

    .line 46
    invoke-static {p1}, Lcom/twitter/model/dms/l$a;->l(Lcom/twitter/model/dms/l$a;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/l;->d:Ljava/util/Collection;

    .line 47
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/l$a;Lcom/twitter/model/dms/l$1;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/l;-><init>(Lcom/twitter/model/dms/l$a;)V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/model/dms/l;->a:Ljava/lang/String;

    return-object v0
.end method
