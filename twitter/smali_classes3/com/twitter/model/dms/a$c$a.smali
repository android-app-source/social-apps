.class public final Lcom/twitter/model/dms/a$c$a;
.super Lcom/twitter/model/dms/e$b$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/a$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/e$b$b",
        "<",
        "Lcom/twitter/model/dms/a$c$a;",
        "Lcom/twitter/model/dms/a$c;",
        ">;"
    }
.end annotation


# instance fields
.field private f:Lcck;

.field private g:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/twitter/model/dms/e$b$b;-><init>()V

    .line 180
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/dms/a$c;)V
    .locals 2

    .prologue
    .line 183
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/e$b$b;-><init>(Lcom/twitter/model/dms/e$b;)V

    .line 184
    invoke-static {p1}, Lcom/twitter/model/dms/a$c;->a(Lcom/twitter/model/dms/a$c;)Lcck;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/a$c$a;->f:Lcck;

    .line 185
    invoke-static {p1}, Lcom/twitter/model/dms/a$c;->b(Lcom/twitter/model/dms/a$c;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/a$c$a;->g:J

    .line 186
    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/a$c$a;)Lcck;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->f:Lcck;

    return-object v0
.end method

.method protected static a(Lcom/twitter/model/core/e;Lcbx;)Lcom/twitter/model/core/ad;
    .locals 5
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 289
    iget-object v2, p0, Lcom/twitter/model/core/e;->a:Ljava/lang/String;

    .line 291
    invoke-static {v2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/twitter/model/dms/a$c$a;->b(Lcbx;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 292
    invoke-virtual {p1}, Lcbx;->e()I

    move-result v0

    if-eq v0, v3, :cond_0

    invoke-virtual {p1}, Lcbx;->f()I

    move-result v0

    if-ne v0, v3, :cond_1

    :cond_0
    move-object v0, v1

    .line 300
    :goto_0
    return-object v0

    .line 295
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/core/e;->b:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 296
    invoke-static {p1, v0, v2}, Lcom/twitter/model/dms/a$c$a;->a(Lcbx;Lcom/twitter/model/core/ad;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 300
    goto :goto_0
.end method

.method protected static a(Lcom/twitter/model/core/e;Lcom/twitter/model/core/ad;)Lcom/twitter/model/core/e;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 275
    iget-object v0, p0, Lcom/twitter/model/core/e;->a:Ljava/lang/String;

    .line 276
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 277
    iget-object v2, p1, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int v2, v1, v2

    .line 278
    new-instance v3, Lcom/twitter/model/core/e;

    .line 279
    invoke-static {v0, v2, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/twitter/model/core/v$a;

    iget-object v2, p0, Lcom/twitter/model/core/e;->b:Lcom/twitter/model/core/v;

    invoke-direct {v0, v2}, Lcom/twitter/model/core/v$a;-><init>(Lcom/twitter/model/core/v;)V

    .line 281
    invoke-virtual {v0, p1}, Lcom/twitter/model/core/v$a;->b(Lcom/twitter/model/core/ad;)Lcom/twitter/model/core/v$a;

    move-result-object v0

    .line 282
    invoke-virtual {v0}, Lcom/twitter/model/core/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    invoke-direct {v3, v1, v0}, Lcom/twitter/model/core/e;-><init>(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    .line 278
    return-object v3
.end method

.method private static a(Lcbx;Lcom/twitter/model/core/ad;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 316
    iget v0, p1, Lcom/twitter/model/core/ad;->g:I

    invoke-virtual {p0}, Lcbx;->e()I

    move-result v3

    if-ne v0, v3, :cond_1

    iget v0, p1, Lcom/twitter/model/core/ad;->h:I

    .line 317
    invoke-virtual {p0}, Lcbx;->f()I

    move-result v3

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 318
    :goto_0
    invoke-static {p0}, Lcom/twitter/model/dms/a$c$a;->b(Lcbx;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 319
    invoke-virtual {p0}, Lcbx;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v1

    .line 320
    :goto_1
    if-nez v0, :cond_0

    if-eqz v3, :cond_3

    :cond_0
    iget-object v0, p1, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_2
    return v1

    :cond_1
    move v0, v2

    .line 317
    goto :goto_0

    :cond_2
    move v3, v2

    .line 319
    goto :goto_1

    :cond_3
    move v1, v2

    .line 320
    goto :goto_2
.end method

.method static synthetic b(Lcom/twitter/model/dms/a$c$a;)J
    .locals 2

    .prologue
    .line 175
    iget-wide v0, p0, Lcom/twitter/model/dms/a$c$a;->g:J

    return-wide v0
.end method

.method private static b(Lcbx;)Z
    .locals 2

    .prologue
    .line 324
    invoke-virtual {p0}, Lcbx;->a()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcbx;)Z
    .locals 2

    .prologue
    .line 328
    invoke-virtual {p0}, Lcbx;->a()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(J)Lcom/twitter/model/dms/a$c$a;
    .locals 1

    .prologue
    .line 196
    iput-wide p1, p0, Lcom/twitter/model/dms/a$c$a;->g:J

    .line 197
    return-object p0
.end method

.method public a(Lcck;)Lcom/twitter/model/dms/a$c$a;
    .locals 1

    .prologue
    .line 190
    iput-object p1, p0, Lcom/twitter/model/dms/a$c$a;->f:Lcck;

    .line 191
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/a$c$a;

    return-object v0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/twitter/model/dms/a$c$a;->e()Lcom/twitter/model/dms/a$c;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/model/dms/a$c;
    .locals 2

    .prologue
    .line 203
    new-instance v0, Lcom/twitter/model/dms/a$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/dms/a$c;-><init>(Lcom/twitter/model/dms/a$c$a;Lcom/twitter/model/dms/a$1;)V

    return-object v0
.end method

.method protected f()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 208
    invoke-super {p0}, Lcom/twitter/model/dms/e$b$b;->f()V

    .line 211
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    if-eqz v0, :cond_0

    .line 212
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    invoke-virtual {v1}, Lcbx;->a()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 214
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    invoke-virtual {v0}, Lcbx;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    iput-object v2, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    .line 268
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/dms/a$c$a;->b:Lcom/twitter/model/core/v;

    move-object v3, v2

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/twitter/model/util/f;->a(Ljava/lang/String;Lcom/twitter/model/core/v;Ljava/lang/Iterable;Ljava/util/List;ZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/a$c$a;->a:Ljava/lang/String;

    .line 269
    return-void

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    invoke-virtual {v0}, Lcbx;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    iget-object v1, p0, Lcom/twitter/model/dms/a$c$a;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/model/dms/a$c$a;->b:Lcom/twitter/model/core/v;

    invoke-static {v0, v1, v3}, Lcom/twitter/model/dms/a;->a(Lcbx;Ljava/lang/String;Lcom/twitter/model/core/v;)Lcom/twitter/model/core/e;

    move-result-object v0

    .line 221
    if-eqz v0, :cond_0

    .line 222
    iput-object v2, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    .line 223
    iget-object v1, v0, Lcom/twitter/model/core/e;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/twitter/model/dms/a$c$a;->a:Ljava/lang/String;

    .line 224
    iget-object v0, v0, Lcom/twitter/model/core/e;->b:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/dms/a$c$a;->b:Lcom/twitter/model/core/v;

    goto :goto_0

    .line 227
    :cond_2
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    invoke-static {v0}, Lcom/twitter/model/dms/a$c$a;->c(Lcbx;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 228
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->b:Lcom/twitter/model/core/v;

    invoke-virtual {v0}, Lcom/twitter/model/core/v;->a()Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 229
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->b:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/f;->a(I)Lcom/twitter/model/core/d;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 230
    iget-object v1, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcch;

    .line 231
    new-instance v3, Lcch$a;

    invoke-direct {v3}, Lcch$a;-><init>()V

    iget-wide v6, v1, Lcch;->c:J

    .line 232
    invoke-virtual {v3, v6, v7}, Lcch$a;->a(J)Lcch$a;

    move-result-object v1

    iget-object v3, v0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    .line 233
    invoke-virtual {v1, v3}, Lcch$a;->a(Ljava/lang/String;)Lcbx$a;

    move-result-object v1

    check-cast v1, Lcch$a;

    iget-object v3, v0, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    .line 234
    invoke-virtual {v1, v3}, Lcch$a;->c(Ljava/lang/String;)Lcbx$a;

    move-result-object v1

    check-cast v1, Lcch$a;

    iget-object v0, v0, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    .line 235
    invoke-virtual {v1, v0}, Lcch$a;->b(Ljava/lang/String;)Lcbx$a;

    move-result-object v0

    check-cast v0, Lcch$a;

    .line 236
    invoke-virtual {v0}, Lcch$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx;

    iput-object v0, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    .line 239
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/model/dms/a$c$a;->a:Ljava/lang/String;

    goto :goto_0

    .line 240
    :cond_3
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    invoke-virtual {v0}, Lcbx;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    new-instance v3, Lcom/twitter/model/core/e;

    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/model/dms/a$c$a;->b:Lcom/twitter/model/core/v;

    invoke-direct {v3, v0, v1}, Lcom/twitter/model/core/e;-><init>(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    .line 243
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    invoke-static {v3, v0}, Lcom/twitter/model/dms/a$c$a;->a(Lcom/twitter/model/core/e;Lcbx;)Lcom/twitter/model/core/ad;

    move-result-object v5

    .line 245
    if-eqz v5, :cond_0

    .line 249
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    instance-of v0, v0, Lccg;

    if-eqz v0, :cond_4

    .line 250
    iget-object v0, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    .line 251
    new-instance v1, Lccg$a;

    invoke-direct {v1}, Lccg$a;-><init>()V

    .line 252
    invoke-virtual {v0}, Lccg;->j()Lcax;

    move-result-object v6

    invoke-virtual {v1, v6}, Lccg$a;->a(Lcax;)Lcbz$a;

    move-result-object v1

    check-cast v1, Lccg$a;

    .line 253
    invoke-virtual {v0}, Lccg;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lccg$a;->a(Ljava/lang/String;)Lcbx$a;

    move-result-object v0

    check-cast v0, Lccg$a;

    iget-object v1, v5, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    .line 254
    invoke-virtual {v0, v1}, Lccg$a;->c(Ljava/lang/String;)Lcbx$a;

    move-result-object v0

    check-cast v0, Lccg$a;

    iget-object v1, v5, Lcom/twitter/model/core/ad;->G:Ljava/lang/String;

    .line 255
    invoke-virtual {v0, v1}, Lccg$a;->b(Ljava/lang/String;)Lcbx$a;

    move-result-object v0

    check-cast v0, Lccg$a;

    .line 256
    invoke-virtual {v0}, Lccg$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx;

    iput-object v0, p0, Lcom/twitter/model/dms/a$c$a;->e:Lcbx;

    .line 260
    :cond_4
    invoke-static {v3, v5}, Lcom/twitter/model/dms/a$c$a;->a(Lcom/twitter/model/core/e;Lcom/twitter/model/core/ad;)Lcom/twitter/model/core/e;

    move-result-object v0

    .line 261
    iget-object v1, v0, Lcom/twitter/model/core/e;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/twitter/model/dms/a$c$a;->a:Ljava/lang/String;

    .line 262
    iget-object v0, v0, Lcom/twitter/model/core/e;->b:Lcom/twitter/model/core/v;

    iput-object v0, p0, Lcom/twitter/model/dms/a$c$a;->b:Lcom/twitter/model/core/v;

    goto/16 :goto_0
.end method
