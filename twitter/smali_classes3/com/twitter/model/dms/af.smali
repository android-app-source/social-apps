.class public Lcom/twitter/model/dms/af;
.super Lcom/twitter/model/dms/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/af$b;,
        Lcom/twitter/model/dms/af$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/c",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/dms/Participant;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/af;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/model/dms/af$b;

    invoke-direct {v0}, Lcom/twitter/model/dms/af$b;-><init>()V

    sput-object v0, Lcom/twitter/model/dms/af;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/af$a;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/c;-><init>(Lcom/twitter/model/dms/c$b;)V

    .line 24
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/af$a;Lcom/twitter/model/dms/af$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/af;-><init>(Lcom/twitter/model/dms/af$a;)V

    return-void
.end method


# virtual methods
.method public a(J)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-virtual {p0}, Lcom/twitter/model/dms/af;->d()Ljava/util/List;

    move-result-object v0

    .line 42
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 43
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 44
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/Participant;

    iget-wide v2, v0, Lcom/twitter/model/dms/Participant;->b:J

    cmp-long v0, v2, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 42
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 44
    goto :goto_0
.end method

.method public c()Lcom/twitter/util/serialization/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 34
    sget-object v0, Lcom/twitter/model/dms/Participant;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/twitter/model/dms/af;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0xb

    return v0
.end method
