.class public final Lcom/twitter/model/dms/l$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/dms/l;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:J

.field private d:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:J

.field private i:J

.field private j:J

.field private k:J

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/model/dms/l$a;->b:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/l$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/model/dms/l$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/l$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/model/dms/l$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/dms/l$a;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/twitter/model/dms/l$a;->b:I

    return v0
.end method

.method static synthetic d(Lcom/twitter/model/dms/l$a;)J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/twitter/model/dms/l$a;->c:J

    return-wide v0
.end method

.method static synthetic e(Lcom/twitter/model/dms/l$a;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/twitter/model/dms/l$a;->g:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/model/dms/l$a;)J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/twitter/model/dms/l$a;->h:J

    return-wide v0
.end method

.method static synthetic g(Lcom/twitter/model/dms/l$a;)J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/twitter/model/dms/l$a;->i:J

    return-wide v0
.end method

.method static synthetic h(Lcom/twitter/model/dms/l$a;)J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/twitter/model/dms/l$a;->j:J

    return-wide v0
.end method

.method static synthetic i(Lcom/twitter/model/dms/l$a;)J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/twitter/model/dms/l$a;->k:J

    return-wide v0
.end method

.method static synthetic j(Lcom/twitter/model/dms/l$a;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/twitter/model/dms/l$a;->l:Z

    return v0
.end method

.method static synthetic k(Lcom/twitter/model/dms/l$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/model/dms/l$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/model/dms/l$a;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/model/dms/l$a;->d:Ljava/util/Collection;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 143
    iget-object v1, p0, Lcom/twitter/model/dms/l$a;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/twitter/model/dms/l$a;->b:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/model/dms/l$a;->b:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/model/dms/l$a;
    .locals 0

    .prologue
    .line 83
    iput p1, p0, Lcom/twitter/model/dms/l$a;->b:I

    .line 84
    return-object p0
.end method

.method public a(J)Lcom/twitter/model/dms/l$a;
    .locals 1

    .prologue
    .line 89
    iput-wide p1, p0, Lcom/twitter/model/dms/l$a;->c:J

    .line 90
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/dms/l$a;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/twitter/model/dms/l$a;->e:Ljava/lang/String;

    .line 72
    return-object p0
.end method

.method public a(Ljava/util/Collection;)Lcom/twitter/model/dms/l$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;)",
            "Lcom/twitter/model/dms/l$a;"
        }
    .end annotation

    .prologue
    .line 95
    iput-object p1, p0, Lcom/twitter/model/dms/l$a;->d:Ljava/util/Collection;

    .line 96
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/dms/l$a;
    .locals 0

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/twitter/model/dms/l$a;->g:Z

    .line 108
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/dms/l$a;
    .locals 1

    .prologue
    .line 113
    iput-wide p1, p0, Lcom/twitter/model/dms/l$a;->h:J

    .line 114
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/dms/l$a;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/twitter/model/dms/l$a;->f:Ljava/lang/String;

    .line 78
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/dms/l$a;
    .locals 0

    .prologue
    .line 137
    iput-boolean p1, p0, Lcom/twitter/model/dms/l$a;->l:Z

    .line 138
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/twitter/model/dms/l$a;->e()Lcom/twitter/model/dms/l;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/twitter/model/dms/l$a;
    .locals 1

    .prologue
    .line 119
    iput-wide p1, p0, Lcom/twitter/model/dms/l$a;->k:J

    .line 120
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/dms/l$a;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/twitter/model/dms/l$a;->a:Ljava/lang/String;

    .line 102
    return-object p0
.end method

.method protected c_()V
    .locals 4

    .prologue
    .line 149
    invoke-super {p0}, Lcom/twitter/util/object/i;->c_()V

    .line 150
    iget-wide v0, p0, Lcom/twitter/model/dms/l$a;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 152
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/model/dms/l$a;->j:J

    .line 154
    :cond_0
    return-void
.end method

.method public d(J)Lcom/twitter/model/dms/l$a;
    .locals 1

    .prologue
    .line 125
    iput-wide p1, p0, Lcom/twitter/model/dms/l$a;->i:J

    .line 126
    return-object p0
.end method

.method public e(J)Lcom/twitter/model/dms/l$a;
    .locals 1

    .prologue
    .line 131
    iput-wide p1, p0, Lcom/twitter/model/dms/l$a;->j:J

    .line 132
    return-object p0
.end method

.method protected e()Lcom/twitter/model/dms/l;
    .locals 2

    .prologue
    .line 159
    new-instance v0, Lcom/twitter/model/dms/l;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/dms/l;-><init>(Lcom/twitter/model/dms/l$a;Lcom/twitter/model/dms/l$1;)V

    return-object v0
.end method
