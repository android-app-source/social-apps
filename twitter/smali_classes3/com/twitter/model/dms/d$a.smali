.class public abstract Lcom/twitter/model/dms/d$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/twitter/model/dms/d;",
        "B:",
        "Lcom/twitter/model/dms/d$a",
        "<TE;TB;>;>",
        "Lcom/twitter/util/object/i",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:J


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 74
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 70
    iput-wide v0, p0, Lcom/twitter/model/dms/d$a;->a:J

    .line 72
    iput-wide v0, p0, Lcom/twitter/model/dms/d$a;->c:J

    .line 74
    return-void
.end method

.method constructor <init>(Lcom/twitter/model/dms/d;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 76
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 70
    iput-wide v0, p0, Lcom/twitter/model/dms/d$a;->a:J

    .line 72
    iput-wide v0, p0, Lcom/twitter/model/dms/d$a;->c:J

    .line 77
    iget-wide v0, p1, Lcom/twitter/model/dms/d;->e:J

    iput-wide v0, p0, Lcom/twitter/model/dms/d$a;->a:J

    .line 78
    iget-object v0, p1, Lcom/twitter/model/dms/d;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/dms/d$a;->b:Ljava/lang/String;

    .line 79
    iget-wide v0, p1, Lcom/twitter/model/dms/d;->g:J

    iput-wide v0, p0, Lcom/twitter/model/dms/d$a;->c:J

    .line 80
    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/d$a;)J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/twitter/model/dms/d$a;->a:J

    return-wide v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/d$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/model/dms/d$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/dms/d$a;)J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/twitter/model/dms/d$a;->c:J

    return-wide v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 90
    iput-object p1, p0, Lcom/twitter/model/dms/d$a;->b:Ljava/lang/String;

    .line 91
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d$a;

    return-object v0
.end method

.method public b(J)Lcom/twitter/model/dms/d$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TB;"
        }
    .end annotation

    .prologue
    .line 84
    iput-wide p1, p0, Lcom/twitter/model/dms/d$a;->a:J

    .line 85
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d$a;

    return-object v0
.end method

.method public c(J)Lcom/twitter/model/dms/d$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TB;"
        }
    .end annotation

    .prologue
    .line 96
    iput-wide p1, p0, Lcom/twitter/model/dms/d$a;->c:J

    .line 97
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d$a;

    return-object v0
.end method
