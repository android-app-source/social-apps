.class public Lcom/twitter/model/dms/z;
.super Lcom/twitter/model/dms/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/z$b;,
        Lcom/twitter/model/dms/z$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/c",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/z;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/model/dms/z$b;

    invoke-direct {v0}, Lcom/twitter/model/dms/z$b;-><init>()V

    sput-object v0, Lcom/twitter/model/dms/z;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/z$a;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/c;-><init>(Lcom/twitter/model/dms/c$b;)V

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/z$a;Lcom/twitter/model/dms/z$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/z;-><init>(Lcom/twitter/model/dms/z$a;)V

    return-void
.end method


# virtual methods
.method public c()Lcom/twitter/util/serialization/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 28
    sget-object v0, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/twitter/model/dms/z;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 39
    const/16 v0, 0x11

    return v0
.end method
