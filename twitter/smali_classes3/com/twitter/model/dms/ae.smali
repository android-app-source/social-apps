.class public Lcom/twitter/model/dms/ae;
.super Lcom/twitter/model/dms/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/ae$c;,
        Lcom/twitter/model/dms/ae$b;,
        Lcom/twitter/model/dms/ae$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/c",
        "<",
        "Lcom/twitter/model/dms/ae$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/ae;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/model/dms/ae$c;

    invoke-direct {v0}, Lcom/twitter/model/dms/ae$c;-><init>()V

    sput-object v0, Lcom/twitter/model/dms/ae;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/ae$a;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/c;-><init>(Lcom/twitter/model/dms/c$b;)V

    .line 32
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/ae$a;Lcom/twitter/model/dms/ae$1;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/ae;-><init>(Lcom/twitter/model/dms/ae$a;)V

    return-void
.end method


# virtual methods
.method public c()Lcom/twitter/util/serialization/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/ae$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    sget-object v0, Lcom/twitter/model/dms/ae$b;->a:Lcom/twitter/util/serialization/l;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/model/dms/ae;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/ae$b;

    iget-object v0, v0, Lcom/twitter/model/dms/ae$b;->b:Ljava/util/List;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/twitter/model/dms/ae;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/ae$b;

    iget-object v0, v0, Lcom/twitter/model/dms/ae$b;->d:Ljava/util/List;

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 58
    const/16 v0, 0xa

    return v0
.end method
