.class public abstract Lcom/twitter/model/dms/a;
.super Lcom/twitter/model/dms/e;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/a$b;,
        Lcom/twitter/model/dms/a$c;,
        Lcom/twitter/model/dms/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/e",
        "<",
        "Lcom/twitter/model/dms/a$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 34
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/util/serialization/j;

    const/4 v1, 0x0

    const-class v2, Lcom/twitter/model/dms/s;

    new-instance v3, Lcom/twitter/model/dms/s$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/s$b;-><init>()V

    .line 36
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/twitter/model/dms/u;

    new-instance v3, Lcom/twitter/model/dms/u$b;

    invoke-direct {v3}, Lcom/twitter/model/dms/u$b;-><init>()V

    .line 38
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    .line 35
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a([Lcom/twitter/util/serialization/j;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcom/twitter/model/dms/a;->a:Lcom/twitter/util/serialization/l;

    .line 34
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/model/dms/a$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/dms/a$a",
            "<+",
            "Lcom/twitter/model/dms/a;",
            "+",
            "Lcom/twitter/model/dms/a$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/e;-><init>(Lcom/twitter/model/dms/e$a;)V

    .line 45
    return-void
.end method

.method protected static a(Lcbx;Ljava/lang/String;Lcom/twitter/model/core/v;)Lcom/twitter/model/core/e;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 106
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcbx;->i()Lcom/twitter/model/core/ad;

    move-result-object v1

    move-object v2, v1

    .line 107
    :goto_0
    if-nez v2, :cond_1

    .line 124
    :goto_1
    return-object v0

    :cond_0
    move-object v2, v0

    .line 106
    goto :goto_0

    .line 111
    :cond_1
    invoke-static {p1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 112
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    const-string/jumbo v1, " "

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    .line 113
    iget-object v1, v2, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    .line 115
    new-instance v4, Lcom/twitter/model/core/ad$c;

    invoke-direct {v4, v2}, Lcom/twitter/model/core/ad$c;-><init>(Lcom/twitter/model/core/ad;)V

    .line 116
    invoke-virtual {v4, v0}, Lcom/twitter/model/core/ad$c;->a(I)Lcom/twitter/model/core/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad$c;

    .line 117
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/ad$c;->b(I)Lcom/twitter/model/core/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad$c;

    .line 118
    invoke-virtual {v0}, Lcom/twitter/model/core/ad$c;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 120
    if-eqz p2, :cond_2

    new-instance v1, Lcom/twitter/model/core/v$a;

    invoke-direct {v1, p2}, Lcom/twitter/model/core/v$a;-><init>(Lcom/twitter/model/core/v;)V

    .line 122
    :goto_2
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/v$a;->a(Lcom/twitter/model/core/ad;)Lcom/twitter/model/core/v$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    .line 124
    new-instance v1, Lcom/twitter/model/core/e;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, v2, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/model/core/e;-><init>(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    move-object v0, v1

    goto :goto_1

    .line 120
    :cond_2
    new-instance v1, Lcom/twitter/model/core/v$a;

    invoke-direct {v1}, Lcom/twitter/model/core/v$a;-><init>()V

    goto :goto_2
.end method


# virtual methods
.method public a(ZZZ)Lcom/twitter/model/dms/a;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0, p1}, Lcom/twitter/model/dms/a;->a(Z)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->e()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-eqz p3, :cond_2

    .line 69
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->F()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 70
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->g()Lcom/twitter/model/dms/a;

    move-result-object p0

    .line 72
    :cond_2
    return-object p0
.end method

.method public a(Z)Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 77
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->u()Lcbx;

    move-result-object v0

    check-cast v0, Lcci;

    iget-object v0, v0, Lcci;->d:Lcom/twitter/model/core/r;

    iget-boolean v0, v0, Lcom/twitter/model/core/r;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b(ZZZ)Lcom/twitter/model/dms/e;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/a;->a(ZZZ)Lcom/twitter/model/dms/a;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/twitter/util/serialization/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/a$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    sget-object v0, Lcom/twitter/model/dms/a$c;->a:Lcom/twitter/util/serialization/l;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/twitter/model/dms/a;->a(I)Z

    move-result v0

    return v0
.end method

.method public f()Lcax;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->u()Lcbx;

    move-result-object v0

    check-cast v0, Lcbz;

    invoke-virtual {v0}, Lcbz;->j()Lcax;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/twitter/model/dms/a;
    .locals 4

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->u()Lcbx;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->r()Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->t()Lcom/twitter/model/core/v;

    move-result-object v2

    .line 88
    invoke-static {v0, v1, v2}, Lcom/twitter/model/dms/a;->a(Lcbx;Ljava/lang/String;Lcom/twitter/model/core/v;)Lcom/twitter/model/core/e;

    move-result-object v1

    .line 90
    if-nez v1, :cond_0

    .line 94
    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->i()Lcom/twitter/model/dms/a$a;

    move-result-object v2

    new-instance v3, Lcom/twitter/model/dms/a$c$a;

    .line 95
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/a$c;

    invoke-direct {v3, v0}, Lcom/twitter/model/dms/a$c$a;-><init>(Lcom/twitter/model/dms/a$c;)V

    iget-object v0, v1, Lcom/twitter/model/core/e;->b:Lcom/twitter/model/core/v;

    .line 96
    invoke-virtual {v3, v0}, Lcom/twitter/model/dms/a$c$a;->b(Lcom/twitter/model/core/v;)Lcom/twitter/model/dms/e$b$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/a$c$a;

    iget-object v1, v1, Lcom/twitter/model/core/e;->a:Ljava/lang/String;

    .line 97
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/a$c$a;->c(Ljava/lang/String;)Lcom/twitter/model/dms/e$b$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/a$c$a;

    const/4 v1, 0x0

    .line 98
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/a$c$a;->a(Lcbx;)Lcom/twitter/model/dms/e$b$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/a$c$a;

    .line 99
    invoke-virtual {v0}, Lcom/twitter/model/dms/a$c$a;->q()Ljava/lang/Object;

    move-result-object v0

    .line 95
    invoke-virtual {v2, v0}, Lcom/twitter/model/dms/a$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/a$a;

    .line 100
    invoke-virtual {v0}, Lcom/twitter/model/dms/a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/a;

    move-object p0, v0

    .line 94
    goto :goto_0
.end method

.method public h()Lcck;
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/a$c;

    invoke-static {v0}, Lcom/twitter/model/dms/a$c;->a(Lcom/twitter/model/dms/a$c;)Lcck;

    move-result-object v0

    return-object v0
.end method

.method protected abstract i()Lcom/twitter/model/dms/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/model/dms/a$a",
            "<**>;"
        }
    .end annotation
.end method
