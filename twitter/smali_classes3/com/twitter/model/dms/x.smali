.class public Lcom/twitter/model/dms/x;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/x$b;,
        Lcom/twitter/model/dms/x$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/x;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/model/dms/x$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/dms/x$b;-><init>(Lcom/twitter/model/dms/x$1;)V

    sput-object v0, Lcom/twitter/model/dms/x;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/x$a;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lcom/twitter/model/dms/x$a;->a(Lcom/twitter/model/dms/x$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/x;->b:Ljava/lang/String;

    .line 28
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/x$a;Lcom/twitter/model/dms/x$1;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/x;-><init>(Lcom/twitter/model/dms/x$a;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/x;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/model/dms/x;->b:Ljava/lang/String;

    return-object v0
.end method

.method private b(Lcom/twitter/model/dms/x;)Z
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/model/dms/x;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/x;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/model/dms/x;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 37
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/x;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/x;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/x;->b(Lcom/twitter/model/dms/x;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/model/dms/x;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
