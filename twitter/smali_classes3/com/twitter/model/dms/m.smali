.class public Lcom/twitter/model/dms/m;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/m$b;,
        Lcom/twitter/model/dms/m$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/dms/m;",
            "Lcom/twitter/model/dms/m$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field private final f:J

.field private final g:Lcom/twitter/model/dms/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/twitter/model/dms/m$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/dms/m$b;-><init>(Lcom/twitter/model/dms/m$1;)V

    sput-object v0, Lcom/twitter/model/dms/m;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/dms/m$a;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/twitter/model/dms/m$a;->a(Lcom/twitter/model/dms/m$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/m;->f:J

    .line 32
    invoke-static {p1}, Lcom/twitter/model/dms/m$a;->b(Lcom/twitter/model/dms/m$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/m;->b:Ljava/lang/String;

    .line 33
    invoke-static {p1}, Lcom/twitter/model/dms/m$a;->c(Lcom/twitter/model/dms/m$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/m;->c:Ljava/lang/String;

    .line 34
    invoke-static {p1}, Lcom/twitter/model/dms/m$a;->d(Lcom/twitter/model/dms/m$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/m;->d:Ljava/lang/String;

    .line 35
    invoke-static {p1}, Lcom/twitter/model/dms/m$a;->e(Lcom/twitter/model/dms/m$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/dms/m;->e:Z

    .line 36
    invoke-static {p1}, Lcom/twitter/model/dms/m$a;->f(Lcom/twitter/model/dms/m$a;)Lcom/twitter/model/dms/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/m;->g:Lcom/twitter/model/dms/c;

    .line 37
    return-void
.end method

.method private a(Lcom/twitter/model/dms/m;)Z
    .locals 4

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/model/dms/m;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/m;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/m;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/m;->c:Ljava/lang/String;

    .line 60
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/m;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/m;->d:Ljava/lang/String;

    .line 61
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/dms/m;->e:Z

    iget-boolean v1, p1, Lcom/twitter/model/dms/m;->e:Z

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/dms/m;->f:J

    .line 63
    invoke-virtual {p1}, Lcom/twitter/model/dms/m;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/m;->g:Lcom/twitter/model/dms/c;

    .line 64
    invoke-virtual {p1}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 59
    :goto_0
    return v0

    .line 64
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/twitter/model/dms/m;->f:J

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/model/dms/m;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lcom/twitter/model/dms/c;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/model/dms/m;->g:Lcom/twitter/model/dms/c;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 55
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/m;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/m;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/m;->a(Lcom/twitter/model/dms/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    iget-object v0, p0, Lcom/twitter/model/dms/m;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/dms/m;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 70
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/m;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/model/dms/m;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 71
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/m;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/model/dms/m;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 72
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/twitter/model/dms/m;->e:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    .line 73
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/model/dms/m;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/dms/m;->g:Lcom/twitter/model/dms/c;

    invoke-virtual {v1}, Lcom/twitter/model/dms/c;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    return v0

    :cond_1
    move v0, v1

    .line 69
    goto :goto_0

    :cond_2
    move v0, v1

    .line 70
    goto :goto_1

    :cond_3
    move v0, v1

    .line 71
    goto :goto_2
.end method
