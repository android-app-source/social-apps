.class public Lcom/twitter/model/dms/h$b;
.super Lcom/twitter/model/dms/c$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/c$a",
        "<",
        "Lcom/twitter/model/dms/h$c;",
        "Lcom/twitter/model/dms/h;",
        "Lcom/twitter/model/dms/h$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 195
    sget-object v0, Lcom/twitter/model/dms/h$c;->a:Lcom/twitter/model/dms/h$c$b;

    invoke-direct {p0, v0}, Lcom/twitter/model/dms/c$a;-><init>(Lcom/twitter/util/serialization/l;)V

    .line 196
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/dms/h$a;
    .locals 1

    .prologue
    .line 201
    new-instance v0, Lcom/twitter/model/dms/h$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/h$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/c$b;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 192
    check-cast p2, Lcom/twitter/model/dms/h$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/h$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/h$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/h$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 209
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/model/dms/c$a;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/c$b;I)V

    .line 212
    :try_start_0
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/h$a;->b(Ljava/lang/String;)Lcom/twitter/model/dms/h$a;
    :try_end_0
    .catch Lcom/twitter/util/serialization/OptionalFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :goto_0
    return-void

    .line 213
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 192
    check-cast p2, Lcom/twitter/model/dms/h$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/h$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/h$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    check-cast p2, Lcom/twitter/model/dms/h;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/h$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/h;)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 220
    invoke-super {p0, p1, p2}, Lcom/twitter/model/dms/c$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/c;)V

    .line 221
    iget-object v0, p2, Lcom/twitter/model/dms/h;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 222
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    check-cast p2, Lcom/twitter/model/dms/h;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/h$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/h;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/twitter/model/dms/h$b;->a()Lcom/twitter/model/dms/h$a;

    move-result-object v0

    return-object v0
.end method
