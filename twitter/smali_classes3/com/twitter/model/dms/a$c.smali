.class public Lcom/twitter/model/dms/a$c;
.super Lcom/twitter/model/dms/e$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/a$c$b;,
        Lcom/twitter/model/dms/a$c$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/a$c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final l:Lcck;

.field private final m:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    new-instance v0, Lcom/twitter/model/dms/a$c$b;

    invoke-direct {v0}, Lcom/twitter/model/dms/a$c$b;-><init>()V

    sput-object v0, Lcom/twitter/model/dms/a$c;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/a$c$a;)V
    .locals 2

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/e$b;-><init>(Lcom/twitter/model/dms/e$b$b;)V

    .line 154
    invoke-static {p1}, Lcom/twitter/model/dms/a$c$a;->a(Lcom/twitter/model/dms/a$c$a;)Lcck;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/a$c;->l:Lcck;

    .line 155
    invoke-static {p1}, Lcom/twitter/model/dms/a$c$a;->b(Lcom/twitter/model/dms/a$c$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/a$c;->m:J

    .line 156
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/a$c$a;Lcom/twitter/model/dms/a$1;)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/a$c;-><init>(Lcom/twitter/model/dms/a$c$a;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/a$c;)Lcck;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/twitter/model/dms/a$c;->l:Lcck;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/a$c;)J
    .locals 2

    .prologue
    .line 146
    iget-wide v0, p0, Lcom/twitter/model/dms/a$c;->m:J

    return-wide v0
.end method

.method private c(Lcom/twitter/model/dms/a$c;)Z
    .locals 2

    .prologue
    .line 164
    invoke-super {p0, p1}, Lcom/twitter/model/dms/e$b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/a$c;->l:Lcck;

    iget-object v1, p1, Lcom/twitter/model/dms/a$c;->l:Lcck;

    .line 165
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 164
    :goto_0
    return v0

    .line 165
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 160
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/a$c;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/a$c;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/a$c;->c(Lcom/twitter/model/dms/a$c;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 170
    invoke-super {p0}, Lcom/twitter/model/dms/e$b;->hashCode()I

    move-result v0

    .line 171
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/dms/a$c;->l:Lcck;

    iget-wide v2, p0, Lcom/twitter/model/dms/a$c;->m:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    return v0
.end method
