.class Lcom/twitter/model/dms/m$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/dms/m;",
        "Lcom/twitter/model/dms/m$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/m$1;)V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/twitter/model/dms/m$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/dms/m$a;
    .locals 1

    .prologue
    .line 151
    new-instance v0, Lcom/twitter/model/dms/m$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/m$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/m$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 157
    .line 158
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {p2, v2, v3}, Lcom/twitter/model/dms/m$a;->a(J)Lcom/twitter/model/dms/m$a;

    move-result-object v1

    .line 159
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/model/dms/m$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/m$a;

    move-result-object v1

    .line 160
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/model/dms/m$a;->b(Ljava/lang/String;)Lcom/twitter/model/dms/m$a;

    move-result-object v1

    .line 161
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/model/dms/m$a;->c(Ljava/lang/String;)Lcom/twitter/model/dms/m$a;

    move-result-object v1

    .line 162
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/m$a;->a(Z)Lcom/twitter/model/dms/m$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/dms/c;->b:Lcom/twitter/util/serialization/l;

    .line 163
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c;

    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/m$a;->a(Lcom/twitter/model/dms/c;)Lcom/twitter/model/dms/m$a;

    .line 164
    return-void

    .line 162
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 135
    check-cast p2, Lcom/twitter/model/dms/m$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/m$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/m$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/m;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    .line 140
    invoke-virtual {p2}, Lcom/twitter/model/dms/m;->a()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 141
    invoke-virtual {p2}, Lcom/twitter/model/dms/m;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/dms/m;->c:Ljava/lang/String;

    .line 142
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/dms/m;->d:Ljava/lang/String;

    .line 143
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v1

    iget-boolean v0, p2, Lcom/twitter/model/dms/m;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 144
    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 145
    invoke-virtual {p2}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/dms/c;->b:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 146
    return-void

    .line 143
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    check-cast p2, Lcom/twitter/model/dms/m;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/m$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/m;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/twitter/model/dms/m$b;->a()Lcom/twitter/model/dms/m$a;

    move-result-object v0

    return-object v0
.end method
