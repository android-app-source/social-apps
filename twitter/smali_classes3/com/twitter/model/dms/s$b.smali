.class public Lcom/twitter/model/dms/s$b;
.super Lcom/twitter/model/dms/a$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/a$b",
        "<",
        "Lcom/twitter/model/dms/s;",
        "Lcom/twitter/model/dms/s$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/twitter/model/dms/a$c;->a:Lcom/twitter/util/serialization/l;

    invoke-direct {p0, v0}, Lcom/twitter/model/dms/a$b;-><init>(Lcom/twitter/util/serialization/l;)V

    .line 88
    return-void
.end method


# virtual methods
.method protected synthetic a()Lcom/twitter/model/dms/a$a;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/twitter/model/dms/s$b;->c()Lcom/twitter/model/dms/s$a;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/c$b;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 85
    check-cast p2, Lcom/twitter/model/dms/s$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/s$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/s$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/s$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/model/dms/a$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/c$b;I)V

    .line 101
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/s$a;->b(Z)Lcom/twitter/model/dms/s$a;

    .line 102
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 85
    check-cast p2, Lcom/twitter/model/dms/s$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/s$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/s$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    check-cast p2, Lcom/twitter/model/dms/s;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/s$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/s;)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/s;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-super {p0, p1, p2}, Lcom/twitter/model/dms/a$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/c;)V

    .line 109
    invoke-virtual {p2}, Lcom/twitter/model/dms/s;->o()I

    move-result v0

    const/16 v1, 0x13

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 110
    return-void

    .line 109
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    check-cast p2, Lcom/twitter/model/dms/s;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/s$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/s;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/twitter/model/dms/s$b;->c()Lcom/twitter/model/dms/s$a;

    move-result-object v0

    return-object v0
.end method

.method protected c()Lcom/twitter/model/dms/s$a;
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/twitter/model/dms/s$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/s$a;-><init>()V

    return-object v0
.end method
