.class public final Lcom/twitter/model/dms/i$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/dms/i;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:Z

.field private j:J

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Lcom/twitter/model/dms/x;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/i$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/model/dms/i$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/i$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/model/dms/i$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/dms/i$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/model/dms/i$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/model/dms/i$a;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/twitter/model/dms/i$a;->d:I

    return v0
.end method

.method static synthetic e(Lcom/twitter/model/dms/i$a;)J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/twitter/model/dms/i$a;->e:J

    return-wide v0
.end method

.method static synthetic f(Lcom/twitter/model/dms/i$a;)J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/twitter/model/dms/i$a;->f:J

    return-wide v0
.end method

.method static synthetic g(Lcom/twitter/model/dms/i$a;)J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/twitter/model/dms/i$a;->g:J

    return-wide v0
.end method

.method static synthetic h(Lcom/twitter/model/dms/i$a;)J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/twitter/model/dms/i$a;->h:J

    return-wide v0
.end method

.method static synthetic i(Lcom/twitter/model/dms/i$a;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/twitter/model/dms/i$a;->i:Z

    return v0
.end method

.method static synthetic j(Lcom/twitter/model/dms/i$a;)J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/twitter/model/dms/i$a;->j:J

    return-wide v0
.end method

.method static synthetic k(Lcom/twitter/model/dms/i$a;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/twitter/model/dms/i$a;->k:Z

    return v0
.end method

.method static synthetic l(Lcom/twitter/model/dms/i$a;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/twitter/model/dms/i$a;->l:Z

    return v0
.end method

.method static synthetic m(Lcom/twitter/model/dms/i$a;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/twitter/model/dms/i$a;->m:Z

    return v0
.end method

.method static synthetic n(Lcom/twitter/model/dms/i$a;)Lcom/twitter/model/dms/x;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/model/dms/i$a;->n:Lcom/twitter/model/dms/x;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/model/dms/i$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/model/dms/i$a;->o:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/twitter/model/dms/i$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/model/dms/i$a;
    .locals 0

    .prologue
    .line 145
    iput p1, p0, Lcom/twitter/model/dms/i$a;->d:I

    .line 146
    return-object p0
.end method

.method public a(J)Lcom/twitter/model/dms/i$a;
    .locals 1

    .prologue
    .line 85
    iput-wide p1, p0, Lcom/twitter/model/dms/i$a;->e:J

    .line 86
    return-object p0
.end method

.method public a(Lcom/twitter/model/dms/x;)Lcom/twitter/model/dms/i$a;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/twitter/model/dms/i$a;->n:Lcom/twitter/model/dms/x;

    .line 80
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/dms/i$a;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/twitter/model/dms/i$a;->a:Ljava/lang/String;

    .line 74
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/dms/i$a;
    .locals 0

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/twitter/model/dms/i$a;->l:Z

    .line 92
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/dms/i$a;
    .locals 1

    .prologue
    .line 115
    iput-wide p1, p0, Lcom/twitter/model/dms/i$a;->f:J

    .line 116
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/dms/i$a;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/twitter/model/dms/i$a;->b:Ljava/lang/String;

    .line 140
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/dms/i$a;
    .locals 0

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/twitter/model/dms/i$a;->k:Z

    .line 98
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/twitter/model/dms/i$a;->e()Lcom/twitter/model/dms/i;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/twitter/model/dms/i$a;
    .locals 1

    .prologue
    .line 121
    iput-wide p1, p0, Lcom/twitter/model/dms/i$a;->g:J

    .line 122
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/dms/i$a;
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/twitter/model/dms/i$a;->o:Ljava/lang/String;

    .line 152
    return-object p0
.end method

.method public c(Z)Lcom/twitter/model/dms/i$a;
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/twitter/model/dms/i$a;->i:Z

    .line 104
    return-object p0
.end method

.method public d(J)Lcom/twitter/model/dms/i$a;
    .locals 1

    .prologue
    .line 127
    iput-wide p1, p0, Lcom/twitter/model/dms/i$a;->j:J

    .line 128
    return-object p0
.end method

.method public d(Z)Lcom/twitter/model/dms/i$a;
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/twitter/model/dms/i$a;->m:Z

    .line 110
    return-object p0
.end method

.method public e(J)Lcom/twitter/model/dms/i$a;
    .locals 1

    .prologue
    .line 133
    iput-wide p1, p0, Lcom/twitter/model/dms/i$a;->h:J

    .line 134
    return-object p0
.end method

.method protected e()Lcom/twitter/model/dms/i;
    .locals 2

    .prologue
    .line 163
    new-instance v0, Lcom/twitter/model/dms/i;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/dms/i;-><init>(Lcom/twitter/model/dms/i$a;Lcom/twitter/model/dms/i$1;)V

    return-object v0
.end method
