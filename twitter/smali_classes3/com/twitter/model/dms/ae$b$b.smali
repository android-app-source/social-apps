.class Lcom/twitter/model/dms/ae$b$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/ae$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/dms/ae$b;",
        "Lcom/twitter/model/dms/ae$b$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/ae$1;)V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/twitter/model/dms/ae$b$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/dms/ae$b$a;
    .locals 1

    .prologue
    .line 143
    new-instance v0, Lcom/twitter/model/dms/ae$b$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/ae$b$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/ae$b$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 149
    .line 150
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/ae$b$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/ae$b$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 151
    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/ae$b$a;->b(Ljava/util/List;)Lcom/twitter/model/dms/ae$b$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/dms/Participant;->a:Lcom/twitter/util/serialization/l;

    .line 152
    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/ae$b$a;->a(Ljava/util/List;)Lcom/twitter/model/dms/ae$b$a;

    .line 153
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 131
    check-cast p2, Lcom/twitter/model/dms/ae$b$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/ae$b$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/ae$b$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/ae$b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p2, Lcom/twitter/model/dms/ae$b;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 136
    iget-object v0, p2, Lcom/twitter/model/dms/ae$b;->d:Ljava/util/List;

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 137
    iget-object v0, p2, Lcom/twitter/model/dms/ae$b;->b:Ljava/util/List;

    sget-object v1, Lcom/twitter/model/dms/Participant;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 138
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    check-cast p2, Lcom/twitter/model/dms/ae$b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/ae$b$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/ae$b;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/twitter/model/dms/ae$b$b;->a()Lcom/twitter/model/dms/ae$b$a;

    move-result-object v0

    return-object v0
.end method
