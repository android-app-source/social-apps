.class public Lcom/twitter/model/dms/k;
.super Lcom/twitter/model/dms/p;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/k$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field private final c:J

.field private final d:J

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/twitter/model/dms/k$a;)V
    .locals 2

    .prologue
    .line 24
    invoke-static {p1}, Lcom/twitter/model/dms/k$a;->a(Lcom/twitter/model/dms/k$a;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/model/dms/p;-><init>(Lcom/twitter/model/dms/p$a;I)V

    .line 25
    invoke-static {p1}, Lcom/twitter/model/dms/k$a;->b(Lcom/twitter/model/dms/k$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/k;->c:J

    .line 26
    invoke-static {p1}, Lcom/twitter/model/dms/k$a;->c(Lcom/twitter/model/dms/k$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/dms/k;->d:J

    .line 27
    invoke-static {p1}, Lcom/twitter/model/dms/k$a;->d(Lcom/twitter/model/dms/k$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/dms/k;->b:I

    .line 28
    invoke-static {p1}, Lcom/twitter/model/dms/k$a;->e(Lcom/twitter/model/dms/k$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/k;->a:Ljava/lang/String;

    .line 29
    invoke-static {p1}, Lcom/twitter/model/dms/k$a;->f(Lcom/twitter/model/dms/k$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/k;->e:Ljava/util/List;

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/k$a;Lcom/twitter/model/dms/k$1;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/k;-><init>(Lcom/twitter/model/dms/k$a;)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/twitter/model/dms/k;->c:J

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/twitter/model/dms/k;->d:J

    return-wide v0
.end method
