.class public final Lcom/twitter/model/dms/q$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/dms/q;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:J

.field private g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Lcom/twitter/model/dms/m;

.field private l:Lcom/twitter/model/dms/x;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/dms/q;)V
    .locals 4

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 155
    iget-object v0, p1, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    .line 156
    invoke-virtual {p0, v0}, Lcom/twitter/model/dms/q$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/dms/q;->c:Ljava/lang/String;

    .line 157
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->b(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/dms/q;->e:Ljava/lang/String;

    .line 158
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->c(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    iget-boolean v1, p1, Lcom/twitter/model/dms/q;->f:Z

    .line 159
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->b(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/model/dms/q;->g:J

    .line 160
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/q$a;->a(J)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    iget-boolean v1, p1, Lcom/twitter/model/dms/q;->h:Z

    .line 161
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->b(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    .line 162
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->a(Ljava/util/List;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    iget-boolean v1, p1, Lcom/twitter/model/dms/q;->j:Z

    .line 163
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->c(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    iget-boolean v1, p1, Lcom/twitter/model/dms/q;->k:Z

    .line 164
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->d(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/dms/q;->l:Lcom/twitter/model/dms/m;

    .line 165
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->a(Lcom/twitter/model/dms/m;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/dms/q;->m:Lcom/twitter/model/dms/x;

    .line 166
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->a(Lcom/twitter/model/dms/x;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/dms/q;->d:Ljava/lang/String;

    .line 167
    invoke-direct {v0, v1}, Lcom/twitter/model/dms/q$a;->d(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    .line 168
    return-void
.end method

.method static synthetic a(Lcom/twitter/model/dms/q$a;Ljava/lang/String;)Lcom/twitter/model/dms/q$a;
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/q$a;->d(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/model/dms/q$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/model/dms/q$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/model/dms/q$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/model/dms/q$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/model/dms/q$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/model/dms/q$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method private d(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/twitter/model/dms/q$a;->c:Ljava/lang/String;

    .line 243
    return-object p0
.end method

.method static synthetic d(Lcom/twitter/model/dms/q$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/model/dms/q$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/model/dms/q$a;)Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/twitter/model/dms/q$a;->e:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/model/dms/q$a;)J
    .locals 2

    .prologue
    .line 138
    iget-wide v0, p0, Lcom/twitter/model/dms/q$a;->f:J

    return-wide v0
.end method

.method private f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 254
    iget-boolean v0, p0, Lcom/twitter/model/dms/q$a;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/q$a;->h:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/q$a;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 255
    :cond_0
    const/4 v0, 0x0

    .line 257
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/model/dms/q$a;->h:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic g(Lcom/twitter/model/dms/q$a;)Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/twitter/model/dms/q$a;->g:Z

    return v0
.end method

.method static synthetic h(Lcom/twitter/model/dms/q$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/model/dms/q$a;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/model/dms/q$a;)Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/twitter/model/dms/q$a;->i:Z

    return v0
.end method

.method static synthetic j(Lcom/twitter/model/dms/q$a;)Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/twitter/model/dms/q$a;->j:Z

    return v0
.end method

.method static synthetic k(Lcom/twitter/model/dms/q$a;)Lcom/twitter/model/dms/m;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/model/dms/q$a;->k:Lcom/twitter/model/dms/m;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/model/dms/q$a;)Lcom/twitter/model/dms/x;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/model/dms/q$a;->l:Lcom/twitter/model/dms/x;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 263
    invoke-super {p0}, Lcom/twitter/util/object/i;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/q$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/q$a;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcom/twitter/model/dms/q$a;
    .locals 1

    .prologue
    .line 196
    iput-wide p1, p0, Lcom/twitter/model/dms/q$a;->f:J

    .line 197
    return-object p0
.end method

.method public a(Lcom/twitter/model/dms/m;)Lcom/twitter/model/dms/q$a;
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/twitter/model/dms/q$a;->k:Lcom/twitter/model/dms/m;

    .line 227
    return-object p0
.end method

.method public a(Lcom/twitter/model/dms/x;)Lcom/twitter/model/dms/q$a;
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/twitter/model/dms/q$a;->l:Lcom/twitter/model/dms/x;

    .line 233
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/twitter/model/dms/q$a;->a:Ljava/lang/String;

    .line 173
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/dms/q$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcom/twitter/model/dms/q$a;"
        }
    .end annotation

    .prologue
    .line 208
    iput-object p1, p0, Lcom/twitter/model/dms/q$a;->h:Ljava/util/List;

    .line 209
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/dms/q$a;
    .locals 0

    .prologue
    .line 190
    iput-boolean p1, p0, Lcom/twitter/model/dms/q$a;->e:Z

    .line 191
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/twitter/model/dms/q$a;->b:Ljava/lang/String;

    .line 179
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/dms/q$a;
    .locals 0

    .prologue
    .line 202
    iput-boolean p1, p0, Lcom/twitter/model/dms/q$a;->g:Z

    .line 203
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/twitter/model/dms/q$a;->e()Lcom/twitter/model/dms/q;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/twitter/model/dms/q$a;->d:Ljava/lang/String;

    .line 185
    return-object p0
.end method

.method public c(Z)Lcom/twitter/model/dms/q$a;
    .locals 0

    .prologue
    .line 214
    iput-boolean p1, p0, Lcom/twitter/model/dms/q$a;->i:Z

    .line 215
    return-object p0
.end method

.method protected c_()V
    .locals 1

    .prologue
    .line 248
    invoke-super {p0}, Lcom/twitter/util/object/i;->c_()V

    .line 249
    invoke-direct {p0}, Lcom/twitter/model/dms/q$a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/q$a;->c:Ljava/lang/String;

    .line 250
    return-void
.end method

.method public d(Z)Lcom/twitter/model/dms/q$a;
    .locals 0

    .prologue
    .line 220
    iput-boolean p1, p0, Lcom/twitter/model/dms/q$a;->j:Z

    .line 221
    return-object p0
.end method

.method protected e()Lcom/twitter/model/dms/q;
    .locals 2

    .prologue
    .line 269
    new-instance v0, Lcom/twitter/model/dms/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/model/dms/q;-><init>(Lcom/twitter/model/dms/q$a;Lcom/twitter/model/dms/q$1;)V

    return-object v0
.end method
