.class public Lcom/twitter/model/dms/ae$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/ae$b$b;,
        Lcom/twitter/model/dms/ae$b$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/ae$b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lcom/twitter/model/dms/ae$b$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/dms/ae$b$b;-><init>(Lcom/twitter/model/dms/ae$1;)V

    sput-object v0, Lcom/twitter/model/dms/ae$b;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/ae$b$a;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {p1}, Lcom/twitter/model/dms/ae$b$a;->a(Lcom/twitter/model/dms/ae$b$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/ae$b;->b:Ljava/util/List;

    .line 78
    invoke-static {p1}, Lcom/twitter/model/dms/ae$b$a;->b(Lcom/twitter/model/dms/ae$b$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/ae$b;->c:Ljava/lang/String;

    .line 79
    invoke-static {p1}, Lcom/twitter/model/dms/ae$b$a;->c(Lcom/twitter/model/dms/ae$b$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/ae$b;->d:Ljava/util/List;

    .line 80
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/ae$b$a;Lcom/twitter/model/dms/ae$1;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/ae$b;-><init>(Lcom/twitter/model/dms/ae$b$a;)V

    return-void
.end method

.method private a(Lcom/twitter/model/dms/ae$b;)Z
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/model/dms/ae$b;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/dms/ae$b;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/ae$b;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/ae$b;->c:Ljava/lang/String;

    .line 89
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/ae$b;->d:Ljava/util/List;

    iget-object v1, p1, Lcom/twitter/model/dms/ae$b;->d:Ljava/util/List;

    .line 90
    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    .line 90
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 84
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/ae$b;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/ae$b;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/ae$b;->a(Lcom/twitter/model/dms/ae$b;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/model/dms/ae$b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    .line 96
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/model/dms/ae$b;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/ae$b;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    .line 97
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/dms/ae$b;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    return v0

    .line 96
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
