.class Lcom/twitter/model/dms/r$c$b;
.super Lcom/twitter/model/dms/e$b$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/r$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/model/dms/e$b$a",
        "<",
        "Lcom/twitter/model/dms/r$c;",
        "Lcom/twitter/model/dms/r$c$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/twitter/model/dms/e$b$a;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/r$1;)V
    .locals 0

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/twitter/model/dms/r$c$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/dms/r$c$a;
    .locals 1

    .prologue
    .line 225
    new-instance v0, Lcom/twitter/model/dms/r$c$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/r$c$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/e$b$b;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 221
    check-cast p2, Lcom/twitter/model/dms/r$c$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/r$c$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/r$c$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/r$c$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 232
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/model/dms/e$b$a;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/e$b$b;I)V

    .line 233
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    .line 234
    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/r$c$a;->b(I)Lcom/twitter/model/dms/r$c$a;

    .line 235
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/r$c$a;->c(I)Lcom/twitter/model/dms/r$c$a;

    .line 236
    sget-object v0, Lcom/twitter/model/drafts/DraftAttachment;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/r$c$a;->a(Lcom/twitter/model/drafts/DraftAttachment;)Lcom/twitter/model/dms/r$c$a;

    .line 237
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/r$c$a;->d(Ljava/lang/String;)Lcom/twitter/model/dms/r$c$a;

    .line 238
    sget-object v0, Lccl;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccl;

    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/r$c$a;->a(Lccl;)Lcom/twitter/model/dms/r$c$a;

    .line 239
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 221
    check-cast p2, Lcom/twitter/model/dms/r$c$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/r$c$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/r$c$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/e$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    check-cast p2, Lcom/twitter/model/dms/r$c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/r$c$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/r$c;)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/r$c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 245
    invoke-super {p0, p1, p2}, Lcom/twitter/model/dms/e$b$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/e$b;)V

    .line 246
    iget v0, p2, Lcom/twitter/model/dms/r$c;->l:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 247
    iget v0, p2, Lcom/twitter/model/dms/r$c;->m:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 248
    iget-object v0, p2, Lcom/twitter/model/dms/r$c;->n:Lcom/twitter/model/drafts/DraftAttachment;

    sget-object v1, Lcom/twitter/model/drafts/DraftAttachment;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 249
    iget-object v0, p2, Lcom/twitter/model/dms/r$c;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 250
    iget-object v0, p2, Lcom/twitter/model/dms/r$c;->p:Lccl;

    sget-object v1, Lccl;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 251
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    check-cast p2, Lcom/twitter/model/dms/r$c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/r$c$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/r$c;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/twitter/model/dms/r$c$b;->a()Lcom/twitter/model/dms/r$c$a;

    move-result-object v0

    return-object v0
.end method
