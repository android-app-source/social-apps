.class public Lcom/twitter/model/dms/r$c;
.super Lcom/twitter/model/dms/e$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/dms/r$c$b;,
        Lcom/twitter/model/dms/r$c$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/dms/r$c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final l:I

.field public final m:I

.field public final n:Lcom/twitter/model/drafts/DraftAttachment;

.field public final o:Ljava/lang/String;

.field public final p:Lccl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 123
    new-instance v0, Lcom/twitter/model/dms/r$c$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/dms/r$c$b;-><init>(Lcom/twitter/model/dms/r$1;)V

    sput-object v0, Lcom/twitter/model/dms/r$c;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/r$c$a;)V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/e$b;-><init>(Lcom/twitter/model/dms/e$b$b;)V

    .line 133
    invoke-static {p1}, Lcom/twitter/model/dms/r$c$a;->a(Lcom/twitter/model/dms/r$c$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/dms/r$c;->l:I

    .line 134
    invoke-static {p1}, Lcom/twitter/model/dms/r$c$a;->b(Lcom/twitter/model/dms/r$c$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/dms/r$c;->m:I

    .line 135
    invoke-static {p1}, Lcom/twitter/model/dms/r$c$a;->c(Lcom/twitter/model/dms/r$c$a;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/r$c;->n:Lcom/twitter/model/drafts/DraftAttachment;

    .line 136
    invoke-static {p1}, Lcom/twitter/model/dms/r$c$a;->d(Lcom/twitter/model/dms/r$c$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/r$c;->o:Ljava/lang/String;

    .line 137
    invoke-static {p1}, Lcom/twitter/model/dms/r$c$a;->e(Lcom/twitter/model/dms/r$c$a;)Lccl;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/dms/r$c;->p:Lccl;

    .line 138
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/r$c$a;Lcom/twitter/model/dms/r$1;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/twitter/model/dms/r$c;-><init>(Lcom/twitter/model/dms/r$c$a;)V

    return-void
.end method

.method private a(Lcom/twitter/model/dms/r$c;)Z
    .locals 2

    .prologue
    .line 146
    invoke-super {p0, p1}, Lcom/twitter/model/dms/e$b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/model/dms/r$c;->l:I

    iget v1, p1, Lcom/twitter/model/dms/r$c;->l:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/model/dms/r$c;->m:I

    iget v1, p1, Lcom/twitter/model/dms/r$c;->m:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/r$c;->n:Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v1, p1, Lcom/twitter/model/dms/r$c;->n:Lcom/twitter/model/drafts/DraftAttachment;

    .line 149
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/r$c;->o:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/dms/r$c;->o:Ljava/lang/String;

    .line 150
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/dms/r$c;->p:Lccl;

    iget-object v1, p1, Lcom/twitter/model/dms/r$c;->p:Lccl;

    .line 151
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 146
    :goto_0
    return v0

    .line 151
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 142
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/model/dms/r$c;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/model/dms/r$c;

    invoke-direct {p0, p1}, Lcom/twitter/model/dms/r$c;->a(Lcom/twitter/model/dms/r$c;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 156
    invoke-super {p0}, Lcom/twitter/model/dms/e$b;->hashCode()I

    move-result v0

    .line 157
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/dms/r$c;->l:I

    add-int/2addr v0, v1

    .line 158
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/model/dms/r$c;->m:I

    add-int/2addr v0, v1

    .line 159
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/dms/r$c;->n:Lcom/twitter/model/drafts/DraftAttachment;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/dms/r$c;->o:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/dms/r$c;->p:Lccl;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    return v0
.end method
