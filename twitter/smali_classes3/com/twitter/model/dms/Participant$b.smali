.class Lcom/twitter/model/dms/Participant$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/dms/Participant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/model/dms/Participant;",
        "Lcom/twitter/model/dms/Participant$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/Participant$1;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/twitter/model/dms/Participant$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/model/dms/Participant$a;
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lcom/twitter/model/dms/Participant$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/Participant$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/Participant$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 142
    .line 143
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/dms/Participant$a;->a(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    .line 144
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/Participant$a;->b(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    .line 145
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/Participant$a;->c(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    .line 146
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/Participant$a;->d(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    .line 147
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/Participant$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/Participant$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    .line 148
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/Participant$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/dms/Participant$a;

    .line 150
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    .line 151
    invoke-virtual {p2, v0}, Lcom/twitter/model/dms/Participant$a;->a(I)Lcom/twitter/model/dms/Participant$a;

    .line 152
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 127
    check-cast p2, Lcom/twitter/model/dms/Participant$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/model/dms/Participant$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/model/dms/Participant$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/Participant;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    iget-wide v0, p2, Lcom/twitter/model/dms/Participant;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/dms/Participant;->c:J

    .line 132
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/dms/Participant;->d:J

    .line 133
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/model/dms/Participant;->e:J

    .line 134
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/dms/Participant;->f:Ljava/lang/String;

    .line 135
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/model/dms/Participant;->g:Lcom/twitter/model/core/TwitterUser;

    sget-object v2, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    .line 136
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 137
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    check-cast p2, Lcom/twitter/model/dms/Participant;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/model/dms/Participant$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/model/dms/Participant;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/twitter/model/dms/Participant$b;->a()Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    return-object v0
.end method
