.class public Lcom/twitter/model/account/UserSettings;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/account/UserSettings$b;,
        Lcom/twitter/model/account/UserSettings$c;,
        Lcom/twitter/model/account/UserSettings$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/model/account/UserSettings;",
            ">;"
        }
    .end annotation
.end field

.field private static final D:Lcom/fasterxml/jackson/core/JsonFactory;


# instance fields
.field public A:Ljava/lang/String;

.field public B:Z

.field public C:Z

.field public a:J

.field public b:Ljava/lang/String;

.field public c:Z

.field public final d:Z

.field public final e:Z

.field public final f:I

.field public final g:I

.field public final h:Ljava/lang/String;

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Z

.field public p:Ljava/lang/String;

.field public q:Z

.field public r:Z

.field public s:Ljava/lang/String;

.field public t:Z

.field public u:Ljava/lang/String;

.field public v:I

.field public w:Z

.field public x:Z

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/twitter/model/account/UserSettings$1;

    invoke-direct {v0}, Lcom/twitter/model/account/UserSettings$1;-><init>()V

    sput-object v0, Lcom/twitter/model/account/UserSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 80
    new-instance v0, Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-direct {v0}, Lcom/fasterxml/jackson/core/JsonFactory;-><init>()V

    sput-object v0, Lcom/twitter/model/account/UserSettings;->D:Lcom/fasterxml/jackson/core/JsonFactory;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->c:Z

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->d:Z

    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/model/account/UserSettings;->a:J

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->b:Ljava/lang/String;

    .line 155
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->e:Z

    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/account/UserSettings;->f:I

    .line 157
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/account/UserSettings;->g:I

    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->h:Ljava/lang/String;

    .line 159
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->j:Z

    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->i:Z

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->k:Z

    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->l:Z

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->m:Ljava/lang/String;

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->n:Ljava/lang/String;

    .line 165
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->o:Z

    .line 166
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->p:Ljava/lang/String;

    .line 167
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->C:Z

    .line 168
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->q:Z

    .line 169
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->s:Ljava/lang/String;

    .line 170
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->t:Z

    .line 171
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->u:Ljava/lang/String;

    .line 173
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/model/account/UserSettings;->v:I

    .line 174
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->w:Z

    .line 175
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_c

    move v0, v1

    :goto_c
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->x:Z

    .line 176
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->y:Ljava/lang/String;

    .line 177
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_d

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->r:Z

    .line 178
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->z:Ljava/lang/String;

    .line 179
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->A:Ljava/lang/String;

    .line 180
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_e

    :goto_e
    iput-boolean v1, p0, Lcom/twitter/model/account/UserSettings;->B:Z

    .line 181
    return-void

    :cond_0
    move v0, v2

    .line 151
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 152
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 155
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 159
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 160
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 161
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 162
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 165
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 167
    goto :goto_8

    :cond_9
    move v0, v2

    .line 168
    goto :goto_9

    :cond_a
    move v0, v2

    .line 170
    goto :goto_a

    :cond_b
    move v0, v2

    .line 174
    goto :goto_b

    :cond_c
    move v0, v2

    .line 175
    goto :goto_c

    :cond_d
    move v0, v2

    .line 177
    goto :goto_d

    :cond_e
    move v1, v2

    .line 180
    goto :goto_e
.end method

.method public constructor <init>(Lcom/twitter/model/account/UserSettings$a;)V
    .locals 2

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->a(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->c:Z

    .line 185
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->b(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->d:Z

    .line 186
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->c(Lcom/twitter/model/account/UserSettings$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/account/UserSettings;->a:J

    .line 187
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->d(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->b:Ljava/lang/String;

    .line 188
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->e(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->e:Z

    .line 189
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->f(Lcom/twitter/model/account/UserSettings$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/account/UserSettings;->f:I

    .line 190
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->g(Lcom/twitter/model/account/UserSettings$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/account/UserSettings;->g:I

    .line 191
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->h(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->h:Ljava/lang/String;

    .line 192
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->i(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->j:Z

    .line 193
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->j(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->i:Z

    .line 194
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->k(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->k:Z

    .line 195
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->l(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->l:Z

    .line 196
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->m(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->m:Ljava/lang/String;

    .line 197
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->n(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->n:Ljava/lang/String;

    .line 198
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->o(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->o:Z

    .line 199
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->p(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->C:Z

    .line 200
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->q(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->q:Z

    .line 201
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->r(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->s:Ljava/lang/String;

    .line 202
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->s(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->t:Z

    .line 203
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->t(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->u:Ljava/lang/String;

    .line 204
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->u(Lcom/twitter/model/account/UserSettings$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/model/account/UserSettings;->v:I

    .line 205
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->v(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->w:Z

    .line 206
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->w(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->x:Z

    .line 207
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->x(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->y:Ljava/lang/String;

    .line 208
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->y(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->r:Z

    .line 209
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->z(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->z:Ljava/lang/String;

    .line 210
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->A(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings;->A:Ljava/lang/String;

    .line 211
    invoke-static {p1}, Lcom/twitter/model/account/UserSettings$a;->B(Lcom/twitter/model/account/UserSettings$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->B:Z

    .line 212
    return-void
.end method

.method private static a(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 265
    const/16 v0, 0xa

    if-lt p0, v0, :cond_0

    .line 266
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 268
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "0%d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/twitter/model/account/UserSettings;->f:I

    invoke-static {v0}, Lcom/twitter/model/account/UserSettings;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/twitter/model/account/UserSettings;->g:I

    invoke-static {v0}, Lcom/twitter/model/account/UserSettings;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 238
    iget v1, p0, Lcom/twitter/model/account/UserSettings;->v:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/model/account/UserSettings;->v:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 246
    iget v0, p0, Lcom/twitter/model/account/UserSettings;->v:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/model/account/UserSettings;->v:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 330
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 251
    const-string/jumbo v0, "all_enabled"

    iget-object v1, p0, Lcom/twitter/model/account/UserSettings;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 255
    const-string/jumbo v0, "all"

    iget-object v1, p0, Lcom/twitter/model/account/UserSettings;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->c:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 278
    const/4 v0, 0x0

    .line 281
    :try_start_0
    new-instance v2, Ljava/io/StringWriter;

    const/16 v1, 0x200

    invoke-direct {v2, v1}, Ljava/io/StringWriter;-><init>(I)V

    .line 282
    sget-object v1, Lcom/twitter/model/account/UserSettings;->D:Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-virtual {v1, v2}, Lcom/fasterxml/jackson/core/JsonFactory;->a(Ljava/io/Writer;)Lcom/fasterxml/jackson/core/JsonGenerator;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 283
    :try_start_1
    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 284
    const-string/jumbo v0, "sleep_time"

    invoke-virtual {v1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->e(Ljava/lang/String;)V

    .line 285
    const-string/jumbo v0, "start_time"

    iget v3, p0, Lcom/twitter/model/account/UserSettings;->f:I

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 286
    const-string/jumbo v0, "enabled"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->e:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 287
    const-string/jumbo v0, "end_time"

    iget v3, p0, Lcom/twitter/model/account/UserSettings;->g:I

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 288
    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 289
    const-string/jumbo v0, "geo_enabled"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->c:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 290
    const-string/jumbo v0, "use_cookie_personalization"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->d:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 291
    const-string/jumbo v0, "protected"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->j:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 292
    const-string/jumbo v0, "discoverable_by_email"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->i:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 293
    const-string/jumbo v0, "display_sensitive_media"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->k:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 294
    const-string/jumbo v0, "discoverable_by_mobile_phone"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->l:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 295
    const-string/jumbo v0, "personalized_trends"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->C:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 296
    const-string/jumbo v0, "trend_location"

    invoke-virtual {v1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->d(Ljava/lang/String;)V

    .line 297
    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 298
    const-string/jumbo v0, "woeid"

    iget-wide v4, p0, Lcom/twitter/model/account/UserSettings;->a:J

    invoke-virtual {v1, v0, v4, v5}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 299
    const-string/jumbo v0, "name"

    iget-object v3, p0, Lcom/twitter/model/account/UserSettings;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 301
    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    .line 302
    const-string/jumbo v0, "language"

    iget-object v3, p0, Lcom/twitter/model/account/UserSettings;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const-string/jumbo v0, "screen_name"

    iget-object v3, p0, Lcom/twitter/model/account/UserSettings;->m:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const-string/jumbo v0, "allow_media_tagging"

    iget-object v3, p0, Lcom/twitter/model/account/UserSettings;->n:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const-string/jumbo v0, "email_follow_enabled"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->o:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 306
    const-string/jumbo v0, "allow_ads_personalization"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->q:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 307
    const-string/jumbo v0, "allow_dms_from"

    iget-object v3, p0, Lcom/twitter/model/account/UserSettings;->s:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const-string/jumbo v0, "smart_mute"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->t:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 309
    const-string/jumbo v0, "ranked_timeline_setting"

    iget v3, p0, Lcom/twitter/model/account/UserSettings;->v:I

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 310
    const-string/jumbo v0, "ranked_timeline_eligible"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->w:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 311
    const-string/jumbo v0, "country_code"

    iget-object v3, p0, Lcom/twitter/model/account/UserSettings;->u:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const-string/jumbo v0, "address_book_live_sync_enabled"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->x:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 313
    const-string/jumbo v0, "dm_receipt_setting"

    iget-object v3, p0, Lcom/twitter/model/account/UserSettings;->y:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    const-string/jumbo v0, "alt_text_compose_enabled"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->r:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 315
    const-string/jumbo v0, "universal_quality_filtering_enabled"

    iget-object v3, p0, Lcom/twitter/model/account/UserSettings;->z:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const-string/jumbo v0, "mention_filter"

    iget-object v3, p0, Lcom/twitter/model/account/UserSettings;->A:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const-string/jumbo v0, "allow_authenticated_periscope_requests"

    iget-boolean v3, p0, Lcom/twitter/model/account/UserSettings;->B:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 318
    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 319
    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->flush()V

    .line 320
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 324
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 322
    :goto_0
    return-object v0

    .line 321
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 322
    :goto_1
    :try_start_2
    const-string/jumbo v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 324
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_2
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 321
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 335
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 336
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->d:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 337
    iget-wide v4, p0, Lcom/twitter/model/account/UserSettings;->a:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 338
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 339
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->e:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 340
    iget v0, p0, Lcom/twitter/model/account/UserSettings;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 341
    iget v0, p0, Lcom/twitter/model/account/UserSettings;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 342
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 343
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->j:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 344
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->i:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 345
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->k:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 346
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->l:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 347
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 348
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 349
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->o:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 350
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 351
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->C:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 352
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->q:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 353
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 354
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->t:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 355
    iget v0, p0, Lcom/twitter/model/account/UserSettings;->v:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 356
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->w:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 357
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 358
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->x:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 359
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 360
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings;->r:Z

    if-eqz v0, :cond_d

    :goto_d
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 361
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 362
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 363
    return-void

    :cond_0
    move v0, v2

    .line 335
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 336
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 339
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 343
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 344
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 345
    goto :goto_5

    :cond_6
    move v0, v2

    .line 346
    goto :goto_6

    :cond_7
    move v0, v2

    .line 349
    goto :goto_7

    :cond_8
    move v0, v2

    .line 351
    goto :goto_8

    :cond_9
    move v0, v2

    .line 352
    goto :goto_9

    :cond_a
    move v0, v2

    .line 354
    goto :goto_a

    :cond_b
    move v0, v2

    .line 356
    goto :goto_b

    :cond_c
    move v0, v2

    .line 358
    goto :goto_c

    :cond_d
    move v1, v2

    .line 360
    goto :goto_d
.end method
