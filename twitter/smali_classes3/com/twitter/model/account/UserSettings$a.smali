.class public final Lcom/twitter/model/account/UserSettings$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/account/UserSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/account/UserSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private A:Ljava/lang/String;

.field private B:Z

.field private a:Z

.field private b:J

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Ljava/lang/String;

.field private s:Z

.field private t:I

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:Z

.field private x:Ljava/lang/String;

.field private y:Z

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 365
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 367
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/twitter/model/account/UserSettings$a;->b:J

    return-void
.end method

.method static synthetic A(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings$a;->A:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic B(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->B:Z

    return v0
.end method

.method static synthetic a(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->a:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/model/account/UserSettings$a;)J
    .locals 2

    .prologue
    .line 365
    iget-wide v0, p0, Lcom/twitter/model/account/UserSettings$a;->b:J

    return-wide v0
.end method

.method static synthetic d(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->d:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/model/account/UserSettings$a;)I
    .locals 1

    .prologue
    .line 365
    iget v0, p0, Lcom/twitter/model/account/UserSettings$a;->e:I

    return v0
.end method

.method static synthetic g(Lcom/twitter/model/account/UserSettings$a;)I
    .locals 1

    .prologue
    .line 365
    iget v0, p0, Lcom/twitter/model/account/UserSettings$a;->f:I

    return v0
.end method

.method static synthetic h(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings$a;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->i:Z

    return v0
.end method

.method static synthetic j(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->j:Z

    return v0
.end method

.method static synthetic k(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->k:Z

    return v0
.end method

.method static synthetic l(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->l:Z

    return v0
.end method

.method static synthetic m(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings$a;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings$a;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->o:Z

    return v0
.end method

.method static synthetic p(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->p:Z

    return v0
.end method

.method static synthetic q(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->q:Z

    return v0
.end method

.method static synthetic r(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings$a;->r:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->s:Z

    return v0
.end method

.method static synthetic t(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings$a;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic u(Lcom/twitter/model/account/UserSettings$a;)I
    .locals 1

    .prologue
    .line 365
    iget v0, p0, Lcom/twitter/model/account/UserSettings$a;->t:I

    return v0
.end method

.method static synthetic v(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->u:Z

    return v0
.end method

.method static synthetic w(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->w:Z

    return v0
.end method

.method static synthetic x(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings$a;->x:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic y(Lcom/twitter/model/account/UserSettings$a;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->y:Z

    return v0
.end method

.method static synthetic z(Lcom/twitter/model/account/UserSettings$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/twitter/model/account/UserSettings$a;->z:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 481
    iput p1, p0, Lcom/twitter/model/account/UserSettings$a;->t:I

    .line 482
    return-object p0
.end method

.method public a(Lcom/twitter/model/account/UserSettings$b;)Lcom/twitter/model/account/UserSettings$a;
    .locals 1

    .prologue
    .line 539
    if-eqz p1, :cond_0

    .line 540
    iget-boolean v0, p1, Lcom/twitter/model/account/UserSettings$b;->a:Z

    iput-boolean v0, p0, Lcom/twitter/model/account/UserSettings$a;->d:Z

    .line 541
    iget v0, p1, Lcom/twitter/model/account/UserSettings$b;->b:I

    iput v0, p0, Lcom/twitter/model/account/UserSettings$a;->e:I

    .line 542
    iget v0, p1, Lcom/twitter/model/account/UserSettings$b;->c:I

    iput v0, p0, Lcom/twitter/model/account/UserSettings$a;->f:I

    .line 544
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/twitter/model/account/UserSettings$a;->g:Ljava/lang/String;

    .line 404
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/account/UserSettings$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/account/UserSettings$c;",
            ">;)",
            "Lcom/twitter/model/account/UserSettings$a;"
        }
    .end annotation

    .prologue
    .line 529
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 530
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserSettings$c;

    .line 531
    iget-wide v2, v0, Lcom/twitter/model/account/UserSettings$c;->a:J

    iput-wide v2, p0, Lcom/twitter/model/account/UserSettings$a;->b:J

    .line 532
    iget-object v0, v0, Lcom/twitter/model/account/UserSettings$c;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/account/UserSettings$a;->c:Ljava/lang/String;

    .line 534
    :cond_0
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 397
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->a:Z

    .line 398
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 439
    iput-object p1, p0, Lcom/twitter/model/account/UserSettings$a;->m:Ljava/lang/String;

    .line 440
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 409
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->h:Z

    .line 410
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 365
    invoke-virtual {p0}, Lcom/twitter/model/account/UserSettings$a;->e()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 445
    iput-object p1, p0, Lcom/twitter/model/account/UserSettings$a;->n:Ljava/lang/String;

    .line 446
    return-object p0
.end method

.method public c(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 415
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->i:Z

    .line 416
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lcom/twitter/model/account/UserSettings$a;->r:Ljava/lang/String;

    .line 470
    return-object p0
.end method

.method public d(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 421
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->j:Z

    .line 422
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 493
    iput-object p1, p0, Lcom/twitter/model/account/UserSettings$a;->v:Ljava/lang/String;

    .line 494
    return-object p0
.end method

.method public e(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 427
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->k:Z

    .line 428
    return-object p0
.end method

.method protected e()Lcom/twitter/model/account/UserSettings;
    .locals 1

    .prologue
    .line 556
    new-instance v0, Lcom/twitter/model/account/UserSettings;

    invoke-direct {v0, p0}, Lcom/twitter/model/account/UserSettings;-><init>(Lcom/twitter/model/account/UserSettings$a;)V

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 505
    iput-object p1, p0, Lcom/twitter/model/account/UserSettings$a;->x:Ljava/lang/String;

    .line 506
    return-object p0
.end method

.method public f(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 433
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->l:Z

    .line 434
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lcom/twitter/model/account/UserSettings$a;->z:Ljava/lang/String;

    .line 518
    return-object p0
.end method

.method public g(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 451
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->o:Z

    .line 452
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/twitter/model/account/UserSettings$a;->A:Ljava/lang/String;

    .line 524
    return-object p0
.end method

.method public h(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 457
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->p:Z

    .line 458
    return-object p0
.end method

.method public i(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 463
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->q:Z

    .line 464
    return-object p0
.end method

.method public j(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 475
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->s:Z

    .line 476
    return-object p0
.end method

.method public k(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 487
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->u:Z

    .line 488
    return-object p0
.end method

.method public l(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 499
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->w:Z

    .line 500
    return-object p0
.end method

.method public m(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 511
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->y:Z

    .line 512
    return-object p0
.end method

.method public n(Z)Lcom/twitter/model/account/UserSettings$a;
    .locals 0

    .prologue
    .line 549
    iput-boolean p1, p0, Lcom/twitter/model/account/UserSettings$a;->B:Z

    .line 550
    return-object p0
.end method
