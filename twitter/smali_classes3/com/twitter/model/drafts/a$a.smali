.class public final Lcom/twitter/model/drafts/a$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/drafts/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/model/drafts/a;",
        ">;"
    }
.end annotation


# instance fields
.field a:J

.field b:J

.field c:J

.field d:J

.field e:Z

.field f:Z

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;"
        }
    .end annotation
.end field

.field h:Ljava/lang/String;

.field i:Lcom/twitter/model/geo/c;

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field k:Lcgi;

.field l:Lcom/twitter/model/core/r;

.field m:Ljava/lang/String;

.field n:Lcau;

.field o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field p:Lcom/twitter/model/timeline/ap;

.field q:Ljava/lang/String;

.field r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 212
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 199
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->g:Ljava/util/List;

    .line 213
    return-void
.end method


# virtual methods
.method public a(J)Lcom/twitter/model/drafts/a$a;
    .locals 1

    .prologue
    .line 264
    iput-wide p1, p0, Lcom/twitter/model/drafts/a$a;->a:J

    .line 265
    return-object p0
.end method

.method public a(Lcau;)Lcom/twitter/model/drafts/a$a;
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/twitter/model/drafts/a$a;->n:Lcau;

    .line 332
    return-object p0
.end method

.method public a(Lcgi;)Lcom/twitter/model/drafts/a$a;
    .locals 0

    .prologue
    .line 342
    iput-object p1, p0, Lcom/twitter/model/drafts/a$a;->k:Lcgi;

    .line 343
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/r;)Lcom/twitter/model/drafts/a$a;
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/twitter/model/drafts/a$a;->l:Lcom/twitter/model/core/r;

    .line 369
    return-object p0
.end method

.method public a(Lcom/twitter/model/drafts/a;)Lcom/twitter/model/drafts/a$a;
    .locals 2

    .prologue
    .line 238
    iget-wide v0, p1, Lcom/twitter/model/drafts/a;->b:J

    iput-wide v0, p0, Lcom/twitter/model/drafts/a$a;->a:J

    .line 239
    iget-wide v0, p1, Lcom/twitter/model/drafts/a;->e:J

    iput-wide v0, p0, Lcom/twitter/model/drafts/a$a;->b:J

    .line 240
    invoke-virtual {p1}, Lcom/twitter/model/drafts/a;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/model/drafts/a$a;->c:J

    .line 241
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->h:Ljava/lang/String;

    .line 242
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->g:Ljava/util/List;

    .line 243
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->g:Lcom/twitter/model/geo/c;

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->i:Lcom/twitter/model/geo/c;

    .line 244
    invoke-virtual {p1}, Lcom/twitter/model/drafts/a;->b()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->j:Ljava/util/List;

    .line 245
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->h:Lcgi;

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->k:Lcgi;

    .line 246
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->i:Lcom/twitter/model/core/r;

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->l:Lcom/twitter/model/core/r;

    .line 247
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->m:Ljava/lang/String;

    .line 248
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->k:Lcau;

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->n:Lcau;

    .line 249
    iget-boolean v0, p1, Lcom/twitter/model/drafts/a;->f:Z

    iput-boolean v0, p0, Lcom/twitter/model/drafts/a$a;->e:Z

    .line 250
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->l:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->o:Ljava/util/List;

    .line 251
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->m:Lcom/twitter/model/timeline/ap;

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->p:Lcom/twitter/model/timeline/ap;

    .line 252
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->o:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->r:Ljava/util/List;

    .line 253
    iget-wide v0, p1, Lcom/twitter/model/drafts/a;->p:J

    iput-wide v0, p0, Lcom/twitter/model/drafts/a$a;->d:J

    .line 254
    iget-boolean v0, p1, Lcom/twitter/model/drafts/a;->q:Z

    iput-boolean v0, p0, Lcom/twitter/model/drafts/a$a;->f:Z

    .line 255
    return-object p0
.end method

.method public a(Lcom/twitter/model/geo/c;)Lcom/twitter/model/drafts/a$a;
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/twitter/model/drafts/a$a;->i:Lcom/twitter/model/geo/c;

    .line 313
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/ap;)Lcom/twitter/model/drafts/a$a;
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Lcom/twitter/model/drafts/a$a;->p:Lcom/twitter/model/timeline/ap;

    .line 385
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/twitter/model/drafts/a$a;->h:Ljava/lang/String;

    .line 276
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;)",
            "Lcom/twitter/model/drafts/a$a;"
        }
    .end annotation

    .prologue
    .line 286
    invoke-static {p1}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->g:Ljava/util/List;

    .line 287
    return-object p0
.end method

.method public a(Ljava/util/List;J)Lcom/twitter/model/drafts/a$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;J)",
            "Lcom/twitter/model/drafts/a$a;"
        }
    .end annotation

    .prologue
    .line 318
    iput-object p1, p0, Lcom/twitter/model/drafts/a$a;->j:Ljava/util/List;

    .line 319
    iput-wide p2, p0, Lcom/twitter/model/drafts/a$a;->c:J

    .line 320
    return-object p0
.end method

.method public a(Z)Lcom/twitter/model/drafts/a$a;
    .locals 0

    .prologue
    .line 306
    iput-boolean p1, p0, Lcom/twitter/model/drafts/a$a;->e:Z

    .line 307
    return-object p0
.end method

.method public b(J)Lcom/twitter/model/drafts/a$a;
    .locals 1

    .prologue
    .line 296
    iput-wide p1, p0, Lcom/twitter/model/drafts/a$a;->b:J

    .line 297
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/twitter/model/drafts/a$a;->m:Ljava/lang/String;

    .line 326
    return-object p0
.end method

.method public b(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/twitter/model/drafts/a$a;"
        }
    .end annotation

    .prologue
    .line 374
    iput-object p1, p0, Lcom/twitter/model/drafts/a$a;->o:Ljava/util/List;

    .line 375
    return-object p0
.end method

.method public b(Z)Lcom/twitter/model/drafts/a$a;
    .locals 0

    .prologue
    .line 413
    iput-boolean p1, p0, Lcom/twitter/model/drafts/a$a;->f:Z

    .line 414
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/twitter/model/drafts/a$a;->m()Lcom/twitter/model/drafts/a;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/twitter/model/drafts/a$a;
    .locals 1

    .prologue
    .line 407
    iput-wide p1, p0, Lcom/twitter/model/drafts/a$a;->d:J

    .line 408
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/twitter/model/drafts/a$a;->q:Ljava/lang/String;

    .line 391
    return-object p0
.end method

.method public c(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/twitter/model/drafts/a$a;"
        }
    .end annotation

    .prologue
    .line 396
    iput-object p1, p0, Lcom/twitter/model/drafts/a$a;->r:Ljava/util/List;

    .line 397
    return-object p0
.end method

.method public e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 216
    iput-wide v2, p0, Lcom/twitter/model/drafts/a$a;->a:J

    .line 217
    iput-wide v2, p0, Lcom/twitter/model/drafts/a$a;->b:J

    .line 218
    iput-wide v2, p0, Lcom/twitter/model/drafts/a$a;->c:J

    .line 219
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/drafts/a$a;->g:Ljava/util/List;

    .line 220
    iput-object v1, p0, Lcom/twitter/model/drafts/a$a;->h:Ljava/lang/String;

    .line 221
    iput-object v1, p0, Lcom/twitter/model/drafts/a$a;->i:Lcom/twitter/model/geo/c;

    .line 222
    iput-object v1, p0, Lcom/twitter/model/drafts/a$a;->j:Ljava/util/List;

    .line 223
    iput-object v1, p0, Lcom/twitter/model/drafts/a$a;->k:Lcgi;

    .line 224
    iput-object v1, p0, Lcom/twitter/model/drafts/a$a;->l:Lcom/twitter/model/core/r;

    .line 225
    iput-object v1, p0, Lcom/twitter/model/drafts/a$a;->m:Ljava/lang/String;

    .line 226
    iput-object v1, p0, Lcom/twitter/model/drafts/a$a;->n:Lcau;

    .line 227
    iput-boolean v4, p0, Lcom/twitter/model/drafts/a$a;->e:Z

    .line 228
    iput-object v1, p0, Lcom/twitter/model/drafts/a$a;->o:Ljava/util/List;

    .line 229
    iput-object v1, p0, Lcom/twitter/model/drafts/a$a;->p:Lcom/twitter/model/timeline/ap;

    .line 230
    iput-object v1, p0, Lcom/twitter/model/drafts/a$a;->q:Ljava/lang/String;

    .line 231
    iput-object v1, p0, Lcom/twitter/model/drafts/a$a;->r:Ljava/util/List;

    .line 232
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/model/drafts/a$a;->d:J

    .line 233
    iput-boolean v4, p0, Lcom/twitter/model/drafts/a$a;->f:Z

    .line 234
    return-void
.end method

.method public f()J
    .locals 2

    .prologue
    .line 259
    iget-wide v0, p0, Lcom/twitter/model/drafts/a$a;->a:J

    return-wide v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/twitter/model/drafts/a$a;->h:Ljava/lang/String;

    return-object v0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 291
    iget-wide v0, p0, Lcom/twitter/model/drafts/a$a;->b:J

    return-wide v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 301
    iget-boolean v0, p0, Lcom/twitter/model/drafts/a$a;->e:Z

    return v0
.end method

.method public j()Lcau;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/twitter/model/drafts/a$a;->n:Lcau;

    return-object v0
.end method

.method public k()Lcom/twitter/model/core/r;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/twitter/model/drafts/a$a;->l:Lcom/twitter/model/core/r;

    return-object v0
.end method

.method public l()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402
    iget-object v0, p0, Lcom/twitter/model/drafts/a$a;->r:Ljava/util/List;

    return-object v0
.end method

.method protected m()Lcom/twitter/model/drafts/a;
    .locals 1

    .prologue
    .line 420
    new-instance v0, Lcom/twitter/model/drafts/a;

    invoke-direct {v0, p0}, Lcom/twitter/model/drafts/a;-><init>(Lcom/twitter/model/drafts/a$a;)V

    return-object v0
.end method
