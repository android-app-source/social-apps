.class public Lcom/twitter/model/drafts/a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/drafts/a$b;,
        Lcom/twitter/model/drafts/a$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/model/drafts/a;",
            "Lcom/twitter/model/drafts/a$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final e:J

.field public final f:Z

.field public final g:Lcom/twitter/model/geo/c;

.field public final h:Lcgi;

.field public final i:Lcom/twitter/model/core/r;

.field public final j:Ljava/lang/String;

.field public final k:Lcau;

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/twitter/model/timeline/ap;

.field public final n:Ljava/lang/String;

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public p:J

.field public q:Z

.field private r:J

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/twitter/model/drafts/a$b;

    invoke-direct {v0}, Lcom/twitter/model/drafts/a$b;-><init>()V

    sput-object v0, Lcom/twitter/model/drafts/a;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method constructor <init>(Lcom/twitter/model/drafts/a$a;)V
    .locals 4

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iget-wide v0, p1, Lcom/twitter/model/drafts/a$a;->a:J

    iput-wide v0, p0, Lcom/twitter/model/drafts/a;->b:J

    .line 137
    iget-object v0, p1, Lcom/twitter/model/drafts/a$a;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->c:Ljava/lang/String;

    .line 138
    iget-object v0, p1, Lcom/twitter/model/drafts/a$a;->g:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    .line 139
    iget-wide v0, p1, Lcom/twitter/model/drafts/a$a;->b:J

    iput-wide v0, p0, Lcom/twitter/model/drafts/a;->e:J

    .line 140
    iget-boolean v0, p1, Lcom/twitter/model/drafts/a$a;->e:Z

    iput-boolean v0, p0, Lcom/twitter/model/drafts/a;->f:Z

    .line 141
    iget-object v0, p1, Lcom/twitter/model/drafts/a$a;->i:Lcom/twitter/model/geo/c;

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->g:Lcom/twitter/model/geo/c;

    .line 142
    iget-object v0, p1, Lcom/twitter/model/drafts/a$a;->k:Lcgi;

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->h:Lcgi;

    .line 143
    iget-object v0, p1, Lcom/twitter/model/drafts/a$a;->l:Lcom/twitter/model/core/r;

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->i:Lcom/twitter/model/core/r;

    .line 144
    iget-object v0, p1, Lcom/twitter/model/drafts/a$a;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->j:Ljava/lang/String;

    .line 145
    iget-object v0, p1, Lcom/twitter/model/drafts/a$a;->n:Lcau;

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->k:Lcau;

    .line 146
    iget-object v0, p1, Lcom/twitter/model/drafts/a$a;->o:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->l:Ljava/util/List;

    .line 147
    iget-object v0, p1, Lcom/twitter/model/drafts/a$a;->p:Lcom/twitter/model/timeline/ap;

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->m:Lcom/twitter/model/timeline/ap;

    .line 148
    iget-object v0, p1, Lcom/twitter/model/drafts/a$a;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->n:Ljava/lang/String;

    .line 149
    iget-object v0, p1, Lcom/twitter/model/drafts/a$a;->r:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->o:Ljava/util/List;

    .line 150
    iget-wide v0, p1, Lcom/twitter/model/drafts/a$a;->d:J

    iput-wide v0, p0, Lcom/twitter/model/drafts/a;->p:J

    .line 151
    iget-boolean v0, p1, Lcom/twitter/model/drafts/a$a;->f:Z

    iput-boolean v0, p0, Lcom/twitter/model/drafts/a;->q:Z

    .line 152
    iget-object v0, p1, Lcom/twitter/model/drafts/a$a;->j:Ljava/util/List;

    iget-wide v2, p1, Lcom/twitter/model/drafts/a$a;->c:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/model/drafts/a;->a(Ljava/util/List;J)Lcom/twitter/model/drafts/a;

    .line 153
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 156
    iget-wide v0, p0, Lcom/twitter/model/drafts/a;->r:J

    return-wide v0
.end method

.method public declared-synchronized a(Ljava/util/List;J)Lcom/twitter/model/drafts/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;J)",
            "Lcom/twitter/model/drafts/a;"
        }
    .end annotation

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The lists of media and IDs should have equal size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->s:Ljava/util/List;

    .line 171
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/model/drafts/a;->r:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    :goto_0
    monitor-exit p0

    return-object p0

    .line 173
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/drafts/a;->s:Ljava/util/List;

    .line 174
    iput-wide p2, p0, Lcom/twitter/model/drafts/a;->r:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lcom/twitter/model/drafts/a;->s:Ljava/util/List;

    return-object v0
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 181
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 182
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v2, v0, v1}, Lcom/twitter/model/drafts/a;->a(Ljava/util/List;J)Lcom/twitter/model/drafts/a;

    .line 183
    iget-object v0, p0, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 184
    invoke-virtual {v0, v2}, Lcom/twitter/model/drafts/DraftAttachment;->a(Lcom/twitter/model/drafts/DraftAttachment;)V

    goto :goto_0

    .line 186
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 4

    .prologue
    .line 189
    iget-wide v0, p0, Lcom/twitter/model/drafts/a;->p:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
